﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ServiceModel;
//using log4net;
using System.Configuration;
using DigiPhoto.DataLayer;
using DigiPhoto.Common;
using DigiPhoto.IMIX.Business;



namespace DigiPhoto.DataSync.Controller
{
    public class ServiceProxy<T>
    {
        //public static ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        public static void Use(Action<T> action)
        {

            string DataSyncServiceURl = string.Empty;
            //BasicHttpBinding binding = new BasicHttpBinding();
            //NetTcpBinding binding = new NetTcpBinding();
            if (string.IsNullOrEmpty(DataSyncServiceURl))
            {
                //ConfigBusiness _objDataServices = new ConfigBusiness();
                //DataSyncServiceURl = _objDataServices.GetOnlineConfigData(ConfigParams.DgServiceURL, LoginUser.SubStoreId);
                DataSyncServiceURl = (new ConfigBusiness()).GetOnlineConfigData(ConfigParams.DgServiceURL, LoginUser.SubStoreId);
                if (DataSyncServiceURl != null)
                    DataSyncServiceURl = DataSyncServiceURl.Trim();
            }
            string apiLocation = DataSyncServiceURl;

            dynamic binding;
            if (apiLocation.ToLower().Contains("net.tcp"))
            {
                binding = new NetTcpBinding();
                binding.TransferMode = TransferMode.StreamedResponse;
                binding.Security.Mode = SecurityMode.None;
            }
            else
            {
                binding = new BasicHttpBinding();
            }

            //ConfigurationManager.AppSettings["API_URL"].ToString();

            if (typeof(T).Name.StartsWith("I"))
            {
                apiLocation = apiLocation + "/" + typeof(T).Name.Substring(1) + ".svc";
            }
            else
            {
                apiLocation = apiLocation + "/" + typeof(T).Name + ".svc";
            }

            EndpointAddress ep = new EndpointAddress(apiLocation);
            string dataSize = string.Empty;
            if (ConfigurationManager.AppSettings["ServiceDataSize"] == null)
            {
                dataSize = Convert.ToString(1024 * 65535);
            }
            else
            {
                dataSize = ConfigurationManager.AppSettings["ServiceDataSize"];
            }
            binding.MaxReceivedMessageSize = Convert.ToInt64(dataSize);
            binding.ReaderQuotas.MaxArrayLength = Convert.ToInt32(dataSize);
            binding.ReaderQuotas.MaxStringContentLength = Convert.ToInt32(dataSize);
            binding.MaxBufferSize = Convert.ToInt32(dataSize);
            binding.OpenTimeout = new TimeSpan(0, 0, 10);
            binding.CloseTimeout = new TimeSpan(0, 0, 10);
            binding.SendTimeout = new TimeSpan(0, 0, 90);   //most important
            binding.ReceiveTimeout = new TimeSpan(0, 0, 10);

            ChannelFactory<T> factory = new ChannelFactory<T>(binding, ep);
            T client = factory.CreateChannel();

            bool sucess = false;

            try
            {
                action(client);
                ((IClientChannel)client).Close();
                factory.Close();
                sucess = true;
            }
            catch (CommunicationException cex)
            {

                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(cex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);

                throw cex;
            }
            catch (TimeoutException tex)
            {

                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(tex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);

                //log.Error(tex);
                throw tex;
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);

                //log.Error(ex);
                throw ex;
            }
            finally
            {
                if (!sucess)
                {
                    //Abort the Channel if it didn't close sucessfully
                    ((IClientChannel)client).Abort();
                    factory.Abort();
                }
            }
        }
    }
}
