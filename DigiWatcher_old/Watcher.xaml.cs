﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.IO;

namespace DigiWatcherSequencial
{
    /// <summary>
    /// Interaction logic for Watcher.xaml
    /// </summary>
    public partial class Watcher : Window
    {
        public Watcher()
        {
            InitializeComponent();

        }
       public  string Directoryname;
       public List<string> _objFileList;
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            string pathtosave = Environment.CurrentDirectory;
            if (File.Exists(pathtosave + "\\watcher.dat"))
            {
                File.Delete(pathtosave + "\\watcher.dat");
            }
            using (StreamWriter b = new StreamWriter(File.Open(pathtosave + "\\watcher.dat", FileMode.Create)))
            {
                DirectoryInfo _objmain = new DirectoryInfo(Directoryname + "\\Download\\");
                foreach (var fileitem in _objmain.GetFiles())
                {
                    if (!_objFileList.Contains(fileitem.Name))
                    {
                        b.Write(fileitem.Name + ',');
                        fileitem.CopyTo(Directoryname + "\\PendingItems\\" + fileitem.Name);
                    }
                }
                b.Close();
            }
            this.Close();
        }
    }
}
