﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Windows;
using System.Diagnostics;
using System.Windows.Media.Animation;
using DigiPhoto.Cache.DataCache;
using Newtonsoft.Json;
using System.Windows.Documents;
using System.Web;
using System.Web.Script;
using System.Web.Script.Serialization;
using Newtonsoft.Json.Linq;

namespace DigiWatcherSequencial
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        #region ADDED bY AJAY
        private List jsonAsList = new List();
        #endregion
        private void Application_Startup(object sender, StartupEventArgs e)
        {
            ErrorHandler.ErrorHandler.LogFileWrite("File Watcher Step 1. startup :" + DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss"));
            DataCacheFactory.Register();
            Timeline.DesiredFrameRateProperty.OverrideMetadata(typeof(Timeline),
                               new FrameworkPropertyMetadata { DefaultValue = 5 });
            // Get Reference to the current Process
            Process thisProc = Process.GetCurrentProcess();
            // Check how many total processes have the same name as the current one
            if (Process.GetProcessesByName(thisProc.ProcessName).Length > 1)
            {
                // If there is more than one, then it is already running.
                MessageBox.Show("Digiwatcher is already running.");
                Application.Current.Shutdown();
                return;
            }
            ErrorHandler.ErrorHandler.LogFileWrite("File Watcher Step 2. startup :" + DateTime.Now.ToString("MM / dd / yyyy HH: mm:ss"));
            #region Add Keys Dynamically by AJay on  1 June
            //READ THE CONFIG FILE.
            //string ConfigPath = @"C:\Program Files (x86)\iMix";
            //var directory = ConfigPath + "\\ConfiDetails\\ConfigDetailsJSON-Digiwatcher.json";
            //string jrf = System.IO.File.ReadAllText(directory);
            //JavaScriptSerializer serializer = new JavaScriptSerializer();
            //jsonAsList = serializer.Deserialize<List>(jrf.ToString());
            //var jsonObj = JsonConvert.DeserializeObject<JObject>(jrf).First.First;
            //System.Configuration.Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);

            //if (ConfigurationManager.AppSettings.AllKeys.Contains("LocationIds") == false)
            //{           
            //    string LocationIds = Convert.ToString(jsonObj["LocationIds"]);

            //    config.AppSettings.Settings.Add("LocationIds", LocationIds);
            //    // Save the changes in App.config file.
            //    config.Save(ConfigurationSaveMode.Full);
            //    ConfigurationManager.RefreshSection("appSettings");
            //}

            //if (ConfigurationManager.AppSettings.AllKeys.Contains("GumballScoreSeperater") == false)
            //{              
            //    string GumballScoreSeperater = Convert.ToString(jsonObj["GumballScoreSeperater"]);                
            //    config.AppSettings.Settings.Add("GumballScoreSeperater", GumballScoreSeperater);
            //    // Save the changes in App.config file.
            //    config.Save(ConfigurationSaveMode.Full);
            //    ConfigurationManager.RefreshSection("appSettings");
            //}

            //if (ConfigurationManager.AppSettings.AllKeys.Contains("RotationAngle") == false)
            //{
            //    string RotationAngle = Convert.ToString(jsonObj["RotationAngle"]);
            //    config.AppSettings.Settings.Add("RotationAngle", RotationAngle);
            //    // Save the changes in App.config file.
            //    config.Save(ConfigurationSaveMode.Full);
            //    ConfigurationManager.RefreshSection("appSettings");
            //}

            #endregion
        }
    }
}
