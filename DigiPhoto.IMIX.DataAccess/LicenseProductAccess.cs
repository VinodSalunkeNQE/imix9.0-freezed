﻿using DigiPhoto.IMIX.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DigiPhoto.IMIX.DataAccess
{
    public class LicenseProductAccess : BaseDataAccess
    {
        public LicenseProductAccess(BaseDataAccess baseaccess) : base(baseaccess)
        {

        }

        public LicenseUserDetails GetLicenseDetails(int photoId) {
            
            DBParameters.Clear();
            AddParameter("PhotoId", photoId);
            System.Data.IDataReader reader = ExecuteReader(DAOConstant.USP_GET_LicenseDetails);

            if (reader.Read())
            {
                LicenseUserDetails lud = new LicenseUserDetails()
                {
                    ID = GetFieldValue(reader, "ID", 0),
                    Age = GetFieldValue(reader, "Age", 0),
                    LicensePrefix = GetFieldValue(reader, "LicensePrefix", 0),
                    LicenseSufix = GetFieldValue(reader, "LicenseSufix", 0),                    
                    Name = GetFieldValue(reader, "GuestName", string.Empty),
                    ExpiryDate = GetFieldValue(reader, "ExpiryDate", DateTime.MinValue),
                    IssueDate = GetFieldValue(reader, "IssueDate", DateTime.MinValue),
                    PofIssue = GetFieldValue(reader, "PofIssue", string.Empty)
                };
                reader.Close();
                return (lud);
            }
            return (null);
        }
        public string GetLicenseNumber(int photoId)
        {
            string licenseNumber = string.Empty;
            DBParameters.Clear();
            AddParameter("PhotoId", photoId);
            System.Data.IDataReader reader = ExecuteReader(DAOConstant.USP_GET_NextLicenseNumber);
            if(reader.Read())
            {
                licenseNumber = GetLicenseNumberFromPrefixSufix(GetFieldValue(reader, "licensePrefix", 0), GetFieldValue(reader, "licenseSufix", 0));
                reader.Close();
                return (licenseNumber);
            }            
            return string.Empty;
        }

        public bool IsLicenseProduct(int borderId)
        {
            //if (borderId == null) return (false);
            bool isLicenseProduct = false;
            DBParameters.Clear();
            AddParameter("@BorderId", borderId);
            object objScalar = ExecuteScalar(DAOConstant.USP_GET_IsLicenseProduct);
            isLicenseProduct = objScalar == null ? false : (bool)objScalar;
            return isLicenseProduct;
        }

        public string GetProductTypeIdByBorderId(int borderId)
        {
            DBParameters.Clear();
            AddParameter("@BorderId", borderId);
            object objScalar = ExecuteScalar(DAOConstant.USP_GET_ProductTypeId);
            return(objScalar == null ? string.Empty : (string)objScalar);
        }

        public void UpdateLicenseDetails(int photoId, string name, int age, int licensePrefix, int licenseSufix, DateTime issueDate, DateTime expireDate)
        {
            DBParameters.Clear();
            AddParameter("@PhotoId", photoId);
            AddParameter("@Name", name);
            AddParameter("@Age", age);
            AddParameter("@IssueDate", issueDate);
            AddParameter("@ExpiryDate", expireDate);        
            AddParameter("@LicensePrefix", licensePrefix);
            AddParameter("@LicenseSufix", licenseSufix);
            ExecuteNonQuery(DAOConstant.USP_UPD_LicenseDetails);
        }

        public bool IsLicenseDetailsRemoved(int photoId)
        {
            DBParameters.Clear();
            AddParameter("@PhotoId", photoId);
            object obj = ExecuteScalar(DAOConstant.USP_GET_IsLicenseDetailsRemoved);
            return (obj == null ? false : (bool)obj);
        }

        //public void LicenseDetailsToggleOnCanvas(int photoId,bool isRemoved )
        public void LicenseDetailsToggleOnCanvas(bool isRemoved, int? photoId=null)
        {
            DBParameters.Clear();
            AddParameter("@PhotoId", photoId);
            AddParameter("@ToggleRemoved", isRemoved);
            ExecuteNonQuery(DAOConstant.USP_UPD_LicenseDetailsToggleRemoved);
        }

        public static string GetLicenseNumberFromPrefixSufix(int prefix, int sufix)
        {
            //Start..Added by Anisur
            if (prefix == -1)
                prefix = 0;
            if (sufix == -1)
                sufix = 0;
            //End
            string licenseNo = new String('0', (3 - Convert.ToString(prefix).Length)) + Convert.ToString(prefix);
            licenseNo += "-";            
            licenseNo += new String('0', (6 - Convert.ToString(sufix).Length)) + Convert.ToString(sufix);
            return (licenseNo);
        }

    }
}
