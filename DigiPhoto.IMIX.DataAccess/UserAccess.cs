﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DigiPhoto.IMIX.Model;
using System.Data;

namespace DigiPhoto.IMIX.DataAccess
{
    public class UserAccess : BaseDataAccess
    {
        #region Constructor
        public UserAccess(BaseDataAccess baseaccess): base(baseaccess)
        {

        }
        public UserAccess()
        { }
        #endregion

        #region Public Functions
        public List<UserInfo> GetPhotoGraphersList()
        {
            DBParameters.Clear();
            IDataReader sqlReader = ExecuteReader("GetPhotoGrapher");
            List<UserInfo> photographerList = PopulatePhotographerList(sqlReader);
            sqlReader.Close();
            return photographerList;
        }
        #endregion

        #region Private Functions
        private List<UserInfo> PopulatePhotographerList(IDataReader sqlReader)
        {
            List<UserInfo> photographerList = new List<UserInfo>();
            while(sqlReader.Read())
            {
                UserInfo photoGrapherInfo = new UserInfo();
                photoGrapherInfo.UserId = GetFieldValue(sqlReader, "DG_User_pkey", 0);
                photoGrapherInfo.Photographer = GetFieldValue(sqlReader, "Photographer", string.Empty);
                photographerList.Add(photoGrapherInfo);
            }
            return photographerList;
        }
        #endregion


    }
}
