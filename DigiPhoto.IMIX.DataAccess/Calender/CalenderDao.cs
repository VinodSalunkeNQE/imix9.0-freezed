﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DigiPhoto.IMIX.Model;
using System.Data;


namespace DigiPhoto.IMIX.DataAccess
{
    public class CalenderDao : BaseDataAccess
    {
        #region Constructor
        public CalenderDao(BaseDataAccess baseaccess)
            : base(baseaccess)
        { }
        public CalenderDao()
        { }
        #endregion

        public List<ItemTemplateDetailModel> GetItemTemplateDetail()
        {
            DBParameters.Clear();
            AddParameter("@ParamDG_Itemtemplatekey", 0);
            IDataReader sqlReader = ExecuteReader(DAOConstant.USP_SEL_ItemTemplateDetail);
            List<ItemTemplateDetailModel> lstItemTemplateDetails = MapItemTemplateDetails(sqlReader);
            sqlReader.Close();
            return lstItemTemplateDetails;
        }
        private List<ItemTemplateDetailModel> MapItemTemplateDetails(IDataReader sqlReader)
        {
            List<ItemTemplateDetailModel> GetItemTemplateDetailList = new List<ItemTemplateDetailModel>();
            while (sqlReader.Read())
            {
                ItemTemplateDetailModel GetItemTemplateDetail = new ItemTemplateDetailModel()
                {
                    Id = GetFieldValue(sqlReader, "Id", 0),
                    MasterTemplateId = GetFieldValue(sqlReader, "MasterTemplateId", 0),
                    TemplateType = GetFieldValue(sqlReader, "TemplateType", 0),
                    FileName = GetFieldValue(sqlReader, "FileName", string.Empty).Replace(".jpg", "").Replace(".JPG", ""),
                    FilePath = GetFieldValue(sqlReader, "FilePath", string.Empty),
                    FileOrder = GetFieldValue(sqlReader, "FileOrder", 0),
                    I1_X1 = GetFieldValue(sqlReader, "I1_X1", 0),
                    I1_X2 = GetFieldValue(sqlReader, "I1_X2", 0),
                    I1_Y1 = GetFieldValue(sqlReader, "I1_Y1", 0),
                    I1_Y2 = GetFieldValue(sqlReader, "I1_Y2", 0)
                };
                GetItemTemplateDetailList.Add(GetItemTemplateDetail);
            }
            return GetItemTemplateDetailList;
        }

        public List<ItemTemplateMasterModel> GetItemTemplateMaster()
        {
            DBParameters.Clear();
            AddParameter("@ParamDG_ItemtemplateMasterkey", 0);
            IDataReader sqlReader = ExecuteReader(DAOConstant.USP_SEL_ItemTemplateMaster);
            List<ItemTemplateMasterModel> lstMapItemTemplateMaster = MapItemTemplateMaster(sqlReader);
            sqlReader.Close();
            return lstMapItemTemplateMaster;
        }
        private List<ItemTemplateMasterModel> MapItemTemplateMaster(IDataReader sqlReader)
        {
            List<ItemTemplateMasterModel> getItemTemplateMasterList = new List<ItemTemplateMasterModel>();
            while (sqlReader.Read())
            {

                ItemTemplateMasterModel getItemTemplateMaster = new ItemTemplateMasterModel()
                {
                    Id = GetFieldValue(sqlReader, "Id", 0),
                    Name = GetFieldValue(sqlReader, "Name", string.Empty),
                    Description = GetFieldValue(sqlReader, "Description", string.Empty),
                    SubtemplateCount = GetFieldValue(sqlReader, "SubtemplateCount", 0),
                    PathType = GetFieldValue(sqlReader, "PathType", 0),
                    TemplateType = GetFieldValue(sqlReader, "TemplateType", 0),
                    ThumbnailImagePath = GetFieldValue(sqlReader, "ThumbnailImagePath", string.Empty),
                    ThumbnailImageName = GetFieldValue(sqlReader, "ThumbnailImageName", string.Empty)
                };
                getItemTemplateMasterList.Add(getItemTemplateMaster);
            }
            return getItemTemplateMasterList;
        }

        public bool AddItemTemplatePrintOrder(ItemTemplatePrintOrderModel objModel)
        {
            DBParameters.Clear();

            AddParameter("@OrderLineItemId", objModel.OrderLineItemId);
            AddParameter("@MasterTemplateId", objModel.MasterTemplateId);
            AddParameter("@DetailTemplateId", objModel.DetailTemplateId);
            AddParameter("@PrintTypeId", objModel.PrintTypeId);
            AddParameter("@PhotoId", objModel.PhotoId);
            AddParameter("@PageNo", objModel.PageNo);
            AddParameter("@PrintPosition", objModel.PrintPosition);
            AddParameter("@RotationAngle", objModel.RotationAngle);
            AddParameter("@Status", objModel.Status);
            AddParameter("@PrinterQueueId", objModel.PrinterQueueId);
            AddParameter("@CreatedBy", objModel.CreatedBy);
            ExecuteNonQuery(DAOConstant.usp_INS_ItemTemplatePrintOrder);

            return true;
        }

        public List<ItemTemplateDetailModel> GetTemplatePath(int Id)
        {
            DBParameters.Clear();
            AddParameter("@OrderLineItemId", Id);
            IDataReader sqlReader = ExecuteReader(DAOConstant.USP_SEL_GetTemplatePath);
            List<ItemTemplateDetailModel> lstItemTemplateDetails = MapPathdetails(sqlReader);
            sqlReader.Close();
            return lstItemTemplateDetails;
        }

        private List<ItemTemplateDetailModel> MapPathdetails(IDataReader sqlReader)
        {
            List<ItemTemplateDetailModel> GetItemTemplateDetailList = new List<ItemTemplateDetailModel>();
            while (sqlReader.Read())
            {
                ItemTemplateDetailModel GetItemTemplateDetail = new ItemTemplateDetailModel()
                {

                    MasterTemplateId = GetFieldValue(sqlReader, "MasterTemplateId", 0),
                    Id = GetFieldValue(sqlReader, "DetailTemplateId", 0),

                };
                GetItemTemplateDetailList.Add(GetItemTemplateDetail);
            }
            return GetItemTemplateDetailList;
        }


    }
}
