﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DigiPhoto.IMIX.DataAccess
{
    internal class DAOConstant
    {
        private static string DBO_SCHEMA_NAME = "dbo.";
        public static readonly string USP_DEL_POSDETAILBASEDONID = DBO_SCHEMA_NAME + "USP_DEL_PosDetailBasedOnID";
        public static readonly string USP_VALIDATEPOSDETAILS = DBO_SCHEMA_NAME + "ValidatePosDetails";
        public static readonly string USP_GET_CHKSPECONLINEPACKAGE = DBO_SCHEMA_NAME + "USP_GET_ChkSpecOnlinePackage";
        public static readonly string USP_GET_GETALLLOCATIONWISECONFIGPARAMS = DBO_SCHEMA_NAME + "usp_GET_GetAllLocationWiseConfigParams";
        public static readonly string USP_GET_PHOTOPLAYERSCORE = DBO_SCHEMA_NAME + "Usp_Get_PhotoPlayerScore";
        public static readonly string USP_STARTSTOPSYSTEM = DBO_SCHEMA_NAME + "usp_StartStopSystem";
        public static readonly string USP_RUNLEVELSERVICE = DBO_SCHEMA_NAME + "usp_runlevelservice";
        public static readonly string USP_GETSERVICESTATUS = DBO_SCHEMA_NAME + "usp_GetServiceStatus";
        public static readonly string USP_GETANYRUNNINGSERVICEBYPOSID = DBO_SCHEMA_NAME + "usp_GetAnyRunningServiceByPosId";
        public static readonly string USP_MAPPEDPOSDETAIL = DBO_SCHEMA_NAME + "usp_MappedPosDetail";
        public static readonly string USP_GETSYSTEMDETAIL = DBO_SCHEMA_NAME + "usp_GetSystemDetail";
        public static readonly string USP_GETSUBSTOREDETAIL = DBO_SCHEMA_NAME + "usp_GetSubStoreDetail";
        public static readonly string USP_GETSERVICEINFO = DBO_SCHEMA_NAME + "usp_GetServiceInfo";
        public static readonly string USP_CHECKRUNNINGSERVICEBYSERVICEID = DBO_SCHEMA_NAME + "usp_CheckRunningServiceByServiceId";
        public static readonly string USP_INSERTUPDATEIMIXPOSDETAIL = DBO_SCHEMA_NAME + "usp_InsertUpdateiMixPOSDetail";
        public static readonly string USP_STARTSTOPSERVICEBYPOSID = DBO_SCHEMA_NAME + "usp_StartStopServiceByPOSID";
        public static readonly string USP_GETPOSDETAIL = DBO_SCHEMA_NAME + "usp_GetPOSDetail";
        public static readonly string USP_GetPOSDetailByLocation = DBO_SCHEMA_NAME + "usp_GetPOSDetailByLocation";///added by latika 
        public static readonly string USP_SERVICESPOSDETAILS = DBO_SCHEMA_NAME + "usp_ServicesPosDetails";
        public static readonly string USP_DEL_SpecSettings = DBO_SCHEMA_NAME + "USP_DEL_SpecSettings";
        public static readonly string USP_CHECK_PRESOLDALREADYORDERED = DBO_SCHEMA_NAME + "usp_Check_PresoldAlreadyOrdered";
        public static readonly string USP_GET_CHKISAUTOPURCHASEACTIVEORNOT = DBO_SCHEMA_NAME + "USP_GET_chKIsAutoPurchaseActiveOrNot";
        public static readonly string USP_UPD_PresoldAUTOONLINEORDER = DBO_SCHEMA_NAME + "usp_UPD_PresoldAutoOnlineOrder";
        public static readonly string USP_SEL_PresoldAUTOONLINEORDER = DBO_SCHEMA_NAME + "usp_SEL_PresoldAutoOnlineOrder";
        public static readonly string USP_UPD_CHNAGETRACKINGSYNC = DBO_SCHEMA_NAME + "usp_UPD_ChangeTrackingSync";
        public static readonly string USP_UPD_CHNAGETRACKINGIMAGESYNC = DBO_SCHEMA_NAME + "usp_UPD_ChangeTrackingImageSync";
        public static readonly string USP_UPD_EMAILSTATUSORDERID = DBO_SCHEMA_NAME + "usp_UPD_EmailStatusOrderId";
        public static readonly string USP_GET_EMAILSTATUSDETAILS = DBO_SCHEMA_NAME + "usp_GET_EmailStatusDetails";
        public static readonly string USP_SEL_CHILDUSERLIST = DBO_SCHEMA_NAME + "usp_SEL_ChildUserList";
        public static readonly string USP_GETINS_USERIDSEQUENCE = DBO_SCHEMA_NAME + "usp_GETINS_UserIdSequence";
        public static readonly string USP_GET_PHOTOIDSEQUENCE = DBO_SCHEMA_NAME + "usp_GET_PhotoIdSequence";
        public static readonly string USP_SEL_CHARACHTER = DBO_SCHEMA_NAME + "usp_SEL_Charachter";
        public static readonly string USP_GET_CHARACHTERID = DBO_SCHEMA_NAME + "usp_GET_CharachterId";
        public static readonly string usp_GetMaxPhotoId = DBO_SCHEMA_NAME + "usp_GetMaxPhotoId";
        public static readonly string USP_INS_GROUPDATA = DBO_SCHEMA_NAME + "usp_INS_GroupData";
        public static readonly string USP_INS_ManualDisplayNumber = DBO_SCHEMA_NAME + "usp_InsertManualDisplayOrder"; // Added for Manual Display Number.
        public static readonly string USP_GET_RIDECAMERASETTING = DBO_SCHEMA_NAME + "usp_GET_RideCameraSetting";
        public static readonly string USP_UPDANDINS_RIDECAMERACONFIGURATION = DBO_SCHEMA_NAME + "usp_UPDANDINS_RideCameraConfiguration";
        public static readonly string USP_UPDANDINS_SEMIORDERSETTINGS = DBO_SCHEMA_NAME + "usp_UPDANDINS_SemiOrderSettings";
        public static readonly string USP_DEL_PHOTOBYNAME = DBO_SCHEMA_NAME + "usp_DEL_PhotoByName";
        public static readonly string USPCHECKUPDATES = DBO_SCHEMA_NAME + "uspCheckUpdates";
        public static readonly string USP_SEL_CAMERADETAILSBYID = DBO_SCHEMA_NAME + "usp_SEL_CameraDetailsById";
        public static readonly string USP_UPDANDINS_PRODUCTPRICINGDATA = DBO_SCHEMA_NAME + "usp_UPDANDINS_ProductPricingData";
        public static readonly string USP_GET_SEMIORDERIMAGEFORVALIDATION = DBO_SCHEMA_NAME + "usp_GET_SemiOrderImageforValidation";
        public static readonly string USP_GET_SEMIORDERIMAGEDETAIL = DBO_SCHEMA_NAME + "USP_GET_SemiOrderImageDetail";
        public static readonly string USP_SEL_GETASSOCIATEDPRINTERS = DBO_SCHEMA_NAME + "usp_SEL_GetAssociatedPrinters";
        public static readonly string USP_SEL_GETPRODUCTTYPEFORORDER = DBO_SCHEMA_NAME + "usp_SEL_GetProductTypeforOrder";
        public static readonly string USP_INS_ALBUMPRINTERPERMISSION = DBO_SCHEMA_NAME + "usp_INS_AlbumPrinterPermission";
        public static readonly string USP_GET_PHOTORFIDBYPHOTOIDLIST = DBO_SCHEMA_NAME + "usp_GET_PhotoRFIDByPhotoIdList";
        public static readonly string USP_UPD_SEMIORDERIMAGEORDERDETAILS = DBO_SCHEMA_NAME + "usp_UPD_SemiOrderImageOrderDetails";
        public static readonly string USP_INSANDDEL_PERMISSIONDATA = DBO_SCHEMA_NAME + "usp_INSANDDEL_PermissionData";
        public static readonly string USP_SEL_PRINTERQUEUEDETAILS = DBO_SCHEMA_NAME + "usp_SEL_PrinterQueueDetails";
        public static readonly string USP_SEL_PRINTJOBINFOCOLLECTION = DBO_SCHEMA_NAME + "usp_SEL_PrintJobInfoCollection";
        public static readonly string USP_SEL_ORDERDETAILEDSYNCSTAUS = DBO_SCHEMA_NAME + "usp_SEL_OrderDetailedSyncStaus";
        public static readonly string USP_DEL_DGORDERSPRODUCTTYPE = DBO_SCHEMA_NAME + "usp_DEL_DGOrdersProductType";
        public static readonly string USP_DEL_USERSROLEBYID = DBO_SCHEMA_NAME + "usp_DEL_UsersRoleById";
        public static readonly string USP_DEL_PERMISSIONDATA = DBO_SCHEMA_NAME + "USP_DEL_PERMISSIONDATA";
        public static readonly string USP_INS_REFUNDDETAILSDATA = DBO_SCHEMA_NAME + "usp_INS_RefundDetailsData";
        public static readonly string USP_INSANDDEL_REFUNDMASTERDATA = DBO_SCHEMA_NAME + "usp_INSANDDEL_RefundMasterData";
        public static readonly string USP_SEARCH_USERDETAIL = DBO_SCHEMA_NAME + "usp_SEARCH_UserDetail";
        public static readonly string USP_UPDANDINS_PRODUCTTYPEINFO = DBO_SCHEMA_NAME + "usp_UPDANDINS_ProductTypeInfo";
        public static readonly string USP_DEL_USERSBYID = DBO_SCHEMA_NAME + "usp_DEL_UsersById";
        public static readonly string USP_SEL_FILTEREDPRINTERQUEUEBYSUBSTOREID = DBO_SCHEMA_NAME + "usp_SEL_FilteredPrinterQueueBySubStoreId";
        public static readonly string USP_UPDANDINS_PRINTERDETAILS = DBO_SCHEMA_NAME + "usp_UPDANDINS_PrinterDetails";
        public static readonly string USP_SEL_ARCHIVEDETAILS = DBO_SCHEMA_NAME + "usp_SEL_ArchiveDetails";
        public static readonly string USP_SEL_ARCHIVEIMAGES = DBO_SCHEMA_NAME + "usp_SEL_ArchiveImages";
        public static readonly string USP_RPT_GETPRINTSUMMARYDETAIL = DBO_SCHEMA_NAME + "usp_RPT_GetPrintSummaryDetail";
        public static readonly string USP_SEL_USERDETAILSBYUSERID = DBO_SCHEMA_NAME + "usp_SEL_UserDetailsByUserId";
        public static readonly string USP_SEL_CHILDUSERDETAILSBYUSERID = DBO_SCHEMA_NAME + "usp_SEL_ChildUserDetailsByUserId";
        public static readonly string USP_RPT_GETPRINTSUMMARY = DBO_SCHEMA_NAME + "usp_RPT_GetPrintSummary";
        public static readonly string USP_RPT_GETPRINTEDPRODUCT = DBO_SCHEMA_NAME + "usp_RPT_GetPrintedProduct";
        public static readonly string USP_RPT_GETPHOTGRAPHERPERFORMANCE = DBO_SCHEMA_NAME + "usp_RPT_GetPhotgrapherPerformance";
        public static readonly string USP_RPT_ACTIVITYREPORTS = DBO_SCHEMA_NAME + "usp_RPT_ActivityReports";
        public static readonly string USP_RPT_OPERATORPERFORMANCEREPORT = DBO_SCHEMA_NAME + "usp_RPT_OperatorPerformanceReport";
        public static readonly string USP_RPT_TAKINGREPORT = DBO_SCHEMA_NAME + "usp_RPT_TakingReport";
        public static readonly string USP_RPT_PRODUCTSUMMARY = DBO_SCHEMA_NAME + "usp_RPT_ProductSummary";
        public static readonly string USP_RPT_IPPRINTTRACKING = DBO_SCHEMA_NAME + "usp_GET_IPPrintTracking";
        public static readonly string usp_UPDANDINS_CardTypeInfo = DBO_SCHEMA_NAME + "usp_UPDANDINS_CardTypeInfo";
        public static readonly string USP_GET_GetCameraDetails = DBO_SCHEMA_NAME + "USP_GET_GetCameraDetails";
        public static readonly string USP_GET_GetPhotoGrapherDetails = DBO_SCHEMA_NAME + "USP_GET_GetPhotoGrapherDetails";
        public static readonly string usp_GET_UserDetails = DBO_SCHEMA_NAME + "usp_GET_UserDetails";
        public static readonly string usp_UPDANDINS_CameraDetails = DBO_SCHEMA_NAME + "usp_UPDANDINS_CameraDetails";
        public static readonly string usp_GET_Configurationdata = DBO_SCHEMA_NAME + "usp_GET_Configurationdata";
        public static readonly string USP_GET_ORDERDATEBYORDERNO = DBO_SCHEMA_NAME + "usp_GET_OrderDateByOrderNo";
        public static readonly string USP_GET_EmailDetails = DBO_SCHEMA_NAME + "GetEmailDetails";
        public static readonly string USP_GET_RideCameraSettingbyId = DBO_SCHEMA_NAME + "GetRideCameraSettingbyId";
        public static readonly string USP_GET_RideCameraSetting = DBO_SCHEMA_NAME + "usp_GET_RideCameraSetting";
        //--------------------for QRcode---------------Hari-------------------------------------------------
        public static readonly string USP_QROnlineOrderImg = DBO_SCHEMA_NAME + "usp_QROnlineOrderImg";

        public static readonly string usp_SEL_SCENE = DBO_SCHEMA_NAME + "usp_SEL_SCENE";
        public static readonly string USP_INS_SCENE = DBO_SCHEMA_NAME + "USP_INS_SCENE";
        public static readonly string USP_DEL_SCENE = DBO_SCHEMA_NAME + "USP_DEL_SCENE";
        public static readonly string USP_sel_SCENEID = DBO_SCHEMA_NAME + "usp_SEL_SCENEID";
        public static readonly string USP_SEL_ALLBLANKPAGEBYTHEMEID = DBO_SCHEMA_NAME + "usp_SEL_AllBlankPageByThemeID";
        public static readonly string USP_GET_TripCameraInfoById = DBO_SCHEMA_NAME + "GetTripCameraInfoById";
        public static readonly string USP_GET_TripCamSettingsValues = DBO_SCHEMA_NAME + "usp_GET_TripCamSettingsValues";
        public static readonly string USP_GET_SavedTripCamSettingsValues = DBO_SCHEMA_NAME + "usp_GET_SavedTripCamSettingsValues";
        public static readonly string USP_ValidateTripCamRunningStatus = DBO_SCHEMA_NAME + "usp_ValidateTripCamRunningStatus";
        public static readonly string USP_ValidateCameraAvailability = DBO_SCHEMA_NAME + "usp_ValidateCameraAvailability";
        public static readonly string USP_GET_CHKISWATERMARKEDORNOT = DBO_SCHEMA_NAME + "USP_GET_chKIsWaterMarkedOrNot";
        public static readonly string USP_RPT_IPCONTENTTRACKING = DBO_SCHEMA_NAME + "GET_IPContentTracking";
        public static readonly string USP_VerifyDummyScanForCaptureDownload = DBO_SCHEMA_NAME + "uspVerifyDummyScanForCaptureDownload";
        public static readonly string USP_GET_SUBSTORENAME = DBO_SCHEMA_NAME + "usp_GET_SubStoreName";

        public static readonly string USPMASTERDATASYNCSTAUS = DBO_SCHEMA_NAME + "uspMasterDataSyncStaus";
        public static readonly string USP_DeletedOldImages_CONFIGURATIONDATA = DBO_SCHEMA_NAME + "usp_DeletedOldImages_CONFIGURATIONDATA";
        #region
        //Added by Saroj on 04-12-2019 to get the result for video products performance Report 
        public static readonly string USP_RPT_GETPHOTGRAPHERVIDEO = DBO_SCHEMA_NAME + "USP_RPT_GetPhotgrapherVideo";
        #endregion
        /// added by latika for evoucher report 25Marc20202
        /// </summary>
        public static readonly string USP_RPT_SP_EVoucherDetails = DBO_SCHEMA_NAME + "Get_EvoucherRpt";
        /// <summary>
        /// /ended by latika
        /// </summareny>


        #region Collect & Purge
        public static readonly string USP_RPT_COLLECT_ORDERDETAILEDDISCOUNTDATA = DBO_SCHEMA_NAME + "usp_RPT_COLLECT_OrderDetailedDiscountData";
        public static readonly string USP_RPT_PURGE_ORDERDETAILEDDISCOUNTDATA = DBO_SCHEMA_NAME + "usp_RPT_PURGE_OrderDetailedDiscountData";
        public static readonly string USP_RPT_COLLECT_ACTIVITYREPORTDATA = DBO_SCHEMA_NAME + "usp_RPT_COLLECT_ActivityReportData";
        public static readonly string USP_RPT_PURGE_ACTIVITYREPORTDATA = DBO_SCHEMA_NAME + "usp_RPT_PURGE_ActivityReportData";
        public static readonly string USP_RPT_COLLECT_OPERATORPERFORMANCEDATA = DBO_SCHEMA_NAME + "usp_RPT_COLLECT_OperatorPerformanceData";
        public static readonly string USP_RPT_PURGE_OPERATORPERFORMANCEDATA = DBO_SCHEMA_NAME + "usp_RPT_PURGE_OperatorPerformanceData";
        public static readonly string USP_RPT_COLLECT_FINANCIALAUDITDATA = DBO_SCHEMA_NAME + "usp_RPT_COLLECT_FinancialAuditData";
        public static readonly string USP_RPT_PURGE_FINANCIALAUDITDATA = DBO_SCHEMA_NAME + "usp_RPT_PURGE_FinancialAuditData";
        #endregion

        #region Public

        public static readonly string GETACTIVITYREPORT = DBO_SCHEMA_NAME + "GetActivityReport";
        public static readonly string PRODUCTSUMMARY = DBO_SCHEMA_NAME + "ProductSummary";
        public static readonly string USP_RPT_OPERATIONALAUDIT = DBO_SCHEMA_NAME + "usp_RPT_OperationalAudit";
        public static readonly string USP_SEL_TAKINGREPORT = DBO_SCHEMA_NAME + "usp_SEL_TakingReport";
        public static readonly string OPERATORPERFORMANCEREPORT = DBO_SCHEMA_NAME + "OperatorPerformanceReport";
        public static readonly string GETPHOTGRAPHERPERFORMANCE = DBO_SCHEMA_NAME + "GetPhotgrapherPerformance";
        public static readonly string USP_RPT_GETLOCATIONPERFORMANCE = DBO_SCHEMA_NAME + "usp_RPT_GetLocationPerformance";
        public static readonly string USP_RPT_FINANCIALAUDITTRAIL = DBO_SCHEMA_NAME + "usp_RPT_FinancialAuditTrail";
        public static readonly string USP_GET_CHECKPHOTOS = DBO_SCHEMA_NAME + "usp_GET_CheckPhotos";
        public static readonly string USP_GET_ACTIVEPRODUCTTYPEDATA = DBO_SCHEMA_NAME + "usp_GET_ActiveProductTypeData";
        public static readonly string GETLOCATIONSTOREWISE = DBO_SCHEMA_NAME + "GetLocationstoreWise";
        public static readonly string USP_SEL_PHOTOLIST = DBO_SCHEMA_NAME + "usp_SEL_PhotoList";
        public static readonly string USP_SEL_PENDINGBURNORDERS = DBO_SCHEMA_NAME + "usp_SEL_PendingBurnOrders";
        public static readonly string USP_INS_ORDERDETAIL = DBO_SCHEMA_NAME + "usp_INS_OrderDetail";
        public static readonly string USP_INS_SEMIORDERDETAIL = DBO_SCHEMA_NAME + "usp_INS_SemiOrderDetail";

        public static readonly string USP_UPD_PHOTOBYPHOTOID = DBO_SCHEMA_NAME + "usp_UPD_PhotoByPhotoId";
        public static readonly string USP_SEL_PACKAGEDETAILS = DBO_SCHEMA_NAME + "usp_SEL_PackageDetails";
        public static readonly string USP_GET_CARDIMAGELIMITBYID = DBO_SCHEMA_NAME + "usp_GET_CardImageLimitById";
        public static readonly string USP_UPD_PHOTOEFFECTSBYPHOTOSID = DBO_SCHEMA_NAME + "usp_UPD_PhotoEffectsByPhotosId";
        public static readonly string USP_GET_ASSOCIATEDPRINTERIDFROMPRODUCTTYPEID = DBO_SCHEMA_NAME + "usp_GET_AssociatedPrinterIdFromPRoductTypeId";
        public static readonly string USP_SEL_PHOTOGRAPHERSLIST = DBO_SCHEMA_NAME + "usp_SEL_PhotoGraphersList";
        public static readonly string USP_SEL_SAVEDGROUPIMAGES = DBO_SCHEMA_NAME + "usp_SEL_SavedGroupImages";
        public static readonly string GETIMAGESBYQRCODE = DBO_SCHEMA_NAME + "GetImagesBYQRCode";
        public static readonly string USP_SEL_BORDERDETAILS = DBO_SCHEMA_NAME + "usp_SEL_BorderDetails";
        public static readonly string USP_INS_ORDERLINEITEMS = DBO_SCHEMA_NAME + "USP_INS_ORDERLINEITEMS";
        public static readonly string USP_GET_PHOTODETAILSBYPHOTOID = DBO_SCHEMA_NAME + "usp_GET_PhotoDetailsbyPhotoId";
        public static readonly string USP_UPD_EFFECTSONPHOTO = DBO_SCHEMA_NAME + "usp_UPD_EffectsonPhoto";

        public static readonly string USP_UPD_UPDATEEFFECTSSPECPRINT = DBO_SCHEMA_NAME + "usp_UPD_UpdateEffectsSpecPrint";
        public static readonly string USP_UPD_UpdateOnlineQRCodeForPhoto = DBO_SCHEMA_NAME + "usp_UPD_OnlineQRCodeForPhoto";

        public static readonly string USP_GET_PRODUCTPRICING = DBO_SCHEMA_NAME + "USP_GET_ProductPricing";

        public static readonly string USP_GET_ORDERSPRODUCTTYPEBYISPACKAGE = DBO_SCHEMA_NAME + "usp_GET_OrdersProductTypeByIsPackage";
        public static readonly string USP_GET_SUBSTOREDATAISACTIVE = DBO_SCHEMA_NAME + "usp_GET_SubstoreDataIsActive";

        public static readonly string USP_SEL_LOGINUSERDEFAULTSUBSTORE = DBO_SCHEMA_NAME + "usp_SEL_LoginUserDefaultSubstore";

        public static readonly string USP_SEL_FILTEREDPRINTERQUEUEFORPRINT = DBO_SCHEMA_NAME + "usp_SEL_FilteredPrinterQueueforPrint";
        public static readonly string USP_SEL_ORDERDETAILSBYID = DBO_SCHEMA_NAME + "usp_SEL_OrderDetailsByID";
        public static readonly string USP_UPD_PRINTERQUEUE = DBO_SCHEMA_NAME + "usp_UPD_PrinterQueue";
        public static readonly string USP_UPD_READYFORPRINT = DBO_SCHEMA_NAME + "usp_UPD_ReadyForPrint";
        public static readonly string USP_SEL_PRINTERQUEUEFORPRINT = DBO_SCHEMA_NAME + "usp_SEL_PrinterQueueforPrint";
        public static readonly string USP_SEL_ASSOCIATEDPRINTERSFORPRINT = DBO_SCHEMA_NAME + "usp_SEL_AssociatedPrintersforPrint";
        public static readonly string USP_GET_SEMIORDERSETTINGS = DBO_SCHEMA_NAME + "usp_GET_SemiOrderSettings";
        public static readonly string USP_GET_CONFIGURATIONSETTINGS = DBO_SCHEMA_NAME + "usp_GET_ConfigurationSettings";
        public static readonly string USP_UPD_CONFIGURATIONBYCONFIGID = DBO_SCHEMA_NAME + "usp_UPD_ConfigurationByConfigId";
        public static readonly string USP_INS_SEMIORDERSETTINGS = DBO_SCHEMA_NAME + "usp_INS_SemiOrderSettings";
        public static readonly string USP_SEL_CONFIGURATIONVALUE = DBO_SCHEMA_NAME + "usp_SEL_ConfigurationValue";
        public static readonly string USP_UPD_CONFIGURATIONVALUE = DBO_SCHEMA_NAME + "usp_UPD_ConfigurationValue";
        public static readonly string USP_GET_CONFIGDATA = DBO_SCHEMA_NAME + "usp_GET_Configdata";

        public static readonly string USP_SEL_VALUETYPE = DBO_SCHEMA_NAME + "usp_SEL_ValueType";
        public static readonly string USP_GET_VALUETYPE = DBO_SCHEMA_NAME + "usp_GET_ValueType";

        public static readonly string USP_SEL_STORE = DBO_SCHEMA_NAME + "usp_SEL_Store";
        public static readonly string USP_GET_STORESUBSTORELOCATIONS = DBO_SCHEMA_NAME + "usp_GET_StoreSubStoreLocations";
        public static readonly string USP_INS_SUBSTORE = DBO_SCHEMA_NAME + "usp_INS_SubStore";
        public static readonly string USP_UPD_STOREQRCODEWEBURL = DBO_SCHEMA_NAME + "usp_UPD_StoreQRCodeWebUrl";
        public static readonly string USP_UPD_SUBSTORE = DBO_SCHEMA_NAME + "usp_UPD_SubStore";
        public static readonly string USP_SEL_SUBSTORELOCATIONS = DBO_SCHEMA_NAME + "usp_SEL_SubStoreLocations";
        public static readonly string USP_SEL_AVAILABLELOCATIONS = DBO_SCHEMA_NAME + "usp_SEL_AvailableLocations";
        public static readonly string USP_INS_SUBSTORELOCATION = DBO_SCHEMA_NAME + "usp_INS_SubStoreLocation";
        public static readonly string USP_INS_LOCATION = DBO_SCHEMA_NAME + "usp_INS_Location";
        public static readonly string USP_UPD_LOCATION = DBO_SCHEMA_NAME + "usp_UPD_Location";
        public static readonly string USP_SEL_LOCATION = DBO_SCHEMA_NAME + "usp_SEL_Location";
        public static readonly string USP_DEL_LOCATION = DBO_SCHEMA_NAME + "usp_DEL_Location";
        public static readonly string USP_SEL_SUBSTOREDATA = DBO_SCHEMA_NAME + "usp_SEL_SubStoreData";
        public static readonly string USP_SEL_LOCATION_ASSOCIATION = DBO_SCHEMA_NAME + "usp_SEL_Location_Association";
        public static readonly string USP_SEL_SITE_ASSOCIATION = DBO_SCHEMA_NAME + "USP_SEL_SITE_ASSOCIATION";
        public static readonly string usp_SEL_SearchBy = DBO_SCHEMA_NAME + "usp_SEL_SearchBy"; ///added by latika 

        public static readonly string USP_GET_USERDETAILSBYUSERNAMEANDPASSWORD = DBO_SCHEMA_NAME + "usp_GET_UserDetailsbyUserNameAndPassword";
        public static readonly string USP_SEL_USERDETAILS = DBO_SCHEMA_NAME + "usp_SEL_UserDetails";
        public static readonly string USP_INS_USER = DBO_SCHEMA_NAME + "usp_INS_User";
        public static readonly string USP_UPD_USERLOCATION = DBO_SCHEMA_NAME + "usp_UPD_UserLocation";
        public static readonly string USP_UPD_USER = DBO_SCHEMA_NAME + "usp_UPD_User";
        public static readonly string USP_SEL_PHOTOGRAPHER = DBO_SCHEMA_NAME + "usp_SEL_Photographer";
        public static readonly string USP_INS_ROLE = DBO_SCHEMA_NAME + "usp_INS_Role";
        public static readonly string USP_SEL_ROLE = DBO_SCHEMA_NAME + "usp_SEL_Role";
        public static readonly string USP_SEL_ROLEPERMISSIONS = DBO_SCHEMA_NAME + "usp_SEL_RolePermissions";
        public static readonly string USP_SEL_PERMISSION = DBO_SCHEMA_NAME + "usp_SEL_Permission";
        public static readonly string USP_GetEmplyeeName = DBO_SCHEMA_NAME + "usp_GetEmplyeeName";//BY KCB ON 20 JUN 2020 to fetch employee name from dg_users table
        public static readonly string USP_INS_ROLEPERMISSION = DBO_SCHEMA_NAME + "usp_INS_RolePermission";
        public static readonly string USP_GET_PERMISSION_ROLE = DBO_SCHEMA_NAME + "usp_GET_Permission_Role";

        public static readonly string USP_INS_DG_BACKGROUND = DBO_SCHEMA_NAME + "usp_INS_DG_BackGround";
        public static readonly string USP_SEL_BACKGROUND = DBO_SCHEMA_NAME + "usp_SEL_Background";
        public static readonly string USP_DEL_BACKGROUND = DBO_SCHEMA_NAME + "usp_DEL_Background";
        public static readonly string USP_DEL_BACKGROUNDBYPRODUCTIDGROUPID = DBO_SCHEMA_NAME + "usp_DEL_BackgroundByProductIdGroupId";
        public static readonly string USP_SEL_BACKGROUNDBYGROUPBYID = DBO_SCHEMA_NAME + "usp_SEL_BackgroundByGroupById";
        public static readonly string USP_UPD_BACKGROUND = DBO_SCHEMA_NAME + "usp_UPD_Background";

        public static readonly string USP_INS_BORDERS = DBO_SCHEMA_NAME + "usp_INS_Borders";
        public static readonly string USP_SEL_BORDERS = DBO_SCHEMA_NAME + "usp_SEL_Borders";
        public static readonly string USP_UPD_BORDERS = DBO_SCHEMA_NAME + "usp_UPD_Borders";
        public static readonly string USP_DEL_BORDER = DBO_SCHEMA_NAME + "usp_DEL_Border";

        public static readonly string USP_SEL_CURRENCY = DBO_SCHEMA_NAME + "usp_SEL_Currency";
        public static readonly string USP_INS_CURRENCY = DBO_SCHEMA_NAME + "usp_INS_Currency";
        public static readonly string USP_UPD_CURRENCY = DBO_SCHEMA_NAME + "usp_UPD_Currency";
        public static readonly string USP_DEL_CURRENCY = DBO_SCHEMA_NAME + "usp_DEL_Currency";
        public static readonly string USP_UPD_CURRENCYDEFAULTBYCURRENCYID = DBO_SCHEMA_NAME + "usp_UPD_CurrencyDefaultByCurrencyId";

        public static readonly string USP_INS_DISCOUNTTYPE = DBO_SCHEMA_NAME + "usp_INS_DiscountType";
        public static readonly string USP_DEL_DISCOUNTTYPE = DBO_SCHEMA_NAME + "usp_DEL_DiscountType";
        public static readonly string USP_UPD_DISCOUNTTYPE = DBO_SCHEMA_NAME + "usp_UPD_DiscountType";
        public static readonly string USP_SEL_DISCOUNTTYPE = DBO_SCHEMA_NAME + "usp_SEL_DiscountType";

        public static readonly string USP_INS_GRAPHICS = DBO_SCHEMA_NAME + "usp_INS_Graphics";
        public static readonly string USP_DEL_GRAPHICS = DBO_SCHEMA_NAME + "usp_DEL_Graphics";
        public static readonly string USP_SEL_GRAPHICS = DBO_SCHEMA_NAME + "usp_SEL_Graphics";
        public static readonly string USP_UPD_GRAPHICS = DBO_SCHEMA_NAME + "usp_UPD_Graphics";

        public static readonly string USP_SEL_RIDECAMERA = DBO_SCHEMA_NAME + "usp_SEL_RideCamera";
        public static readonly string USP_SEL_CAMERADETAILS = DBO_SCHEMA_NAME + "usp_SEL_CameraDetails";
        public static readonly string USP_INS_CAMERA = DBO_SCHEMA_NAME + "usp_INS_Camera";
        public static readonly string USP_DEL_CAMERA = DBO_SCHEMA_NAME + "usp_DEL_Camera";
        public static readonly string USP_SEL_CAMERA = DBO_SCHEMA_NAME + "usp_SEL_Camera";
        public static readonly string USP_UPD_CAMERADETAILS = DBO_SCHEMA_NAME + "usp_UPD_CameraDetails";
        public static readonly string USP_UPD_CAMERA_ID = DBO_SCHEMA_NAME + "usp_UPD_Camera_ID";

        public static readonly string USP_SEL_PRODUCTTYPE = DBO_SCHEMA_NAME + "usp_SEL_ProductType";
        public static readonly string USP_SEL_PRODUCTPRICING = DBO_SCHEMA_NAME + "usp_SEL_ProductPricing";
        public static readonly string USP_UPD_PRODUCT_PRICING = DBO_SCHEMA_NAME + "usp_UPD_Product_Pricing";
        public static readonly string USP_UPD_ORDER_PRODUCTTYPE = DBO_SCHEMA_NAME + "usp_UPD_Order_ProductType";
        public static readonly string USP_SEL_ORDERS_PRODUCTTYPE = DBO_SCHEMA_NAME + "usp_SEL_Orders_ProductType";
        public static readonly string USP_SEL_PRODUCTTYPEDATA = DBO_SCHEMA_NAME + "usp_SEL_ProductTypeData";
        public static readonly string usp_SEL_ProductTypeDataSearch = DBO_SCHEMA_NAME + "usp_SEL_ProductTypeDataSearch";///added by latika 
        public static readonly string USP_GET_ORDERPRODUCTTYPEBYNAME = DBO_SCHEMA_NAME + "usp_GET_OrdersProductTypeByName";
        public static readonly string USP_SEL_PRODUCTTYPEBYISPACKAGE = DBO_SCHEMA_NAME + "usp_SEL_ProductTypeByIsPackage ";
        public static readonly string USP_DEL_PACKAGEDETAILSBYPACKAGEDETAILSID = DBO_SCHEMA_NAME + "usp_DEL_PackageDetailsByPackageDetailsId";

        public static readonly string USP_GET_PRINTERDETAILSBYSUBSTOREID = DBO_SCHEMA_NAME + "usp_GET_PrinterDetailsBySubStoreID";
        public static readonly string USP_INS_ASSOCIATEDPRINTERS = DBO_SCHEMA_NAME + "usp_INS_AssociatedPrinters";
        public static readonly string USP_SEL_ASSOCIATEDPRINTERINFO = DBO_SCHEMA_NAME + "usp_SEL_AssociatedPrinterInfo";
        public static readonly string USP_SEL_ASSOCIATEDPRINTERS = DBO_SCHEMA_NAME + "usp_SEL_AssociatedPrinters";
        public static readonly string USP_DEL_ASSOCIATEDPRINTERS = DBO_SCHEMA_NAME + "usp_DEL_AssociatedPrinters ";
        public static readonly string USP_DEL_ASSOCIATEDPRINTERSBYID = DBO_SCHEMA_NAME + "usp_DEL_AssociatedPrintersById";
        public static readonly string USP_SEL_RECEIPTPRINTERBYSUBSTOREID = DBO_SCHEMA_NAME + "usp_SEL_ReceiptPrinterBySubStoreId";

        public static readonly string USP_INS_PHOTOS = DBO_SCHEMA_NAME + "usp_INS_Photos";
        public static readonly string USP_SEL_PHOTOS = DBO_SCHEMA_NAME + "usp_SEL_Photos";
        public static readonly string USP_SEL_PHOTOGROUP = DBO_SCHEMA_NAME + "usp_SEL_PhotoGroup";
        public static readonly string USP_DEL_PHOTOGROUP = DBO_SCHEMA_NAME + "usp_DEL_PhotoGroup ";
        public static readonly string USP_INS_PHOTOGROUP = DBO_SCHEMA_NAME + "usp_INS_PhotoGroup";
        public static readonly string USP_SEL_PHOTOGROUPIMAGE = DBO_SCHEMA_NAME + "usp_SEL_PhotoGroupImage";
        public static readonly string USP_DEL_PHOTOGROUPBYSUBSTOREID = DBO_SCHEMA_NAME + "usp_DEL_PhotoGroupBySubStoreId";
        public static readonly string USP_DEL_MONTHOLDPHOTOGROUPBYSUBSTOREID = DBO_SCHEMA_NAME + "usp_DEL_MonthOldPhotoGroupBySubStoreId";
        public static readonly string USP_SEL_ASSOCIATEDIMAGE = DBO_SCHEMA_NAME + "usp_SEL_AssociatedImage";
        public static readonly string USP_UPD_PHOTOEFFECTS = DBO_SCHEMA_NAME + "usp_UPD_PhotoEffects";
        public static readonly string USP_UPD_PHOTOLAYERING = DBO_SCHEMA_NAME + "usp_UPD_PhotoLayering";
        public static readonly string USP_UPD_PHOTOLAYERINGSTORYBOOK = DBO_SCHEMA_NAME + "usp_UPD_PhotoLayeringStorybook";

        public static readonly string USP_SEL_MODERATE_PHOTOS = DBO_SCHEMA_NAME + "usp_SEL_Moderate_Photos ";
        public static readonly string USP_SEL_MODERATEPHOTOS = DBO_SCHEMA_NAME + "usp_SEL_ModeratePhotos";

        public static readonly string USP_INS_ORDER = DBO_SCHEMA_NAME + "usp_INS_Order";
        //public static readonly string USP_INS_ORDERDETAILS = DBO_SCHEMA_NAME + "usp_INS_OrderDetails";
        public static readonly string USP_SEL_ORDERSDETAILS = DBO_SCHEMA_NAME + "usp_SEL_OrdersDetails ";
        public static readonly string USP_SEL_ORDERDETAILSFORREFUND = DBO_SCHEMA_NAME + "usp_SEL_OrderDetailsforRefund";
        public static readonly string USP_SEL_REFUND = DBO_SCHEMA_NAME + "usp_SEL_Refund";

        public static readonly string USP_SEL_PRINTERQUEUE = DBO_SCHEMA_NAME + "usp_SEL_PrinterQueue";
        public static readonly string USP_GET_PRINTERQUEUE = DBO_SCHEMA_NAME + "usp_GET_PrinterQueue";
        public static readonly string USP_INS_PRINTERQUEUE = DBO_SCHEMA_NAME + "usp_INS_PrinterQueue";
        public static readonly string USP_SEL_FILTEREDPRINTERQUEUE = DBO_SCHEMA_NAME + "usp_SEL_FilteredPrinterQueue ";

        public static readonly string USP_INS_ACTIVITY = DBO_SCHEMA_NAME + "usp_INS_Activity";
        //public static readonly string USP_INS_DG_ACTIVITY = DBO_SCHEMA_NAME + "usp_INS_DG_Activity";
        public static readonly string USP_SEL_ACTIVITYREPORTS = DBO_SCHEMA_NAME + "usp_SEL_ActivityReports";
        public static readonly string USP_SEL_DG_ACTIVITY = DBO_SCHEMA_NAME + "usp_SEL_DG_Activity";

        public static readonly string USP_INS_CASHBOX = DBO_SCHEMA_NAME + "usp_INS_CashBox";
        public static readonly string USP_SEL_CARDTYPE = DBO_SCHEMA_NAME + "usp_SEL_CardType";
        public static readonly string USP_SEL_IMIXIMAGECARDTYPE = DBO_SCHEMA_NAME + "usp_SEL_iMixImageCardType";
        public static readonly string USP_SEL_BILLFORMAT = DBO_SCHEMA_NAME + "usp_SEL_BillFormat";
        public static readonly string USP_GET_SERVERDATETIME = DBO_SCHEMA_NAME + "usp_GET_ServerDateTime";
        public static readonly string USP_SEL_RECORDSFORARCHIVE = DBO_SCHEMA_NAME + "usp_SEL_RecordsForArchive";
        public static readonly string USP_SEL_SERVICES = DBO_SCHEMA_NAME + "usp_SEL_Services";


        public static readonly string GETPRINTERQUEUEDETAILS = DBO_SCHEMA_NAME + "GetPrinterQueueDetails";
        public static readonly string GETREFUNDEDITEMS = DBO_SCHEMA_NAME + "GetRefundedItems";
        public static readonly string GETPHOTODATABYPAGE = DBO_SCHEMA_NAME + "GetPhotoDataByPage";

        public static readonly string GETALLPHOTOSFORSEARCH = DBO_SCHEMA_NAME + "GetAllPhotosforSearch";
        public static readonly string USP_SEL_PHOTOEFFECTS = DBO_SCHEMA_NAME + "usp_SEL_PhotoEffects";
        public static readonly string GETLASTGENERATEDNUMBER = DBO_SCHEMA_NAME + "GetLastGeneratedNumber";
        public static readonly string GETPRINTERQUEUEDETAILSBYORDERNO = DBO_SCHEMA_NAME + "GetPrinterQueueDetailsByOrderNo";
        public static readonly string GETPHOTOTOUPLOAD = DBO_SCHEMA_NAME + "GetPhotoToUpload";
        public static readonly string UPDATEPOSTEDORDER = DBO_SCHEMA_NAME + "UpdatePostedOrder";
        public static readonly string GETQRORBARCODEBYPHOTOID = DBO_SCHEMA_NAME + "GetQrOrBarCodeByPhotoID";
        public static readonly string GETLOCATIONSUBSTOREWISE = DBO_SCHEMA_NAME + "GetLocationSubstoreWise";
        public static readonly string USP_SEL_LISTAVAILABLELOCATIONS = DBO_SCHEMA_NAME + "usp_SEL_ListAvailableLocations";
        public static readonly string USP_SEL_GROUPEDIMAGES = DBO_SCHEMA_NAME + "usp_SEL_GroupedImages";

        public static readonly string ORDERDETAILEDSYNCSTAUS = DBO_SCHEMA_NAME + "OrderDetailedSyncStaus";
        // public static readonly string UPDATEPOSTEDORDER = DBO_SCHEMA_NAME + "UpdatePostedOrder";
        public static readonly string USP_GET_PHOTOSBYPKEY = DBO_SCHEMA_NAME + "usp_GET_PhotosByPkey";
        public static readonly string USP_GET_PHOTOSBYPKEYSTORYBOOK = DBO_SCHEMA_NAME + "usp_GET_PhotosByPkeyStoryBook";

        public static readonly string USP_GET_PHOTOSBYNAMEANDUSERID = DBO_SCHEMA_NAME + "usp_GET_PhotosByNameAndUserId";
        public static readonly string USP_SEL_ASSOCIATEDPRINTERIDFROMPRODUCTTYPEID = DBO_SCHEMA_NAME + "usp_SEL_AssociatedPrinterIdFromPRoductTypeId";
        public static readonly string USP_GET_BORDERPRODUCTWISE = DBO_SCHEMA_NAME + "usp_GET_BorderProductwise";
        public static readonly string USP_GET_ORDERDETAILSBYID = DBO_SCHEMA_NAME + "usp_GET_OrderDetailsByID";
        public static readonly string USP_GET_PRINTERNAMEFROMID = DBO_SCHEMA_NAME + "usp_GET_PrinterNameFromID";
        public static readonly string USP_GET_PRODUCTTYPEDATA = DBO_SCHEMA_NAME + "usp_GET_ProductTypeData";
        public static readonly string USP_GET_ORDERDETAILSBYPHOTOID = DBO_SCHEMA_NAME + "usp_GET_OrderDetailsByPhotoID";
        public static readonly string USP_GET_PRINTERQUEUEISREADYFORPRINT = DBO_SCHEMA_NAME + "usp_GET_PrinterQueueIsReadyForPrint";
        public static readonly string USP_GET_ORDERSNUMBER = DBO_SCHEMA_NAME + "USP_GET_OrdersNumber";
        public static readonly string USP_GET_SUBSTORE_LOCATIONBYLOCATIONID = DBO_SCHEMA_NAME + "usp_GET_SubStore_LocationByLocationId";
        public static readonly string USP_GET_STORE = DBO_SCHEMA_NAME + "usp_Get_Store";
        public static readonly string USP_GET_SEMIORDERSETTINGOLDBYSUBSTOREID = DBO_SCHEMA_NAME + "usp_GET_SemiOrderSettingOldBySubstoreId";
        public static readonly string USP_GET_USERROLESBYID = DBO_SCHEMA_NAME + "usp_GET_UserRolesById";

        public static readonly string USP_INS_SERVICES = DBO_SCHEMA_NAME + "usp_INS_Services";
        public static readonly string USP_UPD_CANCELORDER = DBO_SCHEMA_NAME + "usp_UPD_CancelOrder";

        public static readonly string USP_UPD_BURNORDERSTATUS = DBO_SCHEMA_NAME + "usp_UPD_BurnOrderStatus";
        public static readonly string USP_DEL_PHOTOGROUPBYSUBSOTREIDANDDAYS = DBO_SCHEMA_NAME + "usp_DEL_PhotoGroupBySubSotreIdAndDays";
        public static readonly string USP_TRU_EMAILSETTINGS = DBO_SCHEMA_NAME + "usp_Tru_EmailSettings";
        public static readonly string USP_INS_PRINTERLOG = DBO_SCHEMA_NAME + "usp_INS_PrinterLog";
        public static readonly string USP_SEL_LOCATIONBYUSERID = DBO_SCHEMA_NAME + "usp_SEL_LocationByUserId";

        public static readonly string USP_DEL_SUBSTORELOCATIONSBYSUBSTOREID = DBO_SCHEMA_NAME + "usp_DEL_SubStoreLocationsBySubStoreId";

        public static readonly string USP_DEL_SUBSTOREBYID = DBO_SCHEMA_NAME + "usp_DEL_SubstoreByID";
        public static readonly string USP_SEL_BACKUPSUBSTORENAMEBYSUBSTOREID = DBO_SCHEMA_NAME + "usp_SEL_BackupSubStoreNameBySubStoreId";
        //Added by Manoj at 9th Sept for Deleting row from BackupHistory table based on backupId
        public static readonly string USP_DELETEBACKUPHISTORY = DBO_SCHEMA_NAME + "DG_DeleteBackHistory";

        public static readonly string USP_GET_IMAGEIDENTIFICATIONTYPE = DBO_SCHEMA_NAME + "usp_GET_ImageIdentificationType";
        public static readonly string USP_UPD_ENCRYPTUSERSPWD = DBO_SCHEMA_NAME + "usp_UPD_EncryptUsersPwd";
        public static readonly string USP_UPD_STORE = DBO_SCHEMA_NAME + "usp_UPD_Store";
        public static readonly string USP_UPDANDINS_USER_ROLES = DBO_SCHEMA_NAME + "usp_UPDANDINS_User_Roles";

        public static readonly string USP_UPDANDINS_USERS = DBO_SCHEMA_NAME + "usp_UPDANDINS_Users";
        public static readonly string USP_UPD_IMIXIMAGECARDTYPE = DBO_SCHEMA_NAME + "usp_UPD_iMixImageCardType";
        public static readonly string USP_GET_CARDNAMEFROMID = DBO_SCHEMA_NAME + "usp_GET_CardNameFromID";
        public static readonly string USP_GET_CARDTYPELISTVIEW = DBO_SCHEMA_NAME + "usp_GET_CardTypeListView";
        public static readonly string USP_GET_CHILDPRODUCTTYPEQUANTITY = DBO_SCHEMA_NAME + "usp_GET_ChildProductTypeQuantity";
        public static readonly string USP_GET_COMPRESSIONLEVEL = DBO_SCHEMA_NAME + "usp_GET_CompressionLevel";
        public static readonly string USP_GET_COUNTRYNAME = DBO_SCHEMA_NAME + "usp_GET_CountryName";
        public static readonly string USP_GET_DEFAULTCURRENCYNAME = DBO_SCHEMA_NAME + "usp_GET_DefaultCurrencyName";
        public static readonly string USP_GET_EMAILSETTINGSDETAIL = DBO_SCHEMA_NAME + "usp_GET_EmailSettingsDetail";
        public static readonly string USP_GET_GROUPNAME = DBO_SCHEMA_NAME + "usp_GET_GroupName";
        public static readonly string USP_GET_LINEITEMDETAILS = DBO_SCHEMA_NAME + "usp_GET_LineItemDetails";
        public static readonly string USP_GET_LOCATIONLIST = DBO_SCHEMA_NAME + "usp_GET_LocationList";
        public static readonly string USP_GET_MAXQUANTITYOFITEMINAPACKAGE = DBO_SCHEMA_NAME + "usp_GET_MaxQuantityofIteminaPackage";
        public static readonly string USP_GET_MODERATEPHOTOS = DBO_SCHEMA_NAME + "usp_GET_ModeratePhotos";
        public static readonly string USP_GET_NEWCONFIGVALUES = DBO_SCHEMA_NAME + "usp_GET_NewConfigValues";
        public static readonly string USP_GET_NEXTPREVIOUSPHOTO = DBO_SCHEMA_NAME + "usp_GET_NextPreviousPhoto";
        public static readonly string USP_GET_PHOTONAMEBYPHOTOID = DBO_SCHEMA_NAME + "usp_GET_PhotoNameByPhotoID";
        public static readonly string USP_GET_QRCODEWEBURL = DBO_SCHEMA_NAME + "usp_GET_QRCodeWebUrl";
        public static readonly string USP_GET_BILLFORMAT = DBO_SCHEMA_NAME + "USP_Get_BILLFORMAT";
        public static readonly string USP_GET_VERSIONDETAILS = DBO_SCHEMA_NAME + "usp_GET_VersionDetails";
        public static readonly string USP_DEL_MODERATE_PHOTOSBYID = DBO_SCHEMA_NAME + "usp_DEL_Moderate_PhotosById";
        public static readonly string GETALLPHOTOSBYPAGE = DBO_SCHEMA_NAME + "GetAllPhotosByPage";
        public static readonly string USP_GET_BACKUPHISTORYDATA = DBO_SCHEMA_NAME + "usp_GET_BackupHistoryData";
        public static readonly string USP_SEL_ALLTABLE = DBO_SCHEMA_NAME + "USP_SEL_ALLTABLE";
        public static readonly string USP_SEL_CARDTYPELIST = DBO_SCHEMA_NAME + "usp_SEL_CardTypeList";
        public static readonly string USP_GET_SUBSTOREDATA = DBO_SCHEMA_NAME + "usp_GET_SubstoreData";
        public static readonly string USP_GET_CARDSERIES = DBO_SCHEMA_NAME + "usp_GET_CardSeries";
        public static readonly string USP_UPD_BORDERIMAGES = DBO_SCHEMA_NAME + "usp_UPD_BorderImages";
        public static readonly string USP_UPD_CROPEDPHOTOS = DBO_SCHEMA_NAME + "usp_UPD_CropedPhotos";
        public static readonly string USP_UPD_CROPEDPHOTOSSTORYBOOK = DBO_SCHEMA_NAME + "usp_UPD_CropedPhotosStoryBook";
        public static readonly string USP_UPD_GREENPHOTOS = DBO_SCHEMA_NAME + "usp_UPD_GreenPhotos";
        public static readonly string USP_UPDANDINS_SCHEDULEDCONFIG = DBO_SCHEMA_NAME + "usp_UPDANDINS_ScheduledConfig";
        public static readonly string USP_UPDANDINS_CARDTYPEINFO = DBO_SCHEMA_NAME + "usp_UPDANDINS_CardTypeInfo";
        public static readonly string USP_UPDANDINS_CONFIGURATIONDATA = DBO_SCHEMA_NAME + "usp_UPDANDINS_ConfigurationData";
        public static readonly string USP_INS_EMAILSETTINGS = DBO_SCHEMA_NAME + "usp_INS_EmailSettings";
        public static readonly string USP_INS_DATATOPRINTERQUEUE = DBO_SCHEMA_NAME + "usp_INS_DataToPrinterQueue";
        public static readonly string USP_UPDANDINS_CAMERADETAILS = DBO_SCHEMA_NAME + "usp_UPDANDINS_CameraDetails";

        public static readonly string USP_GET_VALIDPREPAIDCODETYPE = DBO_SCHEMA_NAME + "usp_GET_ValidPrepaidCodeType";
        public static readonly string USP_GET_VALIDCODETYPE = DBO_SCHEMA_NAME + "usp_GET_ValidCodeType";
        public static readonly string USP_GET_UNIQUECODEEXISTS = DBO_SCHEMA_NAME + "usp_GET_UniqueCodeExists";
        public static readonly string USP_GET_SEMIORDERIMAGE = DBO_SCHEMA_NAME + "usp_GET_SemiOrderImage";
        public static readonly string USP_GET_SCANTYPES = DBO_SCHEMA_NAME + "usp_GET_ScanTypes";
        public static readonly string USP_GET_USERSPWDDETAILS = DBO_SCHEMA_NAME + "usp_GET_UsersPwdDetails";
        public static readonly string USP_GET_PHOTOSBASEDONRFID = DBO_SCHEMA_NAME + "usp_GET_PhotosBasedonRFID";
        public static readonly string USP_GET_PHOTORFIDBYPHOTOID = DBO_SCHEMA_NAME + "usp_GET_PhotoRFIDByPhotoID";
        public static readonly string USP_GET_ONLINECONFIGDATABYID = DBO_SCHEMA_NAME + "usp_GET_OnlineConfigDataById";
        public static readonly string USP_GET_ALLPHOTOSFORSEARCH = DBO_SCHEMA_NAME + "usp_GET_AllPhotosforSearch_NEW";
        public static readonly string ASSOCIATEIMAGE = DBO_SCHEMA_NAME + "AssociateImage";
        public static readonly string ASSOCIATEMOBILEIMAGE = DBO_SCHEMA_NAME + "AssociateMobileImage";
        public static readonly string USP_GET_CARDIMAGESOLDBYID = DBO_SCHEMA_NAME + "usp_GET_CardImageSoldById";
        public static readonly string USP_GET_CARDTYPES = DBO_SCHEMA_NAME + "usp_GET_CardTypes";
        public static readonly string USP_GET_CHILDPRODUCTTYPEBYID = DBO_SCHEMA_NAME + "usp_GET_ChildProductTypeById";
        public static readonly string USP_INS_IMAGEASSOCIATIONINFO = DBO_SCHEMA_NAME + "usp_INS_ImageAssociationInfo";
        public static readonly string USP_INS_MODERATEIMAGE = DBO_SCHEMA_NAME + "usp_INS_ModerateImage";
        public static readonly string USP_INS_PACKAGEDETAILS = DBO_SCHEMA_NAME + "usp_INS_PackageDetails";
        public static readonly string USP_UPDANDDEL_PACKAGEMASTERDETAILS = DBO_SCHEMA_NAME + "usp_UPDANDDEL_PackageMasterDetails";
        public static readonly string USP_INS_PHOTODETAILS = DBO_SCHEMA_NAME + "usp_INS_PhotoDetails";
        public static readonly string USP_INS_PERMISSIONDATA = DBO_SCHEMA_NAME + "usp_INS_PermissionData";
        public static readonly string USP_INS_PREVIEWCOUNTER = DBO_SCHEMA_NAME + "usp_INS_PreviewCounter";
        public static readonly string USP_SEL_CARDTYPEINFO = DBO_SCHEMA_NAME + "USP_SEL_CARDTYPEINFO";
        public static readonly string USP_GET_CARDTYPELISTDETAILS = DBO_SCHEMA_NAME + "USP_GET_CARDTYPELISTDETAILS";
        public static readonly string usp_SEL_ValueType = DBO_SCHEMA_NAME + "usp_SEL_ValueType";
        public static readonly string USP_GET_PreviewWallSubstoreWiseData = DBO_SCHEMA_NAME + "usp_GET_PreviewWallSubstoreWiseData";
        //Added  by Manoj at 14-Dec-2018 for Delivery Note
        public static readonly string USP_GETDELIVERYNOTESTATUS = "usp_GetDeliveryNoteStatus";

        public static readonly string USP_DEL_PreviewWallLocationWiseData = DBO_SCHEMA_NAME + "USP_DeleteLocationWise_iMixConfigurationLocationValue";
        public static readonly string ASSOCIATEVIDEO = DBO_SCHEMA_NAME + "AssociateVideo";
        public static readonly string USP_UPD_AND_INS_RESYNCHISTORY = DBO_SCHEMA_NAME + "usp_UPDANDINS_ResyncHistory";
        #endregion


        public static readonly string USP_UPD_PRINTERQUEUEFORREPRINT = DBO_SCHEMA_NAME + "usp_UPD_PrinterQueueForReprint";
        public static readonly string USP_UPD_PRINTCOUNTFORREPRINT = DBO_SCHEMA_NAME + "usp_UPD_PrintCountForReprint";
        public static readonly string USP_UPD_PRINTQUEUEINDEX = DBO_SCHEMA_NAME + "usp_UPD_PrintQueueIndex";
        public static readonly string USP_UPD_ARCHIVEDETAILS = DBO_SCHEMA_NAME + "usp_UPD_ArchiveDetails";

        public static readonly string USP_UPLOAD_ORDERPRODUCTTYPEDATAFROMEXCEL = DBO_SCHEMA_NAME + "usp_UPLOAD_OrderProductTypeDataFromExcel";

        #region Config
        public static readonly string USP_SEL_DIGIPHOTOCONFIGURATION = DBO_SCHEMA_NAME + "usp_SEL_DigiphotoConfiguration";
        #endregion
        public static readonly string USP_SEARCH_DETAILSBYCRITERIA = DBO_SCHEMA_NAME + "usp_SEARCH_DetailsByCriteria";
        public static readonly string USP_SEARCH_PHOTODATABYPAGE = DBO_SCHEMA_NAME + "usp_SEARCH_PhotoDataByPage";
        public static readonly string USP_SEARCH_PHOTODATABYPAGESTORYBOOK = DBO_SCHEMA_NAME + "usp_SEARCH_PhotoDataByPageStoryBook";
        //-----------Zero Search Substore Wise--------Nilesh-----Start-----
        public static readonly string USP_SEARCH_PHOTODATABYPAGE_SUBSTORE = DBO_SCHEMA_NAME + "usp_SEARCH_PhotoDataByPage_SubStore";
        //-----------Zero Search Substore Wise--------Nilesh-----Start-----
        public static readonly string USP_GET_GROUP = DBO_SCHEMA_NAME + "usp_GET_Group";

        public static readonly string usp_SELPhotoDetailsById = DBO_SCHEMA_NAME + "usp_SELPhotoDetailsById";
        public static readonly string usp_INSEmailDetails = DBO_SCHEMA_NAME + "InsertEmailDetails";
        public static readonly string usp_INS_Character = DBO_SCHEMA_NAME + "usp_INS_Character";
        public static readonly string USP_INS_GROUP = DBO_SCHEMA_NAME + "USP_UPDANDINS_Groups";
        public static readonly string usp_SEL_GROUP = DBO_SCHEMA_NAME + "USP_SEL_Group";
        public static readonly string USP_UPDANDINS_GroupProductCode = DBO_SCHEMA_NAME + "USP_UPDANDINS_GroupProductCode";
        public static readonly string USP_SEL_GroupProduct = DBO_SCHEMA_NAME + "USP_SEL_GroupProduct";
        public static readonly string USP_SEL_ItemTemplateDetail = DBO_SCHEMA_NAME + "USP_SEL_ItemTemplateDetail";
        public static readonly string USP_SEL_ItemTemplateMaster = DBO_SCHEMA_NAME + "USP_SEL_ItemTemplateMaster";
        public static readonly string usp_INS_ItemTemplatePrintOrder = DBO_SCHEMA_NAME + "usp_INS_ItemTemplatePrintOrder";
        public static readonly string USP_SEL_GetTemplatePath = DBO_SCHEMA_NAME + "usp_GetTemplatePath";
        public static readonly string USP_GET_CameraDetailsLocationWise = DBO_SCHEMA_NAME + "GetCameraDetailsLocationWise";
        public const string USP_FetchReportFormat = "DG_GetReportFormat";
        public const string usp_SEL_GetStoreConfigData = "usp_SEL_GetStoreConfigData";
        public const string GetReportTypes = "GetReportTypes";
        public const string SaveUpdateNewStoreConfig = "SaveUpdateNewStoreConfig";
        public const string SaveExportReportStatusLog = "SaveExportReportStatusLog";
        public const string GetExportReportLog = "GetExportReportLog";
        public const string UpdateRportType = "UpdateRportType";
        public const string UpdateReportType = "[UpdateReportType]";
        public const string RFIDEnabledColumn = "IsLocationRFIDEnabled";


        public static readonly string USP_GET_SUBSTOREDATAISACTIVEForGrid = DBO_SCHEMA_NAME + "usp_GET_SubstoreDataIsActiveForGrid";
        public static readonly string USP_GETSITECODE = DBO_SCHEMA_NAME + "usp_GetSiteCode";
        public static readonly string usp_SEL_LogicalSubStoreData = DBO_SCHEMA_NAME + "usp_SEL_LogicalSubStoreData";
        public static readonly string USP_SEL_LogicalSubStore = DBO_SCHEMA_NAME + "usp_SEL_LogicalSubStore";
        public static readonly string SETPRINTSERVER = DBO_SCHEMA_NAME + "setPrintServer";

        public static readonly string USP_SEL_CHANGETRACKINGPROCESSINGSTATUSDTL = DBO_SCHEMA_NAME + "usp_SEL_ChangeTrackingProcessingStatusDtl";


        #region Added on 11 April 18 by Ajay for panorama productlist 
        public static readonly string GetPanoramaProductDimension = DBO_SCHEMA_NAME + "GetPanoramaProductDimension";
        public static readonly string USP_ADUPD_Photo_ProductID = DBO_SCHEMA_NAME + "USP_ADUPD_Photo_ProductID";
        public static readonly string usp_DG_GetOrderProductName =  "usp_DG_GetOrderProductName";
        public static readonly string usp_DG_GetPhoto_ProductId = "usp_DG_GetPhoto_ProductId";
        public static readonly string usp_GET_PhotosByName = "usp_GET_PhotosByName";
        #endregion
        #region Added by Latika on 26 Dec 18

        public static readonly string USP_GET_GetCameraDetailSearch = DBO_SCHEMA_NAME + "USP_GET_GetCameraDetailSearch";

        #endregion
        #region Added on 6-Apr-18 by Vishal for license product
        public static readonly string USP_GET_LicenseDetails = DBO_SCHEMA_NAME + "usp_GET_LicenseDetails";
        public static readonly string USP_GET_NextLicenseNumber = DBO_SCHEMA_NAME + "usp_GET_NextLicenseNumber";
        public static readonly string USP_GET_IsLicenseProduct = DBO_SCHEMA_NAME + "usp_GET_IsLicenseProduct";
        public static readonly string USP_GET_ProductTypeId = DBO_SCHEMA_NAME + "usp_GET_ProductTypeId";
        public static readonly string USP_UPD_LicenseDetails = DBO_SCHEMA_NAME + "usp_UPD_LicenseDetails";
        public static readonly string USP_UPD_LicenseDetailsToggleRemoved = DBO_SCHEMA_NAME + "usp_UPD_LicenseDetailsToggleRemoved";
        public static readonly string USP_GET_IsLicenseDetailsRemoved = DBO_SCHEMA_NAME + "usp_GET_IsLicenseDetailsRemoved";
        #endregion

        #region Added by Monika 10-july-2019
        public static readonly string USP_OperatorGrp = DBO_SCHEMA_NAME + "USP_OperatorGrp";
        #endregion

        #region Added by Vinod Salunke for SET 3
        public static readonly string GETPHOTOSTOUPLOADAZURE = DBO_SCHEMA_NAME + "GetPhotosToUploadAzure";
        #endregion
        #region Added by Suraj Mali for SET 3
        public static readonly string GETEditedPhotosToUploadAzure = DBO_SCHEMA_NAME + "GetEditedPhotosToUploadAzure";
        public static readonly string GETPHOTOSRETRYCOUNT = DBO_SCHEMA_NAME + "GetPhotosToUploadAzure";
        public static readonly string GETAssciateImagesDetails = DBO_SCHEMA_NAME + "GetAssociateImagesDetails";
        public static readonly string GETiMixImageAssociationDetails = DBO_SCHEMA_NAME + "GetiMixImagesAssociationDetails";
        public static readonly string GETProductNameDetails = DBO_SCHEMA_NAME + "GetProductNameDetails";
        public static readonly string GETSiteNameDetails = DBO_SCHEMA_NAME + "GetSiteNameDetails";
        #endregion

        #region Added by Ardsad.
        public static readonly string SAPOnlinePushData = "usp_GET_SAPOnlineDataPush";
        public static readonly string SAPPushDataintoTable = "usp_SAPOnlineDataPush";
        #endregion
        #region Added by Ajinkya
        public static readonly string USP_SEL_Theme = DBO_SCHEMA_NAME + "USP_SEL_Theme";
        public static readonly string usp_SEL_StoryBookDetail = DBO_SCHEMA_NAME + "usp_SEL_StoryBookDetail";
        public static readonly string usp_SEL_SCENEFromThemeID = DBO_SCHEMA_NAME + "usp_SEL_SCENEFromThemeID";
        public static readonly string usp_SEL_SCENEFromSceneID = DBO_SCHEMA_NAME + "usp_SEL_SCENEFromSceneID";
        public static readonly string usp_GET_AllStoryBookPhotosByStoryBookId = DBO_SCHEMA_NAME + "usp_GET_AllStoryBookPhotosByStoryBookId";
        public static readonly string usp_UPD_RestoreStorybook = DBO_SCHEMA_NAME + "usp_UPD_RestoreStorybook";
        #endregion

    }
}
