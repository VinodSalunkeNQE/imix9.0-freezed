﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DigiPhoto.IMIX.DataAccess
{
    internal class DBConstant
    {
        public static DateTime SQL_DATE_MIN_VALUE = new DateTime(1753, 1, 1);
        public const string ReportColumn = "ReportColumn";
        public const string DataColumn = "DataColumn";
        public const string ReportTypeColumn = "ReportType";
        public const string USP_FetchReportFormatParam = "@reportTypeId";
        public const string PositionColumn = "Position";
    }
}
