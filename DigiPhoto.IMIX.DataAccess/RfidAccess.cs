﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DigiPhoto.IMIX.Model;
using System.Data;

namespace DigiPhoto.IMIX.DataAccess
{
    public class RfidAccess : BaseDataAccess
    {
        #region Constructor
        public RfidAccess(BaseDataAccess baseaccess)
            : base(baseaccess)
        {

        }

        public RfidAccess()
        {

        }
        #endregion

        public bool SaveRfidTag(RFIDTagInfo rfidTag)
        {
            //SaveRfidTag(@serialNo VARCHAR(50),@TagId VARCHAR(50),@ScanningTime DATETIME,@RawData VARCHAR(500))
            DBParameters.Clear();
            AddParameter("@serialNo", rfidTag.SerialNo);
            AddParameter("@TagId", rfidTag.TagId);
            AddParameter("@ScanningTime", rfidTag.ScanningTime);
            AddParameter("@RawData", rfidTag.RawData);
            ExecuteNonQuery("SaveRfidTag");
            return true;

        }
//////////////////created by latika
        public bool AssociateRFIDtoPhotosAdvanceTableFlow(DataTable dt_Rfid, string SerailNo)
        {
            DBParameters.Clear();
            AddParameter("@Rfid", dt_Rfid);
            AddParameter("@SerialNo", SerailNo);
            ExecuteNonQuery("SaveRfidTagAdvanceTableWorkFlow");
            return true;
        }
/////////////for table flow end
        public bool AssociateRFIDtoPhotosAdvance(DataTable dt_Rfid, string SerailNo)
        {
            DBParameters.Clear();
            AddParameter("@Rfid", dt_Rfid);
            AddParameter("@SerialNo", SerailNo);
            ExecuteNonQuery("SaveRfidTagAdvance");
            return true;
        }

        public List<RFIDField> GetSubStoreList()
        {
            DBParameters.Clear();
            IDataReader sqlReader = ExecuteReader("GetSubStoreList");
            List<RFIDField> lstRFIDField = PopulateRFIDSubStoreList(sqlReader);
            sqlReader.Close();
            return lstRFIDField;
        }

        private List<RFIDField> PopulateRFIDSubStoreList(IDataReader dataReader)
        {
            List<RFIDField> lstRFIDHotFolderLst = new List<RFIDField>();
            while (dataReader.Read())
            {
                RFIDField RFIDHotFolderInfo = new RFIDField();
                RFIDHotFolderInfo.DeviceID = GetFieldValue(dataReader, "DeviceId", 0);
                RFIDHotFolderInfo.SerialNo = GetFieldValue(dataReader, "SerialNo", string.Empty);
                RFIDHotFolderInfo.SubStoreID = GetFieldValue(dataReader, "DG_Substore_ID", 0);
                RFIDHotFolderInfo.HotFolderpath = GetFieldValue(dataReader, "DG_Hot_Folder_Path", string.Empty);

                lstRFIDHotFolderLst.Add(RFIDHotFolderInfo);
            }
            return lstRFIDHotFolderLst;
        }
        public bool AssociateRFIDtoPhotos()
        {
            DBParameters.Clear();
            //AddParameter("@SubStoreId", subStoreId);
            ExecuteNonQuery("[dbo].[uspRFIDAssociation]");
            return true;
        }

        public bool ArchiveRFIDTags(int subStoreId)
        {
            DBParameters.Clear();
            AddParameter("@SubStoreId", subStoreId);
            ExecuteNonQuery("[dbo].[ArchiveRFIDTags]");
            return true;
        }

        public List<RFIDImageAssociationInfo> GetRFIDAssociationSearch(int photoGrapherId, int deviceId, DateTime dtFrom, DateTime dtTo)
        {
            DBParameters.Clear();
            AddParameter("@PhotographerId", photoGrapherId);
            AddParameter("@DeviceId", deviceId);
            AddParameter("@dtFrom", dtFrom);
            AddParameter("@dtTo", dtTo);
            IDataReader sqlReader = ExecuteReader("GetAssociatedRFIDImageInfo");
            List<RFIDImageAssociationInfo> lstRFIDAssociationInfo = PopulateRFIDAssociationList(sqlReader);
            sqlReader.Close();
            return lstRFIDAssociationInfo;
        }

        public List<PhotoDetail> GetRFIDNotAssociatedPhotos(int photoGrapherId, DateTime dtFrom, DateTime dtTo)
        {

            DBParameters.Clear();
            AddParameter("@PhotographerId", photoGrapherId);
            AddParameter("@dtFrom", dtFrom);
            AddParameter("@dtTo", dtTo);
            IDataReader sqlReader = ExecuteReader("GetNonRFIDAssociatedImages");
            List<PhotoDetail> lstRFIDAssociationInfo = PopulatePhotoDetailsList(sqlReader);
            sqlReader.Close();

            return lstRFIDAssociationInfo;
        }
        public List<PhotoDetail> GetRFIDNotAssociatedPhotos(RFIDPhotoDetails RFIDPhotoObj)
        {

            DBParameters.Clear();
            AddParameter("@PhotographerId", RFIDPhotoObj.PhotoGrapherId);
            AddParameter("@dtFrom", RFIDPhotoObj.StartDate);
            AddParameter("@dtTo", RFIDPhotoObj.EndDate);
            IDataReader sqlReader = ExecuteReader("GetNonRFIDAssociatedImages");
            List<PhotoDetail> lstRFIDAssociationInfo = PopulatePhotoDetailsListFileName(sqlReader, RFIDPhotoObj.FileName);
            sqlReader.Close();

            return lstRFIDAssociationInfo;
        }
        private List<PhotoDetail> PopulatePhotoDetailsList(IDataReader sqlReader)
        {
            List<PhotoDetail> photoList = new List<PhotoDetail>();
            while (sqlReader.Read())
            {
                PhotoDetail photoDetail = new PhotoDetail();
                photoDetail.PhotoId = GetFieldValue(sqlReader, "PhotoId", 0);
                photoDetail.FileName = GetFieldValue(sqlReader, "FileName", string.Empty);
                photoDetail.DG_Photos_CreatedOn = GetFieldValue(sqlReader, "CreatedDate", new DateTime());
                photoDetail.PhotoRFID = GetFieldValue(sqlReader, "PhotoRFID", string.Empty);
                photoList.Add(photoDetail);
            }
            return photoList;
        }
        public bool SaveDummyRFIDTagsInfo(RFIDTagInfo rfidTag)
        {
            DBParameters.Clear();
            AddParameter("@DummyRFIDTagID", rfidTag.DummyRFIDTagID);
            AddParameter("@TagID", rfidTag.TagId);
            AddParameter("@IsActive", rfidTag.IsActive);
            ExecuteNonQuery("SaveUpdateDummyRFIDTags");
            return true;

        }
        public List<RFIDTagInfo> GetDummyRFIDTagsInfo(int dummyRFIDTagID)
        {

            DBParameters.Clear();
            AddParameter("@DummyRFIDTagID", dummyRFIDTagID);
            IDataReader sqlReader = ExecuteReader("GetDummyRFIDTagsInfo");
            List<RFIDTagInfo> lstDummyRFIDTagsInfo = PopulateDummyRFIDTags(sqlReader);
            sqlReader.Close();

            return lstDummyRFIDTagsInfo;
        }

        public bool DeleteDummyRFIDTagsInfo(int dummyRFIDTagID)
        {
            DBParameters.Clear();
            AddParameter("@DummyRFIDTagID", dummyRFIDTagID);
            ExecuteNonQuery("DeleteDummyRFIDTagsInfo");
            return true;
        }

        public bool MapNonAssociatedImages(string cardUniqueIdentifier, string photoIDS)
        {
            DBParameters.Clear();
            AddParameter("@CardUniqueIdentifier", cardUniqueIdentifier);
            AddParameter("@PhotoIDs", photoIDS);
            ExecuteNonQuery("dbo.Map_NonAssociatedImages");
            return true;

        }
        public int? GetPhotographerRFIDEnabledLocation(int photographerID)
        {
            int? locationID = null;
            DBParameters.Clear();
            AddParameter("@PhotographerID", photographerID);
            IDataReader dataReader = ExecuteReader("uspGetPhotographerRFIDEnabledLocation");
            if (dataReader.Read())
            {
                locationID = GetFieldValue(dataReader, "PhotographerLocation", 0);
            }
            return locationID;
        }
        public List<RfidFlushHistotyInfo> GetFlushHistoryData(int SubStoreId, DateTime fromDate, DateTime toDate)
        {
            DBParameters.Clear();
            AddParameter("@SubStoreId", SubStoreId);
            AddParameter("@fromDate", fromDate);
            AddParameter("@toDate", toDate);
            IDataReader sqlReader = ExecuteReader("GetRfidFlushHistory");
            List<RfidFlushHistotyInfo> lstRFIDFlush = PopulateFlushHistoryData(sqlReader);
            sqlReader.Close();
            return lstRFIDFlush;
        }
        public List<LocationInfo> GetAllLocations()
        {

            DBParameters.Clear();
            IDataReader sqlReader = ExecuteReader("dbo.GetAllLocations");
            List<LocationInfo> lstLocationInfo = PopulateLocationList(sqlReader);
            sqlReader.Close();

            return lstLocationInfo;
        }
        public List<LocationInfo> PopulateLocationList(IDataReader sqlReader)
        {
            List<LocationInfo> locationList = new List<LocationInfo>();
            LocationInfo location = null;

            while (sqlReader.Read())
            {
                location = new LocationInfo();
                location.DG_Location_ID = GetFieldValue(sqlReader, "LocationID", 0);
                location.DG_Location_Name = GetFieldValue(sqlReader, "LocationName", string.Empty);
                locationList.Add(location);
            }
            return locationList;
        }
        public bool SaveSeperatorRFIDTagsInfo(SeperatorTagInfo seperatorTagInfo)
        {
            DBParameters.Clear();
            AddParameter("@SeparatorRFIDTagID", seperatorTagInfo.SeparatorRFIDTagID);
            AddParameter("@TagID", seperatorTagInfo.TagID);
            AddParameter("@LocationID", seperatorTagInfo.LocationId);
            AddParameter("@IsActive", seperatorTagInfo.IsActive);
            ExecuteNonQuery("uspSaveSeperatorTag");
            return true;

        }
        public bool DeleteSeperatorRFIDTagsInfo(int seperatorRFIDTagID)
        {
            DBParameters.Clear();
            AddParameter("@SeperatorRFIDTagID", seperatorRFIDTagID);
            ExecuteNonQuery("DeleteSeperatorRFIDTagsInfo");
            return true;
        }
        public List<SeperatorTagInfo> GetSeperatorRFIDTagsInfo(int seperatorRFIDTagID)
        {

            DBParameters.Clear();
            List<SeperatorTagInfo> lstSeperatorRFIDTagsInfo = new List<SeperatorTagInfo>();
            AddParameter("@SeperatorRFIDTagID", seperatorRFIDTagID);
            IDataReader sqlReader = ExecuteReader("GetSeperatorRFIDTagsInfo");
            lstSeperatorRFIDTagsInfo = PopulateSeperatorTagsInfo(sqlReader);
            sqlReader.Close();
            return lstSeperatorRFIDTagsInfo;
        }
        public List<SeperatorTagInfo> PopulateSeperatorTagsInfo(IDataReader sqlReader)
        {
            List<SeperatorTagInfo> lstSeperatorRFIDTagsInfo = new List<SeperatorTagInfo>();

            SeperatorTagInfo seperatorTagInfo = null;
            while (sqlReader.Read())
            {
                seperatorTagInfo = new SeperatorTagInfo();
                seperatorTagInfo.SeparatorRFIDTagID = GetFieldValue(sqlReader, "SeparatorRFIDTagID", 0);
                seperatorTagInfo.TagID = GetFieldValue(sqlReader, "TagID", string.Empty);
                seperatorTagInfo.CreatedOn = GetFieldValue(sqlReader, "CreatedOn", DateTime.Now);
                seperatorTagInfo.LocationName = GetFieldValue(sqlReader, "LocationName", string.Empty);
                seperatorTagInfo.IsActive = GetFieldValue(sqlReader, "IsActive", false);
                lstSeperatorRFIDTagsInfo.Add(seperatorTagInfo);

            }
            return lstSeperatorRFIDTagsInfo;
        }

        public List<PhotographerRFIDAssociationInfo> GetPhotographerRFIDAssociation(int? photoGrapherId, DateTime dtFrom, DateTime dtTo)
        {
            DBParameters.Clear();
            AddParameter("@PhotographerId", photoGrapherId);
            AddParameter("@StartDate", dtFrom);
            AddParameter("@EndDate", dtTo);
            IDataReader sqlReader = ExecuteReader("uspGetPhotographerRFIDAssociationReport");
            List<PhotographerRFIDAssociationInfo> lstPhotographerRFIDAssociationInfo = PopulatePhotographerRFIDAssociationList(sqlReader);
            sqlReader.Close();
            return lstPhotographerRFIDAssociationInfo;
        }

        private List<PhotographerRFIDAssociationInfo> PopulatePhotographerRFIDAssociationList(IDataReader dataReader)
        {
            List<PhotographerRFIDAssociationInfo> lstPhotographerRFIDAssociation = new List<PhotographerRFIDAssociationInfo>();
            while (dataReader.Read())
            {
                PhotographerRFIDAssociationInfo RFIDAssociationInfo = new PhotographerRFIDAssociationInfo();
                RFIDAssociationInfo.PhotographerID = GetFieldValue(dataReader, "PhotographerID", 0);
                RFIDAssociationInfo.PhotographerName = GetFieldValue(dataReader, "PhotographerName", string.Empty);
                RFIDAssociationInfo.Location = GetFieldValue(dataReader, "Location", string.Empty);
                RFIDAssociationInfo.TotalCaptured = GetFieldValue(dataReader, "TotalCaptured", 0);
                RFIDAssociationInfo.TotalAssociated = GetFieldValue(dataReader, "TotalAssociated", 0);
                RFIDAssociationInfo.TotalNonAssociated = GetFieldValue(dataReader, "TotalNonAssociated", 0);
                RFIDAssociationInfo.LastAssociatedOn = GetFieldValue(dataReader, "LastAssociatedOn", DateTime.MinValue);
                if (RFIDAssociationInfo.LastAssociatedOn == DateTime.MinValue)
                {
                    RFIDAssociationInfo.LastAssociatedOn = null;
                }
                lstPhotographerRFIDAssociation.Add(RFIDAssociationInfo);
            }
            return lstPhotographerRFIDAssociation;
        }

        #region Private
        private List<RFIDImageAssociationInfo> PopulateRFIDAssociationList(IDataReader dataReader)
        {
            List<RFIDImageAssociationInfo> lstRFIDAssociationInfo = new List<RFIDImageAssociationInfo>();
            while (dataReader.Read())
            {
                RFIDImageAssociationInfo RFIDAssociationInfo = new RFIDImageAssociationInfo();
                RFIDAssociationInfo.DeviceId = GetFieldValue(dataReader, "DeviceId", 0);
                RFIDAssociationInfo.DeviceName = GetFieldValue(dataReader, "DeviceName", string.Empty);
                RFIDAssociationInfo.RFID = GetFieldValue(dataReader, "RFIDValue", string.Empty);
                RFIDAssociationInfo.Count = GetFieldValue(dataReader, "PhotoCount", 0);
                RFIDAssociationInfo.PhotoIds = GetFieldValue(dataReader, "PhotoIds", string.Empty);
                RFIDAssociationInfo.IsShowDetailActive = RFIDAssociationInfo.Count > 0 ? true : false;
                lstRFIDAssociationInfo.Add(RFIDAssociationInfo);
            }
            return lstRFIDAssociationInfo;
        }

        private List<PhotoDetail> PopulatePhotoDetailsListFileName(IDataReader sqlReader, string FileName)
        {
            List<PhotoDetail> photoList = new List<PhotoDetail>();
            while (sqlReader.Read())
            {
                PhotoDetail photoDetail = new PhotoDetail();
                photoDetail.PhotoId = GetFieldValue(sqlReader, "PhotoId", 0);
                photoDetail.DG_Photos_CreatedOn = GetFieldValue(sqlReader, "CreatedDate", new DateTime());
                photoDetail.FileName = System.IO.Path.Combine(GetFieldValue(sqlReader, "HotFolderPath", string.Empty), photoDetail.DG_Photos_CreatedOn.ToString("yyyyMMdd"), GetFieldValue(sqlReader, "FileName", string.Empty));
                photoDetail.PhotoRFID = GetFieldValue(sqlReader, "PhotoRFID", string.Empty);
                photoDetail.PhotoRFID = GetFieldValue(sqlReader, "PhotoRFID", string.Empty);
                photoList.Add(photoDetail);
            }
            return photoList;
        }

        private List<RFIDTagInfo> PopulateDummyRFIDTags(IDataReader dataReader)
        {
            List<RFIDTagInfo> lstRFIDTagInfo = new List<RFIDTagInfo>();
            while (dataReader.Read())
            {
                RFIDTagInfo DummyRFIDTagInfo = new RFIDTagInfo();
                DummyRFIDTagInfo.DummyRFIDTagID = GetFieldValue(dataReader, "DummyRFIDTagID", 0);
                DummyRFIDTagInfo.TagId = GetFieldValue(dataReader, "TagId", string.Empty);
                DummyRFIDTagInfo.CreatedOn = GetFieldValue(dataReader, "CreatedOn", DateTime.Now);
                DummyRFIDTagInfo.IsActive = GetFieldValue(dataReader, "IsActive", false);

                lstRFIDTagInfo.Add(DummyRFIDTagInfo);
            }
            return lstRFIDTagInfo;
        }

        private List<RfidFlushHistotyInfo> PopulateFlushHistoryData(IDataReader dataReader)
        {
            List<RfidFlushHistotyInfo> lstRFIDFlush = new List<RfidFlushHistotyInfo>();
            RfidFlushHistotyInfo RfidFlush = null;
            DateTime? pr = null;
            while (dataReader.Read())
            {
                RfidFlush = new RfidFlushHistotyInfo();
                RfidFlush.FlushId = GetFieldValue(dataReader, "FlushId", 0);
                RfidFlush.EndDate = GetFieldValue(dataReader, "EndDate", pr);
                RfidFlush.ErrorMessage = GetFieldValue(dataReader, "ErrorMessage", string.Empty);
                RfidFlush.ScheduleDate = GetFieldValue(dataReader, "ScheduleDate", pr);
                RfidFlush.StartDate = GetFieldValue(dataReader, "StartDate", pr);
                RfidFlush.Status = GetFieldValue(dataReader, "Status", 0);
                RfidFlush.SubStoreId = GetFieldValue(dataReader, "SubStoreId", 0);
                RfidFlush.SubStore = GetFieldValue(dataReader, "SubStore", string.Empty);
                RfidFlush.LocationName = GetFieldValue(dataReader, "LocationName", string.Empty);
                lstRFIDFlush.Add(RfidFlush);
            }
            return lstRFIDFlush;
        }

        public bool SaveRfidFlushHistory(RfidFlushHistotyInfo rfidFlush)
        {
            DBParameters.Clear();
            AddParameter("@SubStoreId", rfidFlush.SubStoreId);
            AddParameter("@LocationId", rfidFlush.LocationId);
            AddParameter("@ScheduleDate", rfidFlush.ScheduleDate);
            AddParameter("@status", rfidFlush.Status);
            ExecuteNonQuery("SaveRfidFlushHistory");
            return true;

        }

        public bool RfidFlushNow(int SubStoreId, int LocationId, int ExcludeDays)
        {
            DBParameters.Clear();
            AddParameter("@SubStoreId", SubStoreId);
            AddParameter("@LocId", LocationId);
            AddParameter("@ExcludeDays", ExcludeDays);
            ExecuteNonQuery("ArchiveRFIDTagsNow");
            return true;

        }
       
        #endregion
    }
}
