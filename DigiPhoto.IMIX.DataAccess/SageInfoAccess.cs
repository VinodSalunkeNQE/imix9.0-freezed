﻿using DigiPhoto.IMIX.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace DigiPhoto.IMIX.DataAccess
{
    public class SageInfoAccess : BaseDataAccess
    {
        #region Constructor
        public SageInfoAccess(BaseDataAccess baseaccess)
            : base(baseaccess)
        {

        }

        public SageInfoAccess()
        {

        }
        #endregion

        #region Public Methods
        public SageInfo GetOpenCloseProcDetail(int SubStoreID)
        {
            DBParameters.Clear();
            AddParameter("@SubStoreID", SubStoreID);

            IDataReader sqlReader = ExecuteReader("[Sage].[GetOpenCloseProcDetail]");
            SageInfo _sageInfo = PopulateSageInfo(sqlReader);
            sqlReader.Close();
            return _sageInfo;


        }


        public SageInfo PopulateSageInfo(IDataReader sqlReader)
         {
           //By KCB ON 27 MAR 2018 For optimazation code
           //SageInfo _sageInfo = null;
           SageInfo _sageInfo = new SageInfo();
           while (sqlReader.Read())
           {
               _sageInfo = new SageInfo();
              
               //_sageInfo = new SageInfo();
              //end
 
              //   [OpenCloseProcDetailID],[FormTypeID],OCPD.[SubStoreID],[FilledOn],OCPD.[FilledBy],
 		  //[6x8StartingNumber] ,[8x10StartingNumber] ,PosterStartingNumber ,[FilledOn] as TransDate 
 
                 _sageInfo.OpenCloseProcDetailID = GetFieldValue(sqlReader, "OpenCloseProcDetailID", 0L);
                 _sageInfo.FormTypeID = GetFieldValue(sqlReader, "FormTypeID", 0);
                 _sageInfo.SubStoreID = GetFieldValue(sqlReader, "SubStoreID", 0);
                 _sageInfo.FilledOn = GetFieldValue(sqlReader, "OpeningDate", Convert.ToDateTime("01/01/1753"));
                 _sageInfo.FormID = GetFieldValue(sqlReader, "FormID", 0);
                 _sageInfo.sixEightStartingNumber = GetFieldValue(sqlReader, "6x8StartingNumber", 0L);
                 _sageInfo.eightTenStartingNumber = GetFieldValue(sqlReader, "8x10StartingNumber", 0L);
                 _sageInfo.PosterStartingNumber = GetFieldValue(sqlReader, "PosterStartingNumber", 0L);
                 _sageInfo.TransDate = GetFieldValue(sqlReader, "TransDate", Convert.ToDateTime("01/01/1753"));
                 _sageInfo.BusinessDate = GetFieldValue(sqlReader, "BussinessDate", Convert.ToDateTime("01/01/1753"));
                 _sageInfo.ServerTime = GetFieldValue(sqlReader, "ServerTime", Convert.ToDateTime("01/01/1753"));
                 _sageInfo.sixEightAutoStartingNumber = GetFieldValue(sqlReader, "6X8AutoStartingNumber", 0L);
                 _sageInfo.eightTenAutoStartingNumber = GetFieldValue(sqlReader, "8X10AutoStartingNumber", 0L);
                // BY KCB ON 26 MARCH 2018 FOR storing panorama printer inventry
                if (_sageInfo.FormTypeID == 1)
                {
                    _sageInfo.eightTwentyStartingNumber = GetFieldValue(sqlReader, "820StartingNumber", 0);
                    _sageInfo.eightTwentyAutoStartingNumber = GetFieldValue(sqlReader, "820AutoStartingNumber", 0);
                    _sageInfo.sixTwentyStartingNumber = GetFieldValue(sqlReader, "620StartingNumber", 0);
                    _sageInfo.sixTwentyAutoStartingNumber = GetFieldValue(sqlReader, "620AutoStartingNumber", 0);
                }
                else
                {
                    _sageInfo.eightTwentyStartingNumber = GetFieldValue(sqlReader, "820StartingNumber", 0L);
                    _sageInfo.eightTwentyAutoStartingNumber = GetFieldValue(sqlReader, "820AutoStartingNumber", 0L);
                    _sageInfo.sixTwentyStartingNumber = GetFieldValue(sqlReader, "620StartingNumber", 0L);
                    _sageInfo.sixTwentyAutoStartingNumber = GetFieldValue(sqlReader, "620AutoStartingNumber", 0L);
                }
                //end
                //BY KCB ON 08 AUG 2019 FOR storing panorama printer inventory
                _sageInfo.K6900StartingNumber = GetFieldValue(sqlReader, "6900StartingNumber", 0L);
                _sageInfo.K6900AutoStartingNumber = GetFieldValue(sqlReader, "6900AutoStartingNumber", 0L); 
                //end

            }
             return _sageInfo;
         }
        
               

        
        public string SaveOpeningForm(SageInfo _sageInfo,DataTable dt6850, DataTable dt8810)
         {
            DBParameters.Clear();   
            
            AddParameter("@6x8StartingNumber", _sageInfo.sixEightStartingNumber);
            AddParameter("@8x10StartingNumber", _sageInfo.eightTenStartingNumber);
            AddParameter("@6x8AutoStartingNumber", _sageInfo.sixEightAutoStartingNumber);
            AddParameter("@8x10AutoStartingNumber", _sageInfo.eightTenAutoStartingNumber);            
        
          //  AddParameter("@8x10AutoStartingNumber", _sageInfo.eightTenAutoStartingNumber);
            // BY KCB ON 26 MARCH 2018 FOR storing panorama printer inventry
            AddParameter("@6x20StartingNumber", _sageInfo.sixTwentyStartingNumber);
            AddParameter("@8x20StartingNumber", _sageInfo.eightTwentyStartingNumber);
            AddParameter("@6x20AutoStartingNumber", _sageInfo.sixTwentyAutoStartingNumber);
            AddParameter("@8x20AutoStartingNumber", _sageInfo.eightTwentyAutoStartingNumber);
            //End
            // BY KCB ON 08 AUG 2019 FOR storing panorama printer inventory
            AddParameter("@6900StartingNumber", _sageInfo.K6900StartingNumber);
            AddParameter("@6900AutoStartingNumber", _sageInfo.K6900AutoStartingNumber);
            //End
            AddParameter("@PosterStartingNumber", _sageInfo.PosterStartingNumber);
             AddParameter("@CashFloatAmount", _sageInfo.CashFloatAmount);            
             AddParameter("@SubStoreID", _sageInfo.SubStoreID);
             AddParameter("@OpeningDate", _sageInfo.OpeningDate);
             AddParameter("@FilledBy", _sageInfo.FilledBy);
             AddParameter("@FormTypeID", _sageInfo.FormTypeID);
             AddParameter("@SyncCode", _sageInfo.SyncCode);
             AddParameter("@BusinessDate", _sageInfo.BusinessDate);
             AddParameter("@udt_PrinterInfo6850", dt6850);
             AddParameter("@udt_PrinterInfo8810", dt8810);
             
             object retval = ExecuteScalar("[Sage].[SaveOpeningForm]");
            string ret = string.Empty;
            if (retval != null)
            {
                ret = Convert.ToString(retval);
            }
           
             return ret;
             
         }
        public bool SaveClosingForm(SageOpenClose objOpenClose, DataTable dtInventoryConsumable, DataTable dt6850, DataTable dt8810)
        {
            SageInfoClosing _sageInfoClose = objOpenClose.objClose;
            List<TransDetail> lstTransDetail = new List<TransDetail>();
            DBParameters.Clear();

            AddParameter("@6x8ClosingNumber", _sageInfoClose.sixEightClosingNumber);
            AddParameter("@8x10ClosingNumber", _sageInfoClose.eightTenClosingNumber);
            AddParameter("@6x8AutoClosingNumber", _sageInfoClose.sixEightAutoClosingNumber);
            AddParameter("@8x10AutoClosingNumber", _sageInfoClose.eightTenAutoClosingNumber);
            AddParameter("@PosterClosingNumber", _sageInfoClose.PosterClosingNumber);
            AddParameter("@6x8Westage", _sageInfoClose.SixEightWestage);
            AddParameter("@8x10Westage", _sageInfoClose.EightTenWestage);
            AddParameter("@6x8AutoWestage", _sageInfoClose.SixEightAutoWestage);
            AddParameter("@8x10AutoWestage", _sageInfoClose.EightTenAutoWestage);
            AddParameter("@PosterWestage", _sageInfoClose.PosterWestage);
            AddParameter("@Attendance", _sageInfoClose.Attendance);
            AddParameter("@LaborHour", _sageInfoClose.LaborHour);
            AddParameter("@NoOfCapture", _sageInfoClose.NoOfCapture);
            AddParameter("@NoOfPreview", _sageInfoClose.NoOfPreview);
            AddParameter("@NoOfImageSold", _sageInfoClose.NoOfImageSold);
            AddParameter("@Comments", _sageInfoClose.Comments);
            AddParameter("@SubStoreID", _sageInfoClose.objSubStore.DG_SubStore_pkey);
            AddParameter("@ClosingDate", _sageInfoClose.ClosingDate);
            AddParameter("@FilledBy", _sageInfoClose.FilledBy);
            AddParameter("@FormTypeID", _sageInfoClose.FormTypeID);
            AddParameter("@TransDate", _sageInfoClose.TransDate);
            AddParameter("@6x8PrintCount", _sageInfoClose.SixEightPrintCount);
            AddParameter("@8x10PrintCount", _sageInfoClose.EightTenPrintCount);
            AddParameter("@PosterPrintCount", _sageInfoClose.PosterPrintCount);
            AddParameter("@SyncCode", _sageInfoClose.SyncCode);
            AddParameter("@OpeningDate", _sageInfoClose.OpeningDate);
            AddParameter("@udt_InventoryConsumable", dtInventoryConsumable);
            AddParameter("@udt_PrinterInfo6850", dt6850);
            AddParameter("@udt_PrinterInfo8810", dt8810);
            AddParameter("@NoOfTransactions", _sageInfoClose.NoOfTransactions);//By KCB ON 13 MAR 2018 FOR implementation NoOfTransaction field
            // BY KCB ON 26 MARCH 2018 FOR storing panorama printer inventry
            AddParameter("@620ClosingNumber", _sageInfoClose.SixTwentyClosingNumber);
            AddParameter("@820ClosingNumber", _sageInfoClose.EightTwentyClosingNumber);
            AddParameter("@620AutoClosingNumber", _sageInfoClose.SixTwentyAutoClosingNumber);
            AddParameter("@820AutoClosingNumber", _sageInfoClose.EightTwentyAutoClosingNumber);
            AddParameter("@620Westage", _sageInfoClose.SixTwentytWestage);
            AddParameter("@820Westage", _sageInfoClose.EightTwentyWestage);
            //end
            // BY KCB ON 08 AUG 2019 FOR storing panorama printer inventory
            AddParameter("@6900ClosingNumber", _sageInfoClose.K6900ClosingNumber);
            AddParameter("@6900AutoClosingNumber", _sageInfoClose.K6900AutoClosingNumber);
            AddParameter("@6900Westage", _sageInfoClose.K6900AutoWestage);
            //end
            IDataReader sqlReader = ExecuteReader("[Sage].[SaveClosingForm]");

            while (sqlReader.Read())
            {
                TransDetail _sageTransDetail = new TransDetail();

                // [SubstoreID],[TransDate],[PackageID],[UnitPrice],[Quantity],[Discount],NetPrice,  ClosingFormDetailID        

                _sageTransDetail.SubstoreID = GetFieldValue(sqlReader, "SubstoreID", 0);
                _sageTransDetail.TransDate = GetFieldValue(sqlReader, "TransDate", DateTime.MinValue);
                _sageTransDetail.PackageID = GetFieldValue(sqlReader, "PackageID", 0);
                _sageTransDetail.UnitPrice = GetFieldValue(sqlReader, "UnitPrice", 0.0M);
                _sageTransDetail.Quantity = GetFieldValue(sqlReader, "Quantity", 0.0M);
                _sageTransDetail.Discount = GetFieldValue(sqlReader, "Discount", 0.0M);
                _sageTransDetail.Total = GetFieldValue(sqlReader, "NetPrice", 0.0M);
                //here we are setting hte value of closing form id
                //_sageInfoClose.ClosingFormDetailID = GetFieldValue(sqlReader, "ClosingFormDetailID", 0L);
                _sageTransDetail.PackageSyncCode = GetFieldValue(sqlReader, "SyncCode", string.Empty);
                _sageTransDetail.PackageCode = GetFieldValue(sqlReader, "DG_Orders_ProductCode", string.Empty);
                lstTransDetail.Add(_sageTransDetail);
            }
            _sageInfoClose.TransDetails = lstTransDetail;
            sqlReader.NextResult();
            _sageInfoClose.objSubStore = new SubStoresInfo();
            while (sqlReader.Read())
            {
                _sageInfoClose.objSubStore.DG_SubStore_pkey = GetFieldValue(sqlReader, "DG_SubStore_pkey", 0);
                _sageInfoClose.objSubStore.DG_SubStore_Name = GetFieldValue(sqlReader, "DG_SubStore_Name", string.Empty);
                _sageInfoClose.objSubStore.DG_SubStore_Code = GetFieldValue(sqlReader, "DG_SubStore_Code", string.Empty);
                _sageInfoClose.objSubStore.SyncCode = GetFieldValue(sqlReader, "SyncCode", string.Empty);
                // _sageInfoClose.objSubStore.DG_Orders_ProductCode = GetFieldValue(sqlReader, "DG_Orders_ProductCode", string.Empty);
            }

            sqlReader.NextResult();
            _sageInfoClose.InventoryConsumable = new List<InventoryConsumables>();
            while (sqlReader.Read())
            {
                InventoryConsumables objIC = new InventoryConsumables();
                //AccessoryID,ConsumeValue,ClosingFormDetailID,SyncCode,DG_Orders_ProductCode
                objIC.AccessoryID = GetFieldValue(sqlReader, "AccessoryID", 0L);
                objIC.ConsumeValue = GetFieldValue(sqlReader, "ConsumeValue", 0L);
                //objIC.ClosingFormDetailID = GetFieldValue(sqlReader, "ClosingFormDetailID", 0L);
                objIC.AccessorySyncCode = GetFieldValue(sqlReader, "SyncCode", string.Empty);
                objIC.AccessoryCode = GetFieldValue(sqlReader, "DG_Orders_ProductCode", string.Empty);

                _sageInfoClose.InventoryConsumable.Add(objIC);
            }
            sqlReader.NextResult();
            while (sqlReader.Read())
            {
                _sageInfoClose.FilledBySyncCode = GetFieldValue(sqlReader, "FilledBySyncCode", string.Empty);
            }
            sqlReader.NextResult();
            while (sqlReader.Read())
            {
                _sageInfoClose.ClosingFormDetailID = GetFieldValue(sqlReader, "ClosingFormDetailID", 0L);
                _sageInfoClose.Cash = GetFieldValue(sqlReader, "Cash", 0.0M);
                _sageInfoClose.CreditCard = GetFieldValue(sqlReader, "CreditCard", 0.0M);
                _sageInfoClose.Amex = GetFieldValue(sqlReader, "AMEX", 0.0M);
                _sageInfoClose.FCV = GetFieldValue(sqlReader, "FCV", 0.0M);
                _sageInfoClose.RoomCharges = GetFieldValue(sqlReader, "RoomCharges", 0.0M);
                _sageInfoClose.KVL = GetFieldValue(sqlReader, "KVL", 0.0M);
                _sageInfoClose.Vouchers = GetFieldValue(sqlReader, "Vouchers", 0.0M);
            }

            //OpeningFormDetailID,6X8StartingNumber,8X10StartingNumber,PosterStartingNumber,CashFloatAmount,
            //SubStoreID,OpeningDate,FilledBy,SyncCode,BussinessDate,U.Synccode as FilledBySyncCode

            sqlReader.NextResult();
            while (sqlReader.Read())
            {
                SageInfo _sageInfoOpen = new SageInfo();

                _sageInfoOpen.OpeningFormDetailID = GetFieldValue(sqlReader, "OpeningFormDetailID", 0L);
                _sageInfoOpen.sixEightStartingNumber = GetFieldValue(sqlReader, "6X8StartingNumber", 0L);
                _sageInfoOpen.eightTenStartingNumber = GetFieldValue(sqlReader, "8X10StartingNumber", 0L);
                _sageInfoOpen.PosterStartingNumber = GetFieldValue(sqlReader, "PosterStartingNumber", 0L);
                _sageInfoOpen.CashFloatAmount = GetFieldValue(sqlReader, "CashFloatAmount", 0.0M);
                _sageInfoOpen.OpeningDate = GetFieldValue(sqlReader, "OpeningDate", Convert.ToDateTime("1/1/1753"));
                _sageInfoOpen.SyncCode = GetFieldValue(sqlReader, "SyncCode", string.Empty);
                _sageInfoOpen.BusinessDate = GetFieldValue(sqlReader, "BussinessDate", Convert.ToDateTime("1/1/1753"));
                _sageInfoOpen.FilledBySyncCode = GetFieldValue(sqlReader, "FilledBySyncCode", string.Empty);
                objOpenClose.objOpen = _sageInfoOpen;
            }


            sqlReader.Close();
            return true;
        }

        public List<SageInfoWestage> ProductTypeWestage(DateTime FromDate, DateTime ToDate, int SubStoreID)
        {
            DBParameters.Clear();
            AddParameter("@FromDate", FromDate);
            AddParameter("@ToDate", ToDate);
            AddParameter("@SubStoreID", SubStoreID);
            IDataReader sqlReader = ExecuteReader("[Sage].[ProductTypeWestage]");
            List<SageInfoWestage> lstsageInfoWest = new List<SageInfoWestage>();

            while (sqlReader.Read())
            {
                SageInfoWestage _sageInfoWest = new SageInfoWestage();
                _sageInfoWest.ProductType = GetFieldValue(sqlReader, "DG_Orders_ProductType_pkey", 0);
                _sageInfoWest.Reprint = GetFieldValue(sqlReader, "ReprintCount", 0);
                _sageInfoWest.Printed = GetFieldValue(sqlReader, "PrintedSold", 0L);
                lstsageInfoWest.Add(_sageInfoWest);
            }
            sqlReader.Close();
            return lstsageInfoWest;
        }

        public int GetCaptureBySubStoreAndDateRange(DateTime FromDate, DateTime ToDate, int SubStoreID)
        {
            DBParameters.Clear();
            AddParameter("@FromDate", FromDate);
            AddParameter("@ToDate", ToDate);
            AddParameter("@SubStoreID", SubStoreID);
            Int32 Captures = 0;
            IDataReader dataReader = ExecuteReader("[Sage].[GetCaptureBySubStoreAndDateRange]");
            if (dataReader.Read())
            {
                Captures = GetFieldValue(dataReader, "TotalQuantity", 0);
            }

            return Captures;
        }

        public int GetPreviewBySubStoreAndDateRange(DateTime FromDate, DateTime ToDate, int SubStoreID)
        {
            DBParameters.Clear();
            AddParameter("@FromDate", FromDate);
            AddParameter("@ToDate", ToDate);
            AddParameter("@SubStoreID", SubStoreID);
            Int32 Preview = 0;
            IDataReader dataReader = ExecuteReader("[Sage].[GetPreviewBySubStoreAndDateRange]");
            if (dataReader.Read())
            {
                Preview = GetFieldValue(dataReader, "TotalQuantity", 0);
            }

            return Preview;
        }

        public Int64 GetTotalSoldBySubStoreAndDateRange(DateTime FromDate, DateTime ToDate, int SubStoreID)
        {
            DBParameters.Clear();
            AddParameter("@FromDate", FromDate);
            AddParameter("@ToDate", ToDate);
            AddParameter("@SubStoreID", SubStoreID);
            Int64 ImageSold = 0;
            IDataReader dataReader = ExecuteReader("[Sage].[GetTotalSoldBySubStoreAndDateRange]");
            if (dataReader.Read())
            {
                ImageSold = GetFieldValue(dataReader, "ImageSold", 0L);
            }
            return ImageSold;
        }

        public SageInfoClosing GetRevenueDetails(DateTime FromDate, DateTime ToDate, int SubStoreID)
        {
            SageInfoClosing sageInfoClosing = null;
            DBParameters.Clear();
            AddParameter("@FromDate", FromDate);
            AddParameter("@ToDate", ToDate);
            AddParameter("@SubStoreID", SubStoreID);
            IDataReader dataReader = ExecuteReader("[Sage].[GetRevenueDetails]");
            if (dataReader.Read())
            {
                sageInfoClosing = new SageInfoClosing()
                   {
                       NoOfTransactions = GetFieldValue(dataReader, "NoOfTransactions", 0),
                       //TotalRevenue = GetFieldValue(dataReader, "TotalRevenue", 0L)
                   };
            }
            return sageInfoClosing;
        }

        public List<ImixPOSDetail> GetPrintServerDetails(int SubStoreID)
        {
            DBParameters.Clear();
            AddParameter("@SubStoreID", SubStoreID);
            IDataReader sqlReader = ExecuteReader("[Sage].[GetPrintServerDetails]");
            List<ImixPOSDetail> lstPosDetail = new List<ImixPOSDetail>();

            while (sqlReader.Read())
            {
                ImixPOSDetail _imixPosDetail = new ImixPOSDetail();
                _imixPosDetail.ImixPOSDetailID = GetFieldValue(sqlReader, "iMixPOSDetailID", 0L);
                _imixPosDetail.SystemName = GetFieldValue(sqlReader, "SystemName", string.Empty);
                _imixPosDetail.SubStoreID = GetFieldValue(sqlReader, "SubStoreID", 0L);
                _imixPosDetail.IsActive = GetFieldValue(sqlReader, "IsActive", false);

                lstPosDetail.Add(_imixPosDetail);
            }
            sqlReader.Close();
            return lstPosDetail;
        }
        public List<SageClosingFormDownloadInfo> GetClosingFormDownloadInfoList(DateTime? FromDate, DateTime? ToDate, int SubStoreID)
        {
            List<SageClosingFormDownloadInfo> lstSageInfoClosing = new List<SageClosingFormDownloadInfo>();
            SageClosingFormDownloadInfo sageClosingDownloadInfo = null;
            DBParameters.Clear();
            AddParameter("@FromDate", FromDate);
            AddParameter("@ToDate", ToDate);
            AddParameter("@SubStoreID", SubStoreID);
            IDataReader dataReader = ExecuteReader("[Sage].[GET_ClosingFormList]");
            while (dataReader.Read())
            {
                sageClosingDownloadInfo = new SageClosingFormDownloadInfo()
                {
                    SubstoreName = GetFieldValue(dataReader, "SubstoreName", string.Empty),
                    FilledBy = GetFieldValue(dataReader, "FilledBy", string.Empty),
                    ClosingFormDetailID = GetFieldValue(dataReader, "ClosingFormDetailID", 0L),
                    BusinessDate = GetFieldValue(dataReader, "BusinessDate", DateTime.MinValue),
                    ClosingDate = GetFieldValue(dataReader, "ClosingDate", DateTime.MinValue),

                };
                lstSageInfoClosing.Add(sageClosingDownloadInfo);
            }
            return lstSageInfoClosing;
        }

        public SageOpenClose GetClosingFormDownloadInfo(int ClosingFormDetailID)
        {
            SageOpenClose sageOpenClose = new SageOpenClose();
            SageInfoClosing _sageInfoClose = new SageInfoClosing();
            List<TransDetail> lstTransDetail = new List<TransDetail>();
            DBParameters.Clear();
            AddParameter("@ClosingFormDetailID", ClosingFormDetailID);
            IDataReader sqlReader = ExecuteReader("[Sage].[GET_ClosingFormDetails]");
            while (sqlReader.Read())
            {
                _sageInfoClose = new SageInfoClosing()
                {
                    ClosingFormDetailID = GetFieldValue(sqlReader, "ClosingFormDetailID", 0L),
                    sixEightClosingNumber = GetFieldValue(sqlReader, "6X8ClosingNumber", 0L),
                    eightTenClosingNumber = GetFieldValue(sqlReader, "8X10ClosingNumber", 0L),
                    sixEightAutoClosingNumber = GetFieldValue(sqlReader, "6X8AutoClosingNumber", 0L),
                    eightTenAutoClosingNumber = GetFieldValue(sqlReader, "8X10AutoClosingNumber", 0L),
                    PosterClosingNumber = GetFieldValue(sqlReader, "PosterClosingNumber", 0L),
                    SixEightWestage = GetFieldValue(sqlReader, "6X8Westage", 0L),
                    EightTenWestage = GetFieldValue(sqlReader, "8X10Westage", 0L),
                    SixEightAutoWestage = GetFieldValue(sqlReader, "6X8AutoWestage", 0L),
                    EightTenAutoWestage = GetFieldValue(sqlReader, "8X10AutoWestage", 0L),
                    PosterWestage = GetFieldValue(sqlReader, "PosterWestage", 0L),
                    Attendance = GetFieldValue(sqlReader, "Attendance", 0),
                    LaborHour = GetFieldValue(sqlReader, "LaborHour", 0.0M),
                    NoOfCapture = GetFieldValue(sqlReader, "NoOfCapture", 0L),
                    NoOfPreview = GetFieldValue(sqlReader, "NoOfPreview", 0L),
                    NoOfImageSold = GetFieldValue(sqlReader, "NoOfImageSold", 0L),
                    Comments = GetFieldValue(sqlReader, "Comments", string.Empty),
                    //SubStoreID=GetFieldValue(sqlReader, "SubstoreName", 0), 
                    ClosingDate = GetFieldValue(sqlReader, "ClosingDate", DateTime.MinValue),
                    FilledBy = GetFieldValue(sqlReader, "FilledBy", 0),
                    TransDate = GetFieldValue(sqlReader, "TransDate", DateTime.MinValue),
                    Cash = GetFieldValue(sqlReader, "Cash", 0.0M),
                    CreditCard = GetFieldValue(sqlReader, "CreditCard", 0.0M),
                    Amex = GetFieldValue(sqlReader, "Amex", 0.0M),
                    FCV = GetFieldValue(sqlReader, "FCV", 0.0M),
                    RoomCharges = GetFieldValue(sqlReader, "RoomCharges", 0.0M),
                    KVL = GetFieldValue(sqlReader, "KVL", 0.0M),
                    Vouchers = GetFieldValue(sqlReader, "Vouchers", 0.0M),
                    SixEightPrintCount = GetFieldValue(sqlReader, "6X8PrintCount", 0L),
                    EightTenPrintCount = GetFieldValue(sqlReader, "8X10PrintCount", 0L),
                    PosterPrintCount = GetFieldValue(sqlReader, "PosterPrintCount", 0L),
                    SyncCode = GetFieldValue(sqlReader, "SyncCode", string.Empty),
                    NoOfTransactions = GetFieldValue(sqlReader, "NoOfTransactions", 0)
                };
            }
            sqlReader.NextResult();
            while (sqlReader.Read())
            {
                TransDetail _sageTransDetail = new TransDetail();

                // [SubstoreID],[TransDate],[PackageID],[UnitPrice],[Quantity],[Discount],NetPrice,  ClosingFormDetailID        

                _sageTransDetail.SubstoreID = GetFieldValue(sqlReader, "SubstoreID", 0);
                _sageTransDetail.TransDate = GetFieldValue(sqlReader, "TransDate", DateTime.MinValue);
                _sageTransDetail.PackageID = GetFieldValue(sqlReader, "PackageID", 0);
                _sageTransDetail.UnitPrice = GetFieldValue(sqlReader, "UnitPrice", 0.0M);
                _sageTransDetail.Quantity = GetFieldValue(sqlReader, "Quantity", 0.0M);
                _sageTransDetail.Discount = GetFieldValue(sqlReader, "Discount", 0.0M);
                _sageTransDetail.Total = GetFieldValue(sqlReader, "Total", 0.0M);
                //here we are setting hte value of closing form id
                //_sageInfoClose.ClosingFormDetailID = GetFieldValue(sqlReader, "ClosingFormDetailID", 0L);
                _sageTransDetail.PackageSyncCode = GetFieldValue(sqlReader, "SyncCode", string.Empty);
                _sageTransDetail.PackageCode = GetFieldValue(sqlReader, "DG_Orders_ProductCode", string.Empty);
                lstTransDetail.Add(_sageTransDetail);
            }
            _sageInfoClose.TransDetails = lstTransDetail;
            sqlReader.NextResult();
            _sageInfoClose.objSubStore = new SubStoresInfo();
            while (sqlReader.Read())
            {
                _sageInfoClose.objSubStore.DG_SubStore_pkey = GetFieldValue(sqlReader, "DG_SubStore_pkey", 0);
                _sageInfoClose.objSubStore.DG_SubStore_Name = GetFieldValue(sqlReader, "DG_SubStore_Name", string.Empty);
                _sageInfoClose.objSubStore.DG_SubStore_Code = GetFieldValue(sqlReader, "DG_SubStore_Code", string.Empty);
                _sageInfoClose.objSubStore.SyncCode = GetFieldValue(sqlReader, "SyncCode", string.Empty);
                // _sageInfoClose.objSubStore.DG_Orders_ProductCode = GetFieldValue(sqlReader, "DG_Orders_ProductCode", string.Empty);
            }

            sqlReader.NextResult();
            _sageInfoClose.InventoryConsumable = new List<InventoryConsumables>();
            while (sqlReader.Read())
            {
                InventoryConsumables objIC = new InventoryConsumables();
                //AccessoryID,ConsumeValue,ClosingFormDetailID,SyncCode,DG_Orders_ProductCode
                objIC.AccessoryID = GetFieldValue(sqlReader, "AccessoryID", 0L);
                objIC.ConsumeValue = GetFieldValue(sqlReader, "ConsumeValue", 0L);
                //objIC.ClosingFormDetailID = GetFieldValue(sqlReader, "ClosingFormDetailID", 0L);
                objIC.AccessorySyncCode = GetFieldValue(sqlReader, "SyncCode", string.Empty);
                objIC.AccessoryCode = GetFieldValue(sqlReader, "DG_Orders_ProductCode", string.Empty);

                _sageInfoClose.InventoryConsumable.Add(objIC);
            }
            sqlReader.NextResult();
            while (sqlReader.Read())
            {
                _sageInfoClose.FilledBySyncCode = GetFieldValue(sqlReader, "FilledBySyncCode", string.Empty);
            }
            sqlReader.NextResult();
            //while (sqlReader.Read())
            //{
            //    _sageInfoClose.ClosingFormDetailID = GetFieldValue(sqlReader, "ClosingFormDetailID", 0L);
            //    _sageInfoClose.Cash = GetFieldValue(sqlReader, "Cash", 0.0M);
            //    _sageInfoClose.CreditCard = GetFieldValue(sqlReader, "CreditCard", 0.0M);
            //    _sageInfoClose.Amex = GetFieldValue(sqlReader, "AMEX", 0.0M);
            //    _sageInfoClose.FCV = GetFieldValue(sqlReader, "FCV", 0.0M);
            //    _sageInfoClose.RoomCharges = GetFieldValue(sqlReader, "RoomCharges", 0.0M);
            //    _sageInfoClose.KVL = GetFieldValue(sqlReader, "KVL", 0.0M);
            //    _sageInfoClose.Vouchers = GetFieldValue(sqlReader, "Vouchers", 0.0M);
            //}

            //OpeningFormDetailID,6X8StartingNumber,8X10StartingNumber,PosterStartingNumber,CashFloatAmount,
            //SubStoreID,OpeningDate,FilledBy,SyncCode,BussinessDate,U.Synccode as FilledBySyncCode

            //sqlReader.NextResult();
            while (sqlReader.Read())
            {
                SageInfo _sageInfoOpen = new SageInfo();

                _sageInfoOpen.OpeningFormDetailID = GetFieldValue(sqlReader, "OpeningFormDetailID", 0L);
                _sageInfoOpen.sixEightStartingNumber = GetFieldValue(sqlReader, "6X8StartingNumber", 0L);
                _sageInfoOpen.eightTenStartingNumber = GetFieldValue(sqlReader, "8X10StartingNumber", 0L);
                _sageInfoOpen.PosterStartingNumber = GetFieldValue(sqlReader, "PosterStartingNumber", 0L);
                _sageInfoOpen.CashFloatAmount = GetFieldValue(sqlReader, "CashFloatAmount", 0.0M);
                _sageInfoOpen.OpeningDate = GetFieldValue(sqlReader, "OpeningDate", Convert.ToDateTime("1/1/1753"));
                _sageInfoOpen.SyncCode = GetFieldValue(sqlReader, "SyncCode", string.Empty);
                _sageInfoOpen.BusinessDate = GetFieldValue(sqlReader, "BussinessDate", Convert.ToDateTime("1/1/1753"));
                _sageInfoOpen.FilledBySyncCode = GetFieldValue(sqlReader, "FilledBySyncCode", string.Empty);
                sageOpenClose.objOpen = _sageInfoOpen;
            }


            sqlReader.Close();
            //sageInfoClosing.InventoryConsumable = GetInventoryConsumables(ClosingFormDetailID);
            sageOpenClose.objClose = _sageInfoClose;
            return sageOpenClose;
        }

        //public List<InventoryConsumables> GetInventoryConsumables(int ClosingFormDetailID)
        //{
        //    List<InventoryConsumables> inventoryConsumables = null;
        //    InventoryConsumables inventoryConsumable = null;
        //    DBParameters.Clear();
        //    AddParameter("@ClosingFormDetailID", ClosingFormDetailID);
        //    IDataReader dataReader = ExecuteReader("[Sage].[GET_InventoryConsumables]");
        //    while (dataReader.Read())
        //    {
        //        inventoryConsumable = new InventoryConsumables()
        //        {
        //            AccessoryID = GetFieldValue(dataReader, "AccessoryID", 0L),
        //            ConsumeValue = GetFieldValue(dataReader, "ConsumeValue", 0L),
        //            //objIC.ClosingFormDetailID = GetFieldValue(sqlReader, "ClosingFormDetailID", 0L);
        //            AccessorySyncCode = GetFieldValue(dataReader, "SyncCode", string.Empty),
        //            AccessoryCode = GetFieldValue(dataReader, "DG_Orders_ProductCode", string.Empty)

        //        };
        //        inventoryConsumables.Add(inventoryConsumable);
        //    }
        //    return inventoryConsumables;
        //}


        #endregion
    }
}
