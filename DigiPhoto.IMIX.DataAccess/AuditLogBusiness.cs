﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DigiPhoto.IMIX.DataAccess
{
    public class AuditLogDataAccess : BaseDataAccess
    {
        public AuditLogDataAccess(BaseDataAccess baseaccess)
            : base(baseaccess)
        {

        }

        public void InsertAuditLog(string uniqueId, int threadId, DateTime startTime, DateTime endTime, string fileName)
        {
            DBParameters.Clear();
            AddParameter("@uniqueId", uniqueId);
            AddParameter("@threadId", threadId);
            AddParameter("@startTime", startTime);
            AddParameter("@endTime", endTime);
            AddParameter("@fileName", fileName);
            ExecuteNonQuery("usp_InsertAuditLog");
        }

        public void InsertExceptionLog(string uniqueId, int threadId, DateTime startTime, DateTime endTime, string fileName, long PhotoId, Exception excption = null, string Message = null)
        {
            DBParameters.Clear();
            AddParameter("@uniqueId", uniqueId);
            AddParameter("@threadId", threadId);
            AddParameter("@startTime", startTime);
            AddParameter("@endTime", endTime);
            AddParameter("@fileName", fileName);
            AddParameter("@PhotoId", PhotoId);
            AddParameter("@Message", Message);
            if (excption != null)
            {
                AddParameter("@exMessage", excption.Message);
                AddParameter("@exstacktrace", excption.StackTrace);
            }
            ExecuteNonQuery("usp_InsertAuditExceptionLog");
        }

    }
}
