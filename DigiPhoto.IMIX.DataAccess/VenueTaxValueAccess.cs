﻿using DigiPhoto.IMIX.Model;
using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DigiPhoto.IMIX.DataAccess
{
    public class VenueTaxValueAccess : BaseDataAccess
    {
        #region Constructor
        public VenueTaxValueAccess(BaseDataAccess baseaccess) : base(baseaccess)
        { }

        public VenueTaxValueAccess()
        { }
        #endregion

        public List<VenueTaxValueModel> GetVenueTaxValueDetail()
        {
            DBParameters.Clear();
            IDataReader sqlReader = ExecuteReader("usp_GetTaxPercentage");
            List<VenueTaxValueModel> venueTaxList = PopulateVenueTaxList(sqlReader);
            sqlReader.Close();
            return venueTaxList;
        }
        public List<VenueTaxValueModel> GetVenueTaxValueDetail(int TaxId)
        {
            DBParameters.Clear();
            AddParameter("@TaxId", TaxId);
            IDataReader sqlReader = ExecuteReader("usp_GetTaxPercentage");
            List<VenueTaxValueModel> venueTaxList = PopulateVenueTaxList(sqlReader);
            sqlReader.Close();
            return venueTaxList;
        }
        private List<VenueTaxValueModel> PopulateVenueTaxList(IDataReader sqlReader)
        {
            List<VenueTaxValueModel> VenueTaxList = new List<VenueTaxValueModel>();
            while (sqlReader.Read())
            {
                VenueTaxValueModel List = new VenueTaxValueModel()
                {
                    TaxPercentage = GetFieldValue(sqlReader, "TaxPercentage", 0.0M),
                    IsActive = GetFieldValue(sqlReader, "IsActive", false)
                };
                VenueTaxList.Add(List);
            }
            return VenueTaxList;
        }

        public VenueTaxValueModel GetActiveTaxDetails()
        {
            DBParameters.Clear();
            IDataReader sqlReader = ExecuteReader("usp_GetActiveTaxDetails");
            VenueTaxValueModel venueTaxList = PopulateActiveTax(sqlReader);
            sqlReader.Close();
            return venueTaxList;
        }

        private VenueTaxValueModel PopulateActiveTax(IDataReader sqlReader)
        {
            // List<VenueTaxValueModel> VenueTaxList = new List<VenueTaxValueModel>();
            VenueTaxValueModel objTaxVal = new VenueTaxValueModel();
            while (sqlReader.Read())
            {
                objTaxVal.TaxId = GetFieldValue(sqlReader, "TaxId", 0);
                objTaxVal.IsActive = GetFieldValue(sqlReader, "IsActive", false);
                objTaxVal.TaxName = GetFieldValue(sqlReader, "TaxName", "");
            };
            return objTaxVal;
        }

    }
}
