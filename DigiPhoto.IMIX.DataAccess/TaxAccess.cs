﻿using DigiPhoto.IMIX.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;

namespace DigiPhoto.IMIX.DataAccess
{
    public class TaxAccess : BaseDataAccess
    {
        #region Constructor
        public TaxAccess(BaseDataAccess baseaccess)
            : base(baseaccess)
        {

        }
        public TaxAccess()
        { }
        #endregion
        #region Public Functions
        public bool SaveOrderTaxDetails(int StoreId, int OrderId, int SubStoreId)
        {
            DBParameters.Clear();
            AddParameter("@StoreId", StoreId);
            AddParameter("@OrderId", OrderId);
            AddParameter("@ParamSubStoreId", SubStoreId);
            ExecuteNonQuery("SaveOrderTaxDetails");
            return true;

        }

        public bool SaveTaxDetails(int VenueId, int TaxId, int inActiveTaxId,decimal TaxPercentage, bool Status,DateTime modifiedate)
        {
            DBParameters.Clear();
            AddParameter("@VenueId", VenueId);
            AddParameter("@TaxId", TaxId);
            AddParameter("@InActiveTaxid", inActiveTaxId);
            AddParameter("@TaxPercentage", TaxPercentage);
            AddParameter("@IsActive", Status);
            AddParameter("@ModifiedDate", modifiedate);
            ExecuteNonQuery("uspSaveVenueTaxDetails");
            return true;

        }

        public ObservableCollection<TaxDetailInfo> GetTaxDetail()
        {
            DBParameters.Clear();
            IDataReader sqlReader = ExecuteReader("uspGetTaxDetails");
            ObservableCollection<TaxDetailInfo> TaxList = PopulateTaxListRequired(sqlReader);
            sqlReader.Close();
            return TaxList;
        }

        public List<TaxDetailInfo> GetTaxDetail(int? orderId)
        {
            DBParameters.Clear();
            AddParameter("@OrderId", orderId);
            IDataReader sqlReader = ExecuteReader("uspGetOrderTaxDetails");
            List<TaxDetailInfo> TaxList = PopulateTaxList(sqlReader);
            sqlReader.Close();
            return TaxList;
        }

        public List<TaxDetailInfo> GetReportTaxDetail(DateTime FromDate, DateTime ToDate, int SubStoreID)
        {
            DBParameters.Clear();
            AddParameter("@FROMDate", FromDate);
            AddParameter("@ToDate", ToDate);
            AddParameter("@SubStoreID", SubStoreID);
            IDataReader sqlReader = ExecuteReader("GetReportTaxDetail");
            List<TaxDetailInfo> TaxList = PopulateTaxList(sqlReader);
            sqlReader.Close();
            return TaxList;
        }

        public bool UpdateStoreTaxData(StoreInfo store)
        {
            AddParameter("@BillReceiptTitle", store.BillReceiptTitle);
            AddParameter("@TaxRegistrationNumber", store.TaxRegistrationNumber);
            AddParameter("@TaxRegistrationText", store.TaxRegistrationText);
            AddParameter("@Address", store.Address);
            AddParameter("@PhoneNo", store.PhoneNo);
            AddParameter("@TaxMinSequenceNo", store.TaxMinSequenceNo);
            AddParameter("@TaxMaxSequenceNo", store.TaxMaxSequenceNo);
            AddParameter("@IsSequenceNoRequired", store.IsSequenceNoRequired);
            AddParameter("@IsTaxEnabled", store.IsTaxEnabled);
            AddParameter("@WebsiteURl", store.WebsiteURL);
            AddParameter("@EmailID", store.EmailID);
            AddParameter("@ServerHotFolderPath", store.ServerHotFolderPath);
            AddParameter("@IsActiveStockShot", store.IsActiveStockShot);
            ////change made by latika for LocationWise Setting
           // AddParameter("@RunImageProcessingEngineLocationWise", store.RunImageProcessingEngineLocationWise);
          //  AddParameter("@RunVideoProcessingEngineLocationWise", store.RunVideoProcessingEngineLocationWise);
            AddParameter("@IsTaxIncluded",store.IsTaxIncluded);
            AddParameter("@PlaceSpecOrderAcrossSites", store.PlaceSpecOrderAcrossSites);
            //AddParameter("@RunManualDownloadLocationWise", store.RunManualDownloadLocationWise);
            	///end
			ExecuteNonQuery("UpdateStoreReceiptData");
            return true;
        }
        public StoreInfo getTaxConfigData()
        {
            StoreInfo store = new StoreInfo();
            IDataReader sqlReader = ExecuteReader("GetTaxConfigData");
            store = PopulateTaxConfigData(sqlReader);
            sqlReader.Close();
            return store;
        }
		////changed by latika for locationwise settings
        public StoreInfo getTaxConfigDataBySystemID(string SystemID)
        {
            StoreInfo store = new StoreInfo();
            DBParameters.Clear();
            AddParameter("@PosName", SystemID);
            IDataReader sqlReader = ExecuteReader("GetTaxConfigDataBySystem");
            store = PopulateTaxConfigData(sqlReader);
            sqlReader.Close();
            return store;
        }
        /// <summary>
        /// Priyanka code to chech ISGSTActive from database on 10 july 2019
        /// </summary>
        public List<TaxDetailInfo> GetAllTaxDetails()
        {
            DBParameters.Clear();
            IDataReader sqlReader = ExecuteReader("usp_GetAllTaxDetails");
            List<TaxDetailInfo> lstTaxDetails = PopulateAllTaxDetails(sqlReader);
            sqlReader.Close();
            return lstTaxDetails;
        }        
        private List<TaxDetailInfo> PopulateAllTaxDetails(IDataReader sqlReader)
        {
            List<TaxDetailInfo> lstTaxDetails = new List<TaxDetailInfo>();
            while (sqlReader.Read())
            {
                TaxDetailInfo objTaxDetails = new TaxDetailInfo();
                objTaxDetails.TaxId = GetFieldValue(sqlReader,"TaxID",0);
                objTaxDetails.TaxName = GetFieldValue(sqlReader, "TaxName", string.Empty);
                objTaxDetails.IsActive = GetFieldValue(sqlReader, "IsActive", false);
                lstTaxDetails.Add(objTaxDetails);
            }
            return lstTaxDetails;
        }
        /// <summary>
        /// End Priyanka
        /// </summary>
        /// <param name="sqlReader"></param>
        /// <returns></returns>
        public List<TaxDetailInfo> GetApplicableTaxes(int taxID)
        {
            DBParameters.Clear();
            AddParameter("@ParamTaxId",taxID);
            IDataReader sqlReader = ExecuteReader("usp_SEL_TaxList");
            List<TaxDetailInfo> TaxList = PopulateActiveTaxList(sqlReader);
            sqlReader.Close();
            return TaxList;
        }


        #endregion


        #region Private Functions
        private List<TaxDetailInfo> PopulateTaxList(IDataReader sqlReader)
        {
            List<TaxDetailInfo> TaxList = new List<TaxDetailInfo>();
            while (sqlReader.Read())
            {
                TaxDetailInfo TaxInfo = new TaxDetailInfo();
                TaxInfo.TaxAmount = GetFieldValue(sqlReader, "TaxAmount", 0.0M);
                TaxInfo.TaxPercentage = GetFieldValue(sqlReader, "TaxPercentage", 0.0M);
                TaxInfo.TaxName = GetFieldValue(sqlReader, "TaxName", string.Empty);
                TaxInfo.CurrencyName = GetFieldValue(sqlReader, "CurrencyName", string.Empty);
                TaxList.Add(TaxInfo);
            }
            return TaxList;
        }


        private ObservableCollection<TaxDetailInfo> PopulateTaxListRequired(IDataReader sqlReader)
        {
            ObservableCollection<TaxDetailInfo> TaxList = new ObservableCollection<TaxDetailInfo>();
            while (sqlReader.Read())
            {
                TaxDetailInfo List = new TaxDetailInfo()
                {
                    TaxId = GetFieldValue(sqlReader, "TaxId", 1),
                    TaxName = GetFieldValue(sqlReader, "TaxName", string.Empty)
                };
                TaxList.Add(List);
            }
            return TaxList;
        }

        private List<TaxDetailInfo> PopulateActiveTaxList(IDataReader sqlReader)
        {
            List<TaxDetailInfo> TaxList = new List<TaxDetailInfo>();
            while (sqlReader.Read())
            {
                TaxDetailInfo TaxInfo = new TaxDetailInfo();
                TaxInfo.TaxId = GetFieldValue(sqlReader, "TaxId", 0);
                TaxInfo.TaxPercentage = GetFieldValue(sqlReader, "TaxPercentage", 0.0M);
                TaxInfo.TaxName = GetFieldValue(sqlReader, "TaxName", string.Empty);
                //TaxInfo.CurrencyName = GetFieldValue(sqlReader, "CurrencyName", string.Empty);
                TaxList.Add(TaxInfo);
            }
            return TaxList;
        }
        #endregion

        #region Private Functions
        private StoreInfo PopulateTaxConfigData(IDataReader sqlReader)
        {
            StoreInfo store = new StoreInfo();
            while (sqlReader.Read())
            {
                store.BillReceiptTitle = GetFieldValue(sqlReader, "BillReceiptTitle", string.Empty);
                store.Address = GetFieldValue(sqlReader, "Address", string.Empty);
                store.PhoneNo = GetFieldValue(sqlReader, "PhoneNo", string.Empty);
                store.TaxRegistrationNumber = GetFieldValue(sqlReader, "TaxRegistrationNumber", string.Empty);
                store.TaxRegistrationText = GetFieldValue(sqlReader, "TaxRegistrationText", string.Empty);
                store.IsSequenceNoRequired = GetFieldValue(sqlReader, "IsSequenceNoRequired", false);
                store.IsTaxEnabled = GetFieldValue(sqlReader, "IsTaxEnabled", false);
                store.TaxMinSequenceNo = GetFieldValue(sqlReader, "TaxMinSequenceNo", 0L);
                store.TaxMaxSequenceNo = GetFieldValue(sqlReader, "TaxMaxSequenceNo", 0L);
                store.WebsiteURL = GetFieldValue(sqlReader, "WebsiteURL", string.Empty);
                store.EmailID = GetFieldValue(sqlReader, "Email", string.Empty);
                store.ServerHotFolderPath = GetFieldValue(sqlReader, "ServerHotFolderPath", string.Empty);
                store.IsActiveStockShot = GetFieldValue(sqlReader, "IsActiveStockShot", false);
                store.RunImageProcessingEngineLocationWise = GetFieldValue(sqlReader, "RunImageProcessingEngineLocationWise", false);
                store.RunVideoProcessingEngineLocationWise = GetFieldValue(sqlReader, "RunVideoProcessingEngineLocationWise", false);
                store.IsTaxIncluded = GetFieldValue(sqlReader, "IsTaxIncluded", false);
                store.PlaceSpecOrderAcrossSites = GetFieldValue(sqlReader, "PlaceSpecOrderAcrossSites", false);
                store.RunManualDownloadLocationWise = GetFieldValue(sqlReader, "RunManualDownloadLocationWise", false);

            }
            return store;
        }
        #endregion

        public int Get_TaxPercent()
        {
            int tax_percent = Convert.ToInt32(ExecuteScalar("usp_Get_DigiConfigTaxDetails"));
            return tax_percent;
        }
        /////changed by latika for Evoucher Rpt

        public EvoucherDtlsRpt GetEvocuherDtls(int OrderID)
        {
            EvoucherDtlsRpt EvchrDtls = new EvoucherDtlsRpt();
            DBParameters.Clear();
            AddParameter("@OrderID", OrderID);
            IDataReader sqlReader = ExecuteReader("SP_GetEvoucherRpt");
            EvchrDtls = PopulateEvoucher(sqlReader);
            sqlReader.Close();
            return EvchrDtls;
        }

        private EvoucherDtlsRpt PopulateEvoucher(IDataReader sqlReader)
        {
            EvoucherDtlsRpt Evoucherlist = new EvoucherDtlsRpt();
            while (sqlReader.Read())
            {
                Evoucherlist.EvoucherNo = GetFieldValue(sqlReader, "EvoucherNo", string.Empty);
                Evoucherlist.PhotoCount = GetFieldValue(sqlReader, "PhotoCount", 0);


            }
            return Evoucherlist;
        }

        /// <summary>
        /// //changed by latika 
        /// </summary>
        /// <returns></returns>
        ///end by latika 04March2020


    }
}
