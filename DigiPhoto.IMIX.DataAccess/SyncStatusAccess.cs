﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DigiPhoto.IMIX.Model;
using System.Data;


namespace DigiPhoto.IMIX.DataAccess
{
    public class SyncStatusAccess:BaseDataAccess
    {
         #region Constructor
        public SyncStatusAccess(BaseDataAccess baseaccess)
            : base(baseaccess)
        {

        }

        public SyncStatusAccess()
        {

        }
        #endregion
        public List<SyncStatusInfo> GetSyncStatus(DateTime FromDate, DateTime ToDate)
        {
            DBParameters.Clear();
            AddParameter("@FromDate", FromDate);
            AddParameter("@ToDate", ToDate);
            IDataReader sqlReader = ExecuteReader("uspOrderDetailedSyncStausNormalOrders");
            List<SyncStatusInfo> statusList = PopulateStatusList(sqlReader);
            sqlReader.Close();
            return statusList;
        }

        private List<SyncStatusInfo> PopulateStatusList(IDataReader sqlReader)
        {
            List<SyncStatusInfo> statusList = new List<SyncStatusInfo>();
            while (sqlReader.Read())
            {
                SyncStatusInfo syncStatusInfo = new SyncStatusInfo();
                
                syncStatusInfo.SyncOrderNumber = GetFieldValue(sqlReader, "DG_Orders_Number", string.Empty);
                syncStatusInfo.SyncOrderdate = GetFieldValue(sqlReader, "DG_Orders_Date", DateTime.Now);
                syncStatusInfo.PhotoRfid = GetFieldValue(sqlReader, "PhotoRFID", string.Empty);
                syncStatusInfo.Syncdate = GetFieldValueDateTimeNull(sqlReader, "SyncDate", DateTime.Now);
                syncStatusInfo.IsAvailable = GetFieldValue(sqlReader, "IsAvailable", false);
                #region Take syncstatus from DB  ashirwad
                syncStatusInfo.Syncstatus = GetFieldValue(sqlReader, "SyncstatusInfo", string.Empty);
                //int tempStatus = GetFieldValue(sqlReader, "SyncStatus", 0);
                //switch (tempStatus)
                //{
                //    case 0:
                //        syncStatusInfo.Syncstatus = Constants.SyncOrderStatus.NotSynced.ToString();
                //        break;
                //    case 1:
                //        syncStatusInfo.Syncstatus = Constants.SyncOrderStatus.Synced.ToString();
                //        break;
                //    case -1:
                //        syncStatusInfo.Syncstatus = Constants.SyncOrderStatus.Error.ToString();
                //        break;
                //    case -2:
                //        syncStatusInfo.Syncstatus = Constants.SyncOrderStatus.Invalid.ToString();
                //        break;
                //    case 3:
                //        syncStatusInfo.Syncstatus = Constants.SyncOrderStatus.UploadedDataOnCentralServerandCloudinary.ToString();
                //        break;
                //    case -3:
                //        syncStatusInfo.Syncstatus = Constants.SyncOrderStatus.UploadedDataOnCentralServerButFailedonCloudinary.ToString();
                //        break;
                //    case 4:
                //        syncStatusInfo.Syncstatus = Constants.SyncOrderStatus.ProcessedDataOnCentralServer.ToString();
                //        break;
                //    case -4:
                //        syncStatusInfo.Syncstatus = Constants.SyncOrderStatus.FailedToProcessDataOnCentralServer.ToString();
                //        break;
                //    case -5:
                //        syncStatusInfo.Syncstatus = Constants.SyncOrderStatus.FailedtoProcessonPartnerDB.ToString();
                //        break;
                //    default:
                //        break;
                //}
                #endregion
                statusList.Add(syncStatusInfo);
            }
            return statusList;
        }

        public List<SyncStatusInfo> GetOrdersyncStatus(DateTime? FromDate, DateTime? ToDate,string QRCode)
         {
            DBParameters.Clear();
            AddParameter("@ParamFromDate", FromDate);
            AddParameter("@ParamToDate", ToDate);
            AddParameter("@paramQrCode", QRCode);
            //IDataReader sqlReader = ExecuteReader("OrderDetailedSyncStaus");
            IDataReader sqlReader = ExecuteReader(DAOConstant.USP_SEL_ORDERDETAILEDSYNCSTAUS);
            List<SyncStatusInfo> statusList = PopulateOrdersyncStatus(sqlReader);
            sqlReader.Close();
            return statusList;
        }
        private List<SyncStatusInfo> PopulateOrdersyncStatus(IDataReader sqlReader)
        {
            List<SyncStatusInfo> statusList = new List<SyncStatusInfo>();
            while (sqlReader.Read())
            {
                SyncStatusInfo syncStatusInfo = new SyncStatusInfo();

                syncStatusInfo.SyncOrderNumber = GetFieldValue(sqlReader, "DG_Orders_Number", string.Empty);
                syncStatusInfo.QRCode = GetFieldValue(sqlReader, "DG_Order_ImageUniqueIdentifier", string.Empty);
                syncStatusInfo.DGOrderspkey = GetFieldValue(sqlReader, "DG_Orders_pkey",0);
                syncStatusInfo.SyncOrderdate = GetFieldValue(sqlReader, "DG_Orders_Date", DateTime.Now);
                syncStatusInfo.PhotoRfid = GetFieldValue(sqlReader, "PhotNumber", string.Empty);
                syncStatusInfo.Syncdate = GetFieldValueDateTimeNull(sqlReader, "SyncDate", DateTime.Now);
                syncStatusInfo.SyncStatusID = GetFieldValue(sqlReader, "SyncStatus", 0);
                syncStatusInfo.Syncstatus = GetFieldValue(sqlReader, "SyncstatusInfo", string.Empty);
                syncStatusInfo.IsAvailable = GetFieldValue(sqlReader, "IsAvailable",false);
                syncStatusInfo.ChangeTrackingId = GetFieldValue(sqlReader, "ChangeTrackingId", 0L);
                syncStatusInfo.ImageSynced = GetFieldValue(sqlReader, "ImageSyncStatus", string.Empty);
                statusList.Add(syncStatusInfo);
            }
            return statusList;
        }
        public bool ReSync(string ReSyncId)
        {
            bool Value = false;
            DBParameters.Clear();
            AddParameter("@ParamSyncId", SetNullStringValue(ReSyncId));
            AddParameter("@ParamValue", Value, ParameterDirection.Output);

            ExecuteNonQuery(DAOConstant.USP_UPD_CHNAGETRACKINGSYNC);
            bool objectId = (bool)GetOutParameterValue("@ParamValue");
            return objectId;
        }
        public bool ReSyncImage(string OrderId)
        {
            bool Value = false;
            DBParameters.Clear();
            AddParameter("@ParamOrderId", OrderId);
            AddParameter("@ParamValue", Value, ParameterDirection.Output);

            ExecuteNonQuery(DAOConstant.USP_UPD_CHNAGETRACKINGIMAGESYNC);
            bool objectId = (bool)GetOutParameterValue("@ParamValue");
            return objectId;
        }

        public List<SyncStatusInfo> GetSyncStatusOpenCloseForm(DateTime FromDate, DateTime ToDate)
        {
            DBParameters.Clear();
            AddParameter("@FromDate", FromDate);
            AddParameter("@ToDate", ToDate);
            IDataReader sqlReader = ExecuteReader("sage.uspOpenCloseFormSyncStaus");
            List<SyncStatusInfo> statusList = PopulateOpenCloseStatusList(sqlReader);
            sqlReader.Close();
            return statusList;
        }

        private List<SyncStatusInfo> PopulateOpenCloseStatusList(IDataReader sqlReader)
        {
            List<SyncStatusInfo> statusList = new List<SyncStatusInfo>();
            while (sqlReader.Read())
            {
                SyncStatusInfo syncStatusInfo = new SyncStatusInfo();

                syncStatusInfo.SyncFormID = GetFieldValue(sqlReader, "FormID", 0L);
                syncStatusInfo.SyncFormTransDate = GetFieldValue(sqlReader, "TransDate", DateTime.Now);
                syncStatusInfo.Syncdate = GetFieldValueDateTimeNull(sqlReader, "SyncDate", DateTime.Now);
                syncStatusInfo.IsAvailable = GetFieldValue(sqlReader, "IsAvailable", false);
                syncStatusInfo.Form = GetFieldValue(sqlReader, "Form", string.Empty);
                syncStatusInfo.ChangeTrackingId = GetFieldValue(sqlReader, "ChangeTrackingId", 0L);
                #region Take syncstatus from DB  ashirwad
                syncStatusInfo.Syncstatus = GetFieldValue(sqlReader, "SyncstatusInfo", string.Empty);
                //int tempStatus = GetFieldValue(sqlReader, "SyncStatus", 0);
                //switch (tempStatus)
                //{
                //    case 0:
                //        syncStatusInfo.Syncstatus = Constants.SyncOrderStatus.NotSynced.ToString();
                //        break;
                //    case 1:
                //        syncStatusInfo.Syncstatus = Constants.SyncOrderStatus.Synced.ToString();
                //        break;
                //    case -1:
                //        syncStatusInfo.Syncstatus = Constants.SyncOrderStatus.Error.ToString();
                //        break;
                //    case -2:
                //        syncStatusInfo.Syncstatus = Constants.SyncOrderStatus.Invalid.ToString();
                //        break;
                //    case 3:
                //        syncStatusInfo.Syncstatus = Constants.SyncOrderStatus.UploadedDataOnCentralServerandCloudinary.ToString();
                //        break;
                //    case -3:
                //        syncStatusInfo.Syncstatus = Constants.SyncOrderStatus.UploadedDataOnCentralServerButFailedonCloudinary.ToString();
                //        break;
                //    case 4:
                //        syncStatusInfo.Syncstatus = Constants.SyncOrderStatus.ProcessedDataOnCentralServer.ToString();
                //        break;
                //    case -4:
                //        syncStatusInfo.Syncstatus = Constants.SyncOrderStatus.FailedToProcessDataOnCentralServer.ToString();
                //        break;
                //    case -5:
                //        syncStatusInfo.Syncstatus = Constants.SyncOrderStatus.FailedtoProcessonPartnerDB.ToString();
                //        break;
                //    default:
                //        break;
                //}
                #endregion
                statusList.Add(syncStatusInfo);
            }
            return statusList;
        }
        public bool InsertResyncHistory(DateTime ResyncDatetime, int ResyncStatus, int ResyncType)
        {
            DBParameters.Clear();
            AddParameter("@ParamResyncDatetime", ResyncDatetime);
            AddParameter("@ParamResyncStatus", ResyncStatus);
            AddParameter("@ParamResyncType ", ResyncType);
            ExecuteNonQuery(DAOConstant.USP_UPD_AND_INS_RESYNCHISTORY);
            return true;
        }


        public List<ChangeTrackingProcessingStatusDtl> GetChangeTrackingProcessingStatusDtl(long ChangeTrackingID)
         {
            DBParameters.Clear();
            AddParameter("@ChangeTrackingId", ChangeTrackingID);
           // AddParameter("@ParamToDate", ToDate);
            //IDataReader sqlReader = ExecuteReader("OrderDetailedSyncStaus");
            IDataReader sqlReader = ExecuteReader(DAOConstant.USP_SEL_CHANGETRACKINGPROCESSINGSTATUSDTL);
            List<ChangeTrackingProcessingStatusDtl> statusList = PopulateChangeTrackingProcessingStatus(sqlReader);
            sqlReader.Close();
            return statusList;
        }
        private List<ChangeTrackingProcessingStatusDtl> PopulateChangeTrackingProcessingStatus(IDataReader sqlReader)
        {
            List<ChangeTrackingProcessingStatusDtl> statusList = new List<ChangeTrackingProcessingStatusDtl>();
            while (sqlReader.Read())
            {
                ChangeTrackingProcessingStatusDtl syncStatusInfo = new ChangeTrackingProcessingStatusDtl();

                syncStatusInfo.Message = GetFieldValue(sqlReader, "Message", string.Empty);
                syncStatusInfo.StatusValue = GetFieldValue(sqlReader, "SyncstatusInfo", string.Empty);
                syncStatusInfo.CreatedOn = GetFieldValue(sqlReader, "CreatedOn", DateTime.Now);
                syncStatusInfo.ExceptionType = GetFieldValue(sqlReader, "ExceptionType", string.Empty);
              
                statusList.Add(syncStatusInfo);
            }
            return statusList;
        }

        public DataSet GetMasterDataSyncStaus(DateTime from, DateTime To, long ApplicationObjectID)
        {
            DBParameters.Clear();
            AddParameter("@FromDate", from);
            AddParameter("@ToDate", To);
            AddParameter("@ApplicationObjectID", ApplicationObjectID);
            return ExecuteDataSet(DAOConstant.USPMASTERDATASYNCSTAUS);

        }


      
    }
}
