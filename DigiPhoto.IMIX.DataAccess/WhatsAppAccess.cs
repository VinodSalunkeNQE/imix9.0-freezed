﻿using DigiPhoto.IMIX.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;
using System.Globalization;

namespace DigiPhoto.IMIX.DataAccess
{
    public class WhatsAppAccess : BaseDataAccess
    {
        #region Constructor
        public WhatsAppAccess(BaseDataAccess baseaccess)
            : base(baseaccess)
        {

        }
        public WhatsAppAccess()
        { }
        #endregion
        #region Public Functions

        public ObservableCollection<string> getCountryList()
        {
            DBParameters.Clear();
            IDataReader sqlReader = ExecuteReader("usp_GET_CountryList ");
            ObservableCollection<string> CountryList = PopulateCountryListRequired(sqlReader);
            sqlReader.Close();
            return CountryList;
        }

        public ObservableCollection<string> getGlobalCountryList()
        {
            DBParameters.Clear();
            IDataReader sqlReader = ExecuteReader("usp_get_GlobalCountryList ");
            ObservableCollection<string> CountryList = PopulateGlobalCountryListRequired(sqlReader);
            sqlReader.Close();
            return CountryList;
        }

        public int getDefaultGlobalCountry()
        {
            int countryID = 0;
            try
            {
                DBParameters.Clear();
                IDataReader sqlReader = ExecuteReader("usp_get_DefaultGlobalCountry ");

                while (sqlReader.Read())
                {
                    countryID = GetFieldValue(sqlReader, "countryId", 0);
                    //CountryList.Add(GetFieldValue(sqlReader, "MobileCode", string.Empty));
                }
                sqlReader.Close();
            }
            catch
            {


            }

            
            return countryID;
        }

        private ObservableCollection<string> PopulateGlobalCountryListRequired(IDataReader sqlReader)
        {
            ObservableCollection<string> CountryList = new ObservableCollection<string>();
            CountryList.Add("--Select Country--");
            while (sqlReader.Read())
            {
                CountryList.Add(GetFieldValue(sqlReader, "CountryName", string.Empty)+" "+ "(+" + GetFieldValue(sqlReader, "MobileCode", string.Empty) + ")");
                //CountryList.Add(GetFieldValue(sqlReader, "MobileCode", string.Empty));
            }
            return CountryList;
        }

        private ObservableCollection<string> PopulateCountryListRequired(IDataReader sqlReader)
        {
            ObservableCollection<string> CountryList = new ObservableCollection<string>();
            CountryList.Add("--Select Country--");
            while (sqlReader.Read())
            {
                CountryList.Add(GetFieldValue(sqlReader, "CountryName", string.Empty));
            }
            return CountryList;
        }
        public bool SaveOrderTaxDetails(int StoreId, int OrderId, int SubStoreId)
        {
            DBParameters.Clear();
            AddParameter("@StoreId", StoreId);
            AddParameter("@OrderId", OrderId);
            AddParameter("@ParamSubStoreId", SubStoreId);
            ExecuteNonQuery("SaveOrderTaxDetails");
            return true;

        }

        public bool SaveWhatsAppSettings(string MobileNumber, string CountryName, string APIkey, string HostUrl)
        {
            DBParameters.Clear();
            AddParameter("@mobileNumber", MobileNumber);
            AddParameter("@countryName", CountryName);
            AddParameter("@apiKey", APIkey);
            AddParameter("@hostURL", HostUrl);            
            ExecuteNonQuery("uspSaveWhatsAppSettings");
            return true;

        }

        public bool SaveWhatsAppOrders(string Order_Number, string fileName, string countryName, 
            string CountryMobileCode, string Guest_MobileNumber,
            string WhtsApp_Image_SourcePath, string WhtsApp_URL_SourcePath)
        {
            DBParameters.Clear();
            AddParameter("@Order_Number", Order_Number);
            AddParameter("@FileName", fileName); 
            AddParameter("@countryName", countryName);
            AddParameter("@CountryMobileCode", CountryMobileCode);
            AddParameter("@Guest_MobileNumber", Guest_MobileNumber);
            AddParameter("@WhtsApp_Image_SourcePath", WhtsApp_Image_SourcePath);
            AddParameter("@WhtsApp_URL_SourcePath", WhtsApp_URL_SourcePath);                  
            
            
            ExecuteNonQuery("[dbo].[usp_InsWhatsApp_Order]");
            return true;

        }

        public WhatsAppSettings GetWhatsAppDetail()
        {
            DBParameters.Clear();
            IDataReader sqlReader = ExecuteReader("usp_get_WhatsAppSettings");
            WhatsAppSettings TaxList = PopulateWhatsAppData(sqlReader);
            sqlReader.Close();
            return TaxList;
        }


        public List<WhatsAppSettingsTracking> GetWhatsAppExactStatus()
        {
            DBParameters.Clear();
            IDataReader sqlReader = ExecuteReader("usp_Get_WhatsApp_OrderForStatus");
            List<WhatsAppSettingsTracking> TaxList = PopulateWhatsAppOrdersExact(sqlReader);
            sqlReader.Close();
            return TaxList;
        }

        private List<WhatsAppSettingsTracking> PopulateWhatsAppOrdersExact(IDataReader sqlReader)
        {
            List<WhatsAppSettingsTracking> List = new List<WhatsAppSettingsTracking>();
            while (sqlReader.Read())
            {
                WhatsAppSettingsTracking obj = new WhatsAppSettingsTracking();
                obj.WhatsAppId = GetFieldValue(sqlReader, "WhatsAppId", 0L);
                obj.Order_Number = GetFieldValue(sqlReader, "Order_Number", string.Empty);
                obj.Guest_MobileNumber = GetFieldValue(sqlReader, "Guest_MobileNumber", string.Empty);
                obj.WhtsApp_Image_SourcePath = GetFieldValue(sqlReader, "WhtsApp_Image_SourcePath", string.Empty);
                obj.WhtsApp_URL_SourcePath = GetFieldValue(sqlReader, "WhtsApp_URL_SourcePath", string.Empty);
                

                List.Add(obj);
            }
            return List;
        }


        public List<WhatsAppSettingsTracking> GetWhatsAppStatus(DateTime? FromDate, DateTime? ToDate, string OrderNum, string mobileNum,int newOld)
        {
            DBParameters.Clear();
            if(OrderNum.Equals("") || OrderNum==string.Empty)
            {
                OrderNum = null;
            }
            if (mobileNum.Equals("") || mobileNum == string.Empty)
            {
                mobileNum = null;
            }

            AddParameter("@FromDate", FromDate);
            AddParameter("@ToDate", ToDate);
            AddParameter("@OrderNum", OrderNum);
            AddParameter("@mobileNum", mobileNum);

            //IDataReader sqlReader = ExecuteReader("usp_Get_WhatsApp_Status");
            if (newOld == 1)
            {
                IDataReader sqlReader = ExecuteReader("usp_Get_WhatsApp_Status_new");
                List<WhatsAppSettingsTracking> statusList = PopulateWhtsAppStatus(sqlReader);
                sqlReader.Close();
                return statusList;
            }
            else
            {
                IDataReader sqlReader = ExecuteReader("usp_Get_WhatsApp_Status");
                List<WhatsAppSettingsTracking> statusList = PopulateWhtsAppStatusOld(sqlReader);
                sqlReader.Close();
                return statusList;
            }
           
        }

        private List<WhatsAppSettingsTracking> PopulateWhtsAppStatusOld(IDataReader sqlReader)
        {
            List<WhatsAppSettingsTracking> statusList = new List<WhatsAppSettingsTracking>();
            while (sqlReader.Read())
            {
                WhatsAppSettingsTracking obj = new WhatsAppSettingsTracking();

                obj.WhatsAppId = GetFieldValue(sqlReader, "WhatsAppId", 0L);
                obj.Status = GetFieldValue(sqlReader, "Status", 0);
                obj.Order_Number = GetFieldValue(sqlReader, "Order_Number", string.Empty);
                obj.WhtsApp_Order_Date = GetFieldValue(sqlReader, "WhtsApp_Order_Date", DateTime.Now);
                //obj.Share_Images = GetFieldValue(sqlReader, "ShareStatus", string.Empty);
                //obj.Status_Description = GetFieldValue(sqlReader, "Status_Description", string.Empty);
                obj.Guest_MobileNumber = GetFieldValue(sqlReader, "Guest_MobileNumber_Show", string.Empty);
                obj.process_date = GetFieldValue(sqlReader, "process_date_show", DateTime.Now);
                if (obj.process_date < Convert.ToDateTime("2001-05-21 16:27:47.373"))
                {
                    obj.process_date = null;
                }
                obj.IsAvailable = false;

                statusList.Add(obj);
            }
            return statusList;
        }


        private List<WhatsAppSettingsTracking> PopulateWhtsAppStatus(IDataReader sqlReader)
        {
            List<WhatsAppSettingsTracking> statusList = new List<WhatsAppSettingsTracking>();
            while (sqlReader.Read())
            {
                WhatsAppSettingsTracking obj = new WhatsAppSettingsTracking();

                //obj.WhatsAppId = GetFieldValue(sqlReader, "WhatsAppId", 0L);
                //obj.Status = GetFieldValue(sqlReader, "Status", 0);
                obj.Order_Number = GetFieldValue(sqlReader, "Order_Number", string.Empty);
                obj.WhtsApp_Order_Date = GetFieldValue(sqlReader, "WhtsApp_Order_Date", DateTime.Now);
                obj.Share_Images= GetFieldValue(sqlReader, "ShareStatus", string.Empty);
                obj.Status_Description = GetFieldValue(sqlReader, "Status_Description", string.Empty);
                obj.Guest_MobileNumber = GetFieldValue(sqlReader, "Guest_MobileNumber_Show", string.Empty);
                obj.process_date = GetFieldValue(sqlReader, "process_date_show", DateTime.Now);
                if(obj.process_date<Convert.ToDateTime("2001-05-21 16:27:47.373") )
                {
                    obj.process_date = null;
                }
                obj.IsAvailable = false;

                statusList.Add(obj);
            }
            return statusList;
        }

        public bool UpdateWhatsAppOrder(long whtsAPPID,int status)
        {
            DBParameters.Clear();
            AddParameter("@WhatsAppId", whtsAPPID);
            AddParameter("@Status", status);
            ExecuteNonQuery("usp_Update_WhatsApp_Order");
            return true;
        }

        public bool UpdateWhatsAppOrderToReSend(string Order_Number)
        {
            DBParameters.Clear();
            AddParameter("@Order_Number", Order_Number);            
            ExecuteNonQuery("usp_Update_WhatsApp_Order_Resend");
            return true;
        }

        public bool UpdateWhatsAppOrderStatus(string WhtsApp_URL_SourcePath, int status,
            string creation_date, string process_date, string failed_date, string custom_data)
        {
            DBParameters.Clear();

            DateTime? createDate =null ;
            DateTime createDate_temp = new DateTime();
            DateTime? processDate = null;
            DateTime processDate_temp = new DateTime();
            DateTime? failedDate = null;
            DateTime failedDate_temp = new DateTime();

            try
            {
                createDate_temp = DateTime.Parse(creation_date);                
                createDate_temp = createDate_temp.Add(new TimeSpan(3, 0, 0));
                createDate_temp = TimeZoneInfo.ConvertTimeFromUtc(createDate_temp,TimeZoneInfo.Local);
                createDate = createDate_temp;

            }
            catch
            {
                createDate = null;
            }
            try
            {            
                processDate_temp = DateTime.Parse(process_date);
                
                processDate_temp = processDate_temp.Add(new TimeSpan(3, 0, 0));
                processDate_temp = TimeZoneInfo.ConvertTimeFromUtc(processDate_temp, TimeZoneInfo.Local);

                processDate = processDate_temp;
            }
            catch
            {
                processDate = null;
            }
            try
            {
                
                failedDate_temp = DateTime.Parse(failed_date);
                
                failedDate_temp = failedDate_temp.Add(new TimeSpan(3, 0, 0));
                failedDate_temp = TimeZoneInfo.ConvertTimeFromUtc(failedDate_temp, TimeZoneInfo.Local);


                failedDate = failedDate_temp;

            }
            catch
            {
                failedDate = null;
            }
            
            AddParameter("@WhtsApp_URL_SourcePath", WhtsApp_URL_SourcePath);
            AddParameter("@Status", status);
            AddParameter("@creation_date", createDate);


            AddParameter("@process_date", processDate);
            AddParameter("@failed_date", failedDate);
            AddParameter("@custom_data", custom_data);

            ExecuteNonQuery("usp_Update_WhatsApp_Order_Status");
            return true;
        }

        public List<WhatsAppSettingsTracking> GetWhatsAppOrders()
        {
            DBParameters.Clear();
            IDataReader sqlReader = ExecuteReader("usp_Get_WhatsApp_Order");
            List<WhatsAppSettingsTracking> TaxList = PopulateWhatsAppOrders(sqlReader);
            sqlReader.Close();
            return TaxList;
        }

        private List<WhatsAppSettingsTracking> PopulateWhatsAppOrders(IDataReader sqlReader)
        {
            List<WhatsAppSettingsTracking> List = new List<WhatsAppSettingsTracking>();
            while (sqlReader.Read())
            {
                WhatsAppSettingsTracking obj = new WhatsAppSettingsTracking();
                obj.WhatsAppId = GetFieldValue(sqlReader, "WhatsAppId", 0L);
                obj.Order_Number = GetFieldValue(sqlReader, "Order_Number", string.Empty);
                obj.Guest_MobileNumber = GetFieldValue(sqlReader, "Guest_MobileNumber", string.Empty);
                obj.WhtsApp_Image_SourcePath = GetFieldValue(sqlReader, "WhtsApp_Image_SourcePath", string.Empty);
                obj.WhtsApp_URL_SourcePath = GetFieldValue(sqlReader, "WhtsApp_URL_SourcePath", string.Empty);

                List.Add(obj);
            }
            return List;
        }

        public List<TaxDetailInfo> GetTaxDetail(int? orderId)
        {
            DBParameters.Clear();
            AddParameter("@OrderId", orderId);
            IDataReader sqlReader = ExecuteReader("uspGetOrderTaxDetails");
            List<TaxDetailInfo> TaxList = PopulateTaxList(sqlReader);
            sqlReader.Close();
            return TaxList;
        }

        public List<TaxDetailInfo> GetReportTaxDetail(DateTime FromDate, DateTime ToDate, int SubStoreID)
        {
            DBParameters.Clear();
            AddParameter("@FROMDate", FromDate);
            AddParameter("@ToDate", ToDate);
            AddParameter("@SubStoreID", SubStoreID);
            IDataReader sqlReader = ExecuteReader("GetReportTaxDetail");
            List<TaxDetailInfo> TaxList = PopulateTaxList(sqlReader);
            sqlReader.Close();
            return TaxList;
        }

        public bool UpdateStoreTaxData(StoreInfo store)
        {
            AddParameter("@BillReceiptTitle", store.BillReceiptTitle);
            AddParameter("@TaxRegistrationNumber", store.TaxRegistrationNumber);
            AddParameter("@TaxRegistrationText", store.TaxRegistrationText);
            AddParameter("@Address", store.Address);
            AddParameter("@PhoneNo", store.PhoneNo);
            AddParameter("@TaxMinSequenceNo", store.TaxMinSequenceNo);
            AddParameter("@TaxMaxSequenceNo", store.TaxMaxSequenceNo);
            AddParameter("@IsSequenceNoRequired", store.IsSequenceNoRequired);
            AddParameter("@IsTaxEnabled", store.IsTaxEnabled);
            AddParameter("@WebsiteURl", store.WebsiteURL);
            AddParameter("@EmailID", store.EmailID);
            AddParameter("@ServerHotFolderPath", store.ServerHotFolderPath);
            AddParameter("@IsActiveStockShot", store.IsActiveStockShot);
            AddParameter("@RunImageProcessingEngineLocationWise", store.RunImageProcessingEngineLocationWise);
            AddParameter("@RunVideoProcessingEngineLocationWise", store.RunVideoProcessingEngineLocationWise);
            AddParameter("@IsTaxIncluded",store.IsTaxIncluded);
            AddParameter("@PlaceSpecOrderAcrossSites", store.PlaceSpecOrderAcrossSites);
            AddParameter("@RunManualDownloadLocationWise", store.RunManualDownloadLocationWise);
            ExecuteNonQuery("UpdateStoreReceiptData");
            return true;
        }
        public StoreInfo getTaxConfigData()
        {
            StoreInfo store = new StoreInfo();
            IDataReader sqlReader = ExecuteReader("GetTaxConfigData");
            store = PopulateTaxConfigData(sqlReader);
            sqlReader.Close();
            return store;
        }
        public List<TaxDetailInfo> GetApplicableTaxes(int taxID)
        {
            DBParameters.Clear();
            AddParameter("@ParamTaxId",taxID);
            IDataReader sqlReader = ExecuteReader("usp_SEL_TaxList");
            List<TaxDetailInfo> TaxList = PopulateActiveTaxList(sqlReader);
            sqlReader.Close();
            return TaxList;
        }


        #endregion


        #region Private Functions
        private List<TaxDetailInfo> PopulateTaxList(IDataReader sqlReader)
        {
            List<TaxDetailInfo> TaxList = new List<TaxDetailInfo>();
            while (sqlReader.Read())
            {
                TaxDetailInfo TaxInfo = new TaxDetailInfo();
                TaxInfo.TaxAmount = GetFieldValue(sqlReader, "TaxAmount", 0.0M);
                TaxInfo.TaxPercentage = GetFieldValue(sqlReader, "TaxPercentage", 0.0M);
                TaxInfo.TaxName = GetFieldValue(sqlReader, "TaxName", string.Empty);
                TaxInfo.CurrencyName = GetFieldValue(sqlReader, "CurrencyName", string.Empty);
                TaxList.Add(TaxInfo);
            }
            return TaxList;
        }


        private WhatsAppSettings PopulateWhatsAppData(IDataReader sqlReader)
        {
            WhatsAppSettings List = new WhatsAppSettings();
            while (sqlReader.Read())
            {

                List.MobileNumber = GetFieldValue(sqlReader, "MobileNumber", string.Empty);
                List.CountryName = GetFieldValue(sqlReader, "CountryName", string.Empty);
                List.APIKey = GetFieldValue(sqlReader, "APIKey", string.Empty);
                List.HostUrl = GetFieldValue(sqlReader, "HostUrl", string.Empty);

            }
            return List;
        }

        private List<TaxDetailInfo> PopulateActiveTaxList(IDataReader sqlReader)
        {
            List<TaxDetailInfo> TaxList = new List<TaxDetailInfo>();
            while (sqlReader.Read())
            {
                TaxDetailInfo TaxInfo = new TaxDetailInfo();
                TaxInfo.TaxId = GetFieldValue(sqlReader, "TaxId", 0);
                TaxInfo.TaxPercentage = GetFieldValue(sqlReader, "TaxPercentage", 0.0M);
                TaxInfo.TaxName = GetFieldValue(sqlReader, "TaxName", string.Empty);
                //TaxInfo.CurrencyName = GetFieldValue(sqlReader, "CurrencyName", string.Empty);
                TaxList.Add(TaxInfo);
            }
            return TaxList;
        }
        #endregion

        #region Private Functions
        private StoreInfo PopulateTaxConfigData(IDataReader sqlReader)
        {
            StoreInfo store = new StoreInfo();
            while (sqlReader.Read())
            {
                store.BillReceiptTitle = GetFieldValue(sqlReader, "BillReceiptTitle", string.Empty);
                store.Address = GetFieldValue(sqlReader, "Address", string.Empty);
                store.PhoneNo = GetFieldValue(sqlReader, "PhoneNo", string.Empty);
                store.TaxRegistrationNumber = GetFieldValue(sqlReader, "TaxRegistrationNumber", string.Empty);
                store.TaxRegistrationText = GetFieldValue(sqlReader, "TaxRegistrationText", string.Empty);
                store.IsSequenceNoRequired = GetFieldValue(sqlReader, "IsSequenceNoRequired", false);
                store.IsTaxEnabled = GetFieldValue(sqlReader, "IsTaxEnabled", false);
                store.TaxMinSequenceNo = GetFieldValue(sqlReader, "TaxMinSequenceNo", 0L);
                store.TaxMaxSequenceNo = GetFieldValue(sqlReader, "TaxMaxSequenceNo", 0L);
                store.WebsiteURL = GetFieldValue(sqlReader, "WebsiteURL", string.Empty);
                store.EmailID = GetFieldValue(sqlReader, "Email", string.Empty);
                store.ServerHotFolderPath = GetFieldValue(sqlReader, "ServerHotFolderPath", string.Empty);
                store.IsActiveStockShot = GetFieldValue(sqlReader, "IsActiveStockShot", false);
                store.RunImageProcessingEngineLocationWise = GetFieldValue(sqlReader, "RunImageProcessingEngineLocationWise", false);
                store.RunVideoProcessingEngineLocationWise = GetFieldValue(sqlReader, "RunVideoProcessingEngineLocationWise", false);
                store.IsTaxIncluded = GetFieldValue(sqlReader, "IsTaxIncluded", false);
                store.PlaceSpecOrderAcrossSites = GetFieldValue(sqlReader, "PlaceSpecOrderAcrossSites", false);
                store.RunManualDownloadLocationWise = GetFieldValue(sqlReader, "RunManualDownloadLocationWise", false);

            }
            return store;
        }
        #endregion

    }
}
