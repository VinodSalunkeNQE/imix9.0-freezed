﻿using DigiPhoto.IMIX.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace DigiPhoto.IMIX.DataAccess
{
    public class PrinterQueueAccess : BaseDataAccess
    {
        #region Constructor
        public PrinterQueueAccess(BaseDataAccess baseaccess)
            : base(baseaccess)
        {

        }

        public PrinterQueueAccess()
        {

        }
        #endregion

        public PrinterQueueInfo GetPrinterQueue(int subStoreID)
        {
            DBParameters.Clear();
            AddParameter("@SubStoreID", subStoreID);
            IDataReader sqlReader = ExecuteReader("[dbo].[uspGetPrinterQueue]");
            PrinterQueueInfo printerQueueInfo = null;
            if (sqlReader.Read())
            {
                printerQueueInfo = new PrinterQueueInfo();
                printerQueueInfo.DG_PrinterQueue_Pkey = GetFieldValue(sqlReader, "DG_PrinterQueue_Pkey", 0);
                printerQueueInfo.DG_PrinterQueue_ProductID = GetFieldValue(sqlReader, "DG_PrinterQueue_ProductID", 0);
                printerQueueInfo.DG_Orders_ProductType_pkey = GetFieldValue(sqlReader, "DG_Orders_ProductType_pkey", 0);
                printerQueueInfo.DG_Photos_ID = GetFieldValue(sqlReader, "DG_Photos_ID", string.Empty);
                printerQueueInfo.DG_Order_Details_Pkey = GetFieldValue(sqlReader, "DG_Order_Details_Pkey", 0);
                printerQueueInfo.DG_SentToPrinter = GetFieldValue(sqlReader, "DG_SentToPrinter", false);
                printerQueueInfo.is_Active = GetFieldValue(sqlReader, "is_Active", false);                
            }
            return printerQueueInfo;
        }
    }
}
