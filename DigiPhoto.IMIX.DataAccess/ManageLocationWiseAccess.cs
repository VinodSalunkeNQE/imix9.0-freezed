﻿using DigiPhoto.IMIX.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Data.SqlClient;


namespace DigiPhoto.IMIX.DataAccess
{
  public  class ManageLocationWiseAccess : BaseDataAccess
    {
        #region Constructor
        public ManageLocationWiseAccess(BaseDataAccess baseaccess)
            : base(baseaccess)
        {

        }

        public ManageLocationWiseAccess()
        {

        }
        #endregion
        #region Manage LocationWise
        /// <summary>
        /// ///created by latika for locationwise functionality 2019June24
        /// </summary>
        /// <param name="LocationID"></param>
        /// <returns></returns>
        public List<LocationTypeSettingModel> GeLocationTypes(int LocationID)
        {
            DBParameters.Clear();
            //AddParameter("@LocationID", LocationID);
            //SqlConnection conn = new SqlConnection("Data Source=integrationpc;Initial Catalog=Digiphoto;User ID=webusers;Password=webusers;Integrated Security=true;");
            //conn.Open();
            //// IDataReader sqlReader = ExecuteReader("SP_GETLocationWiseType");
            //SqlCommand cmd = new SqlCommand("SP_GETLocationWiseType "+ LocationID, conn);
            //SqlDataAdapter sda = new SqlDataAdapter(cmd);
            //DataSet ds = new DataSet();
            //sda.Fill(ds);
            //List<LocationTypeSettingModel> deviceList = PopulateLocationTypeList(ds.Tables[0].CreateDataReader());
            ////sqlReader.Close();
            //return deviceList;
            DBParameters.Clear();
            AddParameter("@LocationId", LocationID);
            IDataReader sqlReader = ExecuteReader("SP_GETLocationWiseType");
            List<LocationTypeSettingModel> deviceList = PopulateLocationTypeList(sqlReader);
            sqlReader.Close();
            return deviceList;
        }
        ///end
        /// <summary>
        /// ///created by latika for locationwise functionality 2019June24
        /// </summary>
        /// <param name="sqlReader"></param>
        /// <returns></returns>
        private List<LocationTypeSettingModel> PopulateLocationTypeList(IDataReader sqlReader)
        {
            List<LocationTypeSettingModel> LocationWiseTypeList = new List<LocationTypeSettingModel>();
            //Dictionary<int, string> listDic = GetDeviceTypes();
            while (sqlReader.Read())
            {
                LocationTypeSettingModel TypeList = new LocationTypeSettingModel();
                TypeList.ID =Convert.ToInt32(sqlReader["ID"]);
                TypeList.Name = GetFieldValue(sqlReader, "Name", string.Empty);
                LocationWiseTypeList.Add(TypeList);
            }
            return LocationWiseTypeList;
        }
        ///end
        /// <summary>
        /// ///created by latika for locationwise functionality 2019June24
        /// </summary>
        /// <param name="sqlReader"></param>
        /// <returns></returns>
        public Int64 AddEditPosLocationAssociation(int PosLocationId,string PosName,string LocationId,int IMIXConfigurationValueIdS,bool IsActive)
        {
            DBParameters.Clear();
            AddParameter("@PosLocationId", PosLocationId);
            AddParameter("@PosName", PosName);
            AddParameter("@LocationIds", LocationId);
            AddParameter("@IMIXConfigurationValueId", IMIXConfigurationValueIdS);
            AddParameter("@IsActive", IsActive);
            long Id = Convert.ToInt64(ExecuteScalar("USP_AddEditPosLocationAssociation"));
            return Id;
        }
        ///end
        /// <summary>
        /// ///created by latika for locationwise functionality 2019June24
        /// </summary>
        /// <param name="sqlReader"></param>
        /// <returns></returns>
        public List<PosLocationAssociationModel> GETPosLocationAssociation(int POSid,int LocationId)
        {
            DBParameters.Clear();
            AddParameter("@POSid", POSid);
            AddParameter("@LocationId", LocationId);
            IDataReader sqlReader = ExecuteReader("GETPosLocationAssociation");
            List<PosLocationAssociationModel> deviceList = PopulateLocationAssociationList(sqlReader);
            sqlReader.Close();
            return deviceList;
        }
        ///end
        /// <summary>
        /// ///created by latika for locationwise functionality 2019June24
        /// </summary>
        /// <param name="sqlReader"></param>
        /// <returns></returns>
        private List<PosLocationAssociationModel> PopulateLocationAssociationList(IDataReader sqlReader)
        {
            List<PosLocationAssociationModel> LocationAssociationList = new List<PosLocationAssociationModel>();
            //Dictionary<int, string> listDic = GetDeviceTypes();
            while (sqlReader.Read())
            {
                PosLocationAssociationModel TypeList = new PosLocationAssociationModel();
                TypeList.PosLocationId = GetFieldValue(sqlReader, "PosLocationId", 0);
                TypeList.PosName = GetFieldValue(sqlReader, "PosName", string.Empty);
                TypeList.DG_Location_Name = GetFieldValue(sqlReader, "DG_Location_Name", string.Empty);
                TypeList.IsActiveName = GetFieldValue(sqlReader, "IsActiveName", string.Empty);
                TypeList.Settings = GetFieldValue(sqlReader, "Settings", string.Empty);
                TypeList.IMIXConfigurationValueId = GetFieldValue(sqlReader, "IMIXConfigurationValueId", 0);
                TypeList.IsActive = GetFieldValue(sqlReader, "IsActive", false);
                
                LocationAssociationList.Add(TypeList);
            }
            return LocationAssociationList;
        }
        ///end
        /// <summary>
        /// ///created by latika for locationwise functionality 2019June24
        /// </summary>
        /// <param name="sqlReader"></param>
        /// <returns></returns>
        #endregion
    }
}
