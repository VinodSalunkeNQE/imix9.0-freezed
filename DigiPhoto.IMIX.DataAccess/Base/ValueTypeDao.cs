﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DigiPhoto.IMIX.Model;
using System.Data;

namespace DigiPhoto.IMIX.DataAccess
{
    public class ValueTypeDao : BaseDataAccess
    {
        #region Constructor
        public ValueTypeDao(BaseDataAccess baseaccess)
            : base(baseaccess)
        { }
        public ValueTypeDao()
        { }
        #endregion
        public DateTime GetServerDateTime()
        {
            DateTime serverDatetime;
            object result = ExecuteScalar(DAOConstant.USP_GET_SERVERDATETIME);
            serverDatetime = (DateTime)result;
            return serverDatetime;
        }

        public List<ValueTypeInfo> GetScanTypes()
        {

            IDataReader sqlReader = ExecuteReader(DAOConstant.USP_GET_SCANTYPES);
            List<ValueTypeInfo> ValueTypeList = MapValueTypeInfo(sqlReader);
            sqlReader.Close();
            return ValueTypeList;
        }
        private List<ValueTypeInfo> MapValueTypeInfo(IDataReader sqlReader)
        {
            List<ValueTypeInfo> ValueTypeList = new List<ValueTypeInfo>();
            while (sqlReader.Read())
            {
                ValueTypeInfo ValueType = new ValueTypeInfo()
                {
                    ValueTypeId = GetFieldValue(sqlReader, "ValueTypeId", 0),
                    Name = GetFieldValue(sqlReader, "Name", string.Empty),
                };
                ValueTypeList.Add(ValueType);
            }
            return ValueTypeList;
        }


        public List<ValueTypeInfo> GetCardTypes()
        {
            IDataReader sqlReader = ExecuteReader(DAOConstant.USP_GET_CARDTYPES);
            List<ValueTypeInfo> ValueTypeList = MapValueTypeBase(sqlReader);
            sqlReader.Close();
            return ValueTypeList;
        }
        private List<ValueTypeInfo> MapValueTypeBase(IDataReader sqlReader)
        {
            List<ValueTypeInfo> ValueTypeList = new List<ValueTypeInfo>();
            while (sqlReader.Read())
            {
                ValueTypeInfo ValueType = new ValueTypeInfo()
                {
                    ValueTypeId = GetFieldValue(sqlReader, "ValueTypeId", 0),
                    Name = GetFieldValue(sqlReader, "Name", string.Empty),
                }; 

                ValueTypeList.Add(ValueType);
            }
            return ValueTypeList;
        }
      
        public List<ValueTypeInfo> SelectValueTypeInfoList(int valueTypeGroupId)
        {
            DBParameters.Clear();
            AddParameter("@ParamValueTypeGroupId", SetNullIntegerValue(valueTypeGroupId));
            IDataReader sqlReader = ExecuteReader(DAOConstant.usp_SEL_ValueType);
            List<ValueTypeInfo> ValueTypeList = MapValueTypeBase(sqlReader);
            sqlReader.Close();
            return ValueTypeList;
        }
        public List<ValueTypeInfo> SelectValueTypeList()
        {
            DBParameters.Clear();
            this.OpenConnection();
            AddParameter("@ParamValueTypeGroupId", SetNullIntegerValue(null));
            IDataReader sqlReader = ExecuteReader(DAOConstant.usp_SEL_ValueType);
            List<ValueTypeInfo> ValueTypeList = MapValueType(sqlReader);
            sqlReader.Close();
            return ValueTypeList;
        }
        private List<ValueTypeInfo> MapValueType(IDataReader sqlReader)
        {
            List<ValueTypeInfo> ValueTypeList = new List<ValueTypeInfo>();
            while (sqlReader.Read())
            {
                ValueTypeInfo ValueType = new ValueTypeInfo()
                {
                    ValueTypeId = GetFieldValue(sqlReader, "ValueTypeId", 0),
                    Name = GetFieldValue(sqlReader, "Name", string.Empty),
                    ValueTypeGroupId = GetFieldValue(sqlReader, "ValueTypeGroupId", 0),
                    DisplayOrder = GetFieldValue(sqlReader, "DisplayOrder", 0),
                };

                ValueTypeList.Add(ValueType);
            }
            return ValueTypeList;
        }
    }
}
