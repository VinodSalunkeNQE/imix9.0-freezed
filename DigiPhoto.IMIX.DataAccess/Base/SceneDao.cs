﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DigiPhoto.IMIX.Model;
using System.Data;

namespace DigiPhoto.IMIX.DataAccess
{
    public class SceneDao : BaseDataAccess
    {
        #region Constructor
        public SceneDao(BaseDataAccess baseaccess)
            : base(baseaccess)
        { }
        public SceneDao()
        { }
        #endregion


        public List<SceneInfo> GetSceneDetails()
        {
            List<SceneInfo> objectList;
            this.OpenConnection();
            using (IDataReader sqlReader = ExecuteReader(DAOConstant.usp_SEL_SCENE))
            {
                objectList = MapObject(sqlReader);
            }
            return objectList;
        }
        private List<SceneInfo> MapObject(IDataReader sqlReader)
        {
            List<SceneInfo> objectList = new List<SceneInfo>();
            while (sqlReader.Read())
            {
                SceneInfo objectValue = new SceneInfo
                {
                    SceneId = GetFieldValue(sqlReader, "SceneId", 0),
                    ProductId = GetFieldValue(sqlReader, "ProductId", 0),
                    ProductName = GetFieldValue(sqlReader, "ProductName", string.Empty),
                    BackGroundId = GetFieldValue(sqlReader, "BackgroundId", 0),
                    BackgroundName = GetFieldValue(sqlReader, "BackgroundName", string.Empty),
                    BorderId = GetFieldValue(sqlReader, "BorderId", 0),
                    BorderName = GetFieldValue(sqlReader, "BorderName", string.Empty),
                    SyncCode = GetFieldValue(sqlReader, "SyncCode", string.Empty),
                    IsSynced = GetFieldValue(sqlReader, "IsSynced", false),
                    IsActive = GetFieldValue(sqlReader, "IsActive", false),
                    IsBlankPage = GetFieldValue(sqlReader, "IsBlankPage", false),
                    SceneName = GetFieldValue(sqlReader, "SceneName", string.Empty),
                    PageNo = GetFieldValue(sqlReader, "PageNo", string.Empty),       // Get PageNo Added By Ajinkya
                    ThemeName = GetFieldValue(sqlReader, "ThemeName", string.Empty), // Stored ThemeName by Id Added By Ajinkya
                    ThemeId = GetFieldValue(sqlReader, "ThemeId", 0),
                    ChineseFont = GetFieldValue(sqlReader, "ChineseFont", string.Empty),
                    EnglishFont = GetFieldValue(sqlReader, "EnglishFont", string.Empty),
                };

                objectList.Add(objectValue);
            }
            return objectList;
        }

        public bool Add(SceneInfo objectInfo)
        {
            DBParameters.Clear();
            AddParameter("@DG_Product_Id", objectInfo.ProductId);
            AddParameter("@DG_BackGround_Id", objectInfo.BackGroundId);
            AddParameter("@DG_Border_Id", objectInfo.BorderId);
            AddParameter("@DG_Graphics_Id", objectInfo.GraphicsId);
            AddParameter("@SyncCode", objectInfo.SyncCode);
            AddParameter("@IsSynced", objectInfo.IsSynced);
            AddParameter("@IsActive", objectInfo.IsActive);
            AddParameter("@DG_SceneName", objectInfo.SceneName);
            AddParameter("@DG_Scene_pkey", objectInfo.SceneId);
            AddParameter("@DG_ThemeId", objectInfo.ThemeId);  // Stored ThemeId Added By Ajinkya
            AddParameter("@DG_PageNo", objectInfo.PageNo);  // Stored pageNo Added By Ajinkya
            AddParameter("@IsBlankPage", objectInfo.IsBlankPage);  // Stored pageNo Added By Ajinkya
            AddParameter("@DG_ChineseFont", objectInfo.ChineseFont);  // Stored pageNo Added By Ajinkya
            AddParameter("@DG_EnglishFont", objectInfo.EnglishFont);  // Stored pageNo Added By Ajinkya
            ExecuteNonQuery(DAOConstant.USP_INS_SCENE);
            return true;
        }
        public bool Delete(int objectvalueId)
        {
            DBParameters.Clear();
            AddParameter("@DG_Scene_pkey", objectvalueId);
            ExecuteNonQuery(DAOConstant.USP_DEL_SCENE);
            return true;
        }
      
        private List<SceneInfo> MapObjects(IDataReader sqlReader)
        {
            List<SceneInfo> objectList = new List<SceneInfo>();
            while (sqlReader.Read())
            {
                SceneInfo objectValue = new SceneInfo
                {
                    SceneId = GetFieldValue(sqlReader, "SceneId", 0),
                    ProductId = GetFieldValue(sqlReader, "ProductId", 0),
                    ProductName = GetFieldValue(sqlReader, "ProductName", string.Empty),
                    BackGroundId = GetFieldValue(sqlReader, "BackgroundId", 0),
                    BackgroundName = GetFieldValue(sqlReader, "BackgroundName", string.Empty),
                    BorderId = GetFieldValue(sqlReader, "BorderId", 0),
                    BorderName = GetFieldValue(sqlReader, "BorderName", string.Empty),
                    SyncCode = GetFieldValue(sqlReader, "SyncCode", string.Empty),
                    IsSynced = GetFieldValue(sqlReader, "IsSynced", false),
                    IsActive = GetFieldValue(sqlReader, "IsActive", false),
                    IsBlankPage = GetFieldValue(sqlReader, "IsBlankPage", false),
                    SceneName = GetFieldValue(sqlReader, "SceneName", string.Empty),
                    PageNo = GetFieldValue(sqlReader, "PageNo", string.Empty),       // Get PageNo Added By Ajinkya
                    //ThemeName = GetFieldValue(sqlReader, "ThemeName", string.Empty), // Stored ThemeName by Id Added By Ajinkya
                    ThemeId = GetFieldValue(sqlReader, "ThemeId", 0),
                    ChineseFont = GetFieldValue(sqlReader, "ChineseFont", string.Empty),
                    EnglishFont = GetFieldValue(sqlReader, "EnglishFont", string.Empty),

                };

                objectList.Add(objectValue);
            }
            return objectList;
        }
       
        public SceneInfo GetSceneFromId(int? sceneId)
        {
            DBParameters.Clear();
            AddParameter("@SceneId", SetNullIntegerValue(sceneId));

            List<SceneInfo> objectList;
            using (IDataReader sqlReader = ExecuteReader(DAOConstant.USP_sel_SCENEID))
            {
                objectList = MapObject(sqlReader);
            }
            return objectList.FirstOrDefault();
        }
        public List<SceneInfo> GetSceneFromThemeId(int? themeId)
        {
            DBParameters.Clear();
            AddParameter("@ThemeId", SetNullIntegerValue(themeId));

            List<SceneInfo> objectList;
            using (IDataReader sqlReader = ExecuteReader(DAOConstant.usp_SEL_SCENEFromThemeID))
            {
                objectList = MapObjects(sqlReader);
            }
            return objectList;
        }

        public SceneInfo GetSceneFromSceneId(int? sceneId)
        {
            DBParameters.Clear();
            AddParameter("@SceneId", SetNullIntegerValue(sceneId));

            List<SceneInfo> objectList;
            using (IDataReader sqlReader = ExecuteReader(DAOConstant.usp_SEL_SCENEFromSceneID))
            {
                objectList = MapObject(sqlReader);
            }
            return objectList.FirstOrDefault();
        }
        public List<SceneInfo> GetAllBlankPageByThemeID(int? themeId)
        {
            DBParameters.Clear();
            AddParameter("@ThemeId", SetNullIntegerValue(themeId));

            List<SceneInfo> objectList;
            this.OpenConnection();
            using (IDataReader sqlReader = ExecuteReader(DAOConstant.USP_SEL_ALLBLANKPAGEBYTHEMEID))
            {
                objectList = MapAllBlankPageObject(sqlReader);
            }
            return objectList;
        }
        private List<SceneInfo> MapAllBlankPageObject(IDataReader sqlReader)
        {
            List<SceneInfo> objectList = new List<SceneInfo>();
            while (sqlReader.Read())
            {
                SceneInfo objectValue = new SceneInfo
                {
                    SceneId = GetFieldValue(sqlReader, "SceneId", 0),
                    ProductId = GetFieldValue(sqlReader, "ProductId", 0),
                    ProductName = GetFieldValue(sqlReader, "ProductName", string.Empty),
                    BackGroundId = GetFieldValue(sqlReader, "BackgroundId", 0),
                    BackgroundName = GetFieldValue(sqlReader, "BackgroundName", string.Empty),
                    BorderId = GetFieldValue(sqlReader, "BorderId", 0),
                    BorderName = GetFieldValue(sqlReader, "BorderName", string.Empty),
                    SyncCode = GetFieldValue(sqlReader, "SyncCode", string.Empty),
                    IsSynced = GetFieldValue(sqlReader, "IsSynced", false),
                    IsActive = GetFieldValue(sqlReader, "IsActive", false),
                    IsBlankPage = GetFieldValue(sqlReader, "IsBlankPage", false),
                    SceneName = GetFieldValue(sqlReader, "SceneName", string.Empty),
                    PageNo = GetFieldValue(sqlReader, "PageNo", string.Empty),
                    ThemeName = GetFieldValue(sqlReader, "ThemeName", string.Empty),
                    ThemeId = GetFieldValue(sqlReader, "ThemeId", 0),
                };

                objectList.Add(objectValue);
            }
            return objectList;
        }
    }
}

