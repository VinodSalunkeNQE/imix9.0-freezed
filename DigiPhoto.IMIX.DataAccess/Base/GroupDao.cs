﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DigiPhoto.IMIX.Model;
using System.Data;


namespace DigiPhoto.IMIX.DataAccess
{
    public class GroupDao : BaseDataAccess
    {
        #region Constructor
        public GroupDao(BaseDataAccess baseaccess)
            : base(baseaccess)
        { }
        public GroupDao()
        { }
        #endregion

        public bool AddGroup(GroupDetails groupInfo)
        {
            DBParameters.Clear();
            AddParameter("@ParamDG_Group_pkey", groupInfo.DG_Group_pkey);
            AddParameter("@ParamDG_Group_Name", groupInfo.DG_Group_Name);
            AddParameter("@ParamSyncCode", groupInfo.SyncCode);
            AddParameter("@ParamIsSynced", groupInfo.IsSynced);
            AddParameter("@OperationType", groupInfo.OperationType);
            ExecuteNonQuery(DAOConstant.USP_INS_GROUP);
            return true;
        }

        public List<GroupDetails> GetGroupDetails(int GroupPkey)
        {
            DBParameters.Clear();
            AddParameter("@ParamDG_Group_pkey", GroupPkey);
            List<GroupDetails> objectList;
            this.OpenConnection();
            using (IDataReader sqlReader = ExecuteReader(DAOConstant.usp_SEL_GROUP))
            {
                objectList = MapObject(sqlReader);
            }
            return objectList;
        }

        private List<GroupDetails> MapObject(IDataReader sqlReader)
        {
            List<GroupDetails> objectList = new List<GroupDetails>();
            while (sqlReader.Read())
            {
                GroupDetails objectValue = new GroupDetails
                {
                    DG_Group_pkey = GetFieldValue(sqlReader, "DG_Group_pkey", 0),
                    DG_Group_Name = GetFieldValue(sqlReader, "DG_Group_Name", string.Empty)

                };
                objectList.Add(objectValue);
            }
            return objectList;
        }


        public bool AddGroupProduct(GroupDetails groupInfo)
        {
            DBParameters.Clear();
            AddParameter("@ParamDG_Group_pkey", groupInfo.DG_Group_pkey);
            AddParameter("@ParamDG_ProductCode", groupInfo.DG_ProductCode);
            AddParameter("@OperationType", groupInfo.OperationType);
            ExecuteNonQuery(DAOConstant.USP_UPDANDINS_GroupProductCode);
            return true;
        }

        public List<GroupDetails> GetGroupProductDetails(int GroupPkey)
        {
            DBParameters.Clear();
            AddParameter("@ParamDG_Group_pkey", GroupPkey);
            List<GroupDetails> objectList;
            this.OpenConnection();
            using (IDataReader sqlReader = ExecuteReader(DAOConstant.USP_SEL_GroupProduct))
            {
                objectList = MapGpPrdObject(sqlReader);
            }
            return objectList;
        }

        private List<GroupDetails> MapGpPrdObject(IDataReader sqlReader)
        {
            List<GroupDetails> objectList = new List<GroupDetails>();
            while (sqlReader.Read())
            {
                GroupDetails objectValue = new GroupDetails
                {
                    DG_Group_pkey = GetFieldValue(sqlReader, "DG_GrpPrd_Pkey", 0),
                    DG_ProductCode = GetFieldValue(sqlReader, "ProductCodePrefix", string.Empty),
                    DG_Group_Name  = GetFieldValue(sqlReader, "DG_Group_Name", string.Empty)

                };
                objectList.Add(objectValue);
            }
            return objectList;
        }

    }
}

