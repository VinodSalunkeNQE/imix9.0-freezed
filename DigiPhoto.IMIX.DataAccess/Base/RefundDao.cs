﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DigiPhoto.IMIX.Model;
using System.Data;


namespace DigiPhoto.IMIX.DataAccess
{
    public class RefundDao: BaseDataAccess
    {
        #region Constructor
        public RefundDao(BaseDataAccess baseaccess)
            : base(baseaccess)
        { }
        public RefundDao()
        { }
        #endregion



        public List<RefundInfo> GetRefund(int OrderId)
        {
            DBParameters.Clear();
            AddParameter("@ParamOrderId", OrderId);

            IDataReader sqlReader = ExecuteReader(DAOConstant.USP_SEL_REFUND);
            List<RefundInfo> RefundList = MapRefundInfo(sqlReader);
            sqlReader.Close();
            return RefundList;
        }
        private List<RefundInfo> MapRefundInfo(IDataReader sqlReader)
        {
            List<RefundInfo> RefundList = new List<RefundInfo>();
            while (sqlReader.Read())
            {
                RefundInfo Refund = new RefundInfo()
                {
                    DG_RefundId = GetFieldValue(sqlReader, "DG_RefundId", 0),
                    DG_OrderId = GetFieldValue(sqlReader, "DG_OrderId", 0),
                    RefundAmount = GetFieldValue(sqlReader, "RefundAmount", 0.0M),
                    RefundDate = GetFieldValue(sqlReader, "RefundDate", DateTime.Now),
                    UserId = GetFieldValue(sqlReader, "UserId", 0),
                    Refund_Mode = GetFieldValue(sqlReader, "Refund_Mode", 0),

                };

                RefundList.Add(Refund);
            }
            return RefundList;
        }


        //public List<RefundInfo> Get(int OrderID)
        //{
        //    DBParameters.Clear();
        //    AddParameter("@DG_OrderID", OrderID);

        //    IDataReader sqlReader = ExecuteReader(DAOConstant.GETREFUNDEDITEMS);
        //    List<RefundInfo> RefundList = MapRefundedItemsInfo(sqlReader);
        //    sqlReader.Close();
        //    return RefundList;
        //}

        //private List<RefundInfo> MapRefundedItemsInfo(IDataReader sqlReader)
        //{
        //    List<RefundInfo> RefundList = new List<RefundInfo>();
        //    while (sqlReader.Read())
        //    {
        //        RefundInfo Refund = new RefundInfo()
        //        {
        //            DG_RefundId = GetFieldValue(sqlReader, "DG_RefundId", 0),
        //            DG_OrderId = GetFieldValue(sqlReader, "DG_OrderId", 0),
        //            DG_LineItemId = GetFieldValue(sqlReader, "DG_LineItemId", 0),
        //            RefundPhotoId = GetFieldValue(sqlReader, "RefundPhotoId", 0),
        //            DG_RefundMaster_ID = GetFieldValue(sqlReader, "DG_RefundMaster_ID", 0),

        //        };

        //        RefundList.Add(Refund);
        //    }
        //    return RefundList;
        //}


        public int InsandDelRefundMasterData(RefundInfo objectInfo)
        {
            DBParameters.Clear();
            AddParameter("@ParamOrderId",objectInfo.DG_OrderId);
            AddParameter("@ParamUserId", objectInfo.UserId);
            AddParameter("@ParamRefundDate", objectInfo.RefundDate);
            AddParameter("@ParamRefundAmount", objectInfo.RefundAmount);
            AddParameter("@ParamRefundId", objectInfo.DG_RefundId,ParameterDirection.Output);
            
             ExecuteReader(DAOConstant.USP_INSANDDEL_REFUNDMASTERDATA);
             int RefundId = (int)GetOutParameterValue("@ParamRefundId");
             return RefundId;
        }
        public bool SetRefundDetailsData(int DG_LineItemId, int DG_RefundMaster_ID, string PhotoId, decimal? Refundprice, string Reason)
        {
            DBParameters.Clear();
            AddParameter("@ParamDG_LineItemId", DG_LineItemId);
            AddParameter("@ParamDG_RefundMaster_ID", DG_RefundMaster_ID);
            AddParameter("@ParamPhotoId", PhotoId);
            AddParameter("@ParamRefundprice", Refundprice);
            AddParameter("@ParamReason", Reason);

            ExecuteReader(DAOConstant.USP_INS_REFUNDDETAILSDATA);
            
            return true;
        }

        public List<RefundInfo> GetRefundedItems(int OrderID)
        {
            DBParameters.Clear();
            AddParameter("@DG_OrderID", OrderID);

            IDataReader sqlReader = ExecuteReader(DAOConstant.GETREFUNDEDITEMS);
            List<RefundInfo> RefundList = MapRefundedItemsInfo(sqlReader);
            sqlReader.Close();
            return RefundList;
        }

        private List<RefundInfo> MapRefundedItemsInfo(IDataReader sqlReader)
        {
            List<RefundInfo> RefundList = new List<RefundInfo>();
            while (sqlReader.Read())
            {
                RefundInfo Refund = new RefundInfo()
                {
                    DG_LineItemId = GetFieldValue(sqlReader, "Lineitemid", 0),
                    RefundPhotoId = GetFieldValue(sqlReader, "PhotoID", 0),
                };

                RefundList.Add(Refund);
            }
            return RefundList;
        }
        
    }
}
