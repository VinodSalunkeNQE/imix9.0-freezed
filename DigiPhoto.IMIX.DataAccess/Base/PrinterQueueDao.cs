﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DigiPhoto.IMIX.Model;
using System.Data;
using System.Collections.ObjectModel;

namespace DigiPhoto.IMIX.DataAccess
{
    public class PrinterQueueDao : BaseDataAccess
    {
        #region Constructor
        public PrinterQueueDao(BaseDataAccess baseaccess)
            : base(baseaccess)
        { }
        public PrinterQueueDao()
        { }
        #endregion

        public List<PrinterQueueInfo> GetAllFilteredPrinterQueue()
        {
            IDataReader sqlReader = ExecuteReader(DAOConstant.USP_SEL_FILTEREDPRINTERQUEUE);
            List<PrinterQueueInfo> GetFilteredPrinterQueueList = MapFilteredPrinterQueueInfo(sqlReader);
            sqlReader.Close();
            return GetFilteredPrinterQueueList;
        }

        private List<PrinterQueueInfo> MapFilteredPrinterQueueInfo(IDataReader sqlReader)
        {
            List<PrinterQueueInfo> GetFilteredPrinterQueueList = new List<PrinterQueueInfo>();
            while (sqlReader.Read())
            {
                PrinterQueueInfo GetFilteredPrinterQueue = new PrinterQueueInfo()
                {
                    DG_PrinterQueue_Pkey = GetFieldValue(sqlReader, "DG_PrinterQueue_Pkey", 0),
                    DG_Order_Details_Pkey = GetFieldValue(sqlReader, "DG_Order_Details_Pkey", 0),
                    DG_SentToPrinter = GetFieldValue(sqlReader, "DG_SentToPrinter", false),
                    DG_AssociatedPrinters_Name = GetFieldValue(sqlReader, "DG_AssociatedPrinters_Name", string.Empty),
                    DG_Orders_ProductType_Name = GetFieldValue(sqlReader, "DG_Orders_ProductType_Name", string.Empty),
                    DG_Photos_RFID = GetFieldValue(sqlReader, "DG_Photos_RFID", string.Empty),
                    DG_Associated_PrinterId = GetFieldValue(sqlReader, "DG_Associated_PrinterId", 0),
                    DG_PrinterQueue_ProductID = GetFieldValue(sqlReader, "DG_PrinterQueue_ProductID", 0),
                    is_Active = GetFieldValue(sqlReader, "is_Active", false),
                    QueueIndex = GetFieldValue(sqlReader, "QueueIndex", 0),
                    DG_Orders_ProductType_pkey = GetFieldValue(sqlReader, "DG_Orders_ProductType_pkey", 0),
                    DG_Orders_Number = GetFieldValue(sqlReader, "DG_Orders_Number", string.Empty),
                    DG_Orders_pkey = GetFieldValue(sqlReader, "DG_Orders_pkey", 0),
                    DG_Order_SubStoreId = GetFieldValue(sqlReader, "DG_Order_SubStoreId", 0),
                    DG_Photos_ID = GetFieldValue(sqlReader, "DG_Photos_ID", string.Empty),

                };
                GetFilteredPrinterQueueList.Add(GetFilteredPrinterQueue);
            }
            return GetFilteredPrinterQueueList;
        }


        public List<PrinterQueueInfo> GetAllFilteredPrinterQueueBySubStoreId(int substoreID)
        {
            DBParameters.Clear();
            AddParameter("@ParamSubStoreID", substoreID);
            IDataReader sqlReader = ExecuteReader(DAOConstant.USP_SEL_FILTEREDPRINTERQUEUEBYSUBSTOREID);
            List<PrinterQueueInfo> GetFilteredPrinterQueueList = MapFilteredPrinterQueue(sqlReader);
            sqlReader.Close();
            return GetFilteredPrinterQueueList;
        }

        private List<PrinterQueueInfo> MapFilteredPrinterQueue(IDataReader sqlReader)
        {
            List<PrinterQueueInfo> GetFilteredPrinterQueueList = new List<PrinterQueueInfo>();
            while (sqlReader.Read())
            {
                ////PrinterQueueInfo O = new PrinterQueueInfo();
                ////O.DG_PrinterQueue_Pkey = GetFieldValue(sqlReader, "DG_PrinterQueue_Pkey", 0);
                ////O.DG_Order_Details_Pkey = GetFieldValue(sqlReader, "DG_Order_Details_Pkey", 0);
                ////O.DG_SentToPrinter = GetFieldValue(sqlReader, "DG_SentToPrinter", false);
                ////O.DG_AssociatedPrinters_Name = GetFieldValue(sqlReader, "DG_AssociatedPrinters_Name", string.Empty);
                ////O.DG_Orders_ProductType_Name = GetFieldValue(sqlReader, "DG_Orders_ProductType_Name", string.Empty);
                ////O.DG_Photos_RFID = GetFieldValue(sqlReader, "DG_Photos_RFID", string.Empty);
                ////O.DG_Associated_PrinterId = GetFieldValue(sqlReader, "DG_Associated_PrinterId", 0);
                ////O.DG_PrinterQueue_ProductID = GetFieldValue(sqlReader, "DG_PrinterQueue_ProductID", 0);
                ////O.is_Active = GetFieldValue(sqlReader, "is_Active", false);
                ////O.QueueIndex = GetFieldValue(sqlReader, "QueueIndex", 0);
                ////O.DG_Orders_ProductType_pkey = GetFieldValue(sqlReader, "DG_Orders_ProductType_pkey", 0);
                ////O.DG_Orders_Number = GetFieldValue(sqlReader, "DG_Orders_Number", string.Empty);
                ////O.DG_Orders_pkey = GetFieldValue(sqlReader, "DG_Orders_pkey", 0);
                ////O.DG_Order_SubStoreId = GetFieldValue(sqlReader, "DG_Order_SubStoreId", 0);
                ////O.DG_Photos_ID = GetFieldValue(sqlReader, "DG_Photos_ID", string.Empty);

                PrinterQueueInfo GetFilteredPrinterQueue = new PrinterQueueInfo()
                {
                    DG_PrinterQueue_Pkey = GetFieldValue(sqlReader, "DG_PrinterQueue_Pkey", 0),
                    DG_Order_Details_Pkey = GetFieldValue(sqlReader, "DG_Order_Details_Pkey", 0),
                    DG_SentToPrinter = GetFieldValue(sqlReader, "DG_SentToPrinter", false),
                    DG_AssociatedPrinters_Name = GetFieldValue(sqlReader, "DG_AssociatedPrinters_Name", string.Empty),
                    DG_Orders_ProductType_Name = GetFieldValue(sqlReader, "DG_Orders_ProductType_Name", string.Empty),
                    DG_Photos_RFID = GetFieldValue(sqlReader, "DG_Photos_RFID", string.Empty),
                    DG_Associated_PrinterId = GetFieldValue(sqlReader, "DG_Associated_PrinterId", 0),
                    DG_PrinterQueue_ProductID = GetFieldValue(sqlReader, "DG_PrinterQueue_ProductID", 0),
                    is_Active = GetFieldValue(sqlReader, "is_Active", false),
                    QueueIndex = GetFieldValue(sqlReader, "QueueIndex", 0),
                    DG_Orders_ProductType_pkey = GetFieldValue(sqlReader, "DG_Orders_ProductType_pkey", 0),
                    DG_Orders_Number = GetFieldValue(sqlReader, "DG_Orders_Number", string.Empty),
                    DG_Orders_pkey = GetFieldValue(sqlReader, "DG_Orders_pkey", 0),
                    DG_Order_SubStoreId = GetFieldValue(sqlReader, "DG_Order_SubStoreId", 0),
                    DG_Photos_ID = GetFieldValue(sqlReader, "DG_Photos_ID", string.Empty),

                };
                GetFilteredPrinterQueueList.Add(GetFilteredPrinterQueue);
            }
            return GetFilteredPrinterQueueList;
        }
        public List<PrinterQueueInfo> Select()
        {
            IDataReader sqlReader = ExecuteReader(DAOConstant.USP_SEL_PRINTERQUEUE);
            List<PrinterQueueInfo> PrinterQueueList = MapPrinterQueueInfo(sqlReader);
            sqlReader.Close();
            return PrinterQueueList;
        }
        public PrinterQueueInfo Get()
        {
            IDataReader sqlReader = ExecuteReader(DAOConstant.USP_GET_PRINTERQUEUE);
            List<PrinterQueueInfo> PrinterQueueList = MapPrinterQueueInfo(sqlReader);
            sqlReader.Close();
            return PrinterQueueList.FirstOrDefault();
        }
        private List<PrinterQueueInfo> MapPrinterQueueInfo(IDataReader sqlReader)
        {
            List<PrinterQueueInfo> PrinterQueueList = new List<PrinterQueueInfo>();
            while (sqlReader.Read())
            {
                PrinterQueueInfo PrinterQueue = new PrinterQueueInfo()
                {
                    DG_PrinterQueue_Pkey = GetFieldValue(sqlReader, "DG_PrinterQueue_Pkey", 0),
                    DG_PrinterQueue_ProductID = GetFieldValue(sqlReader, "DG_PrinterQueue_ProductID", 0),
                    DG_PrinterQueue_Image_Pkey = GetFieldValue(sqlReader, "DG_PrinterQueue_Image_Pkey", string.Empty),
                    DG_Associated_PrinterId = GetFieldValue(sqlReader, "DG_Associated_PrinterId", 0),
                    DG_Order_Details_Pkey = GetFieldValue(sqlReader, "DG_Order_Details_Pkey", 0),
                    DG_SentToPrinter = GetFieldValue(sqlReader, "DG_SentToPrinter", false),
                    is_Active = GetFieldValue(sqlReader, "is_Active", false),
                    QueueIndex = GetFieldValue(sqlReader, "QueueIndex", 0),
                    DG_IsSpecPrint = GetFieldValue(sqlReader, "DG_IsSpecPrint", false),
                    DG_Print_Date = GetFieldValue(sqlReader, "DG_Print_Date", DateTime.Now),
                    RotationAngle = GetFieldValue(sqlReader, "RotationAngle", string.Empty),

                };

                PrinterQueueList.Add(PrinterQueue);
            }
            return PrinterQueueList;
        }
        public int Add(PrinterQueueInfo objectInfo)
        {
            DBParameters.Clear();
            AddParameter("@ParamDG_PrinterQueue_Pkey", objectInfo.DG_PrinterQueue_Pkey);
            AddParameter("@ParamDG_PrinterQueue_ProductID", objectInfo.DG_PrinterQueue_ProductID);
            AddParameter("@ParamDG_Associated_PrinterId", objectInfo.DG_Associated_PrinterId);
            AddParameter("@ParamDG_Order_Details_Pkey", objectInfo.DG_Order_Details_Pkey);
            AddParameter("@ParamQueueIndex", objectInfo.QueueIndex);
            AddParameter("@ParamDG_Print_Date", objectInfo.DG_Print_Date);
            AddParameter("@ParamDG_SentToPrinter", objectInfo.DG_SentToPrinter);
            AddParameter("@Paramis_Active", objectInfo.is_Active);
            AddParameter("@ParamDG_IsSpecPrint", objectInfo.DG_IsSpecPrint);
            AddParameter("@ParamPrinterQueueId", objectInfo.DG_PrinterQueue_Pkey, ParameterDirection.Output);
            AddParameter("@ParamDG_PrinterQueue_Image_Pkey", objectInfo.DG_PrinterQueue_Image_Pkey);
            AddParameter("@ParamRotationAngle", objectInfo.RotationAngle);

            ExecuteNonQuery(DAOConstant.USP_INS_PRINTERQUEUE);
            int objectId = (int)GetOutParameterValue("@ParamPrinterQueueId");
            return objectId;
        }


        public List<PrinterQueueInfo> GetPrinterQueueDetails(int QueueID)
        {
            DBParameters.Clear();
            AddParameter("@QueueID", QueueID);

            IDataReader sqlReader = ExecuteReader(DAOConstant.GETPRINTERQUEUEDETAILS);
            List<PrinterQueueInfo> OrdersList = MapPrinterQueueDetails(sqlReader);
            sqlReader.Close();
            return OrdersList;
        }
        private List<PrinterQueueInfo> MapPrinterQueueDetails(IDataReader sqlReader)
        {
            List<PrinterQueueInfo> OrdersList = new List<PrinterQueueInfo>();
            while (sqlReader.Read())
            {
                PrinterQueueInfo Orders = new PrinterQueueInfo()
                {
                    DG_Orders_pkey = GetFieldValue(sqlReader, "DG_Orders_pkey", 0),
                    DG_Orders_Number = GetFieldValue(sqlReader, "DG_Orders_Number", string.Empty),
                    DG_Orders_LineItems_pkey = GetFieldValue(sqlReader, "DG_Orders_LineItems_pkey", 0),
                    DG_Orders_ID = GetFieldValue(sqlReader, "DG_Orders_ID", 0),
                    DG_Orders_ProductType_pkey = GetFieldValue(sqlReader, "DG_Orders_ProductType_pkey", 0),
                    DG_Orders_ProductType_Name = GetFieldValue(sqlReader, "DG_Orders_ProductType_Name", string.Empty),
                    DG_PrinterQueue_Pkey = GetFieldValue(sqlReader, "DG_PrinterQueue_Pkey", 0),
                    DG_PrinterQueue_ProductID = GetFieldValue(sqlReader, "DG_PrinterQueue_ProductID", 0),
                    DG_PrinterQueue_Image_Pkey = GetFieldValue(sqlReader, "DG_PrinterQueue_Image_Pkey", string.Empty),
                    DG_Order_Details_Pkey = GetFieldValue(sqlReader, "DG_Order_Details_Pkey", 0),

                };

                OrdersList.Add(Orders);
            }
            return OrdersList;
        }


        /// <summary>
        /// DG_PrinterQueue_Pkey =@PrinterQueuekey AND DG_SentToPrinter = @SentToPrinter
        /// /// </summary>
        /// <param name="PrinterQueuekey"></param>
        /// <returns></returns>
        public PrinterQueueInfo GetPrinterQueueIsReadyForPrint(int PrinterQueuekey, bool SentToPrinter)
        {
            DBParameters.Clear();
            AddParameter("@ParamPrinterQueuekey", PrinterQueuekey);
            AddParameter("@ParamSentToPrinter", SetNullBoolValue(SentToPrinter));

            IDataReader sqlReader = ExecuteReader(DAOConstant.USP_GET_PRINTERQUEUEISREADYFORPRINT);
            List<PrinterQueueInfo> PrinterQueueList = MapPrinterQueueIsReadyForPrint(sqlReader);
            sqlReader.Close();
            return PrinterQueueList.FirstOrDefault();
        }
        private List<PrinterQueueInfo> MapPrinterQueueIsReadyForPrint(IDataReader sqlReader)
        {
            List<PrinterQueueInfo> PrinterQueueList = new List<PrinterQueueInfo>();
            while (sqlReader.Read())
            {
                PrinterQueueInfo PrinterQueue = new PrinterQueueInfo()
                {
                    DG_PrinterQueue_Pkey = GetFieldValue(sqlReader, "DG_PrinterQueue_Pkey", 0),
                    DG_PrinterQueue_ProductID = GetFieldValue(sqlReader, "DG_PrinterQueue_ProductID", 0),
                    DG_PrinterQueue_Image_Pkey = GetFieldValue(sqlReader, "DG_PrinterQueue_Image_Pkey", string.Empty),
                    DG_Associated_PrinterId = GetFieldValue(sqlReader, "DG_Associated_PrinterId", 0),
                    DG_Order_Details_Pkey = GetFieldValue(sqlReader, "DG_Order_Details_Pkey", 0),
                    DG_SentToPrinter = GetFieldValue(sqlReader, "DG_SentToPrinter", false),
                    is_Active = GetFieldValue(sqlReader, "is_Active", false),
                    QueueIndex = GetFieldValue(sqlReader, "QueueIndex", 0),
                    DG_IsSpecPrint = GetFieldValue(sqlReader, "DG_IsSpecPrint", false),
                    DG_Print_Date = GetFieldValue(sqlReader, "DG_Print_Date", DateTime.Now),
                    RotationAngle = GetFieldValue(sqlReader, "RotationAngle", string.Empty),
                };

                PrinterQueueList.Add(PrinterQueue);
            }
            return PrinterQueueList;
        }


        //public void InsertPrinterLog(PrintLogInfo objectInfo)
        //{
        //    DBParameters.Clear();
        //    AddParameter("@ParamPhotoId", objectInfo.PhotoId);
        //    AddParameter("@ParamProductTypeId", objectInfo.ProductTypeId);
        //    AddParameter("@ParamUserID", objectInfo.UserID);
        //    AddParameter("@ParamPrintTime", objectInfo.PrintTime);
        //    ExecuteNonQuery(DAOConstant.USP_INS_PRINTERLOG);
        //}



        public bool InsDataToPrinterQueue(string PhotoId, int? ProductTypeId, int? AssocatedPrinterId, bool SentToPrinter, int OrderDetailID)
        {
            DBParameters.Clear();
            AddParameter("@ParamProductTypeId", ProductTypeId);
            AddParameter("@ParamDG_AssocatedPrinterId", AssocatedPrinterId);
            AddParameter("@ParamOrderDetailID", OrderDetailID);
            AddParameter("@ParamDG_SentToPrinter", SentToPrinter);
            AddParameter("@ParamPhotoId", PhotoId);
            ExecuteNonQuery(DAOConstant.USP_INS_DATATOPRINTERQUEUE);
            return true;
        }



        public void UpdatePrinterQueueForReprint(int QueueId)
        {
            DBParameters.Clear();
            AddParameter("@ParamQueueId", QueueId);

            ExecuteNonQuery(DAOConstant.USP_UPD_PRINTERQUEUEFORREPRINT);

        }

        public void UpdatePrintCountForReprint(int QueueId)
        {
            DBParameters.Clear();
            AddParameter("@ParamQueueId", QueueId);
            ExecuteNonQuery(DAOConstant.USP_UPD_PRINTCOUNTFORREPRINT);
        }

        public void UpdatePrintQueueIndex(int Pkey, string Flag)
        {
            DBParameters.Clear();
            AddParameter("@ParamPkey", Pkey);
            AddParameter("@ParamFlag", Flag);
            ExecuteNonQuery(DAOConstant.USP_UPD_PRINTQUEUEINDEX);
        }



        public List<AssociatedPrintersInfo> SelectAssociatedPrintersforPrint(int? ProductType_ID, int SubStoreID)
        {
            DBParameters.Clear();
            AddParameter("@ParamProductType_ID", SetNullIntegerValue(ProductType_ID));
            AddParameter("@ParamSubStoreID", SubStoreID);
            IDataReader sqlReader = ExecuteReader(DAOConstant.USP_SEL_ASSOCIATEDPRINTERSFORPRINT);
            List<AssociatedPrintersInfo> AssociatedPrintersList = MapDG_AssociatedPrintersBase(sqlReader);
            sqlReader.Close();
            return AssociatedPrintersList;
        }
        private List<AssociatedPrintersInfo> MapDG_AssociatedPrintersBase(IDataReader sqlReader)
        {
            List<AssociatedPrintersInfo> AssociatedPrintersList = new List<AssociatedPrintersInfo>();
            while (sqlReader.Read())
            {
                AssociatedPrintersInfo AssociatedPrinters = new AssociatedPrintersInfo()
                {
                    DG_AssociatedPrinters_Pkey = GetFieldValue(sqlReader, "DG_AssociatedPrinters_Pkey", 0),
                    DG_AssociatedPrinters_Name = GetFieldValue(sqlReader, "DG_AssociatedPrinters_Name", string.Empty),
                    DG_AssociatedPrinters_ProductType_ID = GetFieldValue(sqlReader, "DG_AssociatedPrinters_ProductType_ID", 0),
                    DG_AssociatedPrinters_IsActive = GetFieldValue(sqlReader, "DG_AssociatedPrinters_IsActive", false),
                    DG_AssociatedPrinters_PaperSize = GetFieldValue(sqlReader, "DG_AssociatedPrinters_PaperSize", string.Empty),
                    DG_AssociatedPrinters_SubStoreID = GetFieldValue(sqlReader, "DG_AssociatedPrinters_SubStoreID", 0),

                };

                AssociatedPrintersList.Add(AssociatedPrinters);
            }
            return AssociatedPrintersList;
        }
        public List<PrinterQueueforPrint> SelectPrinterQueue(int SubStoreID)
        {
            DBParameters.Clear();
            AddParameter("@ParamSubStoreID", SubStoreID);

            IDataReader sqlReader = ExecuteReader(DAOConstant.USP_SEL_PRINTERQUEUEFORPRINT);
            List<PrinterQueueforPrint> PrinterQueueforPrintList = MapPrinterQueueforPrintBase(sqlReader);
            sqlReader.Close();
            return PrinterQueueforPrintList;
        }
        private List<PrinterQueueforPrint> MapPrinterQueueforPrintBase(IDataReader sqlReader)
        {
            List<PrinterQueueforPrint> PrinterQueueforPrintList = new List<PrinterQueueforPrint>();
            while (sqlReader.Read())
            {
                PrinterQueueforPrint PrinterQueueforPrint = new PrinterQueueforPrint()
                {
                    DG_PrinterQueue_Pkey = GetFieldValue(sqlReader, "DG_PrinterQueue_Pkey", 0),
                    DG_Order_Details_Pkey = GetFieldValue(sqlReader, "DG_Order_Details_Pkey", 0),
                    DG_SentToPrinter = GetFieldValue(sqlReader, "DG_SentToPrinter", false),
                    DG_AssociatedPrinters_Name = GetFieldValue(sqlReader, "DG_AssociatedPrinters_Name", string.Empty),
                    DG_Orders_ProductType_Name = GetFieldValue(sqlReader, "DG_Orders_ProductType_Name", string.Empty),
                    DG_Photos_RFID = GetFieldValue(sqlReader, "DG_Photos_RFID", string.Empty),
                    DG_Associated_PrinterId = GetFieldValue(sqlReader, "DG_Associated_PrinterId", 0),
                    DG_PrinterQueue_ProductID = GetFieldValue(sqlReader, "DG_PrinterQueue_ProductID", 0),
                    is_Active = GetFieldValue(sqlReader, "is_Active", false),
                    QueueIndex = GetFieldValue(sqlReader, "QueueIndex", 0),
                    DG_Orders_ProductType_pkey = GetFieldValue(sqlReader, "DG_Orders_ProductType_pkey", 0),
                    DG_Orders_Number = GetFieldValue(sqlReader, "DG_Orders_Number", string.Empty),
                    DG_Orders_pkey = GetFieldValue(sqlReader, "DG_Orders_pkey", 0),
                    DG_Order_SubStoreId = GetFieldValue(sqlReader, "DG_Order_SubStoreId", 0),
                    RotationAngle = GetFieldValue(sqlReader, "RotationAngle", string.Empty),
                    DG_PrinterQueue_Image_Pkey = GetFieldValue(sqlReader, "DG_PrinterQueue_Image_Pkey", string.Empty),
                    DG_Order_ImageUniqueIdentifier = GetFieldValue(sqlReader, "DG_Order_ImageUniqueIdentifier", string.Empty),
                    DG_IsSpecPrint = GetFieldValue(sqlReader, "DG_IsSpecPrint", false),
                    TableName = GetFieldValue(sqlReader, "TableName", string.Empty),////changed by latika for tableworkflow
                    GuestName = GetFieldValue(sqlReader, "Name", string.Empty),
                    EmailID = GetFieldValue(sqlReader, "Email", string.Empty),
                    LocationID = GetFieldValue(sqlReader, "LocationID", 0),
                    ContactNo = GetFieldValue(sqlReader, "Contacts", string.Empty)///end
                };

                PrinterQueueforPrintList.Add(PrinterQueueforPrint);
            }
            return PrinterQueueforPrintList;
        }

        public void UpdateReadyForPrint(int QueueId)
        {
            DBParameters.Clear();
            AddParameter("@ParamQueueId", QueueId);
            ExecuteNonQuery(DAOConstant.USP_UPD_READYFORPRINT);
        }
        public void UpdatePrinterQueue(int QueueId)
        {
            DBParameters.Clear();
            AddParameter("@ParamQueueId", QueueId);
            ExecuteNonQuery(DAOConstant.USP_UPD_PRINTERQUEUE);
        }

        public List<PrinterQueueDetailsInfo> GetPrinterQueueDetailsByOrderNo(string OrderNumber)
        {
            DBParameters.Clear();
            AddParameter("@OrderNumber", OrderNumber);
            IDataReader sqlReader = ExecuteReader(DAOConstant.GETPRINTERQUEUEDETAILSBYORDERNO);
            List<PrinterQueueDetailsInfo> AssociatedPrintersList = MapAssociatedPrintersBase(sqlReader);
            sqlReader.Close();
            return AssociatedPrintersList;
        }
        private List<PrinterQueueDetailsInfo> MapAssociatedPrintersBase(IDataReader sqlReader)
        {
            List<PrinterQueueDetailsInfo> AssociatedPrintersList = new List<PrinterQueueDetailsInfo>();
            while (sqlReader.Read())
            {
                PrinterQueueDetailsInfo AssociatedPrinters = new PrinterQueueDetailsInfo()
                {
                    //DG_AssociatedPrinters_Pkey = GetFieldValue(sqlReader, "DG_AssociatedPrinters_Pkey", 0),
                    DG_AssociatedPrinters_Name = GetFieldValue(sqlReader, "DG_AssociatedPrinters_Name", string.Empty),
                    DG_Orders_pkey = GetFieldValue(sqlReader, "DG_Orders_pkey", 0),
                    DG_Orders_Number = GetFieldValue(sqlReader, "DG_Orders_Number", string.Empty),
                    DG_Order_SubStoreId = GetFieldValue(sqlReader, "DG_Order_SubStoreId", 0),
                    DG_Orders_ProductType_pkey = GetFieldValue(sqlReader, "DG_Orders_ProductType_pkey", 0),
                    DG_Orders_ProductType_Name = GetFieldValue(sqlReader, "DG_Orders_ProductType_Name", string.Empty),
                    DG_PrinterQueue_Pkey = GetFieldValue(sqlReader, "DG_PrinterQueue_Pkey", 0),
                    DG_PrinterQueue_ProductID = GetFieldValue(sqlReader, "DG_PrinterQueue_ProductID", 0),
                    DG_Photos_pKey = GetFieldValue(sqlReader, "DG_Photos_pKey", string.Empty),
                    DG_Associated_PrinterId = GetFieldValue(sqlReader, "DG_Associated_PrinterId", 0),
                    DG_Order_Details_Pkey = GetFieldValue(sqlReader, "DG_Order_Details_Pkey", 0),
                    DG_SentToPrinter = GetFieldValue(sqlReader, "DG_SentToPrinter", false),
                    is_Active = GetFieldValue(sqlReader, "is_Active", false),
                    QueueIndex = GetFieldValue(sqlReader, "QueueIndex", 0),
                    DG_Photos_RFID = GetFieldValue(sqlReader, "DG_Photos_RFID", string.Empty),
                };

                AssociatedPrintersList.Add(AssociatedPrinters);
            }
            return AssociatedPrintersList;
        }

        public void InsertPrinterLog(PrintLogInfo objectInfo)
        {
            DBParameters.Clear();
            AddParameter("@ParamPhotoId", objectInfo.PhotoId);
            AddParameter("@ParamProductTypeId", objectInfo.ProductTypeId);
            AddParameter("@ParamUserID", objectInfo.UserID);
            AddParameter("@ParamPrintTime", objectInfo.PrintTime);
            ExecuteNonQuery(DAOConstant.USP_INS_PRINTERLOG);
        }




        public List<PrinterQueueforPrint> SelectFilteredPrinterQueueforPrint(int SubStoreID)
        {
            DBParameters.Clear();
            AddParameter("@ParamSubStoreID", SubStoreID);

            IDataReader sqlReader = ExecuteReader(DAOConstant.USP_SEL_FILTEREDPRINTERQUEUEFORPRINT);
            List<PrinterQueueforPrint> FilteredPrinterQueueForPrintList = MapPrinterQueueforPrint(sqlReader);
            sqlReader.Close();
            return FilteredPrinterQueueForPrintList;
        }
        private List<PrinterQueueforPrint> MapPrinterQueueforPrint(IDataReader sqlReader)
        {
            List<PrinterQueueforPrint> FilteredPrinterQueueForPrintList = new List<PrinterQueueforPrint>();
            while (sqlReader.Read())
            {
                PrinterQueueforPrint FilteredPrinterQueueForPrint = new PrinterQueueforPrint()
                {
                    DG_PrinterQueue_Pkey = GetFieldValue(sqlReader, "DG_PrinterQueue_Pkey", 0),
                    DG_Order_Details_Pkey = GetFieldValue(sqlReader, "DG_Order_Details_Pkey", 0),
                    DG_SentToPrinter = GetFieldValue(sqlReader, "DG_SentToPrinter", false),
                    DG_AssociatedPrinters_Name = GetFieldValue(sqlReader, "DG_AssociatedPrinters_Name", string.Empty),
                    DG_Orders_ProductType_Name = GetFieldValue(sqlReader, "DG_Orders_ProductType_Name", string.Empty),
                    DG_Photos_RFID = GetFieldValue(sqlReader, "DG_Photos_RFID", string.Empty),
                    DG_Associated_PrinterId = GetFieldValue(sqlReader, "DG_Associated_PrinterId", 0),
                    DG_PrinterQueue_ProductID = GetFieldValue(sqlReader, "DG_PrinterQueue_ProductID", 0),
                    is_Active = GetFieldValue(sqlReader, "is_Active", false),
                    QueueIndex = GetFieldValue(sqlReader, "QueueIndex", 0),
                    DG_Orders_ProductType_pkey = GetFieldValue(sqlReader, "DG_Orders_ProductType_pkey", 0),
                    DG_Orders_Number = GetFieldValue(sqlReader, "DG_Orders_Number", string.Empty),
                    DG_Orders_pkey = GetFieldValue(sqlReader, "DG_Orders_pkey", 0),
                    DG_Order_SubStoreId = GetFieldValue(sqlReader, "DG_Order_SubStoreId", 0),
                    DG_Photos_ID = GetFieldValue(sqlReader, "DG_Photos_ID", string.Empty),

                };

                FilteredPrinterQueueForPrintList.Add(FilteredPrinterQueueForPrint);
            }
            return FilteredPrinterQueueForPrintList;
        }

        public void InsertPrinterQueue(DataTable Dt)
        {
            DBParameters.Clear();
            AddParameter("@ParamPhotoId", Dt);

            ExecuteNonQuery(DAOConstant.USP_INS_PRINTERLOG);
        }





        public List<AssociatedPrintersInfo> SelectPrinterDetailsBySubStoreID(int substoreID)
        {
            DBParameters.Clear();
            AddParameter("@ParamSubStoreID", substoreID);
            IDataReader sqlReader = ExecuteReader(DAOConstant.USP_GET_PRINTERDETAILSBYSUBSTOREID);
            List<AssociatedPrintersInfo> PrinterDetailsList = MapPrinterDetailsBase(sqlReader);
            sqlReader.Close();
            return PrinterDetailsList;
        }
        private List<AssociatedPrintersInfo> MapPrinterDetailsBase(IDataReader sqlReader)
        {
            List<AssociatedPrintersInfo> PrinterDetailsList = new List<AssociatedPrintersInfo>();
            while (sqlReader.Read())
            {
                AssociatedPrintersInfo PrinterDetails = new AssociatedPrintersInfo()
                {
                    DG_AssociatedPrinters_Pkey = GetFieldValue(sqlReader, "DG_AssociatedPrinters_Pkey", 0),
                    DG_AssociatedPrinters_Name = GetFieldValue(sqlReader, "DG_AssociatedPrinters_Name", string.Empty),
                    DG_AssociatedPrinters_ProductType_ID = GetFieldValue(sqlReader, "DG_AssociatedPrinters_ProductType_ID", 0),
                    DG_AssociatedPrinters_IsActive = GetFieldValue(sqlReader, "DG_AssociatedPrinters_IsActive", false),
                    DG_Orders_ProductType_Name = GetFieldValue(sqlReader, "DG_Orders_ProductType_Name", string.Empty),
                    DG_AssociatedPrinters_PaperSize = GetFieldValue(sqlReader, "DG_AssociatedPrinters_PaperSize", string.Empty),
                    DG_AssociatedPrinters_SubStoreID = GetFieldValue(sqlReader, "DG_AssociatedPrinters_SubStoreID", 0),
                };
                PrinterDetailsList.Add(PrinterDetails);
            }
            return PrinterDetailsList;
        }

        public PrinterQueueInfo GetPrinterQueue(int QueueID)
        {
            DBParameters.Clear();
            AddParameter("@QueueID", QueueID);

            IDataReader sqlReader = ExecuteReader(DAOConstant.GETPRINTERQUEUEDETAILS);
            PrinterQueueInfo OrdersList = MapPrinterQueue(sqlReader);
            sqlReader.Close();
            return OrdersList;
        }
        private PrinterQueueInfo MapPrinterQueue(IDataReader sqlReader)
        {
            PrinterQueueInfo Orders = new PrinterQueueInfo();
            while (sqlReader.Read())
            {
                Orders.DG_Orders_ProductType_Name = GetFieldValue(sqlReader, "DG_Orders_ProductType_Name", string.Empty);
                Orders.DG_Orders_Number = GetFieldValue(sqlReader, "DG_Orders_Number", string.Empty);
                Orders.DG_Photos_RFID = GetFieldValue(sqlReader, "RFID", string.Empty);
                Orders.DG_Orders_LineItems_pkey = GetFieldValue(sqlReader, "DG_Orders_LineItems_pkey", 0);
                Orders.DG_Photos_ID = GetFieldValue(sqlReader, "PhotoID", string.Empty);
            }
            return Orders;
        }
        public ObservableCollection<PrinterJobInfo> GetPrinterJobInfo(DataTable Dudt_PrintJobInfot, string PrinterName, string DigiFolderThumbnailPath)
        {
            DBParameters.Clear();
            AddParameter("@ParamPrinterName", PrinterName);
            AddParameter("@ParamDigiFolderThumbnailPath", DigiFolderThumbnailPath);
            AddParameter("@ParamPrintJobInfo", Dudt_PrintJobInfot);

            IDataReader sqlReader = ExecuteReader(DAOConstant.USP_SEL_PRINTERQUEUEDETAILS);
            ObservableCollection<PrinterJobInfo> OrdersList = MapPrinterJobInfo(sqlReader);
            sqlReader.Close();
            return OrdersList;
        }
        private ObservableCollection<PrinterJobInfo> MapPrinterJobInfo(IDataReader sqlReader)
        {
            ObservableCollection<PrinterJobInfo> OrderList = new ObservableCollection<PrinterJobInfo>();
            while (sqlReader.Read())
            {
                PrinterJobInfo Order = new PrinterJobInfo()
                {
                    DG_Orders_ProductType_Name = GetFieldValue(sqlReader, "DG_Orders_ProductType_Name", string.Empty),
                    DG_Orders_Number = GetFieldValue(sqlReader, "DG_Orders_Number", string.Empty),
                    RFID = GetFieldValue(sqlReader, "RFID", string.Empty),
                    DG_Orders_LineItems_pkey = GetFieldValue(sqlReader, "DG_Orders_LineItems_pkey", 0),
                    PhotoID = GetFieldValue(sqlReader, "PhotoID", string.Empty),
                    Filepath1 = GetFieldValue(sqlReader, "DigiFolderThumbnailPath", string.Empty),
                    Printername = GetFieldValue(sqlReader, "PrinterName", string.Empty),
                    JobName = GetFieldValue(sqlReader, "JobName", string.Empty),
                    JobId = GetFieldValue(sqlReader, "JobIdentifier", 0),
                    JobStatus = GetFieldValue(sqlReader, "JobStatus", string.Empty)
                };
                OrderList.Add(Order);
            }
            return OrderList;
        }
        public void SaveAlbumPrintPosition(DataTable udt_Printer)
        {
            DBParameters.Clear();
            AddParameter("@ParamPrinterInfo", udt_Printer);
            ExecuteReader(DAOConstant.USP_INS_ALBUMPRINTERPERMISSION);
            //int objectId = (Int32)GetOutParameterValue("@Retvalue");
            //return objectId;
        }
        public List<PrinterQueueInfo> GetPrintLogDetails()
        {
            DBParameters.Clear();
            IDataReader sqlReader = ExecuteReader(DAOConstant.USP_SEL_PHOTOGRAPHER);
            List<PrinterQueueInfo> photoGrapherList = MapPrintLogDetails(sqlReader);
            sqlReader.Close();
            return photoGrapherList;
        }
        private List<PrinterQueueInfo> MapPrintLogDetails(IDataReader sqlReader)
        {
            List<PrinterQueueInfo> userDetailsList = new List<PrinterQueueInfo>();
            while (sqlReader.Read())
            {
                PrinterQueueInfo UserDetails = new PrinterQueueInfo()
                {
                    DG_Orders_ProductType_Name = GetFieldValue(sqlReader, "DG_Orders_ProductType_Name", string.Empty),
                    DG_Photos_FileName = GetFieldValue(sqlReader, "DG_Photos_FileName", string.Empty),
                    DG_Photos_CreatedOn = GetFieldValue(sqlReader, "DG_Photos_CreatedOn", DateTime.MinValue),
                    DG_Print_Date = GetFieldValue(sqlReader, "DG_Print_Date", DateTime.MinValue),
                    PrintedBy = GetFieldValue(sqlReader, "PrintedBy", string.Empty),
                    TakenBy = GetFieldValue(sqlReader, "TakenBy", string.Empty),
                    ID = GetFieldValue(sqlReader, "ID", 0)
                };

                userDetailsList.Add(UserDetails);
            }
            return userDetailsList;
        }

        /// <summary>
        /// Fetching QR Code
        /// </summary>
        /// <param name="productId"></param>
        /// <returns></returns>
        public string SelectQRCode(int productId)
        {
            string val = string.Empty;
            DBParameters.Clear();
            AddParameter("@ParamDG_Photos_pkey", productId);
            IDataReader sqlReader = ExecuteReader("usp_SEL_QRCode");
            if (sqlReader.Read())
            {
                val = (string)sqlReader["OnlineQRCode"];
            }
            sqlReader.Close();
            return val;
        }

        public string CheckSpecSetting(int printerId)
        {
            string SpecValue = string.Empty;
            DBParameters.Clear();
            AddParameter("@ParamDG_Order_Details_Pkey", printerId);
            IDataReader sqlReader = ExecuteReader("CheckSpecSetting");
            if(sqlReader.Read())
            {
                SpecValue = Convert.ToString(sqlReader["DG_Orders_ID"]);
                if(SpecValue == "" || SpecValue == null)
                {
                    SpecValue = "SpecOrderNull";
                }
            }
            sqlReader.Close();
            return SpecValue;
        }
    }
}
