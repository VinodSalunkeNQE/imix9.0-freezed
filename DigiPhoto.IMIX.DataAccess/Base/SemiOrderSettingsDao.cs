﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DigiPhoto.IMIX.Model;
using System.Data;

namespace DigiPhoto.IMIX.DataAccess
{
    public class SemiOrderSettingsDao : BaseDataAccess
    {
        #region Constructor
        public SemiOrderSettingsDao(BaseDataAccess baseaccess)
            : base(baseaccess)
        { }

        public SemiOrderSettingsDao()
        { }
        #endregion
        public List<SemiOrderSettings> GetSemiOrderSettings()
        {
            DBParameters.Clear();
            this.OpenConnection();
            AddParameter("@ParamSubStoreId", SetNullIntegerValue(null));
            AddParameter("@ParamLocationId", SetNullIntegerValue(null));
            IDataReader sqlReader = ExecuteReader(DAOConstant.USP_GET_SEMIORDERSETTINGS);
            List<SemiOrderSettings> configValueList = MapSemiOrderSettingsInfo(sqlReader);
            sqlReader.Close();
            return configValueList;
        }
        public List<SemiOrderSettings> GetSemiOrderSettings(int? subStoreId, int? locationId)
        {
            DBParameters.Clear();
            AddParameter("@ParamSubStoreId", SetNullIntegerValue(subStoreId));
            AddParameter("@ParamLocationId", SetNullIntegerValue(locationId));
            IDataReader sqlReader = ExecuteReader(DAOConstant.USP_GET_SEMIORDERSETTINGS);
            List<SemiOrderSettings> configValueList = MapSemiOrderSettingsInfo(sqlReader);
            sqlReader.Close();
            return configValueList;
        }
        private List<SemiOrderSettings> MapSemiOrderSettingsInfo(IDataReader sqlReader)
        {
            List<SemiOrderSettings> configValueList = new List<SemiOrderSettings>();
            while (sqlReader.Read())
            {
                //SemiOrderSettings configValue = new SemiOrderSettings
                //{
                //    DG_SemiOrder_Settings_Pkey = GetFieldValue(sqlReader, "DG_SemiOrder_Settings_Pkey", 0),
                //    DG_SemiOrder_Settings_AutoBright = GetFieldValue(sqlReader, "DG_SemiOrder_Settings_AutoBright", false),
                //    DG_SemiOrder_Settings_AutoBright_Value = GetFieldValue(sqlReader, "DG_SemiOrder_Settings_AutoBright_Value", 0.0F),
                //    DG_SemiOrder_Settings_AutoContrast = GetFieldValue(sqlReader, "DG_SemiOrder_Settings_AutoContrast", false),
                //    DG_SemiOrder_Settings_AutoContrast_Value = GetFieldValue(sqlReader, "DG_SemiOrder_Settings_AutoContrast_Value", 0.0F),
                //    ImageFrame_Horizontal = GetFieldValue(sqlReader, "ImageFrame_Horizontal", string.Empty),
                //    DG_SemiOrder_Settings_IsImageFrame = GetFieldValue(sqlReader, "DG_SemiOrder_Settings_IsImageFrame", false),
                //    DG_SemiOrder_ProductTypeId = GetFieldValue(sqlReader, "DG_SemiOrder_ProductTypeId", string.Empty),
                //    ImageFrame_Vertical = GetFieldValue(sqlReader, "ImageFrame_Vertical", string.Empty),
                //    DG_SemiOrder_Environment = GetFieldValue(sqlReader, "DG_SemiOrder_Environment", false),
                //    DG_SemiOrder_BG = GetFieldValue(sqlReader, "DG_SemiOrder_BG", string.Empty),
                //    Background_Horizontal = GetFieldValue(sqlReader, "Background_Horizontal", string.Empty),
                //    Background_Vertical = GetFieldValue(sqlReader, "Background_Vertical", string.Empty),
                //    DG_SemiOrder_Settings_IsImageBG = GetFieldValue(sqlReader, "DG_SemiOrder_Settings_IsImageBG", false),
                //    Graphics_layer_Horizontal = GetFieldValue(sqlReader, "Graphics_layer_Horizontal", string.Empty),
                //    Graphics_layer_Vertical = GetFieldValue(sqlReader, "Graphics_layer_Vertical", string.Empty),
                //    ZoomInfo_Horizontal = GetFieldValue(sqlReader, "ZoomInfo_Horizontal", string.Empty),
                //    ZoomInfo_Vertical = GetFieldValue(sqlReader, "ZoomInfo_Vertical", string.Empty),
                //    DG_SubStoreId = GetFieldValue(sqlReader, "DG_SubStoreId", 0),
                //    DG_SemiOrder_IsPrintActive = GetFieldValue(sqlReader, "DG_SemiOrder_IsPrintActive", false),
                //    DG_SemiOrder_IsCropActive = GetFieldValue(sqlReader, "DG_SemiOrder_IsCropActive", false),
                //    VerticalCropValues = GetFieldValue(sqlReader, "VerticalCropValues", string.Empty),
                //    HorizontalCropValues = GetFieldValue(sqlReader, "HorizontalCropValues", string.Empty),
                //    DG_LocationId = GetFieldValue(sqlReader, "DG_LocationId", 0),
                //    ChromaColor = GetFieldValue(sqlReader,"ChromaColor",string.Empty),
                //    ColorCode = GetFieldValue(sqlReader, "ColorCode", string.Empty),
                //    ClrTolerance = GetFieldValue(sqlReader, "ClrTolerance", string.Empty),
                //    ProductName = GetFieldValue(sqlReader, "ProductName", string.Empty),
                //    TextLogo_Horizontal = GetFieldValue(sqlReader, "TextLogo_Horizontal", string.Empty),
                //    TextLogo_Vertical = GetFieldValue(sqlReader, "TextLogo_Vertical", string.Empty),                   

                //};

                SemiOrderSettings configValue = new SemiOrderSettings();
                configValue.DG_SemiOrder_Settings_Pkey = GetFieldValue(sqlReader, "DG_SemiOrder_Settings_Pkey", 0);
                configValue.DG_SemiOrder_Settings_AutoBright = GetFieldValue(sqlReader, "DG_SemiOrder_Settings_AutoBright", false);
                configValue.DG_SemiOrder_Settings_AutoBright_Value = GetFieldValue(sqlReader, "DG_SemiOrder_Settings_AutoBright_Value", 0.0F);
                configValue.DG_SemiOrder_Settings_AutoContrast = GetFieldValue(sqlReader, "DG_SemiOrder_Settings_AutoContrast", false);
                configValue.DG_SemiOrder_Settings_AutoContrast_Value = GetFieldValue(sqlReader, "DG_SemiOrder_Settings_AutoContrast_Value", 0.0F);                
                configValue.DG_SemiOrder_Settings_IsImageFrame = GetFieldValue(sqlReader, "DG_SemiOrder_Settings_IsImageFrame", false);
                configValue.DG_SemiOrder_ProductTypeId = GetFieldValue(sqlReader, "DG_SemiOrder_ProductTypeId", string.Empty);                
                configValue.DG_SemiOrder_Environment = GetFieldValue(sqlReader, "DG_SemiOrder_Environment", false);                
                configValue.Background_Vertical = GetFieldValue(sqlReader, "Background_Vertical", string.Empty);
                configValue.DG_SemiOrder_Settings_IsImageBG = GetFieldValue(sqlReader, "DG_SemiOrder_Settings_IsImageBG", false);
                configValue.Graphics_layer_Horizontal = GetFieldValue(sqlReader, "Graphics_layer_Horizontal", string.Empty);
                configValue.Graphics_layer_Vertical = GetFieldValue(sqlReader, "Graphics_layer_Vertical", string.Empty);
                configValue.ZoomInfo_Horizontal = GetFieldValue(sqlReader, "ZoomInfo_Horizontal", string.Empty);
                configValue.ZoomInfo_Vertical = GetFieldValue(sqlReader, "ZoomInfo_Vertical", string.Empty);
                configValue.DG_SubStoreId = GetFieldValue(sqlReader, "DG_SubStoreId", 0);
                configValue.DG_SemiOrder_IsPrintActive = GetFieldValue(sqlReader, "DG_SemiOrder_IsPrintActive", false);
                configValue.DG_SemiOrder_IsCropActive = GetFieldValue(sqlReader, "DG_SemiOrder_IsCropActive", false);
                configValue.VerticalCropValues = GetFieldValue(sqlReader, "VerticalCropValues", string.Empty);
                configValue.HorizontalCropValues = GetFieldValue(sqlReader, "HorizontalCropValues", string.Empty);
                configValue.DG_LocationId = GetFieldValue(sqlReader, "DG_LocationId", 0);
                configValue.ChromaColor = GetFieldValue(sqlReader, "ChromaColor", string.Empty);
                configValue.ColorCode = GetFieldValue(sqlReader, "ColorCode", string.Empty);
                configValue.ClrTolerance = GetFieldValue(sqlReader, "ClrTolerance", string.Empty);
                configValue.ProductName = GetFieldValue(sqlReader, "ProductName", string.Empty);
                configValue.TextLogo_Horizontal = GetFieldValue(sqlReader, "TextLogo_Horizontal", string.Empty);
                configValue.TextLogo_Vertical = GetFieldValue(sqlReader, "TextLogo_Vertical", string.Empty);
                
                if(!string.IsNullOrEmpty(GetFieldValue(sqlReader, "DG_SemiOrder_BG", string.Empty)))
                {
                    configValue.Background_Horizontal = GetFieldValue(sqlReader, "DG_SemiOrder_BG", string.Empty);
                }
                else
                {
                    configValue.Background_Horizontal = GetFieldValue(sqlReader, "Background_Horizontal", string.Empty);
                }
                
                if (!string.IsNullOrEmpty(GetFieldValue(sqlReader, "DG_SemiOrder_Settings_ImageFrame", string.Empty)))
                {
                    configValue.ImageFrame_Horizontal = GetFieldValue(sqlReader, "DG_SemiOrder_Settings_ImageFrame", string.Empty);
                }
                else
                {
                    configValue.ImageFrame_Horizontal = GetFieldValue(sqlReader, "ImageFrame_Horizontal", string.Empty);
                }

                if (!string.IsNullOrEmpty(GetFieldValue(sqlReader, "DG_SemiOrder_Settings_ImageFrame_Vertical", string.Empty)))
                {
                    configValue.ImageFrame_Vertical = GetFieldValue(sqlReader, "DG_SemiOrder_Settings_ImageFrame_Vertical", string.Empty);
                }
                else
                {
                    configValue.ImageFrame_Vertical = GetFieldValue(sqlReader, "ImageFrame_Vertical", string.Empty);
                }

                configValueList.Add(configValue);
            }
            return configValueList;
        }

        public bool DeleteSpecSetting(int objectvalueId)
        {
            DBParameters.Clear();
            AddParameter("@ParamSemiOrderId", objectvalueId);
            ExecuteNonQuery(DAOConstant.USP_DEL_SpecSettings);
            return true;
        }


    }
}
