﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DigiPhoto.IMIX.Model;
using System.Data;



namespace DigiPhoto.IMIX.DataAccess
{
    public class SubStoreLocationDao:BaseDataAccess
    {
         #region Constructor
        public SubStoreLocationDao(BaseDataAccess baseaccess)
            : base(baseaccess)
        { }
        public SubStoreLocationDao()
        { }
        #endregion

        public int InsertSubStoreLocation(SubStoreLocationInfo objectInfo)
        {
            DBParameters.Clear();
            AddParameter("@ParamDG_SubStore_ID", objectInfo.DG_SubStore_ID);
            AddParameter("@ParamDG_Location_ID", objectInfo.DG_Location_ID);
            AddParameter("@ParamDG_SubStoreLocationID", objectInfo.DG_SubStore_Location_Pkey,ParameterDirection.Output);

            ExecuteNonQuery(DAOConstant.USP_INS_SUBSTORELOCATION);
            int objectId = (int)GetOutParameterValue("@ParamDG_SubStoreLocationID");
            return objectId;


        }
        public List<LocationInfo> SelectSubStoreLocations(int SubStoreId)
        {
            DBParameters.Clear();
            AddParameter("@ParamSubStoreId", SubStoreId);

            IDataReader sqlReader = ExecuteReader(DAOConstant.USP_SEL_SUBSTORELOCATIONS);
            List<LocationInfo> vw_GetSubStoreLocationsList = MapGetSubStoreLocationsBase(sqlReader);
            sqlReader.Close();
            return vw_GetSubStoreLocationsList;
        }
        private List<LocationInfo> MapGetSubStoreLocationsBase(IDataReader sqlReader)
        {
            List<LocationInfo> GetSubStoreLocationsList = new List<LocationInfo>();
            while (sqlReader.Read())
            {
                LocationInfo GetSubStoreLocations = new LocationInfo()
                {
                    DG_SubStore_Location_Pkey = GetFieldValue(sqlReader, "DG_SubStore_Location_Pkey", 0),
                    DG_Location_Name = GetFieldValue(sqlReader, "DG_Location_Name", string.Empty),
                    DG_Location_pkey = GetFieldValue(sqlReader, "DG_Location_pkey", 0),                    
                    DG_SubStore_ID = GetFieldValue(sqlReader, "DG_SubStore_ID", 0),

                };

                GetSubStoreLocationsList.Add(GetSubStoreLocations);
            }
            return GetSubStoreLocationsList;
        }
        public SubStoreLocationInfo SelectSubStoreLocationsByLocationId(int LocationID)
        {
            DBParameters.Clear();
            AddParameter("@ParamLocationID", LocationID);

            IDataReader sqlReader = ExecuteReader(DAOConstant.USP_GET_SUBSTORE_LOCATIONBYLOCATIONID);
            List<SubStoreLocationInfo> GetSubStoreLocationsList = MapGetSubStoreLocationsByLocationIdBase(sqlReader);
            sqlReader.Close();
            return GetSubStoreLocationsList.FirstOrDefault();
        }
        private List<SubStoreLocationInfo> MapGetSubStoreLocationsByLocationIdBase(IDataReader sqlReader)
        {
            List<SubStoreLocationInfo> GetSubStoreLocationsList = new List<SubStoreLocationInfo>();
            while (sqlReader.Read())
            {
                SubStoreLocationInfo GetSubStoreLocations = new SubStoreLocationInfo()
                {
                    DG_SubStore_Location_Pkey = GetFieldValue(sqlReader, "DG_SubStore_Location_Pkey", 0),
                    DG_Location_ID = GetFieldValue(sqlReader, "DG_Location_ID", 0),
                    DG_SubStore_ID = GetFieldValue(sqlReader, "DG_SubStore_ID", 0),

                };

                GetSubStoreLocationsList.Add(GetSubStoreLocations);
            }
            return GetSubStoreLocationsList;
        }

        public bool DelSubStoreLocationsBySubStoreId(int SubstoreId)
        {
            DBParameters.Clear();
            AddParameter("@ParamSubStorekey", SubstoreId);
            ExecuteNonQuery(DAOConstant.USP_DEL_SUBSTORELOCATIONSBYSUBSTOREID);
            return true;
        }


        public List<LocationInfo> SelListAvailableLocations(int SubstoreId)
        {
            DBParameters.Clear();
            AddParameter("@ParamSubStorekey", SubstoreId);
            IDataReader sqlReader = ExecuteReader(DAOConstant.USP_SEL_LISTAVAILABLELOCATIONS);
            List<LocationInfo> LocationList = MapLocationBase(sqlReader);
            sqlReader.Close();
            return LocationList;
        }
        private List<LocationInfo> MapLocationBase(IDataReader sqlReader)
        {
            List<LocationInfo> LocationList = new List<LocationInfo>();
            while (sqlReader.Read())
            {
                LocationInfo Location = new LocationInfo()
                {
                    DG_Location_pkey = GetFieldValue(sqlReader, "DG_Location_pkey", 0),
                    DG_Location_Name = GetFieldValue(sqlReader, "DG_Location_Name", string.Empty),                    
                   // DG_SubStore_ID = GetFieldValue(sqlReader, "DG_SubStore_ID", 0),
                    DG_Location_ID = GetFieldValue(sqlReader, "DG_Location_ID", 0),
                };
                LocationList.Add(Location);
            }
            return LocationList;
        }

       
    }
}
