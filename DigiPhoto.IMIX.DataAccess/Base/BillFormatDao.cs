﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DigiPhoto.IMIX.Model;
using System.Data;

namespace DigiPhoto.IMIX.DataAccess
{
    public class BillFormatDao : BaseDataAccess
    {
        #region Constructor
        public BillFormatDao(BaseDataAccess baseaccess)
            : base(baseaccess)
        { }
        public BillFormatDao()
        { }
        #endregion

        public List<BillFormatInfo> Select()
        {
            DBParameters.Clear();

            List<BillFormatInfo> objectList;
            using (IDataReader sqlReader = ExecuteReader(DAOConstant.USP_SEL_BILLFORMAT))
            {
                objectList = MapObject(sqlReader);
            }
            return objectList;
        }
        private List<BillFormatInfo> MapObject(IDataReader sqlReader)
        {
            List<BillFormatInfo> objectList = new List<BillFormatInfo>();
            while (sqlReader.Read())
            {
                BillFormatInfo objectValue = new BillFormatInfo
                {
                    DG_Bill_Format_pkey = GetFieldValue(sqlReader, "DG_Bill_Format_pkey", 0),
                    DG_Bill_Type = GetFieldValue(sqlReader, "DG_Bill_Type", 0),
                    DG_Refund_Slogan = GetFieldValue(sqlReader, "DG_Refund_Slogan", string.Empty),
                };

                objectList.Add(objectValue);
            }
            return objectList;
        }



        public string GetBillFormat()
        {
           
            List<BillFormatInfo> objectList;
            using (IDataReader sqlReader = ExecuteReader(DAOConstant.USP_GET_BILLFORMAT))
            {
                objectList = MapObject(sqlReader);
            }
            return objectList.FirstOrDefault().DG_Refund_Slogan;
        }
        private List<BillFormatInfo> MapBillFormatInfo(IDataReader sqlReader)
        {
            List<BillFormatInfo> Bill_FormatList = new List<BillFormatInfo>();
            while (sqlReader.Read())
            {
                BillFormatInfo Bill_Format = new BillFormatInfo()
                {
                    DG_Bill_Format_pkey = GetFieldValue(sqlReader, "DG_Bill_Format_pkey", 0),
                    DG_Bill_Type = GetFieldValue(sqlReader, "DG_Bill_Type", 0),
                    DG_Refund_Slogan = GetFieldValue(sqlReader, "DG_Refund_Slogan", string.Empty),

                };

                Bill_FormatList.Add(Bill_Format);
            }
            return Bill_FormatList;
        }

    }
}
