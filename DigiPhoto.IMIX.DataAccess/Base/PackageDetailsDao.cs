﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DigiPhoto.IMIX.Model;
using System.Data;
namespace DigiPhoto.IMIX.DataAccess
{
    public class PackageDetailsDao : BaseDataAccess
    {
        #region Constructor
        public PackageDetailsDao(BaseDataAccess baseaccess)
            : base(baseaccess)
        { }
        public PackageDetailsDao()
        { }
        #endregion
        public string GetChildProductTypeQuantity(int PackageId, int ProductTypeId)
        {
            DBParameters.Clear();
            AddParameter("@ParamPackageId", PackageId);
            AddParameter("@ParamProductTypeId", ProductTypeId);

            IDataReader sqlReader = ExecuteReader(DAOConstant.USP_GET_CHILDPRODUCTTYPEQUANTITY);
            List<PackageDetailsInfo> PackageDetailsList = MapPackageDetails(sqlReader);
            sqlReader.Close();
            return PackageDetailsList.FirstOrDefault().DG_Product_Quantity.ToString();
        }
        private List<PackageDetailsInfo> MapPackageDetails(IDataReader sqlReader)
        {
            List<PackageDetailsInfo> PackageDetailsList = new List<PackageDetailsInfo>();
            while (sqlReader.Read())
            {
                PackageDetailsInfo PackageDetails = new PackageDetailsInfo()
                {
                    DG_ProductTypeId = GetFieldValue(sqlReader, "DG_ProductTypeId", 0),
                    DG_PackageId = GetFieldValue(sqlReader, "DG_PackageId", 0),
                    DG_Product_Quantity = GetFieldValue(sqlReader, "DG_Product_Quantity", 0),

                };

                PackageDetailsList.Add(PackageDetails);
            }
            return PackageDetailsList;
        }



        public int GetMaxQuantityofIteminaPackage(int PackageId, int ProductTypeId)
        {
            DBParameters.Clear();
            AddParameter("@ParamPackageId", PackageId);
            AddParameter("@ParamProductTypeId", ProductTypeId);
            IDataReader sqlReader = ExecuteReader(DAOConstant.USP_GET_MAXQUANTITYOFITEMINAPACKAGE);
            List<PackageDetailsInfo> PackageDetailsList = MapMaxQuantityofIteminaPackageBase(sqlReader);
            sqlReader.Close();
            return PackageDetailsList.FirstOrDefault().DG_Product_MaxImage;
        }
        private List<PackageDetailsInfo> MapMaxQuantityofIteminaPackageBase(IDataReader sqlReader)
        {
            List<PackageDetailsInfo> PackageDetailsList = new List<PackageDetailsInfo>();
            while (sqlReader.Read())
            {
                PackageDetailsInfo PackageDetails = new PackageDetailsInfo()
                {
                    DG_Package_Details_Pkey = GetFieldValue(sqlReader, "DG_Package_Details_Pkey", 0),
                    DG_ProductTypeId = GetFieldValue(sqlReader, "DG_ProductTypeId", 0),
                    DG_PackageId = GetFieldValue(sqlReader, "DG_PackageId", 0),
                    DG_Product_Quantity = GetFieldValue(sqlReader, "DG_Product_Quantity", 0),
                    DG_Product_MaxImage = GetFieldValue(sqlReader, "DG_Product_MaxImage", 0),
                    SyncCode = GetFieldValue(sqlReader, "SyncCode", string.Empty),
                    IsSynced = GetFieldValue(sqlReader, "IsSynced", false),

                };

                PackageDetailsList.Add(PackageDetails);
            }
            return PackageDetailsList;
        }

        public string GetChildProductTypeById(int Parentid)
        {
            DBParameters.Clear();
            AddParameter("@ParamParentid", Parentid);

            IDataReader sqlReader = ExecuteReader(DAOConstant.USP_GET_CHILDPRODUCTTYPEBYID);
            List<PackageDetailsInfo> PackageDetailsList = MapDG_PackageDetailsBase(sqlReader);
            sqlReader.Close();
            return PackageDetailsList.FirstOrDefault().DG_ProductTypeIdComa;
        }
        private List<PackageDetailsInfo> MapDG_PackageDetailsBase(IDataReader sqlReader)
        {
            List<PackageDetailsInfo> PackageDetailsList = new List<PackageDetailsInfo>();
            while (sqlReader.Read())
            {
                PackageDetailsInfo PackageDetails = new PackageDetailsInfo()
                {
                    DG_ProductTypeIdComa = GetFieldValue(sqlReader, "DG_ProductTypeId", string.Empty),

                };

                PackageDetailsList.Add(PackageDetails);
            }
            return PackageDetailsList;
        }




        public void InsertPackageDetails(int packageId, int ProductTypeId, int? PackageQuantity, int? PackageMaxQuanity,int? VideoLength)
        {
            DBParameters.Clear();
            AddParameter("@ParampackageId", packageId);
            AddParameter("@ParamProductTypeId", ProductTypeId);
            AddParameter("@ParamPackageQuantity", PackageQuantity);
            AddParameter("@ParamPackageMaxQuanity", PackageMaxQuanity);
            AddParameter("@ParamPackageVideoLength", VideoLength);
            ExecuteNonQuery(DAOConstant.USP_INS_PACKAGEDETAILS);
        }


        public void UpdAndDelPackageMasterDetails(int PackageId, string PackageName, string Packageprice)
        {
            DBParameters.Clear();
            AddParameter("@ParamPackageId", PackageId);
            AddParameter("@ParamPackageName", PackageName);
            AddParameter("@ParamPackageprice", Packageprice);

            ExecuteNonQuery(DAOConstant.USP_UPDANDDEL_PACKAGEMASTERDETAILS);
        }



        public List<PackageDetailsViewInfo> GetPackagDetails(int PackageId)
        {
            DBParameters.Clear();
            AddParameter("@ParampackageId", PackageId);

            IDataReader sqlReader = ExecuteReader(DAOConstant.USP_SEL_PACKAGEDETAILS);
            List<PackageDetailsViewInfo> PackageDetailsList = MapPackageDetailsBase(sqlReader);
            sqlReader.Close();
            return PackageDetailsList;
        }
        private List<PackageDetailsViewInfo> MapPackageDetailsBase(IDataReader sqlReader)
        {
            List<PackageDetailsViewInfo> PackageDetailsList = new List<PackageDetailsViewInfo>();
            while (sqlReader.Read())
            {
                PackageDetailsViewInfo PackageDetails = new PackageDetailsViewInfo()
                {
                    DG_Package_Details_Pkey = GetFieldValue(sqlReader, "DG_Package_Details_Pkey", 0),
                    DG_ProductTypeId = GetFieldValue(sqlReader, "DG_ProductTypeId", 0),
                    DG_PackageId = GetFieldValue(sqlReader, "DG_PackageId", 0),
                    DG_Product_Quantity = GetFieldValue(sqlReader, "DG_Product_Quantity", 0),
                    DG_Orders_ProductType_pkey = GetFieldValue(sqlReader, "DG_Orders_ProductType_pkey", 0),
                    DG_Orders_ProductType_Name = GetFieldValue(sqlReader, "DG_Orders_ProductType_Name", string.Empty),
                    DG_Product_MaxImage = GetFieldValue(sqlReader, "DG_Product_MaxImage", 0),
                    DG_Orders_ProductType_IsBundled = GetFieldValue(sqlReader, "DG_Orders_ProductType_IsBundled", false),
                    DG_IsAccessory = GetFieldValue(sqlReader, "DG_IsAccessory", false),
                    DG_IsActive = GetFieldValue(sqlReader, "DG_IsActive", false),
                    DG_Video_Length = GetFieldValue(sqlReader, "DG_Video_Length", 0),

                };

                PackageDetailsList.Add(PackageDetails);
            }
            return PackageDetailsList;
        }
        //////change made by latika for AR personalised
        public bool GETIsPersonalizedAR(int objectvalueId)
        {
            bool IsPersonalizedAR = false;
            DBParameters.Clear();
            AddParameter("@ProductTypeID", objectvalueId);
            IsPersonalizedAR = Convert.ToBoolean(ExecuteScalar("DG_GetIsPersonalizedAR"));
            return IsPersonalizedAR;
        }
        ////end
    }
}
