﻿using DigiPhoto.IMIX.Model.Base;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DigiPhoto.IMIX.DataAccess.Base
{
    public class ThemeDao:BaseDataAccess
    {
      /// <summary>
      /// For gettting All Theme Added By Ajinkya Y.
      /// </summary>
      /// <param name="baseaccess"></param>
        public ThemeDao(BaseDataAccess baseaccess)
            : base(baseaccess)
        { }
        public ThemeDao()
        { }
        public List<ThemeInfo> Select(int? themeId)   // To get list of Theme for Themetype Dropdown.. Added by Ajinkya
        {
            DBParameters.Clear();
            AddParameter("@ParamThemeId", SetNullIntegerValue(themeId));

            List<ThemeInfo> objectList;
            using (IDataReader sqlReader = ExecuteReader(DAOConstant.USP_SEL_Theme))
            {
                objectList = MapObject(sqlReader);
            }
            return objectList;
        }

        private List<ThemeInfo> MapObject(IDataReader sqlReader)
        {
            List<ThemeInfo> objectList = new List<ThemeInfo>();
            while (sqlReader.Read())
            {
                ThemeInfo objectValue = new ThemeInfo
                {
                    DG_ThemeId = GetFieldValue(sqlReader, "DG_ThemeId", 0),
                    DG_ThemeName = GetFieldValue(sqlReader, "DG_ThemeName", string.Empty),
                    DG_IsActive = GetFieldValue(sqlReader, "DG_IsActive", false),
                };

                objectList.Add(objectValue);
            }
            return objectList;
        }

    }
}
