﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DigiPhoto.IMIX.Model;
using System.Data;

namespace DigiPhoto.IMIX.DataAccess
{
    public class LocationDao : BaseDataAccess
    {
        #region Constructor
        public LocationDao(BaseDataAccess baseaccess)
            : base(baseaccess)
        { }
        public LocationDao()
        { }
        #endregion
        public int Add(LocationInfo objectInfo)
        {
            DBParameters.Clear();
            AddParameter("@ParamDG_Location_Name", objectInfo.DG_Location_Name);
            AddParameter("@ParamDG_Store_ID", objectInfo.DG_Store_ID);
            AddParameter("@ParamDG_Location_IsActive", objectInfo.DG_Location_IsActive);
            AddParameter("@ParamSyncCode", objectInfo.SyncCode);
            AddParameter("@ParamIsSynced", objectInfo.IsSynced);
            AddParameter("@LocationID", objectInfo.DG_Location_pkey, ParameterDirection.Output);

            ExecuteNonQuery(DAOConstant.USP_INS_LOCATION);
            int objectId = (int)GetOutParameterValue("@LocationID");
            return objectId;
        }
        public int Update(LocationInfo objectInfo)
        {
            DBParameters.Clear();
            AddParameter("@ParamLocationID", objectInfo.DG_Location_pkey);
            AddParameter("@ParamDG_Location_Name", objectInfo.DG_Location_Name);
            AddParameter("@ParamIsSynced", objectInfo.IsSynced);
            AddParameter("@LocationIDOut", objectInfo.DG_Location_pkey, ParameterDirection.Output);
            ExecuteNonQuery(DAOConstant.USP_UPD_LOCATION);
            int objectId = (int)GetOutParameterValue("@LocationIDOut");
            return objectId;
            //return true;
        }
        public bool Delete(int objectvalueId)
        {
            DBParameters.Clear();
            AddParameter("@ParamLocationID", objectvalueId);
            ExecuteNonQuery(DAOConstant.USP_DEL_LOCATION);
            return true;
        }
        public SubStoreLocationInfo SelectSubStoreLocationsByLocationId(int LocationID)
        {
            DBParameters.Clear();
            AddParameter("@ParamLocationID", LocationID);

            IDataReader sqlReader = ExecuteReader(DAOConstant.USP_GET_SUBSTORE_LOCATIONBYLOCATIONID);
            List<SubStoreLocationInfo> GetSubStoreLocationsList = MapGetSubStoreLocationsByLocationIdBase(sqlReader);
            sqlReader.Close();
            return GetSubStoreLocationsList.FirstOrDefault();
        }
        private List<SubStoreLocationInfo> MapGetSubStoreLocationsByLocationIdBase(IDataReader sqlReader)
        {
            List<SubStoreLocationInfo> GetSubStoreLocationsList = new List<SubStoreLocationInfo>();
            while (sqlReader.Read())
            {
                SubStoreLocationInfo GetSubStoreLocations = new SubStoreLocationInfo()
                {
                    DG_SubStore_Location_Pkey = GetFieldValue(sqlReader, "DG_SubStore_Location_Pkey", 0),
                    DG_Location_ID = GetFieldValue(sqlReader, "DG_Location_ID", 0),
                    DG_SubStore_ID = GetFieldValue(sqlReader, "DG_SubStore_ID", 0),

                };

                GetSubStoreLocationsList.Add(GetSubStoreLocations);
            }
            return GetSubStoreLocationsList;
        }

        public Dictionary<string, string> GetLocationStoreWiseDir(int storeId)
        {
            DBParameters.Clear();
            AddParameter("@StoreId", storeId);
            IDataReader sqlReader = ExecuteReader(DAOConstant.GETLOCATIONSTOREWISE);
            Dictionary<string, string> LocationList = MapLocationLocationStoreWiseInfoDir(sqlReader);
            sqlReader.Close();
            return LocationList;
        }
        private Dictionary<string, string> MapLocationLocationStoreWiseInfoDir(IDataReader sqlReader)
        {
            Dictionary<string, string> LocationList = new Dictionary<string, string>();
            LocationList.Add("--Select--", "0");
            while (sqlReader.Read())
            {
                LocationList.Add(GetFieldValue(sqlReader, "DG_Location_Name", string.Empty), GetFieldValue(sqlReader, "DG_Location_pkey", 0).ToString());
            }
            return LocationList;
        }


        public List<LocationInfo> GetLocationStoreWise(int storeId)
        {
            DBParameters.Clear();
            AddParameter("@StoreId", storeId);

            IDataReader sqlReader = ExecuteReader(DAOConstant.GETLOCATIONSTOREWISE);
            List<LocationInfo> LocationList = MapLocationLocationStoreWiseInfo(sqlReader);
            sqlReader.Close();
            return LocationList;
        }
        private List<LocationInfo> MapLocationLocationStoreWiseInfo(IDataReader sqlReader)
        {
            List<LocationInfo> LocationList = new List<LocationInfo>();
            while (sqlReader.Read())
            {
                LocationInfo Location = new LocationInfo()
                {
                    DG_Location_pkey = GetFieldValue(sqlReader, "DG_Location_pkey", 0),
                    DG_Location_Name = GetFieldValue(sqlReader, "DG_Location_Name", string.Empty),
                };

                LocationList.Add(Location);
            }
            return LocationList;
        }

        public List<LocationInfo> GetLocationList(int Store_ID, bool Location_IsActive)
        {
            DBParameters.Clear();
            AddParameter("@ParamStore_ID", Store_ID);
            AddParameter("@ParamLocation_IsActive", Location_IsActive);

            IDataReader sqlReader = ExecuteReader(DAOConstant.USP_GET_LOCATIONLIST);
            List<LocationInfo> LocationList = MapLocationBase(sqlReader);
            sqlReader.Close();
            return LocationList;
        }
        private List<LocationInfo> MapLocationBase(IDataReader sqlReader)
        {
            List<LocationInfo> LocationList = new List<LocationInfo>();
            while (sqlReader.Read())
            {
                LocationInfo Location = new LocationInfo()
                {
                    DG_Location_pkey = GetFieldValue(sqlReader, "DG_Location_pkey", 0),
                    DG_Location_Name = GetFieldValue(sqlReader, "DG_Location_Name", string.Empty),
                    DG_Store_ID = GetFieldValue(sqlReader, "DG_Store_ID", 0),
                    DG_Location_PhoneNumber = GetFieldValue(sqlReader, "DG_Location_PhoneNumber", string.Empty),
                    DG_Location_IsActive = GetFieldValue(sqlReader, "DG_Location_IsActive", false),
                    SyncCode = GetFieldValue(sqlReader, "SyncCode", string.Empty),
                    IsSynced = GetFieldValue(sqlReader, "IsSynced", false),

                };

                LocationList.Add(Location);
            }
            return LocationList;
        }


        public List<LocationInfo> GetLocationSubstoreWise(int substoreId)
        {
            DBParameters.Clear();
            AddParameter("@substoreId", substoreId);

            IDataReader sqlReader = ExecuteReader(DAOConstant.GETLOCATIONSUBSTOREWISE);
            List<LocationInfo> LocationList = MapLocationSubstoreWiseInfo(sqlReader);
            sqlReader.Close();
            return LocationList;
        }
        private List<LocationInfo> MapLocationSubstoreWiseInfo(IDataReader sqlReader)
        {
            List<LocationInfo> LocationList = new List<LocationInfo>();
            while (sqlReader.Read())
            {
                LocationInfo Location = new LocationInfo()
                {
                    DG_Location_pkey = GetFieldValue(sqlReader, "DG_Location_pkey", 0),
                    DG_Location_Name = GetFieldValue(sqlReader, "DG_Location_Name", string.Empty),
                    DG_Location_PhoneNumber = GetFieldValue(sqlReader, "DG_Location_PhoneNumber", string.Empty),
                    DG_Location_IsActive = GetFieldValue(sqlReader, "DG_Location_IsActive", false),

                };

                LocationList.Add(Location);
            }
            return LocationList;
        }
        public LocationInfo Get(int? locationId)
        {
            DBParameters.Clear();
            AddParameter("@ParamLocationID", SetNullIntegerValue(locationId));

            List<LocationInfo> objectList;
            using (IDataReader sqlReader = ExecuteReader(DAOConstant.USP_SEL_LOCATION))
            {
                objectList = MapObject(sqlReader);
            }
            return objectList.FirstOrDefault();
        }
        private List<LocationInfo> MapObject(IDataReader sqlReader)
        {
            List<LocationInfo> objectList = new List<LocationInfo>();
            while (sqlReader.Read())
            {
                LocationInfo objectValue = new LocationInfo
                {
                    DG_Location_pkey = GetFieldValue(sqlReader, "DG_Location_pkey", 0),
                    DG_Location_Name = GetFieldValue(sqlReader, "DG_Location_Name", string.Empty),
                    DG_Store_ID = GetFieldValue(sqlReader, "DG_Store_ID", 0),
                    DG_Location_PhoneNumber = GetFieldValue(sqlReader, "DG_Location_PhoneNumber", string.Empty),
                    DG_Location_IsActive = GetFieldValue(sqlReader, "DG_Location_IsActive", false),
                    SyncCode = GetFieldValue(sqlReader, "SyncCode", string.Empty),
                    IsSynced = GetFieldValue(sqlReader, "IsSynced", false),
                };

                objectList.Add(objectValue);
            }
            return objectList;
        }

        public bool IsLocationAssociatedToSite(int _locationId)
        {
            DBParameters.Clear();
            AddParameter("@ParamLocationID", _locationId);
            AddParameter("@SubStoreCount", 0, ParameterDirection.Output);
            ExecuteNonQuery(DAOConstant.USP_SEL_LOCATION_ASSOCIATION);
            if ((int)GetOutParameterValue("@SubStoreCount") > 0)
                return true;
            else
                return false;
        }

        public bool IsSiteAssociatedToLocation(int subStoreId)
        {
            DBParameters.Clear();
            AddParameter("@ParamSiteID", subStoreId);
            AddParameter("@LocationCount", 0, ParameterDirection.Output);
            ExecuteNonQuery(DAOConstant.USP_SEL_SITE_ASSOCIATION);
            if ((int)GetOutParameterValue("@LocationCount") > 0)
                return true;
            else
                return false;
        }
    }
}
