﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DigiPhoto.IMIX.Model;
using System.Data;

namespace DigiPhoto.IMIX.DataAccess
{
    public class ReportDao : BaseDataAccess
    {
        #region Constructor
        public ReportDao(BaseDataAccess baseaccess)
            : base(baseaccess)
        { }
        public ReportDao()
        { }
        #endregion
        public DataSet SelectFinancialAudit(DateTime StartDate, DateTime EndDate, string UserName, string StoreName, int SubStoreId)
        {
            DBParameters.Clear();
            AddParameter("@ParamStartDate", StartDate);
            AddParameter("@ParamEndDate", EndDate);
            AddParameter("@ParamUserName", UserName);
            AddParameter("@ParamStoreName", StoreName);
            AddParameter("@ParamSubStoredID", SetNullIntegerValue(SubStoreId));

            return ExecuteDataSet(DAOConstant.USP_RPT_FINANCIALAUDITTRAIL);

        }
        private List<FinancialAuditTrail_Result> FinancialAuditTrail(IDataReader sqlReader)
        {
            List<FinancialAuditTrail_Result> FinancialAuditList = new List<FinancialAuditTrail_Result>();
            while (sqlReader.Read())
            {
                FinancialAuditTrail_Result FinancialAudit = new FinancialAuditTrail_Result()
                {
                    UserName = GetFieldValue(sqlReader, "UserName", string.Empty),
                    StoreName = GetFieldValue(sqlReader, "StoreName", string.Empty),
                    StartDate = GetFieldValue(sqlReader, "StartDate", DateTime.Now),
                    EndDate = GetFieldValue(sqlReader, "EndDate", DateTime.Now),
                    OrderNumber = GetFieldValue(sqlReader, "OrderNumber", string.Empty),
                    OrderDate = GetFieldValue(sqlReader, "OrderDate", DateTime.Now),
                    ProductType = GetFieldValue(sqlReader, "ProductType", string.Empty),
                    SellPrice = GetFieldValue(sqlReader, "SellPrice", string.Empty),
                    Quantity = GetFieldValue(sqlReader, "Quantity", 0),
                    TotalPrice = GetFieldValue(sqlReader, "TotalPrice", string.Empty),
                    Discount = GetFieldValue(sqlReader, "Discount", string.Empty),
                    revenue = GetFieldValue(sqlReader, "revenue", string.Empty),
                    TotalOrderPrice = GetFieldValue(sqlReader, "TotalOrderPrice", string.Empty),
                    DG_Order_SubStoreId = GetFieldValue(sqlReader, "DG_Order_SubStoreId", string.Empty),
                    ProductCode = GetFieldValue(sqlReader, "ProductCode", string.Empty),
                };

                FinancialAuditList.Add(FinancialAudit);
            }
            return FinancialAuditList;
        }


        public DataSet SelectLocationPerformance(DateTime FromDate, DateTime ToDate, DateTime SecondFromDate, DateTime SecondToDate, bool Comparasion, string StoreName, string UserName, int SubStoreId)
        {
            DBParameters.Clear();
            AddParameter("@ParamFromDate", FromDate);
            AddParameter("@ParamToDate", ToDate);
            AddParameter("@ParamSecondFromDate", SecondFromDate);
            AddParameter("@ParamSecondToDate", SecondToDate);
            AddParameter("@ParamComparasion", Comparasion);
            AddParameter("@ParamStoreName", StoreName);
            AddParameter("@ParamUserName", UserName);
            AddParameter("@ParamSubStoredID", SetNullIntegerValue(SubStoreId));

            return ExecuteDataSet(DAOConstant.USP_RPT_GETLOCATIONPERFORMANCE);

        }
        private List<GetLocationPerformance_Result> MapLocationPerformance(IDataReader sqlReader)
        {
            List<GetLocationPerformance_Result> LocationPerformanceList = new List<GetLocationPerformance_Result>();
            while (sqlReader.Read())
            {
                GetLocationPerformance_Result LocationPerformance = new GetLocationPerformance_Result();

                LocationPerformance.selectedSubStore = GetFieldValue(sqlReader, "selectedSubStore", string.Empty);
                LocationPerformance.Number_of_Capture = GetFieldValue(sqlReader, "Number_of_Capture", 0);
                LocationPerformance.Number_of_Sales = GetFieldValue(sqlReader, "Number_of_Sales", 0);
                LocationPerformance.ImageSold = GetFieldValue(sqlReader, "ImageSold", 0);
                LocationPerformance.Revenue = GetFieldValue(sqlReader, "Revenue", 0.0M);
                LocationPerformance.DG_Location_Name = GetFieldValue(sqlReader, "DG_Location_Name", string.Empty);
                LocationPerformance.StoreName = GetFieldValue(sqlReader, "StoreName", string.Empty);
                LocationPerformance.Printedby = GetFieldValue(sqlReader, "Printedby", string.Empty);
                LocationPerformance.DG_Location_pkey = GetFieldValue(sqlReader, "DG_Location_pkey", 0);
                LocationPerformance.DG_SubStore_Name = GetFieldValue(sqlReader, "DG_SubStore_Name", string.Empty);
                LocationPerformance.Shots_Previewed = GetFieldValue(sqlReader, "Shots_Previewed", 0);
                LocationPerformance.DataFlag = GetFieldValue(sqlReader, "DataFlag", string.Empty);
                LocationPerformance.FromDate = GetFieldValue(sqlReader, "FromDate", DateTime.Now);
                LocationPerformance.ToDate = GetFieldValue(sqlReader, "ToDate", DateTime.Now);
                LocationPerformance.SecondFromDate = GetFieldValue(sqlReader, "SecondFromDate", DateTime.Now);
                LocationPerformance.SecondToDate = GetFieldValue(sqlReader, "SecondToDate", DateTime.Now);
                LocationPerformance.AveragePrice = GetFieldValue(sqlReader, "AveragePrice", 0.0M);
                LocationPerformance.SellThru = GetFieldValue(sqlReader, "SellThru", 0.0M);
                LocationPerformance.defaultCurrency = GetFieldValue(sqlReader, "defaultCurrency", string.Empty);
                LocationPerformance.TotalSiteRevenue = GetFieldValue(sqlReader, "TotalSiteRevenue", 0.0M);
                LocationPerformanceList.Add(LocationPerformance);
            }
            return LocationPerformanceList;
        }

        public DataSet SelectPhotgrapherPerformance(DateTime FromDate, DateTime ToDate, DateTime SecondFromDate, DateTime SecondToDate, bool Comparasion, string StoreName, string UserName)
        {
            DBParameters.Clear();
            AddParameter("@ParamFromDate", FromDate);
            AddParameter("@ParamToDate", ToDate);
            AddParameter("@ParamSecondFromDate", SecondFromDate);
            AddParameter("@ParamSecondToDate", SecondToDate);
            AddParameter("@ParamComparasion", Comparasion);
            AddParameter("@ParamStoreName", StoreName);
            AddParameter("@ParamUserName", UserName);

            return ExecuteDataSet(DAOConstant.USP_RPT_GETPHOTGRAPHERPERFORMANCE);

        }

        #region
        //Added by Saroj on 04-12-2019 to get the result for video products performance Report 
        public DataSet SelectPhotoGrapherVideo(DateTime FromDate, DateTime ToDate, DateTime SecondFromDate, DateTime SecondToDate, bool Comparasion, string StoreName, string UserName)
        {
            DBParameters.Clear();
            AddParameter("@ParamFromDate", FromDate);
            AddParameter("@ParamToDate", ToDate);
            AddParameter("@ParamSecondFromDate", SecondFromDate);
            AddParameter("@ParamSecondToDate", SecondToDate);
            AddParameter("@ParamComparasion", Comparasion);
            AddParameter("@ParamStoreName", StoreName);
            AddParameter("@ParamUserName", UserName);

            return ExecuteDataSet(DAOConstant.USP_RPT_GETPHOTGRAPHERVIDEO);
        }
        #endregion
        private List<GetPhotgrapherPerformance_Result> MapLocationPerformanceRptInfo(IDataReader sqlReader)
        {
            List<GetPhotgrapherPerformance_Result> ActivityList = new List<GetPhotgrapherPerformance_Result>();
            while (sqlReader.Read())
            {
                GetPhotgrapherPerformance_Result Activity = new GetPhotgrapherPerformance_Result()
                {
                    NumberofCapture = GetFieldValue(sqlReader, "NumberofCapture", 0),
                    ImageSold = GetFieldValue(sqlReader, "ImageSold", 0),
                    Revenue = GetFieldValue(sqlReader, "Revenue", 0.0M),
                    DataFlag = GetFieldValue(sqlReader, "DataFlag", string.Empty),
                    NumberofSales = GetFieldValue(sqlReader, "NumberofSales", 0),
                    Shots_Previewed = GetFieldValue(sqlReader, "Shots_Previewed", 0),
                    StoreName = GetFieldValue(sqlReader, "StoreName", string.Empty),
                    Printedby = GetFieldValue(sqlReader, "Printedby", string.Empty),
                    FromDate1 = GetFieldValue(sqlReader, "FromDate1", DateTime.Now),
                    ToDate1 = GetFieldValue(sqlReader, "ToDate1", DateTime.Now),
                    FromDate2 = GetFieldValue(sqlReader, "FromDate2", DateTime.Now),
                    ToDate2 = GetFieldValue(sqlReader, "ToDate2", DateTime.Now),
                    UserName = GetFieldValue(sqlReader, "UserName", string.Empty),
                    DG_User_pkey = GetFieldValue(sqlReader, "DG_User_pkey", 0),

                };
                ActivityList.Add(Activity);
            }
            return ActivityList;
        }


        public DataSet SelectOperatorPerformanceReport(int CurrencyId, DateTime FromDate, DateTime ToDate, DateTime SecondFromDate, DateTime SecondToDate, bool Comparasion, string StoreName, string UserName)
        {
            DBParameters.Clear();
            AddParameter("@ParamCurrencyId", CurrencyId);
            AddParameter("@ParamFromDate", FromDate);
            AddParameter("@ParamToDate", ToDate);
            AddParameter("@ParamSecondFromDate", SecondFromDate);
            AddParameter("@ParamSecondToDate", SecondToDate);
            AddParameter("@ParamComparasion", Comparasion);
            AddParameter("@ParamStoreName", StoreName);
            AddParameter("@ParamUserName", UserName);

            return ExecuteDataSet(DAOConstant.USP_RPT_OPERATORPERFORMANCEREPORT);

        }
        //private List<OperatorPerformanceReport_Result> MapDG_ActivityBase(IDataReader sqlReader)
        //{
        //    List<OperatorPerformanceReport_Result> ActivityList = new List<OperatorPerformanceReport_Result>();
        //    while (sqlReader.Read())
        //    {
        //        OperatorPerformanceReport_Result Activity = new OperatorPerformanceReport_Result();

        //        Activity.CurrencySymbol = GetFieldValue(sqlReader, "CurrencySymbol", string.Empty);
        //        Activity.StoreName = GetFieldValue(sqlReader, "StoreName", string.Empty);
        //        Activity.UserName = GetFieldValue(sqlReader, "UserName", string.Empty);
        //        Activity.FromDate = GetFieldValue(sqlReader, "FromDate", DateTime.Now);
        //        Activity.ToDate = GetFieldValue(sqlReader, "ToDate", DateTime.Now);
        //        Activity.Data1 = GetFieldValue(sqlReader, "Data1", 0);
        //        Activity.Revenue = GetFieldValue(sqlReader, "Revenue", 0.0M);
        //        Activity.TotalSale = GetFieldValue(sqlReader, "TotalSale", 0L);
        //        Activity.Images_Sold = GetFieldValue(sqlReader, "Images_Sold", 0L);
        //        Activity.Capture = GetFieldValue(sqlReader, "Capture", 0);
        //        Activity.Shots_Previewed = GetFieldValue(sqlReader, "Shots_Previewed", 0);
        //        Activity.TotalBurned = GetFieldValue(sqlReader, "TotalBurned", 0);
        //        Activity.OperatorName = GetFieldValue(sqlReader, "OperatorName", string.Empty);
        //        ActivityList.Add(Activity);
        //    }
        //    return ActivityList;
        //}


        public DataSet SelectTakingReport(DateTime? FromDate, DateTime? ToDate, int SubStorePKey)
        {
            DBParameters.Clear();
            AddParameter("@ParamFromDate", SetNullDateTimeValue(FromDate));
            AddParameter("@ParamToDate", SetNullDateTimeValue(ToDate));
            AddParameter("@ParamSubStoreID", SetNullIntegerValue(SubStorePKey));
            return ExecuteDataSet(DAOConstant.USP_RPT_TAKINGREPORT);
        }
        //private List<vw_TakingReport> MapTakingReport(IDataReader sqlReader)
        //{
        //    List<vw_TakingReport> TakingReportList = new List<vw_TakingReport>();
        //    while (sqlReader.Read())
        //    {
        //        vw_TakingReport TakingReport = new vw_TakingReport();

        //        TakingReport.DG_Orders_Number = GetFieldValue(sqlReader, "DG_Orders_Number", string.Empty);
        //        TakingReport.DG_Orders_Date = GetFieldValue(sqlReader, "DG_Orders_Date", DateTime.Now);
        //        TakingReport.DG_Orders_Currency_ID = GetFieldValue(sqlReader, "DG_Orders_Currency_ID", string.Empty);
        //        TakingReport.DG_Orders_PaymentMode = GetFieldValue(sqlReader, "DG_Orders_PaymentMode", string.Empty);
        //        TakingReport.NetCost = GetFieldValue(sqlReader, "NetCost", 0.0M);
        //        TakingReport.ItemDetail = GetFieldValue(sqlReader, "ItemDetail", string.Empty);
        //        TakingReport.State = GetFieldValue(sqlReader, "State", 0);
        //        TakingReport.DG_Orders_pkey = GetFieldValue(sqlReader, "DG_Orders_pkey", 0);
        //        TakingReport.DG_SubStore_Name = GetFieldValue(sqlReader, "DG_SubStore_Name", string.Empty);
        //        TakingReport.s1 = GetFieldValue(sqlReader, "s1", string.Empty);
        //        TakingReport.ItemCode = GetFieldValue(sqlReader, "ItemCode", string.Empty);


        //        TakingReportList.Add(TakingReport);
        //    }
        //    return TakingReportList;
        //}

        public DataSet SelectOperationalAudit(DateTime FromDate, DateTime ToDate, int SubStorePKey)
        {
            DBParameters.Clear();
            AddParameter("@ParamFromDate", SetNullDateTimeValue(FromDate));
            AddParameter("@ParamToDate", SetNullDateTimeValue(ToDate));
            AddParameter("@ParamSubStoredID", SetNullIntegerValue(SubStorePKey));
            return ExecuteDataSet(DAOConstant.USP_RPT_OPERATIONALAUDIT);

        }
        private List<OperationalAudit_Result> MapOperationalAudit(IDataReader sqlReader)
        {
            List<OperationalAudit_Result> LocationList = new List<OperationalAudit_Result>();
            while (sqlReader.Read())
            {
                OperationalAudit_Result Location = new OperationalAudit_Result()
                {
                    DG_Orders_pkey = GetFieldValue(sqlReader, "DG_Orders_pkey", 0),
                    DG_Orders_Number = GetFieldValue(sqlReader, "DG_Orders_Number", string.Empty),
                    Location = GetFieldValue(sqlReader, "Location", string.Empty),
                    PhotoID = GetFieldValue(sqlReader, "PhotoID", string.Empty),
                    Qty = GetFieldValue(sqlReader, "Qty", 0),
                    PhotoGrapher = GetFieldValue(sqlReader, "PhotoGrapher", string.Empty),
                    ProductType = GetFieldValue(sqlReader, "ProductType", string.Empty),
                    SubstoreName = GetFieldValue(sqlReader, "SubstoreName", string.Empty),
                    ProductCode = GetFieldValue(sqlReader, "ProductCode", string.Empty),
                };

                LocationList.Add(Location);
            }
            return LocationList;
        }



        public DataSet SelectProductSummary(DateTime ToDate, DateTime FROMDate, string UserName, string StoreName, int SubStorePKey)
        {
            DBParameters.Clear();
            AddParameter("@ParamToDate", ToDate);
            AddParameter("@ParamFromDate", FROMDate);
            AddParameter("@ParamUserName", UserName);
            AddParameter("@ParamStoreName", StoreName);
            AddParameter("@ParamSubStoreID", SetNullIntegerValue(SubStorePKey));

            return ExecuteDataSet(DAOConstant.USP_RPT_PRODUCTSUMMARY);

        }
        //private List<ProductSummary_Result> MapProductSummary(IDataReader sqlReader)
        //{
        //    List<ProductSummary_Result> OrdersList = new List<ProductSummary_Result>();
        //    while (sqlReader.Read())
        //    {
        //        ProductSummary_Result Orders = new ProductSummary_Result();


        //            Orders.ProductName = GetFieldValue(sqlReader, "ProductName", string.Empty);
        //            Orders.TotalQuantity = GetFieldValue(sqlReader, "TotalQuantity", 0);
        //            Orders.UnitPrice = GetFieldValue(sqlReader, "UnitPrice", 0.0M);
        //            Orders.TotalCost = GetFieldValue(sqlReader, "TotalCost", 0.0M);
        //            Orders.Discount = GetFieldValue(sqlReader, "Discount", 0.0M);
        //            Orders.NetPrice = GetFieldValue(sqlReader, "NetPrice", 0.0M);
        //            Orders.TotalRevenue = GetFieldValue(sqlReader, "TotalRevenue", 0.0M);
        //            Orders.Revpercentage = GetFieldValue(sqlReader, "Revpercentage", 0.0M);
        //            Orders.FROMDate = GetFieldValue(sqlReader, "FROMDate", DateTime.Now);
        //            Orders.Todate = GetFieldValue(sqlReader, "Todate", DateTime.Now);
        //            Orders.UserName = GetFieldValue(sqlReader, "UserName", string.Empty);
        //            Orders.StoreName = GetFieldValue(sqlReader, "StoreName", string.Empty);
        //            Orders.Flag = GetFieldValue(sqlReader, "Flag", 0);
        //            Orders.DG_SubStore_Name = GetFieldValue(sqlReader, "DG_SubStore_Name", string.Empty);
        //            Orders.DG_Orders_ProductCode = GetFieldValue(sqlReader, "DG_Orders_ProductCode", string.Empty);


        //        OrdersList.Add(Orders);
        //    }
        //    return OrdersList;
        //}

        public DataSet GetOrderDetailedDiscount(DateTime FromDate, DateTime ToDate, string UserName, string StoreName)
        {
            DBParameters.Clear();
            AddParameter("@ParamFromDate", FromDate);
            AddParameter("@ParamToDate", ToDate);
            AddParameter("@ParamUserName", UserName);
            AddParameter("@ParamStoreName", StoreName);

            return ExecuteDataSet("dbo.usp_RPT_OrderDetailedDiscount");

        }
        //private List<OrderDetailedDiscount_Get_Result> MapOrderDetailedDiscount(IDataReader sqlReader)
        //{
        //    List<OrderDetailedDiscount_Get_Result> OrdersList = new List<OrderDetailedDiscount_Get_Result>();
        //    while (sqlReader.Read())
        //    {
        //        OrderDetailedDiscount_Get_Result OrdersDetail = new OrderDetailedDiscount_Get_Result();
        //        OrdersDetail.DG_Orders_Date = GetFieldValue(sqlReader, "DG_Orders_Date", DateTime.Now);
        //        OrdersDetail.DG_Orders_Cost = GetFieldValue(sqlReader, "DG_Orders_Cost", 0.0M);
        //        OrdersDetail.DG_Orders_Total_Discount = GetFieldValue(sqlReader, "DG_Orders_Total_Discount", 0.0D);
        //        OrdersDetail.DG_Orders_pkey = GetFieldValue(sqlReader, "DG_Orders_pkey", 0);
        //        OrdersDetail.DG_Orders_Number = GetFieldValue(sqlReader, "DG_Orders_Number", string.Empty);
        //        OrdersDetail.DG_Orders_NetCost = GetFieldValue(sqlReader, "DG_Orders_NetCost", 0.0M);
        //        OrdersDetail.OrderDetailId = GetFieldValue(sqlReader, "orderdetailId", 0);
        //        OrdersDetail.DG_Orders_ProductType_Name = GetFieldValue(sqlReader, "DG_Orders_ProductType_Name", string.Empty);
        //        OrdersDetail.DG_Orders_ProductCode = GetFieldValue(sqlReader, "DG_Orders_ProductCode", string.Empty);
        //        OrdersDetail.Quantity = GetFieldValue(sqlReader, "Quantity", 0);
        //        OrdersDetail.Discount = GetFieldValue(sqlReader, "Discount", string.Empty);
        //        OrdersDetail.Value = GetFieldValue(sqlReader, "Value", string.Empty);
        //        OrdersDetail.InPercentmode = GetFieldValue(sqlReader, "InPercentmode", false);
        //        OrdersDetail.PhotNumber = GetFieldValue(sqlReader, "PhotNumber", string.Empty);
        //        OrdersDetail.DG_Orders_LineItems_DiscountAmount = GetFieldValue(sqlReader, "DG_Orders_LineItems_DiscountAmount", 0.0M);
        //        OrdersDetail.DG_Orders_Details_Items_UniPrice = GetFieldValue(sqlReader, "DG_Orders_Details_Items_UniPrice", 0.0M);
        //        OrdersDetail.DG_Orders_Details_Items_TotalCost = GetFieldValue(sqlReader, "DG_Orders_Details_Items_TotalCost", 0.0M);
        //        OrdersDetail.DG_Orders_Details_Items_NetPrice = GetFieldValue(sqlReader, "DG_Orders_Details_Items_NetPrice", 0.0M);
        //        OrdersDetail.StoreName = GetFieldValue(sqlReader, "StoreName", string.Empty);
        //        OrdersDetail.USerName = GetFieldValue(sqlReader, "USerName", string.Empty);
        //        OrdersDetail.FromDate = GetFieldValue(sqlReader, "FromDate", DateTime.Now);
        //        OrdersDetail.ToDate = GetFieldValue(sqlReader, "ToDate", DateTime.Now);
        //        OrdersDetail.ActualValue = GetFieldValue(sqlReader, "ActualValue", 0.0D);
        //        OrdersDetail.TotalOrderDiscountDetails = GetFieldValue(sqlReader, "TotalOrderDiscountDetails", string.Empty);
        //        OrdersDetail.TotalLineItemDiscount = GetFieldValue(sqlReader, "TotalLineItemDiscount", 0.0M);
        //        OrdersList.Add(OrdersDetail);
        //    }
        //    return OrdersList;
        //}



        public DataSet SelectPrintedProduct(DateTime fromdate, DateTime todate, int SubStoreId)
        {
            DBParameters.Clear();
            AddParameter("@ParamFromDate", fromdate);
            AddParameter("@ParamToDate", todate);
            AddParameter("@ParamSubStoreID", SubStoreId);
            return ExecuteDataSet(DAOConstant.USP_RPT_GETPRINTEDPRODUCT);

        }
        private List<GetPrintedProduct_Result> MapPrintedProduct(IDataReader sqlReader)
        {
            List<GetPrintedProduct_Result> OrdersList = new List<GetPrintedProduct_Result>();
            while (sqlReader.Read())
            {
                GetPrintedProduct_Result Orders = new GetPrintedProduct_Result()
                {
                    DG_Orders_ProductType_Name = GetFieldValue(sqlReader, "DG_Orders_ProductType_Name", string.Empty),
                    DG_SubStore_Name = GetFieldValue(sqlReader, "DG_SubStore_Name", string.Empty),
                    PrintedQuantity = GetFieldValue(sqlReader, "PrintedQuantity", 0),
                    DG_Print_Date = GetFieldValue(sqlReader, "DG_Print_Date", DateTime.Now),
                    PhotoNumber = GetFieldValue(sqlReader, "PhotoNumber", string.Empty),
                    ProductCode = GetFieldValue(sqlReader, "ProductCode", string.Empty),
                };

                OrdersList.Add(Orders);
            }
            return OrdersList;
        }




        public DataSet GetPrintSummary(DateTime FromDate, DateTime ToDate ,int SubStoreId)
        {
            DBParameters.Clear();
            AddParameter("@ParamFromDate", FromDate);
            AddParameter("@ParamToDate", ToDate);
            AddParameter("@ParamSubStoreID", SubStoreId);

            return ExecuteDataSet(DAOConstant.USP_RPT_GETPRINTSUMMARY);
        }
        public DataSet GetIPPrintTracking(DateTime FromDate, DateTime ToDate, int subStoreId,int packageID)
        {
            DBParameters.Clear();
            AddParameter("@ParamFromDate", FromDate);
            AddParameter("@ParamToDate", ToDate);
            AddParameter("@ParamSubStoreID", subStoreId);
            AddParameter("@ParamPackageID", packageID);

            return ExecuteDataSet(DAOConstant.USP_RPT_IPPRINTTRACKING);
        }
        public DataSet GetIPContentTracking(DateTime FromDate, DateTime ToDate, int subStoreId, int packageID)
        {
            DBParameters.Clear();
            AddParameter("@ParamFromDate", FromDate);
            AddParameter("@ParamToDate", ToDate);
            AddParameter("@ParamSubStoreID", subStoreId);
            //AddParameter("@ParamPackageID", packageID);

            return ExecuteDataSet(DAOConstant.USP_RPT_IPCONTENTTRACKING);
        }
        private List<PrintSummary_Result> MapPrintSummary(IDataReader sqlReader)
        {
            List<PrintSummary_Result> PrintSummaryList = new List<PrintSummary_Result>();
            while (sqlReader.Read())
            {
                PrintSummary_Result PrintSummary = new PrintSummary_Result()
                {
                    DG_Orders_ProductType_Name = GetFieldValue(sqlReader, "DG_Orders_ProductType_Name", string.Empty),
                    PrintedQuantity = GetFieldValue(sqlReader, "PrintedQuantity", 0),
                    PrintedSold = GetFieldValue(sqlReader, "PrintedSold", 0),
                };
                PrintSummaryList.Add(PrintSummary);
            }
            return PrintSummaryList;
        }

        #region Collecting & Purging  Report Data

        public void CollectOrderDetailedDiscountData(DateTime fromdate, DateTime todate)
        {
            DBParameters.Clear();
            AddParameter("@ParamFromDate", fromdate);
            AddParameter("@ParamToDate", todate);
            ExecuteNonQuery(DAOConstant.USP_RPT_COLLECT_ORDERDETAILEDDISCOUNTDATA);
        }
        public void PurgeOrderDetailedDiscountData(DateTime fromdate, DateTime todate)
        {
            DBParameters.Clear();
            AddParameter("@ParamFromDate", fromdate);
            AddParameter("@ParamToDate", todate);
            ExecuteNonQuery(DAOConstant.USP_RPT_PURGE_ORDERDETAILEDDISCOUNTDATA);
        }
        public void CollectActivityReportData(DateTime fromdate, DateTime todate)
        {
            DBParameters.Clear();
            AddParameter("@ParamFromDate", fromdate);
            AddParameter("@ParamToDate", todate);
            ExecuteNonQuery(DAOConstant.USP_RPT_COLLECT_ACTIVITYREPORTDATA);
        }
        public void PurgeActivityReportData(DateTime fromdate, DateTime todate)
        {
            DBParameters.Clear();
            AddParameter("@ParamFromDate", fromdate);
            AddParameter("@ParamToDate", todate);
            ExecuteNonQuery(DAOConstant.USP_RPT_PURGE_ACTIVITYREPORTDATA);
        }
        public void CollectOperatorPerformanceData(DateTime fromdate, DateTime todate)
        {
            DBParameters.Clear();
            AddParameter("@ParamFromDate", fromdate);
            AddParameter("@ParamToDate", todate);
            ExecuteNonQuery(DAOConstant.USP_RPT_COLLECT_OPERATORPERFORMANCEDATA);
        }
        public void PurgeOperatorPerformanceData(DateTime fromdate, DateTime todate)
        {
            DBParameters.Clear();
            AddParameter("@ParamFromDate", fromdate);
            AddParameter("@ParamToDate", todate);
            ExecuteNonQuery(DAOConstant.USP_RPT_PURGE_OPERATORPERFORMANCEDATA);
        }
        public void CollectFinancialAuditData(DateTime fromdate, DateTime todate)
        {
            DBParameters.Clear();
            AddParameter("@ParamFromDate", fromdate);
            AddParameter("@ParamToDate", todate);
            ExecuteNonQuery(DAOConstant.USP_RPT_COLLECT_FINANCIALAUDITDATA);
        }
        public void PurgeFinancialAuditData(DateTime fromdate, DateTime todate)
        {
            DBParameters.Clear();
            AddParameter("@ParamFromDate", fromdate);
            AddParameter("@ParamToDate", todate);
            ExecuteNonQuery(DAOConstant.USP_RPT_PURGE_FINANCIALAUDITDATA);
        }

        #endregion

        public ReportParams FetchReportFormatDetails(int reportType)
        {
            try
            {
                DBParameters.Clear();
                AddParameter(DBConstant.USP_FetchReportFormatParam, reportType);
                var dr = ExecuteReader(DAOConstant.USP_FetchReportFormat);
                if (dr != null)
                {
                    ReportParams reportParams = new ReportParams();
                    reportParams.ReportFormats = new Dictionary<string, string>();
                    while (dr.Read())
                    {
                        reportParams.ReportFormats.Add(dr[DBConstant.ReportColumn].ToString(), dr[DBConstant.DataColumn].ToString());
                        if (string.IsNullOrEmpty(reportParams.ReportType))
                            reportParams.ReportType = dr[DBConstant.ReportTypeColumn].ToString();
                    }
                    return reportParams;
                }
            }
            catch (Exception ex)
            {
                //Log the exception
            }
            return null;
        }
        /// /added by latika for Evoucher 25 March 2020 
        /// </summary>
        /// <param name="FromDate"></param>
        /// <param name="ToDate"></param>
        /// <returns></returns>

        public DataSet SelectEvoucherDetails(DateTime FromDate, DateTime ToDate)
        {
            DBParameters.Clear();
            AddParameter("@FromDt", FromDate);
            AddParameter("@ToDt", ToDate);

            return ExecuteDataSet(DAOConstant.USP_RPT_SP_EVoucherDetails);
        }
        /// <summary>
        ///  ended by latika
    }
}
