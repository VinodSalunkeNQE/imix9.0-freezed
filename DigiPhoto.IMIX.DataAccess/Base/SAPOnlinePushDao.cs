﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DigiPhoto.IMIX.Model;
using System.Data;
using DigiPhoto.IMIX.Model.Base;

namespace DigiPhoto.IMIX.DataAccess.Base
{
   public class SAPOnlinePushDao : BaseDataAccess
    {
        #region Constructor
        public SAPOnlinePushDao(BaseDataAccess baseaccess)
            : base(baseaccess)
        { }
        public SAPOnlinePushDao()
        { }
        #endregion
        public List<SAPOnlineDataPushInfo> SelectAllSAPOnlinePushData()
        {
            DBParameters.Clear();

            IDataReader sqlReader = ExecuteReader(DAOConstant.SAPOnlinePushData);
            List<SAPOnlineDataPushInfo> PhotosList = MapAllSAPOnlinePushData(sqlReader);
            sqlReader.Close();

            return PhotosList;
        }

        public void DataInsertintoTable( DateTime Todaysdate)
        {
            DBParameters.Clear();
            AddParameter("@Datetime", Todaysdate);
            ExecuteScalar(DAOConstant.SAPPushDataintoTable);
        }

        private List<SAPOnlineDataPushInfo> MapAllSAPOnlinePushData(IDataReader sqlReader)
        {
            List<SAPOnlineDataPushInfo> SAPOnlineDataList = new List<SAPOnlineDataPushInfo>();
            try
            {
                while (sqlReader.Read())
                {
                    SAPOnlineDataPushInfo Photos = new SAPOnlineDataPushInfo()
                    {
                        Id = GetFieldValue(sqlReader, "Id", 0L),
                        SalesOrderno = GetFieldValue(sqlReader, "SalesOrderno", string.Empty),
                        ItemNo = GetFieldValue(sqlReader, "ItemNo", 0),
                        Customer = GetFieldValue(sqlReader, "Customer", string.Empty),
                        CustomerDescription = GetFieldValue(sqlReader, "CustomerDescription", string.Empty),
                        SalesOrg = GetFieldValue(sqlReader, "SalesOrg", string.Empty),

                        SalesOrgDescription = GetFieldValue(sqlReader, "SalesOrgDescription", string.Empty),
                        DistributionChannel = GetFieldValue(sqlReader, "DistributionChannel", string.Empty),
                        DistributionChannelDescription = GetFieldValue(sqlReader, "DistributionChannelDescription", string.Empty),
                        Division = GetFieldValue(sqlReader, "Division", string.Empty),
                        DivisionDescription = GetFieldValue(sqlReader, "DivisionDescription", string.Empty),
                        Material = GetFieldValue(sqlReader, "Material", string.Empty),
                        MaterialDescription = GetFieldValue(sqlReader, "MaterialDescription", string.Empty),
                        SendingPlant = GetFieldValue(sqlReader, "SendingPlant", string.Empty),
                        SendingPlantDescription = GetFieldValue(sqlReader, "SendingPlantDescription", string.Empty),
                        Quantity = GetFieldValue(sqlReader, "Quantity", 0),
                        UOM = GetFieldValue(sqlReader, "UOM", string.Empty),
                        TotalPrice = GetFieldValue(sqlReader, "TotalPrice", 0.0m),
                        Cash = GetFieldValue(sqlReader, "Cash", 0.0m),
                        Visa = GetFieldValue(sqlReader, "Visa", 0.0m),
                        Master = GetFieldValue(sqlReader, "Master", 0.0m),
                        Amex = GetFieldValue(sqlReader, "Amex", 0.0m),
                        JCB = GetFieldValue(sqlReader, "JCB", 0.0m),
                        Unionpay = GetFieldValue(sqlReader, "Unionpay", 0.0m),
                        Voucher = GetFieldValue(sqlReader, "Voucher", 0.0m),
                        MPU = GetFieldValue(sqlReader, "MPU", 0.0m),
                        Exchange = GetFieldValue(sqlReader, "Exchange", 0.0m),
                        MobileBanking = GetFieldValue(sqlReader, "MobileBanking", 0.0m),
                        KVL = GetFieldValue(sqlReader, "KVL", 0.0m),
                        Room = GetFieldValue(sqlReader, "Room", 0.0m),
                        FC = GetFieldValue(sqlReader, "FC", 0.0m),
                        Others = GetFieldValue(sqlReader, "Others", 0.0m),
                        Tax_deduction = GetFieldValue(sqlReader, "Tax_deduction", 0.0m),
                        Tax = GetFieldValue(sqlReader, "Tax", 0.0m),
                        Discount = GetFieldValue(sqlReader, "Discount", 0.0m),
                        OrderDate = GetFieldValue(sqlReader, "OrderDate", DateTime.Now),
                        Customerreference = GetFieldValue(sqlReader, "Customerreference", string.Empty),
                        CanceledInvoicelineitem = GetFieldValue(sqlReader, "CanceledInvoicelineitem", string.Empty),
                        CanceledBillDoc = GetFieldValue(sqlReader, "CanceledBillDoc", string.Empty),
                        Createddate = GetFieldValue(sqlReader, "Createddate", DateTime.Now),
                        CreatedTime = GetFieldValue(sqlReader, "CreatedTime", string.Empty),
                        CountryCode = GetFieldValue(sqlReader, "CountryCode", string.Empty),
                        PkgSyncCode = GetFieldValue(sqlReader, "PkgSyncCode", string.Empty),
                        Syncstatus = GetFieldValue(sqlReader, "Syncstatus", "1"),
                        UploadStatus = GetFieldValue(sqlReader, "UploadStatus", "Upload Successful"),
                        RetryCount = GetFieldValue(sqlReader, "RetryCount", 0),
                        Field1 = GetFieldValue(sqlReader, "Field1", string.Empty),
                        Field2 = GetFieldValue(sqlReader, "Field2", string.Empty)
                    };

                    SAPOnlineDataList.Add(Photos);
                }
            }
            catch(Exception ex)
            {
                SAPOnlineDataList = null;
            }
            return SAPOnlineDataList;
        }

        public bool GetErrorCount(string Photo_Id, int RetryCount)
        {
            bool flag = false;
            DBParameters.Clear();
            AddParameter("@PhotoId", Photo_Id);
            AddParameter("@RetryCount", RetryCount);
            flag = Convert.ToBoolean(ExecuteScalar("CheckUploadFailureErrorCount"));

            return flag;
        }
    }
}
