﻿using DigiPhoto.IMIX.Model.Base;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DigiPhoto.IMIX.DataAccess.Base
{
  public  class StoryBookDao:BaseDataAccess
    {
        /// <summary>
        ///  Added By Ajinkya Y.
        /// </summary>
        /// <param name="baseaccess"></param>
        public StoryBookDao(BaseDataAccess baseaccess)
            : base(baseaccess)
        { }
        public StoryBookDao()
        { }
        public List<StoryInfo> Select(int? storyId)    // Added by Ajinkya
        {
            DBParameters.Clear();
            AddParameter("@ParamStoryBookId", SetNullIntegerValue(storyId));

            List<StoryInfo> objectList;
            using (IDataReader sqlReader = ExecuteReader(DAOConstant.usp_SEL_StoryBookDetail))
            {
                objectList = MapObject(sqlReader);
            }
            return objectList;
        }

        private List<StoryInfo> MapObject(IDataReader sqlReader)
        {
            List<StoryInfo> objectList = new List<StoryInfo>();
            while (sqlReader.Read())
            {
                StoryInfo objectValue = new StoryInfo
                {
                    DG_StoryBookId = GetFieldValue(sqlReader, "DG_StoryBookId", 0),
                    DG_ThemeId = GetFieldValue(sqlReader, "DG_ThemeId", 0),
                    DG_Theme = GetFieldValue(sqlReader, "DG_Theme", string.Empty),
                    DG_PageNo = GetFieldValue(sqlReader, "DG_PageNo", string.Empty),
                    DG_Text_Chinese = GetFieldValue(sqlReader, "DG_Text_Chinese", string.Empty),
                    DG_Text_English = GetFieldValue(sqlReader, "DG_Text_English", string.Empty),
                    DG_Site = GetFieldValue(sqlReader, "DG_Site", string.Empty),
                    DG_Location = GetFieldValue(sqlReader, "DG_Location", string.Empty),
                    DG_CoOrdinate_ChineseText = GetFieldValue(sqlReader, "DG_CoOrdinate_ChineseText", string.Empty),
                    DG_CoOrdinate_EnglishText = GetFieldValue(sqlReader, "DG_CoOrdinate_EnglishText", string.Empty),
                    DG_FontSize_Chinese = GetFieldValue(sqlReader, "DG_FontSize_Chinese", string.Empty),
                    DG_FontSize_English = GetFieldValue(sqlReader, "DG_FontSize_English", string.Empty),
                    DG_FontColor_Chinese = GetFieldValue(sqlReader, "DG_FontColor_Chinese", string.Empty),
                    DG_FontColor_English = GetFieldValue(sqlReader, "DG_FontColor_English", string.Empty),
                    DG_IsActive = GetFieldValue(sqlReader, "DG_IsActive", false),
                    DG_FontChinese = GetFieldValue(sqlReader, "DG_FontChinese", string.Empty),
                    DG_FontEnglish = GetFieldValue(sqlReader, "DG_FontEnglish", string.Empty),
                };

                objectList.Add(objectValue);
            }
            return objectList;
        }
    }
}
