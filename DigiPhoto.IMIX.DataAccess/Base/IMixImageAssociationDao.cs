﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DigiPhoto.IMIX.Model;
using System.Data;
namespace DigiPhoto.IMIX.DataAccess
{
    public class IMixImageAssociationDao : BaseDataAccess
    {
        #region Constructor
        public IMixImageAssociationDao(BaseDataAccess baseaccess)
            : base(baseaccess)
        { }
        public IMixImageAssociationDao()
        { }
        #endregion
        public List<iMixImageAssociationInfo> Get(string PhotoId)
        {
            DBParameters.Clear();
            AddParameter("@PhotoId", PhotoId);


            IDataReader sqlReader = ExecuteReader(DAOConstant.GETQRORBARCODEBYPHOTOID);
            List<iMixImageAssociationInfo> iMixImageAssociationList = MapiMixImageAssociationInfo(sqlReader);
            sqlReader.Close();
            return iMixImageAssociationList;
        }
        private List<iMixImageAssociationInfo> MapiMixImageAssociationInfo(IDataReader sqlReader)
        {
            List<iMixImageAssociationInfo> iMixImageAssociationList = new List<iMixImageAssociationInfo>();
            while (sqlReader.Read())
            {
                iMixImageAssociationInfo iMixImageAssociation = new iMixImageAssociationInfo()
                {
                    CardUniqueIdentifier = GetFieldValue(sqlReader, "CardUniqueIdentifier", string.Empty),
                };

                iMixImageAssociationList.Add(iMixImageAssociation);
            }
            return iMixImageAssociationList;
        }
        public List<string> GetQrOrBarCodeByPhotoID(string PhotoId)
        {
            DBParameters.Clear();
            AddParameter("@PhotoId", PhotoId);


            IDataReader sqlReader = ExecuteReader(DAOConstant.GETQRORBARCODEBYPHOTOID);
            List<string> iMixImageAssociationList = MapQrOrBarCodeByPhotoIDInfo(sqlReader);
            sqlReader.Close();
            return iMixImageAssociationList;
        }
        private List<string> MapQrOrBarCodeByPhotoIDInfo(IDataReader sqlReader)
        {
            List<string> iMixImageAssociationList = new List<string>();
            while (sqlReader.Read())
            {
                string iMixImageAssociation = GetFieldValue(sqlReader, "CardUniqueIdentifier", string.Empty);
                iMixImageAssociationList.Add(iMixImageAssociation);
            }
            return iMixImageAssociationList;
        }



        public List<iMixImageAssociationInfo> GetUniqueCodeExists(bool IsAnonymousQrCodeEnabled, string QRCode)
        {
            DBParameters.Clear();
            AddParameter("@ParamIsAnonymousQrCodeEnabled", IsAnonymousQrCodeEnabled);
            AddParameter("@ParamQRCode", QRCode);

            IDataReader sqlReader = ExecuteReader(DAOConstant.USP_GET_UNIQUECODEEXISTS);
            List<iMixImageAssociationInfo> iMixImageAssociationList = MapUniqueCodeExistsInfo(sqlReader);
            sqlReader.Close();
            return iMixImageAssociationList;
        }
        private List<iMixImageAssociationInfo> MapUniqueCodeExistsInfo(IDataReader sqlReader)
        {
            List<iMixImageAssociationInfo> iMixImageAssociationList = new List<iMixImageAssociationInfo>();
            while (sqlReader.Read())
            {
                iMixImageAssociationInfo iMixImageAssociation = new iMixImageAssociationInfo()
                {
                    IMIXCardTypeId = GetFieldValue(sqlReader, "IMIXCardTypeId", 0),
                };

                iMixImageAssociationList.Add(iMixImageAssociation);
            }
            return iMixImageAssociationList;
        }

    }
}
