﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DigiPhoto.IMIX.Model;
using System.Data;


namespace DigiPhoto.IMIX.DataAccess
{
    public class SubStoreDao : BaseDataAccess
    {
        #region Constructor
        public SubStoreDao(BaseDataAccess baseaccess)
            : base(baseaccess)
        { }
        public SubStoreDao()
        { }
        #endregion

        public List<SubStoresInfo> GetSubstoreData()
        {
            IDataReader sqlReader = ExecuteReader(DAOConstant.USP_GET_SUBSTOREDATAISACTIVE);
            List<SubStoresInfo> SubStoresList = MapSubStoresBase(sqlReader);
            sqlReader.Close();
            return SubStoresList;
        }
        /// <summary>
        ///  /// ////////////// Created by latika for visible in ViewOrderStation 2019 Nov 11
        /// </summary>
        /// <returns></returns>
        public SubStoresInfo GetSubstoreVisibleOrdStion(int SubStoreID)
        {
            DBParameters.Clear();
            AddParameter("@SubStoreID", SubStoreID);
            IDataReader sqlReader = ExecuteReader("usp_GET_SubstoreVisiblVOrderStation");
            SubStoresInfo SubStoresList = MapSubStoresBaseVisiblSub(sqlReader);
            sqlReader.Close();
            return SubStoresList;
        }
        /// <summary>
        /// ended by latika
        /// </summary>
        /// <returns></returns>
        public List<SubStoresInfo> GetSubstoreDataFillGrid()
        {
            IDataReader sqlReader = ExecuteReader(DAOConstant.USP_GET_SUBSTOREDATAISACTIVEForGrid);
            List<SubStoresInfo> SubStoresList = MapSubStoresBase(sqlReader);
            sqlReader.Close();
            return SubStoresList;
        }

        public List<SiteCodeDetail> GetSiteCodeAccess()
        {
            IDataReader sqlReader = ExecuteReader(DAOConstant.USP_GETSITECODE);
            List<SiteCodeDetail> SubStoresList = MapSiteCode(sqlReader);
            sqlReader.Close();
            return SubStoresList;
        }

        public List<SubStoresInfo> GetLoginUserDefaultSubstores(int subStoreId)
        {
            DBParameters.Clear();
            AddParameter("@ParamSubStoreID", subStoreId);

            IDataReader sqlReader = ExecuteReader(DAOConstant.USP_SEL_LOGINUSERDEFAULTSUBSTORE);
            var subStoresList = MapSubStoresBase(sqlReader);
            sqlReader.Close();
            return subStoresList;
        }

        private List<SubStoresInfo> MapSubStoresBase(IDataReader sqlReader)
        {
            List<SubStoresInfo> SubStoresList = new List<SubStoresInfo>();
            while (sqlReader.Read())
            {
                SubStoresInfo SubStores = new SubStoresInfo()
                {
                    DG_SubStore_pkey = GetFieldValue(sqlReader, "DG_SubStore_pkey", 0),
                    DG_SubStore_Name = GetFieldValue(sqlReader, "DG_SubStore_Name", string.Empty),
                    DG_SubStore_Description = GetFieldValue(sqlReader, "DG_SubStore_Description", string.Empty),
                    DG_SubStore_Locations = GetFieldValue(sqlReader, "DG_SubStore_Locations", string.Empty),
                };
                SubStoresList.Add(SubStores);
            }
            return SubStoresList;
        }
        private SubStoresInfo MapSubStoresBaseVisiblSub(IDataReader sqlReader)
        {
           SubStoresInfo SubStoresList = new SubStoresInfo();
            while (sqlReader.Read())
            {
                SubStoresInfo SubStores = new SubStoresInfo()
                {
                    DG_SubStore_pkey = GetFieldValue(sqlReader, "DG_SubStore_pkey", 0),
                    DG_SubStore_Name = GetFieldValue(sqlReader, "DG_SubStore_Name", string.Empty),
                    DG_SubStore_Description = GetFieldValue(sqlReader, "DG_SubStore_Description", string.Empty),
                    DG_SubStore_Locations = GetFieldValue(sqlReader, "DG_SubStore_Locations", string.Empty),
                    VisiblVOrderStationlst = GetFieldValue(sqlReader, "VisiblVOrderStation", string.Empty), /// ////////////// Created by latika for visible in ViewOrderStation 2019 Nov 11
                };
                SubStoresList= SubStores;
            }
            return SubStoresList;
        }
        private List<SiteCodeDetail> MapSiteCode(IDataReader sqlReader)
        {
            List<SiteCodeDetail> SiteCodeList = new List<SiteCodeDetail>();
            while (sqlReader.Read())
            {
                SiteCodeDetail SiteCode = new SiteCodeDetail()
                {
                    SiteId = GetFieldValue(sqlReader, "SiteId", 0),
                    SiteCode = GetFieldValue(sqlReader, "SiteCode", string.Empty),

                };
                SiteCodeList.Add(SiteCode);
            }
            return SiteCodeList;
        }
        public List<string> GetBackupSubStoreNameBySubStoreId(int SubStoreId)
        {
            DBParameters.Clear();
            AddParameter("@ParamSubStoreId", SubStoreId);

            IDataReader sqlReader = ExecuteReader(DAOConstant.USP_SEL_BACKUPSUBSTORENAMEBYSUBSTOREID);
            List<string> SubStoreDataList = MapBackupSubStoreNameBySubStoreIdInfo(sqlReader);
            sqlReader.Close();
            return SubStoreDataList;
        }

        /// <summary>
        /// Manoj Gupta at 7-Sept 2018
        /// This function is used to delete data from backuphistory based on backupId
        /// </summary>
        /// <param name="backupId"></param>
        public int DeleteBackupHistory(int backupId)
        {
            DBParameters.Clear();
            AddParameter("@BackupId", backupId);
            AddParameter("@IsDeleted", "", ParameterDirection.Output);
            ExecuteNonQuery(DAOConstant.USP_DELETEBACKUPHISTORY);
            int Ret = Convert.ToInt32(GetOutParameterValue("@IsDeleted"));
            return Ret;
        }
        private List<string> MapBackupSubStoreNameBySubStoreIdInfo(IDataReader sqlReader)
        {
            List<string> SubStoreDataList = new List<string>();
            while (sqlReader.Read())
            {
                SubStoreDataList.Add(GetFieldValue(sqlReader, "DG_SubStore_Name", string.Empty));
            }
            return SubStoreDataList;
        }
        public Dictionary<string, string> GetSubstoreDataDir()
        {
            IDataReader sqlReader = ExecuteReader(DAOConstant.USP_GET_SUBSTOREDATAISACTIVE);
            Dictionary<string, string> SubStoresList = MapSubStoresBassDir(sqlReader);
            sqlReader.Close();
            return SubStoresList;
        }
        private Dictionary<string, string> MapSubStoresBassDir(IDataReader sqlReader)
        {
            Dictionary<string, string> SubStoresList = new Dictionary<string, string>();

            while (sqlReader.Read())
            {
                SubStoresList.Add(GetFieldValue(sqlReader, "DG_SubStore_pkey", 0).ToString(), GetFieldValue(sqlReader, "DG_SubStore_Name", string.Empty));
            }
            return SubStoresList;
        }
        public SubStoresInfo GetSubstoreData(int SubStore_pkey, bool SubStore_IsActive)
        {
            DBParameters.Clear();
            AddParameter("@ParamSubStore_pkey", SubStore_pkey);
            AddParameter("@ParamSubStore_IsActive", SubStore_IsActive);

            IDataReader sqlReader = ExecuteReader(DAOConstant.USP_GET_SUBSTOREDATA);
            List<SubStoresInfo> SubStoresList = MapDG_SubStoresBase(sqlReader);
            sqlReader.Close();
            return SubStoresList.FirstOrDefault();
        }
        private List<SubStoresInfo> MapDG_SubStoresBase(IDataReader sqlReader)
        {
            List<SubStoresInfo> SubStoresList = new List<SubStoresInfo>();
            while (sqlReader.Read())
            {
                SubStoresInfo SubStores = new SubStoresInfo()
                {
                    DG_SubStore_pkey = GetFieldValue(sqlReader, "DG_SubStore_pkey", 0),
                    DG_SubStore_Name = GetFieldValue(sqlReader, "DG_SubStore_Name", string.Empty),
                    DG_SubStore_Description = GetFieldValue(sqlReader, "DG_SubStore_Description", string.Empty),
                    DG_SubStore_IsActive = GetFieldValue(sqlReader, "DG_SubStore_IsActive", false),
                    SyncCode = GetFieldValue(sqlReader, "SyncCode", string.Empty),
                    IsSynced = GetFieldValue(sqlReader, "IsSynced", false),
                    IsLogicalSubStore = GetFieldValue(sqlReader, "IsLogicalSubStore", false),
                    LogicalSubStoreID = GetFieldValue(sqlReader, "LogicalSubStoreID", 0),
                    DG_SubStore_Code = GetFieldValue(sqlReader, "DG_SubStore_Code", string.Empty),
                    SiteID = GetFieldValue(sqlReader, "SiteID", 0)
                    

                };
                SubStoresList.Add(SubStores);
            }
            return SubStoresList;
        }
        public string DelSubstoreByID(int SubstoreId)
        {

            DBParameters.Clear();
            AddParameter("@ParamSubStorekey", SubstoreId);
            AddParameter("@Ret", "", ParameterDirection.Output);
            ExecuteNonQuery(DAOConstant.USP_DEL_SUBSTOREBYID);
            string Ret = Convert.ToString(GetOutParameterValue("@Ret"));
            return Ret;
        }

        public bool Update(SubStoresInfo objectInfo)
        {
            DBParameters.Clear();
            AddParameter("@ParamDG_SubStore_pkey", objectInfo.DG_SubStore_pkey);
            AddParameter("@ParamIsSynced", objectInfo.IsSynced);
            AddParameter("@ParamDG_SubStore_Name", objectInfo.DG_SubStore_Name);
            AddParameter("@ParamDG_SubStore_Description", objectInfo.DG_SubStore_Description);
            AddParameter("@paramIsLogical", objectInfo.IsLogicalSubStore);
            AddParameter("@ParamLogicalSubStoreID", objectInfo.LogicalSubStoreID);
            AddParameter("@SiteCode", objectInfo.DG_SubStore_Code);
            ExecuteNonQuery(DAOConstant.USP_UPD_SUBSTORE);
            return true;
        }
        public int InsertSubStore(SubStoresInfo objectInfo)
        {
            DBParameters.Clear();
            AddParameter("@ParamSubStoreId", objectInfo.DG_SubStore_pkey);
            AddParameter("@ParamDG_SubStore_IsActive", objectInfo.DG_SubStore_IsActive);
            AddParameter("@ParamIsSynced", objectInfo.IsSynced);
            AddParameter("@ParamDG_SubStore_Name", objectInfo.DG_SubStore_Name);
            AddParameter("@ParamDG_SubStore_Description", objectInfo.DG_SubStore_Description);
            AddParameter("@ParamSyncCode", objectInfo.SyncCode);
            AddParameter("@paramIsLogical", objectInfo.IsLogicalSubStore);
            AddParameter("@ParamLogicalSubStoreID", objectInfo.LogicalSubStoreID);
            AddParameter("@SiteCode", objectInfo.DG_SubStore_Code);
            ExecuteNonQuery(DAOConstant.USP_INS_SUBSTORE);
            int objectId = (int)GetOutParameterValue("@ParamSubStoreId");
            return objectId;


        }
        public List<SubStoresInfo> Getvw_GetSubStoreData()
        {
            IDataReader sqlReader = ExecuteReader(DAOConstant.USP_SEL_SUBSTOREDATA);
            List<SubStoresInfo> SubStoreDataList = MapVWSubStoreInfo(sqlReader);
            sqlReader.Close();
            return SubStoreDataList;
        }

        public List<SubStoresInfo> Getvw_GetLogicalSubStoreData()
        {
            IDataReader sqlReader = ExecuteReader(DAOConstant.usp_SEL_LogicalSubStoreData);
            List<SubStoresInfo> SubStoreDataList = MapVWLogicalSubStoreInfo(sqlReader);
            sqlReader.Close();
            return SubStoreDataList;
        }

        private List<SubStoresInfo> MapVWSubStoreInfo(IDataReader sqlReader)
        {
            List<SubStoresInfo> SubStoreDataList = new List<SubStoresInfo>();
            while (sqlReader.Read())
            {
                SubStoresInfo SubStoreData = new SubStoresInfo()
                {
                    DG_SubStore_pkey = GetFieldValue(sqlReader, "DG_SubStore_pkey", 0),
                    DG_SubStore_Name = GetFieldValue(sqlReader, "DG_SubStore_Name", string.Empty),
                    DG_SubStore_Description = GetFieldValue(sqlReader, "DG_SubStore_Description", string.Empty),
                    DG_SubStore_Locations = GetFieldValue(sqlReader, "DG_SubStore_Locations", string.Empty),
                };
                SubStoreDataList.Add(SubStoreData);
            }
            return SubStoreDataList;
        }

        private List<SubStoresInfo> MapVWLogicalSubStoreInfo(IDataReader sqlReader)
        {
            List<SubStoresInfo> SubStoreDataList = new List<SubStoresInfo>();
            while (sqlReader.Read())
            {
                SubStoresInfo SubStoreData = new SubStoresInfo()
                {
                    DG_SubStore_pkey = GetFieldValue(sqlReader, "DG_SubStore_pkey", 0),
                    DG_SubStore_Name = GetFieldValue(sqlReader, "DG_SubStore_Name", string.Empty),
                    DG_SubStore_Description = GetFieldValue(sqlReader, "DG_SubStore_Description", string.Empty),
                    DG_SubStore_Locations = GetFieldValue(sqlReader, "DG_SubStore_Locations", string.Empty),
                    DG_SubStore_Code = GetFieldValue(sqlReader, "DG_SubStore_Code", string.Empty),
                    SiteID = GetFieldValue(sqlReader, "SiteID", 0),
                    IsLogicalSubStore = GetFieldValue(sqlReader, "IsLogicalSubStore", false),
                    LogicalSubStoreID = GetFieldValue(sqlReader, "LogicalSubStoreID", 0),

                };
                SubStoreDataList.Add(SubStoreData);
            }
            return SubStoreDataList;
        }

        public List<SubStoresInfo> GetLogicalSubStore()
        {
            IDataReader sqlReader = ExecuteReader(DAOConstant.USP_SEL_LogicalSubStore);
            List<SubStoresInfo> SubStoreDataList = MapLogicalSubStoreInfo(sqlReader);
            sqlReader.Close();
            return SubStoreDataList;
        }
        private List<SubStoresInfo> MapLogicalSubStoreInfo(IDataReader sqlReader)
        {
            List<SubStoresInfo> SubStoreDataList = new List<SubStoresInfo>();
            while (sqlReader.Read())
            {
                SubStoresInfo SubStoreData = new SubStoresInfo()
                {
                    DG_SubStore_pkey = GetFieldValue(sqlReader, "DG_SubStore_pkey", 0),
                    DG_SubStore_Name = GetFieldValue(sqlReader, "DG_SubStore_Name", string.Empty),
                    DG_SubStore_Description = GetFieldValue(sqlReader, "DG_SubStore_Description", string.Empty),
                    DG_SubStore_IsActive = GetFieldValue(sqlReader, "DG_SubStore_IsActive", false),

                };
                SubStoreDataList.Add(SubStoreData);
            }
            return SubStoreDataList;
        }
        public List<RfidInfo> GetRfidAccess(int SubStoreId)
        {
            DBParameters.Clear();
            AddParameter("@ParamSubStoreId", SubStoreId);
            IDataReader sqlReader = ExecuteReader("usp_GetConfigInfoSubStoreWise");
            List<RfidInfo> lstrfidInfo = MapRfidInfo(sqlReader);
            sqlReader.Close();
            return lstrfidInfo;
        }

        private List<RfidInfo> MapRfidInfo(IDataReader sqlReader)
        {
            List<RfidInfo> lstRfidInfo = new List<RfidInfo>();
            while (sqlReader.Read())
            {
                RfidInfo rfidInfo = new RfidInfo()
                {
                    ImixConfigValueID = GetFieldValue(sqlReader, "IMIXConfigurationValueId", 0L),
                    ImixConfigMasterID = GetFieldValue(sqlReader, "IMIXConfigurationMasterId", 0L),
                    ImixConfigMasterName = GetFieldValue(sqlReader, "ImixConfigMasterName", string.Empty),
                    ConfigurationValue = GetFieldValue(sqlReader, "ConfigurationValue", string.Empty),
                    SubStoreId = GetFieldValue(sqlReader, "SubStoreId", 0),
                    SubStoreName = GetFieldValue(sqlReader, "SubStoreName", string.Empty),
                    LocationId = GetFieldValue(sqlReader, "LocationId", 0),
                    LocationName = GetFieldValue(sqlReader, "LocationName", string.Empty),
                };
                lstRfidInfo.Add(rfidInfo);
            }
            return lstRfidInfo;
        }

        public int ClearPrinterQueue(int productID, int substoreID)
        {
            DBParameters.Clear();
            AddParameter("@ParamProductID", productID);
            AddParameter("@ParamSubstoreID", substoreID);
            return (int)ExecuteNonQuery("UPD_PrinterQueue");
        }

        public List<OpeningCloseSettinginfo> GetOpeningClosingDeletedData(int SubStoreId)
        {
            DBParameters.Clear();
            AddParameter("@SubStoreID", SubStoreId);
            IDataReader sqlReader = ExecuteReader("Usp_GetOpeningClosingDeletedData");
            List<OpeningCloseSettinginfo> lstrfidInfo = MapOpeningCloseSetting(sqlReader);
            sqlReader.Close();
            return lstrfidInfo;
        }

        private List<OpeningCloseSettinginfo> MapOpeningCloseSetting(IDataReader sqlReader)
        {
            List<OpeningCloseSettinginfo> lstRfidInfo = new List<OpeningCloseSettinginfo>();
            while (sqlReader.Read())
            {
                OpeningCloseSettinginfo rfidInfo = new OpeningCloseSettinginfo()
                {
                    Type = GetFieldValue(sqlReader, "Type", string.Empty),
                    Openingclosingdate = GetFieldValue(sqlReader, "Openingclosingdate", string.Empty),
                    DeletedOn = GetFieldValue(sqlReader, "DeletedOn", string.Empty),
                    Machine = GetFieldValue(sqlReader, "Machine", string.Empty),
                    UserName = GetFieldValue(sqlReader, "UserName", string.Empty),
                    FormID = GetFieldValue(sqlReader, "FormID", 0),
                    FormName = GetFieldValue(sqlReader, "FormName", string.Empty),
                    VisibilitySett = GetFieldValue(sqlReader, "VisibilitySett", string.Empty),
                    SubStoreName = GetFieldValue(sqlReader, "DG_SubStore_Name", string.Empty),


                };
                lstRfidInfo.Add(rfidInfo);
            }
            return lstRfidInfo;
        }
        public string OpeningClosingDelete(OpeningClosingDeleteinfo objOPinfo)
        {

            DBParameters.Clear();
            AddParameter("@TypeID", objOPinfo.TypeID);
            AddParameter("@UserName", objOPinfo.UserName);
            AddParameter("@MachineID", objOPinfo.MachineID);
            AddParameter("@Reason", objOPinfo.Reason);
            AddParameter("@FormID", objOPinfo.FormID);
            AddParameter("@SubStoreID",objOPinfo.SubStoreID);
            string Ret = Convert.ToString(ExecuteScalar("USP_OpeningClosingDelete"));
            return Ret;
        }

        public List<string> GetStoreAzureConfiguration(string storageName)
        {
            DBParameters.Clear();
            AddParameter("@storageName", storageName);

            IDataReader sqlReader = ExecuteReader("USP_SEL_StoreAzureConfiguration");
            List<string> StoreConfList = MapStoreConfiguration(sqlReader);
            sqlReader.Close();
            return StoreConfList;
        }

        private List<string> MapStoreConfiguration(IDataReader sqlReader)
        {
            List<string> SubStoreDataList = new List<string>();
            while (sqlReader.Read())
            {
                SubStoreDataList.Add(GetFieldValue(sqlReader, "StorageAccountName", string.Empty));
                SubStoreDataList.Add(GetFieldValue(sqlReader, "LifeCyclePolicy", string.Empty));
                SubStoreDataList.Add(GetFieldValue(sqlReader, "SubscribedToEventGrid", string.Empty));
                SubStoreDataList.Add(GetFieldValue(sqlReader, "DB_Variable_connectionstring", string.Empty));
                
            }
            return SubStoreDataList;
        }

    }
}
