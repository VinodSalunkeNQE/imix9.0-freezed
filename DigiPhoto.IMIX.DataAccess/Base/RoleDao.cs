﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DigiPhoto.IMIX.Model;
using System.Data;

namespace DigiPhoto.IMIX.DataAccess
{
    public class RoleDao : BaseDataAccess
    {
        #region Constructor
        public RoleDao(BaseDataAccess baseaccess)
            : base(baseaccess)
        { }
        public RoleDao()
        { }
        #endregion

        public List<RoleInfo> SelectRole(int RoleId, string RoleName)
        {
            DBParameters.Clear();
            AddParameter("@ParamRoleId", SetNullIntegerValue(RoleId));
            AddParameter("@ParamRoleName", SetNullStringValue(RoleName));

            IDataReader sqlReader = ExecuteReader(DAOConstant.USP_SEL_ROLE);
            List<RoleInfo> User_RolesList = MapDG_User_RolesBase(sqlReader);
            sqlReader.Close();
            return User_RolesList;
        }
        private List<RoleInfo> MapDG_User_RolesBase(IDataReader sqlReader)
        {
            List<RoleInfo> User_RolesList = new List<RoleInfo>();
            while (sqlReader.Read())
            {
                RoleInfo User_Roles = new RoleInfo()
                {
                    DG_User_Roles_pkey = GetFieldValue(sqlReader, "DG_User_Roles_pkey", 0),
                    DG_User_Role = GetFieldValue(sqlReader, "DG_User_Role", string.Empty),
                    SyncCode = GetFieldValue(sqlReader, "SyncCode", string.Empty),
                    IsSynced = GetFieldValue(sqlReader, "IsSynced", false),

                };

                User_RolesList.Add(User_Roles);
            }
            return User_RolesList;
        }

        public int INS_RolePermission(int RoleId, bool IsSynced, string UserRole, string SyncCode)
        {
            DBParameters.Clear();
            AddParameter("@ParamRoleId", RoleId);
            AddParameter("@ParamIsSynced", IsSynced);
            AddParameter("@ParamDG_User_Role", UserRole);
            AddParameter("@ParamSyncCode", SyncCode);
            ExecuteNonQuery(DAOConstant.USP_INS_ROLE);
            int objectId = (int)GetOutParameterValue("@ParamRoleId");
            return objectId;
        }



        public List<RoleInfo> Get(int RolesId)
        {
            DBParameters.Clear();
            AddParameter("@ParamRolesId", RolesId);
            IDataReader sqlReader = ExecuteReader(DAOConstant.USP_GET_USERROLESBYID);
            List<RoleInfo> User_RolesList = MapUserRolesById(sqlReader);
            sqlReader.Close();
            return User_RolesList;
        }
        private List<RoleInfo> MapUserRolesById(IDataReader sqlReader)
        {
            List<RoleInfo> User_RolesList = new List<RoleInfo>();
            while (sqlReader.Read())
            {
                RoleInfo User_Roles = new RoleInfo()
                {
                    DG_User_Role = GetFieldValue(sqlReader, "DG_User_Role", string.Empty),
                };
                User_RolesList.Add(User_Roles);
            }
            return User_RolesList;
        }





        public int UPDANDINS_User_Roles(int DG_User_Roles_pkey, bool IsSynced, string DG_User_Role, string SyncCode, int ParentRoleId)
        {
            DBParameters.Clear();
            AddParameter("@ParamDG_User_Roles_pkey", DG_User_Roles_pkey);
            AddParameter("@ParamIsSynced", IsSynced);
            AddParameter("@ParamDG_User_Role", DG_User_Role);
            AddParameter("@ParamSyncCode", SyncCode);
            AddParameter("@ParamDG_User_Parent", ParentRoleId);
            // ExecuteNonQuery(DAOConstant.USP_UPDANDINS_USER_ROLES);
            int ReturnVal = Convert.ToInt32(ExecuteScalar(DAOConstant.USP_UPDANDINS_USER_ROLES));
            return ReturnVal;
        }



        public bool DeleteRoleData(int RoleId)
        {
            DBParameters.Clear();
            AddParameter("@ParamRoleId", RoleId);
            ExecuteNonQuery(DAOConstant.USP_DEL_USERSROLEBYID);
            return true;
        }

    }



}
