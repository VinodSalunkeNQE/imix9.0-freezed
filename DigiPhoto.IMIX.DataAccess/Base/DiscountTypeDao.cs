﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DigiPhoto.IMIX.Model;
using System.Data;

namespace DigiPhoto.IMIX.DataAccess
{
    public class DiscountTypeDao : BaseDataAccess
    {
        #region Constructor
        public DiscountTypeDao(BaseDataAccess baseaccess)
            : base(baseaccess)
        { }
        public DiscountTypeDao()
        { }
        #endregion

        public int Add(DiscountTypeInfo objectInfo)
        {
            DBParameters.Clear();
            AddParameter("@ParamDG_Orders_DiscountType_Name", objectInfo.DG_Orders_DiscountType_Name);
            AddParameter("@ParamDG_Orders_DiscountType_Desc", objectInfo.DG_Orders_DiscountType_Desc);
            AddParameter("@ParamDG_Orders_DiscountType_Active", objectInfo.DG_Orders_DiscountType_Active);
            AddParameter("@ParamDG_Orders_DiscountType_Secure", objectInfo.DG_Orders_DiscountType_Secure);
            AddParameter("@ParamDG_Orders_DiscountType_ItemLevel", objectInfo.DG_Orders_DiscountType_ItemLevel);
            AddParameter("@ParamDG_Orders_DiscountType_AsPercentage", objectInfo.DG_Orders_DiscountType_AsPercentage);
            AddParameter("@ParamDG_Orders_DiscountType_Code", objectInfo.DG_Orders_DiscountType_Code);
            AddParameter("@ParamSyncCode", objectInfo.SyncCode);
            AddParameter("@ParamIsSynced", objectInfo.IsSynced);
            AddParameter("@ParamDiscountTypeId", objectInfo.DG_Orders_DiscountType_Pkey, ParameterDirection.Output);

            ExecuteNonQuery(DAOConstant.USP_INS_DISCOUNTTYPE);
            int objectId = (int)GetOutParameterValue("@ParamDiscountTypeId");
            return objectId;
        }
        public bool Update(DiscountTypeInfo objectInfo)
        {
            DBParameters.Clear();
            AddParameter("@ParamDG_Orders_DiscountType_Pkey", objectInfo.DG_Orders_DiscountType_Pkey);
            AddParameter("@ParamDG_Orders_DiscountType_Name", objectInfo.DG_Orders_DiscountType_Name);
            AddParameter("@ParamDG_Orders_DiscountType_Desc", objectInfo.DG_Orders_DiscountType_Desc);
            AddParameter("@ParamDG_Orders_DiscountType_Active", objectInfo.DG_Orders_DiscountType_Active);
            AddParameter("@ParamDG_Orders_DiscountType_Secure", objectInfo.DG_Orders_DiscountType_Secure);
            AddParameter("@ParamDG_Orders_DiscountType_ItemLevel", objectInfo.DG_Orders_DiscountType_ItemLevel);
            AddParameter("@ParamDG_Orders_DiscountType_AsPercentage", objectInfo.DG_Orders_DiscountType_AsPercentage);
            AddParameter("@ParamDG_Orders_DiscountType_Code", objectInfo.DG_Orders_DiscountType_Code);
            AddParameter("@ParamIsSynced", objectInfo.IsSynced);
            ExecuteNonQuery(DAOConstant.USP_UPD_DISCOUNTTYPE);
            return true;
        }
        public bool Delete(int objectvalueId)
        {
            DBParameters.Clear();
            AddParameter("@ParamDG_Orders_DiscountType_Pkey", objectvalueId);
            ExecuteNonQuery(DAOConstant.USP_DEL_DISCOUNTTYPE);
            return true;
        }
        public DiscountTypeInfo Get(int? discountTypeId, string discountTypeName)
        {
            DBParameters.Clear();
            AddParameter("@ParamDiscountTypeId", SetNullIntegerValue(discountTypeId));
            AddParameter("@ParamDiscountTypeName", SetNullStringValue(discountTypeName));

            List<DiscountTypeInfo> objectList;
            using (IDataReader sqlReader = ExecuteReader(DAOConstant.USP_SEL_DISCOUNTTYPE))
            {
                objectList = MapObject(sqlReader);
            }
            return objectList.FirstOrDefault();
        }
        public List<DiscountTypeInfo> Select(int? discountTypeId, string discountTypeName)
        {
            DBParameters.Clear();
            AddParameter("@ParamDiscountTypeId", SetNullIntegerValue(discountTypeId));
            AddParameter("@ParamDiscountTypeName", SetNullStringValue(discountTypeName));

            List<DiscountTypeInfo> objectList;
            using (IDataReader sqlReader = ExecuteReader(DAOConstant.USP_SEL_DISCOUNTTYPE))
            {
                objectList = MapObject(sqlReader);
            }
            return objectList;
        }
        private List<DiscountTypeInfo> MapObject(IDataReader sqlReader)
        {
            List<DiscountTypeInfo> objectList = new List<DiscountTypeInfo>();
            while (sqlReader.Read())
            {
                DiscountTypeInfo objectValue = new DiscountTypeInfo
                {
                    DG_Orders_DiscountType_Pkey = GetFieldValue(sqlReader, "DG_Orders_DiscountType_Pkey", 0),
                    DG_Orders_DiscountType_Name = GetFieldValue(sqlReader, "DG_Orders_DiscountType_Name", string.Empty),
                    DG_Orders_DiscountType_Desc = GetFieldValue(sqlReader, "DG_Orders_DiscountType_Desc", string.Empty),
                    DG_Orders_DiscountType_Active = GetFieldValue(sqlReader, "DG_Orders_DiscountType_Active", false),
                    DG_Orders_DiscountType_Secure = GetFieldValue(sqlReader, "DG_Orders_DiscountType_Secure", false),
                    DG_Orders_DiscountType_ItemLevel = GetFieldValue(sqlReader, "DG_Orders_DiscountType_ItemLevel", false),
                    DG_Orders_DiscountType_AsPercentage = GetFieldValue(sqlReader, "DG_Orders_DiscountType_AsPercentage", false),
                    DG_Orders_DiscountType_Code = GetFieldValue(sqlReader, "DG_Orders_DiscountType_Code", string.Empty),
                    SyncCode = GetFieldValue(sqlReader, "SyncCode", string.Empty),
                    IsSynced = GetFieldValue(sqlReader, "IsSynced", false),
                    IsEvoucher = GetFieldValue(sqlReader, "IsEvoucher", false),//////Changed by latika fro Evoucher



                };

                objectList.Add(objectValue);
            }
            return objectList;
        }
    }
}
