﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DigiPhoto.IMIX.Model;
using System.Data;

namespace DigiPhoto.IMIX.DataAccess
{
    public class CashBoxDao : BaseDataAccess
    {
        #region Constructor
        public CashBoxDao(BaseDataAccess baseaccess)
            : base(baseaccess)
        { }
        public CashBoxDao()
        { }
        #endregion
        public int Add(CashBoxInfo objectInfo)
        {
            DBParameters.Clear();
            AddParameter("@ParamReason", objectInfo.Reason);
            AddParameter("@ParamUserId", objectInfo.UserId);
            AddParameter("@ParamCreatedDate", objectInfo.CreatedDate);
            AddParameter("@ParamCashBoxId", objectInfo.Id, ParameterDirection.Output);

            ExecuteNonQuery(DAOConstant.USP_INS_CASHBOX);
            int objectId = (int)GetOutParameterValue("@ParamCashBoxId");
            return objectId;
        }
     
    }
}
