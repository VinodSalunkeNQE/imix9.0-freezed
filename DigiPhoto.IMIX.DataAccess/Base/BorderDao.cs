﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DigiPhoto.IMIX.Model;
using System.Data;

namespace DigiPhoto.IMIX.DataAccess
{
    public class BorderDao : BaseDataAccess
    {
        #region Constructor
        public BorderDao(BaseDataAccess baseaccess)
            : base(baseaccess)
        { }
        public BorderDao()
        { }
        #endregion
        public int Add(BorderInfo objectInfo)
        {
            DBParameters.Clear();
            AddParameter("@ParamDG_Border", objectInfo.DG_Border);
            AddParameter("@ParamDG_ProductTypeID", objectInfo.DG_ProductTypeID);
            AddParameter("@ParamDG_IsActive", objectInfo.DG_IsActive);
            AddParameter("@ParamSyncCode", objectInfo.SyncCode);
            AddParameter("@ParamIsSynced", objectInfo.IsSynced);
            AddParameter("@ParamCreatedBy", objectInfo.CreatedBy);
            AddParameter("@ParamBorderId", objectInfo.DG_Borders_pkey, ParameterDirection.Output);
            ExecuteNonQuery(DAOConstant.USP_INS_BORDERS);
            int objectId = (int)GetOutParameterValue("@ParamBorderId");
            return objectId;
        }
        public bool Update(BorderInfo objectInfo)
        {
            DBParameters.Clear();
            AddParameter("@ParamDG_Borders_pkey", objectInfo.DG_Borders_pkey);
            AddParameter("@ParamDG_Border", objectInfo.DG_Border);
            AddParameter("@ParamDG_ProductTypeID", objectInfo.DG_ProductTypeID);
            AddParameter("@ParamDG_IsActive", objectInfo.DG_IsActive);
            AddParameter("@ParamModifiedBy", objectInfo.ModifiedBy);
            AddParameter("@ParamIsSynced", objectInfo.IsSynced);
            ExecuteNonQuery(DAOConstant.USP_UPD_BORDERS);
            return true;
        }
        public bool Delete(int objectvalueId)
        {
            DBParameters.Clear();
            AddParameter("@ParamDG_Borders_pkey", objectvalueId);
            ExecuteNonQuery(DAOConstant.USP_DEL_BORDER);
            return true;
        }
        public BorderInfo Get(int? borderId)
        {
            DBParameters.Clear();
            AddParameter("@ParamBorderId", SetNullIntegerValue(borderId));

            List<BorderInfo> objectList;
            using (IDataReader sqlReader = ExecuteReader(DAOConstant.USP_SEL_BORDERS))
            {
                objectList = MapObject(sqlReader);
            }
            return objectList.FirstOrDefault();
        }
        public List<BorderInfo> Select(int? borderId)
        {
            DBParameters.Clear();
            AddParameter("@ParamBorderId", SetNullIntegerValue(borderId));

            List<BorderInfo> objectList;
            using (IDataReader sqlReader = ExecuteReader(DAOConstant.USP_SEL_BORDERS))
            {
                objectList = MapObject(sqlReader);
            }
            return objectList;
        }
        public List<BorderInfo> SelectBorder()
        {
            DBParameters.Clear();
            this.OpenConnection();
            AddParameter("@ParamBorderId", SetNullIntegerValue(null));
            List<BorderInfo> objectList;
            using (IDataReader sqlReader = ExecuteReader(DAOConstant.USP_SEL_BORDERS))
            {
                objectList = MapObject(sqlReader);
            }
            return objectList;
        }
        private List<BorderInfo> MapObject(IDataReader sqlReader)
        {
            List<BorderInfo> objectList = new List<BorderInfo>();
            while (sqlReader.Read())
            {
                BorderInfo objectValue = new BorderInfo
                {
                    DG_Borders_pkey = GetFieldValue(sqlReader, "DG_Borders_pkey", 0),
                    DG_Border = GetFieldValue(sqlReader, "DG_Border", string.Empty),
                    DG_ProductTypeID = GetFieldValue(sqlReader, "DG_ProductTypeID", 0),
                    DG_IsActive = GetFieldValue(sqlReader, "DG_IsActive", false),
                    SyncCode = GetFieldValue(sqlReader, "SyncCode", string.Empty),
                    IsSynced = GetFieldValue(sqlReader, "IsSynced", false),
                    DG_Orders_ProductType_Name = GetFieldValue(sqlReader, "DG_Orders_ProductType_Name", string.Empty),
                    CreatedBy=GetFieldValue(sqlReader,"CreatedBy",0),
                    ModifiedBy = GetFieldValue(sqlReader, "ModifiedBy", 0),
                    CreatedDate = GetFieldValue(sqlReader, "CreatedDate", DateTime.MinValue),
                    ModifiedDate=GetFieldValue(sqlReader,"ModifiedDate",DateTime.MinValue)
                };

                objectList.Add(objectValue);
            }
            return objectList;
        }

        public BorderInfo SelectBorderProductwise(int? ProductTypeID)
        {
            DBParameters.Clear();
            AddParameter("@ParamDG_ProductTypeID", SetNullIntegerValue(ProductTypeID));
            List<BorderInfo> objectList;
            using (IDataReader sqlReader = ExecuteReader(DAOConstant.USP_GET_BORDERPRODUCTWISE))
            {
                objectList = MapBorderProductwise(sqlReader);
            }
            return objectList.FirstOrDefault();
        }
        private List<BorderInfo> MapBorderProductwise(IDataReader sqlReader)
        {
            List<BorderInfo> objectList = new List<BorderInfo>();
            while (sqlReader.Read())
            {
                BorderInfo objectValue = new BorderInfo
                {
                    DG_Borders_pkey = GetFieldValue(sqlReader, "DG_Borders_pkey", 0),
                    DG_Border = GetFieldValue(sqlReader, "DG_Border", string.Empty),
                    DG_ProductTypeID = GetFieldValue(sqlReader, "DG_ProductTypeID", 0),
                    DG_IsActive = GetFieldValue(sqlReader, "DG_IsActive", false),
                    SyncCode = GetFieldValue(sqlReader, "SyncCode", string.Empty),
                    IsSynced = GetFieldValue(sqlReader, "IsSynced", false),
                    CreatedBy=GetFieldValue(sqlReader,"CreatedBy",0),
                    ModifiedBy = GetFieldValue(sqlReader, "ModifiedBy", 0),
                    CreatedDate = GetFieldValue(sqlReader, "CreatedDate", DateTime.MinValue),
                    ModifiedDate=GetFieldValue(sqlReader,"ModifiedDate",DateTime.MinValue),
                    ProductTypeInfo = new ProductTypeInfo
                    {
                        DG_Orders_ProductType_Name = GetFieldValue(sqlReader, "DG_Orders_ProductType_Name", string.Empty),
                    }
                };
                objectList.Add(objectValue);
            }
            return objectList;
        }

        public List<BorderInfo> SelectBorderDetails(string OrdersProductTypeName)
        {
            DBParameters.Clear();
            AddParameter("@ParamOrdersProductTypeName", OrdersProductTypeName);
            IDataReader sqlReader = ExecuteReader(DAOConstant.USP_SEL_BORDERDETAILS);
            List<BorderInfo> BordersList = MapBorderInfo(sqlReader);
            sqlReader.Close();
            return BordersList;
        }
        private List<BorderInfo> MapBorderInfo(IDataReader sqlReader)
        {
            List<BorderInfo> BordersList = new List<BorderInfo>();
            while (sqlReader.Read())
            {
                BorderInfo Borders = new BorderInfo()
                {
                    DG_Borders_pkey = GetFieldValue(sqlReader, "DG_Borders_pkey", 0),
                    DG_Border = GetFieldValue(sqlReader, "DG_Border", string.Empty),
                    DG_ProductTypeID = GetFieldValue(sqlReader, "DG_ProductTypeID", 0),
                    DG_IsActive = GetFieldValue(sqlReader, "DG_IsActive", false),
                    DG_Orders_ProductType_pkey = GetFieldValue(sqlReader, "DG_Orders_ProductType_pkey", 0),
                    DG_Orders_ProductType_Name = GetFieldValue(sqlReader, "DG_Orders_ProductType_Name", string.Empty),
                    CreatedBy = GetFieldValue(sqlReader, "CreatedBy", 0),
                    ModifiedBy = GetFieldValue(sqlReader, "ModifiedBy", 0),
                    CreatedDate = GetFieldValue(sqlReader, "CreatedDate", DateTime.MinValue),
                    ModifiedDate = GetFieldValue(sqlReader, "ModifiedDate", DateTime.MinValue)
                };
                BordersList.Add(Borders);
            }
            return BordersList;
        }

        public List<VideoOverlay> SelectVideoOverlay(int? id)
        {
            DBParameters.Clear();
            List<VideoOverlay> objectList;
            DBParameters.Clear();
            AddParameter("@VideoOverlayId", SetNullIntegerValue(id));
            using (IDataReader sqlReader = ExecuteReader("GetVideoOverlay"))
            {
                objectList = MapObjects(sqlReader);
            }
            return objectList;
        }

        private List<VideoOverlay> MapObjects(IDataReader sqlReader)
        {
            List<VideoOverlay> objectList = new List<VideoOverlay>();
            while (sqlReader.Read())
            {
                VideoOverlay objectValue = new VideoOverlay
                {
                    VideoOverlayId = GetFieldValue(sqlReader, "VideoOverlayId", 0L),
                    Name = GetFieldValue(sqlReader, "Name", string.Empty),
                    DisplayName = GetFieldValue(sqlReader, "DisplayName", string.Empty),
                    VideoLength = GetFieldValue(sqlReader, "VideoLength", 0L),
                    IsActive = GetFieldValue(sqlReader, "IsActive", false),
                    MediaType = GetFieldValue(sqlReader, "MediaType", 0),
                    Description = GetFieldValue(sqlReader, "Description", string.Empty),
                    CreatedBy = GetFieldValue(sqlReader, "CreatedBy", 0),
                    ModifiedBy = GetFieldValue(sqlReader, "ModifiedBy", 0),
                    CreatedOn = GetFieldValue(sqlReader, "CreatedOn", DateTime.MinValue),
                    ModifiedOn = GetFieldValue(sqlReader, "ModifiedOn", DateTime.MinValue)
                };

                objectList.Add(objectValue);
            }
            return objectList;
        }
    }
}
