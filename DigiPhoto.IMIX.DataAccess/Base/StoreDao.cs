﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DigiPhoto.IMIX.Model;
using System.Data;

namespace DigiPhoto.IMIX.DataAccess
{
    public class StoreDao : BaseDataAccess
    {
        #region Constructor
        public StoreDao(BaseDataAccess baseaccess)
            : base(baseaccess)
        { }
        public StoreDao()
        { }
        #endregion



        /// <summary>
        /// Top 1 Store
        /// </summary>
        /// <returns></returns>
        public StoreInfo GetStore()
        {
            DBParameters.Clear();

            IDataReader sqlReader = ExecuteReader(DAOConstant.USP_GET_STORE);
            List<StoreInfo> StoreList = MapGetStoreInfo(sqlReader);
            sqlReader.Close();
            return StoreList.FirstOrDefault();
        }
        #region Get online status- Ashirwad
        public bool GetSyncIsOnline(int subStoreID)
        {
            bool value = false;
            DBParameters.Clear();
            
            DBParameters.Clear();
            AddParameter("@SubStoreID", subStoreID);
            IDataReader sqlReader = ExecuteReader("GetSyncIsOnline");
            while (sqlReader.Read())
            {
                value =Convert.ToBoolean(GetFieldValue(sqlReader, "ConfigurationValue", string.Empty));
            }
                //List<StoreInfo> StoreList = MapGetStoreInfo(sqlReader);
            sqlReader.Close();
            return value;
        }
        #endregion
        private List<StoreInfo> MapGetStoreInfo(IDataReader sqlReader)
        {
            List<StoreInfo> StoreList = new List<StoreInfo>();
            while (sqlReader.Read())
            {
                StoreInfo Store = new StoreInfo()
                {
                    DG_Store_pkey = GetFieldValue(sqlReader, "DG_Store_pkey", 0),
                    DG_Store_Name = GetFieldValue(sqlReader, "DG_Store_Name", string.Empty),
                    Country = GetFieldValue(sqlReader, "Country", string.Empty),
                    DG_CentralServerIP = GetFieldValue(sqlReader, "DG_CentralServerIP", string.Empty),
                    DG_StoreCode = GetFieldValue(sqlReader, "DG_StoreCode", string.Empty),
                    DG_CenetralServerUName = GetFieldValue(sqlReader, "DG_CenetralServerUName", string.Empty),
                    DG_CenetralServerPassword = GetFieldValue(sqlReader, "DG_CenetralServerPassword", string.Empty),
                    DG_PreferredTimeToSyncFrom = GetFieldValue(sqlReader, "DG_PreferredTimeToSyncFrom", 0.0M),
                    DG_PreferredTimeToSyncTo = GetFieldValue(sqlReader, "DG_PreferredTimeToSyncTo", 0.0M),
                    DG_QRCodeWebUrl = GetFieldValue(sqlReader, "DG_QRCodeWebUrl", string.Empty),
                    CountryCode = GetFieldValue(sqlReader, "CountryCode", string.Empty),
                    StoreCode = GetFieldValue(sqlReader, "StoreCode", string.Empty),
                    RunApplicationsSubStoreLevel = GetFieldValue(sqlReader, "RunApplicationsSubStoreLevel", false),
                    IsOnline = GetFieldValue(sqlReader, "ISOnline", false)
                    
                    
                };
                StoreList.Add(Store);
            }
            return StoreList;
        }

        public SubStoresInfo GetSubstoreData(int SubStore_pkey, bool? SubStore_IsActive)
        {
            DBParameters.Clear();
            AddParameter("@ParamSubStore_pkey", SubStore_pkey);
            AddParameter("@ParamSubStore_IsActive", SetNullBoolValue(SubStore_IsActive));
            IDataReader sqlReader = ExecuteReader(DAOConstant.USP_GET_SUBSTOREDATA);
            List<SubStoresInfo> SubStoresList = MapSubStoresBase(sqlReader);
            sqlReader.Close();
            return SubStoresList.FirstOrDefault();
        }
        private List<SubStoresInfo> MapSubStoresBase(IDataReader sqlReader)
        {
            List<SubStoresInfo> SubStoresList = new List<SubStoresInfo>();
            while (sqlReader.Read())
            {
                SubStoresInfo SubStores = new SubStoresInfo()
                {
                    DG_SubStore_pkey = GetFieldValue(sqlReader, "DG_SubStore_pkey", 0),
                    DG_SubStore_Name = GetFieldValue(sqlReader, "DG_SubStore_Name", string.Empty),
                    DG_SubStore_Description = GetFieldValue(sqlReader, "DG_SubStore_Description", string.Empty),
                    DG_SubStore_IsActive = GetFieldValue(sqlReader, "DG_SubStore_IsActive", false),
                    SyncCode = GetFieldValue(sqlReader, "SyncCode", string.Empty),
                    IsSynced = GetFieldValue(sqlReader, "IsSynced", false),
                };

                SubStoresList.Add(SubStores);
            }
            return SubStoresList;
        }

        public List<StoreInfo> Select()
        {
            this.OpenConnection();
            IDataReader sqlReader = ExecuteReader(DAOConstant.USP_SEL_STORE);
            List<StoreInfo> StoreList = MapStoreInfo(sqlReader);
            sqlReader.Close();
            return StoreList;
        }
        public List<StoreInfo> SelectStore()
        {
           
            IDataReader sqlReader = ExecuteReader(DAOConstant.USP_SEL_STORE);
            List<StoreInfo> StoreList = MapStoreInfo(sqlReader);
            sqlReader.Close();
            return StoreList;
        }
        private List<StoreInfo> MapStoreInfo(IDataReader sqlReader)
        {
            List<StoreInfo> StoreList = new List<StoreInfo>();
            while (sqlReader.Read())
            {
                StoreInfo Store = new StoreInfo()
                {
                    DG_Store_pkey = GetFieldValue(sqlReader, "DG_Store_pkey", 0),
                    DG_Store_Name = GetFieldValue(sqlReader, "DG_Store_Name", string.Empty),
                    Country = GetFieldValue(sqlReader, "Country", string.Empty),
                    DG_CentralServerIP = GetFieldValue(sqlReader, "DG_CentralServerIP", string.Empty),
                    DG_StoreCode = GetFieldValue(sqlReader, "DG_StoreCode", string.Empty),
                    DG_CenetralServerUName = GetFieldValue(sqlReader, "DG_CenetralServerUName", string.Empty),
                    DG_CenetralServerPassword = GetFieldValue(sqlReader, "DG_CenetralServerPassword", string.Empty),
                    DG_PreferredTimeToSyncFrom = GetFieldValue(sqlReader, "DG_PreferredTimeToSyncFrom", 0.0M),
                    DG_PreferredTimeToSyncTo = GetFieldValue(sqlReader, "DG_PreferredTimeToSyncTo", 0.0M),
                    DG_QRCodeWebUrl = GetFieldValue(sqlReader, "DG_QRCodeWebUrl", string.Empty),
                    CountryCode = GetFieldValue(sqlReader, "CountryCode", string.Empty),
                    StoreCode = GetFieldValue(sqlReader, "StoreCode", string.Empty),

                };

                StoreList.Add(Store);
            }
            return StoreList;
        }

        public StoreInfo GETQRCodeWebUrl()
        {
            IDataReader sqlReader = ExecuteReader(DAOConstant.USP_GET_QRCODEWEBURL);
            List<StoreInfo> CountryList = MapQRCodeWebUrlInfo(sqlReader);
            sqlReader.Close();
            return CountryList.FirstOrDefault();
        }
        private List<StoreInfo> MapQRCodeWebUrlInfo(IDataReader sqlReader)
        {
            List<StoreInfo> StoreList = new List<StoreInfo>();
            while (sqlReader.Read())
            {
                StoreInfo Store = new StoreInfo()
                {
                    DG_Store_pkey = GetFieldValue(sqlReader, "DG_Store_pkey", 0),
                    DG_Store_Name = GetFieldValue(sqlReader, "DG_Store_Name", string.Empty),
                    Country = GetFieldValue(sqlReader, "Country", string.Empty),
                    DG_CentralServerIP = GetFieldValue(sqlReader, "DG_CentralServerIP", string.Empty),
                    DG_StoreCode = GetFieldValue(sqlReader, "DG_StoreCode", string.Empty),
                    DG_CenetralServerUName = GetFieldValue(sqlReader, "DG_CenetralServerUName", string.Empty),
                    DG_CenetralServerPassword = GetFieldValue(sqlReader, "DG_CenetralServerPassword", string.Empty),
                    DG_PreferredTimeToSyncFrom = GetFieldValue(sqlReader, "DG_PreferredTimeToSyncFrom", 0.0M),
                    DG_PreferredTimeToSyncTo = GetFieldValue(sqlReader, "DG_PreferredTimeToSyncTo", 0.0M),
                    DG_QRCodeWebUrl = GetFieldValue(sqlReader, "DG_QRCodeWebUrl", string.Empty),
                    CountryCode = GetFieldValue(sqlReader, "CountryCode", string.Empty),
                    StoreCode = GetFieldValue(sqlReader, "StoreCode", string.Empty),
                };
                StoreList.Add(Store);
            }
            return StoreList;
        }

        public List<PhotoGraphersInfo> SelectPhotoGraphersList(int StoreId)
        {
            DBParameters.Clear();
            AddParameter("@ParamStoreId", StoreId);

            IDataReader sqlReader = ExecuteReader(DAOConstant.USP_SEL_PHOTOGRAPHERSLIST);
                List<PhotoGraphersInfo> UserDetailsList = MapUserDetailsBase(sqlReader);
            sqlReader.Close();
            return UserDetailsList;
        }
        private List<PhotoGraphersInfo> MapUserDetailsBase(IDataReader sqlReader)
        {
            List<PhotoGraphersInfo> UserDetailsList = new List<PhotoGraphersInfo>();
            while (sqlReader.Read())
            {
                PhotoGraphersInfo UserDetails = new PhotoGraphersInfo()
                {
                    DG_User_pkey = GetFieldValue(sqlReader, "DG_User_pkey", 0),
                    DG_User_Role = GetFieldValue(sqlReader, "DG_User_Role", string.Empty),
                    DG_User_First_Name = GetFieldValue(sqlReader, "DG_User_First_Name", string.Empty),
                    DG_User_Last_Name = GetFieldValue(sqlReader, "DG_User_Last_Name", string.Empty),
                    DG_User_Password = GetFieldValue(sqlReader, "DG_User_Password", string.Empty),
                    DG_User_Name = GetFieldValue(sqlReader, "DG_User_Name", string.Empty),
                    DG_User_Roles_Id = GetFieldValue(sqlReader, "DG_User_Roles_Id", 0),
                    DG_Location_Name = GetFieldValue(sqlReader, "DG_Location_Name", string.Empty),
                    DG_Store_ID = GetFieldValue(sqlReader, "DG_Store_ID", 0),
                    DG_Location_pkey = GetFieldValue(sqlReader, "DG_Location_pkey", 0),
                    DG_User_Status = GetFieldValue(sqlReader, "DG_User_Status", false),
                    DG_User_PhoneNo = GetFieldValue(sqlReader, "DG_User_PhoneNo", string.Empty),
                    UserName = GetFieldValue(sqlReader, "UserName", string.Empty),
                    StatusName = GetFieldValue(sqlReader, "StatusName", string.Empty),
                    DG_User_Email = GetFieldValue(sqlReader, "DG_User_Email", string.Empty),
                    DG_Store_Name = GetFieldValue(sqlReader, "DG_Store_Name", string.Empty),
                    CountryCode = GetFieldValue(sqlReader, "CountryCode", string.Empty),
                    StoreCode = GetFieldValue(sqlReader, "StoreCode", string.Empty),
                    DG_Substore_ID = GetFieldValue(sqlReader, "DG_Substore_ID",0),

                };

                UserDetailsList.Add(UserDetails);
            }
            return UserDetailsList;
        }



        public Dictionary<string, string> SelStoreDir()
        {
            IDataReader sqlReader = ExecuteReader(DAOConstant.USP_GET_STORE);
            Dictionary<string, string> SubStoresList = MapStoresBassDir(sqlReader);
            sqlReader.Close();
            return SubStoresList;
        }
        private Dictionary<string, string> MapStoresBassDir(IDataReader sqlReader)
        {
            Dictionary<string, string> StoresList = new Dictionary<string, string>();
            StoresList.Add( "--Select--","0");
            while (sqlReader.Read())
            {
                StoresList.Add(GetFieldValue(sqlReader, "DG_Store_Name", string.Empty),GetFieldValue(sqlReader, "DG_Store_pkey", 0).ToString());
            }
            return StoresList;
        }
        public bool UpdateStoreQRCode(string QRCodeWebUrl)
        {
            DBParameters.Clear();
            AddParameter("@ParamDG_QRCodeWebUrl", QRCodeWebUrl);
            ExecuteNonQuery(DAOConstant.USP_UPD_STORE);
            return true;
        }
        /// <summary>
        ///  Store azure details for store
        /// </summary>
        /// <returns></returns>
        ///   bool isStorageAccountCreated = false; //This has to be done only once ,save this info to DB
        //bool isSubscribedToEventGrid = false; //This has to be done only once ,save this info to DB
        //bool isLifeCyclePolicySet
        public bool SaveStoreAzureDetails(string StorageAccountName, string LifeCyclePolicy, string SubscribedToEventGrid, string DB_Variable_connectionstring)
        {
            bool rtnVal = false;
            DBParameters.Clear();
            AddParameter("@StorageAccountName", StorageAccountName);
            AddParameter("@LifeCyclePolicy", LifeCyclePolicy);
            AddParameter("@SubscribedToEventGrid", SubscribedToEventGrid);
            AddParameter("@DB_Variable_connectionstring", DB_Variable_connectionstring);
            //AddParameter("@CreatedDate", LifeCyclePolicy);
            rtnVal = Convert.ToBoolean(ExecuteScalar("USP_SaveStoreAzureDetails"));
            return rtnVal;
        }

        // Start Changes // By Suraj
        /// <summary>
        ///  Store MetaDeta Upload Failure Details
        /// </summary>
        /// <param name="PhotoId"></param>
        /// <param name="PhotoJson"></param>
        /// <param name="WebPhotoJson"></param>
        /// <param name="CloudinaryAPIJson"></param>
        /// <param name="ImagesAssociationAPIJson"></param>
        /// <returns></returns>
        public bool SaveMetaDetaUploadFailureDetails(string PhotoId, string PhotoJson, string WebPhotoJson, string CloudinaryAPIJson, string ImagesAssociationAPIJson,string Venue , string Site)
        {
            bool rtnVal = false;
            DBParameters.Clear();
            AddParameter("@PhotoId", PhotoId);
            AddParameter("@PhotoAPIJson", PhotoJson);
            AddParameter("@WebPhotoAPIJson", WebPhotoJson);
            AddParameter("@CloudinaryAPIJson", CloudinaryAPIJson);
            AddParameter("@ImagesAssociationAPIJson", ImagesAssociationAPIJson);
            AddParameter("@Venue", Venue);
            AddParameter("@Site", Site);
            rtnVal = Convert.ToBoolean(ExecuteScalar("INSUploadFailureDetails"));
            return rtnVal;
        }

        public bool UpdateMetaDetaDGPhotos(int PhotoId,bool IsUploadedBob)
        {
            bool rtnVal = false;
            DBParameters.Clear();
            AddParameter("@PhotoId", PhotoId);
            AddParameter("@IsUploadedBob", IsUploadedBob);
            rtnVal = Convert.ToBoolean(ExecuteScalar("USPMetaDetaDGPhotos"));
            return rtnVal;
        }

        public bool UpdateMetaDetaDGEditPhotos(int PhotoId, bool Issynced)
        {
            bool rtnVal = false;
            DBParameters.Clear();
            AddParameter("@PhotoId", PhotoId);
            AddParameter("@Issynced", Issynced);
            rtnVal = Convert.ToBoolean(ExecuteScalar("USPMetaDetaDGEditPhotos"));
            return rtnVal;
        }

        public bool UpdateMetaDetaiMixImagesAssociation(int PhotoId, bool IsUploadedBob)
        {
            bool rtnVal = false;
            DBParameters.Clear();
            AddParameter("@PhotoId", PhotoId);
            AddParameter("@IsUploadedBob", IsUploadedBob);
            rtnVal = Convert.ToBoolean(ExecuteScalar("USPMetaDetaiMixImagesAssociation"));
            return rtnVal;
        }
        // End Changes.

        /////////////////created by latika to search by in product
        public List<SearchbyInfo> SelectSearchby()
        {

            IDataReader sqlReader = ExecuteReader(DAOConstant.usp_SEL_SearchBy);
            List<SearchbyInfo> StoreList = MapSearchbyInfo(sqlReader);
            sqlReader.Close();
            return StoreList;
        }
        private List<SearchbyInfo> MapSearchbyInfo(IDataReader sqlReader)
        {
            List<SearchbyInfo> StoreList = new List<SearchbyInfo>();
            while (sqlReader.Read())
            {
                SearchbyInfo Store = new SearchbyInfo()
                {
                    SearchByID = GetFieldValue(sqlReader, "SearchByID", string.Empty),
                    SearchBy = GetFieldValue(sqlReader, "SearchBy", string.Empty),


                };

                StoreList.Add(Store);
            }
            return StoreList;
        }
        ///////////////end
        ///

        ///////created by latika 
        ///
        public bool SaveSubstoreConfigVisbleInOrdStation(string SubStoreVal,int SubstoreID)
        {
            bool rtnVal = false;
            DBParameters.Clear();
            AddParameter("@SubstoreID", SubstoreID);
            AddParameter("@SubStoreVisible", SubStoreVal);
            rtnVal =Convert.ToBoolean(ExecuteScalar("SP_SaveSubstoreConfigVisbleInOrdStation"));
            return rtnVal;
        }
    //////

        

}
}
