﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DigiPhoto.IMIX.Model;
using System.Data;


namespace DigiPhoto.IMIX.DataAccess
{
    public class ActivityDao : BaseDataAccess
    {
        #region Constructor
        public ActivityDao(BaseDataAccess baseaccess)
            : base(baseaccess)
        { }
        public ActivityDao()
        { }
        #endregion

        public int AddActivity(ActivityInfo activityInfo)
        {
            AddParameter("@ParamDG_Acitivity_ActionType", activityInfo.DG_Acitivity_ActionType);
            AddParameter("@ParamDG_Acitivity_Date", activityInfo.DG_Acitivity_Date);
            AddParameter("@ParamDG_Acitivity_By", activityInfo.DG_Acitivity_By);
            AddParameter("@ParamDG_Acitivity_Descrption", activityInfo.DG_Acitivity_Descrption);
            AddParameter("@ParamDG_Reference_ID", activityInfo.DG_Reference_ID);
            AddParameter("@ParamSyncCode", activityInfo.SyncCode);
            AddParameter("@ParamIsSynced", activityInfo.IsSynced);

            AddParameter("@ParamAcitivityID", activityInfo.DG_Acitivity_Action_Pkey, ParameterDirection.Output);

            ExecuteNonQuery(DAOConstant.USP_INS_ACTIVITY);
            int objectId = (int)GetOutParameterValue("@ParamAcitivityID");
            return objectId;

        }
        public List<ActivityInfo> GetActivityReports()
        {
            DBParameters.Clear();

            IDataReader sqlReader = ExecuteReader(DAOConstant.USP_SEL_ACTIVITYREPORTS);
            List<ActivityInfo> vw_GetActivityReportsList = MapActivityInfo(sqlReader);
            sqlReader.Close();
            return vw_GetActivityReportsList;
        }
        private List<ActivityInfo> MapActivityInfo(IDataReader sqlReader)
        {
            List<ActivityInfo> GetActivityReportsList = new List<ActivityInfo>();
            while (sqlReader.Read())
            {

                ActivityInfo GetActivityReports = new ActivityInfo()
                {
                    DG_Actions_pkey = GetFieldValue(sqlReader, "DG_Actions_pkey", 0),
                    DG_Actions_Name = GetFieldValue(sqlReader, "DG_Actions_Name", string.Empty),
                    DG_Acitivity_Date = GetFieldValue(sqlReader, "DG_Acitivity_Date", DateTime.Now),
                    Name = GetFieldValue(sqlReader, "Name", string.Empty),
                    DG_Acitivity_Descrption = GetFieldValue(sqlReader, "DG_Acitivity_Descrption", string.Empty),
                    DG_Acitivity_Action_Pkey = GetFieldValue(sqlReader, "DG_Acitivity_Action_Pkey", 0),
                    ActivityDate = GetFieldValue(sqlReader, "ActivityDate", DateTime.Now),
                    usersInfo = new UsersInfo()
                    {
                        DG_User_Name = GetFieldValue(sqlReader, "DG_User_Name", string.Empty),
                        DG_User_First_Name = GetFieldValue(sqlReader, "DG_User_First_Name", string.Empty),
                        DG_User_Last_Name = GetFieldValue(sqlReader, "DG_User_Last_Name", string.Empty),
                        DG_User_pkey = GetFieldValue(sqlReader, "DG_User_pkey", 0),
                    },
                };
                GetActivityReportsList.Add(GetActivityReports);
            }
            return GetActivityReportsList;
        }
        public DataSet GetActivityReport(DateTime? FromDate, DateTime? ToDate, int? UserID)
        {
            DBParameters.Clear();
            AddParameter("@ParamFromDate",SetNullDateTimeValue( FromDate));
            AddParameter("@ParamToDate",SetNullDateTimeValue( ToDate));
            AddParameter("@ParamPhotographerID", SetNullIntegerValue( UserID));
            DataSet dsData = ExecuteDataSet(DAOConstant.USP_RPT_ACTIVITYREPORTS);
            return dsData;
        }
        //private List<vw_GetActivityReports> MapActivityReportsInfo(DataTable sqlReader)
        //{
        //    List<vw_GetActivityReports> GetActivityReportsList = new List<vw_GetActivityReports>();
        //    while (sqlReader.Read())
        //    {
        //        vw_GetActivityReports GetActivityReports = new vw_GetActivityReports()
        //        { 
        //            DG_Actions_pkey = GetFieldValue(sqlReader, "DG_Actions_pkey", 0),
        //            DG_Actions_Name = GetFieldValue(sqlReader, "DG_Actions_Name", string.Empty),
        //            DG_Acitivity_Date = GetFieldValue(sqlReader, "DG_Acitivity_Date", DateTime.Now),
        //            Name = GetFieldValue(sqlReader, "Name", string.Empty),
        //            DG_Acitivity_Descrption = GetFieldValue(sqlReader, "DG_Acitivity_Descrption", string.Empty),
        //            DG_Acitivity_Action_Pkey = GetFieldValue(sqlReader, "DG_Acitivity_Action_Pkey", 0),
        //            ActivityDate = GetFieldValue(sqlReader, "ActivityDate", DateTime.Now),


        //            DG_User_Name = GetFieldValue(sqlReader, "DG_User_Name", string.Empty),
        //            DG_User_First_Name = GetFieldValue(sqlReader, "DG_User_First_Name", string.Empty),
        //            DG_User_Last_Name = GetFieldValue(sqlReader, "DG_User_Last_Name", string.Empty),
        //            DG_User_pkey = GetFieldValue(sqlReader, "DG_User_pkey", 0),

        //        };
        //        GetActivityReportsList.Add(GetActivityReports);
        //    }
        //    return GetActivityReportsList;
        //}
        public List<string> IsUpdateAvailable(string currVersion)
        {
            DBParameters.Clear();
            AddParameter("@Version", currVersion);
            IDataReader sqlReader = ExecuteReader(DAOConstant.USPCHECKUPDATES);
            List<string> OrdersList = MapIsUpdateAvailable(sqlReader);
            sqlReader.Close();
            return OrdersList;
        }
        private List<string> MapIsUpdateAvailable(IDataReader sqlReader)
        {
            List<string> GetIsUpdateAvailableList = new List<string>();
            while (sqlReader.Read())
            {
                ActivityInfo GetActivityReports = new ActivityInfo()
                {
                    Version = GetFieldValue(sqlReader, "VERSION", string.Empty)
                };
                GetIsUpdateAvailableList.Add(GetActivityReports.Version);
            }
            return GetIsUpdateAvailableList;
        }
    }
}
