﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DigiPhoto.IMIX.Model;
using System.Data;

namespace DigiPhoto.IMIX.DataAccess
{
    public class UsersDao : BaseDataAccess
    {
        #region Constructor
        public UsersDao(BaseDataAccess baseaccess)
            : base(baseaccess)
        { }
        public UsersDao()
        { }
        #endregion

        public bool UPD_UserLocation(UsersInfo objectInfo)
        {
            DBParameters.Clear();
            AddParameter("@ParamDG_User_pkey", objectInfo.DG_User_pkey);
            AddParameter("@ParamDG_Location_ID", objectInfo.DG_Location_ID);

            ExecuteNonQuery(DAOConstant.USP_UPD_LOCATION);
            return true;
        }
        public bool UPD_User(UsersInfo objectInfo)
        {
            DBParameters.Clear();
            AddParameter("@ParamDG_User_pkey", objectInfo.DG_User_pkey);
            AddParameter("@ParamDG_User_Roles_Id", objectInfo.DG_User_Roles_Id);
            AddParameter("@ParamDG_Location_ID", objectInfo.DG_Location_ID);
            AddParameter("@ParamDG_User_CreatedDate", objectInfo.DG_User_CreatedDate);
            AddParameter("@ParamDG_User_Status", objectInfo.DG_User_Status);
            AddParameter("@ParamIsSynced", objectInfo.IsSynced);
            AddParameter("@ParamDG_User_Name", objectInfo.DG_User_Name);
            AddParameter("@ParamDG_User_First_Name", objectInfo.DG_User_First_Name);
            AddParameter("@ParamDG_User_Last_Name", objectInfo.DG_User_Last_Name);
            AddParameter("@ParamDG_User_PhoneNo", objectInfo.DG_User_PhoneNo);
            AddParameter("@ParamDG_User_Email", objectInfo.DG_User_Email);
            AddParameter("@ParamSyncCode", objectInfo.SyncCode);
            AddParameter("@ParamDG_User_Password", objectInfo.DG_User_Password);
            ExecuteNonQuery(DAOConstant.USP_UPD_USER);
            return true;
        }
        public List<UsersInfo> GetUsersInfo(int Userkey, int StoreID, int UserRolesId, int UserStatus, string UserName,
            string UserFirstName)
        {
            DBParameters.Clear();
            AddParameter("@ParamDG_User_pkey", Userkey);
            AddParameter("@ParamDG_Store_ID", StoreID);
            AddParameter("@ParamDG_User_Roles_Id", UserRolesId);
            AddParameter("@ParamDG_User_Status", UserStatus);
            AddParameter("@ParamDG_User_Name", UserName);
            AddParameter("@ParamDG_User_First_Name", UserFirstName);
            IDataReader sqlReader = ExecuteReader(DAOConstant.USP_SEL_USERDETAILS);
            List<UsersInfo> vw_GetUserDetailsList = MapUsersInfo(sqlReader);
            sqlReader.Close();
            return vw_GetUserDetailsList;
        }
        private List<UsersInfo> MapUsersInfo(IDataReader sqlReader)
        {
            List<UsersInfo> GetUserDetailsList = new List<UsersInfo>();
            while (sqlReader.Read())
            {
                UsersInfo GetUserDetails = new UsersInfo()
                {
                    DG_User_pkey = GetFieldValue(sqlReader, "DG_User_pkey", 0),
                    DG_User_Role = GetFieldValue(sqlReader, "DG_User_Role", string.Empty),
                    DG_User_First_Name = GetFieldValue(sqlReader, "DG_User_First_Name", string.Empty),
                    DG_User_Last_Name = GetFieldValue(sqlReader, "DG_User_Last_Name", string.Empty),
                    DG_User_Password = GetFieldValue(sqlReader, "DG_User_Password", string.Empty),
                    DG_User_Name = GetFieldValue(sqlReader, "DG_User_Name", string.Empty),
                    DG_User_Roles_Id = GetFieldValue(sqlReader, "DG_User_Roles_Id", 0),
                    DG_Location_Name = GetFieldValue(sqlReader, "DG_Location_Name", string.Empty),
                    DG_Store_ID = GetFieldValue(sqlReader, "DG_Store_ID", 0),
                    DG_Location_pkey = GetFieldValue(sqlReader, "DG_Location_pkey", 0),
                    DG_User_Status = GetFieldValue(sqlReader, "DG_User_Status", false),
                    DG_User_PhoneNo = GetFieldValue(sqlReader, "DG_User_PhoneNo", string.Empty),
                    UserName = GetFieldValue(sqlReader, "UserName", string.Empty),
                    StatusName = GetFieldValue(sqlReader, "StatusName", string.Empty),
                    DG_User_Email = GetFieldValue(sqlReader, "DG_User_Email", string.Empty),
                    DG_Store_Name = GetFieldValue(sqlReader, "DG_Store_Name", string.Empty),
                    CountryCode = GetFieldValue(sqlReader, "CountryCode", string.Empty),
                    StoreCode = GetFieldValue(sqlReader, "StoreCode", string.Empty),
                    DG_Substore_ID = GetFieldValue(sqlReader, "DG_Substore_ID", 0)

                };

                GetUserDetailsList.Add(GetUserDetails);
            }
            return GetUserDetailsList;
        }
        public UsersInfo GetUserDetails(string userName, string password, bool userStatus)
        {
            DBParameters.Clear();
            AddParameter("@ParamDG_User_Name", userName);
            AddParameter("@ParamDG_User_Password", password);
            AddParameter("@ParamDG_User_Status", userStatus);

            List<UsersInfo> objectList;
            using (IDataReader sqlReader = ExecuteReader(DAOConstant.USP_GET_USERDETAILSBYUSERNAMEANDPASSWORD))
            {
                objectList = MapObject(sqlReader);
            }
            return objectList.FirstOrDefault();
        }
        private List<UsersInfo> MapObject(IDataReader sqlReader)
        {
            List<UsersInfo> objectList = new List<UsersInfo>();
            while (sqlReader.Read())
            {
                UsersInfo objectValue = new UsersInfo
                {
                    DG_User_pkey = GetFieldValue(sqlReader, "DG_User_pkey", 0),
                    DG_User_Role = GetFieldValue(sqlReader, "DG_User_Role", string.Empty),
                    DG_User_First_Name = GetFieldValue(sqlReader, "DG_User_First_Name", string.Empty),
                    DG_User_Last_Name = GetFieldValue(sqlReader, "DG_User_Last_Name", string.Empty),
                    DG_User_Password = GetFieldValue(sqlReader, "DG_User_Password", string.Empty),
                    DG_User_Name = GetFieldValue(sqlReader, "DG_User_Name", string.Empty),
                    DG_User_Roles_Id = GetFieldValue(sqlReader, "DG_User_Roles_Id", 0),
                    DG_Location_Name = GetFieldValue(sqlReader, "DG_Location_Name", string.Empty),
                    DG_Store_ID = GetFieldValue(sqlReader, "DG_Store_ID", 0),
                    DG_Location_pkey = GetFieldValue(sqlReader, "DG_Location_pkey", 0),
                    DG_User_Status = GetFieldValue(sqlReader, "DG_User_Status", false),
                    DG_User_PhoneNo = GetFieldValue(sqlReader, "DG_User_PhoneNo", string.Empty),
                    UserName = GetFieldValue(sqlReader, "UserName", string.Empty),
                    StatusName = GetFieldValue(sqlReader, "StatusName", string.Empty),
                    DG_User_Email = GetFieldValue(sqlReader, "DG_User_Email", string.Empty),
                    DG_Store_Name = GetFieldValue(sqlReader, "DG_Store_Name", string.Empty),
                    CountryCode = GetFieldValue(sqlReader, "CountryCode", string.Empty),
                    StoreCode = GetFieldValue(sqlReader, "StoreCode", string.Empty),
                    ServerHotFolderPath = GetFieldValue(sqlReader, "ServerHotFolderPath", string.Empty),
                };

                objectList.Add(objectValue);
            }
            return objectList;
        }
        public UsersInfo GetLocationIdbyUserId(int UserId)
        {
            DBParameters.Clear();
            AddParameter("@ParamUserId", UserId);

            List<UsersInfo> objectList;
            using (IDataReader sqlReader = ExecuteReader(DAOConstant.USP_SEL_LOCATIONBYUSERID))
            {
                objectList = MapObjectLocationIdbyUserId(sqlReader);
            }
            return objectList.FirstOrDefault();
        }
        private List<UsersInfo> MapObjectLocationIdbyUserId(IDataReader sqlReader)
        {
            List<UsersInfo> objectList = new List<UsersInfo>();
            while (sqlReader.Read())
            {
                UsersInfo objectValue = new UsersInfo
                {
                    DG_Location_ID = GetFieldValue(sqlReader, "DG_Location_ID", 0),
                };

                objectList.Add(objectValue);
            }
            return objectList;
        }
        public bool UPD_EncryptUsersPwd(int UserKey, string UserValue)
        {
            DBParameters.Clear();
            AddParameter("@ParamDG_User_pkey", UserKey);
            AddParameter("@ParamDG_User_Password", UserValue);
            ExecuteNonQuery(DAOConstant.USP_UPD_ENCRYPTUSERSPWD);
            return true;
        }
        public bool UPD_INS_UserDetails(int DG_User_pkey, int DG_User_Roles_Id, int DG_Location_ID, bool? DG_User_Status,
            string DG_User_Name, string DG_User_First_Name, string DG_User_Last_Name, string DG_User_PhoneNo, string DG_User_Email, string SyncCode,
            string DG_User_Password,string DG_Emp_Id)
        {
            DBParameters.Clear();
            AddParameter("@ParamDG_User_pkey", DG_User_pkey);
            AddParameter("@ParamDG_User_Roles_Id", DG_User_Roles_Id);
            AddParameter("@ParamDG_Location_ID", DG_Location_ID);
            AddParameter("@ParamDG_User_Status", DG_User_Status);
            AddParameter("@ParamIsSynced", false);
            AddParameter("@ParamDG_User_Name", DG_User_Name);
            AddParameter("@ParamDG_User_First_Name", DG_User_First_Name);
            AddParameter("@ParamDG_User_Last_Name", DG_User_Last_Name);
            AddParameter("@ParamDG_User_PhoneNo", DG_User_PhoneNo);
            AddParameter("@ParamDG_User_Email", DG_User_Email);
            AddParameter("@ParamDG_EMP_Id", DG_Emp_Id);
            AddParameter("@ParamSyncCode", SyncCode);
            AddParameter("@ParamDG_User_Password", DG_User_Password);
            ExecuteNonQuery(DAOConstant.USP_UPDANDINS_USERS);

            return true;
        }
        public int UpdAndInsCameraDetails(int CameraId, int PhotoGrapherId, int LoginId, int LocationId, string CameraName, string CameraMake, string CameraModel, string PhotoSeries, string SyncCode)
        {
            DBParameters.Clear();
            AddParameter("@ParamCameraId", CameraId);
            AddParameter("@ParamPhotoGrapherId", PhotoGrapherId);
            AddParameter("@ParamLoginId", LoginId);
            AddParameter("@ParamLocationId", LocationId);
            AddParameter("@ParamCameraName", CameraName);
            AddParameter("@ParamCameraMake", CameraMake);
            AddParameter("@ParamCameraModel", CameraModel);
            AddParameter("@ParamPhotoSeries", PhotoSeries);
            AddParameter("@ParamSyncCode", SyncCode);

            ExecuteNonQuery(DAOConstant.USP_UPDANDINS_CAMERADETAILS);

            //TBD :
            return 0;// CameraDetailsList;
        }
        public List<UsersInfo> GetUsersPwdDetails()
        {
            DBParameters.Clear();
            IDataReader sqlReader = ExecuteReader(DAOConstant.USP_GET_USERSPWDDETAILS);
            List<UsersInfo> UsersList = MapUsersPwdDetailsInfo(sqlReader);
            sqlReader.Close();
            return UsersList;
        }
        private List<UsersInfo> MapUsersPwdDetailsInfo(IDataReader sqlReader)
        {
            List<UsersInfo> UsersList = new List<UsersInfo>();
            while (sqlReader.Read())
            {
                UsersInfo Users = new UsersInfo()
                {
                    DG_User_pkey = GetFieldValue(sqlReader, "DG_User_pkey", 0),
                    DG_User_Password = GetFieldValue(sqlReader, "DG_User_Password", string.Empty),
                };

                UsersList.Add(Users);
            }
            return UsersList;
        }
        public Dictionary<string, int> SelUserDetailsByUserId(int userId)
        {
            DBParameters.Clear();
            AddParameter("@ParamUserpkey", SetNullIntegerValue(userId));
            IDataReader sqlReader = ExecuteReader(DAOConstant.USP_SEL_USERDETAILSBYUSERID);
            Dictionary<string, int> UserDetailsList = MapUserDetailsBase(sqlReader);
            sqlReader.Close();
            return UserDetailsList;
        }
        private Dictionary<string, int> MapUserDetailsBase(IDataReader sqlReader)
        {
            Dictionary<string, int> UserDetailsList = new Dictionary<string, int>();
            UserDetailsList.Add("--Select--", 0);
            while (sqlReader.Read())
            {
                UserDetailsList.Add((GetFieldValue(sqlReader, "DG_User_First_Name", string.Empty) + "" +
                    GetFieldValue(sqlReader, "DG_User_Last_Name", string.Empty) + "(" + GetFieldValue(sqlReader, "DG_User_Name", string.Empty) + ")"), (GetFieldValue(sqlReader, "DG_User_pkey", 0)));
            }
            return UserDetailsList;
        }
        public List<UsersInfo> SelUserDetailByUserId(int userId, string userName,int roleId)
        {
            DBParameters.Clear();
            AddParameter("@ParamUserpkey", SetNullIntegerValue(userId));
            AddParameter("@ParamUserName", SetNullStringValue(userName));
            AddParameter("@ParamRoleId", SetNullIntegerValue(roleId));
            IDataReader sqlReader = ExecuteReader(DAOConstant.USP_SEL_USERDETAILSBYUSERID);
            List<UsersInfo> UserDetailsList = MapUserDetailBase(sqlReader);
            sqlReader.Close();
            return UserDetailsList;
        }
        public List<UsersInfo> GetChildUserDetailByUserId(int roleId)
        {
            DBParameters.Clear();
            //AddParameter("@ParamUserpkey", SetNullIntegerValue(userId));
            //AddParameter("@ParamUserName", SetNullStringValue(userName));
            AddParameter("@ParamRoleId", SetNullIntegerValue(roleId));
            IDataReader sqlReader = ExecuteReader(DAOConstant.USP_SEL_CHILDUSERDETAILSBYUSERID);
            List<UsersInfo> UserDetailsList = MapUserDetailBase(sqlReader);
            sqlReader.Close();
            return UserDetailsList;
        }
        private List<UsersInfo> MapUserDetailBase(IDataReader sqlReader)
        {
            List<UsersInfo> UserDetailsList = new List<UsersInfo>();

            while (sqlReader.Read())
            {
                UsersInfo Users = new UsersInfo()
                {
                    //DG_User_pkey = GetFieldValue(sqlReader, "DG_User_pkey", 0),
                    //DG_User_Name = GetFieldValue(sqlReader, "DG_User_Name", string.Empty),
                    //DG_User_First_Name = GetFieldValue(sqlReader, "DG_User_First_Name", string.Empty),
                    //DG_User_Last_Name = GetFieldValue(sqlReader, "DG_User_Last_Name", string.Empty),

                    //UserName = GetFieldValue(sqlReader, "UserName", string.Empty),
                    //DG_User_PhoneNo = GetFieldValue(sqlReader, "DG_User_PhoneNo", string.Empty),
                    //DG_User_Role = GetFieldValue(sqlReader, "DG_User_Role", string.Empty),
                    //StatusName = GetFieldValue(sqlReader, "StatusName", string.Empty),
                    //DG_Store_ID = GetFieldValue(sqlReader, "DG_Store_ID", 0),


                    DG_User_pkey = GetFieldValue(sqlReader, "DG_User_pkey", 0),
                    DG_User_Role = GetFieldValue(sqlReader, "DG_User_Role", string.Empty),
                    DG_User_First_Name = GetFieldValue(sqlReader, "DG_User_First_Name", string.Empty),
                    DG_User_Last_Name = GetFieldValue(sqlReader, "DG_User_Last_Name", string.Empty),
                    DG_User_Password = GetFieldValue(sqlReader, "DG_User_Password", string.Empty),
                    DG_User_Name = GetFieldValue(sqlReader, "DG_User_Name", string.Empty),
                    DG_User_Roles_Id = GetFieldValue(sqlReader, "DG_User_Roles_Id", 0),
                    DG_Location_Name = GetFieldValue(sqlReader, "DG_Location_Name", string.Empty),
                    DG_Store_ID = GetFieldValue(sqlReader, "DG_Store_ID", 0),
                    DG_Location_pkey = GetFieldValue(sqlReader, "DG_Location_pkey", 0),
                    DG_User_Status = GetFieldValue(sqlReader, "DG_User_Status", false),
                    DG_User_PhoneNo = GetFieldValue(sqlReader, "DG_User_PhoneNo", string.Empty),
                    UserName = GetFieldValue(sqlReader, "UserName", string.Empty),
                    StatusName = GetFieldValue(sqlReader, "StatusName", string.Empty),
                    DG_User_Email = GetFieldValue(sqlReader, "DG_User_Email", string.Empty),
                    DG_Store_Name = GetFieldValue(sqlReader, "DG_Store_Name", string.Empty),
                    DG_Emp_Id = GetFieldValue(sqlReader, "DG_Emp_Id", string.Empty),//BY KCB ON 20 JUN 2020 For display user' employee id
                    CountryCode = GetFieldValue(sqlReader, "CountryCode", string.Empty),
                    StoreCode = GetFieldValue(sqlReader, "StoreCode", string.Empty),
                    DG_Substore_ID = GetFieldValue(sqlReader, "DG_Substore_ID", 0)

                };
                UserDetailsList.Add(Users);
            }
            return UserDetailsList;
        }
        public List<UserInfo> GetPhotoGrapherList()
        {
            IDataReader sqlReader = ExecuteReader(DAOConstant.USP_GET_GetPhotoGrapherDetails);
            List<UserInfo> RideCameraList = MapRidePhotoGrapherList(sqlReader);
            sqlReader.Close();
            return RideCameraList;
        }
        private List<UserInfo> MapRidePhotoGrapherList(IDataReader sqlReader)
        {
            List<UserInfo> RideCameraInfoList = new List<UserInfo>();
            while (sqlReader.Read())
            {
                UserInfo RideCameraInfo = new UserInfo()
                {
                    UserId = GetFieldValue(sqlReader, "DG_User_pkey", 0),
                    UserName = GetFieldValue(sqlReader, "DG_User_Name", string.Empty),
                    Role_ID = GetFieldValue(sqlReader, "DG_User_Roles_Id", 0),
                    Photographer = GetFieldValue(sqlReader, "Photograper", string.Empty),
                };
                RideCameraInfoList.Add(RideCameraInfo);
            }
            return RideCameraInfoList;
        }
        public List<UsersInfo> GetUserByUserId(int Userid)
        {
            DBParameters.Clear();
            AddParameter("@ParamDG_User_pkey", Userid);
            IDataReader sqlReader = ExecuteReader(DAOConstant.usp_GET_UserDetails);
            List<UsersInfo> GetUserDetailsList = MapUsersInfo(sqlReader);
            sqlReader.Close();
            return GetUserDetailsList;
        }

        public bool DeleteUsersbyId(Int32 userID)
        {
            DBParameters.Clear();
            AddParameter("@ParamUserID", userID);
            ExecuteNonQuery(DAOConstant.USP_DEL_USERSBYID);
            return true;
        }

        public List<UsersInfo> SearchUserDetails(string FName, string LName, int StoreId, string Status, int RoleId, string MobileNumber, string EmailId, int locationId, string UserName,string EmpId)
        {
            bool? UserStatus = null;
            if (Status == "1")
            { UserStatus = true; }
            else if (Status == "2")
            {
                UserStatus = false;
            }

            DBParameters.Clear();
            AddParameter("@ParamFirstName", SetNullStringValue(FName));
            AddParameter("@ParamLastName", SetNullStringValue(LName));
            AddParameter("@ParamStoreId", SetNullIntegerValue(StoreId));
            AddParameter("@ParamUserStatus", SetNullBoolValue(UserStatus));
            AddParameter("@ParamRoleId", SetNullIntegerValue(RoleId));
            AddParameter("@ParamMobileNumber", SetNullStringValue(MobileNumber));
            AddParameter("@ParamEmailId", SetNullStringValue(EmailId));
            AddParameter("@ParamLocationId", SetNullIntegerValue(locationId));
            AddParameter("@ParamUserName", SetNullStringValue(UserName));
            AddParameter("@ParamEmpId", SetNullStringValue(UserName));
            IDataReader sqlReader = ExecuteReader(DAOConstant.USP_SEARCH_USERDETAIL);
            List<UsersInfo> UserDetailsList = MapUserDetailList(sqlReader);
            sqlReader.Close();
            return UserDetailsList;
        }
        private List<UsersInfo> MapUserDetailList(IDataReader sqlReader)
        {
            List<UsersInfo> UserDetailsList = new List<UsersInfo>();
            while (sqlReader.Read())
            {
                UsersInfo UserDetailsInfo = new UsersInfo()
                {
                    DG_User_Role = GetFieldValue(sqlReader, "DG_User_Role", string.Empty),
                    DG_User_First_Name = GetFieldValue(sqlReader, "DG_User_First_Name", string.Empty),
                    DG_User_Last_Name = GetFieldValue(sqlReader, "DG_User_Last_Name", string.Empty),
                    DG_User_Password = GetFieldValue(sqlReader, "DG_User_Password", string.Empty),
                    DG_Location_Name = GetFieldValue(sqlReader, "DG_Location_Name", string.Empty),
                    DG_Store_ID = GetFieldValue(sqlReader, "DG_Store_ID", 0),
                    DG_Location_pkey = GetFieldValue(sqlReader, "DG_Location_pkey", 0),
                    DG_User_Status = GetFieldValue(sqlReader, "DG_User_Status", false),
                    DG_User_PhoneNo = GetFieldValue(sqlReader, "DG_User_PhoneNo", string.Empty),
                    StatusName = GetFieldValue(sqlReader, "StatusName", string.Empty),
                    DG_User_Email = GetFieldValue(sqlReader, "DG_User_Email", string.Empty),
                    DG_Store_Name = GetFieldValue(sqlReader, "DG_Store_Name", string.Empty),
                    CountryCode = GetFieldValue(sqlReader, "CountryCode", string.Empty),
                    StoreCode = GetFieldValue(sqlReader, "StoreCode", string.Empty),
                    DG_Substore_ID = GetFieldValue(sqlReader, "DG_Substore_ID", 0),
                    DG_User_pkey = GetFieldValue(sqlReader, "DG_User_pkey", 0),
                    DG_User_Name = GetFieldValue(sqlReader, "DG_User_Name", string.Empty),
                    DG_User_Roles_Id = GetFieldValue(sqlReader, "DG_User_Roles_Id", 0),
                    UserName = GetFieldValue(sqlReader, "UserName", string.Empty),

                };
                UserDetailsList.Add(UserDetailsInfo);
            }
            return UserDetailsList;
        }
        public List<PermissionInfo> SelectPermission(int PermissionId, string PermissionName)
        {
            DBParameters.Clear();
            AddParameter("@ParamPermissionId", SetNullIntegerValue(PermissionId));
            AddParameter("@ParamPermissionName", SetNullStringValue(PermissionName));

            IDataReader sqlReader = ExecuteReader(DAOConstant.USP_SEL_PERMISSION);
            List<PermissionInfo> PermissionsList = MapPermissionInfo(sqlReader);
            sqlReader.Close();
            return PermissionsList;
        }
        private List<PermissionInfo> MapPermissionInfo(IDataReader sqlReader)
        {
            List<PermissionInfo> PermissionsList = new List<PermissionInfo>();
            while (sqlReader.Read())
            {
                PermissionInfo Permissions = new PermissionInfo()
                {
                    DG_Permission_pkey = GetFieldValue(sqlReader, "DG_Permission_pkey", 0),
                    DG_Permission_Name = GetFieldValue(sqlReader, "DG_Permission_Name", string.Empty),
                    SyncCode = GetFieldValue(sqlReader, "SyncCode", string.Empty),
                    IsSynced = GetFieldValue(sqlReader, "IsSynced", false),

                };

                PermissionsList.Add(Permissions);
            }
            return PermissionsList;
        }
        //BY KCB ON 20 JUN 2020 TO get user' employee name
        private string MapEmployeeName(IDataReader sqlReader)
        {
            string empname = string.Empty;
            while (sqlReader.Read())
            {
                empname = GetFieldValue(sqlReader, "DG_User_Name", string.Empty);
            }
            return empname;
        }
        //BY KCB ON 20 JUN 2020 TO get user' employee name
        public string GetEmployeeName(string EmpId)
        {
            string empid = string.Empty;
            DBParameters.Clear();
            AddParameter("@ParamEmpId", SetNullStringValue(EmpId));
            IDataReader sqlReader = ExecuteReader(DAOConstant.USP_GetEmplyeeName);
            empid = MapEmployeeName(sqlReader);
            sqlReader.Close();
            return empid;
        }
    }
}
