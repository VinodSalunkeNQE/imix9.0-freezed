﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using DigiPhoto.IMIX.Model;

namespace DigiPhoto.IMIX.DataAccess
{
    public class TripCamDao : BaseDataAccess
    {
        #region Constructor
        public TripCamDao(BaseDataAccess baseaccess)
            : base(baseaccess)
        { }
        public TripCamDao()
        { }
        #endregion

        public bool UpdCamIdForTripCamPOSMapping(int oldCamId, int NewCamid)
        {
            DBParameters.Clear();
            AddParameter("@ParamOldCamId", oldCamId);
            AddParameter("@ParamNewCamId", NewCamid);
            ExecuteNonQuery("usp_UPD_CamIdForTripCamPOSMapping");
            return true;
        }
        public bool InsUpdTripCamSettings(DataTable dt)
        {
            DBParameters.Clear();
            AddParameter("@TripCamSettingTable", dt);
            ExecuteNonQuery("InsUpdTripCamSettings");
            return true;
        }
        public TripCamInfo GetTripCameraInfoById(string CameraName, string CameraId)
        {

            DBParameters.Clear();
            AddParameter("@ParamCameraName", CameraName);
            AddParameter("@ParamCameraModel", CameraId);
            IDataReader sqlReader = ExecuteReader(DAOConstant.USP_GET_TripCameraInfoById);
            TripCamInfo tripCamInfo = MapTripCamInfo(sqlReader);
            sqlReader.Close();
            return tripCamInfo;
        }
        private TripCamInfo MapTripCamInfo(IDataReader sqlReader)
        {
            TripCamInfo tripCamInfo = new TripCamInfo();
            while (sqlReader.Read())
            {
                tripCamInfo.CameraFolderpath = GetFieldValue(sqlReader, "CameraPath", string.Empty);
                tripCamInfo.Camera_pKey = GetFieldValue(sqlReader, "DG_Camera_pkey", 0);
                tripCamInfo.CameraModel = GetFieldValue(sqlReader, "DG_Camera_Model", string.Empty);
                tripCamInfo.TripCamTypeId = GetFieldValue(sqlReader, "TripCamTypeId", 0);
            }
            return tripCamInfo;
        }
        public List<TripCamSettingInfo> GetTripCamSettingsForCameraId(int CameraId, int TripCamTypeId)
        {
            DBParameters.Clear();
            AddParameter("@ParamCameraId", CameraId);
            AddParameter("@ParamTripCamTypeId", TripCamTypeId);
            IDataReader sqlReader = ExecuteReader(DAOConstant.USP_GET_TripCamSettingsValues);
            List<TripCamSettingInfo> lstTripCamSettingInfo = MapTripCamSettingsInfo(sqlReader);
            sqlReader.Close();
            return lstTripCamSettingInfo;
        }

        public List<TripCamSettingInfo> GetSavedTripCamSettingsForCameraId(int CameraId)
        {
            DBParameters.Clear();
            AddParameter("@CameraId", CameraId);
            IDataReader sqlReader = ExecuteReader(DAOConstant.USP_GET_SavedTripCamSettingsValues);
            List<TripCamSettingInfo> lstTripCamSettingInfo = MapTripCamSettingsInfo(sqlReader);
            sqlReader.Close();
            return lstTripCamSettingInfo;
        }

        private List<TripCamSettingInfo> MapTripCamSettingsInfo(IDataReader sqlReader)
        {
            List<TripCamSettingInfo> lstTripCamSettingsInfo = new List<TripCamSettingInfo>();
            while (sqlReader.Read())
            {
                TripCamSettingInfo tripCamSettingsInfo = new TripCamSettingInfo
                {
                    TripCamValueId = GetFieldValue(sqlReader, "TripCamValueId", 0),
                    TripCamSettingsMasterId = GetFieldValue(sqlReader, "TripCamSettingsMasterId", 0L),
                    SettingsValue = GetFieldValue(sqlReader, "SettingsValue", string.Empty),
                    CameraId = GetFieldValue(sqlReader, "CameraId", 0),
                    ModifiedDate = GetFieldValue(sqlReader, "ModifiedDate", DateTime.MinValue),
                };
                lstTripCamSettingsInfo.Add(tripCamSettingsInfo);
            }
            return lstTripCamSettingsInfo;
        }
        public bool ValidateCameraRunningStatus(string CamMake)
        {
            bool result = false;
            DBParameters.Clear();
            AddParameter("@ParamCameraMake", CamMake);
            result = Convert.ToBoolean(ExecuteScalar(DAOConstant.USP_ValidateTripCamRunningStatus));
            return result;
        }

        public bool ValidateCameraAvailability(string CamMake)
        {
            bool result = false;
            DBParameters.Clear();
            AddParameter("@ParamCameraMake", CamMake);
            result = Convert.ToBoolean(ExecuteScalar(DAOConstant.USP_ValidateCameraAvailability));
            return result;
        }
    }
}
