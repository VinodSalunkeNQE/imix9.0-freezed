﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DigiPhoto.IMIX.Model;
using System.Data;
using DigiPhoto.Utility.Repository.ValueType;

namespace DigiPhoto.IMIX.DataAccess
{
    public class ConfigurationDao : BaseDataAccess
    {
        #region Constructor
        public ConfigurationDao(BaseDataAccess baseaccess)
            : base(baseaccess)
        { }
        public ConfigurationDao()
        { }
        #endregion


        public ConfigurationInfo GetConfigurationSetting(int subStoreId)
        {
            DBParameters.Clear();
            AddParameter("@ParamSubStoreId", subStoreId);

            IDataReader sqlReader = ExecuteReader(DAOConstant.USP_GET_CONFIGURATIONSETTINGS);
            List<ConfigurationInfo> configValueList = MapConfigurationInfo(sqlReader);
            sqlReader.Close();
            return configValueList.FirstOrDefault();
        }
        public List<ConfigurationInfo> GetAllConfigurationSetting()
        {
            List<ConfigurationInfo> configValueList = null;
            DBParameters.Clear();
            this.OpenConnection();
            AddParameter("@ParamSubStoreId", DBNull.Value);
            using (IDataReader sqlReader = ExecuteReader(DAOConstant.USP_GET_CONFIGURATIONSETTINGS))
            {
                configValueList = MapConfigurationInfo(sqlReader);
                sqlReader.Close();
            };

            return configValueList;
        }

        public List<ConfigurationInfo> GetConfigurationSettings()
        {
            List<ConfigurationInfo> configValueList = null;
            DBParameters.Clear();
            this.OpenConnection();
            using (IDataReader sqlReader = ExecuteReader(DAOConstant.USP_GET_CONFIGURATIONSETTINGS))
            {
                configValueList = MapConfigurationInfo(sqlReader);
                sqlReader.Close();
            };

            return configValueList;
        }

        public List<ConfigurationInfo> MapConfigurationInfo(IDataReader sqlReader)
        {
            int currencyId = 0;
            var currencyInfo = (new CurrencyDao()).GetDefaultCurrency();
            if (currencyInfo != null)
                currencyId = currencyInfo.DG_Currency_pkey;

            List<ConfigurationInfo> configValueList = new List<ConfigurationInfo>();
            while (sqlReader.Read())
            {
                ConfigurationInfo configValue = new ConfigurationInfo()
                {
                    DG_Config_pkey = GetFieldValue(sqlReader, "DG_Config_pkey", 0),
                    DG_Hot_Folder_Path = GetFieldValue(sqlReader, "DG_Hot_Folder_Path", string.Empty),
                    DG_Frame_Path = GetFieldValue(sqlReader, "DG_Frame_Path", string.Empty),
                    DG_BG_Path = GetFieldValue(sqlReader, "DG_BG_Path", string.Empty),
                    DG_Mod_Password = GetFieldValue(sqlReader, "DG_Mod_Password", string.Empty),
                    DG_NoOfPhotos = GetFieldValue(sqlReader, "DG_NoOfPhotos", 0),
                    DG_Watermark = GetFieldValue(sqlReader, "DG_Watermark", false),
                    DG_SemiOrder = GetFieldValue(sqlReader, "DG_SemiOrder", false),
                    DG_HighResolution = GetFieldValue(sqlReader, "DG_HighResolution", false),
                    DG_AllowDiscount = GetFieldValue(sqlReader, "DG_AllowDiscount", false),
                    DG_EnableDiscountOnTotal = GetFieldValue(sqlReader, "DG_EnableDiscountOnTotal", false),
                    WiFiStartingNumber = GetFieldValue(sqlReader, "WiFiStartingNumber", 0.0M),
                    FolderStartingNumber = GetFieldValue(sqlReader, "FolderStartingNumber", 0.0M),
                    IsAutoLock = GetFieldValue(sqlReader, "IsAutoLock", false),
                    DG_SemiOrderMain = GetFieldValue(sqlReader, "DG_SemiOrderMain", false),
                    PosOnOff = GetFieldValue(sqlReader, "PosOnOff", false),
                    DG_ReceiptPrinter = GetFieldValue(sqlReader, "DG_ReceiptPrinter", string.Empty),
                    DG_IsAutoRotate = GetFieldValue(sqlReader, "DG_IsAutoRotate", false),
                    DG_Graphics_Path = GetFieldValue(sqlReader, "DG_Graphics_Path", string.Empty),
                    DG_IsCompression = GetFieldValue(sqlReader, "DG_IsCompression", false),
                    DG_IsEnableGroup = GetFieldValue(sqlReader, "DG_IsEnableGroup", false),
                    DG_Substore_Id = GetFieldValue(sqlReader, "DG_Substore_Id", 0),
                    DG_NoOfBillReceipt = GetFieldValue(sqlReader, "DG_NoOfBillReceipt", 0),
                    DG_ChromaColor = GetFieldValue(sqlReader, "DG_ChromaColor", string.Empty),
                    DG_ChromaTolerance = GetFieldValue(sqlReader, "DG_ChromaTolerance", 0.0M),
                    DG_DbBackupPath = GetFieldValue(sqlReader, "DG_DbBackupPath", string.Empty),
                    DG_CleanupTables = GetFieldValue(sqlReader, "DG_CleanupTables", string.Empty),
                    DG_HfBackupPath = GetFieldValue(sqlReader, "DG_HfBackupPath", string.Empty),
                    DG_ScheduleBackup = GetFieldValue(sqlReader, "DG_ScheduleBackup", string.Empty),
                    DG_IsBackupScheduled = GetFieldValue(sqlReader, "DG_IsBackupScheduled", false),
                    DG_Brightness = GetFieldValue(sqlReader, "DG_Brightness", 0.0F),
                    DG_Contrast = GetFieldValue(sqlReader, "DG_Contrast", 0.0F),
                    DG_PageCountGrid = GetFieldValue(sqlReader, "DG_PageCountGrid", 0),
                    DG_PageCountPreview = GetFieldValue(sqlReader, "DG_PageCountPreview", 0),
                    DG_NoOfPhotoIdSearch = GetFieldValue(sqlReader, "DG_NoOfPhotoIdSearch", 0),
                    IsRecursive = GetFieldValue(sqlReader, "IsRecursive", false),
                    IntervalCount = GetFieldValue(sqlReader, "IntervalCount", 0),
                    intervalType = GetFieldValue(sqlReader, "intervalType", 0),
                    DG_MktImgPath = GetFieldValue(sqlReader, "DG_MktImgPath", string.Empty),
                    DG_MktImgTimeInSec = GetFieldValue(sqlReader, "DG_MktImgTimeInSec", 0),
                    EK_SampleImagePath = GetFieldValue(sqlReader, "EK_SampleImagePath", string.Empty),
                    EK_DisplayDuration = GetFieldValue(sqlReader, "EK_DisplayDuration", 0),
                    EK_ScreenStartTime = GetFieldValue(sqlReader, "EK_ScreenStartTime", 0),
                    EK_IsScreenSaverActive = GetFieldValue(sqlReader, "EK_IsScreenSaverActive", false),
                    IsDeleteFromUSB = GetFieldValue(sqlReader, "IsDeleteFromUSB", false),
                    DG_CleanUpDaysBackUp = GetFieldValue(sqlReader, "DG_CleanUpDaysBackUp", 0),
                    IsExportReportToAnyDrive = GetFieldValue(sqlReader, "IsExportReportToAnyDrive", false),
                    FtpIP = GetFieldValue(sqlReader, "FtpIP", string.Empty),
                    FtpUid = GetFieldValue(sqlReader, "FtpUid", string.Empty),
                    FtpPwd = GetFieldValue(sqlReader, "FtpPwd", string.Empty),
                    FtpFolder = GetFieldValue(sqlReader, "FtpFolder", string.Empty),
                    DefaultCurrencyId = currencyId,// GetFieldValue(sqlReader, "DefaultCurrencyId", 0),
                    DefaultCurrency = currencyId, // GetFieldValue(sqlReader, "DefaultCurrency", 0),
                };

                configValueList.Add(configValue);
            }
            return configValueList;
        }
        public List<ConfigurationInfo> SelectConfigurationSettings(int subStoreId)
        {
            DBParameters.Clear();
            AddParameter("@ParamSubStoreId", subStoreId);

            IDataReader sqlReader = ExecuteReader(DAOConstant.USP_GET_CONFIGURATIONSETTINGS);
            List<ConfigurationInfo> configValueList = MapConfigurationInfo(sqlReader);
            sqlReader.Close();
            return configValueList;
        }
        public bool UpdateConfigSettings(ConfigurationInfo objectInfo)
        {
            DBParameters.Clear();
            AddParameter("@ParamDG_Config_pkey", objectInfo.DG_Config_pkey);
            AddParameter("@ParamDG_Hot_Folder_Path", objectInfo.DG_Hot_Folder_Path);
            AddParameter("@ParamDG_Frame_Path", objectInfo.DG_Frame_Path);
            AddParameter("@ParamDG_BG_Path", objectInfo.DG_BG_Path);
            AddParameter("@ParamDG_Mod_Password", objectInfo.DG_Mod_Password);
            AddParameter("@ParamDG_NoOfPhotos", objectInfo.DG_NoOfPhotos);
            AddParameter("@ParamDG_Watermark", objectInfo.DG_Watermark);
            AddParameter("@ParamDG_SemiOrder", objectInfo.DG_SemiOrder);
            AddParameter("@ParamDG_HighResolution", objectInfo.DG_HighResolution);
            AddParameter("@ParamDG_AllowDiscount", objectInfo.DG_AllowDiscount);
            AddParameter("@ParamDG_SemiOrderMain", objectInfo.DG_SemiOrderMain);
            AddParameter("@ParamPosOnOff", objectInfo.PosOnOff);
            AddParameter("@ParamDG_ReceiptPrinter", objectInfo.DG_ReceiptPrinter);
            AddParameter("@ParamDG_IsAutoRotate", objectInfo.DG_IsAutoRotate);
            AddParameter("@ParamDG_Graphics_Path", objectInfo.DG_Graphics_Path);
            AddParameter("@ParamDG_IsCompression", objectInfo.DG_IsCompression);
            AddParameter("@ParamDG_IsEnableGroup", objectInfo.DG_IsEnableGroup);
            AddParameter("@ParamDG_NoOfBillReceipt", objectInfo.DG_NoOfBillReceipt);
            AddParameter("@ParamDG_ChromaColor", objectInfo.DG_ChromaColor);
            AddParameter("@ParamDG_ChromaTolerance", objectInfo.DG_ChromaTolerance);
            AddParameter("@ParamDG_PageCountGrid", objectInfo.DG_PageCountGrid);
            AddParameter("@ParamDG_PageCountPreview", objectInfo.DG_PageCountPreview);
            AddParameter("@ParamIsSynced", objectInfo.IsSynced);
            ExecuteNonQuery(DAOConstant.USP_UPD_CONFIGURATIONBYCONFIGID);
            return true;
        }
        public bool UpdateConfigValue(iMIXConfigurationInfo objectInfo)
        {
            DBParameters.Clear();
            AddParameter("@ParamIMIXConfigurationValueId", objectInfo.IMIXConfigurationValueId);
            AddParameter("@ParamConfigurationValue", objectInfo.ConfigurationValue);
            AddParameter("@ParamIsSynced", objectInfo.IsSynced);
            ExecuteNonQuery(DAOConstant.USP_UPD_CONFIGURATIONVALUE);
            return true;
        }
        public iMIXConfigurationInfo GetConfigValue(int? subStoreId, int? configMasterId)
        {
            DBParameters.Clear();
            AddParameter("@ParamSubStoreId", SetNullIntegerValue(subStoreId));
            AddParameter("@ParamConfigurationMasterId", SetNullIntegerValue(configMasterId));

            List<iMIXConfigurationInfo> objectList;
            using (IDataReader sqlReader = ExecuteReader(DAOConstant.USP_SEL_CONFIGURATIONVALUE))
            {
                objectList = MapObject(sqlReader);
            }
            return objectList.FirstOrDefault();
        }
        public List<iMIXConfigurationInfo> SelectConfigValue(int? subStoreId, int? configMasterId)
        {
            DBParameters.Clear();
            AddParameter("@ParamSubStoreId", SetNullIntegerValue(subStoreId));
            AddParameter("@ParamConfigurationMasterId", SetNullIntegerValue(configMasterId));

            List<iMIXConfigurationInfo> objectList;
            using (IDataReader sqlReader = ExecuteReader(DAOConstant.USP_SEL_CONFIGURATIONVALUE))
            {
                objectList = MapObject(sqlReader);
            }
            return objectList;
        }
        private List<iMIXConfigurationInfo> MapObject(IDataReader sqlReader)
        {
            List<iMIXConfigurationInfo> objectList = new List<iMIXConfigurationInfo>();
            while (sqlReader.Read())
            {
                iMIXConfigurationInfo objectValue = new iMIXConfigurationInfo
                {
                    IMIXConfigurationValueId = GetFieldValue(sqlReader, "IMIXConfigurationValueId", 0L),
                    IMIXConfigurationMasterId = GetFieldValue(sqlReader, "IMIXConfigurationMasterId", 0L),
                    ConfigurationValue = GetFieldValue(sqlReader, "ConfigurationValue", string.Empty),
                    SubstoreId = GetFieldValue(sqlReader, "SubstoreId", 0),
                    SyncCode = GetFieldValue(sqlReader, "SyncCode", string.Empty),
                    IsSynced = GetFieldValue(sqlReader, "IsSynced", false),

                };

                objectList.Add(objectValue);
            }
            return objectList;
        }


        public int GetCompressionLevel(long IMIXConfigurationMasterId)
        {
            DBParameters.Clear();
            AddParameter("@ParamIMIXConfigurationMasterId", IMIXConfigurationMasterId);
            IDataReader sqlReader = ExecuteReader(DAOConstant.USP_GET_COMPRESSIONLEVEL);
            List<iMIXConfigurationInfo> iMIXConfigurationValueList = MapiMIXConfigurationValueBase(sqlReader);
            sqlReader.Close();
            return iMIXConfigurationValueList.First().ConfigurationValue.ToInt32();
        }
        private List<iMIXConfigurationInfo> MapiMIXConfigurationValueBase(IDataReader sqlReader)
        {
            List<iMIXConfigurationInfo> iMIXConfigurationValueList = new List<iMIXConfigurationInfo>();
            while (sqlReader.Read())
            {
                iMIXConfigurationInfo iMIXConfigurationValue = new iMIXConfigurationInfo()
                {
                    IMIXConfigurationValueId = GetFieldValue(sqlReader, "IMIXConfigurationValueId", 0),
                    IMIXConfigurationMasterId = GetFieldValue(sqlReader, "IMIXConfigurationMasterId", 0),
                    ConfigurationValue = GetFieldValue(sqlReader, "ConfigurationValue", string.Empty),
                };
                iMIXConfigurationValueList.Add(iMIXConfigurationValue);
            }
            return iMIXConfigurationValueList;
        }


        public List<iMIXConfigurationInfo> GetNewConfigValues(int SubstoreId)
        {
            DBParameters.Clear();
            AddParameter("@ParamSubstoreId", SubstoreId);

            IDataReader sqlReader = ExecuteReader(DAOConstant.USP_GET_NEWCONFIGVALUES);
            List<iMIXConfigurationInfo> iMIXConfigurationValueList = MapNewConfigValuesBase(sqlReader);
            sqlReader.Close();
            return iMIXConfigurationValueList;
        }
        public List<iMIXConfigurationInfo> GetAllNewConfigValues()
        {
            DBParameters.Clear();
            AddParameter("@ParamSubstoreId", DBNull.Value);
            this.OpenConnection();
            IDataReader sqlReader = ExecuteReader(DAOConstant.USP_GET_NEWCONFIGVALUES);
            List<iMIXConfigurationInfo> iMIXConfigurationValueList = MapNewConfigValuesBase(sqlReader);
            sqlReader.Close();
            return iMIXConfigurationValueList;
        }
		///changed by latika 
        public List<iMIXRFIDTableWorkflowInfo> GetTableworkFlowinfo(int LocationID)
        {
            DBParameters.Clear();
            AddParameter("@LocationID", LocationID);
            this.OpenConnection();
            IDataReader sqlReader = ExecuteReader("usp_GET_TableFlowinfo");
            List<iMIXRFIDTableWorkflowInfo> iMIXConfigurationValueList = PopulateRFIDTableWorkflowByID(sqlReader);
            sqlReader.Close();
            return iMIXConfigurationValueList;
        }
		/////end
        public List<iMIXConfigurationInfo> MapNewConfigValuesBase(IDataReader sqlReader)
        {
            List<iMIXConfigurationInfo> iMIXConfigurationValueList = new List<iMIXConfigurationInfo>();
            while (sqlReader.Read())
            {
                iMIXConfigurationInfo iMIXConfigurationValue = new iMIXConfigurationInfo()
                {
                    IMIXConfigurationValueId = GetFieldValue(sqlReader, "IMIXConfigurationValueId", 0L),
                    IMIXConfigurationMasterId = GetFieldValue(sqlReader, "IMIXConfigurationMasterId", 0L),
                    ConfigurationValue = GetFieldValue(sqlReader, "ConfigurationValue", string.Empty),
                    SubstoreId = GetFieldValue(sqlReader, "SubstoreId", 0),
                    SyncCode = GetFieldValue(sqlReader, "SyncCode", string.Empty),
                    IsSynced = GetFieldValue(sqlReader, "IsSynced", false),

                };

                iMIXConfigurationValueList.Add(iMIXConfigurationValue);
            }
            return iMIXConfigurationValueList;
        }
////changed by latika for table workflow
        private List<iMIXRFIDTableWorkflowInfo> PopulateRFIDTableWorkflowByID(IDataReader dataReader)
        {
            List<iMIXRFIDTableWorkflowInfo> lstRFIDFlush = new List<iMIXRFIDTableWorkflowInfo>();
            iMIXRFIDTableWorkflowInfo RfidTableWorkflow = null;
            DateTime? pr = null;
            while (dataReader.Read())
            {
                RfidTableWorkflow = new iMIXRFIDTableWorkflowInfo();
                RfidTableWorkflow.FontSize = GetFieldValue(dataReader, "FontSize", 0);
                RfidTableWorkflow.FontStyle = GetFieldValue(dataReader, "FontStyle", string.Empty);
                RfidTableWorkflow.FontFamily = GetFieldValue(dataReader, "FontFamily", string.Empty);
                RfidTableWorkflow.FontWeight = GetFieldValue(dataReader, "FontWeight", string.Empty);
                
                RfidTableWorkflow.LocationID = GetFieldValue(dataReader, "LocationID", 0);
                RfidTableWorkflow.TypesName = GetFieldValue(dataReader, "TypesName", string.Empty);
                RfidTableWorkflow.Position = GetFieldValue(dataReader, "Position", string.Empty);
                RfidTableWorkflow.BackColor = GetFieldValue(dataReader, "BackColor", "#ffffff");
                RfidTableWorkflow.Font = GetFieldValue(dataReader, "Font", "#000000");
                RfidTableWorkflow.MarginLeft = GetFieldValue(dataReader, "MarginLeft", 0);
                RfidTableWorkflow.MarginTop = GetFieldValue(dataReader, "MarginTop", 0);
                RfidTableWorkflow.MarginRight = GetFieldValue(dataReader, "MarginRight", 0);
                RfidTableWorkflow.MarginBottom = GetFieldValue(dataReader, "MarginBottom", 0);
                RfidTableWorkflow.Orientation = GetFieldValue(dataReader, "Orientation", string.Empty);

                lstRFIDFlush.Add(RfidTableWorkflow);
            }
            return lstRFIDFlush;
        }
////end
        public iMIXConfigurationInfo GetOnlineConfigDataById(int ObjConfig, int SubStoreId)
        {
            DBParameters.Clear();
            AddParameter("@ParamObjConfig", ObjConfig);
            AddParameter("@ParamSubStoreId", SubStoreId);
            IDataReader sqlReader = ExecuteReader(DAOConstant.USP_GET_ONLINECONFIGDATABYID);
            List<iMIXConfigurationInfo> iMIXConfigurationValueList = MapiMIXConfigurationInfo(sqlReader);
            sqlReader.Close();
            return iMIXConfigurationValueList.FirstOrDefault();
        }
        private List<iMIXConfigurationInfo> MapiMIXConfigurationInfo(IDataReader sqlReader)
        {
            List<iMIXConfigurationInfo> iMIXConfigurationValueList = new List<iMIXConfigurationInfo>();
            while (sqlReader.Read())
            {
                iMIXConfigurationInfo iMIXConfigurationValue = new iMIXConfigurationInfo()
                {
                    ConfigurationValue = GetFieldValue(sqlReader, "ConfigurationValue", string.Empty),
                };
                iMIXConfigurationValueList.Add(iMIXConfigurationValue);
            }
            return iMIXConfigurationValueList;
        }


        public List<ConfigInfo> GetAllConfig()
        {
            List<ConfigInfo> configValueList = null;
            DBParameters.Clear();
            this.OpenConnection();
            using (IDataReader sqlReader = ExecuteReader(DAOConstant.USP_SEL_DIGIPHOTOCONFIGURATION))
            {
                configValueList = MapConfigvalue(sqlReader);
                sqlReader.Close();
            }
            return configValueList;
        }
        public List<ConfigInfo> MapConfigvalue(IDataReader sqlReader)
        {
            List<ConfigInfo> configValueList = new List<ConfigInfo>();
            while (sqlReader.Read())
            {
                ConfigInfo configValue = new ConfigInfo()
                {
                    ConfigID = GetFieldValue(sqlReader, "ConfigID", 0),
                    SubStoreID = GetFieldValue(sqlReader, "SubStoreID", 0),
                    ConfigKey = GetFieldValue(sqlReader, "ConfigKey", string.Empty),
                    ConfigValue = GetFieldValue(sqlReader, "ConfigValue", string.Empty),
                    MasterID = GetFieldValue(sqlReader, "MasterID", 0),
                };
                configValueList.Add(configValue);
            }
            return configValueList;
        }


        public ConfigurationInfo GetDeafultPath()
        {
            ConfigurationInfo configValueList = null;
            DBParameters.Clear();
            this.OpenConnection();
            using (IDataReader sqlReader = ExecuteReader(DAOConstant.usp_GET_Configurationdata))
            {
                configValueList = MapConfigurationInfoDetails(sqlReader);
                sqlReader.Close();
            };

            return configValueList;
        }

        private ConfigurationInfo MapConfigurationInfoDetails(IDataReader sqlReader)
        {
            ConfigurationInfo configValue = new ConfigurationInfo();
            while (sqlReader.Read())
            {
                configValue = new ConfigurationInfo()
                {
                    DG_Config_pkey = GetFieldValue(sqlReader, "DG_Config_pkey", 0),
                    DG_Hot_Folder_Path = GetFieldValue(sqlReader, "DG_Hot_Folder_Path", string.Empty),
                    DG_Frame_Path = GetFieldValue(sqlReader, "DG_Frame_Path", string.Empty),
                    DG_BG_Path = GetFieldValue(sqlReader, "DG_BG_Path", string.Empty),
                    DG_Mod_Password = GetFieldValue(sqlReader, "DG_Mod_Password", string.Empty),
                    DG_NoOfPhotos = GetFieldValue(sqlReader, "DG_NoOfPhotos", 0),
                    DG_Watermark = GetFieldValue(sqlReader, "DG_Watermark", false),
                    DG_SemiOrder = GetFieldValue(sqlReader, "DG_SemiOrder", false),
                    DG_HighResolution = GetFieldValue(sqlReader, "DG_HighResolution", false),
                    DG_AllowDiscount = GetFieldValue(sqlReader, "DG_AllowDiscount", false),
                    DG_EnableDiscountOnTotal = GetFieldValue(sqlReader, "DG_EnableDiscountOnTotal", false),
                    WiFiStartingNumber = GetFieldValue(sqlReader, "WiFiStartingNumber", 0.0M),
                    FolderStartingNumber = GetFieldValue(sqlReader, "FolderStartingNumber", 0.0M),
                    IsAutoLock = GetFieldValue(sqlReader, "IsAutoLock", false),
                    DG_SemiOrderMain = GetFieldValue(sqlReader, "DG_SemiOrderMain", false),
                    PosOnOff = GetFieldValue(sqlReader, "PosOnOff", false),
                    DG_ReceiptPrinter = GetFieldValue(sqlReader, "DG_ReceiptPrinter", string.Empty),
                    DG_IsAutoRotate = GetFieldValue(sqlReader, "DG_IsAutoRotate", false),
                    DG_Graphics_Path = GetFieldValue(sqlReader, "DG_Graphics_Path", string.Empty),
                    DG_IsCompression = GetFieldValue(sqlReader, "DG_IsCompression", false),
                    DG_IsEnableGroup = GetFieldValue(sqlReader, "DG_IsEnableGroup", false),
                    DG_Substore_Id = GetFieldValue(sqlReader, "DG_Substore_Id", 0),
                    DG_NoOfBillReceipt = GetFieldValue(sqlReader, "DG_NoOfBillReceipt", 0),
                    DG_ChromaColor = GetFieldValue(sqlReader, "DG_ChromaColor", string.Empty),
                    DG_ChromaTolerance = GetFieldValue(sqlReader, "DG_ChromaTolerance", 0.0M),
                    DG_DbBackupPath = GetFieldValue(sqlReader, "DG_DbBackupPath", string.Empty),
                    DG_CleanupTables = GetFieldValue(sqlReader, "DG_CleanupTables", string.Empty),
                    DG_HfBackupPath = GetFieldValue(sqlReader, "DG_HfBackupPath", string.Empty),
                    DG_ScheduleBackup = GetFieldValue(sqlReader, "DG_ScheduleBackup", string.Empty),
                    DG_IsBackupScheduled = GetFieldValue(sqlReader, "DG_IsBackupScheduled", false),
                    DG_Brightness = GetFieldValue(sqlReader, "DG_Brightness", 0.0F),
                    DG_Contrast = GetFieldValue(sqlReader, "DG_Contrast", 0.0F),
                    DG_PageCountGrid = GetFieldValue(sqlReader, "DG_PageCountGrid", 0),
                    DG_PageCountPreview = GetFieldValue(sqlReader, "DG_PageCountPreview", 0),
                    DG_NoOfPhotoIdSearch = GetFieldValue(sqlReader, "DG_NoOfPhotoIdSearch", 0),
                    IsRecursive = GetFieldValue(sqlReader, "IsRecursive", false),
                    IntervalCount = GetFieldValue(sqlReader, "IntervalCount", 0),
                    intervalType = GetFieldValue(sqlReader, "intervalType", 0),
                    DG_MktImgPath = GetFieldValue(sqlReader, "DG_MktImgPath", string.Empty),
                    DG_MktImgTimeInSec = GetFieldValue(sqlReader, "DG_MktImgTimeInSec", 0),
                    EK_SampleImagePath = GetFieldValue(sqlReader, "EK_SampleImagePath", string.Empty),
                    EK_DisplayDuration = GetFieldValue(sqlReader, "EK_DisplayDuration", 0),
                    EK_ScreenStartTime = GetFieldValue(sqlReader, "EK_ScreenStartTime", 0),
                    EK_IsScreenSaverActive = GetFieldValue(sqlReader, "EK_IsScreenSaverActive", false),
                    IsDeleteFromUSB = GetFieldValue(sqlReader, "IsDeleteFromUSB", false),
                    DG_CleanUpDaysBackUp = GetFieldValue(sqlReader, "DG_CleanUpDaysBackUp", 0),
                    FtpIP = GetFieldValue(sqlReader, "FtpIP", string.Empty),
                    FtpUid = GetFieldValue(sqlReader, "FtpUid", string.Empty),
                    FtpPwd = GetFieldValue(sqlReader, "FtpPwd", string.Empty),
                    FtpFolder = GetFieldValue(sqlReader, "FtpFolder", string.Empty),
                };
            }
            return configValue;
        }
        public bool UPDANDINS_ConfigurationData(int NumberofImages, int DefaultCurrency, int SubstoreId, int NoOfReceipt, int PageSizeGrid, int PageSizePreview, int NoOfPhotoIdSearch,
           bool? Iswatermark, bool? IsHighResolution, bool? IsSemiorder, bool? IsAutoRotate, bool? IsLineItemDiscount, bool? IsTotalDiscount, bool? IsPosOnOff, string receipt_PrinterName, bool? IsSemiOrderMain, bool? IsCompression, bool? IsEnableGroup, decimal ChromaTolerance, string SyncCode, string Hotfolder, string FramePath, string BgPath, string Graphics, string ChromaColor, string ModPassword, bool? isExportReportToAnyDrive)
        {
            string Receipt_PrinterName = string.Empty;
            DBParameters.Clear();
            AddParameter("@ParamNumberofImages", NumberofImages);
            AddParameter("@ParamDefaultCurrency", DefaultCurrency);
            AddParameter("@ParamSubstoreId", SubstoreId);
            AddParameter("@ParamNoOfReceipt", NoOfReceipt);
            AddParameter("@ParamPageSizeGrid", PageSizeGrid);
            AddParameter("@ParamPageSizePreview", PageSizePreview);
            AddParameter("@ParamNoOfPhotoIdSearch", NoOfPhotoIdSearch);
            AddParameter("@ParamIswatermark", Iswatermark);
            AddParameter("@ParamIsHighResolution", IsHighResolution);
            AddParameter("@ParamIsSemiorder", IsSemiorder);
            AddParameter("@ParamIsAutoRotate", IsAutoRotate);
            AddParameter("@ParamIsLineItemDiscount", IsLineItemDiscount);
            AddParameter("@ParamIsTotalDiscount", IsTotalDiscount);
            AddParameter("@ParamIsPosOnOff", IsPosOnOff);
            AddParameter("@ParamIsSemiOrderMain", IsSemiOrderMain);
            AddParameter("@ParamIsCompression", IsCompression);
            AddParameter("@ParamIsEnableGroup", IsEnableGroup);
            AddParameter("@ParamChromaTolerance", ChromaTolerance);
            AddParameter("@ParamSyncCode", SyncCode);
            AddParameter("@ParamHotfolder", Hotfolder);
            AddParameter("@ParamFramePath", FramePath);
            AddParameter("@ParamBgPath", BgPath);
            AddParameter("@ParamGraphics", Graphics);
            AddParameter("@ParamChromaColor", ChromaColor);
            //AddParameter("@ParamReceipt_PrinterName", Receipt_PrinterName, ParameterDirection.Output);
            AddParameter("@ParamReceipt_PrinterName", receipt_PrinterName);
            AddParameter("@ParamModPassword", ModPassword);
            AddParameter("@ParamIsExportReportToAnyDrive", isExportReportToAnyDrive);
            ExecuteNonQuery(DAOConstant.USP_UPDANDINS_CONFIGURATIONDATA);
            return true;

        }

        public int DeletedOldImages_ConfigurationData(string deletedOldImages)
        {
            DBParameters.Clear();
            AddParameter("@ParamDeletedOldImages", deletedOldImages);
            int objectId = ExecuteScalarDeletedOldImages(DAOConstant.USP_DeletedOldImages_CONFIGURATIONDATA);
            return objectId;
        }

        public int UPDANDINS_SemiOrderConfigurationData(SemiOrderSettings objSemi)
        {
            string Receipt_PrinterName = string.Empty;
            DBParameters.Clear();
            AddParameter("@ParamDG_SemiOrder_Settings_Pkey", objSemi.DG_SemiOrder_Settings_Pkey);
            AddParameter("@ParamDG_SemiOrder_Settings_AutoBright", objSemi.DG_SemiOrder_Settings_AutoBright);
            AddParameter("@ParamDG_SemiOrder_Settings_AutoBright_Value", objSemi.DG_SemiOrder_Settings_AutoBright_Value);
            AddParameter("@ParamDG_SemiOrder_Settings_AutoContrast", objSemi.DG_SemiOrder_Settings_AutoContrast);
            AddParameter("@ParamDG_SemiOrder_Settings_AutoContrast_Value", objSemi.DG_SemiOrder_Settings_AutoContrast_Value);
            AddParameter("@ParamImageFrame_Horizontal", objSemi.ImageFrame_Horizontal);
            AddParameter("@ParamDG_SemiOrder_Settings_IsImageFrame", objSemi.DG_SemiOrder_Settings_IsImageFrame);
            AddParameter("@ParamDG_SemiOrder_Environment", objSemi.DG_SemiOrder_Environment);
            AddParameter("@ParamDG_SemiOrder_ProductTypeId", objSemi.DG_SemiOrder_ProductTypeId);
            AddParameter("@ParamImageFrame_Vertical", objSemi.ImageFrame_Vertical);
            AddParameter("@ParamBackground_Horizontal", objSemi.Background_Horizontal);
            AddParameter("@ParamBackground_Vertical", objSemi.Background_Vertical);
            AddParameter("@ParamDG_SemiOrder_Settings_IsImageBG", objSemi.DG_SemiOrder_Settings_IsImageBG);
            AddParameter("@ParamGraphics_layer_Horizontal", objSemi.Graphics_layer_Horizontal);
            AddParameter("@ParamGraphics_layer_Vertical", objSemi.Graphics_layer_Vertical);
            AddParameter("@ParamZoomInfo_Horizontal", objSemi.ZoomInfo_Horizontal);
            AddParameter("@ParamZoomInfo_Vertical", objSemi.ZoomInfo_Vertical);
            AddParameter("@ParamDG_SubStoreId", objSemi.DG_SubStoreId);
            AddParameter("@ParamDG_SemiOrder_IsPrintActive", objSemi.DG_SemiOrder_IsPrintActive);
            AddParameter("@ParamDG_SemiOrder_IsCropActive", objSemi.DG_SemiOrder_IsCropActive);
            AddParameter("@ParamVerticalCropValues", objSemi.VerticalCropValues);
            AddParameter("@ParamHorizontalCropValues", objSemi.HorizontalCropValues);
            AddParameter("@ParamDG_LocationId", objSemi.DG_LocationId);
            AddParameter("@ParamChromaColor", objSemi.ChromaColor);
            AddParameter("@ParamColorCode", objSemi.ColorCode);
            AddParameter("@ParamClrTolerance", objSemi.ClrTolerance);
            AddParameter("@ParamTextLogo_Horizontal", objSemi.TextLogo_Horizontal);
            AddParameter("@ParamTextLogo_Vertical", objSemi.TextLogo_Vertical);
            AddParameter("@ParamSemiOrderSettingsIDOut", objSemi.DG_SemiOrder_Settings_Pkey, ParameterDirection.InputOutput);
            
            ExecuteNonQuery(DAOConstant.USP_UPDANDINS_SEMIORDERSETTINGS);
            int objectId = (int)GetOutParameterValue("@ParamSemiOrderSettingsIDOut");
            return objectId;

        }
        /// <summary>
        /// Get Delivery Note Status for specific sub store
        /// </summary>
        /// <param name="SubstoreId"></param>
        /// <returns></returns>
        public bool GetDeliveryNoteStatus(int SubstoreId)
        {
            bool isDeliveryNote = false;
            DBParameters.Clear();
            AddParameter("@SubstoreId", SubstoreId);           
            isDeliveryNote = Convert.ToBoolean(ExecuteScalar(DAOConstant.USP_GETDELIVERYNOTESTATUS));
            return isDeliveryNote;

        }
        public List<iMixConfigurationLocationInfo> GetLocationWiseConfigParams(int SubStoreId)
        {
            DBParameters.Clear();
            AddParameter("@ParamSubstoreID", SubStoreId);
            IDataReader sqlReader = ExecuteReader(DAOConstant.USP_GET_PreviewWallSubstoreWiseData);
            List<iMixConfigurationLocationInfo> lstLocationWiseConfigParams = MapLocationWiseConfigParams(sqlReader);
            sqlReader.Close();
            return lstLocationWiseConfigParams;
        }

        public void DeleteLocationWiseConfigParams(int LocationId)
        {
            DBParameters.Clear();
            AddParameter("@LocationID", LocationId);
            ExecuteScalar(DAOConstant.USP_DEL_PreviewWallLocationWiseData);
        }
        private List<iMixConfigurationLocationInfo> MapLocationWiseConfigParams(IDataReader sqlReader)
        {
            List<iMixConfigurationLocationInfo> lstLocationWiseConfigParams = new List<iMixConfigurationLocationInfo>();
            while (sqlReader.Read())
            {
                iMixConfigurationLocationInfo LocationWiseConfigParams = new iMixConfigurationLocationInfo()
                {
                    IMIXConfigurationMasterId = GetFieldValue(sqlReader, "IMIXConfigurationMasterId", 0L),
                    ConfigurationValue = GetFieldValue(sqlReader, "ConfigurationValue", string.Empty),
                    LocationId = GetFieldValue(sqlReader, "LocationId", 0),
                    SubstoreId = GetFieldValue(sqlReader, "SubstoreId", 0),
                };
                lstLocationWiseConfigParams.Add(LocationWiseConfigParams);
            }
            return lstLocationWiseConfigParams;
        }

        public List<iMIXStoreConfigurationInfo> GetAllReportConfiguration()
        {

            DBParameters.Clear();
            this.OpenConnection();
            IDataReader sqlReader = ExecuteReader(DAOConstant.usp_SEL_GetStoreConfigData);
            List<iMIXStoreConfigurationInfo> configValueList = MapReportConfiguration(sqlReader);
            sqlReader.Close();
            return configValueList;
        }

        private List<iMIXStoreConfigurationInfo> MapReportConfiguration(IDataReader sqlReader)
        {
            List<iMIXStoreConfigurationInfo> objectList = new List<iMIXStoreConfigurationInfo>();
            while (sqlReader.Read())
            {
                iMIXStoreConfigurationInfo objectValue = new iMIXStoreConfigurationInfo
                {
                    IMIXConfigurationMasterId = GetFieldValue(sqlReader, "IMIXConfigurationMasterId", 0L),
                    ConfigurationValue = GetFieldValue(sqlReader, "ConfigurationValue", string.Empty),
                    SyncCode = GetFieldValue(sqlReader, "SyncCode", string.Empty),
                    IsSynced = GetFieldValue(sqlReader, "IsSynced", false),

                };

                objectList.Add(objectValue);
            }
            return objectList;
        }

        public List<ReportTypeDetails> GetReportTypes()
        {
            List<ReportTypeDetails> reportType;
            DBParameters.Clear();
            using (IDataReader sqlReader = ExecuteReader(DAOConstant.GetReportTypes))
            {
                reportType = MapReportTypes(sqlReader);
            };
            return reportType;

        }

        private List<ReportTypeDetails> MapReportTypes(IDataReader sqlReader)
        {
            List<ReportTypeDetails> objectList = new List<ReportTypeDetails>();
            while (sqlReader.Read())
            {
                ReportTypeDetails objectValue = new ReportTypeDetails
                {
                    Id = GetFieldValue(sqlReader, "id", 0),
                    ReportTypeName = GetFieldValue(sqlReader, "ReportType", string.Empty),
                    IsActive = GetFieldValue(sqlReader, "IsActive", false),
                    ReportLabel = GetFieldValue(sqlReader, "ReportLabel", string.Empty)
                };

                objectList.Add(objectValue);
            }
            return objectList;
        }


        public bool InsertReportConfig(DataTable dt)
        {
            DBParameters.Clear();
            AddParameter("@ConfigTable", dt);
            ExecuteNonQuery(DAOConstant.SaveUpdateNewStoreConfig);
            return true;
        }
        public bool SaveExportReportLogDetails(ExportServiceLog exportServiceLog)
        {
            DBParameters.Clear();
            AddParameter("@loggingTime", exportServiceLog.EventTime);
            AddParameter("@reportType", exportServiceLog.ReportType);
            AddParameter("@reportsent", exportServiceLog.ReportSent);
            AddParameter("@errorDetails", exportServiceLog.ErrorDetails);
            AddParameter("@fileName", exportServiceLog.ExportFile);
            AddParameter("@exportPath", exportServiceLog.ExportPath);
            ExecuteNonQuery(DAOConstant.SaveExportReportStatusLog);
            return true;
        }
        public List<ExportServiceLog> GetExportServiceLogs()
        {
            List<ExportServiceLog> reportLogs;
            DBParameters.Clear();
            using (IDataReader sqlReader = ExecuteReader(DAOConstant.GetExportReportLog))
            {
                reportLogs = MapExportServiceLog(sqlReader);
            };
            return reportLogs;

        }
        private List<ExportServiceLog> MapExportServiceLog(IDataReader sqlReader)
        {
            List<ExportServiceLog> objectList = new List<ExportServiceLog>();
            while (sqlReader.Read())
            {
                ExportServiceLog objectValue = new ExportServiceLog
                {

                    EventTime = GetFieldValue(sqlReader, "LoggingTime", DateTime.Now),
                    ErrorDetails = GetFieldValue(sqlReader, "ErrorDetails", string.Empty),
                    ExportFile = GetFieldValue(sqlReader, "ExportFile", string.Empty),
                    ExportPath = GetFieldValue(sqlReader, "ExportPath", string.Empty),
                    ReportSent = GetFieldValue(sqlReader, "ReportSent", false),
                    ReportType = GetFieldValue(sqlReader, "ReportType", string.Empty),
                };

                objectList.Add(objectValue);
            }
            return objectList;
        }

        public bool UpdateReportTypes(List<ReportTypeDetails> reportTypes)
        {
            foreach (var reportType in reportTypes)
            {
                DBParameters.Clear();
                AddParameter("@reportId", reportType.Id);
                AddParameter("@isActive", reportType.IsActive);
                ExecuteNonQuery(DAOConstant.UpdateReportType);
            }
            return true;
        }

        public bool CheckDummyScan(long photographerID)
        {
            bool result = false;
            DBParameters.Clear();
            AddParameter("@photographerID", photographerID);
            result = Convert.ToBoolean(ExecuteScalar(DAOConstant.USP_VerifyDummyScanForCaptureDownload));
            return result;
        }


        public string GetSubStoreNameBySuubStoreId(int subStoreId)
        {
            ErrorHandler.ErrorHandler.LogFileWrite("Get SubstoreName By Sub store id : 725  : Id : " + subStoreId) ;
            string substoreName = string.Empty;
            DBParameters.Clear();
            AddParameter("@SubstoreId", subStoreId);

            IDataReader sqlReader = ExecuteReader(DAOConstant.USP_GET_SUBSTORENAME);
            while (sqlReader.Read())
            {
                substoreName = GetFieldValue(sqlReader, "DG_SubStore_Name", string.Empty);
            }
            sqlReader.Close();
            return substoreName;
        }

        public void DeleteLocationWiseConfigParamsGumbleRide(int LocationId)
        {
            DBParameters.Clear();
            AddParameter("@LocationID", LocationId);
            ExecuteScalar("USP_DeleteLocationWise_GumballRide");
        }
        public bool IsLocationRFIDEnabled(int locationId)
        {
            bool result = false;
            DBParameters.Clear();
            AddParameter("@locationId", locationId);
            result = Convert.ToBoolean(ExecuteScalar(DAOConstant.RFIDEnabledColumn));
            return result;
        }
        public List<ConfigurationInfo> GetDeafultPathlist()
        {
            List<ConfigurationInfo> configValueList = new List<ConfigurationInfo>();
            DBParameters.Clear();
            this.OpenConnection();
            using (IDataReader sqlReader = ExecuteReader(DAOConstant.usp_GET_Configurationdata))
            {
                configValueList = MapConfigurationInfoDetailslist(sqlReader);
                sqlReader.Close();
            };

            return configValueList;
        }
        private List<ConfigurationInfo> MapConfigurationInfoDetailslist(IDataReader sqlReader)
        {
            List<ConfigurationInfo> lstconfigValue = new List<ConfigurationInfo>();// ConfigurationInfo();
            ConfigurationInfo configValue = new ConfigurationInfo();
            while (sqlReader.Read())
            {
                configValue = new ConfigurationInfo()
                {
                    DG_Config_pkey = GetFieldValue(sqlReader, "DG_Config_pkey", 0),
                    DG_Hot_Folder_Path = GetFieldValue(sqlReader, "DG_Hot_Folder_Path", string.Empty),
                    DG_Frame_Path = GetFieldValue(sqlReader, "DG_Frame_Path", string.Empty),
                    DG_BG_Path = GetFieldValue(sqlReader, "DG_BG_Path", string.Empty),
                    DG_Mod_Password = GetFieldValue(sqlReader, "DG_Mod_Password", string.Empty),
                    DG_NoOfPhotos = GetFieldValue(sqlReader, "DG_NoOfPhotos", 0),
                    DG_Watermark = GetFieldValue(sqlReader, "DG_Watermark", false),
                    DG_SemiOrder = GetFieldValue(sqlReader, "DG_SemiOrder", false),
                    DG_HighResolution = GetFieldValue(sqlReader, "DG_HighResolution", false),
                    DG_AllowDiscount = GetFieldValue(sqlReader, "DG_AllowDiscount", false),
                    DG_EnableDiscountOnTotal = GetFieldValue(sqlReader, "DG_EnableDiscountOnTotal", false),
                    WiFiStartingNumber = GetFieldValue(sqlReader, "WiFiStartingNumber", 0.0M),
                    FolderStartingNumber = GetFieldValue(sqlReader, "FolderStartingNumber", 0.0M),
                    IsAutoLock = GetFieldValue(sqlReader, "IsAutoLock", false),
                    DG_SemiOrderMain = GetFieldValue(sqlReader, "DG_SemiOrderMain", false),
                    PosOnOff = GetFieldValue(sqlReader, "PosOnOff", false),
                    DG_ReceiptPrinter = GetFieldValue(sqlReader, "DG_ReceiptPrinter", string.Empty),
                    DG_IsAutoRotate = GetFieldValue(sqlReader, "DG_IsAutoRotate", false),
                    DG_Graphics_Path = GetFieldValue(sqlReader, "DG_Graphics_Path", string.Empty),
                    DG_IsCompression = GetFieldValue(sqlReader, "DG_IsCompression", false),
                    DG_IsEnableGroup = GetFieldValue(sqlReader, "DG_IsEnableGroup", false),
                    DG_Substore_Id = GetFieldValue(sqlReader, "DG_Substore_Id", 0),
                    DG_NoOfBillReceipt = GetFieldValue(sqlReader, "DG_NoOfBillReceipt", 0),
                    DG_ChromaColor = GetFieldValue(sqlReader, "DG_ChromaColor", string.Empty),
                    DG_ChromaTolerance = GetFieldValue(sqlReader, "DG_ChromaTolerance", 0.0M),
                    DG_DbBackupPath = GetFieldValue(sqlReader, "DG_DbBackupPath", string.Empty),
                    DG_CleanupTables = GetFieldValue(sqlReader, "DG_CleanupTables", string.Empty),
                    DG_HfBackupPath = GetFieldValue(sqlReader, "DG_HfBackupPath", string.Empty),
                    DG_ScheduleBackup = GetFieldValue(sqlReader, "DG_ScheduleBackup", string.Empty),
                    DG_IsBackupScheduled = GetFieldValue(sqlReader, "DG_IsBackupScheduled", false),
                    DG_Brightness = GetFieldValue(sqlReader, "DG_Brightness", 0.0F),
                    DG_Contrast = GetFieldValue(sqlReader, "DG_Contrast", 0.0F),
                    DG_PageCountGrid = GetFieldValue(sqlReader, "DG_PageCountGrid", 0),
                    DG_PageCountPreview = GetFieldValue(sqlReader, "DG_PageCountPreview", 0),
                    DG_NoOfPhotoIdSearch = GetFieldValue(sqlReader, "DG_NoOfPhotoIdSearch", 0),
                    IsRecursive = GetFieldValue(sqlReader, "IsRecursive", false),
                    IntervalCount = GetFieldValue(sqlReader, "IntervalCount", 0),
                    intervalType = GetFieldValue(sqlReader, "intervalType", 0),
                    DG_MktImgPath = GetFieldValue(sqlReader, "DG_MktImgPath", string.Empty),
                    DG_MktImgTimeInSec = GetFieldValue(sqlReader, "DG_MktImgTimeInSec", 0),
                    EK_SampleImagePath = GetFieldValue(sqlReader, "EK_SampleImagePath", string.Empty),
                    EK_DisplayDuration = GetFieldValue(sqlReader, "EK_DisplayDuration", 0),
                    EK_ScreenStartTime = GetFieldValue(sqlReader, "EK_ScreenStartTime", 0),
                    EK_IsScreenSaverActive = GetFieldValue(sqlReader, "EK_IsScreenSaverActive", false),
                    IsDeleteFromUSB = GetFieldValue(sqlReader, "IsDeleteFromUSB", false),
                    DG_CleanUpDaysBackUp = GetFieldValue(sqlReader, "DG_CleanUpDaysBackUp", 0),
                    FtpIP = GetFieldValue(sqlReader, "FtpIP", string.Empty),
                    FtpUid = GetFieldValue(sqlReader, "FtpUid", string.Empty),
                    FtpPwd = GetFieldValue(sqlReader, "FtpPwd", string.Empty),
                    FtpFolder = GetFieldValue(sqlReader, "FtpFolder", string.Empty),
                };
                lstconfigValue.Add(configValue);
            }
            return lstconfigValue;
        }

         public List<iMixConfigurationLocationInfo> GetAllLocationWiseConfigParams()
         {
             DBParameters.Clear();
             IDataReader sqlReader = ExecuteReader(DAOConstant.USP_GET_GETALLLOCATIONWISECONFIGPARAMS);
             List<iMixConfigurationLocationInfo> lstLocationWiseConfigParams = MapLocationWiseConfigParams(sqlReader);
             sqlReader.Close();
             return lstLocationWiseConfigParams;
         }


         public List<ProductTypeInfo> GetWaterMarkProduct()
         {
             DBParameters.Clear();
             IDataReader sqlReader = ExecuteReader("GETWaterMarkProduct");
             List<ProductTypeInfo> productInfo = MapProductConfig(sqlReader);
             sqlReader.Close();
             return productInfo;
         }

         private List<ProductTypeInfo> MapProductConfig(IDataReader sqlReader)
         {
             List<ProductTypeInfo> listProductInfo = new List<ProductTypeInfo>();
             while (sqlReader.Read())
             {
                 ProductTypeInfo productInfo = new ProductTypeInfo()
                 {
                     DG_Orders_ProductType_pkey = GetFieldValue(sqlReader, "DG_Orders_ProductType_pkey", 0),
                     DG_Orders_ProductType_Name = GetFieldValue(sqlReader, "DG_Orders_ProductType_Name", string.Empty),
                     DG_Product_Pricing_ProductPrice = GetFieldValue(sqlReader, "DG_Product_Pricing_ProductPrice", 0.0D)
                 };
                 listProductInfo.Add(productInfo);
             }
             return listProductInfo;
         }

         public List<iMixConfigurationLocationInfo> GetLocationWiseConfigParams()
         {
             DBParameters.Clear();
             AddParameter("@ParamSubstoreID", null);
             IDataReader sqlReader = ExecuteReader(DAOConstant.USP_GET_PreviewWallSubstoreWiseData);
             List<iMixConfigurationLocationInfo> lstLocationWiseConfigParams = MapLocationWiseConfigParams(sqlReader);
             sqlReader.Close();
             return lstLocationWiseConfigParams;
         }

         public List<WaterMarkTagsUpload> GetCardIdentifier(int locationId)
         {
             DBParameters.Clear();
             AddParameter("@LocationId", locationId);
             IDataReader sqlReader = ExecuteReader("GetIdentifierForWaterMarkUpload");
             List<WaterMarkTagsUpload> lstCardIdentifier = MapCardIdentifier(sqlReader);
             sqlReader.Close();
             return lstCardIdentifier;
         }

         private List<WaterMarkTagsUpload> MapCardIdentifier(IDataReader sqlReader)
         {
             List<WaterMarkTagsUpload> lstLocationWiseConfigParams = new List<WaterMarkTagsUpload>();
             while (sqlReader.Read())
             {
                 WaterMarkTagsUpload LocationWiseConfigParams = new WaterMarkTagsUpload()
                 {
                     IMIXImageAssociationId = GetFieldValue(sqlReader, "IMIXImageAssociationId", 0L),
                     CardUniqueIdentifier = GetFieldValue(sqlReader, "CardUniqueIdentifier", string.Empty),
                     IMIXCardTypeId = GetFieldValue(sqlReader, "IMIXCardTypeId", 0),
                     PhotoId = GetFieldValue(sqlReader, "PhotoId", 0),
                     IsOrdered = GetFieldValue(sqlReader, "IsOrdered", 0),
                     ImageIdentificationType = GetFieldValue(sqlReader, "ImageIdentificationType", 0),

                 };
                 lstLocationWiseConfigParams.Add(LocationWiseConfigParams);
             }
             return lstLocationWiseConfigParams;
         }

        public bool UpdateOrderStatus(string IMIXImageAssociationIds)
        {
            bool Value = false;
            DBParameters.Clear();
            AddParameter("@ParamValue", Value, ParameterDirection.Output);
            AddParameter("@ParamIMIXImageAssociationIds", SetNullStringValue(IMIXImageAssociationIds));
            ExecuteNonQuery("usp_UPD_WaterMarkOnlineOrder");
            bool objectId = (bool)GetOutParameterValue("@ParamValue");
            return objectId;
        }


        public List<ManualDownloadPosLocationAssociation> GetPosLocationMapping(string machineName)
        {
            DBParameters.Clear();
            AddParameter("@machineName", machineName);
            IDataReader sqlReader = ExecuteReader("usp_GetPosLocationMapping");
            List<ManualDownloadPosLocationAssociation> lstPosLocationAssociation = MapPosLocationMapping(sqlReader);
            sqlReader.Close();
            return lstPosLocationAssociation;
        }

        private List<ManualDownloadPosLocationAssociation> MapPosLocationMapping(IDataReader sqlReader)
        {
            List<ManualDownloadPosLocationAssociation> objectList = new List<ManualDownloadPosLocationAssociation>();
            while (sqlReader.Read())
            {
                ManualDownloadPosLocationAssociation objectValue = new ManualDownloadPosLocationAssociation
                {

                    PoslocationId = GetFieldValue(sqlReader, "PosLocationId", 0),
                    PosName = GetFieldValue(sqlReader, "PosName", string.Empty),
                    LocationId = GetFieldValue(sqlReader, "LocationId", 0),
                    LocationName = GetFieldValue(sqlReader, "LocationName", string.Empty)
                };

                objectList.Add(objectValue);
            }
            return objectList;
        }
        ////added by latika for table workflow
        public void DeleteTableFlowByID(int LocationId)
        {
            DBParameters.Clear();
            AddParameter("@LocationID", LocationId);
            ExecuteScalar("USP_DeleteRfidTableFlowByID");
        }
        ///end
        ////////////////change made by latika to get datetime of server
        public string GetDBDateTime()
        {

            string sqlDBdatetime = "";
            DBParameters.Clear();
            IDataReader sqlReader = ExecuteReader("USP_GetDBdatetime");
            while (sqlReader.Read())
            {
                sqlDBdatetime = GetFieldValue(sqlReader, "DbDatetime", string.Empty);
            }
            sqlReader.Close();

            return sqlDBdatetime;
        }
        ////////////end
    }
}