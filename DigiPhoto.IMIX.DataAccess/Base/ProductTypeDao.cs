﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DigiPhoto.IMIX.Model;
using System.Data;

namespace DigiPhoto.IMIX.DataAccess
{
    public class ProductTypeDao : BaseDataAccess
    {
        #region Constructor
        public ProductTypeDao(BaseDataAccess baseaccess)
            : base(baseaccess)
        { }
        public ProductTypeDao()
        { }
        #endregion
        public List<ProductTypeInfo> SelectProductType(int? ProductTypeId, bool? IsPackage,
            string OrdersProductTypeName)
        {
            DBParameters.Clear();
            AddParameter("@ParamProductTypeId", SetNullIntegerValue(ProductTypeId));
            AddParameter("@ParamIsPackage", SetNullBoolValue(IsPackage));
            AddParameter("@ParamDG_Orders_ProductType_Name", SetNullStringValue(OrdersProductTypeName));

            IDataReader sqlReader = ExecuteReader(DAOConstant.USP_SEL_PRODUCTTYPE);
            List<ProductTypeInfo> Orders_ProductTypeList = MapProductTypeInfo(sqlReader);
            sqlReader.Close();
            return Orders_ProductTypeList;
        }
        private List<ProductTypeInfo> MapProductTypeInfo(IDataReader sqlReader)
        {
            List<ProductTypeInfo> Orders_ProductTypeList = new List<ProductTypeInfo>();
            while (sqlReader.Read())
            {
                ProductTypeInfo Orders_ProductType = new ProductTypeInfo()
                {
                    DG_Orders_ProductType_pkey = GetFieldValue(sqlReader, "DG_Orders_ProductType_pkey", 0),
                    DG_Orders_ProductType_Name = GetFieldValue(sqlReader, "DG_Orders_ProductType_Name", string.Empty),
                    DG_Orders_ProductType_Desc = GetFieldValue(sqlReader, "DG_Orders_ProductType_Desc", string.Empty),
                    DG_Orders_ProductType_IsBundled = GetFieldValue(sqlReader, "DG_Orders_ProductType_IsBundled", false),
                    DG_Orders_ProductType_DiscountApplied = GetFieldValue(sqlReader, "DG_Orders_ProductType_DiscountApplied", false),
                    DG_Orders_ProductType_Image = GetFieldValue(sqlReader, "DG_Orders_ProductType_Image", string.Empty),
                    DG_IsPackage = GetFieldValue(sqlReader, "DG_IsPackage", false),
                    DG_MaxQuantity = GetFieldValue(sqlReader, "DG_MaxQuantity", 0),
                    DG_Orders_ProductType_Active = GetFieldValue(sqlReader, "DG_Orders_ProductType_Active", false),
                    DG_IsActive = GetFieldValue(sqlReader, "DG_IsActive", false),
                    DG_IsAccessory = GetFieldValue(sqlReader, "DG_IsAccessory", false),
                    DG_IsPrimary = GetFieldValue(sqlReader, "DG_IsPrimary", false),
                    DG_Orders_ProductCode = GetFieldValue(sqlReader, "DG_Orders_ProductCode", string.Empty),
                    DG_Orders_ProductNumber = GetFieldValue(sqlReader, "DG_Orders_ProductNumber", 0),
                    DG_IsBorder = GetFieldValue(sqlReader, "DG_IsBorder", false),
                    SyncCode = GetFieldValue(sqlReader, "SyncCode", string.Empty),
                    IsSynced = GetFieldValue(sqlReader, "IsSynced", false),
                    DG_IsTaxEnabled = GetFieldValue(sqlReader, "IsTaxEnabled", false),
                    DG_IsWaterMarkIncluded = GetFieldValue(sqlReader, "DG_IsWaterMarked", false),
                    DG_SubStore_pkey = GetFieldValue(sqlReader, "DG_SubStore_pkey", 0),
                    #region Added by Ajay Sinha
                    IsPanorama = GetFieldValue(sqlReader, "IsPanorama", false)
                    #endregion
};

                    Orders_ProductTypeList.Add(Orders_ProductType);
            }
            return Orders_ProductTypeList;
        }



        public List<ProductTypeInfo> GetOrdersProductTypeByPackage(bool IsPackage)
        {
            DBParameters.Clear();
            AddParameter("@ParamIsPackage", IsPackage);
            IDataReader sqlReader = ExecuteReader(DAOConstant.USP_GET_ORDERSPRODUCTTYPEBYISPACKAGE);
            List<ProductTypeInfo> PackageList = MapProductType(sqlReader);
            sqlReader.Close();
            return PackageList;
        }

        private List<ProductTypeInfo> MapProductType(IDataReader sqlReader)
        {
            List<ProductTypeInfo> ProductTypeList = new List<ProductTypeInfo>();
            while (sqlReader.Read())
            {
                ProductTypeInfo ProductTypeValue = new ProductTypeInfo()
                {
                    DG_Orders_ProductType_pkey = GetFieldValue(sqlReader, "DG_Orders_ProductType_pkey", 0),
                    DG_Orders_ProductType_Name = GetFieldValue(sqlReader, "DG_Orders_ProductType_Name", string.Empty),
                    DG_Orders_ProductType_Desc = GetFieldValue(sqlReader, "DG_Orders_ProductType_Desc", string.Empty),
                    DG_Orders_ProductType_IsBundled = GetFieldValue(sqlReader, "DG_Orders_ProductType_IsBundled", false),
                    DG_Orders_ProductType_DiscountApplied = GetFieldValue(sqlReader, "DG_Orders_ProductType_DiscountApplied", false),
                    DG_Orders_ProductType_Image = GetFieldValue(sqlReader, "DG_Orders_ProductType_Image", string.Empty),
                    DG_IsPackage = GetFieldValue(sqlReader, "DG_IsPackage", false),
                    DG_MaxQuantity = GetFieldValue(sqlReader, "DG_MaxQuantity", 0),
                    DG_Orders_ProductType_Active = GetFieldValue(sqlReader, "DG_Orders_ProductType_Active", false),
                    DG_IsActive = GetFieldValue(sqlReader, "DG_IsActive", false),
                    DG_IsAccessory = GetFieldValue(sqlReader, "DG_IsAccessory", false),
                    DG_IsPrimary = GetFieldValue(sqlReader, "DG_IsPrimary", false),
                    DG_Orders_ProductCode = GetFieldValue(sqlReader, "DG_Orders_ProductCode", string.Empty),
                    DG_Orders_ProductNumber = GetFieldValue(sqlReader, "DG_Orders_ProductNumber", 0),
                    DG_IsBorder = GetFieldValue(sqlReader, "DG_IsBorder", false),
                    SyncCode = GetFieldValue(sqlReader, "SyncCode", string.Empty),
                    IsSynced = GetFieldValue(sqlReader, "IsSynced", false),
                    DG_IsWaterMarkIncluded = GetFieldValue(sqlReader, "DG_IsWaterMarked", false),
                    DG_SubStore_pkey = GetFieldValue(sqlReader, "DG_SubStore_pkey", 0)
                };

                ProductTypeList.Add(ProductTypeValue);
            }
            return ProductTypeList;
        }



        public ProductTypeInfo GetProductPricing(int ProductPricingProductType)
        {
            DBParameters.Clear();
            AddParameter("@ParamProductPricingProductType", ProductPricingProductType);

            IDataReader sqlReader = ExecuteReader(DAOConstant.USP_GET_PRODUCTPRICING);
            List<ProductTypeInfo> ProductPricingList = MapProductPricingBase(sqlReader);
            sqlReader.Close();
            return ProductPricingList.FirstOrDefault();
        }
        private List<ProductTypeInfo> MapProductPricingBase(IDataReader sqlReader)
        {
            List<ProductTypeInfo> ProductPricingList = new List<ProductTypeInfo>();
            while (sqlReader.Read())
            {
                ProductTypeInfo Product_Pricing = new ProductTypeInfo()
                {
                    DG_Product_Pricing_ProductPrice = GetFieldValue(sqlReader, "DG_Product_Pricing_ProductPrice", 0.0D),
                };
                ProductPricingList.Add(Product_Pricing);
            }
            return ProductPricingList;
        }



        public ProductTypeInfo GetProductTypeDataByKey(int ProductTypekey)
        {
            DBParameters.Clear();
            AddParameter("@ParamProductTypekey", ProductTypekey);
            IDataReader sqlReader = ExecuteReader(DAOConstant.USP_GET_PRODUCTTYPEDATA);
            List<ProductTypeInfo> Orders_ProductTypeList = MapOrdersProductTypeBase(sqlReader);
            sqlReader.Close();
            return Orders_ProductTypeList.FirstOrDefault();
        }
        private List<ProductTypeInfo> MapOrdersProductTypeBase(IDataReader sqlReader)
        {
            List<ProductTypeInfo> Orders_ProductTypeList = new List<ProductTypeInfo>();
            while (sqlReader.Read())
            {
                ProductTypeInfo Orders_ProductType = new ProductTypeInfo()
                {
                    DG_Orders_ProductType_pkey = GetFieldValue(sqlReader, "DG_Orders_ProductType_pkey", 0),
                    DG_Orders_ProductType_Name = GetFieldValue(sqlReader, "DG_Orders_ProductType_Name", string.Empty),
                    DG_Orders_ProductType_Desc = GetFieldValue(sqlReader, "DG_Orders_ProductType_Desc", string.Empty),
                    DG_Orders_ProductType_IsBundled = GetFieldValue(sqlReader, "DG_Orders_ProductType_IsBundled", false),
                    DG_Orders_ProductType_DiscountApplied = GetFieldValue(sqlReader, "DG_Orders_ProductType_DiscountApplied", false),
                    DG_IsPackage = GetFieldValue(sqlReader, "DG_IsPackage", false),
                    DG_MaxQuantity = GetFieldValue(sqlReader, "DG_MaxQuantity", 0),
                    DG_IsActive = GetFieldValue(sqlReader, "DG_IsActive", false),
                    DG_IsAccessory = GetFieldValue(sqlReader, "DG_IsAccessory", false),
                    DG_IsTaxEnabled = GetFieldValue(sqlReader, "IsTaxEnabled", false),
                    DG_Orders_ProductCode = GetFieldValue(sqlReader, "DG_Orders_ProductCode", string.Empty),
                    DG_Orders_ProductNumber = GetFieldValueIntNull(sqlReader, "DG_Orders_ProductNumber", 0),
                    DG_Product_Pricing_ProductPrice =Math.Round(GetFieldValue(sqlReader, "DG_Product_Pricing_ProductPrice", 0.0F),2),
                    DG_Product_Pricing_Currency_ID = GetFieldValue(sqlReader, "DG_Product_Pricing_Currency_ID", 0),
                    DG_IsWaterMarkIncluded = GetFieldValue(sqlReader, "DG_IsWaterMarked", false),
                    DG_SubStore_pkey = GetFieldValue(sqlReader, "DG_SubStore_pkey", 0),////changed by latika for AR Personalised
                    IsPersonalizedAR = GetFieldValue(sqlReader, "IsPersonalizedAR", false)

                };
                Orders_ProductTypeList.Add(Orders_ProductType);
            }
            return Orders_ProductTypeList;
        }

        public void UpdateBorderImages(bool chk4Large, bool chkUniq4, bool chk4small, bool chk3by3, bool chkUniq4SW)
        {
            DBParameters.Clear();
            AddParameter("@Paramchk4Large", chk4Large);
            AddParameter("@ParamchkUniq4", chkUniq4);
            AddParameter("@Paramchk4small", chk4small);
            AddParameter("@Paramchk3by3", chk3by3);
            AddParameter("@ParamchkUniq4SW", chkUniq4SW);
            ExecuteNonQuery(DAOConstant.USP_UPD_BORDERIMAGES);
        }
        //public List<ProductTypeInfo> GetActiveProductTypeData()
        //{
        //    DBParameters.Clear();

        //    IDataReader sqlReader = ExecuteReader(DAOConstant.USP_GET_ACTIVEPRODUCTTYPEDATA);
        //    List<ProductTypeInfo> Orders_ProductTypeList = MapOrdersProductType(sqlReader);
        //    sqlReader.Close();
        //    return Orders_ProductTypeList;
        //}
        //private List<ProductTypeInfo> MapOrdersProductType(IDataReader sqlReader)
        //{
        //    List<ProductTypeInfo> Orders_ProductTypeList = new List<ProductTypeInfo>();
        //    while (sqlReader.Read())
        //    {
        //        ProductTypeInfo Orders_ProductType = new ProductTypeInfo()
        //        {
        //            DG_Orders_ProductType_pkey = GetFieldValue(sqlReader, "DG_Orders_ProductType_pkey", 0),
        //            DG_Orders_ProductType_Name = GetFieldValue(sqlReader, "DG_Orders_ProductType_Name", string.Empty),
        //            DG_Orders_ProductType_Desc = GetFieldValue(sqlReader, "DG_Orders_ProductType_Desc", string.Empty),
        //            DG_Orders_ProductType_IsBundled = GetFieldValue(sqlReader, "DG_Orders_ProductType_IsBundled", false),
        //            DG_Orders_ProductType_DiscountApplied = GetFieldValue(sqlReader, "DG_Orders_ProductType_DiscountApplied", false),
        //            DG_Orders_ProductType_Image = GetFieldValue(sqlReader, "DG_Orders_ProductType_Image", string.Empty),
        //            DG_IsPackage = GetFieldValue(sqlReader, "DG_IsPackage", false),
        //            DG_MaxQuantity = GetFieldValue(sqlReader, "DG_MaxQuantity", 0),
        //            DG_Orders_ProductType_Active = GetFieldValue(sqlReader, "DG_Orders_ProductType_Active", false),
        //            DG_IsActive = GetFieldValue(sqlReader, "DG_IsActive", false),
        //            DG_IsAccessory = GetFieldValue(sqlReader, "DG_IsAccessory", false),
        //            DG_IsPrimary = GetFieldValue(sqlReader, "DG_IsPrimary", false),
        //            DG_Orders_ProductCode = GetFieldValue(sqlReader, "DG_Orders_ProductCode", string.Empty),
        //            DG_Orders_ProductNumber = GetFieldValue(sqlReader, "DG_Orders_ProductNumber", 0),
        //            DG_IsBorder = GetFieldValue(sqlReader, "DG_IsBorder", false),
        //            SyncCode = GetFieldValue(sqlReader, "SyncCode", string.Empty),
        //            IsSynced = GetFieldValue(sqlReader, "IsSynced", false),

        //        };

        //        Orders_ProductTypeList.Add(Orders_ProductType);
        //    }
        //    return Orders_ProductTypeList;
        //}
		//// changed by latika for AR Personalised
        public int UpdAndInsProductTypeInfo(int ProductTypeId, int StoreId, int UserId, int? IsInvisible, bool? IsDiscount,
            bool? IsPackage, bool? IsActive, bool? IsAccessory, bool? IsTaxIncluded, string ProductTypeName, 
            string ProductTypeDesc, string ProductPrice, string Productcode, string SyncCode, string SyncodeforPackage,
            bool? IschkWaterMarked,int SubStoreID,bool? IsPanorama,bool? IsIsPersonalizedAR)
        {
            DBParameters.Clear();
            AddParameter("@ParamProductTypeId", ProductTypeId);
            AddParameter("@ParamStoreId", StoreId);
            AddParameter("@ParamUserId", UserId);
            AddParameter("@ParamIsInvisible", IsInvisible);
            AddParameter("@ParamIsDiscount", SetNullBoolValue(IsDiscount));
            AddParameter("@ParamIsPackage", SetNullBoolValue(IsPackage));
            AddParameter("@ParamIsActive", SetNullBoolValue(IsActive));
            AddParameter("@ParamIsAccessory", SetNullBoolValue(IsAccessory));
            AddParameter("@ParamIsTaxEnabled", SetNullBoolValue(IsTaxIncluded));
            AddParameter("@ParamProductTypeName", ProductTypeName);
            AddParameter("@ParamProductTypeDesc", ProductTypeDesc);
            AddParameter("@ParamProductPrice", ProductPrice);
            AddParameter("@ParamProductcode", Productcode);
            AddParameter("@ParamSyncCode", SyncCode);
            AddParameter("@ParamSyncodeforPackage", SyncodeforPackage);
            AddParameter("@ParamIsWaterMarked", SetNullBoolValue(IschkWaterMarked));
            AddParameter("@SubStoreID", SubStoreID);
            AddParameter("@IsPanorama", IsPanorama);
            AddParameter("@IsPersonalizedAR", IsIsPersonalizedAR);///added by latika for AR Personalised
          int result= Convert.ToInt32(ExecuteScalar(DAOConstant.USP_UPDANDINS_PRODUCTTYPEINFO));
            return result;
        }
        public List<ProductTypeInfo> SelProductTypeData(bool? IsPackage)
        {
            DBParameters.Clear();
            AddParameter("@ParamIsPackage", IsPackage);
            IDataReader sqlReader = ExecuteReader(DAOConstant.USP_SEL_PRODUCTTYPEDATA);
            List<ProductTypeInfo> ProductTypeList = MapProductTypeBase(sqlReader);
            sqlReader.Close();
            return ProductTypeList;
        }
        private List<ProductTypeInfo> MapProductTypeBase(IDataReader sqlReader)
        {
            List<ProductTypeInfo> ProductTypeList = new List<ProductTypeInfo>();
            while (sqlReader.Read())
            {
                ProductTypeInfo ProductType = new ProductTypeInfo()
                {
                    DG_Orders_ProductType_pkey = GetFieldValue(sqlReader, "DG_Orders_ProductType_pkey", 0),
                    DG_Orders_ProductType_Name = GetFieldValue(sqlReader, "DG_Orders_ProductType_Name", string.Empty),
                    DG_Orders_ProductType_Desc = GetFieldValue(sqlReader, "DG_Orders_ProductType_Desc", string.Empty),
                    DG_Orders_ProductType_IsBundled = GetFieldValue(sqlReader, "DG_Orders_ProductType_IsBundled", false),
                    DG_Orders_ProductType_DiscountApplied = GetFieldValue(sqlReader, "DG_Orders_ProductType_DiscountApplied", false),
                    DG_IsPackage = GetFieldValue(sqlReader, "DG_IsPackage", false),
                    DG_MaxQuantity = GetFieldValue(sqlReader, "DG_MaxQuantity", 0),
                    DG_IsActive = GetFieldValue(sqlReader, "DG_IsActive", false),
                    DG_IsAccessory = GetFieldValue(sqlReader, "DG_IsAccessory", false),
                    DG_Orders_ProductCode = GetFieldValue(sqlReader, "DG_Orders_ProductCode", string.Empty),
                    DG_Orders_ProductNumber = GetFieldValue(sqlReader, "DG_Orders_ProductNumber", 0),
                    DG_Product_Pricing_ProductPrice = GetFieldValue(sqlReader, "DG_Product_Pricing_ProductPrice", 0.0D),
                    DG_Product_Pricing_Currency_ID = GetFieldValue(sqlReader, "DG_Product_Pricing_Currency_ID", 0),
                    DG_IsWaterMarkIncluded = GetFieldValue(sqlReader, "DG_IsWaterMarked", false),
                    DG_SubStore_Name = GetFieldValue(sqlReader, "DG_SubStore_Name", string.Empty),
                    IsPersonalizedAR = GetFieldValue(sqlReader, "IsPersonalizedAR", false)////added by latika for AR Personalised
                };

                ProductTypeList.Add(ProductType);
            }
            return ProductTypeList;
        }


        public void BulkSaveProduct(DataTable productTable, int createdBy, int storeId)
        {
            DBParameters.Clear();
            AddParameter("@ParamOrderProductType", productTable);
            AddParameter("@ParamCreatedBy", createdBy);
            AddParameter("@ParamStoredID", storeId);
            ExecuteNonQuery(DAOConstant.USP_UPLOAD_ORDERPRODUCTTYPEDATAFROMEXCEL);
        }



        public bool Delete(int objectvalueId)
        {
            DBParameters.Clear();
            AddParameter("@ParamProductTypeId", objectvalueId);
            ExecuteNonQuery(DAOConstant.USP_DEL_DGORDERSPRODUCTTYPE);
            return true;
        }


        public List<ProductPriceInfo> SelectProductPrice(int StoreId)
        {
            DBParameters.Clear();
            AddParameter("@ParamDG_Product_Pricing_StoreId", StoreId);
            IDataReader sqlReader = ExecuteReader(DAOConstant.USP_SEL_PRODUCTPRICING);
            List<ProductPriceInfo> ProductTypeList = MapProductPriceBase(sqlReader);
            sqlReader.Close();
            return ProductTypeList;
        }
        private List<ProductPriceInfo> MapProductPriceBase(IDataReader sqlReader)
        {
            List<ProductPriceInfo> ProductTypeList = new List<ProductPriceInfo>();
            while (sqlReader.Read())
            {
                ProductPriceInfo ProductType = new ProductPriceInfo()
                {
                    DG_Product_Pricing_Pkey = GetFieldValue(sqlReader, "DG_Product_Pricing_Pkey", 0),
                    DG_Product_Pricing_ProductType = GetFieldValue(sqlReader, "DG_Product_Pricing_ProductType", 0),
                    DG_Product_Pricing_ProductPrice = GetFieldValue(sqlReader, "DG_Product_Pricing_ProductPrice", 0.0D),
                    DG_Product_Pricing_Currency_ID = GetFieldValue(sqlReader, "DG_Product_Pricing_Currency_ID", 0),
                    DG_Product_Pricing_UpdateDate = GetFieldValue(sqlReader, "DG_Product_Pricing_UpdateDate", DateTime.MinValue),
                    DG_Product_Pricing_CreatedBy = GetFieldValue(sqlReader, "DG_Product_Pricing_CreatedBy", 0),
                    DG_Product_Pricing_StoreId = GetFieldValue(sqlReader, "DG_Product_Pricing_StoreId", 0),
                    DG_Product_Pricing_IsAvaliable = GetFieldValue(sqlReader, "DG_Product_Pricing_IsAvaliable", false),
                };

                ProductTypeList.Add(ProductType);
            }
            return ProductTypeList;
        }

        public List<ProductTypeInfo> GetProductTypeforOrder(int SubStoreID)
        {
            DBParameters.Clear();
            AddParameter("@SiteID", SubStoreID);
            IDataReader sqlReader = ExecuteReader(DAOConstant.USP_SEL_GETPRODUCTTYPEFORORDER);
            List<ProductTypeInfo> ProductTypeList = MapProductTypeforOrder(sqlReader);
            sqlReader.Close();
            return ProductTypeList;
        }
        private List<ProductTypeInfo> MapProductTypeforOrder(IDataReader sqlReader)
        {
            List<ProductTypeInfo> Orders_ProductTypeList = new List<ProductTypeInfo>();
            while (sqlReader.Read())
            {
                ProductTypeInfo Orders_ProductType = new ProductTypeInfo()
                {
                    DG_Orders_ProductType_pkey = GetFieldValue(sqlReader, "DG_Orders_ProductType_pkey", 0),
                    DG_Orders_ProductType_Name = GetFieldValue(sqlReader, "DG_Orders_ProductType_Name", string.Empty),
                    DG_Orders_ProductType_ProductCode = GetFieldValue(sqlReader, "DG_Orders_ProductCode", string.Empty),
                    DG_Orders_ProductType_Desc = GetFieldValue(sqlReader, "DG_Orders_ProductType_Desc", string.Empty),
                    DG_Orders_ProductType_IsBundled = GetFieldValue(sqlReader, "DG_Orders_ProductType_IsBundled", false),
                    DG_Orders_ProductType_DiscountApplied = GetFieldValue(sqlReader, "DG_Orders_ProductType_DiscountApplied", false),
                    DG_Orders_ProductType_Image = GetFieldValue(sqlReader, "DG_Orders_ProductType_Image", string.Empty),
                    DG_IsPackage = GetFieldValue(sqlReader, "DG_IsPackage", false),
                    DG_MaxQuantity = GetFieldValue(sqlReader, "DG_MaxQuantity", 0),
                    DG_Orders_ProductType_Active = GetFieldValue(sqlReader, "DG_Orders_ProductType_Active", false),
                    DG_IsActive = GetFieldValue(sqlReader, "DG_IsActive", false),
                    DG_IsAccessory = GetFieldValue(sqlReader, "DG_IsAccessory", false),
                    Itemcount = GetFieldValue(sqlReader, "Itemcount", 0),
                    DG_Orders_ProductNumber = GetFieldValueIntNull(sqlReader, "DG_Orders_ProductNumber", 0),
                    IsPrintType = GetFieldValue(sqlReader, "IsPrintType", 0),
                    DG_IsWaterMarkIncluded = GetFieldValue(sqlReader, "DG_IsWaterMarked", false),
                    DG_SubStore_pkey = GetFieldValue(sqlReader, "DG_SubStore_pkey", 0),
                    IsPersonalizedAR = GetFieldValue(sqlReader, "IsPersonalizedAR", false),
                };

                Orders_ProductTypeList.Add(Orders_ProductType);
            }
            return Orders_ProductTypeList;
        }
        public bool SetProductPricingData(int? ProductTypeId, double? ProductPrice, int? CurrencyId, int? CreatedBy, int? storeId, bool? Isavailable)
        {
            DBParameters.Clear();
            AddParameter("@ParamProductTypeId", SetNullIntegerValue(ProductTypeId));
            AddParameter("@ParamProductPrice", SetNullDoubleValue(ProductPrice));
            AddParameter("@ParamCurrencyId", SetNullIntegerValue(CurrencyId));
            AddParameter("@ParamCreatedBy", SetNullIntegerValue(CreatedBy));
            AddParameter("@ParamStoreId", SetNullIntegerValue(storeId));
            AddParameter("@ParamIsavailable", SetNullBoolValue(Isavailable));
            ExecuteReader(DAOConstant.USP_UPDANDINS_PRODUCTPRICINGDATA);
            return true;
        }

        public List<GroupInfo> GetGroupList()
        {
            DBParameters.Clear();
            IDataReader sqlReader = ExecuteReader(DAOConstant.USP_GET_GROUP);
            List<GroupInfo> GroupList = new List<GroupInfo>();
            while (sqlReader.Read())
            {
                GroupInfo Group = new GroupInfo()
                {
                    GroupID = GetFieldValue(sqlReader, "dg_group_pkey", 0),
                    GroupName = GetFieldValue(sqlReader, "dg_group_name", string.Empty)
                };
                GroupList.Add(Group);
            }
            return GroupList;
        }
        public bool IsChkSpecOnlinePackage(int PackageID)
        {
            bool Value = false;
            DBParameters.Clear();
            AddParameter("@ParamValue", Value, ParameterDirection.Output);
            AddParameter("@ParamPackageID", SetNullIntegerValue(PackageID));
            ExecuteNonQuery(DAOConstant.USP_GET_CHKSPECONLINEPACKAGE);
            bool objectId = (bool)GetOutParameterValue("@ParamValue");
            return objectId;
        }
        public bool CheckduplicateProduct(string ProductTypeName,int ProductID)
        {
            DBParameters.Clear();
            AddParameter("@ParamProductTypeName", SetNullStringValue(ProductTypeName));
            AddParameter("@ProductID", SetNullIntegerValue(ProductID));
            bool objectId =Convert.ToBoolean(ExecuteScalar("usp_Check_duplicate_Product"));
            return objectId;
        }
		////added by latika ruke for search photos
        public List<ProductTypeInfo> SelProductTypeDataSearch(string strSearch)
        {
            DBParameters.Clear();
            AddParameter("@StrSearch", strSearch);
            IDataReader sqlReader = ExecuteReader(DAOConstant.usp_SEL_ProductTypeDataSearch);
            List<ProductTypeInfo> ProductTypeList = MapProductTypeBase(sqlReader);
            sqlReader.Close();
            return ProductTypeList;
        }
        ////end
        ///
        /// <summary>
        /// added by latika for evoucher 25march20202
        /// </summary>
        /// <param name="SubStoreID"></param>
        /// <param name="EvoucherCode"></param>
        /// <returns></returns>
        public List<ProductTypeInfo> GetProductTypeforOrderEvoucher(int SubStoreID, string EvoucherCode)
        {
            DBParameters.Clear();
            AddParameter("@SiteID", SubStoreID);
            AddParameter("@EvoucherCode", EvoucherCode);
            IDataReader sqlReader = ExecuteReader("[dbo].[usp_SEL_GetProductTypeforOrderEvoucher]");
            List<ProductTypeInfo> ProductTypeList = MapProductTypeforOrder(sqlReader);
            sqlReader.Close();
            return ProductTypeList;
        }
        /// <summary>
        /// //end by latika
        /// </summary>
        /// <param name="sqlReader"></param>
        /// <returns></returns>

    }
}
