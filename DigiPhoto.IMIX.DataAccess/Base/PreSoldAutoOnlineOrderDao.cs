﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DigiPhoto.IMIX.Model;
using System.Data;


namespace DigiPhoto.IMIX.DataAccess
{
    public class PreSoldAutoOnlineOrderDao : BaseDataAccess
    {
        #region Constructor
        public PreSoldAutoOnlineOrderDao(BaseDataAccess baseaccess)
            : base(baseaccess)
        { }
        public PreSoldAutoOnlineOrderDao()
        { }

        public List<PreSoldAutoOnlineOrderInfo> GetAutoOnlineOrder()
        {

            List<PreSoldAutoOnlineOrderInfo> objectList;
            IDataReader sqlReader = ExecuteReader(DAOConstant.USP_SEL_PresoldAUTOONLINEORDER);
            objectList = MapObjectAutoOnlineOrder(sqlReader);
            sqlReader.Close();
            return objectList;
        }
        private List<PreSoldAutoOnlineOrderInfo> MapObjectAutoOnlineOrder(IDataReader sqlReader)
        {
            List<PreSoldAutoOnlineOrderInfo> objectList = new List<PreSoldAutoOnlineOrderInfo>();
            while (sqlReader.Read())
            {
                PreSoldAutoOnlineOrderInfo objectValue = new PreSoldAutoOnlineOrderInfo
                {
                    IMIXImageAssociationId = GetFieldValue(sqlReader, "IMIXImageAssociationId", 0),
                    PhotoId = GetFieldValue(sqlReader, "PhotoId", 0),
                    IsOrdered = GetFieldValue(sqlReader, "IsOrdered", 0),
                    CardUniqueIdentifier = GetFieldValue(sqlReader, "CardUniqueIdentifier", string.Empty),
                    Name = GetFieldValue(sqlReader, "Name", string.Empty),
                    MaxImages = GetFieldValue(sqlReader, "MaxImages", 0),
                    PackageId = GetFieldValue(sqlReader, "PackageId", 0),
                    DG_Product_Pricing_ProductPrice = GetFieldValue(sqlReader, "DG_Product_Pricing_ProductPrice", 0f),
                    ImageIdentificationType = GetFieldValue(sqlReader, "ImageIdentificationType", 0),
                    IsWaterMarked = GetFieldValue(sqlReader, "IsWaterMark", false),
                    IsPresold = GetFieldValue(sqlReader, "IsPresold", false)
                };

                objectList.Add(objectValue);
            }
            return objectList;
        }
        public bool UpdateOrderStatus(string IMIXImageAssociationIds, bool IsWaterMarked)
        {
            bool Value = false;
            DBParameters.Clear();
            AddParameter("@ParamValue", Value, ParameterDirection.Output);
            AddParameter("@ParamIMIXImageAssociationIds", SetNullStringValue(IMIXImageAssociationIds));
            AddParameter("@ParamIsWaterMarked", SetNullBoolValue(IsWaterMarked));
            ExecuteNonQuery(DAOConstant.USP_UPD_PresoldAUTOONLINEORDER);
            bool objectId = (bool)GetOutParameterValue("@ParamValue");
            return objectId;
        }
        public bool chKIsAutoPurchaseActiveOrNot(int SubStoreId)
        {
            bool Value = false;
            DBParameters.Clear();
            AddParameter("@ParamValue", Value, ParameterDirection.Output);
            AddParameter("@ParamSubStoreId", SetNullIntegerValue(SubStoreId));
            ExecuteNonQuery(DAOConstant.USP_GET_CHKISAUTOPURCHASEACTIVEORNOT);
            bool objectId = (bool)GetOutParameterValue("@ParamValue");
            return objectId;
        }


        public bool getOrderStatus(PreSoldAutoOnlineOrderInfo cardNumber, string IMIXImageAssociationIds, string photoId, bool IsWaterMarked)
        {
            //PreSoldAutoOnlineOrderInfo preSoldAutoOnlineOrderInfo = new PreSoldAutoOnlineOrderInfo();
            //  preSoldAutoOnlineOrderInfo.CardUniqueIdentifier = cardNumber.ToString();
            //string cardnumber = cardNumber.CardUniqueIdentifier;
            bool Value = false;
            DBParameters.Clear();
            AddParameter("@ParamValue", Value, ParameterDirection.Output);
            AddParameter("@preSoldAutoOnlineOrderInfo", SetNullStringValue(cardNumber.CardUniqueIdentifier));
            AddParameter("@paramIMIXImageAssociationIds", SetNullStringValue(IMIXImageAssociationIds));
            AddParameter("@photoId", SetNullStringValue(photoId));
            AddParameter("@paramIsWaterMarked", SetNullBoolValue(IsWaterMarked));
            ExecuteNonQuery(DAOConstant.USP_CHECK_PRESOLDALREADYORDERED);
            bool objectId = (bool)GetOutParameterValue("@ParamValue");
            return objectId;
        }
        #endregion
    }
}

