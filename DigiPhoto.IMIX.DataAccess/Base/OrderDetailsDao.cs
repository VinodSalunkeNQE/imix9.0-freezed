﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DigiPhoto.IMIX.Model;
using System.Data;
namespace DigiPhoto.IMIX.DataAccess
{
    public class OrderDetailsDao : BaseDataAccess
    {
        #region Constructor
        public OrderDetailsDao(BaseDataAccess baseaccess)
            : base(baseaccess)
        { }
        public OrderDetailsDao()
        { }
        #endregion
        public List<OrderDetailInfo> Get(int ProductType_pkey, int Date)
        {
            DBParameters.Clear();

            IDataReader sqlReader = ExecuteReader(DAOConstant.USP_SEL_ORDERSDETAILS);
            List<OrderDetailInfo> OrdersList = MapOrderDetailInfo(sqlReader);
            sqlReader.Close();
            return OrdersList;
        }
        private List<OrderDetailInfo> MapOrderDetailInfo(IDataReader sqlReader)
        {
            List<OrderDetailInfo> OrdersList = new List<OrderDetailInfo>();
            while (sqlReader.Read())
            {
                OrderDetailInfo Orders = new OrderDetailInfo()
                {
                    DG_Orders_pkey = GetFieldValue(sqlReader, "DG_Orders_pkey", 0),
                    DG_Orders_Number = GetFieldValue(sqlReader, "DG_Orders_Number", string.Empty),
                    DG_Orders_Date = GetFieldValue(sqlReader, "DG_Orders_Date", DateTime.Now),
                    DG_Orders_LineItems_pkey = GetFieldValue(sqlReader, "DG_Orders_LineItems_pkey", 0),
                    DG_Orders_ID = GetFieldValue(sqlReader, "DG_Orders_ID", 0),
                    DG_Photos_ID = GetFieldValue(sqlReader, "DG_Photos_ID", string.Empty),
                    DG_Orders_Details_ProductType_pkey = GetFieldValue(sqlReader, "DG_Orders_Details_ProductType_pkey", 0),
                    DG_Order_Status = GetFieldValue(sqlReader, "DG_Order_Status", 0),

                };

                OrdersList.Add(Orders);
            }
            return OrdersList;
        }
        public List<OrderDetailInfo> GetOrderDetailsforRefund(string Orders_Number)
        {
            DBParameters.Clear();
            AddParameter("@ParamOrders_Number", Orders_Number);

            IDataReader sqlReader = ExecuteReader(DAOConstant.USP_SEL_ORDERDETAILSFORREFUND);
            List<OrderDetailInfo> GetOrderDetailsforRefundList = MapOrderDetailforRefundInfo(sqlReader);
            sqlReader.Close();
            return GetOrderDetailsforRefundList;
        }

        private List<OrderDetailInfo> MapOrderDetailforRefundInfo(IDataReader sqlReader)
        {
            List<OrderDetailInfo> OrderDetailsforRefundList = new List<OrderDetailInfo>();
            while (sqlReader.Read())
            {
                OrderInfo orderInfo = new OrderInfo();
                OrderDetailInfo OrderDetailsforRefundInfo = new OrderDetailInfo();

                OrderDetailsforRefundInfo.DG_Orders_LineItems_pkey = GetFieldValue(sqlReader, "DG_Orders_LineItems_pkey", 0);
                OrderDetailsforRefundInfo.DG_Orders_ID = GetFieldValue(sqlReader, "DG_Orders_ID", 0);
                OrderDetailsforRefundInfo.DG_Photos_ID = GetFieldValue(sqlReader, "DG_Photos_ID", string.Empty);
                OrderDetailsforRefundInfo.DG_Orders_LineItems_Quantity = GetFieldValue(sqlReader, "DG_Orders_LineItems_Quantity", 0);
                OrderDetailsforRefundInfo.TotalQuantity = GetFieldValue(sqlReader, "TotalQuantity", 0L);
                OrderDetailsforRefundInfo.DG_Orders_LineItems_Created = GetFieldValue(sqlReader, "DG_Orders_LineItems_Created", DateTime.Now);
                OrderDetailsforRefundInfo.DG_Orders_LineItems_DiscountType = GetFieldValue(sqlReader, "DG_Orders_LineItems_DiscountType", string.Empty);
                OrderDetailsforRefundInfo.DG_Orders_LineItems_DiscountAmount = GetFieldValue(sqlReader, "DG_Orders_LineItems_DiscountAmount", 0.0M);
                OrderDetailsforRefundInfo.DG_Orders_Details_Items_UniPrice = GetFieldValue(sqlReader, "DG_Orders_Details_Items_UniPrice", 0.0M);
                OrderDetailsforRefundInfo.DG_Orders_Details_Items_TotalCost = GetFieldValue(sqlReader, "DG_Orders_Details_Items_TotalCost", 0.0M);
                OrderDetailsforRefundInfo.DG_Orders_Details_Items_NetPrice = GetFieldValue(sqlReader, "DG_Orders_Details_Items_NetPrice", 0.0M);
                OrderDetailsforRefundInfo.DG_Orders_Details_ProductType_pkey = GetFieldValue(sqlReader, "DG_Orders_Details_ProductType_pkey", 0);
                OrderDetailsforRefundInfo.DG_Orders_Details_LineItem_ParentID = GetFieldValue(sqlReader, "DG_Orders_Details_LineItem_ParentID", 0);
                OrderDetailsforRefundInfo.DG_Orders_Details_LineItem_PrinterReferenceID = GetFieldValue(sqlReader, "DG_Orders_Details_LineItem_PrinterReferenceID", 0);
                OrderDetailsforRefundInfo.DG_Orders_Number = GetFieldValue(sqlReader, "DG_Orders_Number", string.Empty);
                OrderDetailsforRefundInfo.DG_Orders_Date = GetFieldValue(sqlReader, "DG_Orders_Date", DateTime.Now);
                orderInfo.DG_Orders_Cost = GetFieldValue(sqlReader, "DG_Orders_Cost", 0.0M);
                orderInfo.DG_Orders_NetCost = GetFieldValue(sqlReader, "DG_Orders_NetCost", 0.0M);
                orderInfo.DG_Orders_Currency_ID = GetFieldValue(sqlReader, "DG_Orders_Currency_ID", 0);
                orderInfo.DG_Orders_Currency_Conversion_Rate = GetFieldValue(sqlReader, "DG_Orders_Currency_Conversion_Rate", string.Empty);
                orderInfo.DG_Orders_Total_Discount = GetFieldValue(sqlReader, "DG_Orders_Total_Discount", 0.0D);
                orderInfo.DG_Orders_Total_Discount_Details = GetFieldValue(sqlReader, "DG_Orders_Total_Discount_Details", string.Empty);
                orderInfo.DG_Orders_PaymentDetails = GetFieldValue(sqlReader, "DG_Orders_PaymentDetails", string.Empty);
                orderInfo.DG_Orders_PaymentMode = GetFieldValue(sqlReader, "DG_Orders_PaymentMode", 0);
                OrderDetailsforRefundInfo.DG_Orders_ProductType_Name = GetFieldValue(sqlReader, "DG_Orders_ProductType_Name", string.Empty);
                OrderDetailsforRefundInfo.DG_Orders_ProductCode = GetFieldValue(sqlReader, "DG_Orders_ProductCode", string.Empty);
                OrderDetailsforRefundInfo.DG_Orders_ProductType_IsBundled = GetFieldValue(sqlReader, "DG_Orders_ProductType_IsBundled", false);
                OrderDetailsforRefundInfo.DG_IsPackage = GetFieldValue(sqlReader, "DG_IsPackage", false);
                OrderDetailsforRefundInfo.LineItemshare = GetFieldValue(sqlReader, "LineItemshare", 0.0M);

                OrderDetailsforRefundInfo.OrderInfo = orderInfo;
                OrderDetailsforRefundList.Add(OrderDetailsforRefundInfo);
            }
            return OrderDetailsforRefundList;
        }

        #region Product Naming EN/CN  Added By Suraj Mali.

        public string GetSiteNameDetails(string SiteNameEN)
        {
            try
            {
                string SiteNameCN = string.Empty;
                DBParameters.Clear();
                AddParameter("@SiteNameEN", SiteNameEN);
                IDataReader sqlReader = ExecuteReader(DAOConstant.GETSiteNameDetails);
                while (sqlReader.Read())
                {
                    SiteNameCN = sqlReader["SiteNameCN"].ToString();
                }
                sqlReader.Close();
                return SiteNameCN;
            }
            catch(Exception ex)
            {
                return null;
            }
        }


        /// <summary>
        ///  Added By Suraj Mali. For getting Product EN/CN Name
        /// </summary>
        /// <param name="Orders_Number"></param>
        /// <returns></returns>
        public List<ProductNameInfo> GetProductNameDetails(string ProductNameEN)
        {
            DBParameters.Clear();
            AddParameter("@ProductNameEN", ProductNameEN);

            IDataReader sqlReader = ExecuteReader(DAOConstant.GETProductNameDetails);
            List<ProductNameInfo> GetOrderDetailsforRefundList = MapProductNameInfo(sqlReader);
            sqlReader.Close();
            return GetOrderDetailsforRefundList;
        }

        private List<ProductNameInfo> MapProductNameInfo(IDataReader sqlReader)
        {
            List<ProductNameInfo> ProductNameInfoList = new List<ProductNameInfo>();
            while (sqlReader.Read())
            {
                ProductNameInfo productInfo = new ProductNameInfo();
                
                productInfo.ProductNameEN = GetFieldValue(sqlReader, "ProductNameEN", string.Empty);
                productInfo.ProductNameCN = GetFieldValue(sqlReader, "ProductNameCN", string.Empty);
                productInfo.ProductNumber = GetFieldValue(sqlReader, "ProductNumber", string.Empty);

                ProductNameInfoList.Add(productInfo);
            }
            return ProductNameInfoList;
        }
        #endregion
        //public List<OrderInfo> GetOrderDetailedSyncStaus(DateTime FromDate, DateTime ToDate)
        //{
        //    DBParameters.Clear();
        //    AddParameter("@FromDate", FromDate);
        //    AddParameter("@ToDate", ToDate);

        //    IDataReader sqlReader = ExecuteReader(DAOConstant.ORDERDETAILEDSYNCSTAUS);
        //    List<OrderInfo> ChangeTrackingList = MapOrderInfo(sqlReader);
        //    sqlReader.Close();
        //    return ChangeTrackingList;
        //}
        //private List<OrderInfo> MapOrderInfo(IDataReader sqlReader)
        //{
        //    List<OrderInfo> ChangeTrackingList = new List<OrderInfo>();
        //    while (sqlReader.Read())
        //    {
        //        OrderInfo ChangeTracking = new OrderInfo()
        //        {
        //            SyncStatus = GetFieldValue(sqlReader, "SyncStatus", false),
        //            SyncDate = GetFieldValue(sqlReader, "SyncDate", DateTime.Now),
        //            DG_Orders_pkey = GetFieldValue(sqlReader, "DG_Orders_pkey", 0),
        //            DG_Orders_Number = GetFieldValue(sqlReader, "DG_Orders_Number", string.Empty),
        //            DG_Orders_Date = GetFieldValue(sqlReader, "DG_Orders_Date", DateTime.Now),
        //            DG_Photos_ID = GetFieldValue(sqlReader, "DG_Orders_Date", 0),

        //        };

        //        ChangeTrackingList.Add(ChangeTracking);
        //    }
        //    return ChangeTrackingList;
        //}
        public List<OrderDetailInfo> GetOrderDetailsByID(int LineItemskey)
        {
            DBParameters.Clear();
            AddParameter("@ParamLineItems_pkey", LineItemskey);

            IDataReader sqlReader = ExecuteReader(DAOConstant.USP_GET_ORDERDETAILSBYID);
            List<OrderDetailInfo> OrdersList = MapOrderDetailsByIDInfo(sqlReader);
            sqlReader.Close();
            return OrdersList;
        }
        private List<OrderDetailInfo> MapOrderDetailsByIDInfo(IDataReader sqlReader)
        {
            List<OrderDetailInfo> OrdersList = new List<OrderDetailInfo>();
            while (sqlReader.Read())
            {
                OrderDetailInfo Orders = new OrderDetailInfo()
                {
                    DG_Orders_pkey = GetFieldValue(sqlReader, "DG_Orders_pkey", 0),
                    DG_Orders_Number = GetFieldValue(sqlReader, "DG_Orders_Number", string.Empty),
                    DG_Orders_Date = GetFieldValue(sqlReader, "DG_Orders_Date", DateTime.Now),
                    OrderInfo = new OrderInfo()
                    {
                        DG_Orders_Cost = GetFieldValue(sqlReader, "DG_Orders_Cost", 0.0M),
                        DG_Orders_NetCost = GetFieldValue(sqlReader, "DG_Orders_NetCost", 0.0M),
                        DG_Orders_Currency_ID = GetFieldValue(sqlReader, "DG_Orders_Currency_ID", 0),
                        DG_Orders_Currency_Conversion_Rate = GetFieldValue(sqlReader, "DG_Orders_Currency_Conversion_Rate", string.Empty),
                        DG_Orders_Total_Discount = GetFieldValue(sqlReader, "DG_Orders_Total_Discount", 0.0D),
                        DG_Orders_Total_Discount_Details = GetFieldValue(sqlReader, "DG_Orders_Total_Discount_Details", string.Empty),
                        DG_Orders_PaymentMode = GetFieldValue(sqlReader, "DG_Orders_PaymentMode", 0),
                        DG_Orders_PaymentDetails = GetFieldValue(sqlReader, "DG_Orders_PaymentDetails", string.Empty),
                    },
                    DG_Orders_LineItems_pkey = GetFieldValue(sqlReader, "DG_Orders_LineItems_pkey", 0),
                    DG_Orders_ID = GetFieldValue(sqlReader, "DG_Orders_ID", 0),
                    DG_Photos_ID = GetFieldValue(sqlReader, "DG_Photos_ID", string.Empty),
                    DG_Orders_LineItems_Created = GetFieldValue(sqlReader, "DG_Orders_LineItems_Created", DateTime.Now),
                    DG_Orders_LineItems_DiscountType = GetFieldValue(sqlReader, "DG_Orders_LineItems_DiscountType", string.Empty),
                    DG_Orders_LineItems_DiscountAmount = GetFieldValue(sqlReader, "DG_Orders_LineItems_DiscountAmount", 0.0M),
                    DG_Orders_LineItems_Quantity = GetFieldValue(sqlReader, "DG_Orders_LineItems_Quantity", 0),
                    DG_Orders_Details_Items_UniPrice = GetFieldValue(sqlReader, "DG_Orders_Details_Items_UniPrice", 0),
                    DG_Orders_Details_Items_TotalCost = GetFieldValue(sqlReader, "DG_Orders_Details_Items_TotalCost", 0.0M),
                    DG_Orders_Details_Items_NetPrice = GetFieldValue(sqlReader, "DG_Orders_Details_Items_NetPrice", 0.0M),
                    DG_Orders_Details_ProductType_pkey = GetFieldValue(sqlReader, "DG_Orders_Details_ProductType_pkey", 0),
                    DG_Orders_Details_LineItem_ParentID = GetFieldValue(sqlReader, "DG_Orders_Details_LineItem_ParentID", 0),
                    DG_Orders_Details_LineItem_PrinterReferenceID = GetFieldValue(sqlReader, "DG_Orders_Details_LineItem_PrinterReferenceID", 0),
                    DG_Orders_ProductType_pkey = GetFieldValue(sqlReader, "DG_Orders_ProductType_pkey", 0),
                    DG_Orders_ProductType_Name = GetFieldValue(sqlReader, "DG_Orders_ProductType_Name", string.Empty),
                    DG_IsBorder = GetFieldValue(sqlReader, "DG_IsBorder", false),

                };

                OrdersList.Add(Orders);
            }
            return OrdersList;
        }



        //----start ----------Hari----------20st Feb 2018-----Qr Code Implementation-------
        public List<OnlineorderImgDetail> GetOrderNumberandImg(string QRCode)
        {
            DBParameters.Clear();
            AddParameter("@QRCode", QRCode);

            DataSet sqlDataSet = ExecuteDataSet(DAOConstant.USP_QROnlineOrderImg);
            List<OnlineorderImgDetail> GetOrderNumberandImgLst = MapOnlineordernumwithImg(sqlDataSet);
            return GetOrderNumberandImgLst;


        }
        private List<OnlineorderImgDetail> MapOnlineordernumwithImg(DataSet dataSet)
        {
            List<OnlineorderImgDetail> lstOrderDetails = new List<OnlineorderImgDetail>();

            foreach (DataRow Row in dataSet.Tables[0].Rows)
            {
                OnlineorderImgDetail OnlineordernumImg = new OnlineorderImgDetail();
                OnlineordernumImg.OnlinePhotoID = Row["PhotoID"].ToString();
                OnlineordernumImg.OnlinePhotoName = Row["PhotoFileName"].ToString();
                OnlineordernumImg.OnlineOrderNumber = Row["OrderNumber"].ToString();

                lstOrderDetails.Add(OnlineordernumImg);
            }
            return lstOrderDetails;

        }

        //-----end--------Hari----------20st Feb 2018-----Qr Code Implementation-------




        /// <summary>
        /// GetOrderDetailsByPhotoID AND DG_Orders_ID = null  
        /// </summary>
        /// <param name="PhotosId"></param>
        /// <returns></returns>
        public OrderDetailInfo GetOrderDetailsByPhotoID(string PhotosId)
        {
            DBParameters.Clear();
            AddParameter("@ParamPhotosID", PhotosId);
            IDataReader sqlReader = ExecuteReader(DAOConstant.USP_GET_ORDERDETAILSBYPHOTOID);
            List<OrderDetailInfo> Orders_DetailsList = MapOrderDetailsByPhotoID(sqlReader);
            sqlReader.Close();
            return Orders_DetailsList.FirstOrDefault();
        }
        private List<OrderDetailInfo> MapOrderDetailsByPhotoID(IDataReader sqlReader)
        {
            List<OrderDetailInfo> Orders_DetailsList = new List<OrderDetailInfo>();
            while (sqlReader.Read())
            {
                OrderDetailInfo Orders_Details = new OrderDetailInfo()
                {
                    DG_Orders_LineItems_pkey = GetFieldValue(sqlReader, "DG_Orders_LineItems_pkey", 0),
                    DG_Orders_ID = GetFieldValue(sqlReader, "DG_Orders_ID", 0),
                    DG_Photos_ID = GetFieldValue(sqlReader, "DG_Photos_ID", string.Empty),
                    DG_Orders_LineItems_Created = GetFieldValue(sqlReader, "DG_Orders_LineItems_Created", DateTime.Now),
                    DG_Orders_LineItems_DiscountType = GetFieldValue(sqlReader, "DG_Orders_LineItems_DiscountType", string.Empty),
                    DG_Orders_LineItems_DiscountAmount = GetFieldValue(sqlReader, "DG_Orders_LineItems_DiscountAmount", 0.0M),
                    DG_Orders_LineItems_Quantity = GetFieldValue(sqlReader, "DG_Orders_LineItems_Quantity", 0),
                    DG_Orders_Details_Items_UniPrice = GetFieldValue(sqlReader, "DG_Orders_Details_Items_UniPrice", 0.0M),
                    DG_Orders_Details_Items_TotalCost = GetFieldValue(sqlReader, "DG_Orders_Details_Items_TotalCost", 0.0M),
                    DG_Orders_Details_Items_NetPrice = GetFieldValue(sqlReader, "DG_Orders_Details_Items_NetPrice", 0.0M),
                    DG_Orders_Details_ProductType_pkey = GetFieldValue(sqlReader, "DG_Orders_Details_ProductType_pkey", 0),
                    DG_Orders_Details_LineItem_ParentID = GetFieldValue(sqlReader, "DG_Orders_Details_LineItem_ParentID", 0),
                    DG_Orders_Details_LineItem_PrinterReferenceID = GetFieldValue(sqlReader, "DG_Orders_Details_LineItem_PrinterReferenceID", 0),
                    DG_Photos_Burned = GetFieldValue(sqlReader, "DG_Photos_Burned", false),
                    DG_Order_SubStoreId = GetFieldValue(sqlReader, "DG_Order_SubStoreId", 0),
                    IsPostedToServer = GetFieldValue(sqlReader, "IsPostedToServer", 0),
                    DG_Order_IdentifierType = GetFieldValue(sqlReader, "DG_Order_IdentifierType", 0),
                    DG_Order_ImageUniqueIdentifier = GetFieldValue(sqlReader, "DG_Order_ImageUniqueIdentifier", string.Empty),
                    DG_Order_Status = GetFieldValue(sqlReader, "DG_Order_Status", 0),

                };

                Orders_DetailsList.Add(Orders_Details);
            }
            return Orders_DetailsList;
        }
        public bool UpdateBurnOrderStatus(int OrdersLineItemskey, int OrderStatus)
        {
            DBParameters.Clear();
            AddParameter("@ParamDG_Orders_LineItems_pkey", OrdersLineItemskey);
            AddParameter("@ParamDG_Order_Status", OrderStatus);
            ExecuteNonQuery(DAOConstant.USP_UPD_BURNORDERSTATUS);
            return true;
        }
        public void UpdateSemiOrderImageOrderDetails(int? OrderId, int PhotoID, Int32? parentId, int substorId, string DiscountType, decimal DiscountAmount, decimal TotalCost, decimal NetPrice)
        {
            DBParameters.Clear();
            AddParameter("@ParamDG_Order_SubStoreId", substorId);
            AddParameter("@ParamDG_Orders_ID", OrderId);
            AddParameter("@ParamDG_Orders_Details_LineItem_ParentID", parentId);
            AddParameter("@ParamDG_Photos_ID", PhotoID);
            AddParameter("@DiscountType", DiscountType);
            AddParameter("@DiscountAmount", DiscountAmount);
            AddParameter("@TotalCost", TotalCost);
            AddParameter("@NetPrice", NetPrice);
            ExecuteNonQuery(DAOConstant.USP_UPD_SEMIORDERIMAGEORDERDETAILS);
        }
        public List<BurnOrderInfo> GetBODetailsByID(int LineItemskey)
        {
            DBParameters.Clear();
            AddParameter("@ParamLineItems_pkey", LineItemskey);

            IDataReader sqlReader = ExecuteReader(DAOConstant.USP_GET_ORDERDETAILSBYID);
            List<BurnOrderInfo> OrdersList = MapBODetailsByIDInfo(sqlReader);
            sqlReader.Close();
            return OrdersList;
        }
        private List<BurnOrderInfo> MapBODetailsByIDInfo(IDataReader sqlReader)
        {
            List<BurnOrderInfo> OrdersList = new List<BurnOrderInfo>();
            while (sqlReader.Read())
            {
                BurnOrderInfo Orders = new BurnOrderInfo()
                {
                    PhotosId = GetFieldValue(sqlReader, "DG_Photos_ID", string.Empty),
                    Status = GetFieldValue(sqlReader, "DG_Order_Status", 0),
                    OrderNumber = GetFieldValue(sqlReader, "DG_Orders_Number", string.Empty),
                    OrderDetailId = GetFieldValue(sqlReader, "DG_Orders_LineItems_pkey", 0),
                    ProductType = GetFieldValue(sqlReader, "DG_Orders_Details_ProductType_pkey", 0),
                };

                OrdersList.Add(Orders);
            }
            return OrdersList;
        }
        //public DG_Orders_Details GetLineItemDetails(int Orders_LineItems_pkey)
        //{
        //    DBParameters.Clear();
        //    AddParameter("@ParamOrders_LineItems_pkey", Orders_LineItems_pkey);

        //    IDataReader sqlReader = ExecuteReader(DAOConstant.USP_GET_LINEITEMDETAILS);
        //    List<DG_Orders_Details> Orders_DetailsList = MapOrdersDetailsBase(sqlReader);
        //    sqlReader.Close();
        //    return Orders_DetailsList.FirstOrDefault();
        //}
        //private List<DG_Orders_Details> MapOrdersDetailsBase(IDataReader sqlReader)
        //{
        //    List<DG_Orders_Details> Orders_DetailsList = new List<DG_Orders_Details>();
        //    while (sqlReader.Read())
        //    {
        //        DG_Orders_Details Orders_Details = new DG_Orders_Details()
        //        {
        //            DG_Orders_LineItems_pkey = GetFieldValue(sqlReader, "DG_Orders_LineItems_pkey", 0),
        //            DG_Orders_ID = GetFieldValue(sqlReader, "DG_Orders_ID", 0),
        //            DG_Photos_ID = GetFieldValue(sqlReader, "DG_Photos_ID", string.Empty),
        //            DG_Orders_LineItems_Created = GetFieldValue(sqlReader, "DG_Orders_LineItems_Created", DateTime.Now),
        //            DG_Orders_LineItems_DiscountType = GetFieldValue(sqlReader, "DG_Orders_LineItems_DiscountType", string.Empty),
        //            DG_Orders_LineItems_DiscountAmount = GetFieldValue(sqlReader, "DG_Orders_LineItems_DiscountAmount", 0.0M),
        //            DG_Orders_LineItems_Quantity = GetFieldValue(sqlReader, "DG_Orders_LineItems_Quantity", 0),
        //            DG_Orders_Details_Items_UniPrice = GetFieldValue(sqlReader, "DG_Orders_Details_Items_UniPrice", 0.0M),
        //            DG_Orders_Details_Items_TotalCost = GetFieldValue(sqlReader, "DG_Orders_Details_Items_TotalCost", 0.0M),
        //            DG_Orders_Details_Items_NetPrice = GetFieldValue(sqlReader, "DG_Orders_Details_Items_NetPrice", 0.0M),
        //            DG_Orders_Details_ProductType_pkey = GetFieldValue(sqlReader, "DG_Orders_Details_ProductType_pkey", 0),
        //            DG_Orders_Details_LineItem_ParentID = GetFieldValue(sqlReader, "DG_Orders_Details_LineItem_ParentID", 0),
        //            DG_Orders_Details_LineItem_PrinterReferenceID = GetFieldValue(sqlReader, "DG_Orders_Details_LineItem_PrinterReferenceID", 0),
        //            DG_Photos_Burned = GetFieldValue(sqlReader, "DG_Photos_Burned", false),
        //            DG_Order_SubStoreId = GetFieldValue(sqlReader, "DG_Order_SubStoreId", 0),
        //            IsPostedToServer = GetFieldValue(sqlReader, "IsPostedToServer", 0),
        //            DG_Order_IdentifierType = GetFieldValue(sqlReader, "DG_Order_IdentifierType", 0),
        //            DG_Order_ImageUniqueIdentifier = GetFieldValue(sqlReader, "DG_Order_ImageUniqueIdentifier", string.Empty),
        //            DG_Order_Status = GetFieldValue(sqlReader, "DG_Order_Status", 0),

        //        };

        //        Orders_DetailsList.Add(Orders_Details);
        //    }
        //    return Orders_DetailsList;
        //}
        public OrderDetailInfo GetSemiOrderImage(int OrderDetailsID)
        {
            DBParameters.Clear();
            AddParameter("@ParamOrderDetailsID", OrderDetailsID);

            IDataReader sqlReader = ExecuteReader(DAOConstant.USP_GET_SEMIORDERIMAGE);
            List<OrderDetailInfo> Orders_DetailsList = MapSemiOrderImageInfo(sqlReader);
            sqlReader.Close();
            return Orders_DetailsList.FirstOrDefault();
        }
        private List<OrderDetailInfo> MapSemiOrderImageInfo(IDataReader sqlReader)
        {
            List<OrderDetailInfo> Orders_DetailsList = new List<OrderDetailInfo>();
            while (sqlReader.Read())
            {
                OrderDetailInfo Orders_Details = new OrderDetailInfo()
                {
                    DG_Orders_ID = GetFieldValue(sqlReader, "DG_Orders_ID", 0)
                };

                Orders_DetailsList.Add(Orders_Details);
            }
            return Orders_DetailsList;
        }
        public OrderDetailsViewInfo SelectOrderDetailsByID(int OrdersLineItems)
        {
            DBParameters.Clear();
            AddParameter("@ParamOrdersLineItems", OrdersLineItems);
            IDataReader sqlReader = ExecuteReader(DAOConstant.USP_SEL_ORDERDETAILSBYID);
            List<OrderDetailsViewInfo> OrderDetailsList = MaptOrderDetailsBase(sqlReader);
            sqlReader.Close();
            return OrderDetailsList.FirstOrDefault();
        }
        private List<OrderDetailsViewInfo> MaptOrderDetailsBase(IDataReader sqlReader)
        {
            List<OrderDetailsViewInfo> OrderDetailsList = new List<OrderDetailsViewInfo>();
            while (sqlReader.Read())
            {
                OrderDetailsViewInfo OrderDetails = new OrderDetailsViewInfo()
                {
                    DG_Orders_LineItems_pkey = GetFieldValue(sqlReader, "DG_Orders_LineItems_pkey", 0),
                    DG_Orders_ID = GetFieldValue(sqlReader, "DG_Orders_ID", 0),
                    DG_Photos_ID = GetFieldValue(sqlReader, "DG_Photos_ID", string.Empty),
                    DG_Orders_LineItems_Created = GetFieldValue(sqlReader, "DG_Orders_LineItems_Created", DateTime.Now),
                    DG_Orders_LineItems_DiscountType = GetFieldValue(sqlReader, "DG_Orders_LineItems_DiscountType", string.Empty),
                    DG_Orders_LineItems_DiscountAmount = GetFieldValue(sqlReader, "DG_Orders_LineItems_DiscountAmount", 0.0M),
                    DG_Orders_LineItems_Quantity = GetFieldValue(sqlReader, "DG_Orders_LineItems_Quantity", 0),
                    DG_Orders_Details_Items_UniPrice = GetFieldValue(sqlReader, "DG_Orders_Details_Items_UniPrice", 0.0M),
                    DG_Orders_Details_Items_TotalCost = GetFieldValue(sqlReader, "DG_Orders_Details_Items_TotalCost", 0.0M),
                    DG_Orders_Details_Items_NetPrice = GetFieldValue(sqlReader, "DG_Orders_Details_Items_NetPrice", 0.0M),
                    DG_Orders_Details_ProductType_pkey = GetFieldValue(sqlReader, "DG_Orders_Details_ProductType_pkey", 0),
                    DG_Orders_Details_LineItem_ParentID = GetFieldValue(sqlReader, "DG_Orders_Details_LineItem_ParentID", 0),
                    DG_Orders_Details_LineItem_PrinterReferenceID = GetFieldValue(sqlReader, "DG_Orders_Details_LineItem_PrinterReferenceID", 0),
                    DG_Orders_Number = GetFieldValue(sqlReader, "DG_Orders_Number", string.Empty),
                    DG_Orders_Date = GetFieldValue(sqlReader, "DG_Orders_Date", DateTime.Now),
                    DG_Orders_Cost = GetFieldValue(sqlReader, "DG_Orders_Cost", 0.0M),
                    DG_Orders_NetCost = GetFieldValue(sqlReader, "DG_Orders_NetCost", 0.0M),
                    DG_Orders_Currency_ID = GetFieldValue(sqlReader, "DG_Orders_Currency_ID", 0),
                    DG_Orders_Currency_Conversion_Rate = GetFieldValue(sqlReader, "DG_Orders_Currency_Conversion_Rate", string.Empty),
                    DG_Orders_Total_Discount = GetFieldValue(sqlReader, "DG_Orders_Total_Discount", 0.0F),
                    DG_Orders_Total_Discount_Details = GetFieldValue(sqlReader, "DG_Orders_Total_Discount_Details", string.Empty),
                    DG_Orders_PaymentDetails = GetFieldValue(sqlReader, "DG_Orders_PaymentDetails", string.Empty),
                    DG_Orders_PaymentMode = GetFieldValue(sqlReader, "DG_Orders_PaymentMode", 0),
                    DG_Orders_ProductType_Name = GetFieldValue(sqlReader, "DG_Orders_ProductType_Name", string.Empty),
                    DG_IsBorder = GetFieldValue(sqlReader, "DG_IsBorder", false),
                };
                OrderDetailsList.Add(OrderDetails);
            }
            return OrderDetailsList;
        }
        public int InsetOrderLineItems(OrderDetailInfo Order)
        {
            DBParameters.Clear();
            AddParameter("@ParamOrdersID", Order.DG_Orders_ID);
            AddParameter("@ParamQuantity", Order.DG_Orders_LineItems_Quantity);
            AddParameter("@ParamProductTypepkey", Order.DG_Orders_Details_ProductType_pkey);
            AddParameter("@ParamParentID", Order.DG_Orders_Details_LineItem_ParentID);
            AddParameter("@ParamOrderSubStoreId", Order.DG_Order_SubStoreId);
            AddParameter("@ParamOrderIdentifierType", Order.DG_Order_IdentifierType);
            AddParameter("@OrdersLineItemspkey", Order.DG_Orders_LineItems_pkey, ParameterDirection.Output);
            AddParameter("@ParamDiscountAmount", Order.DG_Orders_LineItems_DiscountAmount);
            AddParameter("@ParamUniPrice", Order.DG_Orders_Details_Items_UniPrice);
            AddParameter("@ParamTotalCost", Order.DG_Orders_Details_Items_TotalCost);
            AddParameter("@ParamNetPrice", Order.DG_Orders_Details_Items_NetPrice);
            AddParameter("@ParamCreated", Order.DG_Orders_LineItems_Created);
            AddParameter("@ParamPhotosID", Order.DG_Photos_ID);
            AddParameter("@ParamDiscountType", base.SetNullStringValue(Order.DG_Orders_LineItems_DiscountType));
            AddParameter("@ParamOrderImageUniqueIdentifier", base.SetNullStringValue(Order.DG_Order_ImageUniqueIdentifier));
            AddParameter("@ParamSyncCode", Order.SyncCode);
            AddParameter("@ParamTaxPercent", Order.TaxPercent);
            AddParameter("@ParamTaxAmount", Order.TaxAmount);
            AddParameter("@ParamIsTaxIncluded", Order.IsTaxIncluded);
            AddParameter("@ParamDGPhotosIDUnSold", Order.DG_Photos_IDUnSold);
            AddParameter("@EvoucherCode", Order.EvoucherCode);/////added by latika for Evoucher
            ExecuteNonQuery(DAOConstant.USP_INS_ORDERLINEITEMS);
            int OrdersLineItemspkey = (int)GetOutParameterValue("@OrdersLineItemspkey");
            return OrdersLineItemspkey;
        }

       
        public List<string> GetCardImageSoldById(string Code)
        {
            DBParameters.Clear();
            AddParameter("@ParamCode", Code);

            IDataReader sqlReader = ExecuteReader(DAOConstant.USP_GET_CARDIMAGESOLDBYID);
            List<string> Orders_DetailsList = MapCardImageSoldById(sqlReader);
            sqlReader.Close();
            return Orders_DetailsList;
        }
        private List<string> MapCardImageSoldById(IDataReader sqlReader)
        {
            List<string> Orders_DetailsList = new List<string>();
            while (sqlReader.Read())
            {
                string DG_Photos_ID = GetFieldValue(sqlReader, "DG_Photos_ID", string.Empty);
                Orders_DetailsList.Add(DG_Photos_ID);
            }
            return Orders_DetailsList;
        }
        public OrderInfo GenerateOrder(OrderInfo ObjOrder)
        {
            DBParameters.Clear();
            AddParameter("@ParamDG_Orders_PaymentMode", ObjOrder.DG_Orders_PaymentMode);
            AddParameter("@ParamDG_Orders_Currency_ID", ObjOrder.DG_Orders_Currency_ID);
            AddParameter("@ParamOrderId", ObjOrder.DG_Orders_pkey, ParameterDirection.Output);
            AddParameter("@ParamDG_Orders_UserID", ObjOrder.DG_Orders_UserID);
            AddParameter("@ParamDG_Orders_Cost", ObjOrder.DG_Orders_Cost);
            AddParameter("@ParamDG_Orders_NetCost", ObjOrder.DG_Orders_NetCost);
            AddParameter("@ParamDG_Orders_Date", ObjOrder.DG_Orders_Date);
            AddParameter("@ParamDG_Orders_Total_Discount", ObjOrder.DG_Orders_Total_Discount);
            AddParameter("@ParamIsSynced", ObjOrder.IsSynced);
            AddParameter("@ParamDG_Orders_Number", ObjOrder.DG_Orders_Number);
            AddParameter("@ParamSyncCode", ObjOrder.SyncCode);
            AddParameter("@ParamDG_Order_Mode", ObjOrder.DG_Order_Mode);
            AddParameter("@ParamDG_Orders_PaymentDetails", ObjOrder.DG_Orders_PaymentDetails);
            AddParameter("@ParamDG_Orders_Currency_Conversion_Rate", ObjOrder.DG_Orders_Currency_Conversion_Rate);
            AddParameter("@ParamDG_Orders_Total_Discount_Details", ObjOrder.DG_Orders_Total_Discount_Details);
            AddOutParameterString("@ParamDG_NewOrderNo", SqlDbType.NVarChar, 350);
            AddParameter("@ParamPosName", ObjOrder.PosName);
            AddParameter("@ParamEmpId", ObjOrder.EmpId);//BY KCB ON 20 JUN 2020 For display user' employee id
            ExecuteNonQuery(DAOConstant.USP_INS_ORDER);
            OrderInfo objNew = new OrderInfo();
            objNew.DG_Orders_pkey = (int)GetOutParameterValue("@ParamOrderId");
            objNew.DG_Orders_Number = (string)GetOutParameterValue("@ParamDG_NewOrderNo");
            return objNew;
        }


        public int AddSemiOrderDetails(OrderDetailInfo objOrderD, int locationId, bool isSentToPrinter)
        {
            int OrdersLineItemspkey = 0;
            DBParameters.Clear();

            AddParameter("@ParamPhotosID", SetNullStringValue(objOrderD.DG_Photos_ID));
            AddParameter("@ParamOrderId", objOrderD.DG_Orders_ID);
            AddParameter("@ParamProductTypepkey", SetNullIntegerValue(objOrderD.DG_Orders_Details_ProductType_pkey));
            AddParameter("@ParamLocationId", locationId);
            AddParameter("@ParamParentID", objOrderD.DG_Orders_Details_LineItem_ParentID);
            AddParameter("@ParamQuantity", objOrderD.DG_Orders_LineItems_Quantity);
            AddParameter("@ParamTotalCost", objOrderD.DG_Orders_Details_Items_TotalCost);
            AddParameter("@ParamDiscountAmount", objOrderD.DG_Orders_LineItems_DiscountAmount);
            AddParameter("@ParamSyncCode", objOrderD.SyncCode);
            AddParameter("@ParamIsSentToPrinter", isSentToPrinter);
            AddParameter("@ParamSubStoreId", objOrderD.DG_Order_SubStoreId);

            AddParameter("@OrdersLineItemspkey", OrdersLineItemspkey, ParameterDirection.Output);

            ExecuteNonQuery(DAOConstant.USP_INS_SEMIORDERDETAIL);
            OrdersLineItemspkey = (int)GetOutParameterValue("@OrdersLineItemspkey");
            return OrdersLineItemspkey;
        }
        public int SetOrderDetails(OrderDetailInfo objOrderD)
        {
            int OrdersLineItemspkey = 0;
            DBParameters.Clear();
            AddParameter("@ParamOrderId", objOrderD.DG_Orders_ID);
            AddParameter("@ParamProductTypepkey", SetNullIntegerValue(objOrderD.DG_Orders_Details_ProductType_pkey));
            AddParameter("@ParamOrderSubStoreId", objOrderD.DG_Order_SubStoreId);
            AddParameter("@ParamParentID", objOrderD.DG_Orders_Details_LineItem_ParentID);
            AddParameter("@ParamQuantity", objOrderD.DG_Orders_LineItems_Quantity);
            AddParameter("@OrdersLineItemspkey", OrdersLineItemspkey, ParameterDirection.Output);

            AddParameter("@ParamUniPrice", objOrderD.DG_Orders_Details_Items_UniPrice);
            AddParameter("@ParamTotalCost", objOrderD.DG_Orders_Details_Items_TotalCost);
            AddParameter("@ParamNetPrice", objOrderD.DG_Orders_Details_Items_NetPrice);
            AddParameter("@ParamDiscountAmount", objOrderD.DG_Orders_LineItems_DiscountAmount);
            AddParameter("@ParamCreated", objOrderD.DG_Orders_LineItems_Created);
            AddParameter("@ParamPhotosID", SetNullStringValue(objOrderD.DG_Photos_ID));
            AddParameter("@ParamSyncCode", objOrderD.SyncCode);
            ExecuteNonQuery(DAOConstant.USP_INS_ORDERDETAIL);
            OrdersLineItemspkey = (int)GetOutParameterValue("@OrdersLineItemspkey");
            return OrdersLineItemspkey;
        }
        public List<BurnOrderInfo> SelectPendingBurnOrders(bool All)
        {
            DBParameters.Clear();
            AddParameter("@ParamAll", All);

            IDataReader sqlReader = ExecuteReader(DAOConstant.USP_SEL_PENDINGBURNORDERS);
            List<BurnOrderInfo> OrdersList = MapPendingBurnOrders(sqlReader);
            sqlReader.Close();
            return OrdersList;
        }
        private List<BurnOrderInfo> MapPendingBurnOrders(IDataReader sqlReader)
        {
            List<BurnOrderInfo> OrdersList = new List<BurnOrderInfo>();
            while (sqlReader.Read())
            {
                BurnOrderInfo Orders = new BurnOrderInfo()
                {
                    OrderNumber = GetFieldValue(sqlReader, "DG_Orders_Number", string.Empty),
                    OrderDetailId = GetFieldValue(sqlReader, "DG_Orders_LineItems_pkey", 0),
                    PhotosId = GetFieldValue(sqlReader, "DG_Photos_ID", string.Empty),
                    ProductType = GetFieldValue(sqlReader, "DG_Orders_Details_ProductType_pkey", 0),
                    Status = GetFieldValue(sqlReader, "DG_Order_Status", 0),
                };

                OrdersList.Add(Orders);
            }
            return OrdersList;
        }
        public bool UpdateCancelOrder(string OrderNo, string CancelReason)
        {//replace datetime to ServerDateTime()
            DBParameters.Clear();
            AddParameter("@ParamDG_Orders_Number", OrderNo);
            AddParameter("@ParamDG_Orders_Canceled_Date", DateTime.Now);
            AddParameter("@ParamDG_Orders_Canceled", true);
            AddParameter("@ParamDG_Orders_Canceled_Reason", CancelReason);
            ExecuteNonQuery(DAOConstant.USP_UPD_CANCELORDER);
            return true;
        }

        public OrderInfo GetOrder(string OrderNo)
        {
            DBParameters.Clear();
            AddParameter("@ParamOrders_Number", OrderNo);
            IDataReader sqlReader = ExecuteReader(DAOConstant.USP_GET_ORDERDATEBYORDERNO);
            OrderInfo Orders_DetailsList = MapGetOrder(sqlReader);
            sqlReader.Close();
            return Orders_DetailsList;
        }
        private OrderInfo MapGetOrder(IDataReader sqlReader)
        {
            OrderInfo Orders_Details = new OrderInfo();
            while (sqlReader.Read())
            {
                Orders_Details.DG_Orders_pkey = GetFieldValue(sqlReader, "DG_Orders_pkey", 0);
                Orders_Details.DG_Orders_Number = GetFieldValue(sqlReader, "DG_Orders_Number", string.Empty);
                Orders_Details.DG_Orders_Date = GetFieldValue(sqlReader, "DG_Orders_Date", DateTime.Now);
                Orders_Details.DG_Albums_ID = GetFieldValue(sqlReader, "DG_Albums_ID", 0);
                Orders_Details.DG_Orders_Cost = GetFieldValue(sqlReader, "DG_Orders_Cost", 0.0M);
                Orders_Details.DG_Orders_NetCost = GetFieldValue(sqlReader, "DG_Orders_NetCost", 0.0M);
                Orders_Details.DG_Orders_Currency_ID = GetFieldValue(sqlReader, "DG_Orders_Currency_ID", 0);
                Orders_Details.DG_Orders_Currency_Conversion_Rate = GetFieldValue(sqlReader, "DG_Orders_Currency_Conversion_Rate", string.Empty);
                Orders_Details.DG_Orders_Total_Discount = GetFieldValue(sqlReader, "DG_Orders_Total_Discount", 0.0F);
                Orders_Details.DG_Orders_Total_Discount_Details = GetFieldValue(sqlReader, "DG_Orders_Total_Discount_Details", string.Empty);
                Orders_Details.DG_Orders_PaymentMode = GetFieldValue(sqlReader, "DG_Orders_PaymentMode", 0);
                Orders_Details.DG_Orders_PaymentDetails = GetFieldValue(sqlReader, "DG_Orders_PaymentDetails", string.Empty);
                Orders_Details.DG_Orders_Canceled = GetFieldValue(sqlReader, "DG_Orders_Canceled", false);
                Orders_Details.DG_Orders_Canceled_Date = GetFieldValue(sqlReader, "DG_Orders_Canceled_Date", DateTime.Now);
                Orders_Details.DG_Orders_Canceled_Reason = GetFieldValue(sqlReader, "DG_Orders_Canceled_Reason", string.Empty);
                Orders_Details.SyncCode = GetFieldValue(sqlReader, "SyncCode", string.Empty);
                Orders_Details.IsSynced = GetFieldValue(sqlReader, "IsSynced", false);


            }
            return Orders_Details;
        }
        public bool GetSemiOrderImageforValidation(string imageNumber)
        {
            bool OrdersLineItemspkey = false;
            DBParameters.Clear();
            AddParameter("@ParamPhotosID", imageNumber);
            AddParameter("@ParamIsExist", OrdersLineItemspkey, ParameterDirection.Output);
            ExecuteNonQuery(DAOConstant.USP_GET_SEMIORDERIMAGEFORVALIDATION);
            OrdersLineItemspkey = (bool)GetOutParameterValue("@ParamIsExist");
            return OrdersLineItemspkey;
        }
        public bool UpdatePostedOrder(int Status, string OrderNumber)
        {
            DBParameters.Clear();
            AddParameter("@Status", Status);
            AddParameter("@OrderNumber", OrderNumber);
            ExecuteNonQuery(DAOConstant.UPDATEPOSTEDORDER);
            return true;
        }

        public OrderInfo GetOrdersNumber(string OrdersNumber)
        {
            DBParameters.Clear();
            AddParameter("@ParamOrdersNumber", OrdersNumber);
            IDataReader sqlReader = ExecuteReader(DAOConstant.USP_GET_ORDERSNUMBER);
            List<OrderInfo> OrdersList = MapOrdersBase(sqlReader);
            sqlReader.Close();
            return OrdersList.FirstOrDefault();
        }
        private List<OrderInfo> MapOrdersBase(IDataReader sqlReader)
        {
            List<OrderInfo> OrdersList = new List<OrderInfo>();
            while (sqlReader.Read())
            {
                OrderInfo Orders = new OrderInfo()
                {
                    DG_Orders_pkey = GetFieldValue(sqlReader, "DG_Orders_pkey", 0),
                    DG_Orders_Number = GetFieldValue(sqlReader, "DG_Orders_Number", string.Empty),
                    DG_Orders_Date = GetFieldValue(sqlReader, "DG_Orders_Date", DateTime.Now),
                    DG_Albums_ID = GetFieldValue(sqlReader, "DG_Albums_ID", 0),
                    DG_Order_Mode = GetFieldValue(sqlReader, "DG_Order_Mode", string.Empty),
                    DG_Orders_UserID = GetFieldValue(sqlReader, "DG_Orders_UserID", 0),
                    DG_Orders_Cost = GetFieldValue(sqlReader, "DG_Orders_Cost", 0.0M),
                    DG_Orders_NetCost = GetFieldValue(sqlReader, "DG_Orders_NetCost", 0.0M),
                    DG_Orders_Currency_ID = GetFieldValue(sqlReader, "DG_Orders_Currency_ID", 0),
                    DG_Orders_Currency_Conversion_Rate = GetFieldValue(sqlReader, "DG_Orders_Currency_Conversion_Rate", string.Empty),
                    DG_Orders_Total_Discount = GetFieldValue(sqlReader, "DG_Orders_Total_Discount", 0.0D),
                    DG_Orders_Total_Discount_Details = GetFieldValue(sqlReader, "DG_Orders_Total_Discount_Details", string.Empty),
                    DG_Orders_PaymentMode = GetFieldValue(sqlReader, "DG_Orders_PaymentMode", 0),
                    DG_Orders_PaymentDetails = GetFieldValue(sqlReader, "DG_Orders_PaymentDetails", string.Empty),
                    DG_Orders_Canceled = GetFieldValue(sqlReader, "DG_Orders_Canceled", false),
                    DG_Orders_Canceled_Date = GetFieldValue(sqlReader, "DG_Orders_Canceled_Date", DateTime.Now),
                    DG_Orders_Canceled_Reason = GetFieldValue(sqlReader, "DG_Orders_Canceled_Reason", string.Empty),
                    SyncCode = GetFieldValue(sqlReader, "SyncCode", string.Empty),
                    IsSynced = GetFieldValue(sqlReader, "IsSynced", false),

                };

                OrdersList.Add(Orders);
            }
            return OrdersList;
        }



        public List<OrderDetailInfo> GetPhotoToUpload()
        {
            DBParameters.Clear();
            IDataReader sqlReader = ExecuteReader(DAOConstant.GETPHOTOTOUPLOAD);
            List<OrderDetailInfo> Orders_DetailsList = MapPhotoToUpload(sqlReader);
            sqlReader.Close();
            return Orders_DetailsList;
        }
        private List<OrderDetailInfo> MapPhotoToUpload(IDataReader sqlReader)
        {
            List<OrderDetailInfo> Orders_DetailsList = new List<OrderDetailInfo>();
            while (sqlReader.Read())
            {
                OrderDetailInfo Orders_Details = new OrderDetailInfo()
                {
                    DG_Orders_Number = GetFieldValue(sqlReader, "DG_Orders_Number", string.Empty),
                    DG_Order_SubStoreId = GetFieldValue(sqlReader, "DG_Order_SubStoreId", 0)
                };
                Orders_DetailsList.Add(Orders_Details);
            }
            return Orders_DetailsList;
        }
        public bool chKIsWaterMarkedOrNot(int PackageID)
        {
            bool Value = false;
            DBParameters.Clear();
            AddParameter("@ParamValue", Value, ParameterDirection.Output);
            AddParameter("@ParamPackageID", SetNullIntegerValue(PackageID));
            ExecuteNonQuery(DAOConstant.USP_GET_CHKISWATERMARKEDORNOT);
            bool objectId = (bool)GetOutParameterValue("@ParamValue");
            return objectId;
        }

    }
}
