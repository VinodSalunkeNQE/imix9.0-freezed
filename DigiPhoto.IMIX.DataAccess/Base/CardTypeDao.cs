﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DigiPhoto.IMIX.Model;
using System.Data;

namespace DigiPhoto.IMIX.DataAccess
{
    public class CardTypeDao : BaseDataAccess
    {
        #region Constructor
        public CardTypeDao(BaseDataAccess baseaccess)
            : base(baseaccess)
        { }
        public CardTypeDao()
        { }
        #endregion


        public ImageCardTypeInfo GetValidCodeType(int CardTypeId, string QRCode)
        {
            DBParameters.Clear();
            AddParameter("@ParamCardTypeId", CardTypeId);
            AddParameter("@ParamQRCode", QRCode);
            IDataReader sqlReader = ExecuteReader(DAOConstant.USP_GET_VALIDCODETYPE);
            ImageCardTypeInfo iMixImageCardType = MapValidCodeTypeInfo(sqlReader);
            sqlReader.Close();
            return iMixImageCardType;
        }
        public ImageCardTypeInfo IsValidPrepaidCodeType(int CardTypeId, string QRCode)
        {
            DBParameters.Clear();
            AddParameter("@ParamCardTypeId", CardTypeId);
            AddParameter("@ParamQRCode", QRCode);
            IDataReader sqlReader = ExecuteReader(DAOConstant.USP_GET_VALIDPREPAIDCODETYPE);
            ImageCardTypeInfo iMixImageCardType = MapValidCodeTypeInfo(sqlReader);
            sqlReader.Close();
            return iMixImageCardType;
        }
        public ImageCardTypeInfo GetCardImageLimitById(string Code)
        {
            DBParameters.Clear();
            AddParameter("@ParamCode", Code);
            IDataReader sqlReader = ExecuteReader(DAOConstant.USP_GET_CARDIMAGELIMITBYID);
            List<ImageCardTypeInfo> iMixImageCardTypeList = MapImageCardTypeInfo(sqlReader);
            sqlReader.Close();
            return iMixImageCardTypeList.FirstOrDefault();
        }
        private List<ImageCardTypeInfo> MapImageCardTypeInfo(IDataReader sqlReader)
        {
            List<ImageCardTypeInfo> iMixImageCardTypeList = new List<ImageCardTypeInfo>();
            while (sqlReader.Read())
            {
                ImageCardTypeInfo iMixImageCardType = new ImageCardTypeInfo()
                {
                    MaxImages = GetFieldValue(sqlReader, "MaxImages", 0),
                };
                iMixImageCardTypeList.Add(iMixImageCardType);
            }
            return iMixImageCardTypeList;
        }

        private ImageCardTypeInfo MapValidCodeTypeInfo(IDataReader sqlReader)
        {
            ImageCardTypeInfo iMixImageCardType = null;
            while (sqlReader.Read())
            {
                iMixImageCardType = new ImageCardTypeInfo()
                {
                    IMIXImageCardTypeId = GetFieldValue(sqlReader, "IMIXImageCardTypeId", 0),
                };
            }
            return iMixImageCardType;
        }

        public ImageCardTypeInfo GetImageIdentificationType(int ImageIdentificationType)
        {

            DBParameters.Clear();

            AddParameter("@ParamImageIdentificationType", ImageIdentificationType);
            IDataReader sqlReader = ExecuteReader(DAOConstant.USP_GET_IMAGEIDENTIFICATIONTYPE);
            List<ImageCardTypeInfo> iMixImageCardTypeList = MapImageIdentificationTypeInfo(sqlReader);
            sqlReader.Close();
            return iMixImageCardTypeList.FirstOrDefault();
        }
        private List<ImageCardTypeInfo> MapImageIdentificationTypeInfo(IDataReader sqlReader)
        {
            List<ImageCardTypeInfo> iMixImageCardTypeList = new List<ImageCardTypeInfo>();
            while (sqlReader.Read())
            {
                ImageCardTypeInfo iMixImageCardType = new ImageCardTypeInfo()
                {
                    IMIXImageCardTypeId = GetFieldValue(sqlReader, "IMIXImageCardTypeId", 0),
                    Name = GetFieldValue(sqlReader, "Name", string.Empty),
                    CardIdentificationDigit = GetFieldValue(sqlReader, "CardIdentificationDigit", string.Empty),
                    ImageIdentificationType = GetFieldValue(sqlReader, "ImageIdentificationType", 0),
                    IsActive = GetFieldValue(sqlReader, "IsActive", false),
                    MaxImages = GetFieldValue(sqlReader, "MaxImages", 0),
                    Description = GetFieldValue(sqlReader, "Description", string.Empty),
                    CreatedOn = GetFieldValue(sqlReader, "CreatedOn", DateTime.Now),
                    IsPrepaid = GetFieldValue(sqlReader, "IsPrepaid", false),
                    PackageId = GetFieldValue(sqlReader, "PackageId", 0),

                };

                iMixImageCardTypeList.Add(iMixImageCardType);
            }
            return iMixImageCardTypeList;
        }
        public long InsertImageAssociationInfo(int PhotoId, string Format, string Code, bool IsAnonymousCodeActive)
        {
            long IMIXImageAssociationId = 0;
            DBParameters.Clear();
            AddParameter("@ParamPhotoId", PhotoId);
            AddParameter("@ParamIsAnonymousCodeActive", IsAnonymousCodeActive);
            AddParameter("@ParamIMIXImageAssociationId", IMIXImageAssociationId, ParameterDirection.Output);
            AddParameter("@ParamCode", Code);
            ExecuteNonQuery(DAOConstant.USP_INS_IMAGEASSOCIATIONINFO);
            long objectId = (long)GetOutParameterValue("@ParamIMIXImageAssociationId");
            return objectId;
        }

        public List<iMixImageCardTypeInfo> SelectCardTypeList()
        {
            IDataReader sqlReader = ExecuteReader(DAOConstant.USP_SEL_CARDTYPELIST);
            List<iMixImageCardTypeInfo> iMixImageCardTypeList = MapCardTypeList(sqlReader);
            sqlReader.Close();
            return iMixImageCardTypeList;
        }

        public Dictionary<string, int> getGlobalCountryList()
        {
            DBParameters.Clear();
            IDataReader sqlReader = ExecuteReader("usp_get_NationalityList ");
            Dictionary<string, int> CountryList = PopulateGlobalCountryListRequired(sqlReader);
            sqlReader.Close();
            return CountryList;
        }

        private Dictionary<string, int> PopulateGlobalCountryListRequired(IDataReader sqlReader)
        {
            Dictionary<string, int>  CountryList = new Dictionary<string, int>();
        
            while (sqlReader.Read())
            {
                CountryList.Add(GetFieldValue(sqlReader, "CountryName", string.Empty) , GetFieldValue(sqlReader, "CountryID", 0));
            }
            return CountryList;
        }

        private List<iMixImageCardTypeInfo> MapCardTypeList(IDataReader sqlReader)
        {
            List<iMixImageCardTypeInfo> iMixImageCardTypeList = new List<iMixImageCardTypeInfo>();
            while (sqlReader.Read())
            {
                iMixImageCardTypeInfo iMixImageCardType = new iMixImageCardTypeInfo()
                {
                    IMIXImageCardTypeId = GetFieldValue(sqlReader, "IMIXImageCardTypeId", 0),
                    Name = GetFieldValue(sqlReader, "Name", string.Empty),
                };
                iMixImageCardTypeList.Add(iMixImageCardType);
            }
            return iMixImageCardTypeList;
        }
        public List<ImageCardTypeInfo> GetCardTypeList()
        {
            IDataReader sqlReader = ExecuteReader(DAOConstant.USP_SEL_CARDTYPEINFO);
            List<ImageCardTypeInfo> imgBiz = MapCardInfoList(sqlReader);
            sqlReader.Close();
            return imgBiz;
        }
        private List<ImageCardTypeInfo> MapCardInfoList(IDataReader sqlReader)
        {
            List<ImageCardTypeInfo> cardTypeInfoList = new List<ImageCardTypeInfo>();
            while (sqlReader.Read())
            {
                ImageCardTypeInfo imgBiz = new ImageCardTypeInfo()
                {
                    IMIXImageCardTypeId = GetFieldValue(sqlReader, "IMIXImageCardTypeId", 0),
                    Name = GetFieldValue(sqlReader, "Name", string.Empty),
                    Status = GetFieldValue(sqlReader, "Status", string.Empty),
                    ImageIdentificationTypeName = GetFieldValue(sqlReader, "ImageIdentificationTypeName", string.Empty),
                    CardIdentificationDigit = GetFieldValue(sqlReader, "CardIdentificationDigit", string.Empty),
                    MaxImages = GetFieldValue(sqlReader, "MaxImages", 0),
                };
                cardTypeInfoList.Add(imgBiz);
            }
            return cardTypeInfoList;
        }

        public List<ImageCardTypeInfo> GetCardTypeDetails()
        {
            IDataReader sqlReader = ExecuteReader(DAOConstant.USP_SEL_CARDTYPEINFO);
            List<ImageCardTypeInfo> imgBiz = MapCardTypeDetails(sqlReader);
            sqlReader.Close();
            return imgBiz;
        }
        private List<ImageCardTypeInfo> MapCardTypeDetails(IDataReader sqlReader)
        {
            List<ImageCardTypeInfo> cardTypeInfoList = new List<ImageCardTypeInfo>();
            while (sqlReader.Read())
            {
                ImageCardTypeInfo imgBiz = new ImageCardTypeInfo()
                {
                    IMIXImageCardTypeId = GetFieldValue(sqlReader, "IMIXImageCardTypeId", 0),
                    Name = GetFieldValue(sqlReader, "Name", string.Empty),
                    Status = GetFieldValue(sqlReader, "Status", string.Empty),
                    ImageIdentificationType = GetFieldValue(sqlReader, "ImageIdentificationType", 0),
                    CardIdentificationDigit = GetFieldValue(sqlReader, "CardIdentificationDigit", string.Empty),
                    MaxImages = GetFieldValue(sqlReader, "MaxImages", 0),
                };
                cardTypeInfoList.Add(imgBiz);
            }
            return cardTypeInfoList;
        }



        public iMixImageCardTypeInfo GetCardTypeListDetails(int ID)
        {
            DBParameters.Clear();
            AddParameter("@IMIXImageCardTypeId", ID);
            IDataReader sqlReader = ExecuteReader(DAOConstant.USP_GET_CARDTYPELISTDETAILS);
            iMixImageCardTypeInfo iMixImageCardType = MapCardTypeListDetails(sqlReader);
            sqlReader.Close();
            return iMixImageCardType;
        }
        private iMixImageCardTypeInfo MapCardTypeListDetails(IDataReader sqlReader)
        {
            iMixImageCardTypeInfo iMixImageCardType = new iMixImageCardTypeInfo();
            while (sqlReader.Read())
            {
                iMixImageCardType.IMIXImageCardTypeId = GetFieldValue(sqlReader, "IMIXImageCardTypeId", 0);
                iMixImageCardType.Name = GetFieldValue(sqlReader, "Name", string.Empty);
                iMixImageCardType.IsActive = GetFieldValue(sqlReader, "IsActive", true);
                iMixImageCardType.ImageIdentificationType = GetFieldValue(sqlReader, "ImageIdentificationType", 0);
                iMixImageCardType.CardIdentificationDigit = GetFieldValue(sqlReader, "CardIdentificationDigit", string.Empty);
                iMixImageCardType.MaxImages = GetFieldValue(sqlReader, "MaxImages", 0);
                iMixImageCardType.Description = GetFieldValue(sqlReader, "Description", string.Empty);
                iMixImageCardType.CreatedOn = GetFieldValue(sqlReader, "CreatedOn", DateTime.Now);
                iMixImageCardType.IsPrepaid = GetFieldValue(sqlReader, "IsPrepaid", false);
                iMixImageCardType.PackageId = GetFieldValue(sqlReader, "PackageId", 0);
                iMixImageCardType.IsWaterMark = GetFieldValue(sqlReader, "IsWaterMark", false);
            }
            return iMixImageCardType;
        }
        public bool UPD_iMixImageCardType(int IMIXImageCardTypeId)
        {
            DBParameters.Clear();
            AddParameter("@ParamIMIXImageCardTypeId", IMIXImageCardTypeId);


            ExecuteNonQuery(DAOConstant.USP_UPD_IMIXIMAGECARDTYPE);
            return true;
        }
        public List<ImageCardTypeInfo> GetCardSeries(string CardIdentificationDigit)
        {
            DBParameters.Clear();
            AddParameter("@ParamCardIdentificationDigit", CardIdentificationDigit);

            IDataReader sqlReader = ExecuteReader(DAOConstant.USP_GET_CARDSERIES);
            List<ImageCardTypeInfo> iMixImageCardTypeList = MapCardSeries(sqlReader);
            sqlReader.Close();
            return iMixImageCardTypeList;
        }
        private List<ImageCardTypeInfo> MapCardSeries(IDataReader sqlReader)
        {
            List<ImageCardTypeInfo> iMixImageCardTypeList = new List<ImageCardTypeInfo>();
            while (sqlReader.Read())
            {
                ImageCardTypeInfo iMixImageCardType = new ImageCardTypeInfo()
                {
                    IMIXImageCardTypeId = GetFieldValue(sqlReader, "IMIXImageCardTypeId", 0),
                    Name = GetFieldValue(sqlReader, "Name", string.Empty),
                    CardIdentificationDigit = GetFieldValue(sqlReader, "CardIdentificationDigit", string.Empty),
                    ImageIdentificationType = GetFieldValue(sqlReader, "ImageIdentificationType", 0),
                    IsActive = GetFieldValue(sqlReader, "IsActive", false),
                    MaxImages = GetFieldValue(sqlReader, "MaxImages", 0),
                    Description = GetFieldValue(sqlReader, "Description", string.Empty),
                    CreatedOn = GetFieldValue(sqlReader, "CreatedOn", DateTime.Now),
                    IsPrepaid = GetFieldValue(sqlReader, "IsPrepaid", false),
                    PackageId = GetFieldValue(sqlReader, "PackageId", 0),

                };

                iMixImageCardTypeList.Add(iMixImageCardType);
            }
            return iMixImageCardTypeList;
        }

        public bool INS_iMixImageCardType(int cardId, string strCardTypeName, string strCardSeries, int strcodeType, bool? status, int maxImage, string strDescription, bool IsPrepaid, int PackageId, bool IsWaterMark)
        {
            DBParameters.Clear();
            AddParameter("@ParamCardId", cardId);
            AddParameter("@ParamCardTypeName", strCardTypeName);
            AddParameter("@ParamCardSeries", strCardSeries);
            AddParameter("@ParamCodeType", strcodeType);
            AddParameter("@ParamStatus", status);
            AddParameter("@ParamMaxImage", maxImage);
            AddParameter("@ParamDescription", strDescription);
            AddParameter("@ParamIsPrepaid", IsPrepaid);
            AddParameter("@ParamPackageId", PackageId);
            AddParameter("@ParamIsWaterMark", IsWaterMark);
            AddParameter("@ParamCardIdOut", cardId, ParameterDirection.InputOutput);
            ExecuteNonQuery(DAOConstant.usp_UPDANDINS_CardTypeInfo);
            int objectId = (int)GetOutParameterValue("@ParamCardIdOut");
            return true;
        }

    }

}
