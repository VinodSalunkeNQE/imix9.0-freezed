﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DigiPhoto.IMIX.Model;
using System.Data;

namespace DigiPhoto.IMIX.DataAccess
{
    public class AssociatedPrintersDao : BaseDataAccess
    {
        #region Constructor
        public AssociatedPrintersDao(BaseDataAccess baseaccess)
            : base(baseaccess)
        { }
        public AssociatedPrintersDao()
        { }


        #endregion

        public int Add(AssociatedPrintersInfo objectInfo)
        {
            DBParameters.Clear();
            AddParameter("@ParamProductType_ID", objectInfo.DG_AssociatedPrinters_ProductType_ID);
            AddParameter("@ParamSubStoreID", objectInfo.DG_AssociatedPrinters_SubStoreID);
            AddParameter("@ParamPkey", objectInfo.DG_AssociatedPrinters_Pkey, ParameterDirection.Output);
            AddParameter("@ParamPaperSize", objectInfo.DG_AssociatedPrinters_PaperSize);
            AddParameter("@ParamName", objectInfo.DG_AssociatedPrinters_Name);
            AddParameter("@ParamIsActive", objectInfo.DG_AssociatedPrinters_IsActive);
            ExecuteNonQuery(DAOConstant.USP_INS_ASSOCIATEDPRINTERS);
            int objectId = (int)GetOutParameterValue("@ParamPkey");
            return objectId;
        }
        public bool Update(AssociatedPrintersInfo objectInfo)
        {
            //DBParameters.Clear();
            //AddParameter("@DeviceId", objectInfo.DG_AssociatedPrinters_IsActive);

            //ExecuteNonQuery(DAOConstant.USP_GET_SEMIORDERSETTINGS);
            return true;
        }
        public bool Delete(int objectvalueId)
        {
            DBParameters.Clear();
            AddParameter("@ParamPrinterId", objectvalueId);
            ExecuteNonQuery(DAOConstant.USP_DEL_ASSOCIATEDPRINTERSBYID);
            return true;
        }
        public AssociatedPrintersInfo Get(int? subStoreId)
        {
            DBParameters.Clear();
            AddParameter("@ParamDG_AssociatedPrinters_SubStoreID", SetNullIntegerValue(subStoreId));

            List<AssociatedPrintersInfo> objectList;
            using (IDataReader sqlReader = ExecuteReader(DAOConstant.USP_SEL_ASSOCIATEDPRINTERS))
            {
                objectList = MapObject(sqlReader);
                //sqlReader.Dispose();
            }
            return objectList.FirstOrDefault();
        }
        public List<AssociatedPrintersInfo> Select(int? subStoreId)
        {
            DBParameters.Clear();
            AddParameter("@ParamDG_AssociatedPrinters_SubStoreID", SetNullIntegerValue(subStoreId));

            List<AssociatedPrintersInfo> objectList;
            using (IDataReader sqlReader = ExecuteReader(DAOConstant.USP_SEL_ASSOCIATEDPRINTERS))
            {
                objectList = MapObject(sqlReader);
            }
            return objectList;
        }
        private List<AssociatedPrintersInfo> MapObject(IDataReader sqlReader)
        {
            List<AssociatedPrintersInfo> objectList = new List<AssociatedPrintersInfo>();
            while (sqlReader.Read())
            {
                AssociatedPrintersInfo objectValue = new AssociatedPrintersInfo
                {
                    DG_AssociatedPrinters_Pkey = GetFieldValue(sqlReader, "DG_AssociatedPrinters_Pkey", 0),
                    DG_AssociatedPrinters_Name = GetFieldValue(sqlReader, "DG_AssociatedPrinters_Name", string.Empty),
                    DG_AssociatedPrinters_ProductType_ID = GetFieldValue(sqlReader, "DG_AssociatedPrinters_ProductType_ID", 0),
                    DG_AssociatedPrinters_IsActive = GetFieldValue(sqlReader, "DG_AssociatedPrinters_IsActive", false),
                    DG_AssociatedPrinters_PaperSize = GetFieldValue(sqlReader, "DG_AssociatedPrinters_PaperSize", string.Empty),
                    DG_AssociatedPrinters_SubStoreID = GetFieldValue(sqlReader, "DG_AssociatedPrinters_SubStoreID", 0),
                };

                objectList.Add(objectValue);
            }
            return objectList;
        }
        public List<AssociatedPrintersInfo> SelectAssociatedPrinter(int? ProductTypeID)
        {
            DBParameters.Clear();
            AddParameter("@ParamDG_AssociatedPrinters_ProductType_ID", SetNullIntegerValue(ProductTypeID));

            IDataReader sqlReader = ExecuteReader(DAOConstant.USP_SEL_ASSOCIATEDPRINTERIDFROMPRODUCTTYPEID);
            List<AssociatedPrintersInfo> AssociatedPrintersList = MapAssociatedPrintersInfo(sqlReader);
            sqlReader.Close();
            return AssociatedPrintersList;
        }
        private List<AssociatedPrintersInfo> MapAssociatedPrintersInfo(IDataReader sqlReader)
        {
            List<AssociatedPrintersInfo> AssociatedPrintersList = new List<AssociatedPrintersInfo>();
            while (sqlReader.Read())
            {
                AssociatedPrintersInfo AssociatedPrinters = new AssociatedPrintersInfo()
                {
                    DG_AssociatedPrinters_Pkey = GetFieldValue(sqlReader, "DG_AssociatedPrinters_Pkey", 0),
                    DG_AssociatedPrinters_Name = GetFieldValue(sqlReader, "DG_AssociatedPrinters_Name", string.Empty),
                    DG_AssociatedPrinters_ProductType_ID = GetFieldValue(sqlReader, "DG_AssociatedPrinters_ProductType_ID", 0),
                    DG_AssociatedPrinters_IsActive = GetFieldValue(sqlReader, "DG_AssociatedPrinters_IsActive", false),
                    DG_AssociatedPrinters_PaperSize = GetFieldValue(sqlReader, "DG_AssociatedPrinters_PaperSize", string.Empty),
                    DG_AssociatedPrinters_SubStoreID = GetFieldValue(sqlReader, "DG_AssociatedPrinters_SubStoreID", 0),

                };

                AssociatedPrintersList.Add(AssociatedPrinters);
            }
            return AssociatedPrintersList;
        }

        /// <summary>
        /// Only Top 1 records
        /// </summary>
        /// <param name="AssociatedPrinterskey"></param>
        /// <returns></returns>
        /// 


        public AssociatedPrintersInfo GetAssociatedPrintersByKey(int AssociatedPrinterskey)
        {
            DBParameters.Clear();
            AddParameter("@ParamDG_AssociatedPrinterskey", AssociatedPrinterskey);

            IDataReader sqlReader = ExecuteReader(DAOConstant.USP_GET_PRINTERNAMEFROMID);
            List<AssociatedPrintersInfo> AssociatedPrintersList = MapAssociatedPrintersBase(sqlReader);
            sqlReader.Close();
            return AssociatedPrintersList.FirstOrDefault();
        }
        private List<AssociatedPrintersInfo> MapAssociatedPrintersBase(IDataReader sqlReader)
        {
            List<AssociatedPrintersInfo> AssociatedPrintersList = new List<AssociatedPrintersInfo>();
            while (sqlReader.Read())
            {
                AssociatedPrintersInfo AssociatedPrinters = new AssociatedPrintersInfo()
                {
                    DG_AssociatedPrinters_Pkey = GetFieldValue(sqlReader, "DG_AssociatedPrinters_Pkey", 0),
                    DG_AssociatedPrinters_Name = GetFieldValue(sqlReader, "DG_AssociatedPrinters_Name", string.Empty),
                    DG_AssociatedPrinters_ProductType_ID = GetFieldValue(sqlReader, "DG_AssociatedPrinters_ProductType_ID", 0),
                    DG_AssociatedPrinters_IsActive = GetFieldValue(sqlReader, "DG_AssociatedPrinters_IsActive", false),
                    DG_AssociatedPrinters_PaperSize = GetFieldValue(sqlReader, "DG_AssociatedPrinters_PaperSize", string.Empty),
                    DG_AssociatedPrinters_SubStoreID = GetFieldValue(sqlReader, "DG_AssociatedPrinters_SubStoreID", 0),

                };

                AssociatedPrintersList.Add(AssociatedPrinters);
            }
            return AssociatedPrintersList;
        }


        public List<AssociatedPrintersInfo> SelectAssociatedPrinterInfo(int? ProductType_ID, int SubStoreID, bool IsActive)
        {
            DBParameters.Clear();
            AddParameter("@ParamProductType_ID", SetNullIntegerValue(ProductType_ID));
            AddParameter("@ParamSubStoreID", SubStoreID);
            AddParameter("@ParamIsActive", true);

            IDataReader sqlReader = ExecuteReader(DAOConstant.USP_SEL_ASSOCIATEDPRINTERINFO);
            List<AssociatedPrintersInfo> AssociatedPrintersList = MapDG_AssociatedPrintersBase(sqlReader);
            sqlReader.Close();
            return AssociatedPrintersList;
        }
        private List<AssociatedPrintersInfo> MapDG_AssociatedPrintersBase(IDataReader sqlReader)
        {
            List<AssociatedPrintersInfo> AssociatedPrintersList = new List<AssociatedPrintersInfo>();
            while (sqlReader.Read())
            {
                AssociatedPrintersInfo AssociatedPrinters = new AssociatedPrintersInfo()
                {
                    DG_AssociatedPrinters_Pkey = GetFieldValue(sqlReader, "DG_AssociatedPrinters_Pkey", 0),
                    DG_AssociatedPrinters_Name = GetFieldValue(sqlReader, "DG_AssociatedPrinters_Name", string.Empty),
                    DG_AssociatedPrinters_ProductType_ID = GetFieldValue(sqlReader, "DG_AssociatedPrinters_ProductType_ID", 0),
                    DG_AssociatedPrinters_IsActive = GetFieldValue(sqlReader, "DG_AssociatedPrinters_IsActive", false),
                    DG_AssociatedPrinters_PaperSize = GetFieldValue(sqlReader, "DG_AssociatedPrinters_PaperSize", string.Empty),
                    DG_AssociatedPrinters_SubStoreID = GetFieldValue(sqlReader, "DG_AssociatedPrinters_SubStoreID", 0),

                };

                AssociatedPrintersList.Add(AssociatedPrinters);
            }
            return AssociatedPrintersList;
        }

        public AssociatedPrintersInfo GetAssociatedPrinterIdFromPRoductTypeId(int? ProductTypeID)
        {
            DBParameters.Clear();
            AddParameter("@ParamDG_AssociatedPrinters_ProductType_ID", SetNullIntegerValue(ProductTypeID));

            IDataReader sqlReader = ExecuteReader(DAOConstant.USP_GET_ASSOCIATEDPRINTERIDFROMPRODUCTTYPEID);
            List<AssociatedPrintersInfo> AssociatedPrintersList = MapAssociatedPrintersInfo(sqlReader);
            sqlReader.Close();
            return AssociatedPrintersList.FirstOrDefault();
        }


        public void AddAndUpdatePrinterDetails(string PrinterName, int productType, bool isactive, string Papersize, int SubStoreId)
        {
            DBParameters.Clear();
            AddParameter("@ParamProductType_ID", productType);
            AddParameter("@ParamSubStoreID", SubStoreId);            
            AddParameter("@ParamPaperSize", Papersize);
            AddParameter("@ParamName", PrinterName);
            AddParameter("@ParamIsActive", isactive);
            ExecuteNonQuery(DAOConstant.USP_UPDANDINS_PRINTERDETAILS);                     
        }
        public List<PrinterDetailsInfo> GetAssociatedPrinters(int? ProductTypeID)
        {
            DBParameters.Clear();
            AddParameter("@ParamProductTypeID", SetNullIntegerValue(ProductTypeID));

            IDataReader sqlReader = ExecuteReader(DAOConstant.USP_SEL_GETASSOCIATEDPRINTERS);
            List<PrinterDetailsInfo> AssociatedPrintersList = MapAssociatedPrinters(sqlReader);
            sqlReader.Close();
            return AssociatedPrintersList;
        }

        private List<PrinterDetailsInfo> MapAssociatedPrinters(IDataReader sqlReader)
        {
            List<PrinterDetailsInfo> AssociatedPrintersList = new List<PrinterDetailsInfo>();
            while (sqlReader.Read())
            {
                PrinterDetailsInfo AssociatedPrinters = new PrinterDetailsInfo()
                {
                    PrinterName = GetFieldValue(sqlReader, "DG_Printers_Pkey", string.Empty),
                    PrinterID = GetFieldValue(sqlReader, "DG_Printers_NickName", 0),
                };
                AssociatedPrintersList.Add(AssociatedPrinters);
            }
            return AssociatedPrintersList;
        }
    }
}
