﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DigiPhoto.IMIX.Model;
using System.Data;


namespace DigiPhoto.IMIX.DataAccess
{
    public class EvoucherDao : BaseDataAccess
    {
        #region Constructor
        public EvoucherDao(BaseDataAccess baseaccess)
            : base(baseaccess)
        { }
        public EvoucherDao()
        { }
        #endregion
        public bool SaveEVoucherMstData(DataTable EvMst)
        {
            DBParameters.Clear();
            AddParameter("@EvMst", EvMst);
            ExecuteNonQuery("SaveEVoucherMstData");
            return true;
        }

        public bool SaveEVoucherProductDetails(DataTable EvDtls)
        {
            DBParameters.Clear();
            AddParameter("@EvPrdDtlsMst", EvDtls);
            ExecuteNonQuery("SaveEVoucherProductDetails");
            return true;
        }
        public bool SaveTourOperatorMaster(DataTable EvDtls)
        {
            DBParameters.Clear();
            AddParameter("@TourList", EvDtls);
            ExecuteNonQuery("SaveTourOperatorMaster");
            return true;
        }
        public bool SaveEVoucherBarcodes(DataTable EvStatus)
        {
            DBParameters.Clear();
            AddParameter("@EvBarcode", EvStatus);
            ExecuteNonQuery("SaveEVoucherBarcodes");
            return true;
        }
        public List<EVoucherBarcodes> UpdEvoucherBarcodeNofUsedList()
        {
            DBParameters.Clear();
            IDataReader sqlReader = ExecuteReader("GET_UpdEvoucherBarcodeNoofUsedList");
            List<EVoucherBarcodes> EvchrUpdList = UpdEvoucherBarcodeNoofUsedList(sqlReader);
            sqlReader.Close();
            return EvchrUpdList;
        }

        private List<EVoucherBarcodes> UpdEvoucherBarcodeNoofUsedList(IDataReader sqlReader)
        {
            List<EVoucherBarcodes> EvchrUpdList = new List<EVoucherBarcodes>();
            while (sqlReader.Read())
            {
                EVoucherBarcodes EvchrType = new EVoucherBarcodes()
                {
                    EVoucherBarcodeID = GetFieldValue(sqlReader, "EVoucherBarcodeID", 0L),
                    NoofTimeUsed = GetFieldValue(sqlReader, "NoofTimeUsed", 0),
                    OrderNos = GetFieldValue(sqlReader, "OrderNos", string.Empty),
                    ID = GetFieldValue(sqlReader, "ID", 0L),
                    EVoucherID = GetFieldValue(sqlReader, "EVoucherID", 0),
                };

                EvchrUpdList.Add(EvchrType);
            }
            return EvchrUpdList;
        }

        public EvoucherClaiminfo EvoucherClaim(string Barcode)
        {
            DBParameters.Clear();
            AddParameter("@Barcode", Barcode);
            IDataReader sqlReader = ExecuteReader("GET_ClaimEvoucherBarcode");
            EvoucherClaiminfo EvclmResp = PopulateClaiminfo(sqlReader);
            sqlReader.Close();
            return EvclmResp;

        }
        public EvoucherClaiminfo PopulateClaiminfo(IDataReader sqlReader)
        {
            EvoucherClaiminfo Evoucher = new EvoucherClaiminfo();
            while (sqlReader.Read())
            {
                Evoucher.Barcode = GetFieldValue(sqlReader, "Barcode", string.Empty);
                Evoucher.Amount = GetFieldValue(sqlReader, "Amount",0);
                Evoucher.VoucherValidity = GetFieldValue(sqlReader, "VoucherValidity", DateTime.Now);
                Evoucher.Redeem = GetFieldValue(sqlReader, "Redeem", false);
                Evoucher.Result = GetFieldValue(sqlReader, "Result", string.Empty);
            }
            return Evoucher;
        }
        public int SaveEvoucherItemDtls(string EvocuerBarcode, int LineItemID, decimal DiscountAmt)
        {
            DBParameters.Clear();
            AddParameter("@EvoucherDiscAMT", DiscountAmt);
            AddParameter("@EvoucherCode", EvocuerBarcode);
            AddParameter("@LineItem", LineItemID);
            IDataReader sqlReader = ExecuteReader("SP_UPDATE_DGOrdersLineItems");
            int RetnVal =Convert.ToInt32(PopulateClaiminfo(sqlReader));
            sqlReader.Close();
            return RetnVal;

        }
        /////////////for Evoucher end


    }
}
