﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DigiPhoto.IMIX.Model;
using System.Data;

namespace DigiPhoto.IMIX.DataAccess
{
    public class BackGroundDao : BaseDataAccess
    {
        #region Constructor
        public BackGroundDao(BaseDataAccess baseaccess)
            : base(baseaccess)
        { }
        public BackGroundDao()
        { }
        #endregion
        public List<BackGroundInfo> SelectBackground()
        {
            DBParameters.Clear();
            this.OpenConnection();
            AddParameter("@ParamBackgroundId", SetNullIntegerValue(null));
            AddParameter("@ParamProductId", SetNullIntegerValue(null));
            AddParameter("@ParamGroupId", SetNullIntegerValue(null));
            AddParameter("@ParamBackgroundImageName", SetNullStringValue(null));

            List<BackGroundInfo> objectList;
            using (IDataReader sqlReader = ExecuteReader(DAOConstant.USP_SEL_BACKGROUND))
            {
                objectList = MapObject(sqlReader);
            }
            return objectList;
        }
        public List<BackGroundInfo> Select(int? backgroundId, int productId, int groupId, string backgroundImageName)
        {
            DBParameters.Clear();
            AddParameter("@ParamBackgroundId", SetNullIntegerValue(backgroundId));
            AddParameter("@ParamProductId", SetNullIntegerValue(productId));
            AddParameter("@ParamGroupId", SetNullIntegerValue(groupId));
            AddParameter("@ParamBackgroundImageName", SetNullStringValue(backgroundImageName));

            List<BackGroundInfo> objectList;
            using (IDataReader sqlReader = ExecuteReader(DAOConstant.USP_SEL_BACKGROUND))
            {
                objectList = MapObject(sqlReader);
            }
            return objectList;
        }

        private List<BackGroundInfo> MapObject(IDataReader sqlReader)
        {
            List<BackGroundInfo> objectList = new List<BackGroundInfo>();
            try
            {
            while (sqlReader.Read())
            {
                BackGroundInfo objectValue = new BackGroundInfo
                {
                    DG_Background_pkey = GetFieldValue(sqlReader, "DG_Background_pkey", 0),
                    DG_Product_Id = GetFieldValue(sqlReader, "DG_Product_Id", 0),
                    DG_BackGround_Image_Name = GetFieldValue(sqlReader, "DG_BackGround_Image_Name", string.Empty),
                    DG_BackGround_Image_Display_Name = GetFieldValue(sqlReader, "DG_BackGround_Image_Display_Name", string.Empty),
                    DG_BackGround_Group_Id = GetFieldValue(sqlReader, "DG_BackGround_Group_Id", 0),
                    SyncCode = GetFieldValue(sqlReader, "SyncCode", string.Empty),
                    IsSynced = GetFieldValue(sqlReader, "IsSynced", false),
                        DG_Background_IsActive = GetFieldValue(sqlReader, "DG_Background_IsActive", false),
                        DG_Background_IsPanorama = GetFieldValue(sqlReader, "IsPanorama", false),
                    };

                objectList.Add(objectValue);
            }
            }
            catch(Exception ex)
            {

            }
            return objectList;
        }
        public BackGroundInfo Get(int? backgroundId, int productId, int groupId, string backgroundImageName)
        {
            DBParameters.Clear();
            AddParameter("@ParamBackgroundId", SetNullIntegerValue(backgroundId));
            AddParameter("@ParamProductId", SetNullIntegerValue(productId));
            AddParameter("@ParamGroupId", SetNullIntegerValue(groupId));
            AddParameter("@ParamBackgroundImageName", SetNullStringValue(backgroundImageName));

            List<BackGroundInfo> objectList;
            using (IDataReader sqlReader = ExecuteReader(DAOConstant.USP_SEL_BACKGROUND))
            {
                objectList = MapObject(sqlReader);
            }
            return objectList.FirstOrDefault();
        }
        public List<BackGroundInfo> GetBackgroundByGroup()
        {
            DBParameters.Clear();
            List<BackGroundInfo> objectList;
            using (IDataReader sqlReader = ExecuteReader(DAOConstant.USP_SEL_BACKGROUNDBYGROUPBYID))
            {
                objectList = MapObject(sqlReader);
            }
            return objectList;
        }
        
        #region Modified by Ajay to save panorama background status
        //to insert background image name
        public int Add(BackGroundInfo objectInfo)
        {
            DBParameters.Clear();
            AddParameter("@ParamDG_Product_Id", objectInfo.DG_Product_Id);
            AddParameter("@ParamDG_BackGround_Image_Name", objectInfo.DG_BackGround_Image_Name);
            AddParameter("@ParamDG_BackGround_Image_Display_Name", objectInfo.DG_BackGround_Image_Display_Name);
            AddParameter("@ParamDG_BackGround_Group_Id", SetNullIntegerValue(objectInfo.DG_BackGround_Group_Id));
            AddParameter("@ParamSyncCode", objectInfo.SyncCode);
            AddParameter("@ParamIsSynced", objectInfo.IsSynced);
            AddParameter("@ParamIsActive", objectInfo.DG_Background_IsActive);
            AddParameter("@ParamCreatedBy", objectInfo.CreatedBy);
            AddParameter("@ParamIsPanorama", objectInfo.DG_Background_IsPanorama);
            AddParameter("@ParamBackGroundID", objectInfo.DG_Background_pkey, ParameterDirection.Output);
            ExecuteNonQuery(DAOConstant.USP_INS_DG_BACKGROUND);
            int objectId = (int)GetOutParameterValue("@ParamBackGroundID");
            return objectId;
        } 
        #endregion
        public bool Update(BackGroundInfo objectInfo)
        {
            DBParameters.Clear();
            AddParameter("@ParamDG_Background_pkey", objectInfo.DG_Background_pkey);
            AddParameter("@ParamDG_Product_Id", objectInfo.DG_Product_Id);
            AddParameter("@ParamDG_BackGround_Image_Name", objectInfo.DG_BackGround_Image_Name);
            AddParameter("@ParamDG_BackGround_Image_Display_Name", objectInfo.DG_BackGround_Image_Display_Name);
            AddParameter("@ParamDG_BackGround_Group_Id", objectInfo.DG_BackGround_Group_Id);
            AddParameter("@ParamIsSynced", objectInfo.IsSynced);
            AddParameter("@ParamIsActive", objectInfo.DG_Background_IsActive);
            AddParameter("@ParamModifiedBy", objectInfo.ModifiedBy);
            AddParameter("@IsPanoramic", objectInfo.DG_Background_IsPanorama);
            ExecuteNonQuery(DAOConstant.USP_UPD_BACKGROUND);
            return true;
        }

        public bool Delete(int bgID)
        {
            DBParameters.Clear();
            AddParameter("@ParamBGId", bgID);
            ExecuteNonQuery(DAOConstant.USP_DEL_BACKGROUNDBYPRODUCTIDGROUPID);
            return true;
        }
    }
}
