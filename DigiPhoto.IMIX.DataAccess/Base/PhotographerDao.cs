﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DigiPhoto.IMIX.Model;
using System.Data;


namespace DigiPhoto.IMIX.DataAccess
{   
    public class PhotographerDao : BaseDataAccess
    {
        #region Constructor
        public PhotographerDao(BaseDataAccess baseaccess)
            : base(baseaccess)
        { }
        public PhotographerDao()
        { }
        #endregion

        public bool UPD_GrpCount(Int32 userID)
        {
            DBParameters.Clear();
            AddParameter("@ParamUserID", userID);

            ExecuteNonQuery(DAOConstant.USP_OperatorGrp);
            return true;
        }

    }
}
