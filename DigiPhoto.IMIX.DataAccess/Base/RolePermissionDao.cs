﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DigiPhoto.IMIX.Model;
using System.Data;

namespace DigiPhoto.IMIX.DataAccess
{
    public class RolePermissionDao : BaseDataAccess
    {
        #region Constructor
        public RolePermissionDao(BaseDataAccess baseaccess)
            : base(baseaccess)
        { }
        public RolePermissionDao()
        { }
        #endregion




        public List<PermissionRoleInfo> SelectRolePermissions(int RoleId)
        {
            DBParameters.Clear();
            AddParameter("@ParamRoleId", RoleId);

            IDataReader sqlReader = ExecuteReader(DAOConstant.USP_SEL_ROLEPERMISSIONS);
            List<PermissionRoleInfo> PermissionRoleList = MapRolePermissionsInfo(sqlReader);
            sqlReader.Close();
            return PermissionRoleList;
        }
        private List<PermissionRoleInfo> MapRolePermissionsInfo(IDataReader sqlReader)
        {
            List<PermissionRoleInfo> PermissionRoleList = new List<PermissionRoleInfo>();
            while (sqlReader.Read())
            {
                PermissionRoleInfo PermissionRole = new PermissionRoleInfo()
                {
                    DG_Permission_Role_pkey = GetFieldValue(sqlReader, "DG_Permission_Role_pkey", 0),
                    DG_User_Roles_Id = GetFieldValue(sqlReader, "DG_User_Roles_Id", 0),
                    DG_Permission_Id = GetFieldValue(sqlReader, "DG_Permission_Id", 0),

                };

                PermissionRoleList.Add(PermissionRole);
            }
            return PermissionRoleList;
        }

        public List<PermissionInfo> SelectPermission(string PermissionId, string PermissionName)
        {
            DBParameters.Clear();
            AddParameter("@ParamPermissionId", SetNullStringValue(PermissionId));
            AddParameter("@ParamPermissionName", SetNullStringValue(PermissionName));

            IDataReader sqlReader = ExecuteReader(DAOConstant.USP_SEL_PERMISSION);
            List<PermissionInfo> PermissionsList = MapPermissionInfo(sqlReader);
            sqlReader.Close();
            return PermissionsList;
        }
        private List<PermissionInfo> MapPermissionInfo(IDataReader sqlReader)
        {
            List<PermissionInfo> PermissionsList = new List<PermissionInfo>();
            while (sqlReader.Read())
            {
                PermissionInfo Permissions = new PermissionInfo()
                {
                    DG_Permission_pkey = GetFieldValue(sqlReader, "DG_Permission_pkey", 0),
                    DG_Permission_Name = GetFieldValue(sqlReader, "DG_Permission_Name", string.Empty),
                    SyncCode = GetFieldValue(sqlReader, "SyncCode", string.Empty),
                    IsSynced = GetFieldValue(sqlReader, "IsSynced", false),

                };

                PermissionsList.Add(Permissions);
            }
            return PermissionsList;
        }
        public bool InsertPermissionData(int RoleId, int PermissonId, bool Retvalue)
        {
            DBParameters.Clear();
            AddParameter("@ParamRoleId", RoleId);
            AddParameter("@ParamPermissonId", PermissonId);
            AddParameter("@Retvalue", Retvalue, ParameterDirection.Output);

            ExecuteNonQuery(DAOConstant.USP_INS_PERMISSIONDATA);
            bool objectId = (bool)GetOutParameterValue("@Retvalue");

            return objectId;
        }


        public bool RemovePermissionData(int User_Roles_Id, int Permission_Id)
        {
            DBParameters.Clear();
            AddParameter("@ParamUser_Roles_Id", User_Roles_Id);
            AddParameter("@ParamPermission_Id", Permission_Id);

            ExecuteReader(DAOConstant.USP_DEL_PERMISSIONDATA);
            return true;
        }
        public List<PermissionRoleInfo> SelectRolePermissionsWithFlag(int RoleId,bool Flag)
        {
            DBParameters.Clear();
            AddParameter("@ParamRoleId", RoleId);

            IDataReader sqlReader = ExecuteReader(DAOConstant.USP_SEL_ROLEPERMISSIONS);
            List<PermissionRoleInfo> PermissionRoleList = MapRolePermissionsInfo(sqlReader);
            sqlReader.Close();
            return PermissionRoleList;
        }

        public bool SetremovePermissionData(DataTable udt_Permission, bool Retvalue)
        {
            DBParameters.Clear();
            AddParameter("@ParamPermissionDataInfo", udt_Permission);
            AddParameter("@Retvalue", Retvalue, ParameterDirection.Output);
            ExecuteReader(DAOConstant.USP_INSANDDEL_PERMISSIONDATA);
            bool objectId = (bool)GetOutParameterValue("@Retvalue");
            return objectId;
        }

        public List<RoleInfo> GetChildUserData(int RoleId)
        {
            DBParameters.Clear();
            AddParameter("@ParamRoleId", RoleId);

            IDataReader sqlReader = ExecuteReader(DAOConstant.USP_SEL_CHILDUSERLIST);
            List<RoleInfo> PermissionRoleList = MapGetChildUserData(sqlReader);
            sqlReader.Close();
            return PermissionRoleList;
        }
        private List<RoleInfo> MapGetChildUserData(IDataReader sqlReader)
        {
            List<RoleInfo> PermissionsList = new List<RoleInfo>();
            while (sqlReader.Read())
            {
                RoleInfo Permissions = new RoleInfo()
                {
                    DG_User_Roles_pkey = GetFieldValue(sqlReader, "DG_User_Roles_pkey", 0),
                    DG_User_Role = GetFieldValue(sqlReader, "DG_User_Role", string.Empty),
                    SyncCode = GetFieldValue(sqlReader, "SyncCode", string.Empty),
                    IsSynced = GetFieldValue(sqlReader, "IsSynced", false),

                };

                PermissionsList.Add(Permissions);
            }
            return PermissionsList;
        }
    }
}
