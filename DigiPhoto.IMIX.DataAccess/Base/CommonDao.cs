﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DigiPhoto.IMIX.Model;
using System.Data;
namespace DigiPhoto.IMIX.DataAccess
{
    public class CommonDao : BaseDataAccess
    {
        #region Constructor
        public CommonDao(BaseDataAccess baseaccess)
            : base(baseaccess)
        {

        }

        public CommonDao()
        {

        }
        #endregion
        public List<TableBaseInfo> SelectAllTable()
        {

            IDataReader sqlReader = ExecuteReader(DAOConstant.USP_SEL_ALLTABLE);
            List<TableBaseInfo> AllTable = MapAllTableBase(sqlReader);
            sqlReader.Close();
            return AllTable;
        }
        private List<TableBaseInfo> MapAllTableBase(IDataReader sqlReader)
        {
            List<TableBaseInfo> AllTableList = new List<TableBaseInfo>();
            while (sqlReader.Read())
            {
                TableBaseInfo AllTable = new TableBaseInfo()
                {
                    name = GetFieldValue(sqlReader, "name", string.Empty)

                };
                AllTableList.Add(AllTable);
            }
            return AllTableList;
        }
       

        public bool ImportMasterData(DataTable dtSite, DataTable dtItem, DataTable dtpkg)
        {
            DBParameters.Clear();
            AddParameter("@udt_Site", dtSite);
            AddParameter("@udt_Item", dtItem);
            AddParameter("@udt_Pkg", dtpkg);
            ExecuteNonQuery("UploadMasterData");
            return true; ;
        }

        #region Added by Ajay for panoramic sizes   
        public string[] GetPanoramicSizes()
        {

            string[] PanoramicSizes = { "8 * 26", "8 * 24", "8 * 18", "6 * 14", "6 * 20" };
            return PanoramicSizes;
        }

        public string[] GetPanoramicSizesCode()
        {
            string[] PanoramicSizes = { "120", "121", "122", "123", "125" };
            return PanoramicSizes;
        }
        #endregion
    }
}
