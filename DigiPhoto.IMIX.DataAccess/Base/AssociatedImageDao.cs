﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DigiPhoto.IMIX.Model;
using System.Data;


namespace DigiPhoto.IMIX.DataAccess
{
    public class AssociatedImageDao : BaseDataAccess
    {
        #region Constructor
        public AssociatedImageDao(BaseDataAccess baseaccess)
            : base(baseaccess)
        { }
        public AssociatedImageDao()
        { }


        #endregion



        public List<iMixImageAssociationInfo> SelectAssociatedImage(string CardUniqueIdentifier)
        {
            DBParameters.Clear();
            AddParameter("@ParamCardUniqueIdentifier", CardUniqueIdentifier);

            IDataReader sqlReader = ExecuteReader(DAOConstant.USP_SEL_ASSOCIATEDIMAGE);
            List<iMixImageAssociationInfo> iMixImageAssociationList = MapiMixImageAssociationBase(sqlReader);
            sqlReader.Close();
            return iMixImageAssociationList;
        }
        private List<iMixImageAssociationInfo> MapiMixImageAssociationBase(IDataReader sqlReader)
        {
            List<iMixImageAssociationInfo> iMixImageAssociationList = new List<iMixImageAssociationInfo>();
            while (sqlReader.Read())
            {
                iMixImageAssociationInfo iMixImageAssociation = new iMixImageAssociationInfo()
                {
                    IMIXImageAssociationId = GetFieldValue(sqlReader, "IMIXImageAssociationId", 0),
                    IMIXCardTypeId = GetFieldValue(sqlReader, "IMIXCardTypeId", 0),
                    PhotoId = GetFieldValue(sqlReader, "PhotoId", 0),
                    CardUniqueIdentifier = GetFieldValue(sqlReader, "CardUniqueIdentifier", string.Empty),
                    MappedIdentifier = GetFieldValue(sqlReader, "MappedIdentifier", string.Empty),
                };

                iMixImageAssociationList.Add(iMixImageAssociation);
            }
            return iMixImageAssociationList;
        }

        public string AssociateImage(int OverWriteStatus, bool IsAnonymousQrCodeEnabled, string UniqueCode, string PhotoIds, int nationality,string EmailID)
        {
            string Result = string.Empty;
            DBParameters.Clear();
            AddParameter("@OverWriteStatus", OverWriteStatus);
            AddParameter("@IsAnonymousQrCodeEnabled", IsAnonymousQrCodeEnabled);
            AddParameter("@UniqueCode", UniqueCode);
            AddParameter("@PhotoIds", PhotoIds);
            AddParameter("@Nationality", nationality); 
            AddParameter("@EmailID", EmailID);
            AddParameter("@Result", Result, ParameterDirection.Output);
            ExecuteNonQuery(DAOConstant.ASSOCIATEIMAGE);
            object objectId = GetOutParameterValue("@Result");
            return objectId.ToString();
        }
        public void AssociateMobileImage(string UniqueCode, int PhotoId)
        {
            string Result = string.Empty;
            DBParameters.Clear();
            AddParameter("@UniqueCode", UniqueCode);
            AddParameter("@PhotoId", PhotoId);
            ExecuteNonQuery(DAOConstant.ASSOCIATEMOBILEIMAGE);
        }
        public void AssociateVideos(int PhotoId, int VideoId)
        {
            string Result = string.Empty;
            DBParameters.Clear();
           
            AddParameter("@PhotoId", PhotoId);
            AddParameter("@VideoId", VideoId);
            ExecuteNonQuery(DAOConstant.ASSOCIATEVIDEO);
        }
        /// <summary>
        /// //created by latika for presold service 2May19
        /// </summary>
        /// <param name="UniqueCode"></param>
        /// <param name="PhotoIds"></param>
        /// <returns></returns>
        public int RemoveAssociateImage(string UniqueCode,string PhotoIds,bool IsShowCount)
        {
            int Result = 0;
            DBParameters.Clear();
            AddParameter("@UniqueCode", UniqueCode);
            AddParameter("@PhotoIds", PhotoIds);
            AddParameter("@IsShowCount", IsShowCount);
            Result = Convert.ToInt32(ExecuteScalar("RemoveAssociateImage"));
            return Result;
        }
        ///End by latika
    }
}
