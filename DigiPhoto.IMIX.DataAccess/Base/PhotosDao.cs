﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DigiPhoto.IMIX.Model;
using System.Data;
using DigiPhoto.IMIX.Model.Base;

namespace DigiPhoto.IMIX.DataAccess
{
    public class PhotosDao : BaseDataAccess
    {
        #region Constructor
        public PhotosDao(BaseDataAccess baseaccess)
            : base(baseaccess)
        { }
        public PhotosDao()
        { }
        #endregion

        public bool DeletePhotoGroupBySubStoreIdAndDays(int days, int substoreID)
        {
            DBParameters.Clear();
            AddParameter("@ParamDays", days);
            AddParameter("@ParamSubstoreId", substoreID);
            ExecuteNonQuery(DAOConstant.USP_DEL_PHOTOGROUPBYSUBSOTREIDANDDAYS);
            return true;
        }
        public bool DelPhotoGroupBySubstoreId(int SubstoreId)
        {
            DBParameters.Clear();
            AddParameter("@ParamSubstoreId", SubstoreId);
            ExecuteNonQuery("dbo.usp_DEL_PhotoGroup");
            return true;
        }
        public void UpdateArchiveByFileName(string PhotosFileName)
        {
            DBParameters.Clear();
            AddParameter("@ParamPhotosFileName", PhotosFileName);
            ExecuteNonQuery(DAOConstant.USP_UPD_ARCHIVEDETAILS);
        }
      
        public int InsertPhotoDetails(int substoreId, int Locationid, int? RfidScanType, int DG_Photos_pkey, DateTime DG_Photos_CreatedOn, DateTime? DateTaken, string DG_Photos_RFID, string DG_Photos_FileName, string Userid, string Imgmetadata, string Photolayer, string DG_Photos_Effects, int? IsImageProcessed, int? CharacterID, long? VideoLength = null, bool? IsVideoProcessed = false, int? mediatype = 1, int? ParentImageId = 0, string OriginalFileName = null, int? SemiOrderProfileId = 0, bool isCroped = false, long photoDisplayOrder = 0)
        {
            DBParameters.Clear();
            AddParameter("@ParamSubStoreId", substoreId);
            AddParameter("@ParamLocationid", Locationid);
            AddParameter("@ParamRfidScanType", RfidScanType);//SetNullIntegerValue(RfidScanType));
            AddParameter("@DG_Photos_pkey", DG_Photos_pkey, ParameterDirection.Output);
            AddParameter("@ParamDG_Photos_CreatedOn", DG_Photos_CreatedOn);
            AddParameter("@ParamDateTaken", SetNullDateTimeValue(DateTaken));
            AddParameter("@ParamDG_Photos_RFID", DG_Photos_RFID);
            AddParameter("@ParamDG_Photos_FileName", DG_Photos_FileName);
            AddParameter("@ParamUserid", Userid);
            AddParameter("@ParamImgmetadata", Imgmetadata);
            AddParameter("@ParamPhotolayer", Photolayer);
            AddParameter("@ParamDG_Photos_Effects", DG_Photos_Effects);
            AddParameter("@ParamIsImageProcessed", IsImageProcessed ?? 0);
            AddParameter("@ParamCharacterID", SetNullIntegerValue(CharacterID));
            // string ext = System.IO.Path.GetExtension(DG_Photos_FileName).ToLower();           
            //if (ext == ".jpg")
            //{
            //    mediatype = 1;
            //}
            //else
            //{
            //    if (DG_Photos_FileName.Contains("_PR"))
            //    {
            //        mediatype =3;
            //    }
            //    else
            //    {
            //        mediatype = 2;
            //    }
            //}
            AddParameter("@ParamDG_MediaType", mediatype);
            AddParameter("@ParamVideoLength", VideoLength ?? 0);
            AddParameter("@ParamIsVideoProcessed", IsVideoProcessed ?? false);
            AddParameter("@OriginalFileName", OriginalFileName);
            AddParameter("@ParentImageId", ParentImageId);
            AddParameter("@ParamSemiOrderProfileId", SemiOrderProfileId);
            AddParameter("@ParamIsCroped", isCroped);
            AddParameter("@ParamDisplayOrder", photoDisplayOrder);

            ExecuteNonQuery(DAOConstant.USP_INS_PHOTODETAILS);
            int objectId = (int)GetOutParameterValue("@DG_Photos_pkey");
            return objectId;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="Photos_pkey"></param>
        /// <param name="Value"></param>
        /// <param name="Operation"></param>
        /// <returns></returns>
        public bool UpdateCropedPhotos(int Photos_pkey, object Value, string Operation)
        {
            DBParameters.Clear();
            AddParameter("@ParamPhotos_pkey", Photos_pkey);
            if (Value != null)
                AddParameter("@ParamValue", Value);
            AddParameter("@ParamOperation", Operation);
            ExecuteNonQuery(DAOConstant.USP_UPD_CROPEDPHOTOS);
            return true;
        }

        public bool UpdateCropedPhotosStoryBook(int Photos_pkey, object Value, string Operation)
        {
            DBParameters.Clear();
            AddParameter("@ParamPhotos_pkey", Photos_pkey);
            if (Value != null)
                AddParameter("@ParamValue", Value);
            AddParameter("@ParamOperation", Operation);
            ExecuteNonQuery(DAOConstant.USP_UPD_CROPEDPHOTOSSTORYBOOK);
            return true;
        }

        public PhotoInfo GetPhotosByPkey(int Photoskey)
        {
            DBParameters.Clear();
            AddParameter("@ParamPhotoskey", Photoskey);
            IDataReader sqlReader = ExecuteReader(DAOConstant.USP_GET_PHOTOSBYPKEY);
            List<PhotoInfo> PhotosList = MapPhotosByPkey(sqlReader);
            sqlReader.Close();
            return PhotosList.FirstOrDefault();
        }
        public PhotoInfo GetPhotosByPkeyStoryBook(int Photoskey)
        {
            DBParameters.Clear();
            AddParameter("@ParamPhotoskey", Photoskey);
            IDataReader sqlReader = ExecuteReader(DAOConstant.USP_GET_PHOTOSBYPKEYSTORYBOOK);
            List<PhotoInfo> PhotosList = MapPhotosByPkeyStoryBook(sqlReader);
            sqlReader.Close();
            return PhotosList.FirstOrDefault();
        }
        #region Added BY Ajay

        public string GetOrderProductName(int PhotoId)
        {
            DBParameters.Clear();
            AddParameter("@DG_Photos_ID", PhotoId);
            string PhotosName = Convert.ToString(ExecuteScalar("usp_DG_GetOrderProductName"));
            return PhotosName;
        }


        #endregion
        private List<PhotoInfo> MapPhotosByPkey(IDataReader sqlReader)
        {
            List<PhotoInfo> PhotosList = new List<PhotoInfo>();
            while (sqlReader.Read())
            {
                PhotoInfo Photos = new PhotoInfo()
                {
                    DG_Photos_pkey = GetFieldValue(sqlReader, "DG_Photos_pkey", 0),
                    DG_Photos_FileName = GetFieldValue(sqlReader, "DG_Photos_FileName", string.Empty),
                    DG_Photos_CreatedOn = GetFieldValue(sqlReader, "DG_Photos_CreatedOn", DateTime.Now),
                    DG_Photos_RFID = GetFieldValue(sqlReader, "DG_Photos_RFID", string.Empty),
                    DG_Photos_UserID = GetFieldValue(sqlReader, "DG_Photos_UserID", 0),
                    DG_Photos_Background = GetFieldValue(sqlReader, "DG_Photos_Background", string.Empty),
                    DG_Photos_Frame = GetFieldValue(sqlReader, "DG_Photos_Frame", string.Empty),
                    DG_Photos_DateTime = GetFieldValue(sqlReader, "DG_Photos_DateTime", DateTime.Now),
                    DG_Photos_Layering = GetFieldValue(sqlReader, "DG_Photos_Layering", string.Empty),
                    DG_Photos_Effects = GetFieldValue(sqlReader, "DG_Photos_Effects", string.Empty),
                    DG_Photos_IsCroped = GetFieldValue(sqlReader, "DG_Photos_IsCroped", false),
                    DG_Photos_IsRedEye = GetFieldValue(sqlReader, "DG_Photos_IsRedEye", false),
                    DG_Photos_IsGreen = GetFieldValue(sqlReader, "DG_Photos_IsGreen", false),
                    DG_Photos_MetaData = GetFieldValue(sqlReader, "DG_Photos_MetaData", string.Empty),
                    DG_Photos_Sizes = GetFieldValue(sqlReader, "DG_Photos_Sizes", string.Empty),
                    DG_Photos_Archive = GetFieldValue(sqlReader, "DG_Photos_Archive", false),
                    DG_Location_Id = GetFieldValue(sqlReader, "DG_Location_Id", 0),
                    DG_SubStoreId = GetFieldValue(sqlReader, "DG_SubStoreId", 0),
                    DG_IsCodeType = GetFieldValue(sqlReader, "DG_IsCodeType", false),
                    DateTaken = GetFieldValue(sqlReader, "DateTaken", DateTime.Now),
                    RfidScanType = GetFieldValue(sqlReader, "RfidScanType", 0),
                    HotFolderPath = GetFieldValue(sqlReader, "HotFolderPath", string.Empty),
                    DG_MediaType = GetFieldValue(sqlReader, "DG_MediaType", 0),
                    DG_VideoLength = GetFieldValue(sqlReader, "DG_VideoLength", 0L),
                    SemiOrderProfileId = GetFieldValue(sqlReader, "SemiOrderProfileId", 0),
                    IsGumRideShow = GetFieldValue(sqlReader, "IsGumRideShow", false),
                    OnlineQRCode = GetFieldValue(sqlReader, "OnlineQRCode", string.Empty),
                };

                PhotosList.Add(Photos);
            }
            return PhotosList;
        }

        private List<PhotoInfo> MapPhotosByPkeyStoryBook(IDataReader sqlReader)
        {
            List<PhotoInfo> PhotosList = new List<PhotoInfo>();
            while (sqlReader.Read())
            {
                PhotoInfo Photos = new PhotoInfo()
                {
                    DG_Photos_pkey = GetFieldValue(sqlReader, "DG_Photos_pkey", 0),
                    DG_Photos_FileName = GetFieldValue(sqlReader, "DG_Photos_FileName", string.Empty),
                    DG_Photos_CreatedOn = GetFieldValue(sqlReader, "DG_Photos_CreatedOn", DateTime.Now),
                    DG_Photos_RFID = GetFieldValue(sqlReader, "DG_Photos_RFID", string.Empty),
                    DG_Photos_UserID = GetFieldValue(sqlReader, "DG_Photos_UserID", 0),
                    DG_Photos_Background = GetFieldValue(sqlReader, "DG_Photos_Background", string.Empty),
                    DG_Photos_Frame = GetFieldValue(sqlReader, "DG_Photos_Frame", string.Empty),
                    DG_Photos_DateTime = GetFieldValue(sqlReader, "DG_Photos_DateTime", DateTime.Now),
                    DG_Photos_Layering = GetFieldValue(sqlReader, "DG_Photos_Layering", string.Empty),
                    DG_Photos_Effects = GetFieldValue(sqlReader, "DG_Photos_Effects", string.Empty),
                    DG_Photos_IsCroped = GetFieldValue(sqlReader, "DG_Photos_IsCroped", false),
                    DG_Photos_IsRedEye = GetFieldValue(sqlReader, "DG_Photos_IsRedEye", false),
                    DG_Photos_IsGreen = GetFieldValue(sqlReader, "DG_Photos_IsGreen", false),
                    DG_Photos_MetaData = GetFieldValue(sqlReader, "DG_Photos_MetaData", string.Empty),
                    DG_Photos_Sizes = GetFieldValue(sqlReader, "DG_Photos_Sizes", string.Empty),
                    DG_Photos_Archive = GetFieldValue(sqlReader, "DG_Photos_Archive", false),
                    DG_Location_Id = GetFieldValue(sqlReader, "DG_Location_Id", 0),
                    DG_SubStoreId = GetFieldValue(sqlReader, "DG_SubStoreId", 0),
                    DG_IsCodeType = GetFieldValue(sqlReader, "DG_IsCodeType", false),
                    DateTaken = GetFieldValue(sqlReader, "DateTaken", DateTime.Now),
                    RfidScanType = GetFieldValue(sqlReader, "RfidScanType", 0),
                    HotFolderPath = GetFieldValue(sqlReader, "HotFolderPath", string.Empty),
                    DG_MediaType = GetFieldValue(sqlReader, "DG_MediaType", 0),
                    DG_VideoLength = GetFieldValue(sqlReader, "DG_VideoLength", 0L),
                    SemiOrderProfileId = GetFieldValue(sqlReader, "SemiOrderProfileId", 0),
                    IsGumRideShow = GetFieldValue(sqlReader, "IsGumRideShow", false),
                    OnlineQRCode = GetFieldValue(sqlReader, "OnlineQRCode", string.Empty),
                    ///Storybook
                    ThemeID = GetFieldValue(sqlReader, "DG_ThemeId", 0),                  
                    PageNo = GetFieldValue(sqlReader, "DG_PageNo", 0),
                    IsVosDisplay = GetFieldValue(sqlReader, "IsVOSDisplay", false),
                    StoryBookID = GetFieldValue(sqlReader, "DG_StoryBook_pkey", 0L)
                };

                PhotosList.Add(Photos);
            }
            return PhotosList;
        }

        public ModratePhotoInfo GetModeratePhotos(Int64 ModPhotoID)
        {
            DBParameters.Clear();
            AddParameter("@ParamMod_Photo_ID", ModPhotoID);

            IDataReader sqlReader = ExecuteReader(DAOConstant.USP_GET_MODERATEPHOTOS);
            List<ModratePhotoInfo> Moderate_PhotosList = MapModeratePhotos(sqlReader);
            sqlReader.Close();
            return Moderate_PhotosList.FirstOrDefault();
        }
        private List<ModratePhotoInfo> MapModeratePhotos(IDataReader sqlReader)
        {
            List<ModratePhotoInfo> Moderate_PhotosList = new List<ModratePhotoInfo>();
            while (sqlReader.Read())
            {
                ModratePhotoInfo Moderate_Photos = new ModratePhotoInfo()
                {
                    DG_Mod_Photo_pkey = GetFieldValue(sqlReader, "DG_Mod_Photo_pkey", 0),
                    DG_Mod_Photo_ID = GetFieldValue(sqlReader, "DG_Mod_Photo_ID", 0),
                    DG_Mod_Date = GetFieldValue(sqlReader, "DG_Mod_Date", DateTime.Now),
                    DG_Mod_User_ID = GetFieldValue(sqlReader, "DG_Mod_User_ID", 0),
                };
                Moderate_PhotosList.Add(Moderate_Photos);
            }
            return Moderate_PhotosList;
        }
        public List<ModratePhotoInfo> GetModeratePhotos(Int64? ModPhotoID)
        {
            DBParameters.Clear();
            AddParameter("@ParamDG_Mod_Photo_ID", SetNullLongValue(ModPhotoID));
            IDataReader sqlReader = ExecuteReader(DAOConstant.USP_SEL_MODERATEPHOTOS);
            List<ModratePhotoInfo> Moderate_PhotosList = MapModeratePhotos(sqlReader);
            sqlReader.Close();
            return Moderate_PhotosList;
        }
        public void DeleteModeratePhotosById(Int64 Mod_Photo_ID)
        {
            DBParameters.Clear();
            AddParameter("@ParamMod_Photo_ID", Mod_Photo_ID);
            ExecuteNonQuery(DAOConstant.USP_DEL_MODERATE_PHOTOSBYID);
        }
        public List<PhotoInfo> SelectAllPhotosforSearch(string substoreId, long imgRfid, int noOfImg, bool isAnyRfidSearch
            , long StartIndex, int RfidSearch, int NewRecord, out long maxPhotoId, out long minPhotoId, int mediaType
            )
        {
            maxPhotoId = 0;
            minPhotoId = 0;
            DBParameters.Clear();
            AddParameter("@ParamStartIndex", StartIndex);
            AddParameter("@ParamNoOfImg", noOfImg);
            AddParameter("@ParamIsAnyRfidSearch", isAnyRfidSearch);
            AddParameter("@ParamImgRfid", imgRfid);
            AddParameter("@ParamSubstoreId", substoreId);

            AddParameter("@ParamRfidSearch", RfidSearch);
            AddParameter("@ParamNewRecord", NewRecord);
            AddOutParameter("@ParamLastKeyIndex", SqlDbType.Int);
            AddOutParameter("@ParamFirstKeyIndex", SqlDbType.Int);
            AddParameter("@ParamMediaType", mediaType);

            IDataReader sqlReader = ExecuteReader(DAOConstant.USP_GET_ALLPHOTOSFORSEARCH);
            List<PhotoInfo> PhotosList = MapAllPhotosforSearchInfo(sqlReader);
            sqlReader.Close();
            if (PhotosList.Count > 0)
            {
                minPhotoId = Convert.ToInt64(GetOutParameterValue("@ParamFirstKeyIndex"));
                maxPhotoId = Convert.ToInt64(GetOutParameterValue("@ParamLastKeyIndex"));
            }
            return PhotosList;
        }
        private List<PhotoInfo> MapAllPhotosforSearchInfo(IDataReader sqlReader)
        {
            List<PhotoInfo> PhotosList = new List<PhotoInfo>();
            while (sqlReader.Read())
            {
                PhotoInfo Photos = new PhotoInfo()
                {
                    DG_Photos_pkey = GetFieldValue(sqlReader, "DG_Photos_pkey", 0),
                    DG_Photos_FileName = GetFieldValue(sqlReader, "DG_Photos_FileName", string.Empty),
                    DG_Photos_CreatedOn = GetFieldValue(sqlReader, "DG_Photos_CreatedOn", DateTime.Now),
                    DG_Photos_RFID = GetFieldValue(sqlReader, "DG_Photos_RFID", string.Empty),
                    DG_Photos_UserID = GetFieldValue(sqlReader, "DG_Photos_UserID", 0),
                    DG_Photos_Background = GetFieldValue(sqlReader, "DG_Photos_Background", string.Empty),
                    DG_Photos_Frame = GetFieldValue(sqlReader, "DG_Photos_Frame", string.Empty),
                    DG_Photos_DateTime = GetFieldValue(sqlReader, "DG_Photos_DateTime", DateTime.Now),
                    DG_Photos_Layering = GetFieldValue(sqlReader, "DG_Photos_Layering", string.Empty),
                    DG_Photos_Effects = GetFieldValue(sqlReader, "DG_Photos_Effects", string.Empty),
                    DG_Photos_IsCroped = GetFieldValue(sqlReader, "DG_Photos_IsCroped", false),
                    DG_Photos_IsRedEye = GetFieldValue(sqlReader, "DG_Photos_IsRedEye", false),
                    DG_Photos_IsGreen = GetFieldValue(sqlReader, "DG_Photos_IsGreen", false),
                    DG_Photos_MetaData = GetFieldValue(sqlReader, "DG_Photos_MetaData", string.Empty),
                    DG_Photos_Sizes = GetFieldValue(sqlReader, "DG_Photos_Sizes", string.Empty),
                    DG_Photos_Archive = GetFieldValue(sqlReader, "DG_Photos_Archive", false),
                    DG_Location_Id = GetFieldValue(sqlReader, "DG_Location_Id", 0),
                    DG_SubStoreId = GetFieldValue(sqlReader, "DG_SubStoreId", 0),
                    DG_IsCodeType = GetFieldValue(sqlReader, "DG_IsCodeType", false),
                    DateTaken = GetFieldValue(sqlReader, "DateTaken", DateTime.Now),
                    RfidScanType = GetFieldValue(sqlReader, "RfidScanType", 0),
                    HotFolderPath = GetFieldValue(sqlReader, "HotFolderPath", string.Empty),
                    DG_MediaType = GetFieldValue(sqlReader, "DG_MediaType", 0),
                    DG_VideoLength = GetFieldValue(sqlReader, "DG_VideoLength", 0L),
                    UploadStatus = GetFieldValue(sqlReader, "UploadStatus", 0),///made changes by latika 
                    DisplayOrder = GetFieldValue(sqlReader, "DisplayOrder", 0L),
                    //Add storybook related parameters_by VINS
                    ThemeID = GetFieldValue(sqlReader, "DG_ThemeId", 0),
                    PageNo = GetFieldValue(sqlReader, "DG_PageNo", 0),
                    IsVosDisplay = GetFieldValue(sqlReader, "IsVOSDisplay", false),
                    StoryBookID = GetFieldValue(sqlReader, "DG_StoryBook_Id", 0L)
                };

                PhotosList.Add(Photos);
            }
            return PhotosList;
        }

        public List<PhotoInfo> SelectAllPhotosToBlobUpload()
        {
            ErrorHandler.ErrorHandler.LogFileWrite("SelectAllPhotosToBlobUpload 2");
            DBParameters.Clear();

            IDataReader sqlReader = ExecuteReader(DAOConstant.GETPHOTOSTOUPLOADAZURE);
            List<PhotoInfo> PhotosList = MapAllPhotosToBlobUpload(sqlReader);
            sqlReader.Close();

            return PhotosList;
        }

        public List<PhotoInfo> SelectAllEditedPhotosToBlobUpload()
        {
            ErrorHandler.ErrorHandler.LogFileWrite("SelectAllPhotosToBlobUpload 2");
            DBParameters.Clear();

            IDataReader sqlReader = ExecuteReader(DAOConstant.GETEditedPhotosToUploadAzure);
            List<PhotoInfo> PhotosList = MapAllPhotosToBlobUpload(sqlReader);
            sqlReader.Close();

            return PhotosList;
        }

        public bool GetErrorCount(string Photo_Id, int RetryCount)
        {
            bool flag = false;
            DBParameters.Clear();
            AddParameter("@PhotoId", Photo_Id);
            AddParameter("@RetryCount", RetryCount);
            flag = Convert.ToBoolean(ExecuteScalar("CheckUploadFailureErrorCount"));

            return flag;
        }

        public bool Updatestatus(long SapId, bool result)
        {
            bool flag = false;
            DBParameters.Clear();
            AddParameter("@SapId", SapId);
            AddParameter("@Status", result);
            flag = Convert.ToBoolean(ExecuteScalar("usp_UpdateStatus"));
            return flag;
        }
        public bool GetParticalEditLocationDetails(int LocationId, int SubStoreId)
        {
            bool flag = false;
            DBParameters.Clear();
            AddParameter("@LocationId", LocationId);
            AddParameter("@SubStoreId", SubStoreId);
            flag = Convert.ToBoolean(ExecuteScalar("CheckParticalEditLocationDetails"));

            return flag;
        }

        public string GetSynccodeValues(string sp_Name, int Id)
        {
            string syncCode = string.Empty;
            DBParameters.Clear();
            AddParameter("@Id", Id);
            syncCode = ExecuteScalar(sp_Name).ToString();

            return syncCode;
        }

        public string GetFileWatcherConfigValues(int SubStoreId)
        {
            try
            {
                string result = string.Empty;
                DBParameters.Clear();
                AddParameter("@Id", SubStoreId);
                result = ExecuteScalar("GetFileWatcherConfigValue").ToString();
                return result;
            }
            catch (Exception)
            { return ""; }
        }
        public bool SaveFileWatcherConfig(string IsFileConfig, int Id)
        {
            DBParameters.Clear();
            AddParameter("@Id", Id);
            AddParameter("@IsFileConfig", IsFileConfig);
            ExecuteNonQuery("SaveFileWatcherConfigValue");
            return true;
        }
        public List<AssociateImageDetails> GetAssociateImageDetails(string Photo_Id)
        {
            DBParameters.Clear();
            AddParameter("@PhotoId", Photo_Id);
            IDataReader sqlReader = ExecuteReader(DAOConstant.GETAssciateImagesDetails);
            List<AssociateImageDetails> AssociateImage = MapGetAssociatedImageDetails(sqlReader);
            sqlReader.Close();

            return AssociateImage;
        }

        public List<AssociateImageDetails> GetAssociateImageDetails()
        {
            DBParameters.Clear();

            IDataReader sqlReader = ExecuteReader(DAOConstant.GETiMixImageAssociationDetails);
            List<AssociateImageDetails> AssociateImage = MapGetAssociatedImageDetails(sqlReader);
            sqlReader.Close();

            return AssociateImage;
        }
        private List<PhotoInfo> MapAllPhotosToBlobUpload(IDataReader sqlReader)
        {
            List<PhotoInfo> PhotosList = new List<PhotoInfo>();
            while (sqlReader.Read())
            {
                PhotoInfo Photos = new PhotoInfo()
                {
                    DG_Photos_UserID = GetFieldValue(sqlReader, "DG_Photos_UserID", 0),
                    DG_User_Name = GetFieldValue(sqlReader, "DG_User_Name", ""),
                    DG_Photos_FileName = GetFieldValue(sqlReader, "DG_Photos_FileName", string.Empty),
                    DG_Photos_CreatedOn = GetFieldValue(sqlReader, "DG_Photos_CreatedOn", DateTime.Now),
                    DG_Photos_RFID = GetFieldValue(sqlReader, "DG_Photos_RFID", string.Empty),

                    DG_Photos_Background = GetFieldValue(sqlReader, "DG_Photos_Background", string.Empty),
                    DG_Photos_Frame = GetFieldValue(sqlReader, "DG_Photos_Frame", string.Empty),
                    DG_Photos_DateTime = GetFieldValue(sqlReader, "DG_Photos_DateTime", DateTime.Now),
                    DG_Photos_pkey = GetFieldValue(sqlReader, "DG_Photos_pkey", 0),
                    DG_Photos_IsCroped = GetFieldValue(sqlReader, "DG_Photos_IsCroped", false),
                    DG_Photos_IsRedEye = GetFieldValue(sqlReader, "DG_Photos_IsRedEye", false),
                    DG_Photos_Archive = GetFieldValue(sqlReader, "DG_Photos_Archive", false),
                    DG_Location_Id = GetFieldValue(sqlReader, "DG_Location_Id", 0),
                    DG_SubStoreId = GetFieldValue(sqlReader, "DG_SubStoreId", 0),
                    DG_IsCodeType = GetFieldValue(sqlReader, "DG_IsCodeType", false),
                    DateTaken = GetFieldValue(sqlReader, "DateTaken", DateTime.Now),
                    RfidScanType = GetFieldValue(sqlReader, "RfidScanType", 0),
                    HotFolderPath = GetFieldValue(sqlReader, "HotFolderPath", string.Empty),

                    OnlineQRCode = GetFieldValue(sqlReader, "OnlineQRCode", string.Empty),
                    IsUploadedToBlob = GetFieldValue(sqlReader, "IsUploadedToBlob", false),
                    //DG_Photo_ID = GetFieldValue(sqlReader, "DG_Photo_ID", 0),

                    DG_Photos_IsGreen = GetFieldValue(sqlReader, "DG_Photos_IsGreen", false),
                    //DG_Photos_MetaData = GetFieldValue(sqlReader, "DG_Photos_MetaData", string.Empty),
                    //DG_Photos_Sizes = GetFieldValue(sqlReader, "DG_Photos_Sizes", string.Empty),
                    SemiOrderProfileId = GetFieldValue(sqlReader, "SemiOrderProfileId", 0)
                    //DG_MediaType = GetFieldValue(sqlReader, "DG_MediaType", 0),
                    //DG_VideoLength = GetFieldValue(sqlReader, "DG_VideoLength", 0L),
                    //UploadStatus = GetFieldValue(sqlReader, "UploadStatus", 0),///made changes by latika 
                    // UploadStatus = 0,///made changes by latika 
                };

                PhotosList.Add(Photos);
            }
            return PhotosList;
        }

        private List<AssociateImageDetails> MapGetAssociatedImageDetails(IDataReader sqlReader)
        {
            List<AssociateImageDetails> AssImages = new List<AssociateImageDetails>();
            while (sqlReader.Read())
            {
                AssociateImageDetails AssociateImg = new AssociateImageDetails()
                {
                    IMIXImageAssociationId = GetFieldValue(sqlReader, "IMIXImageAssociationId", 0L),
                    IMIXCardTypeId = GetFieldValue(sqlReader, "IMIXCardTypeId", 0),
                    PhotoId = GetFieldValue(sqlReader, "PhotoId", 0),
                    CardUniqueIdentifier = GetFieldValue(sqlReader, "CardUniqueIdentifier", string.Empty),
                    MappedIdentifier = GetFieldValue(sqlReader, "MappedIdentifier", string.Empty),

                    ModifiedDate = GetFieldValue(sqlReader, "ModifiedDate", DateTime.Now),
                    IsOrdered = GetFieldValue(sqlReader, "IsOrdered", 0),
                    RfidIdentifierId = GetFieldValue(sqlReader, "RfidIdentifierId", 0),
                    IsMoved = GetFieldValue(sqlReader, "IsMoved", 0),
                    Nationality = GetFieldValue(sqlReader, "Nationality", 0),
                    Email = GetFieldValue(sqlReader, "Email", string.Empty)
                };

                AssImages.Add(AssociateImg);
            }
            return AssImages;
        }
        public PhotoInfo GetNextPreviousPhoto(int Photospkey, string Flag)
        {
            DBParameters.Clear();
            AddParameter("@ParamPhotos_pkey", Photospkey);
            AddParameter("@ParamFlag", Flag);

            IDataReader sqlReader = ExecuteReader(DAOConstant.USP_GET_NEXTPREVIOUSPHOTO);
            List<PhotoInfo> PhotosList = MapNextPreviousPhotoInfo(sqlReader);
            sqlReader.Close();
            return PhotosList.FirstOrDefault();
        }
        private List<PhotoInfo> MapNextPreviousPhotoInfo(IDataReader sqlReader)
        {
            List<PhotoInfo> PhotosList = new List<PhotoInfo>();
            while (sqlReader.Read())
            {
                PhotoInfo Photos = new PhotoInfo()
                {
                    DG_Photos_pkey = GetFieldValue(sqlReader, "DG_Photos_pkey", 0),
                    DG_Photos_FileName = GetFieldValue(sqlReader, "DG_Photos_FileName", string.Empty),
                    DG_Photos_CreatedOn = GetFieldValue(sqlReader, "DG_Photos_CreatedOn", DateTime.Now),
                    DG_Photos_RFID = GetFieldValue(sqlReader, "DG_Photos_RFID", string.Empty),
                    DG_Photos_UserID = GetFieldValue(sqlReader, "DG_Photos_UserID", 0),
                    DG_Photos_Background = GetFieldValue(sqlReader, "DG_Photos_Background", string.Empty),
                    DG_Photos_Frame = GetFieldValue(sqlReader, "DG_Photos_Frame", string.Empty),
                    DG_Photos_DateTime = GetFieldValue(sqlReader, "DG_Photos_DateTime", DateTime.Now),
                    DG_Photos_Layering = GetFieldValue(sqlReader, "DG_Photos_Layering", string.Empty),
                    DG_Photos_Effects = GetFieldValue(sqlReader, "DG_Photos_Effects", string.Empty),
                    DG_Photos_IsCroped = GetFieldValue(sqlReader, "DG_Photos_IsCroped", false),
                    DG_Photos_IsRedEye = GetFieldValue(sqlReader, "DG_Photos_IsRedEye", false),
                    DG_Photos_IsGreen = GetFieldValue(sqlReader, "DG_Photos_IsGreen", false),
                    DG_Photos_MetaData = GetFieldValue(sqlReader, "DG_Photos_MetaData", string.Empty),
                    DG_Photos_Sizes = GetFieldValue(sqlReader, "DG_Photos_Sizes", string.Empty),
                    DG_Photos_Archive = GetFieldValue(sqlReader, "DG_Photos_Archive", false),
                    DG_Location_Id = GetFieldValue(sqlReader, "DG_Location_Id", 0),
                    DG_SubStoreId = GetFieldValue(sqlReader, "DG_SubStoreId", 0),
                    DG_IsCodeType = GetFieldValue(sqlReader, "DG_IsCodeType", false),
                    DateTaken = GetFieldValue(sqlReader, "DateTaken", DateTime.Now),
                    RfidScanType = GetFieldValue(sqlReader, "RfidScanType", 0),

                };

                PhotosList.Add(Photos);
            }
            return PhotosList;
        }
        public bool UpdGreenPhotos(Int64 Photospkey, bool Value)
        {
            DBParameters.Clear();
            AddParameter("@ParamPhotos_pkey", Photospkey);
            AddParameter("@ParamValue", Value);
            ExecuteNonQuery(DAOConstant.USP_UPD_GREENPHOTOS);
            return true;
        }
        public void InsertModerateImage(int PhotoId, int Userid)
        {
            DBParameters.Clear();
            AddParameter("@ParamPhotoId", PhotoId);
            AddParameter("@ParamUserid", Userid);
            ExecuteNonQuery(DAOConstant.USP_INS_MODERATEIMAGE);
        }
        public bool UpdatePhotoEffects(PhotoInfo objectInfo)
        {
            DBParameters.Clear();
            AddParameter("@ParamPhotoId", objectInfo.DG_Photos_pkey);
            AddParameter("@ParamDG_Photos_IsCroped", objectInfo.DG_Photos_IsCroped);
            AddParameter("@ParamDG_Photos_Effects", objectInfo.DG_Photos_Effects);
            ExecuteNonQuery(DAOConstant.USP_UPD_PHOTOEFFECTS);
            return true;
        }
        public void InsertPreviewCounter(int PhotoId)
        {
            DBParameters.Clear();
            AddParameter("@ParamPhotoId", PhotoId);
            ExecuteNonQuery(DAOConstant.USP_INS_PREVIEWCOUNTER);
        }
        public bool UpdatePhotoLayering(PhotoInfo objectInfo)
        {
            DBParameters.Clear();
            AddParameter("@ParamPhotosLayering", objectInfo.DG_Photos_Layering);
            AddParameter("@ParamPhotoId", objectInfo.DG_Photos_pkey);
            AddParameter("@ParamIsImageProcessed", objectInfo.IsImageProcessed);

            ExecuteNonQuery(DAOConstant.USP_UPD_PHOTOLAYERING);
            return true;
        }

        public Int32 UpdatePhotoLayeringStoryBook(PhotoInfo objectInfo)
        {
            DBParameters.Clear();
            AddParameter("@ParamPhotosLayering", objectInfo.DG_Photos_Layering);
            AddParameter("@ParamPhotoId", objectInfo.DG_Photos_pkey);
            AddParameter("@ParamIsImageProcessed", objectInfo.IsImageProcessed);
            AddParameter("@ThemeID", objectInfo.ThemeID);
            AddParameter("@StoryBookID", objectInfo.StoryBookID);
            AddParameter("@PageNo", objectInfo.PageNo);
            AddParameter("@IsVosDisplay", objectInfo.IsVosDisplay);

            //ExecuteNonQuery(DAOConstant.USP_UPD_PHOTOLAYERINGSTORYBOOK);
            Int32 SBID = Convert.ToInt32(ExecuteScalar(DAOConstant.USP_UPD_PHOTOLAYERINGSTORYBOOK));
            return SBID;
        }

        public Int32 RestoringStoryBook(PhotoInfo objectInfo)
        {
            DBParameters.Clear();            
            AddParameter("@ParamPhotoId", objectInfo.DG_Photos_pkey);
            
            Int32 SBID = Convert.ToInt32(ExecuteScalar(DAOConstant.usp_UPD_RestoreStorybook));
            return SBID;
        }

        public bool UpdatePhotoEffectsByPhotosId(PhotoInfo objectInfo)
        {
            DBParameters.Clear();
            AddParameter("@ParamPhotoId", objectInfo.DG_Photos_pkey);
            AddParameter("@ParamDG_Photos_Effects", objectInfo.DG_Photos_Effects);
            ExecuteNonQuery(DAOConstant.USP_UPD_PHOTOEFFECTSBYPHOTOSID);
            return true;
        }
        public bool UpdateEffectsonPhoto(string value, int photoId, bool isgumballshow)
        {
            DBParameters.Clear();
            AddParameter("@ParamPhotoId", photoId);
            AddParameter("@ParamPhotosEffects", value);
            AddParameter("@ParamIsGumRideShow", isgumballshow);
            ExecuteNonQuery(DAOConstant.USP_UPD_EFFECTSONPHOTO);
            return true;
        }

        #region Added by Ajay 11 April 2018 to save Product Background
        public bool PhotoProductLastEdit_PlaceOrder(int PhotoId, int PanoramaSizeSelected)
        {
            DBParameters.Clear();
            AddParameter("@ParamPhotoId", PhotoId);
            AddParameter("@PanoramaSizeSelected", PanoramaSizeSelected);
            ExecuteNonQuery(DAOConstant.USP_ADUPD_Photo_ProductID);
            return true;
        }
        #endregion

        public bool UpdateEffectsSpecPrint(int photoId, string layeringdata, string ImageEffect, bool isGreenSreen, bool isCrop, bool isgumballshow, string GumBallRidetxtContent, string _QRCode)
        {
            DBParameters.Clear();
            AddParameter("@ParamPhotoId", photoId);
            AddParameter("@ParamLayeringData", layeringdata);
            AddParameter("@ParamImageEffect", ImageEffect);
            AddParameter("@ParamIsGreenSreen", isGreenSreen);
            AddParameter("@ParamIsCropped", isCrop);
            AddParameter("@ParamIsGumRideShow", isgumballshow);
            AddParameter("@ParamGumBallRidetxtContent", GumBallRidetxtContent);
            AddParameter("@ParamOnlineQRCode", _QRCode);
            ExecuteNonQuery(DAOConstant.USP_UPD_UPDATEEFFECTSSPECPRINT);
            return true;
        }
        public bool UpdateOnlineQRCodeForPhoto(int photoId, string OnlineQRCode)
        {
            DBParameters.Clear();
            AddParameter("@ParamPhotoId", photoId);
            AddParameter("@ParamOnlineQRCode", OnlineQRCode);
            ExecuteNonQuery(DAOConstant.USP_UPD_UpdateOnlineQRCodeForPhoto);
            return true;
        }
        public List<PhotoInfo> GetPhotoDetailsbyPhotoId(int Photos_pkey)
        {
            DBParameters.Clear();
            AddParameter("@ParamPhotos_pkey", Photos_pkey);

            IDataReader sqlReader = ExecuteReader(DAOConstant.USP_GET_PHOTODETAILSBYPHOTOID);
            List<PhotoInfo> PhotosList = MapPhotoInfo(sqlReader);
            sqlReader.Close();
            return PhotosList;
        }
        private List<PhotoInfo> MapPhotoInfo(IDataReader sqlReader)
        {
            List<PhotoInfo> PhotosList = new List<PhotoInfo>();
            while (sqlReader.Read())
            {
                PhotoInfo Photos = new PhotoInfo()
                {
                    DG_Photos_pkey = GetFieldValue(sqlReader, "DG_Photos_pkey", 0),
                    DG_Photos_FileName = GetFieldValue(sqlReader, "DG_Photos_FileName", string.Empty),
                    DG_Photos_CreatedOn = GetFieldValue(sqlReader, "DG_Photos_CreatedOn", DateTime.Now),
                    DG_Photos_RFID = GetFieldValue(sqlReader, "DG_Photos_RFID", string.Empty),
                    DG_Photos_UserID = GetFieldValue(sqlReader, "DG_Photos_UserID", 0),
                    DG_Photos_Background = GetFieldValue(sqlReader, "DG_Photos_Background", string.Empty),
                    DG_Photos_Frame = GetFieldValue(sqlReader, "DG_Photos_Frame", string.Empty),
                    DG_Photos_DateTime = GetFieldValue(sqlReader, "DG_Photos_DateTime", DateTime.Now),
                    DG_Photos_Layering = GetFieldValue(sqlReader, "DG_Photos_Layering", string.Empty),
                    DG_Photos_Effects = GetFieldValue(sqlReader, "DG_Photos_Effects", string.Empty),
                    DG_Photos_IsCroped = GetFieldValue(sqlReader, "DG_Photos_IsCroped", false),
                    DG_Photos_IsRedEye = GetFieldValue(sqlReader, "DG_Photos_IsRedEye", false),
                    DG_Photos_IsGreen = GetFieldValue(sqlReader, "DG_Photos_IsGreen", false),
                    DG_Photos_MetaData = GetFieldValue(sqlReader, "DG_Photos_MetaData", string.Empty),
                    DG_Photos_Sizes = GetFieldValue(sqlReader, "DG_Photos_Sizes", string.Empty),
                    DG_Photos_Archive = GetFieldValue(sqlReader, "DG_Photos_Archive", false),
                    DG_Location_Id = GetFieldValue(sqlReader, "DG_Location_Id", 0),
                    DG_SubStoreId = GetFieldValue(sqlReader, "DG_SubStoreId", 0),
                    DG_IsCodeType = GetFieldValue(sqlReader, "DG_IsCodeType", false),
                    DateTaken = GetFieldValue(sqlReader, "DateTaken", DateTime.Now),
                    RfidScanType = GetFieldValue(sqlReader, "RfidScanType", 0),

                };

                PhotosList.Add(Photos);
            }
            return PhotosList;
        }
        public List<PhotoInfo> SelectMapImagesBYQRCode(bool IsAnonymousQrCodeEnabled, string QRCode)
        {
            DBParameters.Clear();
            AddParameter("@IsAnonymousQrCodeEnabled", IsAnonymousQrCodeEnabled);
            AddParameter("@QRCode", QRCode);

            IDataReader sqlReader = ExecuteReader(DAOConstant.GETIMAGESBYQRCODE);
            List<PhotoInfo> PhotosList = MapImagesBYQRCode(sqlReader);
            sqlReader.Close();
            return PhotosList;
        }
        private List<PhotoInfo> MapImagesBYQRCode(IDataReader sqlReader)
        {
            List<PhotoInfo> PhotosList = new List<PhotoInfo>();
            while (sqlReader.Read())
            {
                PhotoInfo Photos = new PhotoInfo()
                {
                    DG_Photos_pkey = GetFieldValue(sqlReader, "DG_Photos_pkey", 0),
                    DG_Photos_FileName = GetFieldValue(sqlReader, "DG_Photos_FileName", string.Empty),
                    DG_Photos_CreatedOn = GetFieldValue(sqlReader, "DG_Photos_CreatedOn", DateTime.Now),
                    DG_Photos_RFID = GetFieldValue(sqlReader, "DG_Photos_RFID", string.Empty),
                    DG_Photos_UserID = GetFieldValue(sqlReader, "DG_Photos_UserID", 0),
                    DG_Photos_Background = GetFieldValue(sqlReader, "DG_Photos_Background", string.Empty),
                    DG_Photos_Frame = GetFieldValue(sqlReader, "DG_Photos_Frame", string.Empty),
                    DG_Photos_DateTime = GetFieldValue(sqlReader, "DG_Photos_DateTime", DateTime.Now),
                    DG_Photos_Layering = GetFieldValue(sqlReader, "DG_Photos_Layering", string.Empty),
                    DG_Photos_Effects = GetFieldValue(sqlReader, "DG_Photos_Effects", string.Empty),
                    DG_Photos_IsCroped = GetFieldValue(sqlReader, "DG_Photos_IsCroped", false),
                    DG_Photos_IsRedEye = GetFieldValue(sqlReader, "DG_Photos_IsRedEye", false),
                    DG_Photos_IsGreen = GetFieldValue(sqlReader, "DG_Photos_IsGreen", false),
                    DG_Photos_MetaData = GetFieldValue(sqlReader, "DG_Photos_MetaData", string.Empty),
                    DG_Photos_Sizes = GetFieldValue(sqlReader, "DG_Photos_Sizes", string.Empty),
                    DG_Photos_Archive = GetFieldValue(sqlReader, "DG_Photos_Archive", false),
                    DG_Location_Id = GetFieldValue(sqlReader, "DG_Location_Id", 0),
                    DG_SubStoreId = GetFieldValue(sqlReader, "DG_SubStoreId", 0),
                    IMIXImageAssociationId = GetFieldValue(sqlReader, "IMIXImageAssociationId", 0L),
                    HotFolderPath = GetFieldValue(sqlReader, "HotFolderPath", string.Empty),
                    DG_MediaType = GetFieldValue(sqlReader, "DG_MediaType", 0),
                    DG_VideoLength = GetFieldValue(sqlReader, "DG_VideoLength", 0L),
                    OnlineQRCode = GetFieldValue(sqlReader, "OnlineQRCode", string.Empty),
                    UploadStatus = GetFieldValue(sqlReader, "UploadStatus", 0),///made changes by latika 
                };

                PhotosList.Add(Photos);
            }
            return PhotosList;
        }
        public List<PhotoInfo> SelectSavedGroupImages(string GroupName)
        {
            DBParameters.Clear();
            AddParameter("@ParamGroupName", GroupName);
            IDataReader sqlReader = ExecuteReader(DAOConstant.USP_SEL_SAVEDGROUPIMAGES);
            List<PhotoInfo> GroupedImagesList = MapGetGroupedImages(sqlReader);
            sqlReader.Close();
            return GroupedImagesList;
        }
        private List<PhotoInfo> MapGetGroupedImages(IDataReader sqlReader)
        {
            List<PhotoInfo> GroupedImagesList = new List<PhotoInfo>();
            while (sqlReader.Read())
            {
                PhotoInfo GroupedImages = new PhotoInfo()
                {
                    DG_Group_Name = GetFieldValue(sqlReader, "DG_Group_Name", string.Empty),
                    DG_Group_pkey = GetFieldValue(sqlReader, "DG_Group_pkey", 0L),
                    DG_Photos_pkey = GetFieldValue(sqlReader, "DG_Photos_pkey", 0),
                    DG_Photos_FileName = GetFieldValue(sqlReader, "DG_Photos_FileName", string.Empty),
                    DG_Photos_CreatedOn = GetFieldValue(sqlReader, "DG_Photos_CreatedOn", DateTime.Now),
                    DG_Photos_RFID = GetFieldValue(sqlReader, "DG_Photos_RFID", string.Empty),
                    DG_Photos_UserID = GetFieldValue(sqlReader, "DG_Photos_UserID", 0),
                    DG_Photos_Background = GetFieldValue(sqlReader, "DG_Photos_Background", string.Empty),
                    DG_Photos_Frame = GetFieldValue(sqlReader, "DG_Photos_Frame", string.Empty),
                    DG_Photos_DateTime = GetFieldValue(sqlReader, "DG_Photos_DateTime", DateTime.Now),
                    DG_Photos_Layering = GetFieldValue(sqlReader, "DG_Photos_Layering", string.Empty),
                    DG_Photos_Effects = GetFieldValue(sqlReader, "DG_Photos_Effects", string.Empty),
                    DG_Photos_IsCroped = GetFieldValue(sqlReader, "DG_Photos_IsCroped", false),
                    DG_Photos_IsRedEye = GetFieldValue(sqlReader, "DG_Photos_IsRedEye", false),
                    DG_Photos_IsGreen = GetFieldValue(sqlReader, "DG_Photos_IsGreen", false),
                    DG_Photos_MetaData = GetFieldValue(sqlReader, "DG_Photos_MetaData", string.Empty),
                    DG_Photos_Sizes = GetFieldValue(sqlReader, "DG_Photos_Sizes", string.Empty),
                    DG_Photos_Archive = GetFieldValue(sqlReader, "DG_Photos_Archive", false),
                    DG_Location_Id = GetFieldValue(sqlReader, "DG_Location_Id", 0),
                    DG_SubStoreId = GetFieldValue(sqlReader, "DG_SubStoreId", 0),
                    HotFolderPath = GetFieldValue(sqlReader, "HotFolderPath", string.Empty),
                    DG_MediaType = GetFieldValue(sqlReader, "DG_MediaType", 0),
                    DG_VideoLength = GetFieldValue(sqlReader, "DG_VideoLength", 0L),
                    UploadStatus = GetFieldValue(sqlReader, "UploadStatus", 0),///made changes by latika 
                };
                GroupedImagesList.Add(GroupedImages);
            }
            return GroupedImagesList;
        }
        public bool IsCodeType(int Photos_pkey)
        {
            DBParameters.Clear();
            AddParameter("@ParamPhotos_pkey", Photos_pkey);

            IDataReader sqlReader = ExecuteReader(DAOConstant.USP_GET_PHOTORFIDBYPHOTOID);
            List<PhotoInfo> PhotosList = MapIsCodeType(sqlReader);
            sqlReader.Close();
            return PhotosList.FirstOrDefault().DG_IsCodeType;
        }
        private List<PhotoInfo> MapIsCodeType(IDataReader sqlReader)
        {
            List<PhotoInfo> PhotosList = new List<PhotoInfo>();
            while (sqlReader.Read())
            {
                PhotoInfo Photos = new PhotoInfo()
                {
                    DG_IsCodeType = GetFieldValue(sqlReader, "DG_IsCodeType", false)
                };

                PhotosList.Add(Photos);
            }
            return PhotosList;
        }
        public bool DeletePhotoByPhotoId(int PhotoId)
        {
            DBParameters.Clear();
            AddParameter("@ParamPhotoId", PhotoId);
            ExecuteNonQuery(DAOConstant.USP_UPD_PHOTOBYPHOTOID);
            return true;
        }
        public List<PhotoInfo> GetAllPhotosByPage(int? substoreId, long startIndex, int pazeSize, bool isNext, out long maxPhotoId, out bool IsMoreImages, out long minPhotoId, int mediaType)
        {
            maxPhotoId = 0;
            minPhotoId = 0;
            IsMoreImages = false;
            DBParameters.Clear();
            AddParameter("@ParamStartIndex", startIndex);
            AddParameter("@ParamPageSize", pazeSize);
            AddParameter("@ParamSubStoreId", SetNullIntegerValue(substoreId));
            AddParameter("@ParamNewRecord", isNext);
            //AddParameter("@ParamLastKeyIndex",0, ParameterDirection.Output);
            //AddParameter("@ParamIsMoreImage",0, ParameterDirection.Output);
            AddOutParameter("@ParamLastKeyIndex", SqlDbType.Int);
            AddOutParameter("@ParamFirstKeyIndex", SqlDbType.Int);
            AddOutParameter("@ParamIsMoreImage", SqlDbType.Bit);
            AddParameter("@MediaType", mediaType);

            IDataReader sqlReader = ExecuteReader(DAOConstant.USP_SEARCH_PHOTODATABYPAGE);
            List<PhotoInfo> PhotosList = MapAllPhotosByPage(sqlReader);
            sqlReader.Close();
            if (PhotosList.Count > 0)
            {
                minPhotoId = Convert.ToInt64(GetOutParameterValue("@ParamFirstKeyIndex"));
                maxPhotoId = Convert.ToInt64(GetOutParameterValue("@ParamLastKeyIndex"));
                IsMoreImages = Convert.ToBoolean(GetOutParameterValue("@ParamIsMoreImage"));
            }
            return PhotosList;
        }

        public List<PhotoInfo> GetAllPhotosByPageStoryBook(int? substoreId, long startIndex, int pazeSize, bool isNext, out long maxPhotoId, out bool IsMoreImages, out long minPhotoId, int mediaType)
        {
            maxPhotoId = 0;
            minPhotoId = 0;
            IsMoreImages = false;
            DBParameters.Clear();
            AddParameter("@ParamStartIndex", startIndex);
            AddParameter("@ParamPageSize", pazeSize);
            AddParameter("@ParamSubStoreId", SetNullIntegerValue(substoreId));
            AddParameter("@ParamNewRecord", isNext);
            //AddParameter("@ParamLastKeyIndex",0, ParameterDirection.Output);
            //AddParameter("@ParamIsMoreImage",0, ParameterDirection.Output);
            AddOutParameter("@ParamLastKeyIndex", SqlDbType.Int);
            AddOutParameter("@ParamFirstKeyIndex", SqlDbType.Int);
            AddOutParameter("@ParamIsMoreImage", SqlDbType.Bit);
            AddParameter("@MediaType", mediaType);

            IDataReader sqlReader = ExecuteReader(DAOConstant.USP_SEARCH_PHOTODATABYPAGESTORYBOOK);
            List<PhotoInfo> PhotosList = MapAllPhotosByPageStoryBook(sqlReader);
            sqlReader.Close();
            if (PhotosList.Count > 0)
            {
                minPhotoId = Convert.ToInt64(GetOutParameterValue("@ParamFirstKeyIndex"));
                maxPhotoId = Convert.ToInt64(GetOutParameterValue("@ParamLastKeyIndex"));
                IsMoreImages = Convert.ToBoolean(GetOutParameterValue("@ParamIsMoreImage"));
            }
            return PhotosList;
        }

        /// <summary>
        /// Created new method to take Substore 
        /// </summary>
        /// <param name="substoreId"></param>
        /// <param name="startIndex"></param>
        /// <param name="pazeSize"></param>
        /// <param name="isNext"></param>
        /// <param name="maxPhotoId"></param>
        /// <param name="IsMoreImages"></param>
        /// <param name="minPhotoId"></param>
        /// <param name="mediaType"></param>
        /// <returns></returns>
        public List<PhotoInfo> GetAllPhotosByPageForSubStore(string substoreId, long startIndex, int pazeSize, bool isNext, out long maxPhotoId, out bool IsMoreImages, out long minPhotoId, int mediaType)
        {
            List<PhotoInfo> PhotosList = new List<PhotoInfo>();
            maxPhotoId = 0;
            minPhotoId = 0;
            IsMoreImages = false;
            IDataReader sqlReader = null;
            if (substoreId != "")
            {
                //substoreId = string.Join(",", substoreId.Split(',').Distinct().ToArray());
                //foreach (var item in substoreId.Split(','))
                //{

                DBParameters.Clear();
                AddParameter("@ParamStartIndex", startIndex);
                AddParameter("@ParamPageSize", pazeSize);
                //AddParameter("@ParamSubStoreId", SetNullIntegerValue(Convert.ToInt32(substoreId)));
                AddParameter("@ParamSubStoreId", substoreId);
                AddParameter("@ParamNewRecord", isNext);
                //AddParameter("@ParamLastKeyIndex",0, ParameterDirection.Output);
                //AddParameter("@ParamIsMoreImage",0, ParameterDirection.Output);
                AddOutParameter("@ParamLastKeyIndex", SqlDbType.Int);
                AddOutParameter("@ParamFirstKeyIndex", SqlDbType.Int);
                AddOutParameter("@ParamIsMoreImage", SqlDbType.Bit);
                AddParameter("@MediaType", mediaType);

                sqlReader = ExecuteReader(DAOConstant.USP_SEARCH_PHOTODATABYPAGE_SUBSTORE);
                //List<PhotoInfo> PhotosList = MapAllPhotosByPage(sqlReader);
                //PhotosList = MapAllPhotosByPage(sqlReader);
                foreach (PhotoInfo item1 in MapAllPhotosByPage(sqlReader))
                {
                    PhotosList.Add(item1);
                }

                if (PhotosList.Count > 0)
                {
                    minPhotoId = Convert.ToInt64(GetOutParameterValue("@ParamFirstKeyIndex"));
                    maxPhotoId = Convert.ToInt64(GetOutParameterValue("@ParamLastKeyIndex"));
                    IsMoreImages = Convert.ToBoolean(GetOutParameterValue("@ParamIsMoreImage"));
                }
                //}
            }
            sqlReader.Close();
            return PhotosList;
        }


        //public List<PhotoInfo> GetAllPhotosByPage(int? substoreId, int startIndex, int pazeSize, bool isNext, out long maxPhotoId, out bool IsMoreImages)
        //{
        //    DBParameters.Clear();
        //    AddParameter("@StartIndex", startIndex);
        //    AddParameter("@PageSize", pazeSize);
        //    AddParameter("@SubStoreId", SetNullIntegerValue(substoreId));
        //    AddParameter("@NewRecord", isNext);
        //    AddOutParameter("@LastKeyIndex", SqlDbType.Int);
        //    AddOutParameter("@IsMoreImage", SqlDbType.Bit);

        //    IDataReader sqlReader = ExecuteReader("[dbo].[GetPhotoDataByPage]");
        //    maxPhotoId = (int)GetOutParameterValue("@LastKeyIndex");
        //    IsMoreImages = (bool)GetOutParameterValue("@IsMoreImage");
        //    int i = (int)GetOutParameterValue("@IsMoreImage");
        //    IsMoreImages = true;
        //    //IsMoreImages = (bool)GetOutParameterValue("@ParamIsMoreImage");
        //    List<PhotoInfo> PhotosList = MapAllPhotosByPage(sqlReader);
        //    sqlReader.Close();
        //    return PhotosList;
        //}
        private List<PhotoInfo> MapAllPhotosByPage(IDataReader sqlReader)
        {
            List<PhotoInfo> PhotosList = new List<PhotoInfo>();
            while (sqlReader.Read())
            {
                PhotoInfo Photos = new PhotoInfo()
                {
                    DG_Photos_pkey = GetFieldValue(sqlReader, "DG_Photos_pkey", 0),
                    DG_Photos_FileName = GetFieldValue(sqlReader, "DG_Photos_FileName", string.Empty),
                    DG_Photos_CreatedOn = GetFieldValue(sqlReader, "DG_Photos_CreatedOn", DateTime.Now),
                    DG_Photos_RFID = GetFieldValue(sqlReader, "DG_Photos_RFID", string.Empty),
                    DG_Photos_UserID = GetFieldValue(sqlReader, "DG_Photos_UserID", 0),
                    DG_Photos_Background = GetFieldValue(sqlReader, "DG_Photos_Background", string.Empty),
                    DG_Photos_Frame = GetFieldValue(sqlReader, "DG_Photos_Frame", string.Empty),
                    DG_Photos_DateTime = GetFieldValue(sqlReader, "DG_Photos_DateTime", DateTime.Now),
                    DG_Photos_Layering = GetFieldValue(sqlReader, "DG_Photos_Layering", string.Empty),
                    DG_Photos_Effects = GetFieldValue(sqlReader, "DG_Photos_Effects", string.Empty),
                    DG_Photos_IsCroped = GetFieldValue(sqlReader, "DG_Photos_IsCroped", false),
                    DG_Photos_IsRedEye = GetFieldValue(sqlReader, "DG_Photos_IsRedEye", false),
                    DG_Photos_IsGreen = GetFieldValue(sqlReader, "DG_Photos_IsGreen", false),
                    DG_Photos_MetaData = GetFieldValue(sqlReader, "DG_Photos_MetaData", string.Empty),
                    DG_Photos_Sizes = GetFieldValue(sqlReader, "DG_Photos_Sizes", string.Empty),
                    DG_Photos_Archive = GetFieldValue(sqlReader, "DG_Photos_Archive", false),
                    DG_Location_Id = GetFieldValue(sqlReader, "DG_Location_Id", 0),
                    DG_SubStoreId = GetFieldValue(sqlReader, "DG_SubStoreId", 0),
                    DG_IsCodeType = GetFieldValue(sqlReader, "DG_IsCodeType", false),
                    DateTaken = GetFieldValue(sqlReader, "DateTaken", DateTime.Now),
                    RfidScanType = GetFieldValue(sqlReader, "RfidScanType", 0),
                    HotFolderPath = GetFieldValue(sqlReader, "HotFolderPath", string.Empty),
                    DG_MediaType = GetFieldValue(sqlReader, "DG_MediaType", 0),
                    DG_VideoLength = GetFieldValue(sqlReader, "DG_VideoLength", 0L),
                    OnlineQRCode = GetFieldValue(sqlReader, "OnlineQRCode", string.Empty),
                    UploadStatus = GetFieldValue(sqlReader, "UploadStatus", 0),////changed by latika for showing images in view order station synced and not synced for Presold 26 apr 2019
                    DisplayOrder = GetFieldValue(sqlReader, "DisplayOrder", 0L),
                };

                PhotosList.Add(Photos);
            }
            return PhotosList;
        }


        private List<PhotoInfo> MapAllPhotosByPageStoryBook(IDataReader sqlReader)
        {
            List<PhotoInfo> PhotosList = new List<PhotoInfo>();
            while (sqlReader.Read())
            {
                PhotoInfo Photos = new PhotoInfo()
                {
                    DG_Photos_pkey = GetFieldValue(sqlReader, "DG_Photos_pkey", 0),
                    DG_Photos_FileName = GetFieldValue(sqlReader, "DG_Photos_FileName", string.Empty),
                    DG_Photos_CreatedOn = GetFieldValue(sqlReader, "DG_Photos_CreatedOn", DateTime.Now),
                    DG_Photos_RFID = GetFieldValue(sqlReader, "DG_Photos_RFID", string.Empty),
                    DG_Photos_UserID = GetFieldValue(sqlReader, "DG_Photos_UserID", 0),
                    DG_Photos_Background = GetFieldValue(sqlReader, "DG_Photos_Background", string.Empty),
                    DG_Photos_Frame = GetFieldValue(sqlReader, "DG_Photos_Frame", string.Empty),
                    DG_Photos_DateTime = GetFieldValue(sqlReader, "DG_Photos_DateTime", DateTime.Now),
                    DG_Photos_Layering = GetFieldValue(sqlReader, "DG_Photos_Layering", string.Empty),
                    DG_Photos_Effects = GetFieldValue(sqlReader, "DG_Photos_Effects", string.Empty),
                    DG_Photos_IsCroped = GetFieldValue(sqlReader, "DG_Photos_IsCroped", false),
                    DG_Photos_IsRedEye = GetFieldValue(sqlReader, "DG_Photos_IsRedEye", false),
                    DG_Photos_IsGreen = GetFieldValue(sqlReader, "DG_Photos_IsGreen", false),
                    DG_Photos_MetaData = GetFieldValue(sqlReader, "DG_Photos_MetaData", string.Empty),
                    DG_Photos_Sizes = GetFieldValue(sqlReader, "DG_Photos_Sizes", string.Empty),
                    DG_Photos_Archive = GetFieldValue(sqlReader, "DG_Photos_Archive", false),
                    DG_Location_Id = GetFieldValue(sqlReader, "DG_Location_Id", 0),
                    DG_SubStoreId = GetFieldValue(sqlReader, "DG_SubStoreId", 0),
                    DG_IsCodeType = GetFieldValue(sqlReader, "DG_IsCodeType", false),
                    DateTaken = GetFieldValue(sqlReader, "DateTaken", DateTime.Now),
                    RfidScanType = GetFieldValue(sqlReader, "RfidScanType", 0),
                    HotFolderPath = GetFieldValue(sqlReader, "HotFolderPath", string.Empty),
                    DG_MediaType = GetFieldValue(sqlReader, "DG_MediaType", 0),
                    DG_VideoLength = GetFieldValue(sqlReader, "DG_VideoLength", 0L),
                    OnlineQRCode = GetFieldValue(sqlReader, "OnlineQRCode", string.Empty),
                    UploadStatus = GetFieldValue(sqlReader, "UploadStatus", 0),////changed by latika for showing images in view order station synced and not synced for Presold 26 apr 2019
                    DisplayOrder = GetFieldValue(sqlReader, "DisplayOrder", 0L),
                    StoryBookID = GetFieldValue(sqlReader, "DG_StoryBook_Id", 0L),
                    IsVosDisplay = true,
                };

                PhotosList.Add(Photos);
            }
            return PhotosList;
        }

        public List<PhotoInfo> GetPhotoList(DateTime? FromTime, DateTime? ToTime, Int32? UserId, Int32? LocationId, string SubStoreIDs)
        {
            DBParameters.Clear();
            AddParameter("@ParamUserId", SetNullIntegerValue(UserId));
            AddParameter("@ParamLocationId", SetNullIntegerValue(LocationId));
            AddParameter("@ParamFromTime", SetNullDateTimeValue(FromTime));
            AddParameter("@ParamToTime", SetNullDateTimeValue(ToTime));
            AddParameter("@ParamSubStoreIDs", SetNullStringValue(SubStoreIDs));

            IDataReader sqlReader = ExecuteReader(DAOConstant.USP_SEL_PHOTOLIST);
            List<PhotoInfo> PhotoListList = MapPhotoListBase(sqlReader);
            sqlReader.Close();
            return PhotoListList;
        }
        private List<PhotoInfo> MapPhotoListBase(IDataReader sqlReader)
        {
            List<PhotoInfo> PhotoListList = new List<PhotoInfo>();
            while (sqlReader.Read())
            {
                PhotoInfo PhotoList = new PhotoInfo()
                {
                    DG_Photos_UserID = GetFieldValue(sqlReader, "DG_Photos_UserID", 0),
                    DG_User_Name = GetFieldValue(sqlReader, "DG_User_Name", string.Empty),
                    DG_Photos_FileName = GetFieldValue(sqlReader, "DG_Photos_FileName", string.Empty),
                    DG_Photos_CreatedOn = GetFieldValue(sqlReader, "DG_Photos_CreatedOn", DateTime.Now),
                    DG_Photos_RFID = GetFieldValue(sqlReader, "DG_Photos_RFID", string.Empty),
                    DG_Photos_Background = GetFieldValue(sqlReader, "DG_Photos_Background", string.Empty),
                    DG_Photos_Frame = GetFieldValue(sqlReader, "DG_Photos_Frame", string.Empty),
                    DG_Photos_DateTime = GetFieldValue(sqlReader, "DG_Photos_DateTime", DateTime.Now),
                    DG_Photos_pkey = GetFieldValue(sqlReader, "DG_Photos_pkey", 0),
                    DG_Photos_Layering = GetFieldValue(sqlReader, "DG_Photos_Layering", string.Empty),
                    DG_Photos_Effects = GetFieldValue(sqlReader, "DG_Photos_Effects", string.Empty),
                    DG_Photos_IsCroped = GetFieldValue(sqlReader, "DG_Photos_IsCroped", false),
                    DG_Photos_IsRedEye = GetFieldValue(sqlReader, "DG_Photos_IsRedEye", false),
                    DG_Photos_Archive = GetFieldValue(sqlReader, "DG_Photos_Archive", false),
                    DG_Location_Id = GetFieldValue(sqlReader, "DG_Location_ID", 0),
                    DG_SubStoreId = GetFieldValue(sqlReader, "DG_SubStoreId", 0),
                    DG_IsCodeType = GetFieldValue(sqlReader, "DG_IsCodeType", false),
                    DateTaken = GetFieldValue(sqlReader, "DateTaken", DateTime.Now),
                    RfidScanType = GetFieldValue(sqlReader, "RfidScanType", 0),

                };

                PhotoListList.Add(PhotoList);
            }
            return PhotoListList;
        }
        public PhotoInfo GetLastGeneratedNumber(int UserID)
        {
            DBParameters.Clear();
            AddParameter("@UserID", UserID);
            IDataReader sqlReader = ExecuteReader(DAOConstant.GETLASTGENERATEDNUMBER);
            PhotoInfo Photos = MapLastGeneratedNumber(sqlReader);
            sqlReader.Close();
            return Photos;
        }
        private PhotoInfo MapLastGeneratedNumber(IDataReader sqlReader)
        {
            PhotoInfo Photos = null;
            while (sqlReader.Read())
            {
                Photos = new PhotoInfo()
                {
                    DG_Photos_RFID = GetFieldValue(sqlReader, "NextNumber", string.Empty),
                };
            }
            return Photos;
        }
        public PhotoInfo GetCheckPhotos(string Photono, int PhotographerID)
        {
            DBParameters.Clear();
            AddParameter("@ParamPhotosUserID", PhotographerID);
            AddParameter("@ParamPhotosFileName", Photono);

            IDataReader sqlReader = ExecuteReader(DAOConstant.USP_GET_CHECKPHOTOS);
            PhotoInfo Photosinfo = MapCheckPhotos(sqlReader);
            sqlReader.Close();
            return Photosinfo;
        }
        private PhotoInfo MapCheckPhotos(IDataReader sqlReader)
        {
            PhotoInfo Photos = null;
            while (sqlReader.Read())
            {
                Photos = new PhotoInfo();
                Photos.DG_Photos_pkey = GetFieldValue(sqlReader, "DG_Photos_pkey", 0);
            }
            return Photos;
        }



        public List<PhotoInfo> GetArchiveImages()
        {
            DBParameters.Clear();
            IDataReader sqlReader = ExecuteReader(DAOConstant.USP_SEL_ARCHIVEIMAGES);
            List<PhotoInfo> PhotosList = MapArchiveImages(sqlReader);
            sqlReader.Close();
            return PhotosList;
        }
        private List<PhotoInfo> MapArchiveImages(IDataReader sqlReader)
        {
            List<PhotoInfo> PhotosList = new List<PhotoInfo>();
            while (sqlReader.Read())
            {
                PhotoInfo Photos = new PhotoInfo()
                {
                    DG_Photos_pkey = GetFieldValue(sqlReader, "DG_Photos_pkey", 0),
                    DG_Photos_FileName = GetFieldValue(sqlReader, "DG_Photos_FileName", string.Empty),
                    DG_Photos_CreatedOn = GetFieldValue(sqlReader, "DG_Photos_CreatedOn", DateTime.Now),
                    DG_Photos_Archive = GetFieldValue(sqlReader, "DG_Photos_Archive", false),
                };
                PhotosList.Add(Photos);
            }
            return PhotosList;
        }




        public List<PhotoInfo> SetArchiveDetails(string imgname)
        {
            DBParameters.Clear();
            AddParameter("@ParamPhotosFileName", imgname);
            IDataReader sqlReader = ExecuteReader(DAOConstant.USP_SEL_ARCHIVEDETAILS);
            List<PhotoInfo> PhotosList = MapArchiveDetails(sqlReader);
            sqlReader.Close();
            return PhotosList;
        }
        private List<PhotoInfo> MapArchiveDetails(IDataReader sqlReader)
        {
            List<PhotoInfo> PhotosList = new List<PhotoInfo>();
            while (sqlReader.Read())
            {
                PhotoInfo Photos = new PhotoInfo()
                {
                    DG_Photos_pkey = GetFieldValue(sqlReader, "DG_Photos_pkey", 0),
                    DG_Photos_FileName = GetFieldValue(sqlReader, "DG_Photos_FileName", string.Empty),
                    DG_Photos_CreatedOn = GetFieldValue(sqlReader, "DG_Photos_CreatedOn", DateTime.Now),
                    DG_Photos_RFID = GetFieldValue(sqlReader, "DG_Photos_RFID", string.Empty),
                    DG_Photos_UserID = GetFieldValue(sqlReader, "DG_Photos_UserID", 0),
                    DG_Photos_Background = GetFieldValue(sqlReader, "DG_Photos_Background", string.Empty),
                    DG_Photos_Frame = GetFieldValue(sqlReader, "DG_Photos_Frame", string.Empty),
                    DG_Photos_DateTime = GetFieldValue(sqlReader, "DG_Photos_DateTime", DateTime.Now),
                    DG_Photos_Layering = GetFieldValue(sqlReader, "DG_Photos_Layering", string.Empty),
                    DG_Photos_Effects = GetFieldValue(sqlReader, "DG_Photos_Effects", string.Empty),
                    DG_Photos_IsCroped = GetFieldValue(sqlReader, "DG_Photos_IsCroped", false),
                    DG_Photos_IsRedEye = GetFieldValue(sqlReader, "DG_Photos_IsRedEye", false),
                    DG_Photos_IsGreen = GetFieldValue(sqlReader, "DG_Photos_IsGreen", false),
                    DG_Photos_MetaData = GetFieldValue(sqlReader, "DG_Photos_MetaData", string.Empty),
                    DG_Photos_Sizes = GetFieldValue(sqlReader, "DG_Photos_Sizes", string.Empty),
                    DG_Photos_Archive = GetFieldValue(sqlReader, "DG_Photos_Archive", false),
                    DG_Location_Id = GetFieldValue(sqlReader, "DG_Location_Id", 0),
                    DG_SubStoreId = GetFieldValue(sqlReader, "DG_SubStoreId", 0),
                    DG_IsCodeType = GetFieldValue(sqlReader, "DG_IsCodeType", false),
                    DateTaken = GetFieldValue(sqlReader, "DateTaken", DateTime.Now),
                    RfidScanType = GetFieldValue(sqlReader, "RfidScanType", 0),

                };

                PhotosList.Add(Photos);
            }
            return PhotosList;
        }
        //public string GetPhotoNameByPhotoID(Int64 Photos_pkey)
        //{
        //    DBParameters.Clear();
        //    AddParameter("@ParamPhotos_pkey", Photos_pkey);

        //    IDataReader sqlReader = ExecuteReader(DAOConstant.USP_GET_PHOTONAMEBYPHOTOID);
        //    List<PhotoInfo> PhotosList = MapPhotoNameByPhotoIDBase(sqlReader);
        //    sqlReader.Close();
        //    return PhotosList.FirstOrDefault().DG_Photos_FileName;
        //}
        public PhotoInfo GetPhotoNameByPhotoID(Int64 Photos_pkey)
        {
            DBParameters.Clear();
            AddParameter("@ParamPhotos_pkey", Photos_pkey);

            IDataReader sqlReader = ExecuteReader(DAOConstant.USP_GET_PHOTONAMEBYPHOTOID);
            List<PhotoInfo> PhotosList = MapPhotoNameByPhotoIDBase(sqlReader);
            sqlReader.Close();
            return PhotosList.FirstOrDefault();
        }
        private List<PhotoInfo> MapPhotoNameByPhotoIDBase(IDataReader sqlReader)
        {
            List<PhotoInfo> PhotosList = new List<PhotoInfo>();
            while (sqlReader.Read())
            {
                PhotoInfo Photos = new PhotoInfo()
                {
                    DG_Photos_FileName = GetFieldValue(sqlReader, "DG_Photos_FileName", string.Empty),
                    DG_Photos_CreatedOn = GetFieldValue(sqlReader, "DG_Photos_CreatedOn", DateTime.Now),
                };
                PhotosList.Add(Photos);
            }
            return PhotosList;
        }

        public List<PhotoInfo> GetPhotoRFIDByPhotoIDList(string photoIdList)
        {
            DBParameters.Clear();
            AddParameter("@ParamPhotosIdList", photoIdList);

            IDataReader sqlReader = ExecuteReader(DAOConstant.USP_GET_PHOTORFIDBYPHOTOIDLIST);
            List<PhotoInfo> PhotosList = MapPhotoNameByPhotoIDBaseList(sqlReader);
            sqlReader.Close();
            return PhotosList;
        }
        private List<PhotoInfo> MapPhotoNameByPhotoIDBaseList(IDataReader sqlReader)
        {
            List<PhotoInfo> PhotosList = new List<PhotoInfo>();
            while (sqlReader.Read())
            {
                PhotoInfo Photos = new PhotoInfo()
                {
                    DG_Photos_pkey = GetFieldValue(sqlReader, "DG_Photos_pkey", 0),
                    DG_Photos_RFID = GetFieldValue(sqlReader, "DG_Photos_RFID", string.Empty),
                    DG_Photos_FileName = GetFieldValue(sqlReader, "DG_Photos_FileName", string.Empty),
                    DG_Photos_CreatedOn = GetFieldValue(sqlReader, "DG_Photos_CreatedOn", DateTime.Now),
                    HotFolderPath = GetFieldValue(sqlReader, "HotFolderPath", string.Empty),
                };
                PhotosList.Add(Photos);
            }
            return PhotosList;
        }
        //public string GetPhotoRFIDByPhotoID(Int64 Photos_pkey)
        //{
        //    DBParameters.Clear();
        //    AddParameter("@ParamPhotos_pkey", Photos_pkey);

        //    IDataReader sqlReader = ExecuteReader(DAOConstant.USP_GET_PHOTORFIDBYPHOTOID);
        //    List<PhotoInfo> PhotosList = MapPhotoRFIDByPhotoIDBase(sqlReader);
        //    sqlReader.Close();
        //    return PhotosList.FirstOrDefault().DG_Photos_RFID;
        //}
        public PhotoInfo GetPhotoRFIDByPhotoID(Int64 Photos_pkey)
        {
            DBParameters.Clear();
            AddParameter("@ParamPhotos_pkey", Photos_pkey);

            IDataReader sqlReader = ExecuteReader(DAOConstant.USP_GET_PHOTORFIDBYPHOTOID);
            List<PhotoInfo> PhotosList = MapPhotoRFIDByPhotoIDBase(sqlReader);
            sqlReader.Close();
            return PhotosList.FirstOrDefault();
        }
        private List<PhotoInfo> MapPhotoRFIDByPhotoIDBase(IDataReader sqlReader)
        {
            List<PhotoInfo> PhotosList = new List<PhotoInfo>();
            while (sqlReader.Read())
            {
                PhotoInfo Photos = new PhotoInfo()
                {
                    DG_Photos_pkey = GetFieldValue(sqlReader, "DG_Photos_pkey", 0),
                    DG_Photos_RFID = GetFieldValue(sqlReader, "DG_Photos_RFID", string.Empty),
                    DG_Photos_CreatedOn = GetFieldValue(sqlReader, "DG_Photos_CreatedOn", DateTime.Now),
                };
                PhotosList.Add(Photos);
            }
            return PhotosList;
        }
        public List<PhotoInfo> GetPhotosBasedonRFID(int SubstoreId, string RFID, bool isCrossSubstoreSearchActive)
        {
            DBParameters.Clear();
            AddParameter("@ParamSubstoreId", SubstoreId);
            AddParameter("@ParamRFID", RFID);
            AddParameter("@ParamIsCrossSubstoreSearchActive", isCrossSubstoreSearchActive);
            IDataReader sqlReader = ExecuteReader(DAOConstant.USP_GET_PHOTOSBASEDONRFID);
            List<PhotoInfo> PhotosList = MapPhotosBasedonRFIDInfo(sqlReader);
            sqlReader.Close();
            return PhotosList;
        }
        private List<PhotoInfo> MapPhotosBasedonRFIDInfo(IDataReader sqlReader)
        {
            List<PhotoInfo> PhotosList = new List<PhotoInfo>();
            while (sqlReader.Read())
            {
                PhotoInfo Photos = new PhotoInfo()
                {
                    DG_Photos_pkey = GetFieldValue(sqlReader, "DG_Photos_pkey", 0),
                    DG_Photos_FileName = GetFieldValue(sqlReader, "DG_Photos_FileName", string.Empty),
                    DG_Photos_CreatedOn = GetFieldValue(sqlReader, "DG_Photos_CreatedOn", DateTime.Now),
                    DG_Photos_RFID = GetFieldValue(sqlReader, "DG_Photos_RFID", string.Empty),
                    DG_Photos_UserID = GetFieldValue(sqlReader, "DG_Photos_UserID", 0),
                    DG_Photos_Background = GetFieldValue(sqlReader, "DG_Photos_Background", string.Empty),
                    DG_Photos_Frame = GetFieldValue(sqlReader, "DG_Photos_Frame", string.Empty),
                    DG_Photos_DateTime = GetFieldValue(sqlReader, "DG_Photos_DateTime", DateTime.Now),
                    DG_Photos_Layering = GetFieldValue(sqlReader, "DG_Photos_Layering", string.Empty),
                    DG_Photos_Effects = GetFieldValue(sqlReader, "DG_Photos_Effects", string.Empty),
                    DG_Photos_IsCroped = GetFieldValue(sqlReader, "DG_Photos_IsCroped", false),
                    DG_Photos_IsRedEye = GetFieldValue(sqlReader, "DG_Photos_IsRedEye", false),
                    DG_Photos_IsGreen = GetFieldValue(sqlReader, "DG_Photos_IsGreen", false),
                    DG_Photos_MetaData = GetFieldValue(sqlReader, "DG_Photos_MetaData", string.Empty),
                    DG_Photos_Sizes = GetFieldValue(sqlReader, "DG_Photos_Sizes", string.Empty),
                    DG_Photos_Archive = GetFieldValue(sqlReader, "DG_Photos_Archive", false),
                    DG_Location_Id = GetFieldValue(sqlReader, "DG_Location_Id", 0),
                    DG_SubStoreId = GetFieldValue(sqlReader, "DG_SubStoreId", 0),
                    DG_IsCodeType = GetFieldValue(sqlReader, "DG_IsCodeType", false),
                    DateTaken = GetFieldValue(sqlReader, "DateTaken", DateTime.Now),
                    RfidScanType = GetFieldValue(sqlReader, "RfidScanType", 0),
                    DG_MediaType = GetFieldValue(sqlReader, "DG_MediaType", 1),
                    SemiOrderProfileId = GetFieldValue(sqlReader, "SemiOrderProfileId", 0),
                    OnlineQRCode = GetFieldValue(sqlReader, "OnlineQRCode", string.Empty),
                };

                PhotosList.Add(Photos);
            }
            return PhotosList;
        }
       
        public List<PhotoGraphersInfo> GetPhotoGrapher()
        {
            DBParameters.Clear();
            IDataReader sqlReader = ExecuteReader(DAOConstant.USP_SEL_PHOTOGRAPHER);
            List<PhotoGraphersInfo> photoGrapherList = MapPhotoGrapher(sqlReader);
            sqlReader.Close();
            return photoGrapherList;
        }
        private List<PhotoGraphersInfo> MapPhotoGrapher(IDataReader sqlReader)
        {
            List<PhotoGraphersInfo> userDetailsList = new List<PhotoGraphersInfo>();
            while (sqlReader.Read())
            {
                PhotoGraphersInfo UserDetails = new PhotoGraphersInfo()
                {
                    DG_User_pkey = GetFieldValue(sqlReader, "DG_User_pkey", 0),
                    DG_User_Roles_Id = GetFieldValue(sqlReader, "DG_User_Roles_Id", 0),
                    Photograper = GetFieldValue(sqlReader, "Photograper", string.Empty)
                };

                userDetailsList.Add(UserDetails);
            }
            return userDetailsList;
        }
        public DG_PhotoGroupInfo GetGroupName(string Group_Name)
        {
            DBParameters.Clear();
            AddParameter("@ParamGroup_Name", Group_Name);
            IDataReader sqlReader = ExecuteReader(DAOConstant.USP_GET_GROUPNAME);
            List<DG_PhotoGroupInfo> PhotoGroupList = MapDG_PhotoGroup(sqlReader);
            sqlReader.Close();
            return PhotoGroupList.FirstOrDefault();
        }
        private List<DG_PhotoGroupInfo> MapDG_PhotoGroup(IDataReader sqlReader)
        {
            List<DG_PhotoGroupInfo> PhotoGroupList = new List<DG_PhotoGroupInfo>();
            while (sqlReader.Read())
            {
                DG_PhotoGroupInfo PhotoGroup = new DG_PhotoGroupInfo()
                {
                    DG_Group_pkey = GetFieldValue(sqlReader, "DG_Group_pkey", 0L),
                    DG_Group_Name = GetFieldValue(sqlReader, "DG_Group_Name", string.Empty),
                    DG_Photo_ID = GetFieldValue(sqlReader, "DG_Photo_ID", 0),
                    DG_Photo_RFID = GetFieldValue(sqlReader, "DG_Photo_RFID", string.Empty),
                    DG_CreatedDate = GetFieldValue(sqlReader, "DG_CreatedDate", DateTime.Now),
                    DG_SubstoreId = GetFieldValue(sqlReader, "DG_SubstoreId", 0),

                };

                PhotoGroupList.Add(PhotoGroup);
            }
            return PhotoGroupList;
        }

        public bool DeletePhotoByName(string name)
        {
            DBParameters.Clear();
            AddParameter("@ParamName", name);
            ExecuteNonQuery(DAOConstant.USP_DEL_PHOTOBYNAME);
            return true;
        }

        public void SaveGroupData(DataTable udt_Group)
        {
            DBParameters.Clear();
            AddParameter("@ParamSavGroup", udt_Group);
            ExecuteReader(DAOConstant.USP_INS_GROUPDATA);
        }
        public void GetMaxId(int? substoreId, out long maxPhotoId, int mediaType)
        {
            maxPhotoId = 0;
            DBParameters.Clear();
            AddParameter("@ParamSubStoreId", SetNullIntegerValue(substoreId));
            AddParameter("@ParamMediaType", mediaType);
            IDataReader sqlReader = ExecuteReader(DAOConstant.usp_GetMaxPhotoId);
            if (sqlReader.Read())
            {

                maxPhotoId = (long)GetFieldValue(sqlReader, "DisplayOrder", 0L); // GET MaxDisplay order
                //maxPhotoId = (long)GetFieldValue(sqlReader, "DG_Photos_pkey", 0L);

            }
        }
        public int GetMaxPhotoIdsequence()
        {
            DBParameters.Clear();
            int DG_Photos_pkey = 0;
            AddParameter("@ParamDG_Photos_pkey", DG_Photos_pkey, ParameterDirection.Output);
            ExecuteNonQuery(DAOConstant.USP_GET_PHOTOIDSEQUENCE);
            int objectId = (int)GetOutParameterValue("@ParamDG_Photos_pkey");
            return objectId;
        }

        public int GetMaxUserIdsequence(Int64 frmImgId, Int64 toImgId, string PhotoGrapherID)
        {
            DBParameters.Clear();
            int DG_Photos_pkey = 0;
            AddParameter("@ParamDG_Photos_SequenceFrm", frmImgId);
            AddParameter("@ParamDG_Photos_SequenceTo", toImgId);
            AddParameter("@ParamDG_Photos_SequenceUserId", Convert.ToInt32(PhotoGrapherID));
            AddParameter("@ParamDG_Photos_Sequence", DG_Photos_pkey, ParameterDirection.Output);
            ExecuteNonQuery(DAOConstant.USP_GETINS_USERIDSEQUENCE);
            int objectId = (int)GetOutParameterValue("@ParamDG_Photos_Sequence");
            return objectId;
        }

        public bool ResetImageProcessedStatus(int PhotoId, int SubStoreId)
        {
            DBParameters.Clear();
            AddParameter("@photoID", PhotoId);
            AddParameter("@SubstoreID", SubStoreId);
            ExecuteNonQuery("usp_SetVideoProcessingByPhotoId");
            return true;
        }
        public string GetPhotoPlayerScore(string photoId)
        {
            DBParameters.Clear();
            string DG_Photos_GumRideVal = string.Empty;
            AddParameter("@ParamDG_Photos_pkey", photoId);
            //AddParameter("@ParamDG_Photos_GumRideVal", DG_Photos_GumRideVal, ParameterDirection.Output);
            AddOutParameterString("@ParamDG_Photos_GumRideVal", SqlDbType.NVarChar, 350);
            ExecuteNonQuery(DAOConstant.USP_GET_PHOTOPLAYERSCORE);
            string playScore = (string)GetOutParameterValue("@ParamDG_Photos_GumRideVal");
            return playScore;
        }


        public string GetVideoFrameCropRatio(int locationId)
        {
            DBParameters.Clear();
            AddParameter("@paramLocationId", locationId);
            string cropRatio = Convert.ToString(ExecuteScalar("USP_GetVideoFrameCropRatio"));
            return cropRatio;
        }

        public int PhotoCountCurrentDate(string RFID, int photographerID, int mediaType, long seriesFirstNum, long seriesLastNum)
        {
            DBParameters.Clear();
            AddParameter("@ParamRFID", RFID);
            AddParameter("@ParamPhotographerID", photographerID);
            AddParameter("@ParamMediaType", mediaType);
            AddParameter("@ParamSeriesStartNum", seriesFirstNum);
            AddParameter("@ParamSeriesEndNum", seriesLastNum);
            return (int)ExecuteScalar("GET_PhotoCountCurrentDate");
        }
        public int SaveDownloadSummary(string RFID, string photographerID)
        {
            DBParameters.Clear();
            AddParameter("@ParamRFID", RFID);
            AddParameter("@ParamPhotographerID", photographerID);
            return (int)ExecuteNonQuery("INS_PhotographerDownloadSummary");
        }

        #region Added by Ajay Sinha to get the photo details by Name.
        public List<PhotoInfo> GetPhotoDetailsbyName(string PhotoName)
        {
            DBParameters.Clear();
            AddParameter("@ParamDG_Photos_FileName", PhotoName);
            IDataReader sqlReader = ExecuteReader(DAOConstant.usp_GET_PhotosByName);
            List<PhotoInfo> PhotosList = MapImagesBYName(sqlReader);
            sqlReader.Close();
            return PhotosList;
        }
        private List<PhotoInfo> MapImagesBYName(IDataReader sqlReader)
        {
            List<PhotoInfo> PhotosList = new List<PhotoInfo>();
            while (sqlReader.Read())
            {
                PhotoInfo Photos = new PhotoInfo()
                {
                    DG_Photos_pkey = GetFieldValue(sqlReader, "DG_Photos_pkey", 0),
                    DG_Photos_FileName = GetFieldValue(sqlReader, "DG_Photos_FileName", string.Empty),
                    DG_Photos_CreatedOn = GetFieldValue(sqlReader, "DG_Photos_CreatedOn", DateTime.Now),
                    DG_Photos_RFID = GetFieldValue(sqlReader, "DG_Photos_RFID", string.Empty),
                    DG_Photos_UserID = GetFieldValue(sqlReader, "DG_Photos_UserID", 0),
                    DG_Photos_Background = GetFieldValue(sqlReader, "DG_Photos_Background", string.Empty),
                    DG_Photos_Frame = GetFieldValue(sqlReader, "DG_Photos_Frame", string.Empty),
                    DG_Photos_DateTime = GetFieldValue(sqlReader, "DG_Photos_DateTime", DateTime.Now),
                    DG_Photos_Layering = GetFieldValue(sqlReader, "DG_Photos_Layering", string.Empty),
                    DG_Photos_Effects = GetFieldValue(sqlReader, "DG_Photos_Effects", string.Empty),
                    DG_Photos_IsCroped = GetFieldValue(sqlReader, "DG_Photos_IsCroped", false),
                    DG_Photos_IsRedEye = GetFieldValue(sqlReader, "DG_Photos_IsRedEye", false),
                    DG_Photos_IsGreen = GetFieldValue(sqlReader, "DG_Photos_IsGreen", false),
                    DG_Photos_MetaData = GetFieldValue(sqlReader, "DG_Photos_MetaData", string.Empty),
                    DG_Photos_Sizes = GetFieldValue(sqlReader, "DG_Photos_Sizes", string.Empty),
                    DG_Photos_Archive = GetFieldValue(sqlReader, "DG_Photos_Archive", false),
                    DG_Location_Id = GetFieldValue(sqlReader, "DG_Location_Id", 0),
                    DG_SubStoreId = GetFieldValue(sqlReader, "DG_SubStoreId", 0),
                };

                PhotosList.Add(Photos);
            }
            return PhotosList;
        }
        /// <summary>
        /// ////////////created by latika ruke for group merge
        /// </summary>
        /// <param name="strGroupNames"></param>
        /// <param name="NewGrpName"></param>
        /// <returns></returns>
        public int SaveGroupMerge(string strGroupNames, string NewGrpName)
        {
            DBParameters.Clear();
            AddParameter("@GroupNames", strGroupNames);
            AddParameter("@NewGrpName", NewGrpName);
            int RetVal = Convert.ToInt32(ExecuteScalar("SaveMergeGroup"));
            return RetVal;
        }/// <summary>
         /// //end
        /////created by latika for group merge
        public List<GroupInfo> GetGroupList()
        {
            DBParameters.Clear();
            IDataReader sqlReader = ExecuteReader("GetGroup");
            List<GroupInfo> GroupList = PopulateGroupList(sqlReader);
            sqlReader.Close();
            return GroupList;
        }
        private List<GroupInfo> PopulateGroupList(IDataReader sqlReader)
        {
            List<GroupInfo> GroupList = new List<GroupInfo>();
            //Dictionary<int, string> listDic = GetDeviceTypes();
            while (sqlReader.Read())
            {
                GroupInfo Group = new GroupInfo();
                Group.GroupID = GetFieldValue(sqlReader, "GroupID", 0);
                Group.GroupName = GetFieldValue(sqlReader, "GroupName", string.Empty);

                GroupList.Add(Group);
            }
            return GroupList;
        }
        /////END by latika
        #endregion

        public bool SaveManualDownloadOrderNumber(DataTable mdt_DisplayOrder)
        {
            bool result = false;
            try
            {
                DBParameters.Clear();
                AddParameter("@ManualDisplauOrder", mdt_DisplayOrder);
                ExecuteReader(DAOConstant.USP_INS_ManualDisplayNumber);
                result = true;
            }
            catch (Exception ex)
            {
                result = false;
            }
            return result;
        }
        public List<PhotoInfo> GetAllSBByStoryBookId(long storyBookId)
        {
            DBParameters.Clear();
            AddParameter("@storyBookId", SetNullLongValue(storyBookId));

            List<PhotoInfo> objectList;
            using (IDataReader sqlReader = ExecuteReader(DAOConstant.usp_GET_AllStoryBookPhotosByStoryBookId))
            {
                objectList = MapPhotosByPkeyStoryBook(sqlReader);
            }
            return objectList;
        }
    }
}
