﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DigiPhoto.IMIX.Model;
using System.Data;


namespace DigiPhoto.IMIX.DataAccess
{
    public class CharacterDao : BaseDataAccess
    {
        #region Constructor
        public CharacterDao(BaseDataAccess baseaccess)
            : base(baseaccess)
        { }
        public CharacterDao()
        { }
        #endregion
        public List<CharacterInfo> GetCharacter()
        {
            DBParameters.Clear();
            AddParameter("@ParamIsActive", SetNullIntegerValue(1));
            AddParameter("@ParamCharacterId", 0);
            AddParameter("@CharacterOperationType", 0);
            IDataReader sqlReader = ExecuteReader(DAOConstant.USP_SEL_CHARACHTER);

            List<CharacterInfo> CharacterList = MapCharacterList(sqlReader);
            sqlReader.Close();
            return CharacterList;
        }

        public List<CharacterInfo> GetCharacter(CharacterInfo charinfo)
        {
            DBParameters.Clear();
            AddParameter("@ParamIsActive", SetNullIntegerValue(1));
            AddParameter("@ParamCharacterId", charinfo.DG_Character_Pkey);
            AddParameter("@CharacterOperationType", charinfo.DG_Character_OperationType);
            IDataReader sqlReader = ExecuteReader(DAOConstant.USP_SEL_CHARACHTER);

            List<CharacterInfo> CharacterList = MapCharacterList(sqlReader);
            sqlReader.Close();
            return CharacterList;
        }

        private List<CharacterInfo> MapCharacterList(IDataReader sqlReader)
        {
            List<CharacterInfo> objectList = new List<CharacterInfo>();
            while (sqlReader.Read())
            {
                CharacterInfo objectValue = new CharacterInfo
                {
                    DG_Character_Pkey = GetFieldValue(sqlReader, "DG_Character_Pkey", 0),
                    DG_Character_Name = GetFieldValue(sqlReader, "DG_Character_Name", string.Empty),
                    DG_Character_IsActive = GetFieldValue(sqlReader, "DG_Character_IsActive", 0),
                    DG_Character_CreatedDate = GetFieldValue(sqlReader, "DG_Character_CreatedDate", DateTime.MinValue)
                };
                objectList.Add(objectValue);
            }
            return objectList;
        }
        public int? GetCharacterId(string PhotographerId)
        {
            int CameraCharacter = 0;
            DBParameters.Clear();
            AddParameter("@ParamPhotographerId", PhotographerId);
            AddParameter("@ParamCameraCharacter", CameraCharacter, ParameterDirection.InputOutput);
            ExecuteNonQuery(DAOConstant.USP_GET_CHARACHTERID);
            int? objectId = (int?)GetOutParameterValue("@ParamCameraCharacter");
            return objectId;
        }

        public bool InsertCharacter(CharacterInfo objCharInfo)
        {
            DBParameters.Clear();
            AddParameter("@DG_Character_Name", objCharInfo.DG_Character_Name);
            AddParameter("@DG_Character_IsActive", objCharInfo.DG_Character_IsActive);
            AddParameter("@CharacterID", 0, ParameterDirection.InputOutput);
            ExecuteNonQuery(DAOConstant.usp_INS_Character);
             
            return true;
             
        }
    }
}
