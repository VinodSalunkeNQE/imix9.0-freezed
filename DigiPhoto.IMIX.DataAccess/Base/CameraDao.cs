﻿using DigiPhoto.IMIX.Model;
using System.Collections.Generic;
using System.Data;
using System;
using System.Text;

namespace DigiPhoto.IMIX.DataAccess
{
    public class CameraDao : BaseDataAccess
    {
        #region Constructor
        public CameraDao(BaseDataAccess baseaccess)
            : base(baseaccess)
        { }
        public CameraDao()
        { }
        #endregion

        public List<CameraInfo> SelectRideCamera()
        {
            IDataReader sqlReader = ExecuteReader(DAOConstant.USP_SEL_RIDECAMERA);
            List<CameraInfo> RideCameraList = MapRideCameraInfo(sqlReader);
            sqlReader.Close();
            return RideCameraList;
        }
        private List<CameraInfo> MapRideCameraInfo(IDataReader sqlReader)
        {
            List<CameraInfo> RideCameraInfoList = new List<CameraInfo>();
            while (sqlReader.Read())
            {
                CameraInfo RideCameraInfo = new CameraInfo()
                {
                    RideCameras = GetFieldValue(sqlReader, "RideCameras", string.Empty),
                    DG_Camera_pkey = GetFieldValue(sqlReader, "DG_Camera_pkey", 0)
                };
                RideCameraInfoList.Add(RideCameraInfo);
            }
            return RideCameraInfoList;
        }
        public List<CameraInfo> GetCameraList()
        {
            IDataReader sqlReader = ExecuteReader(DAOConstant.USP_GET_GetCameraDetails);
            List<CameraInfo> RideCameraList = MapRideCameraList(sqlReader);
            sqlReader.Close();
            return RideCameraList;
        }
        public List<CameraInfo> GetCameraListSearch(int SubstoreID,int LocationID)
        {

            DBParameters.Clear();
            AddParameter("@SubstoreID", SubstoreID);
            AddParameter("@LocationID", LocationID);
            IDataReader sqlReader = ExecuteReader(DAOConstant.USP_GET_GetCameraDetailSearch);
            List<CameraInfo> RideCameraList = MapRideCameraList(sqlReader);
            sqlReader.Close();
            return RideCameraList;
        }
        private List<CameraInfo> MapRideCameraList(IDataReader sqlReader)
        {
            List<CameraInfo> RideCameraInfoList = new List<CameraInfo>();
            while (sqlReader.Read())
            {
                CameraInfo RideCameraInfo = new CameraInfo()
                {
                    PhotographerName = GetFieldValue(sqlReader, "PhotographerName", string.Empty),
                    DG_Camera_Name = GetFieldValue(sqlReader, "DG_Camera_Name", string.Empty),
                    DG_Camera_Make = GetFieldValue(sqlReader, "DG_Camera_Make", string.Empty),
                    DG_Camera_Model = GetFieldValue(sqlReader, "DG_Camera_Model", string.Empty),
                    DG_AssignTo = GetFieldValue(sqlReader, "DG_AssignTo", 0),
                    DG_Camera_Start_Series = GetFieldValue(sqlReader, "DG_Camera_Start_Series", string.Empty),
                    DG_Updatedby = GetFieldValue(sqlReader, "DG_Updatedby", 0),
                    DG_UpdatedDate = GetFieldValue(sqlReader, "DG_UpdatedDate", DateTime.Now),
                    DG_Camera_pkey = GetFieldValue(sqlReader, "DG_Camera_pkey", 0),
                    DG_CameraFolder = GetFieldValue(sqlReader, "DG_CameraFolder", string.Empty),
                    DG_Camera_IsDeleted = GetFieldValue(sqlReader, "DG_Camera_IsDeleted", true),
                    DG_Camera_ID = GetFieldValue(sqlReader, "DG_Camera_ID", 0),
                    IsTripCam = GetFieldValue(sqlReader, "IsTripCam", true),
                    IsLiveStream = GetFieldValue(sqlReader,"IsLiveStream",false),
                    SerialNo = GetFieldValue(sqlReader, "SerialNo", string.Empty),
                    LocationName = GetFieldValue(sqlReader, "LocationName", string.Empty),
                    SubstoreName = GetFieldValue(sqlReader, "SubStoreName", string.Empty),
                };
                RideCameraInfoList.Add(RideCameraInfo);
            }
            return RideCameraInfoList;
        }
        public List<CameraInfo> GetCameraDetailsByID(int? DG_Camera_pkey, int? UserId)
        {
            DBParameters.Clear();
            AddParameter("@ParamDG_Camera_pkey", DG_Camera_pkey);
            AddParameter("@ParamUserId", UserId);

            IDataReader sqlReader = ExecuteReader(DAOConstant.USP_SEL_CAMERA);
            List<CameraInfo> CameraDetailsList = MapCameraDetails(sqlReader);
            sqlReader.Close();
            return CameraDetailsList;
        }
        private List<CameraInfo> MapCameraDetails(IDataReader sqlReader)
        {
            List<CameraInfo> CameraDetailsList = new List<CameraInfo>();
            while (sqlReader.Read())
            {
                CameraInfo CameraDetails = new CameraInfo()
                {
                    DG_Camera_pkey = GetFieldValue(sqlReader, "DG_Camera_pkey", 0),
                    DG_Camera_Name = GetFieldValue(sqlReader, "DG_Camera_Name", string.Empty),
                    DG_Camera_Make = GetFieldValue(sqlReader, "DG_Camera_Make", string.Empty),
                    DG_Camera_Model = GetFieldValue(sqlReader, "DG_Camera_Model", string.Empty),
                    DG_AssignTo = GetFieldValue(sqlReader, "DG_AssignTo", 0),
                    DG_Camera_Start_Series = GetFieldValue(sqlReader, "DG_Camera_Start_Series", string.Empty),
                    DG_Updatedby = GetFieldValue(sqlReader, "DG_Updatedby", 0),
                    DG_UpdatedDate = GetFieldValue(sqlReader, "DG_UpdatedDate", DateTime.Now),
                    DG_Camera_IsDeleted = GetFieldValue(sqlReader, "DG_Camera_IsDeleted", false),
                    DG_Camera_ID = GetFieldValue(sqlReader, "DG_Camera_ID", 0),
                    SyncCode = GetFieldValue(sqlReader, "SyncCode", string.Empty),
                    IsSynced = GetFieldValue(sqlReader, "IsSynced", false),
                    IsChromaColor = GetFieldValue(sqlReader, "Is_ChromaColor", false),
                    CharacterId = GetFieldValueIntNull(sqlReader, "CharacterId", 0),
                    IsTripCam = GetFieldValue(sqlReader, "IsTripCam", false),                    
                    IsLiveStream = GetFieldValue(sqlReader, "IsLiveStream", false),
                    SerialNo = GetFieldValue(sqlReader, "SerialNo", string.Empty),

                };

                CameraDetailsList.Add(CameraDetails);
            }
            return CameraDetailsList;
        }

        public List<CameraInfo> Select(int? cameraId)
        {
            DBParameters.Clear();
            AddParameter("@ParamDG_Camera_pkey", SetNullIntegerValue(cameraId));

            List<CameraInfo> objectList;
            using (IDataReader sqlReader = ExecuteReader(DAOConstant.USP_SEL_CAMERA))
            {
                objectList = MapObject(sqlReader);
            }
            return objectList;
        }
        private List<CameraInfo> MapObject(IDataReader sqlReader)
        {
            List<CameraInfo> objectList = new List<CameraInfo>();
            while (sqlReader.Read())
            {
                CameraInfo objectValue = new CameraInfo
                {
                    PhotographerName = GetFieldValue(sqlReader, "PhotographerName", string.Empty),
                    DG_Camera_Name = GetFieldValue(sqlReader, "DG_Camera_Name", string.Empty),
                    DG_Camera_Make = GetFieldValue(sqlReader, "DG_Camera_Make", string.Empty),
                    DG_Camera_Model = GetFieldValue(sqlReader, "DG_Camera_Model", string.Empty),
                    DG_AssignTo = GetFieldValue(sqlReader, "DG_AssignTo", 0),
                    DG_Camera_Start_Series = GetFieldValue(sqlReader, "DG_Camera_Start_Series", string.Empty),
                    DG_Updatedby = GetFieldValue(sqlReader, "DG_Updatedby", 0),
                    DG_UpdatedDate = GetFieldValue(sqlReader, "DG_UpdatedDate", DateTime.MinValue),
                    DG_Camera_pkey = GetFieldValue(sqlReader, "DG_Camera_pkey", 0),
                    DG_CameraFolder = GetFieldValue(sqlReader, "DG_CameraFolder", string.Empty),
                    DG_Camera_IsDeleted = GetFieldValue(sqlReader, "DG_Camera_IsDeleted", false),
                    DG_Camera_ID = GetFieldValue(sqlReader, "DG_Camera_ID", 0),
                    IsChromaColor = GetFieldValue(sqlReader, "Is_ChromaColor", false)

                };

                objectList.Add(objectValue);
            }
            return objectList;
        }
        public string Delete(int objectvalueId)
        {
            DBParameters.Clear();
            AddParameter("@ParamCameraId", objectvalueId);
            object hotFolderPath = ExecuteScalar(DAOConstant.USP_DEL_CAMERA);
            return Convert.ToString(hotFolderPath);
        }
        public bool SetGetCameraDetails(CameraInfo _objCameraInfo)
        {
            DBParameters.Clear();
            AddParameter("@ParamCameraName", _objCameraInfo.DG_Camera_Name);
            AddParameter("@ParamCameraMake", _objCameraInfo.DG_Camera_Make);
            AddParameter("@ParamCameraModel", _objCameraInfo.DG_Camera_Model);
            AddParameter("@ParamPhotoSeries", _objCameraInfo.DG_Camera_Start_Series);
            AddParameter("@ParamCameraId", _objCameraInfo.DG_Camera_ID);
            AddParameter("@ParamPhotoGrapherId", _objCameraInfo.DG_AssignTo);
            AddParameter("@ParamLoginId", _objCameraInfo.DG_Updatedby);
            AddParameter("@ParamLocationId", _objCameraInfo.DG_Location_ID);
            AddParameter("@ParamSyncCode", _objCameraInfo.SyncCode);
            AddParameter("@ParamIsChromaColor", _objCameraInfo.IsChromaColor);
            AddParameter("@ParamSubstoreId", _objCameraInfo.SubStoreId);
            AddParameter("@ParamIsTripCam", _objCameraInfo.IsTripCam);
            AddParameter("@ParamIsLiveStream", _objCameraInfo.IsLiveStream);
            AddParameter("@ParamSerialNo", _objCameraInfo.SerialNo);
            //AddParameter("@ParamCharacterId", _objCameraInfo.CharacterId == 0 ? null : _objCameraInfo.CharacterId);
            AddParameter("@ParamCameraIdOut", _objCameraInfo.CameraId, ParameterDirection.InputOutput);
            AddParameter("@ParamNewCameraIdOut", _objCameraInfo.DG_Camera_pkey, ParameterDirection.InputOutput);
            ExecuteNonQuery(DAOConstant.usp_UPDANDINS_CameraDetails);
            _objCameraInfo.CameraId = (int)GetOutParameterValue("@ParamCameraIdOut");
            _objCameraInfo.DG_Camera_pkey = (int)GetOutParameterValue("@ParamNewCameraIdOut");
            return true;
        }
        public List<CameraInfo> GetIsResetDPIRequired(int PhotoGrapherId)
        {
            DBParameters.Clear();
            AddParameter("@ParamPhotoGrapherId", PhotoGrapherId);
            IDataReader sqlReader = ExecuteReader(DAOConstant.USP_SEL_CAMERADETAILSBYID);
            List<CameraInfo> OrdersList = MapGetIsResetDPIRequired(sqlReader);
            sqlReader.Close();
            return OrdersList;
        }
        private List<CameraInfo> MapGetIsResetDPIRequired(IDataReader sqlReader)
        {
            List<CameraInfo> objectList = new List<CameraInfo>();
            while (sqlReader.Read())
            {
                CameraInfo objectValue = new CameraInfo
                {
                    DG_Camera_Name = GetFieldValue(sqlReader, "DG_Camera_Name", string.Empty),
                    DG_Camera_Make = GetFieldValue(sqlReader, "DG_Camera_Make", string.Empty),
                    DG_Camera_Model = GetFieldValue(sqlReader, "DG_Camera_Model", string.Empty),
                    DG_AssignTo = GetFieldValue(sqlReader, "DG_AssignTo", 0),
                    DG_Camera_Start_Series = GetFieldValue(sqlReader, "DG_Camera_Start_Series", string.Empty),
                    DG_Updatedby = GetFieldValue(sqlReader, "DG_Updatedby", 0),
                    DG_UpdatedDate = GetFieldValue(sqlReader, "DG_UpdatedDate", DateTime.MinValue),
                    DG_Camera_pkey = GetFieldValue(sqlReader, "DG_Camera_pkey", 0),
                    DG_Camera_IsDeleted = GetFieldValue(sqlReader, "DG_Camera_IsDeleted", false),
                    DG_Camera_ID = GetFieldValue(sqlReader, "DG_Camera_ID", 0),
                    IsChromaColor = GetFieldValue(sqlReader, "Is_ChromaColor", false)
                };
                objectList.Add(objectValue);
            }
            return objectList;
        }

        public bool SetTripCameraSetting(RideCameraSettingInfo objRid)
        {
            DBParameters.Clear();
            AddParameter("@ParamDG_RideCamera_pkey", objRid.DG_RideCamera_pkey, ParameterDirection.InputOutput);
            AddParameter("@ParamDG_RideCamera_Id", objRid.DG_RideCamera_Id);
            AddParameter("@ParamAcquisitionFrameCount", objRid.AcquisitionFrameCount);
            AddParameter("@ParamAcquisitionFrameRateAbs", objRid.AcquisitionFrameRateAbs);
            AddParameter("@ParamAcquisitionFrameRateLimit", objRid.AcquisitionFrameRateLimit);
            AddParameter("@ParamAcquisitionMode", objRid.AcquisitionMode);
            AddParameter("@ParamBalanceRatioAbs", objRid.BalanceRatioAbs);
            AddParameter("@ParamBalanceWhiteAuto", objRid.BalanceWhiteAuto);
            AddParameter("@ParamBalanceWhiteAutoAdjustTol", objRid.BalanceWhiteAutoAdjustTol);
            AddParameter("@ParamBalanceWhiteAutoRate", objRid.BalanceWhiteAutoRate);
            AddParameter("@ParamEventSelector", objRid.EventSelector);

            AddParameter("@ParamEventsEnable1", objRid.EventsEnable1);
            AddParameter("@ParamExposureAuto", objRid.ExposureAuto);
            AddParameter("@ParamExposureAutoAdjustTol", objRid.ExposureAutoAdjustTol);
            AddParameter("@ParamExposureAutoAlg", objRid.ExposureAutoAlg);
            AddParameter("@ParamExposureAutoMax", objRid.ExposureAutoMax);
            AddParameter("@ParamExposureAutoMin", objRid.ExposureAutoMin);
            AddParameter("@ParamExposureAutoOutliers", objRid.ExposureAutoOutliers);
            AddParameter("@ParamExposureAutoRate", objRid.ExposureAutoRate);
            AddParameter("@ParamExposureAutoTarget", objRid.ExposureAutoTarget);
            AddParameter("@ParamExposureTimeAbs", objRid.ExposureTimeAbs);
            AddParameter("@ParamGainAuto", objRid.GainAuto);
            AddParameter("@ParamGainAutoAdjustTol", objRid.GainAutoAdjustTol);

            AddParameter("@ParamGainAutoMax", objRid.GainAutoMax);
            AddParameter("@ParamGainAutoMin", objRid.GainAutoMin);
            AddParameter("@ParamGainAutoOutliers", objRid.GainAutoOutliers);
            AddParameter("@ParamGainAutoRate", objRid.GainAutoRate);
            AddParameter("@ParamGainAutoTarget", objRid.GainAutoTarget);
            AddParameter("@ParamGainRaw", objRid.Gain);
            AddParameter("@ParamGainSelector", objRid.GainSelector);
            AddParameter("@ParamHue", objRid.Hue);
            AddParameter("@ParamGamma", objRid.Gamma);
            AddParameter("@ParamStrobeDelay", objRid.StrobeDelay);
            AddParameter("@ParamStrobeDuration", objRid.StrobeDuration);
            AddParameter("@ParamStrobeDurationMode", objRid.StrobeDurationMode);
            AddParameter("@ParamStrobeSource", objRid.StrobeSource);
            AddParameter("@ParamSyncInGlitchFilter", objRid.SyncInGlitchFilter);

            AddParameter("@ParamSyncInLevels", objRid.SyncInLevels);
            AddParameter("@ParamSyncInSelector", objRid.SyncInSelector);
            AddParameter("@ParamSyncOutLevels", objRid.SyncOutLevels);
            AddParameter("@ParamSyncOutPolarity", objRid.SyncOutPolarity);
            AddParameter("@ParamSyncOutSelector", objRid.SyncOutSelector);
            AddParameter("@ParamSyncOutSource", objRid.SyncOutSource);
            AddParameter("@ParamTriggerActivation", objRid.TriggerActivation);
            AddParameter("@ParamTriggerDelayAbs", objRid.TriggerDelayAbs);
            AddParameter("@ParamTriggerMode", objRid.TriggerMode);
            AddParameter("@ParamTriggerOverlap", objRid.TriggerOverlap);
            AddParameter("@ParamTriggerSelector", objRid.TriggerSelector);
            AddParameter("@ParamTriggerSource", objRid.TriggerSource);

            AddParameter("@ParamUserSetDefaultSelector", objRid.UserSetDefaultSelector);
            AddParameter("@ParamUserSetSelector", objRid.UserSetSelector);
            AddParameter("@ParamWidth", objRid.Width);
            AddParameter("@ParamWidthMax", objRid.WidthMax);
            AddParameter("@ParamHeight", objRid.Height);
            AddParameter("@ParamHeightMax", objRid.HeightMax);
            AddParameter("@ParamImageSize", objRid.ImageSize);
            AddParameter("@ParamPixelFormat", objRid.PixelFormat);
            AddParameter("@ParamIrisMode", objRid.IrisMode);
            AddParameter("@ParamIrisAutoTarget", objRid.IrisAutoTarget);
            AddParameter("@ParamLensPIrisFrequency", objRid.LensPIrisFrequency);
            AddParameter("@ParamLensPIrisNumSteps", objRid.LensPIrisNumSteps);
            AddParameter("@ParamLensPIrisPosition", objRid.LensPIrisPosition);
            AddParameter("@ParamColorTransformationMode", objRid.ColorTransformationMode);
            AddParameter("@ParamColorTransformationValues", objRid.ColorTransformationValues);

            ExecuteNonQuery(DAOConstant.USP_UPDANDINS_RIDECAMERACONFIGURATION);
            return true;
        }

        public string SetTestString(RideCameraSettingInfo objRid)
        {
            string strTest = string.Empty;
            StringBuilder MyStringBuilder = new StringBuilder();

            //MyStringBuilder.Append(objRid.DG_RideCamera_Id);
            MyStringBuilder.Append("," + objRid.DG_RideCamera_Id);
            MyStringBuilder.Append("," + objRid.AcquisitionFrameCount);
            MyStringBuilder.Append("," + objRid.AcquisitionFrameRateAbs);
            MyStringBuilder.Append("," + objRid.AcquisitionFrameRateLimit);
            MyStringBuilder.Append("," + objRid.AcquisitionMode);
            MyStringBuilder.Append("," + objRid.BalanceRatioAbs);
            MyStringBuilder.Append("," + objRid.BalanceRatioSelector);
            MyStringBuilder.Append("," + objRid.BalanceWhiteAuto);
            MyStringBuilder.Append("," + objRid.BalanceWhiteAutoAdjustTol);
            MyStringBuilder.Append("," + objRid.BalanceWhiteAutoRate);
            MyStringBuilder.Append("," + objRid.EventSelector);
            MyStringBuilder.Append("," + objRid.EventsEnable1);
            MyStringBuilder.Append("," + objRid.ExposureAuto);
            MyStringBuilder.Append("," + objRid.ExposureAutoAdjustTol);
            MyStringBuilder.Append("," + objRid.ExposureAutoAlg);
            MyStringBuilder.Append("," + objRid.ExposureAutoMax);
            MyStringBuilder.Append("," + objRid.ExposureAutoMin);
            MyStringBuilder.Append("," + objRid.ExposureAutoOutliers);
            MyStringBuilder.Append("," + objRid.ExposureAutoRate);
            MyStringBuilder.Append("," + objRid.ExposureAutoTarget);
            MyStringBuilder.Append("," + objRid.ExposureTimeAbs);
            MyStringBuilder.Append("," + objRid.GainAuto);
            MyStringBuilder.Append("," + objRid.GainAutoAdjustTol);
            MyStringBuilder.Append("," + objRid.GainAutoMax);
            MyStringBuilder.Append("," + objRid.GainAutoMin);
            MyStringBuilder.Append("," + objRid.GainAutoOutliers);
            MyStringBuilder.Append("," + objRid.GainAutoRate);
            MyStringBuilder.Append("," + objRid.GainAutoTarget);
            MyStringBuilder.Append("," + objRid.Gain);
            MyStringBuilder.Append("," + objRid.GainSelector);
            MyStringBuilder.Append("," + objRid.Hue);
            MyStringBuilder.Append("," + objRid.Gamma);
            MyStringBuilder.Append("," + objRid.StrobeDelay);
            MyStringBuilder.Append("," + objRid.StrobeDuration);
            MyStringBuilder.Append("," + objRid.StrobeDurationMode);
            MyStringBuilder.Append("," + objRid.StrobeSource);
            MyStringBuilder.Append("," + objRid.SyncInGlitchFilter);
            MyStringBuilder.Append("," + objRid.SyncInLevels);
            MyStringBuilder.Append("," + objRid.SyncInSelector);
            MyStringBuilder.Append("," + objRid.SyncOutLevels);
            MyStringBuilder.Append("," + objRid.SyncOutPolarity);
            MyStringBuilder.Append("," + objRid.SyncOutSelector);
            MyStringBuilder.Append("," + objRid.SyncOutSource);
            MyStringBuilder.Append("," + objRid.TriggerActivation);
            MyStringBuilder.Append("," + objRid.TriggerDelayAbs);
            MyStringBuilder.Append("," + objRid.TriggerMode);
            MyStringBuilder.Append("," + objRid.TriggerOverlap);
            MyStringBuilder.Append("," + objRid.TriggerSelector);
            MyStringBuilder.Append("," + objRid.TriggerSource);
            MyStringBuilder.Append("," + objRid.UserSetDefaultSelector);
            MyStringBuilder.Append("," + objRid.UserSetSelector);
            MyStringBuilder.Append("," + objRid.Width);
            MyStringBuilder.Append("," + objRid.WidthMax);
            MyStringBuilder.Append("," + objRid.Height);
            MyStringBuilder.Append("," + objRid.HeightMax);
            MyStringBuilder.Append("," + objRid.ImageSize);
            MyStringBuilder.Append("," + objRid.IrisMode);
            MyStringBuilder.Append("," + objRid.IrisAutoTarget);
            MyStringBuilder.Append("," + objRid.LensPIrisFrequency);
            MyStringBuilder.Append("," + objRid.LensPIrisNumSteps);
            MyStringBuilder.Append("," + objRid.LensPIrisPosition);

            strTest = MyStringBuilder.ToString();
            return strTest;
        }
        public RideCameraSettingInfo GetRideCameraSetting(string RideCameraId)
        {
            DBParameters.Clear();
            AddParameter("@ParamRideCamera_Id", RideCameraId);
            IDataReader sqlReader = ExecuteReader(DAOConstant.USP_GET_RideCameraSetting);
            RideCameraSettingInfo OrdersList = FillRideCameraSettingInfo(sqlReader);
            sqlReader.Close();
            return OrdersList;
        }
        private RideCameraSettingInfo FillRideCameraSettingInfo(IDataReader sqlReader)
        {
            RideCameraSettingInfo objectValue = new RideCameraSettingInfo();
            while (sqlReader.Read())
            {
                objectValue.DG_RideCamera_pkey = GetFieldValue(sqlReader, "DG_RideCamera_pkey", 0);
                objectValue.DG_RideCamera_Id = GetFieldValue(sqlReader, "DG_RideCamera_Id", string.Empty);
                objectValue.AcquisitionFrameCount = GetFieldValue(sqlReader, "AcquisitionFrameCount", string.Empty);
                objectValue.AcquisitionFrameRateAbs = GetFieldValue(sqlReader, "AcquisitionFrameRateAbs", string.Empty);
                objectValue.AcquisitionFrameRateLimit = GetFieldValue(sqlReader, "AcquisitionFrameRateLimit", string.Empty);
                objectValue.AcquisitionMode = GetFieldValue(sqlReader, "AcquisitionMode", string.Empty);
                objectValue.BalanceRatioAbs = GetFieldValue(sqlReader, "BalanceRatioAbs", string.Empty);
                objectValue.BalanceWhiteAuto = GetFieldValue(sqlReader, "BalanceWhiteAuto", string.Empty);
                objectValue.BalanceWhiteAutoAdjustTol = GetFieldValue(sqlReader, "BalanceWhiteAutoAdjustTol", string.Empty);
                objectValue.BalanceWhiteAutoRate = GetFieldValue(sqlReader, "BalanceWhiteAutoRate", string.Empty);
                objectValue.EventSelector = GetFieldValue(sqlReader, "EventSelector", string.Empty);
                objectValue.EventsEnable1 = GetFieldValue(sqlReader, "EventsEnable1", string.Empty);
                objectValue.ExposureAuto = GetFieldValue(sqlReader, "ExposureAuto", string.Empty);
                objectValue.ExposureAutoAdjustTol = GetFieldValue(sqlReader, "ExposureAutoAdjustTol", string.Empty);
                objectValue.ExposureAutoAlg = GetFieldValue(sqlReader, "ExposureAutoAlg", string.Empty);
                objectValue.ExposureAutoMax = GetFieldValue(sqlReader, "ExposureAutoMax", string.Empty);
                objectValue.ExposureAutoMin = GetFieldValue(sqlReader, "ExposureAutoMin", string.Empty);
                objectValue.ExposureAutoOutliers = GetFieldValue(sqlReader, "ExposureAutoOutliers", string.Empty);
                objectValue.ExposureAutoRate = GetFieldValue(sqlReader, "ExposureAutoRate", string.Empty);
                objectValue.ExposureAutoTarget = GetFieldValue(sqlReader, "ExposureAutoTarget", string.Empty);
                objectValue.ExposureTimeAbs = GetFieldValue(sqlReader, "ExposureTimeAbs", string.Empty);
                objectValue.GainAuto = GetFieldValue(sqlReader, "GainAuto", string.Empty);
                objectValue.GainAutoAdjustTol = GetFieldValue(sqlReader, "GainAutoAdjustTol", string.Empty);
                objectValue.GainAutoMax = GetFieldValue(sqlReader, "GainAutoMax", string.Empty);
                objectValue.GainAutoMin = GetFieldValue(sqlReader, "GainAutoMin", string.Empty);
                objectValue.GainAutoOutliers = GetFieldValue(sqlReader, "GainAutoOutliers", string.Empty);
                objectValue.GainAutoRate = GetFieldValue(sqlReader, "GainAutoRate", string.Empty);
                objectValue.GainAutoTarget = GetFieldValue(sqlReader, "GainAutoTarget", string.Empty);
                objectValue.Gain = GetFieldValue(sqlReader, "GainRaw", string.Empty);
                objectValue.GainSelector = GetFieldValue(sqlReader, "GainSelector", string.Empty);
                objectValue.Hue = GetFieldValue(sqlReader, "Hue", string.Empty);
                objectValue.Gamma = GetFieldValue(sqlReader, "Gamma", string.Empty);
                objectValue.StrobeDelay = GetFieldValue(sqlReader, "StrobeDelay", string.Empty);
                objectValue.StrobeDuration = GetFieldValue(sqlReader, "StrobeDuration", string.Empty);
                objectValue.StrobeDurationMode = GetFieldValue(sqlReader, "StrobeDurationMode", string.Empty);
                objectValue.StrobeSource = GetFieldValue(sqlReader, "StrobeSource", string.Empty);
                objectValue.SyncInGlitchFilter = GetFieldValue(sqlReader, "SyncInGlitchFilter", string.Empty);
                objectValue.SyncInLevels = GetFieldValue(sqlReader, "SyncInLevels", string.Empty);
                objectValue.SyncInSelector = GetFieldValue(sqlReader, "SyncInSelector", string.Empty);
                objectValue.SyncOutLevels = GetFieldValue(sqlReader, "SyncOutLevels", string.Empty);
                objectValue.SyncOutPolarity = GetFieldValue(sqlReader, "SyncOutPolarity", string.Empty);
                objectValue.SyncOutSelector = GetFieldValue(sqlReader, "SyncOutSelector", string.Empty);
                objectValue.SyncOutSource = GetFieldValue(sqlReader, "SyncOutSource", string.Empty);
                objectValue.TriggerActivation = GetFieldValue(sqlReader, "TriggerActivation", string.Empty);
                objectValue.TriggerDelayAbs = GetFieldValue(sqlReader, "TriggerDelayAbs", string.Empty);
                objectValue.TriggerMode = GetFieldValue(sqlReader, "TriggerMode", string.Empty);
                objectValue.TriggerOverlap = GetFieldValue(sqlReader, "TriggerOverlap", string.Empty);
                objectValue.TriggerSelector = GetFieldValue(sqlReader, "TriggerSelector", string.Empty);
                objectValue.TriggerSource = GetFieldValue(sqlReader, "TriggerSource", string.Empty);
                objectValue.UserSetDefaultSelector = GetFieldValue(sqlReader, "UserSetDefaultSelector", string.Empty);
                objectValue.UserSetSelector = GetFieldValue(sqlReader, "UserSetSelector", string.Empty);
                objectValue.Width = GetFieldValue(sqlReader, "Width", string.Empty);
                objectValue.WidthMax = GetFieldValue(sqlReader, "WidthMax", string.Empty);
                objectValue.Height = GetFieldValue(sqlReader, "Height", string.Empty);
                objectValue.HeightMax = GetFieldValue(sqlReader, "HeightMax", string.Empty);
                objectValue.ImageSize = GetFieldValue(sqlReader, "ImageSize", string.Empty);
                objectValue.PixelFormat = GetFieldValue(sqlReader, "PixelFormat", string.Empty);
                objectValue.IrisMode = GetFieldValue(sqlReader, "IrisMode", string.Empty);
                objectValue.IrisAutoTarget = GetFieldValue(sqlReader, "IrisAutoTarget", string.Empty);
                objectValue.LensPIrisFrequency = GetFieldValue(sqlReader, "LensPIrisFrequency", string.Empty);
                objectValue.LensPIrisNumSteps = GetFieldValue(sqlReader, "LensPIrisNumSteps", string.Empty);
                objectValue.LensPIrisPosition = GetFieldValue(sqlReader, "LensPIrisPosition", string.Empty);
                objectValue.ColorTransformationMode = GetFieldValue(sqlReader, "ColorTransformationMode", string.Empty);
                objectValue.ColorTransformationValues = GetFieldValue(sqlReader, "ColorTransformationValues", string.Empty);
            }
            return objectValue;
        }


        public string GetCameraPathForRideCameraId(string CameraId)
        {
            DBParameters.Clear();
            AddParameter("@DgCameraModel", CameraId);
            object _objnumber = ExecuteScalar(DAOConstant.USP_GET_RideCameraSettingbyId);
            return Convert.ToString(_objnumber);
        }
        public CameraInfo GetLocationWiseCameraDetails(int LocationId)
        {
            DBParameters.Clear();
            AddParameter("@LocationId", LocationId);
            IDataReader sqlReader = ExecuteReader(DAOConstant.USP_GET_CameraDetailsLocationWise);
            List<CameraInfo> CameraDetails = MapCameraList(sqlReader);
            sqlReader.Close();
            if (CameraDetails != null)
                return CameraDetails[0];
            else return null;
        }
        private List<CameraInfo> MapCameraList(IDataReader sqlReader)
        {
            List<CameraInfo> RideCameraInfoList = new List<CameraInfo>();
            while (sqlReader.Read())
            {
                CameraInfo RideCameraInfo = new CameraInfo()
                {
                    DG_Camera_pkey = GetFieldValue(sqlReader, "DG_Camera_pkey", 0),
                    DG_Camera_Name = GetFieldValue(sqlReader, "DG_Camera_Name", string.Empty),                    
                    DG_AssignTo = GetFieldValue(sqlReader, "DG_AssignTo", 0),                 
                    DG_CameraFolder = GetFieldValue(sqlReader, "DG_CameraFolder", string.Empty),                 
                };
                RideCameraInfoList.Add(RideCameraInfo);
            }
            return RideCameraInfoList;
        }
        public int CheckIsCamDeleteAccess(int RoleID)
        {
            DBParameters.Clear();
            AddParameter("@RoleID", RoleID);
            object _objnumber = ExecuteScalar("usp_CheckIsCamDeleteAccess");
            return Convert.ToInt32(_objnumber);
        }
    }
}
