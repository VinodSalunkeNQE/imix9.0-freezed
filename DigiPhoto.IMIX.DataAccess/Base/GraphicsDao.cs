﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DigiPhoto.IMIX.Model;
using System.Data;

namespace DigiPhoto.IMIX.DataAccess
{
    public class GraphicsDao : BaseDataAccess
    {
        #region Constructor
        public GraphicsDao(BaseDataAccess baseaccess)
            : base(baseaccess)
        { }
        public GraphicsDao()
        { }
        #endregion

    
        public List<GraphicsInfo> Select()
        {

            List<GraphicsInfo> objectList;
            using (IDataReader sqlReader = ExecuteReader(DAOConstant.USP_SEL_GRAPHICS))
            {
                objectList = MapObject(sqlReader);
            }
            return objectList;
        }
        private List<GraphicsInfo> MapObject(IDataReader sqlReader)
        {
            List<GraphicsInfo> objectList = new List<GraphicsInfo>();
            while (sqlReader.Read())
            {
                GraphicsInfo objectValue = new GraphicsInfo
                {
                    DG_Graphics_pkey = GetFieldValue(sqlReader, "DG_Graphics_pkey", 0),
                    DG_Graphics_Name = GetFieldValue(sqlReader, "DG_Graphics_Name", string.Empty),
                    DG_Graphics_Displayname = GetFieldValue(sqlReader, "DG_Graphics_Displayname", string.Empty),
                    DG_Graphics_IsActive = GetFieldValue(sqlReader, "DG_Graphics_IsActive", false),
                    SyncCode = GetFieldValue(sqlReader, "SyncCode", string.Empty),
                    IsSynced = GetFieldValue(sqlReader, "IsSynced", false),
                    CreatedBy = GetFieldValue(sqlReader, "CreatedBy", 0),
                    ModifiedBy = GetFieldValue(sqlReader, "ModifiedBy", 0),
                    CreatedDate = GetFieldValue(sqlReader, "CreatedDate", DateTime.MinValue),
                    ModifiedDate = GetFieldValue(sqlReader, "ModifiedDate", DateTime.MinValue)
                };

                objectList.Add(objectValue);
            }
            return objectList;
        }

        public int Add(GraphicsInfo objectInfo)
        {
            DBParameters.Clear();
            AddParameter("@ParamDG_Graphics_Name", objectInfo.DG_Graphics_Name);
            AddParameter("@ParamDG_Graphics_Displayname", objectInfo.DG_Graphics_Displayname);
            AddParameter("@ParamDG_Graphics_IsActive", objectInfo.DG_Graphics_IsActive);
            AddParameter("@ParamSyncCode", objectInfo.SyncCode);
            AddParameter("@ParamIsSynced", objectInfo.IsSynced);
            AddParameter("@ParamCreatedBy", objectInfo.CreatedBy);
            AddParameter("@ParamGraphicsId", objectInfo.DG_Graphics_pkey, ParameterDirection.Output);

            ExecuteNonQuery(DAOConstant.USP_INS_GRAPHICS);
            int objectId = (int)GetOutParameterValue("@ParamGraphicsId");
            return objectId;
        }
        public bool Delete(int objectvalueId)
        {
            DBParameters.Clear();
            AddParameter("@ParamGraphicsId", objectvalueId);
            ExecuteNonQuery(DAOConstant.USP_DEL_GRAPHICS);
            return true;
        }

        public bool Update(GraphicsInfo objectInfo)
        {
            DBParameters.Clear();
            AddParameter("@ParamDG_Graphics_Name", objectInfo.DG_Graphics_Name);
            AddParameter("@ParamDG_Graphics_Displayname", objectInfo.DG_Graphics_Displayname);
            AddParameter("@ParamDG_Graphics_IsActive", objectInfo.DG_Graphics_IsActive);
            AddParameter("@ParamSyncCode", objectInfo.SyncCode);
            AddParameter("@ParamIsSynced", objectInfo.IsSynced);
            AddParameter("@ParamGraphicsPKey", objectInfo.DG_Graphics_pkey);
            AddParameter("@ParamModifedBy", objectInfo.ModifiedBy);
            ExecuteNonQuery(DAOConstant.USP_UPD_GRAPHICS);
            return true;
        }
    }
}
