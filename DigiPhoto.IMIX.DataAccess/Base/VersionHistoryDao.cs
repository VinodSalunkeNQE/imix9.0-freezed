﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DigiPhoto.IMIX.Model;
using System.Data;

namespace DigiPhoto.IMIX.DataAccess
{
    public class VersionHistoryDao : BaseDataAccess
    {
        #region Constructor
        public VersionHistoryDao(BaseDataAccess baseaccess)
            : base(baseaccess)
        {

        }

        public VersionHistoryDao()
        {

        }
        #endregion

        public List<VersionHistoryInfo> GetVersionDetails(string MachineName)
        {
            DBParameters.Clear();
            AddParameter("@ParamMachineName", MachineName);

            IDataReader sqlReader = ExecuteReader(DAOConstant.USP_GET_VERSIONDETAILS);
            List<VersionHistoryInfo> VersionHistoryList = MapDG_VersionHistoryBase(sqlReader);
            sqlReader.Close();
            return VersionHistoryList;
        }
        private List<VersionHistoryInfo> MapDG_VersionHistoryBase(IDataReader sqlReader)
        {
            List<VersionHistoryInfo> VersionHistoryList = new List<VersionHistoryInfo>();
            while (sqlReader.Read())
            {
                VersionHistoryInfo VersionHistory = new VersionHistoryInfo()
                {
                    DG_Version_Pkey = GetFieldValue(sqlReader, "DG_Version_Pkey", 0),
                    DG_Version_Number = GetFieldValue(sqlReader, "DG_Version_Number", string.Empty),
                    DG_Version_Date = GetFieldValue(sqlReader, "DG_Version_Date", DateTime.Now),
                    DG_UpdatedBY = GetFieldValue(sqlReader, "DG_UpdatedBY", 0),
                    DG_Machine = GetFieldValue(sqlReader, "DG_Machine", string.Empty),

                };

                VersionHistoryList.Add(VersionHistory);
            }
            return VersionHistoryList;
        }

    }
}
