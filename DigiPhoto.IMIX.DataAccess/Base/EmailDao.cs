﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DigiPhoto.IMIX.Model;
using System.Data;


namespace DigiPhoto.IMIX.DataAccess
{
    public class EmailDao : BaseDataAccess
    {
        #region Constructor
        public EmailDao(BaseDataAccess baseaccess)
            : base(baseaccess)
        { }
        public EmailDao()
        { }
        #endregion

        public void TruEmailSettingsTable()
        {
            ExecuteNonQuery(DAOConstant.USP_TRU_EMAILSETTINGS);
        }


        public EmailSettingsInfo GetEmailSettingsDetail()
        {
            IDataReader sqlReader = ExecuteReader(DAOConstant.USP_GET_EMAILSETTINGSDETAIL);
            List<EmailSettingsInfo> EmailSettingsDetailList = MapEmailSettingsDetail(sqlReader);
            sqlReader.Close();
            return EmailSettingsDetailList.FirstOrDefault();
        }
        private List<EmailSettingsInfo> MapEmailSettingsDetail(IDataReader sqlReader)
        {
            List<EmailSettingsInfo> EmailSettingsList = new List<EmailSettingsInfo>();
            while (sqlReader.Read())
            {
                EmailSettingsInfo EmailSettings = new EmailSettingsInfo()
                {
                    DG_EmailSettings_pkey = GetFieldValue(sqlReader, "DG-EmailSettings_pkey", 0),
                    DG_MailSendFrom = GetFieldValue(sqlReader, "DG_MailSendFrom", string.Empty),
                    DG_MailSubject = GetFieldValue(sqlReader, "DG_MailSubject", string.Empty),
                    DG_MailBody = GetFieldValue(sqlReader, "DG_MailBody", string.Empty),
                    DG_SmtpServername = GetFieldValue(sqlReader, "DG_SmtpServername", string.Empty),
                    DG_SmtpServerport = GetFieldValue(sqlReader, "DG_SmtpServerport", string.Empty),
                    DG_SmtpUserDefaultCredentials = GetFieldValue(sqlReader, "DG_SmtpUserDefaultCredentials", false),
                    DG_SmtpServerUsername = GetFieldValue(sqlReader, "DG_SmtpServerUsername", string.Empty),
                    DG_SmtpServerPassword = GetFieldValue(sqlReader, "DG_SmtpServerPassword", string.Empty),
                    DG_SmtpServerEnableSSL = GetFieldValue(sqlReader, "DG_SmtpServerEnableSSL", false),
                    DG_MailBCC = GetFieldValue(sqlReader, "DG_MailBCC", string.Empty),
                };
                EmailSettingsList.Add(EmailSettings);
            }
            return EmailSettingsList;
        }

        public bool InsEmailSettings(string From, string subject, string body, string servername, string serverport, bool defaultcredential, string username, string password, bool enablessl, string mailbcc, int SsId)
        {
            DBParameters.Clear();

            AddParameter("@Paramdefaultcredential", defaultcredential);
            AddParameter("@Paramenablessl", enablessl);
            AddParameter("@ParamFrom", From);
            AddParameter("@Paramsubject", subject);
            AddParameter("@Parambody", body);
            AddParameter("@Paramservername", servername);
            AddParameter("@Paramserverport", serverport);
            AddParameter("@Paramusername", username);
            AddParameter("@Parampassword", password);
            AddParameter("@Parammailbcc", mailbcc);

            ExecuteNonQuery(DAOConstant.USP_INS_EMAILSETTINGS);

            return true;
        }
        public List<EmailStatusInfo> GetEmailStatus(EmailStatusInfo rfidEmailObj)
        {
            DBParameters.Clear();
            AddParameter("@ParamStartDate", SetNullDateTimeValue(rfidEmailObj.StartDate));
            AddParameter("@ParamEndDate", SetNullDateTimeValue(rfidEmailObj.EndDate));
            AddParameter("@ParamStatus", rfidEmailObj.Status);

            IDataReader sqlReader = ExecuteReader(DAOConstant.USP_GET_EMAILSTATUSDETAILS);
            List<EmailStatusInfo> emailList = MapEmailStatus(sqlReader);
            sqlReader.Close();
            return emailList;
        }
        private List<EmailStatusInfo> MapEmailStatus(IDataReader sqlReader)
        {
            List<EmailStatusInfo> ImageList = new List<EmailStatusInfo>();
            while (sqlReader.Read())
            {
                EmailStatusInfo User_Roles = new EmailStatusInfo()
                {
                    DG_Email_pkey = GetFieldValue(sqlReader, "DG_Email_pkey", 0),
                    OrderId = GetFieldValue(sqlReader, "DG_OrderId", string.Empty),
                    EmailDateTime = GetFieldValue(sqlReader, "DG_DateTime", DateTime.Now),
                    EmailId = GetFieldValue(sqlReader, "DG_Email_To", string.Empty),
                    PhotoId = GetFieldValue(sqlReader, "DG_Photos_ID", string.Empty),
                    StatusDetail = GetFieldValue(sqlReader, "DG_Email_IsSent", string.Empty),
                    IsAvailable = GetFieldValue(sqlReader, "IsAvailable", false),
                    MediaName = GetFieldValue(sqlReader, "DG_MediaName", string.Empty),
                    PhotoCount = GetFieldValue(sqlReader, "PhotoCount", string.Empty),
                };
                ImageList.Add(User_Roles);
            }
            return ImageList;
        }
        public bool ResendEmail(string EmailId, int SendType)
        {
            bool Value = false;
            DBParameters.Clear();
            AddParameter("@ParamEmailId", SetNullStringValue(EmailId));
            AddParameter("@ParamSendType", SetNullIntegerValue(SendType));
            AddParameter("@ParamValue", Value, ParameterDirection.Output);

            ExecuteNonQuery(DAOConstant.USP_UPD_EMAILSTATUSORDERID);
            bool objectId = (bool)GetOutParameterValue("@ParamValue");

            return objectId;
        }
        public bool InsertEmailDetails(EMailInfo emailInfo)
        {
            bool EmailIsSen = false;
            DBParameters.Clear();

            AddParameter("@OrderId", emailInfo.OrderId);
            AddParameter("@Emailto", emailInfo.Emailto);
            AddParameter("@EmailBcc", emailInfo.EmailBcc);
            AddParameter("@EmailIsSent", emailInfo.EmailIsSent);
            AddParameter("@Sendername", emailInfo.Sendername);
            AddParameter("@EmailMessage", emailInfo.EmailMessage);
            AddParameter("@DG_MediaName", emailInfo.MediaName);
            AddParameter("@DG_OtherMessage", emailInfo.OtherMessage);
            AddParameter("@DG_MailSubject", emailInfo.MailSubject);
            AddParameter("@DG_MessageType", emailInfo.MediaType);
            ExecuteNonQuery(DAOConstant.usp_INSEmailDetails);
            return true;
        }

        public List<EmailDetailInfo> GetEmailDetails()
        {
            DBParameters.Clear();
            IDataReader sqlReader = ExecuteReader(DAOConstant.USP_GET_EmailDetails);
            List<EmailDetailInfo> emailDetailList = MapEmailDetails(sqlReader);
            sqlReader.Close();
            return emailDetailList;
        }
        private List<EmailDetailInfo> MapEmailDetails(IDataReader sqlReader)
        {
            List<EmailDetailInfo> lstEmailDetail = new List<EmailDetailInfo>();
            while (sqlReader.Read())
            {
                EmailDetailInfo emailDetail = new EmailDetailInfo();
                emailDetail.EmailKey = GetFieldValue(sqlReader, "EmailKey", 0);
                emailDetail.EmailDetailId_Pkey = GetFieldValue(sqlReader, "EmailDetailId_Pkey", 0L);
                emailDetail.PhotoId = GetFieldValue(sqlReader, "PhotoId", 0L);
                emailDetail.OrderId = GetFieldValue(sqlReader, "DG_OrderId", string.Empty);
                emailDetail.DG_Email_To = GetFieldValue(sqlReader, "DG_Email_To", string.Empty);
                emailDetail.DG_Email_Bcc = GetFieldValue(sqlReader, "DG_Email_Bcc", string.Empty);
                emailDetail.DG_EmailSender = GetFieldValue(sqlReader, "DG_EmailSender", string.Empty);
                emailDetail.DG_Message = GetFieldValue(sqlReader, "DG_Message", string.Empty);
                emailDetail.EmailTemplate = GetFieldValue(sqlReader, "EmailTemplate", string.Empty);
                emailDetail.DG_ReportMailBody = GetFieldValue(sqlReader, "DG_ReportMailBody", string.Empty);
                emailDetail.DG_OtherMessage = GetFieldValue(sqlReader, "DG_OtherMessage", string.Empty);
                emailDetail.DG_MessageType = GetFieldValue(sqlReader, "DG_MessageType", string.Empty);
                emailDetail.DG_EmailSubject = GetFieldValue(sqlReader, "DG_EmailSubject", string.Empty);
                emailDetail.DG_MediaName = GetFieldValue(sqlReader, "DG_MediaName", string.Empty);
                emailDetail.message = GetFieldValue(sqlReader, "message", string.Empty);
                lstEmailDetail.Add(emailDetail);
            }
            return lstEmailDetail;
        }
    }
}
