﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DigiPhoto.IMIX.Model;
using System.Data;


namespace DigiPhoto.IMIX.DataAccess
{
    public class BackupHistoryDao : BaseDataAccess
    {
        #region Constructor
        public BackupHistoryDao(BaseDataAccess baseaccess)
            : base(baseaccess)
        { }
        public BackupHistoryDao()
        { }
        #endregion
        public bool UpdAndInsScheduledConfig(int SubStoreId, int Status, DateTime ScheduleDate)
        {
            DBParameters.Clear();
            AddParameter("@ParamSubStoreId", SubStoreId);
            AddParameter("@ParamStatus", Status);
            AddParameter("@ParamScheduleDate", ScheduleDate);
            ExecuteNonQuery(DAOConstant.USP_UPDANDINS_SCHEDULEDCONFIG);
            return true;
        }
        public List<BackupHistory> GetBackupHistoryData(int SubStoreId, DateTime StartDate, DateTime EndDate)
        {
            DBParameters.Clear();
            AddParameter("@ParamSubStoreId", SubStoreId);
            AddParameter("@ParamStartDate", StartDate);
            AddParameter("@ParamEndDate", EndDate);

            IDataReader sqlReader = ExecuteReader(DAOConstant.USP_GET_BACKUPHISTORYDATA);
            List<BackupHistory> BackupHistoryList = MapBackupHistory(sqlReader);
            sqlReader.Close();
            return BackupHistoryList;
        }
        private List<BackupHistory> MapBackupHistory(IDataReader sqlReader)
        {
            List<BackupHistory> BackupHistoryList = new List<BackupHistory>();
            while (sqlReader.Read())
            {
                BackupHistory BackupHistory = new BackupHistory()
                {
                    BackupId = GetFieldValue(sqlReader, "BackupId", 0),
                    ScheduleDate = GetFieldValue(sqlReader, "ScheduleDate", DateTime.Now),
                    StartDate = GetFieldValue(sqlReader, "StartDate", DateTime.Now),
                    EndDate = GetFieldValue(sqlReader, "EndDate", DateTime.Now),
                    Status = GetFieldValue(sqlReader, "Status", 0),
                    SetVisibility = GetFieldValue(sqlReader, "Visibility", string.Empty),
                    ErrorMessage = GetFieldValue(sqlReader, "ErrorMessage", string.Empty),
                    SubStoreId = GetFieldValue(sqlReader, "SubStoreId", 0),
                    CleanupStatus = GetFieldValue(sqlReader, "isBackupDone", 0),
                };
                BackupHistoryList.Add(BackupHistory);
            }
            return BackupHistoryList;
        }

    }
}
