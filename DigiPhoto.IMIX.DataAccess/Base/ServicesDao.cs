﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DigiPhoto.IMIX.Model;
using System.Data;

namespace DigiPhoto.IMIX.DataAccess
{
    public class ServicesDao : BaseDataAccess
    {
        #region Constructor
        public ServicesDao(BaseDataAccess baseaccess)
            : base(baseaccess)
        { }
        public ServicesDao()
        { }
        #endregion

        public List<ServicesInfo> GetServices()
        {
            DBParameters.Clear();

            IDataReader sqlReader = ExecuteReader(DAOConstant.USP_SEL_SERVICES);
            List<ServicesInfo> ServicesList = MapDG_ServicesBase(sqlReader);
            sqlReader.Close();
            return ServicesList;
        }
        private List<ServicesInfo> MapDG_ServicesBase(IDataReader sqlReader)
        {
            List<ServicesInfo> ServicesList = new List<ServicesInfo>();
            while (sqlReader.Read())
            {
                ServicesInfo Services = new ServicesInfo()
                {
                    DG_Service_Id = GetFieldValue(sqlReader, "DG_Service_Id", 0),
                    DG_Sevice_Name = GetFieldValue(sqlReader, "DG_Sevice_Name", string.Empty),
                    DG_Service_Display_Name = GetFieldValue(sqlReader, "DG_Service_Display_Name", string.Empty),
                    DG_Service_Path = GetFieldValue(sqlReader, "DG_Service_Path", string.Empty),
                    IsInterface = GetFieldValue(sqlReader, "IsInterface", false),

                };

                ServicesList.Add(Services);
            }
            return ServicesList;
        }


        public bool AddServices(ServicesInfo objectInfo)
        {
            DBParameters.Clear();
            AddParameter("@ParamIsInterface", objectInfo.IsInterface);
            AddParameter("@ParamDG_Sevice_Name", objectInfo.DG_Sevice_Name);
            AddParameter("@ParamDG_Service_Display_Name", objectInfo.DG_Service_Display_Name);
            AddParameter("@ParamDG_Service_Path", objectInfo.DG_Service_Path);
            AddParameter("@IsService", objectInfo.IsService);
            AddParameter("@RunLevel", objectInfo.RunLevel);
            ExecuteNonQuery(DAOConstant.USP_INS_SERVICES);
            return true;
        }
    }
}
