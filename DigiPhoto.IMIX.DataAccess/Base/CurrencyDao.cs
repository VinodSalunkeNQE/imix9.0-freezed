﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DigiPhoto.IMIX.Model;
using System.Data;

namespace DigiPhoto.IMIX.DataAccess
{
    public class CurrencyDao : BaseDataAccess
    {
        #region Constructor
        public CurrencyDao(BaseDataAccess baseaccess)
            : base(baseaccess)
        { }
        public CurrencyDao()
        { }
        #endregion
        public List<CurrencyInfo> SelectCurrency()
        {
            DBParameters.Clear();
            this.OpenConnection();
            AddParameter("@ParamCurrencyId", SetNullIntegerValue(null));
            AddParameter("@ParamIsActive", SetNullBoolValue(null));
            List<CurrencyInfo> objectList;
            using (IDataReader sqlReader = ExecuteReader(DAOConstant.USP_SEL_CURRENCY))
            {
                objectList = MapObject(sqlReader);
            }
            return objectList;
        }
        public List<CurrencyInfo> Select(int? currencyId, bool? isActive)
        {
            DBParameters.Clear();
            AddParameter("@ParamCurrencyId", SetNullIntegerValue(currencyId));
            AddParameter("@ParamIsActive", SetNullBoolValue(isActive));

            List<CurrencyInfo> objectList;
            using (IDataReader sqlReader = ExecuteReader(DAOConstant.USP_SEL_CURRENCY))
            {
                objectList = MapObject(sqlReader);
            }
            return objectList;
        }
        private List<CurrencyInfo> MapObject(IDataReader sqlReader)
        {
            List<CurrencyInfo> objectList = new List<CurrencyInfo>();
            while (sqlReader.Read())
            {
                CurrencyInfo objectValue = new CurrencyInfo
                {
                    DG_Currency_pkey = GetFieldValue(sqlReader, "DG_Currency_pkey", 0),
                    DG_Currency_Name = GetFieldValue(sqlReader, "DG_Currency_Name", string.Empty),
                    DG_Currency_Rate = GetFieldValue(sqlReader, "DG_Currency_Rate", 0.0F),
                    DG_Currency_Symbol = GetFieldValue(sqlReader, "DG_Currency_Symbol", string.Empty),
                    DG_Currency_UpdatedDate = GetFieldValue(sqlReader, "DG_Currency_UpdatedDate", DateTime.MinValue),
                    DG_Currency_ModifiedBy = GetFieldValue(sqlReader, "DG_Currency_ModifiedBy", 0),
                    DG_Currency_Default = GetFieldValue(sqlReader, "DG_Currency_Default", false),
                    DG_Currency_Icon = GetFieldValue(sqlReader, "DG_Currency_Icon", string.Empty),
                    DG_Currency_Code = GetFieldValue(sqlReader, "DG_Currency_Code", string.Empty),
                    DG_Currency_IsActive = GetFieldValue(sqlReader, "DG_Currency_IsActive", false),
                    SyncCode = GetFieldValue(sqlReader, "SyncCode", string.Empty),
                    IsSynced = GetFieldValue(sqlReader, "IsSynced", false),

                };

                objectList.Add(objectValue);
            }
            return objectList;
        }

        public CurrencyInfo GetDefaultCurrencyName()
        {
            List<CurrencyInfo> objectList;
            using (IDataReader sqlReader = ExecuteReader(DAOConstant.USP_GET_DEFAULTCURRENCYNAME))
            {
                objectList = MapDefaultCurrencyNameInfo(sqlReader);
            }
            return objectList.FirstOrDefault();
        }
        public CurrencyInfo GetDefaultCurrency()
        {
            List<CurrencyInfo> objectList;
            this.OpenConnection();
            using (IDataReader sqlReader = ExecuteReader(DAOConstant.USP_GET_DEFAULTCURRENCYNAME))
            {
                objectList = MapDefaultCurrencyNameInfo(sqlReader);
            }
            return objectList.FirstOrDefault();
        }
        private List<CurrencyInfo> MapDefaultCurrencyNameInfo(IDataReader sqlReader)
        {
            List<CurrencyInfo> CurrencyList = new List<CurrencyInfo>();
            while (sqlReader.Read())
            {
                CurrencyInfo Currency = new CurrencyInfo()
                {
                    DG_Currency_pkey = GetFieldValue(sqlReader, "DG_Currency_pkey", 0),
                    DG_Currency_Name = GetFieldValue(sqlReader, "DG_Currency_Name", string.Empty),
                };

                CurrencyList.Add(Currency);
            }
            return CurrencyList;
        }
        public bool Delete(int objectvalueId)
        {
            DBParameters.Clear();
            AddParameter("@ParamCurrencyId", objectvalueId);
            ExecuteNonQuery(DAOConstant.USP_DEL_CURRENCY);
            return true;
        }

        public int Add(CurrencyInfo objectInfo)
        {
            DBParameters.Clear();
            //AddParameter("@ParamDG_Currency_pkey", objectInfo.DG_Currency_pkey);
            AddParameter("@ParamDG_Currency_Name", objectInfo.DG_Currency_Name);
            AddParameter("@ParamDG_Currency_Rate", objectInfo.DG_Currency_Rate);
            AddParameter("@ParamDG_Currency_Symbol", objectInfo.DG_Currency_Symbol);
            AddParameter("@ParamDG_Currency_UpdatedDate", objectInfo.DG_Currency_UpdatedDate);
            AddParameter("@ParamDG_Currency_ModifiedBy", objectInfo.DG_Currency_ModifiedBy);
            AddParameter("@ParamDG_Currency_Default", objectInfo.DG_Currency_Default);
            AddParameter("@ParamDG_Currency_Icon", objectInfo.DG_Currency_Icon);
            AddParameter("@ParamDG_Currency_Code", objectInfo.DG_Currency_Code);
            AddParameter("@ParamDG_Currency_IsActive", objectInfo.DG_Currency_IsActive);
            AddParameter("@ParamSyncCode", objectInfo.SyncCode);
            AddParameter("@ParamIsSynced", objectInfo.IsSynced);
            AddParameter("@ParamCurrencyID", objectInfo.DG_Currency_pkey, ParameterDirection.Output);

            ExecuteNonQuery(DAOConstant.USP_INS_CURRENCY);
            int objectId = (int)GetOutParameterValue("@ParamCurrencyID");
            return objectId;
        }
        public bool Update(CurrencyInfo objectInfo)
        {
            DBParameters.Clear();
            AddParameter("@ParamDG_Currency_pkey", objectInfo.DG_Currency_pkey);
            AddParameter("@ParamDG_Currency_Name", objectInfo.DG_Currency_Name);
            AddParameter("@ParamDG_Currency_Rate", objectInfo.DG_Currency_Rate);
            AddParameter("@ParamDG_Currency_Symbol", objectInfo.DG_Currency_Symbol);
            AddParameter("@ParamDG_Currency_Code", objectInfo.DG_Currency_Code);
            AddParameter("@ParamIsSynced", objectInfo.IsSynced);
            ExecuteNonQuery(DAOConstant.USP_UPD_CURRENCY);
            return true;
        }
    }
}
