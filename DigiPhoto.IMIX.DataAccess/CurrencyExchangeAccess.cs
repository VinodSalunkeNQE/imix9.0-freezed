﻿using DigiPhoto.IMIX.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace DigiPhoto.IMIX.DataAccess
{
    public class CurrencyExchangeAccess : BaseDataAccess
    {
        #region Constructor
        public CurrencyExchangeAccess(BaseDataAccess baseaccess)
            : base(baseaccess)
        {

        }

        public CurrencyExchangeAccess()
        {

        }
        #endregion
        public List<CurrencyExchangeinfo> GetProfileList()
        {
            DBParameters.Clear();
           // AddParameter("@DeviceId", DeviceId);
            IDataReader sqlReader = ExecuteReader("Currency.uspGetProfileAudit");
            List<CurrencyExchangeinfo> profileeList = PopulateProfileList(sqlReader);
            sqlReader.Close();
            return profileeList;
        }

        private List<CurrencyExchangeinfo> PopulateProfileList(IDataReader sqlReader)
        {
            List<CurrencyExchangeinfo> profileList = new List<CurrencyExchangeinfo>();
            //Dictionary<int, string> listDic = GetDeviceTypes();
            while (sqlReader.Read())
            {
                CurrencyExchangeinfo profile = new CurrencyExchangeinfo();
                profile.ProfileAuditID = GetFieldValue(sqlReader, "ProfileAuditID", 0L);
                profile.ProfileName = GetFieldValue(sqlReader, "ProfileName", string.Empty);
                profile.CreatedBy = GetFieldValue(sqlReader, "CreatedBy", 0L);
                profile.Updatedby = GetFieldValue(sqlReader, "Updatedby", 0L);
                profile.CreatedOn = GetFieldValue(sqlReader, "CreatedOn", DateTime.Now);
                profile.updatedon = GetFieldValue(sqlReader, "updatedon", DateTime.Now);
                profile.startDate = GetFieldValue(sqlReader, "startDate", DateTime.Now);
                profile.PublishedOn = GetFieldValue(sqlReader, "PublishedOn", DateTime.Now);
                profile.IsCurrent = GetFieldValue(sqlReader, "IsCurrent", false);
                if (GetFieldValue(sqlReader, "Enddate", DateTime.MinValue) == DateTime.MinValue)
                {
                    profile.Enddate = null;                    
                }
                else
                {
                    profile.Enddate = GetFieldValue(sqlReader, "Enddate", DateTime.Now);
                }
                profile.IsActive = GetFieldValue(sqlReader, "IsActive", false);
                profileList.Add(profile);
            }
            return profileList;
        }



        public List<RateDetailInfo> GetProfilerateDetailList(long profileAuditID)
        {
            DBParameters.Clear();
            AddParameter("@ProfileAuditID", profileAuditID);
            IDataReader sqlReader = ExecuteReader("Currency.uspGetProfilerateByID");
            List<RateDetailInfo> profileeList = PopulateProfileRateList(sqlReader);
            sqlReader.Close();
            return profileeList;
        }

        private List<RateDetailInfo> PopulateProfileRateList(IDataReader sqlReader)
        {
            List<RateDetailInfo> profileList = new List<RateDetailInfo>();
            //Dictionary<int, string> listDic = GetDeviceTypes();
            while (sqlReader.Read())
            {
                RateDetailInfo profilerate = new RateDetailInfo();
                profilerate.DG_Currency_Name = GetFieldValue(sqlReader, "DG_Currency_Name", string.Empty);
                profilerate.DG_Currency_Code = GetFieldValue(sqlReader, "DG_Currency_Code", string.Empty);
                profilerate.ExchangeRate = GetFieldValue(sqlReader, "ExchangeRate", 0M);
                profilerate.IsActive = GetFieldValue(sqlReader, "IsActive", false);
                profileList.Add(profilerate);
            }
            return profileList;
        }


        public bool UploadCurrencyData(long CreatedBy, DateTime CreatedOn, DataTable dtCurrencyExchange)
        {
            DBParameters.Clear();
            AddParameter("@CreatedBy", CreatedBy);
            AddParameter("@CreatedOn", CreatedOn);
            AddParameter("@udt_CurrencyExchange", dtCurrencyExchange);            
            ExecuteNonQuery("[Currency].[UploadCurrencyData]");
            return true;;
        }

        public bool UpdateInsertProfile(long CreatedBy)
        {
            DBParameters.Clear();
            AddParameter("@CreatedBy", CreatedBy);              
            ExecuteNonQuery("[Currency].[uspUpdateInsertProfileAudit]");
            return true;;
        }

        

        
    }
}
