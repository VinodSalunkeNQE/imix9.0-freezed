﻿using DigiPhoto.IMIX.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DigiPhoto.IMIX.DataAccess
{
   public class FRTokenAccess : BaseDataAccess
    {
        public FRTokenAccess(BaseDataAccess baseaccess)
           : base(baseaccess)
        {

        }

        public FRTokenAccess()
        {

        }
        /// <summary>
        /// Added by Kailash ON 25 SEP 2019 to FETCH FR token API
        /// </summary>
        /// <param name="client_id">ID OF API Client</param>
        /// <returns></returns>
        public FRAPIDetails GetToken(FRAPIDetails fRAPIDetails)
        {
            try
            {
                DBParameters.Clear();
                AddParameter("@ClientID", fRAPIDetails.client_id);
                IDataReader sqlReader = ExecuteReader("SELECT TOP 1 ISNULL(Token,'NA') AS Token,ISNULL(ExpDate,GETDATE()) AS ExpDate FROM FR_Token WHERE ClientID=@ClientID AND CONVERT(DATE,ExpDate)>=CONVERT(DATE,GETDATE()) and scope='euclid' ORDER BY CONVERT(DATE,ExpDate) DESC", CommandType.Text);
                while (sqlReader.Read())
                {
                    fRAPIDetails.Token = Convert.ToString(sqlReader["Token"]);
                    fRAPIDetails.ExpireDate = Convert.ToDateTime(sqlReader["ExpDate"]);
                    break;
                }
                if (string.IsNullOrEmpty(fRAPIDetails.Token) || fRAPIDetails.Token.ToUpper() == "NA")
                    fRAPIDetails.ExpireDate = DateTime.Now.AddDays(-1);
                sqlReader.Close();
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
            }
            return fRAPIDetails;
        }
        /// <summary>
        /// Added by Kailash ON 25 SEP 2019 to SAVE FR token API
        /// </summary>
        /// <param name="fRAPIDetails">ID OF API Client</param>
        /// <returns></returns>
        public void SaveNewToken(FRAPIDetails fRAPIDetails)
        {

            try
            {
                DBParameters.Clear();
                AddParameter("@ClientID", fRAPIDetails.client_id);
                AddParameter("@Token", fRAPIDetails.Token);
                AddParameter("@ExpDate", fRAPIDetails.ExpireDate);
                AddParameter("@Scope", "euclid");
                ExecuteNonQuery("INSERT INTO FR_Token ([ClientID],[Token],[ExpDate],[Scope]) VALUES(@ClientID,@Token,@ExpDate,@Scope)", CommandType.Text);
            }
            catch(Exception ex)
            { ErrorHandler.ErrorHandler.LogError(ex); }
        }
    }
}
