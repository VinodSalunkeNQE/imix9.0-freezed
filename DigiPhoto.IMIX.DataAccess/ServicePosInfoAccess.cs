﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DigiPhoto.IMIX.Model;
using System.Data;


namespace DigiPhoto.IMIX.DataAccess
{
    public class ServicePosInfoAccess : BaseDataAccess
    {
        #region Constructor
        public ServicePosInfoAccess(BaseDataAccess baseaccess)
            : base(baseaccess)
        { }
        public ServicePosInfoAccess()
        { }
        #endregion

        public List<ServicePosInfo> GetServices()
        {
            DBParameters.Clear();

            IDataReader sqlReader = ExecuteReader(DAOConstant.USP_SERVICESPOSDETAILS);
            List<ServicePosInfo> ServicesList = MapDG_ServicesBase(sqlReader);
            sqlReader.Close();
            return ServicesList;
        }
        private List<ServicePosInfo> MapDG_ServicesBase(IDataReader sqlReader)
        {
            List<ServicePosInfo> ServicesList = new List<ServicePosInfo>();
            while (sqlReader.Read())
            {
                ServicePosInfo Services = new ServicePosInfo()
                {
                    ServiceId = GetFieldValue(sqlReader, "ServiceID", 0),
                    ImixPosId = GetFieldValue(sqlReader, "iMixPOSDetailID", 0L),
                    SubstoreId = GetFieldValue(sqlReader, "DG_SubStore_pkey", 0),
                    ServiceName = GetFieldValue(sqlReader, "Service Display Name", string.Empty),
                    SystemName = GetFieldValue(sqlReader, "SystemName", string.Empty),
                    SubStoremName = GetFieldValue(sqlReader, "SubStore name", string.Empty),
                    RunLevel = GetFieldValue(sqlReader, "RunLevel", 0),
                    Status = GetFieldValue(sqlReader, "Status", string.Empty),
                    UniqueCode = GetFieldValue(sqlReader, "ServiceID", 0).ToString() + "$$" + GetFieldValue(sqlReader, "iMixPOSDetailID", 0L).ToString() + "$$" +
                    GetFieldValue(sqlReader, "DG_SubStore_pkey", 0) + "$$" + GetFieldValue(sqlReader, "Service Display Name", string.Empty) + "$$" +
                    GetFieldValue(sqlReader, "SubStore name", string.Empty) + "$$" + GetFieldValue(sqlReader, "RunLevel", 0).ToString() + "$$" + GetFieldValue(sqlReader, "IsService", false).ToString()
                    + "$$" + GetFieldValue(sqlReader, "DG_Service_Path", string.Empty).ToString()

                };

                ServicesList.Add(Services);
            }
            return ServicesList;
        }

        public List<SvcRunningInfo> GetServiceInfoAccess(long ServiceID, long ImixPOSDetailID, long SubStoreID)
        {

            DBParameters.Clear();
            AddParameter("@ServiceID", ServiceID);
            AddParameter("@ImixPOSDetailID", ImixPOSDetailID);
            AddParameter("@SubStoreID", SubStoreID);
            IDataReader sqlReader = ExecuteReader(DAOConstant.USP_GETSERVICEINFO);
            //IDataReader sqlReader = ExecuteReader(DAOConstant.usp_GetServiceInfo);
            List<SvcRunningInfo> serviceRuningHistory = PopulateServiceInfo(sqlReader);
            sqlReader.Close();
            return serviceRuningHistory;
        }

        private List<SvcRunningInfo> PopulateServiceInfo(IDataReader sqlReader)
        {
            List<SvcRunningInfo> svcRunningInfo = new List<SvcRunningInfo>();
            while (sqlReader.Read())
            {
                SvcRunningInfo svcRngInfo = new SvcRunningInfo()
                {
                    LastStatusOnDate = GetFieldValue(sqlReader, "LastStatusOnDate", DateTime.Now),
                    Status = GetFieldValue(sqlReader, "Status", string.Empty),
                    ServicePOSMappingID = GetFieldValue(sqlReader, "ServicePOSMappingID", 0L),
                    SubStoreName = GetFieldValue(sqlReader, "SubStoreName", string.Empty),
                    ServiceName = GetFieldValue(sqlReader, "ServiceName", string.Empty),
                    SystemName = GetFieldValue(sqlReader, "SystemName", string.Empty)


                };

                svcRunningInfo.Add(svcRngInfo);

            }

            return svcRunningInfo;
        }

        public List<ImixPOSDetail> GetPOSDetailAccess(long SubStoreID, int RunLevel)
        {
            DBParameters.Clear();
            AddParameter("@SubStoreID", SubStoreID);
            AddParameter("@RunLevel", RunLevel);

            IDataReader sqlReader = ExecuteReader(DAOConstant.USP_GETPOSDETAIL);
            List<ImixPOSDetail> imixPosDetail = PopulatePOSDetail(sqlReader);
            sqlReader.Close();
            return imixPosDetail;
        }
		/////changed by latika for location wise setting image,video and manula download process
        public List<ImixPOSDetail> GetPOSDetailByLocation(long SubStoreID, int LocationID)
        {
            DBParameters.Clear();
            AddParameter("@SubStoreID", SubStoreID);
            AddParameter("@LocationID", LocationID);

            IDataReader sqlReader = ExecuteReader(DAOConstant.USP_GetPOSDetailByLocation);
            List<ImixPOSDetail> imixPosDetail = PopulatePOSDetail(sqlReader);
            sqlReader.Close();
            return imixPosDetail;
        }
////end
        private List<ImixPOSDetail> PopulatePOSDetail(IDataReader sqlReader)
        {
            List<ImixPOSDetail> imixPosDetail = new List<ImixPOSDetail>();
            while (sqlReader.Read())
            {
                ImixPOSDetail _imixPosDetail = new ImixPOSDetail()
                {
                    SystemName = GetFieldValue(sqlReader, "SystemName", string.Empty),
                    ImixPOSDetailID = GetFieldValue(sqlReader, "iMixPOSDetailID", 0L)
                };
                imixPosDetail.Add(_imixPosDetail);
            }
            return imixPosDetail;
        }

        public int StartStopServiceByPosIdAccess(long ServiceId, long SubstoreId, long ImixPosDetailId, bool Status, string CreatedBy)
        {
            DBParameters.Clear();
            AddParameter("@ServiceId", ServiceId);
            AddParameter("@SubstoreId", SubstoreId);
            AddParameter("@ImixPosDetailId", ImixPosDetailId);
            AddParameter("@Status", Status);
            AddParameter("@CreatedBy", CreatedBy);
            this.OpenConnection();
            int Count = (int)ExecuteNonQuery(DAOConstant.USP_STARTSTOPSERVICEBYPOSID);

            return Count;
        }

        public int InsertImixPosAccess(ImixPOSDetail imixposdetail)
        {
            int Count = 0;

            DBParameters.Clear();
            AddParameter("@iMixPOSDetailID", imixposdetail.ImixPOSDetailID);
            AddParameter("@SystemName", imixposdetail.SystemName);
            AddParameter("@IPAddress", imixposdetail.IPAddress);
            AddParameter("@MacAddress", imixposdetail.MacAddress);
            AddParameter("@SubStoreID", imixposdetail.SubStoreID);
            AddParameter("@IsActive", imixposdetail.IsActive);
            AddParameter("@CreatedBy", imixposdetail.CreatedBy);
            AddParameter("@IsStart", imixposdetail.IsStart);
            AddParameter("@SyncCode", imixposdetail.SyncCode);
            Count = (int)ExecuteNonQuery(DAOConstant.USP_INSERTUPDATEIMIXPOSDETAIL);
            return Count;
        }

        public string CheckRunnignServiceAccess(long ServiceId, long SubStoreId, int Level, string SystemName)
        {
            string checkService = string.Empty;
            DBParameters.Clear();
            AddParameter("@ServiceId", ServiceId);
            AddParameter("@SubStoreId", SubStoreId);
            AddParameter("@Level", Level);
            AddParameter("@SystemName", SystemName);
            this.OpenConnection();
            IDataReader sqlReader = ExecuteReader(DAOConstant.USP_CHECKRUNNINGSERVICEBYSERVICEID);
            while (sqlReader.Read())
            {
                checkService = GetFieldValue(sqlReader, "SystemName", string.Empty);
            }
            return checkService;

        }

        public List<SubStore> GetSubstoreDetailsAccess()
        {
            DBParameters.Clear();

            IDataReader sqlReader = ExecuteReader(DAOConstant.USP_GETSUBSTOREDETAIL);
            List<SubStore> substore = PoppulateSubstoreDetails(sqlReader);
            sqlReader.Close();
            return substore;
        }

        private List<SubStore> PoppulateSubstoreDetails(IDataReader sqlReader)
        {
            List<SubStore> substore = new List<SubStore>();
            while (sqlReader.Read())
            {
                SubStore _substore = new SubStore()
                {
                    DG_SubStore_pkey = GetFieldValue(sqlReader, "DG_SubStore_pkey", 0),
                    DG_SubStore_Name = GetFieldValue(sqlReader, "DG_SubStore_Name", string.Empty),
                };

                substore.Add(_substore);
            }
            return substore;
        }
        public List<ImixPOSDetail> GetPosDetailsAccess()
        {
            DBParameters.Clear();

            IDataReader sqlReader = ExecuteReader(DAOConstant.USP_GETSYSTEMDETAIL);
            List<ImixPOSDetail> imixPosDetail = PoppulatePosDetails(sqlReader);
            sqlReader.Close();
            return imixPosDetail;
        }

        private List<ImixPOSDetail> PoppulatePosDetails(IDataReader sqlReader)
        {
            List<ImixPOSDetail> imixPosDetai = new List<ImixPOSDetail>();
            while (sqlReader.Read())
            {
                ImixPOSDetail _imixPosDetail = new ImixPOSDetail()
                {
                    SystemName = GetFieldValue(sqlReader, "SystemName", string.Empty),
                    ImixPOSDetailID = GetFieldValue(sqlReader, "iMixPOSDetailID", 0L),
                };

                imixPosDetai.Add(_imixPosDetail);
            }
            return imixPosDetai;
        }

        public List<MappedPos> MappedPosDetailAccess()
        {
            DBParameters.Clear();
            IDataReader sqlReader = ExecuteReader(DAOConstant.USP_MAPPEDPOSDETAIL);
            List<MappedPos> mappedPosDetail = PoppulateMappedPosDetail(sqlReader);
            sqlReader.Close();
            return mappedPosDetail;
        }

        private List<MappedPos> PoppulateMappedPosDetail(IDataReader sqlReader)
        {
            List<MappedPos> mappedPosDetail = new List<MappedPos>();
            while (sqlReader.Read())
            {
                MappedPos _mappedPosDetail = new MappedPos()
                {
                    SystemName = GetFieldValue(sqlReader, "SystemName", string.Empty),
                    ImixPOSDetailID = GetFieldValue(sqlReader, "iMixPOSDetailID", 0L),
                    SubStoreID = GetFieldValue(sqlReader, "SubStoreID", 0L),
                    SubStoreName = GetFieldValue(sqlReader, "DG_SubStore_Name", string.Empty),
                    UniqueCode = GetFieldValue(sqlReader, "iMixPOSDetailID", 0L).ToString() + "$$"
                               + GetFieldValue(sqlReader, "SystemName", string.Empty) + "$$"
                               + GetFieldValue(sqlReader, "DG_SubStore_Name", string.Empty) + "$$"
                               + GetFieldValue(sqlReader, "SubStoreID", 0L).ToString() + "$$"
                               + GetFieldValue(sqlReader, "MACAddress", string.Empty) + "$$"
                               + GetFieldValue(sqlReader, "IPAddress", string.Empty),
                    MACAddress = GetFieldValue(sqlReader, "MACAddress", string.Empty),
                    IPAddress = GetFieldValue(sqlReader, "IPAddress", string.Empty)
                };
                mappedPosDetail.Add(_mappedPosDetail);
            }
            return mappedPosDetail;
        }

        public int GetAnyRunningServiceByPosIdAccess(long imixposdetailId)
        {
            int Count = 0;

            DBParameters.Clear();
            AddParameter("@iMixPOSDetailID", imixposdetailId);
            Count = (int)ExecuteScalar(DAOConstant.USP_GETANYRUNNINGSERVICEBYPOSID);
            return Count;
        }
        public List<GetServiceStatus> GetServiceStatusAccess(string MachineName, string ServiceName)
        {
            DBParameters.Clear();
            AddParameter("@MachineName", MachineName);
            AddParameter("@ServiceName", ServiceName);
            this.OpenConnection();
            IDataReader sqlReader = ExecuteReader(DAOConstant.USP_GETSERVICESTATUS);
            List<GetServiceStatus> getService = PoppulateServiceStatus(sqlReader);
            sqlReader.Close();
            return getService;
        }
        private List<GetServiceStatus> PoppulateServiceStatus(IDataReader sqlReader)
        {
            List<GetServiceStatus> getServiceStatus = new List<GetServiceStatus>();

            try
            {
                while (sqlReader.Read())
                {
                    GetServiceStatus _getServiceStatus = new GetServiceStatus()
                    {
                        ServiceId = GetFieldValue(sqlReader, "DG_Service_Id", 0),
                        Runlevel = GetFieldValue(sqlReader, "Runlevel", 0),
                        SubStoreID = GetFieldValue(sqlReader, "SubStoreID", 0L),
                        iMixPosId = GetFieldValue(sqlReader, "iMixPOSDetailID", 0L)

                    };

                    getServiceStatus.Add(_getServiceStatus);
                }
            }
            catch (Exception ex )
            {

                throw ;
            }


            return getServiceStatus;
        }

        public int UpdateServiceRunLevelAccess(int _serviceId, int RunLevel, bool isService)
        {
            DBParameters.Clear();
            AddParameter("@ServiceId", _serviceId);
            AddParameter("@RunLevel", RunLevel);
            AddParameter("@isService", isService);
            int Count = (int)ExecuteNonQuery(DAOConstant.USP_RUNLEVELSERVICE);
            return Count;
        }

        public void StartStopSystemlAccess(string mac, string myIP, string SystemName, int Type)
        {
            DBParameters.Clear();
            AddParameter("@mac", mac);
            AddParameter("@myIP", myIP);
            AddParameter("@SystemName", SystemName);
            AddParameter("@Type", Type);
            ExecuteNonQuery(DAOConstant.USP_STARTSTOPSYSTEM);
        }

        public void setPrintServer(string mac, string SystemName, bool isStop)
        {
            DBParameters.Clear();
            AddParameter("@MacAddress", mac);
            AddParameter("@SystemName", SystemName);
            AddParameter("@IsStop", isStop);
            ExecuteNonQuery(DAOConstant.SETPRINTSERVER);
        }
        public int DeletePosDetailBasedonID(Int64 iMixPOSDetailID)
        {
            DBParameters.Clear();
            AddParameter("@ParamImixPOSDetailID", iMixPOSDetailID);
            int Count = (int)ExecuteNonQuery(DAOConstant.USP_DEL_POSDETAILBASEDONID);
            return Count;
        }
        public int ValidatePosDetails(ImixPOSDetail imixposdetail)
        {
            DBParameters.Clear();
            AddParameter("@SystemName", imixposdetail.SystemName);
            AddParameter("@IPAddress", imixposdetail.IPAddress);
            AddParameter("@MacAddress", imixposdetail.MacAddress);
            int Count = (int)ExecuteScalar(DAOConstant.USP_VALIDATEPOSDETAILS);
            return Count;
        }
    }
}