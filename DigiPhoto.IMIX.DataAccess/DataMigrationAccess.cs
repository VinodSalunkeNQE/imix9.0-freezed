﻿using DigiPhoto.IMIX.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace DigiPhoto.IMIX.DataAccess
{
    public class DataMigrationAccess : BaseDataAccess
    {
        #region Constructor
        public DataMigrationAccess(BaseDataAccess baseaccess)
            : base(baseaccess)
        {

        }

        public DataMigrationAccess()
        {

        }
        #endregion

        #region Public Methods
        public string ExecuteDataMigration(string _databasename, string backUpPath)
        {
            DBParameters.Clear();
            AddParameter("@DataBaseName", _databasename);
            AddParameter("@BackupPath", backUpPath);
            var outVal = ExecuteScalar("ExecuteDataMigration");
            string msg = string.Empty;
            if (outVal != null)
            {
                msg = outVal.ToString();
            }
            return msg;
        }
        #endregion
    }
}
