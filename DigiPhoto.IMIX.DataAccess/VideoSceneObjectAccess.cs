﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DigiPhoto.IMIX.Model;
using System.Data;

namespace DigiPhoto.IMIX.DataAccess
{
    public class VideoSceneAccess : BaseDataAccess
    {

        #region Constructor
        public VideoSceneAccess(BaseDataAccess baseaccess)
            : base(baseaccess)
        {

        }

        public VideoSceneAccess()
        {

        }
        #endregion

        public bool SaveVideoSceneObject(string VideoObjectIds)
        {
            DBParameters.Clear();
            AddParameter("@paramVideoObjectIDs", VideoObjectIds);
            //AddParameter("@paramSceneId", videoSceneObject.SceneId);
            ExecuteNonQuery("usp_SaveVideoSceneObject");
            return true;
        }
        public List<VideoSceneObject> GetVideoSceneObjects(VideoSceneObject videoSceneObject)
        {
            DBParameters.Clear();
            AddParameter("@paramSceneID", videoSceneObject.SceneId);
            List<VideoSceneObject> videoObjects = new List<VideoSceneObject>();
            IDataReader sqlReader = ExecuteReader("usp_GetVideoObjects");
            videoObjects = PopulateVideoObjects(sqlReader);
            return videoObjects;
        }
        private List<VideoSceneObject> PopulateVideoObjects(IDataReader sqlReader)
        {
            List<VideoSceneObject> videoObjects = new List<VideoSceneObject>();
            while (sqlReader.Read())
            {
                VideoSceneObject videoObject = new VideoSceneObject();
                videoObject.VideoObject_Pkey = GetFieldValue(sqlReader, "VideoObject_Pkey", 0);
                videoObject.VideoObjectId = GetFieldValue(sqlReader, "VideoObjectId", string.Empty);
                //videoObject.SceneId = GetFieldValue(sqlReader, "SceneId", 0);
                videoObjects.Add(videoObject);
            }
            return videoObjects;
        }
        public List<VideoScene> GetVideoSceneByCriteria(int locationId, int sceneId)
        {
            List<VideoScene> lstVideoScene = new List<VideoScene>();
            DBParameters.Clear();
            AddParameter("@paramLocationId", locationId);
            AddParameter("@paramSceneId", sceneId);
            IDataReader sqlReader = ExecuteReader("dbo.usp_GET_SceneByCriteria");
            lstVideoScene = populateVideoSceneDetails(sqlReader);
            return lstVideoScene;
        }
        private List<VideoScene> populateVideoSceneDetails(IDataReader reader)
        {
            VideoScene objVideoScene;
            List<VideoScene> lstVideoScene = new List<VideoScene>();
            while (reader.Read())
            {
                objVideoScene = new VideoScene();
                objVideoScene.SceneId = GetFieldValue(reader, "SceneId", 0);
                objVideoScene.LocationId = GetFieldValue(reader, "LocationId", 0);
                objVideoScene.Name = GetFieldValue(reader, "Name", string.Empty);
                objVideoScene.ScenePath = GetFieldValue(reader, "ScenePath", string.Empty);
                objVideoScene.VideoLength = GetFieldValue(reader, "VideoLength", 0);
                objVideoScene.Settings = GetFieldValue(reader, "settings", string.Empty);
                objVideoScene.IsActive = GetFieldValue(reader, "IsActive", false);
                objVideoScene.IsMixerScene = GetFieldValue(reader, "IsMixerScene", false);
                objVideoScene.IsActiveForAdvanceProcessing = GetFieldValue(reader, "IsActiveForAdvanceProcessing", false);
                objVideoScene.CG_ConfigID = GetFieldValue(reader, "CG_ConfigId", 0);
                objVideoScene.IsVerticalVideo = GetFieldValue(reader, "isVerticalVideo", false);
                lstVideoScene.Add(objVideoScene);
            }
            return lstVideoScene;
        }

        //Save and update new scene in database
        public bool SaveVideoScene(VideoSceneViewModel VideoScene)
        {
            DBParameters.Clear();
            AddParameter("@paramSceneId", VideoScene.VideoScene.SceneId);
            AddParameter("@paramSceneName", VideoScene.VideoScene.Name);
            AddParameter("@paramScenePath", VideoScene.VideoScene.ScenePath);
            AddParameter("@paramLocationId", VideoScene.VideoScene.LocationId);
            AddParameter("@paramStreamId", VideoScene.VideoSceneObject.VideoObject_Pkey);
            AddParameter("@paramChromaPath", VideoScene.VideoObjectFileMapping.ChromaPath);
            AddParameter("@paramRouteFilePath", VideoScene.VideoObjectFileMapping.RoutePath);
            AddParameter("@paramSettings", VideoScene.VideoScene.Settings);
            AddParameter("@paramVideoLength", VideoScene.VideoScene.VideoLength);
            AddParameter("@paramGuestVideoObject", VideoScene.VideoSceneObject.GuestVideoObject);
            AddParameter("@paramIsActive", VideoScene.VideoScene.IsActive);
            AddParameter("@paramIsActiveForAdvanceProcessing", VideoScene.VideoScene.IsActiveForAdvanceProcessing);
            AddParameter("@paramStreamAudioEnabled", VideoScene.VideoObjectFileMapping.StreamAudioEnabled);
            AddParameter("@paramIsVerticalVideo", VideoScene.VideoScene.IsVerticalVideo);
            ExecuteNonQuery("usp_SaveSceneDetails");
            return true;
        }
        public bool SaveMixerScene(VideoSceneViewModel VideoScene)
        {
            DBParameters.Clear();
            AddParameter("@paramSceneId", VideoScene.VideoScene.SceneId);
            AddParameter("@paramSceneName", VideoScene.VideoScene.Name);
            AddParameter("@paramScenePath", VideoScene.VideoScene.ScenePath);
            AddParameter("@paramLocationId", VideoScene.VideoScene.LocationId);
            AddParameter("@paramGuestObjectID", VideoScene.VideoSceneObject.VideoObjectId);
            AddParameter("@paramChromaPath", VideoScene.VideoObjectFileMapping.ChromaPath);
            //AddParameter("@paramRouteFilePath", VideoScene.VideoObjectFileMapping.RoutePath);
            AddParameter("@paramSettings", VideoScene.VideoScene.Settings);
            AddParameter("@paramVideoLength", VideoScene.VideoScene.VideoLength);
            AddParameter("@paramGuestVideoObject", VideoScene.VideoSceneObject.GuestVideoObject);
            AddParameter("@paramIsActive", VideoScene.VideoScene.IsActive);
            AddParameter("@paramIsActiveForAdvanceProcessing", VideoScene.VideoScene.IsActiveForAdvanceProcessing);
            AddParameter("@paramIsMixerScene", VideoScene.VideoScene.IsMixerScene);
            AddParameter("@paramCg_ConfigId", VideoScene.VideoScene.CG_ConfigID);
            AddParameter("@paramIsVerticalVideo", VideoScene.VideoScene.IsVerticalVideo);
            ExecuteNonQuery("usp_SaveMixerScene");
            return true;
        }
        public List<VideoSceneObject> GuestVideoObjectBySceneID(int sceneId)
        {
            List<VideoSceneObject> lstVideoSceneObject = new List<VideoSceneObject>();
            DBParameters.Clear();
            AddParameter("@paramSceneId", sceneId);
            IDataReader sqlReader = ExecuteReader("dbo.usp_GET_GuestVideoSceneObject");
            lstVideoSceneObject = populateVideoSceneObject(sqlReader);
            return lstVideoSceneObject;
        }

        private List<VideoSceneObject> populateVideoSceneObject(IDataReader reader)
        {
            List<VideoSceneObject> lstVideoSceneObject = new List<VideoSceneObject>();
            VideoSceneObject objVideoScene;
            VideoObjectFileMapping objFileMapping;
            while (reader.Read())
            {
                objVideoScene = new VideoSceneObject();
                objFileMapping = new VideoObjectFileMapping();
                string enableStream = string.Empty;
                objVideoScene.VideoObject_Pkey = GetFieldValue(reader, "VideoObject_Pkey", 0);
                objVideoScene.VideoObjectId = GetFieldValue(reader, "VideoObjectId", string.Empty);
                objVideoScene.GuestVideoObject = GetFieldValue(reader, "IsGuestVideoObject", false);
                objFileMapping.ChromaPath = GetFieldValue(reader, "ChromaPath", string.Empty);
                objFileMapping.RoutePath = GetFieldValue(reader, "RoutePath", string.Empty);
                objFileMapping.StreamAudioEnabled = GetFieldValue(reader, "StreamAudioEnabled", string.Empty);
                enableStream = GetFieldValue(reader, "StreamAudioEnabled", string.Empty);
                objVideoScene.streamAudioEnabled = enableStream == "true" ? true : false;
                objVideoScene.ObjectFileMapping = objFileMapping;
                lstVideoSceneObject.Add(objVideoScene);
            }
            return lstVideoSceneObject;
        }
        public List<VideoScene> GetVideoScene(int sceneId, int? substoreId)
        {
            DBParameters.Clear();
            AddParameter("@paramSceneId", sceneId);
            AddParameter("@paramSubstoreId", substoreId);
            List<VideoScene> videoObjects = new List<VideoScene>();
            IDataReader sqlReader = ExecuteReader("usp_GetVideoScene");
            videoObjects = PopulateVideoScenes(sqlReader);
            return videoObjects;
        }

        private List<VideoScene> PopulateVideoScenes(IDataReader sqlReader)
        {
            List<VideoScene> videoObjects = new List<VideoScene>();
            while (sqlReader.Read())
            {
                VideoScene videoObject = new VideoScene();
                videoObject.Name = GetFieldValue(sqlReader, "Name", string.Empty);
                videoObject.ScenePath = GetFieldValue(sqlReader, "ScenePath", string.Empty);
                videoObject.IsActive = GetFieldValue(sqlReader, "IsActive", false);
                videoObject.IsActiveForAdvanceProcessingStatus = GetFieldValue(sqlReader, "IsActiveForAdvanceProcessing", string.Empty);
                videoObject.SceneId = GetFieldValue(sqlReader, "SceneId", 0);
                videoObject.LocationId = GetFieldValue(sqlReader, "LocationId", 0);
                videoObject.Settings = GetFieldValue(sqlReader, "settings", string.Empty);
                videoObject.LocationName = GetFieldValue(sqlReader, "DG_Location_Name", string.Empty);
                videoObject.VideoLength = GetFieldValue(sqlReader, "VideoLength", 0);
                videoObject.IsVerticalVideo = GetFieldValue(sqlReader, "IsVerticalVideo", false);
                videoObjects.Add(videoObject);
            }
            return videoObjects;
        }

        public string DeleteVideoSceneDetails(int? Sceneid)
        {
            DBParameters.Clear();
            // AddParameter("@paramSceneName", Sceneid, ParameterDirection.Output);
            AddOutParameterString("@paramSceneName", SqlDbType.NVarChar, 350);
            AddParameter("@paramSceneId", Sceneid);
            ExecuteNonQuery("usp_DeleteVideoSceneDetails");
            string SceneName = (string)GetOutParameterValue("@paramSceneName");
            return SceneName;
        }

        public VideoSceneViewModel GetVideoSceneToEdit(int? Sceneid)
        {
            VideoSceneViewModel VideoSceneViewobj = new VideoSceneViewModel();
            DBParameters.Clear();
            AddParameter("@paramSceneID", Sceneid);
            IDataReader sqlReader = ExecuteReader("usp_GetVideoSceneToEdit");
            VideoSceneViewobj = PopulateRecordsToEdit(sqlReader);
            return VideoSceneViewobj;
        }

        private VideoSceneViewModel PopulateRecordsToEdit(IDataReader sqlReader)
        {
            VideoSceneViewModel obj = new VideoSceneViewModel();
            VideoScene vScene = null;
            VideoSceneObject vSeceneObject = null;
            List<VideoSceneObject> lstSeceneObject = new List<VideoSceneObject>();
            while (sqlReader.Read())
            {
                vScene = new VideoScene();
                vSeceneObject = new VideoSceneObject();
                vScene.Name = GetFieldValue(sqlReader, "Name", string.Empty);
                vScene.ScenePath = GetFieldValue(sqlReader, "ScenePath", string.Empty);
                vScene.LocationId = GetFieldValue(sqlReader, "LocationId", 0);
                vScene.VideoLength = GetFieldValue(sqlReader, "VideoLength", 0);
                vScene.IsActive = GetFieldValue(sqlReader, "IsActive", false);
                vScene.IsActiveForAdvanceProcessing = GetFieldValue(sqlReader, "IsActiveForAdvanceProcessing", false);
                vScene.Settings = GetFieldValue(sqlReader, "settings", string.Empty);
                vSeceneObject.VideoObject_Pkey = GetFieldValue(sqlReader, "VideoObject_Pkey", 0);
                vSeceneObject.VideoObjectId = GetFieldValue(sqlReader, "VideoObjectId", string.Empty);
                vSeceneObject.GuestVideoObject = GetFieldValue(sqlReader, "ISGuestVideoObject", false);
                vScene.IsVerticalVideo = GetFieldValue(sqlReader, "IsVerticalVideo", false);

                lstSeceneObject.Add(vSeceneObject);

            }
            obj.VideoScene = vScene;
            obj.ListVideoSceneObject = lstSeceneObject;
            return obj;

        }

        public VideoSceneViewModel GetVideoSceneEditToResource(int? Sceneid)
        {
            VideoSceneViewModel VideoSceneViewobj = new VideoSceneViewModel();
            DBParameters.Clear();
            AddParameter("@paramSceneID", Sceneid);
            IDataReader sqlReader = ExecuteReader("usp_GetVideoSceneEditForResource");
            VideoSceneViewobj = PopulateRecordsEditToResource(sqlReader);
            return VideoSceneViewobj;
        }

        private VideoSceneViewModel PopulateRecordsEditToResource(IDataReader sqlReader)
        {
            VideoSceneViewModel objVideoObjectFileMapping = new VideoSceneViewModel();
            List<VideoObjectFileMapping> vObjectFileMapping = new List<VideoObjectFileMapping>();
            List<VideoSceneObject> vSceneObject = new List<VideoSceneObject>();
            while (sqlReader.Read())
            {
                VideoObjectFileMapping _vObjectFileMapping = new VideoObjectFileMapping();
                VideoSceneObject _vSceneObject = new VideoSceneObject();
                _vObjectFileMapping.ValueTypeId = GetFieldValue(sqlReader, "ValueTypeId", 0);
                _vObjectFileMapping.ResourcePath = GetFieldValue(sqlReader, "ResourcePath", string.Empty);
                _vSceneObject.VideoObject_Pkey = GetFieldValue(sqlReader, "VideoObject_Pkey", 0);
                _vSceneObject.VideoObjectId = GetFieldValue(sqlReader, "VideoObjectId", string.Empty);
                vObjectFileMapping.Add(_vObjectFileMapping);
                vSceneObject.Add(_vSceneObject);
            }
            objVideoObjectFileMapping.ListVideoSceneObject = vSceneObject;
            objVideoObjectFileMapping.ListVideoObjectFileMapping = vObjectFileMapping;
            return objVideoObjectFileMapping;
        }

        public VideoObjectFileMapping GetobjectVideoDetails(int vidObjectID)
        {
            DBParameters.Clear();
            AddParameter("@paramVidObjectId", vidObjectID);
            VideoObjectFileMapping videoObjects = new VideoObjectFileMapping();
            IDataReader sqlReader = ExecuteReader("usp_Get_VideoObjectDetails");
            videoObjects = MapVideoObjectDetails(sqlReader);
            return videoObjects;
        }
        private VideoObjectFileMapping MapVideoObjectDetails(IDataReader reader)
        {
            VideoObjectFileMapping objVFM = new VideoObjectFileMapping();
            while (reader.Read())
            {
                objVFM.ChromaPath = GetFieldValue(reader, "ChromaPath", string.Empty);
                objVFM.RoutePath = GetFieldValue(reader, "RoutePath", string.Empty);
                objVFM.StreamAudioEnabled = GetFieldValue(reader, "StreamAudioEnabled", string.Empty);
            }
            return objVFM;
        }

        public List<Watchersetting> GetSettingWatcher(int LocationId)
        {
            List<Watchersetting> Watchersetting = new List<Watchersetting>();
            DBParameters.Clear();
            AddParameter("@paramLocationid", LocationId);
            IDataReader sqlReader = ExecuteReader("usp_getSetting");
            Watchersetting = PopulateRecordsWatcherSetting(sqlReader);
            return Watchersetting;
        }

        private List<Watchersetting> PopulateRecordsWatcherSetting(IDataReader sqlReader)
        {
            List<Watchersetting> _Watchersetting = new List<Watchersetting>();
            while (sqlReader.Read())
            {
                Watchersetting Watchersetting = new Watchersetting();
                Watchersetting.SceneName = GetFieldValue(sqlReader, "Name", string.Empty);
                Watchersetting.IsMixerScene = GetFieldValue(sqlReader, "IsMixerScene", false);
                Watchersetting.Isstream = GetFieldValue(sqlReader, "IsLiveStream", false);

                _Watchersetting.Add(Watchersetting);
            }
            return _Watchersetting;
        }

        #region Added by Ajay
        public bool SaveVideoSceneSettings(VideoScene VideoScene)
        {
            DBParameters.Clear();
            AddParameter("@paramSceneId", VideoScene.SceneId);
            AddParameter("@paramSceneName", VideoScene.Name);

            AddParameter("@paramSettings", VideoScene.Settings);
            ExecuteNonQuery("Usp_UpadateScene");
            return true;
        }
        #endregion


        public string checkIsActiveForAdvance(int locationId)
        {
            DBParameters.Clear();
            AddOutParameterString("@paramName", SqlDbType.NVarChar, 350);
            AddParameter("@paramLocationId", locationId);
            ExecuteNonQuery("usp_chkIsActiveForAdvance");
            string SceneName = string.Empty;
            if (GetOutParameterValue("@paramName") != DBNull.Value)
                SceneName = (string)GetOutParameterValue("@paramName");
            return SceneName;
        }

        public string checkIsActiveForVideoProcessing(int locationId,int SceneId)
        {

            DBParameters.Clear();
			AddParameter("@paramSceneId", SceneId);
            AddParameter("@paramLocationId", locationId);
            AddOutParameterString("@paramName", SqlDbType.NVarChar, 350);

            ExecuteNonQuery("usp_chkIsActiveForVideo");
            string SceneName = string.Empty;
            if (GetOutParameterValue("@paramName") != DBNull.Value)
                SceneName = (string)GetOutParameterValue("@paramName");
            return SceneName;
        }

        public bool UpdateSceneGraphicProfile(int sceneID, int configID)
        {
            DBParameters.Clear();
            AddParameter("@paramSceneID", sceneID);
            AddParameter("@paramConfigID", configID);
            if (Convert.ToInt32(ExecuteNonQuery("usp_Update_SceneGraphicProfile")) > 0)
                return true;
            else
                return false;
        }
    
        public bool CheckProfileName(string name)
        {
            DBParameters.Clear();
            AddParameter("@paramProfileName", name);
           int result=(Convert.ToInt32(ExecuteScalar("usp_CheckProfileName")));
           if (result>0)
             return true;
           else
             return false;
        }

        ///added by latika 
        public bool SaveUpdLiveVideoQuickSettings(QuickSettings objQuickAeet)
        {
            DBParameters.Clear();
            AddParameter("@FullScreen", objQuickAeet.FullScreen);
            AddParameter("@Video", objQuickAeet.Video);
            AddParameter("@AspectRatio", objQuickAeet.AspectRatio);
            AddParameter("@Audio", objQuickAeet.Audio);
            AddParameter("@Volume", objQuickAeet.Volume);
            AddParameter("@LocationID", objQuickAeet.LocationID);
            AddParameter("@SubStorID", objQuickAeet.SubStorID);
            AddParameter("@ScenID", objQuickAeet.ScenID);
            ExecuteNonQuery("Usp_SaveUpdLiveVideoQuickSettings");
            return true;
        }
        public List<QuickSettings> GetLiveVideoQuickSettings(QuickSettings ObjQst)
        {
            List<QuickSettings> lQuickSettings= new List<QuickSettings>();
            DBParameters.Clear();
            AddParameter("@ScenID", ObjQst.ScenID);
            AddParameter("@LocationID", ObjQst.LocationID);
            AddParameter("@SubstoreID", ObjQst.SubStorID);
            IDataReader sqlReader = ExecuteReader("Usp_GetLiveVideoQuickSettings");
            lQuickSettings = PopulateQuickSettings(sqlReader);
            return lQuickSettings;
        }
        private List<QuickSettings> PopulateQuickSettings(IDataReader sqlReader)
        {
            List<QuickSettings> _Watchersetting = new List<QuickSettings>();
            while (sqlReader.Read())
            {
                QuickSettings OBJQuickSettings = new QuickSettings();
                OBJQuickSettings.FullScreen = GetFieldValue(sqlReader, "FullScreen", false);
                OBJQuickSettings.Video = GetFieldValue(sqlReader, "Video", false);
                OBJQuickSettings.AspectRatio = GetFieldValue(sqlReader, "AspectRatio", false);
                OBJQuickSettings.Audio = GetFieldValue(sqlReader, "Audio", false);
                OBJQuickSettings.Volume = GetFieldValue(sqlReader, "Volume", 0);
              

                _Watchersetting.Add(OBJQuickSettings);
            }
            return _Watchersetting;
        }
        ///END BY LATIKA
    }
}
