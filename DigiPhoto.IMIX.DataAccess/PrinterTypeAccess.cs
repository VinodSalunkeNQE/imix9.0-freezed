﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DigiPhoto.IMIX.Model;
using System.Data;
using DigiPhoto.Utility.Repository.ValueType;
namespace DigiPhoto.IMIX.DataAccess
{
    public class PrinterTypeAccess:BaseDataAccess
    {
         #region Constructor
        public PrinterTypeAccess(BaseDataAccess baseaccess)
            : base(baseaccess)
        {

        }

        public PrinterTypeAccess()
        {

        }
        #endregion
        public bool SaveUpdatePrinterType(PrinterTypeInfo printInfo)
        {
            DBParameters.Clear();
            AddParameter("@PrinterTypeID", printInfo.PrinterTypeID);
            AddParameter("@PrinterType", printInfo.PrinterType);
            AddParameter("@ProductTypeID", printInfo.ProductTypeID);
            AddParameter("@IsActive", printInfo.IsActive);

            ExecuteNonQuery("SaveUpdatePrinterType");
            return true;
        }

        public List<PrinterTypeInfo> GetPrinterTypeList(int PrinterTypeId)
        {
            DBParameters.Clear();
            AddParameter("@PrinterTypeID", PrinterTypeId);
            IDataReader sqlReader = ExecuteReader("GetPrinterTypeInfo");
            List<PrinterTypeInfo> configValueList = PopulatPrinterTypeList(sqlReader);
            sqlReader.Close();
            return configValueList;
        }

        private List<PrinterTypeInfo> PopulatPrinterTypeList(IDataReader sqlReader)
        {
            List<PrinterTypeInfo> printerTypeList = new List<PrinterTypeInfo>();
            while (sqlReader.Read())
            {
                PrinterTypeInfo printerTypeInfo = new PrinterTypeInfo();
                printerTypeInfo.PrinterTypeID = GetFieldValue(sqlReader, "PrinterTypeID", 0);
                printerTypeInfo.PrinterType = GetFieldValue(sqlReader, "PrinterType", string.Empty);
                printerTypeInfo.ProductTypeID = GetFieldValue(sqlReader, "ProductTypeID", 0);
                printerTypeInfo.ProductName = GetFieldValue(sqlReader, "ProductName", string.Empty);
                printerTypeInfo.IsActive = GetFieldValue(sqlReader, "IsActive", false);

                printerTypeList.Add(printerTypeInfo);
            }
            return printerTypeList;
        }
        public bool DeletePrinterType(int PrinterTypeId)
        {
            DBParameters.Clear();
            AddParameter("@PrinterTypeID", PrinterTypeId);
            ExecuteNonQuery("DeletePrinterTypeInfo");
            return true;
        }

        public bool SaveOrActivateNewPrinter(string PrinterName, int SubstoreId, bool active)
        {
            DBParameters.Clear();
            AddParameter("@PrinterName", PrinterName);
            AddParameter("@SubstoreId", SubstoreId);
            AddParameter("@IsPrinterActive", active);
            ExecuteNonQuery("SaveOrActivateNewPrinter").ToInt32();
            return true;
        }

        public bool RemapNewPrinter(string PrinterName, int SubstoreId, bool active)
        {
            DBParameters.Clear();
            AddParameter("@AssociatedPrintersName", PrinterName);
            AddParameter("@SubstoreId", SubstoreId);
            AddParameter("@IsPrinterActive", active);
            ExecuteNonQuery("usp_RemapPrinter").ToInt32();
            return true;
        }



        public bool DeleteAssociatedPrinters(int SubstoreId)
        {
            DBParameters.Clear();
            AddParameter("@SubstoreId", SubstoreId);
            ExecuteNonQuery("DeleteAssociatedPrinters").ToInt32();
            return true;
        }
    }
}
