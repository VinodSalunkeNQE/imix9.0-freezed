﻿using DigiPhoto.IMIX.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace DigiPhoto.IMIX.DataAccess
{
    public class OrderAccess:BaseDataAccess
    {
        #region Constructor
        public OrderAccess(BaseDataAccess baseaccess):base(baseaccess)
        {

        }
        public OrderAccess()
        {

        }
        #endregion
        #region Public Functions
        public string GetOrderInvoiceNumber(int OrderId)
        {
            DBParameters.Clear();
            AddParameter("@OrderId", OrderId);
            return ExecuteScalar("GetOrderInvoiceNumber").ToString();
        }

        /// <summary>
        /// Added By KC to get fresh cancel order invoice number 
        /// </summary>
        /// <param name="OrderInvoiceNumber">Invoice number that generated at the time of order</param>
        /// <returns>String value as Invoice Number</returns>
        public string GetCancelOrderInvoiceNumber(string OrderInvoiceNumber)
        {
            DBParameters.Clear();
            AddParameter("@InvoiceNumber", OrderInvoiceNumber);
            return ExecuteScalar("GETCancelInvoiceNumber").ToString();
        }
        /// <summary>
        /// //adde by latika for Update QR Code for presold photos
        /// </summary>
        /// <param name="QRCode"></param>
        /// <param name="OrderID"></param>
        /// <returns></returns>
        public int UpdOnlineQRCodeForPhotoPresold(string ClaimCode, string PhotoIDs)
        {
            DBParameters.Clear();
            AddParameter("@ClaimCode", ClaimCode);
            AddParameter("@PhotoIDs", PhotoIDs);
            return Convert.ToInt32(ExecuteScalar("usp_UPD_OnlineQRCodeForPhotoPresold"));
        }
        /// <summary>
        /// end
        /// </summary>
        /// <param name="OrderId"></param>
        /// <returns></returns>
        public OrderReceiptReprintInfo GetOrderDetailForReceipt(int OrderId)
        {
            DBParameters.Clear();
            AddParameter("@OrderId", OrderId);
            IDataReader sqlReader = ExecuteReader("GetOrderDetailForReceipt");
            OrderReceiptReprintInfo order = PopulateOrder(sqlReader);
            sqlReader.Close();
            return order;

        }

        public OrderReceiptReprintInfo PopulateOrder(IDataReader sqlReader)
        {
            OrderReceiptReprintInfo order = null;
            while (sqlReader.Read())
            {
                order = new OrderReceiptReprintInfo();
                order.OrderId = GetFieldValue(sqlReader, "OrderId", 0);
                order.OrderNumber = GetFieldValue(sqlReader, "OrderNumber", string.Empty);
                order.PaymentMode = GetFieldValue(sqlReader, "PaymentMode", 0);
                order.PaymentDetail = GetFieldValue(sqlReader, "PaymentDetail", string.Empty);
                order.NetCost = (double)GetFieldValue(sqlReader, "NetCost", 0.0M);
                order.TotalCost = (double)GetFieldValue(sqlReader, "TotalCost", 0.0M);
                order.CurrencySymbol = GetFieldValue(sqlReader, "CurrencySymbol", string.Empty);
                order.DiscountTotal = GetFieldValue(sqlReader, "DiscountTotal", 0.0);
                order.PhotoIds = GetFieldValue(sqlReader, "PhotoIds", string.Empty);
                order.QRCode = GetFieldValue(sqlReader, "QRCode", string.Empty);
               ///change made by latika for Re-pring receipt QR Code is not coming. 
    }
            return order;
        }
        #endregion

        /// ////////////////////created by latika for fix bug multiple files should generate by each QR Code ////////////
        /// </summary>
        /// <param name="OrderNumber"></param>
        /// <returns></returns>
        public List<QRcodes> GETQRcodesbyOrderNumber(string OrderNumber)
        {
            DBParameters.Clear();
            AddParameter("@OrderNumber", OrderNumber);
            IDataReader sqlReader = ExecuteReader("usp_GETQRcodesbyOrderNumber");
            List<QRcodes> order = PopulateQROrder(sqlReader);
            sqlReader.Close();
            return order;

        }
        public List<QRcodes> PopulateQROrder(IDataReader sqlReader)
        {
            List<QRcodes> Orders_DetailsList = new List<QRcodes>();
            while (sqlReader.Read())
            {
                QRcodes Orders_Details = new QRcodes()
                {
                    DG_Orders_Number = GetFieldValue(sqlReader, "OrderNumber", string.Empty),
                    QRCode = GetFieldValue(sqlReader, "QRCode", string.Empty),
                };
                Orders_DetailsList.Add(Orders_Details);
            }
            return Orders_DetailsList;
        }
        ///////////////////////////end//////////////////
    }
}