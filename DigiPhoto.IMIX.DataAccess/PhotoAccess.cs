﻿using DigiPhoto.IMIX.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using DigiPhoto.Utility.Repository.ValueType;
using System.Security.Cryptography;
using DigiPhoto.IMIX.Model.Base;

namespace DigiPhoto.IMIX.DataAccess
{
    public class PhotoAccess : BaseDataAccess
    {
        #region Constructor
        public PhotoAccess(BaseDataAccess baseaccess)
            : base(baseaccess)
        {

        }

        public PhotoAccess()
        {

        }
        #endregion

        #region Public Methods
        public List<PhotoDetail> GetPhotoDetailsByPhotoIds(string PhotoIds)
        {
            DBParameters.Clear();
            AddParameter("@PhotoIds", PhotoIds);
            IDataReader sqlReader = ExecuteReader("GetFileNameFromPhotoId");
            List<PhotoDetail> deviceList = GetPhotoDetailsByPhotoIds(sqlReader);
            sqlReader.Close();
            return deviceList;
        }
        /// <summary>
        /// By KCB on 05 JUN 2019 for fetching photo metadata from database
        /// </summary>
        /// <param name="PhotoIds">List of photos provided by Facial API</param>
        /// <returns></returns>
        public List<PhotoInfo> GetPhotoDetailsByPhotoIds(List<string> PhotoIds)
        {
            string photoids = String.Join(",", PhotoIds);
            photoids = photoids.Replace("id_", "");
            DBParameters.Clear();
            AddParameter("@PhotoIds", photoids);
            IDataReader sqlReader = ExecuteReader("GetPhotosfromIds");
            List<PhotoInfo> deviceList = MapAllPhotosforSearchInfo(sqlReader);
            sqlReader.Close();
            return deviceList;
        }
        /// <summary>
        /// By KCB on 05 JUN 2019 for converting fetched photo metadata from datareader to photoinfo
        /// </summary>
        /// <param name="sqlReader">IDataReader</param>
        /// <returns></returns>
        private List<PhotoInfo> MapAllPhotosforSearchInfo(IDataReader sqlReader)
        {
            List<PhotoInfo> PhotosList = new List<PhotoInfo>();
            while (sqlReader.Read())
            {
                PhotoInfo Photos = new PhotoInfo()
                {
                    DG_Photos_pkey = GetFieldValue(sqlReader, "DG_Photos_pkey", 0),
                    DG_Photos_FileName = GetFieldValue(sqlReader, "DG_Photos_FileName", string.Empty),
                    DG_Photos_CreatedOn = GetFieldValue(sqlReader, "DG_Photos_CreatedOn", DateTime.Now),
                    DG_Photos_RFID = GetFieldValue(sqlReader, "DG_Photos_RFID", string.Empty),
                    DG_Photos_UserID = GetFieldValue(sqlReader, "DG_Photos_UserID", 0),
                    DG_Photos_Background = GetFieldValue(sqlReader, "DG_Photos_Background", string.Empty),
                    DG_Photos_Frame = GetFieldValue(sqlReader, "DG_Photos_Frame", string.Empty),
                    DG_Photos_DateTime = GetFieldValue(sqlReader, "DG_Photos_DateTime", DateTime.Now),
                    DG_Photos_Layering = GetFieldValue(sqlReader, "DG_Photos_Layering", string.Empty),
                    DG_Photos_Effects = GetFieldValue(sqlReader, "DG_Photos_Effects", string.Empty),
                    DG_Photos_IsCroped = GetFieldValue(sqlReader, "DG_Photos_IsCroped", false),
                    DG_Photos_IsRedEye = GetFieldValue(sqlReader, "DG_Photos_IsRedEye", false),
                    DG_Photos_IsGreen = GetFieldValue(sqlReader, "DG_Photos_IsGreen", false),
                    DG_Photos_MetaData = GetFieldValue(sqlReader, "DG_Photos_MetaData", string.Empty),
                    DG_Photos_Sizes = GetFieldValue(sqlReader, "DG_Photos_Sizes", string.Empty),
                    DG_Photos_Archive = GetFieldValue(sqlReader, "DG_Photos_Archive", false),
                    DG_Location_Id = GetFieldValue(sqlReader, "DG_Location_Id", 0),
                    DG_SubStoreId = GetFieldValue(sqlReader, "DG_SubStoreId", 0),
                    DG_IsCodeType = GetFieldValue(sqlReader, "DG_IsCodeType", false),
                    DateTaken = GetFieldValue(sqlReader, "DateTaken", DateTime.Now),
                    RfidScanType = GetFieldValue(sqlReader, "RfidScanType", 0),
                    HotFolderPath = GetFieldValue(sqlReader, "HotFolderPath", string.Empty),
                    DG_MediaType = GetFieldValue(sqlReader, "DG_MediaType", 0),
                    DG_VideoLength = GetFieldValue(sqlReader, "DG_VideoLength", 0L),
                    UploadStatus = GetFieldValue(sqlReader, "UploadStatus", 0),
                };

                PhotosList.Add(Photos);
            }
            return PhotosList;
        }
        public List<PhotoDetail> GetImagesInfoForEditing(int SubStoreId, string PosName)
        {
            DBParameters.Clear();
            AddParameter("@SubstoreId", SubStoreId);
            AddParameter("@PosName", PosName);
            IDataReader sqlReader = ExecuteReader("GetImagesInfoForEditing");
            List<PhotoDetail> lstPhotoDetails = PopulateImagesInfoForEditing(sqlReader);
            sqlReader.Close();
            return lstPhotoDetails;
        }
        public bool UpdateImageProcessedStatus(int PhotoId, bool ProcessedStatus)
        {
            DBParameters.Clear();
            AddParameter("@PhotoId", PhotoId);
            AddParameter("@Status", ProcessedStatus);
            int EffectedRows = Convert.ToInt32(ExecuteNonQuery("UpdateImageProcessedStatus"));
            if (EffectedRows > 0)
                return true;
            else
                return false;
        }
        public List<PhotoDetail> GetFilesForAutoVideoProcessing(int substoreID, string PosName)
        {
            DBParameters.Clear();
            AddParameter("@SubstoreId", substoreID);
            AddParameter("@PosName", PosName);
            IDataReader sqlReader = ExecuteReader("GetFilesForAutoVideoProcessing");
            List<PhotoDetail> lstPhotoDetails = PopulateFilesInfoForAutoVidProcessing(sqlReader);
            sqlReader.Close();
            return lstPhotoDetails;
        }
        public bool UpdateVideoProcessedStatus(int PhotoId, bool ProcessedStatus)
        {
            DBParameters.Clear();
            AddParameter("@PhotoId", PhotoId);
            AddParameter("@Status", ProcessedStatus);
            int EffectedRows = Convert.ToInt32(ExecuteNonQuery("UpdateVideoProcessedStatus"));
            if (EffectedRows > 0)
                return true;
            else
                return false;
        }
        public bool UpdateImageProcessedStatus(int PhotoId, int ProcessedStatus)
        {
            DBParameters.Clear();
            AddParameter("@PhotoId", PhotoId);
            AddParameter("@Status", ProcessedStatus);
            int EffectedRows = ExecuteNonQuery("UpdateImageProcessedStatus").ToInt32();
            if (EffectedRows > 0)
                return true;
            else
                return false;
        }
        #endregion

        #region Private Methods
        private List<PhotoDetail> GetPhotoDetailsByPhotoIds(IDataReader sqlReader)
        {
            List<PhotoDetail> photoList = new List<PhotoDetail>();
            while (sqlReader.Read())
            {
                PhotoDetail photoDetail = new PhotoDetail();
                photoDetail.PhotoId = GetFieldValue(sqlReader, "DG_Photos_pkey", 0);
                photoDetail.FileName = GetFieldValue(sqlReader, "DG_Photos_FileName", string.Empty);
                photoDetail.DG_Photos_CreatedOn = GetFieldValue(sqlReader, "DG_Photos_CreatedOn", new DateTime());
                photoDetail.PhotoRFID = GetFieldValue(sqlReader, "DG_Photos_RFID", string.Empty);
                photoDetail.Layering = GetFieldValue(sqlReader, "DG_Photos_Layering", string.Empty);
                photoDetail.Effects = GetFieldValue(sqlReader, "DG_Photos_Effects", string.Empty);
                photoDetail.HotFolderPath = GetFieldValue(sqlReader, "HotFolderPath", string.Empty);
                photoDetail.MediaType = GetFieldValue(sqlReader, "DG_MediaType", 0);
                photoList.Add(photoDetail);
            }
            return photoList;
        }
        private List<PhotoDetail> PopulateImagesInfoForEditing(IDataReader sqlReader)
        {
            List<PhotoDetail> photoList = new List<PhotoDetail>();
            while (sqlReader.Read())
            {
                PhotoDetail photoDetail = new PhotoDetail();
                photoDetail.PhotoId = GetFieldValue(sqlReader, "DG_Photos_pkey", 0);
                photoDetail.FileName = GetFieldValue(sqlReader, "DG_Photos_FileName", string.Empty);
                photoDetail.DG_Photos_CreatedOn = GetFieldValue(sqlReader, "DG_Photos_CreatedOn", new DateTime());
                photoDetail.PhotoRFID = GetFieldValue(sqlReader, "DG_Photos_RFID", string.Empty);
                photoDetail.Layering = GetFieldValue(sqlReader, "DG_Photos_Layering", string.Empty);
                photoDetail.Effects = GetFieldValue(sqlReader, "DG_Photos_Effects", string.Empty);
                photoDetail.IsCropped = GetFieldValue(sqlReader, "DG_Photos_IsCroped", false);
                photoDetail.IsGreen = GetFieldValue(sqlReader, "DG_Photos_IsGreen", false);
                photoDetail.LocationId = GetFieldValue(sqlReader, "DG_Location_Id", 0);
                photoDetail.SubstoreId = GetFieldValue(sqlReader, "DG_SubStoreId", 0);
                photoDetail.SemiOrderProfileId = GetFieldValue(sqlReader, "SemiOrderProfileId", 0);
                photoDetail.IsGumRideShow = GetFieldValue(sqlReader, "IsGumRideShow", false);
                photoList.Add(photoDetail);
            }
            return photoList;
        }
        private List<PhotoDetail> PopulateFilesInfoForAutoVidProcessing(IDataReader sqlReader)
        {
            List<PhotoDetail> photoList = new List<PhotoDetail>();
            while (sqlReader.Read())
            {
                PhotoDetail photoDetail = new PhotoDetail();
                photoDetail.PhotoId = GetFieldValue(sqlReader, "DG_Photos_pkey", 0);
                photoDetail.FileName = GetFieldValue(sqlReader, "DG_Photos_FileName", string.Empty);
                photoDetail.DG_Photos_CreatedOn = GetFieldValue(sqlReader, "DG_Photos_CreatedOn", new DateTime());
                photoDetail.PhotoRFID = GetFieldValue(sqlReader, "DG_Photos_RFID", string.Empty);
                photoDetail.LocationId = GetFieldValue(sqlReader, "DG_Location_Id", 0);
                photoDetail.SubstoreId = GetFieldValue(sqlReader, "DG_SubStoreId", 0);
                photoDetail.PhotoGrapherId = GetFieldValue(sqlReader, "DG_Photos_UserID", 0);
                photoDetail.MediaType = GetFieldValue(sqlReader, "DG_MediaType", 0);
                photoDetail.VideoLength = GetFieldValue(sqlReader, "DG_VideoLength", 0L);
                // *** Code added by Anis for Magic Shot Kitted on 26th Nov 18 *** //
                photoDetail.SemiOrderProfileId = GetFieldValue(sqlReader, "SemiOrderProfileId", 0);
                // *** End Here  *** //
                photoList.Add(photoDetail);
            }
            return photoList;
        }
        public List<PhotoDetail> GetPhotoDetailsByPhotoIds(PhotoDetail photoDetailObj)
        {
            DBParameters.Clear();
            AddParameter("@PhotoIds", photoDetailObj.PhotoRFID);
            IDataReader sqlReader = ExecuteReader("GetFileNameFromPhotoId");
            List<PhotoDetail> deviceList = GetPhotoDetailsByPhotoIds(sqlReader, photoDetailObj);
            sqlReader.Close();
            return deviceList;
        }
        private List<PhotoDetail> GetPhotoDetailsByPhotoIds(IDataReader sqlReader, PhotoDetail photoDetailObj)
        {
            List<PhotoDetail> photoList = new List<PhotoDetail>();
            while (sqlReader.Read())
            {
                PhotoDetail photoDetail = new PhotoDetail();
                photoDetail.PhotoId = GetFieldValue(sqlReader, "DG_Photos_pkey", 0);
                photoDetail.DG_Photos_CreatedOn = GetFieldValue(sqlReader, "DG_Photos_CreatedOn", new DateTime());
                photoDetail.FileName = System.IO.Path.Combine(GetFieldValue(sqlReader, "HotFolderPath", string.Empty), photoDetail.DG_Photos_CreatedOn.ToString("yyyyMMdd"), GetFieldValue(sqlReader, "DG_Photos_FileName", string.Empty));
                photoDetail.DG_Photos_CreatedOn = GetFieldValue(sqlReader, "DG_Photos_CreatedOn", new DateTime());
                photoDetail.PhotoRFID = GetFieldValue(sqlReader, "DG_Photos_RFID", string.Empty);
                photoDetail.Layering = GetFieldValue(sqlReader, "DG_Photos_Layering", string.Empty);
                photoDetail.Effects = GetFieldValue(sqlReader, "DG_Photos_Effects", string.Empty);
                photoDetail.HotFolderPath = GetFieldValue(sqlReader, "HotFolderPath", string.Empty);
                photoDetail.IsEnabled = photoDetailObj.IsEnabled;
                photoList.Add(photoDetail);
            }
            return photoList;
        }
        #endregion

        public List<PhotoCaptureInfo> GetPhotoCapturedetails(Int32 PhotoId)
        {
            DBParameters.Clear();
            AddParameter("@PhotoId", SetNullIntegerValue(PhotoId));

            IDataReader sqlReader = ExecuteReader(DAOConstant.usp_SELPhotoDetailsById);
            List<PhotoCaptureInfo> photoList = MapPhotoCapture(sqlReader);
            sqlReader.Close();
            return photoList;
        }

        private List<PhotoCaptureInfo> MapPhotoCapture(IDataReader sqlReader)
        {
            List<PhotoCaptureInfo> ImageList = new List<PhotoCaptureInfo>();
            while (sqlReader.Read())
            {
                PhotoCaptureInfo User_Roles = new PhotoCaptureInfo()
                {
                    CaptureDate = GetFieldValue(sqlReader, "CaptureDate", DateTime.Now),
                    GroupName = GetFieldValue(sqlReader, "GroupName", string.Empty),
                    LocationName = GetFieldValue(sqlReader, "LocationName", string.Empty),
                    PhotoGrapherName = GetFieldValue(sqlReader, "PhotoGrapherName", string.Empty),
                    PhotoName = GetFieldValue(sqlReader, "PhotoName", string.Empty),
                    pkey = GetFieldValue(sqlReader, "pkey", 0),
                    LocationId = GetFieldValue(sqlReader, "LocationId", 0),
                    PhotoGrapherId = GetFieldValue(sqlReader, "PhotoGrapherID", 0),
                    RFIDNo = GetFieldValue(sqlReader, "RFIDNo", string.Empty),
                    SubstoreName = GetFieldValue(sqlReader, "SubstoreName", string.Empty),
                    SysDate = GetFieldValue(sqlReader, "SysDate", DateTime.Now),
                    CharacterId = GetFieldValue(sqlReader, "CharacterId", string.Empty),
                    MetaData = GetFieldValue(sqlReader, "MetaData", string.Empty),
                    UniqueIdentifier = GetFieldValue(sqlReader, "CardUniqueIdentifier", string.Empty),
                    TableName = GetFieldValue(sqlReader, "TableName", string.Empty),///changed by latika for table workflow
                    //GuestName = GetFieldValue(sqlReader, "GuestName", string.Empty),///changed by latika for table workflow
                    GuestName = (GetFieldValue(sqlReader, "GuestName", string.Empty).Length > 1) ? CryptorEngineAccess.Decrypt(GetFieldValue(sqlReader, "GuestName", string.Empty), true) : "",
                    EmailID = (GetFieldValue(sqlReader, "Email", string.Empty).Length > 1) ? CryptorEngineAccess.Decrypt(GetFieldValue(sqlReader, "Email", string.Empty), true) : "",
                    ContactNo = (GetFieldValue(sqlReader, "Contacts", string.Empty).Length > 1) ? CryptorEngineAccess.Decrypt(GetFieldValue(sqlReader, "Contacts", string.Empty), true) : "",
                    ///end
                };
                ImageList.Add(User_Roles);
            }
            return ImageList;
        }

        public long GetPhotosDisplayOrder(int batchCount)
        {
            try
            {
                DBParameters.Clear();
                AddParameter("@batchCount", batchCount);
                var maxCount = ExecuteScalar("usp_Get_MaxDisplayOrderNumber");
                return Convert.ToInt64(maxCount);
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        public List<ManualDisplayOrder> GetPhotosDisplayNumber(DataTable PhotoNumber)
        {
            try
            {
                DBParameters.Clear();
                AddParameter("@PhotoNumber", PhotoNumber);
                IDataReader sqlReader = ExecuteReader("GetManualDisplayOrder");
                List<ManualDisplayOrder> MDOList = ManualDisplayList(sqlReader);
                sqlReader.Close();
                return MDOList;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        private List<ManualDisplayOrder> ManualDisplayList(IDataReader sqlReader)
        {
            List<ManualDisplayOrder> MDOList = new List<ManualDisplayOrder>();
            while (sqlReader.Read())
            {
                ManualDisplayOrder MDO = new ManualDisplayOrder()
                {
                    PhotoNumber = GetFieldValue(sqlReader, "PhotoNumber", 0L),
                    FileName = GetFieldValue(sqlReader, "FileName", string.Empty),
                    DisplayOrder = GetFieldValue(sqlReader, "DisplayNumber", 0L)
                };
                MDOList.Add(MDO);
            }
            return MDOList;
        }
    }
    ///changed by latika for table workflow tablename,Name encrypt
    #region Class for Encryption/Decryption bin file

    public class CryptorEngineAccess
    {
        /// <summary>
        /// Encrypt a string using dual encryption method. Return a encrypted cipher Text
        /// </summary>
        /// <param name="toEncrypt">string to be encrypted</param>
        /// <param name="useHashing">use hashing? send to for extra secirity</param>
        /// <returns></returns>
        public static string Encrypt(string toEncrypt, bool useHashing)
        {
            byte[] keyArray;
            byte[] toEncryptArray = UTF8Encoding.UTF8.GetBytes(toEncrypt);

            // System.Configuration.AppSettingsReader settingsReader = new AppSettingsReader();
            // Get the key from config file
            // string key = (string)settingsReader.GetValue("SecurityKey", typeof(String));
            string key = "mykey";
            //System.Windows.Forms.MessageBox.Show(key);
            if (useHashing)
            {
                MD5CryptoServiceProvider hashmd5 = new MD5CryptoServiceProvider();
                keyArray = hashmd5.ComputeHash(UTF8Encoding.UTF8.GetBytes(key));
                hashmd5.Clear();
            }
            else
                keyArray = UTF8Encoding.UTF8.GetBytes(key);

            TripleDESCryptoServiceProvider tdes = new TripleDESCryptoServiceProvider();
            tdes.Key = keyArray;
            tdes.Mode = CipherMode.ECB;
            tdes.Padding = PaddingMode.PKCS7;

            ICryptoTransform cTransform = tdes.CreateEncryptor();
            byte[] resultArray = cTransform.TransformFinalBlock(toEncryptArray, 0, toEncryptArray.Length);
            tdes.Clear();
            return Convert.ToBase64String(resultArray, 0, resultArray.Length);
        }
        /// <summary>
        /// DeCrypt a string using dual encryption method. Return a DeCrypted clear string
        /// </summary>
        /// <param name="cipherString">encrypted string</param>
        /// <param name="useHashing">Did you use hashing to encrypt this data? pass true is yes</param>
        /// <returns></returns>
        public static string Decrypt(string cipherString, bool useHashing)
        {
            byte[] keyArray;
            byte[] toEncryptArray = Convert.FromBase64String(cipherString);

            // System.Configuration.AppSettingsReader settingsReader = new AppSettingsReader();
            //Get your key from config file to open the lock!
            //string key = (string)settingsReader.GetValue("SecurityKey", typeof(String));
            string key = "mykey";
            if (useHashing)
            {
                MD5CryptoServiceProvider hashmd5 = new MD5CryptoServiceProvider();
                keyArray = hashmd5.ComputeHash(UTF8Encoding.UTF8.GetBytes(key));
                hashmd5.Clear();
            }
            else
                keyArray = UTF8Encoding.UTF8.GetBytes(key);

            TripleDESCryptoServiceProvider tdes = new TripleDESCryptoServiceProvider();
            tdes.Key = keyArray;
            tdes.Mode = CipherMode.ECB;
            tdes.Padding = PaddingMode.PKCS7;

            ICryptoTransform cTransform = tdes.CreateDecryptor();
            byte[] resultArray = cTransform.TransformFinalBlock(toEncryptArray, 0, toEncryptArray.Length);

            tdes.Clear();
            return UTF8Encoding.UTF8.GetString(resultArray);
        }
    }
    #endregion
    /////end
}
