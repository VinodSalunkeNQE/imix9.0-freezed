﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Text;
using DigiPhoto.IMIX.Model;

namespace DigiPhoto.IMIX.DataAccess
{
    public class PanoramaConfigDao: BaseDataAccess
    {
        #region Constructor
        public PanoramaConfigDao(BaseDataAccess baseaccess): base(baseaccess)
        { }
        public PanoramaConfigDao()
        { }
        #endregion

        public ObservableCollection<PanoramaProperties> _thelist { get; set; }
        ObservableCollection<PanoramaProperties> PrinterDimensionsList = new ObservableCollection<PanoramaProperties>();
        public ObservableCollection<PanoramaProperties> TheList
        {
            get
            {
                return _thelist;
            }
            set
            {
                _thelist = value;
                this.NotifyPropertyChanged("TheList");

            }
        }
        public event PropertyChangedEventHandler PropertyChanged;
        private void NotifyPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        public ObservableCollection<PanoramaProperties> GetPrinterDimensions()
        {

            IDataReader sqlReader = ExecuteReader(DAOConstant.GetPanoramaProductDimension);
            ObservableCollection<PanoramaProperties> PanoramaDimensionsList = new ObservableCollection<PanoramaProperties>();
            //filter only Ispanorama true sps
            ObservableCollection<ProductTypeInfo> DimensionsList = MapProductTypePanoramaInfo(sqlReader);
            foreach (var item in DimensionsList)
            {
                if (item.IsPanorama)
                {
                    PrinterDimensionsList.Add(new PanoramaProperties { TheText = item.DG_Orders_ProductType_Name, TheValue = item.DG_Orders_ProductType_pkey });
                }
            }
            
            //PrinterDimensionsList.Add(new PanoramaProperties { TheText = "3x3", TheValue = 1 });
            //PrinterDimensionsList.Add(new PanoramaProperties { TheText = "4x6", TheValue = 2 });
            //PrinterDimensionsList.Add(new PanoramaProperties { TheText = "5x7", TheValue = 3 });
            //PrinterDimensionsList.Add(new PanoramaProperties { TheText = "6x8", TheValue = 1 });
            //PrinterDimensionsList.Add(new PanoramaProperties { TheText = "6x14", TheValue = 4 });
            //PrinterDimensionsList.Add(new PanoramaProperties { TheText = "6x20", TheValue = 3 });
            //PrinterDimensionsList.Add(new PanoramaProperties { TheText = "8x10", TheValue = 4 });
            //PrinterDimensionsList.Add(new PanoramaProperties { TheText = "8x18", TheValue = 5 });
            //PrinterDimensionsList.Add(new PanoramaProperties { TheText = "8x24", TheValue = 6 });
            //PrinterDimensionsList.Add(new PanoramaProperties { TheText = "8x26", TheValue = 7 });
            //return PrinterDimensionsList;


            return PrinterDimensionsList;
        }


        private ObservableCollection<ProductTypeInfo> MapProductTypePanoramaInfo(IDataReader sqlReader)
        {
            ObservableCollection<ProductTypeInfo> ProductTypeList = new ObservableCollection<ProductTypeInfo>();
            while (sqlReader.Read())
            {
                ProductTypeInfo ProductType = new ProductTypeInfo()
                {
                    DG_Orders_ProductType_pkey = GetFieldValue(sqlReader, "DG_Orders_ProductType_pkey", 0),
                    DG_Orders_ProductType_Name = GetFieldValue(sqlReader, "DG_Orders_ProductType_Name", string.Empty),
                    DG_Orders_ProductType_Desc = GetFieldValue(sqlReader, "DG_Orders_ProductType_Desc", string.Empty),
                    DG_Orders_ProductType_IsBundled = GetFieldValue(sqlReader, "DG_Orders_ProductType_IsBundled", false),
                    DG_Orders_ProductType_DiscountApplied = GetFieldValue(sqlReader, "DG_Orders_ProductType_DiscountApplied", false),
                    DG_Orders_ProductType_Image = GetFieldValue(sqlReader, "DG_Orders_ProductType_Image", string.Empty),
                    DG_IsPackage = GetFieldValue(sqlReader, "DG_IsPackage", false),
                    DG_MaxQuantity = GetFieldValue(sqlReader, "DG_MaxQuantity", 0),
                    DG_Orders_ProductType_Active = GetFieldValue(sqlReader, "DG_Orders_ProductType_Active", false),
                    DG_IsActive = GetFieldValue(sqlReader, "DG_IsActive", false),
                    DG_IsAccessory = GetFieldValue(sqlReader, "DG_IsAccessory", false),
                    DG_Orders_ProductNumber = GetFieldValueIntNull(sqlReader, "DG_Orders_ProductNumber", 0),
                    IsPrintType = GetFieldValue(sqlReader, "IsPrintType", 0),
                    DG_IsWaterMarkIncluded = GetFieldValue(sqlReader, "DG_IsWaterMarked", false),
                    DG_SubStore_pkey = GetFieldValue(sqlReader, "DG_SubStore_pkey", 0),
                    IsPanorama = GetFieldValue(sqlReader, "IsPanorama", false),
                };

                ProductTypeList.Add(ProductType);
            }
            return ProductTypeList;
        }

        private List<DeviceInfo> GetActiveBorders(IDataReader sqlReader)
        {
            List<DeviceInfo> deviceList = new List<DeviceInfo>();
            while (sqlReader.Read())
            {
                DeviceInfo device = new DeviceInfo();
                device.DeviceId = GetFieldValue(sqlReader, "DeviceId", 0);
                device.Name = GetFieldValue(sqlReader, "DeviceName", string.Empty);
                deviceList.Add(device);
            }
            return deviceList;
        }

        public bool IsProductPanoramic(int dg_printerqueue_pkey)
        {
            DBParameters.Clear();
            AddParameter("@dg_printerqueue_pkey", dg_printerqueue_pkey);
           int countPanPrd =Convert.ToInt16(ExecuteScalar("[dbo].[usp_DG_IsPanoramaProduct]"));
            if (countPanPrd!=0)
            {
                return true;
            }
            return false;
        }
        public bool IsProductPanorama_ProductId(int ProductId)
        {
            DBParameters.Clear();
            AddParameter("@ProductId", ProductId);
            int returnProductId = Convert.ToInt16(ExecuteScalar("usp_DG_IsPanoramaProduct_ProductId"));
            if (returnProductId != 0)
            {
                return true;
            }
            return false;
        }
    }
}
