﻿using DigiPhoto.IMIX.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace DigiPhoto.IMIX.DataAccess
{
   public  class ReportAccess:BaseDataAccess
    {
       #region Constructor
        public ReportAccess(BaseDataAccess baseaccess)
            : base(baseaccess)
        {

        }

        public ReportAccess()
        {

        }
        #endregion
        public DataSet GetPrintSummaryDetail(DateTime from, DateTime To, int SubStoreID)
       {
           DBParameters.Clear();
           AddParameter("@ParamFromDate", from);
           AddParameter("@ParamToDate", To);
           AddParameter("@ParamSubStoreID", SubStoreID); 
           return ExecuteDataSet(DAOConstant.USP_RPT_GETPRINTSUMMARYDETAIL);
           
       }

       private List<PrintSummaryDetail> PopulatePrintSummaryDetail(IDataReader sqlReader)
       {
           List<PrintSummaryDetail> printSummaryList = new List<PrintSummaryDetail>();
           while (sqlReader.Read())
           {
               PrintSummaryDetail printSummary = new PrintSummaryDetail();
               printSummary.SaleType = GetFieldValue(sqlReader, "SaleType", string.Empty);
               printSummary.PhotoNumbers = GetFieldValue(sqlReader, "PhotoNumbers", string.Empty);
               printSummaryList.Add(printSummary);
           }
           return printSummaryList;
       }

       public List<PaymentSummaryInfo> GetPaymentSummary(DateTime FromDt, DateTime ToDt, string storeName,int substoreID)
       {
           DBParameters.Clear();
           AddParameter("@from", FromDt);
           AddParameter("@to", ToDt);
           AddParameter("@subStoreID", substoreID);
           AddParameter("@storeName", storeName);
           IDataReader sqlReader = ExecuteReader("uspGetPaymentSummary");
           List<PaymentSummaryInfo> PaymentSummaryInfoList = PopulatePaymentSummaryInfoDetails(sqlReader);
           sqlReader.Close();
           return PaymentSummaryInfoList;
       }

       private List<PaymentSummaryInfo> PopulatePaymentSummaryInfoDetails(IDataReader sqlReader)
       {
           List<PaymentSummaryInfo> PaymentSummaryList = new List<PaymentSummaryInfo>();
           while (sqlReader.Read())
           {
               PaymentSummaryInfo PaymentSummaryinfo = new PaymentSummaryInfo();
               PaymentSummaryinfo.StoreId = GetFieldValue(sqlReader, "StoreId", int.MinValue);
               PaymentSummaryinfo.StoreName = GetFieldValue(sqlReader, "StoreName", string.Empty);
               PaymentSummaryinfo.StoreCardSale = Convert.ToDecimal(sqlReader["StoreCardSale"]);
               PaymentSummaryinfo.StoreCashSale = Convert.ToDecimal(sqlReader["StoreCashSale"]);
               PaymentSummaryinfo.StoreDiscountSale = Convert.ToDecimal(sqlReader["StoreDiscountSale"]);
               PaymentSummaryinfo.StoreRoomChargeSale = Convert.ToDecimal(sqlReader["StoreRoomChargeSale"]);
               PaymentSummaryinfo.FC = Convert.ToDecimal(sqlReader["FC"]);
               PaymentSummaryinfo.StoreId = GetFieldValue(sqlReader, "SubStoreId", int.MinValue);
               PaymentSummaryinfo.SubStoreName = GetFieldValue(sqlReader, "SubStoreName", string.Empty);
               PaymentSummaryinfo.SubStoreCardSale = Convert.ToDecimal(sqlReader["SubStoreCardSale"]);
               PaymentSummaryinfo.SubStoreCashSale = Convert.ToDecimal(sqlReader["SubStoreCashSale"]);
               PaymentSummaryinfo.SubStoreDiscountSale = Convert.ToDecimal(sqlReader["SubStoreDiscountSale"]);
               PaymentSummaryinfo.SubStoreRoomChargeSale = Convert.ToDecimal(sqlReader["SubStoreRoomChargeSale"]);
               PaymentSummaryinfo.SubStoreFC = Convert.ToDecimal(sqlReader["SubStoreFC"]);
               PaymentSummaryinfo.Productcode = GetFieldValue(sqlReader, "Productcode", string.Empty);
               PaymentSummaryinfo.TotalCost = Convert.ToDecimal(sqlReader["TotalCost"]);
               PaymentSummaryinfo.Quantity = GetFieldValue(sqlReader, "Quantity", int.MinValue);
               PaymentSummaryinfo.ProductUnitPrice = Convert.ToDecimal(sqlReader["ProductUnitPrice"]);
               PaymentSummaryinfo.Tax = Convert.ToDecimal(sqlReader["Tax"]);
               PaymentSummaryinfo.DiscountAmount = Convert.ToDecimal(sqlReader["DiscountAmount"]);
               PaymentSummaryinfo.NetPrice = Convert.ToDecimal(sqlReader["NetPrice"]);
               PaymentSummaryinfo.FromDate = Convert.ToDateTime(sqlReader["FromDate"]);
               PaymentSummaryinfo.ToDate = Convert.ToDateTime(sqlReader["ToDate"]);
               PaymentSummaryinfo.ConSubStoreName = Convert.ToString(sqlReader["ConSubStoreName"]);
               PaymentSummaryinfo.SubStoreCashTransactionCount = Convert.ToInt32(sqlReader["SubStoreCashTransactionCount"]);
               PaymentSummaryinfo.SubStoreCCTransactionCount = Convert.ToInt32(sqlReader["SubStoreCCTransactionCount"]);
               PaymentSummaryinfo.TotalSubStoreFCTransactionCount = Convert.ToInt32(sqlReader["TotalSubStoreFCTransactionCount"]);
               PaymentSummaryinfo.SubStoreDiscountedVoucherTransactionCount = Convert.ToInt32(sqlReader["SubStoreDiscountedVoucherTransactionCount"]);
               PaymentSummaryinfo.SubstoreRoomChargeTransactionCount = Convert.ToInt32(sqlReader["SubstoreRoomChargeTransactionCount"]);
               PaymentSummaryinfo.TotalStoreSale = Convert.ToDecimal(sqlReader["TotalStoreSale"]);
               PaymentSummaryinfo.TotalStoreSaleTransactionCount = Convert.ToInt32(sqlReader["TotalStoreSaleTransactionCount"]);
               PaymentSummaryinfo.StoreVoidSale = Convert.ToDecimal(sqlReader["StoreVoidSale"]);
               PaymentSummaryinfo.StoreVoidSaleTransactionCount = Convert.ToInt32(sqlReader["StoreVoidSaleTransactionCount"]);
               PaymentSummaryinfo.StoreCashSaleTransactionsCount = Convert.ToInt32(sqlReader["StoreCashSaleTransactionsCount"]);
               PaymentSummaryinfo.StoreCardSaleTransactionsCount = Convert.ToInt32(sqlReader["StoreCardSaleTransactionsCount"]);
               PaymentSummaryinfo.StoreDiscountSaleTransactionsCount = Convert.ToInt32(sqlReader["StoreDiscountSaleTransactionsCount"]);
               PaymentSummaryinfo.StoreRoomChargeSaleTransactionsCount = Convert.ToInt32(sqlReader["StoreRoomChargeSaleTransactionsCount"]);
               PaymentSummaryinfo.FCStoreTransactionCount = Convert.ToInt32(sqlReader["FCStoreTransactionCount"]);             
               PaymentSummaryinfo.SubStoreVoidSaleTransactionCount = Convert.ToInt32(sqlReader["SubStoreVoidSaleTransactionCount"]);
               PaymentSummaryinfo.SubStoreVoidSale = Convert.ToDecimal(sqlReader["SubStoreVoidSale"]);
               PaymentSummaryinfo.KVLStoreTransactionCount = Convert.ToInt32(sqlReader["KVLStoreTransactionCount"]);
               PaymentSummaryinfo.KVLStoreTransactionAmount = Convert.ToDecimal(sqlReader["KVLStoreTransactionAmount"]);
               PaymentSummaryinfo.SubStoreKVLTransactionCount = Convert.ToInt32(sqlReader["SubStoreKVLTransactionCount"]);
               PaymentSummaryinfo.SubStoreKVLTransactionAmount = Convert.ToDecimal(sqlReader["SubStoreKVLTransactionAmount"]);
               PaymentSummaryinfo.TotalSubStoreSaleTransactionCount = Convert.ToInt32(sqlReader["TotalSubStoreSaleTransactionCount"]);

               PaymentSummaryinfo.StoreCardSaleVoid = Convert.ToDecimal(sqlReader["StoreCardSaleVoid"]);
               PaymentSummaryinfo.StoreCashSaleVoid = Convert.ToDecimal(sqlReader["StoreCashSaleVoid"]);
               PaymentSummaryinfo.StoreDiscountSaleVoid = Convert.ToDecimal(sqlReader["StoreDiscountSaleVoid"]);
               PaymentSummaryinfo.StoreRoomChargeSaleVoid = Convert.ToDecimal(sqlReader["StoreRoomChargeSaleVoid"]);
               PaymentSummaryinfo.FCVoid = Convert.ToDecimal(sqlReader["FCVoid"]);
               PaymentSummaryinfo.KVLStoreTransactionAmountVoid = Convert.ToDecimal(sqlReader["KVLStoreTransactionAmountVoid"]);
               PaymentSummaryinfo.StoreCashSaleTransactionsCountVoid = Convert.ToInt32(sqlReader["StoreCashSaleTransactionsCountVoid"]);
               PaymentSummaryinfo.StoreCardSaleTransactionsCountVoid = Convert.ToInt32(sqlReader["StoreCardSaleTransactionsCountVoid"]);
               PaymentSummaryinfo.StoreDiscountSaleTransactionsCountVoid = Convert.ToInt32(sqlReader["StoreDiscountSaleTransactionsCountVoid"]);
               PaymentSummaryinfo.StoreRoomChargeSaleTransactionsCountVoid = Convert.ToInt32(sqlReader["StoreRoomChargeSaleTransactionsCountVoid"]);
               PaymentSummaryinfo.FCStoreTransactionCountVoid = Convert.ToInt32(sqlReader["FCStoreTransactionCountVoid"]);
               PaymentSummaryinfo.KVLStoreTransactionCountVoid = Convert.ToInt32(sqlReader["KVLStoreTransactionCountVoid"]);
               PaymentSummaryinfo.SubStoreCardSaleVoid = Convert.ToDecimal(sqlReader["SubStoreCardSaleVoid"]);
               PaymentSummaryinfo.SubStoreCashSaleVoid = Convert.ToDecimal(sqlReader["SubStoreCashSaleVoid"]);
               PaymentSummaryinfo.SubStoreDiscountSaleVoid = Convert.ToDecimal(sqlReader["SubStoreDiscountSaleVoid"]);
               PaymentSummaryinfo.SubStoreRoomChargeSaleVoid = Convert.ToDecimal(sqlReader["SubStoreRoomChargeSaleVoid"]);
               PaymentSummaryinfo.SubStoreFCVoid = Convert.ToDecimal(sqlReader["SubStoreFCVoid"]);
               PaymentSummaryinfo.SubStoreKVLTransactionAmountVoid = Convert.ToDecimal(sqlReader["SubStoreKVLTransactionAmountVoid"]);
               PaymentSummaryinfo.SubStoreCashTransactionCountVoid = Convert.ToInt32(sqlReader["SubStoreCashTransactionCountVoid"]);
               PaymentSummaryinfo.SubStoreCCTransactionCountVoid = Convert.ToInt32(sqlReader["SubStoreCCTransactionCountVoid"]);
               PaymentSummaryinfo.TotalSubStoreFCTransactionCountVoid = Convert.ToInt32(sqlReader["TotalSubStoreFCTransactionCountVoid"]);
               PaymentSummaryinfo.SubStoreDiscountedVoucherTransactionCountVoid = Convert.ToInt32(sqlReader["SubStoreDiscountedVoucherTransactionCountVoid"]);
               PaymentSummaryinfo.SubstoreRoomChargeTransactionCountVoid = Convert.ToInt32(sqlReader["SubstoreRoomChargeTransactionCountVoid"]);
               PaymentSummaryinfo.SubStoreKVLTransactionCountVoid = Convert.ToInt32(sqlReader["SubStoreKVLTransactionCountVoid"]);

                //Monika ...Field add Start
                PaymentSummaryinfo.TotalZeroCount = Convert.ToInt32(sqlReader["TotalZeroCount"]);
                
                //Monika ...Field add End
                PaymentSummaryList.Add(PaymentSummaryinfo);
           }
           return PaymentSummaryList;
       }
    }
}
