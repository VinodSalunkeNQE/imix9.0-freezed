﻿using DigiPhoto.IMIX.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace DigiPhoto.IMIX.DataAccess
{
    public class ManageDeviceAccess : BaseDataAccess
    {
        #region Constructor
        public ManageDeviceAccess(BaseDataAccess baseaccess)
            : base(baseaccess)
        {

        }

        public ManageDeviceAccess()
        {

        }
        #endregion
        #region Manage Device
        public bool SaveDevice(DeviceInfo device)
        {
            DBParameters.Clear();
            AddParameter("@ParamName", device.Name);
            AddParameter("@ParamSerialNo", device.SerialNo);
            AddParameter("@ParamBDA", device.BDA);
            AddParameter("@ParamDeviceType", device.DeviceTypeId);
            AddParameter("@ParamCreatedBy", device.CreatedBy);
            AddParameter("@ParamCreatedDate", device.CreatedDate);
            AddParameter("@ParamIsActive", device.IsActive);
            AddParameter("@ParamDeviceId", device.DeviceId);
            ExecuteNonQuery("usp_UPDANDINS_Device");
            return true;
        }

        public List<DeviceInfo>  GetDeviceList(long DeviceId)
        {
            DBParameters.Clear();
            AddParameter("@DeviceId", DeviceId);
            IDataReader sqlReader = ExecuteReader("GetDevice");
            List<DeviceInfo> deviceList = PopulateDeviceList(sqlReader);
            sqlReader.Close();
            return deviceList;
        }

        private List<DeviceInfo> PopulateDeviceList(IDataReader sqlReader)
        {
            List<DeviceInfo> deviceList = new List<DeviceInfo>();
            //Dictionary<int, string> listDic = GetDeviceTypes();
            while (sqlReader.Read())
            {
                DeviceInfo device = new DeviceInfo();
                device.DeviceId = GetFieldValue(sqlReader, "DeviceId", 0);
                device.Name = GetFieldValue(sqlReader, "Name", string.Empty);
                device.SerialNo = GetFieldValue(sqlReader, "SerialNo", string.Empty);
                device.BDA = GetFieldValue(sqlReader, "BDA", string.Empty);
                device.DeviceTypeId = GetFieldValue(sqlReader, "DeviceTypeId", 0);
                device.CreatedDate = GetFieldValue(sqlReader, "CreatedDate", DateTime.Now);
                device.CreatedBy = GetFieldValue(sqlReader, "CreatedBy", 0);
                device.IsActive = GetFieldValue(sqlReader, "IsActive", false);
                device.DeviceTypeName = GetFieldValue(sqlReader, "DeviceTypeName", string.Empty);
                deviceList.Add(device);
            }
            return deviceList;
        }
        public bool DeleteDevice(long DeviceId)
        {
            DBParameters.Clear();
            AddParameter("@ParamDeviceId", DeviceId);
            ExecuteNonQuery("usp_DEL_Device");
            return true;
        }

        /*
        public Dictionary<int, string> GetDeviceTypes()
        {
            try
            {
                Dictionary<int, string> objDic = new Dictionary<int, string>();
                objDic.Add(0, "--Select--");
                objDic.Add((int)DeviceTypes.Baracoda, DeviceTypes.Baracoda.ToString());
                objDic.Add((int)DeviceTypes.QrCode, DeviceTypes.QrCode.ToString());
                return objDic;
            }
            catch
            {
                return null;
            }
        }
        
        public bool IsDeviceExists(string NameOrSerialNo)
        {
            try
            {
                using (EntityConnection connection = new EntityConnection(connectionstring))
                {
                    DigiphotoEntities context = new DigiphotoEntities(connection);
                    var itm = context.Device.Where(o => o.Name == NameOrSerialNo || o.SerialNo == NameOrSerialNo).FirstOrDefault();
                    if (itm == null)
                        return false;
                    else
                        return true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        */
        #endregion Manage Device

        #region Associate Device

        public List<CameraDeviceAssociationInfo> GetCameraDeviceList(long cameraId)
        {
            DBParameters.Clear();
            AddParameter("@CameraId", cameraId);
            IDataReader sqlReader = ExecuteReader("GetCameraDevice");
            List<CameraDeviceAssociationInfo> deviceList = PopulateCameraDeviceList(sqlReader);
            sqlReader.Close();
            return deviceList;
        }

        private List<CameraDeviceAssociationInfo> PopulateCameraDeviceList(IDataReader sqlReader)
        {
            List<CameraDeviceAssociationInfo> deviceList = new List<CameraDeviceAssociationInfo>();
            //Dictionary<int, string> listDic = GetDeviceTypes();
            while (sqlReader.Read())
            {
                CameraDeviceAssociationInfo device = new CameraDeviceAssociationInfo();
                device.DeviceId = GetFieldValue(sqlReader, "DeviceId", 0);
                device.CameraId = GetFieldValue(sqlReader, "CameraId", 0);
                deviceList.Add(device);
            }
            return deviceList;
        }

        public List<DeviceInfo> GetPhotoGrapherDeviceList(int photographerId)
        {
            DBParameters.Clear();
            AddParameter("@PhotographerId", photographerId);
            IDataReader sqlReader = ExecuteReader("GetPhotoGrapherWiseDevices");
            List<DeviceInfo> deviceList = PopulatePhotographerDeviceList(sqlReader);
            sqlReader.Close();
            return deviceList;
        }

        private List<DeviceInfo> PopulatePhotographerDeviceList(IDataReader sqlReader)
        {
            List<DeviceInfo> deviceList = new List<DeviceInfo>();
            while (sqlReader.Read())
            {
                DeviceInfo device = new DeviceInfo();
                device.DeviceId = GetFieldValue(sqlReader, "DeviceId", 0);
                device.Name = GetFieldValue(sqlReader, "DeviceName", string.Empty);
                deviceList.Add(device);
            }
            return deviceList;
        }

        public bool SaveCameraDeviceAssociation(int CameraId, string DeviceIds)
        {
            DBParameters.Clear();
            AddParameter("@DeviceIds",DeviceIds);
            AddParameter("@CameraId", CameraId);
            ExecuteNonQuery("SaveCameraDeviceAssociation");
            return true;
        }
        public bool SaveCameraCharacterAssociation(int CameraId, int Camerapkey, int? CharacterIds)
        {
            DBParameters.Clear();
            AddParameter("@CharacterIds", CharacterIds == 0 ? null : CharacterIds);
            AddParameter("@CameraId", CameraId);
            AddParameter("@Camerapkey", Camerapkey);
            ExecuteNonQuery("SaveCameraCharacterAssociation");
            return true;
        }

        public bool DeleteCameraDeviceAssociation(int cameraId)
        {
            DBParameters.Clear();
            AddParameter("@CameraId", cameraId);
            ExecuteNonQuery("uspDeleteCameraDeviceAssociation");
            return true;
        }

        public bool IsDeviceAssociatedToOthers(int CameraId, string DeviceIds)
        {
            DBParameters.Clear();
            AddParameter("@DeviceIds", DeviceIds);
            AddParameter("@CameraId", CameraId);
            AddParameter("@CId", 0, ParameterDirection.Output);
            ExecuteNonQuery("CheckDeviceAssociation");
            if ((int)GetOutParameterValue("@CId") > 0)
                return true;
            else
                return false;
        }

        public int DeletCameraDeviceSyncDetailsAccess(int CameraId, string DeviceIds)
        {
            int result = 0;
            int deviceID = Convert.ToInt32(DeviceIds);
            DBParameters.Clear();
            AddParameter("@DeviceId", deviceID);
            AddParameter("@CameraId", CameraId);
            result = (int)ExecuteScalar("DeleteCameraDeviceSyncDetails");
            return result;
        }
        #endregion
    }
}
