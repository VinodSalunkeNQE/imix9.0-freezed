﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DigiPhoto.IMIX.DataAccess
{
    public class Constants
    {
        public enum SyncOrderStatus
        {
            NotSynced = 0,
            Synced = 1,
            Error = -1,
            Invalid = -2,
            UploadedDataOnCentralServerandCloudinary = 3,
            UploadedDataOnCentralServerButFailedonCloudinary = -3,
            ProcessedDataOnCentralServer = 4,
            FailedToProcessDataOnCentralServer = -4,
            FailedtoProcessonPartnerDB = -5,
        }
    }
}
