﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DigiPhoto.IMIX.Model;
using System.Data;
using DigiPhoto.Utility.Repository;
using System.Windows.Media;
using System.Windows;

namespace DigiPhoto.IMIX.DataAccess
{
    public class ConfigAccess : BaseDataAccess
    {
        #region Constructor
        public ConfigAccess(BaseDataAccess baseaccess)
            : base(baseaccess)
        {

        }

        public ConfigAccess()
        {

        }
        #endregion


        public bool SaveUpdateNewConfig(DataTable dt)
        {
            DBParameters.Clear();
            AddParameter("@ConfigTable", dt);
            ExecuteNonQuery("SaveUpdateNewConfig");
            return true;
        }

        public bool SaveUpdateNewConfigForSync(DataTable dt)
        {
            DBParameters.Clear();
            AddParameter("@ConfigTable", dt);
            ExecuteNonQuery("SaveUpdateNewConfigForSync");
            return true;
        }
        public bool SaveUpdateNewStoreConfig(DataTable dt)
        {
            DBParameters.Clear();
            AddParameter("@ConfigTable", dt);
            ExecuteNonQuery("SaveUpdateNewStoreConfig");
            return true;
        }

        public bool SaveUpdatePreviewDummyTag(iMIXStoreConfigurationInfo objImixStoreInfo)
        {
            DBParameters.Clear();
            AddParameter("@iMIXStoreConfigurationValueId", objImixStoreInfo.iMIXStoreConfigurationValueId);
            AddParameter("@ConfigurationValue", objImixStoreInfo.ConfigurationValue);
            AddParameter("@IMIXConfigurationMasterId", objImixStoreInfo.IMIXConfigurationMasterId);
            AddOutParameter("@chkIsExist", SqlDbType.Bit);
            ExecuteReader("SaveUpdatePreviewDummyTag");
            return Convert.ToBoolean(GetOutParameterValue("@chkIsExist"));
        }

        public bool DeletePreviewDummyTag(long iMIXStoreConfigurationValueId)
        {
            DBParameters.Clear();
            AddParameter("@iMIXStoreConfigurationValueId", iMIXStoreConfigurationValueId);

            ExecuteNonQuery("DeletePreviewDummyTag");
            return true;
        }


        //Used only from ConfigManager
        public List<iMIXConfigurationInfo> GetConfigurations(int subStoreId)
        {
            DBParameters.Clear();
            AddParameter("@SubstoreId", subStoreId);
            IDataReader sqlReader = ExecuteReader("GetNewConfigValues");
            List<iMIXConfigurationInfo> configValueList = PopulateConfigValueList(sqlReader);
            sqlReader.Close();
            return configValueList;
        }

        public string GetHotFolderPath(int SubStoreId)
        {
            DBParameters.Clear();
            AddParameter("@SubStoreID", SubStoreId);
            string hotFolderPath = ExecuteScalar("DG_GetConfigData").ToString();
            return hotFolderPath;
        }

        public bool SaveUpdateConfigLocation(DataTable dt)//List<iMixConfigurationLocationInfo> iMixConfigLocInfo)
        {
            DBParameters.Clear();
            AddParameter("@ConfigLocTable", dt);
            ExecuteNonQuery("SaveUpdateConfigLocation");
            return true;
        }

        public bool DeleteConfigLocation(string ids, int substoreId, int locationId)
        {
            DBParameters.Clear();
            AddParameter("@Ids", ids);
            AddParameter("@SubstoreId", substoreId);
            AddParameter("@LocationId", locationId);
            ExecuteNonQuery("DeleteConfigLocation");
            return true;
        }

        public List<iMixConfigurationLocationInfo> GetConfigLocation(int LocationId, int SubstoreId)//List<iMixConfigurationLocationInfo> iMixConfigLocInfo)
        {
            DBParameters.Clear();
            AddParameter("@SubstoreId", SubstoreId);
            AddParameter("@LocationId", LocationId);
            IDataReader sqlReader = ExecuteReader("GetNewConfigLocationWise");
            List<iMixConfigurationLocationInfo> configLocValueList = PopulateConfigLocValueList(sqlReader);
            sqlReader.Close();
            return configLocValueList;
        }
        public List<iMixConfigurationLocationInfo> GetConfigBasedOnLocation(int LocationId)//List<iMixConfigurationLocationInfo> iMixConfigLocInfo)
        {
            DBParameters.Clear();

            AddParameter("@LocationId", LocationId);
            IDataReader sqlReader = ExecuteReader("GetConfigLocationWise");
            List<iMixConfigurationLocationInfo> configLocValueList = PopulateConfigLocValueList(sqlReader);
            sqlReader.Close();
            return configLocValueList;
        }

        public FolderStructureInfo GetFolderStructureInfo(int subStoreId)
        {
            DBParameters.Clear();
            AddParameter("@SubstoreId", subStoreId);
            IDataReader sqlReader = ExecuteReader("GetFolderStructurePaths");
            FolderStructureInfo folderStructureInfo = PopulateFolderStructureInfo(sqlReader);
            sqlReader.Close();
            return folderStructureInfo;
        }
        #region Private
        private FolderStructureInfo PopulateFolderStructureInfo(IDataReader sqlReader)
        {
            FolderStructureInfo folderStructureInfo = new FolderStructureInfo();
            while (sqlReader.Read())
            {
                folderStructureInfo.HotFolderPath = GetFieldValue(sqlReader, "HotFolderPath", string.Empty);
                folderStructureInfo.BorderPath = GetFieldValue(sqlReader, "BorderPath", string.Empty);
                folderStructureInfo.BackgroundPath = GetFieldValue(sqlReader, "BackgroundPath", string.Empty);
                folderStructureInfo.GraphicPath = GetFieldValue(sqlReader, "GraphicPath", string.Empty);
                folderStructureInfo.CroppedPath = GetFieldValue(sqlReader, "CroppedPath", string.Empty);
                folderStructureInfo.EditedImagePath = GetFieldValue(sqlReader, "EditedImagePath", string.Empty);
                // folderStructureInfo.GreenImagePath = GetFieldValue(sqlReader, "GreenImagePath", string.Empty);
                folderStructureInfo.ThumbnailsPath = GetFieldValue(sqlReader, "ThumbnailsPath", string.Empty);
                folderStructureInfo.BigThumbnailsPath = GetFieldValue(sqlReader, "BigThumbnailsPath", string.Empty);
                folderStructureInfo.PrintImagesPath = GetFieldValue(sqlReader, "PrintImagesPath", string.Empty);
            }
            return folderStructureInfo;
        }
        private List<iMIXConfigurationInfo> PopulateConfigValueList(IDataReader sqlReader)
        {
            List<iMIXConfigurationInfo> configValueList = new List<iMIXConfigurationInfo>();
            while (sqlReader.Read())
            {
                iMIXConfigurationInfo configValue = new iMIXConfigurationInfo();
                configValue.IMIXConfigurationValueId = GetFieldValue(sqlReader, "IMIXConfigurationValueId", 0L);
                configValue.IMIXConfigurationMasterId = GetFieldValue(sqlReader, "IMIXConfigurationMasterId", 0L);
                configValue.ConfigurationValue = GetFieldValue(sqlReader, "ConfigurationValue", string.Empty);
                configValue.SubstoreId = GetFieldValue(sqlReader, "SubstoreId", 0);
                configValueList.Add(configValue);
            }
            return configValueList;
        }
        private List<iMixConfigurationLocationInfo> PopulateConfigLocValueList(IDataReader sqlReader)
        {
            List<iMixConfigurationLocationInfo> configValueList = new List<iMixConfigurationLocationInfo>();
            while (sqlReader.Read())
            {
                iMixConfigurationLocationInfo configValue = new iMixConfigurationLocationInfo();
                configValue.IMIXConfigurationValueId = GetFieldValue(sqlReader, "ConfigurationValueId", 0L);
                configValue.IMIXConfigurationMasterId = GetFieldValue(sqlReader, "ConfigurationMasterId", 0L);
                configValue.ConfigurationValue = GetFieldValue(sqlReader, "ConfigurationValue", string.Empty);
                configValue.SubstoreId = GetFieldValue(sqlReader, "SubstoreId", 0);
                configValue.LocationId = GetFieldValue(sqlReader, "LocationId", 0);
                configValueList.Add(configValue);
            }
            return configValueList;
        }
        #endregion
        #region Sync Trigger Enable/Disable
        public bool UpdateTriggerStatus(DataTable dtValues)
        {
            DBParameters.Clear();
            AddParameter("@SyncTriggerStatusValues", dtValues);
            ExecuteNonQuery("UpdateTriggersStatus");
            return true;
        }

        public bool UpdateSyncPriority(List<SyncPriority> lstSyncPriority)
        {
            foreach (SyncPriority item in lstSyncPriority)
            {
                DBParameters.Clear();
                AddParameter("@ApplicationObjectId", item.ApplicationObjectID);
                AddParameter("@SyncPriority", item.OnlineSyncPriority);
                ExecuteNonQuery("UpdateSyncPriority");
            }
            return true;
        }
        public List<SyncTriggerStatusInfo> GetAllSyncTriggerTables()
        {
            DBParameters.Clear();
            IDataReader sqlReader = ExecuteReader("GetAllSyncTriggerTables");
            List<SyncTriggerStatusInfo> lstSyncTriggerTable = PopulateSyncTriggerTableList(sqlReader);
            sqlReader.Close();
            return lstSyncTriggerTable;
        }

        public List<SyncPriority> GetAllSyncPriority()
        {
            DBParameters.Clear();
            IDataReader sqlReader = ExecuteReader("GetAllSyncPriority");
            List<SyncPriority> lstSyncTriggerTable = PopulateSyncPriority(sqlReader);
            sqlReader.Close();
            return lstSyncTriggerTable;
        }

        private List<SyncPriority> PopulateSyncPriority(IDataReader sqlReader)
        {
            List<SyncPriority> syncPriorityTableList = new List<SyncPriority>();
            while (sqlReader.Read())
            {
                SyncPriority SynsTable = new SyncPriority();
                SynsTable.ApplicationObjectID = GetFieldValue(sqlReader, "ApplicationObjectID", 0L);
                SynsTable.ApplicationObjectName = GetFieldValue(sqlReader, "ApplicationObjectName", string.Empty);
                SynsTable.OnlineSyncPriority = GetFieldValue(sqlReader, "SyncPriority", 0);
                syncPriorityTableList.Add(SynsTable);
            }
            return syncPriorityTableList;
        }
        private List<SyncTriggerStatusInfo> PopulateSyncTriggerTableList(IDataReader sqlReader)
        {
            List<SyncTriggerStatusInfo> syncTableList = new List<SyncTriggerStatusInfo>();
            while (sqlReader.Read())
            {
                SyncTriggerStatusInfo SynsTable = new SyncTriggerStatusInfo();
                SynsTable.TableId = GetFieldValue(sqlReader, "TableId", 0L);
                SynsTable.TableName = GetFieldValue(sqlReader, "TableName", string.Empty);
                SynsTable.IsSyncTriggerEnable = GetFieldValue(sqlReader, "IsSyncTriggerEnable", true);
                syncTableList.Add(SynsTable);
            }
            return syncTableList;
        }
        #endregion Sync Trigger Enable/Disable

        #region Audio Template
        public bool SaveUpdateAudioTemplate(AudioTemplateInfo list)
        {
            DBParameters.Clear();
            AddParameter("@AudioTemplateId", list.AudioTemplateId);
            AddParameter("@Name", list.Name);
            AddParameter("@DisplayName", list.DisplayName);
            AddParameter("@Description", list.Description);
            AddParameter("@CreatedBy", list.CreatedBy);
            AddParameter("@CreatedOn", list.CreatedOn);
            AddParameter("@ModifiedBy", list.ModifiedBy);
            AddParameter("@ModifiedOn", list.ModifiedOn);
            AddParameter("@IsActive", list.IsActive);
            AddParameter("@AudioLength", list.AudioLength);

            ExecuteNonQuery("SaveUpdateAudioTemplate");
            return true;
        }

        public List<AudioTemplateInfo> GetAudioTemplateList(long AudioTemplateId)
        {
            DBParameters.Clear();
            AddParameter("@AudioTemplateId", AudioTemplateId);
            IDataReader sqlReader = ExecuteReader("GetAudioTemplateValues");
            List<AudioTemplateInfo> configValueList = PopulatEAudioTemplateValueList(sqlReader);
            sqlReader.Close();
            return configValueList;
        }

        private List<AudioTemplateInfo> PopulatEAudioTemplateValueList(IDataReader sqlReader)
        {
            List<AudioTemplateInfo> audioValueList = new List<AudioTemplateInfo>();
            while (sqlReader.Read())
            {
                AudioTemplateInfo audioValue = new AudioTemplateInfo();
                audioValue.AudioTemplateId = GetFieldValue(sqlReader, "AudioTemplateId", 0L);
                audioValue.Name = GetFieldValue(sqlReader, "Name", string.Empty);
                audioValue.Description = GetFieldValue(sqlReader, "Description", string.Empty);
                audioValue.DisplayName = GetFieldValue(sqlReader, "DisplayName", string.Empty);
                audioValue.CreatedOn = GetFieldValue(sqlReader, "CreatedOn", DateTime.Now);
                audioValue.CreatedBy = GetFieldValue(sqlReader, "CreatedBy", 0);
                audioValue.ModifiedOn = GetFieldValue(sqlReader, "ModifiedOn", DateTime.Now);
                audioValue.ModifiedBy = GetFieldValue(sqlReader, "ModifiedBy", 0);
                audioValue.IsActive = GetFieldValue(sqlReader, "IsActive", false);
                audioValue.IsActiveDisplay = audioValue.IsActive == true ? "Active" : "InActive";
                audioValue.AudioLength = GetFieldValue(sqlReader, "AudioLength", 0L);
                audioValueList.Add(audioValue);
            }
            return audioValueList;
        }
        public bool DeleteAudio(long AudioTemplateId)
        {
            DBParameters.Clear();
            AddParameter("@AudioTemplateId", AudioTemplateId);
            ExecuteNonQuery("DeleteAudioRecord");
            return true;
        }


        #endregion Audio Template

        #region Video template
        public bool SaveVideoTemplate(VideoTemplateInfo videoTemplate)
        {

            DBParameters.Clear();
            AddParameter("@VideoTemplateId", videoTemplate.VideoTemplateId);
            AddParameter("@Name", videoTemplate.Name);
            AddParameter("@DisplayName", videoTemplate.DisplayName);
            AddParameter("@Description", videoTemplate.Description);
            AddParameter("@CreatedBy", videoTemplate.CreatedBy);
            AddParameter("@CreatedOn", videoTemplate.CreatedOn);
            AddParameter("@ModifiedBy", videoTemplate.ModifiedBy);
            AddParameter("@ModifiedOn", videoTemplate.ModifiedOn);
            AddParameter("@IsActive", videoTemplate.IsActive);
            AddParameter("@VideoLength", videoTemplate.VideoLength);

            ExecuteNonQuery("SaveVideoTemplate");
            return true;
        }

        public bool SaveVideoSlot(VideoTemplateInfo videoTemplate)
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("VideoSlotId");
            dt.Columns.Add("FrameTimeIn");
            dt.Columns.Add("PhotoDisplayTime");
            foreach (VideoTemplateInfo.VideoSlot item in videoTemplate.videoSlots)
            {
                dt.Rows.Add(item.VideoSlotId, item.FrameTimeIn, item.PhotoDisplayTime);
            }
            DBParameters.Clear();
            AddParameter("@VideoTemplateId", videoTemplate.VideoTemplateId);
            AddParameter("@VideoSlotsTable", dt);
            ExecuteNonQuery("SaveVideoSlot");
            return true;
        }


        public List<VideoTemplateInfo> GetVideoTemplate(long VideoTemplateId)
        {
            DBParameters.Clear();
            AddParameter("@VideoTemplateId", VideoTemplateId);
            IDataReader sqlReader = ExecuteReader("GetVideoTemplates");
            List<VideoTemplateInfo> videoTemplateList = PopulateVideoTemplate(sqlReader);
            sqlReader.Close();
            return videoTemplateList;
        }

        private List<VideoTemplateInfo> PopulateVideoTemplate(IDataReader sqlReader)
        {
            List<VideoTemplateInfo> videoTemplateList = new List<VideoTemplateInfo>();
            while (sqlReader.Read())
            {
                VideoTemplateInfo videoTemplate = new VideoTemplateInfo();
                videoTemplate.VideoTemplateId = GetFieldValue(sqlReader, "VideoTemplateId", 0L);
                videoTemplate.Name = GetFieldValue(sqlReader, "Name", string.Empty);
                videoTemplate.Description = GetFieldValue(sqlReader, "Description", string.Empty);
                videoTemplate.VideoLength = GetFieldValue(sqlReader, "VideoLength", 0L);
                videoTemplate.DisplayName = GetFieldValue(sqlReader, "DisplayName", string.Empty);
                videoTemplate.CreatedOn = GetFieldValue(sqlReader, "CreatedOn", DateTime.Now);
                videoTemplate.CreatedBy = GetFieldValue(sqlReader, "CreatedBy", 0);
                videoTemplate.ModifiedOn = GetFieldValue(sqlReader, "ModifiedOn", DateTime.Now);
                videoTemplate.ModifiedBy = GetFieldValue(sqlReader, "ModifiedBy", 0);
                videoTemplate.IsActive = GetFieldValue(sqlReader, "IsActive", false);
                videoTemplate.IsActiveDisplay = videoTemplate.IsActive ? "Active" : "InActive";

                videoTemplateList.Add(videoTemplate);
            }
            return videoTemplateList;
        }

        public List<VideoTemplateInfo.VideoSlot> GetVideoSlots(long VideoTemplateId)
        {
            DBParameters.Clear();
            this.OpenConnection();
            AddParameter("@VideoTemplateId", VideoTemplateId);
            IDataReader sqlReader = ExecuteReader("GetVideoSlots");
            List<VideoTemplateInfo.VideoSlot> videoTemplateList = PopulateVideoSlot(sqlReader);
            sqlReader.Close();
            return videoTemplateList;
        }
        private List<VideoTemplateInfo.VideoSlot> PopulateVideoSlot(IDataReader sqlReader)
        {
            List<VideoTemplateInfo.VideoSlot> videoSlotList = new List<VideoTemplateInfo.VideoSlot>();
            while (sqlReader.Read())
            {
                VideoTemplateInfo.VideoSlot videoSlot = new VideoTemplateInfo.VideoSlot();
                videoSlot.VideoSlotId = GetFieldValue(sqlReader, "VideoSlotId", 0L);
                videoSlot.FrameTimeIn = GetFieldValue(sqlReader, "FrameTimeIn", 0L);
                videoSlot.PhotoDisplayTime = GetFieldValue(sqlReader, "PhotoDisplayTime", 0);
                videoSlotList.Add(videoSlot);
            }
            return videoSlotList;
        }

        public bool DeleteVideoTemplate(long videoTemplateId)
        {
            DBParameters.Clear();
            AddParameter("@videoTemplateId", videoTemplateId);
            ExecuteNonQuery("DeleteVideoTemplate");
            return true;
        }

        #endregion
        #region Video Background
        public int SaveUpdateVideoBackground(VideoBackgroundInfo list)
        {
            DBParameters.Clear();
            AddParameter("@VideoBackgroundId", list.VideoBackgroundId);
            AddParameter("@Name", list.Name);
            AddParameter("@DisplayName", list.DisplayName);
            AddParameter("@Description", list.Description);
            AddParameter("@MediaType", list.MediaType);
            AddParameter("@CreatedBy", list.CreatedBy);
            AddParameter("@CreatedOn", list.CreatedOn);
            AddParameter("@ModifiedBy", list.ModifiedBy);
            AddParameter("@ModifiedOn", list.ModifiedOn);
            AddParameter("@IsActive", list.IsActive);
            int id = Convert.ToInt32(ExecuteScalar("SaveUpdateVideoBackground"));
            return id;
        }
        public List<VideoBackgroundInfo> GetVideoBackgrounds(long videoBackgroundId)
        {
            DBParameters.Clear();
            AddParameter("@VideoBackgroundId", videoBackgroundId);
            IDataReader sqlReader = ExecuteReader("GetVideoBackgrounds");
            List<VideoBackgroundInfo> videoBGList = PopulateVideoBackgroundList(sqlReader);
            sqlReader.Close();
            return videoBGList;
        }
        private List<VideoBackgroundInfo> PopulateVideoBackgroundList(IDataReader sqlReader)
        {
            List<VideoBackgroundInfo> videoBGValueList = new List<VideoBackgroundInfo>();
            while (sqlReader.Read())
            {
                VideoBackgroundInfo videoBGValue = new VideoBackgroundInfo();
                videoBGValue.VideoBackgroundId = GetFieldValue(sqlReader, "VideoBackgroundId", 0L);
                videoBGValue.Name = GetFieldValue(sqlReader, "Name", string.Empty);
                videoBGValue.Description = GetFieldValue(sqlReader, "Description", string.Empty);
                videoBGValue.DisplayName = GetFieldValue(sqlReader, "DisplayName", string.Empty);
                videoBGValue.MediaType = GetFieldValue(sqlReader, "MediaType", 0);
                videoBGValue.CreatedOn = GetFieldValue(sqlReader, "CreatedOn", DateTime.Now);
                videoBGValue.CreatedBy = GetFieldValue(sqlReader, "CreatedBy", 0);
                videoBGValue.ModifiedOn = GetFieldValue(sqlReader, "ModifiedOn", DateTime.Now);
                videoBGValue.ModifiedBy = GetFieldValue(sqlReader, "ModifiedBy", 0);
                videoBGValue.IsActive = GetFieldValue(sqlReader, "IsActive", false);
                videoBGValue.MediaTypeDisplay = videoBGValue.MediaType == 1 ? "Image" : "Video";
                videoBGValue.IsActiveDisplay = videoBGValue.IsActive == true ? "Active" : "InActive";
                videoBGValueList.Add(videoBGValue);
            }
            return videoBGValueList;
        }
        public bool DeleteVideoBackground(long videoBackgroundId)
        {
            DBParameters.Clear();
            AddParameter("@VideoBackgroundId", videoBackgroundId);
            ExecuteNonQuery("DeleteVideoBackgroundRecord");
            return true;
        }

        #endregion Video Background

        #region Output Video Profiles
        //public long SaveUpdateVideoOutputFormatProfile(VideoConfigProfiles outputProfile)
        //{
        //    long ProfileId = 0;
        //    DBParameters.Clear();
        //    AddParameter("@ProfileId", outputProfile.ProfileId, 0L);
        //    AddParameter("@ProfileName", outputProfile.ProfileName);
        //    AddParameter("@AspectRatio", outputProfile.AspectRatio);
        //    AddParameter("@FrameRate", outputProfile.FrameRate);
        //    AddParameter("@OutputFormat", outputProfile.OutputFormat);
        //    AddParameter("@VideoCodec", outputProfile.VideoCodec);
        //    AddParameter("@AudioCodec", outputProfile.AudioCodec);
        //    AddParameter("@AutoVideoEffects", outputProfile.AutoVideoEffects);
        //    AddParameter("@LocationId", outputProfile.LocationId);
        //    ProfileId = Convert.ToInt64(ExecuteScalar("SaveUpdateVideoOutputProfile"));
        //    //ExecuteNonQuery("SaveUpdateVideoOutputProfile");
        //    return ProfileId;
        //}
        public List<VideoSceneViewModel> GetVideoConfigProfileList(long ProfileId, int substoreid)
        {
            DBParameters.Clear();
            AddParameter("@ProfileId", ProfileId);
            AddParameter("@Substoreid", substoreid);
            IDataReader sqlReader = ExecuteReader("GetVideoConfigProfiles");
            List<VideoSceneViewModel> configProfileValueList = PopulateVideoConfigProfileList(sqlReader);
            sqlReader.Close();
            return configProfileValueList;
        }
        private List<VideoSceneViewModel> PopulateVideoConfigProfileList(IDataReader sqlReader)
        {
            List<VideoSceneViewModel> lstVideoSceneVM = new List<VideoSceneViewModel>();
            while (sqlReader.Read())
            {
                VideoSceneViewModel videoSceneVM = new VideoSceneViewModel();
                VideoScene videoScene = new VideoScene();
                VideoObjectFileMapping VOFM = new VideoObjectFileMapping();
                videoScene.SceneId = GetFieldValue(sqlReader, "SceneId", 0);
                videoScene.Name = GetFieldValue(sqlReader, "Name", string.Empty);
                videoScene.IsActiveForAdvanceProcessing = GetFieldValue(sqlReader, "IsActiveForAdvanceProcessing", false);
                videoScene.ScenePath = GetFieldValue(sqlReader, "ScenePath", string.Empty);
                videoScene.LocationId = GetFieldValue(sqlReader, "LocationId", 0);
                videoScene.VideoLength = GetFieldValue(sqlReader, "VideoLength", 0);
                videoScene.CG_ConfigID = GetFieldValue(sqlReader, "CG_ConfigId", 0);
                videoScene.IsActive = GetFieldValue(sqlReader, "IsActive", false);
                videoScene.Settings = GetFieldValue(sqlReader, "settings", string.Empty);
                VOFM.ChromaPath = GetFieldValue(sqlReader, "ChromaPath", string.Empty);
                videoScene.IsVerticalVideo = GetFieldValue(sqlReader, "IsVerticalVideo", false);
                videoSceneVM.VideoScene = videoScene;
                lstVideoSceneVM.Add(videoSceneVM);
            }
            return lstVideoSceneVM;
        }

        public bool DeleteVideoConfigProfile(long ProfileId)
        {
            int isDeleted = 0;
            DBParameters.Clear();
            AddOutParameterString("@paramSceneName", SqlDbType.NVarChar, 350);
            AddParameter("@paramSceneId", ProfileId);
            isDeleted = Convert.ToInt32(ExecuteNonQuery("usp_DeleteVideoSceneDetails"));
            if (isDeleted > 0)
                return true;
            else
                return false;
        }
        # endregion

        public bool UpdateReceiptData(StoreInfo store)
        {
            AddParameter("@BillReceiptTitle", store.BillReceiptTitle);
            AddParameter("@TaxRegistrationNumber", store.TaxRegistrationNumber);
            AddParameter("@TaxRegistrationText", store.TaxRegistrationText);
            AddParameter("@Address", store.Address);
            AddParameter("@PhoneNo", store.PhoneNo);
            AddParameter("@TaxMinSequenceNo", store.TaxMinSequenceNo);
            AddParameter("@TaxMaxSequenceNo", store.TaxMaxSequenceNo);
            AddParameter("@IsSequenceNoRequired", store.IsSequenceNoRequired);
            AddParameter("@IsTaxEnabled", store.IsTaxEnabled);
            ExecuteNonQuery("UpdateStoreReceiptData");
            return true;
        }
        public StoreInfo getTaxConfigData()
        {
            StoreInfo store = new StoreInfo();
            IDataReader sqlReader = ExecuteReader("GetTaxConfigData");
            store = PopulateTaxConfigData(sqlReader);
            sqlReader.Close();
            return store;
        }
        private StoreInfo PopulateTaxConfigData(IDataReader sqlReader)
        {
            StoreInfo store = new StoreInfo();
            while (sqlReader.Read())
            {
                store.BillReceiptTitle = GetFieldValue(sqlReader, "BillReceiptTitle", string.Empty);
                store.Address = GetFieldValue(sqlReader, "Address", string.Empty);
                store.PhoneNo = GetFieldValue(sqlReader, "PhoneNo", string.Empty);
                store.TaxRegistrationNumber = GetFieldValue(sqlReader, "TaxRegistrationNumber", string.Empty);
                store.TaxRegistrationText = GetFieldValue(sqlReader, "TaxRegistrationText", string.Empty);
                store.IsSequenceNoRequired = GetFieldValue(sqlReader, "IsSequenceNoRequired", false);
                store.IsTaxEnabled = GetFieldValue(sqlReader, "IsTaxEnabled", false);
                store.TaxMinSequenceNo = GetFieldValue(sqlReader, "TaxMinSequenceNo", 0L);
                store.TaxMaxSequenceNo = GetFieldValue(sqlReader, "TaxMaxSequenceNo", 0L);
            }
            return store;
        }
        public int GetSubstoreLocationWise(int locationid)
        {
            DBParameters.Clear();
            AddParameter("@locationId", locationid);
            int Substore = Convert.ToInt32(ExecuteScalar("GetSubstoreLocationWise"));
            return Substore;
        }

        public bool SaveEmailDetails(string toAddress, string toBCC, string sender, string msgBody, string msgType)
        {
            DBParameters.Clear();
            AddParameter("@OrderId", null);
            AddParameter("@Emailto", toAddress);
            AddParameter("@EmailBcc", toBCC);
            AddParameter("@EmailIsSent", false);
            AddParameter("@Sendername", sender);
            AddParameter("@EmailMessage", msgBody);
            AddParameter("@DG_MediaName", "EM");
            AddParameter("@DG_MessageType", msgType);
            ExecuteNonQuery("InsertEmailDetails");
            return true;
        }
        public bool SaveEmailDetails(string toAddress, string toBCC, string sender, string msgBody, string msgType, int subStoreID)
        {
            DBParameters.Clear();
            AddParameter("@OrderId", null);
            AddParameter("@Emailto", toAddress);
            AddParameter("@EmailBcc", toBCC);
            AddParameter("@EmailIsSent", false);
            AddParameter("@Sendername", sender);
            AddParameter("@EmailMessage", msgBody);
            AddParameter("@DG_MediaName", "EM");
            AddParameter("@DG_MessageType", msgType);
            AddParameter("@SubStoreId", subStoreID);
            ExecuteNonQuery("InsertEmailDetails");
            return true;
        }
        public bool DeleteConfigLocation(int substoreId, int locationId)
        {
            DBParameters.Clear();
            AddParameter("@ParamsubstoreId", substoreId);
            AddParameter("@ParamlocationId", locationId);
            ExecuteNonQuery("usp_DEL_ConfigLocation");
            return true;
        }
        public List<StockShot> GetStockShotImagesList(Int64 ImageId)
        {
            DBParameters.Clear();
            AddParameter("@paramImageId", ImageId);
            IDataReader sqlReader = ExecuteReader("usp_GETStockShotImages");
            List<StockShot> stockShotList = PopulateGetStockShotList(sqlReader);
            sqlReader.Close();
            return stockShotList;
        }

        private List<StockShot> PopulateGetStockShotList(IDataReader sqlReader)
        {
            List<StockShot> stockShotList = new List<StockShot>();
            while (sqlReader.Read())
            {
                StockShot stockShot = new StockShot();
                stockShot.ImageId = GetFieldValue(sqlReader, "ImageId", 0L);
                stockShot.ImageName = GetFieldValue(sqlReader, "ImageName", string.Empty);
                stockShot.IsActive = GetFieldValue(sqlReader, "IsActive", false);
                stockShotList.Add(stockShot);
            }
            return stockShotList;
        }
        public Int64 SaveUpdateStockShotImage(StockShot img)
        {
            DBParameters.Clear();
            AddParameter("@ParamImageId", img.ImageId);
            AddParameter("@ParamImageName", img.ImageName);
            AddParameter("@ParamCreatedBy", img.CreatedBy);
            AddParameter("@ParamModifiedBy", img.ModifiedBy);
            AddParameter("@ParamIsActive", img.IsActive);
            long Id = Convert.ToInt64(ExecuteScalar("usp_SaveStockShotImage"));
            return Id;
        }
        public bool DeleteStockShotImg(Int64 ImgId)
        {
            DBParameters.Clear();
            AddParameter("@ParamStockShotImgId", ImgId);
            ExecuteNonQuery("usp_DeleteStockShotImage");
            return true;

        }
        public List<ConfigurationInfo> GetAllSubstoreConfigdata()
        {
            DBParameters.Clear();
            IDataReader sqlReader = ExecuteReader("usp_GET_AllSubstoreConfigdata");
            List<ConfigurationInfo> substoreConfigList = PopulateSubstoreConfigdataList(sqlReader);
            sqlReader.Close();
            return substoreConfigList;
        }
        private List<ConfigurationInfo> PopulateSubstoreConfigdataList(IDataReader sqlReader)
        {
            List<ConfigurationInfo> configInfoList = new List<ConfigurationInfo>();
            while (sqlReader.Read())
            {
                ConfigurationInfo objConfig = new ConfigurationInfo();
                objConfig.DG_Hot_Folder_Path = GetFieldValue(sqlReader, "DG_Hot_Folder_Path", string.Empty);
                objConfig.DG_Frame_Path = GetFieldValue(sqlReader, "DG_Frame_Path", string.Empty);
                objConfig.DG_BG_Path = GetFieldValue(sqlReader, "DG_BG_Path", string.Empty);
                objConfig.DG_Graphics_Path = GetFieldValue(sqlReader, "DG_Graphics_Path", string.Empty);
                objConfig.DG_Substore_Id = GetFieldValue(sqlReader, "DG_Substore_Id", 0);
                configInfoList.Add(objConfig);
            }
            return configInfoList;
        }
        public List<iMIXStoreConfigurationInfo> GetStoreConfigData()
        {
            DBParameters.Clear();

            IDataReader sqlReader = ExecuteReader("usp_SEL_GetStoreConfigData");
            List<iMIXStoreConfigurationInfo> dummyTagList = PopulateStoreConfigDataList(sqlReader);
            sqlReader.Close();
            return dummyTagList;
        }
        private List<iMIXStoreConfigurationInfo> PopulateStoreConfigDataList(IDataReader sqlReader)
        {
            List<iMIXStoreConfigurationInfo> configInfoList = new List<iMIXStoreConfigurationInfo>();
            while (sqlReader.Read())
            {
                iMIXStoreConfigurationInfo objConfig = new iMIXStoreConfigurationInfo();
                objConfig.iMIXStoreConfigurationValueId = GetFieldValue(sqlReader, "iMIXStoreConfigurationValueId", 0L);
                objConfig.IMIXConfigurationMasterId = GetFieldValue(sqlReader, "IMIXConfigurationMasterId", 0L);
                objConfig.ConfigurationValue = GetFieldValue(sqlReader, "ConfigurationValue", string.Empty);
                objConfig.SyncCode = GetFieldValue(sqlReader, "SyncCode", string.Empty);
                objConfig.IsSynced = GetFieldValue(sqlReader, "IsSynced", false);
                objConfig.ModifiedDate = GetFieldValue(sqlReader, "ModifiedDate", DateTime.MinValue);
                configInfoList.Add(objConfig);
            }
            return configInfoList;
        }

        public VideoSceneViewModel GetVideoSceneBasedOnPhotoId(int PhotoId)
        {
            DBParameters.Clear();
            AddParameter("@PhototId", PhotoId);
            IDataReader sqlReader = ExecuteReader("GetVideoConfigProfilesBasedOnPhotoId");
            List<VideoSceneViewModel> configProfileValueList = PopulateVideoConfigProfileList(sqlReader);
            sqlReader.Close();
            if (configProfileValueList.Count > 0)
                return configProfileValueList[0];
            else return null;
        }
        public int SaveCGConfigSetting(CGConfigSettings CGConfigSettings)
        {
            DBParameters.Clear();
            AddParameter("@ParamID", CGConfigSettings.ID, ParameterDirection.InputOutput);
            AddParameter("@ParamConfigName", CGConfigSettings.ConfigFileName);
            AddParameter("@ParamExtension", CGConfigSettings.Extension);
            AddParameter("@ParamDisplayName", CGConfigSettings.ConfigFileName);
            AddParameter("@ParamIsActive", CGConfigSettings.IsActive);
            Convert.ToInt64(ExecuteScalar("[dbo].[usp_INS_CGConfigSettings]"));
            int Id = Convert.ToInt32(GetOutParameterValue("@ParamID"));
            return Id;
        }
        public List<CGConfigSettings> GetCGConfigSettngs(int configId)
        {
            DBParameters.Clear();
            AddParameter("@ParamConfigId", configId);
            IDataReader sqlReader = ExecuteReader("usp_GET_CGConfigSettings");
            List<CGConfigSettings> configList = PopulateGetCGConfigSettngs(sqlReader);
            sqlReader.Close();
            return configList;
        }

        private List<CGConfigSettings> PopulateGetCGConfigSettngs(IDataReader sqlReader)
        {
            List<CGConfigSettings> configInfoList = new List<CGConfigSettings>();
            while (sqlReader.Read())
            {
                CGConfigSettings objConfig = new CGConfigSettings();
                objConfig.ID = GetFieldValue(sqlReader, "ID", 0);
                objConfig.ConfigFileName = GetFieldValue(sqlReader, "ConfigName", string.Empty);
                objConfig.Extension = GetFieldValue(sqlReader, "Extension", string.Empty);
                objConfig.DisplayName = GetFieldValue(sqlReader, "DisplayName", string.Empty);
                configInfoList.Add(objConfig);
            }
            return configInfoList;
        }
        public bool DeleteCGConfigSettngs(int ConfigId)
        {
            DBParameters.Clear();
            AddParameter("@ParamConfigID", ConfigId);
            int result = Convert.ToInt32(ExecuteNonQuery("usp_DEL_CGConfigSettings"));
            if (result > 0)
                return true;
            else
                return false;
        }
        public int SaveUpdateVideoOverlay(VideoOverlay list)
        {
            DBParameters.Clear();
            AddParameter("@VideoOverlayId", list.VideoOverlayId);
            AddParameter("@Name", list.Name);
            AddParameter("@DisplayName", list.DisplayName);
            AddParameter("@Description", list.Description);
            AddParameter("@MediaType", list.MediaType);
            AddParameter("@VideoLength", list.VideoLength);
            AddParameter("@CreatedBy", list.CreatedBy);
            AddParameter("@CreatedOn", list.CreatedOn);
            AddParameter("@ModifiedBy", list.ModifiedBy);
            AddParameter("@ModifiedOn", list.ModifiedOn);
            AddParameter("@IsActive", list.IsActive);
            int id = Convert.ToInt32(ExecuteScalar("SaveUpdateVideoOverlay"));
            return id;
        }
        public bool DeleteVideoOverlay(long overlayId)
        {
            DBParameters.Clear();
            AddParameter("@videoOverlayId", overlayId);
            ExecuteNonQuery("DeleteVideoOverlay");
            return true;
        }
        public List<VideoBackgroundInfo> GetVideoOverlay(long VideoOverlayId)
        {
            DBParameters.Clear();
            AddParameter("@VideoOverlayId", VideoOverlayId);
            IDataReader sqlReader = ExecuteReader("GetVideoTemplates");
            List<VideoBackgroundInfo> videoList = PopulateVideoOverlay(sqlReader);
            sqlReader.Close();
            return videoList;
        }
        private List<VideoBackgroundInfo> PopulateVideoOverlay(IDataReader sqlReader)
        {
            List<VideoBackgroundInfo> videoTemplateList = new List<VideoBackgroundInfo>();
            while (sqlReader.Read())
            {
                VideoBackgroundInfo videoTemplate = new VideoBackgroundInfo();
                videoTemplate.Name = GetFieldValue(sqlReader, "Name", string.Empty);
                videoTemplate.Description = GetFieldValue(sqlReader, "Description", string.Empty);
                videoTemplate.MediaType = GetFieldValue(sqlReader, "MediaType", 0);
                videoTemplate.VideoLength = GetFieldValue(sqlReader, "VideoLength", 0);
                videoTemplate.DisplayName = GetFieldValue(sqlReader, "DisplayName", string.Empty);
                videoTemplate.CreatedOn = GetFieldValue(sqlReader, "CreatedOn", DateTime.Now);
                videoTemplate.CreatedBy = GetFieldValue(sqlReader, "CreatedBy", 0);
                videoTemplate.ModifiedOn = GetFieldValue(sqlReader, "ModifiedOn", DateTime.Now);
                videoTemplate.ModifiedBy = GetFieldValue(sqlReader, "ModifiedBy", 0);
                videoTemplate.IsActive = GetFieldValue(sqlReader, "IsActive", false);
                videoTemplate.IsActiveDisplay = videoTemplate.IsActive ? "Active" : "InActive";

                videoTemplateList.Add(videoTemplate);
            }
            return videoTemplateList;
        }
        public List<VideoOverlay> GetVideoOverlays(long videoBackgroundId)
        {
            DBParameters.Clear();
            AddParameter("@VideoOverlayId", videoBackgroundId);
            IDataReader sqlReader = ExecuteReader("GetVideoOverlay");
            List<VideoOverlay> videoBGList = PopulateVideoOverlays(sqlReader);
            sqlReader.Close();
            return videoBGList;
        }

        private List<VideoOverlay> PopulateVideoOverlays(IDataReader sqlReader)
        {
            List<VideoOverlay> videoOverlayList = new List<VideoOverlay>();
            while (sqlReader.Read())
            {
                VideoOverlay videoOverlay = new VideoOverlay();
                videoOverlay.VideoOverlayId = GetFieldValue(sqlReader, "VideoOverlayId",0L);
                videoOverlay.Name = GetFieldValue(sqlReader, "Name", string.Empty);
                videoOverlay.Description = GetFieldValue(sqlReader, "Description", string.Empty);
                videoOverlay.MediaType = GetFieldValue(sqlReader, "MediaType", 0);
                videoOverlay.VideoLength = GetFieldValue(sqlReader, "VideoLength", 0L);
                videoOverlay.DisplayName = GetFieldValue(sqlReader, "DisplayName", string.Empty);
                videoOverlay.CreatedOn = GetFieldValue(sqlReader, "CreatedOn", DateTime.Now);
                videoOverlay.CreatedBy = GetFieldValue(sqlReader, "CreatedBy", 0);
                videoOverlay.ModifiedOn = GetFieldValue(sqlReader, "ModifiedOn", DateTime.Now);
                videoOverlay.ModifiedBy = GetFieldValue(sqlReader, "ModifiedBy", 0);
                videoOverlay.IsActive = GetFieldValue(sqlReader, "IsActive", false);
                videoOverlay.IsActiveDisplay = videoOverlay.IsActive ? "Active" : "InActive";

                videoOverlayList.Add(videoOverlay);
            }
            return videoOverlayList;
        }
        ////////////////created by latika for table work flow
        public int SaveUpdateRFIDTableWorkflow(iMIXRFIDTableWorkflowInfo imixRFIDflowInfoInfo)
        {
            int sReturnVal = 0;
            DBParameters.Clear();
            AddParameter("@FontSize", imixRFIDflowInfoInfo.FontSize);
            AddParameter("@FontStyle", imixRFIDflowInfoInfo.FontStyle);
            AddParameter("@FontFamily", imixRFIDflowInfoInfo.FontFamily);
            AddParameter("@FontWeight", imixRFIDflowInfoInfo.FontWeight);
            AddParameter("@Settings", imixRFIDflowInfoInfo.Settings);
            AddParameter("@SubStoreID", imixRFIDflowInfoInfo.SubStoreID);
            AddParameter("@LocationID", imixRFIDflowInfoInfo.LocationID);
            AddParameter("@UserID", imixRFIDflowInfoInfo.UserID);
            AddParameter("@IsActive", imixRFIDflowInfoInfo.IsActive);
            long Id = Convert.ToInt64(ExecuteScalar("SP_SaveUpdateRFIDTableWorkflow"));////made changes by lathi
            sReturnVal = Convert.ToInt32(Id);

            return sReturnVal;
        }
        public List<iMIXRFIDTableWorkflowInfo> GetAllTableWorkFlow()
        {
            DBParameters.Clear();
            AddParameter("@LocationID", 0);
            IDataReader sqlReader = ExecuteReader("SP_GetAllTableWorkFlow");
            List<iMIXRFIDTableWorkflowInfo> lstWorkflowInfo = PopulateRFIDTableWorkflowData(sqlReader);
            sqlReader.Close();
            return lstWorkflowInfo;
        }
        public List<iMIXRFIDTableWorkflowInfo> GetByIDTableWorkFlow(int LocationID)
        {
            DBParameters.Clear();
            AddParameter("@LocationID", LocationID);
            IDataReader sqlReader = ExecuteReader("SP_GetAllTableWorkFlow");
            List<iMIXRFIDTableWorkflowInfo> lstWorkflowInfo = PopulateRFIDTableWorkflowByID(sqlReader);
            sqlReader.Close();
            return lstWorkflowInfo;
        }
        private List<iMIXRFIDTableWorkflowInfo> PopulateRFIDTableWorkflowData(IDataReader dataReader)
        {
            List<iMIXRFIDTableWorkflowInfo> lstRFIDFlush = new List<iMIXRFIDTableWorkflowInfo>();
            iMIXRFIDTableWorkflowInfo RfidTableWorkflow = null;
            DateTime? pr = null;
            while (dataReader.Read())
            {
                RfidTableWorkflow = new iMIXRFIDTableWorkflowInfo();
                RfidTableWorkflow.FontSize = GetFieldValue(dataReader, "FontSize", 0);
                RfidTableWorkflow.FontStyle = GetFieldValue(dataReader, "FontStyle", string.Empty);
                // RfidTableWorkflow.ErrorMessage = GetFieldValue(dataReader, "ErrorMessage", string.Empty);
                RfidTableWorkflow.FontFamily = GetFieldValue(dataReader, "FontFamily", string.Empty);
                RfidTableWorkflow.FontWeight = GetFieldValue(dataReader, "FontWeight", string.Empty);
                RfidTableWorkflow.Status = GetFieldValue(dataReader, "Status", string.Empty);
                RfidTableWorkflow.UserName = GetFieldValue(dataReader, "UserName", string.Empty);
                RfidTableWorkflow.LocationID = GetFieldValue(dataReader, "LocationID", 0);
                RfidTableWorkflow.LocationName = GetFieldValue(dataReader, "LocationName", string.Empty);
                lstRFIDFlush.Add(RfidTableWorkflow);
            }
            return lstRFIDFlush;
        }
        private List<iMIXRFIDTableWorkflowInfo> PopulateRFIDTableWorkflowByID(IDataReader dataReader)
        {
            List<iMIXRFIDTableWorkflowInfo> lstRFIDFlush = new List<iMIXRFIDTableWorkflowInfo>();
            iMIXRFIDTableWorkflowInfo RfidTableWorkflow = null;
            DateTime? pr = null;
            while (dataReader.Read())
            {
                RfidTableWorkflow = new iMIXRFIDTableWorkflowInfo();
                RfidTableWorkflow.FontSize = GetFieldValue(dataReader, "FontSize", 0);
                RfidTableWorkflow.FontStyle = GetFieldValue(dataReader, "FontStyle", string.Empty);
                RfidTableWorkflow.IsActive = GetFieldValue(dataReader, "IsActive", false);
                RfidTableWorkflow.FontFamily = GetFieldValue(dataReader, "FontFamily", string.Empty);
                RfidTableWorkflow.FontWeight = GetFieldValue(dataReader, "FontWeight", string.Empty);
                RfidTableWorkflow.Status = GetFieldValue(dataReader, "Status", string.Empty);
                RfidTableWorkflow.UserName = GetFieldValue(dataReader, "UserName", string.Empty);
                RfidTableWorkflow.LocationID = GetFieldValue(dataReader, "LocationID", 0);
                RfidTableWorkflow.LocationName = GetFieldValue(dataReader, "LocationName", string.Empty);
                RfidTableWorkflow.TypesName = GetFieldValue(dataReader, "TypesName", string.Empty);
                RfidTableWorkflow.Position = GetFieldValue(dataReader, "Position", string.Empty);
                RfidTableWorkflow.BackColor = GetFieldValue(dataReader, "BackColor", "#ffffff");
                RfidTableWorkflow.Font = GetFieldValue(dataReader, "Font", "#000000");
                RfidTableWorkflow.MarginLeft = GetFieldValue(dataReader, "MarginLeft", 0);
                RfidTableWorkflow.MarginTop = GetFieldValue(dataReader, "MarginTop", 0);
                RfidTableWorkflow.MarginRight = GetFieldValue(dataReader, "MarginRight", 0);
                RfidTableWorkflow.MarginBottom = GetFieldValue(dataReader, "MarginBottom", 0);
                RfidTableWorkflow.Orientation = GetFieldValue(dataReader, "Orientation", string.Empty);

                lstRFIDFlush.Add(RfidTableWorkflow);
            }
            return lstRFIDFlush;
        }
        /////////////end by latika
        public int CheckIsRemapPrintersAccess(int RoleID)///created by latika to check rights of Remap printers
        {
            DBParameters.Clear();
            AddParameter("@RoleID", RoleID);
            object _objnumber = ExecuteScalar("usp_CheckIsRemapPrintersAccess");
            return Convert.ToInt32(_objnumber);
        }

        //  -----------------------------added by latika group delete----------------------------------------------------
        public bool GETGroupDeleteID(int SubstoreID)
        {
            DBParameters.Clear();
            AddParameter("@SubstoreID", SubstoreID);
            bool strReturn = Convert.ToBoolean(ExecuteScalar("SP_GETGroupDeleteID").ToString());
            return strReturn;
        }
        public int GETGroupDeleteMasterID()
        {
            DBParameters.Clear();
            int strReturn = Convert.ToInt32(ExecuteScalar("SP_GETGroupDeleteMasterID").ToString());
            return strReturn;
        }

        // ----------------------------------end-------------------------------------------------------------------------

        //  -----------------------------added by latika Stop showing editing image in Client view----------------------------------------------------
        public bool GETStopEditingImageClntvwBySubID(int SubStoreId)
        {
            DBParameters.Clear();
            AddParameter("@SubStoreId", SubStoreId);
            bool strReturn = Convert.ToBoolean(ExecuteScalar("GETStopEditingImageClntvwBySubID").ToString());
            return strReturn;
        }
        public int GETStopEditingImagesinclientViewMasterID()
        {
            DBParameters.Clear();
            int strReturn = Convert.ToInt32(ExecuteScalar("SP_GETStopEditingImagesinclientViewMasterID").ToString());
            return strReturn;
        }

        // ----------------------------------end-------------------------------------------------------------------------

        /// <summary>
        /// //created by pre sold settings by latika
        /// </summary>
        /// <returns></returns>
        public int GETPreSoldDelayTmMasterID()
        {
            DBParameters.Clear();
            int strReturn = Convert.ToInt32(ExecuteScalar("SP_GETPreSoldDelayTmMasterID").ToString());
            return strReturn;
        }
        
        public List<Presold> GETPreSoldDelayTmVal(int SubstoreID, int LocationID)
        {
            DBParameters.Clear();
            AddParameter("@SubstoreID", SubstoreID);
            AddParameter("@LocationID", LocationID);
            IDataReader sqlReader = ExecuteReader("SP_GETPreSoldDelayTmVal");
            List<Presold> PresoldList = PopulatePresold(sqlReader);
            sqlReader.Close();
            return PresoldList;
        }
        private List<Presold> PopulatePresold(IDataReader sqlReader)
        {
            List<Presold> PresoldList = new List<Presold>();
            while (sqlReader.Read())
            {
                Presold iPresold = new Presold();
                iPresold.PreSoldDelayTime = GetFieldValue(sqlReader, "PreSoldDelayTime", 0);
                iPresold.IsPrefixActiveFlow =GetFieldValue(sqlReader, "IsPrefixActiveFlow", false);
                PresoldList.Add(iPresold);
            }
            return PresoldList;
        }
        // ----------------------------------end-------------------------------------------------------------------------

        #region Added by Ajay on 31 Jan 20 for watermark  client view
        /// <summary>
        /// Added by Ajay on 31 Jan 20 for watermark client view 
        /// </summary>
        /// <param name="objWatermarkModel"></param>
        /// <returns></returns>
        public bool SaveClientviewWatermark(WatermarkModel objWatermarkModel)
        {
            DBParameters.Clear();
            AddParameter("@SubStoreID", objWatermarkModel.SubStoreId);
            AddParameter("@FontSize", objWatermarkModel.TextFontSize);
            AddParameter("@FontWeight", objWatermarkModel.TextFontWeights.ToString());
            AddParameter("@FontStyle", objWatermarkModel.FontStyle.ToString());
            AddParameter("@FontFamily", objWatermarkModel.TextFontfamilyNames.ToString());
            AddParameter("@FontColour", objWatermarkModel.TextFontColor.ToString());
            AddParameter("@WatermarkText", objWatermarkModel.WatermarkText);
            AddParameter("@Opacity", objWatermarkModel.Opacity);
            AddParameter("@IsUnderline", objWatermarkModel.IsUnderline);
            AddParameter("@CreatedFrom", objWatermarkModel.UserId);
            AddParameter("@ModifiedFrom", objWatermarkModel.UserId);
            AddOutParameter("@CVId", SqlDbType.Int);
            ExecuteNonQuery("DG_USP_ClientviewWatermark");
            return Convert.ToBoolean(GetOutParameterValue("@CVId")); ;
        }
        /// <summary>
        /// Get the detail of Client watermark as per substore id.
        /// </summary>
        /// <param name="SubStoreID"></param>
        /// <returns></returns>
        public WatermarkModel GetClientWatermarkConfig(int SubStoreID)
        {
            WatermarkModel objWatermarkModel = new WatermarkModel();
            DBParameters.Clear();
            AddParameter("@SubStoreID", SubStoreID);
            IDataReader sqlReader = ExecuteReader("DG_GetClientviewWatermark");
            objWatermarkModel = PopulateClientWatermarkConfig(sqlReader);
            return objWatermarkModel;
        }
        /// <summary>
        /// Map the watermark details with properties
        /// </summary>
        /// <param name="sqlReader"></param>
        /// <returns></returns>

        private WatermarkModel PopulateClientWatermarkConfig(IDataReader sqlReader)
        {
            WatermarkModel iClientWatermarkConfig = new WatermarkModel();
            while (sqlReader.Read())
            {
                iClientWatermarkConfig.TextFontSize = GetFieldValue(sqlReader, "FontSize", 0);
                iClientWatermarkConfig.TextFontWeights = (FontWeight)new FontWeightConverter().ConvertFromString(GetFieldValue(sqlReader, "FontWeight", string.Empty));
                var FontStyle = ParseStyle(GetFieldValue(sqlReader, "FontStyle", string.Empty));
                iClientWatermarkConfig.FontStyle = FontStyle;
                System.Windows.Media.FontFamily mfont = new System.Windows.Media.FontFamily(GetFieldValue(sqlReader, "FontFamily", string.Empty));
                iClientWatermarkConfig.TextFontfamilyNames = mfont;
                iClientWatermarkConfig.TextFontColor = ((System.Windows.Media.Color)System.Windows.Media.ColorConverter.ConvertFromString(GetFieldValue(sqlReader, "FontColour", string.Empty)));
                iClientWatermarkConfig.WatermarkText = GetFieldValue(sqlReader, "WatermarkText", string.Empty);
                iClientWatermarkConfig.Opacity = Convert.ToDouble(GetFieldValue(sqlReader, "Opacity", string.Empty));
            }
            return iClientWatermarkConfig;
        }
        /// <summary>
        /// Parsing String style to Fontstyle
        /// </summary>
        /// <param name="styleString"></param>
        /// <returns></returns>
        public static FontStyle ParseStyle(string styleString)
        {
            FontStyle style = FontStyles.Normal;
            FontStyleConverter converter = new FontStyleConverter();
            style = (FontStyle)converter.ConvertFromString(styleString);
            return style;
        }
        #endregion

        //changed by latika for Evoucher 24March20
        public string GETVenuName()
        {
            DBParameters.Clear();
            string strReturn = Convert.ToString(ExecuteScalar("SP_GETVenue").ToString());
            return strReturn;
        }
        public int GETEvoucherID()
        {
            DBParameters.Clear();
            int strReturn = Convert.ToInt32(ExecuteScalar("SP_GETIsEvoucherMasterID").ToString());
            return strReturn;
        }////end by latika

        #region SET3 methods

        public int GETSet3EnabledID()
        {
            DBParameters.Clear();
            int strReturn = Convert.ToInt32(ExecuteScalar("SP_GETIsSETThreeEnabled").ToString());
            return strReturn;
        }
        public int GETSet3DayWiseID()
        {
            DBParameters.Clear();
            int strReturn = Convert.ToInt32(ExecuteScalar("SP_GETIsSetThreeDayWiseEnabled").ToString());
            return strReturn;
        }
        public int GETSet3HourWiseID()
        {
            DBParameters.Clear();
            int strReturn = Convert.ToInt32(ExecuteScalar("SP_GETIsSetThreeHourWiseEnabled").ToString());
            return strReturn;
        }

        #endregion
    }

}
