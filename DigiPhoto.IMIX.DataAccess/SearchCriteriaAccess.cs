﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using DigiPhoto.IMIX.Model;

namespace DigiPhoto.IMIX.DataAccess
{
   public class SearchCriteriaAccess:BaseDataAccess

    {
        #region Constructor
        public SearchCriteriaAccess(BaseDataAccess baseaccess)
            : base(baseaccess)
        {

        }

        public SearchCriteriaAccess()
        {

        }
        #endregion

       //public List<SearchDetailInfo> GetSearchDetailWithQrcode(SearchDetailInfo Searchdetails)
       //{
       //    DBParameters.Clear();
       //    AddParameter("@fromdate", Searchdetails.FromDate);
       //    AddParameter("@Todate", Searchdetails.ToDate);
       //    AddParameter("@LocationId", Searchdetails.Locationid);
       //    AddParameter("@photographerName", Searchdetails.Userid);
       //    AddParameter("@Substore", Searchdetails.SubstoreId);
       //    AddParameter("@Qrcode", Searchdetails.Qrcode);
       //    AddParameter("@IsAnonymousQrCodeEnabled", Searchdetails.IsAnonymousQrcodeEnabled);
       //    IDataReader sqlReader = ExecuteReader("GetSearchDetailsWithQRcde");
       //    List<SearchDetailInfo> searchList = GetSearchDetailByPhotoIds(sqlReader);
       //    sqlReader.Close();
       //    return searchList;
       //}
        public List<SearchDetailInfo> GetSearchDetailWithQrcode(SearchDetailInfo Searchdetails, out long maxPhotoId, out long minPhotoId, out long imgCount,int mediaType)
       {
           imgCount = 0;
           maxPhotoId = 0;
           minPhotoId = 0;
           DBParameters.Clear();
           AddParameter("@ParamFromdate", Searchdetails.FromDate);
           AddParameter("@ParamTodate", Searchdetails.ToDate);
           AddParameter("@ParamLocationId", Searchdetails.Locationid);
           AddParameter("@ParamPhotographerName", Searchdetails.Userid);
           AddParameter("@ParamSubstore", Searchdetails.SubstoreId);
           AddParameter("@ParamQrCode", Searchdetails.Qrcode);
           AddParameter("@ParamIsAnonymousQrCodeEnabled", Searchdetails.IsAnonymousQrcodeEnabled);
           AddParameter("@ParamPageNumber", Searchdetails.PageNumber);
           AddParameter("@ParamPageSize", Searchdetails.PageSize);
           AddParameter("@ParamStartIndex", Searchdetails.StartIndex);
           AddParameter("@ParamNewRecord", Searchdetails.NewRecord);

           AddParameter("@ParamCharacterId",SetNullIntegerValue(Searchdetails.CharacterId));
           AddParameter("@ParamMediaType", mediaType);
           AddOutParameter("@ParamLastKeyIndex", SqlDbType.Int);
           AddOutParameter("@ParamFirstKeyIndex", SqlDbType.Int);
           AddOutParameter("@ParamImgCount", SqlDbType.Int);
           //AddParameter("@ParamTotalPage", Searchdetails.IsAnonymousQrcodeEnabled);
           IDataReader sqlReader = ExecuteReader(DAOConstant.USP_SEARCH_DETAILSBYCRITERIA);
           List<SearchDetailInfo> searchList = GetSearchDetailByPhotoIdsOnly(sqlReader);
           sqlReader.Close();
           if (searchList.Count > 0)
           {
               minPhotoId = Convert.ToInt64(GetOutParameterValue("@ParamFirstKeyIndex"));
               maxPhotoId = Convert.ToInt64(GetOutParameterValue("@ParamLastKeyIndex"));
               imgCount = Convert.ToInt64(GetOutParameterValue("@ParamImgCount"));
           }
           return searchList;
       }
	   ////changed by latika for table workflow
        public List<SearchDetailInfo> GetSearchDetailWithQrcodeTableFlow (SearchDetailInfo Searchdetails, out long maxPhotoId, out long minPhotoId, out long imgCount, int mediaType)
        {
            imgCount = 0;
            maxPhotoId = 0;
            minPhotoId = 0;
            DBParameters.Clear();
            //for testing
            string str2 = "usp_SEARCH_DetailsByCriteria_TableFlow '" + Searchdetails.FromDate + "','";
            str2 += Searchdetails.ToDate + "',";
            str2 += Searchdetails.Locationid + ",";
            str2 += Searchdetails.Userid + ",'";
            str2 += Searchdetails.SubstoreId + "','";
            str2 += Searchdetails.Qrcode + "',";
            str2 += Searchdetails.IsAnonymousQrcodeEnabled + ",";
            str2 += Searchdetails.PageNumber + ",";
            str2 += Searchdetails.PageSize + ",";
            str2 += Searchdetails.StartIndex + ",";
            str2 += Searchdetails.NewRecord + ",";

            str2 += SetNullIntegerValue(Searchdetails.CharacterId) + ",";
            str2 += mediaType + ",";
            str2 += SqlDbType.Int + ",";
            str2 += SqlDbType.Int + ",";
            str2 += SqlDbType.Int + ",'";
            str2 += Searchdetails.TableName + "','";
            str2 += Searchdetails.GuestName + "','";
            str2 += Searchdetails.EmailID + "','";
            str2 += Searchdetails.ContactNo + "'";
            ///end

            AddParameter("@ParamFromdate", Searchdetails.FromDate);
            AddParameter("@ParamTodate", Searchdetails.ToDate);
            AddParameter("@ParamLocationId", Searchdetails.Locationid);
            AddParameter("@ParamPhotographerName", Searchdetails.Userid);
            AddParameter("@ParamSubstore", Searchdetails.SubstoreId);
            AddParameter("@ParamQrCode", Searchdetails.Qrcode);
            AddParameter("@ParamIsAnonymousQrCodeEnabled", Searchdetails.IsAnonymousQrcodeEnabled);
            AddParameter("@ParamPageNumber", Searchdetails.PageNumber);
            AddParameter("@ParamPageSize", Searchdetails.PageSize);
            AddParameter("@ParamStartIndex", Searchdetails.StartIndex);
            AddParameter("@ParamNewRecord", Searchdetails.NewRecord);

            AddParameter("@ParamCharacterId", SetNullIntegerValue(Searchdetails.CharacterId));
            AddParameter("@ParamMediaType", mediaType);
            AddOutParameter("@ParamLastKeyIndex", SqlDbType.Int);
            AddOutParameter("@ParamFirstKeyIndex", SqlDbType.Int);
            AddOutParameter("@ParamImgCount", SqlDbType.Int);
            AddParameter("@TableName", Searchdetails.TableName);
            AddParameter("@GuestName", Searchdetails.GuestName);
            AddParameter("@EmailID", Searchdetails.EmailID);
            AddParameter("@ContactNo", Searchdetails.ContactNo);
            //AddParameter("@ParamTotalPage", Searchdetails.IsAnonymousQrcodeEnabled);
            IDataReader sqlReader = ExecuteReader("usp_SEARCH_DetailsByCriteria_TableFlow");
            List<SearchDetailInfo> searchList = GetSearchDetailByPhotoIdsOnly(sqlReader);
            sqlReader.Close();
            if (searchList.Count > 0)
            {
                minPhotoId = Convert.ToInt64(GetOutParameterValue("@ParamFirstKeyIndex"));
                maxPhotoId = Convert.ToInt64(GetOutParameterValue("@ParamLastKeyIndex"));
                imgCount = Convert.ToInt64(GetOutParameterValue("@ParamImgCount"));
            }
            return searchList;
        }
////end
        #region Private Methods
        public List<SearchDetailInfo> GetSearchDetailByPhotoIds(IDataReader sqlReader)
       {
           List<SearchDetailInfo> SearchList = new List<SearchDetailInfo>();
           while (sqlReader.Read())
           {
               SearchDetailInfo SearchDetail = new SearchDetailInfo();
               //SearchDetail.TotalRecords = GetFieldValue(sqlReader, "RowsCount", 0);
               SearchDetail.PhotoId = GetFieldValue(sqlReader, "DG_Photos_pkey", 0);
               SearchDetail.Name = GetFieldValue(sqlReader, "DG_Photos_RFID", string.Empty);
               SearchDetail.FileName = GetFieldValue(sqlReader, "DG_Photos_FileName", string.Empty);               
               SearchDetail.CreatedOn = GetFieldValue(sqlReader, "DG_Photos_CreatedOn", DateTime.Now);
               SearchDetail.HotFolderPath = GetFieldValue(sqlReader, "HotFolderPath", string.Empty);
               SearchDetail.MediaType = GetFieldValue(sqlReader, "DG_MediaType", 0);
               SearchDetail.VideoLength = GetFieldValue(sqlReader, "DG_VideoLength", 0L);
               SearchList.Add(SearchDetail);
           }
           return SearchList;
       }
       public List<SearchDetailInfo> GetSearchDetailByPhotoIdsOnly(IDataReader sqlReader)
       {
           List<SearchDetailInfo> SearchList = new List<SearchDetailInfo>();
           while (sqlReader.Read())
           {
               SearchDetailInfo SearchDetail = new SearchDetailInfo();
               //SearchDetail.TotalRecords = GetFieldValue(sqlReader, "RowsCount", 0);
               SearchDetail.PhotoId = GetFieldValue(sqlReader, "DG_Photos_pkey", 0);
               SearchDetail.Name = GetFieldValue(sqlReader, "DG_Photos_RFID", string.Empty);
               SearchDetail.FileName = GetFieldValue(sqlReader, "DG_Photos_FileName", string.Empty);
               SearchDetail.CreatedOn = GetFieldValue(sqlReader, "DG_Photos_CreatedOn", DateTime.Now);
               SearchDetail.HotFolderPath = GetFieldValue(sqlReader, "HotFolderPath", string.Empty);
               SearchDetail.MediaType = GetFieldValue(sqlReader, "DG_MediaType", 0);
               SearchDetail.VideoLength = GetFieldValue(sqlReader, "DG_VideoLength", 0L);
               SearchDetail.OnlineQRCode = GetFieldValue(sqlReader, "OnlineQRCode", string.Empty);
               SearchDetail.UploadStatus = GetFieldValue(sqlReader, "UploadStatus", 0);//added by latika
                SearchList.Add(SearchDetail);
           }
           return SearchList;
       }
       #endregion
    }
}
