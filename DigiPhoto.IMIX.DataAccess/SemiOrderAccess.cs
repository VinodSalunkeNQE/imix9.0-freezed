﻿using DigiPhoto.IMIX.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace DigiPhoto.IMIX.DataAccess
{
    public class SemiOrderAccess:BaseDataAccess
    {
        #region Constructor
        public SemiOrderAccess(BaseDataAccess baseaccess)
            : base(baseaccess)
        {

        }

        public SemiOrderAccess()
        {

        }
        #endregion

        #region Public
        public List<SemiOrderSettingsInfo> GetSemiOrderSettings()
        {
            DBParameters.Clear();
            IDataReader sqlReader = ExecuteReader("GetSemiOrderSettings");
            List<SemiOrderSettingsInfo> lstSemiOrderSettingsInfo = PopulateSemiOrderSettings(sqlReader);
            sqlReader.Close();
            return lstSemiOrderSettingsInfo;
        }

        public List<SpecProfileProductMappingInfo> GetSemiOrderProfileProductMapping(int ProfileId)
        {
            DBParameters.Clear();
            AddParameter("@ParamProfileId", ProfileId);
            IDataReader sqlReader = ExecuteReader("GetSemiOrderProfileProductMapping");
            List<SpecProfileProductMappingInfo> lstSemiOrderSettingsInfo = PopulateSemiOrderProfileProductMapping(sqlReader);
            sqlReader.Close();
            return lstSemiOrderSettingsInfo;
        }
        #endregion

        #region Private
        private List<SemiOrderSettingsInfo> PopulateSemiOrderSettings(IDataReader sqlReader)
        {
            List<SemiOrderSettingsInfo> lstSemiOrderSettingsInfo = new List<SemiOrderSettingsInfo>();
            while(sqlReader.Read())
            {
                SemiOrderSettingsInfo semiOrderSettingsInfo = new SemiOrderSettingsInfo();
                semiOrderSettingsInfo.Id = GetFieldValue(sqlReader, "Id", 0);
                semiOrderSettingsInfo.IsBrightActive = GetFieldValue(sqlReader,"IsBrightActive",false);
                semiOrderSettingsInfo.BrightValue = GetFieldValue(sqlReader, "BrightValue", 0.0);
                semiOrderSettingsInfo.IsContrastActive = GetFieldValue(sqlReader,"IsContrastActive",false);
                semiOrderSettingsInfo.ContrastValue = GetFieldValue(sqlReader,"ContrastValue",0.0);
                semiOrderSettingsInfo.BorderName = GetFieldValue(sqlReader,"BorderName",string.Empty);
                semiOrderSettingsInfo.IsBorderActive = GetFieldValue(sqlReader,"IsBorderActive",false);
                semiOrderSettingsInfo.ProductTypeId = GetFieldValue(sqlReader, "ProductTypeId", string.Empty);
                semiOrderSettingsInfo.VerticalBorderName = GetFieldValue(sqlReader,"VerticalBorderName",string.Empty);
                semiOrderSettingsInfo.IsGreenScreenActive = GetFieldValue(sqlReader,"IsGreenScreenActive",false);
                semiOrderSettingsInfo.BackgroundName = GetFieldValue(sqlReader,"BackgroundName",string.Empty);
                semiOrderSettingsInfo.IsChromaActive = GetFieldValue(sqlReader,"IsChromaActive",false);
                semiOrderSettingsInfo.Graphics_layer_Horizontal = GetFieldValue(sqlReader,"GraphicsLayerHorizontal",string.Empty);
                semiOrderSettingsInfo.Graphics_layer_Vertical = GetFieldValue(sqlReader, "GraphicsLayerVertical", string.Empty);
                semiOrderSettingsInfo.ZoomInfo_Horizontal = GetFieldValue(sqlReader, "ZoomInfoHorizontal", string.Empty);
                semiOrderSettingsInfo.ZoomInfo_Vertical = GetFieldValue(sqlReader, "ZoomInfoVertical", string.Empty);
                semiOrderSettingsInfo.SubstoreId = GetFieldValue(sqlReader, "SubstoreId", 0);
                semiOrderSettingsInfo.IsPrintActive = GetFieldValue(sqlReader,"IsPrintActive",false);
                semiOrderSettingsInfo.IsCropActive = GetFieldValue(sqlReader,"IsCropActive",false);
                semiOrderSettingsInfo.VerticalCropValues = GetFieldValue(sqlReader,"VerticalCropValues",string.Empty);
                semiOrderSettingsInfo.HorizontalCropValues = GetFieldValue(sqlReader,"HorizontalCropValues",string.Empty);
                semiOrderSettingsInfo.LocationId = GetFieldValue(sqlReader, "LocationId", 0);
                semiOrderSettingsInfo.ChromaColor = GetFieldValue(sqlReader, "ChromaColor", string.Empty);
                semiOrderSettingsInfo.ColorCode = GetFieldValue(sqlReader, "ColorCode", string.Empty);
                semiOrderSettingsInfo.ClrTolerance = GetFieldValue(sqlReader, "ClrTolerance", string.Empty);
                semiOrderSettingsInfo.TextLogo_Horizontal = GetFieldValue(sqlReader, "TextXMLHorizontal", string.Empty);
                semiOrderSettingsInfo.TextLogo_Vertical = GetFieldValue(sqlReader, "TextXMLVertical", string.Empty);
                lstSemiOrderSettingsInfo.Add(semiOrderSettingsInfo);
            }
            return lstSemiOrderSettingsInfo;
        }

        private List<SpecProfileProductMappingInfo> PopulateSemiOrderProfileProductMapping(IDataReader sqlReader)
        {
            List<SpecProfileProductMappingInfo> lstSemiOrderSettingsInfo = new List<SpecProfileProductMappingInfo>();
            while(sqlReader.Read())
            {
                SpecProfileProductMappingInfo specProfileProductMappingInfo = new SpecProfileProductMappingInfo();
                specProfileProductMappingInfo.SemiOrderProfileId = GetFieldValue(sqlReader, "SemiOrderProfileId", 0);
                specProfileProductMappingInfo.ProductTypeId = GetFieldValue(sqlReader, "ProductTypeId", 0);
                lstSemiOrderSettingsInfo.Add(specProfileProductMappingInfo);
            }
            return lstSemiOrderSettingsInfo;
        }
        #endregion
    }
}
