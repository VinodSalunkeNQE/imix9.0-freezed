﻿using log4net;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DigiPhoto.Utility.Repository.ValueType;

namespace DigiPhoto.IMIX.DataAccess
{
    public class BaseDataAccess
    {
        #region Declaration
        private SqlConnection _conn = null;
        private SqlCommand _command = null;
        private SqlTransaction _trans = null;
        private SqlDataAdapter _adapter = null;
        //private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        #endregion

        #region Public Properties
        public List<SqlParameter> DBParameters { get; set; }

        public string MyConString
        {
            get
            {

                //ErrorHandler.ErrorHandler.LogFileWrite(ConfigurationManager.ConnectionStrings["DigiConnectionString"].ConnectionString);
                return ConfigurationManager.ConnectionStrings["DigiConnectionString"].ConnectionString;
            }
        }

        public SqlTransaction Transaction { get { return _trans; } }
        #endregion

        #region Constructor
        public BaseDataAccess()
        {
            DBParameters = new List<SqlParameter>();
            //log4net.Config.XmlConfigurator.Configure();
        }

        public BaseDataAccess(BaseDataAccess baseAccess)
        {
            DBParameters = new List<SqlParameter>();
            this._conn = baseAccess._conn;
            this._trans = baseAccess._trans;
            this._adapter = baseAccess._adapter;
        }
        #endregion

        #region Public Methods

        protected DataSet ExecuteDataSet(string spName)
        {
            try
            {
                DataSet recordsDs = new DataSet();
                using (SqlConnection sqlConnection = new SqlConnection(MyConString))
                {
                    sqlConnection.Open();
                    _command = sqlConnection.CreateCommand();
                    _command.CommandTimeout = 180;
                    _command.CommandText = spName;
                    _command.CommandType = CommandType.StoredProcedure;
                    if (DBParameters != null)
                        _command.Parameters.AddRange(DBParameters.ToArray());

                    if (_adapter == null)
                        _adapter = new SqlDataAdapter();
                    _adapter.SelectCommand = _command;
                    _adapter.Fill(recordsDs);

                    sqlConnection.Close();
                }
                return recordsDs;

            }
            catch (Exception ex)
            {
                //log.StartMethod();
                //if (ex.InnerException != null)
                //    log.Error("ExecuteDataSet: " + ex.Message + ex.InnerException + ex.StackTrace.ToString());
                //else
                //    log.Error("ExecuteDataSet: " + ex.Message + ex.StackTrace.ToString());
                //log.EndMethod();
                ErrorHandler.ErrorHandler.LogError(ex);
                throw ex;
            }
        }

        protected IDataReader ExecuteReader(string spName)
        {
            try
            {
                _conn = new SqlConnection(MyConString);
                _conn.Open();
                _command = _conn.CreateCommand();
                _command.CommandTimeout = 180;
                _command.CommandText = spName;
                _command.CommandType = CommandType.StoredProcedure;
                if (DBParameters != null)
                    _command.Parameters.AddRange(DBParameters.ToArray());
                return _command.ExecuteReader(CommandBehavior.CloseConnection);
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogFileWrite("SelectAllPhotosToBlobUpload : " + spName);
                ErrorHandler.ErrorHandler.LogError(ex);
                throw;
            }
        }

        protected object ExecuteScalar(string spName)
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(MyConString))
                {
                    conn.Open();
                    _command = conn.CreateCommand();
                    _command.CommandText = spName;
                    _command.CommandTimeout = 120;
                    _command.CommandType = CommandType.StoredProcedure;
                    if (DBParameters != null)
                        _command.Parameters.AddRange(DBParameters.ToArray());

                    var returnValue = _command.ExecuteScalar();
                    conn.Close();
                    return returnValue;

                }

            }
            catch (Exception ex)
            {
                //log.StartMethod();
                //if (ex.InnerException != null)
                //    log.Error("ExecuteScalar: " + ex.Message + ex.InnerException + ex.StackTrace.ToString());
                //else
                //    log.Error("ExecuteScalar: " + ex.Message + ex.StackTrace.ToString());
                //log.EndMethod();
                ErrorHandler.ErrorHandler.LogError(ex);
                throw ex;
            }
        }

        protected int ExecuteScalarDeletedOldImages(string spName)
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(MyConString))
                {
                    conn.Open();
                    _command = conn.CreateCommand();
                    _command.CommandText = spName;
                    _command.CommandTimeout = 120;
                    _command.CommandType = CommandType.StoredProcedure;
                    if (DBParameters != null)
                        _command.Parameters.AddRange(DBParameters.ToArray());

                    var returnValue = _command.ExecuteScalar();
                    int id = Convert.ToInt32(returnValue);
                    conn.Close();
                    return id;
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
                throw ex;
            }
        }

        //Added by KAILASH ON 25 09 2019 FOR EXECUTING SQL TEXT
        protected IDataReader ExecuteReader(string spName, CommandType commandType)
        {
            try
            {
                _conn = new SqlConnection(MyConString);
                _conn.Open();
                _command = _conn.CreateCommand();
                _command.CommandTimeout = 180;
                _command.CommandText = spName;
                _command.CommandType = commandType;
                if (DBParameters != null)
                    _command.Parameters.AddRange(DBParameters.ToArray());
                return _command.ExecuteReader(CommandBehavior.CloseConnection);

            }
            catch (Exception ex)
            {
                //log.StartMethod();
                //if (ex.InnerException != null)
                //    log.Error("ExecuteReader: " + ex.Message + ex.InnerException + ex.StackTrace.ToString());
                //else
                //    log.Error("ExecuteReader: " + ex.Message + ex.StackTrace.ToString());
                //log.EndMethod();
                ErrorHandler.ErrorHandler.LogError(ex);
                throw;
            }
        }
        //End
        //Added by KAILASH ON 25 09 2019 FOR EXECUTING SQL TEXT
        protected object ExecuteNonQuery(string commandText, CommandType commandType)
        {
            try
            {
                using (SqlConnection sqlConnection = new SqlConnection(MyConString))
                {
                    sqlConnection.Open();
                    _command = sqlConnection.CreateCommand();
                    _command.CommandText = commandText;
                    _command.CommandTimeout = 120;
                    _command.CommandType = commandType;
                    if (DBParameters != null)
                        _command.Parameters.AddRange(DBParameters.ToArray());
                    var returnValue = _command.ExecuteNonQuery();
                    sqlConnection.Close();
                    return returnValue;
                }

            }
            catch (Exception ex)
            {
                //log.StartMethod();
                //if (ex.InnerException != null)
                //    log.Error("ExecuteNonQuery: " + ex.Message + ex.InnerException + ex.StackTrace.ToString());
                //else
                //    log.Error("ExecuteNonQuery: " + ex.Message + ex.StackTrace.ToString());
                //log.EndMethod();
                ErrorHandler.ErrorHandler.LogError(ex);
                throw ex;
            }
        }
        //End
        protected object ExecuteNonQuery(string spName)
        {
            try
            {
                using (SqlConnection sqlConnection = new SqlConnection(MyConString))
                {
                    sqlConnection.Open();
                    _command = sqlConnection.CreateCommand();
                    _command.CommandText = spName;
                    _command.CommandTimeout = 120;
                    _command.CommandType = CommandType.StoredProcedure;
                    if (DBParameters != null)
                        _command.Parameters.AddRange(DBParameters.ToArray());
                    var returnValue = _command.ExecuteNonQuery();
                    sqlConnection.Close();
                    return returnValue;
                }
            }
            catch (Exception ex)
            {
                //log.StartMethod();
                //if (ex.InnerException != null)
                //    log.Error("ExecuteNonQuery: " + ex.Message + ex.InnerException + ex.StackTrace.ToString());
                //else
                //    log.Error("ExecuteNonQuery: " + ex.Message + ex.StackTrace.ToString());
                //log.EndMethod();
                ErrorHandler.ErrorHandler.LogError(ex);
                throw ex;
            }
        }
        protected object ExecuteNonQueryWithSqlDependency(string spName)
        {
            try
            {
                SqlDependency.Stop(MyConString);
                SqlDependency.Start(MyConString);

                using (SqlConnection sqlConnection = new SqlConnection(MyConString))
                {
                    sqlConnection.Open();
                    _command = sqlConnection.CreateCommand();
                    _command.CommandText = spName;
                    _command.CommandTimeout = 120;
                    _command.CommandType = CommandType.StoredProcedure;
                    if (DBParameters != null)
                        _command.Parameters.AddRange(DBParameters.ToArray());

                    _command.Notification = null;
                    SqlDependency dependency = new SqlDependency(_command);
                    // Add the event handler
                    dependency.OnChange += new OnChangeEventHandler(OnChange);

                    var returnValue = _command.ExecuteNonQuery();
                    sqlConnection.Close();
                    return returnValue;
                }
            }
            catch (Exception ex)
            {
                //log.StartMethod();
                //if (ex.InnerException != null)
                //    log.Error("ExecuteNonQuery: " + ex.Message + ex.InnerException + ex.StackTrace.ToString());
                //else
                //    log.Error("ExecuteNonQuery: " + ex.Message + ex.StackTrace.ToString());
                //log.EndMethod();
                ErrorHandler.ErrorHandler.LogError(ex);
                throw ex;
            }
        }
        public void OnChange(object sender, SqlNotificationEventArgs e)
        {
            //SqlDependency dependency = sender as SqlDependency;
            //invalid the cache entry
        }
        #endregion

        #region Parameters

        protected void AddParameter(string name, object value)
        {
            DBParameters.Add(new SqlParameter(name, value));
        }

        protected void AddParameter(string name, object value, ParameterDirection direction)
        {
            SqlParameter parameter = new SqlParameter(name, value);
            parameter.Direction = direction;
            DBParameters.Add(parameter);
        }
        protected void AddOutParameter(string name, SqlDbType dbType)
        {
            SqlParameter parameter = new SqlParameter(name, dbType);
            parameter.Direction = ParameterDirection.Output;
            DBParameters.Add(parameter);
        }
        protected void AddOutParameterString(string name, SqlDbType dbType, int size)
        {
            SqlParameter parameter = new SqlParameter(name, dbType, size);
            parameter.Direction = ParameterDirection.Output;
            DBParameters.Add(parameter);
        }
        protected object GetOutParameterValue(string parameterName)
        {
            if (_command != null)
            {
                return _command.Parameters[parameterName].Value;
            }
            return null;
        }
        #endregion

        #region SaveData
        protected bool SaveData(string spName)
        {
            try
            {
                _command = _conn.CreateCommand();
                _command.CommandTimeout = 180;
                _command.CommandText = spName;
                _command.CommandType = CommandType.StoredProcedure;
                _command.Parameters.AddRange(DBParameters.ToArray());

                int result = _command.ExecuteNonQuery();
                if (result > 0)
                {
                    return true;
                }
                else
                    return false;
            }
            catch (Exception ex)
            {
                //log.StartMethod();
                //if (ex.InnerException != null)
                //    log.Error("SaveData: " + ex.Message + ex.InnerException + ex.StackTrace.ToString());
                //else
                //    log.Error("SaveData: " + ex.Message + ex.StackTrace.ToString());
                //log.EndMethod();
                ErrorHandler.ErrorHandler.LogError(ex);
                throw ex;
            }
        }
        #endregion

        #region Transaction Members

        public bool BeginTransaction()
        {
            try
            {
                //bool IsOK = this.OpenConnection();

                //if (IsOK)
                _trans = _conn.BeginTransaction();
            }
            catch (Exception ex)
            {
                CloseConnection();
                //log.StartMethod();
                //if (ex.InnerException != null)
                //    log.Error("BeginTransaction: " + ex.Message + ex.InnerException + ex.StackTrace.ToString());
                //else
                //    log.Error("BeginTransaction: " + ex.Message + ex.StackTrace.ToString());
                //log.EndMethod();
                ErrorHandler.ErrorHandler.LogError(ex);
                throw ex;
            }

            return true;
        }

        public bool CommitTransaction()
        {
            try
            {
                _trans.Commit();
            }

            catch (Exception ex)
            {
                //log.StartMethod();
                //if (ex.InnerException != null)
                //    log.Error("CommitTransaction: " + ex.Message + ex.InnerException + ex.StackTrace.ToString());
                //else
                //    log.Error("CommitTransaction: " + ex.Message + ex.StackTrace.ToString());
                //log.EndMethod();
                ErrorHandler.ErrorHandler.LogError(ex);
                throw ex;
            }
            finally
            {
                this.CloseConnection();
            }
            return true;
        }

        public void RollbackTransaction()
        {
            try
            {
                _trans.Rollback();
            }
            catch (Exception ex)
            {
                //log.StartMethod();
                //if (ex.InnerException != null)
                //    log.Error("RollbackTransaction: " + ex.Message + ex.InnerException + ex.StackTrace.ToString());
                //else
                //    log.Error("RollbackTransaction: " + ex.Message + ex.StackTrace.ToString());
                //log.EndMethod();
                ErrorHandler.ErrorHandler.LogError(ex);
                throw ex;
            }
            finally
            {
                this.CloseConnection();
            }
            return;
        }

        public bool OpenConnection()
        {
            try
            {
                if (this._conn == null)
                    _conn = new SqlConnection(MyConString);

                if (this._conn.State != ConnectionState.Open)
                {
                    this._conn.Open();
                }
            }
            catch (Exception ex)
            {
                //log.StartMethod();
                //if (ex.InnerException != null)
                //    log.Error("OpenConnection: " + ex.Message + ex.InnerException + ex.StackTrace.ToString());
                //else
                //    log.Error("OpenConnection: " + ex.Message + ex.StackTrace.ToString());
                //log.EndMethod();
                ErrorHandler.ErrorHandler.LogError(ex);
                throw ex;
            }
            return true;
        }

        public bool CloseConnection()
        {
            try
            {
                if (_conn != null && this._conn.State != ConnectionState.Closed)
                    this._conn.Close();
            }
            catch (Exception ex)
            {

                //log.StartMethod();
                //if (ex.InnerException != null)
                //    log.Error("CloseConnection: " + ex.Message + ex.InnerException + ex.StackTrace.ToString());
                //else
                //    log.Error("CloseConnection: " + ex.Message + ex.StackTrace.ToString());
                //log.EndMethod();
                ErrorHandler.ErrorHandler.LogError(ex);
                throw ex;
            }
            finally
            {
                if (_conn != null)
                {
                    _conn.Dispose();
                    this._conn = null;
                }


            }
            return true;
        }

        #endregion

        #region Utility Functions
        protected long GetFieldValue(IDataReader sqlReader, string fieldName, long defaultValue)
        {
            int pos = sqlReader.GetOrdinal(fieldName);
            return sqlReader.IsDBNull(pos) ? 0L : sqlReader.GetInt64(pos);
        }

        protected int GetFieldValue(IDataReader sqlReader, string fieldName, int defaultValue)
        {
            int pos = sqlReader.GetOrdinal(fieldName);
            return sqlReader.IsDBNull(pos) ? 0 : sqlReader.GetInt32(pos);
        }
        protected int? GetFieldValueIntNull(IDataReader sqlReader, string fieldName, int defaultValue)
        {
            int pos = sqlReader.GetOrdinal(fieldName);
            return sqlReader.IsDBNull(pos) ? (int?)null : sqlReader.GetInt32(pos);
        }
        protected float GetFieldValue(IDataReader sqlReader, string fieldName, float defaultValue)
        {
            int pos = sqlReader.GetOrdinal(fieldName);
            //return sqlReader.IsDBNull(pos) ? 0 : sqlReader.GetFloat(pos);
            return sqlReader.IsDBNull(pos) ? 0 : (float)sqlReader.GetDouble(pos);
        }

        //Added on 3-march 
        protected double GetFieldValue(IDataReader sqlReader, string fieldName, double defaultValue)
        {
            int pos = sqlReader.GetOrdinal(fieldName);
            return sqlReader.IsDBNull(pos) ? 0 : sqlReader.GetDouble(pos);
        }
        protected decimal GetFieldValue(IDataReader sqlReader, string fieldName, decimal defaultValue)
        {
            int pos = sqlReader.GetOrdinal(fieldName);
            return sqlReader.IsDBNull(pos) ? 0 : sqlReader.GetDecimal(pos);
        }

        protected string GetFieldValue(IDataReader sqlReader, string fieldName, string defaultValue)
        {
            int pos = sqlReader.GetOrdinal(fieldName);
            return sqlReader.IsDBNull(pos) ? String.Empty : sqlReader.GetString(pos);
        }

        protected DateTime GetFieldValue(IDataReader sqlReader, string fieldName, DateTime defaultValue)
        {
            int pos = sqlReader.GetOrdinal(fieldName);
            return sqlReader.IsDBNull(pos) ? new DateTime() : sqlReader.GetDateTime(pos);
        }

        protected DateTime? GetFieldValueDateTimeNull(IDataReader sqlReader, string fieldName, DateTime defaultValue)
        {
            int pos = sqlReader.GetOrdinal(fieldName);
            return sqlReader.IsDBNull(pos) ? (DateTime?)null : sqlReader.GetDateTime(pos);
        }

        protected DateTime GetFieldValue(IDataReader sqlReader, string fieldName, DateTime? defaultValue)
        {
            int pos = sqlReader.GetOrdinal(fieldName);
            return sqlReader.IsDBNull(pos) ? new DateTime() : sqlReader.GetDateTime(pos);
        }
        protected bool GetFieldValue(IDataReader sqlReader, string fieldName, bool defaultValue)
        {
            int pos = sqlReader.GetOrdinal(fieldName);
            return sqlReader.IsDBNull(pos) ? false : sqlReader.GetBoolean(pos);
        }

        protected object SetNullIntegerValue(int? value)
        {
            object objVal = null;
            if (value == -1 || value == 0 || !value.HasValue)
                objVal = DBNull.Value;
            else
                objVal = value;
            return objVal;
        }
        //protected bool GetFieldValue(IDataReader sqlReader, string fieldName, bool defaultValue)
        //{
        //    object objVal = null;
        //    if (value == -1 || value == 0 || !value.HasValue)
        //        objVal = DBNull.Value;
        //    else
        //        objVal = value;
        //    return objVal;
        //}
        protected object SetNullLongValue(long? value)
        {
            object objVal = null;
            if (value == -1 || value == 0 || !value.HasValue)
                objVal = DBNull.Value;
            else
                objVal = value;
            return objVal;
        }
        protected object SetNullDoubleValue(double? value)
        {
            object objVal = null;
            if (value == -1 || value == 0 || !value.HasValue)
                objVal = DBNull.Value;
            else
                objVal = value;
            return objVal;
        }
        protected object SetNullBoolValue(bool? value)
        {
            object objVal = null;
            if (!value.HasValue)
                objVal = DBNull.Value;
            else
                objVal = value;
            return objVal;
        }
        protected object SetNullDecimalValue(Decimal? value)
        {
            object objVal = null;
            if (value == -1 || value == 0 || !value.HasValue)
                objVal = DBNull.Value;
            else
                objVal = value;
            return objVal;
        }
        protected object SetNullStringValue(string value)
        {
            object objVal = null;
            if (string.IsNullOrEmpty(value))
                objVal = DBNull.Value;
            else
                objVal = value.ToString();
            return objVal;
        }
        protected object SetNullDateTimeValue(DateTime? value)
        {
            object objVal = null;
            if (value == DateTime.MinValue || value == ("1/1/1900").ToDateTime())
                objVal = DBNull.Value;
            else
                objVal = value;
            return objVal;
        }
        #endregion
    }
}
