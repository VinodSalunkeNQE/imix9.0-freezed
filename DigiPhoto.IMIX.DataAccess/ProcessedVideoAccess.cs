﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DigiPhoto.IMIX.Model;
using System.Data;

namespace DigiPhoto.IMIX.DataAccess
{
    public class ProcessedVideoAccess:BaseDataAccess
    {
          #region Constructor
        public ProcessedVideoAccess(BaseDataAccess baseaccess)
            : base(baseaccess)
        {

        }

        public ProcessedVideoAccess()
        {

        }
        #endregion
        //Save Processed Video Details
        public int SaveProcessedVideoAndDetails(ProcessedVideoInfo processedvideo, DataTable dtPVDetails)
        {

            DBParameters.Clear();
            AddParameter("@ProcessedVideoId", processedvideo.ProcessedVideoId);
            AddParameter("@VideoId", processedvideo.VideoId);
            AddParameter("@Effects", processedvideo.Effects);
            AddParameter("@OutputVideoFileName", processedvideo.OutputVideoFileName);
            AddParameter("@CreatedBy", processedvideo.CreatedBy);
            AddParameter("@CreatedOn", processedvideo.CreatedOn);
            AddParameter("@ProductId", processedvideo.ProductId);
            AddParameter("@FirstMediaRFID", processedvideo.FirstMediaRFID);
            AddParameter("@SubStoreId", processedvideo.SubStoreId);
            AddParameter("@ProcessVideoDetailsValues", dtPVDetails);
            AddParameter("@VideoLength", processedvideo.VideoLength);
            AddParameter("@LastVideoId",0, ParameterDirection.Output);
            ExecuteNonQuery("SaveUpdateProcessedVideoDetails");
            return (int)GetOutParameterValue("@LastVideoId");
        }

        //Video Packages
        public List<VideoProducts> GetVideoPackages()
        {
            DBParameters.Clear();
            IDataReader sqlReader = ExecuteReader("GetVideoPackages");
            List<VideoProducts> videoProductList = PopulateVideoPackages(sqlReader);
            sqlReader.Close();
            return videoProductList;
        }
        private List<VideoProducts> PopulateVideoPackages(IDataReader sqlReader)
        {
            List<VideoProducts> videoProductList = new List<VideoProducts>();
            while (sqlReader.Read())
            {
                VideoProducts videoProduct = new VideoProducts();
                videoProduct.ProductID = GetFieldValue(sqlReader, "DG_Orders_ProductType_pkey", 0);
                videoProduct.ProductName = GetFieldValue(sqlReader, "DG_Orders_ProductType_Name", string.Empty);
                videoProduct.ProductLength = GetFieldValue(sqlReader, "DG_Video_Length", 0);
                videoProduct.ProductQuantity = GetFieldValue(sqlReader, "DG_Product_Quantity", 0);              
                videoProductList.Add(videoProduct);
            }
            return videoProductList;
        }
        
        //Processed Video Details
        public ProcessedVideoInfo GetProcessedVideoDetails(int VideoId)
        {
            DBParameters.Add(new System.Data.SqlClient.SqlParameter("VideoId", VideoId));
            IDataReader sqlReader = ExecuteReader("GetProcessedVideoDetails");
            ProcessedVideoInfo processedVideoProductList = PopulateProcessedVideo(sqlReader, VideoId);
            sqlReader.Close();
            return processedVideoProductList;
        }
        private ProcessedVideoInfo PopulateProcessedVideo(IDataReader sqlReader,int VideoId)
        {
            ProcessedVideoInfo processedVideo = new ProcessedVideoInfo();
            while (sqlReader.Read())
            {
                processedVideo.ProcessedVideoId = GetFieldValue(sqlReader, "ProcessedVideoId", 0L);
                processedVideo.VideoId = GetFieldValue(sqlReader, "VideoId", 0);
                processedVideo.OutputVideoFileName = GetFieldValue(sqlReader, "DG_Photos_FileName", string.Empty);
                processedVideo.CreatedOn = GetFieldValue(sqlReader, "DG_Photos_CreatedOn", DateTime.MinValue);
                processedVideo.FirstMediaRFID = GetFieldValue(sqlReader, "DG_Photos_RFID", string.Empty);
                processedVideo.CreatedBy = GetFieldValue(sqlReader, "DG_Photos_UserID", 0);
                processedVideo.SubStoreId = GetFieldValue(sqlReader, "DG_SubStoreId", 0);
                processedVideo.ProductId = GetFieldValue(sqlReader, "ProductId", 0);
                processedVideo.Effects = GetFieldValue(sqlReader, "Effects", string.Empty);
                processedVideo.VideoLength = GetFieldValue(sqlReader, "DG_VideoLength", 0L);
                processedVideo.HotFolderPath = GetFieldValue(sqlReader, "HotFolderPath", string.Empty);
                processedVideo.processedVideoItemsDetails = GetProcessedVideoItemsDetails(VideoId);
                
            }
            return processedVideo;
        }
        
        //Get Processed Item Details
        public List<ProcessedVideoDetailsInfo> GetProcessedVideoItemsDetails(int VideoId)
        {
            DBParameters.Clear();
            DBParameters.Add(new System.Data.SqlClient.SqlParameter("VideoId", VideoId));
            IDataReader sqlReader1 = ExecuteReader("GetProcessedVideoItemsDetails");
            List<ProcessedVideoDetailsInfo> processedVideoDetailsList = PopulateProcessedVideoItemsDetails(sqlReader1);
            return processedVideoDetailsList;
        }
        private List<ProcessedVideoDetailsInfo> PopulateProcessedVideoItemsDetails(IDataReader sqlReader)
        {
            List<ProcessedVideoDetailsInfo> processedVideoDetailsList = new List<ProcessedVideoDetailsInfo>();
            while (sqlReader.Read())
            {
                ProcessedVideoDetailsInfo pvDetailsInfo = new ProcessedVideoDetailsInfo();
                pvDetailsInfo.DisplayTime = GetFieldValue(sqlReader, "DisplayTime", 0);
                pvDetailsInfo.FrameTime = GetFieldValue(sqlReader, "FrameTime", 0);
                pvDetailsInfo.JoiningOrder = GetFieldValue(sqlReader, "JoiningOrder", 0);
                pvDetailsInfo.MediaId = GetFieldValue(sqlReader, "MediaId", 0L);
                pvDetailsInfo.MediaType = GetFieldValue(sqlReader, "MediaType", 0);
                pvDetailsInfo.ProcessedVideoDetailId = GetFieldValue(sqlReader, "ProcessedVideoDetailId", 0L);
                pvDetailsInfo.ProcessedVideoId = GetFieldValue(sqlReader, "VideoId", 0);
                pvDetailsInfo.StartTime = GetFieldValue(sqlReader, "StartTime", 0);
                pvDetailsInfo.EndTime= GetFieldValue(sqlReader, "EndTime", 0);
                processedVideoDetailsList.Add(pvDetailsInfo);
            }
            return processedVideoDetailsList;
        }

        //Get File Information
        public FilePhotoInfo GetFileInfo(int photos_Key)
        {
            DBParameters.Add(new System.Data.SqlClient.SqlParameter("Photo_Key", photos_Key));
            IDataReader sqlReader = ExecuteReader("GetFileFromPhoto_Key");
            FilePhotoInfo fileInfoList = PopulateFileInfo(sqlReader);
            sqlReader.Close();
            return fileInfoList;
        }
        private FilePhotoInfo PopulateFileInfo(IDataReader sqlReader)
        {
            FilePhotoInfo fInfo = new FilePhotoInfo();
            while (sqlReader.Read())
            {
                fInfo.Photo_RFID = GetFieldValue(sqlReader, "DG_Photos_RFID", string.Empty);
                fInfo.FileName = GetFieldValue(sqlReader, "DG_Photos_FileName", string.Empty);
                fInfo.CreatedOn = GetFieldValue(sqlReader, "DG_Photos_CreatedOn", DateTime.MinValue);
                fInfo.HotFolderPath = GetFieldValue(sqlReader, "HotFolderPath", string.Empty);  
            }
            return fInfo;
        }

        public List<ProcessedVideoInfo> GetProcessedVideosByPackageId(int packageID)
        {
            DBParameters.Add(new System.Data.SqlClient.SqlParameter("@PackageId", packageID));
            IDataReader sqlReader = ExecuteReader("GetProcessedVideosByPackageID");
            List<ProcessedVideoInfo> processedVideoList = PopulatePVListByPackageID(sqlReader);
            sqlReader.Close();
            return processedVideoList;
        }

        private List<ProcessedVideoInfo> PopulatePVListByPackageID(IDataReader sqlReader)
        {
            List<ProcessedVideoInfo> processedVideoList = new List<ProcessedVideoInfo>();
            while (sqlReader.Read())
            {
                ProcessedVideoInfo obj = new ProcessedVideoInfo();
                obj.VideoId = GetFieldValue(sqlReader, "DG_Photos_pkey", 0);
                processedVideoList.Add(obj);
            }
            return processedVideoList;
        }

    }
}
