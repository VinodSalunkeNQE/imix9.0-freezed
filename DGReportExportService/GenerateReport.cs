﻿using DigiPhoto.DataLayer;
using DigiPhoto.IMIX.Business;
using DigiPhoto.IMIX.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DigiPhoto.ExtensionMethods;
namespace DGReportExportService
{
    public class GenerateReport
    {
        const string DateFormat = "yyyyddMMhhmm";
        public static void ExportToCsv(ReportTypes reportType, ReportConfigurationDetails configDetails)
        {
            LoggerHelper.LogError("GenerateReport", "ExportToCsv", "Export of "+reportType.ToString()+" from " + DateTime.Now.AddDays(-1) + "  to " + DateTime.Now + " begins");
            ExportServiceLog reportResult = new ExportServiceLog()
            {
                ErrorDetails = string.Empty,
                EventTime = DateTime.Now,
                ExportFile = string.Empty,
                ReportSent = false,
                ReportType = reportType.ToString(),
                ExportPath = string.Empty
            };
            try
            {

                switch (reportType)
                {
                    case ReportTypes.ProductionSummaryReport:
                        reportResult = ProductionSummaryReport(configDetails);
                        break;
                    case ReportTypes.SitePerformanceReport:
                        reportResult = ReportLocationPerformance(configDetails);
                        break;
                    case ReportTypes.TakingReport:
                        reportResult = ReportTaking(configDetails);
                        break;
                    case ReportTypes.PaymentSummary:
                        reportResult = PaymentSummaryReport(configDetails);
                        break;
                    case ReportTypes.IPPrintTrackingReport:
                        reportResult = GenerateIPPrintTrackingReport(configDetails);
                        break;
                }

            }
            catch (Exception ex)
            {
                LoggerHelper.LogError(ex);
                reportResult.ReportSent = false;
                reportResult.EventTime = DateTime.Now;
                reportResult.ErrorDetails = "The export to CSV of this report has been failed. The exception : " + ex.Message;
                reportResult.ReportType = reportType.ToString();
            }
            // here log the erport log 
            ConfigBusiness conFigBusiness = new ConfigBusiness();
            try
            {
                conFigBusiness.SaveExportReportLogDetails(reportResult);
            }
            catch (Exception ex1)
            {
                LoggerHelper.LogError(ex1);
            }
            LoggerHelper.LogError("GenerateReport", "ExportToCsv", "Export of " + reportType.ToString() + " from " + DateTime.Now.AddDays(-1) + "  to " + DateTime.Now + " ends");
        }

        
        #region Reports
        static ExportServiceLog ProductionSummaryReport(ReportConfigurationDetails storeConfig)
        {
            LoggerHelper.LogError("GenerateReport", "ProductionSummaryReport", "Export of Production summary report from " + DateTime.Now.AddDays(-1) + "  to " + DateTime.Now + " begins");
            ExportServiceLog reportResult = new ExportServiceLog()
            {
                ErrorDetails = "The export to CSV of this report has been failed.No such orders have been placed between two days",
                EventTime = DateTime.Now,
                ExportFile = string.Empty,
                ReportSent = false,
                ReportType = ReportTypes.ProductionSummaryReport.ToString(),
                ExportPath = string.Empty
            };
            DateTime fromDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 00, 00, 00).AddDays(-1);
            DateTime toDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 23, 59, 00);
           
            try
            {
                ProductBusiness productBusiness = new ProductBusiness();
                DataSet result = productBusiness.GetProductSummary(fromDate, toDate, storeConfig.StoreName, "", 0);
                if (result!=null && result.Tables.Count > 0 && result.Tables[0].Rows.Count > 0)
                {
                    var reportData = new ConfigBusiness().CreateReportData(result.Tables[0], DigiPhoto.DataLayer.ReportTypes.ProductionSummaryReport);
                    var fileName = string.Format("{0}_{1}_{2}.csv", storeConfig.StoreName,ReportTypes.ProductionSummaryReport.ToString(), DateTime.Now.ToString(DateFormat));
                    if (System.IO.Directory.Exists(storeConfig.ExportPath))
                    {
                        var filePath = storeConfig.ExportPath + fileName;
                        string exceptionMessage;
                        bool isExported = reportData.ExportToCSV("vw_GetActivityReports.DG_", filePath, out exceptionMessage);
                        reportResult.ErrorDetails = (isExported) ? "The report has been successfully exported in csv format" : "The export to CSV of this report has been failed. The exception : " + exceptionMessage;
                        reportResult.ExportFile = fileName;
                        reportResult.ExportPath = filePath;
                        reportResult.ReportSent = isExported;                        
                    }
                    else
                    {
                        reportResult.ErrorDetails = "The export to CSV of this report has been failed. The exception : " + "The export path : " + storeConfig.ExportPath + " doesn't exist";
                    }

                    reportResult.EventTime = DateTime.Now;
                    
                }
                if (!reportResult.ReportSent)
                    EmailManagement.SaveEmailDetails(reportResult.ErrorDetails, storeConfig.EmailAddress, ReportTypes.ProductionSummaryReport.ToString());
               
            }
            catch (Exception ex)
            {
                LoggerHelper.LogError(ex);
                reportResult.ReportSent = false;
                reportResult.EventTime = DateTime.Now;
                reportResult.ErrorDetails = "The export to CSV of this report has been failed. The exception : " + ex.Message;
                EmailManagement.SaveEmailDetails(ex.Message + "\n" + ex.StackTrace, storeConfig.EmailAddress, ReportTypes.ProductionSummaryReport.ToString());
            }
            LoggerHelper.LogError("GenerateReport", "ProductionSummaryReport", "Export of Production summary report from " + DateTime.Now.AddDays(-1) + "  to " + DateTime.Now + " ends");
            return reportResult;

        }

        static ExportServiceLog ReportLocationPerformance(ReportConfigurationDetails storeConfig)
        {
            LoggerHelper.LogError("GenerateReport", "ReportLocationPerformance", "Export of Report Location Performance report from " + DateTime.Now.AddDays(-1) + "  to " + DateTime.Now + " begins");
            ExportServiceLog reportResult = new ExportServiceLog()
            {
                ErrorDetails = "The export to CSV of this report has been failed.No such orders have been placed between two days",
                EventTime = DateTime.Now,
                ExportFile = string.Empty,
                ReportSent = false,
                ReportType = ReportTypes.SitePerformanceReport.ToString(),
                ExportPath = string.Empty
            };
            try
            {
                DateTime fromDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 00, 00, 00).AddDays(-1);
                DateTime toDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 23, 59, 00);


                ReportBusiness repBiz = new ReportBusiness();
                var storeSubStoreBusiness = new StoreSubStoreDataBusniess();

                DataSet result = repBiz.GetLocationPerformanceReports(fromDate, toDate, fromDate, toDate, storeConfig.StoreName, "", false, "", 0);
               
                if (result !=null && result.Tables.Count > 0 && result.Tables[0].Rows.Count > 0)
                {
                    var reportData = new ConfigBusiness().CreateReportData(result.Tables[0], DigiPhoto.DataLayer.ReportTypes.SitePerformanceReport);
                    var fileName = string.Format("{0}_{1}_{2}.csv", storeConfig.StoreName, ReportTypes.SitePerformanceReport.ToString(), DateTime.Now.ToString(DateFormat));
                    if (System.IO.Directory.Exists(storeConfig.ExportPath))
                    {
                        var filePath = storeConfig.ExportPath + fileName;
                        string exceptionMessage;
                        bool isExported = reportData.ExportToCSV("vw_GetActivityReports.DG_", filePath, out exceptionMessage);
                        reportResult.ErrorDetails = (isExported) ? "The report has been successfully exported in csv format" : "The export to CSV of this report has been failed. The exception : " + exceptionMessage;
                      
                      
                        reportResult.ExportFile = fileName;
                        reportResult.ExportPath = filePath;
                        reportResult.ReportSent = isExported;
                    }
                    else
                    {
                        reportResult.ErrorDetails = "The export to CSV of this report has been failed. The exception : " + "The export path : " + storeConfig.ExportPath + " doesn't exist";
                    }
                    reportResult.EventTime = DateTime.Now;
                   
                }
                if (!reportResult.ReportSent)
                    EmailManagement.SaveEmailDetails(reportResult.ErrorDetails, storeConfig.EmailAddress, ReportTypes.SitePerformanceReport.ToString());

            }
            catch (Exception ex)
            {
                LoggerHelper.LogError(ex);
                reportResult.ReportSent = false;
                reportResult.EventTime = DateTime.Now;
                reportResult.ErrorDetails = "The export to CSV of this report has been failed. The exception : " + ex.Message;
                EmailManagement.SaveEmailDetails(ex.Message + "\n" + ex.StackTrace, storeConfig.EmailAddress, ReportTypes.SitePerformanceReport.ToString());
            }
           LoggerHelper.LogError("GenerateReport", "ReportLocationPerformance", "Export of Report Location Performance report from " + DateTime.Now.AddDays(-1) + "  to " + DateTime.Now + " ends");
            return reportResult;

        }

        static ExportServiceLog ReportTaking(ReportConfigurationDetails storeConfig)
        {
            LoggerHelper.LogError("GenerateReport", "TakingReport", "Export of Report taking  report from " + DateTime.Now.AddDays(-1) + "  to " + DateTime.Now + " begins");
            ExportServiceLog reportResult = new ExportServiceLog()
            {
                ErrorDetails = "The export to CSV of this report has been failed.No such orders have been placed between two days",
                EventTime = DateTime.Now,
                ExportFile = string.Empty,
                ReportSent = false,
                ReportType = ReportTypes.TakingReport.ToString(),
                ExportPath = string.Empty
            };
            DateTime fromDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 00, 00, 00).AddDays(-1);
            DateTime toDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 23, 59, 00);

            try
            {

                ReportBusiness repBiz = new ReportBusiness();
                DataSet result = repBiz.GetTakingReport(true, true, fromDate, toDate, 0, 0);

                if (result!=null && result.Tables.Count > 0 && result.Tables[0].Rows.Count > 0)
                {
                    result.Tables[0].Columns.Add(new DataColumn("StoreName"));
                    foreach (DataRow dr in result.Tables[0].Rows)
                    {
                        dr["StoreName"] = storeConfig.StoreName;
                    }
                    var reportData = new ConfigBusiness().CreateReportData(result.Tables[0], ReportTypes.TakingReport);
                    var fileName = string.Format("{0}_{1}_{2}.csv", storeConfig.StoreName, ReportTypes.TakingReport.ToString(), DateTime.Now.ToString(DateFormat));
                    if (System.IO.Directory.Exists(storeConfig.ExportPath))
                    {
                        var filePath = storeConfig.ExportPath + fileName;
                        string exceptionMessage;
                        bool isExported = reportData.ExportToCSV("vw_GetActivityReports.DG_", filePath, out exceptionMessage);
                        reportResult.ErrorDetails = (isExported) ? "The report has been successfully exported in csv format" : "The export to CSV of this report has been failed. The exception : " + exceptionMessage;
                        reportResult.EventTime = DateTime.Now;
                        reportResult.ExportFile = fileName;
                        reportResult.ExportPath = filePath;
                        reportResult.ReportSent = isExported;
                    }
                    else
                    {
                        reportResult.ErrorDetails = "The export to CSV of this report has been failed. The exception : " + "The export path : " + storeConfig.ExportPath + " doesn't exist";
                    }
                    reportResult.EventTime = DateTime.Now;
                }
                if (!reportResult.ReportSent)
                    EmailManagement.SaveEmailDetails(reportResult.ErrorDetails, storeConfig.EmailAddress, ReportTypes.TakingReport.ToString());
            }
            catch (Exception ex)
            {
                LoggerHelper.LogError(ex);
                reportResult.ReportSent = false;
                reportResult.EventTime = DateTime.Now;
                reportResult.ErrorDetails = "The export to CSV of this report has been failed. The exception : " + ex.Message;
                EmailManagement.SaveEmailDetails(ex.Message + "\n" + ex.StackTrace, storeConfig.EmailAddress, ReportTypes.TakingReport.ToString());
            }
            LoggerHelper.LogError("GenerateReport", "TakingReport", "Export of Report taking  report from " + DateTime.Now.AddDays(-1) + "  to " + DateTime.Now + " ends");
            return reportResult;
        }
        static ExportServiceLog PaymentSummaryReport(ReportConfigurationDetails storeConfig)
        {
           LoggerHelper.LogError("GenerateReport", "PaymentSummaryReport", "Export of Payment Summary Report report from " + DateTime.Now.AddDays(-1) + "  to " + DateTime.Now + " begins");
            ExportServiceLog reportResult = new ExportServiceLog()
            {
                ErrorDetails = "The export to CSV of this report has been failed.No such orders have been placed between two days",
                EventTime = DateTime.Now,
                ExportFile = string.Empty,
                ReportSent = false,
                ReportType = ReportTypes.PaymentSummary.ToString(),
                ExportPath = string.Empty
            };
            try
            {
                var reportBusiness = new ReportBusiness();

                DateTime FromDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 00, 00, 00).AddDays(-1);
                DateTime ToDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 23, 59, 00);
                List<PaymentSummaryInfo> result = reportBusiness.GetPaymentSummary(FromDate, ToDate, storeConfig.StoreName.ToString());
              
                if (result!=null && result.Count > 0)
                {
                    var reportData = new ConfigBusiness().ParseListIntoPaymentDataTable(result);
                    var fileName = string.Format("{0}_{1}_{2}.csv", storeConfig.StoreName, ReportTypes.PaymentSummary.ToString(), DateTime.Now.ToString(DateFormat));
                    if (System.IO.Directory.Exists(storeConfig.ExportPath))
                    {
                        var filePath = storeConfig.ExportPath + fileName;
                        string exceptionMessage;
                        bool isExported = reportData.ExportToCSV("vw_GetActivityReports.DG_", filePath, out exceptionMessage);
                        reportResult.ErrorDetails = (isExported) ? "The report has been successfully exported in csv format" : "The export to CSV of this report has been failed. The exception : " + exceptionMessage;
                        reportResult.EventTime = DateTime.Now;
                        reportResult.ExportFile = fileName;
                        reportResult.ExportPath = filePath;
                        reportResult.ReportSent = isExported;
                    }
                    else
                    {
                        reportResult.ErrorDetails = "The export to CSV of this report has been failed. The exception : " + "The export path : " + storeConfig.ExportPath + " doesn't exist";
                    }
                    reportResult.EventTime = DateTime.Now;
                }
                if (!reportResult.ReportSent)
                    EmailManagement.SaveEmailDetails(reportResult.ErrorDetails, storeConfig.EmailAddress, ReportTypes.PaymentSummary.ToString());

            }
            catch (Exception ex)
            {
                LoggerHelper.LogError(ex);
                reportResult.ReportSent = false;
                reportResult.EventTime = DateTime.Now;
                reportResult.ErrorDetails = "The export to CSV of this report has been failed. The exception : " + ex.Message;
                EmailManagement.SaveEmailDetails(ex.Message + "\n" + ex.StackTrace, storeConfig.EmailAddress, ReportTypes.PaymentSummary.ToString());
            }
            LoggerHelper.LogError("GenerateReport", "PaymentSummaryReport", "Export of Payment Summary Report report from " + DateTime.Now.AddDays(-1) + "  to " + DateTime.Now + " ends");
            return reportResult;

        }

        static ExportServiceLog GenerateIPPrintTrackingReport(ReportConfigurationDetails storeConfig)
        {
            LoggerHelper.LogError("GenerateReport", "IPPrintTrackingReport", "Export of IP Print Tracking Report from " + DateTime.Now.AddDays(-1) + "  to " + DateTime.Now + " begins");
            ExportServiceLog reportResult = new ExportServiceLog()
            {
                ErrorDetails = "The export to CSV of this report has been failed.No such orders have been placed between two days",
                EventTime = DateTime.Now,
                ExportFile = string.Empty,
                ReportSent = false,
                ReportType = ReportTypes.IPPrintTrackingReport.ToString(),
                ExportPath = string.Empty
            };
            try
            {
                var reportBusiness = new ReportBusiness();

                DateTime FromDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 00, 00, 00).AddDays(-1);
                DateTime ToDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 23, 59, 00);
                DataSet DataIPPrintTracking = reportBusiness.GetDataForIPPrintTracking(FromDate, ToDate, 0, 0);

                if (DataIPPrintTracking != null && DataIPPrintTracking.Tables.Count > 0)
                {
                    #region to be moved to business
                    string columnToLeave = "Order Date,Order No,Site,Package,Sell Price,Payment Mode,DG_Orders_LineItems_pkey";
                    int index = 0;
                    DataIPPrintTracking.Tables[0].Columns.Remove("DG_Orders_LineItems_pkey");
                    DataRow dr = DataIPPrintTracking.Tables[0].NewRow();
                    dr[5] = "Total";
                    foreach (DataColumn column in DataIPPrintTracking.Tables[0].Columns)
                    {
                        int i = DataIPPrintTracking.Tables[0].Columns.Count;
                        if (!columnToLeave.Contains(column.ColumnName))
                        {
                            dr[index] = DataIPPrintTracking.Tables[0].Compute("sum([" + column.ColumnName + "])", "");
                            if (column.ColumnName.ToLower() != "total")
                                column.ColumnName = column.ColumnName + "(No of Images)";

                        }
                        index++;

                    }
                    DataIPPrintTracking.Tables[0].Rows.Add(dr);
                    #endregion
                    var fileName = string.Format("{0}_{1}_{2}.csv", storeConfig.StoreName, ReportTypes.IPPrintTrackingReport.ToString(), DateTime.Now.ToString(DateFormat));
                    if (System.IO.Directory.Exists(storeConfig.ExportPath))
                    {
                        var filePath = storeConfig.ExportPath + fileName;
                        string exceptionMessage;
                        bool isExported = DataIPPrintTracking.Tables[0].ExportToCSV("vw_GetActivityReports.DG_", filePath, out exceptionMessage);
                        reportResult.ErrorDetails = (isExported) ? "The report has been successfully exported in csv format" : "The export to CSV of this report has been failed. The exception : " + exceptionMessage;
                        reportResult.EventTime = DateTime.Now;
                        reportResult.ExportFile = fileName;
                        reportResult.ExportPath = filePath;
                        reportResult.ReportSent = isExported;
                    }
                    else
                    {
                        reportResult.ErrorDetails = "The export to CSV of this report has been failed. The exception : " + "The export path : " + storeConfig.ExportPath + " doesn't exist";
                    }
                    reportResult.EventTime = DateTime.Now;
                }
                if (!reportResult.ReportSent)
                    EmailManagement.SaveEmailDetails(reportResult.ErrorDetails, storeConfig.EmailAddress, ReportTypes.IPPrintTrackingReport.ToString());

            }
            catch (Exception ex)
            {
                LoggerHelper.LogError(ex);
                reportResult.ReportSent = false;
                reportResult.EventTime = DateTime.Now;
                reportResult.ErrorDetails = "The export to CSV of this report has been failed. The exception : " + ex.Message;
                EmailManagement.SaveEmailDetails(ex.Message + "\n" + ex.StackTrace, storeConfig.EmailAddress, ReportTypes.IPPrintTrackingReport.ToString());
            }
            LoggerHelper.LogError("GenerateReport", "IPPrintTrackingReport", "Export of IP Print Tracking Report from " + DateTime.Now.AddDays(-1) + "  to " + DateTime.Now + " ends");
            return reportResult;
        }
        #endregion
    }
  



}
