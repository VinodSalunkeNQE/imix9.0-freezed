﻿using DigiPhoto.DataLayer;
using DigiPhoto.IMIX.Business;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace DGReportExportService
{
    public class EmailManagement
    {
        public static string FromMail
        {
            get
            {
                return Convert.ToString(ConfigurationManager.AppSettings["MailSendFrom"]);
            }
        }
        public static string MailSubject
        {
            get
            {
                return Convert.ToString(ConfigurationManager.AppSettings["MailSubject"]);
            }
        }
        public static string MailBody
        {
            get
            {
                return Convert.ToString(ConfigurationManager.AppSettings["MailBody"]);
            }
        }
        public static string SmtpServerName
        {
            get
            {
                return Convert.ToString(ConfigurationManager.AppSettings["SmtpServerName"]);
            }
        }
        public static int Port
        {
            get
            {
                return Convert.ToInt32(ConfigurationManager.AppSettings["SmtpServerPort"]);
            }
        }
        public static bool UseDefaultCredentials { get { return Convert.ToBoolean(ConfigurationManager.AppSettings["SmtpServerUseDefaultCredentials"]); } }
        public static  string UserName
        {
            get
            {
               
                return Convert.ToString(ConfigurationManager.AppSettings["SmtpServerUsername"]);
            }
        }
        public static string Password
        {
            get
            {
                return Convert.ToString(ConfigurationManager.AppSettings["SmtpServerPassword"]);
            }
        }
        public static bool EnableSSl { get { return Convert.ToBoolean(ConfigurationManager.AppSettings["SmtpServerEnableSSL"]); } }
        public static int RetryCount { get { return Convert.ToInt32(ConfigurationManager.AppSettings["MailRetryCount"]); } }
        /// <summary>
        /// Sends the mail.
        /// </summary>
        /// <param name="from">From.</param>
        /// <param name="to">The automatic.</param>
        /// <param name="Bcc">The BCC.</param>
        /// <param name="subject">The subject.</param>
        /// <param name="body">The body.</param>
        /// <param name="attachment">The attachment.</param>
        /// <param name="smtpservername">The smtpservername.</param>
        /// <param name="smtpportno">The smtpportno.</param>
        /// <param name="defaultcredentials">if set to <c>true</c> [defaultcredentials].</param>
        /// <param name="uname">The uname.</param>
        /// <param name="password">The password.</param>
        /// <param name="enabelSSl">if set to <c>true</c> [enabel arguments sl].</param>
        public static int SendMail(string toEmail,string reportType,string errorDetails)
        {
            int MailStatus = 0;
            try
            {
                //here we will close the db connection and reset the timer  
                if (String.IsNullOrWhiteSpace(FromMail) || String.IsNullOrWhiteSpace(toEmail))
                {
                    MailStatus = 5;
                }
                else
                {
                    MailAddress mailfrom = new MailAddress(FromMail);
                    MailAddress mailto = new MailAddress(toEmail);
                    MailMessage newmsg = new MailMessage(mailfrom, mailto);
                    //if (Bcc != "" && Bcc != null)
                    //{
                    //    MailAddress bcc = null;
                    //    foreach (string bc in Bcc.Split(','))
                    //    {
                    //        bcc = new MailAddress(bc);
                    //        newmsg.Bcc.Add(bcc);
                    //    }
                    //}
                    string mailBody = MailBody;
                    mailBody = mailBody.Replace("{b}", "<br/>");
                    mailBody = mailBody.Replace("{ReportType}", reportType);
                    mailBody = mailBody.Replace("{errorDetails}", errorDetails);
                    newmsg.Subject = MailSubject;
                    newmsg.Body = MailBody;
                    newmsg.IsBodyHtml = true;
                    newmsg.Priority = MailPriority.High;
                                      

                    SmtpClient smtp = new SmtpClient(SmtpServerName);
                    smtp.Port = Port;
                    smtp.UseDefaultCredentials = UseDefaultCredentials;

                    for (int a = 1; a <= RetryCount; a++)
                    {
                        if (SendToSMTP(UseDefaultCredentials, UserName, Password, EnableSSl, newmsg, smtp, "", ""))
                        {
                            MailStatus = 1;
                            break;
                        }
                        else if (a == RetryCount)
                        {
                            MailStatus = 2;
                        }
                    }
                }
                return MailStatus;
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);                
                return MailStatus = 2;
            }
        }
        static bool PingTest()
        {
            try
            {
                System.Net.NetworkInformation.Ping ping = new System.Net.NetworkInformation.Ping();
                System.Net.NetworkInformation.PingReply pingStatus = ping.Send("www.google.com", 1000);

                if (pingStatus.Status == System.Net.NetworkInformation.IPStatus.Success)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                return false;
            }
        }
        static bool SendToSMTP(bool defaultcredentials, string uname, string password, bool enabelSSl, MailMessage newmsg, SmtpClient smtp, string logorderno, string LogEmalOrderno)
        {
            try
            {
                if (PingTest())
                {
                    if (!defaultcredentials)
                    {
                        smtp.Credentials = new NetworkCredential(uname, password);
                        smtp.EnableSsl = enabelSSl;
                        smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
                        smtp.Timeout = 700000;
                        smtp.Send(newmsg);
                    }
                    else
                    {
                        smtp.EnableSsl = enabelSSl;
                        smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
                        smtp.Timeout = 700000;
                        smtp.Send(newmsg);
                    }
                    return true;
                }
                else
                {
                    ErrorHandler.ErrorHandler.LogFileWrite("Ping Test Failed. Now trying emailing without Ping Test.");
                    try
                    {
                        if (!defaultcredentials)
                        {
                            smtp.Credentials = new NetworkCredential(uname, password);
                            smtp.EnableSsl = enabelSSl;
                            smtp.Timeout = 700000;
                            smtp.Send(newmsg);
                        }
                        else
                        {
                            smtp.EnableSsl = enabelSSl;
                            smtp.Timeout = 700000;
                            smtp.Send(newmsg);
                        }
                        return true;
                    }
                    catch (Exception ex)
                    {                        
                        
                        string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                        ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);                        
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {               
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                return false;
            }
        }
        public static bool SaveEmailDetails(string exceptionMsg, string ToAddress,string reportType)
        {
            ConfigBusiness buss = new ConfigBusiness();
            var mailMessage = MailBody;
            mailMessage = mailMessage.Replace("{br}", "<br/>");
            mailMessage = mailMessage.Replace("{ReportType}", reportType);
            mailMessage = mailMessage.Replace("{data}", exceptionMsg);
            Dictionary<long, string> iMIXConfigurations = new Dictionary<long, string>();           
            if (!string.IsNullOrEmpty(ToAddress))
            {
                bool success = buss.SaveEmailDetails(ToAddress, string.Empty, string.Empty, mailMessage, "CSV EXPORT SERVICE EXCEPTION", 0);
            }
            return true;
        }

    }
}
