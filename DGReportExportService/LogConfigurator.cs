﻿using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DGReportExportService
{
    public class LogConfigurator
    {
        /// <summary>
        /// Configures the log4net.
        /// </summary>
        static LogConfigurator()
        {
            try
            {
                log4net.Config.XmlConfigurator.Configure();
            }
            catch(Exception ex)
            {

            }
        }
        public static ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
    }
}
