﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DGReportExportService
{
    public class LoggerHelper
    {
        public static void LogError(Exception ex)
        {
            string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
            ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            //new EventLog().WriteEntry(errorMessage);            

        }
        public static void LogError(string className,string methodName,string message)
        {
            string errorMessage = string.Format("Application name : DGReportExportService,Class name : " + className + " Method : " + methodName + " message : " + message);
            ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);            

        }
    }
}
