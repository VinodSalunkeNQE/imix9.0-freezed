﻿using DigiPhoto.DataLayer;
using DigiPhoto.IMIX.Business;
using DigiPhoto.IMIX.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using DigiPhoto.ExtensionMethods;
using System.Xml.Linq;

namespace DGReportExportService
{
    public partial class DigiReportExportService : ServiceBase
    {
        #region variables
        Thread dataLoadingThread;
        Thread exportProcessThread;
        Thread mainThread;
        bool isfirstime;
        #endregion
        public DigiReportExportService()
        {
            try
            {
                InitializeComponent();
                isfirstime = true;

            }
            catch (Exception ex)
            {
                LoggerHelper.LogError(ex);
            }

        }

        static ReportConfigurationDetails ReportConfigurationDetails { get; set; }
     
        public void CallingThread()
        {
            try
            {
                dataLoadingThread = new Thread(LoadConfiguration);
                exportProcessThread = new Thread(GetReportsExported);
                dataLoadingThread.Start();
                dataLoadingThread.Join();
                exportProcessThread.Start();

            }
            catch (Exception ex)
            {
                LoggerHelper.LogError(ex);
            }

        }

        private void LoadConfiguration()
        {
            try
            {
                ConfigBusiness configBusiness = new ConfigBusiness();
                if (configBusiness != null)
                {
                    ReportConfigurationDetails = configBusiness.GetReportConfigurationDetails();
                    if (ReportConfigurationDetails != null)
                    {
                        UserBusiness useBiz = new UserBusiness();
                        if (useBiz != null)
                        {
                            var storeDetails = new StoreSubStoreDataBusniess().GetStore();
                            if(storeDetails!=null)
                            {

                                ReportConfigurationDetails.StoreId = storeDetails.DG_Store_pkey;
                                ReportConfigurationDetails.StoreName = storeDetails.DG_Store_Name;
                            }
                            
                        }
                    }

                }

            }
            catch (Exception ex)
            {
                LoggerHelper.LogError(ex);
                exportProcessThread.Abort();
                this.Stop();
            }
        }

       

        void ExportReports()
        {
            try
            {
                foreach (var reportType in ReportConfigurationDetails.ReportTypeDetails)
                {
                    if (reportType.IsActive)
                    {
                        ReportTypes rptType=ReportTypes.NONE;
                        if(Enum.TryParse(reportType.ReportTypeName,out rptType))
                        {
                            GenerateReport.ExportToCsv(rptType, ReportConfigurationDetails);
                        }
                        else
                        {
                            LoggerHelper.LogError("DigiReportExportService", "ExportReports", "Unable to parse " + reportType.ReportTypeName);
                        }                        
                        
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerHelper.LogError(ex);
            }
        }

        private void GetReportsExported()
        {
            try
            {
               

                while(true)
                {
                    TimeSpan sleepingTime = new TimeSpan(0,0,0);
                    
                    if (ReportConfigurationDetails != null)
                    {
                        if(!ReportConfigurationDetails.HasError)
                        {
                            TimeSpan scheduledTime = new TimeSpan(ReportConfigurationDetails.SelectedHour, ReportConfigurationDetails.SelectedMinute, 0);
                            TimeSpan currentTime = new TimeSpan(DateTime.Now.Hour, DateTime.Now.Minute,DateTime.Now.Second);
                            bool timingMatched = (scheduledTime.Hours == currentTime.Hours) && (scheduledTime.Minutes == currentTime.Minutes);
                            if(isfirstime && !timingMatched)
                            {
                                if (scheduledTime > currentTime)
                                {
                                    sleepingTime = (scheduledTime - currentTime);
                                }
                                else
                                {
                                    sleepingTime = currentTime - scheduledTime;
                                    sleepingTime = new TimeSpan(24, 0, 0) - sleepingTime;
                                }
                                isfirstime = false;
                            }
                            else if (!isfirstime && !timingMatched)
                            {
                                if (scheduledTime > currentTime)
                                {
                                    sleepingTime = (scheduledTime - currentTime);
                                }
                                else
                                {
                                    sleepingTime = currentTime - scheduledTime;
                                    sleepingTime = new TimeSpan(24, 0, 0) - sleepingTime;
                                }
                            }
                            else if(timingMatched)
                            {
                                sleepingTime = new TimeSpan(24,0,0);
                            }
                           
                            if(timingMatched)
                            {                               
                                ExportReports();
                            }

                        }
                        else
                        {
                            Exception ex = new Exception(ReportConfigurationDetails.ErrorMessage + "\n" + ReportConfigurationDetails.ErrorDetails);
                            string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                            ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                            this.Stop();
                        }
                    }
                    else
                    {
                        Exception ex = new Exception("unable to load report configuration");
                        string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                        ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                        this.Stop();
                       
                    }
                   string err = ErrorHandler.ErrorHandler.CreateErrorMessage(new Exception(sleepingTime.Hours + ":"+ sleepingTime.Minutes + ":" +sleepingTime.Seconds));
                    ErrorHandler.ErrorHandler.LogFileWrite(err);
                    if (sleepingTime.Ticks > 0)
                    {
                        Thread.Sleep(sleepingTime);
                    }
                    else
                    {
                        this.Stop();
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerHelper.LogError(ex);
            }

        }

        protected override void OnStart(string[] args)
        {
            try
            {
                ServicePosInfoBusiness svcPosinfoBusiness = new ServicePosInfoBusiness();
                string ret = svcPosinfoBusiness.ServiceStart(false);
                
                if (string.IsNullOrEmpty(ret))
                {
                    LoggerHelper.LogError(new Exception("DigiReportService:DigiReportService:OnStart event:DigiReportService starts"));
                    mainThread = new Thread(CallingThread);

                    mainThread.Start();
                }
                else
                {
                    throw new Exception("Already Started");

                }
            }
            catch (Exception ex)
            {
                ExitCode = 13816;
                this.Stop();
               LoggerHelper.LogError(ex);
            }
        }

        protected override void OnStop()
        {
            try
            {
                ServicePosInfoBusiness svcPosinfoBusiness = new ServicePosInfoBusiness();
                svcPosinfoBusiness.StopService(false);
                LoggerHelper.LogError(new Exception("DigiReportService:DigiReportService:OnStart event:DigiReportService stops"));

            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }


    }

}
