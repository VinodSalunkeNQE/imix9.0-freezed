﻿

using System;

using AVT.VmbAPINET;

namespace RideCameraSetting
{
    public class FirewireCamera : Camera
    {
        public FirewireCamera(string cameraID,
                                string cameraName,
                                string cameraModel,
                                string cameraSerialNumber,
                                string interfaceID,
                                VmbInterfaceType interfaceType,
                                string interfaceName,
                                string interfaceSerialNumber,
                                VmbAccessModeType interfacePermittedAccess)
            : base(cameraID,
                    cameraName,
                    cameraModel,
                    cameraSerialNumber,
                    interfaceID,
                    interfaceType,
                    interfaceName,
                    interfaceSerialNumber,
                    interfacePermittedAccess)
        {
        }

        public void addonFirewire(string info)  // custom camera function
        {
            info = "1394 interface connection detected";
        }
    };

    class GigECamera : Camera
    {
        public GigECamera(string cameraID,
                            string cameraName,
                            string cameraModel,
                            string cameraSerialNumber,
                            string interfaceID,
                            VmbInterfaceType interfaceType,
                            string interfaceName,
                            string interfaceSerialNumber,
                            VmbAccessModeType interfacePermittedAccess)
            : base(cameraID,
                    cameraName,
                    cameraModel,
                    cameraSerialNumber,
                    interfaceID,
                    interfaceType,
                    interfaceName,
                    interfaceSerialNumber,
                    interfacePermittedAccess)
        {
        }

        public void addonGigE(string info)  // custom camera function
        {
            info = "ethernet interface connection detected";
        }
    };

    class USBCamera : Camera
    {
        public USBCamera(string cameraID,
                            string cameraName,
                            string cameraModel,
                            string cameraSerialNumber,
                            string interfaceID,
                            VmbInterfaceType interfaceType,
                            string interfaceName,
                            string interfaceSerialNumber,
                            VmbAccessModeType interfacePermittedAccess)
            : base(cameraID,
                    cameraName,
                    cameraModel,
                    cameraSerialNumber,
                    interfaceID,
                    interfaceType,
                    interfaceName,
                    interfaceSerialNumber,
                    interfacePermittedAccess)
        {
        }

        public void addonUSB(string info)  // custom camera function
        {
            info = "usb interface connection detected";
        }
    }

    // class to give possiblility to create standart camera
    class DefaultCamera : Camera
    {
        public DefaultCamera(string cameraID,
                             string cameraName,
                             string cameraModel,
                             string cameraSerialNumber,
                             string interfaceID,
                             VmbInterfaceType interfaceType,
                             string interfaceName,
                             string interfaceSerialNumber,
                             VmbAccessModeType interfacePermittedAccess)
            : base(cameraID,
                    cameraName,
                    cameraModel,
                    cameraSerialNumber,
                    interfaceID,
                    interfaceType,
                    interfaceName,
                    interfaceSerialNumber,
                    interfacePermittedAccess)
        {
        }
    }

    public class UserCameraFactory
    {
        static public Camera MyCameraFactory(string cameraID,
                                                string cameraName,
                                                string cameraModel,
                                                string cameraSerialNumber,
                                                string interfaceID,
                                                VmbInterfaceType interfaceType,
                                                string interfaceName,
                                                string interfaceSerialNumber,
                                                VmbAccessModeType interfacePermittedAccess)
        {
            // create camera class, depending on camera interface type
            if (VmbInterfaceType.VmbInterfaceFirewire == interfaceType)
            {
                return new FirewireCamera(cameraID,
                                          cameraName,
                                          cameraModel,
                                          cameraSerialNumber,
                                          interfaceID,
                                          interfaceType,
                                          interfaceName,
                                          interfaceSerialNumber,
                                          interfacePermittedAccess);
            }
            else if (VmbInterfaceType.VmbInterfaceEthernet == interfaceType)
            {
                return new GigECamera(cameraID,
                                      cameraName,
                                      cameraModel,
                                      cameraSerialNumber,
                                      interfaceID,
                                      interfaceType,
                                      interfaceName,
                                      interfaceSerialNumber,
                                      interfacePermittedAccess);
            }
            else if (VmbInterfaceType.VmbInterfaceUsb == interfaceType)
            {
                return new USBCamera(cameraID,
                                     cameraName,
                                     cameraModel,
                                     cameraSerialNumber,
                                     interfaceID,
                                     interfaceType,
                                     interfaceName,
                                     interfaceSerialNumber,
                                     interfacePermittedAccess);
            }
            else // unknown camera interface
            {
                // use default camera class
                return new DefaultCamera(cameraID,
                                         cameraName,
                                         cameraModel,
                                         cameraSerialNumber,
                                         interfaceID,
                                         interfaceType,
                                         interfaceName,
                                         interfaceSerialNumber,
                                         interfacePermittedAccess);
            }
        }
    }

} // Namespace AVT::Vimba::Exanples