﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration.Install;
using System.Linq;
using System.Configuration;
using System.Diagnostics;
using System.Windows;


namespace RideCameraSetting
{
	[RunInstaller(true)]
	public partial class RideCameraSettingInstaller: System.Configuration.Install.Installer
	{
        public RideCameraSettingInstaller()
		{
			InitializeComponent();
        ////#if DEBUG
        //  int processId = Process.GetCurrentProcess().Id;
        //  string message = string.Format("Please attach the debugger (elevated on Vista or Win 7) to process [{0}].", processId);
        //  MessageBox.Show(message, "Debug");
        ////#endif
        }
        public override void Install(System.Collections.IDictionary stateSaver)
        {

            base.Install(stateSaver);
             System.Diagnostics.Debugger.Launch();
            System.Diagnostics.Debugger.Break();
            MessageBox.Show("Debugme");
            string targetDirectory = Context.Parameters["targetdir"];

            string servername = Context.Parameters["Servername"];

            string username = Context.Parameters["Username"];

            string password = Context.Parameters["Password"];
            string hotfolder = Context.Parameters["HotFolder"];

            string database = "Digiphoto";
            System.Diagnostics.Debugger.Break();


            string exePath = string.Format("{0}RideCameraSetting.exe", targetDirectory);

            Configuration config = ConfigurationManager.OpenExeConfiguration(exePath);

            string connectionsection = config.ConnectionStrings.ConnectionStrings
   ["DigiConnectionString"].ConnectionString;

            //Removing Existing Connection string if available Adding new one

            string connectionsection2 = config.ConnectionStrings.ConnectionStrings
   ["DigiphotoEntities"].ConnectionString;
            ConnectionStringSettings connectionstring = null;
            if (connectionsection != null)
            {
                config.ConnectionStrings.ConnectionStrings.Remove("DigiConnectionString");
            }
            if (connectionsection2 != null)
            {
                config.ConnectionStrings.ConnectionStrings.Remove("DigiphotoEntities");
            }

            String conn = "Data Source=" + servername + ";Initial Catalog=" + database + ";User ID=" + username + ";Password=" + password + ";MultipleActiveResultSets=True";
            string conn2 = "metadata=res://*/Model.DigiDataModel.csdl|res://*/Model.DigiDataModel.ssdl|res://*/Model.DigiDataModel.msl;provider=System.Data.SqlClient;provider connection string ='Data Source= " + servername + ";Initial Catalog=" + database + ";User ID=" + username + ";Password=" + password + ";MultipleActiveResultSets=True'";

            connectionstring = new ConnectionStringSettings("DigiConnectionString", conn);
            config.ConnectionStrings.ConnectionStrings.Add(connectionstring);

            ConfigurationSection section = config.GetSection("connectionStrings");

            //Ensures that the section is not already protected
            if (!section.SectionInformation.IsProtected)
            {
                //Uses the Windows Data Protection API (DPAPI) to encrypt the configuration section
                //using a machine-specific secret key
                section.SectionInformation.ProtectSection("DataProtectionConfigurationProvider");
            }
            config.Save(ConfigurationSaveMode.Modified, true);
            ConfigurationManager.RefreshSection("connectionStrings");

            connectionstring = new ConnectionStringSettings("DigiphotoEntities", conn2);
            config.ConnectionStrings.ConnectionStrings.Add(connectionstring);
            config.Save(ConfigurationSaveMode.Modified, true);
            ConfigurationManager.RefreshSection("connectionStrings");

            //For RideCaptureUtility

            exePath = string.Format("{0}RideCaptureUtility.exe", targetDirectory);
            config = ConfigurationManager.OpenExeConfiguration(exePath);

            connectionsection = config.ConnectionStrings.ConnectionStrings
   ["DigiConnectionString"].ConnectionString;


            connectionstring = null;
            if (connectionsection != null)
            {
                config.ConnectionStrings.ConnectionStrings.Remove("DigiConnectionString");
            }

            connectionstring = new ConnectionStringSettings("DigiConnectionString", conn);
            config.ConnectionStrings.ConnectionStrings.Add(connectionstring);
            ConfigurationSection rideutilitysection = config.GetSection("connectionStrings");
            //Ensures that the section is not already protected
            if (!rideutilitysection.SectionInformation.IsProtected)
            {
                //Uses the Windows Data Protection API (DPAPI) to encrypt the configuration section
                //using a machine-specific secret key
                rideutilitysection.SectionInformation.ProtectSection("DataProtectionConfigurationProvider");
            }
            config.Save(ConfigurationSaveMode.Modified, true);
            ConfigurationManager.RefreshSection("connectionStrings");

        }
	}
}
