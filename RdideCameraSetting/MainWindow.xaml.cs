﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using DigiPhoto.DataLayer;
using AVT.VmbAPINET;
using System.Drawing;
using System.IO;
using DigiPhoto.DataLayer.Model;
using System.Data;
using System.Collections.Specialized;
using System.Xml;
using DigiPhoto.IMIX.Business;
using DigiPhoto.IMIX.Model;

namespace RideCameraSetting
{
    public partial class MainWindow : Window
    {

        #region Constructor
        /// <summary>
        /// Initializes a new instance of the <see cref="MainWindow"/> class.
        /// </summary>
        public MainWindow()
        {
            InitializeComponent();

        }
        #endregion

        #region Declaration
        string CameraId = "";
        string camerapath = string.Empty;
        public AVT.VmbAPINET.Camera cam = null;
        private VimbaHelper m_VimbaHelper = null;
        private bool m_Acquiring = false;
        RideCameraSettingInfo rideCameraSettingsInfo = new RideCameraSettingInfo();
        enum SettingsMode
        {
            Unknown = 0,
            Save = 1,
            Load = 2
        };
        #endregion

        #region Events
        /// <summary>
        /// Handles the Click event of the btnSave control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        /// <exception cref="System.Exception">Invalid camera settings xml file.</exception>
        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            try
            {

                //DigiPhotoDataServices _objDataLAyer = new DigiPhotoDataServices();
                //bool result = _objDataLAyer.SetTripCameraSetting(CameraId,txtAcquisitionFrameCount.Text, txtAcquisitionFrameRateAbs.Text, txtAcquisitionFrameRateLimit.Text,
                //    cmbAcquistionMode.SelectedValue.ToString(),txtBalanceRatioAbs.Text, cmbBalanceRatioSelector.SelectedValue.ToString(), cmbBalanceWhiteAuto.SelectedValue.ToString(), txtBalanceWhiteAutoAdjustTol.Text,
                //    txtBalanceWhiteAutoRate.Text, cmbEventSelector.SelectedValue.ToString(), txtEventsEnable.Text, cmbExposureAuto.SelectedValue.ToString(), txtExposureAutoAdjustTol.Text,
                //    cmbExposureAutoAlg.SelectedValue.ToString(), txtExposureAutoMax.Text, txtExposureAutoMin.Text, txtExposureAutoOutliers.Text, txtExposureAutoRate.Text,
                //    txtExposureAutoTarget.Text, txtExposureTimeAbs.Text, cmbGainAuto.SelectedValue.ToString(), txtGainAutoAdjustTol.Text, txtGainAutoMax.Text,
                //    txtGainAutoMin.Text, txtGainAutoOutliers.Text, txtGainAutoRate.Text, txtGainAutoTarget.Text, txtGainRaw.Text, cmbGainSelector.SelectedValue.ToString(),
                //    txtStrobeDelay.Text, txtStrobeDuration.Text, cmbStrobeDurationMode.SelectedValue.ToString(), cmbStrobeSource.SelectedValue.ToString(), txtSyncInGlitchFilter.Text,
                //    txtSyncInLevels.Text, cmbSyncInSelector.SelectedValue.ToString(), txtSyncOutLevels.Text, cmbSyncOutPolarity.SelectedValue.ToString(), cmbSyncOutSelector.SelectedValue.ToString(),
                //    cmbSyncOutSource.SelectedValue.ToString(), cmbTriggerActivation.SelectedValue.ToString(), txtTriggerDelayAbs.Text, cmbTriggerMode.SelectedValue.ToString(), cmbTriggerOverlap.SelectedValue.ToString(),
                //    cmbTriggerSelector.SelectedValue.ToString(), cmbTriggerSource.SelectedValue.ToString(), cmbUserSetDefaultSelector.SelectedValue.ToString(), cmbUserSetSelector.SelectedValue.ToString(), txtWidth.Text,
                //    txtWidthMax.Text, txtHeight.Text, txtHeightMax.Text, txtImageSize.Text);

                RideCameraSettingInfo objRid = new RideCameraSettingInfo();
                objRid.DG_RideCamera_pkey = rideCameraSettingsInfo.DG_RideCamera_pkey;
                objRid.DG_RideCamera_Id = CameraId.ToString();
                objRid.AcquisitionFrameCount = txtAcquisitionFrameCount.Text;
                objRid.AcquisitionFrameRateAbs = txtAcquisitionFrameRateAbs.Text;
                objRid.AcquisitionFrameRateLimit = txtAcquisitionFrameRateLimit.Text;
                objRid.AcquisitionMode = cmbAcquisitionMode.SelectedValue.ToString();
                objRid.BalanceRatioAbs = txtBalanceWhiteForBlue.Text + "," + txtBalanceWhiteForRed.Text;
                objRid.BalanceWhiteAuto = cmbBalanceWhiteAuto.SelectedValue.ToString();
                objRid.BalanceWhiteAutoAdjustTol = txtBalanceWhiteAutoAdjustTol.Text;
                objRid.BalanceWhiteAutoRate = txtBalanceWhiteAutoRate.Text;
                objRid.EventSelector = cmbEventSelector.SelectedValue.ToString();
                objRid.EventsEnable1 = txtEventsEnable.Text;
                objRid.ExposureAuto = cmbExposureAuto.SelectedValue.ToString();
                objRid.ExposureAutoAdjustTol = txtExposureAutoAdjustTol.Text;
                objRid.ExposureAutoAlg = cmbExposureAutoAlg.SelectedValue.ToString();
                objRid.ExposureAutoMax = txtExposureAutoMax.Text;
                objRid.ExposureAutoMin = txtExposureAutoMin.Text;
                objRid.ExposureAutoOutliers = txtExposureAutoOutliers.Text;
                objRid.ExposureAutoTarget = txtExposureAutoRate.Text;
                objRid.ExposureAutoRate = txtExposureAutoRate.Text;
                objRid.ExposureTimeAbs = txtExposureTimeAbs.Text;
                objRid.GainAuto = cmbGainAuto.SelectedValue.ToString();
                objRid.GainAutoAdjustTol = txtGainAutoAdjustTol.Text;
                objRid.GainAutoMax = txtGainAutoMax.Text;
                objRid.GainAutoMin = txtGainAutoMin.Text;
                objRid.GainAutoOutliers = txtGainAutoOutliers.Text;
                objRid.GainAutoRate = txtGainAutoRate.Text;
                objRid.GainAutoTarget = txtGainAutoTarget.Text;
                objRid.Gain = txtGainRaw.Text;
                objRid.GainSelector = cmbGainSelector.SelectedValue.ToString();
                objRid.Hue = txtHue.Text;
                objRid.Gamma = txtGamma.Text;
                objRid.StrobeDelay = txtStrobeDelay.Text;
                objRid.StrobeDuration = txtStrobeDuration.Text;
                objRid.StrobeDurationMode = cmbStrobeDurationMode.SelectedValue.ToString();
                objRid.StrobeSource = cmbStrobeSource.SelectedValue.ToString();
                objRid.SyncInGlitchFilter = txtSyncInGlitchFilter.Text;
                objRid.SyncInLevels = txtSyncInLevels.Text;
                objRid.SyncInSelector = cmbSyncInSelector.SelectedValue.ToString();
                objRid.SyncOutLevels = txtSyncOutLevels.Text;
                objRid.SyncOutPolarity = cmbSyncOutPolarity.SelectedValue.ToString();
                objRid.SyncOutSelector = cmbSyncOutSelector.SelectedValue.ToString();
                objRid.SyncOutSource = cmbSyncOutSource.SelectedValue.ToString();
                objRid.TriggerActivation = cmbTriggerActivation.SelectedValue.ToString();
                objRid.TriggerDelayAbs = txtTriggerDelayAbs.Text;
                objRid.TriggerMode = cmbTriggerMode.SelectedValue.ToString();
                objRid.TriggerOverlap = cmbTriggerOverlap.SelectedValue.ToString();
                objRid.TriggerSelector = cmbTriggerSelector.SelectedValue.ToString();
                objRid.TriggerSource = cmbTriggerSource.SelectedValue.ToString();
                objRid.UserSetDefaultSelector = cmbUserSetDefaultSelector.SelectedValue.ToString();
                objRid.UserSetSelector = cmbUserSetSelector.SelectedValue.ToString();
                objRid.Width = txtWidth.Text;
                objRid.WidthMax = txtWidthMax.Text;
                objRid.Height = txtHeight.Text;
                objRid.HeightMax = txtHeightMax.Text;
                objRid.ImageSize = txtImageSize.Text;
                objRid.PixelFormat = cmbPixelFormat.SelectedValue.ToString();
                objRid.IrisMode = cmbIrisMode.SelectedValue.ToString();
                objRid.IrisAutoTarget = txtIrisAutoTarget.Text;
                objRid.LensPIrisFrequency = txtLensPIrisFrequency.Text;
                objRid.LensPIrisNumSteps = txtLensPIrisNumSteps.Text;
                objRid.LensPIrisPosition = txtLensPIrisPosition.Text;
                objRid.ColorTransformationMode = cmbColorTransformationMode.SelectedValue.ToString();
                objRid.ColorTransformationValues = CreateCommaSeparatedCTValues();
                bool result = (new CameraBusiness()).SetTripCameraSetting(objRid);


                XmlDocument xmlDocument = new XmlDocument();
                xmlDocument.Load("CameraSettings.xml");
                var element = xmlDocument.GetElementsByTagName("Settings");
                XmlNodeList xmlNodeList = xmlDocument.GetElementsByTagName("Settings");
                XmlNode settingsNode = xmlNodeList[0];
                foreach (XmlNode xmlNode in settingsNode.ChildNodes)
                {
                    string type = xmlNode.Name;
                    XmlAttribute nameAttribute = xmlNode.Attributes["Name"];
                    if (null == nameAttribute)
                    {
                        throw new Exception("Invalid camera settings xml file.");
                    }
                    string name = nameAttribute.Value;

                    switch (name)
                    {
                        case "AcquisitionFrameCount": { xmlNode.InnerText = txtAcquisitionFrameCount.Text; break; }

                        case "AcquisitionFrameRateAbs": { xmlNode.InnerText = txtAcquisitionFrameRateAbs.Text; break; }

                        case "AcquisitionFrameRateLimit": { xmlNode.InnerText = txtAcquisitionFrameRateLimit.Text; break; }

                        case "AcquisitionMode": { xmlNode.InnerText = cmbAcquisitionMode.SelectedValue.ToString(); break; }

                        case "BalanceRatioAbs":
                            {
                                XmlAttribute enumAttribute = xmlNode.Attributes["Enumeration"];
                                string enume = enumAttribute.Value;
                                switch (enume)
                                {
                                    case "Blue": { xmlNode.InnerText = txtBalanceWhiteForBlue.Text; break; }

                                    case "Red": { xmlNode.InnerText = txtBalanceWhiteForRed.Text; break; }

                                }
                                break;
                            }

                        case "BalanceWhiteAuto": { xmlNode.InnerText = cmbBalanceWhiteAuto.SelectedValue.ToString(); break; }

                        case "BalanceWhiteAutoAdjustTol": { xmlNode.InnerText = txtBalanceWhiteAutoAdjustTol.Text; break; }

                        case "BalanceWhiteAutoRate": { xmlNode.InnerText = txtBalanceWhiteAutoRate.Text; break; }

                        case "EventSelector": { xmlNode.InnerText = cmbEventSelector.SelectedValue.ToString(); break; }

                        case "EventsEnable1": { xmlNode.InnerText = txtEventsEnable.Text; break; }

                        case "ExposureAuto": { xmlNode.InnerText = cmbExposureAuto.SelectedValue.ToString(); break; }

                        case "ExposureAutoAdjustTol": { xmlNode.InnerText = txtExposureAutoAdjustTol.Text; break; }

                        case "ExposureAutoAlg": { xmlNode.InnerText = cmbExposureAutoAlg.SelectedValue.ToString(); break; }

                        case "ExposureAutoMax": { xmlNode.InnerText = txtExposureAutoMax.Text; break; }

                        case "ExposureAutoMin": { xmlNode.InnerText = txtExposureAutoMin.Text; break; }

                        case "ExposureAutoOutliers": { xmlNode.InnerText = txtExposureAutoOutliers.Text; break; }

                        case "ExposureAutoRate": { xmlNode.InnerText = txtExposureAutoRate.Text; break; }

                        case "ExposureAutoTarget": { xmlNode.InnerText = txtExposureAutoTarget.Text; break; }

                        case "ExposureTimeAbs": { xmlNode.InnerText = txtExposureTimeAbs.Text; break; }

                        case "GainAuto": { xmlNode.InnerText = cmbGainAuto.SelectedValue.ToString(); break; }

                        case "GainAutoAdjustTol": { xmlNode.InnerText = txtGainAutoAdjustTol.Text; break; }

                        case "GainAutoMax": { xmlNode.InnerText = txtGainAutoMax.Text; break; }

                        case "GainAutoMin": { xmlNode.InnerText = txtGainAutoMin.Text; break; }

                        case "GainAutoOutliers": { xmlNode.InnerText = txtGainAutoOutliers.Text; break; }

                        case "GainAutoRate": { xmlNode.InnerText = txtGainAutoRate.Text; break; }

                        case "GainAutoTarget": { xmlNode.InnerText = txtGainAutoTarget.Text; break; }

                        case "Gain": { xmlNode.InnerText = txtGainRaw.Text; break; }

                        case "GainSelector": { xmlNode.InnerText = cmbGainSelector.SelectedValue.ToString(); break; }

                        case "Hue": { xmlNode.InnerText = txtHue.Text; break; }

                        case "Gamma": { xmlNode.InnerText = txtGamma.Text; break; }

                        case "StrobeDelay": { xmlNode.InnerText = txtStrobeDelay.Text; break; }

                        case "StrobeDuration": { xmlNode.InnerText = txtStrobeDuration.Text; break; }

                        case "StrobeDurationMode": { xmlNode.InnerText = cmbStrobeDurationMode.SelectedValue.ToString(); break; }

                        case "StrobeSource": { xmlNode.InnerText = cmbStrobeSource.SelectedValue.ToString(); break; }

                        case "SyncInGlitchFilter": { xmlNode.InnerText = txtSyncInGlitchFilter.Text; break; }

                        case "SyncInLevels": { xmlNode.InnerText = txtSyncInLevels.Text; break; }

                        case "SyncInSelector": { xmlNode.InnerText = cmbSyncInSelector.SelectedValue.ToString(); break; }

                        case "SyncOutLevels": { xmlNode.InnerText = txtSyncInLevels.Text; break; }

                        case "SyncOutPolarity": { xmlNode.InnerText = txtSyncOutLevels.Text; break; }

                        case "SyncOutSelector": { xmlNode.InnerText = cmbSyncOutSelector.SelectedValue.ToString(); break; }

                        case "SyncOutSource": { xmlNode.InnerText = cmbSyncOutSource.SelectedValue.ToString(); break; }

                        case "TriggerActivation": { xmlNode.InnerText = cmbTriggerActivation.SelectedValue.ToString(); break; }

                        case "TriggerDelayAbs": { xmlNode.InnerText = txtTriggerDelayAbs.Text; break; }

                        case "TriggerMode": { xmlNode.InnerText = cmbTriggerMode.SelectedValue.ToString(); break; }

                        case "TriggerOverlap": { xmlNode.InnerText = cmbTriggerOverlap.SelectedValue.ToString(); break; }

                        case "TriggerSelector": { xmlNode.InnerText = cmbTriggerSelector.SelectedValue.ToString(); break; }

                        case "TriggerSource": { xmlNode.InnerText = cmbTriggerSource.SelectedValue.ToString(); break; }

                        case "UserSetDefaultSelector": { xmlNode.InnerText = cmbUserSetDefaultSelector.SelectedValue.ToString(); break; }

                        case "UserSetSelector": { xmlNode.InnerText = cmbUserSetSelector.SelectedValue.ToString(); break; }

                        case "Width": { xmlNode.InnerText = txtWidth.Text; break; }

                        case "WidthMax": { xmlNode.InnerText = txtWidthMax.Text; break; }

                        case "Height": { xmlNode.InnerText = txtHeight.Text; break; }

                        case "HeightMax": { xmlNode.InnerText = txtHeightMax.Text; break; }

                        case "ImageSize": { xmlNode.InnerText = txtImageSize.Text; break; }

                        case "PixelFormat": { xmlNode.InnerText = cmbPixelFormat.SelectedValue.ToString(); break; }

                        case "ColorTransformationValue":
                            {
                                XmlAttribute enumAttribute = xmlNode.Attributes["Enumeration"];
                                string enume = enumAttribute.Value;
                                switch (enume)
                                {
                                    case "Gain00": { xmlNode.InnerText = txtCTValue00.Text; break; }

                                    case "Gain01": { xmlNode.InnerText = txtCTValue01.Text; break; }

                                    case "Gain02": { xmlNode.InnerText = txtCTValue02.Text; break; }

                                    case "Gain10": { xmlNode.InnerText = txtCTValue10.Text; break; }

                                    case "Gain11": { xmlNode.InnerText = txtCTValue11.Text; break; }

                                    case "Gain12": { xmlNode.InnerText = txtCTValue12.Text; break; }

                                    case "Gain20": { xmlNode.InnerText = txtCTValue20.Text; break; }

                                    case "Gain21": { xmlNode.InnerText = txtCTValue21.Text; break; }

                                    case "Gain22": { xmlNode.InnerText = txtCTValue22.Text; break; }
                                }
                                break;
                            }

                        case "ColorTransformationMode": { xmlNode.InnerText = cmbColorTransformationMode.SelectedValue.ToString(); break; }

                        case "IrisMode": { xmlNode.InnerText = cmbIrisMode.SelectedValue.ToString(); break; }

                        case "IrisAutoTarget": { xmlNode.InnerText = txtIrisAutoTarget.Text; break; }

                        case "IrisVideoLevel": { xmlNode.InnerText = txtIrisVideoLevel.Text; break; }

                        case "LensPIrisFrequency": { xmlNode.InnerText = txtLensPIrisFrequency.Text; break; }

                        case "LensPIrisNumSteps": { xmlNode.InnerText = txtLensPIrisNumSteps.Text; break; }

                        case "LensPIrisPosition": { xmlNode.InnerText = txtLensPIrisPosition.Text; break; }
                    }
                }
                xmlDocument.Save("CameraSettings.xml");
                SetMethod();
                if (result == true)
                {
                    MessageBox.Show("Record Saved Successfully");
                }
                else
                {
                    MessageBox.Show("There is some problem in database connectivity.Please check the configuration file");
                }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }

        public string CreateCommaSeparatedCTValues()
        {
            string strTest = string.Empty;
            StringBuilder MyStringBuilder = new StringBuilder();

            MyStringBuilder.Append(txtCTValue00.Text);
            MyStringBuilder.Append("," + txtCTValue01.Text);
            MyStringBuilder.Append("," + txtCTValue02.Text);
            MyStringBuilder.Append("," + txtCTValue10.Text);
            MyStringBuilder.Append("," + txtCTValue11.Text);
            MyStringBuilder.Append("," + txtCTValue12.Text);
            MyStringBuilder.Append("," + txtCTValue20.Text);
            MyStringBuilder.Append("," + txtCTValue21.Text);
            MyStringBuilder.Append("," + txtCTValue22.Text);

            strTest = MyStringBuilder.ToString();
            return strTest;
        }
        /// <summary>
        /// Handles the Closing event of the Window control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.ComponentModel.CancelEventArgs"/> instance containing the event data.</param>
        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {

        }
        /// <summary>
        /// Handles the Loaded event of the Window control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                FillallCombo();
                GetCameraId();
                getRideCameraSetting();
                if (!string.IsNullOrEmpty(CameraId))
                {
                    txtcameraModel.Text = CameraId.ToString();
                }
                else
                {
                    txtcameraModel.Text = "Test";
                }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }
        /// <summary>
        /// Handles the Click event of the btnCapture control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void btnCapture_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                List<CameraInfo> cInfo = new List<CameraInfo>();
                if (m_VimbaHelper == null)
                {
                    VimbaHelper vimbaHelper = new VimbaHelper();
                    m_VimbaHelper = vimbaHelper;
                }
                m_VimbaHelper.Startup();
                cInfo = m_VimbaHelper.CameraList;
                if (cInfo.Count > 0)
                {
                    CameraInfo camera = cInfo[0];
                    System.Drawing.Image _objimage = m_VimbaHelper.AcquireSingleImage(camera.ID);
                    imgImage.Source = CreateBitmapSourceFromBitmap(new Bitmap(_objimage));
                }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }
        /// <summary>
        /// Handles the Click event of the btnClear control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void btnClear_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                GetCameraId();
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }
        /// <summary>
        /// Handles the 1 event of the btnCapture_Click control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void btnCapture_Click_1(object sender, RoutedEventArgs e)
        {
            GetCameraId();
        }
        /// <summary>
        /// Handles the Closed event of the Window control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void Window_Closed(object sender, EventArgs e)
        {
            if (null != m_VimbaHelper)
            {
                try
                {
                    //Shutdown Vimba SDK when application exits
                    m_VimbaHelper.Shutdown();

                }
                catch (Exception ex)
                {
                    string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                    ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                }
                finally
                {
                    m_VimbaHelper = null;
                }
            }
        }
        #endregion

        #region Common
        /// <summary>
        /// Gets the camera unique identifier.
        /// </summary>
        public void GetCameraId()
        {
            try
            {
                List<CameraInfo> cInfo = new List<CameraInfo>();
                VimbaHelper vimbaHelper = new VimbaHelper();
                vimbaHelper.Startup();
                m_VimbaHelper = vimbaHelper;
                cInfo = m_VimbaHelper.CameraList;
                if (cInfo.Count > 0)
                {
                    btnSave.IsEnabled = true;
                    btnCapture_Copy.IsEnabled = true;

                    CameraInfo camera = cInfo[0];
                    CameraId = camera.ID;
                    if (!string.IsNullOrEmpty(CameraId))
                        camerapath = (new CameraBusiness()).GetCameraPathForRideCameraId(CameraId);

                    AVT.VmbAPINET.Vimba vimbaSystem = new AVT.VmbAPINET.Vimba();

                    if (null == camera.ID)
                    {
                        //Open first available camera

                        //Fetch all cameras known to Vimba
                        AVT.VmbAPINET.CameraCollection cameras = vimbaSystem.Cameras;
                        if (cameras.Count < 0)
                        {
                            MessageBox.Show("No camera available.");
                        }

                        foreach (AVT.VmbAPINET.Camera currentCamera in cameras)
                        {
                            //Check if we can open the camere in full mode
                            VmbAccessModeType accessMode = currentCamera.PermittedAccess;
                            if (VmbAccessModeType.VmbAccessModeFull == (VmbAccessModeType.VmbAccessModeFull & accessMode))
                            {
                                //Try to open the camera
                                try
                                {
                                    currentCamera.Open(VmbAccessModeType.VmbAccessModeFull);
                                }
                                catch
                                {
                                    //We can ignore this exception because we simply try
                                    //to open the next camera.
                                    continue;
                                }

                                cam = currentCamera;

                                break;
                            }
                        }

                        if (null == camera)
                        {
                            MessageBox.Show("Could not open any camera.");
                        }
                    }
                    else
                    {
                        //Open specific camera
                        cam = vimbaSystem.OpenCameraByID(cam.Id, VmbAccessModeType.VmbAccessModeFull);

                    }
                }
                else
                {
                    MessageBox.Show("No camera available.");
                    btnSave.IsEnabled = false;
                    btnCapture_Copy.IsEnabled = false;

                }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }
        private void UpdateSettings(string TriggerMode)
        {
            string clsExe = System.IO.Path.GetDirectoryName(System.Diagnostics.Process.GetCurrentProcess().MainModule.FileName) + "\\CameraSettings.xml";
            XmlDocument xdoc = new XmlDocument();
            xdoc.Load(clsExe);
            XmlNodeList xndList = xdoc.GetElementsByTagName("Enumeration");
            foreach (XmlNode item in xndList)
            {
                if (item.Attributes["Name"].Value == "TriggerMode")
                {
                    item.InnerText = TriggerMode;
                    break;
                }
            }
            xdoc.Save(clsExe);
        }

        /// <summary>
        /// Gets the ride camera setting.
        /// </summary>
        public void getRideCameraSetting()
        {
            try
            {
                //DigiPhotoDataServices _objDataLAyer = new DigiPhotoDataServices();
                //DataRow item = _objDataLAyer.GetRideCameraSetting(CameraId);
                rideCameraSettingsInfo = (new CameraBusiness()).GetRideCameraSetting(CameraId);
                if (rideCameraSettingsInfo != null && rideCameraSettingsInfo.DG_RideCamera_pkey > 0)
                {
                    txtAcquisitionFrameCount.Text = rideCameraSettingsInfo.AcquisitionFrameCount;
                    txtAcquisitionFrameRateAbs.Text = rideCameraSettingsInfo.AcquisitionFrameRateAbs;
                    txtAcquisitionFrameRateLimit.Text = rideCameraSettingsInfo.AcquisitionFrameRateLimit;
                    cmbAcquisitionMode.SelectedValue = rideCameraSettingsInfo.AcquisitionMode;
                    txtBalanceWhiteForBlue.Text = rideCameraSettingsInfo.BalanceRatioAbs.Split(',')[0].ToString();
                    txtBalanceWhiteForRed.Text = rideCameraSettingsInfo.BalanceRatioAbs.Split(',')[1].ToString();
                    cmbBalanceWhiteAuto.SelectedValue = rideCameraSettingsInfo.BalanceWhiteAuto;
                    txtBalanceWhiteAutoAdjustTol.Text = rideCameraSettingsInfo.BalanceWhiteAutoAdjustTol;
                    txtBalanceWhiteAutoRate.Text = rideCameraSettingsInfo.BalanceWhiteAutoRate;
                    cmbEventSelector.SelectedValue = rideCameraSettingsInfo.EventSelector;
                    txtEventsEnable.Text = rideCameraSettingsInfo.EventsEnable1;
                    cmbExposureAuto.SelectedValue = rideCameraSettingsInfo.ExposureAuto;
                    txtExposureAutoAdjustTol.Text = rideCameraSettingsInfo.ExposureAutoAdjustTol;
                    cmbExposureAutoAlg.SelectedValue = rideCameraSettingsInfo.ExposureAutoAlg;
                    txtExposureAutoMax.Text = rideCameraSettingsInfo.ExposureAutoMax;
                    txtExposureAutoMin.Text = rideCameraSettingsInfo.ExposureAutoMin;
                    txtExposureAutoOutliers.Text = rideCameraSettingsInfo.ExposureAutoOutliers;
                    txtExposureAutoRate.Text = rideCameraSettingsInfo.ExposureAutoRate;
                    txtExposureAutoTarget.Text = rideCameraSettingsInfo.ExposureAutoTarget;
                    txtExposureTimeAbs.Text = rideCameraSettingsInfo.ExposureTimeAbs;
                    cmbGainAuto.SelectedValue = rideCameraSettingsInfo.GainAuto;
                    txtGainAutoAdjustTol.Text = rideCameraSettingsInfo.GainAutoAdjustTol;
                    txtGainAutoMax.Text = rideCameraSettingsInfo.GainAutoMax;
                    txtGainAutoMin.Text = rideCameraSettingsInfo.GainAutoMin;
                    txtGainAutoOutliers.Text = rideCameraSettingsInfo.GainAutoOutliers;
                    txtGainAutoRate.Text = rideCameraSettingsInfo.GainAutoRate;
                    txtGainAutoTarget.Text = rideCameraSettingsInfo.GainAutoTarget;
                    txtGainRaw.Text = rideCameraSettingsInfo.Gain;
                    cmbGainSelector.SelectedValue = rideCameraSettingsInfo.GainSelector;
                    txtHue.Text = rideCameraSettingsInfo.Hue;
                    txtGamma.Text = rideCameraSettingsInfo.Gamma;
                    txtStrobeDelay.Text = rideCameraSettingsInfo.StrobeDelay;
                    txtStrobeDuration.Text = rideCameraSettingsInfo.StrobeDuration;
                    cmbStrobeDurationMode.SelectedValue = rideCameraSettingsInfo.StrobeDurationMode;
                    cmbStrobeSource.SelectedValue = rideCameraSettingsInfo.StrobeSource;
                    txtSyncInGlitchFilter.Text = rideCameraSettingsInfo.SyncInGlitchFilter;
                    txtSyncInLevels.Text = rideCameraSettingsInfo.SyncInLevels;
                    cmbSyncInSelector.SelectedValue = rideCameraSettingsInfo.SyncInSelector;
                    txtSyncOutLevels.Text = rideCameraSettingsInfo.SyncOutLevels;
                    cmbSyncOutPolarity.SelectedValue = rideCameraSettingsInfo.SyncOutPolarity;
                    cmbSyncOutSelector.SelectedValue = rideCameraSettingsInfo.SyncOutSelector;
                    cmbSyncOutSource.SelectedValue = rideCameraSettingsInfo.SyncOutSource;
                    cmbTriggerActivation.SelectedValue = rideCameraSettingsInfo.TriggerActivation;
                    txtTriggerDelayAbs.Text = rideCameraSettingsInfo.TriggerDelayAbs;
                    cmbTriggerMode.SelectedValue = rideCameraSettingsInfo.TriggerMode;
                    cmbTriggerOverlap.SelectedValue = rideCameraSettingsInfo.TriggerOverlap;
                    cmbTriggerSelector.SelectedValue = rideCameraSettingsInfo.TriggerSelector;
                    cmbTriggerSource.SelectedValue = rideCameraSettingsInfo.TriggerSource;
                    cmbUserSetDefaultSelector.SelectedValue = rideCameraSettingsInfo.UserSetDefaultSelector;
                    cmbUserSetSelector.SelectedValue = rideCameraSettingsInfo.UserSetSelector;
                    txtWidth.Text = rideCameraSettingsInfo.Width;
                    txtWidthMax.Text = rideCameraSettingsInfo.WidthMax;
                    txtHeight.Text = rideCameraSettingsInfo.Height;
                    txtHeightMax.Text = rideCameraSettingsInfo.HeightMax;
                    txtImageSize.Text = rideCameraSettingsInfo.ImageSize;
                    cmbIrisMode.SelectedValue = rideCameraSettingsInfo.IrisMode;
                    txtIrisAutoTarget.Text = rideCameraSettingsInfo.IrisAutoTarget;
                    txtLensPIrisFrequency.Text = rideCameraSettingsInfo.LensPIrisFrequency;
                    txtLensPIrisNumSteps.Text = rideCameraSettingsInfo.LensPIrisNumSteps;
                    txtLensPIrisPosition.Text = rideCameraSettingsInfo.LensPIrisPosition;
                    cmbColorTransformationMode.SelectedValue = rideCameraSettingsInfo.ColorTransformationMode;
                    string[] ColorTransformationValues = rideCameraSettingsInfo.ColorTransformationValues.Split(',');
                    txtCTValue00.Text = ColorTransformationValues[0];
                    txtCTValue01.Text = ColorTransformationValues[1];
                    txtCTValue02.Text = ColorTransformationValues[2];
                    txtCTValue10.Text = ColorTransformationValues[3];
                    txtCTValue11.Text = ColorTransformationValues[4];
                    txtCTValue12.Text = ColorTransformationValues[5];
                    txtCTValue20.Text = ColorTransformationValues[6];
                    txtCTValue21.Text = ColorTransformationValues[7];
                    txtCTValue22.Text = ColorTransformationValues[8];
                }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }

        /// <summary>
        /// Sets the method.
        /// </summary>
        /// <exception cref="System.Exception">
        /// No camera available.
        /// or
        /// Could not open any camera.
        /// </exception>
        public void SetMethod()
        {
            string cameraID = null;
            string fileName = null;
            SettingsMode settingsMode = SettingsMode.Load;
            bool printHelp = false;
            bool ignoreStreamable = false;
            try
            {
                if (null == fileName)
                {
                    fileName = "CameraSettings.xml";
                }

                //Create a new Vimba entry object
                AVT.VmbAPINET.Vimba vimbaSystem = new AVT.VmbAPINET.Vimba();

                //Startup API
                vimbaSystem.Startup();

                try
                {
                    //Open camera
                    AVT.VmbAPINET.Camera camera = null;
                    try
                    {
                        if (null == cameraID)
                        {
                            //Open first available camera

                            //Fetch all cameras known to Vimba
                            AVT.VmbAPINET.CameraCollection cameras = vimbaSystem.Cameras;
                            if (cameras.Count < 0)
                            {
                                throw new Exception("No camera available.");
                            }

                            foreach (AVT.VmbAPINET.Camera currentCamera in cameras)
                            {
                                //Check if we can open the camere in full mode
                                VmbAccessModeType accessMode = currentCamera.PermittedAccess;
                                if (VmbAccessModeType.VmbAccessModeFull == (VmbAccessModeType.VmbAccessModeFull & accessMode))
                                {
                                    //Now get the camera ID
                                    cameraID = currentCamera.Id;

                                    //Try to open the camera
                                    try
                                    {
                                        currentCamera.Open(VmbAccessModeType.VmbAccessModeFull);
                                    }
                                    catch
                                    {
                                        //We can ignore this exception because we simply try
                                        //to open the next camera.
                                        continue;
                                    }

                                    camera = currentCamera;
                                    break;
                                }
                            }

                            if (null == camera)
                            {
                                throw new Exception("Could not open any camera.");
                            }
                        }
                        else
                        {
                            //Open specific camera
                            camera = vimbaSystem.OpenCameraByID(cameraID, VmbAccessModeType.VmbAccessModeFull);
                        }



                        switch (settingsMode)
                        {
                            case SettingsMode.Load:
                                {
                                    RideCameraSetting.VmbAPINET.Examples.LoadSaveSettings.LoadFromFile(camera, fileName);
                                }
                                break;
                        }
                    }
                    finally
                    {
                        if (null != camera)
                        {
                            camera.Close();
                        }
                    }
                }
                finally
                {
                    vimbaSystem.Shutdown();
                }
            }
            catch (Exception exception)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(exception);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }
        /// <summary>
        /// Fillalls the combo.
        /// </summary>
        private void FillallCombo()
        {
            #region AcquisitionMode
            Dictionary<string, string> lstAcquisitionMode = new Dictionary<string, string>();
            lstAcquisitionMode.Add("Continuous", "Continuous");
            lstAcquisitionMode.Add("SingleFrame", "SingleFrame");
            lstAcquisitionMode.Add("MultiFrame", "MultiFrame");
            lstAcquisitionMode.Add("Recorder", "Recorder");
            cmbAcquisitionMode.ItemsSource = lstAcquisitionMode;
            cmbAcquisitionMode.SelectedValue = "Continuous";
            #endregion

            #region PixelFormat
            Dictionary<string, string> lstPixelFormat = new Dictionary<string, string>();
            lstPixelFormat.Add("Mono8", "Mono8");
            lstPixelFormat.Add("BayerGR8", "BayerGR8");
            lstPixelFormat.Add("BayerGR12", "BayerGR12");
            lstPixelFormat.Add("BayerGR12Packed", "BayerGR12Packed");
            lstPixelFormat.Add("RGB8Packed", "RGB8Packed");
            lstPixelFormat.Add("BGR8Packed", "BGR8Packed");
            lstPixelFormat.Add("RGBA8Packed", "RGBA8Packed");
            lstPixelFormat.Add("BGRA8Packed", "BGRA8Packed");
            lstPixelFormat.Add("YUV411Packed", "YUV411Packed");
            lstPixelFormat.Add("YUV422Packed", "YUV422Packed");
            lstPixelFormat.Add("YUV444Packed", "YUV444Packed");
            cmbPixelFormat.ItemsSource = lstPixelFormat;
            cmbPixelFormat.SelectedValue = "BayerGR8";
            #endregion

            #region BalanceWhileAuto
            Dictionary<string, string> lstBalanceWhileAuto = new Dictionary<string, string>();
            lstBalanceWhileAuto.Add("Once", "Once");
            lstBalanceWhileAuto.Add("Off", "Off");
            lstBalanceWhileAuto.Add("Continuous", "Continuous");
            cmbBalanceWhiteAuto.ItemsSource = lstBalanceWhileAuto;
            cmbBalanceWhiteAuto.SelectedValue = "Once";
            #endregion

            #region EventSelector
            Dictionary<string, string> lstEventSelector = new Dictionary<string, string>();
            lstEventSelector.Add("AcquisitionStart", "AcquisitionStart");
            lstEventSelector.Add("AcquisitionEnd", "AcquisitionEnd");
            lstEventSelector.Add("FrameTriggerExposureEnd", "FrameTriggerExposureEnd");
            lstEventSelector.Add("AcquisitionRecordTrigger", "AcquisitionRecordTrigger");
            lstEventSelector.Add("Line1RaisingEdge", "Line1RaisingEdge");
            lstEventSelector.Add("Line1FailingEdge", "Line1FailingEdge");
            lstEventSelector.Add("Line2RaisingEdge", "Line2RaisingEdge");
            lstEventSelector.Add("Line2FailingEdge", "Line2FailingEdge");
            lstEventSelector.Add("Line3RaisingEdge", "Line3RaisingEdge");
            lstEventSelector.Add("Line3FailingEdge", "Line3FailingEdge");
            lstEventSelector.Add("Line4RaisingEdge", "Line4RaisingEdge");
            lstEventSelector.Add("Line4FailingEdge", "Line4FailingEdge");
            cmbEventSelector.ItemsSource = lstEventSelector;
            cmbEventSelector.SelectedValue = "AcquisitionStart";
            #endregion

            #region ExposureAuto
            Dictionary<string, string> lstExposureAuto = new Dictionary<string, string>();
            lstExposureAuto.Add("Off", "Off");
            lstExposureAuto.Add("Once", "Once");
            lstExposureAuto.Add("Continuous", "Continuous");
            lstExposureAuto.Add("Other", "Other");
            cmbExposureAuto.ItemsSource = lstExposureAuto;
            cmbExposureAuto.SelectedValue = "Off";
            #endregion

            #region ExposureAutoAlg
            Dictionary<string, string> lstExposureAutoAlg = new Dictionary<string, string>();
            lstExposureAutoAlg.Add("Mean", "Mean");
            lstExposureAutoAlg.Add("FitRange", "FitRange");
            cmbExposureAutoAlg.ItemsSource = lstExposureAutoAlg;
            cmbExposureAutoAlg.SelectedValue = "Mean";
            #endregion

            #region GainAuto
            Dictionary<string, string> lstGainAuto = new Dictionary<string, string>();
            lstGainAuto.Add("On", "On");
            lstGainAuto.Add("Off", "Off");
            cmbGainAuto.ItemsSource = lstGainAuto;
            cmbGainAuto.SelectedValue = "On";
            #endregion

            #region GainSelector
            Dictionary<string, string> lstGainSelector = new Dictionary<string, string>();
            lstGainSelector.Add("All", "All");
            cmbGainSelector.ItemsSource = lstGainSelector;
            cmbGainSelector.SelectedValue = "All";
            #endregion

            #region StrobeDurationMode
            Dictionary<string, string> lstStrobeDurationMode = new Dictionary<string, string>();
            lstStrobeDurationMode.Add("Controlled", "Controlled");
            lstStrobeDurationMode.Add("Source", "Source");
            cmbStrobeDurationMode.ItemsSource = lstStrobeDurationMode;
            cmbStrobeDurationMode.SelectedValue = "Controlled";
            #endregion

            #region StrobeSource
            Dictionary<string, string> lstStrobeSource = new Dictionary<string, string>();
            lstStrobeSource.Add("AcquisitionTriggerReady", "AcquisitionTriggerReady");
            lstStrobeSource.Add("FrameTriggerReady", "FrameTriggerReady");
            lstStrobeSource.Add("FrameTrigger", "FrameTrigger");
            lstStrobeSource.Add("FrameReadout", "FrameReadout");
            lstStrobeSource.Add("Exposing", "Exposing");
            lstStrobeSource.Add("Acquiring", "Acquiring");
            lstStrobeSource.Add("LineIn1", "LineIn1");
            lstStrobeSource.Add("LineIn2", "LineIn2");
            cmbStrobeSource.ItemsSource = lstStrobeSource;
            cmbStrobeSource.SelectedValue = "AcquisitionTriggerReady";
            #endregion

            #region SyncInSelector
            Dictionary<string, string> lstSyncInSelector = new Dictionary<string, string>();
            lstSyncInSelector.Add("SyncIn1", "SyncIn1");
            cmbSyncInSelector.ItemsSource = lstSyncInSelector;
            cmbSyncInSelector.SelectedValue = "SyncIn1";
            #endregion

            #region SyncOutPolarity
            Dictionary<string, string> lstSyncOutPolarity = new Dictionary<string, string>();
            lstSyncOutPolarity.Add("Normal", "Normal");
            lstSyncOutPolarity.Add("Invert", "Invert");
            cmbSyncOutPolarity.ItemsSource = lstSyncOutPolarity;
            cmbSyncOutPolarity.SelectedValue = "Normal";
            #endregion

            #region SyncOutSelector
            Dictionary<string, string> lstSyncOutSelector = new Dictionary<string, string>();
            lstSyncOutSelector.Add("SyncOut1", "SyncOut1");
            lstSyncOutSelector.Add("SyncOut2", "SyncOut2");
            lstSyncOutSelector.Add("SyncOut3", "SyncOut3");
            cmbSyncOutSelector.ItemsSource = lstSyncOutSelector;
            cmbSyncOutSelector.SelectedValue = "SyncOut1";
            #endregion

            #region SyncOutSource
            Dictionary<string, string> lstSyncOutSource = new Dictionary<string, string>();
            lstSyncOutSource.Add("GPO", "GPO");
            lstSyncOutSource.Add("AcquisitionTriggerReady", "AcquisitionTriggerReady");
            lstSyncOutSource.Add("FrameTriggerReady", "FrameTriggerReady");
            lstSyncOutSource.Add("FrameTrigger", "FrameTrigger");
            lstSyncOutSource.Add("Exposing", "Exposing");
            lstSyncOutSource.Add("FrameReadout", "FrameReadout");
            lstSyncOutSource.Add("Imaging", "Imaging");
            lstSyncOutSource.Add("Acquiring", "Acquiring");
            lstSyncOutSource.Add("LineIn1", "LineIn1");
            lstSyncOutSource.Add("Strobe1", "Strobe1");
            cmbSyncOutSource.ItemsSource = lstSyncOutSource;
            cmbSyncOutSource.SelectedValue = "GPO";
            #endregion

            #region TriggerActivation
            Dictionary<string, string> lstTriggerActivation = new Dictionary<string, string>();
            lstTriggerActivation.Add("RisingEdge", "RisingEdge");
            lstTriggerActivation.Add("FallingEdge", "FallingEdge");
            lstTriggerActivation.Add("AnyEdge", "AnyEdge");
            lstTriggerActivation.Add("LevelHigh", "LevelHigh");
            lstTriggerActivation.Add("LevelLow", "LevelLow");
            cmbTriggerActivation.ItemsSource = lstTriggerActivation;
            cmbTriggerActivation.SelectedValue = "RisingEdge";
            #endregion

            #region TriggerMode
            Dictionary<string, string> lstTriggerMode = new Dictionary<string, string>();
            lstTriggerMode.Add("On", "On");
            lstTriggerMode.Add("Off", "Off");
            cmbTriggerMode.ItemsSource = lstTriggerMode;
            cmbTriggerMode.SelectedValue = "On";
            #endregion

            #region TriggerOverlap
            Dictionary<string, string> lstTriggerOverlap = new Dictionary<string, string>();
            lstTriggerOverlap.Add("Off", "Off");
            lstTriggerOverlap.Add("PreviousFrame", "PreviousFrame");
            cmbTriggerOverlap.ItemsSource = lstTriggerOverlap;
            cmbTriggerOverlap.SelectedValue = "Off";
            #endregion

            #region TriggerSelector
            Dictionary<string, string> lstTriggerSelector = new Dictionary<string, string>();
            lstTriggerSelector.Add("FrameStart", "FrameStart");
            lstTriggerSelector.Add("AcquisitionStart", "AcquisitionStart");
            lstTriggerSelector.Add("AcquisitionEnd", "AcquisitionEnd");
            lstTriggerSelector.Add("AcquisitionRecord", "AcquisitionRecord");
            cmbTriggerSelector.ItemsSource = lstTriggerSelector;
            cmbTriggerSelector.SelectedValue = "FrameStart";
            #endregion

            #region TriggerSource
            Dictionary<string, string> lstTriggerSource = new Dictionary<string, string>();
            lstTriggerSource.Add("freeRun", "freeRun");
            lstTriggerSource.Add("Line1", "Line1");
            lstTriggerSource.Add("Line2", "Line2");
            lstTriggerSource.Add("FixedRate", "FixedRate");
            lstTriggerSource.Add("Software", "Software");
            cmbTriggerSource.ItemsSource = lstTriggerSource;
            cmbTriggerSource.SelectedValue = "freeRun";
            #endregion

            #region UserSetDefaultSelector & UserSetSelector
            Dictionary<string, string> lstUserSetDefaultSelector = new Dictionary<string, string>();
            lstUserSetDefaultSelector.Add("Default", "Default");
            lstUserSetDefaultSelector.Add("UserSet1", "UserSet1");
            lstUserSetDefaultSelector.Add("UserSet2", "UserSet2");
            lstUserSetDefaultSelector.Add("UserSet3", "UserSet3");
            lstUserSetDefaultSelector.Add("UserSet4", "UserSet4");
            lstUserSetDefaultSelector.Add("UserSet5", "UserSet5");
            cmbUserSetDefaultSelector.ItemsSource = lstUserSetDefaultSelector;
            cmbUserSetDefaultSelector.SelectedValue = "Default";
            cmbUserSetSelector.ItemsSource = lstUserSetDefaultSelector;
            cmbUserSetSelector.SelectedValue = "Default";

            #endregion

            #region Color Transformation Mode
            Dictionary<string, string> lstColorTransformationMode = new Dictionary<string, string>();
            lstColorTransformationMode.Add("Off", "Off");
            lstColorTransformationMode.Add("Manual", "Manual");
            cmbColorTransformationMode.ItemsSource = lstColorTransformationMode;
            cmbColorTransformationMode.SelectedValue = "Off";
            #endregion

            #region IrisMode
            Dictionary<string, string> lstIrisMode = new Dictionary<string, string>();
            lstIrisMode.Add("Disabled", "Disabled");
            lstIrisMode.Add("PIrisAuto", "PIrisAuto");
            lstIrisMode.Add("PIrisManual", "PIrisManual");
            cmbIrisMode.ItemsSource = lstIrisMode;
            cmbIrisMode.SelectedValue = "Disabled";
            #endregion
        }
        /// <summary>
        /// Creates the bitmap source from bitmap.
        /// </summary>
        /// <param name="bitmap">The bitmap.</param>
        /// <returns></returns>
        /// <exception cref="System.ArgumentNullException">bitmap</exception>
        public BitmapSource CreateBitmapSourceFromBitmap(Bitmap bitmap)
        {
            if (bitmap == null)
                throw new ArgumentNullException("bitmap");

            return System.Windows.Interop.Imaging.CreateBitmapSourceFromHBitmap(
                bitmap.GetHbitmap(),
                IntPtr.Zero,
                Int32Rect.Empty,
                BitmapSizeOptions.FromEmptyOptions());
        }
        #endregion

        private void btnRFIDSync_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                UpdateSettings("Off");
                SetMethod();
                Bitmap b;
                List<CameraInfo> cInfo = new List<CameraInfo>();
                if (m_VimbaHelper == null)
                {
                    VimbaHelper vimbaHelper = new VimbaHelper();
                    m_VimbaHelper = vimbaHelper;
                }
                m_VimbaHelper.Startup();
                cInfo = m_VimbaHelper.CameraList;
                if (cInfo.Count > 0)
                {
                    CameraInfo camera = cInfo[0];
                    System.Drawing.Image _objimage = m_VimbaHelper.AcquireSingleImage(camera.ID);
                    imgImage.Source = CreateBitmapSourceFromBitmap(new Bitmap(_objimage));
                    using (b = new Bitmap(_objimage))
                    {
                        b.Save(camerapath + "\\" + System.Guid.NewGuid().ToString() + ".jpg");
                    }
                }
                UpdateSettings("On");
                SetMethod();
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }

        private void cmbColorTransformationMode_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (cmbColorTransformationMode.SelectedValue == null)
            {
                DisableGainControls();
            }
            else
            {
                if (cmbColorTransformationMode.SelectedValue == "Off")
                    DisableGainControls();
                else
                    EnableGainControls();
            }
        }

        private void DisableGainControls()
        {
            txtCTValue00.IsEnabled = false;
            txtCTValue01.IsEnabled = false;
            txtCTValue02.IsEnabled = false;
            txtCTValue10.IsEnabled = false;
            txtCTValue11.IsEnabled = false;
            txtCTValue12.IsEnabled = false;
            txtCTValue20.IsEnabled = false;
            txtCTValue21.IsEnabled = false;
            txtCTValue22.IsEnabled = false;
            txtColorTransformationSelector.IsEnabled = false;
        }
        private void EnableGainControls()
        {
            txtCTValue00.IsEnabled = true;
            txtCTValue01.IsEnabled = true;
            txtCTValue02.IsEnabled = true;
            txtCTValue10.IsEnabled = true;
            txtCTValue11.IsEnabled = true;
            txtCTValue12.IsEnabled = true;
            txtCTValue20.IsEnabled = true;
            txtCTValue21.IsEnabled = true;
            txtCTValue22.IsEnabled = true;
            txtColorTransformationSelector.IsEnabled = true;
        }
    }
}
