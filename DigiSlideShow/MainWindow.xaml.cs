﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Linq;
using System.Xml.Linq;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.IO;
using System.Threading;
using System.Windows.Threading;
using System.Configuration;
using System.Runtime.InteropServices;
using System.Diagnostics;

namespace DigiSlideShow
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        #region Declarations
        public DispatcherTimer timer;
        public DispatcherTimer timer1;
        Dictionary<string, string> lstnumberofscreens;
        Dictionary<int, int> lstRotationAngles;
        Random random = new Random();
        List<string> imaglist;
        List<string> mImagelist;
        string DirectoryPath;
        int numberofImages;
        int PeriodInterval;
        int imageinterval;
        int counter;

        DateTime LastUpadtedDate;
        DateTime? UpdatedListKeyTimeOld;
        int numberofscreens;
        int DisplayDelay;
        bool Radiobtnshowall;
        bool Radiobtnnoclip;
        bool Radiobtnclip;
        bool IsRandomImageDisplayActive;
        int RotationAngle = 0;
        FileInfo[] CickedImages;
        TimeSpan tsTimer1;
        Dictionary<DateTime?, List<string>> UpdatedList = new Dictionary<DateTime?, List<string>>();
        RotateTransform transform;
        System.Drawing.Bitmap img;
        #endregion

        #region Constructor
        public MainWindow()
        {
            this.InitializeComponent();
            lstnumberofscreens = new Dictionary<string, string>();
            lstnumberofscreens.Add("One", "1");
            lstnumberofscreens.Add("Two", "2");
            lstnumberofscreens.Add("Four", "4");
            lstnumberofscreens.Add("Six", "6");
            cmbnumberofscreens.ItemsSource = lstnumberofscreens;
            cmbnumberofscreens.SelectedValue = "1";

            cmbRotationAngle.Items.Add(0);
            cmbRotationAngle.Items.Add(90);
            //cmbRotationAngle.Items.Add(180);
            //cmbRotationAngle.Items.Add(270);
            cmbRotationAngle.SelectedIndex = 0;
        }
        #endregion

        #region Events
        //Window Loaded Event
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                DirectoryPath = System.Configuration.ConfigurationManager.AppSettings["PreviewImagePath"];
                numberofImages = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["NumberOfCaptures"]);
                PeriodInterval = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["ImageBatchInterval"]);
                imageinterval = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["TileRefreshInterval"]);
                numberofscreens = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["NumberOfTiles"]);
                Radiobtnshowall = Convert.ToBoolean((System.Configuration.ConfigurationManager.AppSettings["ShowALL"]));
                Radiobtnclip = Convert.ToBoolean((System.Configuration.ConfigurationManager.AppSettings["FullScreenClipImage"]));
                Radiobtnnoclip = Convert.ToBoolean((System.Configuration.ConfigurationManager.AppSettings["FullScreenNoMargin"]));
                DisplayDelay = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["GuestArrivalDelay"]);
                IsRandomImageDisplayActive = Convert.ToBoolean((System.Configuration.ConfigurationManager.AppSettings["IsRandomImageDisplayActive"]));
                RotationAngle = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["RotationAngle"]);
                LastUpadtedDate = System.DateTime.Now;

                LoadSettings();
                counter = 0;
                MarketingImages();

                timer1 = new DispatcherTimer();
                tsTimer1 = TimeSpan.FromSeconds(0);
                timer1.Interval = TimeSpan.FromSeconds(PeriodInterval);// tsTimer1;
                timer1.Tick += new EventHandler(timer1_Tick);
                timer1.IsEnabled = true;
                timer = new DispatcherTimer();

                timer.Interval = TimeSpan.FromSeconds(imageinterval);
                timer.Tick += new EventHandler(timer_Tick);
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }

        //Image Batch Refresh Timer
        void timer1_Tick(object sender, EventArgs e)
        {
            //if(tsTimer1 == TimeSpan.FromSeconds(0))
            //    timer1.Interval = TimeSpan.FromSeconds(PeriodInterval);
            try
            {
                DirectoryInfo Dir = new DirectoryInfo(DirectoryPath);
                FileInfo[] FileList = Dir.GetFiles("*.jpg");
                //imaglist = FileList.OrderByDescending(file => file.CreationTime).Take(numberofImages).Where(p => p.CreationTime > DateTime.Now.AddSeconds(-PeriodInterval)).OrderBy(file => file.CreationTime).Select(p => p.FullName).ToList();
                imaglist = FileList.OrderByDescending(file => file.CreationTime).Take(numberofImages).Select(p => p.FullName).ToList();
                if (imaglist != null && imaglist.Count > 0)
                {
                    UpdatedList.Add(DateTime.Now.AddSeconds(-PeriodInterval), imaglist);
                }

                if (UpdatedListKeyTimeOld != null && UpdatedList.ContainsKey(UpdatedListKeyTimeOld))
                    UpdatedList.Remove(UpdatedListKeyTimeOld);
                if (UpdatedList.Count > 0)
                {
                    imaglist = UpdatedList.Where(x => x.Key <= DateTime.Now.AddSeconds(-DisplayDelay)).FirstOrDefault().Value;
                    UpdatedListKeyTimeOld = UpdatedList.Where(x => x.Key <= DateTime.Now.AddSeconds(-DisplayDelay)).FirstOrDefault().Key;
                }
                else
                {
                    imaglist = null;
                    UpdatedListKeyTimeOld = null;
                }
                timer.Start();
            }
            catch { }
        }

        //Tile Refresh Timer
        void timer_Tick(object sender, EventArgs e)
        {
            if (imaglist != null && imaglist.Count > 0)
            {
                if (IsRandomImageDisplayActive)
                    GetImages(imaglist[random.Next(0, imaglist.Count())], imaglist[random.Next(0, imaglist.Count())], imaglist[random.Next(0, imaglist.Count())]
                        , imaglist[random.Next(0, imaglist.Count())], imaglist[random.Next(0, imaglist.Count())],
                        imaglist[random.Next(0, imaglist.Count())]);
                else
                    GetImagessequentially();
            }
            else
            {
                //timer.Stop();
                GetmImages();
                if (imaglist != null)
                {
                    imaglist.Clear();
                }
                //timer1.Interval = TimeSpan.FromSeconds(PeriodInterval);// tsTimer1;
                //timer1.Start();
            }
        }

        //Show Settings Popup
        private void btnsettingform_Click(object sender, RoutedEventArgs e)
        {
            GrdPopup.Visibility = Visibility.Visible;
        }

        //Hide Settings Popup
        private void btnClose_Click(object sender, RoutedEventArgs e)
        {
            GrdPopup.Visibility = Visibility.Collapsed;
        }

        //Select Display Image Folder Path
        private void btnbrowsefolder_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                System.Windows.Forms.FolderBrowserDialog fbDialog = new System.Windows.Forms.FolderBrowserDialog();
                var result = fbDialog.ShowDialog();
                if (result == System.Windows.Forms.DialogResult.OK)
                {
                    txtFolderPath.Text = fbDialog.SelectedPath;
                }
                else
                {
                    MessageBox.Show("Please select the folder");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }

        //Save Slide Show Settings  
        private void btnsubmit_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (IsValid())
                {
                    bool IsRandomDisplayActive = false;
                    if (rbtnRandom.IsChecked == true)
                        IsRandomDisplayActive = true;

                    // Open App.Config of executable
                    Configuration config = ConfigurationManager.OpenExeConfiguration(Environment.CurrentDirectory + "\\DigiSlideShow.exe");

                    // Add an Application Setting
                    config.AppSettings.Settings.Remove("PreviewImagePath");
                    config.AppSettings.Settings.Add("PreviewImagePath", txtFolderPath.Text);

                    config.AppSettings.Settings.Remove("NumberOfCaptures");
                    config.AppSettings.Settings.Add("NumberOfCaptures", txtImageCount.Text);

                    config.AppSettings.Settings.Remove("ImageBatchInterval");
                    config.AppSettings.Settings.Add("ImageBatchInterval", txtPeriodInterval.Text);

                    config.AppSettings.Settings.Remove("TileRefreshInterval");
                    config.AppSettings.Settings.Add("TileRefreshInterval", txtImageinterval.Text);

                    config.AppSettings.Settings.Remove("NumberOfTiles");
                    config.AppSettings.Settings.Add("NumberOfTiles", cmbnumberofscreens.SelectedValue.ToString());

                    config.AppSettings.Settings.Remove("ShowALL");
                    config.AppSettings.Settings.Add("ShowALL", rbtnshowall.IsChecked.ToString());

                    config.AppSettings.Settings.Remove("FullScreenNoMargin");
                    config.AppSettings.Settings.Add("FullScreenNoMargin", rbtnfullscreennomargin.IsChecked.ToString());

                    config.AppSettings.Settings.Remove("FullScreenClipImage");
                    config.AppSettings.Settings.Add("FullScreenClipImage", rbtnfullscreenclipimage.IsChecked.ToString());

                    config.AppSettings.Settings.Remove("GuestArrivalDelay");
                    config.AppSettings.Settings.Add("GuestArrivalDelay", txtGuestArrivalDelay.Text);

                    config.AppSettings.Settings.Remove("IsRandomImageDisplayActive");
                    config.AppSettings.Settings.Add("IsRandomImageDisplayActive", IsRandomDisplayActive.ToString());

                    config.AppSettings.Settings.Remove("RotationAngle");
                    config.AppSettings.Settings.Add("RotationAngle", cmbRotationAngle.SelectedValue.ToString());

                    // Save the configuration file.
                    config.Save(ConfigurationSaveMode.Modified);

                    // Force a reload of a changed section.
                    ConfigurationManager.RefreshSection("appSettings");

                    MessageBox.Show("Settings Saved Successfully");
                    GrdPopup.Visibility = Visibility.Collapsed;
                    ProcessStartInfo startInfo = new ProcessStartInfo();
                    startInfo.FileName = Environment.CurrentDirectory + "\\DigiSlideShow.exe";
                    Process.Start(startInfo);
                    Application.Current.Shutdown();
                }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                MessageBox.Show(ex.Message);
            }
        }

        //ShutDown Application
        private void btnshutdown_Click(object sender, RoutedEventArgs e)
        {
            Application.Current.Shutdown();
        }

        //Reset the Slide show settings popup
        private void btnreset_Click(object sender, RoutedEventArgs e)
        {
            txtFolderPath.Text = "";
            txtImageCount.Text = "";
            txtImageinterval.Text = "";
            txtPeriodInterval.Text = "";
            txtGuestArrivalDelay.Text = "";
            cmbnumberofscreens.SelectedValue = "1";
        }
        #endregion

        #region Private Functions
        //Set Marketing Images In list
        private void MarketingImages()
        {
            try
            {
                DirectoryInfo Dir = new DirectoryInfo(DirectoryPath + @"\marketing\");
                FileInfo[] FileList = Dir.GetFiles("*.jpg");
                mImagelist = FileList.OrderByDescending(file => file.CreationTime).Take(numberofImages).Select(p => p.FullName).ToList();
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }

        //Display Images Randomly
        private void GetImages(string stfile1, string stfile2, string stfile3, string stfile4, string stfile5, string stfile6)
        {
            try
            {
                if (numberofscreens == 1)
                {
                    SingleImageGrid.Visibility = Visibility.Visible;
                    slideImage.Source = new BitmapImage(new Uri(stfile1, UriKind.Absolute));
                    tbslideImage.Text = System.IO.Path.GetFileNameWithoutExtension(stfile1);
                    tbslideImage.Background = Brushes.White;
                    TwoImageGrid.Visibility = Visibility.Collapsed;
                    FourImageGrid.Visibility = Visibility.Collapsed;
                    SixImageGrid.Visibility = Visibility.Collapsed;
                }
                else if (numberofscreens == 2)
                {


                    TwoImageGrid.Visibility = Visibility.Visible;
                    oneofTwo.Source = new BitmapImage(new Uri(stfile1, UriKind.Absolute));
                    twoofTwo.Source = new BitmapImage(new Uri(stfile2, UriKind.Absolute));
                    tboneofTwo.Text = System.IO.Path.GetFileNameWithoutExtension(stfile1);
                    tbtwoofTwo.Text = System.IO.Path.GetFileNameWithoutExtension(stfile2);
                    tboneofTwo.Background = Brushes.White;
                    tbtwoofTwo.Background = Brushes.White;
                    SingleImageGrid.Visibility = Visibility.Collapsed;
                    FourImageGrid.Visibility = Visibility.Collapsed;
                    SixImageGrid.Visibility = Visibility.Collapsed;
                }
                else if (numberofscreens == 4)
                {
                    FourImageGrid.Visibility = Visibility.Visible;
                    oneofFour.Source = new BitmapImage(new Uri(stfile1, UriKind.Absolute));
                    twoofFour.Source = new BitmapImage(new Uri(stfile2, UriKind.Absolute));
                    threeofFour.Source = new BitmapImage(new Uri(stfile3, UriKind.Absolute));
                    fourofFour.Source = new BitmapImage(new Uri(stfile4, UriKind.Absolute));
                    tboneofFour.Text = System.IO.Path.GetFileNameWithoutExtension(stfile1);
                    tbtwoofFour.Text = System.IO.Path.GetFileNameWithoutExtension(stfile2);
                    tbthreeofFour.Text = System.IO.Path.GetFileNameWithoutExtension(stfile3);
                    tbfourofFour.Text = System.IO.Path.GetFileNameWithoutExtension(stfile4);
                    tboneofFour.Background = Brushes.White;
                    tbtwoofFour.Background = Brushes.White;
                    tbthreeofFour.Background = Brushes.White;
                    tbfourofFour.Background = Brushes.White;
                    SingleImageGrid.Visibility = Visibility.Collapsed;
                    TwoImageGrid.Visibility = Visibility.Collapsed;

                    SixImageGrid.Visibility = Visibility.Collapsed;
                }
                else
                {
                    SixImageGrid.Visibility = Visibility.Visible;
                    oneofsix.Source = new BitmapImage(new Uri(stfile1, UriKind.Absolute));
                    twoofsix.Source = new BitmapImage(new Uri(stfile2, UriKind.Absolute));
                    threeofsix.Source = new BitmapImage(new Uri(stfile3, UriKind.Absolute));
                    fourofsix.Source = new BitmapImage(new Uri(stfile4, UriKind.Absolute));
                    fiveofsix.Source = new BitmapImage(new Uri(stfile5, UriKind.Absolute));
                    sixofsix.Source = new BitmapImage(new Uri(stfile6, UriKind.Absolute));

                    tboneofsix.Text = System.IO.Path.GetFileNameWithoutExtension(stfile1);
                    tbtwoofsix.Text = System.IO.Path.GetFileNameWithoutExtension(stfile2);
                    tbthreeofsix.Text = System.IO.Path.GetFileNameWithoutExtension(stfile3);
                    tbfourofsix.Text = System.IO.Path.GetFileNameWithoutExtension(stfile4);
                    tbfiveofsix.Text = System.IO.Path.GetFileNameWithoutExtension(stfile5);
                    tbsixofsix.Text = System.IO.Path.GetFileNameWithoutExtension(stfile6);

                    tboneofsix.Background = Brushes.White;
                    tbtwoofsix.Background = Brushes.White;
                    tbthreeofsix.Background = Brushes.White;
                    tbfourofsix.Background = Brushes.White;


                    tbfiveofsix.Background = Brushes.White;
                    tbsixofsix.Background = Brushes.White;

                    SingleImageGrid.Visibility = Visibility.Collapsed;
                    TwoImageGrid.Visibility = Visibility.Collapsed;
                    FourImageGrid.Visibility = Visibility.Collapsed;

                }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }

        // Display Images Sequentially
        private void GetImagessequentially()
        {
            try
            {
                string File1 = string.Empty;
                transform = new RotateTransform();
                transform.Angle = Convert.ToDouble(RotationAngle);
                if (numberofscreens == 1)
                {
                    SingleImageGrid.Visibility = Visibility.Visible;
                    img = CreateBitmap(imaglist[0]);
                    slideImage.Source = img.ToBitmapImage();
                    tbslideImage.Text = System.IO.Path.GetFileNameWithoutExtension(imaglist[0]);
                    tbslideImage.LayoutTransform = transform;
                    File1 = imaglist[0];
                    imaglist.RemoveAll(x => x == File1);
                    imaglist.Add(File1);
                    tbslideImage.Background = Brushes.White;
                    TwoImageGrid.Visibility = Visibility.Collapsed;
                    FourImageGrid.Visibility = Visibility.Collapsed;
                    SixImageGrid.Visibility = Visibility.Collapsed;
                }
                else if (numberofscreens == 2)
                {
                    TwoImageGrid.Visibility = Visibility.Visible;
                    TwoImageGridLandscape.Visibility = Visibility.Collapsed;

                    img = CreateBitmap(imaglist[0]);
                    oneofTwo.Source = img.ToBitmapImage();
                    tboneofTwo.Text = System.IO.Path.GetFileNameWithoutExtension(imaglist[0]);
                    tboneofTwo.LayoutTransform = transform;
                    File1 = imaglist[0];
                    imaglist.RemoveAll(x => x == File1);
                    imaglist.Add(File1);

                    img = CreateBitmap(imaglist[0]);
                    twoofTwo.Source = img.ToBitmapImage();
                    tbtwoofTwo.Text = System.IO.Path.GetFileNameWithoutExtension(imaglist[0]);
                    tbtwoofTwo.LayoutTransform = transform;
                    File1 = imaglist[0];
                    imaglist.RemoveAll(x => x == File1);
                    imaglist.Add(File1);

                    tboneofTwo.Background = Brushes.White;
                    tbtwoofTwo.Background = Brushes.White;
                    SingleImageGrid.Visibility = Visibility.Collapsed;
                    FourImageGrid.Visibility = Visibility.Collapsed;
                    SixImageGrid.Visibility = Visibility.Collapsed;
                }
                else if (numberofscreens == 4)
                {
                    FourImageGrid.Visibility = Visibility.Visible;
                    img = CreateBitmap(imaglist[0]);
                    oneofFour.Source = img.ToBitmapImage();
                    tboneofFour.Text = System.IO.Path.GetFileNameWithoutExtension(imaglist[0]);
                    tboneofFour.LayoutTransform = transform;
                    File1 = imaglist[0];
                    imaglist.RemoveAll(x => x == File1);
                    imaglist.Add(File1);

                    img = CreateBitmap(imaglist[0]);
                    twoofFour.Source = img.ToBitmapImage();
                    tbtwoofFour.Text = System.IO.Path.GetFileNameWithoutExtension(imaglist[0]);
                    tbtwoofFour.LayoutTransform = transform;
                    File1 = imaglist[0];
                    imaglist.RemoveAll(x => x == File1);
                    imaglist.Add(File1);

                    img = CreateBitmap(imaglist[0]);
                    threeofFour.Source = img.ToBitmapImage();
                    tbthreeofFour.Text = System.IO.Path.GetFileNameWithoutExtension(imaglist[0]);
                    tbthreeofFour.LayoutTransform = transform;
                    File1 = imaglist[0];
                    imaglist.RemoveAll(x => x == File1);
                    imaglist.Add(File1);

                    img = CreateBitmap(imaglist[0]);
                    fourofFour.Source = img.ToBitmapImage();
                    tbfourofFour.Text = System.IO.Path.GetFileNameWithoutExtension(imaglist[0]);
                    tbfourofFour.LayoutTransform = transform;
                    File1 = imaglist[0];
                    imaglist.RemoveAll(x => x == File1);
                    imaglist.Add(File1);

                    tboneofFour.Background = Brushes.White;
                    tbtwoofFour.Background = Brushes.White;
                    tbthreeofFour.Background = Brushes.White;
                    tbfourofFour.Background = Brushes.White;
                    SingleImageGrid.Visibility = Visibility.Collapsed;
                    TwoImageGrid.Visibility = Visibility.Collapsed;
                    SixImageGrid.Visibility = Visibility.Collapsed;
                }
                else
                {
                    SixImageGrid.Visibility = Visibility.Visible;

                    img = CreateBitmap(imaglist[0]);
                    oneofsix.Source = img.ToBitmapImage();
                    tboneofsix.Text = System.IO.Path.GetFileNameWithoutExtension(imaglist[0]);
                    tboneofsix.LayoutTransform = transform;
                    if (RotationAngle > 0)
                    {
                        tboneofsix.HorizontalAlignment = HorizontalAlignment.Left;
                        tboneofsix.VerticalAlignment = VerticalAlignment.Center;
                    }
                    else
                    {
                        tboneofsix.HorizontalAlignment = HorizontalAlignment.Center;
                        tboneofsix.VerticalAlignment = VerticalAlignment.Bottom;
                    }
                    File1 = imaglist[0];
                    imaglist.RemoveAll(x => x == File1);
                    imaglist.Add(File1);

                    img = CreateBitmap(imaglist[0]);
                    twoofsix.Source = img.ToBitmapImage();
                    tbtwoofsix.Text = System.IO.Path.GetFileNameWithoutExtension(imaglist[0]);
                    tbtwoofsix.LayoutTransform = transform;
                    if (RotationAngle > 0)
                    {
                        tbtwoofsix.HorizontalAlignment = HorizontalAlignment.Left;
                        tbtwoofsix.VerticalAlignment = VerticalAlignment.Center;
                    }
                    else
                    {
                        tbtwoofsix.HorizontalAlignment = HorizontalAlignment.Center;
                        tbtwoofsix.VerticalAlignment = VerticalAlignment.Bottom;
                    }
                    File1 = imaglist[0];
                    imaglist.RemoveAll(x => x == File1);
                    imaglist.Add(File1);

                    img = CreateBitmap(imaglist[0]);
                    threeofsix.Source = img.ToBitmapImage();
                    tbthreeofsix.Text = System.IO.Path.GetFileNameWithoutExtension(imaglist[0]);
                    tbthreeofsix.LayoutTransform = transform;
                    if (RotationAngle > 0)
                    {
                        tbthreeofsix.HorizontalAlignment = HorizontalAlignment.Left;
                        tbthreeofsix.VerticalAlignment = VerticalAlignment.Center;
                    }
                    else
                    {
                        tbthreeofsix.HorizontalAlignment = HorizontalAlignment.Center;
                        tbthreeofsix.VerticalAlignment = VerticalAlignment.Bottom;
                    }
                    File1 = imaglist[0];
                    imaglist.RemoveAll(x => x == File1);
                    imaglist.Add(File1);

                    img = CreateBitmap(imaglist[0]);
                    fourofsix.Source = img.ToBitmapImage();
                    tbfourofsix.Text = System.IO.Path.GetFileNameWithoutExtension(imaglist[0]);
                    tbfourofsix.LayoutTransform = transform;
                    if (RotationAngle > 0)
                    {
                        tbfourofsix.HorizontalAlignment = HorizontalAlignment.Left;
                        tbfourofsix.VerticalAlignment = VerticalAlignment.Center;
                    }
                    else
                    {
                        tbfourofsix.HorizontalAlignment = HorizontalAlignment.Center;
                        tbfourofsix.VerticalAlignment = VerticalAlignment.Bottom;
                    }
                    File1 = imaglist[0];
                    imaglist.RemoveAll(x => x == File1);
                    imaglist.Add(File1);

                    img = CreateBitmap(imaglist[0]);
                    fiveofsix.Source = img.ToBitmapImage();
                    tbfiveofsix.Text = System.IO.Path.GetFileNameWithoutExtension(imaglist[0]);
                    tbfiveofsix.LayoutTransform = transform;
                    if (RotationAngle > 0)
                    {
                        tbfiveofsix.HorizontalAlignment = HorizontalAlignment.Left;
                        tbfiveofsix.VerticalAlignment = VerticalAlignment.Center;
                    }
                    else
                    {
                        tbfiveofsix.HorizontalAlignment = HorizontalAlignment.Center;
                        tbfiveofsix.VerticalAlignment = VerticalAlignment.Bottom;
                    }
                    File1 = imaglist[0];
                    imaglist.RemoveAll(x => x == File1);
                    imaglist.Add(File1);

                    img = CreateBitmap(imaglist[0]);
                    sixofsix.Source = img.ToBitmapImage();
                    tbsixofsix.Text = System.IO.Path.GetFileNameWithoutExtension(imaglist[0]);
                    tbsixofsix.LayoutTransform = transform;
                    if (RotationAngle > 0)
                    {
                        tbsixofsix.HorizontalAlignment = HorizontalAlignment.Left;
                        tbsixofsix.VerticalAlignment = VerticalAlignment.Center;
                    }
                    else
                    {
                        tbsixofsix.HorizontalAlignment = HorizontalAlignment.Center;
                        tbsixofsix.VerticalAlignment = VerticalAlignment.Bottom;
                    }
                    File1 = imaglist[0];
                    imaglist.RemoveAll(x => x == File1);
                    imaglist.Add(File1);

                    tboneofsix.Background = Brushes.White;
                    tbtwoofsix.Background = Brushes.White;
                    tbthreeofsix.Background = Brushes.White;
                    tbfourofsix.Background = Brushes.White;
                    tbfiveofsix.Background = Brushes.White;
                    tbsixofsix.Background = Brushes.White;
                    SingleImageGrid.Visibility = Visibility.Collapsed;
                    TwoImageGrid.Visibility = Visibility.Collapsed;
                    FourImageGrid.Visibility = Visibility.Collapsed;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }

        private System.Drawing.Bitmap CreateBitmap(string filePath)
        {
            System.Drawing.Bitmap img1;
            img1 = new System.Drawing.Bitmap(filePath);
            if (RotationAngle == 90)
            {
                img1.RotateFlip(System.Drawing.RotateFlipType.Rotate90FlipNone);
            }
            else if (RotationAngle == 180)
            {
                img1.RotateFlip(System.Drawing.RotateFlipType.Rotate180FlipNone);
            }
            else if (RotationAngle == 270)
            {
                img1.RotateFlip(System.Drawing.RotateFlipType.Rotate270FlipNone);
            }
            //else if (img1.Width > img1.Height && RotationAngle == 0)
            //{
            //    img1.RotateFlip(System.Drawing.RotateFlipType.Rotate270FlipNone);
            //}
            return img1;

        }





        // Display Marketing Images
        private void GetmImages()
        {
            if (mImagelist != null && mImagelist.Count > 0)
            {
                String stfile1 = mImagelist[random.Next(0, mImagelist.Count())];
                String stfile2 = mImagelist[random.Next(0, mImagelist.Count())];
                String stfile3 = mImagelist[random.Next(0, mImagelist.Count())];
                String stfile4 = mImagelist[random.Next(0, mImagelist.Count())];
                String stfile5 = mImagelist[random.Next(0, mImagelist.Count())];
                String stfile6 = mImagelist[random.Next(0, mImagelist.Count())];
                if (numberofscreens == 1)
                {
                    tbslideImage.Text = "";
                    tbslideImage.Background = Brushes.Transparent;

                    SingleImageGrid.Visibility = Visibility.Visible;
                    img = CreateBitmap(stfile1);
                    slideImage.Source = img.ToBitmapImage();
                    TwoImageGrid.Visibility = Visibility.Collapsed;
                    FourImageGrid.Visibility = Visibility.Collapsed;
                    SixImageGrid.Visibility = Visibility.Collapsed;

                }
                else if (numberofscreens == 2)
                {
                    tboneofTwo.Text = "";
                    tbtwoofTwo.Text = "";
                    tboneofTwo.Background = Brushes.Transparent;
                    tbtwoofTwo.Background = Brushes.Transparent;

                    img = CreateBitmap(stfile1);
                    oneofTwo.Source = img.ToBitmapImage();
                    img = CreateBitmap(stfile2);
                    slideImage.Source = img.ToBitmapImage();
                    twoofTwo.Source = img.ToBitmapImage();
                    if (oneofTwo.Source.Height > oneofTwo.Source.Width)
                    {
                        TwoImageGrid.Visibility = Visibility.Visible;
                        SingleImageGrid.Visibility = Visibility.Collapsed;
                        FourImageGrid.Visibility = Visibility.Collapsed;
                        SixImageGrid.Visibility = Visibility.Collapsed;
                    }
                    else
                    {
                        TwoImageGrid.Visibility = Visibility.Visible;
                        SingleImageGrid.Visibility = Visibility.Collapsed;
                        FourImageGrid.Visibility = Visibility.Collapsed;
                        SixImageGrid.Visibility = Visibility.Collapsed;
                    }

                }
                else if (numberofscreens == 4)
                {
                    tboneofFour.Text = "";
                    tbtwoofFour.Text = "";
                    tbthreeofFour.Text = "";
                    tbfourofFour.Text = "";
                    tboneofFour.Background = Brushes.Transparent;
                    tbtwoofFour.Background = Brushes.Transparent;
                    tbthreeofFour.Background = Brushes.Transparent;
                    tbfourofFour.Background = Brushes.Transparent;

                    FourImageGrid.Visibility = Visibility.Visible;
                    img = CreateBitmap(stfile1);
                    oneofFour.Source = img.ToBitmapImage();
                    img = CreateBitmap(stfile2);
                    twoofFour.Source = img.ToBitmapImage();
                    img = CreateBitmap(stfile3);
                    threeofFour.Source = img.ToBitmapImage();
                    img = CreateBitmap(stfile4);
                    fourofFour.Source = img.ToBitmapImage();
                    SingleImageGrid.Visibility = Visibility.Collapsed;
                    TwoImageGrid.Visibility = Visibility.Collapsed;
                    SixImageGrid.Visibility = Visibility.Collapsed;

                }
                else
                {
                    tboneofsix.Text = "";
                    tbtwoofsix.Text = "";
                    tbthreeofsix.Text = "";
                    tbfourofsix.Text = "";
                    tbfiveofsix.Text = "";
                    tbsixofsix.Text = "";
                    tboneofsix.Background = Brushes.Transparent;
                    tbtwoofsix.Background = Brushes.Transparent;
                    tbthreeofsix.Background = Brushes.Transparent;
                    tbfourofsix.Background = Brushes.Transparent;
                    tbfiveofsix.Background = Brushes.Transparent;
                    tbsixofsix.Background = Brushes.Transparent;


                    SixImageGrid.Visibility = Visibility.Visible;
                    img = CreateBitmap(stfile1);
                    oneofsix.Source = img.ToBitmapImage();
                    img = CreateBitmap(stfile1);
                    twoofsix.Source = img.ToBitmapImage();
                    img = CreateBitmap(stfile1);
                    threeofsix.Source = img.ToBitmapImage();
                    img = CreateBitmap(stfile1);
                    fourofsix.Source = img.ToBitmapImage();
                    img = CreateBitmap(stfile1);
                    fiveofsix.Source = img.ToBitmapImage();
                    img = CreateBitmap(stfile1);
                    sixofsix.Source = img.ToBitmapImage();
                    SingleImageGrid.Visibility = Visibility.Collapsed;
                    TwoImageGrid.Visibility = Visibility.Collapsed;
                    FourImageGrid.Visibility = Visibility.Collapsed;
                }
            }
        }

        //Validate the Required Field
        private bool IsValid()
        {
            if (txtFolderPath.Text == "")
            {
                MessageBox.Show("Please select preview image folder path");
                txtFolderPath.Focus();
                return false;
            }
            else if (txtImageCount.Text == "")
            {
                MessageBox.Show("Please enter the no. of captures");
                txtImageCount.Focus();
                return false;
            }
            else if (txtPeriodInterval.Text == "")
            {
                MessageBox.Show("Please enter image batch interval.");
                txtPeriodInterval.Focus();
                return false;
            }
            else if (txtImageinterval.Text == "")
            {
                MessageBox.Show("Please enter tile refresh interval.");
                txtImageinterval.Focus();
                return false;
            }

            else if (!(bool)rbtnshowall.IsChecked && !(bool)rbtnfullscreennomargin.IsChecked && !(bool)rbtnfullscreenclipimage.IsChecked)
            {
                MessageBox.Show("Please select display options");
                return false;
            }
            else if (txtGuestArrivalDelay.Text == "")
            {
                MessageBox.Show("Please enter guest arrival delay");
                txtGuestArrivalDelay.Focus();
                return false;
            }
            else if (!(bool)rbtnSequential.IsChecked && !(bool)rbtnRandom.IsChecked)
            {
                MessageBox.Show("Please select any image preview order");
                return false;
            }
            else
            {
                return true;
            }
        }

        //Load Slide Show Settings
        private void LoadSettings()
        {
            try
            {
                txtFolderPath.Text = DirectoryPath;
                txtImageCount.Text = numberofImages.ToString();
                txtImageinterval.Text = imageinterval.ToString();
                txtPeriodInterval.Text = PeriodInterval.ToString();
                txtGuestArrivalDelay.Text = DisplayDelay.ToString();
                cmbnumberofscreens.SelectedValue = numberofscreens;
                cmbRotationAngle.SelectedValue = RotationAngle;
                if (Radiobtnclip)
                {
                    rbtnfullscreenclipimage.IsChecked = true;
                }
                else if (Radiobtnnoclip)
                {
                    rbtnfullscreennomargin.IsChecked = true;
                }
                else if (Radiobtnshowall)
                {
                    rbtnshowall.IsChecked = true;
                }
                if (IsRandomImageDisplayActive)
                {
                    rbtnRandom.IsChecked = true;
                    rbtnSequential.IsChecked = false;
                }
                else
                {
                    rbtnRandom.IsChecked = false;
                    rbtnSequential.IsChecked = true;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }
        #endregion
    }

    public static class ExtensionMethods
    {
        public static BitmapImage ToBitmapImage(this System.Drawing.Bitmap bitmap)
        {
            using (var memory = new MemoryStream())
            {
                bitmap.Save(memory, System.Drawing.Imaging.ImageFormat.Jpeg);
                memory.Position = 0;
                var bitmapImage = new BitmapImage();
                bitmapImage.BeginInit();
                bitmapImage.StreamSource = memory;
                bitmapImage.CacheOption = BitmapCacheOption.OnLoad;
                bitmapImage.EndInit();
                return bitmapImage;
            }
        }
    }
}