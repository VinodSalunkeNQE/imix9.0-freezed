﻿using DigiPhoto.IMIX.Business;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;

namespace DigiAzureUploadServiceSecond
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main()
        {
            //Service1 svc = new Service1();
            //svc.UploadToAzureBlob();
            ErrorHandler.ErrorHandler.LogFileWrite("Azure service Second started");
            ServiceBase[] ServicesToRun;
            ServicesToRun = new ServiceBase[]
            {
                new Service1()
            };
            ServiceBase.Run(ServicesToRun);
        }
    }
}
