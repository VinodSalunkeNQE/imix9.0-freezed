﻿using DigiPhoto.DataLayer;
using DigiPhoto.IMIX.Business;
using DigiPhoto.IMIX.Model;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using DigiAuditLogger;
using FrameworkHelper;
using DigiPhoto.Common;
using DigiPhoto.DataLayer.Model;
//using LevDan.Exif;
//using ExifLib;
using DigiPhoto.DigiSync.Model;
using System.Xml;
using System.Drawing;
using VideoProcessingEngine.Shader;
using System.Runtime.InteropServices;
using System.ComponentModel;
using System.Diagnostics;
using System.Threading;
using MControls;
using MPLATFORMLib;
//using MCOLORSLib;
using FrameworkHelper.Common;
using MCOLORSLib;
//using MCOLORSLib;

namespace VideoProcessingEngine
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class VideoProcessor : Window
    {
        #region Delcaration
        DispatcherTimer VidTimer;
        DispatcherTimer AdvVidTimer;
        FolderStructureInfo FolderInfo;
        Hashtable htVideosToProcess = new Hashtable();
        int ProcessedVideos = 0;
        int substoreId = 0;
        int LocationId = 0;
        static int processedVideosCount = 0;
        int totalVideos = 0;
        static long availableMemoryPercentage;
        BackgroundWorker bwCopyFiles = new BackgroundWorker();
        bool _IsAdvancedVideoEditActive = false;
        List<StreamList> lstStreamDetails = new List<StreamList>();
        List<string> lstFilependingCopy = new List<string>();
        #endregion

        #region MediaLooks Declaration
        public UIElement _parent;
        MComposerClass m_objComposer;
        MMixerClass m_objMixer;
        //MCHROMAKEYLib.IMPersist persist;
        /// /Writer
        //public MLiveClass m_objLive;
        public MWriterClass m_objWriter;
        IMConfig m_pConfigRoot;
        // For extern object
        //public MPreviewClass m_objExtPreview;
        private IMPersist m_pMPersist;
        // Called if user change playlist selection
        // int loopCount = 0;
        IMStreams m_pMixerStreams;
        MPreviewControl mPreviewControl = new MPreviewControl();
        MLiveControl mLiveControl = new MLiveControl();
        MPersistControl mPersistControl1 = new MPersistControl();
        MFormatControl mFormatControl1 = new MFormatControl();
        MConfigList mConfigList1 = new MConfigList();
        MStreamsList mMixerList1 = new MStreamsList();
        //MElementsTree mElementsTree1 = new MElementsTree();
        MScenesCombo mScenesCombo1 = new MScenesCombo();
        MAttributesList mAttributesList1 = new MAttributesList();
        MStreamsBackground mMixerBackground1 = new MStreamsBackground();
        MComposerElementsTree mElementsTree1 = new MComposerElementsTree();
        List<VideoScene> lstVideoScene;
        VideoSceneBusiness business;
        VideoScene objVideoScene;
        VideoScene objMixerScene;

        #region added by Ajay
        List<VideoScene> lstMixerScene;
        #endregion

        MFileSeeking mFileSeeking1 = new MFileSeeking();
        MFileState mFileState1 = new MFileState();
        Hashtable htVideoObjects = new Hashtable();
        string uniqueName = string.Empty;//System.Reflection.Assembly.GetExecutingAssembly().Location
        //string processVideoTemp =System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory,"DigiProcessVideoTemp");
        string processVideoTemp = System.IO.Path.Combine(System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location), "DigiProcessVideoTemp");
        int videoLength;
        PhotoDetail photoDetails;
        string tempFile;
        string outputFormat = "mp4";
        string path_tempThumbnail;
        int frameCount = 0;
        /// <summary>
        /// 1 MediaLokks
        /// 2 VisioForge
        /// </summary>
        #endregion
        public VideoProcessor()
        {
            InitializeComponent();
            CommonUtility.CleanFolder(AppDomain.CurrentDomain.BaseDirectory, new string[] { "Download", "VideoImage" });
            bwCopyFiles.DoWork += bwCopyFiles_DoWork;
            bwCopyFiles.RunWorkerCompleted += bwCopyFiles_RunWorkerCompleted;
            VidTimer = new DispatcherTimer();
            VidTimer.Interval = new TimeSpan(0, 0, 0, 0, 500);
            VidTimer.Tick += new EventHandler(VidTimer_Tick);
            AdvVidTimer = new DispatcherTimer();
            AdvVidTimer.Interval = new TimeSpan(0, 0, 0, 0, 500);
            AdvVidTimer.Tick += new EventHandler(AdvVidTimer_Tick);
            mMixerList1.OnMixerSelChanged += new EventHandler(mMixerList1_OnMixerSelChanged);
            if (!Directory.Exists(processVideoTemp))
                Directory.CreateDirectory(processVideoTemp);
            GetSubStoreConfigData();
            GetPhysicalMemoryThreshold();

            #region MediaLooks
            try
            {
                //m_objLive = new MLiveClass();

                m_objWriter = new MWriterClass();

            }
            catch (Exception exception)
            {
                return;
            }
            //SwitchControlledObject();

            SetWriterControlledObject(m_objWriter);
            //mFormatControl1.SetControlledObject(m_objLive);
            mConfigList1.SetControlledObject(m_objWriter);
            mConfigList1.OnConfigChanged += new EventHandler(mConfigList1_OnConfigChanged);

            // Fill Senders
            FillSenders((IMSenders)m_objWriter);

            // Update config and enable/disable URL field
            mConfigList1_OnConfigChanged(null, EventArgs.Empty);



            //bwRoute.DoWork += new System.ComponentModel.DoWorkEventHandler(bwRoute_DoWork);
            //bwRoute.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(bwRoute_RunWorkerCompleted);
            //bwRoute.WorkerSupportsCancellation = true;(

            loadSceneDetails();

            #endregion
        }
        private void SwitchControlledObject(int controlledObject)
        {
            if (controlledObject == (int)ControlledObject.Composer)
            {
                m_objComposer = new MComposerClass();
                MPersistSetControlledObject(m_objComposer);
                mPersistControl1.SetControlledObject(m_objComposer);
                mMixerList1.SetControlledObject(m_objComposer);
                mPreviewComposerControl.SetControlledObject(m_objComposer);
                mFormatControl1.SetControlledObject(m_objComposer);
                mElementsTree1.SetControlledObject(m_objComposer);
                mScenesCombo1.SetControlledObject(m_objComposer);
                m_objComposer.ObjectStart(null);
                m_objComposer.PreviewEnable("", 0, 1);
            }
            else
            {
                //if (m_objMixer != null)
                //{
                //    try
                //    {
                //        RemovePlugins();
                //    }
                //    catch { }
                //}
                m_objMixer = new MMixerClass();
                MPersistSetControlledObject(m_objMixer);
                mPersistControl1.SetControlledObject(m_objMixer);
                mMixerList1.SetControlledObject(m_objMixer);
                mPreviewComposerControl.SetControlledObject(m_objMixer);
                mFormatControl1.SetControlledObject(m_objMixer);
                mElementsTree1.SetControlledObject(m_objMixer);
                mScenesCombo1.SetControlledObject(m_objMixer);
                m_objMixer.ObjectStart(null);
                m_objMixer.PreviewEnable("", 0, 1);
                m_objColors = new CoMColorsClass();
            }
            mFormatControl1.comboBoxVideo.SelectedIndex = 0;
            mFormatControl1.comboBoxAudio.SelectedIndex = 0;

            mAttributesList1.ElementDescriptors = MHelpers.MComposerElementDescriptors;
            mElementsTree1.ElementDescriptors = MHelpers.MComposerElementDescriptors;
        }
        void m_objComposer_OnFrame(string bsChannelID, object pMFrame)
        {            
            if (pMFrame != null)
            {
                frameCount++;
                foreach (var item in htVideoObjects.Keys)
                {
                    MElement elemRoute = GetMElement(item.ToString());
                    ApplyVideoObjectRoute(elemRoute, (Hashtable)htVideoObjects[item], frameCount);
                    Marshal.ReleaseComObject(elemRoute);
                }

                if (frameCount == 5)
                {
                    StartWriter(m_objComposer);
                }
            }
        }

        double NextAudioStartTime = 0; double CurrentAudioStoptTime = 0; string CurrentAudioStream = string.Empty;
        MElement AudioElement;
        MElement BorderElement;
        private void GetGuestElement()
        {
            // elemRoute1 = null;
            MElement myElementRoute1 = null;
            try
            {
                string entry = string.Empty;
                m_objMixer.ElementsGetByIndex(0, out myElementRoute1);
                //  ((IMElements)myElementRoute1).ElementsGetByID("Guest_Video", out GuestElement);
                //  _guestVideoObject = "Guest_Video";
                // GuestElement = elemRoute1;
                // isGuestElementLoaded = true;
                ((IMElements)myElementRoute1).ElementsGetByID("Audio_Video", out AudioElement);
                ((IMElements)myElementRoute1).ElementsGetByID("Border_Video", out BorderElement);
                ((IMElements)myElementRoute1).ElementsGetByID("Overlay_Video", out OverlayElement);


            }
            catch (Exception ex)
            {

            }
        }
        bool thumCaptured = false;
        static bool takeNextProfile = false;
        private void AdvVidTimer_Tick(object sender, EventArgs e)
        {
            eMState eState;
            decimal vidLengthNow = 0;
            decimal vidLength = videoLength + 0.043M;
            UpdateStatus(m_objWriter, out vidLengthNow);
            if (!thumCaptured && vidLengthNow > 0.50M)
            {
                ExtractThumbnail();
                thumCaptured = true;
            }
            m_objWriter.ObjectStateGet(out eState);
            pbProgress.Value = Convert.ToDouble(vidLengthNow);
            if (_loadMixerConfig)
            {
                if (NextAudioStartTime != 0 && (double)vidLengthNow >= NextAudioStartTime)
                {
                    if (!string.IsNullOrEmpty(CurrentAudioStream))
                    {                        
                        int inx = lstStreamDetails.Where(o => o.streamName == CurrentAudioStream).FirstOrDefault().Index;
                        StreamList sl = lstStreamDetails.Where(o => o.Index == inx + 1).FirstOrDefault();
                        if (sl != null)
                        {
                            string stname = sl.streamName;
                            InitializeStream(stname, false);
                            AudioElement.ElementMultipleSet("stream_id=" + stname + " audio_gain=1", 0.0);
                            StreamList slAudioNext = lstStreamDetails.Where(o => o.Index == sl.Index + 1).FirstOrDefault();
                            if (slAudioNext != null)
                                NextAudioStartTime = slAudioNext.InsertTime;
                            else NextAudioStartTime = 0;
                            CurrentAudioStream = stname;
                            CurrentAudioStoptTime = sl.InsertTime + (sl.Out - sl.In);
                        }
                    }
                }
                if (CurrentAudioStoptTime != 0 && (double)vidLengthNow >= CurrentAudioStoptTime)
                {
                    InitializeStream(CurrentAudioStream, true);
                    CurrentAudioStoptTime = 0;
                }
            }
                        
            if (vidLengthNow >= vidLength)
            {
                ErrorHandler.ErrorHandler.LogFileWrite("videoLength=" + Convert.ToString(videoLength));
                ErrorHandler.ErrorHandler.LogFileWrite("vidLengthNow=" + Convert.ToString(vidLengthNow));
                ErrorHandler.ErrorHandler.LogFileWrite("vidLength=" + Convert.ToString(vidLength));

                stopWriter();
                lstMixerScene = new List<VideoScene>();
                foreach (var item in lstVideoScene)
                {
                    if (item.IsActive && item.LocationId.Equals(LocationId) && item.IsMixerScene && item.IsActiveForAdvanceProcessing)
                    {
                        lstMixerScene.Add(item);
                    }
                }

                if (videoprofilecount == lstMixerScene.Count - 1)
                //if (videoprofilecount == lstMixerScene.Count)
                {
                    //UpdateVideoProcessedStatus(photoDetails.PhotoId, true);
                    takeNextProfile = true;
                    videoprofilecount = 0;
                }
                else
                {
                    takeNextProfile = false;
                    //videoprofilecount = videoprofilecount + 1;
                    //videoprofilecount =GetUPDProfileVideocount(photoDetails.PhotoId, "Update", videoprofilecount++);
                    //videoprofilecount++;
                }
                //bwSaveVideos.RunWorkerAsync();
            }           
        }

        private MElement GetMElement(string vidObject)
        {
            MElement elemRoute;
            try
            {
                MElement myElementRoute; string entry = string.Empty;
                m_objComposer.ElementsGetByIndex(3, out myElementRoute);
                //  m_objComposer.ElementsGetByID("video_000", out myElementRoute);              
                ((IMElements)myElementRoute).ElementsGetByID(vidObject, out elemRoute);
                return elemRoute;
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                return elemRoute = null;
            }
        }

        //private void ApplyVideoObjectRoute(MElement elemRoute, Hashtable htRouteList)
        //{
        //    for (int i = 0; i <= htRouteList.Count - 1; i++)
        //    {
        //        string varRoute = GetUpdateElementString((Hashtable)htRouteList[i]);
        //        elemRoute.ElementMultipleSet(varRoute, 0.0);
        //        Thread.Sleep(50);
        //        if (CancelbwRoute)
        //        {
        //            //  KillBackgroundWorker(bwRoute);
        //            break;
        //        }
        //    }
        //}
        private void ApplyVideoObjectRoute(MElement elemRoute, Hashtable htRouteList, int count)
        {
            if (htRouteList.Count > count)
            {
                string varRoute = GetUpdateElementString((Hashtable)htRouteList[count]);
                elemRoute.ElementMultipleSet(varRoute, 0.0);
            }
        }
        private string GetUpdateElementString(Hashtable htSingleRoute)
        {
            string strVar = string.Empty;
            foreach (string key in htSingleRoute.Keys)
            {
                strVar += htSingleRoute[key].ToString() + " ";
            }
            return strVar;
        }
        public void KillBackgroundWorker(System.ComponentModel.BackgroundWorker bw)
        {
            bw.CancelAsync();
            bw.Dispose();
            bw = null;
            GC.Collect();
        }
        #region Events
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                this.Left = 0;
                this.Top = 0;
                ConfigBusiness configBusiness = new ConfigBusiness();
                FolderInfo = configBusiness.GetFolderStructureInfo(ConfigManager.SubStoreId);
                VidTimer.Start();
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }
        private void VidTimer_Tick(object sender, EventArgs e)
        {
            try
            {
                ////Start the checking for Memory_Start
                //if (IsMemoryOverflow())
                //{
                //    KillMserverProcesses();
                //    Process thisProc = Process.GetCurrentProcess();
                //    System.Windows.Forms.Application.Restart();
                //    thisProc.Kill();
                //    Application.Current.Shutdown();
                //}
                ////Start the checking for Memory_End
                ProcessVideo();
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }
        string UpdateStatus(MWriterClass pCapture, out decimal VidLength)
        {
            VidLength = 0; string strVidlen;
            string strRes = "";
            try
            {
                eMState eState;
                pCapture.ObjectStateGet(out eState);
                string strPid;
                m_objWriter.PropsGet("stat::last::server_pid", out strPid);
                string strSkip;
                pCapture.PropsGet("stat::skip", out strSkip);
                strRes = " State: " + eState + (strSkip != null ? " Skip rest:" + DblStrTrim(strSkip, 3) : "") + " Server PID:" + strPid + "\r\n";

                string sFile;
                pCapture.WriterNameGet(out sFile);
                strRes += " Path: " + sFile + "\r\n";

                {
                    strRes += "TOTAL:\r\n";
                    string strBuffer;
                    pCapture.PropsGet("stat::buffered", out strBuffer);
                    string strFrames;
                    pCapture.PropsGet("stat::frames", out strFrames);

                    string strFPS;
                    pCapture.PropsGet("stat::fps", out strFPS);

                    string strTime;
                    pCapture.PropsGet("stat::video_time", out strTime);

                    string strBreaks = "0";
                    pCapture.PropsGet("stat::breaks", out strBreaks);
                    string strDropped = "0";
                    pCapture.PropsGet("stat::frames_dropped", out strDropped);

                    m_objWriter.PropsGet("stat::video_len", out strVidlen);
                    VidLength = Convert.ToDecimal(strVidlen);
                    string strAVtime;
                    m_objWriter.PropsGet("stat::av_sync_time", out strAVtime);
                    string strAVlen;
                    m_objWriter.PropsGet("stat::av_sync_len", out strAVlen);
                    strRes += "  Buffers:" + strBuffer + " Frames:" + strFrames + " Fps:" + DblStrTrim(strFPS, 3) + " Dropped:" + strDropped +
                    " Breaks:" + strBreaks + "  Video time:" + DblStrTrim(strTime, 3) + "\r\n" + " Video Len:" + strVidlen + " AV Sync time:" +
                    strAVtime + " AV sync len:" + strAVlen + "\r\n";

                    string strSamples;
                    pCapture.PropsGet("stat::samples", out strSamples);
                    pCapture.PropsGet("stat::audio_time", out strTime);

                    strRes += "  Samples:" + strSamples + " Audio time:" + DblStrTrim(strTime, 3) + "\r\n";
                }
                {
                    strRes += "LAST:\r\n";

                    string strFrames;
                    pCapture.PropsGet("stat::last::frames", out strFrames);

                    string strFPS = "";
                    pCapture.PropsGet("stat::last::fps", out strFPS);

                    string strTime = "";
                    pCapture.PropsGet("stat::last::video_time", out strTime);

                    string strBreaks;
                    pCapture.PropsGet("stat::breaks", out strBreaks);

                    strRes += "  Frames:" + strFrames + " Breaks:" + strBreaks +
                        " Video time:" + DblStrTrim(strTime, 3) + "\r\n";

                    string strSamples;
                    pCapture.PropsGet("stat::last::samples", out strSamples);
                    pCapture.PropsGet("stat::last::audio_time", out strTime);

                    strRes += "  Samples:" + strSamples + " Audio time:" + DblStrTrim(strTime, 3);
                }
                return strRes;
            }
            catch (Exception ex)
            {
                return strRes;
            }
        }

        string DblStrTrim(string str, int nPeriod)
        {
            if (str == null)
                return str = "0";
            int nDot = str.LastIndexOf('.');
            int nPeriodHave = nDot >= 0 ? str.Length - nDot - 1 : 0;
            if (nPeriodHave > nPeriod)
            {
                return str.Substring(0, nDot + nPeriod + 1);
            }
            // Add zeroes
            if (nDot < 0)
                str += ".";

            for (int i = nPeriodHave; i < nPeriod; i++)
            {
                str += "0";
            }
            return str;
        }

        bool _loadMixerConfig;
        int videoprofilecount = 0;
        private void ProcessVideo()
        {
            try
            {
                PhotoBusiness photoBusiness = new PhotoBusiness();
                List<PhotoDetail> lstPhotoDetails = photoBusiness.GetFilesForAutoVideoProcessing(ConfigManager.SubStoreId, Environment.MachineName);
                processedVideosCount = lstPhotoDetails.Count() + ProcessedVideos;
                totalVideos = lstPhotoDetails.Count;
                prgbar.Maximum = processedVideosCount;
                txbtotalfiles.Text = processedVideosCount.ToString();
                if (lstPhotoDetails.Count > 0)
                {
                    VidTimer.Stop();
                }
                foreach (PhotoDetail FileDetail in lstPhotoDetails)
                {
                    //initialize the profile count to zero
                    if (takeNextProfile)
                    {
                        videoprofilecount = 0;
                    }
                    //check if video count already created with the same file name then increase videoprofilecount.
                    string DateFolder = DateTime.Now.ToString("yyyyMMdd");
                    string directoryTemp = System.IO.Path.Combine(ConfigManager.DigiFolderPath + "ProcessedVideos", DateFolder);
                    DirectoryInfo hdDirectoryInWhichToSearchTemp = new DirectoryInfo(directoryTemp);
                    if (!Directory.Exists(directoryTemp))
                    {
                        Directory.CreateDirectory(directoryTemp);
                    }
                    FileInfo[] filesInDirTemp = hdDirectoryInWhichToSearchTemp.GetFiles("*" + Convert.ToString(FileDetail.PhotoRFID) + "*.mp4*");
                    lstMixerScene = new List<VideoScene>();
                    ///lstMixerScene is the list of profiles created for Magicshot location
                    substoreId = FileDetail.SubstoreId;
                    LocationId = FileDetail.LocationId;
                    SetNewConfigLocationValues(LocationId);
                    objVideoScene = new VideoScene();
                    objMixerScene = new VideoScene();
                    photoDetails = new PhotoDetail();
                    photoDetails = FileDetail;
                    objVideoScene = lstVideoScene.Where(x => x.LocationId == LocationId && x.IsActive == true && x.IsMixerScene == false && x.IsActiveForAdvanceProcessing == true).FirstOrDefault();
                    objMixerScene = lstVideoScene.Where(x => x.LocationId == LocationId && x.IsActive == true && x.IsMixerScene == true && x.IsActiveForAdvanceProcessing == true).FirstOrDefault();

                    foreach (var item in lstVideoScene)
                    {
                        if (item.IsActive && item.LocationId.Equals(LocationId) && item.IsMixerScene && item.IsActiveForAdvanceProcessing)
                        {
                            lstMixerScene.Add(item);
                        }
                    }
                    if (filesInDirTemp.Count() >= 0)
                    {
                        if (filesInDirTemp.Count() < lstMixerScene.Count)
                        {
                            videoprofilecount = filesInDirTemp.Count();
                            if (_IsAdvancedVideoEditActive && objVideoScene != null)
                            {
                                _loadMixerConfig = false;
                                SwitchControlledObject((int)ControlledObject.Composer);
                                processVideoMediaLooks(FileDetail);
                            }
                            else if (objMixerScene != null)
                            {
                                _loadMixerConfig = true;
                                SwitchControlledObject((int)ControlledObject.Mixer);
                                processVideoMediaLooks(FileDetail);
                            }
                            else
                            {
                                string errorMessage = "Auto Video Profile settings are not available for the Location. Please save profile/sccene settings for the location auto video processing is enabled for.";
                                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                            }
                            break;
                        }
                        else
                        {
                            UpdateVideoProcessedStatus(photoDetails.PhotoId, true);
                            VidTimer.Start();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }
        #endregion
        #region Video Auto Processing
        private void processVideoMediaLooks(PhotoDetail filedetails)
        {
            try
            {                
                thumCaptured = false;
                string writerSettings = string.Empty;
                if (_loadMixerConfig)
                {
                    videoLength = lstMixerScene[videoprofilecount].VideoLength;
                    writerSettings = lstMixerScene[videoprofilecount].Settings;

                }
                else
                {
                    videoLength = objVideoScene.VideoLength;
                    writerSettings = objVideoScene.Settings;
                }
                LoadWriterSettings(writerSettings);
                photoDetails = filedetails;
                string filePath = string.Empty, folderName = string.Empty;
                if (filedetails.MediaType == 2)
                    folderName = "Videos";
                else if (filedetails.MediaType == 3)
                    folderName = "ProcessedVideos";
                filePath = System.IO.Path.Combine(ConfigManager.DigiFolderPath, folderName, filedetails.DG_Photos_CreatedOn.ToString("yyyyMMdd"), filedetails.FileName);
                loadScene(filePath);
                //Implement vertical design changes here
                //if ( ||(objMixerScene != null && objMixerScene.IsVerticalVideo))
                //{
                MPLATFORMLib.M_VID_PROPS props = new MPLATFORMLib.M_VID_PROPS();
                props.eVideoFormat = MPLATFORMLib.eMVideoFormat.eMVF_Custom;
                props.dblRate = 29;
                props.nAspectX = 9;
                props.nAspectY = 16;
                props.nHeight = 1920;
                props.nWidth = 1080;
                if (objMixerScene != null && objMixerScene.IsVerticalVideo)
                    m_objMixer.FormatVideoSet(eMFormatType.eMFT_Convert, ref props);
                else if (objVideoScene != null && objVideoScene.IsVerticalVideo)
                    m_objComposer.FormatVideoSet(eMFormatType.eMFT_Convert, ref props);
                //}
                
                if (_loadMixerConfig)
                {
                    GetAudioListToBeApplied(lstMixerScene[videoprofilecount]);
                    startProcessMixer();
                }
                else
                    startProcess();
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }
        string licenseKey = string.Empty;
        string userName = string.Empty;
        string emailID = string.Empty;
        #region Video processing

        int textLogoId = 17;
        private static void RestartApplication()
        {
            Process thisProc = Process.GetCurrentProcess();
            System.Windows.Forms.Application.Restart();
            thisProc.Kill();
            Application.Current.Shutdown();
        }
        #endregion Video processing

        #region Save Video
        static int folderCount = 0;
        int physicalMemoryThreshold = 0;
        private void GetPhysicalMemoryThreshold()
        {
            try
            {
                physicalMemoryThreshold = ConfigManager.IMIXConfigurations.ContainsKey((int)ConfigParams.IsAppMemoryOverflow) ? Convert.ToInt32(ConfigManager.IMIXConfigurations[(int)ConfigParams.IsAppMemoryOverflow]) : 20;

            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }
        private static void CopyVideoToDisplayFolder(string videofile)
        {
            // if (ConfigManager.IMIXConfigurations.ContainsKey((int)ConfigParams.AutoVidDisplayEnabled) && Convert.ToBoolean(ConfigManager.IMIXConfigurations[(int)ConfigParams.AutoVidDisplayEnabled]) == true)
            {
                string dispPath = ConfigManager.IMIXConfigurations.ContainsKey((int)ConfigParams.AutoVidDisplayFolder) ? ConfigManager.IMIXConfigurations[(int)ConfigParams.AutoVidDisplayFolder] : null;
                int NoOfScreen = ConfigManager.IMIXConfigurations.ContainsKey((int)ConfigParams.NoOfDisplayScreens) ? Convert.ToInt32(ConfigManager.IMIXConfigurations[(int)ConfigParams.NoOfDisplayScreens]) : 0;
                folderCount++;
                bool IsCyclic = ConfigManager.IMIXConfigurations.ContainsKey((int)ConfigParams.IsCyclicDisplay) ? Convert.ToBoolean(ConfigManager.IMIXConfigurations[(int)ConfigParams.IsCyclicDisplay]) : false;
                if (IsCyclic)
                {
                    if (folderCount <= NoOfScreen)
                    {
                        string Screenfolder = dispPath + "\\Display\\Display" + folderCount;
                        if (!string.IsNullOrEmpty(Screenfolder))
                        {
                            if (!Directory.Exists(Screenfolder))
                            {
                                Directory.CreateDirectory(Screenfolder);
                            }
                            File.Copy(videofile, Screenfolder + "\\" + System.IO.Path.GetFileName(videofile));
                        }
                        if (folderCount == NoOfScreen)
                        {
                            folderCount = 0;
                        }
                    }
                }
                else
                {
                    for (int i = 1; i <= NoOfScreen; i++)
                    {
                        string Screenfolder = dispPath + "\\Display\\Display" + i.ToString();
                        if (!Directory.Exists(Screenfolder))
                        {
                            Directory.CreateDirectory(Screenfolder);
                        }
                        File.Copy(videofile, Screenfolder + "\\" + System.IO.Path.GetFileName(videofile));
                    }
                }
            }
        }
        void bwCopyFiles_DoWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                string videoFile = Convert.ToString(e.Argument);
                CopyVideoToDisplayFolder(videoFile);
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }
        void bwCopyFiles_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            //ErrorHandler.ErrorHandler.LogFileWrite("File copied!!");
            lstFilependingCopy.RemoveAt(0);
            CheckFilesPendingForCopy();
        }
        private void CheckFilesPendingForCopy()
        {
            if (lstFilependingCopy.Count > 0)
            {
                if (!bwCopyFiles.IsBusy)
                {
                    string outFile = lstFilependingCopy[0];
                    bwCopyFiles.RunWorkerAsync(outFile);
                }
            }
        }

        private void UpdateVideoProcessedStatus(int PhotoId, bool ProcessedStatus)
        {
            PhotoBusiness photoBusiness = new PhotoBusiness();
            photoBusiness.UpdateVideoProcessedStatus(PhotoId, ProcessedStatus);
        }
        #endregion Save Video
        /// <summary>ip
        /// Resiz es the WPF image.
        /// </summary>
        /// <param name="sourceImage">The source image.</param>
        /// <param name="maxHeight">The maximum height.</param>
        /// <param name="saveToPath">The save automatic path.</param>
        private void ResizeWPFImage(string sourceImage, int maxHeight, string saveToPath)
        {
            try
            {
                BitmapImage bi = new BitmapImage();
                BitmapImage bitmapImage = new BitmapImage();
                using (FileStream fileStream = File.OpenRead(sourceImage.ToString()))
                {
                    MemoryStream ms = new MemoryStream();
                    fileStream.CopyTo(ms);
                    ms.Seek(0, SeekOrigin.Begin);
                    fileStream.Close();
                    bi.BeginInit();
                    bi.StreamSource = ms;
                    bi.EndInit();
                    bi.Freeze();
                    decimal ratio = Convert.ToDecimal(bi.Width) / Convert.ToDecimal(bi.Height);

                    int newWidth = Convert.ToInt32(maxHeight * ratio);
                    int newHeight = maxHeight;

                    ms.Seek(0, SeekOrigin.Begin);
                    bitmapImage.BeginInit();
                    bitmapImage.StreamSource = ms;
                    bitmapImage.DecodePixelWidth = newWidth;
                    bitmapImage.DecodePixelHeight = newHeight;
                    bitmapImage.EndInit();
                    bitmapImage.Freeze();
                }

                using (var fileStreamForSave = new FileStream(saveToPath, FileMode.Create, FileAccess.ReadWrite))
                {
                    JpegBitmapEncoder encoder = new JpegBitmapEncoder();
                    encoder.QualityLevel = 94;
                    encoder.Frames.Add(BitmapFrame.Create(bitmapImage));
                    encoder.Save(fileStreamForSave);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
            finally
            {
                //if (DateTime.Now.Subtract(lastmemoryUpdateTime).Seconds > 30)
                //{
                //    MemoryManagement.FlushMemory();
                //    lastmemoryUpdateTime = DateTime.Now;
                //}
            }
        }
        #endregion Video Auto Processing
        #region Auto purchase
        void VideoAssociation(int photoId, int videoId)
        {
            try
            {
                new AssociateImageBusiness().AssociateVideos(photoId, videoId);
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }
        //if (IsBarcodeActive && IsAutoPurchaseActive)
        //   if (IsAnonymousCodeActive)
        //if (IsAutoPurchaseActive)
        //{
        //    ImgInfoPostScanList imgInfoPreScanList = new ImgInfoPostScanList();
        //    imgInfoPreScanList.Barcode = ConfigManager.IMIXConfigurations.ContainsKey((int)ConfigParams.QRCodeAutoPurchase) ? ConfigManager.IMIXConfigurations[(int)ConfigParams.QRCodeAutoPurchase] : string.Empty;
        //    imgInfoPreScanList.Format = "QRCODE";
        //    imgInfoPreScanList.PhotoGrapherId = PhotoGrapherId;
        //    imgInfoPreScanList.ListImgPhotoId.Add(VideoId);
        //    imgInfoPreScanList.LastImgTime = DateTime.Now;
        //    AutoPurchasePostScan(imgInfoPreScanList);
        //}

        //private void AutoPurchasePostScan(ImgInfoPostScanList item)
        //{
        //    //if (IsAnonymousCodeActive)
        //    // if (IsAutoPurchaseActive && !IsAnonymousCodeActive)

        //    // VTODO
        //    //if (IsAutoPurchaseActive)
        //    //{
        //    //    DigiPhotoDataServices objDigiPhotoDataServices = new DigiPhotoDataServices();
        //    //    if (objDigiPhotoDataServices.IsValidPrepaidCodeType(item.Barcode, 405))
        //    //    {
        //    //        /*
        //    //        if (ReadSystemOnlineStatus())
        //    //        {
        //    //            try
        //    //            {
        //    //                ServiceProxy<IDataSyncService>.Use(client =>
        //    //                {
        //    //                    objCardLimit = client.CheckCardLimit(item.Barcode);
        //    //                });
        //    //                IsOnline = true;
        //    //            }
        //    //            catch (Exception ex)
        //    //            {
        //    //                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
        //    //                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
        //    //                IsOnline = false;
        //    //            }
        //    //        }
        //    //        */

        //    //        if (IsOnline)
        //    //        {
        //    //            if (objCardLimit.ValidCard)
        //    //            {
        //    //                CardImageLimit = objCardLimit.Allowed;
        //    //                CardImageSold = objCardLimit.Associated;
        //    //            }
        //    //        }
        //    //        else
        //    //        {
        //    //            CardImageLimit = objDigiPhotoDataServices.GetCardImageLimit(item.Barcode);
        //    //            //CardImageSold = objDigiPhotoDataServices.GetCardImageSold(item.Barcode);
        //    //        }
        //    //        //Set flag to set the package price.
        //    //        if (CardImageSold > 0)
        //    //            IsQrCodeUsed = true;
        //    //        else
        //    //            IsQrCodeUsed = false;

        //    //        if (CardImageSold < CardImageLimit)
        //    //        {
        //    //            rest = CardImageLimit - CardImageSold;
        //    //            rest = rest < 0 ? 0 : rest;
        //    //        }
        //    //        else
        //    //        {
        //    //            rest = 0;
        //    //        }
        //    //        string images = string.Empty;
        //    //        if (scanType == 501)
        //    //            images = string.Join(",", item.ListImgPhotoId.Skip(0).Take(rest));
        //    //        else if (scanType == 502)
        //    //            images = string.Join(",", item.ListImgPhotoId.Take(rest));

        //    //        if (!String.IsNullOrWhiteSpace(images))
        //    //        {
        //    //            iMixImageCardType imgCardTypeInfo = objDigiPhotoDataServices.GetCardTypeList().Where(d => d.CardIdentificationDigit == item.Barcode.Substring(0, 4)).FirstOrDefault();
        //    //            // iMixImageCardType imgCardTypeInfo = objDigiPhotoDataServices.GetCardTypeList().Where(d => d.CardIdentificationDigit == item.Barcode.Substring(0, 4)).FirstOrDefault();
        //    //            int pkgId = imgCardTypeInfo.PackageId == null ? 0 : (int)imgCardTypeInfo.PackageId;
        //    //            decimal pkgPrice = 0;
        //    //            if (CardImageSold == 0)
        //    //            {
        //    //                pkgPrice = (Decimal)objDigiPhotoDataServices.GetProductPricing(pkgId);
        //    //            }

        //    //            int PaymentMode = 0;
        //    //            DG_Currency currency = objDigiPhotoDataServices.GetCurrencyList().Where(g => g.DG_Currency_Default == true).FirstOrDefault();
        //    //            int prodId = objDigiPhotoDataServices.GetPackagDetails(pkgId).Where(k => k.DG_Product_Quantity > 0).FirstOrDefault().DG_Orders_ProductType_pkey;
        //    //            string PaymentDetails = "<Payment Mode = 'prepaid' Amount = '" + pkgPrice.ToString() + "' OrignalAmount = '" + pkgPrice.ToString() + "' CurrencyID = '" + currency.DG_Currency_pkey.ToString() + "' CurrencySyncCode = '" + currency.SyncCode.ToString() + "'/>";
        //    //            string OrderNumber = GenerateOrderNumber();
        //    //            string SyncCode = CommonUtility.GetUniqueSynccode(Convert.ToInt32(ApplicationObjectEnum.Order).ToString().PadLeft(2, '0'), LoginUser.countrycode, LoginUser.Storecode, "14");
        //    //            int OrderID = objDigiPhotoDataServices.GenerateOrder(OrderNumber, pkgPrice, pkgPrice, PaymentDetails, PaymentMode, 0, null, 3, objDigiPhotoDataServices.GetDefaultCurrency(), "0", SyncCode);
        //    //            TaxBusiness buss = new TaxBusiness();
        //    //            buss.SaveOrderTaxDetails(LoginUser.StoreId, OrderID,substoreId);
        //    //            AuditLog.AddUserLog(3, 42, "Create Order of No :" + OrderNumber + " of total Amount " + currency.DG_Currency_Symbol + " " + pkgPrice.ToString("0.000"));
        //    //            string OrderDetailsSyncCode = CommonUtility.GetUniqueSynccode(Convert.ToInt32(ApplicationObjectEnum.OrderDetails).ToString().PadLeft(2, '0'), LoginUser.countrycode, LoginUser.Storecode, substoreId.ToString());
        //    //            int ParentID = objDigiPhotoDataServices.SaveOrderLineItems(pkgId, OrderID, "", 1, null, 0, pkgPrice, pkgPrice, pkgPrice, -1, substoreId, 0, null, OrderDetailsSyncCode);
        //    //            objDigiPhotoDataServices.SaveOrderLineItems(prodId, OrderID, images, 1, null, 0, 0, 0, 0, ParentID, substoreId, imgCardTypeInfo.ImageIdentificationType, item.Barcode, OrderDetailsSyncCode);
        //    //        }
        //    //    }
        //    //}
        //}
        //public string GenerateOrderNumber()
        //{
        //    Random rand = new Random((int)DateTime.UtcNow.Ticks);
        //    IEnumerable<int> lst = Enumerable.Range(0, 256).OrderBy(_ => rand.NextDouble());
        //    string OrderNumber = "DG" + "-" + (new CustomBusineses()).ServerDateTime().Day;
        //    int counter = 0;

        //    foreach (int num in lst)
        //    {
        //        if (counter < 2)
        //        {
        //            OrderNumber += num;
        //        }
        //        else
        //        {
        //            return OrderNumber;
        //        }
        //        counter++;
        //    }
        //    return OrderNumber;
        //}
        #endregion Auto purchase
        #region CropImage
        public TransformGroup transformGroup = null;
        public ScaleTransform zoomTransform = null;
        System.Drawing.Image cropImg(System.Drawing.Image img, double aspectRatio_X, double aspectRatio_Y)
        {
            // 4:3 Aspect Ratio. You can also add it as parameters
            //double aspectRatio_X = 9;
            //double aspectRatio_Y = 16;

            double imgWidth = Convert.ToDouble(img.Width);
            double imgHeight = Convert.ToDouble(img.Height);

            //if (imgWidth / imgHeight > (aspectRatio_X / aspectRatio_Y))
            {
                double extraWidth = imgWidth - (imgHeight * (aspectRatio_X / aspectRatio_Y));
                double cropStartFrom = extraWidth / 2;
                Bitmap bmp = new Bitmap((int)(img.Width - extraWidth), img.Height);
                Graphics grp = Graphics.FromImage(bmp);
                grp.DrawImage(img, new System.Drawing.Rectangle(0, 0, (int)(img.Width - extraWidth), img.Height), new System.Drawing.Rectangle((int)cropStartFrom, 0, (int)(imgWidth - extraWidth), img.Height), GraphicsUnit.Pixel);
                //grp.RotateTransform(90);
                return (System.Drawing.Image)bmp;
            }
            //else
            //    return null;
        }
        #endregion CropImage
        #region General Functions
        private void SetNewConfigLocationValues(int locationId)
        {
            ConfigBusiness configBusiness = new ConfigBusiness();
            List<iMixConfigurationLocationInfo> _objNewConfigList = configBusiness.GetConfigLocation(locationId, substoreId);
            foreach (iMixConfigurationLocationInfo _objNewConfig in _objNewConfigList)
            {
                switch (_objNewConfig.IMIXConfigurationMasterId)
                {
                    case (int)ConfigParams.IsAdvancedVideoEditActive:
                        if (!string.IsNullOrEmpty(_objNewConfig.ConfigurationValue))
                            _IsAdvancedVideoEditActive = Convert.ToBoolean(_objNewConfig.ConfigurationValue) == true ? true : false;
                        if (_IsAdvancedVideoEditActive)
                            winMPreviewComposer.Visibility = Visibility.Visible;
                        break;
                    case (int)ConfigParams.VideoFlipMode:
                        if (!string.IsNullOrEmpty(_objNewConfig.ConfigurationValue))
                            FlipMode = Convert.ToInt32(_objNewConfig.ConfigurationValue);
                        else FlipMode = 0;
                        break;
                    default:
                        break;
                }
            }
        }
        private bool IsMemoryOverflow()
        {
            bool retValue = false;
            try
            {
                availableMemoryPercentage = (PerformanceInfo.GetPhysicalAvailableMemoryInMiB() * 100) / PerformanceInfo.GetTotalMemoryInMiB();
                if (availableMemoryPercentage <= physicalMemoryThreshold)
                {
                    retValue = true;
                    ErrorHandler.ErrorHandler.LogFileWrite("Total avail memory: " + availableMemoryPercentage.ToString());
                }
                return retValue;
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                return retValue;
            }

        }
        bool _isGreenScrrenFlow = false;
        int FlipMode = 0;
        private void GetSubStoreConfigData()
        {
            try
            {
                ConfigBusiness conBiz = new ConfigBusiness();
                List<iMIXConfigurationInfo> _objdata = conBiz.GetNewConfigValues(ConfigManager.SubStoreId);

                if (_objdata != null && _objdata.Count > 0)
                {
                    foreach (iMIXConfigurationInfo imixConfigValue in _objdata)
                    {
                        switch (imixConfigValue.IMIXConfigurationMasterId)
                        {
                            case (int)ConfigParams.IsGreenScreenFlow:
                                _isGreenScrrenFlow = Convert.ToBoolean(imixConfigValue.ConfigurationValue);
                                break;
                            //case (int)ConfigParams.VideoFlipMode:
                            //    if (!string.IsNullOrEmpty(imixConfigValue.ConfigurationValue))
                            //        FlipMode = Convert.ToInt32(imixConfigValue.ConfigurationValue);
                            //    else FlipMode = 0;
                            //    break;
                            default:
                                break;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }

        #endregion
        #region MediaLooks
        public Object MPersistSetControlledObject(Object pObject)
        {
            Object pOld = (Object)m_pMPersist;
            try
            {
                m_pMPersist = (IMPersist)pObject;
            }
            catch (System.Exception) { }

            return pOld;
        }
        public Object SetWriterControlledObject(Object pObject)
        {
            Object pOld = (Object)m_pConfigRoot;
            try
            {
                m_pConfigRoot = (IMConfig)pObject;
            }
            catch (System.Exception) { }
            return pOld;
        }

        void mConfigList1_OnConfigChanged(object sender, EventArgs e)
        {
            // Update config string 
            string strConfig;
            m_objWriter.ConfigGetAll(1, out strConfig);
            // Check if format support network streaming
            string strFormat;
            IMAttributes pFormatConfig;
            m_objWriter.ConfigGet("format", out strFormat, out pFormatConfig);
            int bNetwork = 0;
            try
            {
                pFormatConfig.AttributesBoolGet("network", out bNetwork);
            }
            catch (System.Exception) { }

        }
        private void FillSenders(IMSenders pSenders)
        {
            int nCount = 0;
            pSenders.SendersGetCount(out nCount);
            for (int i = 0; i < nCount; i++)
            {
                string strName;
                MPLATFORMLib.M_VID_PROPS vidProps;
                MPLATFORMLib.M_AUD_PROPS audProps;
                pSenders.SendersGetByIndex(i, out strName, out vidProps, out audProps);
            }
        }
        bool ApplyColor = false;
        void loadScene(string filePath)
        {
            try
            {
                string sceneFilePath = string.Empty;
                string streamID = string.Empty;
                if (_loadMixerConfig)
                {
                    m_pMPersist = (IMPersist)m_objMixer;
                    sceneFilePath = System.IO.Path.Combine(ConfigManager.DigiFolderPath, lstMixerScene[videoprofilecount].ScenePath);
                    streamID = "stream-000";
                }
                else
                {
                    m_pMPersist = (IMPersist)m_objComposer;
                    sceneFilePath = System.IO.Path.Combine(ConfigManager.DigiFolderPath, objVideoScene.ScenePath);
                    streamID = objVideoScene.lstVideoSceneObject.Where(x => x.GuestVideoObject == true).FirstOrDefault().VideoObjectId;
                }
                if (System.IO.File.Exists(sceneFilePath))
                {
                    m_pMPersist.PersistLoad("", sceneFilePath, "");
                    mMixerList1.UpdateList(true, 1);
                    mElementsTree1.UpdateTree(false);

                    if (!string.IsNullOrEmpty(streamID))
                        addFile(filePath, streamID);
                    if (_loadMixerConfig)
                    {
                        GetGuestElement();
                        LoadChromaAndCGConfig();
                        //RemovePlugins();

                        if (BorderElement != null)
                            BorderElement.ElementReorder(1000);
                        VideoColorEffects colorEffects = FrameworkHelper.Common.MLHelpers.ReadColorXml(System.IO.Path.Combine(ConfigManager.DigiFolderPath, "Profiles", lstMixerScene[videoprofilecount].Name, "Color.xml"));
                        if (colorEffects != null)
                        {
                            m_objMixer.PluginsAdd(m_objColors, 0);
                            Thread.Sleep(500);
                            SetColorConversion(colorEffects);
                            SetBrightnessContrast(colorEffects);
                            Thread.Sleep(500);
                            ApplyColor = true;
                        }
                    }
                    else
                    {
                        LoadChromaAndRouteSettings(objVideoScene.lstVideoSceneObject);
                        if (FlipMode > 0)
                        {
                            MElement me = GetGuestVideoElement(streamID);
                            ApplyFlip(me, FlipMode);
                            Marshal.ReleaseComObject(me);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }
        private CoMColorsClass m_objColors;

        private void SetColorConversion(VideoColorEffects colorEffects)
        {
            if (m_objColors != null)
            {
                MG_COLOR_PARAM colorParam = new MG_COLOR_PARAM
                {
                    dblUlevel = colorEffects.ULevel / 100,
                    dblUVGain = colorEffects.UVGain / 100,
                    dblVlevel = colorEffects.VLevel / 100,
                    dblYGain = colorEffects.YGain / 100,
                    dblYlevel = colorEffects.YLevel / 100
                };
                m_objColors.SetColorParam(ref colorParam, 1, 0);
            }
        }
        private void SetBrightnessContrast(VideoColorEffects colorEffects)
        {
            if (m_objColors != null)
            {
                MG_BRIGHT_CONT_PARAM brightContParam = new MG_BRIGHT_CONT_PARAM
                {
                    dblWhiteLevel = colorEffects.WhiteLevel / 100,
                    dblContrast = colorEffects.Contrast / 100,
                    dblColorGain = colorEffects.ColorGain / 100,
                    dblBrightness = colorEffects.Brightness / 100,
                    dblBlackLevel = colorEffects.BlackLevel / 100
                };
                m_objColors.SetBrightContParam(ref brightContParam, 1, 0);
            }
        }
        private MElement GetGuestVideoElement(string comparedID)
        {
            MElement GuestElement = null;
            int tempCount = 0;
            m_objComposer.ElementsGetCount(out tempCount);

            for (int i = 0; i < tempCount; i++)
            {
                MElement tmpElem;
                m_objComposer.ElementsGetByIndex(i, out tmpElem);
                if (tmpElem != null)
                {
                    int tempCount2 = 0;
                    ((IMElements)tmpElem).ElementsGetCount(out tempCount2);
                    for (int k = 0; k < tempCount2; k++)
                    {
                        MElement tmpChildElem;
                        ((IMElements)tmpElem).ElementsGetByIndex(k, out tmpChildElem);

                        if (tmpChildElem != null)
                        {
                            string strID; int have = 0;
                            tmpChildElem.AttributesHave("stream_id", out have, out strID);
                            ///  tmpChildElem.("stream_id", out strID);
                            if (strID == comparedID)
                            {
                                GuestElement = tmpChildElem;
                                break;
                            }
                        }
                    }

                }
            }
            return GuestElement;
        }

        private void ApplyFlip(MElement pElement, int FlipMode)
        {
            //MElement elemRoute = GetMElement(LiveVideoObjectID);
            if (FlipMode == 1)
                pElement.ElementMultipleSet("rh=180", 0.0);
            else if (FlipMode == 2)
                pElement.ElementMultipleSet("rv=180", 0.0);
        }
        public MLCHARGENLib.CoMLCharGen m_objCharGen;
        string OverlayStream = string.Empty;
        MElement OverlayElement;
        bool IsOverlayApplied = false;

        private void LoadChromaAndCGConfig()
        {
            try
            {
                string streamID = "stream-000";
                //RemovePlugins();
                m_objCharGen = new MLCHARGENLib.CoMLCharGen();
                m_objMixer.PluginsAdd(m_objCharGen, 0);
                ConfigBusiness objConfigBL = new ConfigBusiness();
                //CGConfigSettings objCGConfig = new CGConfigSettings();
                CGConfigSettings objCGConfig = null;
                if (objMixerScene.CG_ConfigID != 0)
                    objCGConfig = objConfigBL.GetCGConfigSettngs(lstMixerScene[videoprofilecount].CG_ConfigID).FirstOrDefault();
                Thread.Sleep(1000);
                int have = 0;
                string value;
                OverlayElement.AttributesHave("show", out have, out value);
                if (value == "true")
                    IsOverlayApplied = true;

                if (loadChroma)
                {
                    //MItem pItem; int myIndex;

                    //if (!String.IsNullOrEmpty(OverlayStream))
                    //    m_objMixer.StreamsGet(OverlayStream, out myIndex, out pItem);//now
                    //else
                    //    m_objMixer.StreamsGet(streamID, out myIndex, out pItem);

                    string chromaPath = "";// System.IO.Path.Combine(ConfigManager.DigiFolderPath, "Profiles", objMixerScene.Name, objMixerScene.Name + "_Chroma.xml");

                    //  if (!IsOverlayApplied)
                    chromaPath = System.IO.Path.Combine(ConfigManager.DigiFolderPath, "Profiles", lstMixerScene[videoprofilecount].Name, lstMixerScene[videoprofilecount].Name + "_Chroma.xml");
                    LoadChromaFromFile(streamID, chromaPath);
                }
                if (IsOverlayApplied)
                {
                    OverlayElement.AttributesStringGet("stream_id", out OverlayStream);
                    string OverlaychromaPath = System.IO.Path.Combine(ConfigManager.DigiFolderPath, "Profiles", lstMixerScene[videoprofilecount].Name, lstMixerScene[videoprofilecount].Name + "_OverlayChroma.xml");
                    LoadChromaFromFile(OverlayStream, OverlaychromaPath);
                }

                if (objCGConfig != null)
                {
                    MLCHARGENLib.IMLXMLPersist pXMLPersist = (MLCHARGENLib.IMLXMLPersist)m_objCharGen;
                    string cgConfigPath = System.IO.Path.Combine(ConfigManager.DigiFolderPath, "CGSettings", objCGConfig.ConfigFileName + objCGConfig.Extension);
                    if (File.Exists(cgConfigPath))
                        pXMLPersist.LoadFromXML(cgConfigPath, 0);
                    //mMixerList1.UpdateList(true);

                    //List<CGBaseItem> items = m_Editor.CGItems;
                    //m_Editor.UpdateItemsList();
                    //m_objMixer.PluginsGetByIndex
                }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }
        private void LoadChromaFromFile(string streamID, string chromaPath)
        {
            try
            {
                MItem pItem; int myIndex;
                m_objMixer.StreamsGet(streamID, out myIndex, out pItem);
                //
                //Load ChromaPlugin For MItem
                LoadChromaPlugin(true, pItem);
                Thread.Sleep(500);
                MCHROMAKEYLib.MChromaKey objChromaKey = GetChromakeyFilter(pItem);
                try
                {
                    ((IMProps)objChromaKey).PropsSet("gpu_mode", "true");
                }
                catch { }
                Thread.Sleep(500);
                //Apply chroma
                if (File.Exists(chromaPath))
                    (objChromaKey as IMPersist).PersistLoad("", chromaPath, "");
                Thread.Sleep(1000);
                Marshal.ReleaseComObject(objChromaKey);
                Marshal.ReleaseComObject(pItem);
                GC.Collect();
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }
        private void RemovePlugins()
        {
            try
            {
                if (m_objMixer != null)
                {
                    m_objCharGen.RemoveItem("", 1000);
                    m_objMixer.PluginsRemove(m_objCharGen);

                    if (ApplyColor)
                        m_objMixer.PluginsRemove(m_objColors);
                    //Marshal.ReleaseComObject(m_objCharGen);
                    m_objCharGen = null;
                }
            }
            catch (System.Exception ex)
            { }


        }
        private void startProcess()
        {

            try
            {
                //VidTimer.Stop();
                m_objComposer.OnFrame += new IMEvents_OnFrameEventHandler(m_objComposer_OnFrame);
                //CancelbwRoute = false;
                //Initilizing of  input files
                InitializeAllInputFiles();
                //  m_objComposer.FilePlayStart();
                //Apply Route
                // bwRoute.RunWorkerAsync();    

                //StartWriter(m_objComposer);
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }
        string VideoSettings = string.Empty;
        private void LoadWriterSettings(string settings)
        {
            try
            {
                XmlDocument xdoc = new XmlDocument();
                xdoc.LoadXml(settings);
                XmlNodeList nodes = xdoc.GetElementsByTagName("Settings");
                if (nodes.Count > 0)
                {
                    foreach (XmlNode node in nodes)
                    {
                        int Videoid = Convert.ToInt32(node.ChildNodes[1].InnerText);
                        int Audioid = Convert.ToInt32(node.ChildNodes[3].InnerText);
                        string OpFormat = node.ChildNodes[4].InnerText;
                        string Videocodec = node.ChildNodes[6].InnerText.Trim();
                        string Audiocodec = node.ChildNodes[5].InnerText.Trim();
                        mFormatControl1.comboBoxVideo.SelectedIndex = Videoid;
                        mFormatControl1.comboBoxAudio.SelectedIndex = Audioid;

                        if (node.ChildNodes.Count >= 6)
                            VideoSettings = node.ChildNodes[7].InnerText.Trim();

                        (((MControls.ListViewEx)(mConfigList1)).Columns[1].ListView).Items[0].SubItems[1].Text = OpFormat;//o/p format
                        (((MControls.ListViewEx)(mConfigList1)).Columns[1].ListView).Items[1].SubItems[1].Text = Audiocodec;//audiocodec

                        if (Videocodec != "")
                        { (((MControls.ListViewEx)(mConfigList1)).Columns[1].ListView).Items[2].SubItems[1].Text = Videocodec; }//videocodec   }

                        IMAttributes pConfigProps;
                        m_objWriter.ConfigSet("format", OpFormat, out pConfigProps);
                        int bHave = 0; string strExtensions = string.Empty;
                        pConfigProps.AttributesHave("extensions", out bHave, out strExtensions);

                        string[] arr = strExtensions.Split(',');
                        outputFormat = arr[0]; //set output format
                    }
                }
            }
            catch
            {

            }
        }
        private void startProcessMixer()
        {

            try
            {
                //VidTimer.Stop();
                //m_objComposer.OnFrame += new IMEvents_OnFrameEventHandler(m_objComposer_OnFrame);
                //CancelbwRoute = false;
                //Initilizing of  input files
                //InitializeAllInputFilesMixer();
                //  m_objComposer.FilePlayStart();
                //Apply Route
                // bwRoute.RunWorkerAsync(); 
                StreamList slAudio = lstStreamDetails.FirstOrDefault();
                if (slAudio != null)
                {
                    StreamList slAudioNext = lstStreamDetails.Where(o => o.Index == slAudio.Index + 1).FirstOrDefault();
                    if (slAudioNext != null)
                        NextAudioStartTime = slAudioNext.InsertTime;
                    else NextAudioStartTime = 0;
                    if (lstStreamDetails.Count == 1)
                        NextAudioStartTime = videoLength;
                    CurrentAudioStream = slAudio.streamName;
                    CurrentAudioStoptTime = slAudio.InsertTime + (slAudio.Out - slAudio.In);
                    if (slAudio.InsertTime == 0)
                    {
                        AudioElement.ElementMultipleSet("stream_id=" + CurrentAudioStream + " audio_gain=1", 0.0);
                        //InitializeStream(CurrentAudioStream, false);//Not supported in 6.4 onwards
                    }
                }
                Thread.Sleep(1000);
                StartWriter(m_objMixer);
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }
        MItem thisBackground;
        private void Initilizebackground()
        {
            string ss = string.Empty;
            m_objMixer.StreamsBackgroundGet(out ss, out thisBackground);
            if (thisBackground != null)
            {
                //myBackground.FilePosSet(MHelpers.ParsePos("00:00:00"), 1.0);
                thisBackground.FilePlayPause(0);
                thisBackground.FilePosSet(0, 0);
                thisBackground.FilePlayStart();
            }
        }
        private void InitializeAllInputFilesMixer()
        {
            MItem m_pFile; string streamId = string.Empty;
            int nCount = 0;
            m_objMixer.StreamsGetCount(out nCount);

            ErrorHandler.ErrorHandler.LogFileWrite("StreamsGetCount nCount" + Convert.ToString(nCount));

            for (int i = 0; i < nCount; i++)
            {
                m_objMixer.StreamsGetByIndex(i, out streamId, out m_pFile);
                try
                {
                    if (m_pFile != null)
                    {
                        //m_pFile.FilePosSet(MHelpers.ParsePos("00:00:00"), 1.0);
                        //m_pFile.FilePlayPause(0);
                        //m_pFile.FilePosSet(0, 0);

                        m_objMixer.FilePlayPause(0);
                        m_objMixer.FilePosSet(0, 0);
                    }
                }
                catch (Exception ex)
                {
                    string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                    ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                }
            }
        }

        bool isVideoInProgress = false;//Vins
        private void StartWriter(IMObject m_object)
        {
            isVideoInProgress = true;            
            //uniqueName = GetVideoName();
            AdvVidTimer.Start();
            pbProgress.Maximum = videoLength;
            //  ExtractThumbnail();
            mFormatControl1.SetControlledObjectForMWriter(m_object);

            eMState eState;
            m_objWriter.ObjectStateGet(out eState);
            if (eState == eMState.eMS_Paused)
            {
                // Continue capture
                m_objWriter.ObjectStart(m_object);
            }
            if (eState == eMState.eMS_Running)
            {
                m_objWriter.WriterNameSet("", "");
            }
            else
            {
                string strCapturePath_or_URL = "";
                string strFormat;
                IMAttributes pFormatConfig;
                m_objWriter.ConfigGet("format", out strFormat, out pFormatConfig);
                tempFile = processVideoTemp + "EngineOutput" + "." + outputFormat;
                strCapturePath_or_URL = tempFile;
                try
                {
                    m_objWriter.WriterNameSet(strCapturePath_or_URL, VideoSettings != "" ? VideoSettings : "video::bitrate=1M audio::bitrate=64K");
                    if (m_object == m_objMixer)
                    {
                        Initilizebackground();
                        InitializeAllInputFilesMixer();

                        ErrorHandler.ErrorHandler.LogFileWrite("1595 Convert.ToString(lstMixerScene[videoprofilecount].VideoLength) =" + Convert.ToString(lstMixerScene[videoprofilecount].VideoLength));

                        m_objWriter.PropsSet("max_duration", Convert.ToString(lstMixerScene[videoprofilecount].VideoLength));
                        m_objWriter.ObjectStart(m_object);                        
                        m_objMixer.FilePlayStart();
                    }
                    else
                    {
                        ErrorHandler.ErrorHandler.LogFileWrite("1603");
                        m_objWriter.ObjectStart(m_object);
                    }
                }
                catch (System.Exception ex)
                {
                    string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                    ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                    //MessageBox.Show("Error start capture to '" + strCapturePath_or_URL + "' file." +
                    //"\r\nFile Format: " + strFormat + "\r\n\r\n" + ex.Message);
                    //return;
                }
            }

            isVideoInProgress = false;//Vins         
        }

        private void DisposeMLObjects()
        {
            m_objComposer.ObjectClose();
            m_objWriter.ObjectClose();
            //Marshal.ReleaseComObject(m_objComposer);
            //Marshal.ReleaseComObject(m_objWriter);
        }
        private void stopWriter()
        {
            try
            {
                AdvVidTimer.Stop();
                if (!_loadMixerConfig)
                {                    
                    m_objComposer.OnFrame -= new IMEvents_OnFrameEventHandler(m_objComposer_OnFrame);
                    m_objComposer.FilePlayStop(0);
                }
                else
                {                    
                    Thread.Sleep(500);
                    m_objMixer.FilePlayStop(0);
                    if (ApplyColor)
                        m_objMixer.PluginsRemove(m_objColors);
                    Marshal.ReleaseComObject(m_objColors);
                    Marshal.ReleaseComObject(m_objCharGen);
                    //Marshal.FinalReleaseComObject(m_objCharGen);
                }
                frameCount = 0;
                m_objWriter.ObjectClose();
                saveOutputVideo();
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }

        private static void KillMserverProcesses()
        {
            try
            {
                Process[] MserverRunning = Process.GetProcessesByName("MServer");
                if (MserverRunning.Length > 1)
                {
                    foreach (Process mserver in MserverRunning)
                        mserver.Kill();
                }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }
        private void ExtractThumbnail()
        {
            MFrame mf1;
            if (_loadMixerConfig)
                m_objMixer.ObjectFrameGet(out mf1, "");
            else
                m_objComposer.ObjectFrameGet(out mf1, "");
            path_tempThumbnail = processVideoTemp + "EngineOutput.jpg";
            if (File.Exists(path_tempThumbnail))
            {
                File.Delete(path_tempThumbnail);
            }
            mf1.FrameVideoSaveToFile(path_tempThumbnail);
            Marshal.ReleaseComObject(mf1);

        }
        private string GetVideoName()
        {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            var random = new Random();
            string name = "live_" + new string(Enumerable.Repeat(chars, 6).Select(s => s[random.Next(s.Length)]).ToArray());

            return name;
        }
        bool loadChroma = true;
        void addFile(string filePath, string streamId)
        {
            // If some streams selected -> change this stream
            //string strItemID = listViewFiles.SelectedItems.Count > 0 ? listViewFiles.SelectedItems[0].Text : "";
            //string strTrans = listViewFiles.SelectedItems.Count > 0 && strTransition != "" ? " transition='" + strTransition + "'" : "";
            //double dblTimeForChange = listViewFiles.SelectedItems.Count > 0 ? (double)numericChangeTime.Value : 0;
            if (_loadMixerConfig)
            {
                string m_editedImgPtah = string.Empty;
                if (_isGreenScrrenFlow)
                {
                    m_editedImgPtah = filePath.Replace("jpg", "png").Replace("JPG", "PNG");
                    if (File.Exists(m_editedImgPtah))
                    {
                        filePath = m_editedImgPtah;
                        loadChroma = false;
                    }
                }
                m_pMixerStreams = (IMStreams)m_objMixer;

            }

            else
                m_pMixerStreams = (IMStreams)m_objComposer;

            MItem pFile = null;
            Cursor prev = this.Cursor;
            //this.Cursor = Cursors.WaitCursor;

            try
            {
                //m_pMixerStreams.StreamsAdd(streamId, null, filePath, null, out pFile, 0);
                m_pMixerStreams.StreamsAdd(streamId, null, filePath, "loop=false", out pFile, 0);
                //"loop=false"
            }
            catch (Exception ex)
            {
                // MessageBox.Show("File " + filePath + " can't be added as stream");
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }

            this.Cursor = prev;


            if (pFile != null)
                mMixerList1.SelectFile(pFile);
            // mMixerList1.UpdateList(true, 1);

        }
        private void loadSceneDetails()
        {
            try
            {
                business = new VideoSceneBusiness();
                lstVideoScene = new List<VideoScene>();
                objVideoScene = new VideoScene();
                lstVideoScene = business.GetVideoSceneByCriteria(0, 0);
                foreach (VideoScene item in lstVideoScene)
                {
                    item.lstVideoSceneObject = business.GuestVideoObjectBySceneID(item.SceneId);
                }
                //objVideoScene = lstVideoScene.Where(x => x.LocationId == LocationId).FirstOrDefault();
                //objSceneObject = business.GuestVideoObjectBySceneID(objVideoScene.SceneId).Where(x => x.GuestVideoObject == true).FirstOrDefault();
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }
        private void LoadChromaAndRouteSettings(List<VideoSceneObject> lstVideoSceneObject)
        {
            try
            {
                foreach (var item in lstVideoSceneObject.Where(o => o.ObjectFileMapping.ChromaPath != null || o.ObjectFileMapping.RoutePath != null))
                {
                    if (item.ObjectFileMapping.ChromaPath != "")
                    {
                        MItem pItem; int myIndex;
                        m_objComposer.StreamsGet(item.VideoObjectId, out myIndex, out pItem);
                        //string hotfolder = ConfigManager.DigiFolderPath.Replace("\\\\DigiImages\\\\", "\\DigiImages\\");
                        string chromaPath = System.IO.Path.Combine(ConfigManager.DigiFolderPath, item.ObjectFileMapping.ChromaPath);
                        //Load ChromaPlugin For MItem
                        LoadChromaPlugin(true, pItem);
                        MCHROMAKEYLib.MChromaKey objChromaKey = GetChromakeyFilter(pItem);
                        try
                        {
                            ((IMProps)objChromaKey).PropsSet("gpu_mode", "true");
                        }
                        catch (Exception ex)
                        {
                            string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                            ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                        }
                        ////Apply chroma
                        //if (System.IO.File.Exists(chromaPath))
                        //    (objChromaKey as IMPersist).PersistLoad("", chromaPath, "");
                        if (System.IO.File.Exists(chromaPath) && objChromaKey != null)
                        {
                            (objChromaKey as IMPersist).PersistLoad("", chromaPath, "");
                            Marshal.ReleaseComObject(objChromaKey);
                            Marshal.ReleaseComObject(pItem);
                            GC.Collect();
                        }
                        Thread.Sleep(500);
                    }
                    if (item.ObjectFileMapping.RoutePath != "")
                    {
                        string vidId = "";
                        string rootpath = System.IO.Path.Combine(ConfigManager.DigiFolderPath, item.ObjectFileMapping.RoutePath);
                        vidId = item.ObjectFileMapping.RoutePath.Substring(item.ObjectFileMapping.RoutePath.LastIndexOf("\\") + 1).Replace("Route-", "");
                        vidId = vidId.Substring(0, vidId.Length - 4);

                        ReadVideoObjectRouteFile(rootpath, vidId);
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }
        private MCHROMAKEYLib.MChromaKey GetChromakeyFilter(object source)
        {
            MCHROMAKEYLib.MChromaKey pChromaKey = null;
            try
            {
                int nCount = 0;
                IMPlugins pPlugins = (IMPlugins)source;
                pPlugins.PluginsGetCount(out nCount);
                for (int i = 0; i < nCount; i++)
                {
                    object pPlugin;
                    long nCBCookie;
                    pPlugins.PluginsGetByIndex(i, out pPlugin, out nCBCookie);
                    try
                    {
                        pChromaKey = (MCHROMAKEYLib.MChromaKey)pPlugin;
                        break;
                    }
                    catch { }
                }
            }
            catch { }
            return pChromaKey;
        }
        private void ReadVideoObjectRouteFile(string filePath, string vidId)
        {
            int counter = 0; Hashtable htRouteList = new Hashtable();
            string line;
            string x = string.Empty;
            string y = string.Empty;
            string h = string.Empty;
            string w = string.Empty;
            string r = string.Empty;
            string d = string.Empty;
            System.IO.StreamReader file = new System.IO.StreamReader(filePath);
            while ((line = file.ReadLine()) != null)
            {
                string[] coordinates = line.Split(';');
                if (coordinates.Length > 1)
                {
                    Hashtable htListVar = new Hashtable();

                    x = coordinates[0];
                    y = coordinates[1];
                    h = coordinates[2];
                    w = coordinates[3];
                    r = coordinates[4];
                    d = coordinates[5];

                    htListVar.Add("x", x);
                    htListVar.Add("y", y);
                    htListVar.Add("h", h);
                    htListVar.Add("w", w);
                    htListVar.Add("r", r);
                    htListVar.Add("tc", d);
                    htRouteList.Add(counter, htListVar);
                    counter++;
                }
            }
            file.Close();
            if (!htVideoObjects.ContainsKey(vidId))
                htVideoObjects.Add(vidId, htRouteList);
        }
        private void LoadChromaPlugin(bool onloadChroma, object source)
        {
            IMPlugins pPlugins = source as IMPlugins;
            int nCount;
            long nCBCookie;
            object pPlugin = null;
            bool isCK = false;
            pPlugins.PluginsGetCount(out nCount);
            for (int i = 0; i < nCount; i++)
            {
                pPlugins.PluginsGetByIndex(i, out pPlugin, out nCBCookie);

                if (pPlugin.GetType().Name == "CoMChromaKeyClass" || pPlugin.GetType().Name == "MChromaKeyClass")
                {
                    isCK = true;
                    break;
                }
            }
            if (isCK == false)
            {
                pPlugins.PluginsAdd(new MCHROMAKEYLib.MChromaKey(), 0);
            }
        }
        void mMixerList1_OnMixerSelChanged(object sender, EventArgs e)
        {
            ListView listView = (ListView)sender;
            if (listView.SelectedItems.Count > 0)
            {
                mFileSeeking1.Enabled = true;
                mFileSeeking1.SetControlledObject(m_objComposer);
                mFileState1.Enabled = true;
                mFileState1.SetControlledObject(m_objComposer);
            }
            else
            {
                mFileSeeking1.Enabled = true;
                mFileSeeking1.SetControlledObject(m_objComposer);
                mFileState1.Enabled = true;
                mFileState1.SetControlledObject(m_objComposer);
            }
        }
        private void InitializeAllInputFiles()
        {
            try
            {
                MItem m_pFile; string streamId = string.Empty;
                int nCount = 0;
                m_objComposer.StreamsGetCount(out nCount);
                m_objComposer.FilePlayPause(0);
                for (int i = 0; i < nCount; i++)
                {
                    m_objComposer.StreamsGetByIndex(i, out streamId, out m_pFile);
                    try
                    {
                        //string name = string.Empty;
                        //m_pFile.FileNameGet(out name);
                        // if (m_pFile != null)
                        // {
                        m_pFile.FilePosSet(0, 0);

                        //}
                    }
                    catch { }
                }
                m_objComposer.FilePlayStart();
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }
        //static int _processedVideoCount = 0;
        private void saveOutputVideo()
        {
            try
            {
                //get length of video
                //tempFile
                
                PhotoBusiness photoBusiness = new PhotoBusiness();
                string pathToSave = FolderInfo.HotFolderPath + "ProcessedVideos\\" + DateTime.Now.ToString("yyyyMMdd");
                string fileName = photoDetails.PhotoRFID.ToString() + "_" + DateTime.Now.ToString("HH:mm:ss").Replace(":", "") + "." + outputFormat;
                string outputfile = pathToSave + "\\" + fileName;
                string thumbPath = System.IO.Path.Combine(FolderInfo.HotFolderPath, "Thumbnails", DateTime.Now.ToString("yyyyMMdd"));
                if (!Directory.Exists(pathToSave))
                    Directory.CreateDirectory(pathToSave);
                if (!Directory.Exists(thumbPath))
                    Directory.CreateDirectory(thumbPath);
                if (File.Exists(tempFile))
                    File.Move(tempFile, outputfile);
                if (File.Exists(path_tempThumbnail))
                    //File.Move(path_tempThumbnail, thumbPath);
                    ResizeWPFImage(path_tempThumbnail, 210, System.IO.Path.Combine(thumbPath, System.IO.Path.GetFileNameWithoutExtension(fileName) + ".jpg"));
                //*** Code Added by Anis for Magic Shot kitted on 26th Nov 18 ***//
                //int VideoId = photoBusiness.SetPhotoDetails(ConfigManager.SubStoreId, photoDetails.PhotoRFID, fileName, DateTime.Now, photoDetails.PhotoGrapherId.ToString(), "", photoDetails.LocationId, string.Empty, string.Empty, null, null, 1, null, videoLength, true, 3);
                int VideoId = photoBusiness.SetPhotoDetails(ConfigManager.SubStoreId, photoDetails.PhotoRFID, fileName, DateTime.Now, photoDetails.PhotoGrapherId.ToString(), "", photoDetails.LocationId, string.Empty, string.Empty, null, null, 1, null, videoLength, true, 3, null, null, photoDetails.SemiOrderProfileId);
                //*** End Here ***//
                //UpdateVideoProcessedStatus(photoDetails.PhotoId, true);
                ProcessedVideos++;
                prgbar.Value = ProcessedVideos;
                txbProcessed.Text = ProcessedVideos.ToString();
                pbProgress.Value = 0;

                if (ConfigManager.IMIXConfigurations.ContainsKey((int)ConfigParams.AutoVidDisplayEnabled) && Convert.ToBoolean(ConfigManager.IMIXConfigurations[(int)ConfigParams.AutoVidDisplayEnabled]) == true)
                {
                    lstFilependingCopy.Add(outputfile);
                    CheckFilesPendingForCopy();
                    //bwCopyFiles.RunWorkerAsync(outputfile);
                }
                VideoAssociation(photoDetails.PhotoId, VideoId);

                if (IsMemoryOverflow())
                {
                    KillMserverProcesses();
                    Process thisProc = Process.GetCurrentProcess();
                    System.Windows.Forms.Application.Restart();
                    thisProc.Kill();
                    Application.Current.Shutdown();
                }
                //_processedVideoCount++;
                //if (_processedVideoCount == totalVideos)
                VidTimer.Start();
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
            //string pathToSave = FolderInfo.HotFolderPath + "ProcessedVideos\\" + DateTime.Now.ToString("yyyyMMdd");
        }

        #endregion
        private void Window_Closed(object sender, EventArgs e)
        {
            Process thisProc = Process.GetCurrentProcess();
            // Check how many total processes have the same name as the current one
            if (Process.GetProcessesByName(thisProc.ProcessName).Length >= 1)
            {
                thisProc.Kill();
                Application.Current.Shutdown();
            }
        }

        private void Window_Closing(object sender, CancelEventArgs e)
        {
            //System.Windows.Application.Current.Shutdown();
        }

        private void GetAudioListToBeApplied(VideoScene scene)
        {
            XmlDocument xmlDoc = new XmlDocument();
            string audioXmlPath = System.IO.Path.Combine(ConfigManager.DigiFolderPath, "Profiles", scene.Name, scene.Name + "_Audio.xml");
            if (File.Exists(audioXmlPath))
            {
                xmlDoc.Load(audioXmlPath);
                XmlNodeList nodeList = xmlDoc.GetElementsByTagName("audio");
                int index = 1;
                StreamList st;
                foreach (XmlNode node in nodeList)
                {
                    st = new StreamList();
                    //lstStreamDetails.Add(st);

                    st.Index = index;
                    st.ItemID = Convert.ToInt32(node["ItemID"].InnerText);
                    st.FilePath = System.IO.Path.Combine(ConfigManager.DigiFolderPath, "Audio", node["FilePath"].InnerText.ToString());
                    st.In = Convert.ToDouble(node["StartTime"].InnerText);
                    st.Out = Convert.ToDouble(node["StopTime"].InnerText);
                    st.InsertTime = Convert.ToDouble(node["InsertTime"].InnerText);

                    string streamName = AddAudioFile(st.FilePath, st.In, st.Out, st.InsertTime, 0);
                    st.streamName = streamName;
                    lstStreamDetails.Add(st);
                    index++;
                }
                if (nodeList.Count > 0 && m_objMixer != null)
                {
                    MItem myBackground; string ss = string.Empty;
                    m_objMixer.StreamsBackgroundGet(out ss, out myBackground);
                    ((MPLATFORMLib.IMProps)myBackground).PropsSet("object::audio_gain", "-100"); //to mute the background video
                }
            }

        }

        string AddAudioFile(string filePath, double InTime, double OutTime, double InsertTime, int MediaType)
        {
            string streamID = string.Empty;
            try
            {
                m_pMixerStreams = (IMStreams)m_objMixer;
                MItem pFile = null;
                try
                {
                    m_pMixerStreams.StreamsAdd("", null, filePath, "loop=false", out pFile, 0);
                    int count;
                    m_pMixerStreams.StreamsGetCount(out count);
                    m_pMixerStreams.StreamsGetByIndex(count - 1, out streamID, out pFile);
                    if (MediaType != 601)
                        InitializeStream(streamID, true);

                    // Get new In/Out
                    double dblIn = MHelpers.ParsePos(InTime.ToString());
                    double dblOut = MHelpers.ParsePos(OutTime.ToString());
                    // Set new in-out
                    pFile.FileInOutSet(dblIn, dblOut);
                }
                catch (Exception)
                {
                    MessageBox.Show("File " + filePath + " can't be added as stream");
                }
                try
                {
                    if (pFile != null)
                        mMixerList1.SelectFile(pFile);
                }
                catch { }
                mMixerList1.UpdateList(true, 1);
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
            return streamID;
        }
        private void InitializeStream(string streamId, bool isStop)
        {
            try
            {
                MItem m_pFileItem;
                int nCount = 0;
                m_objMixer.StreamsGet(streamId, out nCount, out m_pFileItem);
                if (m_pFileItem != null)
                {
                    m_pFileItem.FilePosSet(MHelpers.ParsePos("00:00:00"), 1.0);
                    if (isStop)
                        m_pFileItem.FilePlayStop(0);
                    else
                        m_pFileItem.FilePlayStart();
                }
            }
            catch (Exception ex)
            {
                //string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                //ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }
        class StreamList
        {
            public int Index { get; set; }
            public string streamName { get; set; }
            public double In { get; set; }
            public double Out { get; set; }
            public int ItemID { get; set; }
            public string FilePath { get; set; }
            public double InsertTime { get; set; }
            public int MediaType { get; set; }
        }
    }

    public static class PerformanceInfo
    {
        [DllImport("psapi.dll", SetLastError = true)]
        [return: MarshalAs(UnmanagedType.Bool)]
        public static extern bool GetPerformanceInfo([Out] out PerformanceInformation PerformanceInformation, [In] int Size);

        [StructLayout(LayoutKind.Sequential)]
        public struct PerformanceInformation
        {
            public int Size;
            public IntPtr CommitTotal;
            public IntPtr CommitLimit;
            public IntPtr CommitPeak;
            public IntPtr PhysicalTotal;
            public IntPtr PhysicalAvailable;
            public IntPtr SystemCache;
            public IntPtr KernelTotal;
            public IntPtr KernelPaged;
            public IntPtr KernelNonPaged;
            public IntPtr PageSize;
            public int HandlesCount;
            public int ProcessCount;
            public int ThreadCount;
        }

        public static Int64 GetPhysicalAvailableMemoryInMiB()
        {
            PerformanceInformation pi = new PerformanceInformation();
            if (GetPerformanceInfo(out pi, Marshal.SizeOf(pi)))
            {
                return Convert.ToInt64((pi.PhysicalAvailable.ToInt64() * pi.PageSize.ToInt64() / 1048576));
            }
            else
            {
                return -1;
            }
        }

        public static Int64 GetTotalMemoryInMiB()
        {
            PerformanceInformation pi = new PerformanceInformation();
            if (GetPerformanceInfo(out pi, Marshal.SizeOf(pi)))
            {
                return Convert.ToInt64((pi.PhysicalTotal.ToInt64() * pi.PageSize.ToInt64() / 1048576));
            }
            else
            {
                return -1;
            }
        }

    }
    class MemoryManagement
    {
        PerformanceCounter cpuCounter = new PerformanceCounter("Processor", "% Processor Time", "_Total");
        PerformanceCounter ramCounter = new PerformanceCounter("Memory", "Available MBytes");

        public int getCurrentCpuUsage()
        {
            cpuCounter.NextValue();
            Thread.Sleep(500);
            return (int)cpuCounter.NextValue();
        }

        public int getAvailableRAM()
        {
            return (int)ramCounter.NextValue();
        }
    }

}
