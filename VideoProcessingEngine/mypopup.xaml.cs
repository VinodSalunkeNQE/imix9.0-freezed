﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Diagnostics;
using System.Threading;
using System.Diagnostics;
using System.Threading;
using DigiPhoto.IMIX.Business;
using DigiPhoto.IMIX.Model;

namespace VideoProcessingEngine
{
    /// <summary>
    /// Interaction logic for mypopup.xaml
    /// </summary>
    public partial class mypopup : UserControl
    {
        public mypopup()
        {
            InitializeComponent();
            ///by latika to implement locationwise settings 25/Feb/2019
            StoreInfo store = new StoreInfo();
            TaxBusiness taxBusiness = new TaxBusiness();
            store = taxBusiness.getTaxConfigDataBySystemID(Environment.MachineName);

            bool RunVideoProcessingEngine = false;
            Thread newWindowThread = new Thread(new ThreadStart(() =>
            {


                RunVideoProcessingEngine = store.RunVideoProcessingEngineLocationWise;
                ///////commented by latika as per requirement 
                //if (RunVideoProcessingEngine == false)
                //{
                //    MessageBox.Show("Video Processing Engine is not configured in Locationwise setting with this System-" + Environment.MachineName + ". \n Currently running sitewise for faster process Configure Locationwise setting from DigConfig-VOS .");
                //}
                /////end
                VideoProcessor objImageProcessor = new VideoProcessor();
                    objImageProcessor.Show();
                
              
                System.Windows.Threading.Dispatcher.Run();
            }));
            ///End
            newWindowThread.SetApartmentState(ApartmentState.STA);
            newWindowThread.IsBackground = false;
            newWindowThread.Start();
        }
        private void OnButtonClick(object sender, RoutedEventArgs e)
        {
            Process thisProc = Process.GetCurrentProcess();
            // Check how many total processes have the same name as the current one
            if (Process.GetProcessesByName(thisProc.ProcessName).Length >= 1)
            {
                Application.Current.Shutdown();
                thisProc.Kill();
            } 
        }
    }
}
