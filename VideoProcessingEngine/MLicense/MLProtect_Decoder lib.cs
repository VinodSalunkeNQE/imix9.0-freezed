//---------------------------------------------------------------------------
// MLProtect_Decoderlib.cs : Personal protection code for the MediaLooks License system
//---------------------------------------------------------------------------
// Copyright (c) 2012, MediaLooks Soft
// www.medialooks.com (dev@medialooks.com)
//
// Authors: MediaLooks Team
// Version: 3.0.0.0
//
//---------------------------------------------------------------------------
// CONFIDENTIAL INFORMATION
//
// This file is Intellectual Property (IP) of MediaLooks Soft and is
// strictly confidential. You can gain access to this file only if you
// sign a License Agreement and a Non-Disclosure Agreement (NDA) with
// MediaLooks Soft If you had not signed any of these documents, please
// contact <dev@medialooks.com> immediately.
//
//---------------------------------------------------------------------------
// Usage:
//
// 1. Copy MLProxy.dll from the license package to any folder and register it
//    in the system registry by using regsvr32.exe utility
//
// 2. Add the reference to MLProxy.dll in the C# project
//
// 3. Add this file to the project
//
// 4. Call DecoderlibLic.IntializeProtection() method before creating any Medialooks 
//    objects (for e.g. in Form_Load event handler)
//
// 5. Compile the project
//
// IMPORTANT: If your application is running for a period longer than 24 
//            hours, the IntializeProtection() call should be repeated 
//            periodically  (every 24 hours or less).
//
// IMPORTANT: If your have several Medialooks products, don't forget to initialize
//            protection for all of them. For e.g.
//
//             MPlatformLic.IntializeProtection();
//             MDecodersLic.IntializeProtection();
//             etc.

using System;

    public class DecoderlibLic
    {        
        private static MLPROXYLib.CoMLProxyClass m_objMLProxy;
        private static string strLicInfo = @"[MediaLooks]
License.ProductName=Decoder lib
License.IssuedTo=Digiphoto Entertainment Imaging (DIGI PHOTO STUDIO)
License.CompanyID=9935
License.UUID={CAC391B7-218B-410B-9269-881276411AF3}
License.Key={8361373E-7C62-4E40-9231-C290BCD94A0B}
License.Name=MPlaylist Module
License.UpdateExpirationDate=June 22, 2017
License.Edition=MDecoders
License.AllowedModule=*.*
License.Signature=BEFC5A2332886E72C6D6CCAF130772D68038BEF3D0ADED7BB564AA0B7FF030C7D253141F78A243B54E59216D75829AFC22F73848600529A2133A086911108061ADE8F264E2DAA5D11CE9EF2DC0015D6EA2502D6311B266F448120D1DEBFCC1B15DA1F10B050E38DC663DF93FFBD2BE4F96D9D5875AFCAC4BB2C7B672186AE746

[MediaLooks]
License.ProductName=Decoder lib
License.IssuedTo=Digiphoto Entertainment Imaging (DIGI PHOTO STUDIO)
License.CompanyID=9935
License.UUID={72F1AEC9-A74D-48AD-B5E6-4A2ADB89FD94}
License.Key={9DC01825-9316-06D3-94A1-76DAD3352A3D}
License.Name=MFile Module
License.UpdateExpirationDate=June 22, 2017
License.Edition=MDecoders
License.AllowedModule=*.*
License.Signature=6238F712AAF0F520B9F6084C54E5DD5C1F01B6F57126A2FAA7E2B20C4A15E75BE233DF1ABB1C00B01708760C7491353AD2DFF65B6B707ED233BA770384CEE3FE9697757821425126727CF9ABA498D001EA7091AE4FB18BD22347817A3DBBD6B10F715E35A37DEF1BBED9DF9612427274574E8D8480D0135E5F1FD99A516B2A33

[MediaLooks]
License.ProductName=Decoder lib
License.IssuedTo=Digiphoto Entertainment Imaging (DIGI PHOTO STUDIO)
License.CompanyID=9935
License.UUID={3A11BCFD-AA9C-4BFE-BAAA-369B442DD720}
License.Key={9DC01825-FA59-2230-B66C-F1F5757FD85C}
License.Name=MMixer Module (BETA)
License.UpdateExpirationDate=June 22, 2017
License.Edition=MDecoders
License.AllowedModule=*.*
License.Signature=EE1F7549FED0C3F3EB0F17461EEF430908163E2E665F1B698CEE4552A48B97A0E1EB2E7601C9769746C81AE13A97D18A35A36DDEC25157BFE61349C1A79936C22A81292E60EAFEBCD6AE8E43B8979F00D243323FE37A093D9B5C1D9C2D217896DB57D039A129BA6830CCCF514BD950319082FC299A530086007EED3989468459

";

		//License initialization
        public static void IntializeProtection()
        {
            if (m_objMLProxy == null)
            {
                // Create MLProxy object 
                m_objMLProxy = new MLPROXYLib.CoMLProxyClass();
                m_objMLProxy.PutString(strLicInfo);                
            }
           UpdatePersonalProtection();
        }

        private static void UpdatePersonalProtection()
        {
            ////////////////////////////////////////////////////////////////////////
            // MediaLooks License secret key
            // Issued to: Digiphoto Entertainment Imaging (DIGI PHOTO STUDIO)
            const long _Q1_ = 62370083;
            const long _P1_ = 53855743;
            const long _Q2_ = 52173661;
            const long _P2_ = 55443887;

            try
            {

                int nFirst = 0;
                int nSecond = 0;
                m_objMLProxy.GetData(out nFirst, out  nSecond);

                // Calculate First * Q1 mod P1
                long llFirst = (long)nFirst * _Q1_ % _P1_;
                // Calculate Second * Q2 mod P2
                long llSecond = (long)nSecond * _Q2_ % _P2_;

                uint uRes = SummBits((uint)(llFirst + llSecond));

                // Calculate check value
                long llCheck = (long)(nFirst - 29) * (nFirst - 23) % nSecond;
                // Calculate return value
                int nRand = new Random().Next(0x7FFF);
                int nValue = (int)llCheck + (int)nRand * (uRes > 0 ? 1 : -1);

                m_objMLProxy.SetData(nFirst, nSecond, (int)llCheck, nValue);

            }
            catch (System.Exception) { }

        }

        private static uint SummBits(uint _nValue)
        {
            uint nRes = 0;
            while (_nValue > 0)
            {
                nRes += (_nValue & 1);
                _nValue >>= 1;
            }

            return nRes % 2;
        }
    }