﻿using Hardcodet.Wpf.TaskbarNotification;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Windows;
using System.Windows.Media.Animation;
using System.Windows.Threading;

namespace VideoProcessingEngine
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    ///      
    public partial class App : Application
    { 
        #region Declaration
        private TaskbarIcon notifyIcon;
        public static int SubStoreId;
        #endregion
        private void Application_Startup(object sender, StartupEventArgs e)
        {
            // Get Reference to the current Process
            Timeline.DesiredFrameRateProperty.OverrideMetadata(typeof(Timeline),
                              new FrameworkPropertyMetadata { DefaultValue = 5 });
            Process thisProc = Process.GetCurrentProcess();
            // Check how many total processes have the same name as the current one
            //if (Process.GetProcessesByName(thisProc.ProcessName).Length > 1)
            //{
            //    // If there is more than one, then it is already running.
            //    MessageBox.Show("VideoProcessingEngine is already running.");
            //    Application.Current.Shutdown();
            //    return;

            //}
            notifyIcon = new TaskbarIcon();
            notifyIcon.Icon = VideoProcessingEngine.Properties.Resources.VideoProcessSmallIcon;
            notifyIcon.ToolTipText = "VideoProcessingEngine";
            notifyIcon.Visibility = Visibility.Visible;
            notifyIcon.TrayPopup = new mypopup();
            try
            {
                ChromaKeypluginLic.IntializeProtection();
                DecoderlibLic.IntializeProtection();
                EncoderlibLic.IntializeProtection();
                MComposerlibLic.IntializeProtection();
                MPlatformSDKLic.IntializeProtection();
                string pathtosave = AppDomain.CurrentDomain.BaseDirectory;               
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }
        static void MyHandler(object sender, UnhandledExceptionEventArgs args)
        {
            Exception e = (Exception)args.ExceptionObject;
            MessageBox.Show(e.Message);
        }
        void AppDispatcherUnhandledException(object sender, DispatcherUnhandledExceptionEventArgs e)
        {
            ShowUnhandeledException(e);
        }
        void ShowUnhandeledException(DispatcherUnhandledExceptionEventArgs e)
        {
            e.Handled = true;
            string errorMessage = string.Format("An application error occurred.\nPlease check whether your data is correct and repeat the action. If this error occurs again there seems to be a more serious malfunction in the application, and you better close it.\n\nError:{0}\n\nDo you want to continue?\n(if you click Yes you will continue with your work, if you click No the application will close)",
            e.Exception.Message + (e.Exception.InnerException != null ? "\n" +
            e.Exception.InnerException.Message : null));
            if (MessageBox.Show(errorMessage, "Application Error", MessageBoxButton.YesNoCancel, MessageBoxImage.Error) == MessageBoxResult.No)
            {
                if (MessageBox.Show("WARNING: The application will close. Any changes will not be saved!\nDo you really want to close it?", "Close the application!", MessageBoxButton.YesNoCancel, MessageBoxImage.Warning) == MessageBoxResult.Yes)
                {
                    Application.Current.Shutdown();
                }
            }
        }
    }
}
