﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
//using System.Threading;
using System.IO;
using System.Windows.Forms;
using System.Diagnostics;
using System.Data.SqlClient;
using System.Data;


namespace DigiScreenSaver
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        Timer tmr; int ctr = 0; string retPath = "";
        string sampleImgPath = ""; int displayDur = 10; int ssDelay = 10; bool isSSactive; //Set default values
        Timer SysTmr = new Timer();
        public MainWindow()
        {
            InitializeComponent();
            this.WindowState = System.Windows.WindowState.Maximized;
            this.WindowStyle = System.Windows.WindowStyle.None;
            GetConfigData();
            SysTmr.Interval = 250;
            SysTmr.Tick += new EventHandler(SysTmr_Tick);
            SysTmr.Enabled = true;
            this.Hide();
        }
        private void GetConfigData()
        {
            try
            {
                string _connstr = System.Configuration.ConfigurationManager.ConnectionStrings["DigiConnectionString"].ConnectionString;
                using (SqlConnection con = new SqlConnection(_connstr))
                {
                    using (SqlCommand cmd = new SqlCommand("DG_GetScreenSaverConfigDetails", con))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        //cmd.Parameters.Add("@SubStoreId", SqlDbType.VarChar).Value = 1; //LoginUser.SubStoreId;
                        con.Open();
                        using (SqlDataReader rdr = cmd.ExecuteReader(CommandBehavior.CloseConnection))
                        {
                            if (rdr.HasRows)
                            {
                                rdr.Read();
                                sampleImgPath = rdr["EK_SampleImagePath"].ToString();

                                string Duration = rdr["EK_DisplayDuration"].ToString();
                                if (!string.IsNullOrEmpty(Duration))
                                    displayDur = Convert.ToInt32(Duration);

                                string Delay = rdr["EK_ScreenStartTime"].ToString();
                                if (!string.IsNullOrEmpty(Delay))
                                    ssDelay = Convert.ToInt32(Delay);

                                isSSactive = Convert.ToBoolean(string.IsNullOrEmpty(rdr["EK_IsScreenSaverActive"].ToString()) ? false : rdr["EK_IsScreenSaverActive"]);
                            }
                        }
                    }
                }
                if (displayDur != 0)
                {
                    displayDur = displayDur * 1000; //Convert seconds to milliseconds
                }
                if (ssDelay != 0)
                {
                    ssDelay = ssDelay * 1000; //Convert seconds to milliseconds
                }
            }

            catch { }
        }

        void SysTmr_Tick(object sender, EventArgs e)
        {
            if (isSSactive)
            {
                if (!string.IsNullOrEmpty(sampleImgPath))
                {
                    //double SettingTime = 10000; //string SettingPath = "C:\\media";
                    double DelayTime = IdleTime.AsTimeSpan.TotalMilliseconds;
                    if (DelayTime > ssDelay)
                    {
                        StartSaver();
                        PlayMediaNow(sampleImgPath);
                        setMediaSource(retPath);
                        SysTmr.Stop();
                        this.Show();
                        this.BringIntoView();
                    }
                }
            }
        }
        private void StartSaver()
        {
            tmr = new Timer();
            tmr.Interval = displayDur;
            tmr.Tick += new EventHandler(tmr_Tick);
        }
        void tmr_Tick(object sender, EventArgs e)
        {
            setMediaSource(retPath);

        }
        private void setMediaSource(string mSource)
        {
            PlayMediaNow(sampleImgPath);
            myMediaElement.Source = new Uri(mSource, UriKind.Relative);
            string exten = System.IO.Path.GetExtension(myMediaElement.Source.ToString());
            //if (myMediaElement.HasVideo)
            if (exten != ".JPG")
            {
                if (tmr.Enabled)
                {
                    tmr.Stop();
                }
            }
            myMediaElement.Play();
            //if (!myMediaElement.HasVideo)
            if (exten == ".JPG")
            {
                if (tmr.Enabled == false)
                {
                    tmr.Start();
                }
            }
            //tmr.Dispose();
        }
        private void myMediaElement_MediaEnded(object sender, RoutedEventArgs e)
        {
            if (!tmr.Enabled)
            {
                tmr.Start();
            }
        }
        private void PlayMediaNow(string clrDirectoryPath)
        {
            //string path = "D:\\DigiImages";

            try
            {
                if (Directory.Exists(clrDirectoryPath))
                {
                    System.IO.DirectoryInfo myDirInfo = new DirectoryInfo(clrDirectoryPath);
                    FileInfo[] flr = myDirInfo.GetFiles();
                    //foreach (FileInfo file in myDirInfo.GetFiles())
                    if (ctr == flr.Length)
                    {
                        ctr = 0;
                    }

                    for (int k = ctr; k < flr.Length; k++)
                    {
                        if ((flr[k].Extension.ToUpper() == ".JPG") || (flr[k].Extension.ToUpper() == ".MP4") || (flr[k].Extension.ToUpper() == ".WMV") || (flr[k].Extension.ToUpper() == ".3GP") || (flr[k].Extension.ToUpper() == ".AVI") || (flr[k].Extension.ToUpper() == ".MPG") || (flr[k].Extension.ToUpper() == ".PNG"))
                        {
                            retPath = flr[k].FullName;
                            ctr++;
                            break;
                        }
                    }
                }
            }
            catch
            {

            }
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            //StartSaver();
            //PlayMediaNow("C:\\media");
            //setMediaSource(retPath);
        }

        private void Window_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (tmr.Enabled)
            {
                tmr.Stop();
            }
            tmr.Dispose();
            myMediaElement.Stop();
            myMediaElement.Source = null;
            this.Hide();
            SysTmr.Start();
        }

        private void Window_TouchDown(object sender, TouchEventArgs e)
        {
            if (tmr.Enabled)
            {
                tmr.Stop();
            }
            tmr.Dispose();
            myMediaElement.Stop();
            myMediaElement.Source = null;
            this.Hide();
            SysTmr.Start();
        }
    }
}
