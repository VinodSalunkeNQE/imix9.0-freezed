﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;

namespace DigiDbCleanupService
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main()
        {
            //DbCleanupService dbs = new DbCleanupService();
            //dbs.debug();
            ServiceBase[] ServicesToRun;
            ServicesToRun = new ServiceBase[]
            {
                new DbCleanupService()
            };
            ServiceBase.Run(ServicesToRun);
        }
    }
}
