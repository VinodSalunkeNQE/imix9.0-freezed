﻿using DigiPhoto.IMIX.Business;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using DigiPhoto.Utility.Repository.ValueType;
using System.Configuration;
using System.IO;
using DigiPhoto.DataLayer;

namespace DigiDbCleanupService
{
    public partial class DbCleanupService : ServiceBase
    {
        static List<SubStoreBackupConfigurationInfo> lstBackupCofig = new List<SubStoreBackupConfigurationInfo>();
        static int maxBackupDays = 7;
        static System.Timers.Timer timer;
        static string _connstr = "";//ConfigurationManager.ConnectionStrings["DigiConnectionString"].ConnectionString;
        static string _databasename = "";//ConfigurationManager.AppSettings["DatabaseName"];
        static string _username = "";//ConfigurationManager.AppSettings["UserName"];
        static string _pass = "";//ConfigurationManager.AppSettings["UserPass"];
        static string _server = "";//ConfigurationManager.AppSettings["ServerName"];
        static Hashtable htSubStoreConfigurations = new Hashtable();
        static int _timerMiliSeconds = 15000;

        public DbCleanupService()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            try
            {

                ServicePosInfoBusiness svcPosinfoBusiness = new ServicePosInfoBusiness();
                string ret = svcPosinfoBusiness.ServiceStart(false);

                if (string.IsNullOrEmpty(ret))
                {
                    fillConfigServerVariables();
                    StartScheduler();
                }
                else
                {
                    throw new Exception("Already Started");

                }


            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                this.ExitCode = 13816;
                this.Stop();
            }
        }
        private static void fillConfigServerVariables()
        {
            try
            {
                string appConString = "";
                _connstr = System.Configuration.ConfigurationManager.ConnectionStrings["DigiConnectionString"].ConnectionString;
                appConString = _connstr;
                string[] strVar = appConString.Split(';');
                Hashtable htCon = new Hashtable();
                if (strVar.Length != 0)
                {
                    for (int k = 0; k <= strVar.Length - 1; k++)
                    {
                        string[] tempK = strVar[k].Split('=');
                        if (tempK.Length != 0)
                        {
                            if (!htCon.ContainsKey(tempK[0]))
                            {
                                htCon.Add(tempK[0], tempK[1]);
                            }
                        }
                    }
                }

                _databasename = htCon.ContainsKey("Initial Catalog") ? htCon["Initial Catalog"].ToString() : "";
                _username = htCon.ContainsKey("User ID") ? htCon["User ID"].ToString() : "";
                _pass = htCon.ContainsKey("Password") ? htCon["Password"].ToString() : "";
                _server = htCon.ContainsKey("Data Source") ? htCon["Data Source"].ToString() : "";
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
            }

        }
        /// <summary>
        /// retrive the backup days based on sub storeid
        /// </summary>
        private static void fillActiveSubStores()
        {
            try
            {
                List<int> SubstoreList = GetActiveSubstoreList();
                for (int counter = 0; counter < SubstoreList.Count; counter++)
                {
                    int subStoreId = SubstoreList[counter];
                    SubStoreBackupConfigurationInfo subStoreConfigInfo = GetBackupConfigData(subStoreId);

                    if (subStoreConfigInfo != null)
                    {
                        lstBackupCofig.Add(subStoreConfigInfo);
                        htSubStoreConfigurations.Add(subStoreId, subStoreConfigInfo);
                    }
                }
                if (lstBackupCofig.Count > 0)
                {
                    // Delete all the folder older than max retention period of images in DigiImages
                    maxBackupDays = lstBackupCofig.Max(x => x.FailedOnlineOrderCleanupdays);
                    maxBackupDays = maxBackupDays == 0 ? 7 : maxBackupDays;
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
            }

        }
        /// <summary>
        /// Start service and calling timer_Elapsed 
        /// </summary>
        private void StartScheduler()
        {
            try
            {
                timer = new System.Timers.Timer();
                timer.Interval = _timerMiliSeconds;
                timer.AutoReset = true;
                timer.Elapsed += new System.Timers.ElapsedEventHandler(this.timer_Elapsed);
                timer.Start();
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="subStorId"></param>
        /// <returns></returns>
        public static SubStoreBackupConfigurationInfo GetBackupConfigData(int subStorId)
        {
            //Commented by Anand, not required for every timer tick
            SubStoreBackupConfigurationInfo subStoreBackupConfig = null;
            using (SqlConnection con = new SqlConnection(_connstr))
            {
                using (SqlCommand cmd = new SqlCommand("DG_GetBackupConfigDetails", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("@SubStoreId", SqlDbType.Int).Value = subStorId;
                    con.Open();
                    //cmd.ExecuteNonQuery();

                    using (SqlDataReader rdr = cmd.ExecuteReader(CommandBehavior.CloseConnection))
                    {
                        if (rdr.HasRows)
                        {
                            rdr.Read();
                            subStoreBackupConfig = new SubStoreBackupConfigurationInfo();
                            subStoreBackupConfig.CleanupTables = rdr["DG_CleanupTables"].ToString();
                            subStoreBackupConfig.CleanUpDaysBackUp = Convert.ToInt32(rdr["DG_CleanUpDaysBackUp"]);
                            subStoreBackupConfig.CleanUpDaysOldBackup = Convert.ToInt32(rdr["DG_CleanUpDaysOldBackup"]);
                            subStoreBackupConfig.FailedOnlineOrderCleanupdays = Convert.ToInt32(rdr["DG_FailedOnlineOrderCleanUpDays"]);
                            //***********************Added by Manoj Regarding Archive Cleanup*******************
                            subStoreBackupConfig.ArchiveCleanupDays = (rdr["DG_ArchiveCleanupDays"] as int?) ?? 60; //Convert.ToInt32(rdr["DG_ArchiveCleanupDays"]);
                        }
                    }
                }
            }
            return subStoreBackupConfig;
        }
        public static List<int> GetActiveSubstoreList()
        {
            List<int> SubstoreIds = new List<int>();
            //fillConfigServerVariables();          //Commented by Anand, not required for every timer tick
            using (SqlConnection con = new SqlConnection(_connstr))
            {
                using (SqlCommand cmd = new SqlCommand("DG_GetActiveSubstoreIds", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    con.Open();
                    using (SqlDataReader rdr = cmd.ExecuteReader(CommandBehavior.CloseConnection))
                    {
                        if (rdr.HasRows)
                        {
                            while (rdr.Read())
                            {
                                SubstoreIds.Add(Convert.ToInt32(rdr["SubstoreId"]));
                            }
                        }
                    }
                }
            }
            return SubstoreIds;
        }

        public void debug()
        {
            string message = string.Empty;
            int subStoreId = 0;
            bool isTableCleanup;
            BackupHistoryInfo backupHistory = null;

            try
            {
                ErrorHandler.ErrorHandler.LogFileWrite("------------------------------------timer_Elapsed Clean Database Started-------- " + DateTime.Now.ToString());

                if (htSubStoreConfigurations == null || htSubStoreConfigurations.Count == 0)
                {
                    fillConfigServerVariables();
                    fillActiveSubStores();
                }
                if (!GetDBCleanUpReq())
                {
                    backupHistory = GetBackupDetailsForDBCleanUp();
                    if (backupHistory != null)
                    {
                        if (backupHistory.ScheduleDate < DateTime.Now && Convert.ToInt32(backupHistory.isBackUpDone) == 1 && backupHistory.Status == 1)
                        {
                            subStoreId = backupHistory.SubStoreId;
                            ChangeCleanUpToInProgress(backupHistory.BackupId, Convert.ToInt32(CleanupStatus.INPROGRESS));//---------Will make the status as Inprogress-------------                            
                            SubStoreBackupConfigurationInfo currentSubStoreConfig = GetBackupConfigData(backupHistory.SubStoreId);
                            ErrorHandler.ErrorHandler.LogFileWrite("------------------------------------Clean Database Started-------- " + DateTime.Now.ToString());
                            isTableCleanup = CleanDBTables(currentSubStoreConfig.CleanupTables, currentSubStoreConfig.CleanUpDaysBackUp, currentSubStoreConfig.CleanUpDaysOldBackup, subStoreId, currentSubStoreConfig.FailedOnlineOrderCleanupdays);
                            ErrorHandler.ErrorHandler.LogFileWrite("------------------------------------Clean Database End-------- " + DateTime.Now.ToString());
                            if (isTableCleanup)
                            {
                                message += "- Specified tables have been emptied! \n\r\n\r";

                                //----------------Start Added by Manoj for archive DB cleanup---------------------------------
                                ErrorHandler.ErrorHandler.LogFileWrite("------------------------------------Archive cleanup Database Started-------- " + DateTime.Now.ToString());
                                CleanArchivedDBTables(currentSubStoreConfig.ArchiveCleanupDays);
                                ErrorHandler.ErrorHandler.LogFileWrite("------------------------------------Archive cleanup Database End-------- " + DateTime.Now.ToString());
                                //Below function is used for shrinking the digiPhoto database
                                ErrorHandler.ErrorHandler.LogFileWrite("------------------------------------Shrinking Digiphoto Database Started-------- " + DateTime.Now.ToString());
                                ShrinkDigiPhotoDatabase();
                                ErrorHandler.ErrorHandler.LogFileWrite("------------------------------------Shrinking Digiphoto Database end-------- " + DateTime.Now.ToString());
                                //----------------------------End----------------------------------
                                ErrorHandler.ErrorHandler.LogFileWrite("------------------------------------Shrinking CleanTableBackup Database Started-------- " + DateTime.Now.ToString());
                                ShrinkArchiveDatabase();
                                ErrorHandler.ErrorHandler.LogFileWrite("------------------------------------Shrinking CleanTableBackup Database end-------- " + DateTime.Now.ToString());
                                ChangeCleanUpToInProgress(backupHistory.BackupId, Convert.ToInt32(CleanupStatus.SUCCESS));
                                SaveEmailDetails(subStoreId, "Cleanup is done successfully");
                            }
                            else
                            {
                                ErrorHandler.ErrorHandler.LogFileWrite("------------------------------------specified Tables are not Cleaned-------- " + DateTime.Now.ToString());
                                throw new Exception("specified Tables are not Cleaned");
                            }
                            GC.Collect();
                        }
                    }

                }

            }
            catch (Exception ex)
            {
                if (backupHistory != null)
                    IsBackupCompleted(backupHistory.BackupId, Convert.ToInt32(CleanupStatus.FAILED));
                SaveEmailDetails(subStoreId, "Cleanup is failed");
                ErrorHandler.ErrorHandler.LogFileWrite("------------------------------------timer_Elapsed catch-------- " + DateTime.Now.ToString());
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
            finally
            {

            }
        }
        private void timer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            string message = string.Empty;
            int subStoreId = 0;
            bool isTableCleanup;
            BackupHistoryInfo backupHistory = null;

            try
            {
                ErrorHandler.ErrorHandler.LogFileWrite("------------------------------------timer_Elapsed Clean Database Started-------- " + DateTime.Now.ToString());
                timer.Stop();
                timer.Enabled = false;

                if (htSubStoreConfigurations == null || htSubStoreConfigurations.Count == 0)
                {
                    fillConfigServerVariables();
                    fillActiveSubStores();
                }
                if (!GetDBCleanUpReq())
                {
                    backupHistory = GetBackupDetailsForDBCleanUp();
                    if (backupHistory != null)
                    {
                        if (backupHistory.ScheduleDate < DateTime.Now && Convert.ToInt32(backupHistory.isBackUpDone) == 1 && backupHistory.Status == 1)
                        {
                            subStoreId = backupHistory.SubStoreId;
                            ErrorHandler.ErrorHandler.LogFileWrite("backupHistory.BackupId =  " + backupHistory.BackupId + " : " + DateTime.Now.ToString());
                            ChangeCleanUpToInProgress(backupHistory.BackupId, Convert.ToInt32(CleanupStatus.INPROGRESS));//---------Will make the status as Inprogress-------------                            
                            SubStoreBackupConfigurationInfo currentSubStoreConfig = GetBackupConfigData(backupHistory.SubStoreId);
                            ErrorHandler.ErrorHandler.LogFileWrite("------------------------------------Clean Database Started-------- " + DateTime.Now.ToString());
                            isTableCleanup = CleanDBTables(currentSubStoreConfig.CleanupTables, currentSubStoreConfig.CleanUpDaysBackUp, currentSubStoreConfig.CleanUpDaysOldBackup, subStoreId, currentSubStoreConfig.FailedOnlineOrderCleanupdays);
                            ErrorHandler.ErrorHandler.LogFileWrite("------------------------------------Clean Database End-------- " + DateTime.Now.ToString());
                            if (isTableCleanup)
                            {
                                message += "- Specified tables have been emptied! \n\r\n\r";

                                //----------------Start Added by Manoj for archive DB cleanup---------------------------------
                                ErrorHandler.ErrorHandler.LogFileWrite("------------------------------------Archive cleanup Database Started-------- " + DateTime.Now.ToString());
                                CleanArchivedDBTables(currentSubStoreConfig.ArchiveCleanupDays);
                                ErrorHandler.ErrorHandler.LogFileWrite("------------------------------------Archive cleanup Database End-------- " + DateTime.Now.ToString());
                                //Below function is used for shrinking the digiPhoto database
                                ErrorHandler.ErrorHandler.LogFileWrite("------------------------------------Shrinking DigiPhoto Database Started-------- " + DateTime.Now.ToString());
                                ShrinkDigiPhotoDatabase();
                                ErrorHandler.ErrorHandler.LogFileWrite("------------------------------------Shrinking DigiPhoto Database end-------- " + DateTime.Now.ToString());
                                //----------------------------End----------------------------------
                                ErrorHandler.ErrorHandler.LogFileWrite("------------------------------------Shrinking Archive Database Started-------- " + DateTime.Now.ToString());
                                ShrinkArchiveDatabase();
                                ErrorHandler.ErrorHandler.LogFileWrite("------------------------------------Shrinking Archive Database end-------- " + DateTime.Now.ToString());

                                ChangeCleanUpToInProgress(backupHistory.BackupId, Convert.ToInt32(CleanupStatus.SUCCESS));
                                SaveEmailDetails(subStoreId, "Cleanup is done successfully");
                            }
                            else
                            {
                                ErrorHandler.ErrorHandler.LogFileWrite("------------------------------------specified Tables are not Cleaned-------- " + DateTime.Now.ToString());
                                throw new Exception("specified Tables are not Cleaned");
                            }
                            GC.Collect();
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                if (backupHistory != null)
                    IsBackupCompleted(backupHistory.BackupId, Convert.ToInt32(CleanupStatus.FAILED));
                SaveEmailDetails(subStoreId, "Cleanup is failed");
                ErrorHandler.ErrorHandler.LogFileWrite("------------------------------------timer_Elapsed catch-------- " + DateTime.Now.ToString());
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
            finally
            {
                timer.Start();
            }
        }
        private bool SaveEmailDetails(int subStoreID, string exceptionMsg)
        {
            ConfigBusiness buss = new ConfigBusiness();
            Dictionary<long, string> iMIXConfigurations = new Dictionary<long, string>();
            iMIXConfigurations = buss.GetConfigurations(iMIXConfigurations, subStoreID);
            string ToAddress = iMIXConfigurations.Where(x => x.Key == (int)ConfigParams.BackUpEmailAddress).FirstOrDefault().Value;
            if (ToAddress != null)
            {
                bool success = buss.SaveEmailDetails(ToAddress, string.Empty, string.Empty, exceptionMsg, "BACKUP FAILED", subStoreID);
            }
            return true;
        }
        /// <summary>
        /// This function will be executed everyday and shrink the digiphoto database after backup and cleanup added by Manoj
        /// </summary>
        private void ShrinkDigiPhotoDatabase()
        {
            try
            {
                int _sqlCommandTimeOutInSec;
                string _connstr = System.Configuration.ConfigurationManager.ConnectionStrings["DigiConnectionString"].ConnectionString;
                if (ConfigurationManager.AppSettings["SQLCommandTimeOut"] != null)
                    _sqlCommandTimeOutInSec = Convert.ToInt32(ConfigurationManager.AppSettings["SQLCommandTimeOut"]);
                else
                    _sqlCommandTimeOutInSec = 300;
                using (SqlConnection con = new SqlConnection(_connstr))
                {
                    using (SqlCommand cmd = new SqlCommand("usp_dg_ShrinkDB", con))
                    {
                        cmd.CommandTimeout = _sqlCommandTimeOutInSec;
                        cmd.CommandType = CommandType.StoredProcedure;
                        con.Open();
                        cmd.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
            }
        }
        /// <summary>
        /// This function will be executed everyday and shrink the CleanTableBackup database after backup and cleanup added by Manoj
        /// </summary>
        private void ShrinkArchiveDatabase()
        {
            try
            {
                int _sqlCommandTimeOutInSec;
                string _connstr = System.Configuration.ConfigurationManager.ConnectionStrings["DigiArchiveConnectionString"].ConnectionString;
                if (ConfigurationManager.AppSettings["SQLCommandTimeOut"] != null)
                    _sqlCommandTimeOutInSec = Convert.ToInt32(ConfigurationManager.AppSettings["SQLCommandTimeOut"]);
                else
                    _sqlCommandTimeOutInSec = 300;
                using (SqlConnection con = new SqlConnection(_connstr))
                {
                    using (SqlCommand cmd = new SqlCommand("usp_dg_ShrinkArchiveDB", con))
                    {
                        cmd.CommandTimeout = _sqlCommandTimeOutInSec;
                        cmd.CommandType = CommandType.StoredProcedure;
                        con.Open();
                        cmd.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
            }
        }
        //------------------Clean up Database ----------Nilesh------------------------------
        /// <summary>
        /// It will do cleanup and archive activity from digiphoto database based on below parameters.
        /// </summary>
        /// <param name="heavyTables"></param>
        /// <param name="_cleanupdaysBackup"></param>
        /// <param name="oldCleanUpDays"></param>
        /// <param name="subStoreId"></param>
        /// <param name="_FailedOnlineOrderCleanupdays"></param>
        /// <returns></returns>
        public static bool CleanDBTables(string heavyTables, int _cleanupdaysBackup, int oldCleanUpDays, int subStoreId, int _FailedOnlineOrderCleanupdays)
        {
            bool IsTableEmpty;
            try
            {
                int _sqlCommandTimeOutInSec;
                string _connstr = System.Configuration.ConfigurationManager.ConnectionStrings["DigiConnectionString"].ConnectionString;
                if (ConfigurationManager.AppSettings["SQLCommandTimeOut"] != null)
                    _sqlCommandTimeOutInSec = Convert.ToInt32(ConfigurationManager.AppSettings["SQLCommandTimeOut"]);
                else
                    _sqlCommandTimeOutInSec = 300;
                if (!String.IsNullOrEmpty(heavyTables))
                {
                    using (SqlConnection con = new SqlConnection(_connstr))
                    {
                        // using (SqlCommand cmd = new SqlCommand("DG_TableCleanup", con))
                        using (SqlCommand cmd = new SqlCommand("DG_TableCleanup_Backup_New", con))
                        {
                            cmd.CommandTimeout = _sqlCommandTimeOutInSec;
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.Add("@tables", SqlDbType.VarChar).Value = heavyTables;
                            cmd.Parameters.Add("@BackUpDays", SqlDbType.Int).Value = _cleanupdaysBackup;
                            cmd.Parameters.Add("@OldCleanUpDays", SqlDbType.Int).Value = oldCleanUpDays;
                            cmd.Parameters.Add("@SubStoreId", SqlDbType.Int).Value = subStoreId;
                            cmd.Parameters.Add("@FaliledOnlineOrderCleanupdays", SqlDbType.Int).Value = _FailedOnlineOrderCleanupdays;
                            con.Open();
                            cmd.ExecuteNonQuery(); //uncomment
                        }
                    }
                }
                IsTableEmpty = true;
            }
            catch (Exception ex)
            {
                IsTableEmpty = false;
                ErrorHandler.ErrorHandler.LogFileWrite("--------------------CleanDBTables catch-------- " + DateTime.Now.ToString());
                throw;
            }
            return IsTableEmpty;

        }
        //------------------Clean up Database ------17-05-2018----Manoj------------------------------
        /// <summary>
        /// It will do cleanup activity from archive database.
        /// </summary>
        /// <param name="_ArchiveCleanupDays"></param>
        /// <returns></returns>
        public static bool CleanArchivedDBTables(int _ArchiveCleanupDays)
        {
            bool IsTableEmpty = false;
            try
            {
                int _sqlCommandTimeOutInSec;
                string _connstr = System.Configuration.ConfigurationManager.ConnectionStrings["DigiArchiveConnectionString"].ConnectionString;
                if (ConfigurationManager.AppSettings["SQLCommandTimeOut"] != null)
                    _sqlCommandTimeOutInSec = Convert.ToInt32(ConfigurationManager.AppSettings["SQLCommandTimeOut"]);
                else
                    _sqlCommandTimeOutInSec = 300;
                using (SqlConnection con = new SqlConnection(_connstr))
                {
                    // using (SqlCommand cmd = new SqlCommand("DG_TableCleanup", con))
                    using (SqlCommand cmd = new SqlCommand("usp_dg_ArchiveTableCleanup", con))
                    {
                        cmd.CommandTimeout = _sqlCommandTimeOutInSec;
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add("@ArchiveCleanDays", SqlDbType.Int).Value = _ArchiveCleanupDays;
                        con.Open();
                        cmd.ExecuteNonQuery(); //uncomment
                    }

                }
                IsTableEmpty = true;
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogFileWrite("--------CleanArchivedPhotosDBTables---Catch----------" + ex.StackTrace);
                //  throw;
            }
            return IsTableEmpty;

        }
        //------------------Clean up Database ----------Manoj------------------------------

        //------------------Clean up Database ----------Nilesh------------------------------
        public static BackupHistoryInfo GetBackupDetailsForDBCleanUp()
        {
            BackupHistoryInfo backupHistory = null;
            try
            {
                using (SqlConnection con = new SqlConnection(_connstr))
                {
                    using (SqlCommand cmd = new SqlCommand("usp_dg_GetBackup", con))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;

                        //cmd.Parameters.Add("@SubStoreId", SqlDbType.Int).Value = subStoreId;
                        con.Open();
                        SqlDataReader rdr = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                        //using (SqlDataReader rdr = cmd.ExecuteReader())
                        //{
                        if (rdr.HasRows)
                        {
                            rdr.Read();
                            backupHistory = new BackupHistoryInfo();
                            backupHistory.BackupId = rdr["BackupId"].ToInt32();
                            backupHistory.ScheduleDate = rdr["ScheduleDate"].ToDateTime();
                            backupHistory.SubStoreId = rdr["SubStoreId"].ToInt32();
                            backupHistory.isBackUpDone = Convert.ToInt32(rdr["isBackupDone"]);
                            backupHistory.Status = rdr["Status"].ToInt32();
                        }
                        //}
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }

            return backupHistory;
        }
        public static bool GetDBCleanUpReq()
        {
            bool isDBCleanUpReq = false;
            using (SqlConnection con = new SqlConnection(_connstr))
            {
                using (SqlCommand cmd = new SqlCommand("usp_dg_GetIsDBCleanUp", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;

                    con.Open();
                    SqlDataReader rdr = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                    //using (SqlDataReader rdr = cmd.ExecuteReader())
                    //{
                    if (rdr.HasRows)
                    {
                        rdr.Read();

                        isDBCleanUpReq = Convert.ToBoolean(rdr["isDBCleanUp"]);

                    }
                    //}
                }
            }
            return isDBCleanUpReq;
        }
        /// <summary>
        /// Once backup will be completed then status updated in backup history table isBackupDone as 3 (successful)
        /// </summary>
        /// <param name="backupId"></param>
        /// <param name="status"></param>
        /// <returns></returns>
        public static bool IsBackupCompleted(int backupId, int status)
        {
            bool isBackHistoryUpdated;
            try
            {
                string _connstr = System.Configuration.ConfigurationManager.ConnectionStrings["DigiConnectionString"].ConnectionString;

                using (SqlConnection con = new SqlConnection(_connstr))
                {
                    using (SqlCommand cmd = new SqlCommand("usp_dg_ScheduledCleanupDone", con))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add("@BackupId", SqlDbType.Int).Value = backupId;
                        cmd.Parameters.Add("@isBackupDone", SqlDbType.Int).Value = CleanupStatus.SUCCESS;
                        con.Open();
                        cmd.ExecuteNonQuery(); //uncomment
                    }
                }

                isBackHistoryUpdated = true;
                return isBackHistoryUpdated;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        /// <summary>
        /// Update status in backup history table
        /// </summary>
        /// <param name="backupId"></param>
        /// <param name="status"></param>
        /// <returns></returns>
        public static bool ChangeCleanUpToInProgress(int backupId, int status)
        {
            bool isUpdatedDone;
            try
            {
                string _connstr = System.Configuration.ConfigurationManager.ConnectionStrings["DigiConnectionString"].ConnectionString;

                using (SqlConnection con = new SqlConnection(_connstr))
                {
                    using (SqlCommand cmd = new SqlCommand("usp_dg_CleanUpInProgressUpdate", con))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add("@BackupId", SqlDbType.Int).Value = backupId;
                        cmd.Parameters.Add("@Status", SqlDbType.Int).Value = status;
                        con.Open();
                        cmd.ExecuteNonQuery(); //uncomment
                    }
                }

                isUpdatedDone = true;
                return isUpdatedDone;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected override void OnStop()
        {
            timer.Stop();
            timer = null;
            ServicePosInfoBusiness svcPosinfoBusiness = new ServicePosInfoBusiness();
            svcPosinfoBusiness.StopService(false);
        }
    }

    public class BackupHistoryInfo
    {
        public int BackupId { get; set; }
        public DateTime ScheduleDate { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public int Status { get; set; }
        public string ErrorMessage { get; set; }
        public int SubStoreId { get; set; }
        public int isBackUpDone { get; set; }

    }


    public class SubStoreBackupConfigurationInfo
    {
        public string DbBackupPath { get; set; }
        public string HotFolderPath { get; set; }
        public string HfBackupPath { get; set; }
        public string CleanupTables { get; set; }
        public int CleanUpDaysBackUp { get; set; }
        public int CleanUpDaysOldBackup { get; set; }
        public int FailedOnlineOrderCleanupdays { get; set; }
        public int ArchiveCleanupDays { get; set; }
    }
    public enum CleanupStatus
    {
        NA = 0,
        SCHEDULED = 1,
        FAILED = 2,
        SUCCESS = 3,
        INPROGRESS = 4,

    }
}