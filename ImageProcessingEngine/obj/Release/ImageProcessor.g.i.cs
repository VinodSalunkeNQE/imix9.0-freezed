﻿#pragma checksum "..\..\ImageProcessor.xaml" "{8829d00f-11b8-4213-878b-770e8597ac16}" "B97459D3A3BCDF0ADC29168D1E67F3215ABB9DF4C34E7723DC031ACF08266D42"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using DigiPhoto.Common;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace ImageProcessingEngine {
    
    
    /// <summary>
    /// ImageProcessor
    /// </summary>
    public partial class ImageProcessor : System.Windows.Window, System.Windows.Markup.IComponentConnector {
        
        
        #line 103 "..\..\ImageProcessor.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid IMGFrame;
        
        #line default
        #line hidden
        
        
        #line 104 "..\..\ImageProcessor.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid img;
        
        #line default
        #line hidden
        
        
        #line 110 "..\..\ImageProcessor.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid CanBgParent;
        
        #line default
        #line hidden
        
        
        #line 116 "..\..\ImageProcessor.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid GrdSize;
        
        #line default
        #line hidden
        
        
        #line 117 "..\..\ImageProcessor.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Image widthimg;
        
        #line default
        #line hidden
        
        
        #line 120 "..\..\ImageProcessor.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid grdZoomCanvas;
        
        #line default
        #line hidden
        
        
        #line 121 "..\..\ImageProcessor.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid forWdht;
        
        #line default
        #line hidden
        
        
        #line 122 "..\..\ImageProcessor.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid canbackground;
        
        #line default
        #line hidden
        
        
        #line 123 "..\..\ImageProcessor.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal DigiPhoto.Common.DragCanvas dragCanvas;
        
        #line default
        #line hidden
        
        
        #line 124 "..\..\ImageProcessor.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid GrdBrightness;
        
        #line default
        #line hidden
        
        
        #line 126 "..\..\ImageProcessor.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid GrdHueShift;
        
        #line default
        #line hidden
        
        
        #line 127 "..\..\ImageProcessor.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid GrdGreyScale;
        
        #line default
        #line hidden
        
        
        #line 128 "..\..\ImageProcessor.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid GrdInvert;
        
        #line default
        #line hidden
        
        
        #line 129 "..\..\ImageProcessor.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid GrdSharpen;
        
        #line default
        #line hidden
        
        
        #line 130 "..\..\ImageProcessor.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid GrdSketchGranite;
        
        #line default
        #line hidden
        
        
        #line 131 "..\..\ImageProcessor.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid GrdEmboss;
        
        #line default
        #line hidden
        
        
        #line 132 "..\..\ImageProcessor.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid Grdcolorfilter;
        
        #line default
        #line hidden
        
        
        #line 133 "..\..\ImageProcessor.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid GrdSepia;
        
        #line default
        #line hidden
        
        
        #line 134 "..\..\ImageProcessor.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid GrdUnderWater;
        
        #line default
        #line hidden
        
        
        #line 135 "..\..\ImageProcessor.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid Grdcartoonize;
        
        #line default
        #line hidden
        
        
        #line 136 "..\..\ImageProcessor.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid GrdRedEyeFirst;
        
        #line default
        #line hidden
        
        
        #line 137 "..\..\ImageProcessor.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid GrdRedEyeSecond;
        
        #line default
        #line hidden
        
        
        #line 138 "..\..\ImageProcessor.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid GrdRedEyeMultiple;
        
        #line default
        #line hidden
        
        
        #line 139 "..\..\ImageProcessor.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid GrdRedEyeMultiple1;
        
        #line default
        #line hidden
        
        
        #line 140 "..\..\ImageProcessor.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid GrdGreenScreenDefault3;
        
        #line default
        #line hidden
        
        
        #line 142 "..\..\ImageProcessor.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.InkCanvas MyInkCanvas;
        
        #line default
        #line hidden
        
        
        #line 144 "..\..\ImageProcessor.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Ink.DrawingAttributes attribute;
        
        #line default
        #line hidden
        
        
        #line 146 "..\..\ImageProcessor.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Image mainImage;
        
        #line default
        #line hidden
        
        
        #line 148 "..\..\ImageProcessor.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Shapes.Rectangle squre;
        
        #line default
        #line hidden
        
        
        #line 165 "..\..\ImageProcessor.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Shapes.Ellipse inkCanvasEllipse;
        
        #line default
        #line hidden
        
        
        #line 167 "..\..\ImageProcessor.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Shapes.Rectangle inkCanvasRectangle;
        
        #line default
        #line hidden
        
        
        #line 169 "..\..\ImageProcessor.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid frm;
        
        #line default
        #line hidden
        
        
        #line 170 "..\..\ImageProcessor.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid grdGumball;
        
        #line default
        #line hidden
        
        
        #line 171 "..\..\ImageProcessor.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.StackPanel spGumball;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/ImageProcessingEngine;component/imageprocessor.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\ImageProcessor.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            
            #line 8 "..\..\ImageProcessor.xaml"
            ((ImageProcessingEngine.ImageProcessor)(target)).Loaded += new System.Windows.RoutedEventHandler(this.Window_Loaded);
            
            #line default
            #line hidden
            return;
            case 2:
            this.IMGFrame = ((System.Windows.Controls.Grid)(target));
            return;
            case 3:
            this.img = ((System.Windows.Controls.Grid)(target));
            return;
            case 4:
            this.CanBgParent = ((System.Windows.Controls.Grid)(target));
            return;
            case 5:
            this.GrdSize = ((System.Windows.Controls.Grid)(target));
            return;
            case 6:
            this.widthimg = ((System.Windows.Controls.Image)(target));
            return;
            case 7:
            this.grdZoomCanvas = ((System.Windows.Controls.Grid)(target));
            return;
            case 8:
            this.forWdht = ((System.Windows.Controls.Grid)(target));
            return;
            case 9:
            this.canbackground = ((System.Windows.Controls.Grid)(target));
            return;
            case 10:
            this.dragCanvas = ((DigiPhoto.Common.DragCanvas)(target));
            return;
            case 11:
            this.GrdBrightness = ((System.Windows.Controls.Grid)(target));
            return;
            case 12:
            this.GrdHueShift = ((System.Windows.Controls.Grid)(target));
            return;
            case 13:
            this.GrdGreyScale = ((System.Windows.Controls.Grid)(target));
            return;
            case 14:
            this.GrdInvert = ((System.Windows.Controls.Grid)(target));
            return;
            case 15:
            this.GrdSharpen = ((System.Windows.Controls.Grid)(target));
            return;
            case 16:
            this.GrdSketchGranite = ((System.Windows.Controls.Grid)(target));
            return;
            case 17:
            this.GrdEmboss = ((System.Windows.Controls.Grid)(target));
            return;
            case 18:
            this.Grdcolorfilter = ((System.Windows.Controls.Grid)(target));
            return;
            case 19:
            this.GrdSepia = ((System.Windows.Controls.Grid)(target));
            return;
            case 20:
            this.GrdUnderWater = ((System.Windows.Controls.Grid)(target));
            return;
            case 21:
            this.Grdcartoonize = ((System.Windows.Controls.Grid)(target));
            return;
            case 22:
            this.GrdRedEyeFirst = ((System.Windows.Controls.Grid)(target));
            return;
            case 23:
            this.GrdRedEyeSecond = ((System.Windows.Controls.Grid)(target));
            return;
            case 24:
            this.GrdRedEyeMultiple = ((System.Windows.Controls.Grid)(target));
            return;
            case 25:
            this.GrdRedEyeMultiple1 = ((System.Windows.Controls.Grid)(target));
            return;
            case 26:
            this.GrdGreenScreenDefault3 = ((System.Windows.Controls.Grid)(target));
            return;
            case 27:
            this.MyInkCanvas = ((System.Windows.Controls.InkCanvas)(target));
            return;
            case 28:
            this.attribute = ((System.Windows.Ink.DrawingAttributes)(target));
            return;
            case 29:
            this.mainImage = ((System.Windows.Controls.Image)(target));
            return;
            case 30:
            this.squre = ((System.Windows.Shapes.Rectangle)(target));
            return;
            case 31:
            this.inkCanvasEllipse = ((System.Windows.Shapes.Ellipse)(target));
            return;
            case 32:
            this.inkCanvasRectangle = ((System.Windows.Shapes.Rectangle)(target));
            return;
            case 33:
            this.frm = ((System.Windows.Controls.Grid)(target));
            return;
            case 34:
            this.grdGumball = ((System.Windows.Controls.Grid)(target));
            return;
            case 35:
            this.spGumball = ((System.Windows.Controls.StackPanel)(target));
            return;
            }
            this._contentLoaded = true;
        }
    }
}

