﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using DigiPhoto.IMIX.Model;
using DigiPhoto.IMIX.Business;
using ImageProcessingEngine.Shader;
using System.IO;
using DigiPhoto.Common;
using System.Xml;
using DigiPhoto.Utility.Repository.ValueType;
using ImageProcessingEngine.Model;
using DigiPhoto.DataLayer;
using ImageProcessingEngine.Shader;
using XnaFan.ImageComparison;

namespace ImageProcessingEngine
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class ImageProcessor : Window
    {
        #region Declaration
        public DispatcherTimer EditTimer;
        public FolderStructureInfo FolderInfo;
        public string OriginalImagePath = string.Empty;
        public string DirectoryNameWithDate = string.Empty;
        string _thumbnailFilePathDate = string.Empty;
        string _bigThumbnailFilePathDate = string.Empty;
        public List<SemiOrderSettingsInfo> lstSemiOrderSettingsInfo = null;
        public SemiOrderSettingsInfo semiOrderSettingsInfo = null;
        int productId = 0;
        public TransformGroup transformGroup = null;
        public RotateTransform rotateTransform = null;
        public ScaleTransform zoomTransform = null;
        string _centerX = "";
        string _centerY = "";
        double _ZoomFactor = 0;
        bool crop = false;
        bool first = true;
        double sizeWidth = 0;
        double sizeHeight = 0;
        // bool IsLocationWise = false;
        ContrastAdjustEffect _brightConteff = new ContrastAdjustEffect();
        public static int SubStoreId = 0;
        string SubStoreName = string.Empty;
        string _previewWallSubStore = string.Empty;
        List<iMixConfigurationLocationInfo> _lstLocationWiseConfigParams = null;
        List<ImgCopyLocation> imgCopyLocationlst = new List<ImgCopyLocation>();
        private List<iMixConfigurationLocationInfo> _lstPreviewWallLocation;
        private string screen = string.Empty;
        private string _copyFolderName;
        bool gumEditImage = false;
        double sharpen = 0;
        double currentsharpen = 0;
        double hueshift = 0;
        double currenthueshift = 0;
        int graphicsTextBoxCount = 0;
        ShEffect _sharpeff = new ShEffect();
        MonochromeEffect _colorfiltereff = new MonochromeEffect();
        ContrastAdjustEffect _brighteff = new ContrastAdjustEffect();
        MultiChannelContrastEffect _under = new MultiChannelContrastEffect();
        ShiftHueEffect _shifthueeff = new ShiftHueEffect();
        bool IsEnablePartialEditedImage = false;
        List<iMixConfigurationLocationInfo> _lstiMixConfigurationLocationInfo = new List<iMixConfigurationLocationInfo>();
        #endregion

        #region Constructor
        public ImageProcessor()
        {
            InitializeComponent();
            mainImage.SnapsToDevicePixels = true;
            mainImage.SetValue(RenderOptions.EdgeModeProperty, EdgeMode.Aliased);
            lstSemiOrderSettingsInfo = new List<SemiOrderSettingsInfo>();
            SemiOrderBusiness semiOrderBusiness = new SemiOrderBusiness();
            lstSemiOrderSettingsInfo = semiOrderBusiness.GetSemiOrderSettings();
            // IsLocationWise = System.Configuration.ConfigurationManager.AppSettings["RunLocationWise"] != null ? Convert.ToBoolean(System.Configuration.ConfigurationManager.AppSettings["RunLocationWise"]) : true;
            // ErrorHandler.ErrorHandler.LogFileWrite("Location is: " + IsLocationWise.ToString() + ", config:" + System.Configuration.ConfigurationManager.AppSettings["RunLocationWise"].ToString());
            EditTimer = new DispatcherTimer();
            EditTimer.Interval = new TimeSpan(0, 0, 0, 0, 500);
            EditTimer.Tick += new EventHandler(EditTimer_Tick);
        }
        #endregion

        #region Events
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                this.Left = 0;
                this.Top = 0;
                ConfigBusiness configBusiness = new ConfigBusiness();
                SubStoreId = ReadSubStore();
                FolderInfo = configBusiness.GetFolderStructureInfo(SubStoreId);
                SubStoreName = (new StoreSubStoreDataBusniess()).GetSubstoreNameById(SubStoreId);

                _lstLocationWiseConfigParams = new List<iMixConfigurationLocationInfo>();
                _lstLocationWiseConfigParams = (new ConfigBusiness()).GetLocationWiseConfigParams(SubStoreId);
                _lstPreviewWallLocation = _lstLocationWiseConfigParams.Where(s => s.IMIXConfigurationMasterId == Convert.ToDouble(ConfigParams.NoOfScreenPW)).ToList();
                if (_lstPreviewWallLocation != null && _lstPreviewWallLocation.Count > 0)
                {
                    ImgCopyLocation imgCopyLocation;
                    foreach (iMixConfigurationLocationInfo PreviewWallLocation in _lstPreviewWallLocation)
                    {
                        imgCopyLocation = new ImgCopyLocation();
                        imgCopyLocation.locID = PreviewWallLocation.LocationId;
                        imgCopyLocation.counter = 1;
                        imgCopyLocation.range = Convert.ToInt32(PreviewWallLocation.ConfigurationValue);
                        imgCopyLocationlst.Add(imgCopyLocation);
                    }
                }
                _lstiMixConfigurationLocationInfo = (configBusiness.GetAllLocationWiseConfigParams())
                    .Where(x => x.IMIXConfigurationMasterId == Convert.ToInt64(ConfigParams.IsEnablePartialEditedImage)
                    && x.ConfigurationValue.ToLower() == "true").ToList();
                _previewWallSubStore = System.IO.Path.Combine(FolderInfo.HotFolderPath, "PreviewWall", SubStoreName);
                //GetConfigurationInfo();
                EditTimer.Start();
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
            }
        }

        private void EditTimer_Tick(object sender, EventArgs e)
        {
            try
            {
                EditTimer.Stop();
                EditImages();
                EditTimer.Start();
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
            }
        }
        private void EditImages()
        {
            try
            {
                PhotoBusiness photoBusiness = new PhotoBusiness();
                List<PhotoDetail> lstPhotoDetails = photoBusiness.GetImagesInfoForEditing(SubStoreId, Environment.MachineName);
                foreach (PhotoDetail photoDetail in lstPhotoDetails)
                {
#if DEBUG
                    var watch = FrameworkHelper.CommonUtility.Watch();
                    watch.Start();
#endif

                    bool isEffectsApplied = ApplyEffects(photoDetail);
#if DEBUG
                    if (watch != null)
                        FrameworkHelper.CommonUtility.WatchStop("Image Processing EditImages :ApplyEffects", watch);
#endif
                }


                //modify the wrongly created images 
                DirectoryInfo exeFolder, projectFolder, sampleImagesFolder, duplicateImagesFolder;
                string Photoids = string.Empty;

                string[] imagesCount = Directory.GetFiles(@"\\" + FolderInfo.HotFolderPath + "\\Thumbnails\\Temp");
                for (int i = imagesCount.Length - 1; i > 0; i--)
                {
                    string file1 = System.IO.Path.GetFileName(imagesCount[i]);
                    if (file1.Contains("tmp"))
                    {
                        if (file1.Contains("_1"))
                        {
                            string file2 = file1.Replace("tmp", "");
                            Compare(file1, file2);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
            }
        }
        #endregion

        #region Apply Different Effects
        private bool ApplyEffects(PhotoDetail photoDetail)
        {
            bool isEffectApplied = false;
            try
            {
                DirectoryNameWithDate = FolderInfo.HotFolderPath + photoDetail.DG_Photos_CreatedOn.ToString("yyyyMMdd");
                _thumbnailFilePathDate = System.IO.Path.Combine(FolderInfo.HotFolderPath, "Thumbnails", photoDetail.DG_Photos_CreatedOn.ToString("yyyyMMdd"));
                _bigThumbnailFilePathDate = System.IO.Path.Combine(FolderInfo.HotFolderPath, "Thumbnails_Big", photoDetail.DG_Photos_CreatedOn.ToString("yyyyMMdd"));
                if (!Directory.Exists(_thumbnailFilePathDate))
                    Directory.CreateDirectory(_thumbnailFilePathDate);
                if (!Directory.Exists(_bigThumbnailFilePathDate))
                    Directory.CreateDirectory(_bigThumbnailFilePathDate);
                OriginalImagePath = System.IO.Path.Combine(DirectoryNameWithDate, photoDetail.FileName);
                if (!File.Exists(OriginalImagePath))
                {
                    // Set IsImageProcessed =2 Error state if OriginalImagePath does not exists
                    UpdateImageProcessedStatus(photoDetail.PhotoId, 2);
                    return false;
                }
                //semiOrderSettingsInfo = lstSemiOrderSettingsInfo.Where(x => x.LocationId == photoDetail.LocationId && x.SubstoreId == photoDetail.SubstoreId).FirstOrDefault();
                semiOrderSettingsInfo = lstSemiOrderSettingsInfo.Where(x => x.LocationId == photoDetail.LocationId && x.Id == photoDetail.SemiOrderProfileId).FirstOrDefault();
                if (semiOrderSettingsInfo == null || semiOrderSettingsInfo.ProductTypeId.Contains(','))
                    productId = 1;
                else
                    productId = Convert.ToInt32(semiOrderSettingsInfo.ProductTypeId);
                Clear();
                grdZoomCanvas.UpdateLayout();
                GrdSize.UpdateLayout();
                if (photoDetail.IsCropped == true && File.Exists(System.IO.Path.Combine(FolderInfo.CroppedPath, photoDetail.FileName)))
                    crop = true;
                else
                    ApplyCrop(photoDetail);
                ApplyChromaEffect(photoDetail);
                bool isProcessed = ImageLoader(photoDetail);

                if (isProcessed == false)
                {
                    // Set IsImageProcessed =2 Error state
                    UpdateImageProcessedStatus(photoDetail.PhotoId, 2);
                    return false;
                }
                if (photoDetail.IsGumRideShow == true)
                {
                    grdGumball.Visibility = Visibility.Visible;
                    gumEditImage = false;
                    ShowhidePlayerScore(Visibility.Visible);
                }
                else
                {
                    grdGumball.Visibility = Visibility.Hidden;
                    ShowhidePlayerScore(Visibility.Hidden);
                }

                this.InvalidateVisual();

                RenderTargetBitmap _objeditedImage = jCaptureScreen();
                int imageQuality;
                var memoryStream = new MemoryStream();

                if (photoDetail.IsGreen == true)
                {
                    imageQuality = 80;
                }
                else
                {
                    imageQuality = 98;
                }
                if (!Directory.Exists(FolderInfo.ThumbnailsPath + "Temp\\"))
                    Directory.CreateDirectory(FolderInfo.ThumbnailsPath + "Temp\\");
                using (var fileStream = new FileStream(FolderInfo.ThumbnailsPath + "Temp\\" + "tmp" + photoDetail.FileName, FileMode.Create, FileAccess.ReadWrite))
                {
                    JpegBitmapEncoder encoder = new JpegBitmapEncoder();
                    encoder.QualityLevel = imageQuality;
                    encoder.Frames.Add(BitmapFrame.Create(_objeditedImage));
                    encoder.Save(fileStream);
                    //_objeditedImage.Clear();
                    fileStream.Position = 0;
                    fileStream.CopyTo(memoryStream);
                }
                #region Ajay check if the product is panorama then create the panoram folder inside the edited imgage
                PanoramaBusiness _objPanoramaBusiness = new PanoramaBusiness();
                bool IsProductPanoramic = _objPanoramaBusiness.IsProductPanorama_ProductId(Convert.ToInt16(productId));
                string editedImagePath = string.Empty;
                string FinalImagePath = System.IO.Path.Combine(FolderInfo.ThumbnailsPath, "Temp", ("tmp" + photoDetail.FileName));
                if (IsProductPanoramic)
                {
                    if (!Directory.Exists(System.IO.Path.Combine(FolderInfo.EditedImagePath, "Panorama")))
                        Directory.CreateDirectory(System.IO.Path.Combine(FolderInfo.EditedImagePath, "Panorama"));

                    editedImagePath = System.IO.Path.Combine(FolderInfo.EditedImagePath + "\\Panorama", photoDetail.FileName);
                    File.Copy(FinalImagePath, editedImagePath, true);
                    #region Updated to copy image in both edit images if comes via spec and not edited.
                    File.Copy(FinalImagePath, System.IO.Path.Combine(FolderInfo.EditedImagePath, photoDetail.FileName), true);
                    #endregion
                }
                else
                {
                    editedImagePath = System.IO.Path.Combine(FolderInfo.EditedImagePath, photoDetail.FileName);
                    File.Copy(FinalImagePath, editedImagePath, true);
                    #endregion
                }
                //string FinalImagePath = System.IO.Path.Combine(FolderInfo.ThumbnailsPath, "Temp", ("tmp" + photoDetail.FileName));
                //File.Copy(FinalImagePath, editedImagePath, true);
                var lstconfiglst = _lstLocationWiseConfigParams.Where(s => s.LocationId == photoDetail.LocationId && s.IMIXConfigurationMasterId == Convert.ToInt64(ConfigParams.IsPreviewEnabledPW)
                                          && s.ConfigurationValue.ToUpper() == "TRUE").ToList();
                if (lstconfiglst != null && lstconfiglst.Count > 0)
                {
                    var lstconfig = _lstLocationWiseConfigParams.Where(s => s.LocationId == photoDetail.LocationId && s.IMIXConfigurationMasterId == Convert.ToInt64(ConfigParams.IsSpecImgPW) && s.ConfigurationValue.ToUpper() == "TRUE")
                        .ToList();
                    if (lstconfig != null && lstconfig.Count > 0)
                        CopyImgToDisplayFolder(FinalImagePath, photoDetail.PhotoRFID, photoDetail.PhotoId, photoDetail.LocationId);
                }
                forWdht.UpdateLayout();
                List<ResizeInfo> objResizeInfo = new List<ResizeInfo>();
                objResizeInfo.Add(new ResizeInfo { Size = 210, FileName = System.IO.Path.Combine(_thumbnailFilePathDate, photoDetail.FileName) });
                objResizeInfo.Add(new ResizeInfo { Size = 1200, FileName = System.IO.Path.Combine(_bigThumbnailFilePathDate, photoDetail.FileName) });

                //get the memorystream of the real image apply the background again for the _1 one compare 
                ErrorHandler.ErrorHandler.LogFileWrite("");

                ResizeWPFImageWithoutRotation(memoryStream, objResizeInfo);

                forWdht.UpdateLayout();
                //Remove Gumball score card from Edited Folder Image to be printed on runtime by Printing Console
                if (photoDetail.IsGumRideShow == true)
                {
                    string editedGumImagePath = System.IO.Path.Combine(FolderInfo.EditedImagePath, "RideCDUSB", photoDetail.FileName);
                    if (!Directory.Exists(System.IO.Path.Combine(FolderInfo.EditedImagePath, "RideCDUSB")))
                        Directory.CreateDirectory(System.IO.Path.Combine(FolderInfo.EditedImagePath, "RideCDUSB"));
                    File.Copy(FinalImagePath, editedGumImagePath, true);
                    gumEditImage = true;
                    //grdGumball.Visibility = Visibility.Hidden;
                    //ShowhidePlayerScore(Visibility.Hidden);
                    RenderTargetBitmap _objeditedImage1 = jCaptureScreen();
                    if (!Directory.Exists(FolderInfo.ThumbnailsPath + "Temp\\"))
                        Directory.CreateDirectory(FolderInfo.ThumbnailsPath + "Temp\\");
                    using (var fileStream = new FileStream(FolderInfo.ThumbnailsPath + "Temp\\" + "tmp" + photoDetail.FileName, FileMode.Create, FileAccess.ReadWrite))
                    {
                        JpegBitmapEncoder encoder = new JpegBitmapEncoder();
                        encoder.QualityLevel = imageQuality;
                        encoder.Frames.Add(BitmapFrame.Create(_objeditedImage1));
                        encoder.Save(fileStream);
                        fileStream.Position = 0;
                        fileStream.CopyTo(memoryStream);
                    }
                    BitmapImage bi = new BitmapImage();
                    using (FileStream fileStream = File.OpenRead(FinalImagePath))
                    {
                        MemoryStream ms = new MemoryStream();
                        fileStream.CopyTo(ms);
                        fileStream.Close();
                        bi.BeginInit();
                        ms.Seek(0, SeekOrigin.Begin);
                        bi.StreamSource = ms;
                        bi.DecodePixelWidth = Convert.ToInt32(OriginalWidth);
                        bi.DecodePixelHeight = Convert.ToInt32(OriginalHeight);
                        bi.EndInit();
                    }
                    using (var fileStream = new FileStream(editedImagePath, FileMode.Create, FileAccess.ReadWrite))
                    {
                        JpegBitmapEncoder encoder = new JpegBitmapEncoder();
                        encoder.QualityLevel = imageQuality;
                        encoder.Frames.Add(BitmapFrame.Create(bi));
                        encoder.Save(fileStream);
                    }
                    //File.Copy(FinalImagePath, editedImagePath, true);
                }
                IsEnablePartialEditedImage = _lstiMixConfigurationLocationInfo.Where(x => x.LocationId == photoDetail.LocationId).Count() > 0 ? true : false;
                if (IsEnablePartialEditedImage)
                {
                    SavePartialEditedImage(photoDetail, imageQuality, FinalImagePath);
                }
                memoryStream.Close();
                memoryStream.Dispose();

                UpdateImageProcessedStatus(photoDetail.PhotoId, 1);
                isEffectApplied = true;


            }
            catch (Exception ex)
            {
                //Create thumbnails for original Image and change isProcessed status to true
                ErrorHandler.ErrorHandler.LogError(ex);
            }
            return isEffectApplied;
        }
        //Saves a copy of image after removing borders and graphics
        private void SavePartialEditedImage(PhotoDetail photoDetail, int imageQuality, string FinalImagePath)
        {
            //string FinalImagePath = System.IO.Path.Combine(FolderInfo.ThumbnailsPath, "Temp", ("tmp" + photoDetail.FileName));
            double _tempZoomFactor = _ZoomFactor;
            double _tempFHeight = forWdht.Height;
            double _tempFWidth = forWdht.Width;
            double _tempTopPos = Canvas.GetTop(GrdBrightness);
            double _tempLeftPos = Canvas.GetLeft(GrdBrightness);
            try
            {
                if (canbackground.Background == null)
                {
                    forWdht.Height = GrdGreenScreenDefault3.ActualHeight;
                    forWdht.Width = GrdGreenScreenDefault3.ActualWidth;
                }
                var memoryStreamPartial = new MemoryStream();
                ClearGraphics();

                if (canbackground.Background == null)
                {
                    _ZoomFactor = 1.0;
                    ResetZoom(_ZoomFactor);
                    Zomout(true);
                    SetTempImagePosition(0, 0);
                    dragCanvas.UpdateLayout();
                    _ZoomFactor = _tempZoomFactor;
                }


                RenderTargetBitmap _objpartialeditedImage = jCapturePartialEditedImage();

                if (!Directory.Exists(FolderInfo.ThumbnailsPath + "Temp\\"))
                    Directory.CreateDirectory(FolderInfo.ThumbnailsPath + "Temp\\");

                using (var fileStream = new FileStream(FolderInfo.ThumbnailsPath + "Temp\\" + "tmp" + photoDetail.FileName, FileMode.Create, FileAccess.ReadWrite))
                {
                    JpegBitmapEncoder encoder = new JpegBitmapEncoder();
                    encoder.QualityLevel = imageQuality;
                    encoder.Frames.Add(BitmapFrame.Create(_objpartialeditedImage));
                    encoder.Save(fileStream);
                    fileStream.Position = 0;
                    fileStream.CopyTo(memoryStreamPartial);
                }
                if (!Directory.Exists(System.IO.Path.Combine(FolderInfo.HotFolderPath, "PartialEditedImages")))
                {
                    Directory.CreateDirectory(System.IO.Path.Combine(FolderInfo.HotFolderPath, "PartialEditedImages"));
                }
                //File.Copy("Temp\\" + tempfilename, System.IO.Path.Combine(FolderInfo.HotFolderPath, "PartialEditedImages", photoDetail.FileName), true);
                File.Copy(FinalImagePath, System.IO.Path.Combine(FolderInfo.HotFolderPath, "PartialEditedImages", photoDetail.FileName), true);
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                MessageBox.Show(ex.Message);
            }
            finally
            {
                if (canbackground.Background == null)
                {
                    forWdht.Height = _tempFHeight;
                    forWdht.Width = _tempFWidth;
                    ResetZoom(_ZoomFactor);
                    SetTempImagePosition(_tempTopPos, _tempLeftPos);
                }
                //MemoryManagement.FlushMemory();
            }
        }
        private void SetTempImagePosition(double _tempTopPos, double _tempLeftPos)
        {
            Canvas.SetLeft(GrdBrightness, _tempLeftPos);
            Canvas.SetTop(GrdBrightness, _tempTopPos);
        }
        public RenderTargetBitmap jCapturePartialEditedImage()
        {

            if (mainImage == null)
            {
                ErrorHandler.ErrorHandler.LogError(new Exception("mainImage is null or not found"));
                throw new Exception("mainImage is null or not found");
            }
            BitmapImage bi = mainImage.Source as BitmapImage;
            double dpiX = bi.DpiX;
            double dpiY = bi.DpiY;

            RenderTargetBitmap renderBitmap = null;
            RenderOptions.SetBitmapScalingMode(forWdht, BitmapScalingMode.HighQuality);

            RenderOptions.SetEdgeMode(forWdht, EdgeMode.Aliased);
            try
            {
                double wdt = 0.0;
                double hgt = 0.0;
                Size size;
                if (canbackground.Background == null)
                {
                    wdt = forWdht.ActualWidth;
                    hgt = forWdht.ActualHeight;
                }
                else
                {
                    wdt = forWdht.Width;
                    hgt = forWdht.Height;
                }

                size = new Size(wdt, hgt);

                //Size size = new Size(forWdht.ActualWidth, forWdht.ActualHeight);
                if (_ZoomFactor > 1.4)
                {
                    renderBitmap = new RenderTargetBitmap((int)(size.Width * dpiX / 96.0 * (1 / _ZoomFactor)), (int)(size.Height * dpiY / 96.0 * (1 / _ZoomFactor)),
                        dpiX, dpiY, PixelFormats.Default);
                    RenderOptions.SetEdgeMode(forWdht, EdgeMode.Aliased);
                    forWdht.SnapsToDevicePixels = true;
                    forWdht.RenderTransform = new ScaleTransform(1 / _ZoomFactor, 1 / _ZoomFactor, 0.5, 0.5);
                    forWdht.Measure(size);
                    forWdht.Arrange(new Rect(size));
                    renderBitmap.Render(forWdht);
                    renderBitmap.Freeze();
                    forWdht.RenderTransform = null;
                }
                else
                {
                    renderBitmap = new RenderTargetBitmap((int)(size.Width * dpiY / 96.0 * 1.5), (int)(size.Height * dpiY / 96.0 * 1.5), dpiX, dpiY, PixelFormats.Default);
                    RenderOptions.SetEdgeMode(forWdht, EdgeMode.Aliased);
                    forWdht.SnapsToDevicePixels = true;
                    forWdht.RenderTransform = new ScaleTransform(1.5, 1.5, 0.5, 0.5);
                    forWdht.Measure(size);
                    forWdht.Arrange(new Rect(size));
                    renderBitmap.Render(forWdht);
                    renderBitmap.Freeze();
                    forWdht.RenderTransform = null;
                }
                return renderBitmap;
            }
            catch (Exception ex)
            {
                Rect bounds = VisualTreeHelper.GetDescendantBounds(GrdBrightness);
                DrawingVisual dv = new DrawingVisual();
                renderBitmap = new RenderTargetBitmap((int)(bounds.Width * dpiX / 96.0),
                                                              (int)(bounds.Height * dpiY / 96.0),
                                                              dpiX,
                                                              dpiY,
                                                              PixelFormats.Default);

                using (DrawingContext ctx = dv.RenderOpen())
                {
                    VisualBrush vb = new VisualBrush(forWdht);
                    ctx.DrawRectangle(vb, null, new Rect(new System.Windows.Point(), bounds.Size));
                }

                renderBitmap.Render(dv);
                return renderBitmap;
            }
        }
        /// <summary>
        /// Removes all graphics effect.
        /// </summary>
        private void ClearGraphics()
        {
#if DEBUG
            var watch = FrameworkHelper.CommonUtility.Watch();
            watch.Start();
#endif

            try
            {
                //imageundoGrid.Background = null;
                List<UIElement> itemstoremove = new List<UIElement>();
                foreach (UIElement ui in dragCanvas.Children)
                {
                    if (ui is Grid)
                    {
                    }
                    else if (ui is Ellipse)
                    {
                    }
                    else if (ui is Rectangle)
                    {
                    }
                    else if (ui is System.Windows.Shapes.Path)
                    {

                    }
                    else
                    {
                        itemstoremove.Add(ui);
                    }
                }
                foreach (UIElement ui in itemstoremove)
                {
                    dragCanvas.Children.Remove(ui);
                }

                itemstoremove = new List<UIElement>();
                foreach (UIElement ui in frm.Children)
                {
                    if (ui is OpaqueClickableImage)
                    {
                        itemstoremove.Add(ui);
                    }
                }
                foreach (UIElement ui in itemstoremove)
                {
                    frm.Children.Remove(ui);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }

#if DEBUG
            if (watch != null)
                FrameworkHelper.CommonUtility.WatchStop(this.GetType().FullName + "." + System.Reflection.MethodBase.GetCurrentMethod().Name + "|", watch);
#endif
        }
        private void ResetZoom(double _zFactor)
        {
            if (zoomTransform != null)
            {
                zoomTransform.CenterX = mainImage.ActualWidth / 2;
                zoomTransform.CenterY = mainImage.ActualHeight / 2;
                zoomTransform.ScaleX = _zFactor;
                zoomTransform.ScaleY = _zFactor;
                transformGroup = new TransformGroup();
                transformGroup.Children.Add(zoomTransform);
                canbackground.RenderTransform = GrdGreenScreenDefault3.RenderTransform = transformGroup;
            }
        }
        /// <summary>
        /// Removes all borders.
        /// </summary>
        public void RemoveAllBorders()
        {
            List<UIElement> itemstoremove = new List<UIElement>();
            foreach (UIElement ui in dragCanvas.Children)
            {
                if (ui.Uid.StartsWith("brdr"))
                {
                    itemstoremove.Add(ui);
                }
                else if (ui is TextBox)
                {
                    TextBox tx = (TextBox)ui;
                    tx.BorderBrush = null;
                }
            }
            foreach (UIElement ui in itemstoremove)
            {
                double top = Convert.ToDouble(ui.GetValue(Canvas.TopProperty));
                double left = Convert.ToDouble(ui.GetValue(Canvas.LeftProperty));
                string[] splitter = new string[] { "@#@" };
                string Name = ui.Uid.ToString().Split(splitter, StringSplitOptions.None)[1];

                string strUri2 = @"D:\\Projects\\DigiPhoto\\Images\\" + Name.ToString();

                RotateTransform rt = ui.RenderTransform as RotateTransform;

                OpaqueClickableImage img = new OpaqueClickableImage();
                img.Source = new BitmapImage(new Uri(strUri2));
                img.SetValue(Canvas.TopProperty, top);
                img.SetValue(Canvas.LeftProperty, left);
                //img.MouseLeftButtonUp += new MouseButtonEventHandler(SelectObject);

                if (rt != null)
                {
                    RotateTransform rtm = new RotateTransform();
                    rtm.CenterX = 0;
                    rtm.CenterY = 0;
                    rtm.Angle = rt.Angle;
                    img.RenderTransform = rtm;
                }
                dragCanvas.Children.Remove(ui);
                dragCanvas.Children.Add(img);
                img.Source = null;
            }
        }
        private void ApplyCrop(PhotoDetail photoDetails)
        {
            try
            {
                if (photoDetails.IsCropped == true)
                {
                    MemoryStream memoryStream = new MemoryStream();
                    byte[] fileBytes = File.ReadAllBytes(System.IO.Path.Combine(DirectoryNameWithDate, photoDetails.FileName));
                    memoryStream.Write(fileBytes, 0, fileBytes.Length);
                    memoryStream.Position = 0;
                    BitmapSource bs = BitmapFrame.Create(memoryStream);
                    double ratioValue = bs.PixelWidth / bs.Width;
                    CroppedBitmap cb;
                    string[] rectValues;
                    double x;
                    double y;
                    double width;
                    double height;
                    double m;
                    double n;

                    if (semiOrderSettingsInfo.HorizontalCropValues != null || semiOrderSettingsInfo.VerticalCropValues != null)
                    {
                        if (bs.Width > bs.Height)
                        {
                            rectValues = semiOrderSettingsInfo.HorizontalCropValues.Split(',');
                            if (Convert.ToDouble(rectValues[2]) > Convert.ToDouble(rectValues[3]))
                                m = Convert.ToDouble(rectValues[3]) / Convert.ToDouble(rectValues[2]);
                            else
                                m = Convert.ToDouble(rectValues[2]) / Convert.ToDouble(rectValues[3]);
                            x = ratioValue * Convert.ToDouble(rectValues[0]);
                            y = ratioValue * Convert.ToDouble(rectValues[1]);
                            width = ratioValue * Convert.ToDouble(rectValues[2]);
                            height = ratioValue * Convert.ToDouble(rectValues[3]);
                            m = GetActualCropRatio(m);
                            if (bs.PixelHeight < (y + height))
                            {
                                height = Convert.ToInt32(bs.PixelHeight - y);
                                if (height > width)
                                    width = m * height;
                                else
                                    width = height / m;
                            }
                        }
                        else
                        {
                            rectValues = semiOrderSettingsInfo.VerticalCropValues.Split(',');
                            if (Convert.ToDouble(rectValues[2]) > Convert.ToDouble(rectValues[3]))
                                n = Convert.ToDouble(rectValues[3]) / Convert.ToDouble(rectValues[2]);
                            else
                                n = Convert.ToDouble(rectValues[2]) / Convert.ToDouble(rectValues[3]);
                            x = ratioValue * Convert.ToDouble(rectValues[0]);
                            y = ratioValue * Convert.ToDouble(rectValues[1]);
                            width = ratioValue * Convert.ToDouble(rectValues[2]);
                            height = ratioValue * Convert.ToDouble(rectValues[3]);
                            n = GetActualCropRatio(n);
                            if (bs.PixelWidth < (x + width))
                            {
                                if (height > width)
                                    width = n * height;
                                else
                                    width = height / n;
                                width = bs.PixelWidth - x;
                            }
                        }
                        width = Math.Round(width, 0);
                        height = Math.Round(height, 0);

                        // If Width & height is more than PixelWidth &  PixelHeight of image than adjust it.
                        if (x + width > bs.PixelWidth)
                            width = bs.PixelWidth - x;

                        if (y + height > bs.PixelHeight)
                            height = bs.PixelHeight - y;


                        try
                        {
                            cb = new CroppedBitmap(bs, new Int32Rect(Convert.ToInt32(x), Convert.ToInt32(y), Convert.ToInt32(width), Convert.ToInt32(height)));       //select region rect
                        }
                        catch (ArgumentException ex)
                        {
                            //System.ArgumentException: Value does not fall within the expected range When Int32Rect is large
                            // A CroppedBitmap with a SourceRect set to Int32Rect.Empty will render the entire image.
                            //A rectangle that is not fully within the bounds of the bitmap will not render.

                            cb = new CroppedBitmap(bs, Int32Rect.Empty);
                        }

                        MemoryStream mStream = new MemoryStream();
                        JpegBitmapEncoder jEncoder = new JpegBitmapEncoder();
                        jEncoder.Frames.Add(BitmapFrame.Create(cb));  //the croppedBitmap is a CroppedBitmap object 
                        jEncoder.QualityLevel = 50;
                        jEncoder.Save(mStream);

                        System.Drawing.Image imgSave = System.Drawing.Image.FromStream(mStream);
                        System.Drawing.Bitmap bmSave = new System.Drawing.Bitmap(imgSave);
                        bmSave.SetResolution(300, 300);
                        bmSave.Save(System.IO.Path.Combine(FolderInfo.CroppedPath, photoDetails.FileName));

                        mStream.Dispose();
                        bs.Freeze();
                        bs = null;
                        memoryStream.Dispose();
                        crop = true;
                    }
                    else
                    {
                        throw new Exception("Invalid CropValues in Semiorder settings");
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
            }
        }

        private void ApplyChromaEffect(PhotoDetail photoDetails)
        {
            try
            {
                if (photoDetails.IsGreen == true)
                {
                    if (crop)
                    {
                        using (FileStream fileStream = File.OpenRead(System.IO.Path.Combine(FolderInfo.CroppedPath, photoDetails.FileName)))
                        {
                            BitmapImage bi = new BitmapImage();
                            MemoryStream ms = new MemoryStream();
                            fileStream.CopyTo(ms);
                            ms.Seek(0, SeekOrigin.Begin);
                            fileStream.Close();
                            bi.BeginInit();
                            bi.StreamSource = ms;
                            bi.EndInit();
                            forWdht.Height = bi.Height;
                            forWdht.Width = bi.Width;
                            forWdht.RenderTransform = null;
                            forWdht.UpdateLayout();
                            forWdht.InvalidateVisual();
                            forWdht.InvalidateMeasure();
                            forWdht.InvalidateArrange();
                            mainImage.Source = bi;
                            widthimg.Source = bi;
                        }
                    }
                    else
                    {
                        using (FileStream fileStream = File.OpenRead(System.IO.Path.Combine(DirectoryNameWithDate, photoDetails.FileName)))
                        {
                            BitmapImage bi = new BitmapImage();
                            MemoryStream ms = new MemoryStream();
                            fileStream.CopyTo(ms);
                            fileStream.Close();
                            bi.BeginInit();
                            ms.Seek(0, SeekOrigin.Begin);
                            bi.StreamSource = ms;
                            bi.EndInit();
                            forWdht.Height = bi.Height;
                            forWdht.Width = bi.Width;
                            forWdht.RenderTransform = null;
                            forWdht.UpdateLayout();
                            forWdht.InvalidateVisual();
                            forWdht.InvalidateMeasure();
                            forWdht.InvalidateArrange();
                            mainImage.Source = bi;
                            widthimg.Source = bi;
                        }
                    }
                    // string greenImagePath = System.IO.Path.Combine(FolderInfo.GreenImagePath, photoDetails.FileName);
                    string originalImagePath = System.IO.Path.Combine(DirectoryNameWithDate, photoDetails.FileName);
                    string FileNameWithoutExt = System.IO.Path.GetFileNameWithoutExtension(originalImagePath);
                    //File.Copy(originalImagePath, greenImagePath, true);

                    switch (semiOrderSettingsInfo.ChromaColor.ToString())
                    {
                        case "Green":
                            ChromaKeyHSVEffect _greenscreendefault3 = new ChromaKeyHSVEffect();
                            _greenscreendefault3.HueMin = 0.2;
                            _greenscreendefault3.HueMax = 0.5;
                            _greenscreendefault3.LightnessShift = 0.15;
                            _greenscreendefault3.SaturationShift = 0.38;
                            GrdGreenScreenDefault3.Effect = _greenscreendefault3;
                            break;

                        case "Blue":
                        case "Red":
                        case "Gray":
                            ColorKeyAlphaEffect ckae = new ColorKeyAlphaEffect();
                            ckae.ColorKey = (Color)ColorConverter.ConvertFromString(semiOrderSettingsInfo.ColorCode);
                            ckae.Tolerance = Convert.ToDouble(semiOrderSettingsInfo.ClrTolerance);
                            GrdGreenScreenDefault3.Effect = ckae;
                            //ChromaEffectAllColor _colorscreendefault = new ChromaEffectAllColor();
                            //_colorscreendefault.ColorKey = (Color)ColorConverter.ConvertFromString(semiOrderSettingsInfo.ColorCode);
                            //_colorscreendefault.Tolerance = float.Parse(semiOrderSettingsInfo.ClrTolerance);
                            //GrdGreenScreenDefault3.Effect = _colorscreendefault;
                            break;

                        default:
                            break;
                    }
                    GrdGreenScreenDefault3.UpdateLayout();

                    RenderTargetBitmap _objeditedImage = CaptureScreenForPNGImage();
                    using (var fileStream = new FileStream(System.IO.Path.Combine(DirectoryNameWithDate, (FileNameWithoutExt + ".png")), FileMode.Create, FileAccess.ReadWrite))
                    {
                        PngBitmapEncoder encoder = new PngBitmapEncoder();
                        encoder.Interlace = PngInterlaceOption.On;
                        encoder.Frames.Add(BitmapFrame.Create(_objeditedImage));
                        encoder.Save(fileStream);
                        //_objeditedImage.Clear();
                    }
                }
                else
                {
                    canbackground.Background = null;
                    GrdGreenScreenDefault3.Effect = null;
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
            }
        }

        private void CopyImgToDisplayFolder(string imgSourceFilePath, string RFID, Int32 photoId, int _locationId)
        {
            LocationBusniess locBiz = new LocationBusniess();
            var item = locBiz.GetLocationsbyId(_locationId);
            if (item != null)
            {
                ImgCopyLocation targetFolderLocation = imgCopyLocationlst.Where(s => s.locID == _locationId).FirstOrDefault();
                if (targetFolderLocation.counter <= targetFolderLocation.range)
                {
                    screen = "Display" + targetFolderLocation.counter;
                    imgCopyLocationlst.Where(s => s.locID == _locationId).ToList().ForEach(s => s.counter++);
                }
                else
                {
                    screen = "Display1";
                    imgCopyLocationlst.Where(s => s.locID == _locationId).ToList().ForEach(s => s.counter = 2);
                }
                if (Directory.Exists(_previewWallSubStore + "\\" + item.DG_Location_Name + "\\" + screen))
                {
                    _copyFolderName = System.IO.Path.Combine(_previewWallSubStore, item.DG_Location_Name, screen, (RFID + "@" + photoId + ".jpg"));
                    File.Copy(imgSourceFilePath, _copyFolderName, true);
                }
            }
        }
        #endregion

        #region Common Functions
        private int ReadSubStore()
        {
            try
            {
                int subStoreId = 0;
                string pathtosave = Environment.CurrentDirectory;
                //Commented by Ajay on 2 April 18 as below code always returning 0.
                //if (!System.IO.File.Exists(pathtosave + "\\ss.dat")) return 0;
                if (System.IO.File.Exists(pathtosave + "\\ss.dat"))
                    using (var reader = new System.IO.StreamReader(pathtosave + "\\ss.dat"))
                    {
                        string line = reader.ReadLine();
                        string subStoreIds = CryptorEngine.Decrypt(line, true);
                        subStoreId = Convert.ToInt32(subStoreIds.Split(',')[0]);
                    }
                return subStoreId;
            }
            catch (Exception ex)
            {

                ErrorHandler.ErrorHandler.LogError(ex);
                return 0;
            }
        }
        private void UpdateImageProcessedStatus(int PhotoId, int ProcessedStatus)
        {
            PhotoBusiness photoBusiness = new PhotoBusiness();
            photoBusiness.UpdateImageProcessedStatus(PhotoId, ProcessedStatus);
        }
        private void ResizeWPFImageWithoutRotation(string sourceImage, int maxHeight, string saveToPath)
        {
            try
            {
                BitmapImage bi = new BitmapImage();
                BitmapImage bitmapImage = new BitmapImage();
                using (FileStream fileStream = File.OpenRead(sourceImage.ToString()))
                {
                    MemoryStream ms = new MemoryStream();
                    fileStream.CopyTo(ms);
                    ms.Seek(0, SeekOrigin.Begin);
                    fileStream.Close();
                    bi.BeginInit();
                    bi.StreamSource = ms;
                    bi.EndInit();
                    bi.Freeze();

                    decimal ratio = 0;
                    int newWidth = 0;
                    int newHeight = 0;

                    if (bi.Width >= bi.Height)
                    {
                        ratio = Convert.ToDecimal(bi.Width) / Convert.ToDecimal(bi.Height);
                        newWidth = maxHeight;
                        newHeight = Convert.ToInt32(maxHeight / ratio);
                    }
                    else
                    {
                        ratio = Convert.ToDecimal(bi.Height) / Convert.ToDecimal(bi.Width);
                        newHeight = maxHeight;
                        newWidth = Convert.ToInt32(maxHeight / ratio);
                    }

                    ms.Seek(0, SeekOrigin.Begin);
                    bitmapImage.BeginInit();
                    bitmapImage.StreamSource = ms;
                    bitmapImage.DecodePixelWidth = newWidth;
                    bitmapImage.DecodePixelHeight = newHeight;
                    bitmapImage.EndInit();
                    bitmapImage.Freeze();
                    fileStream.Close();
                }

                using (var fileStreamForSave = new FileStream(saveToPath, FileMode.Create, FileAccess.ReadWrite))
                {
                    JpegBitmapEncoder encoder = new JpegBitmapEncoder();
                    encoder.QualityLevel = 94;
                    encoder.Frames.Add(BitmapFrame.Create(bitmapImage));
                    encoder.Save(fileStreamForSave);
                    fileStreamForSave.Close();
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
            }
        }
        private RenderTargetBitmap CaptureScreenForPNGImage()
        {
            BitmapImage bi = mainImage.Source as BitmapImage;
            double dpiX = bi.DpiX;
            double dpiY = bi.DpiY;
            RenderTargetBitmap renderBitmap = null;
            try
            {
                RenderOptions.SetBitmapScalingMode(forWdht, BitmapScalingMode.HighQuality);
                RenderOptions.SetEdgeMode(forWdht, EdgeMode.Aliased);
                Size size = new Size(forWdht.ActualWidth, forWdht.ActualHeight);

                if (_ZoomFactor > 1.4)
                {
                    renderBitmap = new RenderTargetBitmap((int)(size.Width * CroppingAdorner.s_dpiY / 96.0 * (1 / (_ZoomFactor))), (int)(size.Height * CroppingAdorner.s_dpiY / 96.0 * (1 / (_ZoomFactor))), CroppingAdorner.s_dpiX, CroppingAdorner.s_dpiY, PixelFormats.Default);// PixelFormats.Pbgra32);
                    RenderOptions.SetEdgeMode(forWdht, EdgeMode.Aliased);
                    forWdht.SnapsToDevicePixels = true;
                    forWdht.RenderTransform = new ScaleTransform(1 / (_ZoomFactor), 1 / (_ZoomFactor), 0.5, 0.5);
                }
                else
                {
                    if (_ZoomFactor > .1)
                    {
                        renderBitmap = new RenderTargetBitmap((int)(size.Width * dpiY / 96.0),
                            (int)(size.Height * dpiY / 96.0), dpiX, dpiY, PixelFormats.Default);
                        RenderOptions.SetEdgeMode(forWdht, EdgeMode.Aliased);
                        forWdht.SnapsToDevicePixels = true;
                        forWdht.RenderTransform = new ScaleTransform(1, 1, 0.5, 0.5);


                        //renderBitmap = new RenderTargetBitmap((int)(size.Width * CroppingAdorner.s_dpiY / 96.0), (int)(size.Height * CroppingAdorner.s_dpiY / 96.0), CroppingAdorner.s_dpiX, CroppingAdorner.s_dpiY, PixelFormats.Pbgra32);// PixelFormats.Pbgra32);
                        //RenderOptions.SetEdgeMode(forWdht, EdgeMode.Aliased);
                        //forWdht.SnapsToDevicePixels = true;
                        //forWdht.RenderTransform = new ScaleTransform(1, 1, 0.5, 0.5);
                    }
                }

                forWdht.Measure(size);
                forWdht.Arrange(new Rect(size));
                renderBitmap.Render(forWdht);
                forWdht.RenderTransform = null;
                return renderBitmap;
            }
            catch (Exception ex)
            {
                // string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                //ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                return renderBitmap;
            }
            finally
            {
                //MemoryManagement.FlushMemory();
            }
        }
        private void ResizeWPFImageWithoutRotation(MemoryStream ms, List<ResizeInfo> oList)
        {
            try
            {
                BitmapImage bi = new BitmapImage();
                ms.Seek(0, SeekOrigin.Begin);
                bi.BeginInit();
                bi.StreamSource = ms;
                bi.EndInit();
                bi.Freeze();

                ProcessImage(ms, bi, oList);
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
            }
        }

        private static void ProcessImage(MemoryStream ms, BitmapImage bi, List<ResizeInfo> oList)
        {

            decimal ratio = 0;
            int newWidth = 0;
            int newHeight = 0;
            bool landScapeImage = false;

            if (bi.Width >= bi.Height)
            {
                ratio = bi.Width.ToDecimal() / bi.Height.ToDecimal();
                landScapeImage = true;
            }
            else
            {
                ratio = bi.Height.ToDecimal() / bi.Width.ToDecimal();
            }

            if (oList != null && oList.Count > 0)
            {
                foreach (var item in oList)
                {
                    if (landScapeImage)
                    {
                        newWidth = item.Size;
                        newHeight = (item.Size / ratio).ToInt32();
                    }
                    else
                    {
                        newHeight = item.Size;
                        newWidth = (item.Size / ratio).ToInt32();
                    }

                    SaveProcessedImage(ms, newWidth, newHeight, item.FileName);
                }
            }

        }

        private static void SaveProcessedImage(MemoryStream ms, int newWidth, int newHeight, string fileName)
        {
            BitmapImage bitmapImage = new BitmapImage();
            ms.Seek(0, SeekOrigin.Begin);
            bitmapImage.BeginInit();
            bitmapImage.StreamSource = ms;
            bitmapImage.DecodePixelWidth = newWidth;
            bitmapImage.DecodePixelHeight = newHeight;
            bitmapImage.EndInit();
            bitmapImage.Freeze();

            using (var fileStreamForSave = new FileStream(fileName, FileMode.Create, FileAccess.ReadWrite))
            {
                JpegBitmapEncoder encoder = new JpegBitmapEncoder();
                encoder.QualityLevel = 94;
                encoder.Frames.Add(BitmapFrame.Create(bitmapImage));
                encoder.Save(fileStreamForSave);
                fileStreamForSave.Close();
            }
        }
        private double GetActualCropRatio(double m)
        {
            if (m < 0.68 && m > 0.64)
                m = 0.6667;
            else if (m < 0.76 && m > 0.74)
                m = 0.75;
            else if (m < 0.81 && m > 0.79)
                m = 0.8;
            return m;
        }
        #endregion

        public RenderTargetBitmap jCaptureScreen()
        {

            if (mainImage == null)
            {
                ErrorHandler.ErrorHandler.LogError(new Exception("mainImage is null or not found"));
                throw new Exception("mainImage is null or not found");
            }
            BitmapImage bi = mainImage.Source as BitmapImage;
            double dpiX = bi.DpiX;
            double dpiY = bi.DpiY;

            RenderTargetBitmap renderBitmap = null;
            RenderOptions.SetBitmapScalingMode(forWdht, BitmapScalingMode.HighQuality);
            RenderOptions.SetEdgeMode(forWdht, EdgeMode.Aliased);
            try
            {
                Size size = new Size(forWdht.ActualWidth, forWdht.ActualHeight);
                if (_ZoomFactor > 1.4)
                {
                    renderBitmap = new RenderTargetBitmap((int)(size.Width * dpiX / 96.0 * (1 / _ZoomFactor)), (int)(size.Height * dpiY / 96.0 * (1 / _ZoomFactor)),
                        dpiX, dpiY, PixelFormats.Default);
                    RenderOptions.SetEdgeMode(forWdht, EdgeMode.Aliased);
                    forWdht.SnapsToDevicePixels = true;
                    forWdht.RenderTransform = new ScaleTransform(1 / _ZoomFactor, 1 / _ZoomFactor, 0.5, 0.5);
                    forWdht.Measure(size);
                    forWdht.Arrange(new Rect(size));
                    renderBitmap.Render(forWdht);
                    renderBitmap.Freeze();
                    forWdht.RenderTransform = null;
                }
                else
                {
                    renderBitmap = new RenderTargetBitmap((int)(size.Width * dpiY / 96.0 * 1.5), (int)(size.Height * dpiY / 96.0 * 1.5), dpiX, dpiY, PixelFormats.Default);
                    RenderOptions.SetEdgeMode(forWdht, EdgeMode.Aliased);
                    forWdht.SnapsToDevicePixels = true;
                    forWdht.RenderTransform = new ScaleTransform(1.5, 1.5, 0.5, 0.5);
                    forWdht.Measure(size);
                    forWdht.Arrange(new Rect(size));
                    renderBitmap.Render(forWdht);
                    renderBitmap.Freeze();
                    forWdht.RenderTransform = null;
                }
                return renderBitmap;
            }
            catch (Exception ex)
            {
                Rect bounds = VisualTreeHelper.GetDescendantBounds(GrdBrightness);
                DrawingVisual dv = new DrawingVisual();
                renderBitmap = new RenderTargetBitmap((int)(bounds.Width * dpiX / 96.0),
                                                              (int)(bounds.Height * dpiY / 96.0),
                                                              dpiX,
                                                              dpiY,
                                                              PixelFormats.Default);

                using (DrawingContext ctx = dv.RenderOpen())
                {
                    VisualBrush vb = new VisualBrush(forWdht);
                    ctx.DrawRectangle(vb, null, new Rect(new System.Windows.Point(), bounds.Size));
                }

                renderBitmap.Render(dv);
                return renderBitmap;
            }
        }
        double OriginalWidth = 0;
        double OriginalHeight = 0;

        public bool ImageLoader(PhotoDetail _objphoto)
        {
            bool isProcessed = false;
            grdZoomCanvas.UpdateLayout();
            GrdSize.UpdateLayout();
            _centerX = "";
            _centerY = "";

            mainImage.SnapsToDevicePixels = true;
            RenderOptions.SetEdgeMode(mainImage, EdgeMode.Aliased);
            forWdht.RenderTransform = new RotateTransform();
            string fileFullName = string.Empty;
            try
            {
                if (crop && !_objphoto.IsGreen)
                {
                    fileFullName = System.IO.Path.Combine(FolderInfo.CroppedPath, _objphoto.FileName);
                    if (File.Exists(fileFullName))
                    {
                        //using (FileStream fileStream = File.OpenRead(System.IO.Path.Combine(FolderInfo.CroppedPath, _objphoto.FileName)))
                        using (FileStream fileStream = File.OpenRead(fileFullName))
                        {
                            BitmapImage bi = new BitmapImage();
                            MemoryStream ms = new MemoryStream();
                            fileStream.CopyTo(ms);
                            ms.Seek(0, SeekOrigin.Begin);
                            fileStream.Close();
                            bi.BeginInit();
                            bi.StreamSource = ms;
                            bi.EndInit();
                            mainImage.Source = bi;
                            widthimg.Source = bi;
                            OriginalWidth = bi.PixelWidth;
                            OriginalHeight = bi.PixelHeight;
                        }
                    }
                    else
                    {
                        return isProcessed;
                    }
                }
                else if (_objphoto.IsGreen)
                {
                    fileFullName = System.IO.Path.Combine(DirectoryNameWithDate, _objphoto.FileName.Replace("jpg", "png"));
                    if (File.Exists(fileFullName))
                    {
                        //using (FileStream fileStream = File.OpenRead(System.IO.Path.Combine(DirectoryNameWithDate, _objphoto.FileName.Replace("jpg", "png"))))
                        using (FileStream fileStream = File.OpenRead(fileFullName))
                        {
                            BitmapImage bi = new BitmapImage();
                            MemoryStream ms = new MemoryStream();
                            fileStream.CopyTo(ms);
                            fileStream.Close();
                            bi.BeginInit();
                            ms.Seek(0, SeekOrigin.Begin);
                            bi.StreamSource = ms;
                            bi.EndInit();
                            mainImage.Source = bi;
                            widthimg.Source = bi;
                            OriginalWidth = bi.PixelWidth;
                            OriginalHeight = bi.PixelHeight;
                        }
                    }
                    else
                    {
                        return isProcessed;
                    }
                }
                else
                {
                    fileFullName = System.IO.Path.Combine(DirectoryNameWithDate, _objphoto.FileName);
                    if (File.Exists(fileFullName))
                    {
                        //using (FileStream fileStream = File.OpenRead(System.IO.Path.Combine(DirectoryNameWithDate, _objphoto.FileName)))
                        using (FileStream fileStream = File.OpenRead(fileFullName))
                        {
                            BitmapImage bi = new BitmapImage();
                            MemoryStream ms = new MemoryStream();
                            fileStream.CopyTo(ms);
                            fileStream.Close();
                            bi.BeginInit();
                            ms.Seek(0, SeekOrigin.Begin);
                            bi.StreamSource = ms;
                            bi.EndInit();
                            mainImage.Source = bi;
                            widthimg.Source = bi;
                            OriginalWidth = bi.PixelWidth;
                            OriginalHeight = bi.PixelHeight;
                        }
                    }
                    else
                    {
                        return isProcessed;
                    }
                }


                if (widthimg.Source != null)
                {
                    forWdht.Height = widthimg.Source.Height;
                    forWdht.Width = widthimg.Source.Width;
                }

                //if (_objphoto.Effects != null && _objphoto.Effects != string.Empty)
                if (_objphoto.Effects != null && _objphoto.Effects != string.Empty && !_objphoto.IsGreen)
                {
                    LoadXml(_objphoto.Effects);
                }

                dragCanvas.AllowDragOutOfView = true;

                GrdBrightness.Margin = new Thickness(0, 0, 0, 0);
                forWdht.HorizontalAlignment = System.Windows.HorizontalAlignment.Center;
                forWdht.VerticalAlignment = System.Windows.VerticalAlignment.Center;

                LoadFromDB(_objphoto);
                Zomout(true);
                dragCanvas.AllowDragging = false;
                dragCanvas.IsEnabled = false;

                isProcessed = true;
                int PhotoId = Convert.ToInt32(_objphoto.PhotoId);
                //get the Spec selected check box items and insert the  product id and the size selected in 
                PhotoProductLastEdit_PlaceOrder(PhotoId, productId);


            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
            }

            return isProcessed;
        }

        #region Added by Ajay on 11 April 2018 For add Product Background For Auto Spec 
        /// <summary>
        /// 
        /// </summary>
        /// <param name="PhotoId"></param>
        /// <param name="PanoramaSizeSelected"></param>
        protected void PhotoProductLastEdit_PlaceOrder(int PhotoId, int PanoramaSizeSelected)
        {
            PhotoBusiness phBiz = new PhotoBusiness();
            phBiz.PhotoProductLastEdit_PlaceOrder(PhotoId, PanoramaSizeSelected);
        }
        #endregion

        public void LoadXml(string ImageXml)
        {
            try
            {
                bool checkluminiosity = false;
                double bright = 0;
                double cont = 0;
                System.Xml.XmlDocument Xdocument = new System.Xml.XmlDocument();
                Xdocument.LoadXml(ImageXml);
                foreach (var xn in (Xdocument.ChildNodes[0]).Attributes)
                {
                    switch (((System.Xml.XmlAttribute)xn).Name.ToLower())
                    {
                        case "brightness":
                            if (((System.Xml.XmlAttribute)xn).Value.ToString() != "0")
                            {
                                if (((System.Xml.XmlAttribute)xn).Value.ToString() == "##")
                                {
                                    bright = 0;
                                }
                                else
                                {
                                    bright = Convert.ToDouble(((System.Xml.XmlAttribute)xn).Value.ToString());
                                }
                                checkluminiosity = true;
                                _brightConteff.Brightness = bright;
                            }
                            break;

                        case "contrast":
                            if (((System.Xml.XmlAttribute)xn).Value.ToString() != "1")
                            {
                                if (((System.Xml.XmlAttribute)xn).Value.ToString() == "##")
                                {
                                    cont = 1;
                                }
                                else
                                {
                                    cont = Convert.ToDouble(((System.Xml.XmlAttribute)xn).Value.ToString());
                                }
                                checkluminiosity = true;
                                _brightConteff.Contrast = cont;
                            }
                            break;
                    }
                }
                if (checkluminiosity)
                    GrdBrightness.Effect = _brightConteff;

                #region Effect

                bool coloreffect = false;
                bool checkDigimagic = false;
                foreach (var xn in (Xdocument.ChildNodes[0]).FirstChild.Attributes)
                {
                    switch (((System.Xml.XmlAttribute)xn).Name.ToLower())
                    {
                        case "sharpen":
                            if (((System.Xml.XmlAttribute)xn).Value.ToString() != "##")
                            {
                                sharpen = Convert.ToDouble(((System.Xml.XmlAttribute)xn).Value.ToString());
                                _sharpeff.Strength = sharpen;
                                _sharpeff.PixelWidth = 0.0015;
                                _sharpeff.PixelHeight = 0.0015;
                                currentsharpen = sharpen;
                                GrdSharpen.Effect = _sharpeff;
                                coloreffect = true;
                            }
                            else
                            {
                                GrdSharpen.Effect = null;
                            }
                            break;

                        case "greyscale":
                            if (((System.Xml.XmlAttribute)xn).Value.ToString() != "0")
                            {
                                GreyScaleEffect _greyscaleeff = new GreyScaleEffect();
                                _greyscaleeff.Desaturation = Convert.ToDouble(((System.Xml.XmlAttribute)xn).Value.ToString());
                                _greyscaleeff.Toned = 0;
                                GrdGreyScale.Effect = _greyscaleeff;

                                coloreffect = true;
                            }
                            else
                            {
                                GrdGreyScale.Effect = null;
                            }
                            break;
                        case "digimagic":
                            if (((System.Xml.XmlAttribute)xn).Value.ToString() != "0")
                            {
                                _brighteff.Brightness = bright;
                                _brighteff.Contrast = cont;
                                GrdBrightness.Effect = _brighteff;
                                _sharpeff.Strength = sharpen;
                                _sharpeff.PixelWidth = 0.0015;
                                _sharpeff.PixelHeight = 0.0015;
                                GrdSharpen.Effect = _sharpeff;
                                checkDigimagic = true;
                            }
                            else
                            {
                                if (!checkluminiosity)
                                {
                                    GrdBrightness.Effect = null;
                                }
                            }
                            break;
                        case "sepia":
                            if (((System.Xml.XmlAttribute)xn).Value.ToString() != "0")
                            {
                                _colorfiltereff.FilterColor = (System.Windows.Media.Color)System.Windows.Media.ColorConverter.ConvertFromString("#FFE6B34D");
                                GrdSepia.Effect = _colorfiltereff;

                                coloreffect = true;
                            }
                            else
                            {
                                GrdSepia.Effect = null;
                            }
                            break;

                        case "defog":
                            if (((System.Xml.XmlAttribute)xn).Value.ToString() != "0")
                            {
                                _brighteff.Brightness = -0.10;
                                _brighteff.Contrast = 1.09;
                                GrdBrightness.Effect = _brighteff;
                                bright = -0.10;
                                cont = 1.09;
                                coloreffect = true;
                            }
                            else
                            {
                                if (!checkluminiosity && !checkDigimagic)
                                {
                                    GrdBrightness.Effect = null;
                                }
                            }
                            break;
                        case "underwater":
                            if (((System.Xml.XmlAttribute)xn).Value.ToString() != "0")
                            {
                                _under.FogColor = (System.Windows.Media.Color)System.Windows.Media.ColorConverter.ConvertFromString("#FF4F9CEF");
                                _under.Defog = .40;
                                _under.Contrastr = .67;
                                _under.Contrastg = 1;
                                _under.Contrastb = 1;
                                _under.Exposure = .77;
                                _under.Gamma = .91;
                                GrdUnderWater.Effect = _under;
                                coloreffect = true;
                            }
                            else
                            {
                                GrdUnderWater.Effect = null;
                            }
                            break;
                        case "emboss":
                            if (((System.Xml.XmlAttribute)xn).Value.ToString() != "0")
                            {
                                EmbossedEffect emboseff = new EmbossedEffect();
                                emboseff.Amount = 0.7;
                                emboseff.Width = 0.002;
                                GrdEmboss.Effect = emboseff;

                                coloreffect = true;
                            }
                            else
                            {
                                GrdEmboss.Effect = null;
                            }
                            break;
                        case "invert":
                            if (((System.Xml.XmlAttribute)xn).Value.ToString() != "0")
                            {
                                InvertColorEffect _inverteff = new InvertColorEffect();
                                GrdInvert.Effect = _inverteff;

                                coloreffect = true;
                            }
                            else
                            {
                                GrdInvert.Effect = null;
                            }
                            break;
                        case "granite":
                            if (((System.Xml.XmlAttribute)xn).Value.ToString() != "0")
                            {
                                SketchGraniteEffect sketcheff = new SketchGraniteEffect();
                                sketcheff.BrushSize = .005;
                                GrdSketchGranite.Effect = sketcheff;

                                coloreffect = true;
                            }
                            else
                            {
                                GrdSketchGranite.Effect = null;
                            }
                            break;
                        case "hue":
                            if (((System.Xml.XmlAttribute)xn).Value.ToString() != "##")
                            {
                                hueshift = Convert.ToDouble(((System.Xml.XmlAttribute)xn).Value.ToString());
                                _shifthueeff.HueShift = hueshift;
                                GrdHueShift.Effect = _shifthueeff;
                                currenthueshift = hueshift;
                                coloreffect = true;
                            }
                            else
                            {
                                GrdHueShift.Effect = null;
                            }
                            break;
                        case "cartoon":
                            if (((System.Xml.XmlAttribute)xn).Value.ToString() != "0")
                            {
                                Cartoonize carteff = new Cartoonize();
                                carteff.Width = 150;
                                carteff.Height = 150;
                                Grdcartoonize.Effect = carteff;
                                coloreffect = true;
                            }
                            else
                            {
                                Grdcartoonize.Effect = null;
                            }
                            break;

                    }
                }

                #endregion

            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
            }
        }
        private void LoadFromDB(PhotoDetail _objPhotoDetails)
        {
            try
            {
                graphicsTextBoxCount = 0;
                if (_objPhotoDetails.Layering != null && !_objPhotoDetails.Layering.Equals("test", StringComparison.CurrentCultureIgnoreCase) && !_objPhotoDetails.Layering.Equals("NULL", StringComparison.CurrentCultureIgnoreCase))
                {
                    GrdGreenScreenDefault3.RenderTransform = null;
                    Canvas.SetLeft(GrdBrightness, 0);
                    Canvas.SetTop(GrdBrightness, 0);
                    GrdBrightness.RenderTransform = null;
                    grdZoomCanvas.RenderTransform = null;
                    if (!string.IsNullOrEmpty(_objPhotoDetails.Layering) && !string.IsNullOrWhiteSpace(_objPhotoDetails.Layering))
                        LoadXaml(_objPhotoDetails.Layering);
                }
                else
                {
                    forWdht.Width = widthimg.Source.Width;
                    forWdht.Height = widthimg.Source.Height;
                    GrdGreenScreenDefault3.RenderTransform = null;
                    Canvas.SetLeft(GrdBrightness, 0);
                    Canvas.SetTop(GrdBrightness, 0);
                    GrdBrightness.RenderTransform = null;
                    grdZoomCanvas.RenderTransform = null;
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
            }
        }
        public void Zomout(bool orignal)
        {
            try
            {
                first = false;
                grdZoomCanvas.UpdateLayout();
                double currentwidth = widthimg.Source.Width;
                double currentheight = widthimg.Source.Height;

                double ratiowidth = currentwidth / 600;
                double ratioheight = currentheight / 600;
                ratiowidth = 100 / ratiowidth / 100;
                ratioheight = 100 / ratioheight / 100;

                if (frm.Children.Count == 1)
                {
                    currentwidth = forWdht.Width;
                    currentheight = forWdht.Height;
                    ratiowidth = currentwidth / 600;
                    ratioheight = currentheight / 600;
                    ratiowidth = 100 / ratiowidth / 100;
                    ratioheight = 100 / ratioheight / 100;
                }

                ScaleTransform zoomTransform = new ScaleTransform(); ;
                TransformGroup transformGroup = new TransformGroup();
                if (currentheight > currentwidth)
                {
                    zoomTransform.ScaleX = ratioheight - .01;
                    zoomTransform.ScaleY = ratioheight - .01;
                }

                if (currentheight < currentwidth)
                {
                    zoomTransform.ScaleX = ratiowidth - .01;
                    zoomTransform.ScaleY = ratiowidth - .01;
                }
                zoomTransform.CenterX = forWdht.ActualWidth / 2;
                zoomTransform.CenterY = forWdht.ActualHeight / 2;
                transformGroup.Children.Add(zoomTransform);
                if (orignal)
                {
                    MyInkCanvas.RenderTransform = null;

                    grdZoomCanvas.LayoutTransform = transformGroup;
                }
                else
                {

                    GrdGreenScreenDefault3.RenderTransform = null;
                    MyInkCanvas.RenderTransform = transformGroup;

                    MyInkCanvas.DefaultDrawingAttributes.StylusTipTransform = new Matrix(zoomTransform.ScaleX, 0, 0, zoomTransform.ScaleY, 0, 0);

                }
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
            }
            finally
            {
                DigiPhoto.MemoryManagement.FlushMemory();
            }
        }
        private void Clear()
        {
            RemoveAllShaderEffects();
            RemoveAllGraphicsEffect();
        }
        private void LoadXaml(String inputXml)
        {
            System.Xml.XmlDocument Xdocument = new System.Xml.XmlDocument();
            Xdocument.LoadXml(inputXml);

            double translateX = 0;
            double translateY = 0;
            transformGroup = new TransformGroup();
            rotateTransform = new RotateTransform();
            try
            {
                foreach (var xn in (Xdocument.ChildNodes[0]).Attributes)
                {
                    switch (((System.Xml.XmlAttribute)xn).Name.ToLower())
                    {
                        #region Border
                        case "border":
                            if (((System.Xml.XmlAttribute)xn).Value.ToString() != string.Empty)
                            {
                                BitmapImage img = new BitmapImage();
                                BitmapImage img1 = new BitmapImage();
                                img1.BeginInit();
                                img1.UriSource = new Uri(System.IO.Path.Combine(FolderInfo.BorderPath, ((System.Xml.XmlAttribute)xn).Value.ToString()));
                                img1.EndInit();
                                img.BeginInit();
                                img.UriSource = new Uri(System.IO.Path.Combine(FolderInfo.BorderPath, ((System.Xml.XmlAttribute)xn).Value.ToString()));
                                double ratio = 1;
                                if (productId == 1 || productId == 5)
                                {
                                    if (img1.Height > img1.Width)
                                    {
                                        img.DecodePixelHeight = 2400;
                                        img.DecodePixelWidth = 1800;
                                        img.EndInit();
                                        ratio = (double)((double)img.PixelWidth / (double)img.PixelHeight);
                                    }
                                    else
                                    {
                                        img.DecodePixelHeight = 1800;
                                        img.DecodePixelWidth = 2400;
                                        img.EndInit();
                                        ratio = (double)((double)img.PixelHeight / (double)img.PixelWidth);
                                    }
                                    if (forWdht.Height > forWdht.Width)
                                    {
                                        forWdht.Width = forWdht.Height * ratio;
                                    }
                                    else
                                    {
                                        forWdht.Height = forWdht.Width * ratio;
                                    }
                                }
                                else if (productId == 2)
                                {
                                    if (img1.Height > img1.Width)
                                    {
                                        img.DecodePixelHeight = 3000;
                                        img.DecodePixelWidth = 2400;
                                        img.EndInit();
                                        ratio = (double)((double)img.PixelWidth / (double)img.PixelHeight);
                                    }
                                    else
                                    {
                                        img.DecodePixelHeight = 2400;
                                        img.DecodePixelWidth = 3000;
                                        img.EndInit();
                                        ratio = (double)((double)img.PixelHeight / (double)img.PixelWidth);
                                    }
                                    if (forWdht.Height > forWdht.Width)
                                    {
                                        forWdht.Width = forWdht.Height * ratio;
                                    }
                                    else
                                    {
                                        forWdht.Height = forWdht.Width * ratio;
                                    }
                                }
                                else if (productId == 30 || productId == 104)
                                {
                                    if (img1.Height > img1.Width)
                                    {
                                        img.DecodePixelHeight = 3000;
                                        img.DecodePixelWidth = 2000;
                                        img.EndInit();
                                        ratio = (double)((double)img.PixelWidth / (double)img.PixelHeight);
                                    }
                                    else
                                    {
                                        img.DecodePixelHeight = 2000;
                                        img.DecodePixelWidth = 3000;
                                        img.EndInit();
                                        ratio = (double)((double)img.PixelHeight / (double)img.PixelWidth);
                                    }
                                    if (forWdht.Height > forWdht.Width)
                                    {
                                        forWdht.Width = forWdht.Height * ratio;
                                    }
                                    else
                                    {
                                        forWdht.Height = forWdht.Width * ratio;
                                    }
                                }
                                else if (productId == 98)
                                {
                                    img.DecodePixelHeight = 900;
                                    img.DecodePixelWidth = 900;
                                    img.EndInit();
                                    ratio = (double)((double)img.PixelWidth / (double)img.PixelHeight);

                                    if (forWdht.Height > forWdht.Width)
                                    {
                                        forWdht.Width = forWdht.Height * ratio;
                                    }
                                    else
                                    {
                                        forWdht.Height = forWdht.Width * ratio;
                                    }
                                }


                                #region Added by ajay sinha on  12 march for the panorama product.
                                else if (productId == 121)//8x26
                                {
                                    //img.DecodePixelHeight = 900;
                                    //img.DecodePixelWidth = 900;
                                    //img.EndInit();
                                    //ratio = (double)((double)img.PixelWidth / (double)img.PixelHeight);

                                    if (img1.Height > img1.Width)
                                    {
                                        img.DecodePixelHeight = 7800;
                                        img.DecodePixelWidth = 2400;
                                        img.EndInit();
                                        ratio = (double)((double)img.PixelWidth / (double)img.PixelHeight);
                                    }
                                    else
                                    {
                                        img.DecodePixelHeight = 2400;
                                        img.DecodePixelWidth = 7800;
                                        img.EndInit();
                                        ratio = (double)((double)img.PixelHeight / (double)img.PixelWidth);
                                    }

                                    if (forWdht.Height > forWdht.Width)
                                    {
                                        forWdht.Width = forWdht.Height * ratio;
                                    }
                                    else
                                    {
                                        forWdht.Height = forWdht.Width * ratio;
                                    }
                                }
                                else if (productId == 122)//8x18
                                {
                                    //img.DecodePixelHeight = 900;
                                    //img.DecodePixelWidth = 900;
                                    //img.EndInit();
                                    //ratio = (double)((double)img.PixelWidth / (double)img.PixelHeight);

                                    if (img1.Height > img1.Width)
                                    {
                                        img.DecodePixelHeight = 5400;
                                        img.DecodePixelWidth = 2400;
                                        img.EndInit();
                                        ratio = (double)((double)img.PixelWidth / (double)img.PixelHeight);
                                    }
                                    else
                                    {
                                        img.DecodePixelHeight = 2400;
                                        img.DecodePixelWidth = 5400;
                                        img.EndInit();
                                        ratio = (double)((double)img.PixelHeight / (double)img.PixelWidth);
                                    }

                                    if (forWdht.Height > forWdht.Width)
                                    {
                                        forWdht.Width = forWdht.Height * ratio;
                                    }
                                    else
                                    {
                                        forWdht.Height = forWdht.Width * ratio;
                                    }
                                }
                                else if (productId == 123)//6x14
                                {
                                    //img.DecodePixelHeight = 900;
                                    //img.DecodePixelWidth = 900;
                                    //img.EndInit();
                                    //ratio = (double)((double)img.PixelWidth / (double)img.PixelHeight);

                                    if (img1.Height > img1.Width)
                                    {
                                        img.DecodePixelHeight = 4200;
                                        img.DecodePixelWidth = 1800;
                                        img.EndInit();
                                        ratio = (double)((double)img.PixelWidth / (double)img.PixelHeight);
                                    }
                                    else
                                    {
                                        img.DecodePixelHeight = 1800;
                                        img.DecodePixelWidth = 4200;
                                        img.EndInit();
                                        ratio = (double)((double)img.PixelHeight / (double)img.PixelWidth);
                                    }

                                    if (forWdht.Height > forWdht.Width)
                                    {
                                        forWdht.Width = forWdht.Height * ratio;
                                    }
                                    else
                                    {
                                        forWdht.Height = forWdht.Width * ratio;
                                    }
                                }

                                else if (productId == 125)//6x20
                                {
                                    //img.DecodePixelHeight = 900;
                                    //img.DecodePixelWidth = 900;
                                    //img.EndInit();
                                    //ratio = (double)((double)img.PixelWidth / (double)img.PixelHeight);

                                    if (img1.Height > img1.Width)
                                    {
                                        img.DecodePixelHeight = 6000;
                                        img.DecodePixelWidth = 1800;
                                        img.EndInit();
                                        ratio = (double)((double)img.PixelWidth / (double)img.PixelHeight);
                                    }
                                    else
                                    {
                                        img.DecodePixelHeight = 1800;
                                        img.DecodePixelWidth = 6000;
                                        img.EndInit();
                                        ratio = (double)((double)img.PixelHeight / (double)img.PixelWidth);
                                    }

                                    if (forWdht.Height > forWdht.Width)
                                    {
                                        forWdht.Width = forWdht.Height * ratio;
                                    }
                                    else
                                    {
                                        forWdht.Height = forWdht.Width * ratio;
                                    }
                                }
                                #endregion



                                else
                                {
                                    img.EndInit();
                                    if (img.Height > img.Width)
                                    {
                                        ratio = (double)(img.Width / img.Height);
                                    }
                                    else
                                    {
                                        ratio = (double)(img.Height / img.Width);
                                    }
                                    if (forWdht.Height > forWdht.Width)
                                    {
                                        forWdht.Width = forWdht.Height * ratio;
                                    }
                                    else
                                    {
                                        forWdht.Height = forWdht.Width * ratio;
                                    }
                                }

                                string[] tempFileName = img.ToString().Split('/');
                                OpaqueClickableImage objCurrent = new OpaqueClickableImage();
                                objCurrent.Uid = "frame";
                                objCurrent.Source = img;
                                objCurrent.IsHitTestVisible = false;
                                objCurrent.Stretch = Stretch.Fill; //temprorary Fix till Crop works properly
                                objCurrent.Loaded += new RoutedEventHandler(objCurrent_Loaded);

                                frm.Width = forWdht.Width;
                                frm.Height = forWdht.Height;
                                forWdht.InvalidateArrange();
                                forWdht.InvalidateMeasure();
                                forWdht.InvalidateVisual();
                                frm.Children.Add(objCurrent);
                            }
                            break;
                        #endregion
                        case "bg":
                            if (((System.Xml.XmlAttribute)xn).Value.ToString() != string.Empty)
                            {
                                #region Updated by ajay on 13 mar 18 for the applying panoramic backround.

                                BitmapImage objCurrent = null;
                                if (productId == 120)
                                {
                                    objCurrent = new BitmapImage(new Uri(System.IO.Path.Combine(FolderInfo.BackgroundPath + "\\8x24" + @"\", ((System.Xml.XmlAttribute)xn).Value.ToString())));
                                }
                                if (productId == 121)
                                {
                                    objCurrent = new BitmapImage(new Uri(System.IO.Path.Combine(FolderInfo.BackgroundPath + "\\8x26" + @"\", ((System.Xml.XmlAttribute)xn).Value.ToString())));

                                }
                                else if (productId == 122)
                                {
                                    objCurrent = new BitmapImage(new Uri(System.IO.Path.Combine(FolderInfo.BackgroundPath + "\\8x18" + @"\", ((System.Xml.XmlAttribute)xn).Value.ToString())));

                                }
                                else if (productId == 123)
                                {
                                    objCurrent = new BitmapImage(new Uri(System.IO.Path.Combine(FolderInfo.BackgroundPath + "\\6x14" + @"\", ((System.Xml.XmlAttribute)xn).Value.ToString())));

                                }
                                else if (productId == 125)
                                {
                                    objCurrent = new BitmapImage(new Uri(System.IO.Path.Combine(FolderInfo.BackgroundPath + "\\6x20" + @"\", ((System.Xml.XmlAttribute)xn).Value.ToString())));
                                }
                                #endregion
                                else
                                {
                                    objCurrent = new BitmapImage(new Uri(System.IO.Path.Combine(FolderInfo.BackgroundPath + "\\8x10" + @"\", ((System.Xml.XmlAttribute)xn).Value.ToString())));
                                }
                                canbackground.Background = new ImageBrush { ImageSource = objCurrent };
                                LoadBackgroundInGrid(objCurrent);
                            }
                            break;
                        case "canvasleft":
                            Canvas.SetLeft(GrdBrightness, Convert.ToDouble((((System.Xml.XmlAttribute)xn).Value)));
                            break;
                        case "canvastop":
                            Canvas.SetTop(GrdBrightness, Convert.ToDouble((((System.Xml.XmlAttribute)xn).Value)));
                            break;
                        case "rotatetransform":
                            RotateTransform rtm = new RotateTransform();
                            rtm.CenterX = 0;
                            rtm.CenterY = 0;
                            rtm.Angle = Convert.ToDouble((((System.Xml.XmlAttribute)xn).Value));
                            GrdBrightness.RenderTransformOrigin = new System.Windows.Point(0.5, 0.5);
                            if (rtm.Angle != -1 && rtm.Angle != 0)
                            {
                                GrdBrightness.RenderTransform = rtm;
                            }
                            break;
                        case "scalecentrex":
                            _centerX = ((System.Xml.XmlAttribute)xn).Value.ToString();
                            break;
                        case "scalecentrey":
                            _centerY = ((System.Xml.XmlAttribute)xn).Value.ToString();
                            break;
                        case "zoomfactor":
                            _ZoomFactor = Convert.ToDouble(((System.Xml.XmlAttribute)xn).Value.ToString());
                            break;
                    }
                }

                if (_centerX != "-1")
                {
                    ScaleTransform st = new ScaleTransform();
                    st.CenterX = Convert.ToDouble(_centerX);
                    st.CenterY = Convert.ToDouble(_centerY);
                    if (_ZoomFactor != -1)
                    {
                        st.ScaleX = _ZoomFactor;
                        st.ScaleY = _ZoomFactor;
                    }
                    zoomTransform = st;
                    transformGroup.Children.Add(st);
                }

                if (translateX != 0)
                {
                    TranslateTransform st = new TranslateTransform();
                    st.X = Convert.ToDouble(translateX);
                    st.Y = Convert.ToDouble(translateY);
                    transformGroup.Children.Add(st);
                }

                if (GrdGreenScreenDefault3.RenderTransform == null)
                    GrdGreenScreenDefault3.RenderTransform = transformGroup;

                System.Xml.XmlReader rdr = System.Xml.XmlReader.Create(new System.IO.StringReader(Xdocument.InnerXml.ToString()));
                while (rdr.Read())
                {
                    if (rdr.NodeType == XmlNodeType.Element)
                    {
                        switch (rdr.Name.ToString().ToLower())
                        {
                            #region Graphics
                            case "graphics":
                                Button btngrph = new Button();
                                btngrph.HorizontalAlignment = System.Windows.HorizontalAlignment.Center;
                                btngrph.VerticalAlignment = System.Windows.VerticalAlignment.Center;
                                Style defaultStyle = (Style)FindResource("ButtonStyleGraphic");
                                btngrph.Style = defaultStyle;
                                System.Windows.Controls.Image imgctrl = new System.Windows.Controls.Image();
                                BitmapImage img = new BitmapImage(new Uri(FolderInfo.GraphicPath + rdr.GetAttribute("source").ToString()));
                                imgctrl.Name = "A" + Guid.NewGuid().ToString().Split('-')[0].ToString();
                                btngrph.Name = "btn" + Guid.NewGuid().ToString().Split('-')[0].ToString();
                                btngrph.Uid = "uid" + Guid.NewGuid().ToString().Split('-')[0].ToString();
                                imgctrl.Source = img;
                                btngrph.Width = 90;
                                btngrph.Height = 90;

                                btngrph.Content = imgctrl;
                                dragCanvas.Children.Add(btngrph);
                                var left = String.Format("{0:0.00}", Convert.ToDouble(rdr.GetAttribute("left").ToString()));
                                var top = String.Format("{0:0.00}", Convert.ToDouble(rdr.GetAttribute("top").ToString()));

                                //below is the points of extreme left position of logo to extreme 
                                //right and extreme top to extreme bottom for the 6*20 product
                                //extreme left top positions :-- left:-366 top: 7.8 
                                //extreme left mid positions left:- -372 mid :288
                                //extreme left bottom  positions left: -356 bottom: 531
                                //extreme  top mid positions   left :371  top 29.9
                                //extreme mid mid positions  left :387 mid : 215
                                //extreme mid bottom positions   left : 380 bottom 540
                                //extreme right  top positions  left: 1229 top: 55
                                //extreme right  top positions  left: 1222  top :269  
                                //extreme right  top positions bottom : left :1216 : top 502

                                //so we need to dix the position of the logo as per the ratio of the image

                                double configleft = Convert.ToDouble(left);
                                double configtop = Convert.ToDouble(top);

                                #region coordinates
                                if (productId == 120)
                                {
                                    if (configleft > 850)
                                    {
                                        //847
                                        left = Convert.ToString(configleft * 0.825);
                                        //202
                                    }
                                    if (configleft < -500)
                                    {
                                        //847
                                        left = Convert.ToString(configleft * (-0.001));
                                        //202
                                    }
                                    if (configleft > -500 && configleft < -200)
                                    {
                                        //847
                                        left = Convert.ToString(configleft * (-0.5));
                                        //202
                                    }
                                    if (configleft > -200 && configleft < 0)
                                    {
                                        //847
                                        left = Convert.ToString(configleft * (-0.5));
                                        //202
                                    }
                                    if (configtop > 200)
                                    {
                                        top = Convert.ToString(configtop * .6);
                                    }
                                    if (configtop > 0 && configtop < 50)
                                    {
                                        top = Convert.ToString(configtop * .075);
                                    }
                                }
                                if (productId == 121)
                                {
                                    if (configleft > 850)
                                    {
                                        //847
                                        left = Convert.ToString(configleft * 0.70);
                                        //202
                                    }
                                    if (configleft < -500)
                                    {
                                        //847
                                        left = Convert.ToString(configleft * (-0.001));
                                        //202
                                    }
                                    if (configleft > -500 && configleft < -200)
                                    {
                                        //847
                                        left = Convert.ToString(configleft * (-0.5));
                                        //202
                                    }
                                    if (configleft > -200 && configleft < 0)
                                    {
                                        //847
                                        left = Convert.ToString(configleft * (-0.5));
                                        //202
                                    }
                                    if (configtop > 200)
                                    {
                                        top = Convert.ToString(configtop * .3);
                                    }
                                    if (configtop < 200)
                                    {
                                        top = Convert.ToString(configtop * .25);
                                    }


                                }
                                if (productId == 122)
                                {
                                    if (configleft > 850)
                                    {
                                        //847
                                        left = Convert.ToString(configleft * 0.825);
                                        //202
                                    }
                                    if (configleft < -500)
                                    {
                                        //847
                                        left = Convert.ToString(configleft * (-0.001));
                                        //202
                                    }
                                    if (configleft > -500 && configleft < -200)
                                    {
                                        //847
                                        left = Convert.ToString(configleft * (-0.5));
                                        //202
                                    }
                                    if (configleft > -200 && configleft < 0)
                                    {
                                        //847
                                        left = Convert.ToString(configleft * (-0.5));
                                        //202
                                    }
                                    if (configtop > 200)
                                    {
                                        top = Convert.ToString(configtop * .6);
                                    }
                                    if (configtop > 0 && configtop < 50)
                                    {
                                        top = Convert.ToString(configtop * .075);
                                    }
                                }
                                if (productId == 123)
                                {
                                    if (configleft > 850)
                                    {
                                        //847
                                        left = Convert.ToString(configleft * 0.825);
                                        //202
                                    }
                                    if (configleft < -500)
                                    {
                                        //847
                                        left = Convert.ToString(configleft * (-0.001));
                                        //202
                                    }
                                    if (configleft > -500 && configleft < -200)
                                    {
                                        //847
                                        left = Convert.ToString(configleft * (-0.5));
                                        //202
                                    }
                                    if (configleft > -200 && configleft < 0)
                                    {
                                        //847
                                        left = Convert.ToString(configleft * (-0.5));
                                        //202
                                    }
                                    if (configtop > 200)
                                    {
                                        top = Convert.ToString(configtop * .6);
                                    }
                                    if (configtop > 0 && configtop < 50)
                                    {
                                        top = Convert.ToString(configtop * .075);
                                    }
                                }
                                if (productId == 125)
                                {
                                    if (configleft > 850)
                                    {
                                        //847
                                        left = Convert.ToString(configleft * 0.60);
                                        //202
                                    }
                                    if (configleft < -500)
                                    {
                                        //847
                                        left = Convert.ToString(configleft * (-0.001));
                                        //202
                                    }
                                    if (configleft > -500 && configleft < -200)
                                    {
                                        //847
                                        left = Convert.ToString(configleft * (-0.5));
                                        //202
                                    }
                                    if (configleft > -200 && configleft < 0)
                                    {
                                        //847
                                        left = Convert.ToString(configleft * (-0.5));
                                        //202
                                    }
                                    if (configtop > 200)
                                    {
                                        top = Convert.ToString(configtop * .3);
                                    }


                                }
                                #endregion

                                Canvas.SetLeft(btngrph, Convert.ToDouble(left));
                                Canvas.SetTop(btngrph, Convert.ToDouble(top));
                                Canvas.SetZIndex(btngrph, Convert.ToInt32(rdr.GetAttribute("zindex")));
                                Double ZoomFactor = Convert.ToDouble(rdr.GetAttribute("zoomfactor").ToString());
                                btngrph.Width = Convert.ToDouble(rdr.GetAttribute("wthsource").ToString()) != null ? Convert.ToDouble(rdr.GetAttribute("wthsource").ToString()) : 90;
                                btngrph.Height = Convert.ToDouble(rdr.GetAttribute("wthsource").ToString()) != null ? Convert.ToDouble(rdr.GetAttribute("wthsource").ToString()) : 90;
                                btngrph.Tag = ZoomFactor.ToString();
                                TransformGroup tg = new TransformGroup();
                                RotateTransform rotation = new RotateTransform();
                                ScaleTransform scale = new ScaleTransform();
                                scale.CenterX = 0;
                                scale.CenterY = 0;
                                if (rdr.GetAttribute("scalex") != null)
                                {
                                    scale.ScaleX = Convert.ToDouble(rdr.GetAttribute("scalex").ToString());
                                }
                                if (rdr.GetAttribute("scaley") != null)
                                {
                                    scale.ScaleY = Convert.ToDouble(rdr.GetAttribute("scaley").ToString());
                                }

                                btngrph.RenderTransformOrigin = new System.Windows.Point(0.5, 0.5);
                                tg.Children.Add(scale);
                                rotation.CenterX = 0;
                                rotation.CenterY = 0;
                                rotation.Angle = Convert.ToDouble(rdr.GetAttribute("angle").ToString());
                                btngrph.RenderTransformOrigin = new System.Windows.Point(0.5, 0.5);
                                tg.Children.Add(rotation);
                                btngrph.RenderTransform = tg;
                                break;
                            #endregion
                            #region TextLogo
                            case "textlogo":
                                TextBox txtLogo = new TextBox();
                                txtLogo.ContextMenu = dragCanvas.ContextMenu;
                                txtLogo.Background = new SolidColorBrush(Colors.Transparent);
                                if (rdr.GetAttribute("FontColor").ToString() != string.Empty)
                                {
                                    var converter = new System.Windows.Media.BrushConverter();
                                    var brush = (System.Windows.Media.Brush)converter.ConvertFromString(rdr.GetAttribute("FontColor").ToString());
                                    txtLogo.Foreground = brush;
                                }
                                else
                                {
                                    txtLogo.Foreground = new SolidColorBrush(Colors.DarkRed);
                                }

                                if (rdr.GetAttribute("FontSize").ToString() != string.Empty)
                                {
                                    txtLogo.FontSize = (rdr.GetAttribute("FontSize").ToString()).ToDouble();
                                }
                                else
                                {
                                    txtLogo.FontSize = 20.00;
                                }

                                if (rdr.GetAttribute("FontFamily").ToString() != string.Empty)
                                {
                                    System.Windows.Media.FontFamily fw = (System.Windows.Media.FontFamily)new FontFamilyConverter().ConvertFromString(rdr.GetAttribute("FontFamily").ToString());
                                    txtLogo.FontFamily = fw;
                                }

                                txtLogo.RenderTransformOrigin = new System.Windows.Point(0.5, 0.5);
                                txtLogo.Uid = "txtLogoBlock";
                                txtLogo.FontWeight = FontWeights.Bold;
                                txtLogo.BorderBrush = null;
                                txtLogo.Style = (Style)FindResource("SearchIDTB");
                                dragCanvas.Children.Add(txtLogo);
                                Canvas.SetLeft(txtLogo, (rdr.GetAttribute("Left").ToString()).ToDouble());
                                Canvas.SetTop(txtLogo, (rdr.GetAttribute("Top").ToString()).ToDouble());
                                Canvas.SetZIndex(txtLogo, Convert.ToInt32(rdr.GetAttribute("ZIndex")));
                                rotation = new RotateTransform();
                                rotation.CenterX = 0;
                                rotation.CenterY = 0;
                                rotation.Angle = Convert.ToDouble(rdr.GetAttribute("Angle").ToString());
                                txtLogo.RenderTransformOrigin = new System.Windows.Point(0.5, 0.5);
                                txtLogo.RenderTransform = rotation;
                                txtLogo.Text = rdr.GetAttribute("Content").ToString();

                                graphicsTextBoxCount++;
                                break;
                            #endregion TextLogo
                            #region Textbox
                            case "textbox":
                                TextBox txtTest = new TextBox();
                                txtTest.ContextMenu = dragCanvas.ContextMenu;
                                txtTest.Background = new SolidColorBrush(Colors.Transparent);
                                if (rdr.GetAttribute("foreground").ToString() != string.Empty)
                                {
                                    var converter = new System.Windows.Media.BrushConverter();
                                    var brush = (System.Windows.Media.Brush)converter.ConvertFromString(rdr.GetAttribute("foreground").ToString());
                                    txtTest.Foreground = brush;
                                }
                                else
                                {
                                    txtTest.Foreground = new SolidColorBrush(Colors.DarkRed);
                                }

                                if (rdr.GetAttribute("fontsize").ToString() != string.Empty)
                                {
                                    txtTest.FontSize = (rdr.GetAttribute("fontsize").ToString()).ToDouble();
                                }
                                else
                                {
                                    txtTest.FontSize = 20.00;
                                }

                                if (rdr.GetAttribute("_fontfamily").ToString() != string.Empty)
                                {
                                    System.Windows.Media.FontFamily fw = (System.Windows.Media.FontFamily)new FontFamilyConverter().ConvertFromString(rdr.GetAttribute("_fontfamily").ToString());
                                    txtTest.FontFamily = fw;
                                }

                                txtTest.RenderTransformOrigin = new System.Windows.Point(0.5, 0.5);
                                txtTest.Uid = "txtblock";
                                txtTest.FontWeight = FontWeights.Bold;
                                txtTest.BorderBrush = null;
                                txtTest.Style = (Style)FindResource("SearchIDTB");
                                dragCanvas.Children.Add(txtTest);
                                Canvas.SetLeft(txtTest, (rdr.GetAttribute("left").ToString()).ToDouble());
                                Canvas.SetTop(txtTest, (rdr.GetAttribute("top").ToString()).ToDouble());
                                Canvas.SetZIndex(txtTest, Convert.ToInt32(rdr.GetAttribute("zindex").ToString()));
                                rotation = new RotateTransform();
                                rotation.CenterX = 0;
                                rotation.CenterY = 0;
                                rotation.Angle = (rdr.GetAttribute("angle").ToString()).ToDouble();
                                txtTest.RenderTransformOrigin = new System.Windows.Point(0.5, 0.5);
                                txtTest.RenderTransform = rotation;

                                txtTest.Text = rdr.GetAttribute("text").ToString();

                                graphicsTextBoxCount++;
                                break;
                            #endregion
                            case "gumball":
                                #region Gumball
                                //count++;
                                //txtGumBall.Visibility = Visibility.Visible;
                                //btnGumBall.Visibility = Visibility.Visible;
                                StackPanel spGBall = new StackPanel();
                                TextBox txtGBallPlayerScore = new TextBox();
                                txtGBallPlayerScore.Template = this.FindResource("TextBoxBaseControlTemplate") as ControlTemplate;
                                TextBox txtGumball1 = new TextBox();
                                // txtGumball1.Template = this.FindResource("TextBoxBaseControlTemplate") as ControlTemplate;-----Commented By Nilesh
                                grdGumball.ContextMenu = dragCanvas.ContextMenu;
                                if (rdr.GetAttribute("background").ToString() != string.Empty)
                                {
                                    //var converter = new System.Windows.Media.BrushConverter();
                                    //var brush = (System.Windows.Media.Brush)converter.ConvertFromString(rdr.GetAttribute("background").ToString());
                                    //txtGumball1.Background = brush;//--------Commented By Nilesh
                                    //txtGBallPlayerScore.Background = brush;-----Commented By Nilesh
                                    txtGBallPlayerScore.Background = new SolidColorBrush(Colors.Transparent);
                                }
                                else
                                {
                                    //  txtGumball1.Background = new SolidColorBrush(Colors.Transparent); ------Commented By Nilesh
                                    txtGBallPlayerScore.Background = new SolidColorBrush(Colors.Transparent);
                                }

                                if (rdr.GetAttribute("foreground").ToString() != string.Empty)
                                {
                                    var converter = new System.Windows.Media.BrushConverter();
                                    var brush = (System.Windows.Media.Brush)converter.ConvertFromString(rdr.GetAttribute("foreground").ToString());
                                    txtGumball1.Foreground = brush;
                                    //txtGBallPlayerScore.Foreground = brush; //For Ani-mayhem
                                    //txtGBallPlayerScore.Foreground = new SolidColorBrush(Colors.Red);
                                    txtGBallPlayerScore.Foreground = new SolidColorBrush(Colors.DarkRed);////For Ani-mayhem
                                }
                                else
                                {
                                    txtGumball1.Foreground = new SolidColorBrush(Colors.DarkRed);
                                    txtGBallPlayerScore.Foreground = new SolidColorBrush(Colors.DarkRed);
                                }

                                if (rdr.GetAttribute("zindex").ToString() != string.Empty)
                                    Canvas.SetZIndex(spGBall, Convert.ToInt32(rdr.GetAttribute("zindex").ToString()));
                                else
                                    Canvas.SetZIndex(spGBall, 4);

                                if (rdr.GetAttribute("fontsize").ToString() != string.Empty)
                                {
                                    txtGumball1.FontSize = Convert.ToDouble(rdr.GetAttribute("fontsize").ToString());
                                    txtGBallPlayerScore.FontSize = Convert.ToDouble(rdr.GetAttribute("fontsize").ToString()) + 6;
                                    //txtGumball1.FontSize = 14;
                                    //txtGBallPlayerScore.FontSize = 20;
                                }
                                else
                                {
                                    txtGumball1.FontSize = 36.00;
                                    txtGBallPlayerScore.FontSize = 36.00;
                                }
                                if (rdr.GetAttribute("fontfamily").ToString() != string.Empty)
                                {
                                    System.Windows.Media.FontFamily fw = (System.Windows.Media.FontFamily)new FontFamilyConverter().ConvertFromString(rdr.GetAttribute("fontfamily").ToString());
                                    txtGumball1.FontFamily = fw;
                                    txtGBallPlayerScore.FontFamily = fw;
                                }

                                if (rdr.GetAttribute("fontweight").ToString() != string.Empty)
                                {
                                    txtGumball1.FontWeight = (FontWeight)new FontWeightConverter().ConvertFromString(rdr.GetAttribute("fontweight").ToString());
                                    txtGBallPlayerScore.FontWeight = (FontWeight)new FontWeightConverter().ConvertFromString(rdr.GetAttribute("fontweight").ToString());
                                }
                                else
                                {
                                    txtGumball1.FontWeight = FontWeights.Bold;
                                    txtGBallPlayerScore.FontWeight = FontWeights.Bold;
                                }

                                if (rdr.GetAttribute("fontStyle").ToString() != string.Empty)
                                {
                                    txtGumball1.FontStyle = (FontStyle)new FontStyleConverter().ConvertFromString(rdr.GetAttribute("fontStyle").ToString());
                                    txtGBallPlayerScore.FontStyle = (FontStyle)new FontStyleConverter().ConvertFromString(rdr.GetAttribute("fontStyle").ToString());
                                }
                                else
                                {
                                    txtGumball1.FontStyle = FontStyles.Normal;
                                    txtGBallPlayerScore.FontStyle = FontStyles.Normal;
                                }

                                //Player Number
                                string pn = string.Empty; string ps = string.Empty;
                                string ft = rdr.GetAttribute("text").ToString();
                                if (!String.IsNullOrEmpty(ft))
                                {
                                    pn = ft.Split('=')[0].Trim() + " Score";
                                    //Player Score
                                    ps = ft.Split('=')[1].Trim();
                                }

                                //------Start--------Nilesh---Gummball---------
                                StackPanel spImg = new StackPanel();
                                Canvas objcan = new Canvas();
                                BitmapImage btm = null;
                                string bgimgPath = rdr.GetAttribute("BGImgPath");

                                //if (pn == "Player1 Score")
                                //{
                                //    pn = "Player 1 Score";
                                //    if (bgimgPath != "")
                                //    {
                                //        //btm = new BitmapImage(new Uri("D://Gum Ball//BGDiamondImg//11.png"));
                                //        btm = new BitmapImage(new Uri(bgimgPath));
                                //    }
                                //    //Ani-mayhem
                                //    txtGumball1.RenderTransform = new RotateTransform(5);
                                //    txtGBallPlayerScore.RenderTransform = new RotateTransform(5);

                                //}
                                //if (pn == "Player2 Score")
                                //{
                                //    pn = "Player 2 Score";
                                //    if (bgimgPath != "")
                                //    {
                                //        btm = new BitmapImage(new Uri(bgimgPath));
                                //    }
                                //    //Ani-mayhem
                                //    txtGumball1.RenderTransform = new RotateTransform(15);
                                //    txtGBallPlayerScore.RenderTransform = new RotateTransform(15);
                                //}
                                //if (pn == "Player3 Score")
                                //{
                                //    pn = "Player 3 Score";
                                //    if (bgimgPath != "")
                                //    {
                                //        btm = new BitmapImage(new Uri(bgimgPath));
                                //    }
                                //    //Ani-mayhem
                                //    txtGumball1.RenderTransform = new RotateTransform(3);
                                //    txtGBallPlayerScore.RenderTransform = new RotateTransform(3);
                                //}
                                //if (pn == "Player4 Score")
                                //{
                                //    pn = "Player 4 Score";
                                //    if (bgimgPath != "")
                                //    {
                                //        btm = new BitmapImage(new Uri(bgimgPath));
                                //    }
                                //    //Ani-mayhem
                                //    txtGumball1.RenderTransform = new RotateTransform(10);
                                //    txtGBallPlayerScore.RenderTransform = new RotateTransform(10);
                                //}
                                //if (pn == "Player5 Score")
                                //{
                                //    pn = "Player 5 Score";
                                //    if (bgimgPath != "")
                                //    {
                                //        btm = new BitmapImage(new Uri(bgimgPath));
                                //    }
                                //    //Ani-mayhem
                                //    txtGumball1.RenderTransform = new RotateTransform(-5);
                                //    txtGBallPlayerScore.RenderTransform = new RotateTransform(-5);
                                //}
                                if (pn == "Player1 Score")
                                {
                                    pn = "Player 1 Score";
                                    //btm = new BitmapImage(new Uri("D://Gum Ball//BGDiamondImg//11.png"));
                                    btm = new BitmapImage(new Uri(bgimgPath));
                                    //Ani-mayhem
                                    txtGumball1.RenderTransform = new RotateTransform(1);
                                    txtGBallPlayerScore.RenderTransform = new RotateTransform(1);
                                }
                                if (pn == "Player2 Score")
                                {
                                    pn = "Player 2 Score";
                                    btm = new BitmapImage(new Uri(bgimgPath));
                                    //Ani-mayhem
                                    txtGumball1.RenderTransform = new RotateTransform(20);
                                    txtGBallPlayerScore.RenderTransform = new RotateTransform(20);
                                }
                                if (pn == "Player3 Score")
                                {
                                    pn = "Player 3 Score";
                                    btm = new BitmapImage(new Uri(bgimgPath));
                                    //Ani-mayhem
                                    txtGumball1.RenderTransform = new RotateTransform(2);
                                    txtGBallPlayerScore.RenderTransform = new RotateTransform(2);
                                }
                                if (pn == "Player4 Score")
                                {
                                    pn = "Player 4 Score";
                                    btm = new BitmapImage(new Uri(bgimgPath));
                                    //Ani-mayhem
                                    txtGumball1.RenderTransform = new RotateTransform(10);
                                    txtGBallPlayerScore.RenderTransform = new RotateTransform(10);
                                }
                                if (pn == "Player5 Score")
                                {
                                    pn = "Player 5 Score";
                                    btm = new BitmapImage(new Uri(bgimgPath));
                                    //Ani-mayhem
                                    txtGumball1.RenderTransform = new RotateTransform(-10);
                                    txtGBallPlayerScore.RenderTransform = new RotateTransform(-10);
                                }
                                if (pn == "Player6 Score")
                                {
                                    pn = "Player 6 Score";
                                    if (bgimgPath != "")
                                    {
                                        btm = new BitmapImage(new Uri(bgimgPath));
                                    }
                                    //Ani-mayhem
                                    txtGumball1.RenderTransform = new RotateTransform(15);
                                    txtGBallPlayerScore.RenderTransform = new RotateTransform(15);
                                }

                                string[] bgImgWidth = rdr.GetAttribute("BGImgHeightWidth").Split(':');
                                string[] bgImgPosition = rdr.GetAttribute("BGImgScorePosition").Split(':');

                                Image img1 = new Image();
                                //img1.Source = btm; //Ani-mayhem 
                                img1.Source = null;
                                img1.Width = Convert.ToDouble(bgImgWidth[0]);//170;
                                img1.Height = Convert.ToDouble(bgImgWidth[1]);//170;
                                img1.VerticalAlignment = VerticalAlignment.Top;
                                //spGBall.Children.Add(img1);
                                //------End--------Nilesh---Gummball---------

                                txtGumball1.Uid = "txtgumball";
                                pn = pn.Replace("Score", ""); //For Ani-Mayhem
                                //txtGumball1.Text = pn + Environment.NewLine + ps;
                                txtGumball1.Text = pn + Environment.NewLine;//By Vins for Ani-mayhem

                                txtGumball1.IsReadOnly = true;
                                ///Added by VinS for make Player's score background transparent for Ex. Textbox 'Player 1 : 100' with bakground trans[parent_19Sep18
                                txtGumball1.Background = Brushes.Transparent;
                                Thickness thick = new Thickness(0, 0, 0, 0);
                                txtGumball1.BorderThickness = thick;

                                txtGBallPlayerScore.BorderThickness = thick;

                                txtGumball1.BorderThickness = thick;
                                spGBall.Name = !String.IsNullOrEmpty(ft) ? ft.Split('=')[0].Trim() : "";
                                //txtGBallPlayerScore.Foreground = new SolidColorBrush(Colors.Red);//For Ani-Mayhem
                                txtGBallPlayerScore.Text = Environment.NewLine + ps + " ";//By Vins
                                txtGBallPlayerScore.TextAlignment = TextAlignment.Center;
                                txtGBallPlayerScore.IsReadOnly = true;
                                txtGBallPlayerScore.MaxLength = 20;

                                grdGumball.Height = dragCanvas.ActualHeight;
                                grdGumball.Width = dragCanvas.ActualWidth;
                                string position = rdr.GetAttribute("position").ToString();
                                string margin = rdr.GetAttribute("margin").ToString();
                                spGBall.Uid = "spgumball";
                                spGBall.Orientation = Orientation.Vertical;
                                //------Start--------Nilesh---Gummball---------

                                objcan.Children.Add(img1);
                                objcan.Children.Add(txtGumball1);
                                Canvas.SetTop(txtGumball1, Convert.ToDouble(bgImgPosition[0]));
                                Canvas.SetLeft(txtGumball1, Convert.ToDouble(bgImgPosition[1]));


                                objcan.Children.Add(txtGBallPlayerScore); //Added by Vins Ani-Mayhem
                                Canvas.SetTop(txtGBallPlayerScore, Convert.ToDouble(bgImgPosition[0]));
                                Canvas.SetLeft(txtGBallPlayerScore, Convert.ToDouble(bgImgPosition[1]));
                                spGBall.Children.Add(objcan);
                                //------End--------Nilesh---Gummball---------

                                //spGBall.Children.Add(txtGumball1);--Commented--By-Nilesh------------
                                // spGBall.Children.Add(txtGBallPlayerScore);--Commented--By-Nilesh------------

                                dragCanvas.Children.Add(spGBall);
                                spGBall.UpdateLayout();
                                spGBall.Measure(new Size(double.PositiveInfinity, double.PositiveInfinity));
                                Size size = spGBall.DesiredSize; //MeasureString(txtGumball.Text, txtGumball);
                                //actualHgt = size.Height; //actualHgt + size.Height;
                                //if (size.Width > actualWdth)
                                //    actualWdth = size.Width;
                                Canvas.SetZIndex(grdGumball, 7);
                                //Position Related Change
                                SetGumballPosition(position, margin, spGBall);
                                //GumBallAppend.Append("<Gumball player = '" + rdr.GetAttribute("player").ToString() + "' zindex='" + rdr.GetAttribute("zindex").ToString() + "' text= '" + rdr.GetAttribute("text").ToString() + "' foreground='" + rdr.GetAttribute("foreground").ToString() + "' fontfamily='" + rdr.GetAttribute("fontfamily").ToString() + "' fontsize= '" + rdr.GetAttribute("fontsize").ToString() + "' fontweight= '" + rdr.GetAttribute("fontweight").ToString() + "' fontStyle='" + rdr.GetAttribute("fontStyle").ToString() + "' background='" + rdr.GetAttribute("background").ToString() + "' position='" + position + "' margin='" + margin + "'/>");
                                spGumball.RenderTransform = null;
                                DragCanvas.SetCanBeDragged(spGBall, false);
                                #endregion
                                break;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
            }
        }

        private void SetGumballPosition(string position, string margin, StackPanel txtbox)
        {
            if (position == "Top-Left")
            {
                txtbox.HorizontalAlignment = HorizontalAlignment.Left;
                txtbox.VerticalAlignment = VerticalAlignment.Top;
                //Thickness mg = new Thickness(Convert.ToDouble(margin.Split(',')[1]), Convert.ToDouble(margin.Split(',')[0]), 0, 0);
                Thickness mg = new Thickness(Convert.ToDouble(margin.Split(',')[0]), Convert.ToDouble(margin.Split(',')[1]), Convert.ToDouble(margin.Split(',')[2]), Convert.ToDouble(margin.Split(',')[3]));
                txtbox.Margin = mg;
                txtbox.UpdateLayout();
                Canvas.SetLeft(txtbox, 10);
                Canvas.SetTop(txtbox, 10);
            }
            else if (position == "Top-Center")
            {
                txtbox.HorizontalAlignment = HorizontalAlignment.Center;
                txtbox.VerticalAlignment = VerticalAlignment.Top;
                //Thickness mg = new Thickness(Convert.ToDouble(margin.Split(',')[1]), Convert.ToDouble(margin.Split(',')[0]), 0, 0);
                Thickness mg = new Thickness(Convert.ToDouble(margin.Split(',')[0]), Convert.ToDouble(margin.Split(',')[1]), Convert.ToDouble(margin.Split(',')[2]), Convert.ToDouble(margin.Split(',')[3]));
                txtbox.Margin = mg;
                txtbox.UpdateLayout();

                double left = (forWdht.ActualWidth - txtbox.ActualWidth) / 2;
                Canvas.SetLeft(txtbox, left);

                //double top = (forWdht.ActualHeight - txtbox.ActualHeight) / 2;
                //Canvas.SetTop(txtbox, top);

                Canvas.SetTop(txtbox, 10);

            }
            else if (position == "Top-Right")
            {
                txtbox.HorizontalAlignment = HorizontalAlignment.Right;
                txtbox.VerticalAlignment = VerticalAlignment.Top;
                //Thickness mg = new Thickness(0, Convert.ToDouble(margin.Split(',')[0]), Convert.ToDouble(margin.Split(',')[1]), 0);
                Thickness mg = new Thickness(Convert.ToDouble(margin.Split(',')[0]), Convert.ToDouble(margin.Split(',')[1]), Convert.ToDouble(margin.Split(',')[2]), Convert.ToDouble(margin.Split(',')[3]));
                txtbox.Margin = mg;
                txtbox.UpdateLayout();
                Canvas.SetTop(txtbox, 10);
                Canvas.SetLeft(txtbox, forWdht.ActualWidth - (txtbox.ActualWidth) - mg.Right);

            }
            else if (position == "Bottom-Left")
            {
                txtbox.HorizontalAlignment = HorizontalAlignment.Left;
                txtbox.VerticalAlignment = VerticalAlignment.Bottom;
                //Thickness mg = new Thickness(Convert.ToDouble(margin.Split(',')[1]), 0, 0, Convert.ToDouble(margin.Split(',')[0]));
                Thickness mg = new Thickness(Convert.ToDouble(margin.Split(',')[0]), Convert.ToDouble(margin.Split(',')[1]), Convert.ToDouble(margin.Split(',')[2]), Convert.ToDouble(margin.Split(',')[3]));
                txtbox.Margin = mg;
                txtbox.UpdateLayout();
                Canvas.SetLeft(txtbox, 10);
                Canvas.SetTop(txtbox, forWdht.ActualHeight - txtbox.ActualHeight - mg.Bottom);
                //Canvas.SetLeft(grdGumball, 10);
                //Canvas.SetTop(grdGumball, forWdht.ActualHeight - grdGumball.Height);
            }
            else if (position == "Bottom-Center")
            {
                txtbox.HorizontalAlignment = HorizontalAlignment.Center;
                txtbox.VerticalAlignment = VerticalAlignment.Bottom;
                //Thickness mg = new Thickness(Convert.ToDouble(margin.Split(',')[1]), 0, 0, Convert.ToDouble(margin.Split(',')[0]));
                Thickness mg = new Thickness(Convert.ToDouble(margin.Split(',')[0]), Convert.ToDouble(margin.Split(',')[1]), Convert.ToDouble(margin.Split(',')[2]), Convert.ToDouble(margin.Split(',')[3]));
                txtbox.Margin = mg;
                txtbox.UpdateLayout();
                //Canvas.SetLeft(txtbox, 10);
                //Canvas.SetTop(txtbox, forWdht.Height - actualHgt - mg.Bottom - 10);
                Canvas.SetTop(txtbox, forWdht.ActualHeight - txtbox.ActualHeight - mg.Bottom);
                double left = (forWdht.ActualWidth - txtbox.ActualWidth) / 2;
                Canvas.SetLeft(txtbox, left);
                //txtbox.Margin = mg;
                //txtbox.UpdateLayout();
                //Canvas.SetLeft(grdGumball, 10);
                //Canvas.SetTop(grdGumball, forWdht.ActualHeight - grdGumball.Height);
                //txtbox.HorizontalAlignment = HorizontalAlignment.Center;
                //txtbox.VerticalAlignment = VerticalAlignment.Bottom;
            }
            else if (position == "Bottom-Right")
            {
                txtbox.HorizontalAlignment = HorizontalAlignment.Right;
                txtbox.VerticalAlignment = VerticalAlignment.Bottom;
                //Thickness mg = new Thickness(0, 0, Convert.ToDouble(margin.Split(',')[1]), Convert.ToDouble(margin.Split(',')[0]));
                Thickness mg = new Thickness(Convert.ToDouble(margin.Split(',')[0]), Convert.ToDouble(margin.Split(',')[1]), Convert.ToDouble(margin.Split(',')[2]), Convert.ToDouble(margin.Split(',')[3]));
                txtbox.Margin = mg;
                txtbox.UpdateLayout();
                Canvas.SetLeft(txtbox, forWdht.ActualWidth - txtbox.ActualWidth - mg.Right);
                Canvas.SetTop(txtbox, forWdht.ActualHeight - txtbox.ActualHeight - mg.Bottom);
                //Canvas.SetLeft(grdGumball, forWdht.ActualWidth - grdGumball.Width);
                //Canvas.SetTop(grdGumball, forWdht.ActualHeight - grdGumball.Height);
            }
            else
            {
                txtbox.HorizontalAlignment = HorizontalAlignment.Right;
                txtbox.VerticalAlignment = VerticalAlignment.Bottom;
                //Thickness mg = new Thickness(0, 0, Convert.ToDouble(margin.Split(',')[1]), Convert.ToDouble(margin.Split(',')[0]));
                Thickness mg = new Thickness(Convert.ToDouble(margin.Split(',')[0]), Convert.ToDouble(margin.Split(',')[1]), Convert.ToDouble(margin.Split(',')[2]), Convert.ToDouble(margin.Split(',')[3]));
                txtbox.Margin = mg;
                txtbox.UpdateLayout();
                Canvas.SetLeft(txtbox, forWdht.ActualWidth - txtbox.ActualWidth);
                Canvas.SetTop(txtbox, forWdht.ActualHeight - txtbox.ActualHeight);
                //Canvas.SetLeft(grdGumball, forWdht.ActualWidth - grdGumball.Width);
                //Canvas.SetTop(grdGumball, forWdht.ActualHeight - grdGumball.Height);
            }
        }

        private void LoadBackgroundInGrid(BitmapImage objCurrent)
        {
            if (frm.Children.Count == 0)
            {

                double ratio = 1;
                if (objCurrent.Height > objCurrent.Width)
                {
                    ratio = (double)(objCurrent.Width / objCurrent.Height);
                }
                else
                {
                    ratio = (double)(objCurrent.Height / objCurrent.Width);
                }

                if (forWdht.Height > forWdht.Width)
                {
                    forWdht.Width = forWdht.Height * ratio;
                }
                else
                {
                    forWdht.Height = forWdht.Width * ratio;
                }

                forWdht.InvalidateArrange();
                forWdht.InvalidateMeasure();
                forWdht.InvalidateVisual();
                Zomout(true);
            }
            objCurrent.Freeze();


        }
        void objCurrent_Loaded(object sender, RoutedEventArgs e)
        {
            Canvas.SetLeft(frm, (dragCanvas.ActualWidth - frm.ActualWidth) / 2); Canvas.SetTop(frm, (dragCanvas.ActualHeight - frm.ActualHeight) / 2);
        }
        private void RemoveAllShaderEffects()
        {
            try
            {
                _centerX = "";
                _centerY = "";
                _ZoomFactor = 0;
                OriginalHeight = 0;
                OriginalWidth = 0;
                crop = false;
                GrdInvert.Effect = null;
                GrdSharpen.Effect = null;
                GrdSketchGranite.Effect = null;
                GrdEmboss.Effect = null;
                Grdcartoonize.Effect = null;
                GrdGreyScale.Effect = null;
                GrdHueShift.Effect = null;
                Grdcolorfilter.Effect = null;
                GrdBrightness.Effect = null;
                GrdSepia.Effect = null;
                GrdRedEyeFirst.Effect = null;
                GrdRedEyeSecond.Effect = null;
                GrdRedEyeMultiple.Effect = null;
                GrdRedEyeMultiple1.Effect = null;
                GrdGreenScreenDefault3.Effect = null;
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
            }
        }
        private void RemoveAllGraphicsEffect()
        {
            try
            {
                canbackground.Background = null;
                List<UIElement> itemstoremove = new List<UIElement>();
                foreach (UIElement ui in dragCanvas.Children)
                {
                    if (ui is Grid)
                    {
                    }
                    else
                    {
                        itemstoremove.Add(ui);
                    }
                }
                foreach (UIElement ui in itemstoremove)
                {
                    dragCanvas.Children.Remove(ui);
                }

                itemstoremove = new List<UIElement>();
                foreach (UIElement ui in frm.Children)
                {
                    if (ui is OpaqueClickableImage)
                    {
                        itemstoremove.Add(ui);
                    }
                }
                foreach (UIElement ui in itemstoremove)
                {
                    frm.Children.Remove(ui);
                }

                //Clean old Gumball Data
                itemstoremove = new List<UIElement>();
                foreach (UIElement ui in spGumball.Children)
                {
                    if (ui is TextBox)
                    {
                        itemstoremove.Add(ui);
                    }
                }
                foreach (UIElement ui in itemstoremove)
                {
                    spGumball.Children.Remove(ui);
                }

                _ZoomFactor = 1;
                if (zoomTransform != null)
                {
                    zoomTransform.CenterX = mainImage.ActualWidth / 2;
                    zoomTransform.CenterY = mainImage.ActualHeight / 2;

                    zoomTransform.ScaleX = _ZoomFactor;
                    zoomTransform.ScaleY = _ZoomFactor;
                    zoomTransform = new ScaleTransform();
                    transformGroup = new TransformGroup();
                    rotateTransform = new RotateTransform();
                    GrdBrightness.RenderTransform = null;
                    Canvas.SetLeft(GrdBrightness, 0);
                    Canvas.SetTop(GrdBrightness, 0);
                }
                Canvas.SetLeft(grdGumball, 0);
                Canvas.SetTop(grdGumball, 0);
                GrdGreenScreenDefault3.RenderTransform = null;
                GrdBrightness.RenderTransform = null;
                grdZoomCanvas.RenderTransform = null;
                grdZoomCanvas.LayoutTransform = null;
                MyInkCanvas.RenderTransform = null;
                zoomTransform = new ScaleTransform();
                rotateTransform = new RotateTransform();
                transformGroup = new TransformGroup();
                mainImage.Source = null;
                widthimg.Source = null;
                GrdGreenScreenDefault3.UpdateLayout();
                GrdSize.UpdateLayout();
                GrdSize.InvalidateVisual();
                GrdSize.InvalidateMeasure();
                GrdSize.InvalidateArrange();
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
            }
        }

        private void ShowhidePlayerScore(Visibility vsb)
        {
            for (int i = 1; i < 7; i++)
            {
                StackPanel foundTextBox = FindChild<StackPanel>(dragCanvas, "Player" + i.ToString());
                if (foundTextBox != null)
                {
                    foundTextBox.Visibility = vsb;
                }
            }
        }
        /// <summary>
        /// Finds a Child of a given item in the visual tree. 
        /// </summary>
        /// <param name="parent">A direct parent of the queried item.</param>
        /// <typeparam name="T">The type of the queried item.</typeparam>
        /// <param name="childName">x:Name or Name of child. </param>
        /// <returns>The first parent item that matches the submitted type parameter. 
        /// If not matching item can be found, 
        /// a null parent is being returned.</returns>
        public static T FindChild<T>(DependencyObject parent, string childName)
           where T : DependencyObject
        {
            // Confirm parent and childName are valid. 
            if (parent == null) return null;

            T foundChild = null;

            int childrenCount = VisualTreeHelper.GetChildrenCount(parent);
            for (int i = 0; i < childrenCount; i++)
            {
                var child = VisualTreeHelper.GetChild(parent, i);
                // If the child is not of the request child type child
                T childType = child as T;
                if (childType == null)
                {
                    // recursively drill down the tree
                    foundChild = FindChild<T>(child, childName);

                    // If the child is found, break so we do not overwrite the found child. 
                    if (foundChild != null) break;
                }
                else if (!string.IsNullOrEmpty(childName))
                {
                    var frameworkElement = child as FrameworkElement;
                    // If the child's name is set for search
                    if (frameworkElement != null && frameworkElement.Name == childName)
                    {
                        // if the child's name is of the request name
                        foundChild = (T)child;
                        break;
                    }
                }
                else
                {
                    // child element found.
                    foundChild = (T)child;
                    break;
                }
            }

            return foundChild;
        }

        string Photoids = string.Empty;
        void Compare(string bmp1, string bmp2, byte threshold = 3)
        {


            string image1Path = System.IO.Path.Combine(@"\\" + FolderInfo.HotFolderPath + "\\Thumbnails\\Temp", bmp1);
            string image2Path = System.IO.Path.Combine(@"\\" + FolderInfo.HotFolderPath + "\\Thumbnails\\" + DateTime.Now.ToString("yyyyMMdd"), bmp2);

            System.Drawing.Bitmap firstBmp = (System.Drawing.Bitmap)System.Drawing.Image.FromFile(image1Path);
            System.Drawing.Bitmap secondBmp = (System.Drawing.Bitmap)System.Drawing.Image.FromFile(image2Path);
            float ImageDiff = firstBmp.PercentageDifference(secondBmp, threshold) * 100;
            if (ImageDiff > 35)
            {
                //write to file
                Photoids += image2Path + "\n";
                //update the status to 0 for these files
                List<PhotoInfo> lstAllLatest = new List<PhotoInfo>();
                PhotoBusiness phBiz = new PhotoBusiness();
                lstAllLatest = phBiz.GetPhotoDetailsbyName(bmp2);
                if (lstAllLatest.Count > 0)
                {
                    UpdateImageProcessedStatus(lstAllLatest[0].DG_Photos_pkey, 0);
                    ErrorHandler.ErrorHandler.LogFileWrite(string.Format("Wrong Images : {0:0.0} %", ImageDiff) + lstAllLatest[0].DG_Photos_pkey);

                }
            }


        }
    }
    public class ImgCopyLocation
    {
        public Int32 locID { get; set; }
        public string locName { get; set; }
        public Int32 counter { get; set; }
        public Int32 range { get; set; }

    }
}