﻿using System;
using System.IO;
using System.Reflection;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Effects;
using System.Windows.Media.Media3D;

namespace ImageProcessingEngine.Shader
{
    public class SketchGraniteEffect : ShaderEffect
    {
        private static string executableLocation = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
        private static string xslLocation = Path.Combine(executableLocation, "Shader\\SketchGranite.ps");
        
        public static readonly DependencyProperty InputProperty = ShaderEffect.RegisterPixelShaderSamplerProperty("Input", typeof(SketchGraniteEffect), 0);
        public static readonly DependencyProperty BrushSizeProperty = DependencyProperty.Register("BrushSize", typeof(double), typeof(SketchGraniteEffect), new UIPropertyMetadata(((double)(0.003D)), PixelShaderConstantCallback(0)));
        public SketchGraniteEffect()
        {
            PixelShader pixelShader = new PixelShader();
            pixelShader.UriSource = new Uri(xslLocation);
            this.PixelShader = pixelShader;

            this.UpdateShaderValue(InputProperty);
            this.UpdateShaderValue(BrushSizeProperty);
        }
        public Brush Input
        {
            get
            {
                return ((Brush)(this.GetValue(InputProperty)));
            }
            set
            {
                this.SetValue(InputProperty, value);
            }
        }
        /// <summary>The brush size of the sketch effect.</summary>
        public double BrushSize
        {
            get
            {
                return ((double)(this.GetValue(BrushSizeProperty)));
            }
            set
            {
                this.SetValue(BrushSizeProperty, value);
            }
        }
    }
}
