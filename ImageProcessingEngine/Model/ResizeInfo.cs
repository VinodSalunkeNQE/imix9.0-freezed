﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ImageProcessingEngine.Model
{
    public class ResizeInfo
    {
        public int Size { get; set; }
        public string FileName { get; set; }
    }

    #region Start of Changes by Suraj Mali for Bug : Create new Image resize factor Date: 26122020
    public class ImageResizeInfo
    {
        public double scaleFactor { get; set; }
        public string sourcePath { get; set; }
        public string targetPath { get; set; }
    }
    #endregion
}
