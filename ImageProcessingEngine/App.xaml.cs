﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Threading;
using System.Diagnostics;
using DigiPhoto;
using Hardcodet.Wpf.TaskbarNotification;
using ImageProcessingEngine.Properties;
using System.IO;
using System.Windows.Media.Animation;
using DigiPhoto.Cache.DataCache;
namespace ImageProcessingEngine
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        #region Declaration
        private TaskbarIcon notifyIcon;
        public static int SubStoreId;
        #endregion
        private void Application_Startup(object sender, StartupEventArgs e)
        {

            DataCacheFactory.Register();
            // Get Reference to the current Process
            Timeline.DesiredFrameRateProperty.OverrideMetadata(typeof(Timeline),
                              new FrameworkPropertyMetadata { DefaultValue = 5 });
            Process thisProc = Process.GetCurrentProcess();
            // Check how many total processes have the same name as the current one
            if (Process.GetProcessesByName(thisProc.ProcessName).Length > 1)
            {
               // If there is more than one, then it is already running.
                MessageBox.Show("ImageProcessingEngine is already running.");
                Application.Current.Shutdown();
                return;

            }
            notifyIcon = new TaskbarIcon();
            notifyIcon.Icon = ImageProcessingEngine.Properties.Resources.ImgProcessSmallIcon;
            notifyIcon.ToolTipText = "ImageProcessingEngine";
            notifyIcon.Visibility = Visibility.Visible;
             notifyIcon.TrayPopup = new mypopup();
            try
            {
                //MessageBox.Show("Application_Startup 5 ---------------");
                //string pathtosave = Environment.CurrentDirectory;
                //MessageBox.Show("Application_Startup 6 ---------------"+ pathtosave);
                //if (File.Exists(pathtosave + "\\ss.dat"))
                //{
                //    string line;

                //    using (StreamReader reader = new StreamReader(pathtosave + "\\ss.dat"))
                //    {
                //        line = reader.ReadLine();
                //        string subID = CryptorEngine.Decrypt(line, true);
                //        SubStoreId = Convert.ToInt32(subID.Split(',')[0]);

                //    }
                //}
                //else
                //{
                //    MessageBox.Show("Please select substore from Configuration Section in Imix for this machine.");
                //    Application.Current.Shutdown();
                //}
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
            }
        }
        static void MyHandler(object sender, UnhandledExceptionEventArgs args)
        {
            Exception e = (Exception)args.ExceptionObject;
            MessageBox.Show(e.Message);
        }
        void AppDispatcherUnhandledException(object sender, DispatcherUnhandledExceptionEventArgs e)
        {
            ShowUnhandeledException(e);
        }
        void ShowUnhandeledException(DispatcherUnhandledExceptionEventArgs e)
        {
            e.Handled = true;
            string errorMessage = string.Format("An application error occurred.\nPlease check whether your data is correct and repeat the action. If this error occurs again there seems to be a more serious malfunction in the application, and you better close it.\n\nError:{0}\n\nDo you want to continue?\n(if you click Yes you will continue with your work, if you click No the application will close)",
            e.Exception.Message + (e.Exception.InnerException != null ? "\n" +
            e.Exception.InnerException.Message : null));
            if (MessageBox.Show(errorMessage, "Application Error", MessageBoxButton.YesNoCancel, MessageBoxImage.Error) == MessageBoxResult.No)
            {
                if (MessageBox.Show("WARNING: The application will close. Any changes will not be saved!\nDo you really want to close it?", "Close the application!", MessageBoxButton.YesNoCancel, MessageBoxImage.Warning) == MessageBoxResult.Yes)
                {
                    Application.Current.Shutdown();
                }
            }
        }

    }
}
