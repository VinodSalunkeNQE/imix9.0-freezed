﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Configuration;

namespace DigiGetSystemInfo
{
    class DataAccessLayer
    {
        SqlCommand _command;
        public DataAccessLayer()
        {

        }

        public string MyConString
        {
            get
            {
                return ConfigurationManager.ConnectionStrings["DigiConnectionString"].ConnectionString;
            }
        }
        public bool InsertSystemInfo(SystemInfo sInfo)
        {
            try
            {
                using (SqlConnection sqlConnection = new SqlConnection(MyConString))
                {
                    sqlConnection.Open();
                    _command = sqlConnection.CreateCommand();
                    _command.CommandText = "InsertSystemInfo";
                    _command.CommandTimeout = 120;
                    _command.CommandType = CommandType.StoredProcedure;
                    _command.Parameters.AddWithValue("@SystemName",sInfo.SystemName);
                    _command.Parameters.AddWithValue("@MACAddress", sInfo.MACAddress);
                    _command.Parameters.AddWithValue("@IPAddress", sInfo.IPAddress);
                    _command.Parameters.AddWithValue("@CPU", sInfo.CPU);
                    _command.Parameters.AddWithValue("@Memory", sInfo.Memory);
                    _command.Parameters.AddWithValue("@DriveC", sInfo.DriveC);
                    _command.Parameters.AddWithValue("@DriveD", sInfo.DriveD);
                    _command.Parameters.AddWithValue("@DriveE", sInfo.DriveE);
                    var returnValue = _command.ExecuteNonQuery();
                    sqlConnection.Close();            
                    return true;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}
