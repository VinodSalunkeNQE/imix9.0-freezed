﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace DigiGetSystemInfo
{
    static class Loggers
    {
        private static volatile object _object = new Object();
        private static bool IsLog = false;

        static Loggers()
        {
            if (ConfigurationManager.AppSettings["Sequancelog"] != null)
            {
                IsLog = Convert.ToBoolean(ConfigurationManager.AppSettings["PartnerId"]);                    
            }
        }
        private static void errorlog(string Message)
        {
            string root = Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location));
            string logFilePath = root + "\\";
            logFilePath = System.IO.Path.Combine(logFilePath, "DigiLogError");
            logFilePath = logFilePath + "\\" + "SystemInfoLog" + "-" + DateTime.Today.ToString("yyyyMMdd") + "." + "txt";

            DirectoryInfo logDirInfo = null;
            FileInfo logFileInfo = new FileInfo(logFilePath);
            logDirInfo = new DirectoryInfo(logFileInfo.DirectoryName);

            if (!logDirInfo.Exists)
                logDirInfo.Create();

            if (!File.Exists(logFilePath))
            {
                // Create a file to write to.   
                using (StreamWriter sw = File.CreateText(logFilePath))
                {
                    sw.WriteLine(Message);
                }
            }
            else
            {
                using (StreamWriter sw = File.AppendText(logFilePath))
                {
                    sw.WriteLine(Message);
                }
            }
        }

        private static void LogFileWrite(string message)
        {
            lock (_object)
            {
                errorlog(message);
            }
        }

        private static string CreateErrorMessage(Exception ServiceException)
        {
            StringBuilder messageBuilder = new StringBuilder();

            try
            {
                messageBuilder.Append(DateTime.Now.ToString("HH:mm:ss tt"));
                messageBuilder.Append(" :" + " The Exception is:- " + ServiceException.ToString());

                if (ServiceException.InnerException != null)
                {
                    messageBuilder.Append(" InnerException :: " + ServiceException.InnerException.ToString());
                }
                if (ServiceException.StackTrace != null)
                {
                    messageBuilder.Append(" StackTrace :: " + ServiceException.StackTrace.ToString());
                }
                if (ServiceException.Message != null)
                {
                    messageBuilder.Append(" Message is :: " + ServiceException.Message.ToString());
                }

                return messageBuilder.ToString();
            }
            catch
            {
                messageBuilder.Append("Exception:: Unknown Exception.");
                return messageBuilder.ToString();
            }

        }

        public static void Exceptionlog(Exception ex)
        {
            string errormsg = string.Empty;
            errormsg = CreateErrorMessage(ex);
            LogFileWrite(errormsg);
        }

        public static void Sequencelog(string Message)
        {
            if (IsLog)
                LogFileWrite(Message);
        }
    }

}
