﻿using System;
using System.Diagnostics;
using System.ServiceProcess;
using System.Net;
using System.Net.Sockets;
using System.Net.NetworkInformation;
using System.IO;

namespace DigiGetSystemInfo
{
    public partial class Service1 : ServiceBase
    {
        private System.Timers.Timer timer;
        public Service1()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            try
            {
                Loggers.Sequencelog("Method : OnStart :  step -1");
                StartScheduler();
                Loggers.Sequencelog("Method : OnStart :  step -2");
            }
            catch(Exception ex)
            {
                Loggers.Exceptionlog(ex);
            }
        }

        protected override void OnStop()
        {
        }

        public void StartScheduler()
        {
            try
            {
                Loggers.Sequencelog("Method : StartSchedular :  step -1");
                this.timer = new System.Timers.Timer();
                this.timer.Interval = new TimeSpan(0, 0, 10).TotalMilliseconds;
                this.timer.AutoReset = true;
                this.timer.Elapsed += new System.Timers.ElapsedEventHandler(this.timer_Elapsed);
                this.timer.Start();
                this.timer.Enabled = true;
                Loggers.Sequencelog("Method : StartSchedular :  step -2");
            }
            catch (Exception ex)
            {
                Loggers.Exceptionlog(ex);
            }
        }

        private void timer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            try
            {
                Loggers.Sequencelog("Method : timer_Elapsed :  step -1");
                timer.Stop();
                timer.Enabled = false;

                GetSystemInfo();
                timer.Interval = 60000;
                Loggers.Sequencelog("Method : timer_Elapsed :  step -2");
            }
            catch (Exception ex)
            {
                Loggers.Exceptionlog(ex);
                timer.Start();
            }
            finally
            {
                timer.Start();
                timer.Enabled = true;
            }
        }

        public void GetSystemInfo()
        {
            try
            {
                Loggers.Sequencelog("Get System Info Called");
                SystemInfo sys_Info = new SystemInfo();
                sys_Info.SystemName = string.Empty;
                sys_Info.MACAddress = string.Empty;
                sys_Info.IPAddress = string.Empty;
                sys_Info.CPU = 0;
                sys_Info.Memory = 0;
                sys_Info.DriveC = 0;
                sys_Info.DriveD = 0;
                sys_Info.DriveE = 0;

                sys_Info.SystemName = System.Environment.MachineName;
                sys_Info.MACAddress = GetMacAddress();
                sys_Info.IPAddress = GetLocalIPAddress();
                sys_Info.CPU = GetCpuUtilization();
                sys_Info.Memory = GetRamUtilization();
                //GetDriveInfo();
                BusinessLayer objBusiness = new BusinessLayer();
                objBusiness.InsertSystemInfo(sys_Info);
            }
            catch(Exception ex)
            {
                Loggers.Exceptionlog(ex);
            }
        }

        private int GetCpuUtilization()
        {
            try
            {
                PerformanceCounter cpuCounter = new PerformanceCounter();
                cpuCounter.CategoryName = "Processor";
                cpuCounter.CounterName = "% Processor Time";
                cpuCounter.InstanceName = "_Total";
                dynamic firstValue = cpuCounter.NextValue();
                System.Threading.Thread.Sleep(1000);
                dynamic secondValue = cpuCounter.NextValue();

                return (int)secondValue;
            }
            catch(Exception ex)
            {
                Loggers.Exceptionlog(ex);
                return 0;
            }
        }

        private int GetRamUtilization()
        {
            try
            {
                PerformanceCounter performanceCounterRAM = new PerformanceCounter();
                performanceCounterRAM.CounterName = "% Committed Bytes In Use";
                performanceCounterRAM.CategoryName = "Memory";
                return (int)performanceCounterRAM.NextValue();
            }
            catch(Exception ex)
            {
                Loggers.Exceptionlog(ex);
                return 0;
            }
        }

        private string GetLocalIPAddress()
        {
            try
            {
                var host = Dns.GetHostEntry(Dns.GetHostName());
                foreach (var ip in host.AddressList)
                {
                    if (ip.AddressFamily == AddressFamily.InterNetwork)
                    {
                        return ip.ToString();
                    }
                }
                throw new Exception("No network adapters with an IPv4 address in the system!");
            }
            catch(Exception ex)
            {
                Loggers.Exceptionlog(ex);
                return string.Empty;
            }
        }

        private string GetMacAddress()
        {
            try
            {
                string macAddresses = string.Empty;

                foreach (NetworkInterface nic in NetworkInterface.GetAllNetworkInterfaces())
                {
                    if (nic.OperationalStatus == OperationalStatus.Up)
                    {
                        macAddresses += nic.GetPhysicalAddress().ToString();
                        break;
                    }
                }
                return macAddresses;
            }
            catch(Exception ex)
            {
                Loggers.Exceptionlog(ex);
                return string.Empty;
            }
        }

        private void GetDriveInfo()
        {
            long Cdrivememory;
            long Ddrivememory;
            long Edrivememory;
            DriveInfo[] drives = DriveInfo.GetDrives();
            foreach (DriveInfo drive in drives)
            {
                //There are more attributes you can use.
                //Check the MSDN link for a complete example.
                if (drive.Name == "C:\\")
                {
                    if (drive.IsReady)
                        Console.WriteLine(drive.TotalSize);
                    Cdrivememory = drive.AvailableFreeSpace;
                }
                if (drive.Name == "D:\\")
                {
                    if (drive.IsReady)
                        Console.WriteLine(drive.TotalSize);
                    Ddrivememory = drive.AvailableFreeSpace;
                }
                if (drive.Name == "E:\\")
                {
                    if (drive.IsReady)
                        Console.WriteLine(drive.TotalSize);
                    Edrivememory = drive.AvailableFreeSpace;
                }
            }
        }
    }
}
