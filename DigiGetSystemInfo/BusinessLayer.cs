﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DigiGetSystemInfo
{
    class BusinessLayer
    {
        public bool InsertSystemInfo(SystemInfo sInfo)
        {
            bool result = false;
            DataAccessLayer objDAL = new DataAccessLayer();
            result = objDAL.InsertSystemInfo(sInfo);
            return result;
        }
    }

    public class SystemInfo
    {
        public string SystemName { get; set; }
        public string MACAddress { get; set; }
        public string IPAddress { get; set; }
        public int CPU { get; set; }
        public int Memory { get; set; }
        public int DriveC { get; set; }
        public int DriveD { get; set; }
        public int DriveE { get; set; }
    }
}
