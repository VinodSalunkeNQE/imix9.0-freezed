﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Net.Mail;
using System.Net;
using System.Data.SqlClient;
using System.Configuration;
using System.IO;
using DigiPhoto.DigiSync.Utility;
using DigiPhoto.IMIX.Model;
using DigiPhoto.IMIX.Business;
using System.Runtime.InteropServices;

namespace EmailService
{
    public partial class Service1 : ServiceBase
    {
        #region Declaration
        int OrderStatus = 1;
        DataTable dtEmailDetail = new DataTable();
        public string DGconn = ConfigurationManager.ConnectionStrings["DigiConnectionString"].ConnectionString;
        SqlConnection cn;
        EmailBusniess emailBusiness = new EmailBusniess();
        private System.Timers.Timer timer;
        string emailfolderpath;
        string mailfrom;
        string mailsubject;
        string mailbody;
        string smtpservername;
        int smtpserverport;
        bool UseDefaultCredentials;
        string username;
        string password;
        bool enableSSL;
        string emailImgPath;
        private string _errorMsg = string.Empty;
        string bcc;
        int runstatus = 0;
        string logorderno;
        string LogEmailOrderno;
        #endregion

        #region Constructor
        /// <summary>
        /// Initializes a new instance of the <see cref="Service1"/> class.
        /// </summary>
        public Service1()
        {
            InitializeComponent();

            try
            {
                cn = new SqlConnection(DGconn);

            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }
        #endregion

        #region Events
        /// <summary>
        /// When implemented in a derived class, executes when a Start command is sent to the service by the Service Control Manager (SCM) or when the operating system starts (for a service that starts automatically). Specifies actions to take when the service starts.
        /// </summary>
        /// <param name="args">Data passed by the start command.</param>
        protected override void OnStart(string[] args)
        {
            //System.Diagnostics.Debugger.Launch();
            //System.Diagnostics.Debugger.Break();
            try
            {

                ServicePosInfoBusiness svcPosinfoBusiness = new ServicePosInfoBusiness();
                string ret = svcPosinfoBusiness.ServiceStart(false);
                //ret = 1;
                if (string.IsNullOrEmpty(ret))
                {
                GetEmailSettingsDetails();
                this.timer = new System.Timers.Timer();
                this.timer.Interval = 1000 * 60 * 0.5;
                this.timer.AutoReset = true;
                this.timer.Elapsed += new System.Timers.ElapsedEventHandler(this.timer_Elapsed);
                this.timer.Start();
                CreateColumn();
            }
                else
            {
                    throw new Exception("Already Started");
            }
            }
            catch (Exception ex)
            {
                this.ExitCode = 13816;
                this.Stop();
            }
        }

        /// <summary>
        /// Handles the Elapsed event of the timer control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Timers.ElapsedEventArgs"/> instance containing the event data.</param>
        private void timer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            try
            {
                this.timer.Stop();
                GetEmailSettingsDetails();
                GetEmailDetailsNew();
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
            finally
            {
                this.timer.Start();
            }
        }
        protected override void OnStop()
        {
            ServicePosInfoBusiness svcPosinfoBusiness = new ServicePosInfoBusiness();
            svcPosinfoBusiness.StopService(false);
        }
        #endregion

        #region Common Functions
        void CreateColumn()
        {
            dtEmailDetail.Columns.Add("PhotoId", typeof(long));
            dtEmailDetail.Columns.Add("Status", typeof(int));
            dtEmailDetail.Columns.Add("Error", typeof(string));
        }

        private void GetEmailDetailsNew()
        {
            try
            {
                List<EmailDetailInfo> lstEmailDetail = emailBusiness.GetEmailDetails();
                string OldOrderNo = string.Empty;
                if (lstEmailDetail != null && lstEmailDetail.Count > 0)
                {
                    int count = 1;
                    foreach (EmailDetailInfo emailDetail in lstEmailDetail)
                        {
                            _errorMsg = string.Empty;

                            if (emailDetail.DG_MessageType.ToLower().Equals("backup failed"))
                                {
                            string[] moreMessage = emailDetail.DG_OtherMessage.Split('{');
                            var status = SendBackUpMail(emailDetail.DG_Email_To, emailDetail.DG_EmailSender, emailDetail.DG_Message, emailDetail.EmailTemplate, emailDetail.DG_OtherMessage, logorderno, LogEmailOrderno);
                                    if (status == 5)
                                    {
                                        runstatus = 5;
                                        break;
                                    }
                                    else
                                    {
                                UpdateBackUpEmailStatus(emailDetail.EmailKey, status, out _errorMsg);
                                    }
                                }
                            else if (emailDetail.DG_MessageType.ToUpper().Equals("RFID FLUSH"))
                            {
                                string[] moreMessage = emailDetail.DG_OtherMessage.Split('{');
                                var status = SendRFIDFlushMail(emailDetail.DG_Email_To, emailDetail.DG_EmailSender, emailDetail.DG_Message, emailDetail.EmailTemplate, emailDetail.DG_OtherMessage, logorderno, LogEmailOrderno);
                                if (status == 5)
                                {
                                    runstatus = 5;
                                    break;
                                }
                                else
                                {
                                    UpdateBackUpEmailStatus(emailDetail.EmailKey, status, out _errorMsg);
                                }
                            }
                        else if (emailDetail.DG_MessageType.ToUpper().Equals("DOC"))
                                {
                                    #region
                            string[] moreMessage = emailDetail.DG_OtherMessage.Split('{');
                                    string msg = moreMessage.Length > 0 ? moreMessage[0] : string.Empty;
                            string EmailTemplate = emailDetail.DG_ReportMailBody;
                            logorderno = emailDetail.OrderId;
                            var MailStatus = SendMail(mailfrom, emailDetail.DG_Email_To, emailDetail.DG_Email_Bcc, emailDetail.DG_EmailSubject, EmailTemplate.Replace("{Message}", emailDetail.DG_Message + "<br>" + msg).Replace("{Sendername}", emailDetail.DG_EmailSender), emailDetail.OrderId, smtpservername, smtpserverport, UseDefaultCredentials, username, password, enableSSL, logorderno, LogEmailOrderno);
                                    if (MailStatus == 5)
                                    {
                                        runstatus = 5;
                                        break;
                                    }
                                    else
                                    {
                                UpdateEmailDetails(emailDetail.OrderId, MailStatus, dtEmailDetail);
                                    }
                                    #endregion
                                }
                        else if (emailDetail.DG_MessageType.ToUpper().Equals("CSV EXPORT SERVICE EXCEPTION"))
                        {
                            string[] moreMessage = emailDetail.DG_OtherMessage.Split('{');
                            var status = SendExportReportFailedMail(emailDetail.DG_Email_To, emailDetail.DG_EmailSender, emailDetail.DG_Message, emailDetail.EmailTemplate, emailDetail.DG_OtherMessage, logorderno, LogEmailOrderno);
                            if (status == 5)
                            {
                                runstatus = 5;
                                break;
                            }
                            else
                            {
                                UpdateBackUpEmailStatus(emailDetail.EmailKey, status, out _errorMsg);
                            }
                        }
                                else
                                {
                            logorderno = emailDetail.OrderId;
                            LogEmailOrderno = emailDetail.EmailDetailId_Pkey.ToString();
                            string orderno = logorderno;
                                        if (orderno.IndexOf("DG-", StringComparison.OrdinalIgnoreCase) < 0)
                                        {
                                            orderno = "DG-" + orderno;
                                        }
                                        emailfolderpath = Path.Combine(GetHotFolderPath(orderno), "PrintImages", "Email");
                                        int status = 2;
                                        if (string.IsNullOrEmpty(OldOrderNo) || string.Compare(OldOrderNo, orderno, true) != 0)
                                        {
                                            if (string.IsNullOrEmpty(OldOrderNo) == false && string.Compare(OldOrderNo, orderno, true) != 0)
                                                UpdateEmailDetails(OldOrderNo, OrderStatus, dtEmailDetail);
                                            OldOrderNo = orderno;
                                            dtEmailDetail.Rows.Clear();
                                            OrderStatus = 1;
                            }
                            if (emailDetail.PhotoId == 0)
                            {
                                string errorMessage = "For Email Detail Id: " + LogEmailOrderno + " and Order Number: " + logorderno + "The photoId is coming as 0";
                                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                            }
                            string photoId = Convert.ToString(emailDetail.PhotoId);

                                        if (string.IsNullOrEmpty(photoId) == false)
                                        {
                                status = GoPhotoEmail(orderno, emailDetail.DG_Email_To, emailDetail.DG_EmailSender, emailDetail.DG_Message, emailDetail.EmailTemplate, emailDetail.message, photoId);

                                            if (status == 5)
                                            {
                                                runstatus = 5;
                                                break;
                                            }
                                            else
                                            {
                                                DataRow dtRow = dtEmailDetail.NewRow();
                                                dtRow["PhotoId"] = photoId;
                                                dtRow["Status"] = status;
                                                dtRow["Error"] = _errorMsg;
                                                dtEmailDetail.Rows.Add(dtRow);
                                                if (status != 1)
                                                    OrderStatus = 2;
                                            }
                                        }
                                    }
                        if (count == lstEmailDetail.Count)
                            UpdateEmailDetails(OldOrderNo, OrderStatus, dtEmailDetail);
                        count++;
                                }
                    if (runstatus == 5)
                    {
                        if (cn.State == ConnectionState.Open)
                        {
                            cn.Close();
                        }
                        cn = new SqlConnection(DGconn);
                        GetEmailSettingsDetails();
                        this.timer.Stop();
                        this.timer.Start();
                        dtEmailDetail = new DataTable();
                        CreateColumn();
                        runstatus = 0;
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }

        private int SendBackUpMail(string mailto, string sendername, string msgbody, string EmailTemplate, string OtherMessage, string logorderno, string LogEmalOrderno)
        {
            int ret = 0;
            try
            {
                string[] moreMessage = OtherMessage.Split('{');
                string msg = moreMessage.Length > 0 ? moreMessage[0] : string.Empty;
                string subject = "BackUp Failed";
                //only here we are sending true and false
                ret = SendMail(mailfrom, mailto, bcc, subject, EmailTemplate.Replace("{Message}", msgbody + "<br>" + msg).Replace("{Sendername}", sendername), null, smtpservername, smtpserverport, UseDefaultCredentials, username, password, enableSSL, logorderno, LogEmailOrderno);
                return ret;
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                return ret;
            }
        }

        /// <summary>
        /// Sends the mail.
        /// </summary>
        /// <param name="from">From.</param>
        /// <param name="to">The automatic.</param>
        /// <param name="Bcc">The BCC.</param>
        /// <param name="subject">The subject.</param>
        /// <param name="body">The body.</param>
        /// <param name="attachment">The attachment.</param>
        /// <param name="smtpservername">The smtpservername.</param>
        /// <param name="smtpportno">The smtpportno.</param>
        /// <param name="defaultcredentials">if set to <c>true</c> [defaultcredentials].</param>
        /// <param name="uname">The uname.</param>
        /// <param name="password">The password.</param>
        /// <param name="enabelSSl">if set to <c>true</c> [enabel arguments sl].</param>
        private int SendMail(string from, string to, string Bcc, string subject, string body, string attachment, string smtpservername, int smtpportno, bool defaultcredentials, string uname, string password, bool enabelSSl, string logorderno, string LogEmalOrderno)
        {
            int MailStatus = 0;
            try
            {
                //here we will close the db connection and reset the timer  
                if (String.IsNullOrWhiteSpace(from) || String.IsNullOrWhiteSpace(to))
                {
                    MailStatus = 5;
                }
                else
                {
                    MailAddress mailfrom = new MailAddress(from);
                    MailAddress mailto = new MailAddress(to);
                    MailMessage newmsg = new MailMessage(mailfrom, mailto);
                    if (Bcc != "" && Bcc != null)
                    {
                        MailAddress bcc = null;
                        foreach (string bc in Bcc.Split(','))
                        {
                            bcc = new MailAddress(bc);
                            newmsg.Bcc.Add(bcc);
                        }
                    }
                    newmsg.Subject = subject;
                    newmsg.Body = body;
                    newmsg.IsBodyHtml = true;
                    newmsg.Priority = MailPriority.High;

                    //For File Attachment, more file can also be attached
                    if (attachment != null)
                    {
                        Attachment att = new Attachment(attachment);
                        att.TransferEncoding = System.Net.Mime.TransferEncoding.Base64;
                        newmsg.Attachments.Add(att);
                    }

                    SmtpClient smtp = new SmtpClient(smtpservername);
                    smtp.Port = smtpportno;
                    smtp.UseDefaultCredentials = defaultcredentials;
                    int retryCount = Convert.ToInt32(ConfigurationManager.AppSettings["MailRetryCount"].ToString());
                    for (int a = 1; a <= retryCount; a++)
                    {
                        if (SendToSMTP(defaultcredentials, uname, password, enabelSSl, newmsg, smtp, logorderno, LogEmailOrderno))
                        {
                            MailStatus = 1;
                            break;
                        }
                        else if (a == retryCount)
                        {
                            MailStatus = 2;
                        }
                    }
                }
                return MailStatus;
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                _errorMsg = errorMessage;
                return MailStatus = 2;
            }
        }

        private bool SendToSMTP(bool defaultcredentials, string uname, string password, bool enabelSSl, MailMessage newmsg, SmtpClient smtp, string logorderno, string LogEmalOrderno)
        {
            try
            {
                if (PingTest())
                {
                    if (!defaultcredentials)
                    {
                        smtp.Credentials = new NetworkCredential(uname, password);
                        smtp.EnableSsl = enabelSSl;
                        smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
                        smtp.Timeout = 700000;
                        smtp.Send(newmsg);
                    }
                    else
                    {
                        smtp.EnableSsl = enabelSSl;
                        smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
                        smtp.Timeout = 700000;
                        smtp.Send(newmsg);
                    }
                    return true;
                }
                else
                {
                    ErrorHandler.ErrorHandler.LogFileWrite("Ping Test Failed. Now trying emailing without Ping Test.");
                    try
                    {
                        if (!defaultcredentials)
                        {
                            smtp.Credentials = new NetworkCredential(uname, password);
                            smtp.EnableSsl = enabelSSl;
                            smtp.Timeout = 700000;
                            smtp.Send(newmsg);
                        }
                        else
                        {
                            smtp.EnableSsl = enabelSSl;
                            smtp.Timeout = 700000;
                            smtp.Send(newmsg);
                        }
                        return true;
                    }
                    catch (Exception ex)
                    {
                        string msg = "Internet may not be working fine or check the below log for any issue for OrderId: " + logorderno + " and Email Detail ID: " + LogEmailOrderno;
                        ErrorHandler.ErrorHandler.LogFileWrite(logorderno != null ? msg : string.Empty);
                        string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                        ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                        _errorMsg = errorMessage;
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {
                string msg = "Error occured for OrderId " + logorderno + " Email Detail ID: " + LogEmailOrderno;
                ErrorHandler.ErrorHandler.LogFileWrite(logorderno != null ? msg : string.Empty);
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                _errorMsg = errorMessage;
                return false;
            }
        }

        private int GoPhotoEmail(string OrderId, string mailto, string sendername, string msgbody, string EmailTemplate, string OtherMessage, string PhotoId)
        {
            int MailStatus = 2;
            string FilePath = string.Empty;
            string items = System.IO.Path.Combine(emailfolderpath, OrderId, PhotoId + ".jpg");
            try
            {
                int x = 0;
                string filename = System.IO.Path.GetFileName(items);
                if (filename != "Thumbs.db")
                {
                    FilePath = items;
                    string DirectoryNameWithOrderId = "";
                    FileInfo emailFile = new FileInfo(items);
                    if (emailFile.Length > 2.8 * 1024 * 1024)
                    {
                        DirectoryNameWithOrderId = Path.Combine(GetHotFolderPath(OrderId), "PrintImages", "Email", OrderId, "CompressedImages");
                        if (!Directory.Exists(DirectoryNameWithOrderId))
                            Directory.CreateDirectory(DirectoryNameWithOrderId);
                        //Calculate imageCompression

                        decimal imageRatio = decimal.Divide(emailFile.Length, Convert.ToDecimal(2.8 * 1024 * 1024));
                        decimal imageCompression = 100 / imageRatio;
                        imageCompression = Math.Round(imageCompression);
                        FilePath = Path.Combine(DirectoryNameWithOrderId, filename);

                        ImageCompress imgCompress = ImageCompress.GetImageCompressObject;
                        imgCompress.GetImage = new System.Drawing.Bitmap(items);
                        imgCompress.Height = Convert.ToInt32(imgCompress.GetImage.Height * (imageCompression / 100));
                        imgCompress.Width = Convert.ToInt32(imgCompress.GetImage.Width * (imageCompression / 100));
                        imgCompress.Save(filename, DirectoryNameWithOrderId);

                        //code to compress the email untill its size is reduce to 3 MB

                        string imagefile = System.IO.Path.Combine(DirectoryNameWithOrderId, filename);
                        string getCompressedFile = System.IO.Path.GetFileName(imagefile);
                        FileInfo compressedEmailFile = new FileInfo(imagefile);
                        double a = 2.8;

                        while (compressedEmailFile.Length > 2.8 * 1024 * 1024)
                        {
                            double divisor = a * 1024 * 1024;
                            imageRatio = decimal.Divide(emailFile.Length, Convert.ToDecimal(divisor));
                            imageCompression = 100 / imageRatio;
                            imageCompression = Math.Round(imageCompression);
                            FilePath = Path.Combine(DirectoryNameWithOrderId, filename);

                            imgCompress = ImageCompress.GetImageCompressObject;
                            imgCompress.GetImage = new System.Drawing.Bitmap(items);
                            imgCompress.Height = Convert.ToInt32(imgCompress.GetImage.Height * (imageCompression / 100));
                            imgCompress.Width = Convert.ToInt32(imgCompress.GetImage.Width * (imageCompression / 100));
                            imgCompress.Save(filename, DirectoryNameWithOrderId);
                            a = a - 0.2;
                            imagefile = System.IO.Path.Combine(DirectoryNameWithOrderId, filename);
                            getCompressedFile = System.IO.Path.GetFileName(imagefile);
                            compressedEmailFile = new FileInfo(imagefile);
                        }
                    }
                    string msg = OtherMessage;// moreMessage.Length > x ? moreMessage[x] : string.Empty;
                    MailStatus = SendMail(mailfrom, mailto, bcc, mailsubject, EmailTemplate.Replace("{Message}", msgbody + "<br>" + msg).Replace("{Sendername}", sendername), FilePath, smtpservername, smtpserverport, UseDefaultCredentials, username, password, enableSSL, logorderno, LogEmailOrderno);
                }
                x++;
                return MailStatus;
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                _errorMsg = errorMessage;
                return MailStatus;
            }
        }

        /// <summary>
        /// Gets the hot folder path.
        /// </summary>
        /// <returns></returns>
        private string GetHotFolderPath(string OrderNumber)
        {
            try
            {
                cn = new SqlConnection(DGconn);
                SqlCommand cmd = new SqlCommand("GetHotFolderPathsOrderWise", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@OrderNumber", OrderNumber);
                cn.Open();
                return cmd.ExecuteScalar().ToString();
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                return null;
            }
            finally
            {
                if (cn.State == ConnectionState.Open)
                {
                    cn.Close();
                }
            }
        }

        /// <summary>
        /// Updates the email details.
        /// </summary>
        /// <param name="OrderId">The order unique identifier.</param>
        private void UpdateEmailDetails(string OrderId, int Status, DataTable dtEmailDetail)
        {
            try
            {
                if (cn.State == ConnectionState.Closed)
                    cn.Open();
                SqlCommand cmd = new SqlCommand("UpdateEmailDetails", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@OrderId", OrderId);
                cmd.Parameters.AddWithValue("@Status", Status);
                cmd.Parameters.AddWithValue("@EmailDetail", dtEmailDetail);
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
            finally
            {
                if (cn.State == ConnectionState.Open)
                {
                    cn.Close();
                }
            }
        }

        private void UpdateBackUpEmailStatus(int EmailID, int Status, out string _errorMsg)
        {
            try
            {
                if (cn.State == ConnectionState.Closed)
                    cn.Open();
                SqlCommand cmd = new SqlCommand("UpdateBackUPEmailStatus", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Email_Pkey", EmailID);
                cmd.Parameters.AddWithValue("@Status", Status);
                cmd.ExecuteNonQuery();
                _errorMsg = "Success";
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                _errorMsg = errorMessage;
            }
            finally
            {
                if (cn.State == ConnectionState.Open)
                {
                    cn.Close();
        }
            }
        }

        /// <summary>
        /// Gets the email settings details.
        /// </summary>
        private void GetEmailSettingsDetails()
        {
            try
            {
                SqlCommand cmd = new SqlCommand("GetEmailSettingDetails", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cn.Open();
                using (var reader = cmd.ExecuteReader())
                {
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            if (!reader.IsDBNull(0))
                            {
                                mailfrom = reader.GetString(0);
                                mailsubject = reader.GetString(1);
                                mailbody = reader.GetString(2);
                                smtpservername = reader.GetString(3);
                                smtpserverport = Convert.ToInt32(reader.GetString(4));
                                UseDefaultCredentials = reader.GetBoolean(5);
                                username = reader.GetString(6);
                                password = reader.GetString(7);
                                enableSSL = reader.GetBoolean(8);
                                bcc = reader.GetString(9);
                            }
                        }
                    }
                    reader.Close();
                }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
            finally
            {
                if (cn.State == ConnectionState.Open)
                {
                    cn.Close();
                }
            }
        }
        
        public bool PingTest()
        {
            try
            {
                System.Net.NetworkInformation.Ping ping = new System.Net.NetworkInformation.Ping();
                System.Net.NetworkInformation.PingReply pingStatus = ping.Send("www.google.com", 1000);

                if (pingStatus.Status == System.Net.NetworkInformation.IPStatus.Success)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                return false;
            }
        }
        #endregion
        private int SendExportReportFailedMail(string mailto, string sendername, string msgbody, string EmailTemplate, string OtherMessage, string logorderno, string LogEmalOrderno)
        {
            int ret = 0;
            try
            {
                string[] moreMessage = OtherMessage.Split('{');
                string msg = moreMessage.Length > 0 ? moreMessage[0] : string.Empty;
                string subject = "Export Report Service Failure Report";
                //only here we are sending true and false
                ret = SendMail(mailfrom, mailto, bcc, subject, EmailTemplate.Replace("{Message}", msgbody + "<br>" + msg).Replace("{Sendername}", sendername), null, smtpservername, smtpserverport, UseDefaultCredentials, username, password, enableSSL, logorderno, LogEmailOrderno);
                return ret;
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                return ret;
            }
        }
        private int SendRFIDFlushMail(string mailto, string sendername, string msgbody, string EmailTemplate, string OtherMessage, string logorderno, string LogEmalOrderno)
        {
            int ret = 0;
            try
            {
                string[] moreMessage = OtherMessage.Split('{');
                string msg = moreMessage.Length > 0 ? moreMessage[0] : string.Empty;
                string subject = "RFID FLUSH";
                //only here we are sending true and false
                ret = SendMail(mailfrom, mailto, bcc, subject, EmailTemplate.Replace("{Message}", msgbody + "<br>" + msg).Replace("{Sendername}", sendername), null, smtpservername, smtpserverport, UseDefaultCredentials, username, password, enableSSL, logorderno, LogEmailOrderno);
                return ret;
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                return ret;
            }
        }
        
    }
}
