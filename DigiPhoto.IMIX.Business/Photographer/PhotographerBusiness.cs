﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using DigiPhoto.DataLayer.Model;
using System.Xml.Linq;
using System.Data;
using System.ComponentModel;
using System.Collections;
using System.Reflection;
using System.Globalization;
using DigiPhoto.IMIX.Model;
using DigiPhoto.IMIX.DataAccess;
using DigiPhoto.IMIX.Business;
using DigiPhoto.DataLayer;

namespace DigiPhoto.IMIX.Business
{
    public partial class PhotographerBusiness : BaseBusiness
    {
        public bool Update_GrpCount(Int32 userID)
        {
            try
            {
                bool result = false;
                this.operation = () =>
                {
                    PhotographerDao access = new PhotographerDao(this.Transaction);

                    result= access.UPD_GrpCount(userID);
                };
                this.Start(false);
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
    }
}
