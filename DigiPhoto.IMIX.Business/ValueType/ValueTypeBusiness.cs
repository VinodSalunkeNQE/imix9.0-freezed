﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using DigiPhoto.DataLayer.Model;
using System.Xml.Linq;
using System.Data;
using System.ComponentModel;
using System.Collections;
using System.Reflection;
using System.Globalization;
using DigiPhoto.IMIX.Model;
using DigiPhoto.IMIX.DataAccess;
using DigiPhoto.IMIX.Business;



namespace DigiPhoto.IMIX.Business
{
    public class ValueTypeBusiness : BaseBusiness
    {
        public List<ValueTypeInfo> GetReasonType(int valueTypeGroupId)
        {
            try
            {
                List<ValueTypeInfo> valueTypeInfoList = new List<ValueTypeInfo>();
                this.operation = () =>
                {
                    ValueTypeDao access = new ValueTypeDao(this.Transaction);
                    valueTypeInfoList = access.SelectValueTypeInfoList(valueTypeGroupId);
                };
                this.Start(false);
                return valueTypeInfoList;

            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogFileWrite(ErrorHandler.ErrorHandler.CreateErrorMessage(ex));
                return null;
            }
        }
        public List<ValueTypeInfo> GetScanTypes()
        {
            try
            {
                List<ValueTypeInfo> objDic = null;
                this.operation = () =>
                {
                    ValueTypeDao access = new ValueTypeDao(this.Transaction);
                    objDic = access.GetScanTypes();

                }; this.Start(false);
                return objDic;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
