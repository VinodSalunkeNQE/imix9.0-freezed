﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using DigiPhoto.DataLayer.Model;
using System.Xml.Linq;
using System.Data;
using System.ComponentModel;
using System.Collections;
using System.Reflection;
using System.Globalization;
using DigiPhoto.IMIX.Model;
using DigiPhoto.IMIX.DataAccess;
using DigiPhoto.IMIX.Business;
using DigiPhoto.DataLayer;
using DigiPhoto.Utility.Repository.ValueType;
namespace DigiPhoto.IMIX.Business
{
    public partial class LocationBusniess : BaseBusiness
    {
        public bool DeleteLocations(int LocationId)
        {          
                bool result = false;
                this.operation = () =>
                {
                    LocationDao access = new LocationDao(this.Transaction);
                    result = access.Delete(LocationId);
                };
                this.Start(false);
                return result;                   
        }
        public int GetLocationIdbyUserId(string UserId)
        {
            try
            {
                int userId = UserId.ToInt32();
                UsersInfo objectInfo = new UsersInfo();
                this.operation = () =>
                {
                    UsersDao access = new UsersDao(this.Transaction);
                    objectInfo = access.GetLocationIdbyUserId(userId);
                };
                this.Start(false);
                return objectInfo.DG_Location_ID;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }
        public int GetSubStoreIdbyLocationId(int LocationId)
        {
            try
            {
                SubStoreLocationInfo objectInfo = new SubStoreLocationInfo();
                this.operation = () =>
                {
                    LocationDao access = new LocationDao(this.Transaction);
                    objectInfo = access.SelectSubStoreLocationsByLocationId(LocationId);
                };
                this.Start(false);
                return objectInfo.DG_SubStore_ID;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }
        public string GetQRCodeWebUrl()
        {
            try
            {
                StoreInfo objectInfo = new StoreInfo();
                this.operation = () =>
                {
                    StoreDao access = new StoreDao(this.Transaction);
                    objectInfo = access.GETQRCodeWebUrl();
                };
                this.Start(false);
                return objectInfo.DG_QRCodeWebUrl;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<LocationInfo> GetLocationName(int StoreId)
        {
            List<LocationInfo> objectListInfo = new List<LocationInfo>();
            this.operation = () =>
            {
                LocationDao access = new LocationDao(this.Transaction);
                objectListInfo = access.GetLocationStoreWise(StoreId);
            };
            this.Start(false);
            return objectListInfo;
        }

        public Dictionary<string, string> GetLocationNameDir(int StoreId)
        {
            Dictionary<string, string> objectListInfo = new Dictionary<string, string>();
            this.operation = () =>
            {
                LocationDao access = new LocationDao(this.Transaction);
                objectListInfo = access.GetLocationStoreWiseDir(StoreId);
            };
            this.Start(false);
            return objectListInfo;
        }

        public List<SubStoresInfo> GetSubstoreData()
        {

            List<SubStoresInfo> SubStoresList = new List<SubStoresInfo>();
            this.operation = () =>
            {
                SubStoreDao access = new SubStoreDao(this.Transaction);
                SubStoresList = access.GetSubstoreData();
            };
            this.Start(false);
            return SubStoresList;


        }

        public List<LocationInfo> GetLocationList(int storeId)
        {
            List<LocationInfo> objectListInfo = new List<LocationInfo>();
            this.operation = () =>
            {
                LocationDao access = new LocationDao(this.Transaction);
                objectListInfo = access.GetLocationList(storeId, true);
            };
            this.Start(false);
            return objectListInfo;
        }
        public LocationInfo GetLocationsbyId(int LocationId)
        {

            LocationInfo objectInfo = new LocationInfo();
            this.operation = () =>
            {
                LocationDao access = new LocationDao(this.Transaction);
                objectInfo = access.Get(LocationId);
            };
            this.Start(false);
            return objectInfo;
        }
        public bool SetLocations(int LocationId, string LocationName, int StoreId, string SyncCode)
        {

            LocationInfo objInfo = new LocationInfo();
            objInfo.DG_Location_pkey = LocationId;
            objInfo.DG_Location_Name = LocationName;
            objInfo.DG_Store_ID = StoreId;
            objInfo.DG_Location_IsActive = true;
            objInfo.SyncCode = SyncCode;
            objInfo.IsSynced = false;
            int locid = 0;
            if (LocationId <= 0)
            {
                this.operation = () =>
                {
                    LocationDao access = new LocationDao(this.Transaction);
                   locid= access.Add(objInfo);
                };
                this.Start(false);
            }
            else
            {
                objInfo.DG_Location_pkey = LocationId;
                objInfo.DG_Location_Name = LocationName;
                objInfo.IsSynced = false;
                this.operation = () =>
                {
                    LocationDao access = new LocationDao(this.Transaction);
                   locid= access.Update(objInfo);
                };
                this.Start(false);
            }

            if (locid > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
            
        }
        public List<LocationInfo> GetLocations(int storeId)
        {

            List<LocationInfo> objectList = new List<LocationInfo>();
            this.operation = () =>
            {
                LocationDao access = new LocationDao(this.Transaction);
                objectList = access.GetLocationList(storeId, true);
            };
            this.Start(false);
            return objectList;
        }

        public bool IsLocationAssociatedToSite(int _locationId)
        {
            try
            {
                bool issucess = false;
                this.operation = () =>
                {
                    LocationDao access = new LocationDao(this.Transaction);
                    issucess = access.IsLocationAssociatedToSite(_locationId);
                };
                this.Start(false);
                return issucess;
            }
            catch
            {
                return false;
            }
        }

        public bool IsSiteAssociatedToLocation(int subStoreId)
        {
            try
            {
                bool issucess = false;
                this.operation = () =>
                {
                    LocationDao access = new LocationDao(this.Transaction);
                    issucess = access.IsSiteAssociatedToLocation(subStoreId);
                };
                this.Start(false);
                return issucess;
            }
            catch
            {
                return false;
            }
        }
    }
}
