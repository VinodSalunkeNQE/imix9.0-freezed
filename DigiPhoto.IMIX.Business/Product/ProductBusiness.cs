﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
//using DigiPhoto.DataLayer.Model;
using System.Xml.Linq;
using System.Data;
using System.ComponentModel;
using System.Collections;
using System.Reflection;
using System.Globalization;
using DigiPhoto.IMIX.Model;
using DigiPhoto.IMIX.DataAccess;
using DigiPhoto.IMIX.Business;
using DigiPhoto.DataLayer;
using DigiPhoto.Utility.Repository.ValueType;

namespace DigiPhoto.IMIX.Business
{

    public class ProductBusiness : BaseBusiness
    {
        private DataColumn SetColumn(string columnName, string dataType, bool IsNullable)
        {
            DataColumn column = new DataColumn("DG_Orders_ProductCode", System.Type.GetType("System.String"));
            column.AllowDBNull = true;
            return column;
        }
        private DataTable ProductDataTable()
        {
            DataTable table = new DataTable();
            table.Columns.Add("DG_Orders_ProductCode", typeof(string));
            table.Columns.Add("DG_Orders_ProductType_Desc", typeof(string));
            table.Columns.Add("DG_Orders_ProductType_Name", typeof(string));
            table.Columns.Add("DG_IsAccessory", typeof(bool));
            table.Columns.Add("DG_IsActive", typeof(bool));
            table.Columns.Add("DG_IsPackage", typeof(bool));
            table.Columns.Add("DG_Orders_ProductType_DiscountApplied", typeof(bool));
            table.Columns.Add("DG_MaxQuantity", typeof(int));
            table.Columns.Add("DG_Orders_ProductType_Image", typeof(string));
            table.Columns.Add("SyncCode", typeof(string));
            table.Columns.Add("IsSynced", typeof(int));
            table.Columns.Add("DG_Currency_Symbol", typeof(string));
            table.Columns.Add("DG_Product_Pricing_ProductPrice", typeof(double));
            return table;
        }

        public bool BulkSaveProduct(DataTable productDataTable, int createdBy, int storeId)
        {
            DataTable product = new DataTable();
            product.Columns.Add("DG_Orders_ProductCode", typeof(string));
            product.Columns.Add("DG_Orders_ProductType_Desc", typeof(string));
            product.Columns.Add("DG_Orders_ProductType_Name", typeof(string));
            product.Columns.Add("DG_IsAccessory", typeof(bool));
            product.Columns.Add("DG_IsActive", typeof(bool));
            product.Columns.Add("DG_IsPackage", typeof(bool));
            product.Columns.Add("DG_Orders_ProductType_DiscountApplied", typeof(bool));
            product.Columns.Add("DG_MaxQuantity", typeof(int));
            product.Columns.Add("DG_Orders_ProductType_Image", typeof(string));
            product.Columns.Add("SyncCode", typeof(string));
            product.Columns.Add("IsSynced", typeof(int));
            product.Columns.Add("DG_Currency_Symbol", typeof(string));
            product.Columns.Add("DG_Product_Pricing_ProductPrice", typeof(double));

            try
            {
                if (productDataTable.Rows[0][3] is System.DBNull)
                    productDataTable.Rows.Remove(productDataTable.Rows[0]);
                foreach (DataRow dataRow in productDataTable.Rows)
                {
                    DataRow newDataRow = product.NewRow();
                    newDataRow["DG_Orders_ProductCode"] = dataRow.ItemArray[1].ToString();
                    newDataRow["DG_Orders_ProductType_Desc"] = dataRow.ItemArray[2].ToString();
                    newDataRow["DG_Orders_ProductType_Name"] = dataRow.ItemArray[2].ToString();
                    newDataRow["DG_IsAccessory"] = Convert.ToBoolean(dataRow.ItemArray[4]);
                    newDataRow["DG_IsActive"] = Convert.ToBoolean(dataRow.ItemArray[5]);
                    newDataRow["DG_IsPackage"] = Convert.ToBoolean(dataRow.ItemArray[6]);
                    newDataRow["DG_Orders_ProductType_DiscountApplied"] = Convert.ToBoolean(dataRow.ItemArray[7]);
                    newDataRow["DG_MaxQuantity"] = 0; // dataRow.ItemArray[1].ToString();
                    newDataRow["DG_Orders_ProductType_Image"] = "/images/jpgfloppy.png";
                    newDataRow["SyncCode"] = dataRow.ItemArray[8].ToString();
                    newDataRow["IsSynced"] = false;
                    newDataRow["DG_Currency_Symbol"] = dataRow.ItemArray[0].ToString();
                    newDataRow["DG_Product_Pricing_ProductPrice"] = dataRow.ItemArray[3].ToDouble();
                    product.Rows.Add(newDataRow);
                }

                this.operation = () =>
                {
                    ProductTypeDao access = new ProductTypeDao(this.Transaction);
                    access.BulkSaveProduct(product, createdBy, storeId);
                };
                this.Start(false);
                return true;
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
                return false;
            }

        }
        public int GetProductID(string ProductTypeName)
        {
            try
            {
                List<ProductTypeInfo> objList = new List<ProductTypeInfo>();
                this.operation = () =>
                {
                    ProductTypeDao access = new ProductTypeDao(this.Transaction);
                    objList = access.SelectProductType(0, null, ProductTypeName);
                };
                this.Start(false);
                if (objList != null)
                    return objList.FirstOrDefault().DG_Orders_ProductType_pkey;
                else
                    return 0;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }
        public double GetProductPricing(int ProductTypeID)
        {
            try
            {
                ProductTypeInfo objInfo = new ProductTypeInfo();
                this.operation = () =>
                {
                    ProductTypeDao access = new ProductTypeDao(this.Transaction);
                    objInfo = access.GetProductPricing(ProductTypeID);
                };
                this.Start(false);
                return (double)objInfo.DG_Product_Pricing_ProductPrice;

            }
            catch (Exception ex)
            {
                return -1;
            }
        }
        public List<PackageDetailsViewInfo> GetPackagDetails(int PackageId)
        {
            try
            {
                List<PackageDetailsViewInfo> PackageDetailsList = new List<PackageDetailsViewInfo>();
                this.operation = () =>
                {
                    PackageDetailsDao access = new PackageDetailsDao(this.Transaction);
                    PackageDetailsList = access.GetPackagDetails(PackageId);
                };
                this.Start(false);
                return PackageDetailsList;

            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public List<ProductTypeInfo> GetProductType()
        {
            List<ProductTypeInfo> objectList = new List<ProductTypeInfo>();
            this.operation = () =>
            {
                ProductTypeDao access = new ProductTypeDao(this.Transaction);
                objectList = access.SelectProductType(0, false, string.Empty);
            };
            this.Start(false);
            return objectList;
        }
        public List<ProductTypeInfo> GetPackageType()
        {
            List<ProductTypeInfo> objectList = new List<ProductTypeInfo>();
            this.operation = () =>
            {
                ProductTypeDao access = new ProductTypeDao(this.Transaction);
                objectList = access.SelectProductType(0, true, string.Empty);
            };
            this.Start(false);
            return objectList;
        }
        public ProductTypeInfo GetProductType(int productId)
        {
            List<ProductTypeInfo> objectList = new List<ProductTypeInfo>();
            this.operation = () =>
            {
                ProductTypeDao access = new ProductTypeDao(this.Transaction);
                objectList = access.SelectProductType(productId, null, string.Empty);
            };
            this.Start(false);
            return objectList.FirstOrDefault();
        }
        public bool SaveBorderFor4Images(bool chk4Large, bool chkUniq4, bool chk4small, bool chk3by3, bool chkUniq4SW)
        {
            try
            {
                List<ProductTypeInfo> _objConfig = new List<ProductTypeInfo>();
                this.operation = () =>
                {
                    ProductTypeDao access = new ProductTypeDao(this.Transaction);
                    access.UpdateBorderImages(chk4Large, chkUniq4, chk4small, chk3by3, chkUniq4SW);

                }; this.Start(false);


                return true;

            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public DataSet GetProductSummary(DateTime FromDate, DateTime ToDate, String StoreName, String UserName, int SubStorePKey)
        {
            DataSet dsResult = new DataSet();
            this.operation = () =>
             {
                 ReportDao context = new ReportDao(this.Transaction);
                 dsResult = context.SelectProductSummary(ToDate, FromDate, UserName, StoreName, SubStorePKey);
             };
            this.Start(false);
            return dsResult;
        }


        //public List<ProductTypeInfo> GetProductTypeActive()
        //{
        //    List<ProductTypeInfo> objectList = new List<ProductTypeInfo>();
        //    this.operation = () =>
        //    {
        //        ProductTypeDao access = new ProductTypeDao(this.Transaction);
        //        objectList = access.GetActiveProductTypeData();
        //    };
        //    this.Start(false);
        //    return objectList;
        //}
        public bool CheckIsVisibleProductForBackGround(int BGID, int ProductID)
        {
            BackGroundInfo _objcheckBackground = new BackGroundInfo();
            this.operation = () =>
            {
                BackGroundDao access = new BackGroundDao(this.Transaction);
                _objcheckBackground = access.Get(BGID, ProductID, 0, string.Empty);
            };
            this.Start(false);
            if (_objcheckBackground == null)
            {
                return false;
            }
            else
            {
                return true;
            }

        }

        public ProductTypeInfo GetProductByID(int productid)
        {
            List<ProductTypeInfo> _objprod = new List<ProductTypeInfo>();
            this.operation = () =>
              {
                  ProductTypeDao access = new ProductTypeDao(this.Transaction);
                  _objprod = access.SelectProductType(productid, null, string.Empty);
              };
            this.Start(false);
            return _objprod.FirstOrDefault();
        }




        /// <summary>
        /// modifid by latika for AR Personalised 4Apr2019
        /// </summary>
        /// <param name="ProductTypeId"></param>
        /// <param name="ProductTypeName"></param>
        /// <param name="ProductTypeDesc"></param>
        /// <param name="IsDiscount"></param>
        /// <param name="ProductPrice"></param>
        /// <param name="stroreId"></param>
        /// <param name="UserId"></param>
        /// <param name="ispackage"></param>
        /// <param name="Isactive"></param>
        /// <param name="IsAccessory"></param>
        /// <param name="IsTaxIncluded"></param>
        /// <param name="Productcode"></param>
        /// <param name="SyncCode"></param>
        /// <param name="syncodeforPackage"></param>
        /// <param name="IsInvisible"></param>
        /// <param name="IschkWaterMarked"></param>
        /// <param name="SubStoreID"></param>
        /// <param name="IsPanorama"></param>
        /// <param name="IsIsPersonalizedAR"></param>
        /// <returns></returns>
        public int SetProductTypeInfo(int ProductTypeId, string ProductTypeName, string ProductTypeDesc, bool? IsDiscount, string ProductPrice, int stroreId, int UserId, bool? ispackage, bool? Isactive, bool? IsAccessory, bool? IsTaxIncluded, string Productcode, string SyncCode, string syncodeforPackage, int? IsInvisible, bool? IschkWaterMarked, int SubStoreID, bool? IsPanorama, bool? IsIsPersonalizedAR)
        {
            int Result = 0;
            this.operation = () =>
            {
                ProductTypeDao access = new ProductTypeDao(this.Transaction);
                Result= access.UpdAndInsProductTypeInfo(ProductTypeId, stroreId, UserId, IsInvisible, IsDiscount, ispackage, Isactive, IsAccessory, IsTaxIncluded, ProductTypeName, ProductTypeDesc,
                    ProductPrice, Productcode, SyncCode, syncodeforPackage, IschkWaterMarked, SubStoreID, IsPanorama, IsIsPersonalizedAR);
            };
            this.Start(false);


            return Result;

        }
        public List<ProductTypeInfo> GetProductTypeList(bool? IsPackage)
        {
            List<ProductTypeInfo> _objprod = new List<ProductTypeInfo>();
            this.operation = () =>
            {
                ProductTypeDao access = new ProductTypeDao(this.Transaction);
                _objprod = access.SelProductTypeData(IsPackage);
            }; this.Start(false);
            return _objprod;
        }


        public ProductTypeInfo GetProductTypeListById(int ProductId)
        {
            ProductTypeInfo _objprod = new ProductTypeInfo();
            this.operation = () =>
                {
                    ProductTypeDao access = new ProductTypeDao(this.Transaction);
                    _objprod = access.GetProductTypeDataByKey(ProductId);

                };
            this.Start(false);
            return _objprod;
        }
        public List<ProductTypeInfo> GetPackageNames(bool IsPackage)
        {
            try
            {
                List<ProductTypeInfo> _objprod = new List<ProductTypeInfo>();
                this.operation = () =>
                 {
                     ProductTypeDao access = new ProductTypeDao(this.Transaction);
                     _objprod = access.GetOrdersProductTypeByPackage(IsPackage);

                 };
                this.Start(false);
                return _objprod;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public bool DeleteProductType(int ProductTypeId)
        {
            try
            {
                bool result = false;
                this.operation = () =>
                {
                    ProductTypeDao access = new ProductTypeDao(this.Transaction);
                    result = access.Delete(ProductTypeId); //TBD delete by t => t.DG_Orders_ProductType_pkey == ProductTypeId && t.DG_IsPrimary != true
                };
                this.Start(false);
                return result;

            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public bool CheckduplicateProduct(string ProductTypeName,int productID)
        {
            try
            {
                bool result = false;
                this.operation = () =>
                {
                    ProductTypeDao access = new ProductTypeDao(this.Transaction);
                    result = access.CheckduplicateProduct(ProductTypeName, productID); //TBD delete by t => t.DG_Orders_ProductType_pkey == ProductTypeId && t.DG_IsPrimary != true
                };
                this.Start(false);
                return result;

            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public List<ProductPriceInfo> GetProductPricingStoreWise(int StoreId)
        {
            List<ProductPriceInfo> objectList = new List<ProductPriceInfo>();
            this.operation = () =>
            {
                ProductTypeDao access = new ProductTypeDao(this.Transaction);
                objectList = access.SelectProductPrice(StoreId); ///TBD by DG_Product_Pricing_StoreId
            };
            this.Start(false);
            return objectList;
        }

        public List<ProductTypeInfo> GetProductTypeforOrder(int SubStoreID)
        {
            List<ProductTypeInfo> objectList = new List<ProductTypeInfo>();
            this.operation = () =>
            {
                ProductTypeDao access = new ProductTypeDao(this.Transaction);
                objectList = access.GetProductTypeforOrder(SubStoreID);
            };
            this.Start(false);
            return objectList;
        }

        public int GetProductsynccodeName(string ProductTypeName)
        {
            List<ProductTypeInfo> _objprod = new List<ProductTypeInfo>();
            this.operation = () =>
            {
                ProductTypeDao access = new ProductTypeDao(this.Transaction);
                _objprod = access.SelectProductType(null, null, ProductTypeName);
            };
            this.Start(false);
            if (_objprod != null)
                return _objprod.FirstOrDefault().DG_Orders_ProductType_pkey;
            else
                return 0;
        }
        public bool SetProductPricingData(int? ProductTypeId, double? ProductPrice, int? CurrencyId, int? CreatedBy, int? storeId, bool? Isavailable)
        {
            this.operation = () =>
            {
                ProductTypeDao access = new ProductTypeDao(this.Transaction);
                access.SetProductPricingData(ProductTypeId, ProductPrice, CurrencyId, CreatedBy, storeId, Isavailable);
            };
            this.Start(false);
            return true;

        }

        public List<GroupInfo> GetGroupList()
        {
            List<GroupInfo> GroupList = new List<GroupInfo>();
            this.operation = () =>
            {
                ProductTypeDao access = new ProductTypeDao(this.Transaction);
                GroupList = access.GetGroupList();
            };
            this.Start(false);
            return GroupList;
        }
        public bool IsChkSpecOnlinePackage(int PackageId)
        {
            try
            {
                bool result = false;
                this.operation = () =>
                {
                    ProductTypeDao access = new ProductTypeDao(this.Transaction);
                    result = access.IsChkSpecOnlinePackage(PackageId);
                };
                this.Start(false);
                return result;
            }
            catch
            {
                return false;
            }

        }
        //////////////////CREATED NY LATIKA FOR SEARCH MANAGE PRODUCT 
        public List<ProductTypeInfo> GetProductTypeListSearch(string strSearch)
        {
            List<ProductTypeInfo> _objprod = new List<ProductTypeInfo>();
            this.operation = () =>
            {
                ProductTypeDao access = new ProductTypeDao(this.Transaction);
                _objprod = access.SelProductTypeDataSearch(strSearch);
            }; this.Start(false);
            return _objprod;
        }

        //////////END BY LATIKA
        /// <summary>
        /// /////////////made changes by latika for Evoucher 04 feb20100
        /// </summary>
        /// <param name="SubStoreID"></param>
        /// <returns></returns>
        public List<ProductTypeInfo> GetProductTypeforOrderEvoucher(int SubStoreID, string EvoucherCode)
        {
            List<ProductTypeInfo> objectList = new List<ProductTypeInfo>();
            this.operation = () =>
            {
                ProductTypeDao access = new ProductTypeDao(this.Transaction);
                objectList = access.GetProductTypeforOrderEvoucher(SubStoreID, EvoucherCode);
            };
            this.Start(false);
            return objectList;
        }
        /// <summary>
        /// ///end by latka
        /// </summary>
        /// <param name="ProductTypeName"></param>
        /// <returns></returns>

    }
}
