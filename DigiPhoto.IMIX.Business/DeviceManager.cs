﻿using DigiPhoto.IMIX.DataAccess;
using DigiPhoto.IMIX.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DigiPhoto.IMIX.Business
{
    public class DeviceManager : BaseBusiness
    {

        public bool IsDeviceAssociatedToOthers(int CameraId, string DeviceIds)
        {
            try
            {
                bool issucess = false;
                this.operation = () =>
                {
                    ManageDeviceAccess access = new ManageDeviceAccess(this.Transaction);
                    issucess = access.IsDeviceAssociatedToOthers(CameraId, DeviceIds);
                };
                this.Start(false);
                return issucess;
            }
            catch
            {
                return false;
            }
        }
        public bool SaveCameraDeviceAssociation(int CameraId, string DeviceIds)
        {
            try
            {
                bool issucess = false;
                this.operation = () =>
                {
                    ManageDeviceAccess access = new ManageDeviceAccess(this.Transaction);
                    issucess = access.SaveCameraDeviceAssociation(CameraId, DeviceIds);
                };
                this.Start(false);
                return issucess;
            }
            catch
            {
                return false;
            }
        }

        public bool SaveCameraCharacterAssociation(int CameraId, int Camerapkey, int? CharacterIds)
        {
            try
            {
                bool issucess = false;
                this.operation = () =>
                {
                    ManageDeviceAccess access = new ManageDeviceAccess(this.Transaction);
                    issucess = access.SaveCameraCharacterAssociation(CameraId, Camerapkey, CharacterIds);
                };
                this.Start(false);
                return issucess;
            }
            catch
            {
                return false;
            }
        }
        public bool DeleteCameraDeviceAssociation(int cameraId)
        {
            try
            {
                bool issucess = false;
                this.operation = () =>
                {
                    ManageDeviceAccess access = new ManageDeviceAccess(this.Transaction);
                    issucess = access.DeleteCameraDeviceAssociation(cameraId);
                };
                this.Start(false);
                return issucess;
            }
            catch
            {
                return false;
            }
        }
        public List<CameraDeviceAssociationInfo> GetCameraDeviceList()
        {
            try
            {
                List<CameraDeviceAssociationInfo> list = new List<CameraDeviceAssociationInfo>();
                this.operation = () =>
                {
                    ManageDeviceAccess access = new ManageDeviceAccess(this.Transaction);
                    list = access.GetCameraDeviceList(0);
                    //  return list;
                };
                this.Start(false);
                return list;
            }
            catch
            {
                return null;
            }
        }
        public List<CameraDeviceAssociationInfo> GetCameraDeviceList(int CameraId)
        {
            try
            {
                List<CameraDeviceAssociationInfo> list = new List<CameraDeviceAssociationInfo>();
                this.operation = () =>
                {
                    ManageDeviceAccess access = new ManageDeviceAccess(this.Transaction);
                    list = access.GetCameraDeviceList(CameraId);
                    //  return list;
                };
                this.Start(false);
                return list;
            }
            catch
            {
                return null;
            }
        }

        public List<DeviceInfo> GetPhotoGrapherDeviceList(int photographerId)
        {
            List<DeviceInfo> list = new List<DeviceInfo>();
            try
            {
                this.operation = () =>
                {
                    ManageDeviceAccess access = new ManageDeviceAccess(this.Transaction);
                    list = access.GetPhotoGrapherDeviceList(photographerId);
                };
                this.Start(false);
                return list;
            }
            catch
            {
                return list;
            }
        }

        public bool SaveDevice(DeviceInfo device)
        {
            try
            {
                bool issucess = false;
                this.operation = () =>
                {
                    ManageDeviceAccess access = new ManageDeviceAccess(this.Transaction);
                    issucess = access.SaveDevice(device);
                };
                this.Start(false);
                return issucess;
            }
            catch
            {
                return false;
            }
        }
        public bool DeleteDevice(int deviceId)
        {
            try
            {
                bool issucess = false;
                this.operation = () =>
                {
                    ManageDeviceAccess access = new ManageDeviceAccess(this.Transaction);
                    issucess = access.DeleteDevice(deviceId);
                };
                this.Start(false);
                return issucess;
            }
            catch
            {
                return false;
            }
        }
        //Video Packages
        public List<DeviceInfo> GetDeviceList()
        {
            try
            {
                List<DeviceInfo> list = new List<DeviceInfo>();
                this.operation = () =>
                {
                    ManageDeviceAccess access = new ManageDeviceAccess(this.Transaction);
                    list = access.GetDeviceList(0);
                    //  return list;
                };
                this.Start(false);
                return list;
            }
            catch
            {
                return null;
            }
        }
        public List<DeviceInfo> GetDevice(int DeviceId)
        {
            try
            {
                List<DeviceInfo> list = new List<DeviceInfo>();
                if (DeviceId <= 0)
                    return list;
                this.operation = () =>
                {
                    ManageDeviceAccess access = new ManageDeviceAccess(this.Transaction);
                    list = access.GetDeviceList(DeviceId);
                    //  return list;
                };
                this.Start(false);
                return list;
            }
            catch
            {
                return null;
            }
        }

         public int DeleteCameraDeviceSyncDetails(int CameraId, string DeviceIds)
        {
            int result = 0;
            try
            {
                this.operation = () =>
                {
                    ManageDeviceAccess access = new ManageDeviceAccess(this.Transaction);
                    result = access.DeletCameraDeviceSyncDetailsAccess(CameraId, DeviceIds);
                };
                this.Start(false);
                return result;
            }
            catch
            {
                return result;
            }

        }
        /*
        public Dictionary<int, string> GetDeviceTypes()
        {
            try
            {
                Dictionary<int, string> list=new Dictionary<int,string>();
                this.operation = () =>
                {
                    ManageDeviceAccess access = new ManageDeviceAccess(this.Transaction);
                    list = access.GetDeviceTypes();
                    //  return list;
                };
                this.Start(false);
                return list;
            }
            catch 
            {
                return null;
            }
        }
        */
        //
    }
}
