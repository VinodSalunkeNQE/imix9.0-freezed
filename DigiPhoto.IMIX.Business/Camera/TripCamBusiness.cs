﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DigiPhoto.IMIX.DataAccess;
using DigiPhoto.IMIX.Model;
using System.Data;
using DigiPhoto.DataLayer;

namespace DigiPhoto.IMIX.Business
{
    public class TripCamBusiness : BaseBusiness
    {
        public bool UpdCamIdForTripCamPOSMapping(int oldCamId,int NewCamid)
        {
            try
            {
                bool result = false;
                this.operation = () =>
                {
                    TripCamDao access = new TripCamDao(this.Transaction);
                    result = access.UpdCamIdForTripCamPOSMapping(oldCamId, NewCamid);
                };
                this.Start(false);
                return result;
            }
            catch
            {
                return false;
            }
        }
        public TripCamInfo GetTripCameraInfoById(string CameraName, string CameraId)
        {
            TripCamInfo tripCamInfo = new TripCamInfo();
            try
            {
                this.operation = () =>
                {
                    TripCamDao access = new TripCamDao(this.Transaction);
                    tripCamInfo = access.GetTripCameraInfoById(CameraName, CameraId);
                };
                this.Start(false);
                return tripCamInfo;
            }
            catch
            {
                return tripCamInfo;
            }
        }
        public List<TripCamSettingInfo> GetTripCamSettingsForCameraId(int CameraId, int TripCamTypeId)
        {
            List<TripCamSettingInfo> lstTripCamSettingsInfo = new List<TripCamSettingInfo>();
            try
            {
                this.operation = () =>
                    {
                        TripCamDao access = new TripCamDao(this.Transaction);
                        lstTripCamSettingsInfo = access.GetTripCamSettingsForCameraId(CameraId, TripCamTypeId);
                    };
                this.Start(false);
                return lstTripCamSettingsInfo;
            }
            catch
            {
                return lstTripCamSettingsInfo;
            }
        }

        public List<TripCamSettingInfo> GetSavedTripCamSettingsForCameraId(int Cameraid)
        {
            List<TripCamSettingInfo> lstTripCamSettingsInfo = new List<TripCamSettingInfo>();
            try
            {
                this.operation = () =>
                {
                    TripCamDao access = new TripCamDao(this.Transaction);
                    lstTripCamSettingsInfo = access.GetSavedTripCamSettingsForCameraId(Cameraid);
                };
                this.Start(false);
                return lstTripCamSettingsInfo;
            }
            catch
            {
                return lstTripCamSettingsInfo;
            }
        }

        public bool ValidateCameraRunningStatus(string CamMake)
        {
            bool result = false;
            this.operation = () =>
                {
                    TripCamDao access = new TripCamDao(this.Transaction);
                    result = access.ValidateCameraRunningStatus(CamMake);
                };
            this.Start(false);
            return result;
        }

        public bool ValidateCameraAvailability(string CamMake)
        {
            bool result = false;
            this.operation = () =>
            {
                TripCamDao access = new TripCamDao(this.Transaction);
                result = access.ValidateCameraAvailability(CamMake);
            };
            this.Start(false);
            return result;
        }
        public bool InsUpdTripCamSettings(TripCamFeaturesInfo tripCamFeaturesInfo, int CamId)
        {
            DataTable dt = GetTripCamSettingsDatatable(tripCamFeaturesInfo, CamId);
            bool issucess = false;
            this.operation = () =>
            {
                TripCamDao access = new TripCamDao(this.Transaction);
                issucess = access.InsUpdTripCamSettings(dt);
            };
            this.Start(false);
            return issucess;
        }
        private DataTable GetTripCamSettingsDatatable(TripCamFeaturesInfo tripCamFeaturesInfo, int CamId)
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("TripCamSettingsMasterId");
            dt.Columns.Add("SettingsValue");
            dt.Columns.Add("CameraId");

            dt.Rows.Add(Convert.ToInt32(TripCamFeatures.Height), Convert.ToString(tripCamFeaturesInfo.Height), CamId);
            dt.Rows.Add(Convert.ToInt32(TripCamFeatures.HeightMax), Convert.ToString(tripCamFeaturesInfo.HeightMax), CamId);
            dt.Rows.Add(Convert.ToInt32(TripCamFeatures.OffsetX), Convert.ToString(tripCamFeaturesInfo.OffsetX), CamId);
            dt.Rows.Add(Convert.ToInt32(TripCamFeatures.OffsetY), Convert.ToString(tripCamFeaturesInfo.OffsetY), CamId);
            dt.Rows.Add(Convert.ToInt32(TripCamFeatures.Width), Convert.ToString(tripCamFeaturesInfo.Width), CamId);
            dt.Rows.Add(Convert.ToInt32(TripCamFeatures.WidthMax), Convert.ToString(tripCamFeaturesInfo.WidthMax), CamId);
            dt.Rows.Add(Convert.ToInt32(TripCamFeatures.Hue), Convert.ToString(tripCamFeaturesInfo.Hue), CamId); 
            dt.Rows.Add(Convert.ToInt32(TripCamFeatures.Saturation), Convert.ToString(tripCamFeaturesInfo.Saturation), CamId);
            dt.Rows.Add(Convert.ToInt32(TripCamFeatures.WhiteBalanceRed), Convert.ToString(tripCamFeaturesInfo.WhiteBalanceRed), CamId); 
            dt.Rows.Add(Convert.ToInt32(TripCamFeatures.WhiteBalanceBlue), Convert.ToString(tripCamFeaturesInfo.WhiteBalanceBlue), CamId);
            dt.Rows.Add(Convert.ToInt32(TripCamFeatures.CameraName), Convert.ToString(tripCamFeaturesInfo.CameraName), CamId);
            dt.Rows.Add(Convert.ToInt32(TripCamFeatures.NoOfImages), Convert.ToString(tripCamFeaturesInfo.NoOfImages), CamId);
            dt.Rows.Add(Convert.ToInt32(TripCamFeatures.TriggerDelay), Convert.ToString(tripCamFeaturesInfo.TriggerDelay), CamId);
            dt.Rows.Add(Convert.ToInt32(TripCamFeatures.ExposureTime), Convert.ToString(tripCamFeaturesInfo.ExposureTime), CamId);
            dt.Rows.Add(Convert.ToInt32(TripCamFeatures.CameraRotation), Convert.ToString(tripCamFeaturesInfo.CameraRotation), CamId);
            dt.Rows.Add(Convert.ToInt32(TripCamFeatures.StrobeDelay), Convert.ToString(tripCamFeaturesInfo.StrobeDelay), CamId);
            dt.Rows.Add(Convert.ToInt32(TripCamFeatures.StrobeDuration), Convert.ToString(tripCamFeaturesInfo.StrobeDuration), CamId);
            dt.Rows.Add(Convert.ToInt32(TripCamFeatures.StrobeDurationMode), Convert.ToString(tripCamFeaturesInfo.StrobeDurationMode), CamId);
            dt.Rows.Add(Convert.ToInt32(TripCamFeatures.StrobeSource), Convert.ToString(tripCamFeaturesInfo.StrobeSource), CamId);
            dt.Rows.Add(Convert.ToInt32(TripCamFeatures.PacketSize), Convert.ToString(tripCamFeaturesInfo.PacketSize), CamId);
            dt.Rows.Add(Convert.ToInt32(TripCamFeatures.MACAddress), Convert.ToString(tripCamFeaturesInfo.MACAddress), CamId);
            dt.Rows.Add(Convert.ToInt32(TripCamFeatures.IPConfigurationMode), Convert.ToString(tripCamFeaturesInfo.IPConfigurationMode), CamId);
            dt.Rows.Add(Convert.ToInt32(TripCamFeatures.IPAddress), Convert.ToString(tripCamFeaturesInfo.IPAddress), CamId);
            dt.Rows.Add(Convert.ToInt32(TripCamFeatures.Subnet), Convert.ToString(tripCamFeaturesInfo.Subnet), CamId);
            dt.Rows.Add(Convert.ToInt32(TripCamFeatures.Gateway), Convert.ToString(tripCamFeaturesInfo.Gateway), CamId);
            dt.Rows.Add(Convert.ToInt32(TripCamFeatures.PixelFormat), Convert.ToString(tripCamFeaturesInfo.PixelFormat), CamId);
            dt.Rows.Add(Convert.ToInt32(TripCamFeatures.TriggerSource), Convert.ToString(tripCamFeaturesInfo.TriggerSource), CamId);
            dt.Rows.Add(Convert.ToInt32(TripCamFeatures.LensFocus), Convert.ToString(tripCamFeaturesInfo.LensFocus), CamId);
            dt.Rows.Add(Convert.ToInt32(TripCamFeatures.Aperture), Convert.ToString(tripCamFeaturesInfo.Aperture), CamId);
            dt.Rows.Add(Convert.ToInt32(TripCamFeatures.CameraRotation), Convert.ToString(tripCamFeaturesInfo.CameraRotation), CamId);
            dt.Rows.Add(Convert.ToInt32(TripCamFeatures.StreamBytesPerSec), Convert.ToString(tripCamFeaturesInfo.StreamBytesPerSec), CamId);
            dt.Rows.Add(Convert.ToInt32(TripCamFeatures.BlackLevel), Convert.ToString(tripCamFeaturesInfo.BlackLevel), CamId);
            dt.Rows.Add(Convert.ToInt32(TripCamFeatures.Gain), Convert.ToString(tripCamFeaturesInfo.Gain), CamId);
            dt.Rows.Add(Convert.ToInt32(TripCamFeatures.Gamma), Convert.ToString(tripCamFeaturesInfo.Gamma), CamId);
            dt.Rows.Add(Convert.ToInt32(TripCamFeatures.Gain00), Convert.ToString(tripCamFeaturesInfo.Gain00), CamId);
            dt.Rows.Add(Convert.ToInt32(TripCamFeatures.Gain01), Convert.ToString(tripCamFeaturesInfo.Gain01), CamId);
            dt.Rows.Add(Convert.ToInt32(TripCamFeatures.Gain02), Convert.ToString(tripCamFeaturesInfo.Gain02), CamId);
            dt.Rows.Add(Convert.ToInt32(TripCamFeatures.Gain10), Convert.ToString(tripCamFeaturesInfo.Gain10), CamId);
            dt.Rows.Add(Convert.ToInt32(TripCamFeatures.Gain11), Convert.ToString(tripCamFeaturesInfo.Gain11), CamId);
            dt.Rows.Add(Convert.ToInt32(TripCamFeatures.Gain12), Convert.ToString(tripCamFeaturesInfo.Gain12), CamId);
            dt.Rows.Add(Convert.ToInt32(TripCamFeatures.Gain20), Convert.ToString(tripCamFeaturesInfo.Gain20), CamId);
            dt.Rows.Add(Convert.ToInt32(TripCamFeatures.Gain21), Convert.ToString(tripCamFeaturesInfo.Gain21), CamId);
            dt.Rows.Add(Convert.ToInt32(TripCamFeatures.Gain22), Convert.ToString(tripCamFeaturesInfo.Gain22), CamId);
            dt.Rows.Add(Convert.ToInt32(TripCamFeatures.ColorTransformationMode), Convert.ToString(tripCamFeaturesInfo.ColorTransformationMode), CamId);
            dt.Rows.Add(Convert.ToInt32(TripCamFeatures.StrobeSource), Convert.ToString(tripCamFeaturesInfo.ExposureMode), CamId);
            dt.Rows.Add(Convert.ToInt32(TripCamFeatures.SyncOutPolarity), Convert.ToString(tripCamFeaturesInfo.SyncOutPolarity), CamId);
            dt.Rows.Add(Convert.ToInt32(TripCamFeatures.SyncOutSelector), Convert.ToString(tripCamFeaturesInfo.SyncOutSelector), CamId);
            dt.Rows.Add(Convert.ToInt32(TripCamFeatures.SyncOutSource), Convert.ToString(tripCamFeaturesInfo.SyncOutSource), CamId);
            dt.Rows.Add(Convert.ToInt32(TripCamFeatures.AcquisitionMode), Convert.ToString(tripCamFeaturesInfo.AcquisitionMode), CamId);

            return dt;
        }
    }
}
