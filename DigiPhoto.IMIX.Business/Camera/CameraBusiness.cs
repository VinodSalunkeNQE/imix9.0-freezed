﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using System.Xml.Linq;
using System.Data;
using System.ComponentModel;
using System.Collections;
using System.Reflection;
using System.Globalization;
using DigiPhoto.IMIX.Model;
using DigiPhoto.IMIX.DataAccess;
using DigiPhoto.IMIX.Business;
using DigiPhoto.DataLayer;
using DigiPhoto.DataLayer.Model;

namespace DigiPhoto.IMIX.Business
{

    public partial class CameraBusiness : BaseBusiness
    {

        /// <summary>
        /// Gets the available ride cameras.
        /// </summary>
        /// <returns></returns>
        public List<CameraInfo> GetAvailableRideCameras()
        {
            try
            {
                List<CameraInfo> _objridecamera = new List<CameraInfo>();

                this.operation = () =>
                   {
                       CameraDao access = new CameraDao(this.Transaction);
                       _objridecamera = access.SelectRideCamera();
                   }; this.Start(false);
                return _objridecamera;
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogFileWrite(ErrorHandler.ErrorHandler.CreateErrorMessage(ex));
                return null;
            }
        }
        public List<CameraInfo> GetCameraList()
        {
            try
            {
                List<CameraInfo> _objridecamera = new List<CameraInfo>();
                this.operation = () =>
                   {
                       CameraDao access = new CameraDao(this.Transaction);
                       _objridecamera = access.GetCameraList();
                   }; this.Start(false);
                return _objridecamera;
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogFileWrite(ErrorHandler.ErrorHandler.CreateErrorMessage(ex));
                return null;
            }
        }
        public List<CameraInfo> GetCameraListSearch(int SubStoreID, int LocationID)
        {
            try
            {
                List<CameraInfo> _objridecamera = new List<CameraInfo>();
                this.operation = () =>
                {
                    CameraDao access = new CameraDao(this.Transaction);
                    _objridecamera = access.GetCameraListSearch(SubStoreID, LocationID);
                }; this.Start(false);
                return _objridecamera;
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogFileWrite(ErrorHandler.ErrorHandler.CreateErrorMessage(ex));
                return null;
            }
        }
        public CameraInfo GetCameraByID(int CameraId)
        {
            try
            {

                List<CameraInfo> objectList = new List<CameraInfo>();
                this.operation = () =>
                {
                    CameraDao access = new CameraDao(this.Transaction);
                    objectList = access.GetCameraDetailsByID(CameraId, null);
                };
                this.Start(false);
                return objectList.FirstOrDefault();
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogFileWrite(ErrorHandler.ErrorHandler.CreateErrorMessage(ex));
                return null;
            }
        }
        public CameraInfo GetCameraByPhotographerId(int PhotographerId)
        {
            try
            {

                List<CameraInfo> objectList = new List<CameraInfo>();
                this.operation = () =>
                {
                    CameraDao access = new CameraDao(this.Transaction);
                    objectList = access.GetCameraDetailsByID(null, PhotographerId);
                };
                this.Start(false);
                return objectList.FirstOrDefault();
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogFileWrite(ErrorHandler.ErrorHandler.CreateErrorMessage(ex));
                return null;
            }
        }
        public string DeleteCameraDetails(int CameraId)
        {
            string result = string.Empty;
            try
            {
                this.operation = () =>
                {
                    CameraDao access = new CameraDao(this.Transaction);
                    result = access.Delete(CameraId);
                };
                this.Start(false);
                return result;
            }
            catch (Exception ex)
            {
                return result;
            }
        }
        public bool SetCameraDetails(CameraInfo _objCameraInfo)
        {
            try
            {
                bool result = false;
                this.operation = () =>
                {
                    CameraDao access = new CameraDao(this.Transaction);
                    result = access.SetGetCameraDetails(_objCameraInfo);
                };
                this.Start(false);
                return result;
            }
            catch
            {
                return false;
            }
        }
        public bool CheckResetDPIRequired(int PhotographerId)
        {
            try
            {
                bool IsCromaColor = false;
                CameraInfo camInfo = this.GetCameraByPhotographerId(PhotographerId);
                if (camInfo != null)
                    IsCromaColor = camInfo.IsChromaColor != null ? Convert.ToBoolean(camInfo.IsChromaColor) : false;
                return IsCromaColor;
            }
            catch
            {
                return false;
            }
        }
        public bool CheckResetDPIRequired(string HR, string VR, int PhotographerId)
        {
            try
            {
                int hr = Convert.ToInt32(HR.Replace("'", ""));
                int vr = Convert.ToInt32(VR.Replace("'", ""));
                bool IsCromaColor = false;
                CameraInfo camInfo = this.GetCameraByPhotographerId(PhotographerId);
                if (camInfo != null)
                    IsCromaColor = camInfo.IsChromaColor != null ? Convert.ToBoolean(camInfo.IsChromaColor) : false;

                if ((hr != 300 || vr != 300) && IsCromaColor)
                    return true;
                else
                    return false;
            }
            catch
            {
                return false;
            }
        }

        public bool GetIsResetDPIRequired(int PhotoGrapherId)
        {
            List<CameraInfo> objectList = new List<CameraInfo>();
            this.operation = () =>
            {
                CameraDao access = new CameraDao(this.Transaction);
                objectList = access.GetIsResetDPIRequired(PhotoGrapherId);
            };
            this.Start(false);

            var tempVal = objectList.Where(t => t.DG_AssignTo == PhotoGrapherId && t.DG_Camera_IsDeleted == false).FirstOrDefault().IsChromaColor;
            bool? reqVal = tempVal != null ? tempVal : false;
            return (bool)reqVal;
        }

        public bool SetTripCameraSetting(RideCameraSettingInfo objRid)
        {
            bool flag = false;
            List<CameraInfo> objectList = new List<CameraInfo>();
            this.operation = () =>
            {
                CameraDao access = new CameraDao(this.Transaction);
                //string testStr = access.SetTestString(objRid);
                flag = access.SetTripCameraSetting(objRid);
            };
            this.Start(false);
            return flag;
        }

        public RideCameraSettingInfo GetRideCameraSetting(string CameraId)
        {
            RideCameraSettingInfo dtData = new RideCameraSettingInfo();
            this.operation = () =>
            {
                CameraDao access = new CameraDao(this.Transaction);
                dtData = access.GetRideCameraSetting(CameraId);
            };
            this.Start(false);
            return dtData;
        }

        public string GetCameraPathForRideCameraId(string CameraId)
        {
            try
            {
                string cameraPath = string.Empty;
                this.operation = () =>
                {
                    CameraDao access = new CameraDao(this.Transaction);
                    cameraPath = access.GetCameraPathForRideCameraId(CameraId);
                };
                this.Start(false);
                return cameraPath;
            }
            catch
            {
                return string.Empty;
            }
        }
        public CameraInfo GetLocationWiseCameraDetails(int LocationId)
        {
            try
            {
                CameraInfo _objcamera = new CameraInfo();
                this.operation = () =>
                {
                    CameraDao access = new CameraDao(this.Transaction);
                    _objcamera = access.GetLocationWiseCameraDetails(LocationId);
                }; this.Start(false);
                return _objcamera;
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogFileWrite(ErrorHandler.ErrorHandler.CreateErrorMessage(ex));
                return null;
            }
        }
        public int CheckIsCamDeleteAccess(int RoleID)
        {
            try
            {
                int result = 0;
                this.operation = () =>
                {
                    CameraDao access = new CameraDao(this.Transaction);
                    result = access.CheckIsCamDeleteAccess(RoleID);
                };
                this.Start(false);
                return result;
            }
            catch
            {
                return 0;
            }
        }
    }
}
