﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using DigiPhoto.DataLayer.Model;
using System.Xml.Linq;
using System.Data;
using System.ComponentModel;
using System.Collections;
using System.Reflection;
using System.Globalization;
using DigiPhoto.IMIX.Model;
using DigiPhoto.IMIX.DataAccess;
using DigiPhoto.IMIX.Business;
using DigiPhoto.DataLayer;
using DigiPhoto.Cache.Repository;
using DigiPhoto.Cache.DataCache;
using DigiPhoto.Cache.MasterDataCache;

namespace DigiPhoto.IMIX.Business
{

    public class GraphicsBusiness : BaseBusiness
    {
        public List<GraphicsInfo> GetGraphicsDetails()
        {
            try
            {
                List<GraphicsInfo> objList = new List<GraphicsInfo>();
                this.operation = () =>
                {
                    GraphicsDao access = new GraphicsDao(this.Transaction);
                    objList = access.Select();
                };
                this.Start(false);
                return objList;
            }
            catch (Exception ex)
            {
                //throw ex;
                return null;
            }
        }
        public void SetGraphicsDetails(string graphicsname, string graphicsdisplayname, bool isactive, string SyncCode,int userId)
        {
            GraphicsInfo _objnew = new GraphicsInfo();
            _objnew.DG_Graphics_Name = graphicsname;
            _objnew.DG_Graphics_Displayname = graphicsdisplayname;
            _objnew.DG_Graphics_IsActive = isactive;
            _objnew.SyncCode = SyncCode;
            _objnew.IsSynced = false;
            _objnew.CreatedBy = userId;
            this.operation = () =>
            {
                GraphicsDao access = new GraphicsDao(this.Transaction);
                access.Add(_objnew);
            };
            this.Start(false);
            ICacheRepository imixCache = DataCacheFactory.GetFactory<ICacheRepository>(typeof(ConfigurationCache).FullName);
            imixCache.RemoveFromCache();

        }

        public bool DeleteGraphics(int id)
        {
            try
            {
                bool result = false;
                this.operation = () =>
                {
                    GraphicsDao access = new GraphicsDao(this.Transaction);
                    result = access.Delete(id);
                };
                this.Start(false);
                return result;

            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public void UpdateGraphicsDetails(string graphicsname, string graphicsdisplayname, bool isactive, string SyncCode,int graphicsId,int userId)
        {
            GraphicsInfo _objnew = new GraphicsInfo();
            _objnew.DG_Graphics_Name = graphicsname;
            _objnew.DG_Graphics_Displayname = graphicsdisplayname;
            _objnew.DG_Graphics_IsActive = isactive;
            _objnew.SyncCode = SyncCode;
            _objnew.DG_Graphics_pkey = graphicsId;
            _objnew.IsSynced = false;
            _objnew.ModifiedBy = userId;
            this.operation = () =>
            {
                GraphicsDao access = new GraphicsDao(this.Transaction);
                access.Update(_objnew);
            };
            this.Start(false);
            ICacheRepository imixCache = DataCacheFactory.GetFactory<ICacheRepository>(typeof(ConfigurationCache).FullName);
            imixCache.RemoveFromCache();
          

        }
    }
}
