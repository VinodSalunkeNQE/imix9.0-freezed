﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DigiPhoto.IMIX.DataAccess;
using DigiPhoto.IMIX.Model;

namespace DigiPhoto.IMIX.Business
{
    public class VenueTaxValueBusiness : BaseBusiness
    {
        public List<VenueTaxValueModel> GetVenueTaxValue()
        {
            List<VenueTaxValueModel> lstVenueTaxValue = new List<VenueTaxValueModel>();

            this.Operation = () =>
                {
                    VenueTaxValueAccess access = new VenueTaxValueAccess(this.Transaction);
                    lstVenueTaxValue = access.GetVenueTaxValueDetail();
                };
            this.Start(false);
            return lstVenueTaxValue;
        }
        public List<VenueTaxValueModel> GetVenueTaxValue(int TaxId)
        {
            List<VenueTaxValueModel> lstVenueTaxValue = new List<VenueTaxValueModel>();

            this.Operation = () =>
            {
                VenueTaxValueAccess access = new VenueTaxValueAccess(this.Transaction);
                lstVenueTaxValue = access.GetVenueTaxValueDetail(TaxId);
            };
            this.Start(false);
            return lstVenueTaxValue;
        }

        public VenueTaxValueModel GetActiveTaxDetails()
        {
            VenueTaxValueModel objTaxDetails = new VenueTaxValueModel();
            this.Operation = () =>
            {
                VenueTaxValueAccess access = new VenueTaxValueAccess(this.Transaction);
                objTaxDetails = access.GetActiveTaxDetails();
            };
            this.Start(false);
            return objTaxDetails;
        }
    }
}
