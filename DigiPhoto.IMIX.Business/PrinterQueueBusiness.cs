﻿using DigiPhoto.IMIX.DataAccess;
using DigiPhoto.IMIX.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DigiPhoto.IMIX.Business
{
    public class PrinterQueueBusiness : BaseBusiness
    {
        public PrinterQueueInfo GetPrinterQueue(int subStoreID)
        {
            try
            {
                PrinterQueueInfo printerQueue = null;
                this.operation = () =>
                {
                    PrinterQueueAccess access = new PrinterQueueAccess(this.Transaction);
                    printerQueue = access.GetPrinterQueue(subStoreID);
                };
                this.Start(false);
                return printerQueue;
            }
            catch
            {
                return null;
            }
        }
    }
}
