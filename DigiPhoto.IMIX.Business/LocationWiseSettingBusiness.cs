﻿using DigiPhoto.IMIX.DataAccess;
using DigiPhoto.IMIX.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DigiPhoto.IMIX.Business
{
   public  class LocationWiseSettingBusiness : BaseBusiness
    {
        public List<LocationTypeSettingModel> GetLocationTypeSettings(int LocationID)
        {
            try
            {
                List<LocationTypeSettingModel> list = new List<LocationTypeSettingModel>();
                this.operation = () =>
                {
                    ManageLocationWiseAccess access = new ManageLocationWiseAccess(this.Transaction);
                    list = access.GeLocationTypes(LocationID);
                    //  return list;
                };
                this.Start(false);
                return list;
            }
            catch
            {
                return null;
            }
        }
        public int AddEditPosLocationAssociation(int PosLocationId, string PosName, string LocationIds, int IMIXConfigurationValueIdS, bool IsActive)
        {
            ConfigBusiness obj = new ConfigBusiness();
            int isSuccess = 0;
            this.operation = () =>
            {
                ManageLocationWiseAccess access = new ManageLocationWiseAccess(this.Transaction);
                isSuccess =Convert.ToInt32(access.AddEditPosLocationAssociation(PosLocationId, PosName, LocationIds, IMIXConfigurationValueIdS, IsActive));
            };
            this.Start(false);
            return isSuccess;
        }
        public List<PosLocationAssociationModel> GetPosLocationAssociationList(int PosLocationId,int LocationID)
        {
            try
            {
                List<PosLocationAssociationModel> list = new List<PosLocationAssociationModel>();
                this.operation = () =>
                {
                    ManageLocationWiseAccess access = new ManageLocationWiseAccess(this.Transaction);
                    list = access.GETPosLocationAssociation(PosLocationId, LocationID);
                    //  return list;
                };
                this.Start(false);
                return list;
            }
            catch
            {
                return null;
            }
        }
        public PosLocationAssociationModel GetPosLocationAssociationByID(int PosLocationId, int LocationID)


        {
            try
            {

                List<PosLocationAssociationModel> objectList = new List<PosLocationAssociationModel>();
                this.operation = () =>
                {
                    ManageLocationWiseAccess access = new ManageLocationWiseAccess(this.Transaction);
                    objectList = access.GETPosLocationAssociation(PosLocationId, LocationID);
                };
                this.Start(false);
                return objectList.FirstOrDefault();
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogFileWrite(ErrorHandler.ErrorHandler.CreateErrorMessage(ex));
                return null;
            }
        }

    }
}
