﻿using DigiPhoto.IMIX.DataAccess;
using DigiPhoto.IMIX.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace DigiPhoto.IMIX.Business
{
    public class EvoucherManager : BaseBusiness
    {

        public bool SaveEVoucherMstData(DataTable Dtmst)
        {
            try
            {
                bool issucess = false;
                this.operation = () =>
                {
                    EvoucherDao access = new EvoucherDao(this.Transaction);
                    issucess = access.SaveEVoucherMstData(Dtmst);
                };
                this.Start(false);
                return issucess;
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogFileWrite(ErrorHandler.ErrorHandler.CreateErrorMessage(ex));
                return false;
            }
        }
        public bool SaveEVoucherDetails(DataTable DtDtls)
        {
            try
            {
                bool issucess = false;
                this.operation = () =>
                {
                    EvoucherDao access = new EvoucherDao(this.Transaction);
                    issucess = access.SaveEVoucherProductDetails(DtDtls);
                };
                this.Start(false);
                return issucess;
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogFileWrite(ErrorHandler.ErrorHandler.CreateErrorMessage(ex));
                return false;
            }
        }
        public bool SaveTourOperatorMaster(DataTable DtDtls)
        {
            try
            {
                bool issucess = false;
                this.operation = () =>
                {
                    EvoucherDao access = new EvoucherDao(this.Transaction);
                    issucess = access.SaveTourOperatorMaster(DtDtls);
                };
                this.Start(false);
                return issucess;
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogFileWrite(ErrorHandler.ErrorHandler.CreateErrorMessage(ex));
                return false;
            }
        }
        public bool SaveEVoucherBarcodes(DataTable DtStatus)
        {
            try
            {
                bool issucess = false;
                this.operation = () =>
                {
                    EvoucherDao access = new EvoucherDao(this.Transaction);
                    issucess = access.SaveEVoucherBarcodes(DtStatus);
                };
                this.Start(false);
                return issucess;
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogFileWrite(ErrorHandler.ErrorHandler.CreateErrorMessage(ex));
                return false;
            }
        }

        public EvoucherClaiminfo EvoucherClaim(string Barcode)
        {
            EvoucherClaiminfo objEvchr = new EvoucherClaiminfo();


            try
            {

                this.operation = () =>
                {
                    EvoucherDao access = new EvoucherDao(this.Transaction);
                    objEvchr = access.EvoucherClaim(Barcode);
                };
                this.Start(false);
                return objEvchr;
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogFileWrite(ErrorHandler.ErrorHandler.CreateErrorMessage(ex));
                return objEvchr;
            }
        }

        public List<EVoucherBarcodes> UpdEvoucherBarcodeNofUsedList()
        {
            List<EVoucherBarcodes> objEvchr = new List<EVoucherBarcodes>();


            try
            {

                this.operation = () =>
                {
                    EvoucherDao access = new EvoucherDao(this.Transaction);
                    objEvchr = access.UpdEvoucherBarcodeNofUsedList();
                };
                this.Start(false);
                return objEvchr;
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogFileWrite(ErrorHandler.ErrorHandler.CreateErrorMessage(ex));
                return objEvchr;
            }
        }


        public int SaveEvoucherItemDtls(string EvocuerBarcode, int LineItemID, decimal DiscountAmt)
        {
            int RetunVal = 0;

            try
            {

                this.operation = () =>
                {
                    EvoucherDao access = new EvoucherDao(this.Transaction);
                    RetunVal = access.SaveEvoucherItemDtls(EvocuerBarcode,LineItemID,DiscountAmt);
                };
                this.Start(false);
                return RetunVal;
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogFileWrite(ErrorHandler.ErrorHandler.CreateErrorMessage(ex));
                return RetunVal;
            }
        }

    }
}
