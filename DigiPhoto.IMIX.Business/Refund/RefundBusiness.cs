﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using DigiPhoto.DataLayer.Model;
using System.Xml.Linq;
using System.Data;
using System.ComponentModel;
using System.Collections;
using System.Reflection;
using System.Globalization;
using DigiPhoto.IMIX.Model;
using DigiPhoto.IMIX.DataAccess;
using DigiPhoto.IMIX.Business;
using DigiPhoto.DataLayer;
using DigiPhoto.Utility.Repository.ValueType;

namespace DigiPhoto.IMIX.Business
{

    public partial class RefundBusiness : BaseBusiness
    {
        public double GetOrderRefundedAmount(int Orderid)
        {
            double ItemAmount = 0;
            try
            {
                RefundInfo objectInfo = new RefundInfo();
                this.operation = () =>
                {
                    RefundDao access = new RefundDao(this.Transaction);
                    objectInfo = access.GetRefund(Orderid).FirstOrDefault(); // get by t => t.DG_OrderId == Orderid
                };
                this.Start(false);
                if (objectInfo != null)
                {
                    ItemAmount = Convert.ToDouble(objectInfo.RefundAmount);
                }
                else
                {
                    ItemAmount = 0;
                }
                return ItemAmount;

            }
            catch (Exception ex)
            {
                return 0;
            }
        }
        public int SetRefundMasterData(int OrderId, decimal RefundAmount, DateTime RefundDate, int UserId)
        {
            
                RefundInfo objectInfo = new RefundInfo();
                objectInfo.DG_OrderId = OrderId;
                objectInfo.RefundAmount = decimal.Round(RefundAmount, 3, MidpointRounding.ToEven);
                objectInfo.RefundDate = RefundDate;
                objectInfo.UserId = UserId;
                int RefundId = 0;
                this.operation = () =>
                {
                    RefundDao access = new RefundDao(this.Transaction);
                    RefundId = access.InsandDelRefundMasterData(objectInfo); // get DG_Refund by orderid and delete all DG_RefundDetails,DG_Refund by DG_RefundId and Add DG_Refund ref: DigiPhotoDataservice.cs file same method
                };
                this.Start(false);
                return RefundId;

          
        }
        public bool SetRefundDetailsData(int DG_LineItemId, int DG_RefundMaster_ID, string PhotoId, decimal? refundprice, string reason)
        {
           
                RefundDetailInfo objectInfo = new RefundDetailInfo();
                objectInfo.DG_LineItemId = DG_LineItemId;
                objectInfo.DG_RefundMaster_ID = DG_RefundMaster_ID;
                objectInfo.RefundPhotoId = PhotoId;
                objectInfo.Refunded_Amount = decimal.Round(Convert.ToDecimal(refundprice), 3, MidpointRounding.ToEven);
                objectInfo.RefundReason = reason;

                this.operation = () =>
                {
                    RefundDao access = new RefundDao(this.Transaction);
                     access.SetRefundDetailsData(DG_LineItemId, DG_RefundMaster_ID, PhotoId, refundprice, reason); 
                };
                this.Start(false);
                return true; 
        }

        public List<RefundInfo> GetRefundedItems(int OrderId)
        {

            List<RefundInfo> objectInfo = new List<RefundInfo>();
            this.operation = () =>
            {
                RefundDao access = new RefundDao(this.Transaction);
                objectInfo = access.GetRefundedItems(OrderId);
            };
            this.Start(false);
            return objectInfo;
        }
        public string GetRefundText()
        {
            string RefundSlogan = string.Empty;          
            this.operation = () =>
            {
                BillFormatDao access = new BillFormatDao(this.Transaction);
                RefundSlogan = access.GetBillFormat();
            }; 
            this.Start(false);
            if (RefundSlogan != null)
            {
                return RefundSlogan;
            }
            else
            {
                return null;
            }
        }
    }
}
