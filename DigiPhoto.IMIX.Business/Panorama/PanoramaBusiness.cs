﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DigiPhoto.IMIX.Model;
using DigiPhoto.IMIX.DataAccess;
using System.Collections.ObjectModel;
using System.Data;

namespace DigiPhoto.IMIX.Business
{
  public  class PanoramaBusiness: BaseBusiness
    {
        public ObservableCollection<PanoramaProperties> GetPrinterDimensions()
        {
            ObservableCollection<PanoramaProperties> PanoramaDimesionList = new ObservableCollection<PanoramaProperties>();
            this.operation = () =>
            {
                PanoramaConfigDao objPanoramaDimesion = new PanoramaConfigDao(this.Transaction);
                PanoramaDimesionList = objPanoramaDimesion.GetPrinterDimensions();
            };
            this.Start(false);
            return PanoramaDimesionList;
        }
        public ObservableCollection<PanoramaProperties> GetActiveBorders()
        {
            ObservableCollection<PanoramaProperties> PanoramaDimesionList = new ObservableCollection<PanoramaProperties>();
            this.operation = () =>
            {
                PanoramaConfigDao objPanoramaDimesion = new PanoramaConfigDao(this.Transaction);
                PanoramaDimesionList = objPanoramaDimesion.GetPrinterDimensions();
            };
            this.Start(false);
            return PanoramaDimesionList;
        }
        public bool IsProductPanorama(int dg_printerqueue_pkey)
        {
            //convert to data table

            

            bool issucess = false;
            this.operation = () =>
            {
                PanoramaConfigDao access = new PanoramaConfigDao(this.Transaction);
                issucess = access.IsProductPanoramic(dg_printerqueue_pkey);
            };
            this.Start(false);
            return issucess;
        }
        public bool IsProductPanorama_ProductId(int ProductId)
        {
            //convert to data table
            bool issucess = false;
            this.operation = () =>
            {
                PanoramaConfigDao access = new PanoramaConfigDao(this.Transaction);
                issucess = access.IsProductPanorama_ProductId(ProductId);
            };
            this.Start(false);
            return issucess;
        }
    }
}
