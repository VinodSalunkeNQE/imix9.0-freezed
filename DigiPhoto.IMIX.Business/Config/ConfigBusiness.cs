﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using System.Xml.Linq;
using System.Data;
using System.ComponentModel;
using System.Collections;
using System.Reflection;
using System.Globalization;
using DigiPhoto.IMIX.Model;
using DigiPhoto.IMIX.DataAccess;
using DigiPhoto.IMIX.Business;
using DigiPhoto.DataLayer;
using DigiPhoto.Utility.Repository.ValueType;
using DigiPhoto.Cache.MasterDataCache;
using DigiPhoto.Cache.Repository;
using DigiPhoto.Cache.DataCache;
using Digiphoto.Cache.SqlDependentCache;

namespace DigiPhoto.IMIX.Business
{

    public partial class ConfigBusiness : BaseBusiness
    {
        public List<ConfigInfo> GetAllConfigs()
        {
            ICacheRepository oCache = DataCacheFactory.GetFactory<ICacheRepository>(typeof(ConfigCache).FullName);
            return oCache.GetData() as List<ConfigInfo>;

            //return ((List<ConfigInfo>)new ConfigCache().GetData());
        }
        public List<ConfigInfo> GetSubStoreConfigs(int subStoreId)
        {
            ICacheRepository oCache = DataCacheFactory.GetFactory<ICacheRepository>(typeof(ConfigCache).FullName);
            return ((List<ConfigInfo>)oCache.GetData()).FindAll(o => o.SubStoreID == subStoreId);

        }
        public string GetSubStorePasswordPath(int configId, int subStoreId)
        {
            ICacheRepository oCache = DataCacheFactory.GetFactory<ICacheRepository>(typeof(ConfigCache).FullName);
            return ((List<ConfigInfo>)oCache.GetData()).FindAll(o => o.SubStoreID == subStoreId && o.ConfigID == configId).FirstOrDefault().ConfigValue;
        }
        public List<iMIXConfigurationInfo> GetNewConfigValues(int subStoreId)
        {
            //ICacheRepository oCache = DataCacheFactory.GetFactory<ICacheRepository>(typeof(ImixConfigurationCache).FullName);
            //return ((List<iMIXConfigurationInfo>)oCache.GetData()).FindAll(o => o.SubstoreId == subStoreId);

            var oNewConfigList = SqlDepandencyCache.GetNewConfigValues();
            return oNewConfigList.FindAll(o => o.SubstoreId == subStoreId);
        }
        ///changed by latika 
        public List<iMIXRFIDTableWorkflowInfo> GetTableworkFlowinfo(int LocationID)
        {
            //ICacheRepository oCache = DataCacheFactory.GetFactory<ICacheRepository>(typeof(ImixConfigurationCache).FullName);
            //return ((List<iMIXConfigurationInfo>)oCache.GetData()).FindAll(o => o.SubstoreId == subStoreId);

            var oNewConfigList = SqlDepandencyCache.GetTableworkFlowinfo(LocationID);
            return oNewConfigList;
        }
        ///end
        //TBD:Cache
        public ConfigurationInfo GetConfigurationData(int subStoreId)
        {
            var oConfigList = SqlDepandencyCache.GetAllConfigurationSetting();
            if (oConfigList != null)
                return oConfigList.FindAll(o => o.DG_Substore_Id == subStoreId).FirstOrDefault();
            else
                return null;
        }

        public List<ConfigurationInfo> GetConfigurationData()
        {
            var oConfigList = SqlDepandencyCache.GetAllConfigurationSetting();
            return oConfigList;
        }

        //TBD:Cache
        public Dictionary<string, decimal?> GetCromaColor(int SubStoreId)
        {
            Dictionary<string, decimal?> objDic = null;

            ConfigurationInfo objectInfo = this.GetConfigurationData(SubStoreId);

            if (objectInfo != null)
            {
                objDic = new Dictionary<string, decimal?>();
                objDic.Add(objectInfo.DG_ChromaColor, objectInfo.DG_ChromaTolerance);
            }
            return objDic;
        }
        /// <summary>
        /// Added by Manoj at 14-Dec-2018 for Delivery Note Print
        /// </summary>
        /// <param name="subStoreId"></param>
        /// <returns></returns>
        /// 
        public bool? GetDeliveryNoteStatus(int SubstoreId)
        {
            Boolean flag = false;
            this.operation = () =>
            {
                ConfigurationDao access = new ConfigurationDao(this.Transaction);
                flag = access.GetDeliveryNoteStatus(SubstoreId);
            };
            this.Start(false);
            ICacheRepository configCache = DataCacheFactory.GetFactory<ICacheRepository>(typeof(ConfigurationCache).FullName);
            configCache.RemoveFromCache();
            return flag;
        }
        //TBD:Cache
        //public ConfigurationInfo GetConfigurationData(int SubStoreId)
        //{
        //    ConfigurationInfo objectInfo = new ConfigurationInfo();
        //    this.operation = () =>
        //    {
        //        ConfigurationDao access = new ConfigurationDao(this.Transaction);
        //        objectInfo = access.GetConfigurationSetting(SubStoreId);
        //    };
        //    this.Start(false);
        //    return objectInfo;
        //}

        public bool? GetConfigCompression(int SubstoreId)
        {
            try
            {
                ConfigurationInfo _objConfig = this.GetConfigurationData(SubstoreId);
                if (_objConfig.DG_IsCompression != null)
                {
                    return _objConfig.DG_IsCompression;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
                return false;
            }
        }
        //TBD:Cache
        public bool? GetConfigEnableGroup(int SubstoreID)
        {
            try
            {
                ConfigurationInfo _objConfig = this.GetConfigurationData(SubstoreID);
                if (_objConfig.DG_IsEnableGroup != null)
                {
                    return _objConfig.DG_IsEnableGroup;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
                return false;
            }
        }

        /// <summary>
        /// Get image resize/compression level. Between 1-100.
        /// </summary>
        /// <param name="substoreid">User Id</param>
        /// <param name="mediaEnum">6 for Storage Media, 5 for Social Media</param>
        /// <returns></returns>
        //TBD:Cache
        public int GetCompressionLevel(int substoreid, long mediaEnum)
        {
            int val = 0;
            try
            {
                IMIX.Model.iMIXConfigurationInfo _objConfig = this.GetNewConfigValues(substoreid).Where(o => o.IMIXConfigurationMasterId == mediaEnum).FirstOrDefault();
                if (_objConfig != null)
                    val = _objConfig.ConfigurationValue.ToInt32();
                return val;
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
                return val;
            }
        }

        //TBD:Cache
        //public List<iMIXConfigurationInfo> GetNewConfigValues(int SubstoreId)
        //{
        //    try
        //    {
        //        List<iMIXConfigurationInfo> _objConfig = new List<iMIXConfigurationInfo>();
        //        this.operation = () =>
        //        {
        //            ConfigurationDao access = new ConfigurationDao(this.Transaction);
        //            _objConfig = access.GetNewConfigValues(SubstoreId);
        //        };
        //        this.Start(false);
        //        return _objConfig;
        //    }
        //    catch (Exception ex)
        //    {

        //        string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
        //        ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);

        //        return null;
        //        //throw ex;
        //    }
        //}
        public string GetOnlineConfigData(ConfigParams objConfigParams, int SubStoreId)
        {
            try
            {
                iMIXConfigurationInfo _objOrd = this.GetNewConfigValues(SubStoreId).Where(o => o.IMIXConfigurationMasterId == Convert.ToInt64(objConfigParams)).FirstOrDefault();
                if (_objOrd != null)
                    return _objOrd.ConfigurationValue;
                else
                    return string.Empty;
            }
            catch
            {
                return string.Empty;
            }
        }
        public ConfigurationInfo GetDeafultPathData()
        {

            ConfigurationInfo _objConfig = new ConfigurationInfo();
            this.operation = () =>
            {
                ConfigurationDao access = new ConfigurationDao(this.Transaction);
                _objConfig = access.GetDeafultPath();
            };
            this.Start(false);
            return _objConfig;
        }

        public bool SetConfigurationData(string SyncCode, string hotfolder, string framePath, string bgPath, string ModPassword, string graphics, int NumberofImages, bool? Iswatermark, bool? IsHighResolution, bool? isSemiorder, bool? isAutoRotate, bool? IsLineItemDiscount, bool? IsTotalDiscount, bool? IsPosOnOff, string Receipt_PrinterName, int defaultCurrency, bool? IsSemiOrderMain, int substoreId, bool? IsCompression, bool? IsEnableGroup, int NoOfReceipt, string ChromaColor, decimal ChromaTolerance, int PageSizeGrid, int PageSizePreview, int NoOfPhotoIdSearch, bool? isExportReportToAnyDrive)
        {

            this.operation = () =>
            {
                ConfigurationDao access = new ConfigurationDao(this.Transaction);
                access.UPDANDINS_ConfigurationData(NumberofImages, defaultCurrency, substoreId, NoOfReceipt, PageSizeGrid, PageSizePreview, NoOfPhotoIdSearch, Iswatermark,
                    IsHighResolution, isSemiorder, isAutoRotate, IsLineItemDiscount, IsTotalDiscount, IsPosOnOff, Receipt_PrinterName, IsSemiOrderMain, IsCompression, IsEnableGroup, ChromaTolerance, SyncCode,
                    hotfolder, framePath, bgPath, graphics, ChromaColor, ModPassword, isExportReportToAnyDrive);
            };
            this.Start(false);
            SqlDepandencyCache.ClearCacheAllConfigurationSetting();
            ICacheRepository configCache = DataCacheFactory.GetFactory<ICacheRepository>(typeof(ConfigurationCache).FullName);
            configCache.RemoveFromCache();
            return true;
        }

        //public List<iMIXConfigurationInfo> GetNewConfigValues(int SubstoreId)
        //{

        //    List<iMIXConfigurationInfo> _objConfig = new List<iMIXConfigurationInfo>();
        //    this.operation = () =>
        //    {
        //        ConfigurationDao access = new ConfigurationDao(this.Transaction);
        //        _objConfig = access.GetNewConfigValues(SubstoreId);
        //    };
        //    this.Start(false);
        //    return _objConfig;
        //}

        public int GetDeletedOldImagesConfigurationData(string deletedOldImages)
        {
            int id = 0;
            try
            {
                this.operation = () =>
                {
                    ConfigurationDao access = new ConfigurationDao(this.Transaction);
                    id = access.DeletedOldImages_ConfigurationData(deletedOldImages);
                };
                this.Start(false);
                ICacheRepository configCache = DataCacheFactory.GetFactory<ICacheRepository>(typeof(ConfigurationCache).FullName);
                configCache.RemoveFromCache();
                return id;
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogFileWrite("Exception in GetDeletedldImagesConfigurationData()");
                ErrorHandler.ErrorHandler.LogError(ex);
                return id;
            }
        }

        public int SetSemiorderConfigurationData(SemiOrderSettings objSemi)
        {
            int id = 0;
            this.operation = () =>
            {
                ConfigurationDao access = new ConfigurationDao(this.Transaction);
                id = access.UPDANDINS_SemiOrderConfigurationData(objSemi);
            };
            this.Start(false);
            ICacheRepository configCache = DataCacheFactory.GetFactory<ICacheRepository>(typeof(ConfigurationCache).FullName);
            configCache.RemoveFromCache();
            return id;
        }
        public List<iMixConfigurationLocationInfo> GetLocationWiseConfigParams(int SubStoreId)
        {
            List<iMixConfigurationLocationInfo> lstLocationWiseConfigParams = new List<iMixConfigurationLocationInfo>();
            this.operation = () =>
            {
                ConfigurationDao access = new ConfigurationDao(this.Transaction);
                lstLocationWiseConfigParams = access.GetLocationWiseConfigParams(SubStoreId);
            };
            this.Start(false);

            return lstLocationWiseConfigParams;
        }
        public void DeleteLocationWiseConfigParams(int LocationId)
        {
            // List<iMixConfigurationLocationInfo> lstLocationWiseConfigParams = new List<iMixConfigurationLocationInfo>();
            int result;
            this.operation = () =>
            {
                ConfigurationDao access = new ConfigurationDao(this.Transaction);
                access.DeleteLocationWiseConfigParams(LocationId);
            };
            this.Start(false);

            //  return lstLocationWiseConfigParams;
        }
        public bool SaveUpdateNewStoreConfig(List<iMIXStoreConfigurationInfo> lstStoreConfigValue)
        {
            //convert to data table

            DataTable dt = new DataTable();
            dt.Columns.Add("ConfigurationMasterId");
            dt.Columns.Add("ConfigurationValue");
            dt.Columns.Add("SubStroreID");
            foreach (iMIXStoreConfigurationInfo item in lstStoreConfigValue)
            {
                dt.Rows.Add(item.IMIXConfigurationMasterId, item.ConfigurationValue, 0);
            }

            bool issucess = false;
            this.operation = () =>
            {
                ConfigAccess access = new ConfigAccess(this.Transaction);
                issucess = access.SaveUpdateNewStoreConfig(dt);
            };
            this.Start(false);
            //SqlDepandencyCache.ClearCacheNewConfigValues();
            ////Clear the item to update new changes
            //ICacheRepository imixCache = DataCacheFactory.GetFactory<ICacheRepository>(typeof(ImixConfigurationCache).FullName);
            //imixCache.RemoveFromCache();
            //ConfigManager.IMIXConfigurations.Clear();
            return issucess;
        }

        public bool SaveUpdatePreviewDummyTag(iMIXStoreConfigurationInfo objImixStoreInfo)
        {


            bool issucess = false;
            this.operation = () =>
            {
                ConfigAccess access = new ConfigAccess(this.Transaction);
                issucess = access.SaveUpdatePreviewDummyTag(objImixStoreInfo);
            };
            this.Start(false);

            return issucess;
        }

        public bool DeletePreviewDummyTag(long iMIXStoreConfigurationValueId)
        {


            bool issucess = false;
            this.operation = () =>
            {
                ConfigAccess access = new ConfigAccess(this.Transaction);
                issucess = access.DeletePreviewDummyTag(iMIXStoreConfigurationValueId);
            };
            this.Start(false);

            return issucess;
        }

        public ReportConfigurationDetails GetReportConfigurationDetails()
        {
            ReportConfigurationDetails reportConfg = ReportConfigurationDetails.LoadDefaultData();
            try
            {
                var configurationDetail = SqlDepandencyCache.GetAllReportConfiguration();
                if (configurationDetail != null && configurationDetail.Count > 0)
                {
                    var exportPath = configurationDetail.FirstOrDefault(e => e.IMIXConfigurationMasterId == 143);
                    if (exportPath != null)
                    {
                        reportConfg.ExportPath = exportPath.ConfigurationValue;
                    }

                    var scheduleTime = configurationDetail.FirstOrDefault(e => e.IMIXConfigurationMasterId == 144);
                    if (scheduleTime != null)
                    {
                        var scheduleTimeArray = scheduleTime.ConfigurationValue.Split(new char[] { ':', ' ' });
                        if (scheduleTimeArray != null)
                        {
                            reportConfg.SelectedHour = Convert.ToInt32(scheduleTimeArray[0]);
                            reportConfg.SelectedMinute = Convert.ToInt32(scheduleTimeArray[1]);
                            //reportConfg.SelectedFormat = scheduleTimeArray[2].Trim();
                            //reportConfg.ScheduleTime = scheduleTime.ConfigurationValue;                            
                        }

                    }
                    var isRecursive = configurationDetail.FirstOrDefault(e => e.IMIXConfigurationMasterId == 145);
                    if (isRecursive != null)
                    {
                        reportConfg.IsRecursive = isRecursive.ConfigurationValue.ToBoolean();

                    }

                    var emailAddress = configurationDetail.FirstOrDefault(e => e.IMIXConfigurationMasterId == 146);
                    if (emailAddress != null)
                    {
                        reportConfg.EmailAddress = emailAddress.ConfigurationValue;

                    }

                }

                reportConfg.ReportTypeDetails = GetRequestedReports();
                reportConfg.ExportServiceLogs = GetExportServiceLogs();

            }
            catch (Exception ex)
            {
                reportConfg.HasError = true;
                reportConfg.ErrorDetails = ex.Message + '\n' + ex.StackTrace;
                reportConfg.ErrorMessage = "Exception occured";
            }
            return reportConfg;
        }
        public List<ReportTypeDetails> GetReport()
        {
            List<ReportTypeDetails> _objConfig = new List<ReportTypeDetails>();
            this.operation = () =>
            {
                ConfigurationDao access = new ConfigurationDao(this.Transaction);
                _objConfig = access.GetReportTypes();
            };
            this.Start(false);
            return _objConfig;
        }
        public List<ReportTypeDetails> GetActiveReports()
        {
            var reports = GetReport();
            return reports.Where(rpt => rpt.IsActive).ToList();
        }

        public bool InsertReportConfig(ReportConfigurationDetails reportConfigurationDetails)
        {
            Boolean flag = false;
            List<iMIXStoreConfigurationInfo> configurationEntityList = new List<iMIXStoreConfigurationInfo>();

            iMIXStoreConfigurationInfo configurationEntity = new iMIXStoreConfigurationInfo();
            configurationEntity.IMIXConfigurationMasterId = Convert.ToInt32(ConfigParams.ExportPath);
            configurationEntity.ConfigurationValue = reportConfigurationDetails.ExportPath;
            configurationEntityList.Add(configurationEntity);

            configurationEntity = new iMIXStoreConfigurationInfo();
            configurationEntity.IMIXConfigurationMasterId = Convert.ToInt32(ConfigParams.ScheduledTime);
            configurationEntity.ConfigurationValue = reportConfigurationDetails.ScheduleTime;
            configurationEntityList.Add(configurationEntity);

            configurationEntity = new iMIXStoreConfigurationInfo();
            configurationEntity.IMIXConfigurationMasterId = Convert.ToInt32(ConfigParams.IsServiceRecursive);
            configurationEntity.ConfigurationValue = reportConfigurationDetails.IsRecursive.ToString();
            configurationEntityList.Add(configurationEntity);

            configurationEntity = new iMIXStoreConfigurationInfo();
            configurationEntity.IMIXConfigurationMasterId = Convert.ToInt32(ConfigParams.ExportReportEmailAddress);
            configurationEntity.ConfigurationValue = reportConfigurationDetails.EmailAddress;
            configurationEntityList.Add(configurationEntity);


            DataTable dt = new DataTable();
            dt.Columns.Add("ConfigurationMasterId");
            dt.Columns.Add("ConfigurationValue");
            dt.Columns.Add("SubstoreId");



            foreach (iMIXStoreConfigurationInfo item in configurationEntityList)
            {
                dt.Rows.Add(item.IMIXConfigurationMasterId, item.ConfigurationValue, 0);
            }

            this.operation = () =>
            {
                ConfigurationDao access = new ConfigurationDao(this.Transaction);
                flag = access.InsertReportConfig(dt);
            };
            this.Start(false);
            UpdateReportTypes(reportConfigurationDetails.ReportTypeDetails);
            return flag;

        }

        public bool SaveExportReportLogDetails(ExportServiceLog exportReportLogDetails)
        {
            Boolean flag = false;


            this.operation = () =>
            {
                ConfigurationDao access = new ConfigurationDao(this.Transaction);
                flag = access.SaveExportReportLogDetails(exportReportLogDetails);
            };
            this.Start(false);

            return flag;

        }

        public List<ExportServiceLog> GetExportServiceLogs()
        {
            List<ExportServiceLog> _objConfig = new List<ExportServiceLog>();
            this.operation = () =>
            {
                ConfigurationDao access = new ConfigurationDao(this.Transaction);
                _objConfig = access.GetExportServiceLogs();
            };
            this.Start(false);
            return _objConfig;

        }

        public DataTable ParseListIntoPaymentDataTable(List<PaymentSummaryInfo> paymentSummaryInfo)
        {
            DataTable ReportSource = new DataTable();
            if (paymentSummaryInfo != null && paymentSummaryInfo.Count > 0)
            {

                var reportParams = new ReportBusiness().FetchReportFormatDetails((int)ReportTypes.PaymentSummary);

                foreach (var param in reportParams.ReportFormats)
                {
                    ReportSource.Columns.Add(new DataColumn(param.Key));
                }
                var venueName = paymentSummaryInfo.DistinctBy(st => st.StoreName).FirstOrDefault().StoreName;
                var sitenames = paymentSummaryInfo.Where(st => st.StoreName == venueName).DistinctBy(ps => ps.SubStoreName).Select(ss => ss.SubStoreName).ToList();
                #region Store wise
                var storeProducts = paymentSummaryInfo.Where(st => st.StoreName == venueName).DistinctBy(ps => ps.Productcode).Select(ss => ss.Productcode).ToList();
                string storesiteNames = sitenames.Aggregate((i, j) => i + "," + j);
                string storeproductNames = storeProducts.Aggregate((i, j) => i + "," + j);
                var venueData = paymentSummaryInfo.Where(ps => ps.StoreName == venueName).FirstOrDefault();
                var storeccAggregate = venueData.StoreCardSale;
                var storeCashSale = venueData.StoreCashSale;
                var storediscountedSale = venueData.StoreDiscountSale;
                var storeFC = venueData.FC;
                var storeRoomCharge = venueData.StoreRoomChargeSale;
                var soterKVL = venueData.KVLStoreTransactionAmount;
                var storeVoidCC = venueData.StoreCardSaleVoid;
                var storeVoidCash = venueData.StoreCashSaleVoid;
                var storeVoidDiscountedVouchers = venueData.StoreDiscountSaleVoid;
                var storeVoidFC = venueData.FCVoid;
                var storeVoidRoomCharge = venueData.StoreRoomChargeSaleVoid;
                var storeVoidKVL = venueData.KVLStoreTransactionAmountVoid;
                var storeTotalNoofTransactions = venueData.TotalStoreSaleTransactionCount;
                var stroreTotalNoofVoidTransactions = venueData.StoreVoidSaleTransactionCount;
                DataRow dr = ReportSource.NewRow();
                dr["Venue Name"] = venueName;
                dr["Site Name"] = storesiteNames;
                dr["Products"] = storeproductNames;
                dr["CC"] = storeccAggregate;
                dr["Cash"] = storeCashSale;
                dr["Discounted Vouchers"] = storediscountedSale;
                dr["FC"] = storeFC;
                dr["Room Charge"] = storeRoomCharge;
                dr["KVL"] = soterKVL;
                dr["Total"] = storeccAggregate + storeCashSale + storediscountedSale + storeFC + storeRoomCharge + soterKVL;
                dr["Void CC"] = storeVoidCC;
                dr["Void Cash"] = storeVoidCash;
                dr["Void Discounted Vouchers"] = storeVoidDiscountedVouchers;
                dr["Void FC"] = storeVoidFC;
                dr["Void Room Charge"] = storeVoidRoomCharge;
                dr["Void KVL"] = storeVoidKVL;
                dr["Total Void"] = storeVoidCC + storeVoidCash + storeVoidDiscountedVouchers + storeVoidFC + storeVoidRoomCharge + storeVoidKVL;
                dr["Net"] = (storeccAggregate + storeCashSale + storediscountedSale + storeFC + storeRoomCharge + soterKVL)
                    - (storeVoidCC + storeVoidCash + storeVoidDiscountedVouchers + storeVoidFC + storeVoidRoomCharge + storeVoidKVL);
                dr["Total No. of Transactions"] = storeTotalNoofTransactions;
                dr["Total No. of Void Transactions"] = stroreTotalNoofVoidTransactions;
                dr["Net Transactions Count"] = storeTotalNoofTransactions - stroreTotalNoofVoidTransactions;
                ReportSource.Rows.Add(dr);
                #endregion
                #region sub store wise
                foreach (var site in sitenames)
                {
                    var siteCollection = paymentSummaryInfo.Where(st => st.StoreName == venueName && st.SubStoreName == site).ToList();
                    var siteData = siteCollection.FirstOrDefault();
                    var products = siteCollection.DistinctBy(ps => ps.Productcode).Select(ss => ss.Productcode).ToList();
                    string productNamesConcat = products.Aggregate((i, j) => i + "," + j);
                    var siteccAggregate = siteData.SubStoreCardSale;
                    var siteCashSale = siteData.SubStoreCashSale;
                    var siteDiscountedSale = siteData.SubStoreDiscountSale;
                    var siteFC = siteData.SubStoreFC;
                    var siteRoomCharge = siteData.SubStoreRoomChargeSale;
                    var siteKVL = siteData.SubStoreKVLTransactionAmount;
                    var siteVoidCC = siteData.SubStoreCardSaleVoid;
                    var siteVoidCash = siteData.SubStoreCashSaleVoid;
                    var siteVoidDiscountedVouchers = siteData.SubStoreDiscountSaleVoid;
                    var siteVoidFC = siteData.SubStoreFCVoid;
                    var siteVoidRoomCharge = siteData.SubStoreRoomChargeSaleVoid;
                    var siteVoidKVL = siteData.SubStoreKVLTransactionAmountVoid;
                    var siteTotalNoofTransactions = siteData.TotalSubStoreSaleTransactionCount;
                    var siteTotalNoofVoidTransactions = siteData.SubStoreVoidSaleTransactionCount;
                    dr = ReportSource.NewRow();
                    dr["Venue Name"] = venueName;
                    dr["Site Name"] = site;
                    dr["Products"] = productNamesConcat;
                    dr["CC"] = siteccAggregate;
                    dr["Cash"] = siteCashSale;
                    dr["Discounted Vouchers"] = siteDiscountedSale;
                    dr["FC"] = siteFC;
                    dr["Room Charge"] = siteRoomCharge;
                    dr["KVL"] = siteKVL;
                    dr["Total"] = siteccAggregate + siteCashSale + siteDiscountedSale + siteFC + siteRoomCharge + siteKVL;
                    dr["Void CC"] = siteVoidCC;
                    dr["Void Cash"] = siteVoidCash;
                    dr["Void Discounted Vouchers"] = siteVoidDiscountedVouchers;
                    dr["Void FC"] = siteVoidFC;
                    dr["Void Room Charge"] = siteVoidRoomCharge;
                    dr["Void KVL"] = siteVoidKVL;
                    dr["Total Void"] = siteVoidCC + siteVoidCash + siteVoidDiscountedVouchers + siteVoidFC + siteVoidRoomCharge + siteVoidKVL;

                    dr["Net"] = (siteccAggregate + siteCashSale + siteDiscountedSale + siteFC + siteRoomCharge + siteKVL)
                       - (siteVoidCC + siteVoidCash + siteVoidDiscountedVouchers + siteVoidFC + siteVoidRoomCharge + siteVoidKVL);
                    dr["Total No. of Transactions"] = siteTotalNoofTransactions;
                    dr["Total No. of Void Transactions"] = siteTotalNoofVoidTransactions;
                    dr["Net Transactions Count"] = siteTotalNoofTransactions - siteTotalNoofVoidTransactions;
                    ReportSource.Rows.Add(dr);

                }
                #endregion
            }
            return ReportSource;
        }
        public DataTable CreateReportData(DataTable dt, DigiPhoto.DataLayer.ReportTypes reportType)
        {
            DataTable ReportSource = new DataTable();
            string dataValue = string.Empty;
            double dbValue = 0d;
            string finalValue = string.Empty;
            var reportParams = new ReportBusiness().FetchReportFormatDetails((int)reportType);
            ///string columnName = "productname";
            foreach (var param in reportParams.ReportFormats)
            {
                ReportSource.Columns.Add(new DataColumn(param.Key));
            }

            foreach (DataRow dr in dt.Rows)
            {
                bool canceled = false;
                try
                {
                    var productName = Convert.ToString(dr["ProductName"]);
                    if (productName.ToLower().Contains("(c)"))
                        canceled = true;
                }
                catch
                {
                    canceled = false;
                }

                DataRow newRow = ReportSource.NewRow();
                foreach (var param in reportParams.ReportFormats)
                {
                    var columnNames = param.Value.Split('~');
                    finalValue = string.Empty;
                    dbValue = 0d;
                    dataValue = string.Empty;
                    if (columnNames.Length > 1)
                    {

                        foreach (var col in columnNames)
                        {
                            finalValue = dr[col].ToString();



                            if (double.TryParse(finalValue, out dbValue))
                            {

                                finalValue = dbValue.ToString("0.00");
                                if (canceled)
                                    finalValue = "-" + finalValue;
                            }
                            if (string.IsNullOrEmpty(dataValue))
                                dataValue = finalValue;
                            else
                                dataValue = dataValue + " " + finalValue;
                        }
                        newRow[param.Key] = dataValue;
                    }
                    else
                    {

                        finalValue = dr[param.Value].ToString();

                        if (double.TryParse(finalValue, out dbValue))
                        {
                            finalValue = dbValue.ToString("0.00");
                            if (canceled)
                                finalValue = "-" + finalValue;
                        }
                        newRow[param.Key] = finalValue;
                    }
                }
                ReportSource.Rows.Add(newRow);
            }
            return ReportSource;
        }
        public void UpdateReportTypes(List<ReportTypeDetails> reportTypes)
        {
            this.operation = () =>
            {
                ConfigurationDao access = new ConfigurationDao(this.Transaction);
                access.UpdateReportTypes(reportTypes);
            };
            this.Start(false);

        }

        public List<ReportTypeDetails> GetRequestedReports()
        {

            var reportTypes = GetReport();
            var reportTypedetails = Enum.GetNames(typeof(ReportTypes));
            var requestedReportTypes = (from rpt in reportTypes
                                        where reportTypedetails.Contains(rpt.ReportTypeName)
                                        select new ReportTypeDetails { Id = rpt.Id, IsActive = rpt.IsActive, ReportLabel = rpt.ReportLabel, ReportTypeName = rpt.ReportTypeName }).ToList();
            return requestedReportTypes;
        }

        public string GetSubStoreNameBySuubStoreId(int subStoreId)
        {
            ErrorHandler.ErrorHandler.LogFileWrite("Get SubstoreName By Sub store id : 728  ");
            string subStoreName = string.Empty;
            this.operation = () =>
            {
                ConfigurationDao access = new ConfigurationDao(this.Transaction);
                subStoreName = access.GetSubStoreNameBySuubStoreId(subStoreId);
            };
            this.Start(false);
            ErrorHandler.ErrorHandler.LogFileWrite("Get SubstoreName By Sub store id : 736  " + subStoreName);
            return subStoreName;
        }

        public void DeleteLocationWiseConfigParamsGumbleRide(int LocationId)
        {
            int result;
            this.operation = () =>
            {
                ConfigurationDao access = new ConfigurationDao(this.Transaction);
                access.DeleteLocationWiseConfigParamsGumbleRide(LocationId);
            };
            this.Start(false);
        }
        public bool IsLocationRFIDEnabled(int locationId)
        {
            bool result = false;
            this.operation = () =>
            {
                ConfigurationDao access = new ConfigurationDao(this.Transaction);
                result = access.IsLocationRFIDEnabled(locationId);
            };
            this.Start(false);
            return result;
        }

        public List<iMixConfigurationLocationInfo> GetAllLocationWiseConfigParams()
        {
            List<iMixConfigurationLocationInfo> lstLocationWiseConfigParams = new List<iMixConfigurationLocationInfo>();
            this.operation = () =>
            {
                ConfigurationDao access = new ConfigurationDao(this.Transaction);
                lstLocationWiseConfigParams = access.GetAllLocationWiseConfigParams();
            };
            this.Start(false);

            return lstLocationWiseConfigParams;
        }

        public List<ProductTypeInfo> GetWaterMarkProduct()
        {
            List<ProductTypeInfo> lstProductInfo = new List<ProductTypeInfo>();
            this.operation = () =>
            {
                ConfigurationDao access = new ConfigurationDao(this.Transaction);
                lstProductInfo = access.GetWaterMarkProduct();
            };
            this.Start(false);

            return lstProductInfo;
        }

        public List<iMixConfigurationLocationInfo> GetLocationWiseConfigParams()
        {
            List<iMixConfigurationLocationInfo> lstLocationWiseConfigParams = new List<iMixConfigurationLocationInfo>();
            this.operation = () =>
            {
                ConfigurationDao access = new ConfigurationDao(this.Transaction);
                lstLocationWiseConfigParams = access.GetLocationWiseConfigParams();
            };
            this.Start(false);

            return lstLocationWiseConfigParams;
        }

        public List<WaterMarkTagsUpload> GetCardIdentifier(int locationId)
        {
            List<WaterMarkTagsUpload> lstLocationWiseConfigParams = new List<WaterMarkTagsUpload>();
            this.operation = () =>
            {
                ConfigurationDao access = new ConfigurationDao(this.Transaction);
                lstLocationWiseConfigParams = access.GetCardIdentifier(locationId);
            };
            this.Start(false);

            return lstLocationWiseConfigParams;
        }
        public bool UpdateOrderStatus(string IMIXImageAssociationIds)
        {
            bool result = false;
            this.operation = () =>
            {
                ConfigurationDao access = new ConfigurationDao(this.Transaction);
                result = access.UpdateOrderStatus(IMIXImageAssociationIds);
            };
            this.Start(false);
            return result;
        }
        public List<ManualDownloadPosLocationAssociation> GetPosLocationMapping(string machineName)
        {
            List<ManualDownloadPosLocationAssociation> lstPosLocationAssociation = new List<ManualDownloadPosLocationAssociation>();
            this.operation = () =>
            {
                ConfigurationDao access = new ConfigurationDao(this.Transaction);
                lstPosLocationAssociation = access.GetPosLocationMapping(machineName);
            };
            this.Start(false);

            return lstPosLocationAssociation;
        }
        ///////aded by latika for table workflow
        public void DeleteTableFlowByID(int LocationId)
        {
            int result;
            this.operation = () =>
            {
                ConfigurationDao access = new ConfigurationDao(this.Transaction);
                access.DeleteTableFlowByID(LocationId);
            };
            this.Start(false);
        }
        ////end
        /// /////////////change made by latika for getDBdatetime
        public string GetDBDatetime()
        {
            string strDatetime = string.Empty;
            this.operation = () =>
            {
                ConfigurationDao access = new ConfigurationDao(this.Transaction);
                strDatetime = access.GetDBDateTime();
            };
            this.Start(false);
            return strDatetime;
        }
        ////////////end
    }
}
