﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using DigiPhoto.DataLayer.Model;
using System.Xml.Linq;
using System.Data;
using System.ComponentModel;
using System.Collections;
using System.Reflection;
using System.Globalization;
using DigiPhoto.IMIX.Model;
using DigiPhoto.IMIX.DataAccess;
using DigiPhoto.IMIX.Business;
using DigiPhoto.DataLayer;

namespace DigiPhoto.IMIX.Business
{

    public partial class RolePermissionsBusniess : BaseBusiness
    {

        public List<PermissionRoleInfo> GetPermissionData(int RoleId)
        {
            try
            {
                List<PermissionRoleInfo> objList = new List<PermissionRoleInfo>();
                this.operation = () =>
                {
                    RolePermissionDao access = new RolePermissionDao(this.Transaction);
                    objList = access.SelectRolePermissions(RoleId);
                };
                this.Start(false);
                return objList;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public List<RoleInfo> GetRoleNames(int RoleId, string RoleName)
        {
            try
            {
                List<RoleInfo> objectList = new List<RoleInfo>();
                this.operation = () =>
                {
                    RoleDao access = new RoleDao(this.Transaction);
                    objectList = access.SelectRole(RoleId, RoleName);
                };
                this.Start(false);
                return objectList;
            }
            catch (Exception ex)
            {
                return null;
            }




        }

        public List<PermissionInfo> GetPermissionNames(string PermissionId, string PermissionName)
        {
            try
            {
                List<PermissionInfo> objList = new List<PermissionInfo>();
                this.operation = () =>
                {
                    RolePermissionDao access = new RolePermissionDao(this.Transaction);
                    objList = access.SelectPermission(PermissionId, PermissionName); //TBD All GetPermission
                };
                this.Start(false);
                return objList;
            }
            catch (Exception ex)
            {
                return null;
            }
        }


        public string AddUpdateRoleData(int RoleId, string RoleName, string SyncCode,int ParentRoleId)
        {
            try
            {
                int returnval = 0;
                string strResult = "";
                List<PermissionInfo> objList = new List<PermissionInfo>();
                this.operation = () =>
                {
                    RoleDao access = new RoleDao(this.Transaction);
                    returnval = access.UPDANDINS_User_Roles(RoleId, false, RoleName, SyncCode, ParentRoleId);
                };
                this.Start(false);
                if (returnval == 0) { strResult = "Duplicate"; }
                else if (returnval > 0) { strResult = "Saved"; }
                else if (returnval < 0) { strResult = "Error"; }
                return strResult;
            }
            catch (Exception ex)
            {
                if (ex.InnerException.Message.Contains("insert duplicate key"))
                {
                    return "Duplicate";
                }
                else
                {
                    return ex.Message;
                }
            }

        }

        public bool DeleteRoleData(int RoleId)
        {
            try
            {
                bool result = false;
                this.operation = () =>
                {
                    RoleDao access = new RoleDao(this.Transaction);
                    result = access.DeleteRoleData(RoleId);
                };
                this.Start(false);
                return result;

            }
            catch (Exception ex)
            {
                return false;
            }
        }
        public string GetRoleName(int roleID)
        {
            List<RoleInfo> _objRole = new List<RoleInfo>();
            this.operation = () =>
            {
                RoleDao access = new RoleDao(this.Transaction);
                _objRole = access.SelectRole(roleID, string.Empty);
            };
            this.Start(false);
            if (_objRole != null)
            {
                return _objRole.FirstOrDefault().DG_User_Role;
            }
            return "No Role";

        }

        public bool SetPermissionData(int RoleId, int PermissonId)
        {
            bool retvalue = false;
            PermissionRoleInfo _objnew = new PermissionRoleInfo();
            this.operation = () =>
            {
                RolePermissionDao access = new RolePermissionDao(this.Transaction);
                retvalue = access.InsertPermissionData(RoleId, PermissonId, retvalue);
            };
            this.Start(false);
            return retvalue;
        }



        public bool RemovePermissionData(int RoleId, int PermissonId)
        {

            bool retvalue = false;
            this.operation = () =>
                    {
                        RolePermissionDao access = new RolePermissionDao(this.Transaction);
                        retvalue = access.RemovePermissionData(RoleId, PermissonId);

                    };
            this.Start(false);
            return retvalue;

        }
        public bool SetremovePermissionData(DataTable udt_Permission)
        {
            bool result = false;
            this.operation = () =>
            {
                RolePermissionDao access = new RolePermissionDao(this.Transaction);
                result = access.SetremovePermissionData(udt_Permission,result);
            };
            this.Start(false);

            return result;
        }
        public List<RoleInfo> GetChildUserData(int UserId)
        {
            List<RoleInfo> objList = new List<RoleInfo>();
            this.operation = () =>
            {
                RolePermissionDao access = new RolePermissionDao(this.Transaction);
                objList = access.GetChildUserData(UserId);
            };
            this.Start(false);
            return objList;
        }
    }
}
