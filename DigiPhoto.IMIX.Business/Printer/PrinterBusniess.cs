﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using DigiPhoto.DataLayer.Model;
using System.Xml.Linq;
using System.Data;
using System.ComponentModel;
using System.Collections;
using System.Reflection;
using System.Globalization;
using DigiPhoto.IMIX.Model;
using DigiPhoto.IMIX.DataAccess;
using DigiPhoto.IMIX.Business;
using DigiPhoto.DataLayer;
using System.Collections.ObjectModel;

namespace DigiPhoto.IMIX.Business
{
    public partial class PrinterBusniess : BaseBusiness
    {
        public void ReadyForPrint(int QueueId)
        {
            try
            {
                PrinterQueueInfo objectInfo = new PrinterQueueInfo();
                this.operation = () =>
                {
                    PrinterQueueDao access = new PrinterQueueDao(this.Transaction);
                    access.UpdateReadyForPrint(QueueId);
                };
                this.Start(false);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void SetPrinterQueue(int QueueId)
        {
            try
            {
                PrinterQueueInfo _objnew = new PrinterQueueInfo();
                _objnew.DG_SentToPrinter = true;
                this.operation = () =>
                {
                    PrinterQueueDao access = new PrinterQueueDao(this.Transaction);
                    access.UpdatePrinterQueue(QueueId);
                };
                this.Start(false);
            }
            catch (Exception ex)
            {

            }
        }
        public bool IsReadyForPrint(int QueueID)
        {
            try
            {
                PrinterQueueInfo objectInfo = new PrinterQueueInfo();
                this.operation = () =>
                {
                    PrinterQueueDao access = new PrinterQueueDao(this.Transaction);
                    objectInfo = access.GetPrinterQueueIsReadyForPrint(QueueID, false);
                };
                this.Start(false);
                if (objectInfo != null && !(bool)objectInfo.is_Active)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        public List<AssociatedPrintersInfo> GetAssociatedPrintersforPrint(int? ProductType, int SubStoreID)
        {
            try
            {
                List<AssociatedPrintersInfo> objList = new List<AssociatedPrintersInfo>();
                this.operation = () =>
                {
                    PrinterQueueDao access = new PrinterQueueDao(this.Transaction);
                    objList = access.SelectAssociatedPrintersforPrint(ProductType, SubStoreID);
                };
                this.Start(false);
                return objList;

            }
            catch (Exception ex)
            {
                return null;
            }

        }

        public List<PrinterQueueforPrint> GetPrinterQueue(int substoreID, ref string productypeId)
        {
            try
            {
                string ptypeid = productypeId;
                List<PrinterQueueforPrint> objPrinterQueue = new List<PrinterQueueforPrint>();
                this.operation = () =>
                {
                    PrinterQueueDao access = new PrinterQueueDao(this.Transaction);
                    objPrinterQueue = access.SelectPrinterQueue(substoreID);
                };
                this.Start(false);

                if (string.IsNullOrEmpty(productypeId))
                {
                    var firstitemlist = objPrinterQueue.ToList().Where(t => t.DG_Order_SubStoreId == substoreID);
                    if (firstitemlist != null)
                    {
                        if (firstitemlist.Count() > 0)
                        {
                            var item = firstitemlist.OrderBy(t => t.QueueIndex).FirstOrDefault();
                            if (item != null)
                                productypeId = item.DG_Orders_ProductType_pkey.ToString();
                            else
                                productypeId = "";
                            return firstitemlist.OrderBy(t => t.QueueIndex).ToList();
                        }
                        else
                        {
                            productypeId = "";
                            return new List<PrinterQueueforPrint>();
                        }
                    }
                    else
                    {
                        productypeId = "";
                        return new List<PrinterQueueforPrint>();
                    }
                }
                else
                {
                    List<string> _objstr = new List<string>();
                    _objstr = productypeId.Split(',').ToList();
                    var itemlist = objPrinterQueue.ToList().Where(t => t.DG_Order_SubStoreId == substoreID && !_objstr.Contains(t.DG_Orders_ProductType_pkey.ToString()));

                    if (itemlist != null)
                    {
                        if (itemlist.Count() > 0)
                        {

                            productypeId = productypeId + "," + itemlist.OrderBy(t => t.DG_PrinterQueue_Pkey).FirstOrDefault().DG_Orders_ProductType_pkey.ToString();
                            return itemlist.OrderBy(t => t.QueueIndex).ToList();
                        }
                        else
                        {
                            productypeId = "";
                            return new List<PrinterQueueforPrint>();
                        }
                    }
                    else
                    {
                        var otheritem = objPrinterQueue.ToList().Where(t => t.DG_Order_SubStoreId == substoreID && !ptypeid.Contains(t.DG_Orders_ProductType_pkey.ToString()));
                        if (otheritem != null)
                        {
                            if (otheritem.Count() > 0)
                            {
                                productypeId = productypeId + "," + otheritem.OrderBy(t => t.QueueIndex).FirstOrDefault().DG_Orders_ProductType_pkey.ToString();
                                return otheritem.OrderBy(t => t.QueueIndex).ToList();
                            }
                            else
                            {
                                productypeId = "";
                                return new List<PrinterQueueforPrint>();
                            }
                        }
                        else
                        {
                            productypeId = "";
                            return new List<PrinterQueueforPrint>();
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                return new List<PrinterQueueforPrint>();
            }
        }
        public PrinterQueueforPrint GetPrinterQueueforPrint(int SubStoreID, ref List<int> ProducttypeId)
        {
            try
            {
                if (ProducttypeId.Count == 0)
                {
                    List<PrinterQueueforPrint> objFileterdPrinterQueue = new List<PrinterQueueforPrint>();
                    this.operation = () =>
                    {
                        PrinterQueueDao access = new PrinterQueueDao(this.Transaction);
                        objFileterdPrinterQueue = access.SelectFilteredPrinterQueueforPrint(SubStoreID);
                    };
                    this.Start(false);
                    var itemlist = objFileterdPrinterQueue.ToList().Where(t => t.DG_Order_SubStoreId == SubStoreID);
                    //
                    if (itemlist != null)
                    {
                        if (itemlist.Count() > 0)
                        {
                            var trtitem = itemlist.OrderBy(t => t.QueueIndex).FirstOrDefault();
                            ProducttypeId.Add(trtitem.DG_Orders_ProductType_pkey);
                            return trtitem;
                        }
                        else
                        {
                            return new PrinterQueueforPrint();
                        }
                    }
                    else
                    {
                        return new PrinterQueueforPrint();
                    }

                }
                else
                {

                    List<PrinterQueueforPrint> objFileterdPrinterQueue = new List<PrinterQueueforPrint>();
                    this.operation = () =>
                    {
                        PrinterQueueDao access = new PrinterQueueDao(this.Transaction);
                        objFileterdPrinterQueue = access.SelectFilteredPrinterQueueforPrint(SubStoreID);
                    };
                    this.Start(false);
                    List<int> _ptypeId = ProducttypeId;
                    if (ProducttypeId != null)
                    {
                        var itemlist = objFileterdPrinterQueue.ToList().Where(t => t.DG_Order_SubStoreId == SubStoreID
                            && !_ptypeId.Contains(t.DG_Orders_ProductType_pkey));
                        //
                        if (itemlist != null)
                        {
                            if (itemlist.Count() > 0)
                            {
                                var trtitem = itemlist.OrderBy(t => t.QueueIndex).FirstOrDefault();
                                if (trtitem != null)
                                {
                                    ProducttypeId.Add(trtitem.DG_Orders_ProductType_pkey);
                                    return trtitem;
                                }
                                else
                                {
                                    ProducttypeId = new List<int>();
                                    return new PrinterQueueforPrint();
                                }
                            }
                            else
                            {
                                ProducttypeId = new List<int>();
                                return new PrinterQueueforPrint();
                            }
                        }
                        else
                        {
                            ProducttypeId = new List<int>();
                            return new PrinterQueueforPrint();
                        }
                    }
                    else
                    {
                        ProducttypeId = new List<int>();
                        return new PrinterQueueforPrint();
                    }


                }
            }
            catch (Exception ex)
            {
                return new PrinterQueueforPrint();
                throw ex;
            }

        }
        public List<PrinterQueueDetailsInfo> GetPrinterQueueDetails(string ordernumer)
        {
            try
            {

                List<PrinterQueueDetailsInfo> objQueueDetails = new List<PrinterQueueDetailsInfo>();
                this.operation = () =>
                {
                    PrinterQueueDao access = new PrinterQueueDao(this.Transaction);
                    objQueueDetails = access.GetPrinterQueueDetailsByOrderNo(ordernumer);
                };
                this.Start(false);
                return objQueueDetails;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public void SetPrinterLog(int PhotoId, int UserId, int ProductTypeId)
        {
            CustomBusineses objBuiness = new CustomBusineses();
            PrintLogInfo _objnew = new PrintLogInfo();
            _objnew.PhotoId = PhotoId;
            _objnew.UserID = UserId;
            _objnew.ProductTypeId = ProductTypeId;
            _objnew.PrintTime = DateTime.Now;

            this.operation = () =>
            {
                PrinterQueueDao access = new PrinterQueueDao(this.Transaction);
                access.InsertPrinterLog(_objnew); //TBD
            };
            this.Start(false);
        }
        public bool SetDataToPrinterQueue(string PhotoId, int? ProductTypeId, int? DG_AssocatedPrinterId, bool DG_SentToPrinter, int orderDetailID)
        {
            try
            {
                PrinterQueueInfo _objnew = new PrinterQueueInfo();
                bool result = true;
                this.operation = () =>
                {
                    PrinterQueueDao access = new PrinterQueueDao(this.Transaction);
                    result = access.InsDataToPrinterQueue(PhotoId, ProductTypeId, DG_AssocatedPrinterId, DG_SentToPrinter, orderDetailID);
                };
                this.Start(false);
                return true;

            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public int GetAssociatedPrinterIdFromProductTypeId(int? ProductTypeId)
        {
            try
            {
                AssociatedPrintersInfo objInfo = new AssociatedPrintersInfo();
                this.operation = () =>
                {
                    AssociatedPrintersDao access = new AssociatedPrintersDao(this.Transaction);
                    objInfo = access.GetAssociatedPrinterIdFromPRoductTypeId(ProductTypeId);
                };
                this.Start(false);

                if (objInfo != null)
                {
                    return objInfo.DG_AssociatedPrinters_Pkey;
                }
                else
                {
                    return 0;
                }

            }
            catch (Exception eX)
            {
                return 0;
            }
        }

        public List<AssociatedPrintersInfo> GetAssociatedPrintersName(int substoreID)
        {
            try
            {
                List<AssociatedPrintersInfo> objectList = new List<AssociatedPrintersInfo>();
                this.operation = () =>
                {
                    AssociatedPrintersDao access = new AssociatedPrintersDao(this.Transaction);
                    objectList = access.Select(substoreID);
                };
                this.Start(false);
                return objectList;

            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public string GetPrinterNameFromID(int id)
        {
            AssociatedPrintersInfo _objAssPrn = new AssociatedPrintersInfo();
            this.operation = () =>
            {
                AssociatedPrintersDao access = new AssociatedPrintersDao(this.Transaction);
                _objAssPrn = access.GetAssociatedPrintersByKey(id);// TBD  get by DG_AssociatedPrinters_Pkey not
            };
            this.Start(false);
            if (_objAssPrn != null)
            {
                return _objAssPrn.DG_AssociatedPrinters_Name + "#" + _objAssPrn.DG_AssociatedPrinters_IsActive + "#" + _objAssPrn.DG_AssociatedPrinters_ProductType_ID + "#" + _objAssPrn.DG_AssociatedPrinters_PaperSize;
            }
            else
            {
                return null;
            }
        }
        public bool DeletePrinter(int id)
        {
            this.operation = () =>
             {
                 AssociatedPrintersDao access = new AssociatedPrintersDao(this.Transaction);
                 var _objprinters = access.Delete(id);
             };
            this.Start(false);
            return true;
        }
        public int AddImageToPrinterQueue(int ProductTypeId, List<string> Images, int OrderDetailedKey, bool IsBundled, bool GreenSpecPrint, List<PhotoPrintPositionDic> PhotoPrintPositionDicList, int MasterTemplateId = 1)
        {

            ProductBusiness prdBiz = new ProductBusiness();
            int Printerid = this.GetAssociatedPrinterIdFromProductTypeId(ProductTypeId);

            int result = -1;
            if (!IsBundled)
            {
                if (ProductTypeId == 98)
                {
                    if (Images.Count % 2 == 1)
                    {
                        Images.Add(Images[Images.Count - 1]);
                    }
                    int counter = 0;
                    int count = Images.Count;
                    while (counter <= count / 2)
                    {
                        string[] objImg = Images.ToArray();
                        string str = Images.ToArray()[counter] + "," + Images.ToArray()[counter + 1];

                        PrinterQueueInfo objqueue = new PrinterQueueInfo();
                        objqueue.DG_PrinterQueue_Image_Pkey = string.Join(",", Images.ToArray());
                        objqueue.DG_PrinterQueue_Image_Pkey = str;
                        objqueue.DG_Associated_PrinterId = Printerid;
                        objqueue.DG_PrinterQueue_ProductID = ProductTypeId;
                        objqueue.is_Active = true;
                        objqueue.DG_SentToPrinter = false;
                        objqueue.DG_Order_Details_Pkey = OrderDetailedKey;
                        objqueue.DG_IsSpecPrint = GreenSpecPrint;
                        objqueue.DG_Print_Date = DateTime.Now;
                        PrinterQueueInfo itemlist = new PrinterQueueInfo();
                        this.operation = () =>
                        {
                            PrinterQueueDao access = new PrinterQueueDao(this.Transaction);
                            itemlist = access.Get();
                        };
                        this.Start(false);

                        if (itemlist != null)
                        {
                            objqueue.QueueIndex = itemlist.QueueIndex + 1;
                        }
                        else
                        {
                            objqueue.QueueIndex = 1;
                        }

                        this.operation = () =>
                        {
                            PrinterQueueDao access = new PrinterQueueDao(this.Transaction);
                            result = access.Add(objqueue);
                        };
                        this.Start(false);
                        counter = counter + 2;
                    }
                }
                else if (ProductTypeId == 131)
                {
                    PrinterQueueInfo objqueue = new PrinterQueueInfo();
                    objqueue.DG_Associated_PrinterId = Printerid;
                    objqueue.DG_PrinterQueue_Image_Pkey = string.Join(",", Images.ToArray());
                    objqueue.DG_PrinterQueue_ProductID = ProductTypeId;
                    objqueue.is_Active = true;
                    objqueue.DG_SentToPrinter = false;
                    objqueue.DG_Order_Details_Pkey = OrderDetailedKey;
                    objqueue.DG_IsSpecPrint = GreenSpecPrint;
                    objqueue.DG_Print_Date = DateTime.Now;
                    PrinterQueueInfo itemlist = new PrinterQueueInfo();
                    this.operation = () =>
                    {
                        PrinterQueueDao access = new PrinterQueueDao(this.Transaction);
                        itemlist = access.Get();
                    };
                    this.Start(false);
                    if (itemlist != null)
                    {
                        objqueue.QueueIndex = itemlist.QueueIndex + 1;
                    }
                    else
                    {
                        objqueue.QueueIndex = 1;
                    }
                    this.operation = () =>
                    {
                        PrinterQueueDao access = new PrinterQueueDao(this.Transaction);
                        result = access.Add(objqueue);
                    };
                    this.Start(false);
                }
                //if product type==79 save page wise 
                else if (ProductTypeId == 79 && PhotoPrintPositionDicList != null)
                {
                    List<int> SavedImage = new List<int>();

                    foreach (int PageNo in PhotoPrintPositionDicList.Select(o => o.PhotoPrintPositionList.PageNo).Distinct().OrderBy(o => o).ToList())
                    {
                        string imageIds = string.Empty;


                        ProductTypeInfo item = prdBiz.GetProductType(ProductTypeId);

                        if (Convert.ToBoolean(item.DG_IsAccessory) != true)
                        {
                            int MinPageNo = PhotoPrintPositionDicList.Where(o => SavedImage.Any(c => c == o.PhotoId) == false).Min(j => j.PhotoId);
                            string img = string.Empty;
                            // img += from o in PhotoPrintPositionDicList where o.PhotoPrintPositionList.Where(p => p.PageNo == MinPageNo).FirstOrDefault().PageNo == MinPageNo select o.PhotoId;
                            PhotoPrintPositionDicList.Where(o => o.PhotoPrintPositionList.PageNo == PageNo).OrderBy(r => r.PhotoPrintPositionList.PhotoPosition).ToList().ForEach(p => imageIds += "," + p.PhotoId);
                            imageIds = string.Empty;
                            PhotoPrintPositionDicList.Where(o => o.PhotoPrintPositionList.PageNo == PageNo).OrderBy(r => r.PhotoPrintPositionList.PhotoPosition).ToList().ForEach(p => imageIds += "," + p.PhotoPrintPositionList.RotationAngle);

                            PrinterQueueInfo objqueue = new PrinterQueueInfo();
                            objqueue.DG_Associated_PrinterId = Printerid;
                            objqueue.DG_PrinterQueue_Image_Pkey = imageIds.Length > 0 ? imageIds.Substring(1) : string.Empty;
                            objqueue.RotationAngle = imageIds.Length > 0 ? imageIds.Substring(1) : string.Empty;
                            objqueue.DG_PrinterQueue_ProductID = ProductTypeId;
                            objqueue.DG_SentToPrinter = false;
                            objqueue.DG_Order_Details_Pkey = OrderDetailedKey;
                            objqueue.is_Active = true;
                            objqueue.DG_IsSpecPrint = GreenSpecPrint;
                            objqueue.DG_Print_Date = DateTime.Now;

                            PrinterQueueInfo itemlist = new PrinterQueueInfo();
                            this.operation = () =>
                            {
                                PrinterQueueDao access = new PrinterQueueDao(this.Transaction);
                                itemlist = access.Get();
                            };
                            this.Start(false);
                            if (itemlist != null)
                            {
                                objqueue.QueueIndex = itemlist.QueueIndex + 1;
                            }
                            else
                            {
                                objqueue.QueueIndex = 1;
                            }
                            this.operation = () =>
                            {
                                PrinterQueueDao access = new PrinterQueueDao(this.Transaction);
                                result = access.Add(objqueue);
                            };
                            this.Start(false);
                        }

                    }
                }
                else if (ProductTypeId == 100)
                {
                    int detailId = 1;
                    foreach (string image in Images)
                    {

                        ProductTypeInfo item = prdBiz.GetProductType(ProductTypeId);

                        if (Convert.ToBoolean(item.DG_IsAccessory) != true)
                        {
                            PrinterQueueInfo objqueue = new PrinterQueueInfo();
                            objqueue.DG_Associated_PrinterId = Printerid;
                            objqueue.DG_PrinterQueue_Image_Pkey = image;
                            objqueue.DG_PrinterQueue_ProductID = ProductTypeId;
                            objqueue.DG_SentToPrinter = false;
                            objqueue.DG_Order_Details_Pkey = OrderDetailedKey;
                            objqueue.is_Active = true;
                            objqueue.DG_IsSpecPrint = GreenSpecPrint;
                            objqueue.DG_Print_Date = DateTime.Now;

                            PrinterQueueInfo itemlist = new PrinterQueueInfo();
                            this.operation = () =>
                            {
                                PrinterQueueDao access = new PrinterQueueDao(this.Transaction);
                                itemlist = access.Get();
                            };
                            this.Start(false);
                            if (itemlist != null)
                            {
                                objqueue.QueueIndex = itemlist.QueueIndex + 1;
                            }
                            else
                            {
                                objqueue.QueueIndex = 1;
                            }
                            this.operation = () =>
                            {
                                PrinterQueueDao access = new PrinterQueueDao(this.Transaction);
                                result = access.Add(objqueue);
                            };
                            this.Start(false);

                            //ajeet

                            CalenderBusiness calBizz = new CalenderBusiness();
                            List<ItemTemplateDetailModel> objdetModel = new List<ItemTemplateDetailModel>();
                            objdetModel = calBizz.GetItemTemplateDetail().Where(x => x.MasterTemplateId == 1).ToList();



                            ItemTemplatePrintOrderModel order = new ItemTemplatePrintOrderModel();
                            //var printItem = liItem.PrintOrderPageList[1];//liItem.PrintOrderPageList[index];
                            order.MasterTemplateId = MasterTemplateId;
                            order.DetailTemplateId = detailId;
                            order.OrderLineItemId = result;
                            order.PrinterQueueId = 0;
                            order.PageNo = 0;
                            order.PhotoId = 1;
                            order.Status = 0;
                            order.PrintTypeId = 0;
                            order.PrintPosition = 0;
                            order.RotationAngle = 0;
                            order.CreatedBy = "abc";
                            calBizz.AddItemTemplatePrintOrder(order);
                            detailId = detailId + 1;
                            //ItemTemplatePrintOrder.AddPrintOrder(order); todo
                        }

                    }
                }
                #region Added by Ajay for Panorama Printer 8x24 size on 16 Jan 18
                else if (ProductTypeId == 120|| ProductTypeId == 121 || ProductTypeId == 122 || ProductTypeId == 123 || ProductTypeId == 125)
                {
                    int detailId = 1;
                    foreach (string image in Images)
                    {

                        ProductTypeInfo item = prdBiz.GetProductType(ProductTypeId);

                        if (Convert.ToBoolean(item.DG_IsAccessory) != true)
                        {
                            PrinterQueueInfo objqueue = new PrinterQueueInfo();
                            objqueue.DG_Associated_PrinterId = Printerid;
                            objqueue.DG_PrinterQueue_Image_Pkey = image;
                            objqueue.DG_PrinterQueue_ProductID = ProductTypeId;
                            objqueue.DG_SentToPrinter = false;
                            objqueue.DG_Order_Details_Pkey = OrderDetailedKey;
                            objqueue.is_Active = true;
                            objqueue.DG_IsSpecPrint = GreenSpecPrint;
                            objqueue.DG_Print_Date = DateTime.Now;

                            PrinterQueueInfo itemlist = new PrinterQueueInfo();
                            this.operation = () =>
                            {
                                PrinterQueueDao access = new PrinterQueueDao(this.Transaction);
                                itemlist = access.Get();
                            };
                            this.Start(false);
                            if (itemlist != null)
                            {
                                objqueue.QueueIndex = itemlist.QueueIndex + 1;
                            }
                            else
                            {
                                objqueue.QueueIndex = 1;
                            }
                            this.operation = () =>
                            {
                                PrinterQueueDao access = new PrinterQueueDao(this.Transaction);
                                result = access.Add(objqueue);
                            };
                            this.Start(false);

                            //ajeet

                            CalenderBusiness calBizz = new CalenderBusiness();
                            List<ItemTemplateDetailModel> objdetModel = new List<ItemTemplateDetailModel>();
                            objdetModel = calBizz.GetItemTemplateDetail().Where(x => x.MasterTemplateId == 1).ToList();



                            ItemTemplatePrintOrderModel order = new ItemTemplatePrintOrderModel();
                            //var printItem = liItem.PrintOrderPageList[1];//liItem.PrintOrderPageList[index];
                            order.MasterTemplateId = MasterTemplateId;
                            order.DetailTemplateId = detailId;
                            order.OrderLineItemId = result;
                            order.PrinterQueueId = 0;
                            order.PageNo = 0;
                            order.PhotoId = 1;
                            order.Status = 0;
                            order.PrintTypeId = 0;
                            order.PrintPosition = 0;
                            order.RotationAngle = 0;
                            order.CreatedBy = "abc";
                            calBizz.AddItemTemplatePrintOrder(order);
                            detailId = detailId + 1;
                            //ItemTemplatePrintOrder.AddPrintOrder(order); todo
                        }

                    }
                } 
                #endregion
                else
                {
                    foreach (string image in Images)
                    {
                        ProductTypeInfo item = prdBiz.GetProductType(ProductTypeId);

                        if (Convert.ToBoolean(item.DG_IsAccessory) != true)
                        {
                            PrinterQueueInfo objqueue = new PrinterQueueInfo();
                            objqueue.DG_Associated_PrinterId = Printerid;
                            objqueue.DG_PrinterQueue_Image_Pkey = image;
                            objqueue.DG_PrinterQueue_ProductID = ProductTypeId;
                            objqueue.DG_SentToPrinter = false;
                            objqueue.DG_Order_Details_Pkey = OrderDetailedKey;
                            objqueue.is_Active = true;
                            objqueue.DG_IsSpecPrint = GreenSpecPrint;
                            objqueue.DG_Print_Date = DateTime.Now;

                            PrinterQueueInfo itemlist = new PrinterQueueInfo();
                            this.operation = () =>
                            {
                                PrinterQueueDao access = new PrinterQueueDao(this.Transaction);
                                itemlist = access.Get();
                            };
                            this.Start(false);
                            if (itemlist != null)
                            {
                                objqueue.QueueIndex = itemlist.QueueIndex + 1;
                            }
                            else
                            {
                                objqueue.QueueIndex = 1;
                            }
                            this.operation = () =>
                            {
                                PrinterQueueDao access = new PrinterQueueDao(this.Transaction);
                                result = access.Add(objqueue);
                            };
                            this.Start(false);
                        }

                    }
                }
            }
            else
            {
                PrinterQueueInfo objqueue = new PrinterQueueInfo();
                objqueue.DG_Associated_PrinterId = Printerid;
                objqueue.DG_PrinterQueue_Image_Pkey = string.Join(",", Images.ToArray());
                objqueue.DG_PrinterQueue_ProductID = ProductTypeId;
                objqueue.is_Active = true;
                objqueue.DG_SentToPrinter = false;
                objqueue.DG_Order_Details_Pkey = OrderDetailedKey;
                objqueue.DG_IsSpecPrint = GreenSpecPrint;
                objqueue.DG_Print_Date = DateTime.Now;
                PrinterQueueInfo itemlist = new PrinterQueueInfo();
                this.operation = () =>
                {
                    PrinterQueueDao access = new PrinterQueueDao(this.Transaction);
                    itemlist = access.Get();
                };
                this.Start(false);
                if (itemlist != null)
                {
                    objqueue.QueueIndex = itemlist.QueueIndex + 1;
                }
                else
                {
                    objqueue.QueueIndex = 1;
                }
                this.operation = () =>
                {
                    PrinterQueueDao access = new PrinterQueueDao(this.Transaction);
                    result = access.Add(objqueue);
                };
                this.Start(false);
            }
            return result;

        }
        public List<AssociatedPrintersInfo> GetPrinterDetails(int substoreID)
        {
            List<AssociatedPrintersInfo> objectList = new List<AssociatedPrintersInfo>();
            this.operation = () =>
            {
                PrinterQueueDao access = new PrinterQueueDao(this.Transaction);
                objectList = access.SelectPrinterDetailsBySubStoreID(substoreID);
            };
            this.Start(false);
            return objectList;
        }



        public void SetPrinterDetails(string PrinterName, int productType, bool isactive, string Papersize, int SubStoreId)
        {
            this.operation = () =>
             {
                 AssociatedPrintersDao access = new AssociatedPrintersDao(this.Transaction);
                 access.AddAndUpdatePrinterDetails(PrinterName, productType, isactive, Papersize, SubStoreId);
             };
            this.Start(false);
        }
        public void SetPrintQueueIndex(int pkey, string flag)
        {
            this.operation = () =>
            {
                PrinterQueueDao access = new PrinterQueueDao(this.Transaction);
                access.UpdatePrintQueueIndex(pkey, flag);
            };
            this.Start(false);
        }
        public List<PrinterQueueInfo> GetPrinterQueueForUpdown(int substoreID)
        {
            List<PrinterQueueInfo> objectList = new List<PrinterQueueInfo>();
            this.operation = () =>
            {
                PrinterQueueDao access = new PrinterQueueDao(this.Transaction);
                objectList = access.GetAllFilteredPrinterQueueBySubStoreId(substoreID);
            };
            this.Start(false);
            if (objectList != null)
            {
                return objectList.OrderBy(t => t.QueueIndex).ToList();
            }
            else
            {
                return new List<PrinterQueueInfo>();
            }

        }

        public void SetPrinterQueueForReprint(int QueueId)
        {
            this.operation = () =>
            {
                PrinterQueueDao access = new PrinterQueueDao(this.Transaction);
                access.UpdatePrinterQueueForReprint(QueueId);
            };
            this.Start(false);
        }

        public void UpdatePrintCountForReprint(int QueueId)
        {
            this.operation = () =>
            {
                PrinterQueueDao access = new PrinterQueueDao(this.Transaction);
                access.UpdatePrintCountForReprint(QueueId);
            };
            this.Start(false);
        }
        public PrinterQueueInfo GetQueueDetail(int Name)
        {
            PrinterQueueInfo result = new PrinterQueueInfo();
            this.operation = () =>
            {
                PrinterQueueDao access = new PrinterQueueDao(this.Transaction);
                result = access.GetPrinterQueue(Name);
            };
            this.Start(false);

            return result;
        }
        public ObservableCollection<PrinterJobInfo> GetPrinterJobInfo(DataTable Dudt_PrintJobInfot, string PrinterName, string DigiFolderThumbnailPath)
        {
            ObservableCollection<PrinterJobInfo> result = new ObservableCollection<PrinterJobInfo>();
            this.operation = () =>
            {
                PrinterQueueDao access = new PrinterQueueDao(this.Transaction);
                result = access.GetPrinterJobInfo(Dudt_PrintJobInfot, PrinterName, DigiFolderThumbnailPath);
            };
            this.Start(false);

            return result;
        }
        public void SaveAlbumPrintPosition(int OrderLineItemId, List<PhotoPrintPositionDic> PrintPhotoOrderIds)
        {
            if (PrintPhotoOrderIds != null)
            {
                DataTable udt_Printer = new DataTable();
                udt_Printer.Columns.Add("OrderLineItemId", typeof(Int32));
                udt_Printer.Columns.Add("PhotoId", typeof(Int32));
                udt_Printer.Columns.Add("PageNo", typeof(Int32));
                udt_Printer.Columns.Add("PrintPosition", typeof(Int32));
                udt_Printer.Columns.Add("RotationAngle", typeof(Int32));
                foreach (var item in PrintPhotoOrderIds)
                {
                    DataRow dr = udt_Printer.NewRow();
                    dr["OrderLineItemId"] = OrderLineItemId;
                    dr["PhotoId"] = item.PhotoId;
                    dr["PageNo"] = item.PhotoPrintPositionList.PageNo;
                    dr["PrintPosition"] = item.PhotoPrintPositionList.PhotoPosition;
                    dr["RotationAngle"] = item.PhotoPrintPositionList.RotationAngle;
                    udt_Printer.Rows.Add(dr);
                }

                this.operation = () =>
                {
                    PrinterQueueDao access = new PrinterQueueDao(this.Transaction);
                    access.SaveAlbumPrintPosition(udt_Printer);
                };
                this.Start(false);
            }
        }

        public List<PrinterDetailsInfo> GetAssociatedPrinters(int? productType)
        {
            List<PrinterDetailsInfo> objectList = new List<PrinterDetailsInfo>();
            this.operation = () =>
            {
                AssociatedPrintersDao access = new AssociatedPrintersDao(this.Transaction);
                objectList = access.GetAssociatedPrinters(productType);
            };
            this.Start(false);
            return objectList;
        }

        public List<PrinterQueueInfo> GetPrintLogDetails()
        {
            List<PrinterQueueInfo> objectList = new List<PrinterQueueInfo>();
            this.operation = () =>
            {
                PrinterQueueDao access = new PrinterQueueDao(this.Transaction);
                objectList = access.GetPrintLogDetails();
            };
            this.Start(false);
            return objectList;
        }


        /// <summary>
        /// Fetching QR Code
        /// </summary>
        /// <param name="productId"></param>
        /// <returns></returns>
        public string SelectQRCode(int productId)
        {
            try
            {
                string QRCode = string.Empty;
                this.operation = () =>
                {
                    PrinterQueueDao access = new PrinterQueueDao(this.Transaction);
                    QRCode = access.SelectQRCode(productId);
                };
                this.Start(false);
                return QRCode;
            }

            catch (Exception ex)
            {
                return null;
            }
        }

        public string CheckSpecSetting(int PrinterId)
        {
            try
            {
                string SpecValue = string.Empty;
                this.Operation = () =>
                    {
                        PrinterQueueDao access = new PrinterQueueDao(this.Transaction);
                        SpecValue = access.CheckSpecSetting(PrinterId);
                    };
                this.Start(false);
                return SpecValue;
            }
            catch(Exception ex)
            {
                return null;
            }
        }
    }
}
