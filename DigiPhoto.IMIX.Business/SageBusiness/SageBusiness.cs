﻿using Digiphoto.Cache.SqlDependentCache;
using DigiPhoto.Cache.DataCache;
using DigiPhoto.Cache.MasterDataCache;
using DigiPhoto.Cache.Repository;
using DigiPhoto.IMIX.DataAccess;
using DigiPhoto.IMIX.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;


namespace DigiPhoto.IMIX.Business
{
    public partial class SageBusiness : BaseBusiness
    {
        public SageInfo GetOpenCloseProcDetail(int SubStoreID)
        {
            SageInfo _sageInfo = new SageInfo();
            SageBusiness obj = new SageBusiness();
            this.operation = () =>
            {
                // public SageInfo GetOpenCloseProcDetail(Int64 SubStoreID)
                SageInfoAccess access = new SageInfoAccess(this.Transaction);
                _sageInfo = access.GetOpenCloseProcDetail(SubStoreID);
            };
            this.Start(false);

            return _sageInfo;
        }

        public string SaveOpeningForm(SageInfo _sageInfo, List<Printer6850> lst6850Printer, List<Printer8810> lst8810Printer)
        {
            CommonBusiness objCommBus = new CommonBusiness();

            DataTable dt6850 = objCommBus.CopyGenericToDataTable(lst6850Printer);
            DataView view = new System.Data.DataView(dt6850);
            DataTable selected6850 = view.ToTable("Selected", false, "ImageCount", "PrinterSerialNumber", "ErrorMessage");

            DataTable dt8810 = objCommBus.CopyGenericToDataTable(lst8810Printer);
            DataView view8810 = new System.Data.DataView(dt8810);
            DataTable selected8810 = view8810.ToTable("Selected", false, "ImageCount", "PrinterSerialNumber", "ErrorMessage");

            string ret = "";
            SageBusiness obj = new SageBusiness();
            this.operation = () =>
            {
                // public SageInfo GetOpenCloseProcDetail(Int64 SubStoreID)
                SageInfoAccess access = new SageInfoAccess(this.Transaction);
                ret = access.SaveOpeningForm(_sageInfo, selected6850, selected8810);
            };
            this.Start(false);

            return ret;
        }
        public bool SaveClosingForm(SageOpenClose objOpenClose, List<Printer6850> lst6850Printer, List<Printer8810> lst8810Printer)
        {
            CommonBusiness objCommBus = new CommonBusiness();

            DataTable dt = objCommBus.CopyGenericToDataTable(objOpenClose.objClose.InventoryConsumable);
            DataView view = new System.Data.DataView(dt);
            DataTable selected = view.ToTable("Selected", false, "AccessoryID", "ConsumeValue");

            DataTable dt6850 = objCommBus.CopyGenericToDataTable(lst6850Printer);
            DataView view6850 = new System.Data.DataView(dt6850);
            DataTable selected6850 = view6850.ToTable("Selected", false, "ImageCount", "PrinterSerialNumber", "ErrorMessage");

            DataTable dt8810 = objCommBus.CopyGenericToDataTable(lst8810Printer);
            DataView view8810 = new System.Data.DataView(dt8810);
            DataTable selected8810 = view8810.ToTable("Selected", false, "ImageCount", "PrinterSerialNumber", "ErrorMessage");


            bool ret = false;
            SageBusiness obj = new SageBusiness();
            this.operation = () =>
            {
                // public SageInfo GetOpenCloseProcDetail(Int64 SubStoreID)
                SageInfoAccess access = new SageInfoAccess(this.Transaction);
                ret = access.SaveClosingForm(objOpenClose, selected, selected6850, selected8810);
            };
            this.Start(false);

            return ret;
        }

        public List<SageInfoWestage> ProductTypeWestage(DateTime FromDate, DateTime ToDate, int SubStoreID)
        {
            List<SageInfoWestage> lstsageInfoWest = new List<SageInfoWestage>();
            SageBusiness obj = new SageBusiness();
            this.operation = () =>
            {
                // public SageInfo GetOpenCloseProcDetail(Int64 SubStoreID)
                SageInfoAccess access = new SageInfoAccess(this.Transaction);
                lstsageInfoWest = access.ProductTypeWestage(FromDate, ToDate, SubStoreID);
            };
            this.Start(false);

            return lstsageInfoWest;
        }

        public int GetCaptureBySubStoreAndDateRange(DateTime FromDate, DateTime ToDate, int SubStoreID)
        {
            int Capture = 0;
            SageBusiness obj = new SageBusiness();
            this.operation = () =>
            {
                // public SageInfo GetOpenCloseProcDetail(Int64 SubStoreID)
                SageInfoAccess access = new SageInfoAccess(this.Transaction);
                Capture = access.GetCaptureBySubStoreAndDateRange(FromDate, ToDate, SubStoreID);
            };
            this.Start(false);

            return Capture;
        }

        public int GetPreviewBySubStoreAndDateRange(DateTime FromDate, DateTime ToDate, int SubStoreID)
        {
            int Preview = 0;
            SageBusiness obj = new SageBusiness();
            this.operation = () =>
            {
                // public SageInfo GetOpenCloseProcDetail(Int64 SubStoreID)
                SageInfoAccess access = new SageInfoAccess(this.Transaction);
                Preview = access.GetPreviewBySubStoreAndDateRange(FromDate, ToDate, SubStoreID);
            };
            this.Start(false);

            return Preview;
        }
        public Int64 GetTotalSoldBySubStoreAndDateRange(DateTime FromDate, DateTime ToDate, int SubStoreID)
        {
            Int64 ImageSold = 0;
            SageBusiness obj = new SageBusiness();
            this.operation = () =>
            {
                // public SageInfo GetOpenCloseProcDetail(Int64 SubStoreID)
                SageInfoAccess access = new SageInfoAccess(this.Transaction);
                ImageSold = access.GetTotalSoldBySubStoreAndDateRange(FromDate, ToDate, SubStoreID);
            };
            this.Start(false);

            return ImageSold;
        }
        public SageInfoClosing GetRevenueDetails(DateTime FromDate, DateTime ToDate, int SubStoreID)
        {
            SageInfoClosing sageInfoClosing = null;
            SageBusiness obj = new SageBusiness();
            this.operation = () =>
            {
                SageInfoAccess access = new SageInfoAccess(this.Transaction);
                sageInfoClosing = access.GetRevenueDetails(FromDate, ToDate, SubStoreID);
            };
            this.Start(false);
            return sageInfoClosing;
        }

        public List<ImixPOSDetail> GetPrintServerDetails(int SubStoreID)
        {
            List<ImixPOSDetail> lstPosDetail = new List<ImixPOSDetail>();
            SageBusiness obj = new SageBusiness();
            this.operation = () =>
            {
                // public SageInfo GetOpenCloseProcDetail(Int64 SubStoreID)
                SageInfoAccess access = new SageInfoAccess(this.Transaction);
                lstPosDetail = access.GetPrintServerDetails(SubStoreID);
            };
            this.Start(false);

            return lstPosDetail;

        }
        public List<SageClosingFormDownloadInfo> GetClosingFormDownloadInfo(DateTime? FromDate, DateTime? ToDate, int SubStoreID)
        {
            List<SageClosingFormDownloadInfo> sageDownloadInfoList = new List<SageClosingFormDownloadInfo>(); ;
            SageBusiness obj = new SageBusiness();
            this.operation = () =>
            {
                SageInfoAccess access = new SageInfoAccess(this.Transaction);
                sageDownloadInfoList = access.GetClosingFormDownloadInfoList(FromDate, ToDate, SubStoreID);
            };
            this.Start(false);
            return sageDownloadInfoList;
        }

        public SageOpenClose GetClosingFormDownloadInfo(int ClosingFormDetailID)
        {
            SageOpenClose sageOpenClose = new SageOpenClose(); ;
            SageBusiness obj = new SageBusiness();
            this.operation = () =>
            {
                SageInfoAccess access = new SageInfoAccess(this.Transaction);
                sageOpenClose = access.GetClosingFormDownloadInfo(ClosingFormDetailID);
            };
            this.Start(false);
            return sageOpenClose;
        }
    }
}
