﻿using DigiPhoto.IMIX.DataAccess;
using DigiPhoto.IMIX.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace DigiPhoto.IMIX.Business
{
    public class RfidBusiness : BaseBusiness
    {
        public bool SaveRfidTag(RFIDTagInfo rfidTag)
        {
            try
            {
                bool issucess = false;
                this.operation = () =>
                {
                    RfidAccess access = new RfidAccess(this.Transaction);
                    issucess = access.SaveRfidTag(rfidTag);
                };
                this.Start(false);
                return issucess;
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogFileWrite(ErrorHandler.ErrorHandler.CreateErrorMessage(ex));
                return false;
            }
        }

        public bool AssociateRFIDtoPhotosAdvance(DataTable dt_Rfid, string SerailNo)
        {
            try
            {
                bool issucess = false;
                this.operation = () =>
                {
                    RfidAccess access = new RfidAccess(this.Transaction);
                    issucess = access.AssociateRFIDtoPhotosAdvance(dt_Rfid, SerailNo);
                };
                this.Start(false);
                return issucess;
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogFileWrite(ErrorHandler.ErrorHandler.CreateErrorMessage(ex));
                return false;
            }
        }
		///changes made by latika for table flow
        public bool AssociateRFIDtoPhotosAdvanceTableFlow(DataTable dt_Rfid, string SerailNo)
        {
            try
            {
                bool issucess = false;
                this.operation = () =>
                {
                    RfidAccess access = new RfidAccess(this.Transaction);
                    issucess = access.AssociateRFIDtoPhotosAdvanceTableFlow(dt_Rfid, SerailNo);
                };
                this.Start(false);
                return issucess;
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogFileWrite(ErrorHandler.ErrorHandler.CreateErrorMessage(ex));
                return false;
            }
        }///end
        public List<RFIDField> GetSubStoreList()
        {
            List<RFIDField> lstRFIDTagInfo = new List<RFIDField>();
            this.operation = () =>
            {
                RfidAccess access = new RfidAccess(this.Transaction);
                lstRFIDTagInfo = access.GetSubStoreList();
            };
            this.Start(false);
            return lstRFIDTagInfo;
        }

        public bool AssociateRFIDtoPhotos()
        {
            try
            {
                bool issucess = false;
                this.operation = () =>
                {
                    RfidAccess access = new RfidAccess(this.Transaction);
                    issucess = access.AssociateRFIDtoPhotos();
                };
                this.Start(false);
                return issucess;
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogFileWrite(ErrorHandler.ErrorHandler.CreateErrorMessage(ex));
                return false;
            }
        }

        public bool ArchiveRFIDTags(int subStoreId)
        {
            try
            {
                bool issucess = false;
                this.operation = () =>
                {
                    RfidAccess access = new RfidAccess(this.Transaction);
                    issucess = access.ArchiveRFIDTags(subStoreId);
                };
                this.Start(false);
                return issucess;
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogFileWrite(ErrorHandler.ErrorHandler.CreateErrorMessage(ex));
                return false;
            }
        }

        public bool SaveUpdateDummyRFIDTags(RFIDTagInfo rfidTag)
        {
            try
            {
                bool issucess = false;
                this.operation = () =>
                {
                    RfidAccess access = new RfidAccess(this.Transaction);
                    issucess = access.SaveDummyRFIDTagsInfo(rfidTag);
                };
                this.Start(false);
                return issucess;
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogFileWrite(ErrorHandler.ErrorHandler.CreateErrorMessage(ex));
                return false;
            }
        }
        public bool DeleteDummyRFIDTags(int DummyRFIDTagID)
        {
            bool issucess = false;
            this.operation = () =>
            {
                RfidAccess access = new RfidAccess(this.Transaction);
                issucess = access.DeleteDummyRFIDTagsInfo(DummyRFIDTagID);
                //  return list;
            };
            this.Start(false);
            return issucess;
        }
        public List<RFIDImageAssociationInfo> GetRFIDAssociationSearch(int photoGrapherId, int deviceId, DateTime dtFrom, DateTime dtTo)
        {
            List<RFIDImageAssociationInfo> lstRFIDImageAssociationInfo = new List<RFIDImageAssociationInfo>();
            this.operation = () =>
            {
                RfidAccess access = new RfidAccess(this.Transaction);
                lstRFIDImageAssociationInfo = access.GetRFIDAssociationSearch(photoGrapherId, deviceId, dtFrom, dtTo);
            };
            this.Start(false);

            return lstRFIDImageAssociationInfo;
        }

        public List<PhotoDetail> GetRFIDNotAssociatedPhotos(RFIDPhotoDetails RFIDPhotoObj)
        {
            List<PhotoDetail> lstPhotoDetails = new List<PhotoDetail>();
            this.operation = () =>
            {
                RfidAccess access = new RfidAccess(this.Transaction);
                lstPhotoDetails = access.GetRFIDNotAssociatedPhotos(RFIDPhotoObj);
            };
            this.Start(false);

            return lstPhotoDetails;
        }

        public List<RFIDTagInfo> GetDummyRFIDTags(int DummyRFIDTagID)
        {
            List<RFIDTagInfo> lstRFIDTagInfo = new List<RFIDTagInfo>();
            this.operation = () =>
            {
                RfidAccess access = new RfidAccess(this.Transaction);
                lstRFIDTagInfo = access.GetDummyRFIDTagsInfo(DummyRFIDTagID);
            };
            this.Start(false);

            return lstRFIDTagInfo;
        }

        public bool MapNonAssociatedImages(string cardUniqueIdentifier, string photoIDS)
        {
            try
            {
                bool issucess = false;
                this.operation = () =>
                {
                    RfidAccess access = new RfidAccess(this.Transaction);
                    issucess = access.MapNonAssociatedImages(cardUniqueIdentifier, photoIDS);
                };
                this.Start(false);
                return issucess;
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogFileWrite(ErrorHandler.ErrorHandler.CreateErrorMessage(ex));
                return false;
            }
        }

        public int? GetPhotographerRFIDEnabledLocation(int photographerID)
        {
            int? locationID = null;
            this.operation = () =>
            {
                RfidAccess access = new RfidAccess(this.Transaction);
                locationID = access.GetPhotographerRFIDEnabledLocation(photographerID);
            };
            this.Start(false);

            return locationID;
        }

        public List<RfidFlushHistotyInfo> GetFlushHistoryData(int SubStoreId, DateTime fromDate, DateTime toDate)
        {
            List<RfidFlushHistotyInfo> lstRFIDFlush = new List<RfidFlushHistotyInfo>();
            this.operation = () =>
            {
                RfidAccess access = new RfidAccess(this.Transaction);
                lstRFIDFlush = access.GetFlushHistoryData(SubStoreId, fromDate, toDate);
            };
            this.Start(false);
            return lstRFIDFlush;
        }

        public bool SaveRfidFlushHistory(RfidFlushHistotyInfo rfidFlush)
        {
            try
            {
                bool issucess = false;
                this.operation = () =>
                {
                    RfidAccess access = new RfidAccess(this.Transaction);
                    issucess = access.SaveRfidFlushHistory(rfidFlush);
                };
                this.Start(false);
                return issucess;
            }
            catch
            {
                return false;
            }
        }

        public bool RfidFlushNow(int SubStoreId, int LocationId, int NoOfExcludeDays)
        {
            try
            {
                bool issucess = false;
                this.operation = () =>
                {
                    RfidAccess access = new RfidAccess(this.Transaction);
                    issucess = access.RfidFlushNow(SubStoreId, LocationId, NoOfExcludeDays);
                };
                this.Start(false);
                return issucess;
            }
            catch
            {
                return false;
            }
        }
        public List<LocationInfo> GetAllLocations()
        {
            List<LocationInfo> lstLocationInfo = new List<LocationInfo>();
            this.operation = () =>
            {
                RfidAccess access = new RfidAccess(this.Transaction);
                lstLocationInfo = access.GetAllLocations();
            };
            this.Start(false);

            return lstLocationInfo;
        }
        public bool SaveSeperatorRFIDTagsInfo(SeperatorTagInfo rfidTag)
        {
            try
            {
                bool issucess = false;
                this.operation = () =>
                {
                    RfidAccess access = new RfidAccess(this.Transaction);
                    issucess = access.SaveSeperatorRFIDTagsInfo(rfidTag);
                };
                this.Start(false);
                return issucess;
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogFileWrite(ErrorHandler.ErrorHandler.CreateErrorMessage(ex));
                return false;
            }
        }
        public bool DeleteSeperatorTag(int seperatorTagID)
        {
            try
            {
                bool issuccess = false;
                this.operation = () =>
                    {
                        RfidAccess access = new RfidAccess(this.Transaction);
                        issuccess = access.DeleteSeperatorRFIDTagsInfo(seperatorTagID);
                    };
                this.Start(false);
                return issuccess;
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogFileWrite(ErrorHandler.ErrorHandler.CreateErrorMessage(ex));
                return false;
            }
        }
        public List<SeperatorTagInfo> GetSeperatorRFIDTags(int seperatorRFIDTagID)
        {
            List<SeperatorTagInfo> lstSeperatorRFIDTagInfo = new List<SeperatorTagInfo>();
            this.operation = () =>
            {
                RfidAccess access = new RfidAccess(this.Transaction);
                lstSeperatorRFIDTagInfo = access.GetSeperatorRFIDTagsInfo(seperatorRFIDTagID);
            };
            this.Start(false);
            return lstSeperatorRFIDTagInfo;
        }

        public List<PhotographerRFIDAssociationInfo> GetPhotographerRFIDAssociation(int? photoGrapherId, DateTime dtFrom, DateTime dtTo)
        {
            List<PhotographerRFIDAssociationInfo> lstPhotographerRFIDAssociationInfo = new List<PhotographerRFIDAssociationInfo>();
            this.operation = () =>
            {
                RfidAccess access = new RfidAccess(this.Transaction);
                lstPhotographerRFIDAssociationInfo = access.GetPhotographerRFIDAssociation(photoGrapherId, dtFrom, dtTo);
            };
            this.Start(false);
            return lstPhotographerRFIDAssociationInfo;
        }

    }
}
