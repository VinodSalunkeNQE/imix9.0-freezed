﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DigiPhoto.IMIX.DataAccess;
using DigiPhoto.IMIX.Model;
using System.Data;

namespace DigiPhoto.IMIX.Business
{
 public class ProcessedVideoBusiness:BaseBusiness
    {

     public int SaveProcessedVideoAndDetails(ProcessedVideoInfo processedVideo, List<ProcessedVideoDetailsInfo> lstPVDetails)
     {
         try
         {
             DataTable dtPVDetails = new DataTable();
             dtPVDetails.Columns.Add("ProcessedVideoDetailId");
             dtPVDetails.Columns.Add("ProcessedVideoId");
             dtPVDetails.Columns.Add("FrameTime");
             dtPVDetails.Columns.Add("DisplayTime");
             dtPVDetails.Columns.Add("MediaId");
             dtPVDetails.Columns.Add("MediaType");
             dtPVDetails.Columns.Add("JoiningOrder");
             dtPVDetails.Columns.Add("StartTime");
             dtPVDetails.Columns.Add("EndTime");

             foreach (ProcessedVideoDetailsInfo item in lstPVDetails)
             {
                 dtPVDetails.Rows.Add(item.ProcessedVideoDetailId, item.ProcessedVideoId, item.FrameTime, item.DisplayTime, item.MediaId, item.MediaType, item.JoiningOrder,item.StartTime,item.EndTime);
             }
             int issucess = 0;
             this.operation = () =>
             {
                 ProcessedVideoAccess access = new ProcessedVideoAccess(this.Transaction);
                 issucess = access.SaveProcessedVideoAndDetails(processedVideo, dtPVDetails);

             };
             this.Start(false);
             return issucess;
         }
         catch(Exception ex)
         {
             return 0;
         }
     }
     //Video Packages
     public List<VideoProducts> GetVideoPackages()
     {
         try
         {
             List<VideoProducts> list = new List<VideoProducts>();
             this.operation = () =>
             {
                 ProcessedVideoAccess access = new ProcessedVideoAccess(this.Transaction);
                 list = access.GetVideoPackages();
                 //  return list;
             };
             this.Start(false);
             return list;
         }
         catch(Exception ex)
         {
             return null;
         }
     }
     //Processed Video Details
     public ProcessedVideoInfo GetProcessedVideoDetails(int VideoId)
     {
         try
         {
             ProcessedVideoInfo list = new ProcessedVideoInfo();
             this.operation = () =>
             {
                 ProcessedVideoAccess access = new ProcessedVideoAccess(this.Transaction);
                 list = access.GetProcessedVideoDetails(VideoId);
             };
             this.Start(false);
             return list;
         }
         catch(Exception ex)
         {
             return null;
         }
     }

     //File Information 
     public FilePhotoInfo GetFileInfo(int photos_Key)
     {
         try
         {
             FilePhotoInfo list = new FilePhotoInfo();
             this.operation = () =>
             {
                 ProcessedVideoAccess access = new ProcessedVideoAccess(this.Transaction);
                 list = access.GetFileInfo(photos_Key);
             };
             this.Start(false);
             return list;
         }
         catch(Exception ex)
         {
             return null;
         }

     }
     public  List<ProcessedVideoInfo> GetProcessedVideosByPackageId(int packageID)
     {
         try
         {
             List<ProcessedVideoInfo> list = new List<ProcessedVideoInfo>();
             this.operation = () =>
             {
                 ProcessedVideoAccess access = new ProcessedVideoAccess(this.Transaction);
                 list = access.GetProcessedVideosByPackageId(packageID);
             };
             this.Start(false);
             return list;
         }
         catch (Exception ex)
         {
             return null;
         }
     }

    }
}
