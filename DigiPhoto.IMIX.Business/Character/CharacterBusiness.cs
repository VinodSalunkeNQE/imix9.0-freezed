﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using DigiPhoto.DataLayer.Model;
using System.Xml.Linq;
using System.Data;
using System.ComponentModel;
using System.Collections;
using System.Reflection;
using System.Globalization;
using DigiPhoto.IMIX.Model;
using DigiPhoto.IMIX.DataAccess;
using DigiPhoto.IMIX.Business;
using DigiPhoto.Cache.Repository;
using DigiPhoto.Cache.DataCache;
using DigiPhoto.Cache.MasterDataCache;

namespace DigiPhoto.IMIX.Business
{
    public class CharacterBusiness : BaseBusiness
    {
        public List<CharacterInfo> GetCharacter()
        {
            List<CharacterInfo> objectInfo = new List<CharacterInfo>();
            this.operation = () =>
            {
                CharacterDao access = new CharacterDao(this.Transaction);
                objectInfo = access.GetCharacter();
            };
            this.Start(false);
            return objectInfo;
        }
        public int? GetCharacterId(string PhotographerId)
        {
            int? CharacterID = null;
            this.operation = () =>
            {
                CharacterDao access = new CharacterDao(this.Transaction);
                CharacterID = access.GetCharacterId(PhotographerId);
            };
            this.Start(false);
            return CharacterID;
        }
        public List<CharacterInfo> GetCharacter(CharacterInfo obj)
        {
            List<CharacterInfo> objectInfo = new List<CharacterInfo>();
            this.operation = () =>
            {
                CharacterDao access = new CharacterDao(this.Transaction);
                objectInfo = access.GetCharacter(obj);
            };
            this.Start(false);
            if (obj.DG_Character_OperationType == 3)
            {
                ICacheRepository imixCache = DataCacheFactory.GetFactory<ICacheRepository>(typeof(CharacterCache).FullName);
                imixCache.RemoveFromCache();
            }
            return objectInfo;
        }


        public bool InsertCharacter(CharacterInfo charInfo)
        {
            bool CharacterID = false;
            this.operation = () =>
            {
                CharacterDao access = new CharacterDao(this.Transaction);
                CharacterID = access.InsertCharacter(charInfo);
            };
            this.Start(false);
            ICacheRepository imixCache = DataCacheFactory.GetFactory<ICacheRepository>(typeof(CharacterCache).FullName);
            imixCache.RemoveFromCache();

            return CharacterID;
        }


    }

}
