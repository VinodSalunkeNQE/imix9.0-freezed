﻿using DigiPhoto.IMIX.DataAccess;
using DigiPhoto.IMIX.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DigiPhoto.Utility.Repository.ValueType;
using DigiPhoto.Cache.MasterDataCache;
using DigiPhoto.Cache.Repository;
using DigiPhoto.Cache.DataCache;

namespace DigiPhoto.IMIX.Business
{
    public class SemiOrderBusiness : BaseBusiness
    {
        public List<SemiOrderSettingsInfo> GetSemiOrderSettings()
        {
            try
            {
                ErrorHandler.ErrorHandler.LogFileWrite("GetSemiOrderSettings : 20 :  : ");
                List<SemiOrderSettingsInfo> list = new List<SemiOrderSettingsInfo>();
                this.operation = () =>
                {
                    SemiOrderAccess access = new SemiOrderAccess(this.Transaction);
                    list = access.GetSemiOrderSettings();
                };
                this.Start(false);
                ErrorHandler.ErrorHandler.LogFileWrite("GetSemiOrderSettings : 28 :  : ");
                return list;
            }
            catch
            {
                ErrorHandler.ErrorHandler.LogFileWrite("GetSemiOrderSettings : 33 : Exception  : ");
                return null;
            }
        }
        private List<SemiOrderSettings> SelectSemiOrderSettings()
        {
            try
            {
                ICacheRepository oCache = DataCacheFactory.GetFactory<ICacheRepository>(typeof(SemiOrderSettingsInfo).FullName);
                return ((List<SemiOrderSettings>)oCache.GetData());
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
                return new List<SemiOrderSettings>() ;
            }
        }
        public List<SemiOrderSettings> GetLstSemiOrderSettings(int? substoreId)
        {
            try
            {
                List<SemiOrderSettings> objList = new List<SemiOrderSettings>();
                this.operation = () =>
                {
                    SemiOrderSettingsDao access = new SemiOrderSettingsDao(this.Transaction);
                    objList = access.GetSemiOrderSettings(substoreId, null);
                };
                this.Start(false);
                return objList;
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
                return null;
            }
        }

        public List<SemiOrderSettings> GetSemiOrderSetting(int? substoreId, int locationId)
        {
            try
            {
                List<SemiOrderSettings> obj = new List<SemiOrderSettings>();
                this.operation = () =>
                {
                    SemiOrderSettingsDao access = new SemiOrderSettingsDao(this.Transaction);
                    obj = access.GetSemiOrderSettings(substoreId, locationId);
                };
                this.Start(false);
                return obj;
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
                return null;
            }
        }


        public List<SemiOrderSettings> GetSemiOrderSettings(int? substoreId, int locationId)
        {
            try
            {
                List<SemiOrderSettings> obj = new List<SemiOrderSettings>();
                this.operation = () =>
                {
                    SemiOrderSettingsDao access = new SemiOrderSettingsDao(this.Transaction);
                    obj = access.GetSemiOrderSettings(substoreId, locationId);
                };
                this.Start(false);
                return obj;
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
                return null;
            }
        }

        public List<SpecProfileProductMappingInfo> GetSemiOrderProfileProductMapping(int ProfileId)
        {
            try
            {
                List<SpecProfileProductMappingInfo> obj = new List<SpecProfileProductMappingInfo>();
                this.operation = () =>
                {
                    SemiOrderAccess access = new SemiOrderAccess(this.Transaction);
                    obj = access.GetSemiOrderProfileProductMapping(ProfileId);
                };
                this.Start(false);
                return obj;
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
                return null;
            }
        }

        /// <summary>
        /// Determines whether [is semi order image] [the specified order details unique identifier].
        /// </summary>
        /// <param name="OrderDetailsID">The order details unique identifier.</param>
        /// <returns></returns>

        public bool IsSemiOrderImage(int OrderDetailsID)
        {
            OrderDetailInfo objectInfo = new OrderDetailInfo();
            this.operation = () =>
            {
                OrderDetailsDao access = new OrderDetailsDao(this.Transaction);
                objectInfo = access.GetSemiOrderImage(OrderDetailsID); 
            };
            this.Start(false);
            if (objectInfo.DG_Orders_ID == null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool  DeleteSpecSetting(int id)
        {
            bool result = false;
            this.operation = () =>
            {
                SemiOrderSettingsDao access = new SemiOrderSettingsDao(this.Transaction);
                result = access.DeleteSpecSetting(id);
            };
            this.Start(false);
            return result;
        }
    }
}
