﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DigiPhoto.IMIX.Model;
using DigiPhoto.IMIX.DataAccess;
using System.Data;
namespace DigiPhoto.IMIX.Business
{
    public class ReportBusiness : BaseBusiness
    {
        public DataSet GetActivityReport(DateTime fromDate, DateTime toDate, int userId)
        {
            var activityDataSet = new DataSet();
            this.operation = () =>
            {
                activityDataSet = (new ActivityDao(this.Transaction)).GetActivityReport(fromDate, toDate, userId);
            };
            this.Start(false);
            return activityDataSet;
        }
        public DataSet GetPrintSummaryDetail(DateTime from, DateTime to, int SubStoreId)
        {
            DataSet lstPrintSummaryDetail = new DataSet();

            this.operation = () =>
            {
                ReportAccess access = new ReportAccess(this.Transaction);
                lstPrintSummaryDetail = access.GetPrintSummaryDetail(from, to, SubStoreId);
            };
            this.Start(false);

            return lstPrintSummaryDetail;
        }

        public DataSet GetOperatorReports(int CurrencyId, DateTime FromDate, DateTime ToDate, DateTime secfromDate, DateTime SecToDate, String StoreName, String UserName, bool Comparision)
        {
            DataSet result = new DataSet();
            this.operation = () =>
              {
                  ReportDao access = new ReportDao(this.Transaction);

                  result = access.SelectOperatorPerformanceReport(CurrencyId, FromDate, ToDate, secfromDate, SecToDate, Comparision, StoreName, UserName);
              };
            this.Start(false);
            return result;

        }


        public DataSet GetTakingReport(bool ISFromDate, bool IsToDate, DateTime? FromDate, DateTime? ToDate, int UserID, int SubStorePKey)
        {

            DataSet dbResult = new DataSet();
            this.operation = () =>
           {
               ReportDao access = new ReportDao(this.Transaction);
               // access.CommandTimeout = 60000;
               dbResult = access.SelectTakingReport(FromDate, ToDate, SubStorePKey);
           };
            this.Start(false);
            return dbResult;

        }
        public DataSet GetAuditTrail(DateTime FromDate, DateTime ToDate, int SubStorePKey)
        {
            DataSet result = new DataSet();
            this.operation = () =>
              {
                  ReportDao access = new ReportDao(this.Transaction);
                  //context.CommandTimeout = 60000;
                  result = access.SelectOperationalAudit(FromDate, ToDate, SubStorePKey);

              };
            this.Start(false);
            return result;
        }


        public DataSet GetLocationPerformanceReports(DateTime FromDate, DateTime ToDate, DateTime secfromDate, DateTime SecToDate, String StoreName, String UserName, bool Comparision, string subStore, int SubStoreId)
        {
            DataSet result = new DataSet();
            this.operation = () =>
         {
             ReportDao rptBiz = new ReportDao(this.Transaction);
             //context.CommandTimeout = 60000;
             result = rptBiz.SelectLocationPerformance(FromDate, ToDate, secfromDate, SecToDate, Comparision, StoreName, UserName, SubStoreId);
             // ForEach(o => { o.selectedSubStore = subStore; o.FromDate = FromDate; o.ToDate = ToDate; });TBD
         };
            this.Start(false);
            return result;

        }

        public DataSet GetFinancialAuditData(string username, string storename, DateTime startdat, DateTime enddate, int SubStoreId)
        {
            DataSet dsResult = new DataSet();
            this.operation = () =>
                 {
                     ReportDao access = new ReportDao(this.Transaction);
                     dsResult = access.SelectFinancialAudit(startdat, enddate, username, storename, SubStoreId);
                 };
            this.Start(false);
            return dsResult;

        }
        public DataSet GetOrderDetailReport(DateTime FromDate, DateTime ToDate, String StoreName, String UserName)
        {
            DataSet dsResult = new DataSet();

            this.operation = () =>
                {
                    ReportDao access = new ReportDao(this.Transaction);
                    dsResult = access.GetOrderDetailedDiscount(FromDate, ToDate, StoreName, UserName);
                };
            this.Start(false);
            return dsResult;

        }


        public DataSet GetPhotographerPerformanceReports(DateTime FromDate, DateTime ToDate, DateTime secfromDate, DateTime SecToDate, String StoreName, String UserName, bool Comparision)
        {
            DataSet result = new DataSet(); ;
            this.operation = () =>
                    {
                        ReportDao access = new ReportDao(this.Transaction);
                        //   context.CommandTimeout = 60000;
                        result = access.SelectPhotgrapherPerformance(FromDate, ToDate, secfromDate, SecToDate, Comparision, StoreName, UserName);
                    };
            this.Start(false);
            return result;

        }
        #region
        //Added by Saroj on 04-12-2019 to get the result for video products performance Report 
        public DataSet GetPhotoGrapherVideoReports(DateTime FromDate, DateTime ToDate, DateTime secfromDate, DateTime SecToDate, String StoreName, String UserName, bool Comparision)
        {
            DataSet result = new DataSet(); ;
            this.operation = () =>
            {
                ReportDao access = new ReportDao(this.Transaction);
                result = access.SelectPhotoGrapherVideo(FromDate, ToDate, secfromDate, SecToDate, Comparision, StoreName, UserName);
            };
            this.Start(false);
            return result;

        }
        #endregion
        public DataSet GetDataForPrintingLog(DateTime fromdate, DateTime todate,int SubStoreId)
        {
            DataSet result = new DataSet();
            this.operation = () =>
                {
                    ReportDao access = new ReportDao(this.Transaction);
                    result = access.SelectPrintedProduct(fromdate, todate, SubStoreId);
                };
            this.Start(false);
            return result;
        }
        public DataSet GetDataForPrintingSummary(DateTime fromdate, DateTime todate, int SubStoreId)
        {
            DataSet result = new DataSet();
            this.operation = () =>
                {
                    ReportDao access = new ReportDao(this.Transaction);
                    result = access.GetPrintSummary(fromdate, todate, SubStoreId);
                };

            this.Start(false);
            return result;

        }

        #region Collecting & Purging Report Data

        public void CollectOrderDetailedDiscountData(DateTime fromdate, DateTime todate)
        {
            this.operation = () =>
            {
                ReportDao access = new ReportDao(this.Transaction);
                access.CollectOrderDetailedDiscountData(fromdate, todate);
            };
            this.Start(false);

        }
        public void PurgeOrderDetailedDiscountData(DateTime fromdate, DateTime todate)
        {
            this.operation = () =>
            {
                ReportDao access = new ReportDao(this.Transaction);
                access.PurgeOrderDetailedDiscountData(fromdate, todate);
            };
            this.Start(false);

        }

        public void CollectActivityReportData(DateTime fromdate, DateTime todate)
        {
            this.operation = () =>
            {
                ReportDao access = new ReportDao(this.Transaction);
                access.CollectActivityReportData(fromdate, todate);
            };
            this.Start(false);

        }
        public void PurgeActivityReportData(DateTime fromdate, DateTime todate)
        {
            this.operation = () =>
            {
                ReportDao access = new ReportDao(this.Transaction);
                access.PurgeActivityReportData(fromdate, todate);
            };
            this.Start(false);

        }

        public void CollectOperatorPerformanceData(DateTime fromdate, DateTime todate)
        {
            this.operation = () =>
            {
                ReportDao access = new ReportDao(this.Transaction);
                access.CollectOperatorPerformanceData(fromdate, todate);
            };
            this.Start(false);

        }
        public void PurgeOperatorPerformanceData(DateTime fromdate, DateTime todate)
        {
            this.operation = () =>
            {
                ReportDao access = new ReportDao(this.Transaction);
                access.PurgeOperatorPerformanceData(fromdate, todate);
            };
            this.Start(false);

        }

        public void CollectFinancialAuditData(DateTime fromdate, DateTime todate)
        {
            this.operation = () =>
            {
                ReportDao access = new ReportDao(this.Transaction);
                access.CollectFinancialAuditData(fromdate, todate);
            };
            this.Start(false);

        }
        public void PurgeFinancialAuditData(DateTime fromdate, DateTime todate)
        {
            this.operation = () =>
            {
                ReportDao access = new ReportDao(this.Transaction);
                access.PurgeFinancialAuditData(fromdate, todate);
            };
            this.Start(false);

        }
        #endregion

        public List<PaymentSummaryInfo> GetPaymentSummary(DateTime FromDt, DateTime ToDt, string storeName,int substoreID=0)
        {
            List<PaymentSummaryInfo> PaymentSummaryInfoLst = new List<PaymentSummaryInfo>();
            this.operation = () =>
            {
                ReportAccess access = new ReportAccess(this.Transaction);
                PaymentSummaryInfoLst = access.GetPaymentSummary(FromDt, ToDt, storeName, substoreID);
            };
            this.Start(false);

            return PaymentSummaryInfoLst;
        }
        public DataSet GetDataForIPPrintTracking(DateTime fromdate, DateTime todate, int SubStoreId,int packageID)
        {
            DataSet result = new DataSet();
            this.operation = () =>
            {
                ReportDao access = new ReportDao(this.Transaction);
                result = access.GetIPPrintTracking(fromdate, todate, SubStoreId, packageID);
            };

            this.Start(false);
            return result;

        }
        public DataSet GetDataForIPContentTracking(DateTime fromdate, DateTime todate, int SubStoreId, int packageID)
        {
            DataSet result = new DataSet();
            this.operation = () =>
            {
                ReportDao access = new ReportDao(this.Transaction);
                result = access.GetIPContentTracking(fromdate, todate, SubStoreId, packageID);
            };

            this.Start(false);
            return result;

        }
        public ReportParams FetchReportFormatDetails(int reportType)
        {
            ReportParams reportParam = new ReportParams();
            this.operation = () =>
            {
                ReportDao access = new ReportDao(this.Transaction);
                reportParam = access.FetchReportFormatDetails(reportType);
            };

            this.Start(false);
            return reportParam;

        }
        /////changed by latika for evoucher report 25 March 20202
       public DataSet SelectEvoucherDetails(DateTime FromDate, DateTime ToDate)
        {
            DataSet result = new DataSet(); ;
            this.operation = () =>
            {
                ReportDao access = new ReportDao(this.Transaction);
        result = access.SelectEvoucherDetails(FromDate, ToDate);
            };
            this.Start(false);
            return result;

    }

}
}
