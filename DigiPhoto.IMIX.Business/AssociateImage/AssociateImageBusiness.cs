﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using DigiPhoto.DataLayer.Model;
using System.Xml.Linq;
using System.Data;
using System.ComponentModel;
using System.Collections;
using System.Reflection;
using System.Globalization;
using DigiPhoto.IMIX.Model;
using DigiPhoto.IMIX.DataAccess;
using DigiPhoto.IMIX.Business;
using DigiPhoto.DataLayer;


namespace DigiPhoto.IMIX.Business
{
    public partial class AssociateImageBusiness : BaseBusiness
    {

        /// <summary>
        ///Checks if the Code(QR/Bar/unique) already exists.
        /// </summary>
        /// <param name="QRCode"></param>
        /// <param name="CodeType"></param>
        /// <returns></returns>
        public bool IsUniqueCodeExists(string QRCode, bool IsAnonymousQrCodeEnabled)
        {
            try
            {
                List<iMixImageAssociationInfo> iMixImageAssociationInfoList = new List<iMixImageAssociationInfo>();
                this.operation = () =>
                    {
                        IMixImageAssociationDao access = new IMixImageAssociationDao(this.Transaction);
                        iMixImageAssociationInfoList = access.GetUniqueCodeExists(IsAnonymousQrCodeEnabled, QRCode);
                    };
                this.Start(false);

                if (iMixImageAssociationInfoList != null && iMixImageAssociationInfoList.Count>0)
                    return true;
                else
                    return false;
            }
            catch
            {
                return false;
            }

        }


        /// <summary>
        /// Returns "1" when success else error message. 
        /// </summary>
        /// <param name="QRCode"></param>
        /// <param name="CodeType"></param>
        /// <param name="OverWriteStatus">Status to overrite the existing association</param>
        /// <returns></returns>
        public string AssociateImage(int CodeType, string QRCode, string PhotoIds, int OverWriteStatus, bool IsAnonymousEnabled, int Nationality,string EmailID)
        {
            string result = string.Empty;
            this.operation = () =>
                {
                    AssociatedImageDao access = new AssociatedImageDao(this.Transaction);
                    result = access.AssociateImage(OverWriteStatus, IsAnonymousEnabled, QRCode, PhotoIds, Nationality, EmailID);
                };
            this.Start(false);
            return result.ToString();
        }
        public void AssociateMobileImage(string QRCode, int PhotoId)
        {
            string result = string.Empty;
            this.operation = () =>
            {
                AssociatedImageDao access = new AssociatedImageDao(this.Transaction);
                access.AssociateMobileImage(QRCode, PhotoId);
            };
            this.Start(false);
        }
        public void AssociateVideos(int PhotoId, int VideoId)
        {
            string result = string.Empty;
            this.operation = () =>
            {
                AssociatedImageDao access = new AssociatedImageDao(this.Transaction);
                access.AssociateVideos(PhotoId, VideoId);
            };
            this.Start(false);
        }
        /// <summary>
        /// created by latika for presold service Remove association
        /// </summary>
        /// <param name="CodeType"></param>
        /// <param name="QRCode"></param>
        /// <param name="PhotoIds"></param>
        /// <param name="OverWriteStatus"></param>
        /// <param name="IsAnonymousEnabled"></param>
        /// <returns></returns>
        public int RemoveAssociateImage(string QRCode, string PhotoIds,bool isShowCount)
        {
            int result = 0;
            this.operation = () =>
            {
                AssociatedImageDao access = new AssociatedImageDao(this.Transaction);
                result = access.RemoveAssociateImage(QRCode, PhotoIds, isShowCount);
            };
            this.Start(false);
            return result;
        }
        ////end by latika
    }
}
