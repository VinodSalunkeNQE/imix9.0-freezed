﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using DigiPhoto.DataLayer.Model;
using System.Xml.Linq;
using System.Data;
using System.ComponentModel;
using System.Collections;
using System.Reflection;
using System.Globalization;
using DigiPhoto.IMIX.Model;
using DigiPhoto.IMIX.DataAccess;
using DigiPhoto.IMIX.Business;
using DigiPhoto.DataLayer;
using DigiPhoto.Utility.Repository.ValueType;
namespace DigiPhoto.IMIX.Business
{
    public class SoftwareHealthCheckBusiness : BaseBusiness
    {
        public List<ServicesInfo> GetRunningServices()
        {
            try
            {
                List<ServicesInfo> objectList = new List<ServicesInfo>();
                this.operation = () =>
                {
                    ServicesDao access = new ServicesDao(this.Transaction);
                    objectList = access.GetServices();
                };
                this.Start(false);
                return objectList;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public List<VersionHistoryInfo> GetVersionDetails(string MachineName)
        {

            List<VersionHistoryInfo> DG_VersionHistoryList = new List<VersionHistoryInfo>();
            this.operation = () =>
            {
                VersionHistoryDao access = new VersionHistoryDao(this.Transaction);
                DG_VersionHistoryList = access.GetVersionDetails(MachineName);
            };
            this.Start(false);
            return DG_VersionHistoryList;
        }
    }
}
