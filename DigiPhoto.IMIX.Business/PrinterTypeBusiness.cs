﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DigiPhoto.IMIX.DataAccess;
using DigiPhoto.IMIX.Model;

namespace DigiPhoto.IMIX.Business
{
    public class PrinterTypeBusiness : BaseBusiness
    {

        public bool SavePrinterType(PrinterTypeInfo lstPrinterInfo)
        {
            try
            {
                bool issucess = false;
                this.operation = () =>
                {
                    PrinterTypeAccess access = new PrinterTypeAccess(this.Transaction);
                    issucess = access.SaveUpdatePrinterType(lstPrinterInfo);
                };
                this.Start(false);
                return issucess;
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogFileWrite(ErrorHandler.ErrorHandler.CreateErrorMessage(ex));
                return false;
            }
        }
        public List<PrinterTypeInfo> GetPrinterTypeList(int PrinterTypeId)
        {
            try
            {
                List<PrinterTypeInfo> objLst = new List<PrinterTypeInfo>();
                this.operation = () =>
                {
                    PrinterTypeAccess access = new PrinterTypeAccess(this.Transaction);
                    objLst = access.GetPrinterTypeList(PrinterTypeId);
                };
                this.Start(false);
                return objLst;
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogFileWrite(ErrorHandler.ErrorHandler.CreateErrorMessage(ex));
                return null;
            }
        }
        public bool DeletePrinterType(int PrinterTypeId)
        {
            try
            {
                bool issucess = false;
                this.operation = () =>
                {
                    PrinterTypeAccess access = new PrinterTypeAccess(this.Transaction);
                    issucess = access.DeletePrinterType(PrinterTypeId);
                    //  return list;
                };
                this.Start(false);
                return issucess;
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogFileWrite(ErrorHandler.ErrorHandler.CreateErrorMessage(ex));
                throw ex;
            }

        }
        public bool RemapNewPrinter(string PrinterName, int SubstoreId, bool active)
        {
            try
            {
                bool issucess = false;
                this.operation = () =>
                {
                    PrinterTypeAccess access = new PrinterTypeAccess(this.Transaction);
                    issucess = access.RemapNewPrinter(PrinterName, SubstoreId, active);
                };
                this.Start(false);
                return issucess;
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogFileWrite(ErrorHandler.ErrorHandler.CreateErrorMessage(ex));
                throw ex;
            }

        }
        public bool SaveOrActivateNewPrinter(string PrinterName, int SubstoreId, bool active)
        {
            try
            {
                bool issucess = false;
                this.operation = () =>
                {
                    PrinterTypeAccess access = new PrinterTypeAccess(this.Transaction);
                    issucess = access.SaveOrActivateNewPrinter(PrinterName, SubstoreId, active);
                };
                this.Start(false);
                return issucess;
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogFileWrite(ErrorHandler.ErrorHandler.CreateErrorMessage(ex));
                throw ex;
            }

        }

        public bool DeleteAssociatedPrinters(int SubstoreId)
        {
            try
            {
                bool issucess = false;
                this.operation = () =>
                {
                    PrinterTypeAccess access = new PrinterTypeAccess(this.Transaction);
                    issucess = access.DeleteAssociatedPrinters(SubstoreId);
                };
                this.Start(false);
                return issucess;
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogFileWrite(ErrorHandler.ErrorHandler.CreateErrorMessage(ex));
                throw ex;
            }
        }
    }
}
