﻿using DigiPhoto.IMIX.DataAccess.Base;
using DigiPhoto.IMIX.Model.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DigiPhoto.IMIX.Business.StoryBook
{ 
   public class StoryBookBusiness:BaseBusiness
    {

        public List<StoryInfo> GetStoryBookDetails()
        {

            List<StoryInfo> _objList = new List<StoryInfo>();
            this.operation = () =>
            {
                StoryBookDao access = new StoryBookDao(this.Transaction);
                _objList = access.Select(null);
            };
            this.Start(false);
            return _objList;
        }

    }
}
