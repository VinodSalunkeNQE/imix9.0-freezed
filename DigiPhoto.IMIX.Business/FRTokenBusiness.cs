﻿using DigiPhoto.IMIX.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DigiPhoto.IMIX.Model;

namespace DigiPhoto.IMIX.Business
{
    public class FRTokenBusiness : BaseBusiness
    {
        /// <summary>
        /// Added by Kailash ON 25 SEP 2019 to FETCH FR token API
        /// </summary>
        /// <param name="client_id">ID OF API Client</param>
        /// <returns></returns>
        public FRAPIDetails GetToken(FRAPIDetails fRAPIDetails)
        {
            FRAPIDetails fRAPI = fRAPIDetails;
            try
            {
               
                this.operation = () =>
                {
                    FRTokenAccess access = new FRTokenAccess(this.Transaction);
                    fRAPI = access.GetToken(fRAPIDetails);
                    //  return list;
                };
                this.Start(false);
               
            }
            catch(Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
            }
            return fRAPI;
        }
        /// <summary>
        /// Added by Kailash ON 25 SEP 2019 to SAVE FR token API
        /// </summary>
        /// <param name="client_id">ID OF API Client</param>
        /// <returns></returns>
        public void SaveNewToken(FRAPIDetails fRAPIDetails)
        {
            try
            {
                FRAPIDetails fRAPI = fRAPIDetails;
                this.operation = () =>
                {
                    FRTokenAccess access = new FRTokenAccess(this.Transaction);
                     access.SaveNewToken(fRAPIDetails);
                    //  return list;
                };
                this.Start(false);
            }
            catch( Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
            }
        }
    }
}
