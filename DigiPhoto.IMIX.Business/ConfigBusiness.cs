﻿using DigiPhoto.Cache.DataCache;
using DigiPhoto.Cache.MasterDataCache;
using DigiPhoto.Cache.Repository;
using Digiphoto.Cache.SqlDependentCache;
using DigiPhoto.IMIX.DataAccess;
using DigiPhoto.IMIX.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace DigiPhoto.IMIX.Business
{
    public partial class ConfigBusiness : BaseBusiness
    {
        public bool SaveUpdateConfigLocation(List<iMixConfigurationLocationInfo> lstIMixConfigLocInfo)
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("ConfigurationMasterId");
            dt.Columns.Add("ConfigurationValue");
            dt.Columns.Add("SubstoreId");
            dt.Columns.Add("LocationId");
            foreach (iMixConfigurationLocationInfo item in lstIMixConfigLocInfo)
            {
                dt.Rows.Add(item.IMIXConfigurationMasterId, item.ConfigurationValue, item.SubstoreId, item.LocationId);
            }
            bool issucess = false;
            this.operation = () =>
            {
                ConfigAccess access = new ConfigAccess(this.Transaction);
                issucess = access.SaveUpdateConfigLocation(dt);
            };
            this.Start(false);

            return issucess;
        }
        public bool SaveUpdateNewConfig(List<iMIXConfigurationInfo> lstConfigValue)
        {
            //convert to data table

            DataTable dt = new DataTable();
            dt.Columns.Add("ConfigurationMasterId");
            dt.Columns.Add("ConfigurationValue");
            dt.Columns.Add("SubstoreId");
            foreach (iMIXConfigurationInfo item in lstConfigValue)
            {
                dt.Rows.Add(item.IMIXConfigurationMasterId, item.ConfigurationValue, item.SubstoreId);
            }

            bool issucess = false;
            this.operation = () =>
            {
                ConfigAccess access = new ConfigAccess(this.Transaction);
                issucess = access.SaveUpdateNewConfig(dt);
            };
            this.Start(false);
            SqlDepandencyCache.ClearCacheNewConfigValues();
            //Clear the item to update new changes
            ICacheRepository imixCache = DataCacheFactory.GetFactory<ICacheRepository>(typeof(ImixConfigurationCache).FullName);
            imixCache.RemoveFromCache();
            ConfigManager.IMIXConfigurations.Clear();
            return issucess;
        }
        #region Online status saved in DB- Ashirwad
        public bool SaveUpdateNewConfigForSync(List<iMIXConfigurationInfo> lstConfigValue)
        {
            //convert to data table

            DataTable dt = new DataTable();
            dt.Columns.Add("ConfigurationMasterId");
            dt.Columns.Add("ConfigurationValue");
            dt.Columns.Add("SubstoreId");
            foreach (iMIXConfigurationInfo item in lstConfigValue)
            {
                dt.Rows.Add(item.IMIXConfigurationMasterId, item.ConfigurationValue, item.SubstoreId);
            }

            bool issucess = false;
            this.operation = () =>
            {
                ConfigAccess access = new ConfigAccess(this.Transaction);
                issucess = access.SaveUpdateNewConfigForSync(dt);
            };
            this.Start(false);
            SqlDepandencyCache.ClearCacheNewConfigValues();
            //Clear the item to update new changes
            ICacheRepository imixCache = DataCacheFactory.GetFactory<ICacheRepository>(typeof(ImixConfigurationCache).FullName);
            imixCache.RemoveFromCache();
            ConfigManager.IMIXConfigurations.Clear();
            return issucess;
        }
        #endregion
        public Dictionary<long, string> GetConfigurations(Dictionary<long, string> iMIXConfigurations, int subStoreId)
        {
            //bool issucess = false;
            //List<iMIXConfigurationInfo> list = new List<iMIXConfigurationInfo>();
            if (iMIXConfigurations.Count == 0)
            {
                ConfigBusiness obj = new ConfigBusiness();
                this.operation = () =>
                {
                    ConfigAccess access = new ConfigAccess(this.Transaction);
                    iMIXConfigurations = access.GetConfigurations(subStoreId).ToDictionary(k => k.IMIXConfigurationMasterId, k => k.ConfigurationValue); ;
                };
                this.Start(false);
            }

            // temp = iMIXConfigurations;
            return iMIXConfigurations;
        }

        public List<iMixConfigurationLocationInfo> GetConfigLocation(int LocationId, int SubstoreId)
        {
            List<iMixConfigurationLocationInfo> lstConfigLocInfo = new List<iMixConfigurationLocationInfo>();
            ConfigBusiness obj = new ConfigBusiness();
            this.operation = () =>
            {
                ConfigAccess access = new ConfigAccess(this.Transaction);
                lstConfigLocInfo = access.GetConfigLocation(LocationId, SubstoreId);
            };
            this.Start(false);

            return lstConfigLocInfo;
        }
        public List<iMixConfigurationLocationInfo> GetConfigBasedOnLocation(int LocationId)
        {
            List<iMixConfigurationLocationInfo> lstConfigLocInfo = new List<iMixConfigurationLocationInfo>();
            ConfigBusiness obj = new ConfigBusiness();
            this.operation = () =>
            {
                ConfigAccess access = new ConfigAccess(this.Transaction);
                lstConfigLocInfo = access.GetConfigBasedOnLocation(LocationId);
            };
            this.Start(false);

            return lstConfigLocInfo;
        }

        public string GetHotFolderPath(int SubStoreId)
        {
            string digiHotFolderPath = string.Empty;
            ConfigBusiness obj = new ConfigBusiness();
            this.operation = () =>
            {
                ConfigAccess access = new ConfigAccess(this.Transaction);
                digiHotFolderPath = access.GetHotFolderPath(SubStoreId);
            };
            this.Start(false);


            // temp = iMIXConfigurations;
            return digiHotFolderPath;
        }

        public FolderStructureInfo GetFolderStructureInfo(int subStoreId)
        {
            FolderStructureInfo folderStructureInfo = new FolderStructureInfo();
            this.operation = () =>
            {
                ConfigAccess access = new ConfigAccess(this.Transaction);
                folderStructureInfo = access.GetFolderStructureInfo(subStoreId);
            };
            this.Start(false);

            return folderStructureInfo;
        }

        public bool UpdateTriggerStatus(List<SyncTriggerStatusInfo> lstTriggerStatus)
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("TableId");
            dt.Columns.Add("TableName");
            dt.Columns.Add("IsSyncTriggerEnable");
            foreach (SyncTriggerStatusInfo item in lstTriggerStatus)
            {
                dt.Rows.Add(item.TableId, item.TableName, item.IsSyncTriggerEnable);
            }
            bool issucess = false;
            this.operation = () =>
            {
                ConfigAccess access = new ConfigAccess(this.Transaction);
                issucess = access.UpdateTriggerStatus(dt);
            };
            this.Start(false);
            return issucess;
        }
        public bool UpdateSyncPriority(List<SyncPriority> lstSyncPriority)
        {

            bool issucess = false;
            this.operation = () =>
            {
                ConfigAccess access = new ConfigAccess(this.Transaction);
                issucess = access.UpdateSyncPriority(lstSyncPriority);
            };
            this.Start(false);
            return issucess;
        }
        public List<SyncTriggerStatusInfo> GetAllSyncTriggerTables()
        {
            List<SyncTriggerStatusInfo> syncTableList = new List<SyncTriggerStatusInfo>();
            this.operation = () =>
            {
                ConfigAccess access = new ConfigAccess(this.Transaction);
                syncTableList = access.GetAllSyncTriggerTables();
            };
            this.Start(false);
            return syncTableList;
        }


        public List<SyncPriority> GetAllSyncPriority()
        {
            List<SyncPriority> syncTableList = new List<SyncPriority>();
            this.operation = () =>
            {
                ConfigAccess access = new ConfigAccess(this.Transaction);
                syncTableList = access.GetAllSyncPriority();
            };
            this.Start(false);
            return syncTableList;
        }

        public int GetSubstoreLocationWise(int locationid)
        {
            int Substore = 0;
            ConfigBusiness obj = new ConfigBusiness();
            this.operation = () =>
            {
                ConfigAccess access = new ConfigAccess(this.Transaction);
                Substore = access.GetSubstoreLocationWise(locationid);
            };
            this.Start(false);
            return Substore;
        }

        public bool SaveEmailDetails(string toAddress, string toBCC, string sender, string msgBody, string msgType)
        {
            ConfigBusiness obj = new ConfigBusiness();
            bool isSuccess = false;
            this.operation = () =>
            {
                ConfigAccess access = new ConfigAccess(this.Transaction);
                isSuccess = access.SaveEmailDetails(toAddress, toBCC, sender, msgBody, msgType);
            };
            this.Start(false);
            return isSuccess;
        }
        public bool SaveEmailDetails(string toAddress, string toBCC, string sender, string msgBody, string msgType, int subStoreId)
        {
            ConfigBusiness obj = new ConfigBusiness();
            bool isSuccess = false;
            this.operation = () =>
            {
                ConfigAccess access = new ConfigAccess(this.Transaction);
                isSuccess = access.SaveEmailDetails(toAddress, toBCC, sender, msgBody, msgType, subStoreId);
            };
            this.Start(false);
            return isSuccess;
        }
        #region Audio Template
        public bool SaveUpdateAudioTemplate(AudioTemplateInfo lstAudioValue)
        {

            bool issucess = false;
            this.operation = () =>
            {
                ConfigAccess access = new ConfigAccess(this.Transaction);
                issucess = access.SaveUpdateAudioTemplate(lstAudioValue);

            };
            this.Start(false);
            return issucess;
        }
        public List<AudioTemplateInfo> GetAudioTemplateList()
        {
            List<AudioTemplateInfo> list = new List<AudioTemplateInfo>();
            this.operation = () =>
            {
                ConfigAccess access = new ConfigAccess(this.Transaction);
                list = access.GetAudioTemplateList(0);
            };
            this.Start(false);
            return list;
        }
        public AudioTemplateInfo GetAudioTemplateList(long audioID)
        {
            AudioTemplateInfo list = new AudioTemplateInfo();
            this.operation = () =>
            {
                ConfigAccess access = new ConfigAccess(this.Transaction);
                list = access.GetAudioTemplateList(audioID).FirstOrDefault();
                //  return list;
            };
            this.Start(false);
            return list;
        }
        public bool DeleteAudio(long audioId)
        {
            bool issucess = false;
            this.operation = () =>
            {
                ConfigAccess access = new ConfigAccess(this.Transaction);
                issucess = access.DeleteAudio(audioId);
                //  return list;
            };
            this.Start(false);
            return issucess;
        }


        #endregion Audio Template

        #region Video Template

        public bool SaveVideoTemplate(VideoTemplateInfo videoTemplate)
        {

            bool issucess = false;
            this.operation = () =>
            {
                ConfigAccess access = new ConfigAccess(this.Transaction);
                issucess = access.SaveVideoTemplate(videoTemplate);

            };
            this.Start(false);
            return issucess;
        }


        public bool SaveVideoSlot(VideoTemplateInfo videoTemplate)
        {

            bool issucess = false;
            this.operation = () =>
            {
                ConfigAccess access = new ConfigAccess(this.Transaction);
                issucess = access.SaveVideoSlot(videoTemplate);
            };
            this.Start(false);
            return issucess;
        }

        public List<VideoTemplateInfo> GetVideoTemplate()
        {
            List<VideoTemplateInfo> list = new List<VideoTemplateInfo>();
            this.operation = () =>
            {
                ConfigAccess access = new ConfigAccess(this.Transaction);
                list = access.GetVideoTemplate(0);
                //  return list;
            };
            this.Start(false);
            return list;
        }

        public VideoTemplateInfo GetVideoTemplate(long videoTemplateId)
        {
            VideoTemplateInfo videoTemplate = new VideoTemplateInfo();
            this.operation = () =>
            {
                ConfigAccess access = new ConfigAccess(this.Transaction);
                videoTemplate = access.GetVideoTemplate(videoTemplateId).FirstOrDefault();
                videoTemplate.videoSlots = (new ConfigAccess()).GetVideoSlots(videoTemplateId);
                //  return list;
            };
            this.Start(false);
            return videoTemplate;
        }
        public bool DeleteVideoTemplate(long videoId)
        {
            bool issucess = false;
            this.operation = () =>
            {
                ConfigAccess access = new ConfigAccess(this.Transaction);
                issucess = access.DeleteVideoTemplate(videoId);
                //  return list;
            };
            this.Start(false);
            return issucess;
        }

        #endregion

        #region Video Background
        public int SaveUpdateVideoBackground(VideoBackgroundInfo lstVideoBGValue)
        {

            int id = 0;
            this.operation = () =>
            {
                ConfigAccess access = new ConfigAccess(this.Transaction);
                id = access.SaveUpdateVideoBackground(lstVideoBGValue);
            };
            this.Start(false);
            return id;
        }
        public List<VideoBackgroundInfo> GetVideoBackgrounds()
        {
            List<VideoBackgroundInfo> list = new List<VideoBackgroundInfo>();
            this.operation = () =>
            {
                ConfigAccess access = new ConfigAccess(this.Transaction);
                list = access.GetVideoBackgrounds(0);
            };
            this.Start(false);
            return list;
        }
        public VideoBackgroundInfo GetVideoBackgrounds(long vidBgID)
        {
            VideoBackgroundInfo list = new VideoBackgroundInfo();
            this.operation = () =>
            {
                ConfigAccess access = new ConfigAccess(this.Transaction);
                list = access.GetVideoBackgrounds(vidBgID).FirstOrDefault();
            };
            this.Start(false);
            return list;
        }
        public bool DeleteVideoBackground(long vidBgID)
        {
            bool issucess = false;
            this.operation = () =>
            {
                ConfigAccess access = new ConfigAccess(this.Transaction);
                issucess = access.DeleteVideoBackground(vidBgID);
            };
            this.Start(false);
            return issucess;
        }
        #endregion Video Background

        # region Output Profiles
        //public long SaveUpdateVideoOutputProfile(VideoConfigProfiles lstProfiles)
        //{
        //    bool isuccess = false;
        //    long ProfileId = 0;
        //    this.operation = () =>
        //    {
        //        ConfigAccess access = new ConfigAccess(this.Transaction);
        //        ProfileId = access.SaveUpdateVideoOutputFormatProfile(lstProfiles);
        //    };
        //    this.Start(false);
        //    return ProfileId;
        //}

        public List<VideoSceneViewModel> GetVideoConfigProfileList(int profileId, int substoreid)
        {
            List<VideoSceneViewModel> list = new List<VideoSceneViewModel>();
            this.operation = () =>
            {
                ConfigAccess access = new ConfigAccess(this.Transaction);
                list = access.GetVideoConfigProfileList(profileId, substoreid);
            };
            this.Start(false);
            return list;
        }
        public VideoSceneViewModel GetVideoConfigProfileList(long profileId, int substoreid)
        {
            VideoSceneViewModel list = new VideoSceneViewModel();
            this.operation = () =>
            {
                ConfigAccess access = new ConfigAccess(this.Transaction);
                list = access.GetVideoConfigProfileList(profileId, substoreid).FirstOrDefault();
                //  return list;
            };
            this.Start(false);
            return list;
        }

        public bool DeleteProfile(long profileId)
        {
            bool issucess = false;
            this.operation = () =>
            {
                ConfigAccess access = new ConfigAccess(this.Transaction);
                issucess = access.DeleteVideoConfigProfile(profileId);
                //  return list;
            };
            this.Start(false);
            return issucess;
        }
        public List<StockShot> GetStockShotImagesList(Int64 ImageId)
        {
            List<StockShot> stockShotList = new List<StockShot>();
            this.operation = () =>
            {
                ConfigAccess access = new ConfigAccess(this.Transaction);
                stockShotList = access.GetStockShotImagesList(ImageId);
            };
            this.Start(false);
            return stockShotList;
        }
        public Int64 SaveUpdateStockShotImage(StockShot image)
        {
            Int64 SSId = 0;
            this.operation = () =>
            {
                ConfigAccess access = new ConfigAccess(this.Transaction);
                SSId = access.SaveUpdateStockShotImage(image);
            };
            this.Start(false);
            return SSId;
        }
        public bool DeleteStockShotImg(Int64 ImgId)
        {
            try
            {
                bool result = false;
                this.operation = () =>
                {
                    ConfigAccess access = new ConfigAccess(this.Transaction);
                    result = access.DeleteStockShotImg(ImgId);
                };
                this.Start(false);
                return result;

            }
            catch (Exception ex)
            {
                return false;
            }
        }

        #endregion



        public VideoConfigProfiles List { get; set; }
        public List<ConfigurationInfo> GetAllSubstoreConfigdata()
        {
            List<ConfigurationInfo> _objConfigurationInfo = new List<ConfigurationInfo>();
            this.operation = () =>
            {
                ConfigAccess access = new ConfigAccess(this.Transaction);
                _objConfigurationInfo = access.GetAllSubstoreConfigdata();
            };
            this.Start(false);
            return _objConfigurationInfo;
        }
        public VideoSceneViewModel GetVideoSceneBasedOnPhotoId(int photoId)
        {
            VideoSceneViewModel list = new VideoSceneViewModel();
            this.operation = () =>
            {
                ConfigAccess access = new ConfigAccess(this.Transaction);
                list = access.GetVideoSceneBasedOnPhotoId(photoId);
                //  return list;
            };
            this.Start(false);
            return list;
        }
        public int SaveCGConfigSetting(CGConfigSettings CGConfigSettings)
        {
            int result = 0;
            this.operation = () =>
            {
                ConfigAccess access = new ConfigAccess(this.Transaction);
                result = access.SaveCGConfigSetting(CGConfigSettings);
            };
            this.Start(false);

            return result;
        }
        public List<CGConfigSettings> GetCGConfigSettngs(int configId)
        {
            List<CGConfigSettings> _objConfigurationInfo = new List<CGConfigSettings>();
            this.operation = () =>
            {
                ConfigAccess access = new ConfigAccess(this.Transaction);
                _objConfigurationInfo = access.GetCGConfigSettngs(configId);
            };
            this.Start(false);
            return _objConfigurationInfo;
        }
        public bool DeleteCGConfigSettngs(int configId)
        {
            bool isSuccess = false;
            this.operation = () =>
            {
                ConfigAccess access = new ConfigAccess(this.Transaction);
                isSuccess = access.DeleteCGConfigSettngs(configId);
            };
            this.Start(false);
            return isSuccess;
        }
        public int SaveUpdateVideoOverlay(VideoOverlay lstVideoBGValue)
        {

            int id = 0;
            this.operation = () =>
            {
                ConfigAccess access = new ConfigAccess(this.Transaction);
                id = access.SaveUpdateVideoOverlay(lstVideoBGValue);
            };
            this.Start(false);
            return id;
        }
        public bool DeleteVideoOverlay(long videoId)
        {
            bool issucess = false;
            this.operation = () =>
            {
                ConfigAccess access = new ConfigAccess(this.Transaction);
                issucess = access.DeleteVideoOverlay(videoId);
                //  return list;
            };
            this.Start(false);
            return issucess;
        }

        public List<VideoOverlay> GetVideoOverlays()
        {
            List<VideoOverlay> list = new List<VideoOverlay>();
            this.operation = () =>
            {
                ConfigAccess access = new ConfigAccess(this.Transaction);
                list = access.GetVideoOverlays(0);
            };
            this.Start(false);
            return list;
        }
        public VideoOverlay GetVideoOverlays(long VideoOverlayId)
        {
            VideoOverlay list = new VideoOverlay();
            this.operation = () =>
            {
                ConfigAccess access = new ConfigAccess(this.Transaction);
                list = access.GetVideoOverlays(VideoOverlayId).FirstOrDefault();
            };
            this.Start(false);
            return list;
        }

        public List<iMIXStoreConfigurationInfo> GetStoreConfigData()
        {
            List<iMIXStoreConfigurationInfo> _objGetStoreConfigData = new List<iMIXStoreConfigurationInfo>();
            this.operation = () =>
            {
                ConfigAccess access = new ConfigAccess(this.Transaction);
                _objGetStoreConfigData = access.GetStoreConfigData();
            };
            this.Start(false);
            return _objGetStoreConfigData;
        }

        public bool CheckDummyScan(long photographerID)
        {
            bool result = false;
            this.operation = () =>
            {
                ConfigurationDao access = new ConfigurationDao(this.Transaction);
                result = access.CheckDummyScan(photographerID);
            };
            this.Start(false);
            return result;
        }
        public List<ConfigurationInfo> GetDeafultPathlist()
        {

            List<ConfigurationInfo> _objConfig = new List<ConfigurationInfo>();
            this.operation = () =>
            {
                ConfigurationDao access = new ConfigurationDao(this.Transaction);
                _objConfig = access.GetDeafultPathlist();
            };
            this.Start(false);
            return _objConfig;
        }

        public bool DeleteConfigLocation(List<int> lstIMixConfigLocInfo, int substoreId, int locationId)
        {
            StringBuilder ids = new StringBuilder(lstIMixConfigLocInfo[0].ToString());
            for (int x = 1; x < lstIMixConfigLocInfo.Count; x++)
            {
                ids.Append(',');
                ids.Append(lstIMixConfigLocInfo[x]);
            }
            bool issucess = false;
            this.operation = () =>
            {
                ConfigAccess access = new ConfigAccess(this.Transaction);
                issucess = access.DeleteConfigLocation(ids.ToString(), substoreId, locationId);
            };
            this.Start(false);

            return issucess;
        }
        //////////////created by latika for table work flow
        public int SaveUpdateRFIDTableWorkflow(iMIXRFIDTableWorkflowInfo iRfidTabeWOrlinfo)
        {


            int issucess = 0;
            this.operation = () =>
            {
                ConfigAccess access = new ConfigAccess(this.Transaction);
                issucess = access.SaveUpdateRFIDTableWorkflow(iRfidTabeWOrlinfo);
            };
            this.Start(false);

            return issucess;
        }
        public List<iMIXRFIDTableWorkflowInfo> GetAllTableWorkFlow()
        {
            List<iMIXRFIDTableWorkflowInfo> lstRFIDTableWorkFlow = new List<iMIXRFIDTableWorkflowInfo>();
            this.operation = () =>
            {
                ConfigAccess access = new ConfigAccess(this.Transaction);
                lstRFIDTableWorkFlow = access.GetAllTableWorkFlow();
            };
            this.Start(false);
            return lstRFIDTableWorkFlow;
        }
        public List<iMIXRFIDTableWorkflowInfo> GetByIDTableWorkFlow(int Locationid)
        {
            List<iMIXRFIDTableWorkflowInfo> lstRFIDTableWorkFlow = new List<iMIXRFIDTableWorkflowInfo>();
            this.operation = () =>
            {
                ConfigAccess access = new ConfigAccess(this.Transaction);
                lstRFIDTableWorkFlow = access.GetByIDTableWorkFlow(Locationid);
            };
            this.Start(false);
            return lstRFIDTableWorkFlow;
        }
        ///////end by latika 
        public int CheckIsRemapPrintersAccess(int RoleID) ///created by latika to check rights of Remap printers
        {
            try
            {
                int result = 0;
                this.operation = () =>
                {
                    ConfigAccess access = new ConfigAccess(this.Transaction);
                    result = access.CheckIsRemapPrintersAccess(RoleID);
                };
                this.Start(false);
                return result;
            }
            catch
            {
                return 0;
            }
        }
        /// <summary>
        /// ////////////////create by latika for GroupdeletedSettings
        /// </summary>
        /// <returns></returns>
        public bool GETGroupDeleteID(int SubstoreID)
        {

            bool issucess = false;
            this.operation = () =>
            {
                ConfigAccess access = new ConfigAccess(this.Transaction);
                issucess = access.GETGroupDeleteID(SubstoreID);
            };
            this.Start(false);

            return issucess;
        }
        public int GETGroupDeleteMasterID()
        {

            int issucess = 0;
            this.operation = () =>
            {
                ConfigAccess access = new ConfigAccess(this.Transaction);
                issucess = access.GETGroupDeleteMasterID();
            };
            this.Start(false);

            return issucess;
        }
        /////////// end 

        /////////// end 
        //  -----------------------------added by latika Stop showing editing image in Client view----------------------------------------------------

        public int GETStopEditingImagesinclientViewMasterID()
        {

            int issucess = 0;
            this.operation = () =>
            {
                ConfigAccess access = new ConfigAccess(this.Transaction);
                issucess = access.GETStopEditingImagesinclientViewMasterID();
            };
            this.Start(false);

            return issucess;
        }
        public bool GETStopEditingImageClntvwBySubID(int SubStoreId)
        {

            bool issucess = false;
            this.operation = () =>
            {
                ConfigAccess access = new ConfigAccess(this.Transaction);
                issucess = access.GETStopEditingImageClntvwBySubID(SubStoreId);
            };
            this.Start(false);

            return issucess;
        }
        /////////// end 
        ///created by latika for Presold Service
        public int GETPreSoldDelayTmMasterID()
        {

            int issucess = 0;
            this.operation = () =>
            {
                ConfigAccess access = new ConfigAccess(this.Transaction);
                issucess = access.GETPreSoldDelayTmMasterID();
            };
            this.Start(false);

            return issucess;
        }


        public List<Presold> GETPreSoldDelayTmVal(int SubstoreID, int LocationID)
        {

            List<Presold> _objPresold = new List<Presold>();
            this.operation = () =>
            {
                ConfigAccess access = new ConfigAccess(this.Transaction);
                _objPresold = access.GETPreSoldDelayTmVal(SubstoreID, LocationID);
            };
            this.Start(false);
            return _objPresold;
        }

        /////end by latika
        ///

        #region Added  by ajay Water mark 10 Feb20
        /// <summary>
        /// Added by Ajay on 31 Jan 20 for watermark client view 
        /// </summary>
        /// <param name="objWatermarkModel"></param>
        /// <returns></returns>
        public bool SaveClientviewWatermark(WatermarkModel WatermarkModel)
        {

            bool issucess = false;
            this.operation = () =>
            {
                ConfigAccess access = new ConfigAccess(this.Transaction);

                issucess = access.SaveClientviewWatermark(WatermarkModel);
            };
            this.Start(false);

            return issucess;
        }

        /// <summary>
        ///Get the details of watermark  
        /// </summary>
        /// <param name="subStoreId"></param>
        /// <returns></returns>

        public WatermarkModel GetClientWatermarkConfig(int subStoreId)
        {
            WatermarkModel objWatermarkModel = null;
            this.operation = () =>
            {
                ConfigAccess access = new ConfigAccess(this.Transaction);
                objWatermarkModel = access.GetClientWatermarkConfig(subStoreId);
            };
            this.Start(false);

            return objWatermarkModel;
        }

        #endregion

        ///created by latika for Evoucher Service 25 march2020
        public int GETEvoucherID()
        {

            int issucess = 0;
            this.operation = () =>
            {
                ConfigAccess access = new ConfigAccess(this.Transaction);
                issucess = access.GETEvoucherID();
            };
            this.Start(false);

            return issucess;
        }
        public string GETVenue()
        {

            string issucess = "";
            this.operation = () =>
            {
                ConfigAccess access = new ConfigAccess(this.Transaction);
                issucess = access.GETVenuName();
            };
            this.Start(false);

            return issucess;
        }

        /////end by latika

        ///SET Methods
        ///
        #region SET 3 methods


        public int GETSet3EnabledID()
        {
            int issucess = 0;
            this.operation = () =>
            {
                ConfigAccess access = new ConfigAccess(this.Transaction);
                issucess = access.GETSet3EnabledID();
            };
            this.Start(false);
            return issucess;
        }

        public int GETSet3DayWiseID()
        {
            int issucess = 0;
            this.operation = () =>
            {
                ConfigAccess access = new ConfigAccess(this.Transaction);
                issucess = access.GETSet3DayWiseID();
            };
            this.Start(false);
            return issucess;
        }

        public int GETSet3HourWiseID()
        {
            int issucess = 0;
            this.operation = () =>
            {
                ConfigAccess access = new ConfigAccess(this.Transaction);
                issucess = access.GETSet3HourWiseID();
            };
            this.Start(false);
            return issucess;
        }
        #endregion
    }
}
