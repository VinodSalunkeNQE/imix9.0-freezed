﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using DigiPhoto.DataLayer.Model;
using System.Xml.Linq;
using System.Data;
using System.ComponentModel;
using System.Collections;
using System.Reflection;
using System.Globalization;
using DigiPhoto.IMIX.Model;
using DigiPhoto.IMIX.DataAccess;
using DigiPhoto.IMIX.Business;
using DigiPhoto.DataLayer;
using DigiPhoto.Utility.Repository.ValueType;
using DigiPhoto.Cache.MasterDataCache;
using DigiPhoto.Cache.Repository;
using DigiPhoto.Cache.DataCache;

namespace DigiPhoto.IMIX.Business
{

    public partial class StoreSubStoreDataBusniess : BaseBusiness
    {
        public List<StoreInfo> SelectStores()
        {
            ICacheRepository oCache = DataCacheFactory.GetFactory<ICacheRepository>(typeof(StoreCaches).FullName);
            return ((List<StoreInfo>)oCache.GetData());
        }

        public Hashtable GetStoreDetails()
        {
            Hashtable htStore = new Hashtable();
            string tempStoreName = string.Empty;
            string tempCountryName = string.Empty;
            string tempLocationName = string.Empty;
            StoreInfo objList = new StoreInfo();
            this.operation = () =>
                {
                    StoreDao access = new StoreDao(this.Transaction);
                    objList = access.GetStore();
                };
            this.Start(false);
            var storename = objList.DG_Store_Name;
            if (storename != null)
                tempStoreName = storename;

            var countryname = objList.Country.ToString();
            if (countryname != null)
                tempCountryName = countryname;
            int storeId = objList.DG_Store_pkey;
            htStore.Add("Store", tempStoreName);
            htStore.Add("Country", tempCountryName);
            return htStore;
        }

        public string GetSubstoreNameById(int SubstoreId)
        {

            SubStoresInfo _objOrd = new SubStoresInfo();
            this.operation = () =>
            {
                StoreDao access = new StoreDao(this.Transaction);
                _objOrd = access.GetSubstoreData(SubstoreId, null);
            };
            this.Start(false);
            if (_objOrd != null)
                return _objOrd.DG_SubStore_Name;
            else
                return "";

        }

        public List<StoreInfo> GetStoreName()
        {
            List<StoreInfo> objList = new List<StoreInfo>();
            this.operation = () =>
            {
                StoreDao access = new StoreDao(this.Transaction);
                objList = access.SelectStore();
            };
            this.Start(false);
            return objList;
        }

        public string GetQRCodeWebUrl()
        {
            string QRCodeWebUrl = string.Empty;
            this.operation = () =>
            {
                StoreDao access = new StoreDao(this.Transaction);
                QRCodeWebUrl = access.GETQRCodeWebUrl().DG_QRCodeWebUrl;
            }; this.Start(false);
            return QRCodeWebUrl;
        }

        public List<SubStoresInfo> GetSubstoreData()
        {
            List<SubStoresInfo> objList = new List<SubStoresInfo>();
            this.operation = () =>
            {
                SubStoreDao access = new SubStoreDao(this.Transaction);
                objList = access.GetSubstoreData();
            };
            this.Start(false);
            return objList;
        }
        /// <summary>
        /// ////////////// Created by latika for visible in ViewOrderStation 2019 Nov 11
        public SubStoresInfo GetSubstoreVisibleOrdStion(int SubStoreID)
        {
            SubStoresInfo objList = new SubStoresInfo();
            this.operation = () =>
            {
                SubStoreDao access = new SubStoreDao(this.Transaction);
                objList = access.GetSubstoreVisibleOrdStion(SubStoreID);
            };
            this.Start(false);
            return objList;
        }
    /// </summary> ////end
    /// <returns></returns>
    public List<SubStoresInfo> GetSubstoreDataFillGrid()
        {
            List<SubStoresInfo> objList = new List<SubStoresInfo>();
            this.operation = () =>
            {
                SubStoreDao access = new SubStoreDao(this.Transaction);
                objList = access.GetSubstoreDataFillGrid();
            };
            this.Start(false);
            return objList;
        }


        public List<SiteCodeDetail> GetSiteCodeBusiness()
        {
            List<SiteCodeDetail> objList = new List<SiteCodeDetail>();
            this.operation = () =>
            {
                SubStoreDao access = new SubStoreDao(this.Transaction);
                objList = access.GetSiteCodeAccess();
            };
            this.Start(false);
            return objList;
        }

        public List<SubStoresInfo> GetLoginUserDefaultSubstores(int subStoreId)
        {
            var objList = new List<SubStoresInfo>();
            this.operation = () =>
            {
                objList = (new SubStoreDao(this.Transaction)).GetLoginUserDefaultSubstores(subStoreId);
            };
            this.Start(false);
            return objList;
        }

        public Dictionary<string, string> GetSubstoreDataDir(Dictionary<string, string> selectDict)
        {

            var result = selectDict.Concat(GetSubstoreDataDir()).GroupBy(d => d.Key)
              .ToDictionary(d => d.Key, d => d.First().Value);
            return result;
        }
        public Dictionary<string, string> GetSubstoreDataDir()
        {
            Dictionary<string, string> objList = new Dictionary<string, string>();

            this.operation = () =>
            {
                SubStoreDao access = new SubStoreDao(this.Transaction);
                objList = access.GetSubstoreDataDir();
            };
            this.Start(false);
            return objList;
        }


        public List<string> GetBackupsubstorename(int SubStoreId)
        {
            List<string> pname = null;
            try
            {
                List<SubStoresInfo> objList = new List<SubStoresInfo>();
                this.operation = () =>
                {
                    SubStoreDao access = new SubStoreDao(this.Transaction);
                    pname = access.GetBackupSubStoreNameBySubStoreId(SubStoreId);
                };
                this.Start(false);
                if (pname != null)
                    return pname;
                else
                {
                    return null;
                }

            }
            catch (Exception ex)
            {
                return null;
            }
        }
        /// <summary>
        /// Manoj Gupta at 7-Sept 2018
        /// This function is used to delete data from backuphistory based on backupId
        /// </summary>
        /// <param name="backupId"></param>
        public int DeleteBackupHistory(int backupId)
        {
            int isDeleted = 0;
            this.operation = () =>
            {
                SubStoreDao access = new SubStoreDao(this.Transaction);
                isDeleted = access.DeleteBackupHistory(backupId);
            };
            this.Start(false);
            return isDeleted;
        }
        public List<LocationInfo> GetLocationSubstoreWise(int SubstoreId)
        {
            List<LocationInfo> result = null;
            try
            {
                this.operation = () =>
                {
                    LocationDao context = new LocationDao(this.Transaction);
                    result = context.GetLocationSubstoreWise(SubstoreId).ToList();
                };
                this.Start(false);
                return result;
            }
            catch (Exception ex)
            {
                return result;
            }
        }
        public Dictionary<string, string> SelStoreDataDir()
        {
            Dictionary<string, string> objList = new Dictionary<string, string>();
            this.operation = () =>
            {
                StoreDao access = new StoreDao(this.Transaction);
                objList = access.SelStoreDir();
            };
            this.Start(false);
            return objList;

        }


        public bool DeleteSubStoreLocations(int SubstoreID)
        {
            this.operation = () =>
            {
                SubStoreLocationDao access = new SubStoreLocationDao(this.Transaction);
                access.DelSubStoreLocationsBySubStoreId(SubstoreID);
            };
            this.Start(false);
            return true;
        }
        public bool SetSubStoreLocationsDetails(int Substore_ID, int LocationId)
        {
            try
            {
                bool result = false;
                int iSubStoreLocationID;
                SubStoreLocationInfo _objnew = new SubStoreLocationInfo();

                this.operation = () =>
                {
                    SubStoreLocationDao access = new SubStoreLocationDao(this.Transaction);

                    _objnew.DG_Location_ID = LocationId;
                    _objnew.DG_SubStore_ID = Substore_ID;
                    iSubStoreLocationID = access.InsertSubStoreLocation(_objnew);
                    if (iSubStoreLocationID > 0)
                        result = true;
                    else
                        result = false;
                };
                this.Start(false);
                return result;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public List<LocationInfo> GetAvailableLocationsSubstore(int SubstoreId)
        {
            List<LocationInfo> result = new List<LocationInfo>();
            this.operation = () =>
                {
                    SubStoreLocationDao access = new SubStoreLocationDao(this.Transaction);
                    result = access.SelListAvailableLocations(SubstoreId).ToList();

                };
            this.Start(false);

            return result;
        }


        public List<LocationInfo> GetSelectedLocationsSubstore(int SubstoreId)
        {
            List<LocationInfo> result = new List<LocationInfo>();
            this.operation = () =>
                {
                    SubStoreLocationDao access = new SubStoreLocationDao(this.Transaction);
                    result = access.SelectSubStoreLocations(SubstoreId);

                };
            this.Start(false);
            return result;
        }
        public bool UpdateQRCodeWebUrl(string url)
        {
            try
            {
                bool result = false;
                this.operation = () =>
                {
                    StoreDao _objnew = new StoreDao(this.Transaction);
                    result = _objnew.UpdateStoreQRCode(url);
                };
                this.Start(false);
                ICacheRepository objCache = DataCacheFactory.GetFactory<ICacheRepository>(typeof(StoreCaches).FullName);
                objCache.RemoveFromCache();
                return result;
            }
            catch
            {
                return false;
            }
        }
        public SubStoresInfo GetSubstoreData(int subStoreId)
        {
            SubStoresInfo objInfo = new SubStoresInfo();
            this.operation = () =>
            {
                SubStoreDao access = new SubStoreDao(this.Transaction);
                objInfo = access.GetSubstoreData(subStoreId, true);
            }; this.Start(false);
            return objInfo;
        }
        public string DeleteSubstore(int SubStoreId)
        {
            string result = string.Empty;
            this.operation = () =>
            {
                SubStoreDao access = new SubStoreDao(this.Transaction);
                result = access.DelSubstoreByID(SubStoreId);//TBD
            };
            this.Start(false);
            return result;
        }

        public bool SetSubStoreDetails(string SubstoreName, string SubStoreDescription, int Substore_ID, string SyncCode, bool islogiaclsubstore, Int32? logicalsubstoreid, string SiteCode)
        {
            SubStoresInfo objInfo = new SubStoresInfo();
            objInfo.DG_SubStore_pkey = Substore_ID;
            objInfo.DG_SubStore_Name = SubstoreName;
            objInfo.DG_SubStore_Description = SubStoreDescription;
            objInfo.DG_SubStore_IsActive = true;
            objInfo.SyncCode = SyncCode;
            objInfo.IsSynced = false;
            objInfo.IsLogicalSubStore = islogiaclsubstore;
            objInfo.LogicalSubStoreID = logicalsubstoreid;
            objInfo.DG_SubStore_Code = SiteCode;

            if (Substore_ID > 0)
            {
                this.operation = () =>
                {
                    SubStoreDao access = new SubStoreDao(this.Transaction);
                    access.Update(objInfo);
                };
                this.Start(false);

            }
            else
            {
                objInfo.DG_SubStore_pkey = Substore_ID;
                this.operation = () =>
                {
                    SubStoreDao access = new SubStoreDao(this.Transaction);
                    access.InsertSubStore(objInfo);
                };
                this.Start(false);
            }
            return true;
        }

        public List<SubStoresInfo> GetAllSubstoreName()
        {
            List<SubStoresInfo> objList = new List<SubStoresInfo>();
            this.operation = () =>
            {
                SubStoreDao access = new SubStoreDao(this.Transaction);
                objList = access.Getvw_GetSubStoreData().ToList();

            };
            this.Start(false);
            if (objList != null)
                return objList;
            else
                return null;
        }
        public List<SubStoresInfo> GetAllLogicalSubstoreName()
        {
            List<SubStoresInfo> objList = new List<SubStoresInfo>();
            this.operation = () =>
            {
                SubStoreDao access = new SubStoreDao(this.Transaction);
                objList = access.Getvw_GetLogicalSubStoreData().ToList();

            };
            this.Start(false);
            if (objList != null)
                return objList;
            else
                return null;
        }

        public string GetCountryName()
        {
            string QRCodeWebUrl = string.Empty;
            this.operation = () =>
            {
                StoreDao access = new StoreDao(this.Transaction);
                QRCodeWebUrl = access.GetStore().Country;
            }; this.Start(false);
            return QRCodeWebUrl;
        }
        public StoreInfo GetStore()
        {
            StoreInfo store = new StoreInfo();
            this.operation = () =>
            {
                StoreDao access = new StoreDao(this.Transaction);
                store = access.GetStore();
            };
            this.Start(false);
            return store;
        }
        #region Get online status- Ashirwad
        public bool GetSyncIsOnline(int subStoreID)
        {
            bool value = false;
            this.operation = () =>
            {
                StoreDao access = new StoreDao(this.Transaction);
                value = access.GetSyncIsOnline(subStoreID);
            };
            this.Start(false);

            return value;
        }
        #endregion
        public List<SubStoresInfo> GetLogicalSubStore()
        {
            List<SubStoresInfo> objList = new List<SubStoresInfo>();
            this.operation = () =>
            {
                SubStoreDao access = new SubStoreDao(this.Transaction);
                objList = access.GetLogicalSubStore().ToList();
            };
            this.Start(false);
            return objList;
        }
        public List<RfidInfo> GetRfidInfoBussines(int SubStoreId)
        {
            List<RfidInfo> lstRfidInfo = null;
            try
            {
                this.operation = () =>
                    {
                        SubStoreDao access = new SubStoreDao(this.Transaction);
                        lstRfidInfo = access.GetRfidAccess(SubStoreId).ToList();
                    };
                this.Start(false);
                return lstRfidInfo;
            }
            catch
            {
                return lstRfidInfo;
            }
        }

        public int ClearPrinterQueue(int productID,int substoreID)
        {
            int result = 0;
            this.operation = () =>
            {
                SubStoreDao access = new SubStoreDao(this.Transaction);
                result = access.ClearPrinterQueue(productID, substoreID);
            };
            this.Start(false);
            return result;
        }
        ////--------------------Changed by latika for Open close form setting----------------------------
        public List<OpeningCloseSettinginfo> GetOpeningClosingDeletedData(int SubStoreId)
        {
            List<OpeningCloseSettinginfo> lstOpeningCloseSettingInfo = null;
            try
            {
                this.operation = () =>
                {
                    SubStoreDao access = new SubStoreDao(this.Transaction);
                    lstOpeningCloseSettingInfo = access.GetOpeningClosingDeletedData(SubStoreId).ToList();
                };
                this.Start(false);
                return lstOpeningCloseSettingInfo;
            }
            catch
            {
                return lstOpeningCloseSettingInfo;
            }
        }

        public string OpeningClosingDelete(OpeningClosingDeleteinfo objOpClInfo)
        {
            string result = string.Empty;
            this.operation = () =>
            {
                SubStoreDao access = new SubStoreDao(this.Transaction);
                result = access.OpeningClosingDelete(objOpClInfo);
            };
            this.Start(false);
            return result;
        }
        ////--------------------Changed by latika for Open close form setting----------------------------
        //////////////created  by latika for search product

      
        public List<SearchbyInfo> GetSearchby()
        {
            List<SearchbyInfo> objList = new List<SearchbyInfo>();
            this.operation = () =>
            {
                StoreDao access = new StoreDao(this.Transaction);
                objList = access.SelectSearchby().ToList();
            };
            this.Start(false);
            return objList;
        }

        public bool SaveStoreAzureDetails(string StorageAccountName, string LifeCyclePolicy, string SubscribedToEventGrid, string DB_Variable_connectionstring)
        {

            bool iReturnVal = false;
            this.operation = () =>
            {
                StoreDao access = new StoreDao(this.Transaction);
                iReturnVal = access.SaveStoreAzureDetails(StorageAccountName, LifeCyclePolicy, SubscribedToEventGrid, DB_Variable_connectionstring);
            };
            this.Start(false);
            return iReturnVal;
        }

        ///////////////////////end
        
            // Strat changes by Suraj
        /// <summary>
        ///   Store MetaDeta Upload Failure Details
        /// </summary>
        /// <param name="PhotoId"></param>
        /// <param name="PhotoJson"></param>
        /// <param name="WebPhotoJson"></param>
        /// <param name="CloudinaryAPIJson"></param>
        /// <param name="ImagesAssociationAPIJson"></param>
        /// <returns></returns>
        public bool SaveMetaDetaUploadFailureDetails(String PhotoId, string PhotoJson, string WebPhotoJson, string CloudinaryAPIJson, string ImagesAssociationAPIJson,string Venue,String Site)
        {

            bool iReturnVal = false;
            this.operation = () =>
            {
                StoreDao access = new StoreDao(this.Transaction);
                iReturnVal = access.SaveMetaDetaUploadFailureDetails(PhotoId, PhotoJson, WebPhotoJson, CloudinaryAPIJson, ImagesAssociationAPIJson,Venue,Site);
            };
            this.Start(false);
            return iReturnVal;
        }


        public bool UpdateMetaDetaDGPhotos(int PhotoId, bool IsUploadedBob)
        {

            bool iReturnVal = false;
            this.operation = () =>
            {
                StoreDao access = new StoreDao(this.Transaction);
                iReturnVal = access.UpdateMetaDetaDGPhotos(PhotoId, IsUploadedBob);
            };
            this.Start(false);
            return iReturnVal;
        }

        public bool UpdateMetaDetaDGEditPhotos(int PhotoId, bool Issynced)
        {

            bool iReturnVal = false;
            this.operation = () =>
            {
                StoreDao access = new StoreDao(this.Transaction);
                iReturnVal = access.UpdateMetaDetaDGEditPhotos(PhotoId, Issynced);
            };
            this.Start(false);
            return iReturnVal;
        }

        public bool UpdateMetaDetaiMixImagesAssociation(int PhotoId, bool IsUploadedBob)
        {

            bool iReturnVal = false;
            this.operation = () =>
            {
                StoreDao access = new StoreDao(this.Transaction);
                iReturnVal = access.UpdateMetaDetaiMixImagesAssociation(PhotoId, IsUploadedBob);
            };
            this.Start(false);
            return iReturnVal;
        }
        ///////////////////////// End

        ////////////////////////////////changed by latika for Visible in substore 15 Nov 2019
        public bool SaveSubstoreConfigVisbleInOrdStation(string SubStoreVal, int SubstoreID)
        {

            bool iReturnVal =false;
            this.operation = () =>
            {
                StoreDao access = new StoreDao(this.Transaction);
                iReturnVal = access.SaveSubstoreConfigVisbleInOrdStation(SubStoreVal, SubstoreID);
            };
            this.Start(false);
            return iReturnVal;

        }
        //////


        public List<string> GetStoreAzureConfiguration(string StorageName)
        {
            List<string> pname = null;
            try
            {
                List<SubStoresInfo> objList = new List<SubStoresInfo>();
                this.operation = () =>
                {
                    SubStoreDao access = new SubStoreDao(this.Transaction);
                    pname = access.GetStoreAzureConfiguration(StorageName);
                };
                this.Start(false);
                if (pname != null)
                    return pname;
                else
                {
                    return null;
                }

            }
            catch (Exception ex)
            {
                return null;
            }
        }


    }
}
