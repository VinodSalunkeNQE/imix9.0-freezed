﻿using DigiPhoto.IMIX.DataAccess;
using DigiPhoto.IMIX.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DigiPhoto.IMIX.Business
{
    public class DataMigrationBusiness : BaseBusiness
    {
        public string ExecuteDataMigration(string _databasename, string backUpPath)
        {
            string result = string.Empty;
            try
            {
                this.operation = () =>
                {
                    DataMigrationAccess access = new DataMigrationAccess(this.Transaction);
                    result = access.ExecuteDataMigration(_databasename, backUpPath);
                };
                this.Start(false);
                return result;
            }
            catch
            {
                throw;
            }
        }
      
    }
}
