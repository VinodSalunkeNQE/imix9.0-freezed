﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using DigiPhoto.DataLayer.Model;
using System.Xml.Linq;
using System.Data;
using System.ComponentModel;
using System.Collections;
using System.Reflection;
using System.Globalization;
using DigiPhoto.IMIX.Model;
using DigiPhoto.IMIX.DataAccess;
using DigiPhoto.IMIX.Business;
using DigiPhoto.DataLayer;

namespace DigiPhoto.IMIX.Business
{
    public partial class EmailBusniess : BaseBusiness
    {
        public bool TruncateEmailSettingsTable()
        {
            bool result = false;
            try
            {
                this.operation = () =>
                {
                    EmailDao access = new EmailDao(this.Transaction);
                    access.TruEmailSettingsTable();

                }; this.Start(false);
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }


        //public DigiPhoto.IMIX.Model.DG_EmailSettings GetEmailSettingsDetails()
        //{
        //    DigiPhoto.IMIX.Model.DG_EmailSettings item = new DigiPhoto.IMIX.Model.DG_EmailSettings();
        //    EmailDao access = new EmailDao();
        //    try
        //    {
        //        this.operation = () =>
        //        {
        //            item = access.GetEmailSettingsDetail();

        //        }; this.Start(false);
        //        return item;
        //    }
        //    catch (Exception ex)
        //    {
        //        return null;
        //    }
        //}



        public bool SetEmailSettings(string From, string subject, string body, string servername, string serverport, bool defaultcredential, string username, string password, bool enablessl, string mailbcc, int SsId)
        {
            try
            {
               
                this.operation = () =>
                {
                    EmailDao access = new EmailDao(this.Transaction);
                     access.InsEmailSettings(From, subject, body, servername, serverport, defaultcredential, username, password, enablessl, mailbcc, SsId);

                 };
                this.Start(false);
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }


        public EmailSettingsInfo GetEmailSettingsDetails()
        {
            try
            {
                EmailSettingsInfo objInfo = new EmailSettingsInfo();
                this.operation = () =>
                {
                    EmailDao access = new EmailDao(this.Transaction);
                    objInfo = access.GetEmailSettingsDetail(); //TBD 
                };
                this.Start(false);
                return objInfo;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public List<EmailStatusInfo> GetEmailStatus(EmailStatusInfo rfidEmailObj)
        {
            try
            {
                List<EmailStatusInfo> objInfo = new List<EmailStatusInfo>();
                this.operation = () =>
                {
                    EmailDao access = new EmailDao(this.Transaction);
                    objInfo = access.GetEmailStatus(rfidEmailObj); //TBD 
                };
                this.Start(false);
                return objInfo;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public bool ResendEmail(string EmailId, int SendType)
        {
            bool result = false;
            try
            {
                this.operation = () =>
                {
                    EmailDao access = new EmailDao(this.Transaction);
                    access.ResendEmail(EmailId, SendType);

                }; this.Start(false);
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public bool InsertEmailDetails(EMailInfo emailInfo)
        {
            try
            {
                this.operation = () =>
                {
                    EmailDao access = new EmailDao(this.Transaction);
                    access.InsertEmailDetails(emailInfo);

                }; this.Start(false);
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        public List<EmailDetailInfo> GetEmailDetails()
        {
            List<EmailDetailInfo> lstEmailDetails = new List<EmailDetailInfo>();
            try
            {
                this.operation = () =>
                {
                    EmailDao access = new EmailDao(this.Transaction);
                    lstEmailDetails = access.GetEmailDetails();
                }; this.Start(false);
                return lstEmailDetails;
            }
            catch (Exception ex)
            {
                return lstEmailDetails;
            }
        }
    }
}
