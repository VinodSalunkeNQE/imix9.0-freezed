﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DigiPhoto.IMIX.Model;
using System.ServiceProcess;
using DigiPhoto.IMIX.DataAccess;
using DigiPhoto.DataLayer;
using  System.Management;
using System.Configuration;
using System.Diagnostics;
using log4net;

namespace DigiPhoto.IMIX.Business
{
    public class ServicePosInfoBusiness : BaseBusiness
    {
        
        public List<ServicePosInfo> GetRunningServices()
        {
            try
            {
                List<ServicePosInfo> objectList = new List<ServicePosInfo>();
                this.operation = () =>
                {
                    ServicePosInfoAccess access = new ServicePosInfoAccess(this.Transaction);
                    objectList = access.GetServices();
                };
                this.Start(false);
                return objectList;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public List<SvcRunningInfo> GetServiceInfoBusiness(long ServiceID, long ImixPOSDetailID, long SubStoreID)
        {
            try
            {
                List<SvcRunningInfo> serviceRuningHistory = new List<SvcRunningInfo>();
                this.operation = () =>
                    {
                        ServicePosInfoAccess svcPOSInfo = new ServicePosInfoAccess(this.Transaction);
                        serviceRuningHistory = svcPOSInfo.GetServiceInfoAccess(ServiceID, ImixPOSDetailID, SubStoreID);
                    };
                this.Start(false);
                return serviceRuningHistory;
            }
            catch(Exception ex)
            {
                return null;
            }

        }

        public List<ImixPOSDetail> GetPOSDetailBusiness(long SubStoreID, int RunLevel)
        {
            try
            {
                List<ImixPOSDetail> imixPOSDetail = new List<ImixPOSDetail>();
                this.operation = () =>
                    {
                        ServicePosInfoAccess svcPOSInfo = new ServicePosInfoAccess(this.Transaction);
                        imixPOSDetail = svcPOSInfo.GetPOSDetailAccess(SubStoreID, RunLevel);
                    };
                this.Start(false);
                return imixPOSDetail;
            }
            catch(Exception ex)
            {
                return null;

            }
        }
		////changed by latika for location wise settings video process,image process and manual downloading 
        public List<ImixPOSDetail> GetPOSDetailByLocation(long SubStoreID, int LocationID)
        {
            try
            {
                List<ImixPOSDetail> imixPOSDetail = new List<ImixPOSDetail>();
                this.operation = () =>
                {
                    ServicePosInfoAccess svcPOSInfo = new ServicePosInfoAccess(this.Transaction);
                    imixPOSDetail = svcPOSInfo.GetPOSDetailByLocation(SubStoreID, LocationID);
                };
                this.Start(false);
                return imixPOSDetail;
            }
            catch (Exception ex)
            {
                return null;

            }
        }
		////end
        public int StartStopServiceByPosIdBusiness(long ServiceId, long SubstoreId, long ImixPosDetailId, bool Status, string CreatedBy)
        {
            try
                {
            int Count = 0;
            this.operation = () =>
            {
                ServicePosInfoAccess svcPosInfoAccess = new ServicePosInfoAccess();
                Count = svcPosInfoAccess.StartStopServiceByPosIdAccess(ServiceId, SubstoreId, ImixPosDetailId, Status, CreatedBy);
            };
            this.Start(false);
            return Count;
                }
            catch (Exception ex)
            {
                return 0;
            }
        }

        public int InsertImixPosBusiness(ImixPOSDetail imixPosDetail)
        {
            try
            {
                int Count = 0;
                this.operation = () =>
                {
                    ServicePosInfoAccess svcPosInfoAccess = new ServicePosInfoAccess(this.Transaction);
                    Count = svcPosInfoAccess.InsertImixPosAccess(imixPosDetail);
                };
                this.Start(false);
                return Count;
            }
            catch (Exception ex)
            {
                return 0;                
            }
        }

        public string CheckRunnignServiceBusiness(long ServiceId, long SubStoreId, int Level, string SystemName)
        {
             try
            {
            string checkService = string.Empty;
            this.operation = () =>
            {
                ServicePosInfoAccess svcPosInfoAccess = new ServicePosInfoAccess(this.Transaction);
                checkService = svcPosInfoAccess.CheckRunnignServiceAccess(ServiceId, SubStoreId, Level, SystemName);
            };
            this.Start(false);
            return checkService;
            }
             catch (Exception ex)
             {
                 return null;
             }
           

        }

        public List<GetServiceStatus> GetServiceStatusBusiness(string MachineName, string ServiceName)
        {
            try
            {
                List<GetServiceStatus> getService = new List<GetServiceStatus>(); ;
                this.operation = () =>
                {
                    ServicePosInfoAccess svcPosInfoAccess = new ServicePosInfoAccess(this.Transaction);
                    getService = svcPosInfoAccess.GetServiceStatusAccess(MachineName, ServiceName);
                };
                this.Start(false);
                return getService;
            }
            catch (Exception ex)
            {
                return null;
            }


        }


        public List<SubStore> GetSubstoreDetailsBusiness()
        {
            try
            {
                List<SubStore> substore = new List<SubStore>();
                this.operation = () =>
                {
                    ServicePosInfoAccess svcPosInfoAccess = new ServicePosInfoAccess(this.Transaction);
                    substore = svcPosInfoAccess.GetSubstoreDetailsAccess();
                };
                this.Start(false);
                return substore;
            }
            catch (Exception ex)
            {
                return null;
            }
           
       
        }

        public List<ImixPOSDetail> GetPosDetailsBusiness()
        {
            try
            {
                List<ImixPOSDetail> imixPosDetail = new List<ImixPOSDetail>();
                this.operation = () =>
                {
                    ServicePosInfoAccess svcPosInfoAccess = new ServicePosInfoAccess(this.Transaction);
                    imixPosDetail = svcPosInfoAccess.GetPosDetailsAccess();
                };
                this.Start(false);
                return imixPosDetail;
            }
            catch (Exception ex)
            {
                return null;
            }
           

        }

        public List<MappedPos> MappedPosDetailBusiness()
        {
            try
            {
                List<MappedPos> mappedPosDetail = new List<MappedPos>();
                this.operation = () =>
                {
                    ServicePosInfoAccess svcPosInfoAccess = new ServicePosInfoAccess(this.Transaction);
                    mappedPosDetail = svcPosInfoAccess.MappedPosDetailAccess();
                };
                this.Start(false);
                return mappedPosDetail;
            }
            catch (Exception ex)
            {
                return null;
            }
           
        }
        
        public int GetAnyRunningServiceByPosIdBusiness(long imixId)
        {
            try
            {
                int Count = 0;
                this.operation = () =>
                {
                    ServicePosInfoAccess svcPosInfoAccess = new ServicePosInfoAccess(this.Transaction);
                    Count = svcPosInfoAccess.GetAnyRunningServiceByPosIdAccess(imixId);
                };
                this.Start(false);
                return Count;
            }
            catch (Exception ex)
            {
                return 0;
            }

        }

       public string ServiceStart(bool IsExe)
        {
            try
            {
               
                ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
                string SystemName = getSystemName();
                string servicename = getServiceName(IsExe);
                //log.Error("servicename" + servicename + "SystemName" + SystemName);
                ErrorHandler.ErrorHandler.LogFileWrite("Service Start Called 270:");
                bool _Status = true;
                int Count = 0;
                string CreatedBy = "itadmin";
                List<GetServiceStatus> getServiceStatus = new List<GetServiceStatus>();
                ServicePosInfoBusiness servicePosBusiness = new ServicePosInfoBusiness();
                ErrorHandler.ErrorHandler.LogFileWrite("Service Start Called 276:");
                ServiceController serviceController = new ServiceController(servicename, SystemName);
                getServiceStatus = servicePosBusiness.GetServiceStatusBusiness(SystemName, servicename);
                ErrorHandler.ErrorHandler.LogFileWrite("Service Start Called 279:");
                Int32 ServiceId = 0;
                long SubstoreId = 0;
                Int32 RunLevel = 0;
                long newImixPosId = 0;
                ErrorHandler.ErrorHandler.LogFileWrite("Service Start Called 284:");
                if (getServiceStatus != null)
                {
                     ServiceId = getServiceStatus.Select(p => p.ServiceId).FirstOrDefault();
                     SubstoreId = getServiceStatus.Select(p => p.SubStoreID).FirstOrDefault();
                     RunLevel = getServiceStatus.Select(p => p.Runlevel).FirstOrDefault();
                     newImixPosId = getServiceStatus.Select(p => p.iMixPosId).FirstOrDefault();
                }
                ErrorHandler.ErrorHandler.LogFileWrite("Service Start Called 292:");
                string Value = servicePosBusiness.CheckRunnignServiceBusiness(ServiceId, SubstoreId, RunLevel, SystemName);

                ErrorHandler.ErrorHandler.LogFileWrite("Service Start Called 295:");
                if (Value != "")
                {
                    return Value;
                }

                else
                {                   
                   Count = servicePosBusiness.StartStopServiceByPosIdBusiness(ServiceId, SubstoreId, newImixPosId, _Status, CreatedBy);
                   return Value;
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogFileWrite("Service Start Called Excepation 311:"+ ex.Message);
                throw;
            }
        }


       public void setPrintServer(string mac, string SystemName, bool isStop)
       {
           try
           {

             
               //string SystemName = getSystemName();


               try
               {

                   this.operation = () =>
                   {
                       ServicePosInfoAccess svcPosInfoAccess = new ServicePosInfoAccess(this.Transaction);
                       svcPosInfoAccess.setPrintServer(mac, SystemName,isStop);
                   };
                   this.Start(false);

               }
               catch (Exception ex)
               {
                   throw ex;
               }
              


           }
           catch (Exception)
           {

               throw;
           }
       }


        public string getServiceName(bool IsEXE)
        {
            string exename = Process.GetCurrentProcess().ProcessName;

            if (IsEXE)
                return exename;

            //if (!(exename.Contains(".vshost")))
            //{
                ServiceController[] scServices;
                scServices = ServiceController.GetServices();

                // Display the list of services currently running on this computer.
                int my_pid = System.Diagnostics.Process.GetCurrentProcess().Id;

                foreach (ServiceController scTemp in scServices)
                {
                    // Write the service name and the display name
                    // for each running service.

                    // Query WMI for additional information about this service.
                    // Display the start name (LocalSytem, etc) and the service
                    // description.
                    ManagementObject wmiService;
                    wmiService = new ManagementObject("Win32_Service.Name='" + scTemp.ServiceName + "'");
                    wmiService.Get();

                    int id = Convert.ToInt32(wmiService["ProcessId"]);
                    if (id == my_pid)
                    {
                        return scTemp.ServiceName;

                    }
                }
            //}
            //else
            //{ return exename.Split('.')[0]; }
            return "NotFound";
        }
        public string getSystemName()
        {
            object getSystemName = "";


            ManagementObjectSearcher searcher =
                    new ManagementObjectSearcher("root\\CIMV2",
                    "SELECT Name FROM Win32_ComputerSystem");
            foreach (ManagementObject queryObj in searcher.Get())
            {
                return queryObj["Name"].ToString();
            }
            return "";
        }
        public void StopService(bool IsExe)
        {
            string retServiceName=string.Empty;

            string SystemName = getSystemName();
            string servicename = getServiceName(IsExe);
            bool _Status = false;
            int Count = 0;
            string CreatedBy = "itadmin";
            List<GetServiceStatus> getServiceStatus = new List<GetServiceStatus>();
            ServicePosInfoBusiness servicePosBusiness = new ServicePosInfoBusiness();
            ServiceController serviceController = new ServiceController(servicename, SystemName);
            getServiceStatus = servicePosBusiness.GetServiceStatusBusiness(SystemName, servicename);
            Int32 ServiceId = 0;
            long SubstoreId = 0;
            Int32 RunLevel = 0;
            long newImixPosId = 0;

            if (getServiceStatus != null)
            {
                ServiceId = getServiceStatus.Select(p => p.ServiceId).FirstOrDefault();
                SubstoreId = getServiceStatus.Select(p => p.SubStoreID).FirstOrDefault();
                RunLevel = getServiceStatus.Select(p => p.Runlevel).FirstOrDefault();
                newImixPosId = getServiceStatus.Select(p => p.iMixPosId).FirstOrDefault();
            }
            Count = servicePosBusiness.StartStopServiceByPosIdBusiness(ServiceId, SubstoreId, newImixPosId, _Status, CreatedBy);
            if (Count != 0)
            {
                return;
            }
        }

        public int UpdateServiceRunLevelBusiness(int _serviceId,int RunLevel, bool isService)
        {
            try
            {
                int Count = 0;
                this.operation = () =>
                {
                    ServicePosInfoAccess svcPosInfoAccess = new ServicePosInfoAccess(this.Transaction);
                    Count = svcPosInfoAccess.UpdateServiceRunLevelAccess(_serviceId,RunLevel,isService);
                };
                this.Start(false);
                return Count;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }
        public void StartStopSystemBusiness(string mac, string myIP, string SystemName,int Type)
        {
            try
            {
               
                this.operation = () =>
                {
                    ServicePosInfoAccess svcPosInfoAccess = new ServicePosInfoAccess(this.Transaction);
                    svcPosInfoAccess.StartStopSystemlAccess(mac, myIP, SystemName,Type);
                };
                this.Start(false);
              
            }
            catch (Exception ex)
            {
                throw ex;
            }
        
        }
        public int DeletePosDetailBasedonID(Int64 iMixPOSDetailID)
        {
            try
            {
                int Count = 0;
                this.operation = () =>
                {
                    ServicePosInfoAccess svcPosInfoAccess = new ServicePosInfoAccess(this.Transaction);
                    Count = svcPosInfoAccess.DeletePosDetailBasedonID(iMixPOSDetailID);
                };
                this.Start(false);
                return Count;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        public int ValidatePosDetails(ImixPOSDetail imixposdetail)
        {
            try
            {
                int Count = 0;
                this.operation = () =>
                {
                    ServicePosInfoAccess svcPosInfoAccess = new ServicePosInfoAccess(this.Transaction);
                    Count = svcPosInfoAccess.ValidatePosDetails(imixposdetail);
                };
                this.Start(false);
                return Count;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }
        


    }
}