﻿using DigiPhoto.IMIX.DataAccess;
using DigiPhoto.IMIX.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DigiPhoto.IMIX.Business
{
    public class LicenseProductBusiness : BaseBusiness
    {
        public static readonly double PrintImageWidthInInches = 8.6;
        public static readonly double PrintImageHeightInInches = 5.4;

        public static readonly double PrintImageWidthInDIP = PrintImageWidthInInches*96;
        public static readonly double PrintImageHeightInDIP = PrintImageHeightInInches*96;

        public static readonly double PrintImageWidthInDPI300 = PrintImageWidthInInches * 300;
        public static readonly double PrintImageHeightInDPI300 = PrintImageHeightInInches * 300;


        public string GetLicenseNumber(int photoId) {

            string licenseNumber = string.Empty;

            this.operation = () =>
            {
                LicenseProductAccess access = new LicenseProductAccess(this.Transaction);
                licenseNumber = access.GetLicenseNumber(photoId);                
            };
            this.Start(false);

            return (licenseNumber);
        }

        public static string GetLiceseNumberFromPrefixSufix(int prefix, int sufix) {
            return(LicenseProductAccess.GetLicenseNumberFromPrefixSufix(prefix, sufix));
        }
        public LicenseUserDetails GetLicenseDetails(int photoId)
        {
            LicenseUserDetails lud = null;
            this.operation = () =>
            {
                LicenseProductAccess access = new LicenseProductAccess(this.Transaction);
                lud = access.GetLicenseDetails(photoId);
            };
            this.Start(false);

            return (lud);
        }

        public bool IsLicenseProduct(int borderId) {
            bool isLicenseProduct = false;
            this.operation = () =>
            {
                LicenseProductAccess access = new LicenseProductAccess(this.Transaction);
                isLicenseProduct = access.IsLicenseProduct(borderId);
            };
            this.Start(false);

            return (isLicenseProduct);
        }

        public string GetProductTypeId(int borderId)
        {
            string productTypeId = string.Empty;
            this.operation = () =>
            {
                LicenseProductAccess access = new LicenseProductAccess(this.Transaction);
                productTypeId = access.GetProductTypeIdByBorderId(borderId);
            };
            this.Start(false);
            return (productTypeId);
        }

        public void UpdateLicenseDetails(int photoId, string name, int age, int licensePrefix, int licenseSufix, DateTime issueDate, DateTime expireDate)
        {
            string licenseNumber = string.Empty;
            this.operation = () =>
            {
                LicenseProductAccess access = new LicenseProductAccess(this.Transaction);
                access.UpdateLicenseDetails(photoId, name, age, licensePrefix, licenseSufix, issueDate, expireDate);
            };
            this.Start(false);

        }

        //public void LicenseDetailsToggleOnCanvas(bool isRemoved,int photoId)
        /// <summary>
        /// Are the controls removed from the the canvas
        /// </summary>
        /// <param name="isRemoved"></param>
        /// <param name="photoId"></param>
        public void LicenseDetailsToggleOnCanvas(bool isRemoved, int? photoId = null)
        {
            this.operation = () =>
            {
                LicenseProductAccess access = new LicenseProductAccess(this.Transaction);
                //access.LicenseDetailsToggleOnCanvas(photoId, isRemoved);
                access.LicenseDetailsToggleOnCanvas(isRemoved, photoId);
            };
            this.Start(false);
        }
        
        public bool IsLicenseDetailsRemoved(int photoId) {
            bool isLicenseDetailsRemoved = false;
            this.operation = () =>
            {
                LicenseProductAccess access = new LicenseProductAccess(this.Transaction);
                isLicenseDetailsRemoved = access.IsLicenseDetailsRemoved(photoId);
            };
            this.Start(false);
            return (isLicenseDetailsRemoved);
        }

    }
}
