﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using System.Xml.Linq;
using System.Data;
using System.ComponentModel;
using System.Collections;
using System.Reflection;
using System.Globalization;
using DigiPhoto.IMIX.Model;
using DigiPhoto.IMIX.DataAccess;
using DigiPhoto.IMIX.Business;
using DigiPhoto.DataLayer;
using DigiPhoto.DataLayer.Model;

namespace DigiPhoto.IMIX.Business
{

    public partial class CalenderBusiness : BaseBusiness
    {
        public List<ItemTemplateDetailModel> GetItemTemplateDetail()
        {
            try
            {
                List<ItemTemplateDetailModel> _objtemplate = new List<ItemTemplateDetailModel>();
                this.operation = () =>
                   {
                       CalenderDao access = new CalenderDao(this.Transaction);
                       _objtemplate = access.GetItemTemplateDetail();
                   }; this.Start(false);
                return _objtemplate;
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogFileWrite(ErrorHandler.ErrorHandler.CreateErrorMessage(ex));
                return null;
            }
        }

        public List<ItemTemplateMasterModel> GetItemTemplateMaster()
        {
            try
            {
                List<ItemTemplateMasterModel> _objtemplate = new List<ItemTemplateMasterModel>();
                this.operation = () =>
                   {
                       CalenderDao access = new CalenderDao(this.Transaction);
                       _objtemplate = access.GetItemTemplateMaster();
                   }; this.Start(false);
                return _objtemplate;
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogFileWrite(ErrorHandler.ErrorHandler.CreateErrorMessage(ex));
                return null;
            }
        }


        public bool AddItemTemplatePrintOrder(ItemTemplatePrintOrderModel obj)
        {
            try
            {
                bool result = false;
                this.operation = () =>
                   {
                       CalenderDao access = new CalenderDao(this.Transaction);
                       result = access.AddItemTemplatePrintOrder(obj);
                   }; this.Start(false);
                return result;
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogFileWrite(ErrorHandler.ErrorHandler.CreateErrorMessage(ex));
                return false;
            }
        }

        public List<ItemTemplateDetailModel> GetItemTemplatePath(int Id)
        {
            try
            {
                List<ItemTemplateDetailModel> _objtemplate = new List<ItemTemplateDetailModel>();
                this.operation = () =>
                   {
                       CalenderDao access = new CalenderDao(this.Transaction);
                       _objtemplate = access.GetTemplatePath(Id);
                   }; this.Start(false);
                return _objtemplate;
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogFileWrite(ErrorHandler.ErrorHandler.CreateErrorMessage(ex));
                return null;
            }
        }
    }
}