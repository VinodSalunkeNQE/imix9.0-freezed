﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using DigiPhoto.DataLayer.Model;
using System.Xml.Linq;
using System.Data;
using System.ComponentModel;
using System.Collections;
using System.Reflection;
using System.Globalization;
using DigiPhoto.IMIX.Model;
using DigiPhoto.IMIX.DataAccess;
using DigiPhoto.IMIX.Business;


namespace DigiPhoto.IMIX.Business
{

    public class CardBusiness : BaseBusiness
    {
        public bool IsValidCodeType(string QRCode, int CardTypeId)
        {
            //List<int> objCardTypeIds = new List<int>();
            //if (CardTypeId == 405)
            //{
            //    objCardTypeIds.Add(401);
            //    objCardTypeIds.Add(402);
            //    objCardTypeIds.Add(405);
            //}
            //else
            //{
            //    objCardTypeIds.Add(CardTypeId);
            //}
            //Commented code moved in Proc
            ImageCardTypeInfo objectInfo = new ImageCardTypeInfo();
            this.operation = () =>
            {
                CardTypeDao access = new CardTypeDao(this.Transaction);
                objectInfo = access.GetValidCodeType(CardTypeId, QRCode);
            };
            this.Start(false);
            if (objectInfo != null)
                return true;
            else
                return false;
        }
        public string GetCardCode(int ImageIdentificationType)
        {
            string CardCode = string.Empty;
            ImageCardTypeInfo objectInfo = new ImageCardTypeInfo();
            this.operation = () =>
            {
                CardTypeDao access = new CardTypeDao(this.Transaction);
                objectInfo = access.GetImageIdentificationType(ImageIdentificationType);
            };
            this.Start(false);
            if (objectInfo != null)
                CardCode = objectInfo.CardIdentificationDigit;
            return CardCode;
        }
        public bool IsValidPrepaidCodeType(string QRCode, int CardTypeId)
        {
            ImageCardTypeInfo objectInfo = new ImageCardTypeInfo();
            this.operation = () =>
            {
                CardTypeDao access = new CardTypeDao(this.Transaction);
                objectInfo = access.IsValidPrepaidCodeType(CardTypeId, QRCode);
            };
            this.Start(false);
            if (objectInfo != null)
                return true;
            else
                return false;
        }
        public int GetCardImageLimit(string code)
        {
            if (code.Length < 4)
                return 0;
            ImageCardTypeInfo objectInfo = new ImageCardTypeInfo();
            this.operation = () =>
            {
                CardTypeDao access = new CardTypeDao(this.Transaction);
                objectInfo = access.GetCardImageLimitById(code);
            };
            this.Start(false);
            if (objectInfo != null && objectInfo.MaxImages != null)
                return (int)objectInfo.MaxImages;
            else
                return 0;
        }

        public int GetCardImageSold(string code)
        {
            int itemCount = 0;
            List<string> lstImages = new List<string>();
            this.operation = () =>
            {
                OrderDetailsDao access = new OrderDetailsDao(this.Transaction);
                lstImages = access.GetCardImageSoldById(code);
            };
            this.Start(false);

            lstImages.ForEach(c => itemCount += c.Split(',').Length);
            return itemCount;

        }

        public List<iMixImageCardTypeInfo> GetCardTypeList()
        {

            List<iMixImageCardTypeInfo> iMixImageCardTypeList = new List<iMixImageCardTypeInfo>();
            this.operation = () =>
               {
                   CardTypeDao access = new CardTypeDao(this.Transaction);
                   iMixImageCardTypeList = access.SelectCardTypeList();
               };
            this.Start(false);
            return iMixImageCardTypeList;
        }

        public Dictionary<string, int> GetCardCodeTypes()
        {
            Dictionary<string, int> objDic = new Dictionary<string, int>();
            objDic.Add("QR/Bar Code", 405);
            objDic.Add("Unique", 403);
            objDic.Add("Lost", 404);
            objDic.Add("RFID", 406);
            objDic.Add("Email ID", 407);
            return objDic;
        }


        public Dictionary<string, int> getGlobalCountryList()
        {
            Dictionary<string, int>  CountryList = null;
            this.operation = () =>
            {
                CardTypeDao access = new CardTypeDao(this.Transaction);
                CountryList = access.getGlobalCountryList();
            };
            this.Start(false);

            return CountryList;
        }

        public List<ImageCardTypeInfo> GetCardTypeListview()
        {
            List<ImageCardTypeInfo> ImageCardTypeInfo = new List<ImageCardTypeInfo>();
            this.operation = () =>
            {
                CardTypeDao access = new CardTypeDao(this.Transaction);
                ImageCardTypeInfo = access.GetCardTypeList();
            };
            this.Start(false);
            return ImageCardTypeInfo;
        }
        public iMixImageCardTypeInfo GetCardTypeList(int ID)
        {
            iMixImageCardTypeInfo imgBiz = new iMixImageCardTypeInfo();
            this.operation = () =>
            {
                CardTypeDao access = new CardTypeDao(this.Transaction);
                imgBiz = access.GetCardTypeListDetails(ID);
            };
            this.Start(false);
            return imgBiz;

        }
        public bool ChangeCardStatus(int cardId)
        {
            Boolean _flag = true;
            this.operation = () =>
            {
                CardTypeDao access = new CardTypeDao(this.Transaction);
                _flag = access.UPD_iMixImageCardType(cardId);
            }; this.Start(false);
            return _flag;
        }
        public bool IsCardSeriesExits(string series)
        {
            string CardName = string.Empty;
            List<ImageCardTypeInfo> cardTypeList = new List<ImageCardTypeInfo>();
            this.operation = () =>
            {
                CardTypeDao access = new CardTypeDao(this.Transaction);
                cardTypeList = access.GetCardSeries(series);
            };
            this.Start(false);
            if (cardTypeList == null)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        public bool SetCardTypeInfo(int cardId, string strCardTypeName, string strCardSeries, int strcodeType, bool? status, int maxImage, string strDescription, bool IsPrepaid, int PackageId, bool IsWaterMark)
        {
            Boolean _flag = true;
            this.operation = () =>
            {
                CardTypeDao access = new CardTypeDao(this.Transaction);
                _flag = access.INS_iMixImageCardType(cardId, strCardTypeName, strCardSeries, strcodeType, status, maxImage, strDescription, IsPrepaid, PackageId, IsWaterMark);
            }; this.Start(false);
            return _flag;
        }
        public List<ValueTypeInfo> GetCardTypes()
        {

            List<ValueTypeInfo> objDic = null;
            this.operation = () =>
            {
                ValueTypeDao access = new ValueTypeDao(this.Transaction);
                objDic = access.GetCardTypes();
            };
            this.Start(false);
            return objDic;

        }
    }
}
