﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DigiPhoto.IMIX.DataAccess;
using DigiPhoto.IMIX.Model;
using System.Collections.ObjectModel;

namespace DigiPhoto.IMIX.Business
{
  public  class TaxBusiness : BaseBusiness
    {
      public bool SaveOrderTaxDetails(int StoreId, int OrderId, int SubStoreId)
        {
            bool issucess = false;
            this.operation = () =>
            {
                TaxAccess access = new TaxAccess(this.Transaction);
                issucess = access.SaveOrderTaxDetails(StoreId, OrderId, SubStoreId);
            };
            this.Start(false);
          
            return issucess;
        }


      //public bool SaveTaxDetails(int StoreId, int TaxId, float TaxPercentage, bool Status , DateTime ModifiedDate)
      public bool SaveTaxDetails(int VenueId, int TaxId,int inActiveTaxId, decimal TaxPercentage, bool Status, DateTime modifiedate)
      {
          bool issuccess = false;
          this.operation = () =>
              {
                  TaxAccess access = new TaxAccess(this.Transaction);
                  issuccess = access.SaveTaxDetails(VenueId, TaxId, inActiveTaxId, TaxPercentage, Status,modifiedate);
              };
          this.Start(false);

          return issuccess;
      }

        public ObservableCollection<TaxDetailInfo> GetTaxDetail()
      {
          ObservableCollection<TaxDetailInfo> lstTaxInfo = new ObservableCollection<TaxDetailInfo>();

          this.Operation = () =>
              {
                  TaxAccess access = new TaxAccess(this.Transaction);
                  lstTaxInfo = access.GetTaxDetail();
              };
          this.Start(false);
          return lstTaxInfo;
      }
         
        public List<TaxDetailInfo> GetTaxDetail(int? orderId)
        {
            List<TaxDetailInfo> lstTaxInfo = new List<TaxDetailInfo>();

            this.operation = () =>
            {
                TaxAccess access = new TaxAccess(this.Transaction);
                lstTaxInfo = access.GetTaxDetail(orderId);
            };
            this.Start(false);

            return lstTaxInfo;
        }
        public List<TaxDetailInfo> GetReportTaxDetail(DateTime FromDate, DateTime ToDate, int subStoreID)
        {
            List<TaxDetailInfo> lstTaxInfo = new List<TaxDetailInfo>();

            this.operation = () =>
            {
                TaxAccess access = new TaxAccess(this.Transaction);
                lstTaxInfo = access.GetReportTaxDetail(FromDate, ToDate, subStoreID);
            };
            this.Start(false);
            return lstTaxInfo;
        }

        public bool UpdateStoreTaxData(StoreInfo store)

        {
            bool issucess = false;
            this.operation = () =>
            {
                TaxAccess access = new TaxAccess(this.Transaction);
                issucess = access.UpdateStoreTaxData(store);
            };
            this.Start(false);
            return issucess;
        }
        public StoreInfo getTaxConfigData()
        {
            StoreInfo storeInfo = new StoreInfo();
            this.operation = () =>
            {
                TaxAccess access = new TaxAccess(this.Transaction);
                storeInfo = access.getTaxConfigData();
            };
            this.Start(false);
            return storeInfo;
        }
		////changed by latika for location wise settings image process ,manual download and video process enging
        public StoreInfo getTaxConfigDataBySystemID(string SystemName)
        {
            StoreInfo storeInfo = new StoreInfo();
            this.operation = () =>
            {
                TaxAccess access = new TaxAccess(this.Transaction);
                storeInfo = access.getTaxConfigDataBySystemID(SystemName);
            };
            this.Start(false);
            return storeInfo;
        }
		////end
        public List<TaxDetailInfo> GetApplicableTaxes(int taxID)
        {
            List<TaxDetailInfo> lstTaxInfo = new List<TaxDetailInfo>();

            this.operation = () =>
            {
                TaxAccess access = new TaxAccess(this.Transaction);
                lstTaxInfo = access.GetApplicableTaxes(taxID);
            };
            this.Start(false);
            return lstTaxInfo;
        }
        /// <summary>
        /// Priyanka code to chech ISGSTActive from database on 10 july 2019
        /// </summary>
        public List<TaxDetailInfo> GetAllTaxDetails()
        {
            List<TaxDetailInfo> lstTaxDetails = new List<TaxDetailInfo>();

            this.operation = () =>
            {
                TaxAccess access = new TaxAccess(this.Transaction);
                lstTaxDetails = access.GetAllTaxDetails();
            };
            this.Start(false);
            return lstTaxDetails;
        }
        ////End Priyanka
        ///
        public int Get_TaxPercent()
        {
            int tax_Info = 0;
            this.operation = () =>
            {
                TaxAccess access = new TaxAccess(this.Transaction);
                tax_Info = access.Get_TaxPercent();
            };
            this.Start(false);
            return tax_Info;
        }
        /////changed by latika for Evoucher 

        public EvoucherDtlsRpt GetEvocuherDtls(int OrderID)
        {
            EvoucherDtlsRpt storeInfo = new EvoucherDtlsRpt();
            this.operation = () =>
            {
                TaxAccess access = new TaxAccess(this.Transaction);
                storeInfo = access.GetEvocuherDtls(OrderID);
            };
            this.Start(false);
            return storeInfo;
        }
        ////end by latika

    }
}
