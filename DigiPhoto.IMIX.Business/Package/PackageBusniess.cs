﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using DigiPhoto.DataLayer.Model;
using System.Xml.Linq;
using System.Data;
using System.ComponentModel;
using System.Collections;
using System.Reflection;
using System.Globalization;
using DigiPhoto.IMIX.Model;
using DigiPhoto.IMIX.DataAccess;
using DigiPhoto.IMIX.Business;
using DigiPhoto.DataLayer;
using DigiPhoto.Utility.Repository.ValueType;
namespace DigiPhoto.IMIX.Business
{
    public partial class PackageBusniess : BaseBusiness
    {
        /// <summary>
        /// Gets the child product type quantity.
        /// </summary>
        /// <param name="Child">The child.</param>
        /// <param name="parentId">The parent unique identifier.</param>
        /// <returns></returns>
        public string GetChildProductTypeQuantity(int Child, int parentId)
        {
            string ProductQuantity = string.Empty;

            this.operation = () =>
            {
                PackageDetailsDao access = new PackageDetailsDao(this.Transaction);
                ProductQuantity = access.GetChildProductTypeQuantity(parentId, Child);
            };
            this.Start(false);
            if (ProductQuantity != null)
                return ProductQuantity;
            else
                return "0";
        }


        /// <summary>
        /// Gets the maximum quantityof itemina package.
        /// </summary>
        /// <param name="pkgId">The PKG unique identifier.</param>
        /// <param name="producttypeid">The producttypeid.</param>
        /// <returns></returns>
        public int GetMaxQuantityofIteminaPackage(int pkgId, int productTypeId)
        {
            int MaxImage = 1;
            this.operation = () =>
            {
                PackageDetailsDao access = new PackageDetailsDao(this.Transaction);
                MaxImage = access.GetMaxQuantityofIteminaPackage(pkgId, productTypeId);
            };
            this.Start(false);
            return MaxImage;
        }

        /// <summary>
        /// Gets the type of the child product.
        /// </summary>
        /// <param name="parentid">The parentid.</param>
        /// <returns></returns>
        public string GetChildProductType(int parentid)
        {

            string _objProductType = string.Empty;
            this.operation = () =>
            {
                PackageDetailsDao access = new PackageDetailsDao(this.Transaction);
                _objProductType = access.GetChildProductTypeById(parentid);
            };
            this.Start(false);
            return _objProductType;
        }


        /// <summary>
        /// Sets the package details.
        /// </summary>
        /// <param name="packageId">The package unique identifier.</param>
        /// <param name="ProductTypeId">The product type unique identifier.</param>
        /// <param name="PackageQuantity">The package quantity.</param>
        /// <param name="PackageMaxQuanity">The package maximum quanity.</param>
        /// <returns></returns>
        public bool SetPackageDetails(int packageId, int ProductTypeId, int? PackageQuantity, int? PackageMaxQuanity,int? VideoLength)
        {
            this.operation = () =>
            {
                PackageDetailsDao access = new PackageDetailsDao(this.Transaction);
                access.InsertPackageDetails(packageId, ProductTypeId, PackageQuantity, PackageMaxQuanity, VideoLength);
            };
            this.Start(false);
            return true;
        }



        /// <summary>
        /// Sets the package master details.
        /// </summary>
        /// <param name="packageId">The package unique identifier.</param>
        /// <param name="packageName">Name of the package.</param>
        /// <param name="Packageprice">The packageprice.</param>
        /// <returns></returns>
        public bool SetPackageMasterDetails(int packageId, string packageName, string Packageprice)
        {
            this.operation = () =>
                {
                    PackageDetailsDao access = new PackageDetailsDao(this.Transaction);
                    access.UpdAndDelPackageMasterDetails(packageId, packageName, Packageprice);
                };
            this.Start(false);
            return true;
        }
        ////change made by latka for AR personalised
        public bool GETIsPersonalizedAR(int packageId)
        {
            bool isPersonal = false;
            this.operation = () =>
            {
                PackageDetailsDao access = new PackageDetailsDao(this.Transaction);
                isPersonal = access.GETIsPersonalizedAR(packageId);
            };
            this.Start(false);
            return isPersonal;

        }
        /////end

    }
}
