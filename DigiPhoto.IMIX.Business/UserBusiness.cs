﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DigiPhoto.IMIX.Model;
using DigiPhoto.IMIX.DataAccess;

namespace DigiPhoto.IMIX.Business
{
    public partial class UserBusiness : BaseBusiness
    {
        public List<UserInfo> GetPhotoGraphersList()
        {
            List<UserInfo> lstUserInfo = new List<UserInfo>();

            this.operation = () =>
            {
                UserAccess access = new UserAccess(this.Transaction);
                lstUserInfo = access.GetPhotoGraphersList();
            };
            this.Start(false);

            return lstUserInfo;
        }
        public List<PhotoGraphersInfo> GetPhotoGraphersList(int storeId)
        {
            List<PhotoGraphersInfo> UserDetailsList = new List<PhotoGraphersInfo>();
            this.operation = () =>
            {
                StoreDao access = new StoreDao(this.Transaction);
                UserDetailsList = access.SelectPhotoGraphersList(storeId);
            };
            this.Start(false);
            return UserDetailsList;
        }
        public List<UserInfo> GetPhotoGrapher()
        {
            try
            {
                List<UserInfo> _objridecamera = new List<UserInfo>();
                this.operation = () =>
                {
                    UsersDao access = new UsersDao(this.Transaction);
                    _objridecamera = access.GetPhotoGrapherList();
                }; this.Start(false);
                return _objridecamera;
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogFileWrite(ErrorHandler.ErrorHandler.CreateErrorMessage(ex));
                return null;
            }
        }

        public List<UsersInfo> GetUserDetailsByUserIddetail(int userId)
        {
            try
            {
                List<UsersInfo> _objrideUserInfo = new List<UsersInfo>();
                this.operation = () =>
                {
                    UsersDao access = new UsersDao(this.Transaction);
                    _objrideUserInfo = access.GetUserByUserId(userId);
                }; this.Start(false);
                return _objrideUserInfo;
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogFileWrite(ErrorHandler.ErrorHandler.CreateErrorMessage(ex));
                return null;
            }
        }
    }
}
