﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using System.Xml.Linq;
using System.Data;
using System.ComponentModel;
using System.Collections;
using System.Reflection;
using System.Globalization;
using DigiPhoto.IMIX.Model;
using DigiPhoto.IMIX.DataAccess;
using DigiPhoto.IMIX.Business;
using DigiPhoto.DataLayer;

namespace DigiPhoto.IMIX.Business
{

    public partial class AppStartUpBusiness : BaseBusiness
    {
        public string IsUpdateAvailable(string currVersion)
        {
            string avVersion = string.Empty;
            List<string> objList = null;
            this.operation = () =>
            {
                ActivityDao access = new ActivityDao(this.Transaction);
                objList = access.IsUpdateAvailable(currVersion);
            };
            this.Start(false);
            if (objList != null && objList.Count > 0)
            {
                avVersion = string.Join(",", objList);
            }
            return avVersion;
        }

    }
}
