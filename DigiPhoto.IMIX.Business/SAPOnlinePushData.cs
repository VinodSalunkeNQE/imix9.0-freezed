﻿using DigiPhoto.IMIX.DataAccess;
using DigiPhoto.IMIX.Model;
using DigiPhoto.IMIX.DataAccess;
using DigiPhoto.IMIX.Model;
using System;
using System.Collections.Generic;
using DigiPhoto.IMIX.DataAccess;
using DigiPhoto.IMIX.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DigiPhoto.Utility.Repository.ValueType;
using System.Data;
using DigiPhoto.IMIX.Model.Base;
using DigiPhoto.IMIX.DataAccess.Base;

namespace DigiPhoto.IMIX.Business
{
   public class SAPOnlinePushData: BaseBusiness
    {
        public List<SAPOnlineDataPushInfo> SelectAllSAPOnlinePushData()
        {
            List<SAPOnlineDataPushInfo> SAPDataList = new List<SAPOnlineDataPushInfo>();

            this.operation = () =>
            {
                SAPOnlinePushDao access = new SAPOnlinePushDao(this.Transaction);
                SAPDataList = access.SelectAllSAPOnlinePushData();
            };
            this.Start(false);
            return SAPDataList;
        }
        public bool GetretryFiledCount(string Photo_Id, int RetryCount)
        {
            bool flag = false;
            this.operation = () =>
            {
                PhotosDao access = new PhotosDao(this.Transaction);
                flag = access.GetErrorCount(Photo_Id, RetryCount);
            };
            this.Start(false);
            return flag;
        }

        public bool Updatestatus(long SapId,bool result)
        {
            bool flag = false;
            this.operation = () =>
            {
                PhotosDao access = new PhotosDao(this.Transaction);
                flag = access.Updatestatus(SapId, result);
            };
            this.Start(false);
            return flag;
        }

        public void DataInsertintoTable(DateTime Todaysdate)
        {
            this.operation = () =>
            {
                SAPOnlinePushDao access = new SAPOnlinePushDao(this.Transaction);
                access.DataInsertintoTable(Todaysdate);
            };
            this.Start(false);
        }
    }
}
