﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using System.Xml.Linq;
using System.Data;
using System.ComponentModel;
using System.Collections;
using System.Reflection;
using System.Globalization;
using DigiPhoto.IMIX.Model;
using DigiPhoto.IMIX.DataAccess;
using DigiPhoto.IMIX.Business;
using DigiPhoto.DataLayer;


namespace DigiPhoto.IMIX.Business
{
    public partial class CommonBusiness : BaseBusiness
    {
        public Dictionary<string, int> GetCardCodeTypes()
        {
            try
            {
                Dictionary<string, int> objDic = new Dictionary<string, int>();
                this.operation = () =>
                {
                    objDic.Add("QR/Bar Code", 405);
                    objDic.Add("Unique", 403);
                    objDic.Add("Lost", 404);
                    objDic.Add("RFID", 406);
                };
                this.Start(false);
                return objDic;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }




        /// <summary>
        /// Gets the version details.
        /// </summary>
        /// <param name="MachineName">Name of the machine.</param>
        /// <returns></returns>
        //public List<DG_VersionHistory> GetVersionDetails(string MachineName)
        //{
        //    CommonDao access = new CommonDao();
        //    List<DG_VersionHistory> DG_VersionHistoryList = new List<DG_VersionHistory>();
        //    this.operation = () =>
        //    {
        //        DG_VersionHistoryList = access.GetVersionDetails(MachineName);
        //    };
        //    this.Start(false);
        //    return DG_VersionHistoryList;
        //}


        public List<TableBaseInfo> GetAllTable()
        {
            List<TableBaseInfo> allTable = new List<TableBaseInfo>();
            try
            {
                this.operation = () =>
                 {
                     CommonDao access = new CommonDao(this.Transaction);
                     allTable = access.SelectAllTable();
                 };
                this.Start(false);  
                return allTable;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public DataTable CopyGenericToDataTable<T>(IEnumerable<T> items)
        {
            var properties = typeof(T).GetProperties().Where(p => (p.Name != "EntityKey" && p.Name != "EntityState"));
            var result = new DataTable();

            //Build the columns
            foreach (var prop in properties)
            {
                Type propType = prop.PropertyType;
                if (propType.IsGenericType &&
                    propType.GetGenericTypeDefinition() == typeof(Nullable<>))
                {
                    propType = Nullable.GetUnderlyingType(propType);
                }
                result.Columns.Add(prop.Name, propType);
            }

            //Fill the DataTable
            foreach (var item in items)
            {
                var row = result.NewRow();

                foreach (var prop in properties)
                {
                    var itemValue = prop.GetValue(item, new object[] { });
                    if (itemValue != null)
                        row[prop.Name] = itemValue;
                    else
                        row[prop.Name] = DBNull.Value;
                }

                result.Rows.Add(row);
            }

            return result;
        }
        public bool ImportMasterData(DataTable dtSite, DataTable dtItem, DataTable dtpkg)
        {
            //List<TableBaseInfo> allTable = new List<TableBaseInfo>();
            bool ret = false;
            try
            {
                this.operation = () =>
                {
                    CommonDao access = new CommonDao(this.Transaction);
                    ret = access.ImportMasterData(dtSite,dtItem,dtpkg);
                };
                this.Start(false);
                return ret;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        #region Added by Ajay for panoramic sizes 
        public  string[] GetPanoramicSizes() 
        {
            string[] PanoramicSizes = null;
            try
            {
                this.operation = () =>
                {
                    CommonDao access = new CommonDao(this.Transaction);
                    PanoramicSizes = access.GetPanoramicSizes();
                };
                this.Start(false);
                return PanoramicSizes;
            }
            catch (Exception ex)
            {
                return null;
            }
            
        }
        public string[] GetPanoramicSizesCode()
        {
            string[] PanoramicSizes = null;
            try
            {
                this.operation = () =>
                {
                    CommonDao access = new CommonDao(this.Transaction);
                    PanoramicSizes = access.GetPanoramicSizesCode();
                };
                this.Start(false);
                return PanoramicSizes;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        #endregion
    }
   
}
