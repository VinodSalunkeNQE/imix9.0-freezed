﻿using DigiPhoto.IMIX.DataAccess;
using DigiPhoto.IMIX.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DigiPhoto.Utility.Repository.ValueType;
using System.Data;
using DigiPhoto.IMIX.Model.Base;

namespace DigiPhoto.IMIX.Business
{
    public class PhotoBusiness : BaseBusiness
    {
        public List<PhotoDetail> GetPhotoDetailsByPhotoIds(string PhotoIds)
        {
            try
            {
                List<PhotoDetail> list = new List<PhotoDetail>();
                this.operation = () =>
                {
                    PhotoAccess access = new PhotoAccess(this.Transaction);
                    list = access.GetPhotoDetailsByPhotoIds(PhotoIds);
                    //  return list;
                };
                this.Start(false);
                return list;
            }
            catch
            {
                return null;
            }
        }
        /// <summary>
        /// By KCB on 05 JUN 2019 for fetching photo metadata from database
        /// </summary>
        /// <param name="PhotoIds">List of photos provided by Facial API</param>
        /// <returns></returns>
        public List<PhotoInfo> GetPhotoDetailsByPhotoIds(List<string> PhotoIds)
        {
            try
            {
                List<PhotoInfo> list = new List<PhotoInfo>();
                this.operation = () =>
                {
                    PhotoAccess access = new PhotoAccess(this.Transaction);
                    list = access.GetPhotoDetailsByPhotoIds(PhotoIds);
                    //  return list;
                };
                this.Start(false);
                return list;
            }
            catch
            {
                return null;
            }
        }

        public List<PhotoDetail> GetImagesInfoForEditing(int SubStoreId, string PosName)
        {
            List<PhotoDetail> list = null;
            try
            {
                list = new List<PhotoDetail>();
                this.operation = () =>
                {
                    PhotoAccess access = new PhotoAccess(this.Transaction);
                    list = access.GetImagesInfoForEditing(SubStoreId, PosName);
                };
                this.Start(false);
                return list;
            }
            catch
            {
                list = new List<PhotoDetail>();
                //return null;
                return list;
            }

        }
        public bool UpdateImageProcessedStatus(int PhotoId, int ProcessedStatus)
        {
            bool result = false;
            try
            {
                this.operation = () =>
                {
                    PhotoAccess access = new PhotoAccess(this.Transaction);
                    result = access.UpdateImageProcessedStatus(PhotoId, ProcessedStatus);
                };
                this.Start(false);
                return result;
            }
            catch
            {
                return result;
            }
        }
        public List<PhotoDetail> GetFilesForAutoVideoProcessing(int substoreID, string PosName)
        {
            try
            {
                List<PhotoDetail> list = new List<PhotoDetail>();
                this.operation = () =>
                {
                    PhotoAccess access = new PhotoAccess(this.Transaction);
                    list = access.GetFilesForAutoVideoProcessing(substoreID, PosName);
                };
                this.Start(false);
                return list;
            }
            catch
            {
                return null;
            }
        }
        public bool UpdateVideoProcessedStatus(int PhotoId, bool ProcessedStatus)
        {
            bool result = false;
            try
            {
                this.operation = () =>
                {
                    PhotoAccess access = new PhotoAccess(this.Transaction);
                    result = access.UpdateVideoProcessedStatus(PhotoId, ProcessedStatus);
                };
                this.Start(false);
                return result;
            }
            catch
            {
                return result;
            }
        }
        public bool TruncatePhotoGroupTablefordate(int days, int substoreID)
        {
            bool res = false;
            try
            {
                this.operation = () =>
                {
                    PhotosDao access = new PhotosDao(this.Transaction);
                    access.DeletePhotoGroupBySubStoreIdAndDays(days, substoreID);

                };
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        public bool TruncatePhotoGroupTable(int days, int substoreID)
        {
            bool res = false;
            try
            {
                PhotoInfo _objphotos = new PhotoInfo();
                this.operation = () =>
                {
                    PhotosDao access = new PhotosDao(this.Transaction);
                    res = access.DelPhotoGroupBySubstoreId(substoreID);
                };
                this.Start(false);
                if (_objphotos != null)
                {
                    if (_objphotos.DG_IsCodeType == true)
                        res = true;
                }
            }
            catch
            {
                res = false;
            }
            return res;
        }
        public void SetArchiveDetails(string imgname)
        {
            this.operation = () =>
            {
                PhotosDao access = new PhotosDao(this.Transaction);
                access.UpdateArchiveByFileName(imgname); //TBD get by t => t.DG_Photos_FileName == imgname and set DG_Photos_Archive = true;
            };
            this.Start(false);

        }
        public Int32 SetPhotoDetails(int subStoreId, string DG_Photos_RFID, string DG_Photos_FileName, DateTime DG_Photos_CreatedOn, string userid, string imgmetadata, int locationid, string photolayer, string DG_Photos_Effects, DateTime? DateTaken, int? RfidScanType, int IsImageProcessed, int? CharacterID)
        {

            int DG_Photos_pkey = 0;
            this.operation = () =>
            {
                PhotosDao access = new PhotosDao(this.Transaction);
                DG_Photos_pkey = access.InsertPhotoDetails(subStoreId, locationid, RfidScanType, DG_Photos_pkey, DG_Photos_CreatedOn, DateTaken, DG_Photos_RFID, DG_Photos_FileName, userid, imgmetadata, photolayer, DG_Photos_Effects, IsImageProcessed, CharacterID);
            };
            this.Start(false);
            return DG_Photos_pkey;
        }
        public Int32 SetPhotoDetails(int subStoreId, string DG_Photos_RFID, string DG_Photos_FileName, DateTime DG_Photos_CreatedOn, string userid, string imgmetadata, int locationid, string photolayer, string DG_Photos_Effects, DateTime? DateTaken, int? RfidScanType, int? CharacterID)
        {
            PhotosDao access = new PhotosDao(this.Transaction);
            int DG_Photos_pkey = 0;
            this.operation = () =>
            {
                DG_Photos_pkey = access.InsertPhotoDetails(subStoreId, locationid, RfidScanType, DG_Photos_pkey, DG_Photos_CreatedOn, DateTaken, DG_Photos_RFID, DG_Photos_FileName, userid, imgmetadata, photolayer, DG_Photos_Effects, 0, CharacterID);
            };
            this.Start(false);
            return DG_Photos_pkey;
        }
        public Int32 SetPhotoDetails(int subStoreId, string DG_Photos_RFID, string DG_Photos_FileName, DateTime DG_Photos_CreatedOn, string userid, string imgmetadata, int locationid, string photolayer, string DG_Photos_Effects, DateTime? DateTaken, int? RfidScanType, int IsImageProcessed, int? CharacterID, long? VideoLength, bool IsVideoProcessed, int? mediatype = 1, int? ParentImageId = 0, string OriginalFileName = null, int SemiOrderProfileId = 0, bool IsCroped = false, long photoDisplayOrder = 0)
        {

            int DG_Photos_pkey = 0;
            this.operation = () =>
            {
                PhotosDao access = new PhotosDao(this.Transaction);
                DG_Photos_pkey = access.InsertPhotoDetails(subStoreId, locationid, RfidScanType, DG_Photos_pkey, DG_Photos_CreatedOn, DateTaken, DG_Photos_RFID, DG_Photos_FileName, userid, imgmetadata, photolayer, DG_Photos_Effects, IsImageProcessed, CharacterID, VideoLength, IsVideoProcessed, mediatype, ParentImageId, OriginalFileName, SemiOrderProfileId, IsCroped, photoDisplayOrder);
            };
            this.Start(false);
            return DG_Photos_pkey;
        }
        public int PhotoCountCurrentDate(string RFID, int photographerID, int mediaType, long seriesFirstNum = 0, long seriesLastNum = 0)
        {

            int DG_Photos_pkey = 0;
            this.operation = () =>
            {
                PhotosDao access = new PhotosDao(this.Transaction);
                DG_Photos_pkey = access.PhotoCountCurrentDate(RFID, photographerID, mediaType, seriesFirstNum, seriesLastNum);
            };
            this.Start(false);
            return DG_Photos_pkey;
        }
        //public Int32 SetPhotoDetails(int subStoreId, string DG_Photos_RFID, string DG_Photos_FileName, DateTime DG_Photos_CreatedOn, string userid, string imgmetadata, int locationid, string photolayer, string DG_Photos_Effects, DateTime? DateTaken, int? RfidScanType, int? CharacterID,long VideoLength,bool IsVideoProcessed)
        //{
        //    PhotosDao access = new PhotosDao(this.Transaction);
        //    int DG_Photos_pkey = 0;
        //    this.operation = () =>
        //    {
        //        DG_Photos_pkey = access.InsertPhotoDetails(subStoreId, locationid, RfidScanType, DG_Photos_pkey, DG_Photos_CreatedOn, DateTaken, DG_Photos_RFID, DG_Photos_FileName, userid, imgmetadata, photolayer, DG_Photos_Effects, 0, CharacterID);
        //    };
        //    this.Start(false);
        //    return DG_Photos_pkey;
        //}
        public bool SaveIsCropedPhotos(Int64 PhotoId, object value, string operation)
        {
            try
            {
                bool result = false;
                this.operation = () =>
                {
                    PhotosDao access = new PhotosDao(this.Transaction);
                    result = access.UpdateCropedPhotos(PhotoId.ToInt32(), value, operation);
                };
                this.Start(false);
                return result;
            }
            catch (Exception ex)
            {
                return false;
                throw ex;
            }
        }
        public bool SaveIsCropedPhotosStoryBook(Int64 PhotoId, object value, string operation)
        {
            try
            {
                bool result = false;
                this.operation = () =>
                {
                    PhotosDao access = new PhotosDao(this.Transaction);
                    result = access.UpdateCropedPhotosStoryBook(PhotoId.ToInt32(), value, operation);
                };
                this.Start(false);
                return result;
            }
            catch (Exception ex)
            {
                return false;
                throw ex;
            }
        }
        public PhotoInfo GetPhotoDetailsbyPhotoId(int PhotoId)
        {
            try
            {
                PhotoInfo _objPhoto = new PhotoInfo();
                this.operation = () =>
                {
                    PhotosDao access = new PhotosDao(this.Transaction);
                    _objPhoto = access.GetPhotosByPkey(PhotoId);
                };
                this.Start(false);
                return _objPhoto;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public PhotoInfo GetPhotoStoryBookDetailsbyPhotoId(int PhotoId)
        {
            try
            {
                PhotoInfo _objPhoto = new PhotoInfo();
                this.operation = () =>
                {
                    PhotosDao access = new PhotosDao(this.Transaction);
                    _objPhoto = access.GetPhotosByPkeyStoryBook(PhotoId);
                };
                this.Start(false);
                return _objPhoto;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }



        #region GetOrderProductName
        public string GetOrderProductName(int PhotoId)
        {
            try
            {
                string _objPhotoProductName = string.Empty;
                this.operation = () =>
                {
                    PhotosDao access = new PhotosDao(this.Transaction);
                    _objPhotoProductName = access.GetOrderProductName(PhotoId);
                };
                this.Start(false);
                return _objPhotoProductName;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        public string GetFileNameByPhotoID(string PhotoID)
        {
            if (PhotoID.Contains(","))
                return null;
            PhotoInfo _objphotos = new PhotoInfo();
            this.operation = () =>
            {
                PhotosDao access = new PhotosDao(this.Transaction);
                _objphotos = access.GetPhotosByPkey(PhotoID.ToInt32());
            };
            this.Start(false);
            if (_objphotos != null)
            {
                return _objphotos.DG_Photos_FileName;
            }
            else
            {
                return null;
            }

        }
        public PhotoInfo GetPhotoByPhotoID(string PhotoID)
        {
            if (PhotoID.Contains(","))
                return null;
            PhotoInfo _objphotos = new PhotoInfo();
            this.operation = () =>
            {
                PhotosDao access = new PhotosDao(this.Transaction);
                _objphotos = access.GetPhotosByPkey(PhotoID.ToInt32());
            };
            this.Start(false);
            return _objphotos;

        }
        public bool GetModeratePhotos(Int64 PhotoId)
        {
            ModratePhotoInfo objInfo = new ModratePhotoInfo();

            this.operation = () =>
            {
                PhotosDao access = new PhotosDao(this.Transaction);
                objInfo = access.GetModeratePhotos(PhotoId);
            };
            this.Start(false);
            if (objInfo != null)
            {
                return true;
            }
            else
            {
                return false;
            }

        }
        public List<ModratePhotoInfo> GetModeratePhotos()
        {
            List<ModratePhotoInfo> objectList = new List<ModratePhotoInfo>();
            this.operation = () =>
            {
                PhotosDao access = new PhotosDao(this.Transaction);
                objectList = access.GetModeratePhotos(null);
            };
            this.Start(false);
            return objectList;

        }
        public void immoderate(Int64 PhotoId)
        {
            this.operation = () =>
            {
                PhotosDao access = new PhotosDao(this.Transaction);
                access.DeleteModeratePhotosById(PhotoId);
            };
            this.Start(false);
        }
        public List<PhotoInfo> GetAllPhotosforSearch(string substoreId, long imgRfid, int NoOfImg, bool isAnyRfidSearch
            , long StartIndex, int RfidSearch, int NewRecord, out long maxPhotoId, out long minPhotoId, int mediaType
            )
        {
            long maxId = 0;
            long minId = 0;
            List<PhotoInfo> PhotosList = new List<PhotoInfo>();

            List<string> _lststr = new List<string>();
            _lststr = substoreId.Split(',').ToList();

            this.operation = () =>
            {
                PhotosDao access = new PhotosDao(this.Transaction);
                PhotosList = access.SelectAllPhotosforSearch(substoreId, imgRfid, NoOfImg, isAnyRfidSearch
                    , StartIndex, RfidSearch, NewRecord, out maxId, out minId, mediaType
                    );
            };
            this.Start(false);
            maxPhotoId = maxId;
            minPhotoId = minId;
            return PhotosList;

        }
        ///SET 3 by Vinod Salunke
        public List<PhotoInfo> SelectAllPhotosToBlobUpload()
        {
            List<PhotoInfo> PhotosList = new List<PhotoInfo>();
            ErrorHandler.ErrorHandler.LogFileWrite("SelectAllPhotosToBlobUpload");
            this.operation = () =>
            {
                PhotosDao access = new PhotosDao(this.Transaction);
                PhotosList = access.SelectAllPhotosToBlobUpload();
            };
            this.Start(false);
            return PhotosList;
        }

        public List<PhotoInfo> SelectAllEditedPhotosToBlobUpload()
        {
            List<PhotoInfo> PhotosList = new List<PhotoInfo>();
            ErrorHandler.ErrorHandler.LogFileWrite("SelectAllPhotosToBlobUpload");
            this.operation = () =>
            {
                PhotosDao access = new PhotosDao(this.Transaction);
                PhotosList = access.SelectAllEditedPhotosToBlobUpload();
            };
            this.Start(false);
            return PhotosList;
        }

        public bool GetretryFiledCount(string Photo_Id, int RetryCount)
        {
            bool flag = false;
            this.operation = () =>
            {
                PhotosDao access = new PhotosDao(this.Transaction);
                flag = access.GetErrorCount(Photo_Id, RetryCount);
            };
            this.Start(false);
            return flag;
        }

        public bool GetParticalEditLocationDetails(int LocationId, int SubStoreID)
        {
            bool flag = false;
            this.operation = () =>
            {
                PhotosDao access = new PhotosDao(this.Transaction);
                flag = access.GetParticalEditLocationDetails(LocationId, SubStoreID);
            };
            this.Start(false);
            return flag;
        }

        public string GetSynccodeValues(string sp_name, int id)
        {
            string Synccode = string.Empty;
            this.operation = () =>
            {
                PhotosDao access = new PhotosDao(this.Transaction);
                Synccode = access.GetSynccodeValues(sp_name, id);
            };
            this.Start(false);
            return Synccode;
        }

        public string GetFileWatcherConfigValues(int SubstoreId)
        {
            string result = string.Empty;
            this.operation = () =>
            {
                PhotosDao access = new PhotosDao(this.Transaction);
                result = access.GetFileWatcherConfigValues( SubstoreId);
            };
            this.Start(false);
            return result;
        }

        public bool SaveFileWatcherConfig(string IsFileConfig, int SubstoreId)
        {
            try
            {
                bool result = false;

                this.operation = () =>
                {
                    PhotosDao access = new PhotosDao(this.Transaction);
                    result = access.SaveFileWatcherConfig(IsFileConfig, SubstoreId);
                };
                this.Start(false);
                return result;

            }
            catch (Exception ex)
            {
                return false;
                //throw ex;

            }
        }

        public List<AssociateImageDetails> GetAssociateImageDetails(string Photo_Id)
        {
            List<AssociateImageDetails> AssociateImg = new List<AssociateImageDetails>();

            this.operation = () =>
            {
                PhotosDao access = new PhotosDao(this.Transaction);
                AssociateImg = access.GetAssociateImageDetails(Photo_Id);
            };
            this.Start(false);
            return AssociateImg;
        }

        public List<AssociateImageDetails> GetAssociateImageDetails()
        {
            List<AssociateImageDetails> AssociateImg = new List<AssociateImageDetails>();

            this.operation = () =>
            {
                PhotosDao access = new PhotosDao(this.Transaction);
                AssociateImg = access.GetAssociateImageDetails();
            };
            this.Start(false);
            return AssociateImg;
        }
        public bool SaveIsGreenPhotos(Int64 PhotoId, bool value)
        {
            try
            {
                bool result = false;

                this.operation = () =>
                {
                    PhotosDao access = new PhotosDao(this.Transaction);
                    result = access.UpdGreenPhotos(PhotoId, value);
                };
                this.Start(false);
                return result;

            }
            catch (Exception ex)
            {
                return false;
                //throw ex;

            }
        }
        public bool SetModerateImage(int PhotoId, int userid)
        {
            try
            {
                this.operation = () =>
                {
                    PhotosDao access = new PhotosDao(this.Transaction);
                    access.InsertModerateImage(PhotoId, userid);
                };
                this.Start(false);
                return true;

            }
            catch (Exception ex)
            {
                return false;
            }
        }
        public bool SaveCroppedPhotoInfo(Int64 PhotoId, object value, string effects)
        {
            try
            {
                bool result = false;
                PhotoInfo objInfo = new PhotoInfo();
                objInfo.DG_Photos_pkey = PhotoId.ToInt32();
                if (value != null)
                {
                    objInfo.DG_Photos_IsCroped = bool.Parse(value.ToString());
                }
                objInfo.DG_Photos_Effects = effects;


                this.operation = () =>
                {
                    PhotosDao access = new PhotosDao(this.Transaction);
                    result = access.UpdatePhotoEffects(objInfo);
                };
                this.Start(false);
                return result;

            }
            catch (Exception ex)
            {
                return false;
                //throw ex;
            }
        }
        public void SetPreviewCounter(int PhotoId)
        {
            this.operation = () =>
            {
                PhotosDao access = new PhotosDao(this.Transaction);
                access.InsertPreviewCounter(PhotoId);
            };
        }
        public bool UpdateLayering(int PhotoId, string value)
        {
            try
            {
                PhotoInfo objInfo = new PhotoInfo();
                objInfo.DG_Photos_pkey = PhotoId.ToInt32();
                objInfo.DG_Photos_Layering = value;
                objInfo.IsImageProcessed = null;
                bool result = false;

                this.operation = () =>
                {
                    PhotosDao access = new PhotosDao(this.Transaction);
                    result = access.UpdatePhotoLayering(objInfo);
                };
                this.Start(false);
                return result;
            }
            catch (Exception ex)
            {
                //throw ex;
                return false;
            }
        }

        /// <summary>
        /// Added by Vinod Salunke to save edited photo with storybook id and theme etc..
        /// </summary>
        /// <param name="PhotoId"></param>
        /// <param name="value"></param>
        /// <param name="ThemeID"></param>
        /// <param name="StoryBookID"></param>
        /// <param name="PageNo"></param>
        /// <param name="IsVosDisplay"></param>
        /// <returns></returns>
        public Int32 UpdateLayeringStoryBook(int PhotoId, string value, int? ThemeID,long StoryBookID,int? PageNo, bool IsVosDisplay)
        {
            try
            {
                PhotoInfo objInfo = new PhotoInfo();
                objInfo.DG_Photos_pkey = PhotoId.ToInt32();
                objInfo.DG_Photos_Layering = value;
                objInfo.IsImageProcessed = null;
                objInfo.ThemeID = ThemeID;
                objInfo.StoryBookID = StoryBookID;
                objInfo.PageNo = PageNo;
                objInfo.IsVosDisplay = IsVosDisplay;

                Int32 storyBookID = 0;

                this.operation = () =>
                {
                    PhotosDao access = new PhotosDao(this.Transaction);
                    storyBookID = access.UpdatePhotoLayeringStoryBook(objInfo);
                };
                this.Start(false);
                return storyBookID;
            }
            catch (Exception ex)
            {
                //throw ex;
                return 0;
            }
        }


        public Int32 RestoreStoryBook(int PhotoId)
        {
            try
            {
                PhotoInfo objInfo = new PhotoInfo();
                objInfo.DG_Photos_pkey = PhotoId.ToInt32();
                
                this.operation = () =>
                {
                    PhotosDao access = new PhotosDao(this.Transaction);
                    access.RestoringStoryBook(objInfo);
                };
                this.Start(false);
                return 1;
            }
            catch (Exception ex)
            {
                //throw ex;
                return 0;
            }
        }


        public int GetLocationIdByPhotoId(int PhotoId)
        {
            PhotoInfo objInfo = new PhotoInfo();
            this.operation = () =>
            {
                PhotosDao access = new PhotosDao(this.Transaction);
                objInfo = access.GetPhotosByPkey(PhotoId);
            };
            this.Start(false);
            if (objInfo != null)
            {
                int LocationId = objInfo.DG_Location_Id == null ? 0 : objInfo.DG_Location_Id.ToInt32();
                if (LocationId > 0)
                {
                    return LocationId;
                }
                else
                {
                    return 0;
                }
            }
            else
            {
                return 0;
            }

        }
        public bool UpdateLayeringForSpecPrint(int PhotoId, string value)
        {
            try
            {
                try
                {
                    PhotoInfo objInfo = new PhotoInfo();
                    objInfo.DG_Photos_pkey = PhotoId.ToInt32();
                    objInfo.DG_Photos_Layering = value;
                    objInfo.IsImageProcessed = 0;
                    bool result = false;

                    this.operation = () =>
                    {
                        PhotosDao access = new PhotosDao(this.Transaction);
                        result = access.UpdatePhotoLayering(objInfo);
                    };
                    this.Start(false);
                    return result;
                }
                catch (Exception ex)
                {
                    //throw ex;
                    return false;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        public bool SetEffectsonPhoto(string value, int photoId, bool isgumballshow)
        {
            try
            {
                bool result = false;

                this.operation = () =>
                {
                    PhotosDao access = new PhotosDao(this.Transaction);
                    result = access.UpdateEffectsonPhoto(value, photoId, isgumballshow);
                };
                this.Start(false);
                return result;

            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public bool PhotoProductLastEdit_PlaceOrder(int PhotoId, int PanoramaSizeSelected)
        {
            try
            {
                bool result = false;

                this.operation = () =>
                {
                    PhotosDao access = new PhotosDao(this.Transaction);
                    result = access.PhotoProductLastEdit_PlaceOrder(PhotoId, PanoramaSizeSelected);
                };
                this.Start(false);
                return result;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public void UpdateEffectsSpecPrint(int photoId, string layeringdata, string ImageEffect, bool isGreenSreen, bool isCrop, bool isgumballshow, string GumBallRidetxtContent, string _qrCode)
        {
            try
            {

                this.operation = () =>
                {

                    PhotosDao access = new PhotosDao(this.Transaction);
                    access.UpdateEffectsSpecPrint(photoId, layeringdata, ImageEffect, isGreenSreen, isCrop, isgumballshow, GumBallRidetxtContent, _qrCode);
                };
                this.Start(false);

            }
            catch (Exception ex)
            {
                AuditLogBusiness auditLogBusiness = new AuditLogBusiness();
                auditLogBusiness.InsertExceptionLog(Convert.ToString(Guid.NewGuid()), 0, DateTime.Now, DateTime.Now, "UpdateEffectsSpecPrint", photoId, ex, "710");
            }
        }

        public void UpdateOnlineQRCodeForPhoto(int photoId, string OnlineQRCode)
        {
            this.operation = () =>
            {
                PhotosDao access = new PhotosDao(this.Transaction);
                access.UpdateOnlineQRCodeForPhoto(photoId, OnlineQRCode);
            };
            this.Start(false);
        }
        public bool CheckIsCodeType(int PhotoId)
        {
            bool res = false;
            try
            {

                PhotoInfo objInfo = this.GetPhotoDetailsbyPhotoId(PhotoId);
                if (objInfo != null)
                {
                    if (objInfo.DG_IsCodeType == true)
                        res = true;
                }
            }
            catch
            {
                res = false;
            }
            return res;

        }
        public bool DeletePhotoByPhotoId(int PhotoId)
        {
            try
            {
                bool result = false;
                this.operation = () =>
                {
                    PhotosDao access = new PhotosDao(this.Transaction);
                    result = access.DeletePhotoByPhotoId(PhotoId);
                };
                this.Start(false);
                return result;
            }

            catch
            {
                return false;
            }
        }
        public bool SetImageAssociationInfoForPostScan(List<int> PhotoIdList, string Format, string Code, bool IsAnonymousCodeActive)
        {
            try
            {

                if (!IsAnonymousCodeActive || (IsAnonymousCodeActive && Code == "1111"))
                {
                    string CardIdentityCode = Code.Substring(0, 4);
                    foreach (int photoId in PhotoIdList)
                    {
                        long result = 0; ;
                        this.operation = () =>
                        {
                            CardTypeDao access = new CardTypeDao(this.Transaction);
                            result = access.InsertImageAssociationInfo(photoId, Format, Code, IsAnonymousCodeActive);
                        };
                        this.Start(false);
                    }
                    return true;
                }
                else if (IsAnonymousCodeActive && Code != "1111")
                {
                    foreach (int photoId in PhotoIdList)
                    {
                        long result = 0; ;
                        this.operation = () =>
                        {
                            CardTypeDao access = new CardTypeDao(this.Transaction);
                            result = access.InsertImageAssociationInfo(photoId, Format, Code, IsAnonymousCodeActive);
                        };
                        this.Start(false);
                    }
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch
            {
                return false;
            }
        }
        public long SetImageAssociationInfo(int PhotoId, string Format, string Code, bool IsAnonymousCodeActive)
        {
            try
            {
                long result = 0; ;
                this.operation = () =>
                {
                    CardTypeDao access = new CardTypeDao(this.Transaction);
                    result = access.InsertImageAssociationInfo(PhotoId, Format, Code, IsAnonymousCodeActive);
                };
                this.Start(false);
                return result;
            }
            catch
            {
                return 0;
            }
        }
        public List<PhotoInfo> GetImagesBYQRCode(string QRCode, bool IsAnonymousQrCodeEnabled)
        {
            try
            {

                List<PhotoInfo> PhotosList = new List<PhotoInfo>();
                this.operation = () =>
                {
                    PhotosDao access = new PhotosDao(this.Transaction);
                    PhotosList = access.SelectMapImagesBYQRCode(IsAnonymousQrCodeEnabled, QRCode);
                };
                this.Start(false);
                return PhotosList;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        public List<PhotoInfo> GetSavedGroupImages(string GroupName)
        {
            try
            {
                List<PhotoInfo> PhotosList = new List<PhotoInfo>();
                this.operation = () =>
                {
                    PhotosDao access = new PhotosDao(this.Transaction);
                    PhotosList = access.SelectSavedGroupImages(GroupName);
                };
                this.Start(false);
                return PhotosList;
            }
            catch (Exception ex)
            {
                return new List<PhotoInfo>();
            }
        }

        public List<PhotoInfo> GetSearchedPhoto(DateTime? fromTime, DateTime? ToTime, Int32? UserId, Int32? LocationId, string substoreId)
        {
            List<PhotoInfo> PhotosList = new List<PhotoInfo>();
            this.operation = () =>
            {
                PhotosDao access = new PhotosDao(this.Transaction);
                PhotosList = access.GetPhotoList(fromTime, ToTime, UserId, LocationId, substoreId);
            };
            this.Start(false);
            return PhotosList;

        }
        //-----Start-------Nilesh----Zero search --27 Dec 2018---   		
        public List<PhotoInfo> GetAllPhotosByPage(string substoreId, long startIndex, int pazeSize, bool isNext, out long maxPhotoId, out bool IsMoreImages, out long minPhotoId, int mediaType, bool IsSearchPhotoForSubStore)
        {
            if (isNext == false && (startIndex == 1 || startIndex == 0))
                startIndex = -1;
            List<PhotoInfo> objList = new List<PhotoInfo>();
            List<string> _lststr = new List<string>();
            if (!String.IsNullOrEmpty(substoreId))
                _lststr = substoreId.Split(',').ToList();
            else
                _lststr = null;

            int? ssInfo = null;
            //-----Start-------Nilesh----Zero search --27 Dec 2018---   
            if (!IsSearchPhotoForSubStore)
            {
                ssInfo = _lststr == null ? ssInfo : _lststr[0].ToInt32();
            }


            long maxId = 0;
            long minId = 0;
            bool _isMoreImages = false;
            //-----Start-------Nilesh----Zero search --27 Dec 2018---   
            List<PhotoInfo> PhotosList = new List<PhotoInfo>();
            substoreId = string.Join(",", substoreId.Split(',').Distinct().ToArray());

            if (!IsSearchPhotoForSubStore)
            {
                this.operation = () =>
                {
                    PhotosDao access = new PhotosDao(this.Transaction);
                    PhotosList = access.GetAllPhotosByPage(ssInfo, startIndex, pazeSize, isNext, out maxId, out _isMoreImages, out minId, mediaType);
                };
            }
            else
            {
                this.operation = () =>
                {
                    PhotosDao access = new PhotosDao(this.Transaction);
                    if (substoreId.Contains(','))
                    {
                        PhotosList = access.GetAllPhotosByPageForSubStore(substoreId, startIndex, pazeSize, isNext, out maxId, out _isMoreImages, out minId, mediaType);
                    }
                    else
                    {
                        ssInfo = Convert.ToInt32(substoreId);
                        PhotosList = access.GetAllPhotosByPage(ssInfo, startIndex, pazeSize, isNext, out maxId, out _isMoreImages, out minId, mediaType);
                    }
                };
            }


            this.Start(false);
            maxPhotoId = maxId;
            minPhotoId = minId;
            IsMoreImages = _isMoreImages;
            return PhotosList;
        }


        public List<PhotoInfo> GetAllPhotosByPageStoryBook(string substoreId, long startIndex, int pazeSize, bool isNext, out long maxPhotoId, out bool IsMoreImages, out long minPhotoId, int mediaType, bool IsSearchPhotoForSubStore)
        {
            if (isNext == false && (startIndex == 1 || startIndex == 0))
                startIndex = -1;
            List<PhotoInfo> objList = new List<PhotoInfo>();
            List<string> _lststr = new List<string>();
            if (!String.IsNullOrEmpty(substoreId))
                _lststr = substoreId.Split(',').ToList();
            else
                _lststr = null;

            int? ssInfo = null;
            //-----Start-------Nilesh----Zero search --27 Dec 2018---   
            if (!IsSearchPhotoForSubStore)
            {
                ssInfo = _lststr == null ? ssInfo : _lststr[0].ToInt32();
            }


            long maxId = 0;
            long minId = 0;
            bool _isMoreImages = false;
            //-----Start-------Nilesh----Zero search --27 Dec 2018---   
            List<PhotoInfo> PhotosList = new List<PhotoInfo>();
            substoreId = string.Join(",", substoreId.Split(',').Distinct().ToArray());

            if (!IsSearchPhotoForSubStore)
            {
                this.operation = () =>
                {
                    PhotosDao access = new PhotosDao(this.Transaction);
                    PhotosList = access.GetAllPhotosByPageStoryBook(ssInfo, startIndex, pazeSize, isNext, out maxId, out _isMoreImages, out minId, mediaType);
                };
            }
            else
            {
                this.operation = () =>
                {
                    PhotosDao access = new PhotosDao(this.Transaction);
                    if (substoreId.Contains(','))
                    {
                        PhotosList = access.GetAllPhotosByPageForSubStore(substoreId, startIndex, pazeSize, isNext, out maxId, out _isMoreImages, out minId, mediaType);
                    }
                    else
                    {
                        ssInfo = Convert.ToInt32(substoreId);
                        PhotosList = access.GetAllPhotosByPageStoryBook(ssInfo, startIndex, pazeSize, isNext, out maxId, out _isMoreImages, out minId, mediaType);
                    }
                };
            }


            this.Start(false);
            maxPhotoId = maxId;
            minPhotoId = minId;
            IsMoreImages = _isMoreImages;
            return PhotosList;
        }

        public void GetMaxId(string substoreId, out long maxPhotoId, int mediaType)
        {

            List<PhotoInfo> objList = new List<PhotoInfo>();
            List<string> _lststr = new List<string>();
            if (!String.IsNullOrEmpty(substoreId))
                _lststr = substoreId.Split(',').ToList();
            else
                _lststr = null;

            int? ssInfo = null;
            ssInfo = _lststr == null ? ssInfo : _lststr[0].ToInt32();
            long maxId = 0;
            List<PhotoInfo> PhotosList = new List<PhotoInfo>();
            this.operation = () =>
            {
                PhotosDao access = new PhotosDao(this.Transaction);
                access.GetMaxId(ssInfo, out maxId, mediaType);

            };
            this.Start(false);
            maxPhotoId = maxId;

        }
        public bool CheckPhotos(string Photono, int PhotographerID)
        {
            PhotoInfo _objPhoto = new PhotoInfo();
            this.operation = () =>
            {
                PhotosDao access = new PhotosDao(this.Transaction);
                _objPhoto = access.GetCheckPhotos(Photono, PhotographerID);
            };
            this.Start(false);
            if (_objPhoto != null && _objPhoto.DG_Photos_pkey > 0)
            {
                return false;
            }
            else
            {
                return true;
            }

        }
        public string GetPhotoGrapherLastImageId(int photographerid)
        {
            PhotoInfo _objPhoto = new PhotoInfo();
            this.operation = () =>
            {
                PhotosDao access = new PhotosDao(this.Transaction);
                _objPhoto = access.GetLastGeneratedNumber(photographerid);
            };
            this.Start(false);
            return _objPhoto.DG_Photos_RFID;

        }

        public PhotoInfo GetNextPreviousPhoto(int PhotoId, string Flag)
        {
            try
            {
                PhotoInfo _objPhoto = new PhotoInfo();
                this.operation = () =>
                {
                    PhotosDao access = new PhotosDao(this.Transaction);
                    _objPhoto = access.GetNextPreviousPhoto(PhotoId, Flag); //TBD
                };
                this.Start(false);
                if (_objPhoto != null)
                {
                    return _objPhoto;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public List<PhotoInfo> GetArchiveImages()
        {
            List<PhotoInfo> _objPhoto = new List<PhotoInfo>();
            this.operation = () =>
            {
                PhotosDao access = new PhotosDao(this.Transaction);
                _objPhoto = access.GetArchiveImages();
            };
            this.Start(false);
            return _objPhoto;
        }

        public PhotoInfo GetPhotoNameByPhotoID(Int64 PhotoId)
        {

            string FileName = string.Empty;
            PhotoInfo _objPhoto = new PhotoInfo();
            this.operation = () =>
            {
                PhotosDao access = new PhotosDao(this.Transaction);
                _objPhoto = access.GetPhotoNameByPhotoID(PhotoId);
            };
            this.Start(false);
            return _objPhoto;
            //if (FileName != null)
            //{
            //    return FileName;
            //}
            //else
            //{
            //    return null;
            //}
        }
        public List<PhotoDetail> GetPhotoDetailsByPhotoIds(PhotoDetail photoDetailObj)
        {
            List<PhotoDetail> list = new List<PhotoDetail>();
            this.operation = () =>
            {
                PhotoAccess access = new PhotoAccess(this.Transaction);
                list = access.GetPhotoDetailsByPhotoIds(photoDetailObj);
            };
            this.Start(false);
            return list;
        }

        //public string GetPhotoRFIDByPhotoID(Int64 PhotoId)
        //{
        //    string PhotosRFID = string.Empty;
        //    this.operation = () =>
        //    {
        //        PhotosDao access = new PhotosDao(this.Transaction);
        //        PhotosRFID = access.GetPhotoRFIDByPhotoID(PhotoId);
        //    };
        //    this.Start(false);
        //    if (PhotosRFID != null)
        //    {
        //        return PhotosRFID;
        //    }
        //    else
        //    {
        //        return null;
        //    }
        //}
        public PhotoInfo GetPhotoRFIDByPhotoID(Int64 PhotoId)
        {
            PhotoInfo objInfo = new PhotoInfo();
            this.operation = () =>
            {
                PhotosDao access = new PhotosDao(this.Transaction);
                objInfo = access.GetPhotoRFIDByPhotoID(PhotoId);
            };
            this.Start(false);
            return objInfo;

            //if (objInfo != null && !String.IsNullOrEmpty(objInfo.DG_Photos_RFID))
            //{
            //    return objInfo;
            //}
            //else
            //{
            //    return null;
            //}
        }
        public List<PhotoInfo> GetPhotoRFIDByPhotoIDList(string photoIdList)
        {
            List<PhotoInfo> list = new List<PhotoInfo>();
            this.operation = () =>
            {
                PhotosDao access = new PhotosDao(this.Transaction);
                list = access.GetPhotoRFIDByPhotoIDList(photoIdList); //TBD get by DG_Photos_pkey
            };
            this.Start(false);
            return list;
        }

        public List<PhotoInfo> GetPhotosBasedonRFID(int substoreId, string RFID, bool isCrossSubstoreSearchActive)
        {
            List<string> _lstRFID = new List<string>();
            List<PhotoInfo> _objAllPhotos = new List<PhotoInfo>();

            _lstRFID = RFID.Split(',').ToList();
            this.operation = () =>
            {
                PhotosDao access = new PhotosDao(this.Transaction);
                _objAllPhotos = access.GetPhotosBasedonRFID(substoreId, RFID, isCrossSubstoreSearchActive);
            };
            this.Start(false);
            return _objAllPhotos;
        }

        public List<PhotoGraphersInfo> GetPhotoGrapher()
        {
            List<PhotoGraphersInfo> PhotosList = new List<PhotoGraphersInfo>();
            this.operation = () =>
            {
                PhotosDao access = new PhotosDao(this.Transaction);
                PhotosList = access.GetPhotoGrapher();
            };
            this.Start(false);
            return PhotosList;
        }


        public DG_PhotoGroupInfo GetGroupName(string groupname)
        {
            DG_PhotoGroupInfo _objPhoto = new DG_PhotoGroupInfo();
            this.operation = () =>
            {
                PhotosDao access = new PhotosDao(this.Transaction);
                _objPhoto = access.GetGroupName(groupname);
            };
            this.Start(false);
            return _objPhoto;

        }

        public List<string> GetQRCodeBYImages(string PhotoId)
        {
            try
            {
                List<string> list = new List<string>();
                this.operation = () =>
                {
                    IMixImageAssociationDao access = new IMixImageAssociationDao(this.Transaction);
                    list = access.GetQrOrBarCodeByPhotoID(PhotoId);
                };
                this.Start(false);
                return list;
            }
            catch
            {
                return null;
            }
        }

        public bool DeletePhotoGroupByName(string name)
        {
            bool result = false;
            this.operation = () =>
            {
                PhotosDao access = new PhotosDao(this.Transaction);
                result = access.DeletePhotoByName(name);
            };
            this.Start(false);
            return result;
        }

        public void SaveGroupData(Dictionary<string, string> _objPhotoDetails, string groupname, int SubStoreId)
        {
            DataTable udt_Group = new DataTable();
            udt_Group.Columns.Add("DG_Group_Name", typeof(string));
            udt_Group.Columns.Add("DG_Photo_ID", typeof(Int32));
            udt_Group.Columns.Add("DG_Photo_RFID", typeof(string));
            udt_Group.Columns.Add("DG_CreatedDate", typeof(DateTime));
            udt_Group.Columns.Add("DG_SubstoreId", typeof(Int32));
            foreach (var item in _objPhotoDetails)
            {
                DataRow dr = udt_Group.NewRow();
                dr["DG_Group_Name"] = groupname;
                dr["DG_Photo_ID"] = item.Key.ToInt32();
                dr["DG_Photo_RFID"] = item.Value;
                dr["DG_CreatedDate"] = (new CustomBusineses()).ServerDateTime();
                dr["DG_SubstoreId"] = SubStoreId;
                udt_Group.Rows.Add(dr);
            }
            this.operation = () =>
            {
                PhotosDao access = new PhotosDao(this.Transaction);
                access.SaveGroupData(udt_Group);
            };
            this.Start(false);
        }

        public int GetMaxPhotoIdsequence()
        {
            int result = 0; ;
            this.operation = () =>
            {
                PhotosDao access = new PhotosDao(this.Transaction);
                result = access.GetMaxPhotoIdsequence();
            };
            this.Start(false);
            return result;
        }
        public int GetMaxUserIdsequence(Int64 frmImgId, Int64 toImgId, string PhotoGrapherID)
        {
            int result = 0; ;
            this.operation = () =>
            {
                PhotosDao access = new PhotosDao(this.Transaction);
                result = access.GetMaxUserIdsequence(frmImgId, toImgId, PhotoGrapherID);
            };
            this.Start(false);
            return result;
        }



        public List<PhotoCaptureInfo> GetphotoCapturedetails(Int32 pkey)
        {
            List<PhotoCaptureInfo> PhotosList = new List<PhotoCaptureInfo>();
            this.operation = () =>
            {
                PhotoAccess access = new PhotoAccess(this.Transaction);
                PhotosList = access.GetPhotoCapturedetails(pkey);
            };
            this.Start(false);
            return PhotosList;
        }

        public bool ResetImageProcessedStatus(int PhotoId, int SubStoreId)
        {
            bool result = false;
            this.operation = () =>
            {
                PhotosDao access = new PhotosDao(this.Transaction);
                result = access.ResetImageProcessedStatus(PhotoId, SubStoreId);
            };
            this.Start(false);
            return result;
        }

        public string GetPhotoPlayerScore(string photoId)
        {
            string result = string.Empty;
            this.operation = () =>
            {
                PhotosDao access = new PhotosDao(this.Transaction);
                result = access.GetPhotoPlayerScore(photoId);
            };
            this.Start(false);
            return result;
        }

        public string GetVideoFrameCropRatio(int locationid)
        {
            string cropRatio = string.Empty;
            this.operation = () =>
            {
                PhotosDao access = new PhotosDao(this.Transaction);
                cropRatio = access.GetVideoFrameCropRatio(locationid);
            };
            this.Start(false);
            return cropRatio;

        }

        public int SaveDownloadSummary(string RFID, string photographerID)
        {

            int DG_Photos_pkey = 0;
            this.operation = () =>
            {
                PhotosDao access = new PhotosDao(this.Transaction);
                DG_Photos_pkey = access.SaveDownloadSummary(RFID, photographerID);
            };
            this.Start(false);
            return DG_Photos_pkey;
        }

        #region Added By Ajay to Get PhotoDetailsby photo Name

        public List<PhotoInfo> GetPhotoDetailsbyName(string PhotoName)
        {
            try
            {
                List<PhotoInfo> PhotosList = new List<PhotoInfo>();
                this.operation = () =>
                {
                    PhotosDao access = new PhotosDao(this.Transaction);
                    PhotosList = access.GetPhotoDetailsbyName(PhotoName);
                };
                this.Start(false);
                return PhotosList;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        #endregion

        //////changed by latika for Merge Group
        public List<GroupInfo> GetGroupList()
        {
            try
            {
                List<GroupInfo> list = new List<GroupInfo>();
                this.operation = () =>
                {
                    PhotosDao access = new PhotosDao(this.Transaction);
                    list = access.GetGroupList();
                    //  return list;
                };
                this.Start(false);
                return list;
            }
            catch
            {
                return null;
            }
        }
        public int SaveGroupMerge(string GroupNames, string NewGrpName)
        {

            int ReturnVal = 0;
            this.operation = () =>
            {
                PhotosDao access = new PhotosDao(this.Transaction);
                ReturnVal = access.SaveGroupMerge(GroupNames, NewGrpName);
            };
            this.Start(false);
            return ReturnVal;
        }
        /////////////////END
        ///

        public long GetPhotosDisplayOrder(int batchCount)
        {
            long _maxCount = 0;
            this.operation = () =>
            {
                PhotoAccess access = new PhotoAccess(this.Transaction);
                _maxCount = access.GetPhotosDisplayOrder(batchCount);
            };
            this.Start(false);
            return _maxCount;
        }

        public bool SaveManualDownloadOrderNumber(List<ManualDisplayOrder> imgList)
        {
            bool result = false;
            DataTable ManualDisplayOrder = new DataTable();
            ManualDisplayOrder.Columns.Add("PhotoNumber", typeof(Int64));
            ManualDisplayOrder.Columns.Add("FileName", typeof(string));
            ManualDisplayOrder.Columns.Add("DisplayOrder", typeof(Int64));
            
            foreach (var item in imgList)
            {
                DataRow dr = ManualDisplayOrder.NewRow();
                dr["PhotoNumber"] = item.PhotoNumber;
                dr["FileName"] = item.FileName;
                dr["DisplayOrder"] = item.DisplayOrder;
                ManualDisplayOrder.Rows.Add(dr);
            }
            this.operation = () =>
            {
                PhotosDao access = new PhotosDao(this.Transaction);
                result = access.SaveManualDownloadOrderNumber(ManualDisplayOrder);
            };
            this.Start(false);
            return result;
        }

        public List<ManualDisplayOrder> GetPhotosDisplayNumber(List<long> PhotoNumbers)
        {
            List<ManualDisplayOrder> MDOList = new List<ManualDisplayOrder>();

            DataTable ManualPhotoNumber = new DataTable();
            ManualPhotoNumber.Columns.Add("PhotoNumber", typeof(Int64));
            foreach(var item in PhotoNumbers)
            {
                DataRow dr = ManualPhotoNumber.NewRow();
                dr["PhotoNumber"] = item;
                ManualPhotoNumber.Rows.Add(dr);
            }
            this.operation = () =>
            {
                PhotoAccess access = new PhotoAccess(this.Transaction);
                MDOList = access.GetPhotosDisplayNumber(ManualPhotoNumber);
            };
            this.Start(false);
            return MDOList;
        }
        public List<PhotoInfo> GetAllStoryBookPhotoBYSTID(long id)
        {
            
            List<PhotoInfo> _objList = new List<PhotoInfo>();
            this.operation = () =>
            {
                PhotosDao access = new PhotosDao(this.Transaction);
                _objList = access.GetAllSBByStoryBookId(id);
            };
            this.Start(false);
            return _objList;
        }
    }
}
