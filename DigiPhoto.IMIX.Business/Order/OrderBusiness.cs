﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using DigiPhoto.DataLayer.Model;
using System.Xml.Linq;
using System.Data;
using System.ComponentModel;
using System.Collections;
using System.Reflection;
using System.Globalization;
using DigiPhoto.IMIX.Model;
using DigiPhoto.IMIX.DataAccess;
using DigiPhoto.IMIX.Business;
using DigiPhoto.DataLayer;
using DigiPhoto.Utility.Repository.ValueType;
using System.Management;

namespace DigiPhoto.IMIX.Business
{

    public partial class OrderBusiness : BaseBusiness
    {
        public bool IsSemiOrderImage(int OrderDetailsID)
        {
            OrderDetailInfo objectInfo = new OrderDetailInfo();
            this.operation = () =>
            {
                OrderDetailsDao access = new OrderDetailsDao(this.Transaction);
                objectInfo = access.GetSemiOrderImage(OrderDetailsID);
            };
            this.Start(false);
            if (objectInfo == null || objectInfo.DG_Orders_ID == null || objectInfo.DG_Orders_ID <= 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public OrderDetailsViewInfo GetOrderDetailsByID(int orderid)
        {
            try
            {
                OrderDetailsViewInfo _objdata = new OrderDetailsViewInfo();
                this.operation = () =>
                {
                    OrderDetailsDao access = new OrderDetailsDao(this.Transaction);
                    _objdata = access.SelectOrderDetailsByID(orderid); // get by DG_Orders_LineItems_pkey
                };
                this.Start(false);

                if (_objdata != null)
                {
                    return _objdata;
                }
                else return null;
            }
            catch (Exception ex)
            {
                //log error
                return null;
                // throw ex
            }

        }

        public int AddSemiOrderDetails(int? PhotoId, string ProductTypeId, int LocationId, string SyncCode, bool IsSentToPrinter, int SubStoreId)
        {
            try
            {
                OrderDetailInfo objInfo = new OrderDetailInfo();

                objInfo.DG_Photos_ID = PhotoId.ToString();
                objInfo.DG_Orders_Details_ProductType_pkey = Convert.ToInt32(ProductTypeId.Split(',')[0]);
                objInfo.DG_Orders_Details_LineItem_ParentID = -1;
                objInfo.DG_Orders_LineItems_Quantity = 1;
                objInfo.DG_Orders_Details_Items_TotalCost = 0;
                objInfo.DG_Orders_LineItems_DiscountAmount = 0;
                objInfo.SyncCode = SyncCode;
                objInfo.DG_Order_SubStoreId = SubStoreId;
                int orderDetailId = 0;
                this.operation = () =>
                {
                    OrderDetailsDao access = new OrderDetailsDao(this.Transaction);
                    orderDetailId = access.AddSemiOrderDetails(objInfo, LocationId, IsSentToPrinter); // get by DG_Orders_LineItems_pkey
                };
                this.Start(false);
                return orderDetailId;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }
        public int SetOrderDetails(int? PhotoId, string ProductTypeId, int SubStoreId, string SyncCode)
        {
            try
            {
                OrderDetailInfo objInfo = new OrderDetailInfo();
                CustomBusineses cusBusiness = new CustomBusineses();
                ProductBusiness prdBusiness = new ProductBusiness();

                objInfo.DG_Photos_ID = PhotoId.ToString();
                objInfo.DG_Orders_LineItems_Created = DateTime.Now;
                objInfo.DG_Orders_Details_ProductType_pkey = Convert.ToInt32(ProductTypeId.Split(',')[0]);
                objInfo.DG_Order_SubStoreId = SubStoreId;
                objInfo.DG_Orders_Details_LineItem_ParentID = -1;
                double _itemprice = prdBusiness.GetProductPricing(ProductTypeId.ToInt32());
                objInfo.DG_Orders_Details_Items_UniPrice = (decimal)_itemprice;
                objInfo.DG_Orders_LineItems_Quantity = 1;
                objInfo.DG_Orders_Details_Items_TotalCost = 0;
                objInfo.DG_Orders_Details_Items_NetPrice = (decimal)_itemprice;
                objInfo.DG_Orders_LineItems_DiscountAmount = 0;
                objInfo.SyncCode = SyncCode;
                int orderDetailId = 0;
                this.operation = () =>
                {
                    OrderDetailsDao access = new OrderDetailsDao(this.Transaction);
                    orderDetailId = access.SetOrderDetails(objInfo); // get by DG_Orders_LineItems_pkey
                };
                this.Start(false);
                return orderDetailId;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }
        public OrderInfo GenerateOrder(string OrderNumber, decimal Ordercost, decimal OrderNetCost, String PaymentDetails, int PaymentMode, double TotalDiscount, String DiscountDetails, int UserID, int CurrencyId, String OrderMode, String SyncCode, string StoreCode)
        {
            return this.GenerateOrder(OrderNumber, Ordercost, OrderNetCost, PaymentDetails, PaymentMode, TotalDiscount, DiscountDetails, UserID, CurrencyId, OrderMode, SyncCode, StoreCode,string.Empty);
        }
        public OrderInfo GenerateOrder(string OrderNumber, decimal Ordercost, decimal OrderNetCost, String PaymentDetails, int PaymentMode, double TotalDiscount, String DiscountDetails, int UserID, int CurrencyId, String OrderMode, String SyncCode, string StoreCode,string EmpId)//BY KCB ON 20 JUN 2020 to store user' employee id
        //public OrderInfo GenerateOrder(string OrderNumber, decimal Ordercost, decimal OrderNetCost, String PaymentDetails, int PaymentMode, double TotalDiscount, String DiscountDetails, int UserID, int CurrencyId, String OrderMode, String SyncCode, string StoreCode)
        {
            var order = new OrderInfo();
            CurrencyBusiness curBiz = new CurrencyBusiness();
            CustomBusineses cusBiz = new CustomBusineses();
            String ConversionRate = string.Empty;
            string PosName = getSystemName();
            OrderInfo Order = new OrderInfo();
            Order.DG_Orders_Cost = decimal.Round(Ordercost, 3, MidpointRounding.ToEven);
            Order.DG_Orders_NetCost = decimal.Round(OrderNetCost, 3, MidpointRounding.ToEven);
            Order.DG_Orders_PaymentDetails = PaymentDetails;
            Order.DG_Orders_Currency_Conversion_Rate = ConversionRate;
            Order.DG_Orders_PaymentMode = PaymentMode;
            Order.DG_Orders_Total_Discount = decimal.Round(TotalDiscount.ToDecimal(), 3, MidpointRounding.ToEven).ToDouble();
            Order.DG_Orders_Total_Discount_Details = DiscountDetails;
            Order.DG_Orders_UserID = UserID;
            Order.DG_Orders_Currency_ID = CurrencyId;
            Order.DG_Orders_Currency_Conversion_Rate = curBiz.CurrentCurrencyConversionRate();
            Order.DG_Order_Mode = OrderMode;
            Order.DG_Orders_Date = DateTime.Now;
            Order.DG_Orders_Number = OrderNumber;
            Order.SyncCode = SyncCode;
            Order.IsSynced = false;
            Order.DG_StoreCode = StoreCode;
            Order.PosName = PosName;
            Order.EmpId = EmpId;
            this.operation = () =>
            {
                OrderDetailsDao access = new OrderDetailsDao(this.Transaction);
                order = access.GenerateOrder(Order); // get by DG_Orders_LineItems_pkey
            };
            this.Start(false);

            return order;


        }
        /*///  public int SaveOrderLineItems(int ProductType, int? OrderID, String PhotoId, int Qty, string DisCountDetails, decimal TotalDiscount, decimal UnitPrice, decimal TotalPrice, decimal NetPrice, int ParentID, int SubStoreID, int IdentifierType, string UniqueIdentifier, string SyncCode, string photoIDsUnsold, double? TaxPercent = null, decimal? TaxAmount = null, bool? IsTaxIncluded = null)
          {
              try
              {
                  int orderdetailId = 0;
                  //CustomBusineses cusBiz = new CustomBusineses();
                  OrderDetailInfo Order = new OrderDetailInfo();
                  Order.DG_Orders_ID = OrderID;
                  Order.DG_Photos_ID = PhotoId;
                  Order.DG_Orders_LineItems_Quantity = Qty;
                  Order.DG_Orders_LineItems_DiscountType = DisCountDetails;
                  Order.DG_Orders_LineItems_DiscountAmount = decimal.Round(TotalDiscount, 3, MidpointRounding.ToEven);
                  Order.DG_Orders_LineItems_Created = DateTime.Now;
                  Order.DG_Orders_Details_ProductType_pkey = ProductType;
                  Order.DG_Orders_Details_Items_UniPrice = decimal.Round(UnitPrice, 3, MidpointRounding.ToEven);
                  Order.DG_Orders_Details_LineItem_ParentID = ParentID;
                  Order.DG_Orders_Details_Items_TotalCost = decimal.Round(TotalPrice, 3, MidpointRounding.ToEven);
                  Order.DG_Orders_Details_Items_NetPrice = decimal.Round(NetPrice, 3, MidpointRounding.ToEven);
                  Order.DG_Order_SubStoreId = SubStoreID;
                  Order.DG_Order_IdentifierType = IdentifierType;
                  Order.DG_Order_ImageUniqueIdentifier = UniqueIdentifier;
                  Order.SyncCode = SyncCode;
                  Order.TaxAmount = TaxAmount;
                  Order.TaxPercent = TaxPercent;
                  Order.IsTaxIncluded = IsTaxIncluded;
                  Order.DG_Photos_IDUnSold = photoIDsUnsold;


                  this.operation = () =>
                  {
                      OrderDetailsDao access = new OrderDetailsDao(this.Transaction);
                      orderdetailId = access.InsetOrderLineItems(Order);
                  };
                  this.Start(false);
                  return orderdetailId;

              }
              catch (Exception ex)
              {
                  return -1;

              }

          }*/
        public int SaveOrderLineItems(int ProductType, int? OrderID, String PhotoId, int Qty, string DisCountDetails, decimal TotalDiscount, decimal UnitPrice, decimal TotalPrice, decimal NetPrice, int ParentID, int SubStoreID, int IdentifierType, string UniqueIdentifier, string SyncCode, string EvoucherCode, string photoIDsUnsold, double? TaxPercent = null, decimal? TaxAmount = null, bool? IsTaxIncluded = null)
        {
            try
            {
                int orderdetailId = 0;
                //CustomBusineses cusBiz = new CustomBusineses();
                OrderDetailInfo Order = new OrderDetailInfo();
                Order.DG_Orders_ID = OrderID;
                Order.DG_Photos_ID = PhotoId;
                Order.DG_Orders_LineItems_Quantity = Qty;
                Order.DG_Orders_LineItems_DiscountType = DisCountDetails;
                Order.DG_Orders_LineItems_DiscountAmount = decimal.Round(TotalDiscount, 3, MidpointRounding.ToEven);
                Order.DG_Orders_LineItems_Created = DateTime.Now;
                Order.DG_Orders_Details_ProductType_pkey = ProductType;
                Order.DG_Orders_Details_Items_UniPrice = decimal.Round(UnitPrice, 3, MidpointRounding.ToEven);
                Order.DG_Orders_Details_LineItem_ParentID = ParentID;
                Order.DG_Orders_Details_Items_TotalCost = decimal.Round(TotalPrice, 3, MidpointRounding.ToEven);
                Order.DG_Orders_Details_Items_NetPrice = decimal.Round(NetPrice, 3, MidpointRounding.ToEven);
                Order.DG_Order_SubStoreId = SubStoreID;
                Order.DG_Order_IdentifierType = IdentifierType;
                Order.DG_Order_ImageUniqueIdentifier = UniqueIdentifier;
                Order.SyncCode = SyncCode;
                Order.TaxAmount = TaxAmount;
                Order.TaxPercent = TaxPercent;
                Order.IsTaxIncluded = IsTaxIncluded;
                Order.DG_Photos_IDUnSold = photoIDsUnsold;
                Order.EvoucherCode = EvoucherCode;

                this.operation = () =>
                {
                    OrderDetailsDao access = new OrderDetailsDao(this.Transaction);
                    orderdetailId = access.InsetOrderLineItems(Order);
                };
                this.Start(false);
                return orderdetailId;

            }
            catch (Exception ex)
            {
                return -1;

            }

        }

        public List<IMIX.Model.BurnOrderInfo> GetPendingBurnOrders(bool all)
        {

            List<IMIX.Model.BurnOrderInfo> OrdersList = new List<IMIX.Model.BurnOrderInfo>();
            this.operation = () =>
            {
                OrderDetailsDao access = new OrderDetailsDao(this.Transaction);
                OrdersList = access.SelectPendingBurnOrders(all); // get by DG_Orders_LineItems_pkey
            };
            this.Start(false);
            return OrdersList;
        }

        public bool UpdateBurnOrderStatus(int boId, int stat, int procBy, DateTime dateProcessed)
        {
            bool result = false;

            this.operation = () =>
            {
                OrderDetailsDao access = new OrderDetailsDao(this.Transaction);
                access.UpdateBurnOrderStatus(boId, stat);
                result = true;
            }; this.Start(false);
            return result;

        }
        public List<DigiPhoto.IMIX.Model.BurnOrderInfo> GetBODetails(int boId)
        {
            List<DigiPhoto.IMIX.Model.BurnOrderInfo> BurnOrderInfo = new List<IMIX.Model.BurnOrderInfo>();

            this.operation = () =>
            {
                OrderDetailsDao access = new OrderDetailsDao(this.Transaction);
                List<OrderDetailInfo> OrderDetailInfo = new List<IMIX.Model.OrderDetailInfo>();
                BurnOrderInfo = access.GetBODetailsByID(boId);
            }; this.Start(false);
            return BurnOrderInfo;

        }
        public bool SetOrderDetailsForReprint(int LineItemId, int substoreID)
        {

            bool result = false;
            this.operation = () =>
            {
                OrderDetailsDao access = new OrderDetailsDao(this.Transaction);
                result = access.UpdateBurnOrderStatus(LineItemId, substoreID);
            };
            this.Start(false);

            return result;
        }
        public bool SetCancelOrder(string OrderNo, string CancelReason)
        {
            try
            {
                bool result = false;
                this.operation = () =>
                {
                    OrderDetailsDao access = new OrderDetailsDao(this.Transaction);
                    result = access.UpdateCancelOrder(OrderNo, CancelReason);
                };
                this.Start(false);
                return true;
            }
            catch (Exception ex)
            {
                return false;


            }
        }

        public List<OrderDetailInfo> GetOrderDetailsforRefund(string OrderNo)
        {
            List<OrderDetailInfo> orderInfo = new List<OrderDetailInfo>();
            try
            {
                this.operation = () =>
                {
                    OrderDetailsDao access = new OrderDetailsDao(this.Transaction);
                    orderInfo = access.GetOrderDetailsforRefund(OrderNo);
                };
                this.Start(false);
                if (orderInfo != null)
                    return orderInfo.ToList();
                else
                    return null;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        //----Start ----------Hari----------20st Feb 2018-----Qr Code Implementation-------

        public List<OnlineorderImgDetail> GetOrderNumberandImg(string QrCode)
        {
            List<OnlineorderImgDetail> orderInfo = new List<OnlineorderImgDetail>();
            if (string.IsNullOrEmpty(QrCode) == true)
                return orderInfo;

            try
            {
                this.operation = () =>
                {
                    OrderDetailsDao access = new OrderDetailsDao(this.Transaction);
                    orderInfo = access.GetOrderNumberandImg(QrCode);
                };
                this.Start(false);
                if (orderInfo != null)
                    return orderInfo.ToList();
                else
                    return null;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        //---End ----------Hari----------20st Feb 2018-----Qr Code Implementation-------


        public OrderInfo GetOrder(string OrderNo)
        {
            OrderInfo orderInfo = new OrderInfo();
            this.operation = () =>
            {
                OrderDetailsDao access = new OrderDetailsDao(this.Transaction);
                orderInfo = access.GetOrder(OrderNo);
            };
            this.Start(false);
            return orderInfo;
        }

        public bool setSemiOrderImageOrderDetails(int? orderId, string imageNumber, Int32? parentId, int substorId, string discountType, decimal discountAmt, decimal totalCost, decimal netPrice)
        {
            bool result = false;
            this.operation = () =>
            {
                OrderDetailsDao access = new OrderDetailsDao(this.Transaction);
                foreach (var pitem in imageNumber.Split(','))
                {
                    if (pitem != null)
                    {
                        access.UpdateSemiOrderImageOrderDetails(orderId, Convert.ToInt32(pitem), parentId, substorId, discountType, discountAmt, totalCost, netPrice);
                    }
                }
            }; this.Start(false);
            return true;
        }

        public OrderDetailInfo GetSemiOrderImage(string photoId)
        {
            OrderDetailInfo objectInfo = new OrderDetailInfo();
            this.operation = () =>
            {
                OrderDetailsDao access = new OrderDetailsDao(this.Transaction);
                objectInfo = access.GetOrderDetailsByPhotoID(photoId);
            };
            this.Start(false);
            return objectInfo;
        }
        public bool GetSemiOrderImageforValidation(string imageNumber)
        {
            bool result = false;
            OrderDetailInfo objectInfo = new OrderDetailInfo();
            this.operation = () =>
            {
                OrderDetailsDao access = new OrderDetailsDao(this.Transaction);
                result = access.GetSemiOrderImageforValidation(imageNumber);
            };
            this.Start(false);
            return result;
        }

        public void UpdatePostedOrder(string orderNumber, int Status)
        {
            this.operation = () =>
            {
                OrderDetailsDao access = new OrderDetailsDao(this.Transaction);
                access.UpdatePostedOrder(Status, orderNumber);
            }; this.Start(false);
        }

        public string GetOrderDateByOrderNo(string OrderNo)
        {
            OrderInfo OrderInfo = new OrderInfo();
            string y = string.Empty;
            this.operation = () =>
            {
                OrderDetailsDao access = new OrderDetailsDao(this.Transaction);
                OrderInfo = access.GetOrdersNumber(OrderNo);
                y = Convert.ToDateTime(OrderInfo.DG_Orders_Date).ToString("ddMMyyyy");

            }; this.Start(false);
            return y;
        }

        public List<OrderDetailInfo> GetPhotoToUpload()
        {
            List<OrderDetailInfo> orderInfo = new List<OrderDetailInfo>();
            this.operation = () =>
            {
                OrderDetailsDao access = new OrderDetailsDao(this.Transaction);
                orderInfo = access.GetPhotoToUpload();
            };
            this.Start(false);
            if (orderInfo != null)
                return orderInfo.ToList();
            else
                return null;
        }
        public OrderReceiptReprintInfo GetOrderDetailForReceipt(int OrderId)
        {
            OrderReceiptReprintInfo order = null;
            this.operation = () =>
            {
                OrderAccess access = new OrderAccess(this.Transaction);
                order = access.GetOrderDetailForReceipt((OrderId));
            };
            this.Start(false);
            return order;
        }
       
        public bool chKIsWaterMarkedOrNot(int PackageID)
        {
            try
            {
                bool result = false;
                this.operation = () =>
                {
                    OrderDetailsDao access = new OrderDetailsDao(this.Transaction);
                    result = access.chKIsWaterMarkedOrNot(PackageID);
                };
                this.Start(false);
                return result;

            }
            catch
            {
                return false;
            }
        }
        public string getSystemName()
        {
            try
            {
                object getSystemName = "";


                ManagementObjectSearcher searcher =
                        new ManagementObjectSearcher("root\\CIMV2",
                        "SELECT Name FROM Win32_ComputerSystem");
                foreach (ManagementObject queryObj in searcher.Get())
                {
                    return queryObj["Name"].ToString();
                }
                return "";
            }
            catch (Exception ex)
            {
                return "";
            }
        }

        #region Product Naming EN/CN Added By Suraj Mali.
        /// <summary>
        ///  Added By Suraj Mali. For getting Product EN/CN Name
        /// </summary>
        /// <param name="OrderNo"></param>
        /// <returns></returns>
        public List<ProductNameInfo> GetProductNameDetails(string ProductNameEN)
        {
            List<ProductNameInfo> productInfo = new List<ProductNameInfo>();
            try
            {
                this.operation = () =>
                {
                    OrderDetailsDao access = new OrderDetailsDao(this.Transaction);
                    productInfo = access.GetProductNameDetails(ProductNameEN);
                };
                this.Start(false);
                if (productInfo != null)
                    return productInfo.ToList();
                else
                    return null;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public string GetSiteNameDetails(string SiteNameEN)
        {
            string SiteNameCN = string.Empty;
            try
            {
                this.operation = () =>
                {
                    OrderDetailsDao access = new OrderDetailsDao(this.Transaction);
                    SiteNameCN = access.GetSiteNameDetails(SiteNameEN);
                };
                this.Start(false);
                if (string.IsNullOrEmpty(SiteNameCN))
                    return null;
                else
                    return SiteNameCN;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        #endregion
    }
}
