﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using System.Xml.Linq;
using System.Data;
using System.ComponentModel;
using System.Collections;
using System.Reflection;
using System.Globalization;
using DigiPhoto.IMIX.Model;
using DigiPhoto.IMIX.DataAccess;
using DigiPhoto.IMIX.Business;
using DigiPhoto.DataLayer;

namespace DigiPhoto.IMIX.Business
{
    public partial class BackupHistoryBusniess : BaseBusiness
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="SubstoreId"></param>
        /// <param name="dfrom"></param>
        /// <param name="dto"></param>
        /// <returns></returns>
        public List<BackupHistory> GetBackupHistoryData(int SubstoreId, DateTime dfrom, DateTime dto)
        {
            

            List<BackupHistory> BackupHistory = new List<BackupHistory>();
            try
            {
                
                this.operation = () =>
                {
                    BackupHistoryDao access = new BackupHistoryDao(this.Transaction);
                     BackupHistory = access.GetBackupHistoryData(SubstoreId, dfrom, dto);
                 };
                this.Start(false);
                return BackupHistory;
            }
            catch
            {
                return null;
            }
        }
        public bool SaveScheduledConfig(int substoreId, DateTime backupDateTime, int status)
        {
            try
            {
                BackupHistory objbackup = new BackupHistory();
                
                this.operation = () =>
                  {
                      BackupHistoryDao access = new BackupHistoryDao(this.Transaction);
                      access.UpdAndInsScheduledConfig(substoreId, status, backupDateTime);
                  };
                this.Start(false);
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

    }
}
