﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using DigiPhoto.DataLayer.Model;
using System.Xml.Linq;
using System.Data;
using System.ComponentModel;
using System.Collections;
using System.Reflection;
using System.Globalization;
using DigiPhoto.IMIX.Model;
using DigiPhoto.IMIX.DataAccess;
using DigiPhoto.IMIX.Business;
using DigiPhoto.DataLayer;
using DigiPhoto.Cache.Repository;
using DigiPhoto.Cache.DataCache;
using DigiPhoto.Cache.MasterDataCache;

namespace DigiPhoto.IMIX.Business
{

    public class GroupBusiness : BaseBusiness
    {
        public List<GroupDetails> GetGroupDetails(int GroupPkey)
        {
            try
            {
                List<GroupDetails> objList = new List<GroupDetails>();
                this.operation = () =>
                {
                    GroupDao access = new GroupDao(this.Transaction);
                    objList = access.GetGroupDetails(GroupPkey);
                };
                this.Start(false);
                return objList;
            }
            catch (Exception ex)
            {
                //throw ex;
                return null;
            }
        }
        public bool SetGroupDetails(GroupDetails groupInfo)
        {
            bool success = false;
            this.operation = () =>
            {
                GroupDao access = new GroupDao(this.Transaction);
                success = access.AddGroup(groupInfo);
            };
            this.Start(false);
            return success;
            //ICacheRepository imixCache = DataCacheFactory.GetFactory<ICacheRepository>(typeof(SceneCache).FullName);
            //imixCache.RemoveFromCache();

        }

        public List<GroupDetails> GetGroupproductDetails(int GroupPkey)
        {
            try
            {
                List<GroupDetails> objList = new List<GroupDetails>();
                this.operation = () =>
                {
                    GroupDao access = new GroupDao(this.Transaction);
                    objList = access.GetGroupProductDetails(GroupPkey);
                };
                this.Start(false);
                return objList;
            }
            catch (Exception ex)
            {
                //throw ex;
                return null;
            }
        }

        public bool SetGroupProductDetails(GroupDetails groupInfo)
        {
            bool success = false;
            this.operation = () =>
            {
                GroupDao access = new GroupDao(this.Transaction);
                success = access.AddGroupProduct(groupInfo);
            };
            this.Start(false);
            return success;
            //ICacheRepository imixCache = DataCacheFactory.GetFactory<ICacheRepository>(typeof(SceneCache).FullName);
            //imixCache.RemoveFromCache();

        }

    }
}

