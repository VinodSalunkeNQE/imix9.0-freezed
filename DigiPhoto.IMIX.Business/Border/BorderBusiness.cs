﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using System.Xml.Linq;
using System.Data;
using System.ComponentModel;
using System.Collections;
using System.Reflection;
using System.Globalization;
using DigiPhoto.IMIX.Model;
using DigiPhoto.IMIX.DataAccess;
using DigiPhoto.IMIX.Business;
using DigiPhoto.DataLayer;

namespace DigiPhoto.IMIX.Business
{

    public partial class BorderBusiness : BaseBusiness
    {
        public List<BorderInfo> GetBorderDetails()
        {

            List<BorderInfo> _objList = new List<BorderInfo>();
            this.operation = () =>
            {
                BorderDao access = new BorderDao(this.Transaction);
                _objList = access.Select(null);
            };
            this.Start(false);
            return _objList;
        }
        public bool DeleteBorder(int borderId)
        {
            bool result = false;
            this.operation = () =>
            {
                BorderDao access = new BorderDao(this.Transaction);
                result = access.Delete(borderId);
            };
            this.Start(false);
            return result;
        }
        public void SetBorderDetails(string Bordername, int productType, bool isactive, int borderid, string SyncCode,int userId)
        {
            BorderInfo _objnew = new BorderInfo();
            _objnew.DG_Border = Bordername;
            _objnew.DG_ProductTypeID = productType;
            _objnew.DG_IsActive = isactive;
            _objnew.SyncCode = SyncCode;
            _objnew.IsSynced = false;
            _objnew.DG_Borders_pkey = borderid;
           
            if (borderid <= 0)
            {
                _objnew.CreatedBy = userId;
                this.operation = () =>
                {
                    BorderDao access = new BorderDao(this.Transaction);
                    access.Add(_objnew);
                };
                this.Start(false);
            }
            else
            {
                _objnew.ModifiedBy = userId;
                this.operation = () =>
                {
                    BorderDao access = new BorderDao(this.Transaction);
                    access.Update(_objnew);
                };
                this.Start(false);
            }

        }

        public BorderInfo GetBorderNameFromID(int id)
        {

            BorderInfo _objList = new BorderInfo();
            this.operation = () =>
            {
                BorderDao access = new BorderDao(this.Transaction);
                _objList = access.Get(id);
            };
            this.Start(false);
            return _objList;
        }

        public List<VideoOverlay> GetVideoOverlays()
        {

            List<VideoOverlay> _objList = new List<VideoOverlay>();
            this.operation = () =>
            {
                BorderDao access = new BorderDao(this.Transaction);
                _objList = access.SelectVideoOverlay(null);
            };
            this.Start(false);
            return _objList;
        }
      
    }
}
