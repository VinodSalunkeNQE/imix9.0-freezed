﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DigiPhoto.IMIX.DataAccess;
using DigiPhoto.IMIX.Model;
using System.Collections.ObjectModel;

namespace DigiPhoto.IMIX.Business
{
  public  class WhatsAppBusiness : BaseBusiness
    {

        public ObservableCollection<string> getCountryList()
        {
            ObservableCollection<string> CountryList = null;
            this.operation = () =>
            {
                WhatsAppAccess access = new WhatsAppAccess(this.Transaction);
                CountryList = access.getCountryList();
            };
            this.Start(false);

            return CountryList;
        }

        public ObservableCollection<string> getGlobalCountryList()
        {
            ObservableCollection<string> CountryList = null;
            this.operation = () =>
            {
                WhatsAppAccess access = new WhatsAppAccess(this.Transaction);
                CountryList = access.getGlobalCountryList();
            };
            this.Start(false);

            return CountryList;
        }

        public int getDefaultGlobalCountry()
        {
            int countryID = 0;
            this.operation = () =>
            {
                WhatsAppAccess access = new WhatsAppAccess(this.Transaction);
                countryID = access.getDefaultGlobalCountry();
            };
            this.Start(false);

            
            return countryID;
        }




        //public bool SaveTaxDetails(int StoreId, int TaxId, float TaxPercentage, bool Status , DateTime ModifiedDate)
        public bool SaveWhatsAppDetails(string MobileNumber, string CountryName, string APIkey, string HostUrl)
      {
          bool issuccess = false;
          this.operation = () =>
              {
                  WhatsAppAccess access = new WhatsAppAccess(this.Transaction);
                  issuccess = access.SaveWhatsAppSettings(MobileNumber, CountryName, APIkey, HostUrl);
              };
          this.Start(false);

          return issuccess;
      }


        public bool SaveWhatsAppOrders(string Order_Number, string fileName, string countryName,
            string CountryMobileCode, string Guest_MobileNumber,
            string WhtsApp_Image_SourcePath, string WhtsApp_URL_SourcePath)
        {
            bool issuccess = false;
            this.operation = () =>
            {
                WhatsAppAccess access = new WhatsAppAccess(this.Transaction);
                issuccess = access.SaveWhatsAppOrders(Order_Number, fileName, countryName, CountryMobileCode,
                    Guest_MobileNumber, WhtsApp_Image_SourcePath, WhtsApp_URL_SourcePath);
            };
            this.Start(false);

            return issuccess;
        }

        public WhatsAppSettings GetWhatsAppDetail()
        {
            WhatsAppSettings wtsAppObj = new WhatsAppSettings();

            this.Operation = () =>
                {
                    WhatsAppAccess access = new WhatsAppAccess(this.Transaction);
                    wtsAppObj = access.GetWhatsAppDetail();
                };
            this.Start(false);
            return wtsAppObj;
        }




        public List<WhatsAppSettingsTracking> GetWhatsAppExactStatus()
        {
            List<WhatsAppSettingsTracking> objList = new List<WhatsAppSettingsTracking>();
            this.Operation = () =>
            {
                WhatsAppAccess access = new WhatsAppAccess(this.Transaction);
                objList = access.GetWhatsAppExactStatus();
            };
            this.Start(false);
            return objList;
        }

        public List<WhatsAppSettingsTracking> GetWhtsAppOrderStatus(DateTime? FrmDate, DateTime? ToDate, string QRCode, string MobileNum,int NewOld)
        {

            List<WhatsAppSettingsTracking> objLst = new List<WhatsAppSettingsTracking>();
            this.operation = () =>
            {
                WhatsAppAccess access = new WhatsAppAccess(this.Transaction);
                objLst = access.GetWhatsAppStatus(FrmDate, ToDate, QRCode, MobileNum,NewOld);
            };
            this.Start(false);
            return objLst;

        }

        public bool UpdateWtsAppOrder(long wtsAPPID,int status)
        {
            bool wtsAppObj = false;
            this.Operation = () =>
            {
                WhatsAppAccess access = new WhatsAppAccess(this.Transaction);
                wtsAppObj = access.UpdateWhatsAppOrder(wtsAPPID, status);
            };
            this.Start(false);
            return wtsAppObj;
        }


        public bool ResyncWtsAppOrder(string Order_Number)
        {
            bool wtsAppObj = false;
            this.Operation = () =>
            {
                WhatsAppAccess access = new WhatsAppAccess(this.Transaction);
                wtsAppObj = access.UpdateWhatsAppOrderToReSend(Order_Number);
            };
            this.Start(false);
            return wtsAppObj;
        }


        public bool UpdateWtsAppOrderStatus(string WhtsApp_URL_SourcePath, int status,
            string creation_date, string process_date,string failed_date,string custom_data)
        {
            bool wtsAppObj = false;
            this.Operation = () =>
            {
                WhatsAppAccess access = new WhatsAppAccess(this.Transaction);
                wtsAppObj = access.UpdateWhatsAppOrderStatus(WhtsApp_URL_SourcePath, status,
                    creation_date, process_date,failed_date,custom_data);
            };
            this.Start(false);
            return wtsAppObj;
        }

        public List<WhatsAppSettingsTracking> GetWhatsAppOrders()
        {
            List<WhatsAppSettingsTracking> objList = new List<WhatsAppSettingsTracking>();
            this.Operation = () =>
            {
                WhatsAppAccess access = new WhatsAppAccess(this.Transaction);
                objList = access.GetWhatsAppOrders();
            };
            this.Start(false);
            return objList;
        }


        public List<TaxDetailInfo> GetTaxDetail(int? orderId)
        {
            List<TaxDetailInfo> lstTaxInfo = new List<TaxDetailInfo>();

            this.operation = () =>
            {
                TaxAccess access = new TaxAccess(this.Transaction);
                lstTaxInfo = access.GetTaxDetail(orderId);
            };
            this.Start(false);

            return lstTaxInfo;
        }
        public List<TaxDetailInfo> GetReportTaxDetail(DateTime FromDate, DateTime ToDate, int subStoreID)
        {
            List<TaxDetailInfo> lstTaxInfo = new List<TaxDetailInfo>();

            this.operation = () =>
            {
                TaxAccess access = new TaxAccess(this.Transaction);
                lstTaxInfo = access.GetReportTaxDetail(FromDate, ToDate, subStoreID);
            };
            this.Start(false);
            return lstTaxInfo;
        }

        public bool UpdateStoreTaxData(StoreInfo store)

        {
            bool issucess = false;
            this.operation = () =>
            {
                TaxAccess access = new TaxAccess(this.Transaction);
                issucess = access.UpdateStoreTaxData(store);
            };
            this.Start(false);
            return issucess;
        }
        public StoreInfo getTaxConfigData()
        {
            StoreInfo storeInfo = new StoreInfo();
            this.operation = () =>
            {
                TaxAccess access = new TaxAccess(this.Transaction);
                storeInfo = access.getTaxConfigData();
            };
            this.Start(false);
            return storeInfo;
        }
        public List<TaxDetailInfo> GetApplicableTaxes(int taxID)
        {
            List<TaxDetailInfo> lstTaxInfo = new List<TaxDetailInfo>();

            this.operation = () =>
            {
                TaxAccess access = new TaxAccess(this.Transaction);
                lstTaxInfo = access.GetApplicableTaxes(taxID);
            };
            this.Start(false);
            return lstTaxInfo;
        }

    }
}
