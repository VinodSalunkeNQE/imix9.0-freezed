﻿using DigiPhoto.IMIX.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DigiPhoto.IMIX.Business
{
    public class AuditLogBusiness : BaseBusiness
    {
        public void InsertAuditLog(string uniqueId, int threadId, DateTime startTime, DateTime endTime, string fileName)
        {
            this.operation = () =>
            {
                AuditLogDataAccess access = new AuditLogDataAccess(this.Transaction);
                access.InsertAuditLog(uniqueId, threadId, startTime, endTime, fileName);
            };
            this.Start(false);
        }

        public void InsertExceptionLog(string uniqueId, int threadId, DateTime startTime, DateTime endTime, string fileName, long PhotoId, Exception excption = null, String Message = null)
        {
            try
            {
                this.operation = () =>
                {
                    AuditLogDataAccess access = new AuditLogDataAccess(this.Transaction);
                    access.InsertExceptionLog(uniqueId, threadId, startTime, endTime, fileName, PhotoId, excption, Message);
                };
                this.Start(false);
            }
            catch(Exception ex)
            {
                ErrorHandler.ErrorHandler.LogFileWrite("35 : InsertExceptionLog : exMessage : "+ex.Message+" : StackTrace  : "+ex.StackTrace);
            }
        }
    }
}
