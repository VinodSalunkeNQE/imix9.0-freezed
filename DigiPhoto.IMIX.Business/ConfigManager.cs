﻿using DigiPhoto.IMIX.DataAccess;
using DigiPhoto.IMIX.Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Security.Cryptography;
using System.Text;
using DigiPhoto.Utility.Repository.ValueType;


namespace DigiPhoto.IMIX.Business
{
    public static class ConfigManager
    {
        private static Dictionary<long, string> iMIXConfigurations = new Dictionary<long, string>();
        public static Dictionary<long, string> IMIXConfigurations
        {
            get
            {
                if (iMIXConfigurations.Count == 0)
                {
                    ConfigBusiness business = new ConfigBusiness();
                    iMIXConfigurations = business.GetConfigurations(iMIXConfigurations, SubStoreId);
                }
                return iMIXConfigurations;
            }
            set
            {
                iMIXConfigurations = value;
            }
        }

        
        private static int subStoreId;
        public static int SubStoreId
        {
            get
            {
                //if (subStoreId == 0)
                {
                    string pathtosave = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
                    if (File.Exists(pathtosave + "\\ss.dat"))
                    {
                        string line;
                        string SbStoreId;
                        using (StreamReader reader = new StreamReader(pathtosave + "\\ss.dat"))
                        {
                            line = reader.ReadLine();
                            SbStoreId = CryptorEngine.Decrypt(line, true);
                            subStoreId = SbStoreId.Split(',')[0].ToInt32();
                            if (subStoreId == 0)
                                throw new Exception("Please configure your sub-store.");
                        }
                    }
                }
                return subStoreId;
            }
            set
            {
                subStoreId = value;
            }
        }

        private static string digiFolderPath;
        public static string DigiFolderPath
        {
            get
            {
                ConfigBusiness business = new ConfigBusiness();
                digiFolderPath = business.GetHotFolderPath(SubStoreId);                       
                return digiFolderPath;
            }
            set
            {
                digiFolderPath = value;
            }
        }
    }

    public class CryptorEngine
    {
        /// <summary>
        /// Encrypt a string using dual encryption method. Return a encrypted cipher Text
        /// </summary>
        /// <param name="toEncrypt">string to be encrypted</param>
        /// <param name="useHashing">use hashing? send to for extra secirity</param>
        /// <returns></returns>
        public static string Encrypt(string toEncrypt, bool useHashing)
        {
            byte[] keyArray;
            byte[] toEncryptArray = UTF8Encoding.UTF8.GetBytes(toEncrypt);

            // System.Configuration.AppSettingsReader settingsReader = new AppSettingsReader();
            // Get the key from config file
            // string key = (string)settingsReader.GetValue("SecurityKey", typeof(String));
            string key = "mykey";
            //System.Windows.Forms.MessageBox.Show(key);
            if (useHashing)
            {
                MD5CryptoServiceProvider hashmd5 = new MD5CryptoServiceProvider();
                keyArray = hashmd5.ComputeHash(UTF8Encoding.UTF8.GetBytes(key));
                hashmd5.Clear();
            }
            else
                keyArray = UTF8Encoding.UTF8.GetBytes(key);

            TripleDESCryptoServiceProvider tdes = new TripleDESCryptoServiceProvider();
            tdes.Key = keyArray;
            tdes.Mode = CipherMode.ECB;
            tdes.Padding = PaddingMode.PKCS7;

            ICryptoTransform cTransform = tdes.CreateEncryptor();
            byte[] resultArray = cTransform.TransformFinalBlock(toEncryptArray, 0, toEncryptArray.Length);
            tdes.Clear();
            return Convert.ToBase64String(resultArray, 0, resultArray.Length);
        }
        /// <summary>
        /// DeCrypt a string using dual encryption method. Return a DeCrypted clear string
        /// </summary>
        /// <param name="cipherString">encrypted string</param>
        /// <param name="useHashing">Did you use hashing to encrypt this data? pass true is yes</param>
        /// <returns></returns>
        public static string Decrypt(string cipherString, bool useHashing)
        {
            byte[] keyArray;
            byte[] toEncryptArray = Convert.FromBase64String(cipherString);

            // System.Configuration.AppSettingsReader settingsReader = new AppSettingsReader();
            //Get your key from config file to open the lock!
            //string key = (string)settingsReader.GetValue("SecurityKey", typeof(String));
            string key = "mykey";
            if (useHashing)
            {
                MD5CryptoServiceProvider hashmd5 = new MD5CryptoServiceProvider();
                keyArray = hashmd5.ComputeHash(UTF8Encoding.UTF8.GetBytes(key));
                hashmd5.Clear();
            }
            else
                keyArray = UTF8Encoding.UTF8.GetBytes(key);

            TripleDESCryptoServiceProvider tdes = new TripleDESCryptoServiceProvider();
            tdes.Key = keyArray;
            tdes.Mode = CipherMode.ECB;
            tdes.Padding = PaddingMode.PKCS7;

            ICryptoTransform cTransform = tdes.CreateDecryptor();
            byte[] resultArray = cTransform.TransformFinalBlock(toEncryptArray, 0, toEncryptArray.Length);

            tdes.Clear();
            return UTF8Encoding.UTF8.GetString(resultArray);
        }
    }
}
