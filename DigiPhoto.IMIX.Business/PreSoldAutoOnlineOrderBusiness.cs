﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using System.Xml.Linq;
using System.Data;
using System.ComponentModel;
using System.Collections;
using System.Reflection;
using System.Globalization;
using DigiPhoto.IMIX.Model;
using DigiPhoto.IMIX.DataAccess;
using DigiPhoto.IMIX.Business;
using DigiPhoto.DataLayer;


namespace DigiPhoto.IMIX.Business
{
    public class  PreSoldAutoOnlineOrderBusiness : BaseBusiness
    {


        public List<PreSoldAutoOnlineOrderInfo> getAutoOnlineOrderDetails()
        {
           
                List<PreSoldAutoOnlineOrderInfo> list = new List<PreSoldAutoOnlineOrderInfo>();
                this.operation = () =>
                {
                    PreSoldAutoOnlineOrderDao access = new PreSoldAutoOnlineOrderDao(this.Transaction);
                    list = access.GetAutoOnlineOrder();
                };
                this.Start(false);
                return list;
        }

        public bool UpdateOrderStatus(string IMIXImageAssociationIds, bool IsWaterMarked)
        {
            try
            {
                this.operation = () =>
                {
                    PreSoldAutoOnlineOrderDao access = new PreSoldAutoOnlineOrderDao(this.Transaction);
                    access.UpdateOrderStatus(IMIXImageAssociationIds, IsWaterMarked);
                };
                this.Start(false);
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool chKIsAutoPurchaseActiveOrNot(int SubStoreId)
        {
            try
            {
                bool result = false;
                this.operation = () =>
                {
                    PreSoldAutoOnlineOrderDao access = new PreSoldAutoOnlineOrderDao(this.Transaction);
                    result=access.chKIsAutoPurchaseActiveOrNot(SubStoreId);
                };
                this.Start(false);
                return result;

            }
            catch
            {
                return false;
            }
        }

        public bool getOrderStatus(PreSoldAutoOnlineOrderInfo cardNumber, string IMIXImageAssociationIds, string photoId, bool IsWaterMarked)
        {
            try
            {
                bool result = false;
                this.operation = () =>
                {
                    PreSoldAutoOnlineOrderDao access = new PreSoldAutoOnlineOrderDao(this.Transaction);
                    result = access.getOrderStatus(cardNumber, IMIXImageAssociationIds, photoId, IsWaterMarked);
                };
                this.Start(false);
                return result;
            }
            catch
            {
                return false;
            }
        }



    }
}

