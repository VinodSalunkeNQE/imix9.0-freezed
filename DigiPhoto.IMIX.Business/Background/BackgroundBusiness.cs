﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using DigiPhoto.DataLayer.Model;
using System.Xml.Linq;
using System.Data;
using System.ComponentModel;
using System.Collections;
using System.Reflection;
using System.Globalization;
using DigiPhoto.IMIX.Model;
using DigiPhoto.IMIX.DataAccess;
using DigiPhoto.IMIX.Business;
using DigiPhoto.DataLayer;

namespace DigiPhoto.IMIX.Business
{

    public class BackgroundBusiness : BaseBusiness
    {
        public List<BackGroundInfo> GetBackgoundDetails()
        {
            List<BackGroundInfo> objectList = new List<BackGroundInfo>();
            this.operation = () =>
            {
                BackGroundDao access = new BackGroundDao(this.Transaction);
                objectList = access.GetBackgroundByGroup();
            };
            this.Start(false);
            return objectList;
        }

        public List<BackGroundInfo> GetBackgoundDetails(int productId)
        {

            List<BackGroundInfo> objList = new List<BackGroundInfo>();
            this.operation = () =>
            {
                BackGroundDao access = new BackGroundDao(this.Transaction);
                objList = access.Select(null, productId, 0, string.Empty);
            };
            this.Start(false);
            return objList;

        }
        public List<BackGroundInfo> GetBackgoundDetailsALL()
        {
            List<BackGroundInfo> objList = new List<BackGroundInfo>();
            this.operation = () =>
            {
                BackGroundDao access = new BackGroundDao(this.Transaction);
                objList = access.Select(null, 0, 0, string.Empty);
            };
            this.Start(false);
            return objList;

        }
        public int GetProductTypeforBackgorund(string bakgroundName)
        {
            BackGroundInfo obj = new BackGroundInfo();
            this.operation = () =>
            {
                BackGroundDao access = new BackGroundDao(this.Transaction);
                obj = access.Get(null, 0, 0, bakgroundName);
            };
            this.Start(false);
            if (obj != null)
                return obj.DG_Product_Id;
            else
                return 0;
        }
        public int SetBackGroundDetails(int productid, string backgroundname, string backgrounddisplayname, string SyncCode, bool isActive,int loggedinUser,bool isBackgroundPanorama)
        {
            BackGroundInfo _objnew = new BackGroundInfo();
            _objnew.DG_BackGround_Image_Name = backgroundname;
            _objnew.DG_BackGround_Image_Display_Name = backgrounddisplayname;
            _objnew.DG_Product_Id = productid;
            _objnew.SyncCode = SyncCode;
            _objnew.IsSynced = false;
            _objnew.DG_Background_IsActive = isActive;
            _objnew.DG_Background_IsPanorama = isBackgroundPanorama;
            _objnew.CreatedBy = loggedinUser;
            this.operation = () =>
            {
                BackGroundDao access = new BackGroundDao(this.Transaction);
                _objnew.DG_Background_pkey = access.Add(_objnew);
            };
            this.Start(false);
            return _objnew.DG_Background_pkey;
        }
        public void SetBackGroundDetails(int productid, string backgroundname, string backgrounddisplayname, BackGroundInfo backGroundInfo, string SyncCode, bool? isActive,int loggedinuser)
        {
            BackGroundInfo _objnew = new BackGroundInfo();
            this.operation = () =>
            {
                BackGroundDao access = new BackGroundDao(this.Transaction);
                _objnew = access.Get(backGroundInfo.DG_Background_pkey, productid, 0, string.Empty);
            };
            this.Start(false);
            if (_objnew != null)
            {
                _objnew.DG_BackGround_Image_Name = backgroundname;
                _objnew.DG_BackGround_Image_Display_Name = backgrounddisplayname;
                _objnew.DG_Product_Id = productid;
                _objnew.DG_BackGround_Group_Id = backGroundInfo.DG_Background_pkey;
                _objnew.IsSynced = false;
                _objnew.DG_Background_IsActive = isActive;
                _objnew.ModifiedBy = loggedinuser;
                _objnew.DG_Background_IsPanorama = backGroundInfo.DG_Background_IsPanorama;
                if (_objnew.DG_Background_pkey > 0)
                {
                    this.operation = () =>
                    {
                        BackGroundDao access = new BackGroundDao(this.Transaction);
                        access.Update(_objnew);
                    };
                    this.Start(false);
                }
                else
                {
                    this.operation = () =>
                    {
                        _objnew.SyncCode = SyncCode;
                        BackGroundDao access = new BackGroundDao(this.Transaction);
                        access.Add(_objnew);
                    };
                    this.Start(false);
                }
        }
        }
        public string GetBackGroundFileName(int productId, int BgGroupId)
        {
            try
            {
                BackGroundInfo _objbackGround = new BackGroundInfo();
                this.operation = () =>
                {
                    BackGroundDao access = new BackGroundDao(this.Transaction);
                    _objbackGround = access.Get(BgGroupId, productId, 0, string.Empty);
                };
                this.Start(false);
                if (_objbackGround != null)
                {
                    return _objbackGround.DG_BackGround_Image_Name;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                return null;
            }
        }
        public bool DeleteBackGround(int bgID)
        {
            try
            {
                bool result = false;
                this.operation = () =>
                {
                    BackGroundDao access = new BackGroundDao(this.Transaction);
                    result = access.Delete(bgID);
                };
                this.Start(false);
                return result;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public void UpdateBackgroundDetails(int productid, string syncCode, BackGroundInfo backgroundInfo)
        {
            BackGroundInfo _objnew = new BackGroundInfo();
            this.operation = () =>
            {
                BackGroundDao access = new BackGroundDao(this.Transaction);
                _objnew = access.Get(backgroundInfo.DG_Background_pkey, productid, 0, string.Empty);
            };
            this.Start(false);
            if (_objnew != null)
            {
                _objnew.DG_BackGround_Image_Name = backgroundInfo.DG_BackGround_Image_Name;
                _objnew.DG_BackGround_Image_Display_Name = backgroundInfo.DG_BackGround_Image_Display_Name;
                _objnew.DG_Product_Id = productid;
                _objnew.DG_BackGround_Group_Id = backgroundInfo.DG_BackGround_Group_Id;
                _objnew.IsSynced = backgroundInfo.IsSynced;
                _objnew.DG_Background_IsActive = backgroundInfo.DG_Background_IsActive;
                _objnew.ModifiedBy = backgroundInfo.ModifiedBy;
                this.operation = () =>
                    {
                        BackGroundDao access = new BackGroundDao(this.Transaction);
                        access.Update(_objnew);
                    };
                    this.Start(false);
            }
        }


    }
}
