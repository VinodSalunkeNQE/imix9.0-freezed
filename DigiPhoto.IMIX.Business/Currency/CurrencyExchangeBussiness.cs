﻿using DigiPhoto.IMIX.DataAccess;
using DigiPhoto.IMIX.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace DigiPhoto.IMIX.Business
{
    public class CurrencyExchangeBussiness : BaseBusiness
    {
        public List<CurrencyExchangeinfo> GetCurrencyProfileList()
        {
            try
            {
                List<CurrencyExchangeinfo> list = new List<CurrencyExchangeinfo>();
                this.operation = () =>
                {
                    CurrencyExchangeAccess access = new CurrencyExchangeAccess(this.Transaction);
                    list = access.GetProfileList();
                    //  return list;
                };
                this.Start(false);
                return list;
            }
            catch
            {
                return null;
            }
        }


        public List<RateDetailInfo> GetProfilerateDetailList(long profileAuditID)
        {
            try
            {
                List<RateDetailInfo> list = new List<RateDetailInfo>();
                this.operation = () =>
                {
                    CurrencyExchangeAccess access = new CurrencyExchangeAccess(this.Transaction);
                    list = access.GetProfilerateDetailList(profileAuditID);
                    //  return list;
                };
                this.Start(false);
                return list;
            }
            catch
            {
                return null;
            }
        }

        public bool UploadCurrencyData(long CreatedBy, DateTime CreatedOn, DataTable dtCurrencyExchange)
        {
            try
            {
                bool ret = false;
                this.operation = () =>
                {
                    CurrencyExchangeAccess access = new CurrencyExchangeAccess(this.Transaction);
                    ret = access.UploadCurrencyData(CreatedBy, CreatedOn, dtCurrencyExchange);
                    //  return list;
                };
                this.Start(false);
                return ret;
            }
            catch
            {
                return false;
            }
        }


        public bool UpdateInsertProfile(long CreatedBy)
        {
            try
            {
                bool ret = false;
                this.operation = () =>
                {
                    CurrencyExchangeAccess access = new CurrencyExchangeAccess(this.Transaction);
                    ret = access.UpdateInsertProfile(CreatedBy);
                    // return list;
                };
                this.Start(false);
                return ret;
            }
            catch
            {
                return false;
            }
        }

        
    }
}
