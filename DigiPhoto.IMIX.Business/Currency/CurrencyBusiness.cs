﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using DigiPhoto.DataLayer.Model;
using System.Xml.Linq;
using System.Data;
using System.ComponentModel;
using System.Collections;
using System.Reflection;
using System.Globalization;
using DigiPhoto.IMIX.Model;
using DigiPhoto.IMIX.DataAccess;
using DigiPhoto.IMIX.Business;
using DigiPhoto.DataLayer;

namespace DigiPhoto.IMIX.Business
{

    public class CurrencyBusiness : BaseBusiness
    {
        public List<CurrencyInfo> GetCurrencyList()
        {
            List<CurrencyInfo> objectList = new List<CurrencyInfo>();
            this.operation = () =>
            {
                CurrencyDao access = new CurrencyDao(this.Transaction);
                objectList = access.Select(0, null);
            };
            this.Start(false);

            List<CurrencyInfo> _templist = new List<CurrencyInfo>();
            CurrencyInfo _obj = new CurrencyInfo();
            _obj.DG_Currency_Symbol = "--Select--";
            _obj.DG_Currency_pkey = 0;
            _templist.Add(_obj);
            foreach (var item in objectList)
            {
                _templist.Add(item);
            }
            return _templist;
        }
        public int GetDefaultCurrency()
        {
            CurrencyInfo objInfo = new CurrencyInfo();
            this.operation = () =>
            {
                CurrencyDao access = new CurrencyDao(this.Transaction);
                objInfo = access.GetDefaultCurrencyName();
            };
            this.Start(false);
            if (objInfo != null)
            {
                return objInfo.DG_Currency_pkey;
            }
            return 0;
        }

        public string CurrentCurrencyConversionRate()
        {
            List<CurrencyInfo> objectList = new List<CurrencyInfo>();
            this.operation = () =>
            {
                CurrencyDao access = new CurrencyDao(this.Transaction);
                objectList = access.Select(0, null);
            };
            this.Start(false);

            XElement root = new XElement("Currencies",
               (from row in objectList
                select new
                {
                    row.DG_Currency_pkey,
                    row.DG_Currency_Rate,
                    row.DG_Currency_Default,
                    row.SyncCode
                }).ToList().Select(
                   x => new XElement("Currency", new XAttribute("ID", x.DG_Currency_pkey), new XAttribute("Rate", x.DG_Currency_Rate), new XAttribute("DefaultCurrency", x.DG_Currency_Default), new XAttribute("SyncCode", x.SyncCode)
                 )));

            return root.ToString();
        }
        public List<CurrencyInfo> GetCurrencyListforconfig()
        {
            List<CurrencyInfo> objectList = new List<CurrencyInfo>();
            this.operation = () =>
            {
                CurrencyDao access = new CurrencyDao(this.Transaction);
                objectList = access.Select(0, null);
            };
            this.Start(false);
            return objectList;
        }
        public List<CurrencyInfo> GetCurrencyDetails()
        {
            try
            {
                List<CurrencyInfo> objectList = new List<CurrencyInfo>();
                this.operation = () =>
                {
                    CurrencyDao access = new CurrencyDao(this.Transaction);
                    objectList = access.Select(0, true);
                };
                this.Start(false);
                return objectList;
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
                return null;
            }
        }
        public string GetDefaultCurrencyName()
        {
            try
            {
                List<CurrencyInfo> _objCurrency = new List<CurrencyInfo>();
                this.operation = () =>
                {
                    CurrencyDao access = new CurrencyDao(this.Transaction);
                    _objCurrency = access.Select(0, true);
                };
                this.Start(false);
                return _objCurrency.Where(o=>o.DG_Currency_Default==true).FirstOrDefault().DG_Currency_Name;

            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
                return null;
            }
        }
        public List<CurrencyInfo> GetCurrencyOnly()
        {
            List<CurrencyInfo> objectList = new List<CurrencyInfo>();
            this.operation = () =>
            {
                CurrencyDao access = new CurrencyDao(this.Transaction);
                objectList = access.Select(null, true);
            };
            this.Start(false);
            return objectList;
        }
        public string GetCurrencyDetailFromID(int currencyId)
        {
            string currency = string.Empty;
            CurrencyInfo objInfo = new CurrencyInfo();
            this.operation = () =>
            {
                CurrencyDao access = new CurrencyDao(this.Transaction);
                objInfo = access.Select(currencyId, null).FirstOrDefault();
            };
            this.Start(false);
            if (objInfo != null)
                currency = objInfo.DG_Currency_Name + "#" + objInfo.DG_Currency_Rate + "#" + objInfo.DG_Currency_Symbol + "#" + objInfo.DG_Currency_Code;
            return currency;
        }

        public bool DeleteCurrency(int id)
        {
            bool result = false;
            this.operation = () =>
            {
                CurrencyDao access = new CurrencyDao(this.Transaction);
                result = access.Delete(id);
            };
            this.Start(false);
            return result;
        }

        //public List<CurrencyInfo> GetCurrencyListAll()
        //{
        //    List<CurrencyInfo> objInfo = new List<CurrencyInfo>();
        //    this.operation = () =>
        //    {
        //        CurrencyDao access = new CurrencyDao(this.Transaction);
        //        objInfo = access.SelectCurrency();
        //    };
        //    this.Start(false);
        //    return objInfo;
        //}

        public void SetCurrencyDetails(string CurrencyName, float rate, string symbol, int id, int modifiedby, bool IsDefault, DateTime UpdateDate, string Icon, string Currencycode, string SyncCode)
        {
            CurrencyInfo _objnew = new CurrencyInfo();
            _objnew.DG_Currency_pkey = id;
            _objnew.DG_Currency_Name = CurrencyName;
            _objnew.DG_Currency_Rate = rate;
            _objnew.DG_Currency_Symbol = symbol;
            _objnew.DG_Currency_ModifiedBy = modifiedby;
            _objnew.DG_Currency_Default = IsDefault;
            _objnew.DG_Currency_UpdatedDate = UpdateDate;
            _objnew.DG_Currency_Icon = Icon;
            _objnew.DG_Currency_Code = Currencycode;
            _objnew.DG_Currency_IsActive = true;
            _objnew.SyncCode = SyncCode;
            _objnew.IsSynced = false;
            if (id <= 0)
            {
                this.operation = () =>
                {
                    CurrencyDao access = new CurrencyDao(this.Transaction);
                    access.Add(_objnew);
                };
                this.Start(false);
            }
            else
            {
                this.operation = () =>
                {
                    CurrencyDao access = new CurrencyDao(this.Transaction);
                    access.Update(_objnew);
                };
                this.Start(false);
            }

        }
    }
}
