﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using DigiPhoto.DataLayer.Model;
using System.Xml.Linq;
using System.Data;
using System.ComponentModel;
using System.Collections;
using System.Reflection;
using System.Globalization;
using DigiPhoto.IMIX.Model;
using DigiPhoto.IMIX.DataAccess;
using DigiPhoto.IMIX.Business;
using DigiPhoto.DataLayer;

namespace DigiPhoto.IMIX.Business
{

    public partial class CustomBusineses : BaseBusiness
    {
        public DateTime ServerDateTime()
        {
            DateTime result = DateTime.Now;
            this.operation = () =>
            {
                ValueTypeDao access = new ValueTypeDao(this.Transaction);
                result = access.GetServerDateTime();
            };
            this.Start(false);
            return result;
        }

        public List<ServicesInfo> GetRunningServices()
        {
            try
            {
                List<ServicesInfo> objectList = new List<ServicesInfo>();
                this.operation = () =>
                {
                    ServicesDao access = new ServicesDao(this.Transaction);
                    objectList = access.GetServices();
                };
                this.Start(false);
                return objectList;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        private bool ServiceFound(string serviceName, List<ServicesInfo> ServiceList)
        {
            bool found = false;
            foreach (var ServiceItem in ServiceList)
            {
                if (ServiceItem.DG_Sevice_Name == serviceName)
                {
                    found = true;
                    break;
                }
            }
            return found;
        }
        public bool setServicePathForApplication(string directoryPath)
        {
            try
            {
                var item1 = GetRunningServices();
                ServicesInfo item;
                if (!ServiceFound("DigiWatcher", item1))
                {
                    item = new ServicesInfo();
                    item.DG_Service_Display_Name = "DG Watcher";
                    item.DG_Service_Path = directoryPath + "\\DigiWatcher.exe";
                    item.DG_Sevice_Name = "DigiWatcher";
                    item.IsService = false;
                    item.RunLevel = 2;
                    item.IsInterface = true;

                    this.operation = () =>
                    {
                        ServicesDao access = new ServicesDao(this.Transaction);
                        access.AddServices(item);
                    };
                    this.Start(false);
                }
                if (!ServiceFound("DigiPrintingConsole", item1))
                {
                    item = new ServicesInfo();
                    item.DG_Service_Display_Name = "DG Photo Printing Desktop Tool";
                    item.DG_Service_Path = directoryPath + "\\DigiPrintingConsole.exe";
                    item.DG_Sevice_Name = "DigiPrintingConsole";
                    item.IsInterface = true;
                    item.IsService = false;
                    item.RunLevel = 2;
                    this.operation = () =>
                    {
                        ServicesDao access = new ServicesDao(this.Transaction);
                        //item.DG_Service_Id = access.AddService(item);//TBD 
                        access.AddServices(item);
                    };
                    this.Start(false);
                }
                if (!ServiceFound("DigiWifiImageProcessing", item1))
                {
                    item = new ServicesInfo();
                    item.DG_Service_Display_Name = "DG Image Processing Service";
                    item.DG_Service_Path = directoryPath + "\\DigiWifiImageProcessing.exe";
                    item.DG_Sevice_Name = "DigiWifiImageProcessing";
                    item.IsInterface = false;
                    item.IsService = true;
                    item.RunLevel = 2;
                    this.operation = () =>
                    {
                        ServicesDao access = new ServicesDao(this.Transaction);
                        //item.DG_Service_Id = access.AddService(item);//TBD 
                        access.AddServices(item);
                    };
                    this.Start(false);
                }

                if (!ServiceFound("DigiEmailService", item1))
                {
                    item = new ServicesInfo();
                    item.DG_Service_Display_Name = "DG Email Service";
                    item.DG_Service_Path = directoryPath + "\\EmailService.exe";
                    item.DG_Sevice_Name = "DigiEmailService";
                    item.IsInterface = false;
                    item.IsService = true;
                    item.RunLevel = 1;
                    this.operation = () =>
                    {
                        ServicesDao access = new ServicesDao(this.Transaction);
                        //item.DG_Service_Id = access.AddService(item);//TBD 
                        access.AddServices(item);
                    };
                    this.Start(false);
                }
                if (!ServiceFound("DigiBackupService", item1))
                {
                    //Added towards including this service in system health monitor
                    item = new ServicesInfo();
                    item.DG_Service_Display_Name = "DigiBackupService";
                    item.DG_Service_Path = directoryPath + "\\DigiBackupService.exe";
                    item.DG_Sevice_Name = "DigiBackupService";
                    item.IsInterface = false;
                    item.IsService = true;
                    item.RunLevel = 1;
                    this.operation = () =>
                    {
                        ServicesDao access = new ServicesDao(this.Transaction);
                        //item.DG_Service_Id = access.AddService(item);//TBD 
                        access.AddServices(item);
                    };
                    this.Start(false);
                }
                //if (!ServiceFound("SocialMediaService", item1))
                //{
                //    //Social Media File upload service
                //    item = new ServicesInfo();
                //    item.DG_Service_Display_Name = "SocialMediaService";
                //    item.DG_Service_Path = directoryPath + "\\SocialMediaService.exe";
                //    item.DG_Sevice_Name = "SocialMediaService";
                //    item.IsInterface = false;
                //    item.IsService = true;
                //    item.RunLevel = 2;
                //    this.operation = () =>
                //    {
                //        ServicesDao access = new ServicesDao(this.Transaction);
                //        //item.DG_Service_Id = access.AddService(item);//TBD 
                //        access.AddServices(item);
                //    };
                //    this.Start(false);
                //}
                if (!ServiceFound("DigiphotoDataSyncService", item1))
                {
                    item = new ServicesInfo();
                    item.DG_Service_Display_Name = "DigiphotoDataSyncService";
                    item.DG_Service_Path = directoryPath + "\\DataSyncWinService.exe";
                    item.DG_Sevice_Name = "DigiphotoDataSyncService";
                    item.IsInterface = false;
                    item.IsService = true;
                    item.RunLevel = 1;
                    this.operation = () =>
                    {
                        ServicesDao access = new ServicesDao(this.Transaction);
                        //item.DG_Service_Id = access.AddService(item);//TBD 
                        access.AddServices(item);
                    };
                    this.Start(false);
                }
                if (!ServiceFound("DigiRFIDAssociationService", item1))
                {
                    item = new ServicesInfo();
                    item.DG_Service_Display_Name = "DigiRFIDAssociationService";
                    item.DG_Service_Path = directoryPath + "\\DigiRFIDAssociationService.exe";
                    item.DG_Sevice_Name = "DigiRFIDAssociationService";
                    item.IsInterface = false;
                    item.IsService = true;
                    item.RunLevel = 1;
                    this.operation = () =>
                    {
                        ServicesDao access = new ServicesDao(this.Transaction);
                        //item.DG_Service_Id = access.AddService(item);//TBD 
                        access.AddServices(item);
                    };
                    this.Start(false);
                }
                if (!ServiceFound("DigiRfidService", item1))
                {
                    item = new ServicesInfo();
                    item.DG_Service_Display_Name = "DigiRfidService";
                    item.DG_Service_Path = directoryPath + "\\DigiRfidService.exe";
                    item.DG_Sevice_Name = "DigiRfidService";
                    item.IsInterface = false;
                    item.IsService = true;
                    item.RunLevel = 2;
                    this.operation = () =>
                    {
                        ServicesDao access = new ServicesDao(this.Transaction);
                        //item.DG_Service_Id = access.AddService(item);//TBD 
                        access.AddServices(item);
                    };
                    this.Start(false);
                }

                if (!ServiceFound("DigiPreSoldService", item1))
                {
                    item = new ServicesInfo();
                    item.DG_Service_Display_Name = "DigiPreSoldService";
                    item.DG_Service_Path = directoryPath + "\\PreSoldService.exe";
                    item.DG_Sevice_Name = "DigiPreSoldService";
                    item.IsInterface = false;
                    item.IsService = true;
                    item.RunLevel = 1;
                    this.operation = () =>
                    {
                        ServicesDao access = new ServicesDao(this.Transaction);
                        //item.DG_Service_Id = access.AddService(item);//TBD 
                        access.AddServices(item);
                    };
                    this.Start(false);
                }


                if (!ServiceFound("DigiWatchManualProcess", item1))
                {
                    item = new ServicesInfo();
                    item.DG_Service_Display_Name = "DigiWatchManualProcess";
                    item.DG_Service_Path = directoryPath + "\\ManualDownloadProcess.exe";
                    item.DG_Sevice_Name = "DigiWatchManualProcess";
                    item.IsInterface = false;
                    item.IsService = true;
                    item.RunLevel = 2;
                    this.operation = () =>
                    {
                        ServicesDao access = new ServicesDao(this.Transaction);
                        //item.DG_Service_Id = access.AddService(item);//TBD 
                        access.AddServices(item);
                    };
                    this.Start(false);
                }

                if (!ServiceFound("ImageProcessingEngine", item1))
                {
                    item = new ServicesInfo();
                    item.DG_Service_Display_Name = "ImageProcessingEngine";
                    item.DG_Service_Path = directoryPath + "\\ImageProcessingEngine.exe";
                    item.DG_Sevice_Name = "ImageProcessingEngine";
                    item.IsInterface = false;
                    item.IsService = false;
                    item.RunLevel = 2;
                    this.operation = () =>
                    {
                        ServicesDao access = new ServicesDao(this.Transaction);
                        //item.DG_Service_Id = access.AddService(item);//TBD 
                        access.AddServices(item);
                    };
                    this.Start(false);
                }
                if (!ServiceFound("DigiReportExportService", item1))
                {
                    item = new ServicesInfo();
                    item.DG_Service_Display_Name = "DigiReportExportService";
                    item.DG_Service_Path = directoryPath + "\\DigiReportExportService.exe";
                    item.DG_Sevice_Name = "DigiReportExportService";
                    item.IsInterface = false;
                    item.IsService = true;
                    item.RunLevel = 1;
                    this.operation = () =>
                    {
                        ServicesDao access = new ServicesDao(this.Transaction);
                    
                        access.AddServices(item);
                    };
                    this.Start(false);
                }
                if (!ServiceFound("DigiWatermarkService", item1))
                {
                    item = new ServicesInfo();
                    item.DG_Service_Display_Name = "DigiWatermarkService";
                    item.DG_Service_Path = directoryPath + "\\DigiWatermarkService.exe";
                    item.DG_Sevice_Name = "DigiWatermarkService";
                    item.IsInterface = false;
                    item.IsService = true;
                    item.RunLevel = 1;
                    this.operation = () =>
                    {
                        ServicesDao access = new ServicesDao(this.Transaction);

                        access.AddServices(item);
                    };
                    this.Start(false);
                }
                
                return true;

            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                return false;
            }
        }
    }
}
