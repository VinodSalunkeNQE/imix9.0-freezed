﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
//using DigiPhoto.DataLayer.Model;
using System.Xml.Linq;
using System.Data;
using System.ComponentModel;
using System.Collections;
using System.Reflection;
using System.Globalization;
using DigiPhoto.IMIX.Model;
using DigiPhoto.IMIX.DataAccess;
using DigiPhoto.IMIX.Business;
using DigiPhoto.DataLayer;
using DigiPhoto.Cache.Repository;
using DigiPhoto.Cache.DataCache;
using DigiPhoto.Cache.MasterDataCache;

namespace DigiPhoto.IMIX.Business
{

    public class ActivityBusiness : BaseBusiness
    {
        CustomBusineses customBusiness;
        public bool RegisterLog(int CurrentUser, int ActivityType, String Description, string SyncCode)
        {
           
                customBusiness = new CustomBusineses();
                ActivityInfo _objnewActivity = new ActivityInfo();
                _objnewActivity.DG_Acitivity_By = CurrentUser;
                _objnewActivity.DG_Acitivity_ActionType = ActivityType;
                _objnewActivity.DG_Acitivity_Descrption = Description;
                _objnewActivity.DG_Acitivity_Date = DateTime.Now;
                _objnewActivity.SyncCode = SyncCode;
                _objnewActivity.IsSynced = false;
                this.operation = () =>
                {
                    ActivityDao access = new ActivityDao(this.Transaction);
                     _objnewActivity.DG_Acitivity_Action_Pkey = access.AddActivity(_objnewActivity);
                };
                this.Start(false);
                if (_objnewActivity.DG_Acitivity_Action_Pkey > 0)
                {
                    return true;

                }
                else
                {
                    return false;
                }     
        }
        public bool RegisterLog(int CurrentUser, int ActivityType, String Description, String SyncCode, int RefID)
        {
            
                customBusiness = new CustomBusineses();
                ActivityInfo _objnewActivity = new ActivityInfo();
                _objnewActivity.DG_Acitivity_By = CurrentUser;
                _objnewActivity.DG_Acitivity_ActionType = ActivityType;
                _objnewActivity.DG_Acitivity_Descrption = Description;
                _objnewActivity.DG_Acitivity_Date = DateTime.Now;
                _objnewActivity.DG_Reference_ID = RefID;
                _objnewActivity.SyncCode = SyncCode;
                _objnewActivity.IsSynced = false;
                this.operation = () =>
                {
                    ActivityDao access = new ActivityDao(this.Transaction);
                    _objnewActivity.DG_Acitivity_Action_Pkey = access.AddActivity(_objnewActivity);
                };
                this.Start(false);
                if (_objnewActivity.DG_Acitivity_Action_Pkey > 0)
                {
                    return true;

                }
                else
                {
                    return false;
                }
            
           
        }
        public List<ActivityInfo> GetActivity(bool ISFromDate, bool IsToDate, DateTime FromDate, DateTime ToDate, int UserID)
        {
            List<ActivityInfo> result = null;
            try
            {
                this.operation = () =>
                {

                    ActivityDao access = new ActivityDao(this.Transaction);

                    result = access.GetActivityReports();
                };
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
            return result;
        }

        public DataSet GetActivityReport( DateTime? FromDate, DateTime? ToDate, int? UserID)
        {
           
               // List<vw_GetActivityReports> objresult = null;
            DataSet dsResult = new DataSet();
                this.operation = () =>
                {
                    //context.CommandTimeout = 60000;                     
                    ActivityDao access = new ActivityDao(this.Transaction);
                    dsResult = access.GetActivityReport(FromDate, ToDate, UserID);
                    //if (UserID != 0 && !ISFromDate && !IsToDate)
                    //{
                    //    objresult = (from item in _objPackageDetailsdata
                    //                 where item.DG_User_pkey == UserID
                    //                 select item).Distinct().ToList();
                    //}
                    //else if (UserID != 0 && ISFromDate && !IsToDate)
                    //{
                    //    objresult = (from item in _objPackageDetailsdata
                    //                 where item.DG_User_pkey == UserID && item.DG_Acitivity_Date >= FromDate
                    //                 select item).Distinct().ToList();
                    //}
                    //else if (UserID != 0 && !ISFromDate && IsToDate)
                    //{
                    //    objresult = (from item in _objPackageDetailsdata
                    //                 where item.DG_User_pkey == UserID && item.DG_Acitivity_Date <= ToDate
                    //                 select item).Distinct().ToList();
                    //}
                    //else if (UserID != 0 && ISFromDate && IsToDate)
                    //{
                    //    objresult = (from item in _objPackageDetailsdata
                    //                 where item.DG_User_pkey == UserID && item.DG_Acitivity_Date >= FromDate && item.DG_Acitivity_Date <= ToDate
                    //                 select item).Distinct().ToList();
                    //}
                    //else if (UserID == 0 && ISFromDate && !IsToDate)
                    //{
                    //    objresult = (from item in _objPackageDetailsdata
                    //                 where item.DG_Acitivity_Date >= FromDate
                    //                 select item).Distinct().ToList();
                    //}
                    //else if (UserID == 0 && !ISFromDate && IsToDate)
                    //{
                    //    objresult = (from item in _objPackageDetailsdata
                    //                 where item.DG_Acitivity_Date <= ToDate
                    //                 select item).Distinct().ToList();
                    //}
                    //else if (UserID == 0 && ISFromDate && IsToDate)
                    //{
                    //    objresult = (from item in _objPackageDetailsdata
                    //                 where item.DG_Acitivity_Date >= FromDate && item.DG_Acitivity_Date <= ToDate
                    //                 select item).Distinct().ToList();
                    //}


                };
                this.Start(false);
                return dsResult;
           
        }
    }
}
