﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DigiPhoto.IMIX.Model;
using DigiPhoto.IMIX.DataAccess;


namespace DigiPhoto.IMIX.Business
{
   public  class VideoSceneBusiness:BaseBusiness
   {
       #region Declaration
       VideoSceneAccess videoSceneObjectAccess = null;
       #endregion
       public bool SaveVideoSceneObject(string VideoObjectIds)
       {
           bool isSuccess = false;
           this.operation = () =>
           {
               videoSceneObjectAccess = new VideoSceneAccess(this.Transaction);
               isSuccess = videoSceneObjectAccess.SaveVideoSceneObject(VideoObjectIds);
           };
           this.Start(false);
           return isSuccess;
 
       }
       public List<VideoSceneObject> GetVideoSceneObjects(VideoSceneObject videoSceneObject)
       {
           List<VideoSceneObject> videoObjects = new List<VideoSceneObject>();
           this.operation = () =>
           {
               videoSceneObjectAccess = new VideoSceneAccess(this.Transaction);
               videoObjects = videoSceneObjectAccess.GetVideoSceneObjects(videoSceneObject);
           };
           this.Start(false);
           return videoObjects;

       }

       public bool SaveVideoScene(VideoSceneViewModel VideoScene)
       {
           bool isSuccess = false;
           this.operation = () =>
           {
               videoSceneObjectAccess = new VideoSceneAccess(this.Transaction);
               isSuccess = videoSceneObjectAccess.SaveVideoScene(VideoScene);
           };
           this.Start(false);
           return isSuccess;

       }

       public bool SaveMixerScene(VideoSceneViewModel VideoScene)
       {
           bool isSuccess = false;
           this.operation = () =>
           {
               videoSceneObjectAccess = new VideoSceneAccess(this.Transaction);
               isSuccess = videoSceneObjectAccess.SaveMixerScene(VideoScene);
           };
           this.Start(false);
           return isSuccess;

       }


       public List<VideoScene> GetVideoScene(int sceneId,int? substoreId=0)
       {
           List<VideoScene> videoObjects = new List<VideoScene>();
           this.operation = () =>
           {
               videoSceneObjectAccess = new VideoSceneAccess(this.Transaction);
               videoObjects = videoSceneObjectAccess.GetVideoScene(sceneId, substoreId);
           };
           this.Start(false);
           return videoObjects;
          
       }

       public string   DeleteVideoSceneDetails(int? Sceneid)
       {
           
           string SceneName = string.Empty;
           this.operation = () =>
           {
               videoSceneObjectAccess = new VideoSceneAccess(this.Transaction);
               SceneName = videoSceneObjectAccess.DeleteVideoSceneDetails(Sceneid);
           };
           this.Start(false);
           return SceneName;
       }

       public VideoSceneViewModel GetVideoSceneToEdit(int? Sceneid)
       {
          VideoSceneViewModel obj = new VideoSceneViewModel();

           this.operation = () =>
           {
               videoSceneObjectAccess = new VideoSceneAccess(this.Transaction);
               obj = videoSceneObjectAccess.GetVideoSceneToEdit(Sceneid);
               
           };
           this.Start(false);
         

        return obj;
       }

       public VideoSceneViewModel GetVideoSceneEditToResource(int? Sceneid)
       {
           VideoSceneViewModel obj = new VideoSceneViewModel();

           this.operation = () =>
           {
               videoSceneObjectAccess = new VideoSceneAccess(this.Transaction);
               obj = videoSceneObjectAccess.GetVideoSceneEditToResource(Sceneid);

           };
           this.Start(false);


           return obj;
       }
       public List<VideoScene> GetVideoSceneByCriteria(int locationId, int sceneId)
       {
           List<VideoScene> objVideoScene = new List<VideoScene>();
           this.operation = () =>
           {
               videoSceneObjectAccess = new VideoSceneAccess(this.Transaction);
               objVideoScene = videoSceneObjectAccess.GetVideoSceneByCriteria(locationId, sceneId);
           };
           this.Start(false);
           return objVideoScene;
       }

       public List<VideoSceneObject> GuestVideoObjectBySceneID(int sceneId)
       {
           List<VideoSceneObject> lstVideoSceneObject = new List<VideoSceneObject>();
           this.operation = () =>
           {
               videoSceneObjectAccess = new VideoSceneAccess(this.Transaction);
               lstVideoSceneObject = videoSceneObjectAccess.GuestVideoObjectBySceneID(sceneId);
           };
           this.Start(false);
           return lstVideoSceneObject;

       }
       public VideoObjectFileMapping GetobjectVideoDetails(int vidobjectId)
       {
           VideoObjectFileMapping objVideoScene = new VideoObjectFileMapping();
           this.operation = () =>
           {
               videoSceneObjectAccess = new VideoSceneAccess(this.Transaction);
               objVideoScene = videoSceneObjectAccess.GetobjectVideoDetails(vidobjectId);
           };
           this.Start(false);
           return objVideoScene;

       }


       public List<Watchersetting> GetSettingWatcher(int LocationId)
       {
           List<Watchersetting> Watchersetting = new List<Watchersetting>();
           this.operation = () =>
           {
               videoSceneObjectAccess = new VideoSceneAccess(this.Transaction);
               Watchersetting = videoSceneObjectAccess.GetSettingWatcher(LocationId);
           };
           this.Start(false);
           return Watchersetting;

       }

       public string  checkIsActiveForAdvance(int locationId)
       {
           string sceneName = string.Empty;
           this.operation = () =>
           {
               videoSceneObjectAccess = new VideoSceneAccess(this.Transaction);
               sceneName = videoSceneObjectAccess.checkIsActiveForAdvance(locationId);
           };
           this.Start(false);
           return sceneName;
       }

       public string checkIsActiveForVideoProcessing(int locationId, int SceneId)
       {
           string sceneName = string.Empty;
           this.operation = () =>
           {
               videoSceneObjectAccess = new VideoSceneAccess(this.Transaction);
               sceneName = videoSceneObjectAccess.checkIsActiveForVideoProcessing(locationId, SceneId);
           };
           this.Start(false);
           return sceneName;
       }
       public bool UpdateSceneGraphicProfile(int sceneID,int configID)
       {
           bool isSuccess = false;
           this.operation = () =>
           {
               videoSceneObjectAccess = new VideoSceneAccess(this.Transaction);
               isSuccess = videoSceneObjectAccess.UpdateSceneGraphicProfile(sceneID, configID);
           };
           this.Start(false);
           return isSuccess;

       }

       public bool CheckProfileName(string name)
       {
           bool isSuccess = false;
           this.operation = () =>
           {
               videoSceneObjectAccess = new VideoSceneAccess(this.Transaction);
               isSuccess = videoSceneObjectAccess.CheckProfileName(name);
           };
           this.Start(false);
           return isSuccess;
       }

        #region added by Ajay
        public bool SaveVideoSceneSettings(VideoScene VideoScene)
        {
            bool isSuccess = false;
            this.operation = () =>
            {
                videoSceneObjectAccess = new VideoSceneAccess(this.Transaction);
                isSuccess = videoSceneObjectAccess.SaveVideoSceneSettings(VideoScene);
            };
            this.Start(false);
            return isSuccess;

        }
        #endregion
        #region added by latika
        public bool SaveUpdLiveVideoQuickSettings(QuickSettings ObjQickSett)
        {
            bool isSuccess = false;
            this.operation = () =>
            {
                videoSceneObjectAccess = new VideoSceneAccess(this.Transaction);
                isSuccess = videoSceneObjectAccess.SaveUpdLiveVideoQuickSettings(ObjQickSett);
            };
            this.Start(false);
            return isSuccess;

        }
       
        public List<QuickSettings> GetLiveVideoQuickSettings(QuickSettings ObjQickSett)
        {
            List<QuickSettings> Watchersetting = new List<QuickSettings>();
            this.operation = () =>
            {
                videoSceneObjectAccess = new VideoSceneAccess(this.Transaction);
                Watchersetting = videoSceneObjectAccess.GetLiveVideoQuickSettings(ObjQickSett);
            };
            this.Start(false);
            return Watchersetting;

        }
        #endregion

    }
}
