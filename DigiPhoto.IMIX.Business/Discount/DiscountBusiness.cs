﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using DigiPhoto.DataLayer.Model;
using System.Xml.Linq;
using System.Data;
using System.ComponentModel;
using System.Collections;
using System.Reflection;
using System.Globalization;
using DigiPhoto.IMIX.Model;
using DigiPhoto.IMIX.DataAccess;
using DigiPhoto.IMIX.Business;

namespace DigiPhoto.IMIX.Business
{
    public class DiscountBusiness : BaseBusiness
    {
        public void SetDiscountDetails(string DiscountName, string DiscountDesc, bool isactive, bool issecure, bool isitemlevel, bool isaspercentage, string Discountcode, string SyncCode)
        {
            DiscountTypeInfo _objnew = new DiscountTypeInfo();
            DiscountTypeInfo _objnewchk = new DiscountTypeInfo();
            _objnew.DG_Orders_DiscountType_Name = DiscountName;
            _objnew.DG_Orders_DiscountType_Desc = DiscountDesc;
            _objnew.DG_Orders_DiscountType_Active = isactive;
            _objnew.DG_Orders_DiscountType_Secure = issecure;
            _objnew.DG_Orders_DiscountType_ItemLevel = isitemlevel;
            _objnew.DG_Orders_DiscountType_AsPercentage = isaspercentage;
            _objnew.DG_Orders_DiscountType_Code = Discountcode;
            _objnew.SyncCode = SyncCode;
            _objnew.IsSynced = false;
            this.operation = () =>
            {
                DiscountTypeDao access = new DiscountTypeDao(this.Transaction);
                _objnewchk = access.Get(null, DiscountName);
            }; this.Start(false);

            if (_objnewchk == null)
            {
                this.operation = () =>
                {
                    DiscountTypeDao access = new DiscountTypeDao(this.Transaction);
                    access.Add(_objnew);
                };
                this.Start(false);
            }
            else
            {
                this.operation = () =>
                {
                    _objnew.DG_Orders_DiscountType_Pkey = _objnewchk.DG_Orders_DiscountType_Pkey;
                    DiscountTypeDao access = new DiscountTypeDao(this.Transaction);
                    access.Update(_objnew);
                };
                this.Start(false);
            }

        }
        public List<DiscountTypeInfo> GetDiscountDetails()
        {

            List<DiscountTypeInfo> objectList = new List<DiscountTypeInfo>();
            this.operation = () =>
            {
                DiscountTypeDao access = new DiscountTypeDao(this.Transaction);
                objectList = access.Select(null, string.Empty);
            };
            this.Start(false);
            return objectList;
        }
        public bool DeleteDiscount(int id)
        {
            bool result = false;
            this.operation = () =>
            {
                DiscountTypeDao access = new DiscountTypeDao(this.Transaction);
                result = access.Delete(id);
            };
            this.Start(false);
            return result;

        }
        public string GetDiscountDetailsFromID(int id)
        {
            DiscountTypeInfo _objdiscount = new DiscountTypeInfo();
            this.operation = () =>
            {
                DiscountTypeDao access = new DiscountTypeDao(this.Transaction);
                _objdiscount = access.Get(id, string.Empty);
            };
            this.Start(false);
            if (_objdiscount != null)
            {
                return _objdiscount.DG_Orders_DiscountType_Name + "#" + _objdiscount.DG_Orders_DiscountType_Desc + "#" + _objdiscount.DG_Orders_DiscountType_Active + "#" + _objdiscount.DG_Orders_DiscountType_Secure + "#" + _objdiscount.DG_Orders_DiscountType_ItemLevel + "#" + _objdiscount.DG_Orders_DiscountType_AsPercentage + "#" + _objdiscount.DG_Orders_DiscountType_Code;
            }
            else
            {
                return null;
            }
        }
        public List<DiscountTypeInfo> GetDiscountType()
        {
            List<DiscountTypeInfo> objectList = new List<DiscountTypeInfo>();
            this.operation = () =>
            {
                DiscountTypeDao access = new DiscountTypeDao(this.Transaction);
                objectList = access.Select(null, string.Empty);
            };
            this.Start(false);
            return objectList;
        }
    }
}
