﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using DigiPhoto.DataLayer.Model;
using System.Xml.Linq;
using System.Data;
using System.ComponentModel;
using System.Collections;
using System.Reflection;
using System.Globalization;
using DigiPhoto.IMIX.Model;
using DigiPhoto.IMIX.DataAccess;
using DigiPhoto.IMIX.Business;
using DigiPhoto.DataLayer;

namespace DigiPhoto.IMIX.Business
{
    public partial class UserBusiness : BaseBusiness
    {
        public UsersInfo GetUserDetails(string username, string password)
        {
            UsersInfo objectInfo = new UsersInfo();
            this.operation = () =>
            {
                UsersDao access = new UsersDao(this.Transaction);
                objectInfo = access.GetUserDetails(username, password, true);
            };
            this.Start(false);
            return objectInfo;
        }


        public bool EncryptUsersPwd(Dictionary<int, string> ltEncryptedpwd)
        {
            try
            {
                bool result = false;
                this.operation = () =>
                {
                    UsersDao access = new UsersDao(this.Transaction);

                    foreach (KeyValuePair<int, string> pair in ltEncryptedpwd)
                    {
                        int UId = pair.Key;
                        access.UPD_EncryptUsersPwd(pair.Key, pair.Value);
                    }

                }; this.Start(false);
                return true;
            }
            catch
            {
                return false;
            }
        }
        /// <summary>
        /// Adds the update user details.
        /// </summary>
        /// <param name="DG_User_pkey">The command g_ user_pkey.</param>
        /// <param name="DG_User_Name">Name of the command g_ user_.</param>
        /// <param name="DG_User_First_Name">Name of the command g_ user_ first_.</param>
        /// <param name="DG_User_Last_Name">Name of the command g_ user_ last_.</param>
        /// <param name="DG_User_Password">The command g_ user_ password.</param>
        /// <param name="DG_User_Roles_Id">The command g_ user_ roles_ unique identifier.</param>
        /// <param name="DG_Location_ID">The command g_ location_ unique identifier.</param>
        /// <param name="DG_User_Status">The command g_ user_ status.</param>
        /// <param name="DG_User_PhoneNo">The command g_ user_ phone no.</param>
        /// <param name="DG_User_Email">The command g_ user_ email.</param>
        /// <returns></returns>
        public bool AddUpdateUserDetails(int DG_User_pkey, string DG_User_Name, string DG_User_First_Name, string DG_User_Last_Name,
            string DG_User_Password, int DG_User_Roles_Id, int DG_Location_ID, bool? DG_User_Status, string DG_User_PhoneNo, string DG_User_Email, string SyncCode, string DG_Emp_Id)
        {
            try
            {
                bool result = false;
                this.operation = () =>
                {
                    UsersDao access = new UsersDao(this.Transaction);

                    access.UPD_INS_UserDetails(DG_User_pkey, DG_User_Roles_Id, DG_Location_ID, DG_User_Status, DG_User_Name, DG_User_First_Name,
                        DG_User_Last_Name, DG_User_PhoneNo, DG_User_Email, SyncCode, DG_User_Password, DG_Emp_Id);
                };
                this.Start(false);
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }



        public Dictionary<int, string> GetUsersPwdDetails()
        {
            try
            {

                Dictionary<int, string> itemList = null;
                this.operation = () =>
                    {
                        UsersDao access = new UsersDao(this.Transaction);
                        itemList = access.GetUsersPwdDetails().ToDictionary(k => k.DG_User_pkey, l => l.DG_User_Password);
                    };
                this.Start(false);
                return itemList;
            }
            catch
            {
                return null;
            }
        }


        public Dictionary<string, int> GetUserDetailsByUserId(int userId)
        {
            Dictionary<string, int> itemList = new Dictionary<string, int>();
            this.operation = () =>
            {
                UsersDao access = new UsersDao(this.Transaction);
                itemList = access.SelUserDetailsByUserId(userId);
            };
            this.Start(false);
            return itemList;
        }

        public List<UsersInfo> GetUserDetailByUserId(int userId, string userName, int roleId)
        {
            List<UsersInfo> itemList = new List<UsersInfo>();
            this.operation = () =>
            {
                UsersDao access = new UsersDao(this.Transaction);
                itemList = access.SelUserDetailByUserId(userId, userName, roleId);
            };
            this.Start(false);
            return itemList;
        }
        public List<UsersInfo> GetChildUserDetailByUserId(int roleId)
        {
            List<UsersInfo> itemList = new List<UsersInfo>();
            this.operation = () =>
            {
                UsersDao access = new UsersDao(this.Transaction);
                itemList = access.GetChildUserDetailByUserId(roleId);
            };
            this.Start(false);
            return itemList;
        }
        
        public bool DeleteUsers(Int32 userID)
        {
            bool result = false;
            this.operation = () =>
            {
                UsersDao access = new UsersDao(this.Transaction);
                result = access.DeleteUsersbyId(userID);
            };
            this.Start(false);
            return result;
        }



        public List<UsersInfo> SearchUserDetails(string FName, string LName, int StoreId, string Status, int RoleId, string MobileNumber, string EmailId, int locationId, string userName,string EmpId)
        {
            List<UsersInfo> itemList = new List<UsersInfo>();
            this.operation = () =>
             {
                 UsersDao access = new UsersDao(this.Transaction);
                 itemList = access.SearchUserDetails(FName, LName, StoreId, Status, RoleId, MobileNumber, EmailId, locationId, userName, EmpId);
             };
            this.Start(false);
            return itemList;
        }
        public string GetEmployeeName(string EmpId)
        {
            string empname = "NA";
            this.operation = () =>
            {
                UsersDao access = new UsersDao(this.Transaction);
                empname = access.GetEmployeeName(EmpId);
            };
            this.Start(false);
            return empname;
        }
    }
}
