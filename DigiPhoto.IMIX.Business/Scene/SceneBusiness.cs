﻿


using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using DigiPhoto.DataLayer.Model;
using System.Xml.Linq;
using System.Data;
using System.ComponentModel;
using System.Collections;
using System.Reflection;
using System.Globalization;
using DigiPhoto.IMIX.Model;
using DigiPhoto.IMIX.DataAccess;
using DigiPhoto.IMIX.Business;
using DigiPhoto.DataLayer;
using DigiPhoto.Cache.Repository;
using DigiPhoto.Cache.DataCache;
using DigiPhoto.Cache.MasterDataCache;
using DigiPhoto.IMIX.Model.Base;
using DigiPhoto.IMIX.DataAccess.Base;

namespace DigiPhoto.IMIX.Business
{

    public class SceneBusiness : BaseBusiness
    {
        public List<SceneInfo> GetSceneDetails()
        {
            try
            {
                List<SceneInfo> objList = new List<SceneInfo>();
                this.operation = () =>
                {
                    SceneDao access = new SceneDao(this.Transaction);
                    objList = access.GetSceneDetails();
                };
                this.Start(false);
                return objList;
            }
            catch (Exception ex)
            {
                //throw ex;
                return null;
            }
        }
        public void SetSceneDetails(SceneInfo sceneInfo)
        {
            this.operation = () =>
            {
                SceneDao access = new SceneDao(this.Transaction);
                access.Add(sceneInfo);
            };
            this.Start(false);
            ICacheRepository imixCache = DataCacheFactory.GetFactory<ICacheRepository>(typeof(SceneCache).FullName);
            imixCache.RemoveFromCache();

        }

        public SceneInfo GetSceneNameFromID(int id)
        {

            SceneInfo _objList = new SceneInfo();
            this.operation = () =>
            {
                SceneDao access = new SceneDao(this.Transaction);
                _objList = access.GetSceneFromId(id);
            };
            this.Start(false);
            return _objList;
        }

        public bool DeleteScene(int id)
        {
            try
            {
                bool result = false;
                this.operation = () =>
                {
                    SceneDao access = new SceneDao(this.Transaction);
                    result = access.Delete(id);
                };
                this.Start(false);
                ICacheRepository imixCache = DataCacheFactory.GetFactory<ICacheRepository>(typeof(SceneCache).FullName);
                imixCache.RemoveFromCache();
                return result;

            }
            catch (Exception ex)
            {
                return false;
            }
        }
        public List<ThemeInfo> GetAllTheme()
        {

            List<ThemeInfo> _objList = new List<ThemeInfo>();
            this.operation = () =>
            {
                ThemeDao access = new ThemeDao(this.Transaction);
                _objList = access.Select(null);
            };
            this.Start(false);
            return _objList;
        }
        public SceneInfo GetSceneDetailsSceneID(int id)
        {

            SceneInfo _objList = new SceneInfo();
            this.operation = () =>
            {
                SceneDao access = new SceneDao(this.Transaction);
                _objList = access.GetSceneFromSceneId(id);
            };
            this.Start(false);
            return _objList;
        }
        public List<SceneInfo> GetSceneFromThemeID(int id)
        {

            List<SceneInfo> _objList = new List<SceneInfo>();
            this.operation = () =>
            {
                SceneDao access = new SceneDao(this.Transaction);
                _objList = access.GetSceneFromThemeId(id);
            };
            this.Start(false);
            return _objList;
        }

        /// <summary>
        /// Added by Vinod Salunke_30Oct2021
        /// </summary>
        /// <param name="id">Theme ID</param>
        /// <returns>All blank pages for given theme ID</returns>
        public List<SceneInfo> GetAllBlankPageByThemeID(int id)
        {

            List<SceneInfo> _objList = new List<SceneInfo>();
            this.operation = () =>
            {
                SceneDao access = new SceneDao(this.Transaction);
                _objList = access.GetAllBlankPageByThemeID(id);
            };
            this.Start(false);
            return _objList;
        }
    }
}
