﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using DigiPhoto.DataLayer.Model;
using System.Xml.Linq;
using System.Data;
using System.ComponentModel;
using System.Collections;
using System.Reflection;
using System.Globalization;
using DigiPhoto.IMIX.Model;
using DigiPhoto.IMIX.DataAccess;
using DigiPhoto.IMIX.Business;


namespace DigiPhoto.IMIX.Business
{
    public class CashBoxBusiness : BaseBusiness
    {
        public bool SetcashBoxReason(DateTime createddate, int userid, string reason)
        {
            CashBoxInfo objectInfo = new CashBoxInfo();
            objectInfo.UserId = userid;
            objectInfo.CreatedDate = createddate;
            objectInfo.Reason = reason;
            this.operation = () =>
            {
                CashBoxDao access = new CashBoxDao(this.Transaction);
                objectInfo.Id = access.Add(objectInfo); 
            };
            this.Start(false);
            if (objectInfo.Id > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
