﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DigiPhoto.IMIX.DataAccess;
using DigiPhoto.IMIX.Model;
using System.Data;

namespace DigiPhoto.IMIX.Business
{
    public class SyncStatusBusiness:BaseBusiness
    {
        public List<SyncStatusInfo> GetSyncStatusList(DateTime FrmDate, DateTime ToDate)
        {
            try
            {
                List<SyncStatusInfo> objLst = new List<SyncStatusInfo>();
                this.operation = () =>
                {
                    SyncStatusAccess access = new SyncStatusAccess(this.Transaction);
                    objLst = access.GetSyncStatus(FrmDate, ToDate);
                };
                this.Start(false);
                return objLst;
            }
            catch
            {
                return null;
            }
        }
        public List<SyncStatusInfo> GetOrdersyncStatus(DateTime? FrmDate, DateTime? ToDate, string QRCode)
        {
          
                List<SyncStatusInfo> objLst = new List<SyncStatusInfo>();
                this.operation = () =>
                {
                    SyncStatusAccess access = new SyncStatusAccess(this.Transaction);
                    objLst = access.GetOrdersyncStatus(FrmDate, ToDate, QRCode);
                };
                this.Start(false);
                return objLst;
            
        }
        public bool ReSync(string ReSyncId)
        {
            bool result = false;
            this.operation = () =>
            {
                SyncStatusAccess access = new SyncStatusAccess(this.Transaction);
                result = access.ReSync(ReSyncId);

            }; this.Start(false);
            return result;
        }
        public bool ReSyncImages(string OrderId)
        {
            bool result = false;
            this.operation = () =>
            {
                SyncStatusAccess access = new SyncStatusAccess(this.Transaction);
                result = access.ReSyncImage(OrderId);

            }; this.Start(false);
            return result;
        }

        public List<SyncStatusInfo> GetFormSyncStatusList(DateTime FrmDate, DateTime ToDate)
        {
            try
            {
                List<SyncStatusInfo> objLst = new List<SyncStatusInfo>();
                this.operation = () =>
                {
                    SyncStatusAccess access = new SyncStatusAccess(this.Transaction);
                    objLst = access.GetSyncStatusOpenCloseForm(FrmDate, ToDate);
                };
                this.Start(false);
                return objLst;
            }
            catch
            {
                return null;
            }
        }

        public bool SetResyncHistory(DateTime ResyncDatetime, int ResyncStatus, int ResyncType)
        {
            try
            {
                this.operation = () =>
                {
                    SyncStatusAccess access = new SyncStatusAccess(this.Transaction);
                    access.InsertResyncHistory(ResyncDatetime, ResyncStatus, ResyncType);
                };
                this.Start(false);
                return true;

            }
            catch (Exception ex)
            {
                return false;
            }
        }


        public List<ChangeTrackingProcessingStatusDtl> GetChangeTrackingProcessingStatusDtl(long ChangeTackingID)
        {
            try
            {
                List<ChangeTrackingProcessingStatusDtl> objLst = new List<ChangeTrackingProcessingStatusDtl>();
                this.operation = () =>
                {
                    SyncStatusAccess access = new SyncStatusAccess(this.Transaction);
                    objLst = access.GetChangeTrackingProcessingStatusDtl(ChangeTackingID);
                };
                this.Start(false);
                return objLst;
            }
            catch
            {
                return null;
            }
        }
        public DataSet GetMasterDataSyncStaus(DateTime from, DateTime To, long ApplicationObjectID)
        {
            DataSet lstPrintSummaryDetail = new DataSet();

            this.operation = () =>
            {
                SyncStatusAccess access = new SyncStatusAccess(this.Transaction);
                lstPrintSummaryDetail = access.GetMasterDataSyncStaus(from,To, ApplicationObjectID);
            };
            this.Start(false);

            return lstPrintSummaryDetail;
        }
    }
}
