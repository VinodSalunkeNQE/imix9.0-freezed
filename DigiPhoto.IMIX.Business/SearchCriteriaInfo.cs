﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DigiPhoto.IMIX.DataAccess;
using DigiPhoto.IMIX.Model;

namespace DigiPhoto.IMIX.Business
{
    public class SearchCriteriaInfo : BaseBusiness
    {
        public List<SearchDetailInfo> GetSearchDetailWithQrcode(SearchDetailInfo Searchdetails, out long maxPhotoId, out long minPhotoId, out long imgCount, int mediaType)
        {
            long maxId = 0;
            long minId = 0;
            long imgcout = 0;
            List<SearchDetailInfo> list = new List<SearchDetailInfo>();
            this.operation = () =>
            {
                SearchCriteriaAccess access = new SearchCriteriaAccess(this.Transaction);
                list = access.GetSearchDetailWithQrcode(Searchdetails, out maxId, out minId, out imgcout,mediaType);
                //  return list;
            };
            this.Start(false);
            maxPhotoId = maxId;
            minPhotoId = minId;
            imgCount = imgcout;
            return list;
        }
		////changed by latika for table Workflow
        public List<SearchDetailInfo> GetSearchDetailWithQrcodeTableFlow(SearchDetailInfo Searchdetails, out long maxPhotoId, out long minPhotoId, out long imgCount, int mediaType)
        {
            long maxId = 0;
            long minId = 0;
            long imgcout = 0;
            List<SearchDetailInfo> list = new List<SearchDetailInfo>();
            this.operation = () =>
            {
                SearchCriteriaAccess access = new SearchCriteriaAccess(this.Transaction);
                list = access.GetSearchDetailWithQrcodeTableFlow(Searchdetails, out maxId, out minId, out imgcout, mediaType);
                //  return list;
            };
            this.Start(false);
            maxPhotoId = maxId;
            minPhotoId = minId;
            imgCount = imgcout;
            return list;
        }
		////end
    }
}
