﻿using DigiPhoto.IMIX.DataAccess;
using DigiPhoto.IMIX.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DigiPhoto.IMIX.Business
{
   public partial class OrderBusiness : BaseBusiness
    {
       public string GetOrderInvoiceNumber(int OrderId)
        {
            string issucess = string.Empty;
            this.operation = () =>
            {
                OrderAccess access = new OrderAccess(this.Transaction);
                issucess = access.GetOrderInvoiceNumber(OrderId);
            };
            this.Start(false);
            return issucess;
        }

        /// <summary>
        /// Added By KC to get fresh cancel order invoice number 
        /// </summary>
        /// <param name="OrderInvoiceNumber">Invoice number that generated at the time of order</param>
        /// <returns>String value as Invoice Number</returns>
        public string GetCancelOrderInvoiceNumber(string OrderInvoiceNumber)
        {
            string issucess = string.Empty;
            this.operation = () =>
            {
                OrderAccess access = new OrderAccess(this.Transaction);
                issucess = access.GetCancelOrderInvoiceNumber(OrderInvoiceNumber);
            };
            this.Start(false);
            return issucess;
        }
        /////////////created by latika for bug fix /////////
        public List<QRcodes> GETQRcodesbyOrderNumber(string OrderNumber)
        {
            List<QRcodes> orderInfo = new List<QRcodes>();
            this.operation = () =>
            {
                OrderAccess access = new OrderAccess(this.Transaction);
                orderInfo = access.GETQRcodesbyOrderNumber(OrderNumber);
            };
            this.Start(false);
            if (orderInfo != null)
                return orderInfo.ToList();
            else
                return null;
        }
		/////changed by latika for presols service
        public int UpdOnlineQRCodeForPhotoPresold(string ClaimCode, string PhotoIDs)
        {
            int issucess = 0;
            this.operation = () =>
            {
                OrderAccess access = new OrderAccess(this.Transaction);
                issucess = access.UpdOnlineQRCodeForPhotoPresold(ClaimCode, PhotoIDs);
            };
            this.Start(false);
            return issucess;
        }
////end
    }
}
