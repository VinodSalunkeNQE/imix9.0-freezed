﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using DigiPhoto.Cache.DataCache;
using System.Windows.Media.Animation;
using System.Diagnostics;
using System.Windows.Threading;
using System.IO;
using System.Xml;
using DigiPhoto.IMIX.Business;
using DigiPhoto.IMIX.Model;
using DigiPhoto.DataLayer;

namespace DigiPhoto.PreviewWall
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        
        private void Application_Startup(object sender, StartupEventArgs e)
        {
            try
            {
                string _xmlFilePathMaster = string.Empty;
                string _PrewViewType = "MainWindow.xaml";
                Int32 _locationID, _subStroeId = 0;
                _xmlFilePathMaster = System.IO.Path.Combine(Environment.CurrentDirectory, "PreviewSettingLocation.xml");
                // Get Reference to the current Process
                Process thisProc = Process.GetCurrentProcess();
                // Check how many total processes have the same name as the current one
                if (Process.GetProcessesByName(thisProc.ProcessName).Length > 1)
                {
                    // If there is more than one, then it is already running.
                    MessageBox.Show("Preview Wall is already running.");

                    Application.Current.Shutdown();
                    return;
                }
                ErrorHandler.ErrorHandler.DeleteLog();
                if (File.Exists(_xmlFilePathMaster))
                {
                    XmlDataDocument xmldoc = new XmlDataDocument();
                    xmldoc.Load(_xmlFilePathMaster);
                    _subStroeId = Convert.ToInt32(xmldoc.GetElementsByTagName("SubStore")[0].ChildNodes.Item(0).InnerText.Trim());
                    _locationID = Convert.ToInt32(xmldoc.GetElementsByTagName("Location")[0].ChildNodes.Item(0).InnerText.Trim());
                    List<iMixConfigurationLocationInfo> ConfigValuesList = (new ConfigBusiness()).GetConfigBasedOnLocation(_locationID).Where(s => s.SubstoreId == _subStroeId).ToList();
                    if (ConfigValuesList != null)
                    {
                        var isGumBallActive = ConfigValuesList.Where(n => n.IMIXConfigurationMasterId == (long)ConfigParams.IsGumRideActive
                        && n.ConfigurationValue.ToUpper() == "TRUE").Select(x => Convert.ToInt64(x.LocationId)).ToList();
                        if (isGumBallActive != null && isGumBallActive.Count > 0)
                        {
                            _PrewViewType = "MainWindowGumBallRide.xaml";
                        }
                    }
                }
                StartupUri = new System.Uri(_PrewViewType, UriKind.Relative);
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }

        }
        void App_DispatcherUnhandledException(object sender, DispatcherUnhandledExceptionEventArgs args)
        {


            // Prevent default unhandled exception processing
            args.Handled = true;

            Environment.Exit(0);
        }
    }
}
