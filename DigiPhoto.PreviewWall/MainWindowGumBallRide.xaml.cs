﻿using DigiPhoto.DataLayer;
using DigiPhoto.IMIX.Business;
using DigiPhoto.IMIX.Model;
using DigiPhoto.IMIX.Model.Base;
using DigiPhoto.Utility.Repository.DirectoryInfoExt;
using FrameworkHelper;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Threading;
using System.Xml;
using WpfAnimatedGif;

namespace DigiPhoto.PreviewWall
{
    /// <summary>
    /// Interaction logic for PreviewWall in IMIX
    /// </summary>
    public partial class MainWindowGumBallRide : Window
    {
        #region Declaration

        bool _showMsg = false;
        List<FileInfo> latestGusetImageList = new List<FileInfo>();
        List<iMixConfigurationLocationInfo> _locationWiseConfigParamsList;
        List<iMixConfigurationLocationInfo> _locationWiseConfigList;
        PreviewWallInfo _previewWallInfoSetting;
        //static string _xmlFilePath = string.Empty;
        static string _xmlFilePathMaster, _xmlFilePathMasterLocation = string.Empty;
        Timer _tmrGetMediaFile = new Timer();
        List<string> _rotationDisplayScreenlist;
        List<string> _RFIDFontWeightlist, _horizentalBlinkBorderlist, _verticalBlinkBorderlist, _PlayerScoreFontWeightlist;
        List<string> _dispalyScreenlist;
        int _mktMediaCount, _guestImgCount = 0;
        string _waterMarkImg, _logoImg, _photoId, _mktImgPath, _guestImgPath, _horizentalFrame, _verticalFrame = string.Empty,_guestExplainatoryImg;
        string siteName, locationName = string.Empty;//Added by Vins_30Aug2019
        int _reloadGuestImgTimeInterval = 1;
        bool _isMktImagesActive = false;
        List<FileInfo> _gusetImgList = new List<FileInfo>();
        List<FileInfo> _mrkMediaList = new List<FileInfo>();
        List<iMIXStoreConfigurationInfo> _dummyTag = new List<iMIXStoreConfigurationInfo>();
        string _hotFolderPath = string.Empty;
        string _mediaPlayPath = string.Empty;
        string _subStoreID = string.Empty;
        string _locationID = string.Empty;
        string _screenID = string.Empty;
        string _rotationAngle = "0";
        string _RFIDfontSize, _PlayerScorefontSize = "1";
        string _PhotoMarginLeft, _PhotoMarginTop, _PhotoMarginRight, _PhotoMarginBottom = "0";
        string _Player1ScoreMarginLeft, _Player1ScoreMarginTop, _Player1ScoreMarginRight, _Player1ScoreMarginBottom,
            _Player2ScoreMarginLeft, _Player2ScoreMarginTop, _Player2ScoreMarginRight, _Player2ScoreMarginBottom,
            _Player3ScoreMarginLeft, _Player3ScoreMarginTop, _Player3ScoreMarginRight, _Player3ScoreMarginBottom,
            _Player4ScoreMarginLeft, _Player4ScoreMarginTop, _Player4ScoreMarginRight, _Player4ScoreMarginBottom,
            _Player5ScoreMarginLeft, _Player5ScoreMarginTop, _Player5ScoreMarginRight, _Player5ScoreMarginBottom,
            _Player6ScoreMarginLeft, _Player6ScoreMarginTop, _Player6ScoreMarginRight, _Player6ScoreMarginBottom = "0";
        string _MessageMarginLeft, _MessageMarginTop, _MessageMarginRight, _MessageMarginBottom = "0";
        string _StretchImg = "FALSE";
        string _RFIDFontWeight = "ExtraLight";
        string _PlayerScoreFontWeight = "ExtraLight";
        string _HorizentalBorderWidth, _VerticalBorderWidth = "X";
        string _RFIDBackColor, _Player1ScoreBackColor, _Player2ScoreBackColor, _Player3ScoreBackColor, _Player4ScoreBackColor, _Player5ScoreBackColor, _Player6ScoreBackColor = "#FFFFFF";

        string _RFIDForeColor = "#FFFFFF";
        Int32 RFIDOverWritesStatus = 0;
        bool IsAnonymousQrCodeEnabled = false;
        DateTime _lastPickTime = DateTime.MinValue;
        DateTime _lastCleanUpTime = DateTime.Now;
        DateTime _currentDateTime;
        bool _mrkMediaPreviewTime = false;

        string _QrCodeURL = string.Empty;
        string webURL = string.Empty;

        double rotateAngle = 0;
        string QRCODE = string.Empty;
        bool isPlayGuestImg = true;
        //DateTime _PreviewScreenTime;
        //DateTime _blinkScreenTime;
        //bool _set = false;
        Int32 _guestMediaReloadTime, _marketingMediaReloadTime, _previewHighlightAdjustTime, _previewWallCleanUpTimeInMinute = 0;
        String _marketingMediaCopy, _marketingMediaDelete = "No";
        string _copyMarketinMediaPath = string.Empty;
        List<string> _PositionList;

        string _MSGFontSize = "1";
        string _MsgForeColor = "#FFFFFF";
        string _MSGPosition = "Bottom-Left";
        string _MSGBGColor = "#FFFFFF";
        string _MsgShowTime = "1";

        bool ismktActive = false;
        string result = string.Empty;
        int counter = 0;
        int previewTimeCounter = 0;
        DateTime lastFilePlayTime;
        bool needToLoop = true;
        bool dummyTag = false;
        StringBuilder Sb = new StringBuilder();

        string _playerScore = string.Empty;
        string _isVisibleGumBallZeroScore = "True";
        #endregion

        #region Constructor
        public MainWindowGumBallRide()
        {
            InitializeComponent();
            AssignValueToControl();
            //AssignLocationValue();
            txtBarcode.Focus();
            if (ConfigurationManager.AppSettings.AllKeys.Contains("GuestMediaReloadTime"))
                _guestMediaReloadTime = Convert.ToInt32(ConfigurationManager.AppSettings["GuestMediaReloadTime"].ToString());
            if (ConfigurationManager.AppSettings.AllKeys.Contains("MarketingMediaReloadTime"))
                _marketingMediaReloadTime = Convert.ToInt32(ConfigurationManager.AppSettings["MarketingMediaReloadTime"].ToString());
            if (ConfigurationManager.AppSettings.AllKeys.Contains("MarketingMediaCopy"))
                _marketingMediaCopy = ConfigurationManager.AppSettings["MarketingMediaCopy"].ToString();
            if (ConfigurationManager.AppSettings.AllKeys.Contains("MarketingMediaDelete"))
                _marketingMediaDelete = ConfigurationManager.AppSettings["MarketingMediaDelete"].ToString();
            if (ConfigurationManager.AppSettings.AllKeys.Contains("PreviewHighlightAdjustTime"))
                _previewHighlightAdjustTime = Convert.ToInt32(ConfigurationManager.AppSettings["PreviewHighlightAdjustTime"].ToString());
            if (ConfigurationManager.AppSettings.AllKeys.Contains("PreviewWallCleanUpTimeInMinute"))
                _previewWallCleanUpTimeInMinute = Convert.ToInt32(ConfigurationManager.AppSettings["PreviewWallCleanUpTimeInMinute"].ToString());
            _xmlFilePathMaster = System.IO.Path.Combine(Environment.CurrentDirectory, "PreviewSettingGumBall.xml");
            _xmlFilePathMasterLocation = System.IO.Path.Combine(Environment.CurrentDirectory, "PreviewSettingLocation.xml");
            if (File.Exists(_xmlFilePathMaster) && LoadInitialSettings())
            {
                grdSettings.Visibility = Visibility.Collapsed;
                btnSave_Click(null, null);
            }
            else
            {
                btnCloseScreen.Visibility = Visibility.Visible;
                grdSettings.Visibility = Visibility.Visible;
            }
            GetQrCodeURL();
        }
        private void GetQrCodeURL()
        {
            try
            {
                webURL = (new StoreSubStoreDataBusniess()).GetQRCodeWebUrl();
                _QrCodeURL = string.IsNullOrEmpty(webURL) ? " " : webURL;
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
            }
        }
        private void GetMarketingVideo()
        {
            try
            {
                if (!string.IsNullOrEmpty(_mktImgPath) && _previewWallInfoSetting.IsMktImgPW)
                {
                    if (Directory.Exists(_mktImgPath))
                    {
                        var myDirInfo = new DirectoryInfo(_mktImgPath);
                        _mrkMediaList = myDirInfo.GetFilesByExtensions(".jpg", ".png", ".mp4", ".jpeg", ".JPEG",
                                                        ".wmv", ".mpg", ".avi", ".3gp", ".JPG", ".MP4", ".WMV", ".MPG", ".AVI", ".3GP").ToList();
                    }
                    if (_marketingMediaCopy.ToUpper() == "YES" && _mrkMediaList.Count > 0)
                    {
                        _copyMarketinMediaPath = System.IO.Path.Combine(Environment.CurrentDirectory, "MarketingMedia");
                        if (_marketingMediaDelete.ToUpper() == "YES")
                        {
                            if (Directory.Exists(_copyMarketinMediaPath))
                                Directory.Delete(_copyMarketinMediaPath, true);
                        }
                        if (!Directory.Exists(_copyMarketinMediaPath))
                            Directory.CreateDirectory(_copyMarketinMediaPath);
                        foreach (FileInfo mrkVideo in _mrkMediaList)
                        {
                            File.Copy(mrkVideo.FullName, System.IO.Path.Combine(_copyMarketinMediaPath, mrkVideo.Name), false);
                        }
                        _mrkMediaList = (new DirectoryInfo(_copyMarketinMediaPath)).GetFilesByExtensions(".jpg", ".png", ".mp4", ".jpeg", ".JPEG",
                                                            ".wmv", ".mpg", ".avi", ".3gp", ".JPG", ".MP4", ".WMV", ".MPG", ".AVI", ".3GP").ToList();
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
            }
        }
        #endregion

        #region Events
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                int ScreenCount = System.Windows.Forms.Screen.AllScreens.Count();
                if (ScreenCount > 1)
                {
                    MainWindow window = null;
                    foreach (Window wnd in System.Windows.Application.Current.Windows)
                    {
                        if (wnd.Title == "DEI")
                        {
                            window = (MainWindow)wnd;
                        }
                    }

                    if (window == null)
                    {

                        window = new MainWindow();
                        window.WindowStartupLocation = System.Windows.WindowStartupLocation.Manual;

                    }

                    System.Windows.Forms.Screen s1 = System.Windows.Forms.Screen.AllScreens[ScreenCount - 1];

                    System.Drawing.Rectangle r1 = s1.WorkingArea;
                    window.WindowState = System.Windows.WindowState.Normal;
                    window.WindowStartupLocation = System.Windows.WindowStartupLocation.Manual;
                    window.Top = r1.Top;
                    window.Left = r1.Left;
                    window.WindowState = System.Windows.WindowState.Maximized;
                }
                LoadInitialSettings();
                if (_gusetImgList.Count == 0)
                {
                    PreViewWallMedia.Source = new Uri(_mediaPlayPath, UriKind.Relative);
                    PreViewWallMedia.Play();
                    System.Threading.Thread.Sleep(_marketingMediaReloadTime);
                    //PreViewWallMedia.BeginAnimation(UIElement.OpacityProperty, new DoubleAnimation(PreViewWallMedia.Opacity, 1, TimeSpan.FromSeconds(10)));
                }
                CheckIsPreviewWall();
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
            }

        }
        private void AssignLocationValue()
        {
            if (clsLocation.LocationId != 0)
            {
                cmbSubStore.SelectedValue = clsLocation.SiteId;
                cmbLocation.SelectedValue = clsLocation.LocationId;
                cmbLocation.IsEnabled = true;
            }
        }
        private void cmbSubStore_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                Int32 subStoreId = Convert.ToInt32(cmbSubStore.SelectedValue);
                if (subStoreId != 0)
                {
                    cmbLocation.IsEnabled = true;
                    List<LocationInfo> locations = new List<LocationInfo>();
                    locations = (new StoreSubStoreDataBusniess()).GetSelectedLocationsSubstore(subStoreId).ToList();
                    CommonUtility.BindComboWithSelect<LocationInfo>(cmbLocation, locations, "DG_Location_Name", "DG_Location_pkey", 0, DigiPhoto.Utility.Repository.Data.ClientConstant.SelectString);
                }
                else
                {
                    cmbLocation.IsEnabled = false;
                    cmbScreen.IsEnabled = false;
                }
                cmbLocation.SelectedValue = "0";
                cmbScreen.SelectedValue = "--Select--";
                if (!cmbLocation.IsEnabled)
                    cmbScreen.IsEnabled = false;
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
            }
        }
        private void cmbLocation_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                List<iMixConfigurationLocationInfo> ConfigValuesList = (new ConfigBusiness()).GetConfigBasedOnLocation(Convert.ToInt32(cmbLocation.SelectedValue)).Where(s => s.SubstoreId == Convert.ToInt32(cmbSubStore.SelectedValue)).ToList();
                if (Convert.ToInt32(cmbLocation.SelectedValue) != 0)
                {
                    List<iMixConfigurationLocationInfo> LocationConfigValuesList = (new ConfigBusiness()).GetConfigBasedOnLocation(Convert.ToInt32(cmbLocation.SelectedValue)).Where(s => s.SubstoreId == Convert.ToInt32(cmbSubStore.SelectedValue)).ToList();
                    if (ConfigValuesList != null)
                    {
                        var isGumBallActive = ConfigValuesList.Where(n => n.IMIXConfigurationMasterId == (long)ConfigParams.IsGumRideActive
                        && n.ConfigurationValue.ToUpper() == "TRUE").Select(x => Convert.ToInt64(x.LocationId)).ToList();
                        if (isGumBallActive != null && isGumBallActive.Count == 0)
                        {
                            clsLocation.SiteId = Convert.ToInt32(cmbSubStore.SelectedValue);
                            clsLocation.LocationId = Convert.ToInt32(cmbLocation.SelectedValue);
                            new MainWindow().Show();
                            this.Close();
                        }
                    }
                    Int32 ScreenNo = Convert.ToInt32((ConfigValuesList.Where(s => s.IMIXConfigurationMasterId == Convert.ToInt32(ConfigParams.NoOfScreenPW)).FirstOrDefault()).ConfigurationValue);
                    if (ScreenNo != 0)
                    {
                        cmbScreen.IsEnabled = true;
                        _dispalyScreenlist = new List<string>();
                        _dispalyScreenlist.Add("--Select--");
                        for (int i = 1; i <= ScreenNo; i++)
                        {
                            _dispalyScreenlist.Add("Display" + i);
                        }
                        cmbScreen.ItemsSource = _dispalyScreenlist;
                    }
                    else
                    {
                        cmbScreen.IsEnabled = false;
                    }
                    cmbScreen.SelectedValue = "--Select--";
                }
                else
                {
                    cmbScreen.SelectedValue = "--Select--";
                    cmbScreen.IsEnabled = false;
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
            }
        }
        private bool GetPreviewTime()
        {
            int previewTime;
            _locationWiseConfigList = (new ConfigBusiness()).GetLocationWiseConfigParams(Convert.ToInt32(cmbSubStore.SelectedValue));
            iMixConfigurationLocationInfo objLocation = _locationWiseConfigList.Where(n => n.LocationId == Convert.ToInt32(cmbLocation.SelectedValue)).Where(x => x.IMIXConfigurationMasterId == (int)ConfigParams.PreviewTimePW).FirstOrDefault();
            if (objLocation != null)
                previewTime = Convert.ToInt32(objLocation.ConfigurationValue);
            else
                previewTime = 0;
            if (txtMsgShowTime.Text == "0")
            {
                System.Windows.MessageBox.Show("Message Display Time cannot be 0");
                return false;
            }
            if (!string.IsNullOrEmpty(txtMsgShowTime.Text))
            {
                if (previewTime < Convert.ToInt32(txtMsgShowTime.Text))
                {
                    System.Windows.MessageBox.Show("Message Display Time cannot be greater than Preview Time\n\n Your Image Preview time for this location is  " + previewTime + "sec.");
                    return false;
                }
                else
                    return true;
            }
            return false;
        }
        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (CheckValidation())
                {
                    ResetOldSetting();

                    if (!GetPreviewTime())
                        return;

                    //if (File.Exists(_xmlFilePath))
                    //{
                    //    File.Delete(_xmlFilePath);
                    //}

                    XmlTextWriter writerLoc = new XmlTextWriter(_xmlFilePathMasterLocation, System.Text.Encoding.UTF8);
                    writerLoc.WriteStartDocument(true);
                    writerLoc.Formatting = Formatting.Indented;
                    writerLoc.Indentation = 2;
                    writerLoc.WriteStartElement("Settings");

                    writerLoc.WriteStartElement("SubStore");
                    writerLoc.WriteString(cmbSubStore.SelectedValue.ToString());
                    writerLoc.WriteEndElement();
                    //_subStoreID = cmbSubStore.SelectedValue.ToString();

                    writerLoc.WriteStartElement("Location");
                    writerLoc.WriteString(cmbLocation.SelectedValue.ToString());
                    writerLoc.WriteEndElement();
                    //_locationID = cmbLocation.SelectedValue.ToString();
                    writerLoc.WriteEndDocument();
                    writerLoc.Close();



                    XmlTextWriter writer = new XmlTextWriter(_xmlFilePathMaster, System.Text.Encoding.UTF8);
                    writer.WriteStartDocument(true);
                    writer.Formatting = Formatting.Indented;
                    writer.Indentation = 2;
                    writer.WriteStartElement("Settings");

                    writer.WriteStartElement("SubStore");
                    writer.WriteString(cmbSubStore.SelectedValue.ToString());
                    writer.WriteEndElement();
                    _subStoreID = cmbSubStore.SelectedValue.ToString();

                    writer.WriteStartElement("Location");
                    writer.WriteString(cmbLocation.SelectedValue.ToString());
                    writer.WriteEndElement();
                    _locationID = cmbLocation.SelectedValue.ToString();

                    writer.WriteStartElement("Screen");
                    writer.WriteString(cmbScreen.SelectedValue.ToString());
                    writer.WriteEndElement();
                    _screenID = cmbScreen.SelectedValue.ToString();

                    writer.WriteStartElement("RotationAngle");
                    writer.WriteString(cmbRotation.SelectedValue.ToString());
                    writer.WriteEndElement();
                    _rotationAngle = cmbRotation.SelectedValue.ToString();

                    writer.WriteStartElement("RFIDFontSize");
                    writer.WriteString(txtRFIDFont.Text.Trim());
                    writer.WriteEndElement();
                    _RFIDfontSize = txtRFIDFont.Text.Trim();

                    writer.WriteStartElement("PlayerScoreFontSize");
                    writer.WriteString(txtPlayerScoreFont.Text.Trim());
                    writer.WriteEndElement();
                    _PlayerScorefontSize = txtPlayerScoreFont.Text.Trim();

                    writer.WriteStartElement("PhotoMarginLeft");
                    writer.WriteString(txtPhotoMarginLeft.Text.Trim());
                    writer.WriteEndElement();
                    _PhotoMarginLeft = txtPhotoMarginLeft.Text.Trim();

                    writer.WriteStartElement("PhotoMarginTop");
                    writer.WriteString(txtPhotoMarginTop.Text.Trim());
                    writer.WriteEndElement();
                    _PhotoMarginTop = txtPhotoMarginTop.Text.Trim();

                    writer.WriteStartElement("PhotoMarginRight");
                    writer.WriteString(txtPhotoMarginRight.Text.Trim());
                    writer.WriteEndElement();
                    _PhotoMarginRight = txtPhotoMarginRight.Text.Trim();

                    writer.WriteStartElement("PhotoMarginBottom");
                    writer.WriteString(txtPhotoMarginBottom.Text.Trim());
                    writer.WriteEndElement();
                    _PhotoMarginBottom = txtPhotoMarginBottom.Text.Trim();

                    writer.WriteStartElement("MessageMarginLeft");
                    writer.WriteString(txtMessageMarginLeft.Text.Trim());
                    writer.WriteEndElement();
                    _MessageMarginLeft = txtMessageMarginLeft.Text.Trim();

                    writer.WriteStartElement("MessageMarginTop");
                    writer.WriteString(txtMessageMarginTop.Text.Trim());
                    writer.WriteEndElement();
                    _MessageMarginTop = txtMessageMarginTop.Text.Trim();

                    writer.WriteStartElement("MessageMarginRight");
                    writer.WriteString(txtMessageMarginRight.Text.Trim());
                    writer.WriteEndElement();
                    _MessageMarginRight = txtMessageMarginRight.Text.Trim();

                    writer.WriteStartElement("MessageMarginBottom");
                    writer.WriteString(txtMessageMarginBottom.Text.Trim());
                    writer.WriteEndElement();
                    _MessageMarginBottom = txtMessageMarginBottom.Text.Trim();


                    writer.WriteStartElement("RFIDBackColor");
                    writer.WriteString(cmbRFIDBackColor.SelectedColor.ToString());
                    writer.WriteEndElement();
                    _RFIDBackColor = cmbRFIDBackColor.SelectedColor.ToString();

                    writer.WriteStartElement("RFIDForeColor");
                    writer.WriteString(cmbRFIDForeColor.SelectedColor.ToString());
                    writer.WriteEndElement();
                    _RFIDForeColor = cmbRFIDForeColor.SelectedColor.ToString();

                    writer.WriteStartElement("RFIDFontWeight");
                    writer.WriteString(cmbRFIDFontWeight.SelectedValue.ToString());
                    writer.WriteEndElement();
                    _RFIDFontWeight = cmbRFIDFontWeight.SelectedValue.ToString();

                    writer.WriteStartElement("PlayerScoreFontWeight");
                    writer.WriteString(cmbPlayerScoreFontWeight.SelectedValue.ToString());
                    writer.WriteEndElement();
                    _PlayerScoreFontWeight = cmbPlayerScoreFontWeight.SelectedValue.ToString();

                    writer.WriteStartElement("HorizentalBorderWidth");
                    writer.WriteString(cmbHorizentalBorderWidth.SelectedValue.ToString());
                    writer.WriteEndElement();
                    _HorizentalBorderWidth = cmbHorizentalBorderWidth.SelectedValue.ToString();

                    writer.WriteStartElement("VerticalBorderWidth");
                    writer.WriteString(cmbVerticalBorderWidth.SelectedValue.ToString());
                    writer.WriteEndElement();
                    _VerticalBorderWidth = cmbVerticalBorderWidth.SelectedValue.ToString();


                    writer.WriteStartElement("StretchImg");
                    writer.WriteString(chkStretchImg.IsChecked.ToString());
                    writer.WriteEndElement();
                    _StretchImg = chkStretchImg.IsChecked.ToString();

                    writer.WriteStartElement("MSGFontSize");
                    writer.WriteString(txtFont.Text.Trim());
                    writer.WriteEndElement();
                    _MSGFontSize = txtFont.Text.Trim();

                    writer.WriteStartElement("MSGForeColor");
                    writer.WriteString(cmbMessageForeColor.SelectedColor.ToString());
                    writer.WriteEndElement();
                    _MsgForeColor = cmbMessageForeColor.SelectedColor.ToString();

                    writer.WriteStartElement("MSGPosition");
                    writer.WriteString(cmbPosition.SelectedValue.ToString());
                    writer.WriteEndElement();
                    _MSGPosition = cmbPosition.SelectedValue.ToString();

                    writer.WriteStartElement("MSGBGColor");
                    writer.WriteString(cmbMsgBGColor.SelectedColor.ToString());
                    writer.WriteEndElement();
                    _MSGBGColor = cmbMessageForeColor.SelectedColor.ToString();


                    writer.WriteStartElement("MSGShowTime");
                    writer.WriteString(txtMsgShowTime.Text.Trim());
                    writer.WriteEndElement();
                    _MsgShowTime = txtMsgShowTime.Text.Trim();

                    writer.WriteStartElement("Player1ScoreMarginLeft");
                    writer.WriteString(txtPlayer1ScoreMarginLeft.Text.Trim());
                    writer.WriteEndElement();
                    _Player1ScoreMarginLeft = txtPlayer1ScoreMarginLeft.Text.Trim();

                    writer.WriteStartElement("Player1ScoreMarginTop");
                    writer.WriteString(txtPlayer1ScoreMarginTop.Text.Trim());
                    writer.WriteEndElement();
                    _Player1ScoreMarginTop = txtPlayer1ScoreMarginTop.Text.Trim();

                    writer.WriteStartElement("Player1ScoreMarginRight");
                    writer.WriteString(txtPlayer1ScoreMarginRight.Text.Trim());
                    writer.WriteEndElement();
                    _Player1ScoreMarginRight = txtPlayer1ScoreMarginRight.Text.Trim();

                    writer.WriteStartElement("Player1ScoreMarginBottom");
                    writer.WriteString(txtPlayer1ScoreMarginBottom.Text.Trim());
                    writer.WriteEndElement();
                    _Player1ScoreMarginBottom = txtPlayer1ScoreMarginBottom.Text.Trim();

                    writer.WriteStartElement("Player2ScoreMarginLeft");
                    writer.WriteString(txtPlayer2ScoreMarginLeft.Text.Trim());
                    writer.WriteEndElement();
                    _Player2ScoreMarginLeft = txtPlayer2ScoreMarginLeft.Text.Trim();

                    writer.WriteStartElement("Player2ScoreMarginTop");
                    writer.WriteString(txtPlayer2ScoreMarginTop.Text.Trim());
                    writer.WriteEndElement();
                    _Player2ScoreMarginTop = txtPlayer2ScoreMarginTop.Text.Trim();

                    writer.WriteStartElement("Player2ScoreMarginRight");
                    writer.WriteString(txtPlayer2ScoreMarginRight.Text.Trim());
                    writer.WriteEndElement();
                    _Player2ScoreMarginRight = txtPlayer2ScoreMarginRight.Text.Trim();

                    writer.WriteStartElement("Player2ScoreMarginBottom");
                    writer.WriteString(txtPlayer2ScoreMarginBottom.Text.Trim());
                    writer.WriteEndElement();
                    _Player2ScoreMarginBottom = txtPlayer2ScoreMarginBottom.Text.Trim();

                    writer.WriteStartElement("Player3ScoreMarginLeft");
                    writer.WriteString(txtPlayer3ScoreMarginLeft.Text.Trim());
                    writer.WriteEndElement();
                    _Player3ScoreMarginLeft = txtPlayer3ScoreMarginLeft.Text.Trim();

                    writer.WriteStartElement("Player3ScoreMarginTop");
                    writer.WriteString(txtPlayer3ScoreMarginTop.Text.Trim());
                    writer.WriteEndElement();
                    _Player3ScoreMarginTop = txtPlayer3ScoreMarginTop.Text.Trim();

                    writer.WriteStartElement("Player3ScoreMarginRight");
                    writer.WriteString(txtPlayer3ScoreMarginRight.Text.Trim());
                    writer.WriteEndElement();
                    _Player3ScoreMarginRight = txtPlayer3ScoreMarginRight.Text.Trim();

                    writer.WriteStartElement("Player3ScoreMarginBottom");
                    writer.WriteString(txtPlayer3ScoreMarginBottom.Text.Trim());
                    writer.WriteEndElement();
                    _Player3ScoreMarginBottom = txtPlayer3ScoreMarginBottom.Text.Trim();

                    writer.WriteStartElement("Player4ScoreMarginLeft");
                    writer.WriteString(txtPlayer4ScoreMarginLeft.Text.Trim());
                    writer.WriteEndElement();
                    _Player4ScoreMarginLeft = txtPlayer4ScoreMarginLeft.Text.Trim();

                    writer.WriteStartElement("Player4ScoreMarginTop");
                    writer.WriteString(txtPlayer4ScoreMarginTop.Text.Trim());
                    writer.WriteEndElement();
                    _Player4ScoreMarginTop = txtPlayer4ScoreMarginTop.Text.Trim();

                    writer.WriteStartElement("Player4ScoreMarginRight");
                    writer.WriteString(txtPlayer4ScoreMarginRight.Text.Trim());
                    writer.WriteEndElement();
                    _Player4ScoreMarginRight = txtPlayer4ScoreMarginRight.Text.Trim();

                    writer.WriteStartElement("Player4ScoreMarginBottom");
                    writer.WriteString(txtPlayer4ScoreMarginBottom.Text.Trim());
                    writer.WriteEndElement();
                    _Player4ScoreMarginBottom = txtPlayer4ScoreMarginBottom.Text.Trim();

                    writer.WriteStartElement("Player5ScoreMarginLeft");
                    writer.WriteString(txtPlayer5ScoreMarginLeft.Text.Trim());
                    writer.WriteEndElement();
                    _Player5ScoreMarginLeft = txtPlayer5ScoreMarginLeft.Text.Trim();

                    writer.WriteStartElement("Player5ScoreMarginTop");
                    writer.WriteString(txtPlayer5ScoreMarginTop.Text.Trim());
                    writer.WriteEndElement();
                    _Player5ScoreMarginTop = txtPlayer5ScoreMarginTop.Text.Trim();

                    writer.WriteStartElement("Player5ScoreMarginRight");
                    writer.WriteString(txtPlayer5ScoreMarginRight.Text.Trim());
                    writer.WriteEndElement();
                    _Player5ScoreMarginRight = txtPlayer5ScoreMarginRight.Text.Trim();

                    writer.WriteStartElement("Player5ScoreMarginBottom");
                    writer.WriteString(txtPlayer5ScoreMarginBottom.Text.Trim());
                    writer.WriteEndElement();
                    _Player5ScoreMarginBottom = txtPlayer5ScoreMarginBottom.Text.Trim();

                    writer.WriteStartElement("Player6ScoreMarginLeft");
                    writer.WriteString(txtPlayer6ScoreMarginLeft.Text.Trim());
                    writer.WriteEndElement();
                    _Player6ScoreMarginLeft = txtPlayer6ScoreMarginLeft.Text.Trim();

                    writer.WriteStartElement("Player6ScoreMarginTop");
                    writer.WriteString(txtPlayer6ScoreMarginTop.Text.Trim());
                    writer.WriteEndElement();
                    _Player6ScoreMarginTop = txtPlayer6ScoreMarginTop.Text.Trim();

                    writer.WriteStartElement("Player6ScoreMarginRight");
                    writer.WriteString(txtPlayer6ScoreMarginRight.Text.Trim());
                    writer.WriteEndElement();
                    _Player6ScoreMarginRight = txtPlayer6ScoreMarginRight.Text.Trim();

                    writer.WriteStartElement("Player6ScoreMarginBottom");
                    writer.WriteString(txtPlayer6ScoreMarginBottom.Text.Trim());
                    writer.WriteEndElement();
                    _Player6ScoreMarginBottom = txtPlayer6ScoreMarginBottom.Text.Trim();

                    writer.WriteStartElement("Player1ScoreBackColor");
                    writer.WriteString(cmbPlayer1ScoreBackColor.SelectedColor.ToString());
                    writer.WriteEndElement();
                    _Player1ScoreBackColor = cmbPlayer1ScoreBackColor.SelectedColor.ToString();

                    writer.WriteStartElement("Player2ScoreBackColor");
                    writer.WriteString(cmbPlayer2ScoreBackColor.SelectedColor.ToString());
                    writer.WriteEndElement();
                    _Player2ScoreBackColor = cmbPlayer2ScoreBackColor.SelectedColor.ToString();

                    writer.WriteStartElement("Player3ScoreBackColor");
                    writer.WriteString(cmbPlayer3ScoreBackColor.SelectedColor.ToString());
                    writer.WriteEndElement();
                    _Player3ScoreBackColor = cmbPlayer3ScoreBackColor.SelectedColor.ToString();

                    writer.WriteStartElement("Player4ScoreBackColor");
                    writer.WriteString(cmbPlayer4ScoreBackColor.SelectedColor.ToString());
                    writer.WriteEndElement();
                    _Player4ScoreBackColor = cmbPlayer4ScoreBackColor.SelectedColor.ToString();

                    writer.WriteStartElement("Player5ScoreBackColor");
                    writer.WriteString(cmbPlayer5ScoreBackColor.SelectedColor.ToString());
                    writer.WriteEndElement();
                    _Player5ScoreBackColor = cmbPlayer5ScoreBackColor.SelectedColor.ToString();

                    writer.WriteStartElement("Player6ScoreBackColor");
                    writer.WriteString(cmbPlayer6ScoreBackColor.SelectedColor.ToString());
                    writer.WriteEndElement();
                    _Player6ScoreBackColor = cmbPlayer6ScoreBackColor.SelectedColor.ToString();

                    writer.WriteEndDocument();
                    writer.Close();

                    if (sender == null)
                    {
                        grdSettings.Visibility = Visibility.Hidden;
                        Window_Loaded(sender, e);
                        PreviewWallConfigSetting();
                        GetMarketingVideo();
                        btnCloseScreen.Visibility = Visibility.Collapsed;
                        btnSettings.Visibility = Visibility.Collapsed;
                        CheckIsPreviewWall();
                    }
                    // previewTimeCounter = 0;
                    // if (sender != null)
                    else
                    {
                        System.Windows.Forms.MessageBox.Show("Preview Wall details saved Successfully");
                        Process thisProc = Process.GetCurrentProcess();
                        System.Windows.Forms.Application.Restart();
                        thisProc.Kill();
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }

        private void btnReset_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                cmbSubStore.SelectedValue = "0";
                cmbLocation.SelectedValue = "0";
                cmbScreen.SelectedValue = "--Select--";
                cmbRotation.SelectedValue = "0";
                txtRFIDFont.Text = string.Empty;
                cmbLocation.IsEnabled = false;
                cmbScreen.IsEnabled = false;
                cmbRFIDFontWeight.SelectedValue = "Normal";
                cmbPlayerScoreFontWeight.SelectedValue = "Normal";
                cmbRFIDBackColor.SelectedColor = ((System.Windows.Media.Color)System.Windows.Media.ColorConverter.ConvertFromString("White"));
                cmbPlayer1ScoreBackColor.SelectedColor = ((System.Windows.Media.Color)System.Windows.Media.ColorConverter.ConvertFromString("White"));
                cmbPlayer2ScoreBackColor.SelectedColor = ((System.Windows.Media.Color)System.Windows.Media.ColorConverter.ConvertFromString("White"));
                cmbPlayer3ScoreBackColor.SelectedColor = ((System.Windows.Media.Color)System.Windows.Media.ColorConverter.ConvertFromString("White"));
                cmbPlayer4ScoreBackColor.SelectedColor = ((System.Windows.Media.Color)System.Windows.Media.ColorConverter.ConvertFromString("White"));
                cmbPlayer5ScoreBackColor.SelectedColor = ((System.Windows.Media.Color)System.Windows.Media.ColorConverter.ConvertFromString("White"));
                cmbPlayer6ScoreBackColor.SelectedColor = ((System.Windows.Media.Color)System.Windows.Media.ColorConverter.ConvertFromString("White"));
                cmbRFIDForeColor.SelectedColor = ((System.Windows.Media.Color)System.Windows.Media.ColorConverter.ConvertFromString("White"));
                cmbHorizentalBorderWidth.SelectedValue = "X";
                cmbVerticalBorderWidth.SelectedValue = "X";
                txtFont.Text = string.Empty;
                cmbMessageForeColor.SelectedColor = ((System.Windows.Media.Color)System.Windows.Media.ColorConverter.ConvertFromString("White"));
                cmbPosition.SelectedValue = "Top-Left";
                cmbMsgBGColor.SelectedColor = ((System.Windows.Media.Color)System.Windows.Media.ColorConverter.ConvertFromString("White"));
                chkStretchImg.IsChecked = false;
                txtPlayer1ScoreMarginLeft.Text = "0";
                txtPlayer1ScoreMarginTop.Text = "0";
                txtPlayer1ScoreMarginRight.Text = "0";
                txtPlayer1ScoreMarginBottom.Text = "0";
                txtPlayer2ScoreMarginLeft.Text = "0";
                txtPlayer2ScoreMarginTop.Text = "0";
                txtPlayer2ScoreMarginRight.Text = "0";
                txtPlayer2ScoreMarginBottom.Text = "0";
                txtPlayer3ScoreMarginLeft.Text = "0";
                txtPlayer3ScoreMarginTop.Text = "0";
                txtPlayer3ScoreMarginRight.Text = "0";
                txtPlayer3ScoreMarginBottom.Text = "0";
                txtPlayer4ScoreMarginLeft.Text = "0";
                txtPlayer4ScoreMarginTop.Text = "0";
                txtPlayer4ScoreMarginRight.Text = "0";
                txtPlayer4ScoreMarginBottom.Text = "0";
                txtPlayer5ScoreMarginLeft.Text = "0";
                txtPlayer5ScoreMarginTop.Text = "0";
                txtPlayer5ScoreMarginRight.Text = "0";
                txtPlayer5ScoreMarginBottom.Text = "0";
                txtPlayer6ScoreMarginLeft.Text = "0";
                txtPlayer6ScoreMarginTop.Text = "0";
                txtPlayer6ScoreMarginRight.Text = "0";
                txtPlayer6ScoreMarginBottom.Text = "0";
                txtPhotoMarginLeft.Text = "0";
                txtPhotoMarginTop.Text = "0";
                txtPhotoMarginRight.Text = "0";
                txtPhotoMarginBottom.Text = "0";
                txtMessageMarginLeft.Text = "0";
                txtMessageMarginTop.Text = "0";
                txtMessageMarginRight.Text = "0";
                txtMessageMarginBottom.Text = "0";

            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }
        private void btnSettings_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                PreViewWallMedia.Stop();
                grdSettings.Visibility = Visibility.Visible;
                LoadInitialSettings();
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }
        private void btnClose_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                PreViewWallMedia.Play();
                grdSettings.Visibility = Visibility.Collapsed;
                btnCloseScreen.Visibility = Visibility.Visible;
                btnSettings.Visibility = Visibility.Visible;
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }
        private void btnCloseScreen_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Process thisProc = Process.GetCurrentProcess();
                // Check how many total processes have the same name as the current one
                if (Process.GetProcessesByName(thisProc.ProcessName).Length >= 1)
                {
                    PreViewWallMedia.Stop();
                    System.Windows.Application.Current.Shutdown();
                    thisProc.Kill();
                }
                //PreViewWallMedia.Stop();
                //System.Windows.Application.Current.Shutdown();
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
            }

        }
        private void txtRFIDFont_KeyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {
            try
            {
                e.Handled = !IsNumberKey(e.Key);
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
            }
        }

        private void PreViewWallMedia_MediaEnded(object sender, RoutedEventArgs e)
        {
            try
            {
                if (_isMktImagesActive)
                    PlayMarketingMedia();
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
            }

        }

        private void tmr_Tick(object sender, EventArgs e)
        {
            try
            {
                if (counter > 0 && counter <= Convert.ToInt32(_MsgShowTime))
                {
                    counter++;
                }
                else if (counter > Convert.ToInt32(_MsgShowTime))
                {
                    counter = 0;
                    lblRFIDAssociate.Visibility = Visibility.Collapsed;
                }

                GetGuestMediaFromFolder();
                if (_gusetImgList.Count > 0 & IsGuestMediaAvailable())
                {
                    if (previewTimeCounter != _previewWallInfoSetting.PreviewTimePW && isPlayGuestImg && _guestImgCount != 0)
                    {
                        if (previewTimeCounter == _previewWallInfoSetting.HighLightTimePW)
                            BoarderBlink(false);
                        if (_gusetImgList.Count == _guestImgCount && previewTimeCounter == _previewWallInfoSetting.HighLightTimePW - 1
                            && _previewWallInfoSetting.PreviewTimePW == _previewWallInfoSetting.HighLightTimePW)
                            BoarderBlink(false);
                        previewTimeCounter++;
                        return;
                    }
                    else
                    {
                        PlayGuestMedia();
                    }
                }
                else
                {
                    if ((!ismktActive && _previewWallInfoSetting.IsMktImgPW && _gusetImgList.Count == _guestImgCount) || (previewTimeCounter == _previewWallInfoSetting.PreviewTimePW))
                    {
                        previewTimeCounter = 0;
                        PlayMarketingMedia();
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
            }
        }


        // bool isDummyTagChk = false;
        private void txtBarcode_KeyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {
            try
            {
                if (e.Key == Key.Enter)
                {
                    if (lblRFID.Visibility == Visibility.Visible)
                    {
                        if (txtBarcode.Text.Length < 20)
                            QRCODE = txtBarcode.Text.Trim();
                        else if (!string.IsNullOrEmpty(_QrCodeURL))
                            QRCODE = txtBarcode.Text.Replace(_QrCodeURL, string.Empty);
                        if (_dummyTag != null && _dummyTag.Count > 0)
                        {
                            var IsDummyTag = _dummyTag.Where(x => x.ConfigurationValue == QRCODE).FirstOrDefault();
                            if (IsDummyTag != null)
                            {
                                ResetScannSetting();
                                if (_guestImgCount == _gusetImgList.Count)
                                {
                                    ControlVisibilityFalse();
                                    imgWaterMarkControl.Visibility = Visibility.Collapsed;
                                    SetFrame(false);
                                    _mediaPlayPath = string.Empty;
                                    PreViewWallMedia.Source = null;
                                    BoarderBlink(false);
                                    btnCloseScreen.Visibility = Visibility.Visible;
                                    btnSettings.Visibility = Visibility.Visible;
                                    previewTimeCounter = _previewWallInfoSetting.PreviewTimePW;
                                    if (_previewWallInfoSetting.IsMktImgPW && _mrkMediaList.Count > 0)
                                        dummyTag = true;
                                    //if (_previewWallInfoSetting.IsMktImgPW && _mrkMediaList.Count > 0)
                                    //{
                                    //    if (_mktMediaCount == _mrkMediaList.Count)
                                    //        _mktMediaCount = 0;
                                    //    _mediaPlayPath = _mrkMediaList[_mktMediaCount].FullName;
                                    //    lblRFID.Visibility = Visibility.Collapsed;
                                    //    imgWaterMarkControl.Visibility = Visibility.Collapsed;
                                    //    BoarderBlink(false);
                                    //    _mrkMediaPreviewTime = false;
                                    //    PlayFile(_mediaPlayPath);
                                    //    System.Threading.Thread.Sleep(_marketingMediaReloadTime);
                                    //    _mktMediaCount++;
                                    //    //isPlayGuestImg = false;
                                    //    //_isMktImagesActive = true;
                                    //    //ismktActive = true;
                                    //}
                                }
                                else
                                {
                                    previewTimeCounter = 0;
                                    btnCloseScreen.Visibility = Visibility.Collapsed;
                                    btnSettings.Visibility = Visibility.Collapsed;
                                    PlayGuestMedia();
                                    //isDummyTagChk = false;
                                }
                                return;
                            }

                        }
                        if (!string.IsNullOrEmpty(QRCODE) && QRCODE.Length < 20)
                        {
                            result = new AssociateImageBusiness().AssociateImage(406, "2222" + QRCODE, _photoId, RFIDOverWritesStatus, IsAnonymousQrCodeEnabled,0,string.Empty);
                            if (result == "1")
                            {
                                lblRFIDAssociate.Content = "Success";
                                //lblRFIDAssociate.Visibility = Visibility.Visible;
                                lblRFIDAssociate.Visibility = Visibility.Collapsed;//By Vins

                                imgRFIDAssociate.Visibility = Visibility.Visible;//By Vins
                                counter = counter + 1;
                            }
                            else
                            {
                                lblRFIDAssociate.Content = "Failure";
                                lblRFIDAssociate.Visibility = Visibility.Visible;
                                counter = counter + 1;
                            }

                        }
                        ResetScannSetting();
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
            }
        }

        private void hoverRect_MouseEnter(object sender, System.Windows.Input.MouseEventArgs e)
        {
            try
            {
                if (btnCloseScreen.Visibility == Visibility.Collapsed)
                {
                    btnCloseScreen.Visibility = Visibility.Visible;
                    btnSettings.Visibility = Visibility.Visible;
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
            }
        }
        private void PreViewWallMedia_MouseDown(object sender, MouseButtonEventArgs e)
        {
            try
            {
                EnableSetting();
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
            }
        }

        private void imgBlinkVertical_MouseDown(object sender, MouseButtonEventArgs e)
        {
            try
            {
                EnableSetting();
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
            }
        }
        private void imgHorizentalFrame_MouseDown(object sender, MouseButtonEventArgs e)
        {
            try
            {
                EnableSetting();
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
            }
        }
        private void imgVerticalFrame_MouseDown(object sender, MouseButtonEventArgs e)
        {
            try
            {
                EnableSetting();
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
            }
        }


        private void imgBlinkHorizental_MouseDown(object sender, MouseButtonEventArgs e)
        {
            try
            {
                EnableSetting();
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
            }
        }
        private void imgWaterMarkControl_MouseDown(object sender, MouseButtonEventArgs e)
        {
            try
            {
                EnableSetting();
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
            }
        }


        #endregion

        #region Common Methods
        private void CheckIsPreviewWall()
        {
            if (_previewWallInfoSetting != null && !_previewWallInfoSetting.IsPreviewEnabledPW)
            {
                btnCloseScreen.Visibility = Visibility.Visible;
                btnSettings.Visibility = Visibility.Collapsed;
                grdSettings.Visibility = Visibility.Collapsed;
                PreViewWallMedia.Visibility = Visibility.Collapsed;
                imgWaterMarkControl.Visibility = Visibility.Collapsed;
                imgLogoControl.Visibility = Visibility.Collapsed;
                imgBlinkVertical.Visibility = Visibility.Collapsed;
                imgBlinkHorizental.Visibility = Visibility.Collapsed;
                imgHorizentalFrame.Visibility = Visibility.Collapsed;
                imgVerticalFrame.Visibility = Visibility.Collapsed;
                if (!_showMsg)
                {
                    System.Windows.MessageBox.Show("Please enable Preview Wall setting from digiconfig..");
                    _showMsg = true;
                }
            }
            if (_gusetImgList != null && _mrkMediaList != null && _gusetImgList.Count == 0 && _mrkMediaList.Count == 0)
            {
                btnCloseScreen.Visibility = Visibility.Visible;
                btnSettings.Visibility = Visibility.Visible;
            }

        }
        private void PreviewWallConfigSetting()
        {
            ConfigurationInfo _objdata = (new ConfigBusiness()).GetConfigurationData(Convert.ToInt32(cmbSubStore.SelectedValue));
            if (_objdata != null)
            {
                //string siteName, locationName = string.Empty;//Commmneted by Vins
                List<SubStoresInfo> SiteLst = (new StoreSubStoreDataBusniess()).GetLoginUserDefaultSubstores(Convert.ToInt32(cmbSubStore.SelectedValue));
                if (SiteLst.Count > 0)
                {
                    siteName = SiteLst[0].DG_SubStore_Name;
                    LocationInfo locBiz = (new LocationBusniess()).GetLocationsbyId(Convert.ToInt32(cmbLocation.SelectedValue));
                    if (locBiz != null)
                    {
                        locationName = locBiz.DG_Location_Name;
                        _hotFolderPath = _objdata.DG_Hot_Folder_Path;
                        _guestImgPath = _hotFolderPath + "\\PreviewWall\\" + siteName + "\\" + locationName + "\\" + cmbScreen.SelectedValue;
                        _waterMarkImg = _hotFolderPath + "\\PreviewWall\\" + siteName + "\\" + locationName + "\\" + cmbScreen.SelectedValue + "\\MasterData\\WaterMark.PNG";
                        _verticalFrame = _hotFolderPath + "\\PreviewWall\\" + siteName + "\\" + locationName + "\\" + cmbScreen.SelectedValue + "\\MasterData\\VerticalBorder.PNG";
                        _horizentalFrame = _hotFolderPath + "\\PreviewWall\\" + siteName + "\\" + locationName + "\\" + cmbScreen.SelectedValue + "\\MasterData\\HorizentalBorder.PNG";
                        _logoImg = _hotFolderPath + "\\PreviewWall\\" + siteName + "\\" + locationName + "\\" + cmbScreen.SelectedValue + "\\MasterData\\Logo.PNG";
                        _gusetImgList.Clear();

                        _locationWiseConfigParamsList = (new ConfigBusiness()).GetLocationWiseConfigParams(Convert.ToInt32(cmbSubStore.SelectedValue));
                        List<iMixConfigurationLocationInfo> lstLocation = _locationWiseConfigParamsList.Where(n => n.LocationId == Convert.ToInt32(cmbLocation.SelectedValue)).ToList();
                        _previewWallInfoSetting = new PreviewWallInfo();

                        #region Added By VinS 30Aug2019
                        //Set the values for GuestExplanatoryImgPathPW, IsHorizontalGuestExplatory, IsLoopPreviewPhotos, LoopPreviewPhotosTime 
                        _previewWallInfoSetting.GuestExplanatoryImgPathPW = "";
                        _previewWallInfoSetting.IsHorizontalGuestExplatory = false;
                        _previewWallInfoSetting.IsLoopPreviewPhotos = true;
                        _previewWallInfoSetting.LoopPreviewPhotosTime = "";
                        //
                        #endregion


                        foreach (var loc in lstLocation)
                        {
                            switch (loc.IMIXConfigurationMasterId)
                            {
                                case (int)ConfigParams.NoOfScreenPW:
                                    _previewWallInfoSetting.NoOfScreenPW = Convert.ToInt32(loc.ConfigurationValue);
                                    break;
                                case (int)ConfigParams.DelayTimePW:
                                    _previewWallInfoSetting.DelayTimePW = Convert.ToInt32(loc.ConfigurationValue);
                                    break;
                                case (int)ConfigParams.PreviewTimePW:
                                    _previewWallInfoSetting.PreviewTimePW = Convert.ToInt32(loc.ConfigurationValue) - _previewHighlightAdjustTime;
                                    break;
                                case (int)ConfigParams.HighLightTimePW:
                                    _previewWallInfoSetting.HighLightTimePW = Convert.ToInt32(loc.ConfigurationValue) - _previewHighlightAdjustTime;
                                    break;
                                case (int)ConfigParams.IsMktImgPW:
                                    _previewWallInfoSetting.IsMktImgPW = Convert.ToBoolean(loc.ConfigurationValue);
                                    break;
                                case (int)ConfigParams.WaterMarkPathPW:
                                    _previewWallInfoSetting.WaterMarkPathPW = loc.ConfigurationValue;
                                    break;
                                case (int)ConfigParams.IsLogoPW:
                                    _previewWallInfoSetting.IsLogoPW = Convert.ToBoolean(loc.ConfigurationValue);
                                    break;
                                case (int)ConfigParams.LogoPathPW:
                                    _previewWallInfoSetting.LogoPathPW = loc.ConfigurationValue;
                                    break;
                                case (int)ConfigParams.IsPreviewEnabledPW:
                                    _previewWallInfoSetting.IsPreviewEnabledPW = Convert.ToBoolean(loc.ConfigurationValue);
                                    break;
                                case (int)ConfigParams.IsSpecImgPW:
                                    _previewWallInfoSetting.IsSpecImgPW = Convert.ToBoolean(loc.ConfigurationValue);
                                    break;
                                case (int)ConfigParams.MktImgPathPW:
                                    _previewWallInfoSetting.MarketingImgPathPW = loc.ConfigurationValue;
                                    _mktImgPath = loc.ConfigurationValue;
                                    break;
                                case (int)ConfigParams.HorizentalBorderPW:
                                    _previewWallInfoSetting.HorizentalBorderPathPW = loc.ConfigurationValue;
                                    break;
                                case (int)ConfigParams.VerticalBorderPW:
                                    _previewWallInfoSetting.VerticalBorderPathPW = loc.ConfigurationValue;
                                    break;
                                case (int)ConfigParams.PreviewWallLogoPosition:
                                    _previewWallInfoSetting.LogoPosition = loc.ConfigurationValue;
                                    break;
                                case (int)ConfigParams.PreviewWallRFIDPostion:
                                    _previewWallInfoSetting.RFIDPostion = loc.ConfigurationValue;
                                    break;
                                case (int)ConfigParams.IsVisibleGumBallZeroScore:
                                    _previewWallInfoSetting.IsVisibleGumBallZeroScore = loc.ConfigurationValue;
                                    if (!string.IsNullOrEmpty(loc.ConfigurationValue))
                                        _isVisibleGumBallZeroScore = loc.ConfigurationValue;
                                    break;
                                case (int)ConfigParams.GuestExplanatoryImgPathPW:
                                    _previewWallInfoSetting.GuestExplanatoryImgPathPW = loc.ConfigurationValue;
                                    break;
                                case (int)ConfigParams.IsLoopPreviewPhotos:
                                    _previewWallInfoSetting.IsLoopPreviewPhotos = Convert.ToBoolean(loc.ConfigurationValue);
                                    break;
                                case (int)ConfigParams.IsHorizontalGuestExplatory:
                                    _previewWallInfoSetting.IsHorizontalGuestExplatory = Convert.ToBoolean(loc.ConfigurationValue);
                                    break;
                                case (int)ConfigParams.LoopPreviewPhotosTime:
                                    _previewWallInfoSetting.LoopPreviewPhotosTime = loc.ConfigurationValue;
                                    break;
                                case (int)ConfigParams.RideNoOfCaptures:
                                    _previewWallInfoSetting.RideNoOfCaptures = loc.ConfigurationValue;
                                    break;
                                case (int)ConfigParams.RideNoOfCapturesIterations:
                                    _previewWallInfoSetting.RideNoOfCapturesIterations = loc.ConfigurationValue;
                                    break;
                            }
                        }
                        ImageLoad();
                        SetLogoPosition();
                        SetRFIDPosition();
                        SetAssociatedRFIdPosition();
                        CleanPreviewGuestFile();
                        SetGuestExplanatoryImgPosition();//Added by Vins_30Aug2019
                    }
                }
                _dummyTag = ((new ConfigBusiness()).GetStoreConfigData()).Where(x => x.IMIXConfigurationMasterId == Convert.ToInt64(ConfigParams.PreviewWallDummyTag)).ToList();

            }
        }

        private void gif_MediaEnded(object sender, RoutedEventArgs e)
        {
            imgHorzGuestExplain.Position = new TimeSpan(0, 0, 1);
            imgHorzGuestExplain.Play();

            imgVertGuestExplain.Position = new TimeSpan(0, 0, 1);
            imgVertGuestExplain.Play();
        }

        private void SetGuestExplanatoryImgPosition()
        {
            try
            {


                //imgHorzGuestExplain imgVertGuestExplain
                if (_previewWallInfoSetting.IsHorizontalGuestExplatory)
                {
                    imgHorzGuestExplain.Visibility = Visibility.Visible;
                    imgVertGuestExplain.Visibility = Visibility.Collapsed;

                    _guestExplainatoryImg = "\\" + _hotFolderPath.Replace("\\\\", "\\") + "PreviewWall\\" + siteName + "\\" + locationName + "\\" + cmbScreen.SelectedValue + "\\MasterData\\HorizontalScan.gif";

                    //string _guest = string.Empty;
                    //_guestExplainatoryImg = _hotFolderPath + "\\PreviewWall\\" + siteName + "\\" + locationName + "\\" + cmbScreen.SelectedValue + "\\MasterData\\HorizontalScan.gif";

                    if (string.IsNullOrEmpty(siteName) || string.IsNullOrEmpty(locationName))
                    {
                        _guestExplainatoryImg = "\\" + _hotFolderPath.Replace("\\\\", "\\") + "PreviewWall\\" + cmbScreen.SelectedValue + "\\MasterData\\HorizontalScan.gif";
                        _guestExplainatoryImg = _guestExplainatoryImg.Trim();
                    }
                    else
                    {
                        _guestExplainatoryImg = "\\" + _hotFolderPath.Replace("\\\\", "\\") + "PreviewWall\\" + siteName + "\\" + locationName + "\\" + cmbScreen.SelectedValue + "\\MasterData\\HorizontalScan.gif";
                        _guestExplainatoryImg = _guestExplainatoryImg.Trim();
                    }
                    if (File.Exists(_guestExplainatoryImg))
                    {
                        ErrorHandler.ErrorHandler.LogFileWrite("Gumball ride File exists");
                        imgHorzGuestExplain.Source = new Uri(_guestExplainatoryImg, UriKind.RelativeOrAbsolute);
                    }
                }
                else if (!_previewWallInfoSetting.IsHorizontalGuestExplatory)
                {
                    imgVertGuestExplain.Visibility = Visibility.Visible;
                    imgHorzGuestExplain.Visibility = Visibility.Collapsed;

                    if (string.IsNullOrEmpty(siteName) || string.IsNullOrEmpty(locationName))
                    {
                        _guestExplainatoryImg = "\\" + _hotFolderPath.Replace("\\\\", "\\") + "PreviewWall\\" + cmbScreen.SelectedValue + "\\MasterData\\VerticalScan.gif";
                        _guestExplainatoryImg = _guestExplainatoryImg.Trim();
                    }
                    else
                    {
                        _guestExplainatoryImg = "\\" + _hotFolderPath.Replace("\\\\", "\\") + "\\PreviewWall\\" + siteName + "\\" + locationName + "\\" + cmbScreen.SelectedValue + "\\MasterData\\VerticalScan.gif";
                        _guestExplainatoryImg = _guestExplainatoryImg.Trim();
                    }
                    //var gstExplainatoryImg = new BitmapImage();
                    //gstExplainatoryImg.BeginInit();
                    //gstExplainatoryImg.UriSource = new Uri(_guestExplainatoryImg, UriKind.RelativeOrAbsolute);
                    //gstExplainatoryImg.EndInit();
                    //ImageBehavior.SetAnimatedSource(imgVertGuestExplain, gstExplainatoryImg);
                    //ImageBehavior.SetRepeatBehavior(imgVertGuestExplain, new RepeatBehavior(0));
                    //ImageBehavior.SetRepeatBehavior(imgVertGuestExplain, RepeatBehavior.Forever);
                    
                    
                    if (File.Exists(_guestExplainatoryImg))
                    {
                        imgVertGuestExplain.Source = new Uri(_guestExplainatoryImg, UriKind.RelativeOrAbsolute);
                    }

                }
            }
            catch (Exception ex)
            {

            }
        }


        private void SetAssociatedRFIdPosition()
        {
            if (_MSGPosition == "Top-Left")
            {
                RFIDAssociateStackPanel.VerticalAlignment = System.Windows.VerticalAlignment.Top;
                RFIDAssociateStackPanel.HorizontalAlignment = System.Windows.HorizontalAlignment.Left;
            }
            else if (_MSGPosition == "Top-Center")
            {
                RFIDAssociateStackPanel.VerticalAlignment = System.Windows.VerticalAlignment.Top;
                RFIDAssociateStackPanel.HorizontalAlignment = System.Windows.HorizontalAlignment.Center;
            }
            else if (_MSGPosition == "Top-Right")
            {
                RFIDAssociateStackPanel.VerticalAlignment = System.Windows.VerticalAlignment.Top;
                RFIDAssociateStackPanel.HorizontalAlignment = System.Windows.HorizontalAlignment.Right;
            }
            else if (_MSGPosition == "Bottom-Left")
            {
                RFIDAssociateStackPanel.VerticalAlignment = System.Windows.VerticalAlignment.Bottom;
                RFIDAssociateStackPanel.HorizontalAlignment = System.Windows.HorizontalAlignment.Left;
            }
            else if (_MSGPosition == "Bottom-Center")
            {
                RFIDAssociateStackPanel.VerticalAlignment = System.Windows.VerticalAlignment.Bottom;
                RFIDAssociateStackPanel.HorizontalAlignment = System.Windows.HorizontalAlignment.Center;
            }
            else if (_MSGPosition == "Bottom-Right")
            {
                RFIDAssociateStackPanel.VerticalAlignment = System.Windows.VerticalAlignment.Bottom;
                RFIDAssociateStackPanel.HorizontalAlignment = System.Windows.HorizontalAlignment.Right;
            }
            else if (_MSGPosition == "Center")
            {
                RFIDAssociateStackPanel.VerticalAlignment = System.Windows.VerticalAlignment.Center;
                RFIDAssociateStackPanel.HorizontalAlignment = System.Windows.HorizontalAlignment.Center;
            }
            RFIDAssociateStackPanel.Margin = new Thickness(Convert.ToDouble(txtMessageMarginLeft.Text.Trim()), Convert.ToDouble(txtMessageMarginTop.Text.Trim()),
                                              Convert.ToDouble(txtMessageMarginRight.Text.Trim()), Convert.ToDouble(txtMessageMarginBottom.Text.Trim()));
        }
        private void SetLogoPosition()
        {
            if (_previewWallInfoSetting.LogoPosition == "Top Left")
            {
                imgLogoControl.VerticalAlignment = System.Windows.VerticalAlignment.Top;
                imgLogoControl.HorizontalAlignment = System.Windows.HorizontalAlignment.Left;
            }
            else if (_previewWallInfoSetting.LogoPosition == "Top Centre")
            {
                imgLogoControl.VerticalAlignment = System.Windows.VerticalAlignment.Top;
                imgLogoControl.HorizontalAlignment = System.Windows.HorizontalAlignment.Center;
            }
            else if (_previewWallInfoSetting.LogoPosition == "Top Right")
            {
                imgLogoControl.VerticalAlignment = System.Windows.VerticalAlignment.Top;
                imgLogoControl.HorizontalAlignment = System.Windows.HorizontalAlignment.Right;
            }
            else if (_previewWallInfoSetting.LogoPosition == "Bottom Left")
            {
                imgLogoControl.VerticalAlignment = System.Windows.VerticalAlignment.Bottom;
                imgLogoControl.HorizontalAlignment = System.Windows.HorizontalAlignment.Left;
            }
            else if (_previewWallInfoSetting.LogoPosition == "Bottom Centre")
            {
                imgLogoControl.VerticalAlignment = System.Windows.VerticalAlignment.Bottom;
                imgLogoControl.HorizontalAlignment = System.Windows.HorizontalAlignment.Center;
            }
            else if (_previewWallInfoSetting.LogoPosition == "Bottom Right")
            {
                imgLogoControl.VerticalAlignment = System.Windows.VerticalAlignment.Bottom;
                imgLogoControl.HorizontalAlignment = System.Windows.HorizontalAlignment.Right;
            }
        }
        private void SetRFIDPosition()
        {
            if (_previewWallInfoSetting.RFIDPostion == "Top Left")
            {
                RFIDStackPanel.VerticalAlignment = System.Windows.VerticalAlignment.Top;
                RFIDStackPanel.HorizontalAlignment = System.Windows.HorizontalAlignment.Left;
            }
            else if (_previewWallInfoSetting.RFIDPostion == "Top Centre")
            {
                RFIDStackPanel.VerticalAlignment = System.Windows.VerticalAlignment.Top;
                RFIDStackPanel.HorizontalAlignment = System.Windows.HorizontalAlignment.Center;
            }
            else if (_previewWallInfoSetting.RFIDPostion == "Top Right")
            {
                RFIDStackPanel.VerticalAlignment = System.Windows.VerticalAlignment.Top;
                RFIDStackPanel.HorizontalAlignment = System.Windows.HorizontalAlignment.Right;
            }
            else if (_previewWallInfoSetting.RFIDPostion == "Bottom Left")
            {
                RFIDStackPanel.VerticalAlignment = System.Windows.VerticalAlignment.Bottom;
                RFIDStackPanel.HorizontalAlignment = System.Windows.HorizontalAlignment.Left;
            }
            else if (_previewWallInfoSetting.RFIDPostion == "Bottom Centre")
            {
                RFIDStackPanel.VerticalAlignment = System.Windows.VerticalAlignment.Bottom;
                RFIDStackPanel.HorizontalAlignment = System.Windows.HorizontalAlignment.Center;
            }
            else if (_previewWallInfoSetting.RFIDPostion == "Bottom Right")
            {
                RFIDStackPanel.VerticalAlignment = System.Windows.VerticalAlignment.Bottom;
                RFIDStackPanel.HorizontalAlignment = System.Windows.HorizontalAlignment.Right;
            }
        }
        private void CleanPreviewGuestFile()
        {
            _lastCleanUpTime = DateTime.Now.AddMinutes(_previewWallCleanUpTimeInMinute);
            var myDirInfo = new DirectoryInfo(_guestImgPath);
            var filelist = myDirInfo.GetFilesByExtensions(".jpg", ".png", ".mp4",
                                            ".wmv", ".mpg", ".avi", ".3gp").ToList();
            filelist = filelist.Where(s => s.Attributes == FileAttributes.Offline).ToList();
            if (filelist != null && filelist.Count > 0)
            {
                foreach (FileInfo delFile in filelist)
                    File.Delete(delFile.FullName);
            }
            //for (int i = 0; i <= filelist.Count; i++)
            //{
            //    File.Delete(filelist[i].FullName);
            //}
        }

        private void ResetOldSetting()
        {
            latestGusetImageList.Clear();
            _mktMediaCount = 0;
            _guestImgCount = 0;
            _lastPickTime = DateTime.MinValue;
        }
        private void BoarderBlink(bool blinkActive)
        {
            if (!string.IsNullOrEmpty(_previewWallInfoSetting.HighLightTimePW.ToString()))
            {
                if (blinkActive)
                {
                    if (_rotationAngle == "90" || _rotationAngle == "270")
                    {
                        imgBlinkVertical.Visibility = Visibility.Visible;
                        imgBlinkHorizental.Visibility = Visibility.Hidden;
                    }
                    else
                    {
                        imgBlinkVertical.Visibility = Visibility.Hidden;
                        imgBlinkHorizental.Visibility = Visibility.Visible;
                    }
                }
                else
                {
                    imgBlinkVertical.Visibility = Visibility.Hidden;
                    imgBlinkHorizental.Visibility = Visibility.Hidden;
                }
            }
        }
        private void SetFrame(bool blinkActive)
        {
            if (!string.IsNullOrEmpty(_previewWallInfoSetting.VerticalBorderPathPW) || !string.IsNullOrEmpty(_previewWallInfoSetting.HorizentalBorderPathPW))
            {
                if (blinkActive)
                {
                    if (_rotationAngle == "90" || _rotationAngle == "270")
                    {
                        imgVerticalFrame.Visibility = Visibility.Visible;
                        imgHorizentalFrame.Visibility = Visibility.Hidden;
                    }
                    else
                    {
                        imgVerticalFrame.Visibility = Visibility.Hidden;
                        imgHorizentalFrame.Visibility = Visibility.Visible;
                    }
                }
                else
                {
                    imgHorizentalFrame.Visibility = Visibility.Hidden;
                    imgVerticalFrame.Visibility = Visibility.Hidden;
                }
            }
        }

        private void ResetScannSetting()
        {
            txtBarcode.Text = "";
            QRCODE = "";
            //_photoId = "";
        }

        private void EnableSetting()
        {
            if (btnCloseScreen.Visibility == Visibility.Hidden || btnCloseScreen.Visibility == Visibility.Collapsed)
            {
                btnCloseScreen.Visibility = Visibility.Visible;
                btnSettings.Visibility = Visibility.Visible;
            }
            else
            {
                btnCloseScreen.Visibility = Visibility.Hidden;
                btnSettings.Visibility = Visibility.Hidden;
            }

        }
        private void ImageLoad()
        {
            if (Directory.Exists(_guestImgPath))
            {
                if (!string.IsNullOrEmpty(_previewWallInfoSetting.WaterMarkPathPW))
                {
                    if (File.Exists(_waterMarkImg))
                        imgWaterMarkControl.Source = new BitmapImage(new Uri(_waterMarkImg));//,UriKind.Absolute));
                    else
                        imgWaterMarkControl.Source = null;
                }
                if (!string.IsNullOrEmpty(_previewWallInfoSetting.HorizentalBorderPathPW))
                {
                    if (File.Exists(_horizentalFrame))
                        imgHorizentalFrame.Source = new BitmapImage(new Uri(_horizentalFrame));//,UriKind.Absolute));
                    else
                        imgHorizentalFrame.Source = null;
                }
                if (!string.IsNullOrEmpty(_previewWallInfoSetting.VerticalBorderPathPW))
                {
                    if (File.Exists(_verticalFrame))
                        imgVerticalFrame.Source = new BitmapImage(new Uri(_verticalFrame));//,UriKind.Absolute));
                    else
                        imgVerticalFrame.Source = null;
                }
                if (_previewWallInfoSetting.IsLogoPW == true)
                {
                    if (File.Exists(_logoImg))
                        imgLogoControl.Source = new BitmapImage(new Uri(_logoImg));//, UriKind.Absolute));
                    else
                        imgLogoControl.Source = null;
                }
            }
            ReloadGuestImgTimer();
        }
        public void PlayMarketingMedia()
        {
            _mediaPlayPath = string.Empty;
            if (_isMktImagesActive && _previewWallInfoSetting.IsMktImgPW && _mrkMediaList.Count > 0)
            {
                if (_mktMediaCount == _mrkMediaList.Count)
                    _mktMediaCount = 0;
                _mediaPlayPath = _mrkMediaList[_mktMediaCount].FullName;
                ControlVisibilityFalse();
                imgWaterMarkControl.Visibility = Visibility.Collapsed;
                SetFrame(false);
                BoarderBlink(false);
                _mrkMediaPreviewTime = false;
                PreViewWallMedia.Margin = new Thickness(0, 0, 0, 0);
                PlayFile(_mediaPlayPath);
                System.Threading.Thread.Sleep(_marketingMediaReloadTime);
                _mktMediaCount++;
                isPlayGuestImg = false;
                _isMktImagesActive = true;
                ismktActive = true;
            }
        }

        private void ControlVisibilityFalse()
        {
            lblRFID.Visibility = Visibility.Collapsed;
            StackPanelPlayer1Score.Visibility = Visibility.Collapsed;
            StackPanelPlayer2Score.Visibility = Visibility.Collapsed;
            StackPanelPlayer3Score.Visibility = Visibility.Collapsed;
            StackPanelPlayer4Score.Visibility = Visibility.Collapsed;
            StackPanelPlayer5Score.Visibility = Visibility.Collapsed;
            StackPanelPlayer6Score.Visibility = Visibility.Collapsed;
        }
        private void ControlVisibilityTrue()
        {
            lblRFID.Visibility = Visibility.Visible;
            StackPanelPlayer1Score.Visibility = Visibility.Visible;
            StackPanelPlayer2Score.Visibility = Visibility.Visible;
            StackPanelPlayer3Score.Visibility = Visibility.Visible;
            StackPanelPlayer4Score.Visibility = Visibility.Visible;
            StackPanelPlayer5Score.Visibility = Visibility.Visible;
            StackPanelPlayer6Score.Visibility = Visibility.Visible;
        }
        private void txtRFIDFont_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = !IsTextAllowed(e.Text);
        }
        private static bool IsTextAllowed(string text)
        {
            Regex regex = new Regex("[^0-9]+");
            return !regex.IsMatch(text);
        }
        private void PlayFile(string fileName)
        {
            PreViewWallMedia.Stop();
            PreViewWallMedia.Source = new Uri(_mediaPlayPath, UriKind.Relative);
            PreViewWallMedia.Play();
        }
        private void GetGuestMediaFromFolder()
        {
            if (!string.IsNullOrEmpty(_guestImgPath))
            {
                _currentDateTime = DateTime.Now.AddSeconds(-_previewWallInfoSetting.DelayTimePW);
                var myDirInfo = new DirectoryInfo(_guestImgPath);
                latestGusetImageList = myDirInfo.GetFilesByExtensions(".jpg", ".png", ".mp4",
                                                ".wmv", ".mpg", ".avi", ".3gp", ".JPG", ".PNG", ".MP4",
                                                ".WMV", ".MPG", ".AVI", ".3GP").ToList();
                latestGusetImageList = latestGusetImageList.Where(file => file.CreationTime <= _currentDateTime && file.CreationTime > _lastPickTime && file.Attributes != FileAttributes.Offline).OrderBy(file => file.CreationTime).ToList();

                if (latestGusetImageList.Count > 0)
                {
                    TimeSpan diff = DateTime.Now - lastFilePlayTime;
                    if (diff.Seconds >= _previewWallInfoSetting.PreviewTimePW)
                        previewTimeCounter = _previewWallInfoSetting.PreviewTimePW;
                    _gusetImgList.AddRange(latestGusetImageList);
                    _gusetImgList = _gusetImgList.DistinctBy(p => new { p.FullName, p.Name }).ToList();
                    _lastPickTime = _currentDateTime.AddSeconds(-2);
                    needToLoop = true;
                    dummyTag = false;
                    //isDummyTagChk = true;
                }
                if (_lastCleanUpTime < DateTime.Now)
                {
                    CleanPreviewGuestFile();
                }
                //_lastPickTime = _currentDateTime.AddSeconds(-2);
            }

        }

        private string PlayGuestMedia()
        {
            ControlVisibilityFalse();
            counter = 0;
            int k = 0;
            if (!string.IsNullOrEmpty(_guestImgPath))
            {
                isPlayGuestImg = false;
                for (k = _guestImgCount; k < _gusetImgList.Count; k++)
                {
                    isPlayGuestImg = true;
                    _guestImgCount++;
                    _mediaPlayPath = _gusetImgList[k].FullName;

                    if (!string.IsNullOrEmpty(_mediaPlayPath) && System.IO.File.Exists(_mediaPlayPath))
                    {
                        lblRFID.Content = _gusetImgList[k].Name.Split('@')[0].ToString();
                        _photoId = _gusetImgList[k].Name.Split('@')[1].ToString().Split('.')[0].ToString();  //.Replace(".jpg", "").Trim();
                        _playerScore = (new PhotoBusiness()).GetPhotoPlayerScore(_photoId);
                        if (!string.IsNullOrEmpty(_playerScore))
                        {
                            txtPlayer1Score.Text = "0";
                            txtPlayer2Score.Text = "0";
                            txtPlayer3Score.Text = "0";
                            txtPlayer4Score.Text = "0";
                            txtPlayer5Score.Text = "0";
                            txtPlayer6Score.Text = "0";
                            ControlVisibilityFalse();
                            string[] ScoreByPlayer = _playerScore.Split(',');
                            for (int i = 1; i <= ScoreByPlayer.Length; i++)
                            {
                                if (i == 1)
                                {
                                    if (ScoreByPlayer[0] != "0" || _isVisibleGumBallZeroScore.ToUpper() == "TRUE")
                                    {
                                        txtPlayer1Score.Text = ScoreByPlayer[0];
                                        StackPanelPlayer1Score.Visibility = Visibility.Visible;
                                    }
                                }
                                else if (i == 2)
                                {
                                    if (ScoreByPlayer[1] != "0" || _isVisibleGumBallZeroScore.ToUpper() == "TRUE")
                                    {
                                        txtPlayer2Score.Text = ScoreByPlayer[1];
                                        StackPanelPlayer2Score.Visibility = Visibility.Visible;
                                    }
                                }
                                else if (i == 3)
                                {
                                    if (ScoreByPlayer[2] != "0" || _isVisibleGumBallZeroScore.ToUpper() == "TRUE")
                                    {
                                        txtPlayer3Score.Text = ScoreByPlayer[2];
                                        StackPanelPlayer3Score.Visibility = Visibility.Visible;
                                    }
                                }
                                else if (i == 4)
                                {
                                    if (ScoreByPlayer[3] != "0" || _isVisibleGumBallZeroScore.ToUpper() == "TRUE")
                                    {
                                        txtPlayer4Score.Text = ScoreByPlayer[3];
                                        StackPanelPlayer4Score.Visibility = Visibility.Visible;
                                    }
                                }
                                else if (i == 5)
                                {
                                    if (ScoreByPlayer[4] != "0" || _isVisibleGumBallZeroScore.ToUpper() == "TRUE")
                                    {
                                        txtPlayer5Score.Text = ScoreByPlayer[4];
                                        StackPanelPlayer5Score.Visibility = Visibility.Visible;
                                    }
                                }
                                else if (i == 6)
                                {
                                    if (ScoreByPlayer[5] != "0" || _isVisibleGumBallZeroScore.ToUpper() == "TRUE")
                                    {
                                        txtPlayer6Score.Text = ScoreByPlayer[5];
                                        StackPanelPlayer6Score.Visibility = Visibility.Visible;
                                    }
                                }
                            }
                        }
                        break;
                    }
                }
                if (!string.IsNullOrEmpty(_mediaPlayPath) && System.IO.File.Exists(_mediaPlayPath))
                {
                    PreViewWallMedia.Margin = new Thickness(Convert.ToDouble(txtPhotoMarginLeft.Text.Trim()), Convert.ToDouble(txtPhotoMarginTop.Text.Trim()),
                                              Convert.ToDouble(txtPhotoMarginRight.Text.Trim()), Convert.ToDouble(txtPhotoMarginBottom.Text.Trim()));
                    StackPanelPlayer1Score.Margin = new Thickness(Convert.ToDouble(txtPlayer1ScoreMarginLeft.Text.Trim()), Convert.ToDouble(txtPlayer1ScoreMarginTop.Text.Trim())
                        , Convert.ToDouble(txtPlayer1ScoreMarginRight.Text.Trim()), Convert.ToDouble(txtPlayer1ScoreMarginBottom.Text.Trim()));
                    StackPanelPlayer2Score.Margin = new Thickness(Convert.ToDouble(txtPlayer2ScoreMarginLeft.Text.Trim()), Convert.ToDouble(txtPlayer2ScoreMarginTop.Text.Trim())
                        , Convert.ToDouble(txtPlayer2ScoreMarginRight.Text.Trim()), Convert.ToDouble(txtPlayer2ScoreMarginBottom.Text.Trim()));
                    StackPanelPlayer3Score.Margin = new Thickness(Convert.ToDouble(txtPlayer3ScoreMarginLeft.Text.Trim()), Convert.ToDouble(txtPlayer3ScoreMarginTop.Text.Trim())
                        , Convert.ToDouble(txtPlayer3ScoreMarginRight.Text.Trim()), Convert.ToDouble(txtPlayer3ScoreMarginBottom.Text.Trim()));
                    StackPanelPlayer4Score.Margin = new Thickness(Convert.ToDouble(txtPlayer4ScoreMarginLeft.Text.Trim()), Convert.ToDouble(txtPlayer4ScoreMarginTop.Text.Trim())
                        , Convert.ToDouble(txtPlayer4ScoreMarginRight.Text.Trim()), Convert.ToDouble(txtPlayer4ScoreMarginBottom.Text.Trim()));
                    StackPanelPlayer5Score.Margin = new Thickness(Convert.ToDouble(txtPlayer5ScoreMarginLeft.Text.Trim()), Convert.ToDouble(txtPlayer5ScoreMarginTop.Text.Trim())
                        , Convert.ToDouble(txtPlayer5ScoreMarginRight.Text.Trim()), Convert.ToDouble(txtPlayer5ScoreMarginBottom.Text.Trim()));
                    StackPanelPlayer6Score.Margin = new Thickness(Convert.ToDouble(txtPlayer6ScoreMarginLeft.Text.Trim()), Convert.ToDouble(txtPlayer6ScoreMarginTop.Text.Trim())
                        , Convert.ToDouble(txtPlayer6ScoreMarginRight.Text.Trim()), Convert.ToDouble(txtPlayer6ScoreMarginBottom.Text.Trim()));
                    //ControlVisibilityTrue();
                    lblRFID.Visibility = Visibility.Visible;
                    BoarderBlink(true);
                    imgWaterMarkControl.Visibility = Visibility.Collapsed;
                    SetFrame(false);
                    PlayFile(_mediaPlayPath);
                    System.Threading.Thread.Sleep(_guestMediaReloadTime);
                    imgWaterMarkControl.Visibility = Visibility.Visible;
                    SetFrame(true);
                    _mrkMediaPreviewTime = true;
                    lastFilePlayTime = DateTime.Now;
                    _isMktImagesActive = false;
                    previewTimeCounter = 0;
                    _gusetImgList[k].Attributes = FileAttributes.Offline;
                }
            }
            return _mediaPlayPath;
        }

        private bool IsGuestMediaAvailable()
        {
            if (_gusetImgList.Count == 0)
            {
                _isMktImagesActive = true;
                return false;
            }

            if (_gusetImgList.Count > _guestImgCount)
            {
                _isMktImagesActive = false;
                return true;
            }
            else if (_gusetImgList.Count == _guestImgCount && needToLoop)
            {
                if (previewTimeCounter == _previewWallInfoSetting.HighLightTimePW && previewTimeCounter != 0)
                {
                    BoarderBlink(false);
                    return true;
                }
                else if (previewTimeCounter == _previewWallInfoSetting.PreviewTimePW - 1)
                {
                    needToLoop = false;
                    return true;
                }
                else if (dummyTag)
                {
                    _isMktImagesActive = true;
                    return false;
                }
                else if (previewTimeCounter == _previewWallInfoSetting.PreviewTimePW && _gusetImgList.Count == _guestImgCount && _previewWallInfoSetting.IsMktImgPW
                    && _mrkMediaList.Count > 0)
                {
                    _isMktImagesActive = true;
                    needToLoop = false;
                    return false;
                }
                else
                    return true;
            }
            else
            {
                _isMktImagesActive = true;
                return false;

            }
        }
        private bool CheckValidation()
        {
            bool _flag = true;
            if (cmbSubStore.SelectedValue.ToString() == "0")
            {
                System.Windows.MessageBox.Show("Please select Site");
                _flag = false;
            }
            else if (cmbLocation.SelectedValue.ToString() == "0")
            {
                System.Windows.MessageBox.Show("Please select Location");
                _flag = false;
            }
            else if (cmbScreen.SelectedValue.ToString() == "0" || cmbScreen.SelectedValue.ToString() == "--Select--")
            {
                System.Windows.MessageBox.Show("Please select Screen");
                _flag = false;
            }
            //else if (cmbRotation.SelectedValue.ToString() == "--Select--")
            //{
            //    System.Windows.MessageBox.Show("Please select Rotation angle");
            //    _flag = false;
            //}
            //else if (cmbRFIDFontWeight.SelectedValue.ToString() == "--Select--")
            //{
            //    System.Windows.MessageBox.Show("Please select PhotoId font weight");
            //    _flag = false;
            //}
            //else if (cmbPlayerScoreFontWeight.SelectedValue.ToString() == "--Select--")
            //{
            //    System.Windows.MessageBox.Show("Please select PlayerScore font weight");
            //    _flag = false;
            //}
            else if (string.IsNullOrWhiteSpace(txtRFIDFont.Text))
            {
                System.Windows.MessageBox.Show("Please enter PhotoId font size");
                _flag = false;
            }
            else if (Convert.ToInt32(txtRFIDFont.Text) <= 0)
            {
                System.Windows.MessageBox.Show("PhotoId font size should be greater than zero");
                _flag = false;
            }
            else if (Convert.ToInt32(txtPlayerScoreFont.Text) <= 0)
            {
                System.Windows.MessageBox.Show("PlayerScore font size should be greater than zero");
                _flag = false;
            }
            else if (string.IsNullOrWhiteSpace(txtPhotoMarginLeft.Text))
            {
                System.Windows.MessageBox.Show("Photo Margin Left can't be blank");
                _flag = false;
            }
            else if (string.IsNullOrWhiteSpace(txtPhotoMarginTop.Text))
            {
                System.Windows.MessageBox.Show("Photo Margin Top can't be blank");
                _flag = false;
            }
            else if (string.IsNullOrWhiteSpace(txtPhotoMarginRight.Text))
            {
                System.Windows.MessageBox.Show("Photo Margin Right can't be blank");
                _flag = false;
            }
            else if (string.IsNullOrWhiteSpace(txtPhotoMarginBottom.Text))
            {
                System.Windows.MessageBox.Show("Photo Margin Bottom can't be blank");
                _flag = false;
            }
            else if (string.IsNullOrWhiteSpace(txtMessageMarginLeft.Text))
            {
                System.Windows.MessageBox.Show("Message Position Margin Left can't be blank");
                _flag = false;
            }
            else if (string.IsNullOrWhiteSpace(txtMessageMarginTop.Text))
            {
                System.Windows.MessageBox.Show("Message Position Margin Top can't be blank");
                _flag = false;
            }
            else if (string.IsNullOrWhiteSpace(txtMessageMarginRight.Text))
            {
                System.Windows.MessageBox.Show("Message Position Margin Right can't be blank");
                _flag = false;
            }
            else if (string.IsNullOrWhiteSpace(txtMessageMarginBottom.Text))
            {
                System.Windows.MessageBox.Show("Message Position Margin Bottom can't be blank");
                _flag = false;
            }
            else if (string.IsNullOrWhiteSpace(txtFont.Text))
            {
                System.Windows.MessageBox.Show("Message Font Size can't be blank");
                _flag = false;
            }
            else if (string.IsNullOrWhiteSpace(txtMsgShowTime.Text))
            {
                System.Windows.MessageBox.Show("Message Display Time can't be blank");
                _flag = false;
            }
            else if (string.IsNullOrWhiteSpace(txtPlayer1ScoreMarginLeft.Text))
            {
                System.Windows.MessageBox.Show("Player1 Score Margin Left can't be blank");
                _flag = false;
            }
            else if (string.IsNullOrWhiteSpace(txtPlayer1ScoreMarginTop.Text))
            {
                System.Windows.MessageBox.Show("Player1 Score Margin Top can't be blank");
                _flag = false;
            }
            else if (string.IsNullOrWhiteSpace(txtPlayer1ScoreMarginRight.Text))
            {
                System.Windows.MessageBox.Show("Player1 Score Margin Right can't be blank");
                _flag = false;
            }
            else if (string.IsNullOrWhiteSpace(txtPlayer1ScoreMarginBottom.Text))
            {
                System.Windows.MessageBox.Show("Player1 Score Margin Bottom can't be blank");
                _flag = false;
            }
            else if (string.IsNullOrWhiteSpace(txtPlayer2ScoreMarginLeft.Text))
            {
                System.Windows.MessageBox.Show("Player2 Score Margin Left can't be blank");
                _flag = false;
            }
            else if (string.IsNullOrWhiteSpace(txtPlayer2ScoreMarginTop.Text))
            {
                System.Windows.MessageBox.Show("Player2 Score Margin Top can't be blank");
                _flag = false;
            }
            else if (string.IsNullOrWhiteSpace(txtPlayer2ScoreMarginRight.Text))
            {
                System.Windows.MessageBox.Show("Player2 Score Margin Right can't be blank");
                _flag = false;
            }
            else if (string.IsNullOrWhiteSpace(txtPlayer2ScoreMarginBottom.Text))
            {
                System.Windows.MessageBox.Show("Player2 Score Margin Bottom can't be blank");
                _flag = false;
            }
            else if (string.IsNullOrWhiteSpace(txtPlayer3ScoreMarginLeft.Text))
            {
                System.Windows.MessageBox.Show("Player3 Score Margin Left can't be blank");
                _flag = false;
            }
            else if (string.IsNullOrWhiteSpace(txtPlayer3ScoreMarginTop.Text))
            {
                System.Windows.MessageBox.Show("Player3 Score Margin Top can't be blank");
                _flag = false;
            }
            else if (string.IsNullOrWhiteSpace(txtPlayer3ScoreMarginRight.Text))
            {
                System.Windows.MessageBox.Show("Player3 Score Margin Right can't be blank");
                _flag = false;
            }
            else if (string.IsNullOrWhiteSpace(txtPlayer3ScoreMarginBottom.Text))
            {
                System.Windows.MessageBox.Show("Player3 Score Margin Bottom can't be blank");
                _flag = false;
            }
            else if (string.IsNullOrWhiteSpace(txtPlayer4ScoreMarginLeft.Text))
            {
                System.Windows.MessageBox.Show("Player4 Score Margin Left can't be blank");
                _flag = false;
            }
            else if (string.IsNullOrWhiteSpace(txtPlayer4ScoreMarginTop.Text))
            {
                System.Windows.MessageBox.Show("Player4 Score Margin Top can't be blank");
                _flag = false;
            }
            else if (string.IsNullOrWhiteSpace(txtPlayer4ScoreMarginRight.Text))
            {
                System.Windows.MessageBox.Show("Player4 Score Margin Right can't be blank");
                _flag = false;
            }
            else if (string.IsNullOrWhiteSpace(txtPlayer4ScoreMarginBottom.Text))
            {
                System.Windows.MessageBox.Show("Player4 Score Margin Bottom can't be blank");
                _flag = false;
            }
            else if (string.IsNullOrWhiteSpace(txtPlayer5ScoreMarginLeft.Text))
            {
                System.Windows.MessageBox.Show("Player5 Score Margin Left can't be blank");
                _flag = false;
            }
            else if (string.IsNullOrWhiteSpace(txtPlayer5ScoreMarginTop.Text))
            {
                System.Windows.MessageBox.Show("Player5 Score Margin Top can't be blank");
                _flag = false;
            }
            else if (string.IsNullOrWhiteSpace(txtPlayer5ScoreMarginRight.Text))
            {
                System.Windows.MessageBox.Show("Player5 Score Margin Right can't be blank");
                _flag = false;
            }
            else if (string.IsNullOrWhiteSpace(txtPlayer5ScoreMarginBottom.Text))
            {
                System.Windows.MessageBox.Show("Player5 Score Margin Bottom can't be blank");
                _flag = false;
            }
            else if (string.IsNullOrWhiteSpace(txtPlayer6ScoreMarginLeft.Text))
            {
                System.Windows.MessageBox.Show("Player6 Score Margin Left can't be blank");
                _flag = false;
            }
            else if (string.IsNullOrWhiteSpace(txtPlayer6ScoreMarginTop.Text))
            {
                System.Windows.MessageBox.Show("Player6 Score Margin Top can't be blank");
                _flag = false;
            }
            else if (string.IsNullOrWhiteSpace(txtPlayer6ScoreMarginRight.Text))
            {
                System.Windows.MessageBox.Show("Player6 Score Margin Right can't be blank");
                _flag = false;
            }
            else if (string.IsNullOrWhiteSpace(txtPlayer6ScoreMarginBottom.Text))
            {
                System.Windows.MessageBox.Show("Player6 Score Margin Bottom can't be blank");
                _flag = false;
            }
            return _flag;
        }
        private bool IsNumberKey(Key inkey)
        {
            if (inkey == Key.Tab)
                return true;

            if (inkey < Key.D0 || inkey > Key.D9)
            {
                if (inkey < Key.NumPad0 || inkey > Key.NumPad9)
                {
                    return false;
                }
            }
            return true;
        }
        private bool LoadInitialSettings()
        {
            bool retVal = true;
            try
            {
                if (File.Exists(_xmlFilePathMaster))
                {
                    XmlDataDocument xmldoc = new XmlDataDocument();
                    xmldoc.Load(_xmlFilePathMaster);
                    _subStoreID = xmldoc.GetElementsByTagName("SubStore")[0].ChildNodes.Item(0).InnerText.Trim();
                    cmbSubStore.SelectedValue = _subStoreID;
                    clsLocation.SiteId = Convert.ToInt32(_subStoreID);


                    _locationID = xmldoc.GetElementsByTagName("Location")[0].ChildNodes.Item(0).InnerText.Trim();
                    cmbLocation.SelectedValue = _locationID;
                    cmbLocation.IsEnabled = true;
                    clsLocation.LocationId = Convert.ToInt32(_locationID);

                    cmbLocation_SelectionChanged(null, null);

                    _screenID = xmldoc.GetElementsByTagName("Screen")[0].ChildNodes.Item(0).InnerText.Trim();
                    cmbScreen.SelectedValue = _screenID;
                    cmbScreen.IsEnabled = true;

                    _rotationAngle = xmldoc.GetElementsByTagName("RotationAngle")[0].ChildNodes.Item(0).InnerText.Trim();
                    cmbRotation.SelectedValue = _rotationAngle;

                    _RFIDFontWeight = xmldoc.GetElementsByTagName("RFIDFontWeight")[0].ChildNodes.Item(0).InnerText.Trim();
                    cmbRFIDFontWeight.SelectedValue = _RFIDFontWeight;

                    _PlayerScoreFontWeight = xmldoc.GetElementsByTagName("PlayerScoreFontWeight")[0].ChildNodes.Item(0).InnerText.Trim();
                    cmbPlayerScoreFontWeight.SelectedValue = _PlayerScoreFontWeight;

                    _HorizentalBorderWidth = xmldoc.GetElementsByTagName("HorizentalBorderWidth")[0].ChildNodes.Item(0).InnerText.Trim();
                    cmbHorizentalBorderWidth.SelectedValue = _HorizentalBorderWidth;

                    _VerticalBorderWidth = xmldoc.GetElementsByTagName("VerticalBorderWidth")[0].ChildNodes.Item(0).InnerText.Trim();
                    cmbVerticalBorderWidth.SelectedValue = _VerticalBorderWidth;


                    _RFIDBackColor = xmldoc.GetElementsByTagName("RFIDBackColor")[0].ChildNodes.Item(0).InnerText.Trim();
                    cmbRFIDBackColor.SelectedColor = ((System.Windows.Media.Color)System.Windows.Media.ColorConverter.ConvertFromString(_RFIDBackColor));

                    _RFIDForeColor = xmldoc.GetElementsByTagName("RFIDForeColor")[0].ChildNodes.Item(0).InnerText.Trim();
                    cmbRFIDForeColor.SelectedColor = ((System.Windows.Media.Color)System.Windows.Media.ColorConverter.ConvertFromString(_RFIDForeColor));

                    _RFIDfontSize = xmldoc.GetElementsByTagName("RFIDFontSize")[0].ChildNodes.Item(0).InnerText.Trim();
                    txtRFIDFont.Text = _RFIDfontSize;

                    _PlayerScorefontSize = xmldoc.GetElementsByTagName("PlayerScoreFontSize")[0].ChildNodes.Item(0).InnerText.Trim();
                    txtPlayerScoreFont.Text = _PlayerScorefontSize;

                    _PhotoMarginLeft = xmldoc.GetElementsByTagName("PhotoMarginLeft")[0].ChildNodes.Item(0).InnerText.Trim();
                    txtPhotoMarginLeft.Text = _PhotoMarginLeft;

                    _PhotoMarginTop = xmldoc.GetElementsByTagName("PhotoMarginTop")[0].ChildNodes.Item(0).InnerText.Trim();
                    txtPhotoMarginTop.Text = _PhotoMarginTop;

                    _PhotoMarginRight = xmldoc.GetElementsByTagName("PhotoMarginRight")[0].ChildNodes.Item(0).InnerText.Trim();
                    txtPhotoMarginRight.Text = _PhotoMarginRight;

                    _PhotoMarginBottom = xmldoc.GetElementsByTagName("PhotoMarginBottom")[0].ChildNodes.Item(0).InnerText.Trim();
                    txtPhotoMarginBottom.Text = _PhotoMarginBottom;

                    _MessageMarginLeft = xmldoc.GetElementsByTagName("MessageMarginLeft")[0].ChildNodes.Item(0).InnerText.Trim();
                    txtMessageMarginLeft.Text = _MessageMarginLeft;

                    _MessageMarginTop = xmldoc.GetElementsByTagName("MessageMarginTop")[0].ChildNodes.Item(0).InnerText.Trim();
                    txtMessageMarginTop.Text = _MessageMarginTop;

                    _MessageMarginRight = xmldoc.GetElementsByTagName("MessageMarginRight")[0].ChildNodes.Item(0).InnerText.Trim();
                    txtMessageMarginRight.Text = _MessageMarginRight;

                    _MessageMarginBottom = xmldoc.GetElementsByTagName("MessageMarginBottom")[0].ChildNodes.Item(0).InnerText.Trim();
                    txtMessageMarginBottom.Text = _MessageMarginBottom;

                    _MSGFontSize = xmldoc.GetElementsByTagName("MSGFontSize")[0].ChildNodes.Item(0).InnerText.Trim();
                    txtFont.Text = _MSGFontSize;

                    _MsgForeColor = xmldoc.GetElementsByTagName("MSGForeColor")[0].ChildNodes.Item(0).InnerText.Trim();
                    cmbMessageForeColor.SelectedColor = ((System.Windows.Media.Color)System.Windows.Media.ColorConverter.ConvertFromString(_MsgForeColor));

                    _MSGPosition = xmldoc.GetElementsByTagName("MSGPosition")[0].ChildNodes.Item(0).InnerText.Trim();
                    cmbPosition.SelectedValue = _MSGPosition;

                    _MSGBGColor = xmldoc.GetElementsByTagName("MSGBGColor")[0].ChildNodes.Item(0).InnerText.Trim();
                    cmbMsgBGColor.SelectedColor = ((System.Windows.Media.Color)System.Windows.Media.ColorConverter.ConvertFromString(_MSGBGColor));

                    _MsgShowTime = xmldoc.GetElementsByTagName("MSGShowTime")[0].ChildNodes.Item(0).InnerText.Trim();
                    txtMsgShowTime.Text = _MsgShowTime;

                    _StretchImg = xmldoc.GetElementsByTagName("StretchImg")[0].ChildNodes.Item(0).InnerText.Trim();

                    _Player1ScoreMarginLeft = xmldoc.GetElementsByTagName("Player1ScoreMarginLeft")[0].ChildNodes.Item(0).InnerText.Trim();
                    txtPlayer1ScoreMarginLeft.Text = _Player1ScoreMarginLeft;
                    _Player1ScoreMarginTop = xmldoc.GetElementsByTagName("Player1ScoreMarginTop")[0].ChildNodes.Item(0).InnerText.Trim();
                    txtPlayer1ScoreMarginTop.Text = _Player1ScoreMarginTop;
                    _Player1ScoreMarginRight = xmldoc.GetElementsByTagName("Player1ScoreMarginRight")[0].ChildNodes.Item(0).InnerText.Trim();
                    txtPlayer1ScoreMarginRight.Text = _Player1ScoreMarginRight;
                    _Player1ScoreMarginBottom = xmldoc.GetElementsByTagName("Player1ScoreMarginBottom")[0].ChildNodes.Item(0).InnerText.Trim();
                    txtPlayer1ScoreMarginBottom.Text = _Player1ScoreMarginBottom;

                    _Player2ScoreMarginLeft = xmldoc.GetElementsByTagName("Player2ScoreMarginLeft")[0].ChildNodes.Item(0).InnerText.Trim();
                    txtPlayer2ScoreMarginLeft.Text = _Player2ScoreMarginLeft;
                    _Player2ScoreMarginTop = xmldoc.GetElementsByTagName("Player2ScoreMarginTop")[0].ChildNodes.Item(0).InnerText.Trim();
                    txtPlayer2ScoreMarginTop.Text = _Player2ScoreMarginTop;
                    _Player2ScoreMarginRight = xmldoc.GetElementsByTagName("Player2ScoreMarginRight")[0].ChildNodes.Item(0).InnerText.Trim();
                    txtPlayer2ScoreMarginRight.Text = _Player2ScoreMarginRight;
                    _Player2ScoreMarginBottom = xmldoc.GetElementsByTagName("Player2ScoreMarginBottom")[0].ChildNodes.Item(0).InnerText.Trim();
                    txtPlayer2ScoreMarginBottom.Text = _Player2ScoreMarginBottom;

                    _Player3ScoreMarginLeft = xmldoc.GetElementsByTagName("Player3ScoreMarginLeft")[0].ChildNodes.Item(0).InnerText.Trim();
                    txtPlayer3ScoreMarginLeft.Text = _Player3ScoreMarginLeft;
                    _Player3ScoreMarginTop = xmldoc.GetElementsByTagName("Player3ScoreMarginTop")[0].ChildNodes.Item(0).InnerText.Trim();
                    txtPlayer3ScoreMarginTop.Text = _Player3ScoreMarginTop;
                    _Player3ScoreMarginRight = xmldoc.GetElementsByTagName("Player3ScoreMarginRight")[0].ChildNodes.Item(0).InnerText.Trim();
                    txtPlayer3ScoreMarginRight.Text = _Player3ScoreMarginRight;
                    _Player3ScoreMarginBottom = xmldoc.GetElementsByTagName("Player3ScoreMarginBottom")[0].ChildNodes.Item(0).InnerText.Trim();
                    txtPlayer3ScoreMarginBottom.Text = _Player3ScoreMarginBottom;

                    _Player4ScoreMarginLeft = xmldoc.GetElementsByTagName("Player4ScoreMarginLeft")[0].ChildNodes.Item(0).InnerText.Trim();
                    txtPlayer4ScoreMarginLeft.Text = _Player4ScoreMarginLeft;
                    _Player4ScoreMarginTop = xmldoc.GetElementsByTagName("Player4ScoreMarginTop")[0].ChildNodes.Item(0).InnerText.Trim();
                    txtPlayer4ScoreMarginTop.Text = _Player4ScoreMarginTop;
                    _Player4ScoreMarginRight = xmldoc.GetElementsByTagName("Player4ScoreMarginRight")[0].ChildNodes.Item(0).InnerText.Trim();
                    txtPlayer4ScoreMarginRight.Text = _Player4ScoreMarginRight;
                    _Player4ScoreMarginBottom = xmldoc.GetElementsByTagName("Player4ScoreMarginBottom")[0].ChildNodes.Item(0).InnerText.Trim();
                    txtPlayer4ScoreMarginBottom.Text = _Player4ScoreMarginBottom;

                    _Player5ScoreMarginLeft = xmldoc.GetElementsByTagName("Player5ScoreMarginLeft")[0].ChildNodes.Item(0).InnerText.Trim();
                    txtPlayer5ScoreMarginLeft.Text = _Player5ScoreMarginLeft;
                    _Player5ScoreMarginTop = xmldoc.GetElementsByTagName("Player5ScoreMarginTop")[0].ChildNodes.Item(0).InnerText.Trim();
                    txtPlayer5ScoreMarginTop.Text = _Player5ScoreMarginTop;
                    _Player5ScoreMarginRight = xmldoc.GetElementsByTagName("Player5ScoreMarginRight")[0].ChildNodes.Item(0).InnerText.Trim();
                    txtPlayer5ScoreMarginRight.Text = _Player5ScoreMarginRight;
                    _Player5ScoreMarginBottom = xmldoc.GetElementsByTagName("Player5ScoreMarginBottom")[0].ChildNodes.Item(0).InnerText.Trim();
                    txtPlayer5ScoreMarginBottom.Text = _Player5ScoreMarginBottom;

                    _Player6ScoreMarginLeft = xmldoc.GetElementsByTagName("Player6ScoreMarginLeft")[0].ChildNodes.Item(0).InnerText.Trim();
                    txtPlayer6ScoreMarginLeft.Text = _Player6ScoreMarginLeft;
                    _Player6ScoreMarginTop = xmldoc.GetElementsByTagName("Player6ScoreMarginTop")[0].ChildNodes.Item(0).InnerText.Trim();
                    txtPlayer6ScoreMarginTop.Text = _Player6ScoreMarginTop;
                    _Player6ScoreMarginRight = xmldoc.GetElementsByTagName("Player6ScoreMarginRight")[0].ChildNodes.Item(0).InnerText.Trim();
                    txtPlayer6ScoreMarginRight.Text = _Player6ScoreMarginRight;
                    _Player6ScoreMarginBottom = xmldoc.GetElementsByTagName("Player6ScoreMarginBottom")[0].ChildNodes.Item(0).InnerText.Trim();
                    txtPlayer6ScoreMarginBottom.Text = _Player6ScoreMarginBottom;


                    _Player1ScoreBackColor = xmldoc.GetElementsByTagName("Player1ScoreBackColor")[0].ChildNodes.Item(0).InnerText.Trim();
                    cmbPlayer1ScoreBackColor.SelectedColor = ((System.Windows.Media.Color)System.Windows.Media.ColorConverter.ConvertFromString(_Player1ScoreBackColor));

                    _Player2ScoreBackColor = xmldoc.GetElementsByTagName("Player2ScoreBackColor")[0].ChildNodes.Item(0).InnerText.Trim();
                    cmbPlayer2ScoreBackColor.SelectedColor = ((System.Windows.Media.Color)System.Windows.Media.ColorConverter.ConvertFromString(_Player2ScoreBackColor));

                    _Player3ScoreBackColor = xmldoc.GetElementsByTagName("Player3ScoreBackColor")[0].ChildNodes.Item(0).InnerText.Trim();
                    cmbPlayer3ScoreBackColor.SelectedColor = ((System.Windows.Media.Color)System.Windows.Media.ColorConverter.ConvertFromString(_Player3ScoreBackColor));

                    _Player4ScoreBackColor = xmldoc.GetElementsByTagName("Player4ScoreBackColor")[0].ChildNodes.Item(0).InnerText.Trim();
                    cmbPlayer4ScoreBackColor.SelectedColor = ((System.Windows.Media.Color)System.Windows.Media.ColorConverter.ConvertFromString(_Player4ScoreBackColor));

                    _Player5ScoreBackColor = xmldoc.GetElementsByTagName("Player5ScoreBackColor")[0].ChildNodes.Item(0).InnerText.Trim();
                    cmbPlayer5ScoreBackColor.SelectedColor = ((System.Windows.Media.Color)System.Windows.Media.ColorConverter.ConvertFromString(_Player5ScoreBackColor));

                    _Player6ScoreBackColor = xmldoc.GetElementsByTagName("Player6ScoreBackColor")[0].ChildNodes.Item(0).InnerText.Trim();
                    cmbPlayer6ScoreBackColor.SelectedColor = ((System.Windows.Media.Color)System.Windows.Media.ColorConverter.ConvertFromString(_Player6ScoreBackColor));

                    if (_StretchImg.ToUpper() == "FALSE")
                    {
                        chkStretchImg.IsChecked = false;
                        PreViewWallMedia.Stretch = Stretch.Uniform;
                        imgWaterMarkControl.Stretch = Stretch.Uniform;
                        //imgHorizentalFrame.Stretch = Stretch.Uniform;
                        //imgVerticalFrame.Stretch = Stretch.Uniform;
                    }
                    else
                    {
                        chkStretchImg.IsChecked = true;
                        PreViewWallMedia.Stretch = Stretch.Fill;
                        imgWaterMarkControl.Stretch = Stretch.Fill;
                        //imgHorizentalFrame.Stretch = Stretch.Fill;
                        //imgVerticalFrame.Stretch = Stretch.Fill;

                    }
                    RIFDSetting();
                }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                retVal = false;
            }
            return retVal;
        }
        private void RIFDSetting()
        {
            lblPlayer1Score.Background = new SolidColorBrush(((System.Windows.Media.Color)System.Windows.Media.ColorConverter.ConvertFromString(_Player1ScoreBackColor)));
            lblPlayer2Score.Background = new SolidColorBrush(((System.Windows.Media.Color)System.Windows.Media.ColorConverter.ConvertFromString(_Player2ScoreBackColor)));
            lblPlayer3Score.Background = new SolidColorBrush(((System.Windows.Media.Color)System.Windows.Media.ColorConverter.ConvertFromString(_Player3ScoreBackColor)));
            lblPlayer4Score.Background = new SolidColorBrush(((System.Windows.Media.Color)System.Windows.Media.ColorConverter.ConvertFromString(_Player4ScoreBackColor)));
            lblPlayer5Score.Background = new SolidColorBrush(((System.Windows.Media.Color)System.Windows.Media.ColorConverter.ConvertFromString(_Player5ScoreBackColor)));
            lblPlayer6Score.Background = new SolidColorBrush(((System.Windows.Media.Color)System.Windows.Media.ColorConverter.ConvertFromString(_Player6ScoreBackColor)));
            lblPlayer1Score.FontSize = Convert.ToDouble(_PlayerScorefontSize);
            txtPlayer1Score.FontSize = Convert.ToDouble(_PlayerScorefontSize);
            lblPlayer2Score.FontSize = Convert.ToDouble(_PlayerScorefontSize);
            txtPlayer2Score.FontSize = Convert.ToDouble(_PlayerScorefontSize);
            lblPlayer3Score.FontSize = Convert.ToDouble(_PlayerScorefontSize);
            txtPlayer3Score.FontSize = Convert.ToDouble(_PlayerScorefontSize);
            lblPlayer4Score.FontSize = Convert.ToDouble(_PlayerScorefontSize);
            txtPlayer4Score.FontSize = Convert.ToDouble(_PlayerScorefontSize);
            lblPlayer5Score.FontSize = Convert.ToDouble(_PlayerScorefontSize);
            txtPlayer5Score.FontSize = Convert.ToDouble(_PlayerScorefontSize);
            lblPlayer6Score.FontSize = Convert.ToDouble(_PlayerScorefontSize);
            txtPlayer6Score.FontSize = Convert.ToDouble(_PlayerScorefontSize);

            lblPlayer1Score.FontWeight = (FontWeight)new FontWeightConverter().ConvertFromString(_PlayerScoreFontWeight);
            txtPlayer1Score.FontWeight = (FontWeight)new FontWeightConverter().ConvertFromString(_PlayerScoreFontWeight);
            lblPlayer2Score.FontWeight = (FontWeight)new FontWeightConverter().ConvertFromString(_PlayerScoreFontWeight);
            txtPlayer2Score.FontWeight = (FontWeight)new FontWeightConverter().ConvertFromString(_PlayerScoreFontWeight);
            lblPlayer3Score.FontWeight = (FontWeight)new FontWeightConverter().ConvertFromString(_PlayerScoreFontWeight);
            txtPlayer3Score.FontWeight = (FontWeight)new FontWeightConverter().ConvertFromString(_PlayerScoreFontWeight);
            lblPlayer4Score.FontWeight = (FontWeight)new FontWeightConverter().ConvertFromString(_PlayerScoreFontWeight);
            txtPlayer4Score.FontWeight = (FontWeight)new FontWeightConverter().ConvertFromString(_PlayerScoreFontWeight);
            lblPlayer5Score.FontWeight = (FontWeight)new FontWeightConverter().ConvertFromString(_PlayerScoreFontWeight);
            txtPlayer5Score.FontWeight = (FontWeight)new FontWeightConverter().ConvertFromString(_PlayerScoreFontWeight);
            lblPlayer6Score.FontWeight = (FontWeight)new FontWeightConverter().ConvertFromString(_PlayerScoreFontWeight);
            txtPlayer6Score.FontWeight = (FontWeight)new FontWeightConverter().ConvertFromString(_PlayerScoreFontWeight);

            lblRFID.FontSize = Convert.ToDouble(_RFIDfontSize);
            lblRFID.FontWeight = (FontWeight)new FontWeightConverter().ConvertFromString(_RFIDFontWeight);
            lblRFID.Background = new SolidColorBrush(((System.Windows.Media.Color)System.Windows.Media.ColorConverter.ConvertFromString(_RFIDBackColor)));
            lblRFID.Foreground = new SolidColorBrush(((System.Windows.Media.Color)System.Windows.Media.ColorConverter.ConvertFromString(_RFIDForeColor)));

            lblRFIDAssociate.FontSize = Convert.ToDouble(_MSGFontSize);
            lblRFIDAssociate.Foreground = new SolidColorBrush(((System.Windows.Media.Color)System.Windows.Media.ColorConverter.ConvertFromString(_MsgForeColor)));
            lblRFIDAssociate.Background = new SolidColorBrush(((System.Windows.Media.Color)System.Windows.Media.ColorConverter.ConvertFromString(_MSGBGColor)));


            var horizentalimage = new BitmapImage();
            horizentalimage.BeginInit();
            horizentalimage.UriSource = new Uri(@"/DigiPhoto.PreviewWall;component/Images/HorizentalBorder" + cmbHorizentalBorderWidth.SelectedValue + ".gif", UriKind.Relative);
            horizentalimage.EndInit();
            ImageBehavior.SetAnimatedSource(imgBlinkHorizental, horizentalimage);
            ImageBehavior.SetRepeatBehavior(imgBlinkHorizental, new RepeatBehavior(0));
            ImageBehavior.SetRepeatBehavior(imgBlinkHorizental, RepeatBehavior.Forever);

            var verticalimage = new BitmapImage();
            verticalimage.BeginInit();
            verticalimage.UriSource = new Uri(@"/DigiPhoto.PreviewWall;component/Images/VerticalBorder" + cmbVerticalBorderWidth.SelectedValue + ".gif", UriKind.Relative);
            verticalimage.EndInit();
            ImageBehavior.SetAnimatedSource(imgBlinkVertical, verticalimage);
            ImageBehavior.SetRepeatBehavior(imgBlinkVertical, new RepeatBehavior(0));
            ImageBehavior.SetRepeatBehavior(imgBlinkVertical, RepeatBehavior.Forever);

            if (cmbHorizentalBorderWidth.SelectedValue.ToString() == "X")
            {
                RFIDStackPanel.Margin = new Thickness(14);
                imgLogoControl.Margin = new Thickness(14);
            }
            else if (cmbHorizentalBorderWidth.SelectedValue.ToString() == "2X")
            {
                RFIDStackPanel.Margin = new Thickness(18);
                imgLogoControl.Margin = new Thickness(18);
            }
            else if (cmbHorizentalBorderWidth.SelectedValue.ToString() == "3X")
            {
                RFIDStackPanel.Margin = new Thickness(23);
                imgLogoControl.Margin = new Thickness(23);
            }
            else if (cmbHorizentalBorderWidth.SelectedValue.ToString() == "4X")
            {
                RFIDStackPanel.Margin = new Thickness(30);
                imgLogoControl.Margin = new Thickness(30);
            }
            RotateTransform rt = new RotateTransform();
            rt.Angle = Convert.ToDouble(_rotationAngle);
            rotateAngle = rt.Angle;
            grdMain.LayoutTransform = rt;
        }
        private void AssignValueToControl()
        {
            _rotationDisplayScreenlist = new List<string>();
            //_rotationDisplayScreenlist.Add("--Select--");
            _rotationDisplayScreenlist.Add("0");
            _rotationDisplayScreenlist.Add("90");
            _rotationDisplayScreenlist.Add("180");
            _rotationDisplayScreenlist.Add("270");
            cmbRotation.ItemsSource = _rotationDisplayScreenlist;
            cmbRotation.SelectedValue = "0";

            _dispalyScreenlist = new List<string>();
            _dispalyScreenlist.Add("--Select--");
            for (int i = 1; i <= 100; i++)
            {
                _dispalyScreenlist.Add("Display" + i);
            }
            cmbScreen.ItemsSource = _dispalyScreenlist;
            cmbScreen.SelectedValue = "--Select--";

            _RFIDFontWeightlist = new List<string>();
            //_RFIDFontWeightlist.Add("--Select--");
            _RFIDFontWeightlist.Add("Normal");
            _RFIDFontWeightlist.Add("Black");
            _RFIDFontWeightlist.Add("ExtraBlack");
            _RFIDFontWeightlist.Add("Thin");
            _RFIDFontWeightlist.Add("Bold");
            _RFIDFontWeightlist.Add("SemiBold");
            _RFIDFontWeightlist.Add("ExtraBold");
            _RFIDFontWeightlist.Add("Light");
            _RFIDFontWeightlist.Add("ExtraLight");
            _RFIDFontWeightlist.Add("Medium");
            cmbRFIDFontWeight.ItemsSource = _RFIDFontWeightlist;
            cmbRFIDFontWeight.SelectedValue = "Normal";

            _PlayerScoreFontWeightlist = new List<string>();
            //_PlayerScoreFontWeightlist.Add("--Select--");
            _PlayerScoreFontWeightlist.Add("Normal");
            _PlayerScoreFontWeightlist.Add("Black");
            _PlayerScoreFontWeightlist.Add("ExtraBlack");
            _PlayerScoreFontWeightlist.Add("Thin");
            _PlayerScoreFontWeightlist.Add("Bold");
            _PlayerScoreFontWeightlist.Add("SemiBold");
            _PlayerScoreFontWeightlist.Add("ExtraBold");
            _PlayerScoreFontWeightlist.Add("Light");
            _PlayerScoreFontWeightlist.Add("ExtraLight");
            _PlayerScoreFontWeightlist.Add("Medium");
            cmbPlayerScoreFontWeight.ItemsSource = _PlayerScoreFontWeightlist;
            cmbPlayerScoreFontWeight.SelectedValue = "Normal";

            _horizentalBlinkBorderlist = new List<string>();
            _horizentalBlinkBorderlist.Add("X");
            _horizentalBlinkBorderlist.Add("2X");
            _horizentalBlinkBorderlist.Add("3X");
            _horizentalBlinkBorderlist.Add("4X");
            cmbHorizentalBorderWidth.ItemsSource = _horizentalBlinkBorderlist;
            cmbHorizentalBorderWidth.SelectedValue = "X";

            _verticalBlinkBorderlist = new List<string>();
            _verticalBlinkBorderlist.Add("X");
            _verticalBlinkBorderlist.Add("2X");
            _verticalBlinkBorderlist.Add("3X");
            _verticalBlinkBorderlist.Add("4X");
            cmbVerticalBorderWidth.ItemsSource = _verticalBlinkBorderlist;
            cmbVerticalBorderWidth.SelectedValue = "X";

            _PositionList = new List<string>();
            _PositionList.Add("Top-Left");
            _PositionList.Add("Top-Center");
            _PositionList.Add("Top-Right");
            _PositionList.Add("Bottom-Left");
            _PositionList.Add("Bottom-Center");
            _PositionList.Add("Bottom-Right");
            _PositionList.Add("Center");
            cmbPosition.ItemsSource = _PositionList;
            cmbPosition.SelectedValue = "Top-Left";

            try
            {
                List<SubStoresInfo> subStores = new List<SubStoresInfo>();
                if (clsLocation.LocationId == 0)
                {
                    subStores = (new StoreSubStoreDataBusniess()).GetSubstoreData().ToList();
                    CommonUtility.BindComboWithSelect<SubStoresInfo>(cmbSubStore, subStores, "DG_SubStore_Name", "DG_SubStore_pkey", 0, DigiPhoto.Utility.Repository.Data.ClientConstant.SelectString);
                    cmbSubStore.SelectedValue = "0";

                    List<LocationInfo> locations = new List<LocationInfo>();
                    locations = (new LocationBusniess()).GetLocationList(1).ToList();
                    CommonUtility.BindComboWithSelect<LocationInfo>(cmbLocation, locations, "DG_Location_Name", "DG_Location_pkey", 0, DigiPhoto.Utility.Repository.Data.ClientConstant.SelectString);
                    cmbLocation.SelectedValue = "0";
                }
                else
                {
                    subStores = (new StoreSubStoreDataBusniess()).GetSubstoreData().ToList();
                    CommonUtility.BindComboWithSelect<SubStoresInfo>(cmbSubStore, subStores, "DG_SubStore_Name", "DG_SubStore_pkey", 0, DigiPhoto.Utility.Repository.Data.ClientConstant.SelectString);
                    cmbSubStore.SelectedValue = clsLocation.SiteId;
                    cmbSubStore_SelectionChanged(null, null);
                    cmbLocation.SelectedValue = clsLocation.LocationId;
                    cmbLocation.IsEnabled = true;
                    cmbLocation_SelectionChanged(null, null);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }

        }
        private void ReloadGuestImgTimer()
        {
            _tmrGetMediaFile.Interval = _reloadGuestImgTimeInterval * 1000;
            _tmrGetMediaFile.Tick += new EventHandler(tmr_Tick);
            _tmrGetMediaFile.Start();
        }

        #endregion

        private void NumericOnly(System.Object sender, System.Windows.Input.TextCompositionEventArgs e)
        {
            e.Handled = IsTextNumeric(e.Text);

        }
        private static bool IsTextNumeric(string str)
        {
            System.Text.RegularExpressions.Regex reg = new System.Text.RegularExpressions.Regex("[^0-9]");
            return reg.IsMatch(str);
        }

    }
    #region Class
    //public static class externClassGumBallRide
    //{
    //    public static IEnumerable<TSource> DistinctBy<TSource, TKey>(
    //       this IEnumerable<TSource> source,
    //       Func<TSource, TKey> keySelector)
    //    {
    //        var knownKeys = new HashSet<TKey>();
    //        return source.Where(element => knownKeys.Add(keySelector(element)));
    //    }
    //}
    #endregion Class
}

