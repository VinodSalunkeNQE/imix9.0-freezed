﻿using DigiPhoto.DataLayer;
using DigiPhoto.IMIX.Business;
using DigiPhoto.IMIX.Model;
using DigiPhoto.IMIX.Model.Base;
using DigiPhoto.Utility.Repository.DirectoryInfoExt;
using FrameworkHelper;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Threading;
using System.Xml;
using WpfAnimatedGif;

namespace DigiPhoto.PreviewWall
{
    /// <summary>
    /// Interaction logic for PreviewWall in IMIX
    /// </summary>
    public partial class MainWindow : Window
    {
        #region Declaration
        bool _showMsg = false;
        List<FileInfo> latestGusetImageList = new List<FileInfo>();
        List<FileInfo> lstGusetImageIteration = new List<FileInfo>();
        List<iMixConfigurationLocationInfo> _locationWiseConfigParamsList;
        List<iMixConfigurationLocationInfo> _locationWiseConfigList;
        PreviewWallInfo _previewWallInfoSetting;
        //static string _xmlFilePath = string.Empty;
        static string _xmlFilePathMaster, _xmlFilePathMasterLocation = string.Empty;
        Timer _tmrGetMediaFile = new Timer();
        List<string> _rotationDisplayScreenlist;
        List<string> _RFIDFontWeightlist, _horizentalBlinkBorderlist, _verticalBlinkBorderlist;
        List<string> _dispalyScreenlist;
        int _mktMediaCount, _guestImgCount = 0, _iterationImgCount = 0;
        string _waterMarkImg, _logoImg, _photoId, _mktImgPath, _guestImgPath, _horizentalFrame, _verticalFrame = string.Empty, _guestExplainatoryImg;
        string siteName, locationName = string.Empty;//Added by Vins_30Aug2019
        int _reloadGuestImgTimeInterval = 1;
        bool _isMktImagesActive = false;
        List<FileInfo> _gusetImgList = new List<FileInfo>();
        List<FileInfo> _mrkMediaList = new List<FileInfo>();
        List<iMIXStoreConfigurationInfo> _dummyTag = new List<iMIXStoreConfigurationInfo>();
        string _hotFolderPath = string.Empty;
        string _mediaPlayPath = string.Empty;
        string _subStoreID = string.Empty;
        string _locationID = string.Empty;
        string _screenID = string.Empty;
        string _rotationAngle = "0";
        string _RFIDfontSize = "1";
        string _PhotoMarginLeft, _PhotoMarginTop, _PhotoMarginRight, _PhotoMarginBottom = "0";
        string _MessageMarginLeft, _MessageMarginTop, _MessageMarginRight, _MessageMarginBottom = "0";
        string _StretchImg = "FALSE";
        string _RFIDFontWeight = "ExtraLight";
        string _HorizentalBorderWidth, _VerticalBorderWidth = "X";
        string _RFIDBackColor = "#FFFFFF";
        string _RFIDForeColor = "#FFFFFF";
        Int32 RFIDOverWritesStatus = 0;
        bool IsAnonymousQrCodeEnabled = false;
        DateTime _lastPickTime = DateTime.MinValue;
        DateTime _lastCleanUpTime = DateTime.Now;
        DateTime _currentDateTime;
        bool _mrkMediaPreviewTime = false;

        string _QrCodeURL = string.Empty;
        string webURL = string.Empty;

        double rotateAngle = 0;
        string QRCODE = string.Empty;
        bool isPlayGuestImg = true;
        //DateTime _PreviewScreenTime;
        //DateTime _blinkScreenTime;
        //bool _set = false;
        Int32 _guestMediaReloadTime, _marketingMediaReloadTime, _previewHighlightAdjustTime, _previewWallCleanUpTimeInMinute = 0;
        String _marketingMediaCopy, _marketingMediaDelete = "No";
        string _copyMarketinMediaPath = string.Empty;
        List<string> _PositionList;

        string _MSGFontSize = "1";
        string _MsgForeColor = "#FFFFFF";
        string _MSGPosition = "Bottom-Left";
        string _MSGBGColor = "#FFFFFF";
        string _MsgShowTime = "1";

        int _previewWallIteration = 0;
        #endregion

        #region Constructor
        public MainWindow()
        {
            InitializeComponent();
            AssignValueToControl();
            //AssignLocationValue();
            txtBarcode.Focus();
            if (ConfigurationManager.AppSettings.AllKeys.Contains("GuestMediaReloadTime"))
                _guestMediaReloadTime = Convert.ToInt32(ConfigurationManager.AppSettings["GuestMediaReloadTime"].ToString());
            if (ConfigurationManager.AppSettings.AllKeys.Contains("MarketingMediaReloadTime"))
                _marketingMediaReloadTime = Convert.ToInt32(ConfigurationManager.AppSettings["MarketingMediaReloadTime"].ToString());
            if (ConfigurationManager.AppSettings.AllKeys.Contains("MarketingMediaCopy"))
                _marketingMediaCopy = ConfigurationManager.AppSettings["MarketingMediaCopy"].ToString();
            if (ConfigurationManager.AppSettings.AllKeys.Contains("MarketingMediaDelete"))
                _marketingMediaDelete = ConfigurationManager.AppSettings["MarketingMediaDelete"].ToString();
            if (ConfigurationManager.AppSettings.AllKeys.Contains("PreviewHighlightAdjustTime"))
                _previewHighlightAdjustTime = Convert.ToInt32(ConfigurationManager.AppSettings["PreviewHighlightAdjustTime"].ToString());
            if (ConfigurationManager.AppSettings.AllKeys.Contains("PreviewWallCleanUpTimeInMinute"))
                _previewWallCleanUpTimeInMinute = Convert.ToInt32(ConfigurationManager.AppSettings["PreviewWallCleanUpTimeInMinute"].ToString());
            _xmlFilePathMaster = System.IO.Path.Combine(Environment.CurrentDirectory, "PreviewSetting.xml");
            _xmlFilePathMasterLocation = System.IO.Path.Combine(Environment.CurrentDirectory, "PreviewSettingLocation.xml");
            if (File.Exists(_xmlFilePathMaster) && LoadInitialSettings())
            {
                grdSettings.Visibility = Visibility.Collapsed;
                btnSave_Click(null, null);
            }
            else
            {
                btnCloseScreen.Visibility = Visibility.Visible;
                grdSettings.Visibility = Visibility.Visible;
            }
            GetQrCodeURL();


        }
        private void GetQrCodeURL()
        {
            try
            {
                webURL = (new StoreSubStoreDataBusniess()).GetQRCodeWebUrl();
                _QrCodeURL = string.IsNullOrEmpty(webURL) ? " " : webURL;
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
            }
        }
        private void GetMarketingVideo()
        {
            try
            {
                if (!string.IsNullOrEmpty(_mktImgPath) && _previewWallInfoSetting.IsMktImgPW)
                {
                    if (Directory.Exists(_mktImgPath))
                    {
                        var myDirInfo = new DirectoryInfo(_mktImgPath);
                        _mrkMediaList = myDirInfo.GetFilesByExtensions(".jpg", ".png", ".mp4", ".jpeg", ".JPEG",
                                                        ".wmv", ".mpg", ".avi", ".3gp", ".JPG", ".MP4", ".WMV", ".MPG", ".AVI", ".3GP", ".GIF", ".gif").ToList();
                    }
                    if (_marketingMediaCopy != null && _marketingMediaCopy.ToUpper() == "YES" && _mrkMediaList.Count > 0)
                    {
                        _copyMarketinMediaPath = System.IO.Path.Combine(Environment.CurrentDirectory, "MarketingMedia");
                        if (_marketingMediaDelete.ToUpper() == "YES")
                        {
                            if (Directory.Exists(_copyMarketinMediaPath))
                                Directory.Delete(_copyMarketinMediaPath, true);
                        }
                        if (!Directory.Exists(_copyMarketinMediaPath))
                            Directory.CreateDirectory(_copyMarketinMediaPath);
                        foreach (FileInfo mrkVideo in _mrkMediaList)
                        {
                            File.Copy(mrkVideo.FullName, System.IO.Path.Combine(_copyMarketinMediaPath, mrkVideo.Name), false);
                        }
                        _mrkMediaList = (new DirectoryInfo(_copyMarketinMediaPath)).GetFilesByExtensions(".jpg", ".png", ".mp4", ".jpeg", ".JPEG",
                                                            ".wmv", ".mpg", ".avi", ".3gp", ".JPG", ".MP4", ".WMV", ".MPG", ".AVI", ".3GP", ".GIF", ".gif").ToList();
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
            }
        }
        #endregion

        #region Events
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                int ScreenCount = System.Windows.Forms.Screen.AllScreens.Count();
                if (ScreenCount > 1)
                {
                    MainWindow window = null;
                    foreach (Window wnd in System.Windows.Application.Current.Windows)
                    {
                        if (wnd.Title == "DEI")
                        {
                            window = (MainWindow)wnd;
                        }
                    }

                    if (window == null)
                    {

                        window = new MainWindow();
                        window.WindowStartupLocation = System.Windows.WindowStartupLocation.Manual;

                    }

                    System.Windows.Forms.Screen s1 = System.Windows.Forms.Screen.AllScreens[ScreenCount - 1];

                    System.Drawing.Rectangle r1 = s1.WorkingArea;
                    window.WindowState = System.Windows.WindowState.Normal;
                    window.WindowStartupLocation = System.Windows.WindowStartupLocation.Manual;
                    window.Top = r1.Top;
                    window.Left = r1.Left;
                    window.WindowState = System.Windows.WindowState.Maximized;
                }
                LoadInitialSettings();
                if (_gusetImgList.Count == 0)
                {
                    PreViewWallMedia.Source = new Uri(_mediaPlayPath, UriKind.Relative);
                    PreViewWallMedia.Play();
                    System.Threading.Thread.Sleep(_marketingMediaReloadTime);
                    //PreViewWallMedia.BeginAnimation(UIElement.OpacityProperty, new DoubleAnimation(PreViewWallMedia.Opacity, 1, TimeSpan.FromSeconds(10)));
                }
                CheckIsPreviewWall();
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
            }

        }
        private void cmbSubStore_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                Int32 subStoreId = Convert.ToInt32(cmbSubStore.SelectedValue);
                if (subStoreId != 0)
                {
                    cmbLocation.IsEnabled = true;
                    List<LocationInfo> locations = new List<LocationInfo>();
                    locations = (new StoreSubStoreDataBusniess()).GetSelectedLocationsSubstore(subStoreId).ToList();
                    CommonUtility.BindComboWithSelect<LocationInfo>(cmbLocation, locations, "DG_Location_Name", "DG_Location_pkey", 0, DigiPhoto.Utility.Repository.Data.ClientConstant.SelectString);
                }
                else
                {
                    cmbLocation.IsEnabled = false;
                    cmbScreen.IsEnabled = false;
                }
                cmbLocation.SelectedValue = "0";
                cmbScreen.SelectedValue = "--Select--";
                if (!cmbLocation.IsEnabled)
                    cmbScreen.IsEnabled = false;
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
            }
        }
        private void cmbLocation_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                List<iMixConfigurationLocationInfo> ConfigValuesList = (new ConfigBusiness()).GetConfigBasedOnLocation(Convert.ToInt32(cmbLocation.SelectedValue)).Where(s => s.SubstoreId == Convert.ToInt32(cmbSubStore.SelectedValue)).ToList();
                if (Convert.ToInt32(cmbLocation.SelectedValue) != 0)
                {
                    //if (!File.Exists(System.IO.Path.Combine(Environment.CurrentDirectory, "LocationSetting.xml")))
                    //{
                    List<iMixConfigurationLocationInfo> LocationConfigValuesList = (new ConfigBusiness()).GetConfigBasedOnLocation(Convert.ToInt32(cmbLocation.SelectedValue)).Where(s => s.SubstoreId == Convert.ToInt32(cmbSubStore.SelectedValue)).ToList();
                    if (ConfigValuesList != null)
                    {
                        var isGumBallActive = ConfigValuesList.Where(n => n.IMIXConfigurationMasterId == (long)ConfigParams.IsGumRideActive
                        && n.ConfigurationValue.ToUpper() == "TRUE").Select(x => Convert.ToInt64(x.LocationId)).ToList();
                        if (isGumBallActive != null && isGumBallActive.Count > 0)
                        {
                            clsLocation.SiteId = Convert.ToInt32(cmbSubStore.SelectedValue);
                            clsLocation.LocationId = Convert.ToInt32(cmbLocation.SelectedValue);
                            new MainWindowGumBallRide().Show();
                            this.Close();
                        }
                    }
                    //}
                    Int32 ScreenNo = Convert.ToInt32((ConfigValuesList.Where(s => s.IMIXConfigurationMasterId == Convert.ToInt32(ConfigParams.NoOfScreenPW)).FirstOrDefault()).ConfigurationValue);
                    if (ScreenNo != 0)
                    {
                        cmbScreen.IsEnabled = true;
                        _dispalyScreenlist = new List<string>();
                        _dispalyScreenlist.Add("--Select--");
                        for (int i = 1; i <= ScreenNo; i++)
                        {
                            _dispalyScreenlist.Add("Display" + i);
                        }
                        cmbScreen.ItemsSource = _dispalyScreenlist;
                    }
                    else
                    {
                        cmbScreen.IsEnabled = false;
                    }
                    cmbScreen.SelectedValue = "--Select--";
                }
                else
                {
                    cmbScreen.SelectedValue = "--Select--";
                    cmbScreen.IsEnabled = false;
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
            }
        }
        private bool GetPreviewTime()
        {
            int previewTime;
            _locationWiseConfigList = (new ConfigBusiness()).GetLocationWiseConfigParams(Convert.ToInt32(cmbSubStore.SelectedValue));
            iMixConfigurationLocationInfo objLocation = _locationWiseConfigList.Where(n => n.LocationId == Convert.ToInt32(cmbLocation.SelectedValue)).Where(x => x.IMIXConfigurationMasterId == (int)ConfigParams.PreviewTimePW).FirstOrDefault();
            if (objLocation != null)
                previewTime = Convert.ToInt32(objLocation.ConfigurationValue);
            else
                previewTime = 0;
            if (txtMsgShowTime.Text == "0")
            {
                System.Windows.MessageBox.Show("Message Display Time cannot be 0");
                return false;
            }
            if (!string.IsNullOrEmpty(txtMsgShowTime.Text))
            {
                if (previewTime < Convert.ToInt32(txtMsgShowTime.Text))
                {
                    System.Windows.MessageBox.Show("Message Display Time cannot be greater than Preview Time\n\n Your Image Preview time for this location is  " + previewTime + "sec.");
                    return false;
                }
                else
                    return true;
            }
            return false;
        }
        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (CheckValidation())
                {
                    ResetOldSetting();
                    //if (File.Exists(_xmlFilePath))
                    //{
                    //    File.Delete(_xmlFilePath);
                    //}
                    if (!GetPreviewTime())
                        return;


                    XmlTextWriter writerLoc = new XmlTextWriter(_xmlFilePathMasterLocation, System.Text.Encoding.UTF8);
                    writerLoc.WriteStartDocument(true);
                    writerLoc.Formatting = Formatting.Indented;
                    writerLoc.Indentation = 2;
                    writerLoc.WriteStartElement("Settings");

                    writerLoc.WriteStartElement("SubStore");
                    writerLoc.WriteString(cmbSubStore.SelectedValue.ToString());
                    writerLoc.WriteEndElement();
                    //_subStoreID = cmbSubStore.SelectedValue.ToString();

                    writerLoc.WriteStartElement("Location");
                    writerLoc.WriteString(cmbLocation.SelectedValue.ToString());
                    writerLoc.WriteEndElement();
                    //_locationID = cmbLocation.SelectedValue.ToString();
                    writerLoc.WriteEndDocument();
                    writerLoc.Close();


                    XmlTextWriter writer = new XmlTextWriter(_xmlFilePathMaster, System.Text.Encoding.UTF8);
                    writer.WriteStartDocument(true);
                    writer.Formatting = Formatting.Indented;
                    writer.Indentation = 2;
                    writer.WriteStartElement("Settings");

                    writer.WriteStartElement("SubStore");
                    writer.WriteString(cmbSubStore.SelectedValue.ToString());
                    writer.WriteEndElement();
                    _subStoreID = cmbSubStore.SelectedValue.ToString();

                    writer.WriteStartElement("Location");
                    writer.WriteString(cmbLocation.SelectedValue.ToString());
                    writer.WriteEndElement();
                    _locationID = cmbLocation.SelectedValue.ToString();

                    writer.WriteStartElement("Screen");
                    writer.WriteString(cmbScreen.SelectedValue.ToString());
                    writer.WriteEndElement();
                    _screenID = cmbScreen.SelectedValue.ToString();

                    writer.WriteStartElement("RotationAngle");
                    writer.WriteString(cmbRotation.SelectedValue.ToString());
                    writer.WriteEndElement();
                    _rotationAngle = cmbRotation.SelectedValue.ToString();

                    writer.WriteStartElement("RFIDFontSize");
                    writer.WriteString(txtRFIDFont.Text.Trim());
                    writer.WriteEndElement();
                    _RFIDfontSize = txtRFIDFont.Text.Trim();

                    writer.WriteStartElement("PhotoMarginLeft");
                    writer.WriteString(txtPhotoMarginLeft.Text.Trim());
                    writer.WriteEndElement();
                    _PhotoMarginLeft = txtPhotoMarginLeft.Text.Trim();

                    writer.WriteStartElement("PhotoMarginTop");
                    writer.WriteString(txtPhotoMarginTop.Text.Trim());
                    writer.WriteEndElement();
                    _PhotoMarginTop = txtPhotoMarginTop.Text.Trim();

                    writer.WriteStartElement("PhotoMarginRight");
                    writer.WriteString(txtPhotoMarginRight.Text.Trim());
                    writer.WriteEndElement();
                    _PhotoMarginRight = txtPhotoMarginRight.Text.Trim();

                    writer.WriteStartElement("PhotoMarginBottom");
                    writer.WriteString(txtPhotoMarginBottom.Text.Trim());
                    writer.WriteEndElement();
                    _PhotoMarginBottom = txtPhotoMarginBottom.Text.Trim();

                    writer.WriteStartElement("MessageMarginLeft");
                    writer.WriteString(txtMessageMarginLeft.Text.Trim());
                    writer.WriteEndElement();
                    _MessageMarginLeft = txtMessageMarginLeft.Text.Trim();

                    writer.WriteStartElement("MessageMarginTop");
                    writer.WriteString(txtMessageMarginTop.Text.Trim());
                    writer.WriteEndElement();
                    _MessageMarginTop = txtMessageMarginTop.Text.Trim();

                    writer.WriteStartElement("MessageMarginRight");
                    writer.WriteString(txtMessageMarginRight.Text.Trim());
                    writer.WriteEndElement();
                    _MessageMarginRight = txtMessageMarginRight.Text.Trim();

                    writer.WriteStartElement("MessageMarginBottom");
                    writer.WriteString(txtMessageMarginBottom.Text.Trim());
                    writer.WriteEndElement();
                    _MessageMarginBottom = txtMessageMarginBottom.Text.Trim();

                    writer.WriteStartElement("RFIDBackColor");
                    writer.WriteString(cmbRFIDBackColor.SelectedColor.ToString());
                    writer.WriteEndElement();
                    _RFIDBackColor = cmbRFIDBackColor.SelectedColor.ToString();

                    writer.WriteStartElement("RFIDForeColor");
                    writer.WriteString(cmbRFIDForeColor.SelectedColor.ToString());
                    writer.WriteEndElement();
                    _RFIDForeColor = cmbRFIDForeColor.SelectedColor.ToString();

                    writer.WriteStartElement("RFIDFontWeight");
                    writer.WriteString(cmbRFIDFontWeight.SelectedValue.ToString());
                    writer.WriteEndElement();
                    _RFIDFontWeight = cmbRFIDFontWeight.SelectedValue.ToString();

                    writer.WriteStartElement("HorizentalBorderWidth");
                    writer.WriteString(cmbHorizentalBorderWidth.SelectedValue.ToString());
                    writer.WriteEndElement();
                    _HorizentalBorderWidth = cmbHorizentalBorderWidth.SelectedValue.ToString();

                    writer.WriteStartElement("VerticalBorderWidth");
                    writer.WriteString(cmbVerticalBorderWidth.SelectedValue.ToString());
                    writer.WriteEndElement();
                    _VerticalBorderWidth = cmbVerticalBorderWidth.SelectedValue.ToString();


                    writer.WriteStartElement("StretchImg");
                    writer.WriteString(chkStretchImg.IsChecked.ToString());
                    writer.WriteEndElement();
                    _StretchImg = chkStretchImg.IsChecked.ToString();

                    writer.WriteStartElement("MSGFontSize");
                    writer.WriteString(txtFont.Text.Trim());
                    writer.WriteEndElement();
                    _MSGFontSize = txtFont.Text.Trim();

                    writer.WriteStartElement("MSGForeColor");
                    writer.WriteString(cmbMessageForeColor.SelectedColor.ToString());
                    writer.WriteEndElement();
                    _MsgForeColor = cmbMessageForeColor.SelectedColor.ToString();

                    writer.WriteStartElement("MSGPosition");
                    writer.WriteString(cmbPosition.SelectedValue.ToString());
                    writer.WriteEndElement();
                    _MSGPosition = cmbPosition.SelectedValue.ToString();

                    writer.WriteStartElement("MSGBGColor");
                    writer.WriteString(cmbMsgBGColor.SelectedColor.ToString());
                    writer.WriteEndElement();
                    _MSGBGColor = cmbMessageForeColor.SelectedColor.ToString();


                    writer.WriteStartElement("MSGShowTime");
                    writer.WriteString(txtMsgShowTime.Text.Trim());
                    writer.WriteEndElement();
                    _MsgShowTime = txtMsgShowTime.Text.Trim();
                    writer.WriteEndElement();

                    writer.WriteEndDocument();
                    writer.Close();

                    if (sender == null)
                    {
                        grdSettings.Visibility = Visibility.Hidden;
                        Window_Loaded(sender, e);
                        PreviewWallConfigSetting();
                        GetMarketingVideo();
                        btnCloseScreen.Visibility = Visibility.Collapsed;
                        btnSettings.Visibility = Visibility.Collapsed;
                        CheckIsPreviewWall();
                    }
                    // previewTimeCounter = 0;
                    // if (sender != null)
                    else
                    {
                        System.Windows.Forms.MessageBox.Show("Preview Wall details saved Successfully");
                        Process thisProc = Process.GetCurrentProcess();
                        System.Windows.Forms.Application.Restart();
                        thisProc.Kill();
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }

        private void btnReset_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                cmbSubStore.SelectedValue = "0";
                cmbLocation.SelectedValue = "0";
                cmbScreen.SelectedValue = "--Select--";
                cmbRotation.SelectedValue = "0";// "--Select--";
                txtRFIDFont.Text = string.Empty;
                cmbLocation.IsEnabled = false;
                cmbScreen.IsEnabled = false;
                cmbRFIDFontWeight.SelectedValue = "Normal";
                cmbRFIDBackColor.SelectedColor = ((System.Windows.Media.Color)System.Windows.Media.ColorConverter.ConvertFromString("Black"));
                cmbRFIDForeColor.SelectedColor = ((System.Windows.Media.Color)System.Windows.Media.ColorConverter.ConvertFromString("Black"));
                cmbHorizentalBorderWidth.SelectedValue = "X";
                cmbVerticalBorderWidth.SelectedValue = "X";
                txtFont.Text = string.Empty;
                cmbMessageForeColor.SelectedColor = ((System.Windows.Media.Color)System.Windows.Media.ColorConverter.ConvertFromString("Black"));
                cmbPosition.SelectedValue = "Top-Left";
                cmbMsgBGColor.SelectedColor = ((System.Windows.Media.Color)System.Windows.Media.ColorConverter.ConvertFromString("Black"));
                chkStretchImg.IsChecked = false;
                txtPhotoMarginLeft.Text = "0";
                txtPhotoMarginTop.Text = "0";
                txtPhotoMarginRight.Text = "0";
                txtPhotoMarginBottom.Text = "0";
                txtMessageMarginLeft.Text = "0";
                txtMessageMarginTop.Text = "0";
                txtMessageMarginRight.Text = "0";
                txtMessageMarginBottom.Text = "0";

            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }
        private void btnSettings_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                PreViewWallMedia.Stop();
                grdSettings.Visibility = Visibility.Visible;
                LoadInitialSettings();
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }
        private void btnClose_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                PreViewWallMedia.Play();
                grdSettings.Visibility = Visibility.Collapsed;
                btnCloseScreen.Visibility = Visibility.Visible;
                btnSettings.Visibility = Visibility.Visible;
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }
        private void btnCloseScreen_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Process thisProc = Process.GetCurrentProcess();
                // Check how many total processes have the same name as the current one
                if (Process.GetProcessesByName(thisProc.ProcessName).Length >= 1)
                {
                    PreViewWallMedia.Stop();
                    System.Windows.Application.Current.Shutdown();
                    thisProc.Kill();
                }
                //PreViewWallMedia.Stop();
                //System.Windows.Application.Current.Shutdown();
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
            }

        }
        private void txtRFIDFont_KeyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {
            try
            {
                e.Handled = !IsNumberKey(e.Key);
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
            }
        }

        private void PreViewWallMedia_MediaEnded(object sender, RoutedEventArgs e)
        {
            try
            {
                ErrorHandler.ErrorHandler.LogFileWrite("_isMktImagesActive =" + Convert.ToString(_isMktImagesActive));

                if (_isMktImagesActive)
                    PlayMarketingMedia();
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
            }

        }

        private void tmr_Tick(object sender, EventArgs e)
        {
            try
            {
                if (counter > 0 && counter <= Convert.ToInt32(_MsgShowTime))
                {
                    counter++;
                }
                else if (counter > Convert.ToInt32(_MsgShowTime))
                {
                    counter = 0;
                    lblRFIDAssociate.Visibility = Visibility.Collapsed;
                    imgRFIDAssociate.Visibility = Visibility.Collapsed;//Gif by Vins
                }

                GetGuestMediaFromFolder();
                if (_gusetImgList.Count > 0 & IsGuestMediaAvailable())
                {
                    if (previewTimeCounter != _previewWallInfoSetting.PreviewTimePW && isPlayGuestImg && _guestImgCount != 0)
                    {
                        if (previewTimeCounter == _previewWallInfoSetting.HighLightTimePW)
                            BoarderBlink(false);
                        if (_gusetImgList.Count == _guestImgCount && previewTimeCounter == _previewWallInfoSetting.HighLightTimePW - 1
                            && _previewWallInfoSetting.PreviewTimePW == _previewWallInfoSetting.HighLightTimePW)
                            BoarderBlink(false);
                        previewTimeCounter++;
                        return;
                    }
                    else
                    {
                        PlayGuestMedia();
                    }
                }
                else
                {
                    if ((!ismktActive && _previewWallInfoSetting.IsMktImgPW && _gusetImgList.Count == _guestImgCount) || (previewTimeCounter == _previewWallInfoSetting.PreviewTimePW))
                    {
                        previewTimeCounter = 0;
                        PlayMarketingMedia();
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
            }
        }


        // bool isDummyTagChk = false;
        string result = string.Empty;
        private void txtBarcode_KeyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {
            try
            {
                if (e.Key == Key.Enter)
                {
                    if (lblRFID.Visibility == Visibility.Visible)
                    {
                        if (txtBarcode.Text.Length < 20)
                            QRCODE = txtBarcode.Text.Trim();
                        else if (!string.IsNullOrEmpty(_QrCodeURL))
                            QRCODE = txtBarcode.Text.Replace(_QrCodeURL, string.Empty);
                        if (_dummyTag != null && _dummyTag.Count > 0)
                        {
                            var IsDummyTag = _dummyTag.Where(x => x.ConfigurationValue == QRCODE).FirstOrDefault();
                            if (IsDummyTag != null)
                            {
                                ResetScannSetting();
                                if (_guestImgCount == _gusetImgList.Count)
                                {
                                    lblRFID.Visibility = Visibility.Collapsed;
                                    imgWaterMarkControl.Visibility = Visibility.Collapsed;
                                    SetFrame(false);
                                    _mediaPlayPath = string.Empty;
                                    PreViewWallMedia.Source = null;
                                    BoarderBlink(false);
                                    btnCloseScreen.Visibility = Visibility.Visible;
                                    btnSettings.Visibility = Visibility.Visible;
                                    previewTimeCounter = _previewWallInfoSetting.PreviewTimePW;
                                    if (_previewWallInfoSetting.IsMktImgPW && _mrkMediaList.Count > 0)
                                        dummyTag = true;
                                    //if (_previewWallInfoSetting.IsMktImgPW && _mrkMediaList.Count > 0)
                                    //{
                                    //    if (_mktMediaCount == _mrkMediaList.Count)
                                    //        _mktMediaCount = 0;
                                    //    _mediaPlayPath = _mrkMediaList[_mktMediaCount].FullName;
                                    //    lblRFID.Visibility = Visibility.Collapsed;
                                    //    imgWaterMarkControl.Visibility = Visibility.Collapsed;
                                    //    BoarderBlink(false);
                                    //    _mrkMediaPreviewTime = false;
                                    //    PlayFile(_mediaPlayPath);
                                    //    System.Threading.Thread.Sleep(_marketingMediaReloadTime);
                                    //    _mktMediaCount++;
                                    //    //isPlayGuestImg = false;
                                    //    //_isMktImagesActive = true;
                                    //    //ismktActive = true;
                                    //}
                                }
                                else
                                {
                                    previewTimeCounter = 0;
                                    btnCloseScreen.Visibility = Visibility.Collapsed;
                                    btnSettings.Visibility = Visibility.Collapsed;
                                    PlayGuestMedia();
                                    //isDummyTagChk = false;
                                }
                                return;
                            }

                        }
                        if (!string.IsNullOrEmpty(QRCODE) && QRCODE.Length < 20)
                        {
                            result = new AssociateImageBusiness().AssociateImage(406, "2222" + QRCODE, _photoId, RFIDOverWritesStatus, IsAnonymousQrCodeEnabled,0,string.Empty);
                            if (result == "1")
                            {
                                lblRFIDAssociate.Content = "Success";
                                //lblRFIDAssociate.Visibility = Visibility.Visible;
                                lblRFIDAssociate.Visibility = Visibility.Collapsed;//By Vins

                                imgRFIDAssociate.Visibility = Visibility.Visible;//By Vins
                                counter = counter + 1;
                            }
                            else
                            {
                                lblRFIDAssociate.Content = "Failure";
                                lblRFIDAssociate.Visibility = Visibility.Visible;
                                counter = counter + 1;
                            }

                        }
                        ResetScannSetting();
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
            }
        }
        int counter = 0;

        private void hoverRect_MouseEnter(object sender, System.Windows.Input.MouseEventArgs e)
        {
            try
            {
                if (btnCloseScreen.Visibility == Visibility.Collapsed)
                {
                    btnCloseScreen.Visibility = Visibility.Visible;
                    btnSettings.Visibility = Visibility.Visible;
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
            }
        }
        private void PreViewWallMedia_MouseDown(object sender, MouseButtonEventArgs e)
        {
            try
            {
                EnableSetting();
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
            }
        }

        private void imgBlinkVertical_MouseDown(object sender, MouseButtonEventArgs e)
        {
            try
            {
                EnableSetting();
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
            }
        }
        private void imgHorizentalFrame_MouseDown(object sender, MouseButtonEventArgs e)
        {
            try
            {
                EnableSetting();
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
            }
        }
        private void imgVerticalFrame_MouseDown(object sender, MouseButtonEventArgs e)
        {
            try
            {
                EnableSetting();
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
            }
        }


        private void imgBlinkHorizental_MouseDown(object sender, MouseButtonEventArgs e)
        {
            try
            {
                EnableSetting();
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
            }
        }
        private void imgWaterMarkControl_MouseDown(object sender, MouseButtonEventArgs e)
        {
            try
            {
                EnableSetting();
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
            }
        }


        #endregion

        #region Common Methods
        private void CheckIsPreviewWall()
        {
            if (_previewWallInfoSetting != null && !_previewWallInfoSetting.IsPreviewEnabledPW)
            {
                btnCloseScreen.Visibility = Visibility.Visible;
                btnSettings.Visibility = Visibility.Collapsed;
                grdSettings.Visibility = Visibility.Collapsed;
                PreViewWallMedia.Visibility = Visibility.Collapsed;
                imgWaterMarkControl.Visibility = Visibility.Collapsed;
                imgLogoControl.Visibility = Visibility.Collapsed;
                imgBlinkVertical.Visibility = Visibility.Collapsed;
                imgBlinkHorizental.Visibility = Visibility.Collapsed;
                imgHorizentalFrame.Visibility = Visibility.Collapsed;
                imgVerticalFrame.Visibility = Visibility.Collapsed;
                if (!_showMsg)
                {
                    System.Windows.MessageBox.Show("Please enable Preview Wall setting from digiconfig..");
                    _showMsg = true;
                }
            }
            if (_gusetImgList != null && _mrkMediaList != null && _gusetImgList.Count == 0 && _mrkMediaList.Count == 0)
            {
                btnCloseScreen.Visibility = Visibility.Visible;
                btnSettings.Visibility = Visibility.Visible;
            }

        }
        private void PreviewWallConfigSetting()
        {
            ConfigurationInfo _objdata = (new ConfigBusiness()).GetConfigurationData(Convert.ToInt32(cmbSubStore.SelectedValue));
            if (_objdata != null)
            {
                //string siteName, locationName = string.Empty;
                List<SubStoresInfo> SiteLst = (new StoreSubStoreDataBusniess()).GetLoginUserDefaultSubstores(Convert.ToInt32(cmbSubStore.SelectedValue));
                if (SiteLst.Count > 0)
                {
                    siteName = SiteLst[0].DG_SubStore_Name;
                    LocationInfo locBiz = (new LocationBusniess()).GetLocationsbyId(Convert.ToInt32(cmbLocation.SelectedValue));
                    if (locBiz != null)
                    {
                        locationName = locBiz.DG_Location_Name;
                        _hotFolderPath = _objdata.DG_Hot_Folder_Path;
                        _guestImgPath = _hotFolderPath + "\\PreviewWall\\" + siteName + "\\" + locationName + "\\" + cmbScreen.SelectedValue;
                        if (File.Exists(_hotFolderPath + "\\PreviewWall\\" + siteName + "\\" + locationName + "\\" + cmbScreen.SelectedValue + "\\MasterData\\WaterMark.gif"))
                        {
                            _waterMarkImg = _hotFolderPath + "\\PreviewWall\\" + siteName + "\\" + locationName + "\\" + cmbScreen.SelectedValue + "\\MasterData\\WaterMark.gif";
                        }
                        else
                        {
                            _waterMarkImg = _hotFolderPath + "\\PreviewWall\\" + siteName + "\\" + locationName + "\\" + cmbScreen.SelectedValue + "\\MasterData\\WaterMark.PNG";
                        }
                        _verticalFrame = _hotFolderPath + "\\PreviewWall\\" + siteName + "\\" + locationName + "\\" + cmbScreen.SelectedValue + "\\MasterData\\VerticalBorder.PNG";
                        _horizentalFrame = _hotFolderPath + "\\PreviewWall\\" + siteName + "\\" + locationName + "\\" + cmbScreen.SelectedValue + "\\MasterData\\HorizentalBorder.PNG";
                        _logoImg = _hotFolderPath + "\\PreviewWall\\" + siteName + "\\" + locationName + "\\" + cmbScreen.SelectedValue + "\\MasterData\\Logo.PNG";
                        _gusetImgList.Clear();

                        _locationWiseConfigParamsList = (new ConfigBusiness()).GetLocationWiseConfigParams(Convert.ToInt32(cmbSubStore.SelectedValue));
                        List<iMixConfigurationLocationInfo> lstLocation = _locationWiseConfigParamsList.Where(n => n.LocationId == Convert.ToInt32(cmbLocation.SelectedValue)).ToList();
                        _previewWallInfoSetting = new PreviewWallInfo();

                        #region Added By VinS 30Aug2019
                        //Set the values for GuestExplanatoryImgPathPW, IsHorizontalGuestExplatory, IsLoopPreviewPhotos, LoopPreviewPhotosTime 
                        _previewWallInfoSetting.GuestExplanatoryImgPathPW = string.Empty;
                        _previewWallInfoSetting.IsHorizontalGuestExplatory = false;
                        _previewWallInfoSetting.IsLoopPreviewPhotos = true;
                        _previewWallInfoSetting.LoopPreviewPhotosTime = string.Empty;
                        //
                        #endregion

                        foreach (var loc in lstLocation)
                        {
                            switch (loc.IMIXConfigurationMasterId)
                            {
                                case (int)ConfigParams.NoOfScreenPW:
                                    _previewWallInfoSetting.NoOfScreenPW = Convert.ToInt32(loc.ConfigurationValue);
                                    break;
                                case (int)ConfigParams.DelayTimePW:
                                    _previewWallInfoSetting.DelayTimePW = Convert.ToInt32(loc.ConfigurationValue);
                                    break;
                                case (int)ConfigParams.PreviewTimePW:
                                    _previewWallInfoSetting.PreviewTimePW = Convert.ToInt32(loc.ConfigurationValue) - _previewHighlightAdjustTime;
                                    break;
                                case (int)ConfigParams.HighLightTimePW:
                                    _previewWallInfoSetting.HighLightTimePW = Convert.ToInt32(loc.ConfigurationValue) - _previewHighlightAdjustTime;
                                    break;
                                case (int)ConfigParams.IsMktImgPW:
                                    _previewWallInfoSetting.IsMktImgPW = Convert.ToBoolean(loc.ConfigurationValue);
                                    break;
                                case (int)ConfigParams.WaterMarkPathPW:
                                    //_previewWallInfoSetting.WaterMarkPathPW = loc.ConfigurationValue;

                                    if (File.Exists(_hotFolderPath + "\\PreviewWall\\" + siteName + "\\" + locationName + "\\" + cmbScreen.SelectedValue + "\\MasterData\\WaterMark.gif"))
                                    {
                                        _previewWallInfoSetting.WaterMarkPathPW = _hotFolderPath + "\\PreviewWall\\" + siteName + "\\" + locationName + "\\" + cmbScreen.SelectedValue + "\\MasterData\\WaterMark.gif";
                                    }
                                    else
                                    {
                                        _previewWallInfoSetting.WaterMarkPathPW = loc.ConfigurationValue;
                                    }
                                    break;
                                case (int)ConfigParams.IsLogoPW:
                                    _previewWallInfoSetting.IsLogoPW = Convert.ToBoolean(loc.ConfigurationValue);
                                    break;
                                case (int)ConfigParams.LogoPathPW:
                                    _previewWallInfoSetting.LogoPathPW = loc.ConfigurationValue;
                                    break;
                                case (int)ConfigParams.IsPreviewEnabledPW:
                                    _previewWallInfoSetting.IsPreviewEnabledPW = Convert.ToBoolean(loc.ConfigurationValue);
                                    break;
                                case (int)ConfigParams.IsSpecImgPW:
                                    _previewWallInfoSetting.IsSpecImgPW = Convert.ToBoolean(loc.ConfigurationValue);
                                    break;
                                case (int)ConfigParams.MktImgPathPW:
                                    _previewWallInfoSetting.MarketingImgPathPW = loc.ConfigurationValue;
                                    _mktImgPath = loc.ConfigurationValue;
                                    break;
                                case (int)ConfigParams.HorizentalBorderPW:
                                    _previewWallInfoSetting.HorizentalBorderPathPW = loc.ConfigurationValue;
                                    break;
                                case (int)ConfigParams.VerticalBorderPW:
                                    _previewWallInfoSetting.VerticalBorderPathPW = loc.ConfigurationValue;
                                    break;
                                case (int)ConfigParams.PreviewWallLogoPosition:
                                    _previewWallInfoSetting.LogoPosition = loc.ConfigurationValue;
                                    break;
                                case (int)ConfigParams.PreviewWallRFIDPostion:
                                    _previewWallInfoSetting.RFIDPostion = loc.ConfigurationValue;
                                    break;
                                case (int)ConfigParams.GuestExplanatoryImgPathPW:
                                    _previewWallInfoSetting.GuestExplanatoryImgPathPW = loc.ConfigurationValue;
                                    break;
                                case (int)ConfigParams.IsLoopPreviewPhotos:
                                    _previewWallInfoSetting.IsLoopPreviewPhotos = Convert.ToBoolean(loc.ConfigurationValue);
                                    break;
                                case (int)ConfigParams.IsHorizontalGuestExplatory:
                                    _previewWallInfoSetting.IsHorizontalGuestExplatory = Convert.ToBoolean(loc.ConfigurationValue);
                                    break;
                                case (int)ConfigParams.LoopPreviewPhotosTime:
                                    _previewWallInfoSetting.LoopPreviewPhotosTime = loc.ConfigurationValue;
                                    break;
                                case (int)ConfigParams.RideNoOfCaptures:
                                    _previewWallInfoSetting.RideNoOfCaptures = loc.ConfigurationValue;
                                    break;
                                case (int)ConfigParams.RideNoOfCapturesIterations:
                                    _previewWallInfoSetting.RideNoOfCapturesIterations = loc.ConfigurationValue;
                                    break;
                            }
                        }
                        ImageLoad();
                        SetLogoPosition();
                        SetRFIDPosition();
                        SetAssociatedRFIdPosition();
                        CleanPreviewGuestFile();
                        SetGuestExplanatoryImgPosition();//Added by Vins_30Aug2019
                    }
                }
                _dummyTag = ((new ConfigBusiness()).GetStoreConfigData()).Where(x => x.IMIXConfigurationMasterId == Convert.ToInt64(ConfigParams.PreviewWallDummyTag)).ToList();

            }
        }


        private void gif_MediaEnded(object sender, RoutedEventArgs e)
        {
            imgHorzGuestExplain.Position = new TimeSpan(0, 0, 1);
            imgHorzGuestExplain.Play();

            imgVertGuestExplain.Position = new TimeSpan(0, 0, 1);
            imgVertGuestExplain.Play();
        }

        private void SetGuestExplanatoryImgPosition()
        {
            try
            {
                ErrorHandler.ErrorHandler.LogFileWrite("SetGuestExplanatoryImgPosition =>" + _previewWallInfoSetting.IsHorizontalGuestExplatory.ToString());
                //imgHorzGuestExplain imgVertGuestExplain
                if (_previewWallInfoSetting.IsHorizontalGuestExplatory)
                {
                    imgHorzGuestExplain.Visibility = Visibility.Visible;
                    imgVertGuestExplain.Visibility = Visibility.Collapsed;

                    //string _guest = string.Empty;
                    //_guestExplainatoryImg = _hotFolderPath + "\\PreviewWall\\" + siteName + "\\" + locationName + "\\" + cmbScreen.SelectedValue + "\\MasterData\\HorizontalScan.gif";
                    _guestExplainatoryImg = "\\" + _hotFolderPath.Replace("\\\\", "\\") + "PreviewWall\\" + siteName + "\\" + locationName + "\\" + cmbScreen.SelectedValue + "\\MasterData\\HorizontalScan.gif";
                    ErrorHandler.ErrorHandler.LogFileWrite("SetGuestExplanatoryImgPosition =>" + _guestExplainatoryImg);

                    _guestExplainatoryImg = _guestExplainatoryImg.Trim();
                    if (File.Exists(_guestExplainatoryImg))
                    {
                        ErrorHandler.ErrorHandler.LogFileWrite("File exists");
                        imgHorzGuestExplain.Source = new Uri(_guestExplainatoryImg, UriKind.RelativeOrAbsolute);
                    }
                    else
                    {
                        ErrorHandler.ErrorHandler.LogFileWrite(_guestExplainatoryImg + "File not exists");
                    }
                }
                else if (!_previewWallInfoSetting.IsHorizontalGuestExplatory)
                {
                    imgVertGuestExplain.Visibility = Visibility.Visible;
                    imgHorzGuestExplain.Visibility = Visibility.Collapsed;

                    _guestExplainatoryImg = "\\" + _hotFolderPath.Replace("\\\\", "\\") + "\\PreviewWall\\" + siteName + "\\" + locationName + "\\" + cmbScreen.SelectedValue + "\\MasterData\\VerticalScan.gif";
                    //var gstExplainatoryImg = new BitmapImage();
                    //gstExplainatoryImg.BeginInit();
                    //gstExplainatoryImg.UriSource = new Uri(_guestExplainatoryImg, UriKind.RelativeOrAbsolute);
                    //gstExplainatoryImg.EndInit();
                    //ImageBehavior.SetAnimatedSource(imgVertGuestExplain, gstExplainatoryImg);
                    //ImageBehavior.SetRepeatBehavior(imgVertGuestExplain, new RepeatBehavior(0));
                    //ImageBehavior.SetRepeatBehavior(imgVertGuestExplain, RepeatBehavior.Forever);


                    _guestExplainatoryImg = _guestExplainatoryImg.Trim();
                    if (File.Exists(_guestExplainatoryImg))
                    {
                        imgVertGuestExplain.Source = new Uri(_guestExplainatoryImg, UriKind.RelativeOrAbsolute);
                    }

                }
            }
            catch (Exception ex)
            {

            }
        }

        private void SetAssociatedRFIdPosition()
        {
            if (_MSGPosition == "Top-Left")
            {
                RFIDAssociateStackPanel.VerticalAlignment = System.Windows.VerticalAlignment.Top;
                RFIDAssociateStackPanel.HorizontalAlignment = System.Windows.HorizontalAlignment.Left;
            }
            else if (_MSGPosition == "Top-Center")
            {
                RFIDAssociateStackPanel.VerticalAlignment = System.Windows.VerticalAlignment.Top;
                RFIDAssociateStackPanel.HorizontalAlignment = System.Windows.HorizontalAlignment.Center;
            }
            else if (_MSGPosition == "Top-Right")
            {
                RFIDAssociateStackPanel.VerticalAlignment = System.Windows.VerticalAlignment.Top;
                RFIDAssociateStackPanel.HorizontalAlignment = System.Windows.HorizontalAlignment.Right;
            }
            else if (_MSGPosition == "Bottom-Left")
            {
                RFIDAssociateStackPanel.VerticalAlignment = System.Windows.VerticalAlignment.Bottom;
                RFIDAssociateStackPanel.HorizontalAlignment = System.Windows.HorizontalAlignment.Left;
            }
            else if (_MSGPosition == "Bottom-Center")
            {
                RFIDAssociateStackPanel.VerticalAlignment = System.Windows.VerticalAlignment.Bottom;
                RFIDAssociateStackPanel.HorizontalAlignment = System.Windows.HorizontalAlignment.Center;
            }
            else if (_MSGPosition == "Bottom-Right")
            {
                RFIDAssociateStackPanel.VerticalAlignment = System.Windows.VerticalAlignment.Bottom;
                RFIDAssociateStackPanel.HorizontalAlignment = System.Windows.HorizontalAlignment.Right;
            }
            else if (_MSGPosition == "Center")
            {
                RFIDAssociateStackPanel.VerticalAlignment = System.Windows.VerticalAlignment.Center;
                RFIDAssociateStackPanel.HorizontalAlignment = System.Windows.HorizontalAlignment.Center;
            }
            RFIDAssociateStackPanel.Margin = new Thickness(Convert.ToDouble(txtMessageMarginLeft.Text.Trim()), Convert.ToDouble(txtMessageMarginTop.Text.Trim()),
                                              Convert.ToDouble(txtMessageMarginRight.Text.Trim()), Convert.ToDouble(txtMessageMarginBottom.Text.Trim()));
        }
        private void SetLogoPosition()
        {
            if (_previewWallInfoSetting.LogoPosition == "Top Left")
            {
                imgLogoControl.VerticalAlignment = System.Windows.VerticalAlignment.Top;
                imgLogoControl.HorizontalAlignment = System.Windows.HorizontalAlignment.Left;
            }
            else if (_previewWallInfoSetting.LogoPosition == "Top Centre")
            {
                imgLogoControl.VerticalAlignment = System.Windows.VerticalAlignment.Top;
                imgLogoControl.HorizontalAlignment = System.Windows.HorizontalAlignment.Center;
            }
            else if (_previewWallInfoSetting.LogoPosition == "Top Right")
            {
                imgLogoControl.VerticalAlignment = System.Windows.VerticalAlignment.Top;
                imgLogoControl.HorizontalAlignment = System.Windows.HorizontalAlignment.Right;
            }
            else if (_previewWallInfoSetting.LogoPosition == "Bottom Left")
            {
                imgLogoControl.VerticalAlignment = System.Windows.VerticalAlignment.Bottom;
                imgLogoControl.HorizontalAlignment = System.Windows.HorizontalAlignment.Left;
            }
            else if (_previewWallInfoSetting.LogoPosition == "Bottom Centre")
            {
                imgLogoControl.VerticalAlignment = System.Windows.VerticalAlignment.Bottom;
                imgLogoControl.HorizontalAlignment = System.Windows.HorizontalAlignment.Center;
            }
            else if (_previewWallInfoSetting.LogoPosition == "Bottom Right")
            {
                imgLogoControl.VerticalAlignment = System.Windows.VerticalAlignment.Bottom;
                imgLogoControl.HorizontalAlignment = System.Windows.HorizontalAlignment.Right;
            }
        }
        private void SetRFIDPosition()
        {
            if (_previewWallInfoSetting.RFIDPostion == "Top Left")
            {
                RFIDStackPanel.VerticalAlignment = System.Windows.VerticalAlignment.Top;
                RFIDStackPanel.HorizontalAlignment = System.Windows.HorizontalAlignment.Left;
            }
            else if (_previewWallInfoSetting.RFIDPostion == "Top Centre")
            {
                RFIDStackPanel.VerticalAlignment = System.Windows.VerticalAlignment.Top;
                RFIDStackPanel.HorizontalAlignment = System.Windows.HorizontalAlignment.Center;
            }
            else if (_previewWallInfoSetting.RFIDPostion == "Top Right")
            {
                RFIDStackPanel.VerticalAlignment = System.Windows.VerticalAlignment.Top;
                RFIDStackPanel.HorizontalAlignment = System.Windows.HorizontalAlignment.Right;
            }
            else if (_previewWallInfoSetting.RFIDPostion == "Bottom Left")
            {
                RFIDStackPanel.VerticalAlignment = System.Windows.VerticalAlignment.Bottom;
                RFIDStackPanel.HorizontalAlignment = System.Windows.HorizontalAlignment.Left;
            }
            else if (_previewWallInfoSetting.RFIDPostion == "Bottom Centre")
            {
                RFIDStackPanel.VerticalAlignment = System.Windows.VerticalAlignment.Bottom;
                RFIDStackPanel.HorizontalAlignment = System.Windows.HorizontalAlignment.Center;
            }
            else if (_previewWallInfoSetting.RFIDPostion == "Bottom Right")
            {
                RFIDStackPanel.VerticalAlignment = System.Windows.VerticalAlignment.Bottom;
                RFIDStackPanel.HorizontalAlignment = System.Windows.HorizontalAlignment.Right;
            }
        }
        private void CleanPreviewGuestFile()
        {
            _lastCleanUpTime = DateTime.Now.AddMinutes(_previewWallCleanUpTimeInMinute);
            var myDirInfo = new DirectoryInfo(_guestImgPath);
            var filelist = myDirInfo.GetFilesByExtensions(".jpg", ".png", ".mp4",
                                            ".wmv", ".mpg", ".avi", ".3gp", ".gif").ToList();
            filelist = filelist.Where(s => s.Attributes == FileAttributes.Offline).ToList();
            if (filelist != null && filelist.Count > 0)
            {
                foreach (FileInfo delFile in filelist)
                    File.Delete(delFile.FullName);
            }
            //for (int i = 0; i <= filelist.Count; i++)
            //{
            //    File.Delete(filelist[i].FullName);
            //}
        }

        private void ResetOldSetting()
        {
            latestGusetImageList.Clear();
            _mktMediaCount = 0;
            _guestImgCount = 0;
            _lastPickTime = DateTime.MinValue;
        }
        private void BoarderBlink(bool blinkActive)
        {
            if (!string.IsNullOrEmpty(_previewWallInfoSetting.HighLightTimePW.ToString()))
            {
                if (blinkActive)
                {
                    if (_rotationAngle == "90" || _rotationAngle == "270")
                    {
                        imgBlinkVertical.Visibility = Visibility.Visible;
                        imgBlinkHorizental.Visibility = Visibility.Hidden;
                    }
                    else
                    {
                        imgBlinkVertical.Visibility = Visibility.Hidden;
                        imgBlinkHorizental.Visibility = Visibility.Visible;
                    }
                }
                else
                {
                    imgBlinkVertical.Visibility = Visibility.Hidden;
                    imgBlinkHorizental.Visibility = Visibility.Hidden;
                }
            }
        }
        private void SetFrame(bool blinkActive)
        {
            if (!string.IsNullOrEmpty(_previewWallInfoSetting.VerticalBorderPathPW) || !string.IsNullOrEmpty(_previewWallInfoSetting.HorizentalBorderPathPW))
            {
                if (blinkActive)
                {
                    if (_rotationAngle == "90" || _rotationAngle == "270")
                    {
                        imgVerticalFrame.Visibility = Visibility.Visible;
                        imgHorizentalFrame.Visibility = Visibility.Hidden;
                    }
                    else
                    {
                        imgVerticalFrame.Visibility = Visibility.Hidden;
                        imgHorizentalFrame.Visibility = Visibility.Visible;
                    }
                }
                else
                {
                    imgHorizentalFrame.Visibility = Visibility.Hidden;
                    imgVerticalFrame.Visibility = Visibility.Hidden;
                }
            }
        }

        private void ResetScannSetting()
        {
            txtBarcode.Text = string.Empty;
            QRCODE = string.Empty;
            //_photoId = string.Empty;
        }

        private void EnableSetting()
        {
            if (btnCloseScreen.Visibility == Visibility.Hidden || btnCloseScreen.Visibility == Visibility.Collapsed)
            {
                btnCloseScreen.Visibility = Visibility.Visible;
                btnSettings.Visibility = Visibility.Visible;
            }
            else
            {
                btnCloseScreen.Visibility = Visibility.Hidden;
                btnSettings.Visibility = Visibility.Hidden;
            }

        }
        private void ImageLoad()
        {
            if (Directory.Exists(_guestImgPath))
            {
                if (!string.IsNullOrEmpty(_previewWallInfoSetting.WaterMarkPathPW))
                {
                    if (File.Exists(_waterMarkImg))
                    {
                        if (_waterMarkImg.Contains("gif"))
                        {
                            //Enable GIF animation_Vinod Salunke 25Jul2018
                            var waterMarkImage = new BitmapImage();
                            waterMarkImage.BeginInit();
                            waterMarkImage.UriSource = new Uri(_waterMarkImg, UriKind.RelativeOrAbsolute);
                            waterMarkImage.EndInit();
                            ImageBehavior.SetAnimatedSource(imgWaterMarkControl, waterMarkImage);
                            ImageBehavior.SetRepeatBehavior(imgWaterMarkControl, new RepeatBehavior(0));
                            ImageBehavior.SetRepeatBehavior(imgWaterMarkControl, RepeatBehavior.Forever);
                        }
                        else
                        {
                            imgWaterMarkControl.Source = new BitmapImage(new Uri(_waterMarkImg));
                        }
                    }
                    else
                        imgWaterMarkControl.Source = null;
                }
                if (!string.IsNullOrEmpty(_previewWallInfoSetting.HorizentalBorderPathPW))
                {
                    if (File.Exists(_horizentalFrame))
                        imgHorizentalFrame.Source = new BitmapImage(new Uri(_horizentalFrame));//,UriKind.Absolute));
                    else
                        imgHorizentalFrame.Source = null;
                }
                if (!string.IsNullOrEmpty(_previewWallInfoSetting.VerticalBorderPathPW))
                {
                    if (File.Exists(_verticalFrame))
                        imgVerticalFrame.Source = new BitmapImage(new Uri(_verticalFrame));//,UriKind.Absolute));
                    else
                        imgVerticalFrame.Source = null;
                }
                if (_previewWallInfoSetting.IsLogoPW == true)
                {
                    if (File.Exists(_logoImg))
                        imgLogoControl.Source = new BitmapImage(new Uri(_logoImg));//, UriKind.Absolute));
                    else
                        imgLogoControl.Source = null;
                }
            }
            ReloadGuestImgTimer();
        }
        bool ismktActive = false;
        public void PlayMarketingMedia()
        {
            _mediaPlayPath = string.Empty;
            if (_isMktImagesActive && _previewWallInfoSetting.IsMktImgPW && _mrkMediaList.Count > 0)
            {
                if (_mktMediaCount == _mrkMediaList.Count)
                    _mktMediaCount = 0;
                _mediaPlayPath = _mrkMediaList[_mktMediaCount].FullName;
                lblRFID.Visibility = Visibility.Collapsed;
                imgWaterMarkControl.Visibility = Visibility.Collapsed;
                SetFrame(false);
                BoarderBlink(false);
                _mrkMediaPreviewTime = false;
                PreViewWallMedia.Margin = new Thickness(0, 0, 0, 0);
                PlayFile(_mediaPlayPath);
                System.Threading.Thread.Sleep(_marketingMediaReloadTime);

                ErrorHandler.ErrorHandler.LogFileWrite(Convert.ToString(_marketingMediaReloadTime));

                _mktMediaCount++;
                isPlayGuestImg = false;
                _isMktImagesActive = true;
                ismktActive = true;

                //Vins
                //var myDirInfo = new DirectoryInfo(_guestImgPath);
                //latestGusetImageList = myDirInfo.GetFilesByExtensions(".jpg", ".png", ".mp4",
                //                                ".wmv", ".mpg", ".avi", ".3gp", ".JPG", ".PNG", ".MP4",
                //                                ".WMV", ".MPG", ".AVI", ".3GP").ToList();
                //latestGusetImageList = latestGusetImageList.Where(file => file.CreationTime <= _currentDateTime && file.CreationTime > _lastPickTime && file.Attributes != FileAttributes.Offline).OrderBy(file => file.CreationTime).ToList();

                //if (latestGusetImageList.Count > 0)
                //{
                //    foreach (FileInfo delFile in latestGusetImageList)
                //        File.Delete(delFile.FullName);
                //}
            }
        }
        private void txtRFIDFont_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = !IsTextAllowed(e.Text);
        }
        private static bool IsTextAllowed(string text)
        {
            Regex regex = new Regex("[^0-9]+");
            return !regex.IsMatch(text);
        }
        private void PlayFile(string fileName)
        {
            PreViewWallMedia.Stop();
            PreViewWallMedia.Source = new Uri(_mediaPlayPath, UriKind.Relative);
            PreViewWallMedia.Play();
        }
        private void GetGuestMediaFromFolder()
        {
            // _previewWallIteration = 0;

            if (!string.IsNullOrEmpty(_guestImgPath))
            {
                _currentDateTime = DateTime.Now.AddSeconds(-_previewWallInfoSetting.DelayTimePW);
                var myDirInfo = new DirectoryInfo(_guestImgPath);
                latestGusetImageList = myDirInfo.GetFilesByExtensions(".jpg", ".png", ".mp4",
                                                ".wmv", ".mpg", ".avi", ".3gp", ".JPG", ".PNG", ".MP4",
                                                ".WMV", ".MPG", ".AVI", ".3GP").ToList(); //Existing

                lstGusetImageIteration = myDirInfo.GetFilesByExtensions(".jpg", ".png", ".mp4",
                                                 ".wmv", ".mpg", ".avi", ".3gp", ".JPG", ".PNG", ".MP4",
                                                 ".WMV", ".MPG", ".AVI", ".3GP").ToList();

                //lstGusetImageIteration = lstGusetImageIteration.Where(file => file.CreationTime <= _currentDateTime && file.CreationTime > _lastPickTime && file.Attributes != FileAttributes.Offline).OrderBy(file => file.CreationTime).ToList();
                lstGusetImageIteration = lstGusetImageIteration.Where(file => file.Attributes != FileAttributes.Offline).OrderBy(file => file.CreationTime).ToList();

                //latestGusetImageList = myDirInfo.GetFilesByExtensions(".jpg", ".png", ".mp4",
                //                                ".wmv", ".mpg", ".avi", ".3gp", ".JPG", ".PNG", ".MP4",
                //                                ".WMV", ".MPG", ".AVI", ".3GP").OrderBy(file=>file.CreationTime).Skip(Convert.ToInt32(_previewWallInfoSetting.RideNoOfCaptures)).ToList();
                //if(latestGusetImageList.Count <= Convert.ToInt32(_previewWallInfoSetting.RideNoOfCaptures))
                //{
                //    if(_previewWallIteration<= Convert.ToInt32(_previewWallInfoSetting.RideNoOfCapturesIterations))
                //    {

                //    }
                //    else
                //    {
                //        latestGusetImageList = myDirInfo.GetFilesByExtensions(".jpg", ".png", ".mp4",
                //                                ".wmv", ".mpg", ".avi", ".3gp", ".JPG", ".PNG", ".MP4",
                //                                ".WMV", ".MPG", ".AVI", ".3GP").OrderBy(file => file.CreationTime).ToList();
                //    }
                //}


                //Skip the last ride captures to iterate in loop of no of iterations defined in Digiconfig utility
                //latestGusetImageList = latestGusetImageList.Where(file => file.CreationTime <= _currentDateTime && file.CreationTime > _lastPickTime && file.Attributes != FileAttributes.Offline).OrderBy(file => file.CreationTime).Skip(Convert.ToInt32(_previewWallInfoSetting.RideNoOfCaptures)).ToList();

                latestGusetImageList = latestGusetImageList.Where(file => file.CreationTime <= _currentDateTime && file.Attributes != FileAttributes.Offline).OrderBy(file => file.CreationTime).Skip(Convert.ToInt32(_previewWallInfoSetting.RideNoOfCaptures)).ToList();

                if (_previewWallIteration > Convert.ToInt32(_previewWallInfoSetting.RideNoOfCapturesIterations))
                {
                    _previewWallIteration = 0;
                }


                if (latestGusetImageList.Count > 0)
                {
                    TimeSpan diff = DateTime.Now - lastFilePlayTime;
                    if (diff.Seconds >= _previewWallInfoSetting.PreviewTimePW)
                        previewTimeCounter = _previewWallInfoSetting.PreviewTimePW;
                    _gusetImgList.AddRange(latestGusetImageList);



                    _gusetImgList = _gusetImgList.DistinctBy(p => new { p.FullName, p.Name }).ToList();
                    _lastPickTime = _currentDateTime.AddSeconds(-2);
                    needToLoop = true;
                    dummyTag = false;
                    //isDummyTagChk = true;

                    _iterationImgCount = 0;//ResetOldSetting the count
                    _previewWallIteration = 0;
                }
                else
                {
                    ////Start
                    ////Added by Vins
                    if (latestGusetImageList.Count == 0 && lstGusetImageIteration.Count <= Convert.ToInt32(_previewWallInfoSetting.RideNoOfCaptures))
                    {
                        //if (lstGusetImageIteration.Count > 0 && _previewWallIteration <= Convert.ToInt32(_previewWallInfoSetting.RideNoOfCaptures))
                        //{
                        latestGusetImageList = lstGusetImageIteration;
                        _gusetImgList.Clear();

                        TimeSpan diff = DateTime.Now - lastFilePlayTime;
                        if (diff.Seconds >= _previewWallInfoSetting.PreviewTimePW)
                            previewTimeCounter = _previewWallInfoSetting.PreviewTimePW;
                        _gusetImgList.AddRange(latestGusetImageList);

                        //_guestImgCount = 0;
                        _gusetImgList = _gusetImgList.DistinctBy(p => new { p.FullName, p.Name }).ToList();
                        _lastPickTime = _currentDateTime.AddSeconds(-2);
                        needToLoop = true;
                        dummyTag = false;
                        if (_previewWallIteration == 0)
                        {
                            _previewWallIteration++;
                        }
                        //}
                        if (_iterationImgCount != 0 && _iterationImgCount >= _gusetImgList.Count)
                        {
                            _iterationImgCount = 0;
                            _previewWallIteration++;
                        }

                    }
                    else
                    {
                        _previewWallIteration = 0;
                    }
                    ////End
                }
                if (_lastCleanUpTime < DateTime.Now)
                {
                    CleanPreviewGuestFile();
                }
                //_lastPickTime = _currentDateTime.AddSeconds(-2);
            }

        }

        int previewTimeCounter = 0;
        DateTime lastFilePlayTime;
        private string PlayGuestMedia()
        {
            lblRFIDAssociate.Visibility = Visibility.Collapsed;
            //GIF
            imgRFIDAssociate.Visibility = Visibility.Collapsed;

            counter = 0;
            int k = 0;
            if (!string.IsNullOrEmpty(_guestImgPath))
            {
                //if condition for iterating last ride photos in loop
                if (_previewWallIteration <= Convert.ToInt32(_previewWallInfoSetting.RideNoOfCapturesIterations) && _previewWallIteration != 0)
                {
                    isPlayGuestImg = false;

                    if (_guestImgCount < _gusetImgList.Count)
                    {
                        _guestImgCount = 0;
                        for (k = _guestImgCount; k < _gusetImgList.Count; k++)
                        {
                            ErrorHandler.ErrorHandler.LogFileWrite("Iteration loop" + Convert.ToString(_guestImgCount));
                            isPlayGuestImg = true;
                            _guestImgCount++;
                            _mediaPlayPath = _gusetImgList[_iterationImgCount].FullName;
                            if (!string.IsNullOrEmpty(_mediaPlayPath) && System.IO.File.Exists(_mediaPlayPath))
                            {
                                lblRFID.Content = _gusetImgList[_iterationImgCount].Name.Split('@')[0].ToString();
                                _photoId = _gusetImgList[_iterationImgCount].Name.Split('@')[1].ToString().Split('.')[0].ToString();  //.Replace(".jpg", "").Trim();
                                break;
                            }
                        }
                    }

                    if (!string.IsNullOrEmpty(_mediaPlayPath) && System.IO.File.Exists(_mediaPlayPath))
                    {
                        previewTimeCounter = _previewWallInfoSetting.PreviewTimePW;
                        ErrorHandler.ErrorHandler.LogFileWrite("Not Working set ");
                        ErrorHandler.ErrorHandler.LogFileWrite("previewTimeCounter = " + Convert.ToString(previewTimeCounter));
                        ErrorHandler.ErrorHandler.LogFileWrite("_guestMediaReloadTime = " + Convert.ToString(_guestMediaReloadTime));

                        PreViewWallMedia.Margin = new Thickness(Convert.ToDouble(txtPhotoMarginLeft.Text.Trim()), Convert.ToDouble(txtPhotoMarginTop.Text.Trim()),
                                                  Convert.ToDouble(txtPhotoMarginRight.Text.Trim()), Convert.ToDouble(txtPhotoMarginBottom.Text.Trim()));
                        lblRFID.Visibility = Visibility.Visible;
                        BoarderBlink(true);
                        imgWaterMarkControl.Visibility = Visibility.Collapsed;
                        SetFrame(false);
                        PlayFile(_mediaPlayPath);
                        System.Threading.Thread.Sleep(_guestMediaReloadTime);
                        //System.Threading.Thread.Sleep(previewTimeCounter);
                        imgWaterMarkControl.Visibility = Visibility.Visible;
                        SetFrame(true);
                        _mrkMediaPreviewTime = true;
                        lastFilePlayTime = DateTime.Now;
                        _isMktImagesActive = false;
                        previewTimeCounter = 0;
                        if (_previewWallIteration == Convert.ToInt32(_previewWallInfoSetting.RideNoOfCapturesIterations)) //last iteration
                        {
                            _gusetImgList[_iterationImgCount].Attributes = FileAttributes.Offline;
                        }
                        _iterationImgCount++;
                    }
                }
                else
                {
                    isPlayGuestImg = false;
                    for (k = _guestImgCount; k < _gusetImgList.Count; k++)
                    {
                        ErrorHandler.ErrorHandler.LogFileWrite("Normal loop" + Convert.ToString(_guestImgCount));
                        isPlayGuestImg = true;
                        _guestImgCount++;
                        _mediaPlayPath = _gusetImgList[k].FullName;
                        if (!string.IsNullOrEmpty(_mediaPlayPath) && System.IO.File.Exists(_mediaPlayPath))
                        {
                            lblRFID.Content = _gusetImgList[k].Name.Split('@')[0].ToString();
                            _photoId = _gusetImgList[k].Name.Split('@')[1].ToString().Split('.')[0].ToString();  //.Replace(".jpg", "").Trim();
                            break;
                        }
                    }

                    if (!string.IsNullOrEmpty(_mediaPlayPath) && System.IO.File.Exists(_mediaPlayPath))
                    {
                        ErrorHandler.ErrorHandler.LogFileWrite("Working set ");
                        ErrorHandler.ErrorHandler.LogFileWrite("previewTimeCounter = " + Convert.ToString(previewTimeCounter));
                        ErrorHandler.ErrorHandler.LogFileWrite("_guestMediaReloadTime = " + Convert.ToString(_guestMediaReloadTime));

                        PreViewWallMedia.Margin = new Thickness(Convert.ToDouble(txtPhotoMarginLeft.Text.Trim()), Convert.ToDouble(txtPhotoMarginTop.Text.Trim()),
                                                  Convert.ToDouble(txtPhotoMarginRight.Text.Trim()), Convert.ToDouble(txtPhotoMarginBottom.Text.Trim()));
                        lblRFID.Visibility = Visibility.Visible;
                        BoarderBlink(true);
                        imgWaterMarkControl.Visibility = Visibility.Collapsed;
                        SetFrame(false);
                        PlayFile(_mediaPlayPath);
                        System.Threading.Thread.Sleep(_guestMediaReloadTime);
                        imgWaterMarkControl.Visibility = Visibility.Visible;
                        SetFrame(true);
                        _mrkMediaPreviewTime = true;
                        lastFilePlayTime = DateTime.Now;
                        _isMktImagesActive = false;
                        previewTimeCounter = 0;
                        _gusetImgList[k].Attributes = FileAttributes.Offline;
                    }
                }
                //isPlayGuestImg = false;
                //for (k = _guestImgCount; k < _gusetImgList.Count; k++)
                //{
                //    isPlayGuestImg = true;
                //    _guestImgCount++;
                //    _mediaPlayPath = _gusetImgList[k].FullName;
                //    if (!string.IsNullOrEmpty(_mediaPlayPath) && System.IO.File.Exists(_mediaPlayPath))
                //    {
                //        lblRFID.Content = _gusetImgList[k].Name.Split('@')[0].ToString();
                //        _photoId = _gusetImgList[k].Name.Split('@')[1].ToString().Split('.')[0].ToString();  //.Replace(".jpg", "").Trim();
                //        break;
                //    }
                //}
                //if (!string.IsNullOrEmpty(_mediaPlayPath) && System.IO.File.Exists(_mediaPlayPath))
                //{
                //    PreViewWallMedia.Margin = new Thickness(Convert.ToDouble(txtPhotoMarginLeft.Text.Trim()), Convert.ToDouble(txtPhotoMarginTop.Text.Trim()),
                //                              Convert.ToDouble(txtPhotoMarginRight.Text.Trim()), Convert.ToDouble(txtPhotoMarginBottom.Text.Trim()));
                //    lblRFID.Visibility = Visibility.Visible;
                //    BoarderBlink(true);
                //    imgWaterMarkControl.Visibility = Visibility.Collapsed;
                //    SetFrame(false);
                //    PlayFile(_mediaPlayPath);
                //    System.Threading.Thread.Sleep(_guestMediaReloadTime);
                //    imgWaterMarkControl.Visibility = Visibility.Visible;
                //    SetFrame(true);
                //    _mrkMediaPreviewTime = true;
                //    lastFilePlayTime = DateTime.Now;
                //    _isMktImagesActive = false;
                //    previewTimeCounter = 0;
                //    _gusetImgList[k].Attributes = FileAttributes.Offline;
                //}
            }
            return _mediaPlayPath;
        }
        bool needToLoop = true;
        bool dummyTag = false;
        private bool IsGuestMediaAvailable()
        {
            if (_gusetImgList.Count == 0)
            {
                _isMktImagesActive = true;
                return false;
            }

            if (_gusetImgList.Count > _guestImgCount)
            {
                _isMktImagesActive = false;
                ErrorHandler.ErrorHandler.LogFileWrite("1  _isMktImagesActive = false");
                return true;
            }
            else if (_gusetImgList.Count == _guestImgCount && needToLoop)
            {
                if (previewTimeCounter == _previewWallInfoSetting.HighLightTimePW && previewTimeCounter != 0)
                {
                    ErrorHandler.ErrorHandler.LogFileWrite("2  previewTimeCounter = " + Convert.ToString(previewTimeCounter));
                    BoarderBlink(false);
                    return true;
                }
                else if (previewTimeCounter == _previewWallInfoSetting.PreviewTimePW - 1)
                {
                    ErrorHandler.ErrorHandler.LogFileWrite("3  needToLoop = false & previewTimeCounter = " + Convert.ToString(previewTimeCounter));
                    needToLoop = false;
                    return true;
                }
                else if (dummyTag)
                {
                    ErrorHandler.ErrorHandler.LogFileWrite("4 dummyTag");
                    _isMktImagesActive = true;
                    return false;
                }
                else if (previewTimeCounter == _previewWallInfoSetting.PreviewTimePW && _gusetImgList.Count == _guestImgCount && _previewWallInfoSetting.IsMktImgPW
                    && _mrkMediaList.Count > 0)
                {
                    ErrorHandler.ErrorHandler.LogFileWrite("5 previewTimeCounter");
                    _isMktImagesActive = true;
                    needToLoop = false;
                    return false;
                }
                else
                    return true;
            }
            else
            {
                _isMktImagesActive = true;
                return false;

            }
        }
        private bool CheckValidation()
        {
            bool _flag = true;
            if (cmbSubStore.SelectedValue.ToString() == "0")
            {
                System.Windows.MessageBox.Show("Please select Site");
                _flag = false;
            }
            else if (cmbLocation.SelectedValue.ToString() == "0")
            {
                System.Windows.MessageBox.Show("Please select Location");
                _flag = false;
            }
            else if (cmbScreen.SelectedValue.ToString() == "0" || cmbScreen.SelectedValue.ToString() == "--Select--")
            {
                System.Windows.MessageBox.Show("Please select Screen");
                _flag = false;
            }
            //else if (cmbRotation.SelectedValue.ToString() == "--Select--")
            //{
            //    System.Windows.MessageBox.Show("Please select Rotation angle");
            //    _flag = false;
            //}
            //else if (cmbRFIDFontWeight.SelectedValue.ToString() == "--Select--")
            //{
            //    System.Windows.MessageBox.Show("Please select PhotoId font weight");
            //    _flag = false;
            //}
            else if (string.IsNullOrWhiteSpace(txtRFIDFont.Text))
            {
                System.Windows.MessageBox.Show("Please enter PhotoId font size");
                _flag = false;
            }
            else if (Convert.ToInt32(txtRFIDFont.Text) <= 0)
            {
                System.Windows.MessageBox.Show("PhotoId font size should be greater than zero");
                _flag = false;
            }
            else if (string.IsNullOrWhiteSpace(txtPhotoMarginLeft.Text))
            {
                System.Windows.MessageBox.Show("Photo Margin Left can't be blank");
                _flag = false;
            }
            else if (string.IsNullOrWhiteSpace(txtPhotoMarginTop.Text))
            {
                System.Windows.MessageBox.Show("Photo Margin Top can't be blank");
                _flag = false;
            }
            else if (string.IsNullOrWhiteSpace(txtPhotoMarginRight.Text))
            {
                System.Windows.MessageBox.Show("Photo Margin Right can't be blank");
                _flag = false;
            }
            else if (string.IsNullOrWhiteSpace(txtPhotoMarginBottom.Text))
            {
                System.Windows.MessageBox.Show("Photo Margin Bottom can't be blank");
                _flag = false;
            }
            else if (string.IsNullOrWhiteSpace(txtMessageMarginLeft.Text))
            {
                System.Windows.MessageBox.Show("Message Position Margin Left can't be blank");
                _flag = false;
            }
            else if (string.IsNullOrWhiteSpace(txtMessageMarginTop.Text))
            {
                System.Windows.MessageBox.Show("Message Position Margin Top can't be blank");
                _flag = false;
            }
            else if (string.IsNullOrWhiteSpace(txtMessageMarginRight.Text))
            {
                System.Windows.MessageBox.Show("Message Position Margin Right can't be blank");
                _flag = false;
            }
            else if (string.IsNullOrWhiteSpace(txtMessageMarginBottom.Text))
            {
                System.Windows.MessageBox.Show("Message Position Margin Bottom can't be blank");
                _flag = false;
            }
            else if (string.IsNullOrWhiteSpace(txtFont.Text))
            {
                System.Windows.MessageBox.Show("Message Font Size can't be blank");
                _flag = false;
            }
            else if (string.IsNullOrWhiteSpace(txtMsgShowTime.Text))
            {
                System.Windows.MessageBox.Show("Message Display Time can't be blank");
                _flag = false;
            }

            return _flag;
        }
        private bool IsNumberKey(Key inkey)
        {
            if (inkey == Key.Tab)
                return true;

            if (inkey < Key.D0 || inkey > Key.D9)
            {
                if (inkey < Key.NumPad0 || inkey > Key.NumPad9)
                {
                    return false;
                }
            }
            return true;
        }
        private bool LoadInitialSettings()
        {
            bool retVal = true;
            try
            {
                if (File.Exists(_xmlFilePathMaster))
                {
                    XmlDataDocument xmldoc = new XmlDataDocument();
                    xmldoc.Load(_xmlFilePathMaster);
                    _subStoreID = xmldoc.GetElementsByTagName("SubStore")[0].ChildNodes.Item(0).InnerText.Trim();
                    cmbSubStore.SelectedValue = _subStoreID;
                    clsLocation.SiteId = Convert.ToInt32(_subStoreID);

                    _locationID = xmldoc.GetElementsByTagName("Location")[0].ChildNodes.Item(0).InnerText.Trim();
                    cmbLocation.SelectedValue = _locationID;
                    cmbLocation.IsEnabled = true;
                    clsLocation.LocationId = Convert.ToInt32(_locationID);

                    cmbLocation_SelectionChanged(null, null);

                    _screenID = xmldoc.GetElementsByTagName("Screen")[0].ChildNodes.Item(0).InnerText.Trim();
                    cmbScreen.SelectedValue = _screenID;
                    cmbScreen.IsEnabled = true;

                    _rotationAngle = xmldoc.GetElementsByTagName("RotationAngle")[0].ChildNodes.Item(0).InnerText.Trim();
                    cmbRotation.SelectedValue = _rotationAngle;

                    _RFIDFontWeight = xmldoc.GetElementsByTagName("RFIDFontWeight")[0].ChildNodes.Item(0).InnerText.Trim();
                    cmbRFIDFontWeight.SelectedValue = _RFIDFontWeight;

                    _HorizentalBorderWidth = xmldoc.GetElementsByTagName("HorizentalBorderWidth")[0].ChildNodes.Item(0).InnerText.Trim();
                    cmbHorizentalBorderWidth.SelectedValue = _HorizentalBorderWidth;

                    _VerticalBorderWidth = xmldoc.GetElementsByTagName("VerticalBorderWidth")[0].ChildNodes.Item(0).InnerText.Trim();
                    cmbVerticalBorderWidth.SelectedValue = _VerticalBorderWidth;


                    _RFIDBackColor = xmldoc.GetElementsByTagName("RFIDBackColor")[0].ChildNodes.Item(0).InnerText.Trim();
                    cmbRFIDBackColor.SelectedColor = ((System.Windows.Media.Color)System.Windows.Media.ColorConverter.ConvertFromString(_RFIDBackColor));

                    _RFIDForeColor = xmldoc.GetElementsByTagName("RFIDForeColor")[0].ChildNodes.Item(0).InnerText.Trim();
                    cmbRFIDForeColor.SelectedColor = ((System.Windows.Media.Color)System.Windows.Media.ColorConverter.ConvertFromString(_RFIDForeColor));

                    _RFIDfontSize = xmldoc.GetElementsByTagName("RFIDFontSize")[0].ChildNodes.Item(0).InnerText.Trim();
                    txtRFIDFont.Text = _RFIDfontSize;

                    _PhotoMarginLeft = xmldoc.GetElementsByTagName("PhotoMarginLeft")[0].ChildNodes.Item(0).InnerText.Trim();
                    txtPhotoMarginLeft.Text = _PhotoMarginLeft;

                    _PhotoMarginTop = xmldoc.GetElementsByTagName("PhotoMarginTop")[0].ChildNodes.Item(0).InnerText.Trim();
                    txtPhotoMarginTop.Text = _PhotoMarginTop;

                    _PhotoMarginRight = xmldoc.GetElementsByTagName("PhotoMarginRight")[0].ChildNodes.Item(0).InnerText.Trim();
                    txtPhotoMarginRight.Text = _PhotoMarginRight;

                    _PhotoMarginBottom = xmldoc.GetElementsByTagName("PhotoMarginBottom")[0].ChildNodes.Item(0).InnerText.Trim();
                    txtPhotoMarginBottom.Text = _PhotoMarginBottom;

                    _MessageMarginLeft = xmldoc.GetElementsByTagName("MessageMarginLeft")[0].ChildNodes.Item(0).InnerText.Trim();
                    txtMessageMarginLeft.Text = _MessageMarginLeft;

                    _MessageMarginTop = xmldoc.GetElementsByTagName("MessageMarginTop")[0].ChildNodes.Item(0).InnerText.Trim();
                    txtMessageMarginTop.Text = _MessageMarginTop;

                    _MessageMarginRight = xmldoc.GetElementsByTagName("MessageMarginRight")[0].ChildNodes.Item(0).InnerText.Trim();
                    txtMessageMarginRight.Text = _MessageMarginRight;

                    _MessageMarginBottom = xmldoc.GetElementsByTagName("MessageMarginBottom")[0].ChildNodes.Item(0).InnerText.Trim();
                    txtMessageMarginBottom.Text = _MessageMarginBottom;


                    _MSGFontSize = xmldoc.GetElementsByTagName("MSGFontSize")[0].ChildNodes.Item(0).InnerText.Trim();
                    txtFont.Text = _MSGFontSize;

                    _MsgForeColor = xmldoc.GetElementsByTagName("MSGForeColor")[0].ChildNodes.Item(0).InnerText.Trim();
                    cmbMessageForeColor.SelectedColor = ((System.Windows.Media.Color)System.Windows.Media.ColorConverter.ConvertFromString(_MsgForeColor));

                    _MSGPosition = xmldoc.GetElementsByTagName("MSGPosition")[0].ChildNodes.Item(0).InnerText.Trim();
                    cmbPosition.SelectedValue = _MSGPosition;

                    _MSGBGColor = xmldoc.GetElementsByTagName("MSGBGColor")[0].ChildNodes.Item(0).InnerText.Trim();
                    cmbMsgBGColor.SelectedColor = ((System.Windows.Media.Color)System.Windows.Media.ColorConverter.ConvertFromString(_MSGBGColor));

                    _MsgShowTime = xmldoc.GetElementsByTagName("MSGShowTime")[0].ChildNodes.Item(0).InnerText.Trim();
                    txtMsgShowTime.Text = _MsgShowTime;

                    _StretchImg = xmldoc.GetElementsByTagName("StretchImg")[0].ChildNodes.Item(0).InnerText.Trim();
                    if (_StretchImg.ToUpper() == "FALSE")
                    {
                        chkStretchImg.IsChecked = false;
                        PreViewWallMedia.Stretch = Stretch.Uniform;
                        imgWaterMarkControl.Stretch = Stretch.Uniform;
                        //imgHorizentalFrame.Stretch = Stretch.Uniform;
                        //imgVerticalFrame.Stretch = Stretch.Uniform;
                    }
                    else
                    {
                        chkStretchImg.IsChecked = true;
                        PreViewWallMedia.Stretch = Stretch.Fill;
                        imgWaterMarkControl.Stretch = Stretch.Fill;
                        //imgHorizentalFrame.Stretch = Stretch.Fill;
                        //imgVerticalFrame.Stretch = Stretch.Fill;

                    }
                    RIFDSetting();
                }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                retVal = false;
            }
            return retVal;
        }
        private void RIFDSetting()
        {
            lblRFID.FontSize = Convert.ToDouble(_RFIDfontSize);
            lblRFID.FontWeight = (FontWeight)new FontWeightConverter().ConvertFromString(_RFIDFontWeight);
            lblRFID.Background = new SolidColorBrush(((System.Windows.Media.Color)System.Windows.Media.ColorConverter.ConvertFromString(_RFIDBackColor)));
            lblRFID.Foreground = new SolidColorBrush(((System.Windows.Media.Color)System.Windows.Media.ColorConverter.ConvertFromString(_RFIDForeColor)));

            lblRFIDAssociate.FontSize = Convert.ToDouble(_MSGFontSize);
            lblRFIDAssociate.Foreground = new SolidColorBrush(((System.Windows.Media.Color)System.Windows.Media.ColorConverter.ConvertFromString(_MsgForeColor)));
            lblRFIDAssociate.Background = new SolidColorBrush(((System.Windows.Media.Color)System.Windows.Media.ColorConverter.ConvertFromString(_MSGBGColor)));


            var horizentalimage = new BitmapImage();
            horizentalimage.BeginInit();
            horizentalimage.UriSource = new Uri(@"/DigiPhoto.PreviewWall;component/Images/HorizentalBorder" + cmbHorizentalBorderWidth.SelectedValue + ".gif", UriKind.Relative);
            horizentalimage.EndInit();
            ImageBehavior.SetAnimatedSource(imgBlinkHorizental, horizentalimage);
            ImageBehavior.SetRepeatBehavior(imgBlinkHorizental, new RepeatBehavior(0));
            ImageBehavior.SetRepeatBehavior(imgBlinkHorizental, RepeatBehavior.Forever);

            var verticalimage = new BitmapImage();
            verticalimage.BeginInit();
            verticalimage.UriSource = new Uri(@"/DigiPhoto.PreviewWall;component/Images/VerticalBorder" + cmbVerticalBorderWidth.SelectedValue + ".gif", UriKind.Relative);
            verticalimage.EndInit();
            ImageBehavior.SetAnimatedSource(imgBlinkVertical, verticalimage);
            ImageBehavior.SetRepeatBehavior(imgBlinkVertical, new RepeatBehavior(0));
            ImageBehavior.SetRepeatBehavior(imgBlinkVertical, RepeatBehavior.Forever);

            if (cmbHorizentalBorderWidth.SelectedValue.ToString() == "X")
            {
                RFIDStackPanel.Margin = new Thickness(14);
                imgLogoControl.Margin = new Thickness(14);
            }
            else if (cmbHorizentalBorderWidth.SelectedValue.ToString() == "2X")
            {
                RFIDStackPanel.Margin = new Thickness(18);
                imgLogoControl.Margin = new Thickness(18);
            }
            else if (cmbHorizentalBorderWidth.SelectedValue.ToString() == "3X")
            {
                RFIDStackPanel.Margin = new Thickness(23);
                imgLogoControl.Margin = new Thickness(23);
            }
            else if (cmbHorizentalBorderWidth.SelectedValue.ToString() == "4X")
            {
                RFIDStackPanel.Margin = new Thickness(30);
                imgLogoControl.Margin = new Thickness(30);
            }
            RotateTransform rt = new RotateTransform();
            rt.Angle = Convert.ToDouble(_rotationAngle);
            rotateAngle = rt.Angle;
            grdMain.LayoutTransform = rt;
        }
        private void AssignLocationValue()
        {
            if (clsLocation.LocationId != 0)
            {
                cmbSubStore.SelectedValue = clsLocation.SiteId;
                cmbLocation.SelectedValue = clsLocation.LocationId;
                cmbLocation.IsEnabled = true;
            }
        }
        private void AssignValueToControl()
        {
            _rotationDisplayScreenlist = new List<string>();
            //_rotationDisplayScreenlist.Add("--Select--");
            _rotationDisplayScreenlist.Add("0");
            _rotationDisplayScreenlist.Add("90");
            _rotationDisplayScreenlist.Add("180");
            _rotationDisplayScreenlist.Add("270");
            cmbRotation.ItemsSource = _rotationDisplayScreenlist;
            cmbRotation.SelectedValue = "0";

            _dispalyScreenlist = new List<string>();
            _dispalyScreenlist.Add("--Select--");
            for (int i = 1; i <= 100; i++)
            {
                _dispalyScreenlist.Add("Display" + i);
            }
            cmbScreen.ItemsSource = _dispalyScreenlist;
            cmbScreen.SelectedValue = "--Select--";

            _RFIDFontWeightlist = new List<string>();
            //_RFIDFontWeightlist.Add("--Select--");
            _RFIDFontWeightlist.Add("Normal");
            _RFIDFontWeightlist.Add("Black");
            _RFIDFontWeightlist.Add("ExtraBlack");
            _RFIDFontWeightlist.Add("Thin");
            _RFIDFontWeightlist.Add("Bold");
            _RFIDFontWeightlist.Add("SemiBold");
            _RFIDFontWeightlist.Add("ExtraBold");
            _RFIDFontWeightlist.Add("Light");
            _RFIDFontWeightlist.Add("ExtraLight");
            _RFIDFontWeightlist.Add("Medium");
            cmbRFIDFontWeight.ItemsSource = _RFIDFontWeightlist;
            cmbRFIDFontWeight.SelectedValue = "Normal";

            _horizentalBlinkBorderlist = new List<string>();
            _horizentalBlinkBorderlist.Add("X");
            _horizentalBlinkBorderlist.Add("2X");
            _horizentalBlinkBorderlist.Add("3X");
            _horizentalBlinkBorderlist.Add("4X");
            cmbHorizentalBorderWidth.ItemsSource = _horizentalBlinkBorderlist;
            cmbHorizentalBorderWidth.SelectedValue = "X";

            _verticalBlinkBorderlist = new List<string>();
            _verticalBlinkBorderlist.Add("X");
            _verticalBlinkBorderlist.Add("2X");
            _verticalBlinkBorderlist.Add("3X");
            _verticalBlinkBorderlist.Add("4X");
            cmbVerticalBorderWidth.ItemsSource = _verticalBlinkBorderlist;
            cmbVerticalBorderWidth.SelectedValue = "X";

            _PositionList = new List<string>();
            _PositionList.Add("Top-Left");
            _PositionList.Add("Top-Center");
            _PositionList.Add("Top-Right");
            _PositionList.Add("Bottom-Left");
            _PositionList.Add("Bottom-Center");
            _PositionList.Add("Bottom-Right");
            _PositionList.Add("Center");
            cmbPosition.ItemsSource = _PositionList;
            cmbPosition.SelectedValue = "Top-Left";

            try
            {
                List<SubStoresInfo> subStores = new List<SubStoresInfo>();
                if (clsLocation.LocationId == 0)
                {
                    subStores = (new StoreSubStoreDataBusniess()).GetSubstoreData().ToList();
                    CommonUtility.BindComboWithSelect<SubStoresInfo>(cmbSubStore, subStores, "DG_SubStore_Name", "DG_SubStore_pkey", 0, DigiPhoto.Utility.Repository.Data.ClientConstant.SelectString);
                    cmbSubStore.SelectedValue = "0";

                    List<LocationInfo> locations = new List<LocationInfo>();
                    locations = (new LocationBusniess()).GetLocationList(1).ToList();
                    CommonUtility.BindComboWithSelect<LocationInfo>(cmbLocation, locations, "DG_Location_Name", "DG_Location_pkey", 0, DigiPhoto.Utility.Repository.Data.ClientConstant.SelectString);
                    cmbLocation.SelectedValue = "0";
                }
                else
                {
                    subStores = (new StoreSubStoreDataBusniess()).GetSubstoreData().ToList();
                    CommonUtility.BindComboWithSelect<SubStoresInfo>(cmbSubStore, subStores, "DG_SubStore_Name", "DG_SubStore_pkey", 0, DigiPhoto.Utility.Repository.Data.ClientConstant.SelectString);
                    cmbSubStore.SelectedValue = clsLocation.SiteId;
                    cmbSubStore_SelectionChanged(null, null);
                    cmbLocation.SelectedValue = clsLocation.LocationId;
                    cmbLocation.IsEnabled = true;
                    cmbLocation_SelectionChanged(null, null);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }

        }
        private void ReloadGuestImgTimer()
        {
            _tmrGetMediaFile.Interval = _reloadGuestImgTimeInterval * 1000;
            _tmrGetMediaFile.Tick += new EventHandler(tmr_Tick);
            _tmrGetMediaFile.Start();
        }

        #endregion

        private void NumericOnly(System.Object sender, System.Windows.Input.TextCompositionEventArgs e)
        {
            e.Handled = IsTextNumeric(e.Text);

        }
        private static bool IsTextNumeric(string str)
        {
            System.Text.RegularExpressions.Regex reg = new System.Text.RegularExpressions.Regex("[^0-9]");
            return reg.IsMatch(str);
        }

    }
    #region Class
    public static class externClass
    {
        public static IEnumerable<TSource> DistinctBy<TSource, TKey>(
           this IEnumerable<TSource> source,
           Func<TSource, TKey> keySelector)
        {
            var knownKeys = new HashSet<TKey>();
            return source.Where(element => knownKeys.Add(keySelector(element)));
        }
    }
    public static class clsLocation
    {
        public static Int32 SiteId = 0;
        public static Int32 LocationId = 0;
    }
    #endregion Class
}
