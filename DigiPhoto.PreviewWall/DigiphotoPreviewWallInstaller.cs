﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Configuration.Install;
using System.IO;
using System.Linq;
using System.Reflection;

namespace DigiPhoto.PreviewWall
{
    [RunInstaller(true)]
    public partial class DigiphotoPreviewWallInstaller : System.Configuration.Install.Installer
    {
        public DigiphotoPreviewWallInstaller()
        {
            InitializeComponent();

        }
        public override void Uninstall(IDictionary savedState)
        {
            //System.Diagnostics.Debugger.Launch();
            //System.Diagnostics.Debugger.Break();
            base.Uninstall(savedState);
            try
            {
                string settingFilePath = System.IO.Path.Combine(System.IO.Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "PreviewSetting.xml");
                string settingFilePathGumBall = System.IO.Path.Combine(System.IO.Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "PreviewSettingGumBall.xml");
                string settingFilePathLocation = System.IO.Path.Combine(System.IO.Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "PreviewSettingLocation.xml");
                if (File.Exists(settingFilePath))
                    File.Delete(settingFilePath);
                if (File.Exists(settingFilePathGumBall))
                    File.Delete(settingFilePathGumBall);
                if (File.Exists(settingFilePathLocation))
                    File.Delete(settingFilePathLocation);
                string marketingMediaPath = System.IO.Path.Combine(System.IO.Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "MarketingMedia");
                if (Directory.Exists(marketingMediaPath))
                    Directory.Delete(marketingMediaPath, true);
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
            }

        }

        public override void Install(System.Collections.IDictionary stateSaver)
        {

            base.Install(stateSaver);
            //System.Diagnostics.Debugger.Launch();
            //System.Diagnosticss.Debugger.Break();

            string targetDirectory = Context.Parameters["targetdir"];

            string servername = Context.Parameters["Servername"];

            string username = Context.Parameters["Username"];

            string password = Context.Parameters["Password"];
            //string hotfolder = Context.Parameters["HotFolder"];

            string database = "Digiphoto";

            if (string.IsNullOrEmpty(targetDirectory) || string.IsNullOrEmpty(servername) || string.IsNullOrEmpty(username) || string.IsNullOrEmpty(password))
            {
                return;
            }
            string exePath = string.Format("{0}DigiPhoto.PreviewWall.exe", targetDirectory);
            Configuration config = ConfigurationManager.OpenExeConfiguration(exePath);
            String conn = "Data Source=" + servername + ";Initial Catalog=" + database + ";User ID=" + username + ";Password=" + password + ";MultipleActiveResultSets=True";
            string connectionsection = config.ConnectionStrings.ConnectionStrings
    ["DigiConnectionString"].ConnectionString;


            ConnectionStringSettings connectionstring = null;
            if (connectionsection != null)
            {
                config.ConnectionStrings.ConnectionStrings.Remove("DigiConnectionString");
            }

            connectionstring = new ConnectionStringSettings("DigiConnectionString", conn);
            config.ConnectionStrings.ConnectionStrings.Add(connectionstring);
            ConfigurationSection syncsection = config.GetSection("connectionStrings");
            //Ensures that the section is not already protected
            if (!syncsection.SectionInformation.IsProtected)
            {
                //Uses the Windows Data Protection API (DPAPI) to encrypt the configuration section
                //using a machine-specific secret key
                syncsection.SectionInformation.ProtectSection("DataProtectionConfigurationProvider");
            }
            //config.Save(ConfigurationSaveMode.Modified, true);
            config.Save();
            ConfigurationManager.RefreshSection("connectionStrings");

        }
    }
}
