﻿using System;
using System.IO;
using System.Reflection;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Effects;
using System.Windows.Media.Media3D;

namespace ImageProcessingEngine.Shader
{
    public class MonochromeEffect : ShaderEffect
    {
        public static readonly DependencyProperty InputProperty = ShaderEffect.RegisterPixelShaderSamplerProperty("Input", typeof(MonochromeEffect), 0);
        public static readonly DependencyProperty FilterColorProperty = DependencyProperty.Register("FilterColor", typeof(Color), typeof(MonochromeEffect), new UIPropertyMetadata(Color.FromArgb(255, 255, 255, 0), PixelShaderConstantCallback(0)));
        private static string executableLocation = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);

        private static string xslLocation = Path.Combine(executableLocation, "Shader\\Monochrome.ps");

        private readonly static PixelShader shader =
            new PixelShader()
            {
                UriSource = new Uri(xslLocation)
            };
        public MonochromeEffect()
        {
            PixelShader pixelShader = new PixelShader();
           // pixelShader.UriSource = new Uri(@"/Shader/Monochrome.ps", UriKind.Relative);          
           // this.PixelShader = pixelShader;
            this.PixelShader = shader;


            this.UpdateShaderValue(InputProperty);
            this.UpdateShaderValue(FilterColorProperty);
        }
        public Brush Input
        {
            get
            {
                return ((Brush)(this.GetValue(InputProperty)));
            }
            set
            {
                this.SetValue(InputProperty, value);
            }
        }
        /// <summary>The color used to tint the input.</summary>
        public Color FilterColor
        {
            get
            {
                return ((Color)(this.GetValue(FilterColorProperty)));
            }
            set
            {
                this.SetValue(FilterColorProperty, value);
            }
        }
    }
}
