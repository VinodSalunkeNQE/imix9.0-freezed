﻿using System;
using System.IO;
using System.Reflection;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Effects;
using System.Windows.Media.Media3D;

namespace ImageProcessingEngine.Shader
{
   
        public class Cartoonize : ShaderEffect
        {
            public static readonly DependencyProperty InputProperty = ShaderEffect.RegisterPixelShaderSamplerProperty("Input", typeof(Cartoonize), 0);
            public static readonly DependencyProperty WidthProperty = DependencyProperty.Register("Width", typeof(double), typeof(Cartoonize), new UIPropertyMetadata(((double)(500D)), PixelShaderConstantCallback(0)));
            public static readonly DependencyProperty HeightProperty = DependencyProperty.Register("Height", typeof(double), typeof(Cartoonize), new UIPropertyMetadata(((double)(300D)), PixelShaderConstantCallback(1)));
            private static string executableLocation = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);

            private static string xslLocation = Path.Combine(executableLocation, "Shader\\Cartoonize.ps");

            private readonly static PixelShader shader =
                new PixelShader()
                {
                    UriSource = new Uri(xslLocation)
                };
            public Cartoonize()
            {
                PixelShader pixelShader = new PixelShader();
                //pixelShader.UriSource = new Uri(@"/Shader/Cartoonize.ps", UriKind.Relative);
                this.PixelShader = shader; //pixelShader;

                this.UpdateShaderValue(InputProperty);
                this.UpdateShaderValue(WidthProperty);
                this.UpdateShaderValue(HeightProperty);
            }
            public Brush Input
            {
                get
                {
                    return ((Brush)(this.GetValue(InputProperty)));
                }
                set
                {
                    this.SetValue(InputProperty, value);
                }
            }
            /// <summary>The width of the frost.</summary>
            public double Width
            {
                get
                {
                    return ((double)(this.GetValue(WidthProperty)));
                }
                set
                {
                    this.SetValue(WidthProperty, value);
                }
            }
            /// <summary>The height of the frost.</summary>
            public double Height
            {
                get
                {
                    return ((double)(this.GetValue(HeightProperty)));
                }
                set
                {
                    this.SetValue(HeightProperty, value);
                }
            }
        }
}
