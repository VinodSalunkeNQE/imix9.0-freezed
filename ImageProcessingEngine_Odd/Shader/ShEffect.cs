﻿using System;
using System.IO;
using System.Reflection;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Effects;
using System.Windows.Media.Media3D;


namespace ImageProcessingEngine.Shader
{
    class ShEffect : ShaderEffect
    {
        public static readonly DependencyProperty InputProperty = ShaderEffect.RegisterPixelShaderSamplerProperty("Input", typeof(ShEffect), 0);
        public static readonly DependencyProperty StrengthProperty = DependencyProperty.Register("Strength", typeof(double), typeof(ShEffect), new PropertyMetadata(((double)(0D)), PixelShaderConstantCallback(0)));
        public static readonly DependencyProperty PixelWidthProperty = DependencyProperty.Register("PixelWidth", typeof(double), typeof(ShEffect), new PropertyMetadata(((double)(0D)), PixelShaderConstantCallback(1)));
        public static readonly DependencyProperty PixelHeightProperty = DependencyProperty.Register("PixelHeight", typeof(double), typeof(ShEffect), new PropertyMetadata(((double)(0D)), PixelShaderConstantCallback(2)));
        private static string executableLocation = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);

        private static string xslLocation = Path.Combine(executableLocation, "Shader\\sh.ps");

        private readonly static PixelShader shader =
            new PixelShader()
            {
                UriSource = new Uri(xslLocation)
            };
        public ShEffect()
        {
            PixelShader pixelShader = new PixelShader();
            //pixelShader.UriSource = new Uri(@"/Shader/sh.ps", UriKind.Relative);
            this.PixelShader = shader;// pixelShader;

            this.UpdateShaderValue(InputProperty);
            this.UpdateShaderValue(StrengthProperty);
            this.UpdateShaderValue(PixelWidthProperty);
            this.UpdateShaderValue(PixelHeightProperty);
        }
        public Brush Input
        {
            get
            {
                return ((Brush)(this.GetValue(InputProperty)));
            }
            set
            {
                this.SetValue(InputProperty, value);
            }
        }
        public double Strength
        {
            get
            {
                return ((double)(this.GetValue(StrengthProperty)));
            }
            set
            {
                this.SetValue(StrengthProperty, value);
            }
        }
        public double PixelWidth
        {
            get
            {
                return ((double)(this.GetValue(PixelWidthProperty)));
            }
            set
            {
                this.SetValue(PixelWidthProperty, value);
            }
        }
        public double PixelHeight
        {
            get
            {
                return ((double)(this.GetValue(PixelHeightProperty)));
            }
            set
            {
                this.SetValue(PixelHeightProperty, value);
            }
        }
    }
}
