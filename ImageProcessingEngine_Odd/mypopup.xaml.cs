﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Diagnostics;
using System.Threading;
using DigiPhoto.IMIX.Business;
using DigiPhoto.IMIX.Model;

namespace ImageProcessingEngine
{
    /// <summary>
    /// Interaction logic for mypopup.xaml
    /// </summary>
    public partial class mypopup : UserControl
    {
        public mypopup()
        {
            ServicePosInfoBusiness svcPosinfoBusiness = new ServicePosInfoBusiness();
            string ret = svcPosinfoBusiness.ServiceStart(true);

            StoreInfo store = new StoreInfo();
            TaxBusiness taxBusiness = new TaxBusiness();
			///////modified by latika for locationwise setting 25/feb/19
            store = taxBusiness.getTaxConfigDataBySystemID(Environment.MachineName);

            bool RunImageProcessingLocWise=false;

            RunImageProcessingLocWise = store.RunImageProcessingEngineLocationWise;

            //ServicePosInfoBusiness svcPosinfoBusiness = new ServicePosInfoBusiness();
            // int ret = svcPosinfoBusiness.ServiceStart(false);
            ////commented by latika as per requirement 
            //if (RunImageProcessingLocWise == false)
            //{
            //    MessageBox.Show("Image Processing Engine is not configured in Locationwise setting with this System-" + Environment.MachineName + ". \n Currently running sitewise for faster process Configure Locationwise setting from DigConfig-VOS .");
              
            //}
            ////end
            if (!string.IsNullOrEmpty(ret) && RunImageProcessingLocWise == false)
            {
                MessageBox.Show("This application is already running on " + ret);
                Environment.Exit(0);
                // return;
            }

            InitializeComponent();
            Thread newWindowThread = new Thread(new ThreadStart(() =>
            {
                ImageProcessor objImageProcessor = new ImageProcessor();
                objImageProcessor.Show();
                System.Windows.Threading.Dispatcher.Run();
            }));

            newWindowThread.SetApartmentState(ApartmentState.STA);
            newWindowThread.IsBackground = true;
            newWindowThread.Start();
        }
        private void OnButtonClick(object sender, RoutedEventArgs e)
        {
            ServicePosInfoBusiness svcPosinfoBusiness = new ServicePosInfoBusiness();
            svcPosinfoBusiness.StopService(true);
            Process thisProc = Process.GetCurrentProcess();
            // Check how many total processes have the same name as the current one
            if (Process.GetProcessesByName(thisProc.ProcessName).Length >= 1)
            {
                Application.Current.Shutdown();
                thisProc.Kill();
            }
        }
    }
}
