﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using MControls;
using MPLATFORMLib;
using FrameworkHelper;
using DigiPhoto.Common;
using System.IO;
using System.Runtime.InteropServices;
using DigiPhoto.IMIX.Model;
using DigiPhoto.IMIX.Business;
using System.Windows.Threading;
using LiveCapture.Interop;
using FrameworkHelper.Common;
using DigiPhoto;
using System.Xml;
using System.Collections;
using System.Threading;
using System.ComponentModel;
using System.Diagnostics;
using DigiPhoto.DataLayer;
using System.Xml.Linq;
using System.Text.RegularExpressions;

namespace LiveVideoEditing
{
    /// <summary>
    /// Interaction logic for MLLiveCapture.xaml
    /// </summary>
    public partial class MLLiveCapture
    {
        #region Declaration
        public UIElement _parent;
        private bool _result = false;
        MComposerClass m_objComposer;
        /// /Writer
        public MLiveClass m_objLive;
        public MWriterClass m_objWriter;
        public MWriterClass m_objWriterLive;
        IMConfig m_pConfigRoot;
        BackgroundWorker bwSaveVideos = new BackgroundWorker();
        private IMPersist m_pMPersist;
        // Called if user change playlist selection
        public event EventHandler OnLoad;
        IMDevice m_pDevice;
        MLive ml; IMStreams m_pMixerStreams;
        MPreviewControl mPreviewControl = new MPreviewControl();
        MLiveControl mLiveControl = new MLiveControl();
        string outputFormat = "mp4";
        System.ComponentModel.BackgroundWorker bwRoute = new System.ComponentModel.BackgroundWorker();
        BusyWindow bs = new BusyWindow();
        string processVideoTemp = AppDomain.CurrentDomain.BaseDirectory + "\\DigiProcessVideoTemp\\";// Environment.CurrentDirectory + "\\DigiProcessVideoTemp\\";
        List<VideoScene> lstVideoScene;
        DispatcherTimer VidTimer;
        VideoSceneBusiness business;
        bool CancelbwRoute = false;
        decimal VideoLength = 0;
        int frameCount = 0;
        string strCapturePath_or_URL = string.Empty;
        MPreview previewSecondary = new MPreviewClass();
        int ScreenCount = 1;
        bool _isVideoSaved = true;
        int FlipMode = 0;
        List<VideoSceneObject> lstVideoSceneObject = new List<VideoSceneObject>();
        #endregion
        #region Taskbar enable/Disable
        private delegate int LowLevelKeyboardProcDelegate(int nCode, int wParam, ref KBDLLHOOKSTRUCT lParam);
        private struct KBDLLHOOKSTRUCT
        {
            public int vkCode;
            int scanCode;
            public int flags;
            int time;
            int dwExtraInfo;
        }
        [DllImport("kernel32.dll")]
        private static extern IntPtr GetModuleHandle(IntPtr path);
        [DllImport("user32.dll", EntryPoint = "SetWindowsHookExA", CharSet = CharSet.Ansi)]
        private static extern IntPtr SetWindowsHookEx(
            int idHook,
            LowLevelKeyboardProcDelegate lpfn,
            IntPtr hMod,
            int dwThreadId);
        private IntPtr intLLKey;
        const int WH_KEYBOARD_LL = 13;
        [DllImport("user32.dll", EntryPoint = "CallNextHookEx", CharSet = CharSet.Ansi)]
        private static extern int CallNextHookEx(
            int hHook, int nCode,
            int wParam, ref KBDLLHOOKSTRUCT lParam);
        [DllImport("user32.dll")]
        private static extern IntPtr UnhookWindowsHookEx(IntPtr hHook);
        #endregion
        public MLLiveCapture()
        {
            try
            {
                InitializeComponent();
                ChromaKeypluginLic.IntializeProtection();
                DecoderlibLic.IntializeProtection();
                EncoderlibLic.IntializeProtection();
                MComposerlibLic.IntializeProtection();
                MPlatformSDKLic.IntializeProtection();
                VidTimer = new DispatcherTimer();
                this.Loaded += new RoutedEventHandler(MainWindow_Loaded);
                VidTimer.Tick += new EventHandler(VidTimer_Tick);
                bwSaveVideos.DoWork += bwSaveVideos_DoWork;
                bwSaveVideos.RunWorkerCompleted += bwSaveVideos_RunWorkerCompleted;
                if (!Directory.Exists(processVideoTemp))
                {
                    Directory.CreateDirectory(processVideoTemp);
                }
                try
                {
                    m_objComposer = new MComposerClass();
                    m_objLive = new MLiveClass();
                    m_objWriter = new MWriterClass();
                    m_objWriterLive = new MWriterClass();
                }
                catch (Exception exception)
                {
                    return;
                }
                mMixerList1.SetControlledObject(m_objComposer);
                mElementsTree1.SetControlledObject(m_objComposer);
                mPreviewComposerControl.SetControlledObject(m_objComposer);

                mFormatControl1.SetControlledObject(m_objComposer);
                mPersistControl1.SetControlledObject(m_objComposer);
                //mScenesCombo1.SetControlledObject(m_objComposer);
                SetWriterControlledObject(m_objWriter);
                SetWriterControlledObject(m_objWriterLive);
                mFormatControl1.SetControlledObject(m_objLive);
                mConfigList1.SetControlledObject(m_objWriter);
                mLiveControl.SetControlledObject(m_objLive);
                mPreviewControl.SetControlledObject(m_objLive);
                mConfigList1.OnConfigChanged += new EventHandler(mConfigList1_OnConfigChanged);
                //mScenesCombo1.OnActiveSceneChange += new EventHandler(mScenesCombo1_OnActiveSceneChange);
                mMixerList1.OnMixerSelChanged += new EventHandler(mMixerList1_OnMixerSelChanged);
                mPersistControl1.OnLoad += new EventHandler(mPersistControl1_OnLoad);

                //// Fill Senders
                FillSenders((IMSenders)m_objWriter);
                // Update config and enable/disable URL field
                mConfigList1_OnConfigChanged(null, EventArgs.Empty);
                mFormatControl1.comboBoxVideo.SelectedIndex = 0;
                mFormatControl1.comboBoxAudio.SelectedIndex = 0;

                mAttributesList1.ElementDescriptors = MHelpers.MComposerElementDescriptors;

                mElementsTree1.ElementDescriptors = MHelpers.MComposerElementDescriptors;
                //m_objComposer.ObjectStart(null); 
                if (m_objComposer != null)
                    m_objComposer.ObjectClose();

                MPersistSetControlledObject(m_objComposer);

                //bwRoute.DoWork += new System.ComponentModel.DoWorkEventHandler(bwRoute_DoWork);
                //bwRoute.WorkerSupportsCancellation = true;
                if (mPreviewComposerControl.m_pPreview != null)
                    mPreviewComposerControl.m_pPreview.PreviewEnable("", 0, 1);

                ScreenCount = System.Windows.Forms.Screen.AllScreens.Count();
                if (ScreenCount > 1)
                    CreateSecondaryPreview();

                ShowHideSettings(ShowHideSet);
                ShowHideSettings(ShowHideSet);
                SetFormatControlSize();
                FillPositionDropDown();
                ClearAllTempVideos();
                getSubstoreID();


            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }
        public void GetConfigLocationData()
        {
            try
            {
                ConfigBusiness configBusiness = new ConfigBusiness();
                List<long> filterValues = new List<long>();
                //filterValues.Add((long)ConfigParams.IsAdvancedVideoEditActive);
                filterValues.Add((long)ConfigParams.VideoFlipMode);

                List<iMixConfigurationLocationInfo> ConfigValuesList = configBusiness.GetConfigLocation(locationId, ConfigManager.SubStoreId).Where(o => filterValues.Contains(o.IMIXConfigurationMasterId)).ToList();
                if (ConfigValuesList != null || ConfigValuesList.Count > 0)
                {
                    for (int i = 0; i < ConfigValuesList.Count; i++)
                    {
                        switch (ConfigValuesList[i].IMIXConfigurationMasterId)
                        {
                            case (int)ConfigParams.VideoFlipMode:
                                if (!string.IsNullOrEmpty(ConfigValuesList[i].ConfigurationValue))
                                    FlipMode = Convert.ToInt32(ConfigValuesList[i].ConfigurationValue);
                                else FlipMode = 0;
                                break;
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }
        private void CreateSecondaryPreview()
        {
            mPreviewComposerControl.m_pPreview.PreviewWindowSet("", mPreviewComposerControl.panelPreview.Handle.ToInt32());
            previewSecondary.PreviewWindowSet("", mPreviewComposerControl1.panelPreview.Handle.ToInt32());
            previewSecondary.PreviewEnable("", 1, 1);
            m_objComposer.ObjectStart(null);
            ((IMObject)previewSecondary).ObjectStart(m_objComposer);
            m_objComposer.FilePlayStart();
        }
        private void bwSaveVideos_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            bs.Hide();
        }

        private void bwSaveVideos_DoWork(object sender, DoWorkEventArgs e)
        {
            if (System.IO.File.Exists(strCapturePath_or_URL))
            {
                SaveFiles();
                
            }
            else
                MessageBox.Show("Video not found!\n", "Live Video Editor", MessageBoxButton.OK, MessageBoxImage.Information, MessageBoxResult.OK);
        }

        public bool UpdateOtherSettings(VideoScene vScene)
        {
            VideoSceneBusiness business = new VideoSceneBusiness();

            if (!business.SaveVideoSceneSettings(vScene))
            {
                return false;
            }
            return true;
        }
        public bool SaveUpdLiveVideoQuickSettings(QuickSettings QSetting)
        {
            VideoSceneBusiness business = new VideoSceneBusiness();

            if (!business.SaveUpdLiveVideoQuickSettings(QSetting))
            {
                return false;
            }
            return true;
        }
        /// <summary>
        /// latika added by 
        /// </summary>
        public void GETQuickSettings()
        {
            try
            {
                VideoSceneBusiness business = new VideoSceneBusiness();
                QuickSettings objQuickSettings = new QuickSettings();
                List<QuickSettings> objQuickSettings2 = new List<QuickSettings>();
                objQuickSettings.LocationID = locationId;
                objQuickSettings.SubStorID = subStoreId;
                objQuickSettings.ScenID = Convert.ToInt32(cmbScene.SelectedValue);
                objQuickSettings2 = business.GetLiveVideoQuickSettings(objQuickSettings);

                btnFullScreen.IsChecked = objQuickSettings2[0].FullScreen;
                sldVolume.Value = objQuickSettings2[0].Volume;
                btnAspectRatio.IsChecked = objQuickSettings2[0].AspectRatio;
                btnAudio.IsChecked = objQuickSettings2[0].Audio;
                btnVideoPreview.IsChecked= objQuickSettings2[0].Video;

            }
            catch { }
        }
        /// <summary>
        /// added by latika 
        /// </summary>
        private void SetFormatControlSize()
        {
            mConfigList1.Width = 250;
            mConfigList1.Columns[0].Width = 95;
            mConfigList1.Columns[1].Width = 150;
            mConfigList1.Height = 100;
        }
        void m_objComposer_OnFrame(string bsChannelID, object pMFrame)
        {
            if (pMFrame != null)
            {
                frameCount++;
                foreach (var item in htVideoObjects.Keys)
                {
                    MElement elemRoute = GetMElement(item.ToString());
                    ApplyVideoObjectRoute(elemRoute, (Hashtable)htVideoObjects[item], frameCount);
                    Marshal.ReleaseComObject(elemRoute);
                }
                if (frameCount == 5)
                {
                    StartWriter();
                    StartWriterLive();
                }
            }
            Marshal.ReleaseComObject(pMFrame);
        }
        void MainWindow_Loaded(object sender, RoutedEventArgs e)
        {
            if (ScreenCount > 1)
                previewSecondary.PreviewFullScreen("", 1, 1);

            loadSceneCombo();

            if (cmbScene.SelectedValue != null)
                loadScene((int)cmbScene.SelectedValue);
            else
            {
                EnableDisableControlsOnLoad(false);
                MessageBox.Show("No video scene found!\n", "Live Video Editor", MessageBoxButton.OK, MessageBoxImage.Information, MessageBoxResult.OK);

            }

        }
        private void EnableDisableControlsOnLoad(bool value)
        {
            btnStartProcess.IsEnabled = value;
            btnPreviewVideo.IsEnabled = value;
            btnPauseProcess.IsEnabled = value;
            btnSaveFile.IsEnabled = value;
            btnStopProcess.IsEnabled = value;
            if (value == false)
                stkQuickSettings.Visibility = Visibility.Collapsed;
            else
                stkQuickSettings.Visibility = Visibility.Visible;
        }
        void mPersistControl1_OnLoad(object sender, EventArgs e)
        {
            mMixerList1.UpdateList(true, 1);
            mElementsTree1.UpdateTree(false);
        }
        public Object MPersistSetControlledObject(Object pObject)
        {
            Object pOld = (Object)m_pMPersist;
            try
            {
                m_pMPersist = (IMPersist)pObject;
            }
            catch (System.Exception) { }

            return pOld;
        }
        public bool ShowPanHandlerDialog()
        {
            Visibility = Visibility.Visible;
            _parent.IsEnabled = false;

            return _result;
        }
        private void HideHandlerDialog()
        {
            try
            {
                if (m_objComposer != null) m_objComposer.ObjectClose();
                if (m_objLive != null) m_objLive.ObjectClose();
                if (m_objWriter != null) m_objWriter.ObjectClose();//
                if (m_objWriterLive != null) m_objWriterLive.ObjectClose();
                //Marshal.FinalReleaseComObject(m_objLive);
                //Marshal.FinalReleaseComObject(m_objComposer);
                //Marshal.FinalReleaseComObject(m_objWriter);
                //Marshal.FinalReleaseComObject(m_objWriterLive);
            }
            catch (Exception ex)
            {

            }
            finally
            {
                Application.Current.Shutdown();
            }
        }
        public void SetParent(UIElement parent)
        {
            _parent = (UIElement)parent;
        }
        bool IsExitButtonClicked = false;
        private void btnClose_Click(object sender, RoutedEventArgs e)
        {
#if !DEBUG
            EnableTaskbar();
#endif
            if (!_isVideoSaved)
            {
                MessageBoxResult result = MessageBox.Show("Video not saved, would you still like to exit?", "Live Video Editor", MessageBoxButton.YesNo, MessageBoxImage.Information);
                if (result == MessageBoxResult.Yes)
                {
                    IsExitButtonClicked = true;
                    HideHandlerDialog();
                }
            }
            else
            {
                HideHandlerDialog();
            }
        }
        private void EnableTaskbar()
        {
            ReleaseKeyboardHook();
            EnableDisableTaskManager(true);
            //AuditLog.AddUserLog(Common.LoginUser.UserId, (int)FrameworkHelper.ActionType.MinimizeToWindows, "Minimized to Windows on :- ");
            Taskbar.Show();
        }
        public Object SetWriterControlledObject(Object pObject)
        {
            Object pOld = (Object)m_pConfigRoot;
            try
            {
                m_pConfigRoot = (IMConfig)pObject;
            }
            catch (System.Exception) { }
            return pOld;
        }
        void mScenesCombo1_OnActiveSceneChange(object sender, EventArgs e)
        {
            mElementsTree1.UpdateTree(false);
        }
        MElement SelectedTreeElement;
        string strDefaultSceneSettings = string.Empty;
        private void mElementsTree1_AfterSelect(object sender, System.Windows.Forms.TreeViewEventArgs e)
        {
            try
            {
                MElement pElement = (MElement)e.Node.Tag;
                SelectedTreeElement = pElement;
                mAttributesList1.SetControlledObject(pElement);
                string strType, strXML;
                pElement.ElementGet(out strType, out strXML);
                if (string.IsNullOrEmpty(strDefaultSceneSettings))
                    strDefaultSceneSettings = strXML;
                GetElementInformation(strXML);
                bool bDefElement = false;
                foreach (string strDefElement in MHelpers.strDefaultElements)
                {
                    if (strType.Contains(strDefElement))
                    {
                        mPreviewComposerControl.SetEditElement(pElement);
                        bDefElement = true;
                        break;
                    }
                }
                if (!bDefElement)
                {
                    mPreviewComposerControl.SetEditElement(null);
                }

                string selSceneName = Convert.ToString(((System.Collections.Generic.KeyValuePair<int, string>)cmbScene.SelectedItem).Value);
                #region Added by Ajay on 25 April 2019 to load the settings.


                XmlDocument xml = new XmlDocument();
                string filename = System.IO.Path.Combine(ConfigManager.DigiFolderPath, "Profiles", selSceneName, "interactivesettings.xml");
                xml.Load(filename);

                XmlNodeList xNodeList = xml.SelectNodes("/Settings/ " + e.Node.Parent.Text + "/" + (Regex.Replace(e.Node.Text, @"[^0-9a-zA-Z]+", "")));
                foreach (XmlNode xNode in xNodeList)
                {
                    txtXAxis.Text = Convert.ToString(xNode["XAxis"].InnerText);
                    txtYAxis.Text = xNode["YAxis"].InnerText;
                    txtAudioGain.Text = xNode["AudioGain"].InnerText;
                    txtCropH.Text = xNode["CropH"].InnerText;
                    txtCropW.Text = xNode["CropW"].InnerText;
                    txtCropY.Text = xNode["CropY"].InnerText;

                    #region updated by latika
                    txtCropX.Text = xNode["CropX"].InnerText;

                    #endregion
                    txtHeight.Text = xNode["Height"].InnerText;
                    txtWidth.Text = xNode["Width"].InnerText;
                    txtRotation.Text = xNode["Rotation"].InnerText;
                    txtStatus.Text = xNode["Status"].InnerText;
                    txtStreamId.Text = xNode["StreamId"].InnerText;
                    drpCropP.SelectedItem = xNode["CropP"].InnerText;
                    drpPosition.SelectedItem = xNode["Position"].InnerText;
                } 
                #endregion

            }
            catch (System.Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }
        void mConfigList1_OnConfigChanged(object sender, EventArgs e)
        {
            // Update config string 
            string strConfig;
            m_objWriter.ConfigGetAll(1, out strConfig);
            comboBoxProps.Text = strConfig;
            // Check if format support network streaming
            string strFormat;
            IMAttributes pFormatConfig;
            m_objWriter.ConfigGet("format", out strFormat, out pFormatConfig);
            int bNetwork = 0;
            try
            {
                pFormatConfig.AttributesBoolGet("network", out bNetwork);
            }
            catch (System.Exception) { }
        }
        private void btnStartProcess_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (!_isVideoSaved && File.Exists(strCapturePath_or_URL))
                {
                    MessageBoxResult result = MessageBox.Show("Video not saved, would you still like to continue?", "Live Video Editor", MessageBoxButton.YesNo, MessageBoxImage.Information);
                    if (result == MessageBoxResult.Yes)
                    {
                        if (File.Exists(playerOutput))
                            File.Delete(playerOutput);
                        EnableORDisableControls(false);
                        m_objComposer.OnFrame += new IMEvents_OnFrameEventHandler(m_objComposer_OnFrame);
                        CancelbwRoute = false;
                        //Initilizing of  input files
                        InitializeAllInputFiles();

                        // StartWriter();                
                        // StartWriterLive();
                    }
                }
                else
                {
                    EnableORDisableControls(false);
                    m_objComposer.OnFrame += new IMEvents_OnFrameEventHandler(m_objComposer_OnFrame);
                    CancelbwRoute = false;
                    //Initilizing of  input files
                    InitializeAllInputFiles();

                    // StartWriter();
                    // StartWriterLive();
                }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }
        //private void InitializeAllInputFiles()
        //{
        //    try
        //    {
        //        MItem m_pFile; string streamId = string.Empty;
        //        int nCount = 0;
        //        m_objComposer.StreamsGetCount(out nCount);

        //        for (int i = 0; i < nCount; i++)
        //        {
        //            m_objComposer.StreamsGetByIndex(i, out streamId, out m_pFile);
        //            try
        //            {
        //                string name = string.Empty;
        //                m_pFile.FileNameGet(out name);
        //                if (m_pFile != null && !name.Contains("MLive"))
        //                {
        //                    m_objComposer.FilePlayStop(0);
        //                    m_pFile.FilePosSet(MHelpers.ParsePos("00:00:00"), 1.0);
        //                }
        //            }
        //            catch { }
        //        }
        //        m_objComposer.FilePlayStart();
        //    }
        //    catch (Exception ex)
        //    {
        //        string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
        //        ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
        //    }
        //}
        private void InitializeAllInputFiles()
        {
            try
            {
                MItem m_pFile; string streamId = string.Empty;
                int nCount = 0;
                m_objComposer.StreamsGetCount(out nCount);
                m_objComposer.FilePlayPause(0);
                for (int i = 0; i < nCount; i++)
                {
                    m_objComposer.StreamsGetByIndex(i, out streamId, out m_pFile);
                    try
                    {
                        m_pFile.FilePosSet(0, 0);
                    }
                    catch { }
                }
                m_objComposer.FilePlayStart();
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }

        private void StartWriter()
        {
            string uniqueName = GetVideoName();
            VidTimer.Start();
            //      mFormatControl1.SetControlledObjectForMWriter(m_objComposer);

            eMState eState;
            m_objWriter.ObjectStateGet(out eState);
            if (eState == eMState.eMS_Paused)
            {
                // Continue capture
                m_objWriter.ObjectStart(m_objComposer);
            }
            if (eState == eMState.eMS_Running)
            {
                m_objWriter.WriterNameSet("", "");
            }
            else
            {
                strCapturePath_or_URL = processVideoTemp + uniqueName + "." + outputFormat;
                try
                {
                    m_objWriter.WriterNameSet(strCapturePath_or_URL, comboBoxProps.Text != "" ? comboBoxProps.Text : "video::bitrate=1M audio::bitrate=64K");
                    // m_objWriter.WriterNameSet(strCapturePath_or_URL, "audio::bitrate=256K video::bitrate=30M video::codec=mpeg4 gop_size=1 qmin=1 qscale=1 v422=true");
                    //  m_objWriter.ObjectStart(pSource);
                    m_objWriter.ObjectStart(m_objComposer);
                }
                catch (System.Exception ex)
                {
                    string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                    ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                    return;
                }
            }
        }
        private void StartWriterLive()
        {
            //  mFormatControl1.SetControlledObjectForMWriter(m_objLive);
            eMState eState;
            m_objWriterLive.ObjectStateGet(out eState);
            if (eState == eMState.eMS_Paused)
            {
                m_objWriterLive.ObjectStart(m_objLive);
            }
            if (eState == eMState.eMS_Running)
            {
                m_objWriterLive.WriterNameSet("", "");
            }
            else
            {
                string temp = "." + outputFormat;
                string toReplace = "_GS." + outputFormat;
                string greenFile = strCapturePath_or_URL.Replace(temp, toReplace);
                try
                {
                    m_objWriterLive.WriterNameSet(greenFile, comboBoxProps.Text != "" ? comboBoxProps.Text : "video::bitrate=1M audio::bitrate=64K");
                    // m_objWriterLive.WriterNameSet(greenFile, "audio::bitrate=256K video::bitrate=30M video::codec=mpeg4 gop_size=1 qmin=1 qscale=1 v422=true");
                    m_objWriterLive.ObjectStart(m_objLive);
                }
                catch (System.Exception ex)
                {
                    string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                    ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                    return;
                }
            }
        }
        //private void StartWriterLive()
        //{

        //    Object pSource = m_objLive;
        //    Object pSender = null;
        //    try
        //    {
        //        m_objWriterLive.SendersGet("MLive", out pSender);
        //    }
        //    catch (Exception ex)
        //    { }
        //    if (m_objLive != null)
        //    {
        //        pSource = pSender;
        //    }
        //    else
        //        m_objLive.ObjectClose();
        //    m_objLive.ObjectStart(pSender);
        //    mFormatControl1.SetControlledObjectForMWriter(pSender);

        //    eMState eState;
        //    m_objWriterLive.ObjectStateGet(out eState);
        //    if (eState == eMState.eMS_Paused)
        //    {
        //        // Continue capture
        //        m_objWriterLive.ObjectStart(pSource);
        //    }
        //    if (eState == eMState.eMS_Running)
        //    {
        //        m_objWriterLive.WriterNameSet("", "");
        //    }
        //    else
        //    {
        //        string strFormat;
        //        IMAttributes pFormatConfig;
        //        m_objWriterLive.ConfigGet("format", out strFormat, out pFormatConfig);
        //        int bNetwork = 0;
        //        try
        //        {
        //            pFormatConfig.AttributesBoolGet("network", out bNetwork);
        //        }
        //        catch (System.Exception) { }
        //        int bHave = 0;
        //        string strExtensions;
        //        pFormatConfig.AttributesHave("extensions", out bHave, out strExtensions);
        //        string greenFile = strCapturePath_or_URL.Replace(".", "_GS.");

        //        try
        //        {
        //            //  m_objWriterLive.WriterNameSet(strCapturePath_or_URL, comboBoxProps.Text != "" ? comboBoxProps.Text : "video::bitrate=1M audio::bitrate=64K");
        //            m_objWriterLive.WriterNameSet(greenFile, "audio::bitrate=256K video::bitrate=30M video::codec=mpeg4 gop_size=1 qmin=1 qscale=1 v422=true");
        //            m_objWriterLive.ObjectStart(pSource);
        //        }
        //        catch (System.Exception ex)
        //        {
        //            return;
        //        }
        //    }
        //}
        private void btnStopProcess_Click(object sender, RoutedEventArgs e)
        {
            StopWriter(true);
            if (!string.IsNullOrEmpty(strCapturePath_or_URL) && File.Exists(strCapturePath_or_URL))
            {
                File.Delete(strCapturePath_or_URL);
            }
            if (!string.IsNullOrEmpty(playerOutput) && File.Exists(playerOutput))
            {
                File.Delete(playerOutput);
            }
            EnableORDisableControls(true);
            txbPause.Text = "Pause";
        }
        private void buttonChromaProps_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (mMixerList1.SelectedItem != null)
                {
                    MItem mitem = null;
                    LoadChromaPlugin(true, mMixerList1.SelectedItem);
                    MCHROMAKEYLib.MChromaKey objChromaKey = GetChromakeyFilter(mMixerList1.SelectedItem);
                    try
                    {
                        ((IMProps)objChromaKey).PropsSet("gpu_mode", "true");
                        mitem = (MItem)mMixerList1.SelectedItem;
                    }
                    catch { }
                    if (objChromaKey != null)
                    {
                        FormChromaKey formCK = new FormChromaKey(objChromaKey);
                        formCK.ShowDialog();
                        string streamId = string.Empty;
                        mitem.FileNameGet(out streamId);
                        ((IMProps)mitem).PropsGet("stream_id", out streamId);
                        string chpath = lstVideoSceneObject.Where(x => x.VideoObjectId == streamId).FirstOrDefault().ObjectFileMapping.ChromaPath;
                        if (!string.IsNullOrEmpty(chpath))
                        {
                            if (File.Exists(System.IO.Path.Combine(ConfigManager.DigiFolderPath, chpath)))
                            {
                                //  File.Delete(System.IO.Path.Combine(ConfigManager.DigiFolderPath, chpath));
                                //System.IO.Path.Combine(System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location), "DigiProcessVideoTemp");
                                File.Copy(System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "ChromaSettings.xml"), System.IO.Path.Combine(ConfigManager.DigiFolderPath, chpath), true);
                            }
                        }
                    }
                }
            }
            catch (Exception Exception) { }
        }
        private MCHROMAKEYLib.MChromaKey GetChromakeyFilter(object source)
        {
            MCHROMAKEYLib.MChromaKey pChromaKey = null;
            try
            {
                int nCount = 0;
                IMPlugins pPlugins = (IMPlugins)source;
                pPlugins.PluginsGetCount(out nCount);
                for (int i = 0; i < nCount; i++)
                {
                    object pPlugin;
                    long nCBCookie;
                    pPlugins.PluginsGetByIndex(i, out pPlugin, out nCBCookie);
                    try
                    {
                        pChromaKey = (MCHROMAKEYLib.MChromaKey)pPlugin;
                        break;
                    }
                    catch { }
                }
            }
            catch { }
            return pChromaKey;
        }
        private void mMixerList1_OnMixerSelChanged(object sender, EventArgs e)
        {
            System.Windows.Forms.ListView listView = (System.Windows.Forms.ListView)sender;
            if (listView.SelectedItems.Count > 0)
            {
                MCHROMAKEYLib.MChromaKey objChromaKey = GetChromakeyFilter(listView.SelectedItems[0].ToString());
                //checkBoxCK.IsChecked = objChromaKey == null ? false : true;
                buttonChromaProps.IsEnabled = true; ;
            }
            else
            {
                //checkBoxCK.IsChecked = false; ;
                buttonChromaProps.IsEnabled = false;
            }


        }
        private void LoadChromaPlugin(bool onloadChroma, object source)
        {
            if (mMixerList1.SelectedItem != null)
            {
                IMPlugins pPlugins = source as IMPlugins;
                int nCount;
                long nCBCookie;
                object pPlugin = null;
                bool isCK = false;
                pPlugins.PluginsGetCount(out nCount);
                for (int i = 0; i < nCount; i++)
                {
                    pPlugins.PluginsGetByIndex(i, out pPlugin, out nCBCookie);

                    if (pPlugin.GetType().Name == "CoMChromaKeyClass" || pPlugin.GetType().Name == "MChromaKeyClass")
                    {
                        isCK = true;
                        break;
                    }
                }
                if ((isCK == false) || (onloadChroma && isCK == false))
                {
                    pPlugins.PluginsAdd(new MCHROMAKEYLib.MChromaKey(), 0);
                }
                buttonChromaProps.IsEnabled = true;
            }
        }
        private MCHROMAKEYLib.MChromaKey LoadChromaPlugin(bool onloadChroma, object source, int a)
        {
            MCHROMAKEYLib.MChromaKey pChromaKey = null;
            //  if (mMixerList1.SelectedItem != null)
            {
                IMPlugins pPlugins = (IMPlugins)source;
                int nCount;
                long nCBCookie;
                object pPlugin = null;
                bool isCK = false;
                pPlugins.PluginsGetCount(out nCount);
                for (int i = 0; i < nCount; i++)
                {
                    pPlugins.PluginsGetByIndex(i, out pPlugin, out nCBCookie);

                    if (pPlugin.GetType().Name == "CoMChromaKeyClass" || pPlugin.GetType().Name == "MChromaKeyClass")
                    {
                        isCK = true;
                        break;
                    }
                }
                if ((isCK == false) || (onloadChroma && isCK == false))
                {
                    pChromaKey = new MCHROMAKEYLib.MChromaKey();
                    pPlugins.PluginsAdd(pChromaKey, 0);
                }
                else if (isCK == true)
                {
                    pPlugins.PluginsRemove(pPlugin);
                }
                buttonChromaProps.IsEnabled = true;
                //  Marshal.ReleaseComObject(pPlugins);
            }
            return pChromaKey;
        }
        //private void checkBoxCK_Checked(object sender, RoutedEventArgs e)
        //{
        //    if (checkBoxCK.IsChecked == true)
        //    {
        //        LoadChromaPlugin(false, mMixerList1.SelectedItem);
        //    }
        //}
        private void FillSenders(IMSenders pSenders)
        {
            comboBoxExtSources.Items.Clear();
            comboBoxExtSources.Items.Add("<Live Source>");

            int nCount = 0;
            pSenders.SendersGetCount(out nCount);
            for (int i = 0; i < nCount; i++)
            {
                string strName;
                M_VID_PROPS vidProps;
                M_AUD_PROPS audProps;
                pSenders.SendersGetByIndex(i, out strName, out vidProps, out audProps);

                comboBoxExtSources.Items.Add(strName);
            }

            comboBoxExtSources.SelectedIndex = 0;
        }

        private void ExtractThumbnail()
        {
            MFrame mf1;
            m_objComposer.ObjectFrameGet(out mf1, "");
            string path_temp = processVideoTemp + "Output.jpg";
            if (File.Exists(path_temp))
            {
                File.Delete(path_temp);
            }
            mf1.FrameVideoSaveToFile(path_temp);
            Marshal.ReleaseComObject(mf1);

        }
        private void VidTimer_Tick(object sender, EventArgs e)
        {
            decimal vidLengthNow = 0;
            decimal vidLength = VideoLength + 0.043M;
            txtStatus.Text = UpdateStatus(m_objWriter, out vidLengthNow);
            eMState eState;
            m_objWriter.ObjectStateGet(out eState);

            pbProgress.Value = Convert.ToDouble(vidLengthNow);
            decimal percent = ((vidLengthNow / vidLength) * 100);
            txtPercentage.Text = System.Math.Round(percent, 0).ToString() + "%";
            txtTimer.Text = vidLengthNow.ToString("0") + " sec";
            if (vidLengthNow >= vidLength)
            {
                StopWriter(false);
            }
        }
        string UpdateStatus(MWriterClass pCapture, out decimal VidLength)
        {
            VidLength = 0; string strVidlen;
            string strRes = "";
            try
            {
                eMState eState;
                pCapture.ObjectStateGet(out eState);
                string strPid;
                m_objWriter.PropsGet("stat::last::server_pid", out strPid);
                string strSkip;
                pCapture.PropsGet("stat::skip", out strSkip);
                strRes = " State: " + eState + (strSkip != null ? " Skip rest:" + DblStrTrim(strSkip, 3) : "") + " Server PID:" + strPid + "\r\n";

                string sFile;
                pCapture.WriterNameGet(out sFile);
                strRes += " Path: " + sFile + "\r\n";

                {
                    strRes += "TOTAL:\r\n";
                    string strBuffer;
                    pCapture.PropsGet("stat::buffered", out strBuffer);
                    string strFrames;
                    pCapture.PropsGet("stat::frames", out strFrames);

                    string strFPS;
                    pCapture.PropsGet("stat::fps", out strFPS);

                    string strTime;
                    pCapture.PropsGet("stat::video_time", out strTime);

                    string strBreaks = "0";
                    pCapture.PropsGet("stat::breaks", out strBreaks);
                    string strDropped = "0";
                    pCapture.PropsGet("stat::frames_dropped", out strDropped);

                    m_objWriter.PropsGet("stat::video_len", out strVidlen);
                    VidLength = Convert.ToDecimal(strVidlen);
                    string strAVtime;
                    m_objWriter.PropsGet("stat::av_sync_time", out strAVtime);
                    string strAVlen;
                    m_objWriter.PropsGet("stat::av_sync_len", out strAVlen);
                    strRes += "  Buffers:" + strBuffer + " Frames:" + strFrames + " Fps:" + DblStrTrim(strFPS, 3) + " Dropped:" + strDropped +
                    " Breaks:" + strBreaks + "  Video time:" + DblStrTrim(strTime, 3) + "\r\n" + " Video Len:" + strVidlen + " AV Sync time:" +
                    strAVtime + " AV sync len:" + strAVlen + "\r\n";
                    string strSamples;
                    pCapture.PropsGet("stat::samples", out strSamples);
                    pCapture.PropsGet("stat::audio_time", out strTime);

                    strRes += "  Samples:" + strSamples + " Audio time:" + DblStrTrim(strTime, 3) + "\r\n";
                }
                {
                    strRes += "LAST:\r\n";

                    string strFrames;
                    pCapture.PropsGet("stat::last::frames", out strFrames);

                    string strFPS = "";
                    pCapture.PropsGet("stat::last::fps", out strFPS);

                    string strTime = "";
                    pCapture.PropsGet("stat::last::video_time", out strTime);

                    string strBreaks;
                    pCapture.PropsGet("stat::breaks", out strBreaks);

                    strRes += "  Frames:" + strFrames + " Breaks:" + strBreaks +
                        " Video time:" + DblStrTrim(strTime, 3) + "\r\n";

                    string strSamples;
                    pCapture.PropsGet("stat::last::samples", out strSamples);
                    pCapture.PropsGet("stat::last::audio_time", out strTime);

                    strRes += "  Samples:" + strSamples + " Audio time:" + DblStrTrim(strTime, 3);
                }
                return strRes;
            }
            catch (Exception ex)
            {
                return strRes;
            }
        }
        string DblStrTrim(string str, int nPeriod)
        {
            if (str == null)
                return str = "0";
            int nDot = str.LastIndexOf('.');
            int nPeriodHave = nDot >= 0 ? str.Length - nDot - 1 : 0;
            if (nPeriodHave > nPeriod)
            {
                return str.Substring(0, nDot + nPeriod + 1);
            }
            // Add zeroes
            if (nDot < 0)
                str += ".";

            for (int i = nPeriodHave; i < nPeriod; i++)
            {
                str += "0";
            }
            return str;
        }
        private void btnLoadScene_Click(object sender, RoutedEventArgs e)
        {
            strDefaultSceneSettings = string.Empty; 
            txtPercentage.Text = "0%";
            txtTimer.Text = "0 sec";
            if (htVideoObjects != null)
            {
                htVideoObjects.Clear();
            }
            if (cmbScene.SelectedIndex > 0)
            {
                loadScene((int)cmbScene.SelectedValue);
               
            }

        }

        private void loadSceneCombo()
        {
            business = new VideoSceneBusiness();
            lstVideoScene = new List<VideoScene>();
            lstVideoScene = business.GetVideoScene(0, subStoreId);
            Dictionary<int, string> scenes = new Dictionary<int, string>();
            scenes.Clear();
            scenes.Add(0, "--Select Scene--");
            foreach (VideoScene item in lstVideoScene.Where(o => o.IsActive == true))
            {
                scenes.Add(item.SceneId, item.Name);
            }
            cmbScene.ItemsSource = scenes;
            if (scenes.Count > 0)
                cmbScene.SelectedIndex = 1;
        }

        private void LoadVideoObjectList(List<VideoSceneObject> lstVideoObjects)
        {
            foreach (var item in lstVideoObjects)
            {
                MItem mitem; int indx = 0;
                m_objComposer.StreamsGet(item.VideoObjectId, out indx, out mitem);
                string name;
                mitem.FileNameGet(out name);
                item.FileName = System.IO.Path.GetFileName(name);
            }
            datagrdAudioSettings.ItemsSource = lstVideoObjects;
        }
        private void cmbScene_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            strDefaultSceneSettings = string.Empty;
            if (cmbScene.SelectedIndex > 0)
                loadScene((int)cmbScene.SelectedValue);
        }
        int subStoreId = 0;
        public void getSubstoreID()
        {
            try
            {
                string pathtosave = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location);
                if (File.Exists(pathtosave + "\\ss.dat"))
                {
                    string line;
                    using (StreamReader reader = new StreamReader(pathtosave + "\\ss.dat"))
                    {
                        line = reader.ReadLine();
                        string subID = DigiPhoto.CryptorEngine.Decrypt(line, true);
                        if (subID.Contains(','))
                        {
                            subStoreId = Convert.ToInt32((subID.Split(',')[0]));
                        }
                        else
                        {
                            subStoreId = Convert.ToInt32(subID);
                        }
                    }
                }
                else
                {
                    ErrorHandler.ErrorHandler.LogFileWrite("Please select substore from Configuration Section in Imix for this machine.");
                }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }
        int locationId;
        void loadScene(int sceneId)
        {
            try
            {
                mFormatControl1.SetControlledObject(m_objComposer);//reload the format control settings
                var v = cmbScene.SelectedValue;
                lstVideoScene = new List<VideoScene>();
                btnStartProcess.IsEnabled = false;
                lstVideoScene = business.GetVideoScene(sceneId, subStoreId);
                locationId = lstVideoScene.FirstOrDefault().LocationId;
                GetConfigLocationData();
                if (lstVideoScene != null)
                {
                    btnStartProcess.IsEnabled = true;
                    int subStoreID = ConfigManager.SubStoreId;
                    string filePath = System.IO.Path.Combine(ConfigManager.DigiFolderPath, lstVideoScene.FirstOrDefault().ScenePath);
                    //    List<VideoSceneObject> lstVideoSceneObject = new List<VideoSceneObject>();
                    business = new VideoSceneBusiness();
                    lstVideoSceneObject = business.GuestVideoObjectBySceneID(sceneId);
                    if (lstVideoSceneObject.Count > 0)
                    {
                        m_pMPersist = (IMPersist)m_objComposer;
                        m_pMPersist.PersistLoad("", filePath, "");
                        mMixerList1.UpdateList(true, 1);
                        mElementsTree1.UpdateTree(false);
                        VideoSceneObject obj = lstVideoSceneObject.Where(x => x.GuestVideoObject == true).FirstOrDefault();
                        if (obj != null)
                            AddLive(obj.VideoObjectId);
                        else
                            AddLive(null);
                        LoadChromaAndRouteSettings(lstVideoSceneObject);
                        VideoLength = lstVideoScene.FirstOrDefault().VideoLength;
                        LoadWriterSettings(lstVideoScene.FirstOrDefault().Settings);
                        pbProgress.Maximum = Convert.ToDouble(VideoLength);
                        //Implement vertical design changes here
                        if (lstVideoScene.FirstOrDefault().IsVerticalVideo)
                        {
                            MPLATFORMLib.M_VID_PROPS props = new MPLATFORMLib.M_VID_PROPS();
                            props.eVideoFormat = MPLATFORMLib.eMVideoFormat.eMVF_Custom;
                            props.dblRate = 29;
                            props.nAspectX = 9;
                            props.nAspectY = 16;
                            props.nHeight = 1920;
                            props.nWidth = 1080;

                            m_objComposer.FormatVideoSet(eMFormatType.eMFT_Convert, ref props);
                            props.eScaleType = eMScaleType.eMST_IgnoreAR;
                            m_objLive.FormatVideoSet(eMFormatType.eMFT_Convert, ref props);
                            mFormatControl1.comboBoxVideo.Enabled = false;
                        }
                        else
                            mFormatControl1.comboBoxVideo.Enabled = true;
                        LoadVideoObjectList(lstVideoSceneObject);
                        if (FlipMode > 0)
                        {
                            MElement me = GetGuestVideoElement(obj.VideoObjectId);
                            ApplyFlip(me, FlipMode);
                            Marshal.ReleaseComObject(me);
                        }
                        ////added by latika 
                        GETQuickSettings();
                        //end
                    }
                    else
                    {
                        btnStartProcess.IsEnabled = false;
                        MessageBox.Show("No video scene objects found!\n", "Live Video Editor", MessageBoxButton.OK, MessageBoxImage.Information, MessageBoxResult.OK);
                    }
                }
                else
                {
                    btnStartProcess.IsEnabled = false;
                    MessageBox.Show("No video scene settings found!\n", "Live Video Editor", MessageBoxButton.OK, MessageBoxImage.Information, MessageBoxResult.OK);
                }

            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }

        void AddLive(string vidObjectId)
        {
            try
            {
                MLiveClass objLive = new MLiveClass();
                FormLive formLive = new FormLive();
                formLive.m_pDevice = objLive;
                m_pDevice = (IMDevice)objLive;
                //((IMObject)m_pDevice).ObjectStart(null);//init device

                //System.IO.Path.Combine(System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location), "DigiProcessVideoTemp");
                if (!File.Exists(System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "CameraSettings.xml")))
                {
                    formLive.ShowDialog();
                }
                else
                {
                    formLive.Show();
                    formLive.Close();
                }
                ((IMObject)m_pDevice).ObjectStart(null);//init device
                m_pMixerStreams = (IMStreams)m_objComposer;
                eMState eState;
                objLive.ObjectStateGet(out eState);
                if (eState != eMState.eMS_Running)
                    return;
                MItem pLive;
                m_pMixerStreams.StreamsAdd(vidObjectId, objLive, "live_src", "external_object='false'" + null, out pLive, 0);
                mMixerList1.UpdateList(true, 1);

                string strCur = "";
                string strParam = "";
                int nIndex = 0;

                //Get current video device
                objLive.DeviceGet("video", out strCur, out strParam, out nIndex);
                m_objLive.DeviceSet("video", strCur, "");

                //objLive.DeviceGet("video::line-in", out strCur, out strParam, out nIndex);
                //m_objLive.DeviceSet("video::line-in", strCur, "");

                try
                {
                    objLive.DeviceGet("audio", out strCur, out strParam, out nIndex);
                    m_objLive.DeviceSet("audio", strCur, "");
                }
                catch
                { }

                //objLive.DeviceGet("audio::line-in", out strCur, out strParam, out nIndex);
                //m_objLive.DeviceSet("audio::line-in", strCur, "");

                m_objLive.ObjectStart(null);
                mPreviewControlGreenScreen.SetControlledObject(m_objLive);
                Marshal.ReleaseComObject(pLive);
                GC.Collect();
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }

        //Load chroma 
        private void LoadChromaAndRouteSettings(List<VideoSceneObject> lstVideoSceneObject)
        {
            try
            {
                foreach (var item in lstVideoSceneObject.Where(o => o.ObjectFileMapping.ChromaPath != null || o.ObjectFileMapping.RoutePath != null))
                {
                    if (item.ObjectFileMapping.ChromaPath != "")
                    {
                        MItem pItem; int myIndex;
                        m_objComposer.StreamsGet(item.VideoObjectId, out myIndex, out pItem);
                        string chromaPath = System.IO.Path.Combine(ConfigManager.DigiFolderPath, item.ObjectFileMapping.ChromaPath);
                        MCHROMAKEYLib.MChromaKey objChromaKey = LoadChromaPlugin(true, pItem, 1);
                        try
                        {
                            ((IMProps)objChromaKey).PropsSet("gpu_mode", "true");
                        }
                        catch { }
                        //Apply chroma
                        if (System.IO.File.Exists(chromaPath))
                            (objChromaKey as IMPersist).PersistLoad("", chromaPath, "");
                        Marshal.ReleaseComObject(objChromaKey);
                        Marshal.ReleaseComObject(pItem);
                        GC.Collect();
                    }
                    if (item.ObjectFileMapping.RoutePath != "")
                    {
                        string vidId = "";
                        string rootpath = System.IO.Path.Combine(ConfigManager.DigiFolderPath, item.ObjectFileMapping.RoutePath);
                        vidId = item.ObjectFileMapping.RoutePath.Substring(item.ObjectFileMapping.RoutePath.LastIndexOf("\\") + 1).Replace("Route-", "");
                        vidId = vidId.Substring(0, vidId.Length - 4);
                        ReadVideoObjectRouteFile(rootpath, vidId);

                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }
        Hashtable htVideoObjects = new Hashtable();
        #region Apply Route



        //Read Route File
        private void ReadVideoObjectRouteFile(string filePath, string vidId)
        {
            try
            {
                int counter = 0; Hashtable htRouteList = new Hashtable();
                string line;
                string x = string.Empty;
                string y = string.Empty;
                string h = string.Empty;
                string w = string.Empty;
                string r = string.Empty;
                string d = string.Empty;
                System.IO.StreamReader file = new System.IO.StreamReader(filePath);
                while ((line = file.ReadLine()) != null)
                {
                    string[] coordinates = line.Split(';');
                    if (coordinates.Length > 1)
                    {
                        Hashtable htListVar = new Hashtable();

                        x = coordinates[0];
                        y = coordinates[1];
                        h = coordinates[2];
                        w = coordinates[3];
                        r = coordinates[4];
                        d = coordinates[5];

                        htListVar.Add("x", x);
                        htListVar.Add("y", y);
                        htListVar.Add("h", h);
                        htListVar.Add("w", w);
                        htListVar.Add("r", r);
                        htListVar.Add("tc", d);
                        htRouteList.Add(counter, htListVar);
                        counter++;
                    }
                }
                file.Close();
                if (!htVideoObjects.ContainsKey(vidId))
                    htVideoObjects.Add(vidId, htRouteList);
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }
        private void ApplyVideoObjectRoute(MElement elemRoute, Hashtable htRouteList)
        {
            for (int i = 0; i <= htRouteList.Count - 1; i++)
            {
                string varRoute = GetUpdateElementString((Hashtable)htRouteList[i]);
                elemRoute.ElementMultipleSet(varRoute, 0.0);
                Thread.Sleep(50);
                if (CancelbwRoute)
                {
                    KillBackgroundWorker(bwRoute);
                    break;
                }
            }
        }

        private void ApplyVideoObjectRoute(MElement elemRoute, Hashtable htRouteList, int count)
        {
            try
            {
                if (htRouteList.Count > count)
                {
                    string varRoute = GetUpdateElementString((Hashtable)htRouteList[count]);
                    elemRoute.ElementMultipleSet(varRoute, 0.0);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }

        //To get the level of the tree element
        private MElement GetMElement(string vidObject)
        {

            MElement elemRoute = null; MElement myElementRoute = null;
            try
            {
                string entry = string.Empty;

                m_objComposer.ElementsGetByIndex(3, out myElementRoute);
                if (myElementRoute != null)
                    ((IMElements)myElementRoute).ElementsGetByID(vidObject, out elemRoute);
                return elemRoute;
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                return elemRoute = null;
            }
            finally
            {
                //Marshal.ReleaseComObject(elemRoute);
                //Marshal.ReleaseComObject(myElementRoute);
            }
        }
        private string GetUpdateElementString(Hashtable htSingleRoute)
        {
            string strVar = string.Empty;
            foreach (string key in htSingleRoute.Keys)
            {
                strVar += htSingleRoute[key].ToString() + " ";
            }
            return strVar;
        }
        public void KillBackgroundWorker(System.ComponentModel.BackgroundWorker bw)
        {
            bw.CancelAsync();
            bw.Dispose();
            bw = null;
            GC.Collect();
        }
        //private void bwRoute_DoWork(object Sender, System.ComponentModel.DoWorkEventArgs e)
        //{
        //    try
        //    {
        //        if (bwRoute.CancellationPending)
        //        {
        //            Thread.Sleep(1200);
        //            e.Cancel = true;
        //        }
        //        else
        //        {
        //            foreach (var item in htVideoObjects.Keys)
        //            {
        //                MElement elemRoute = GetMElement(item.ToString());
        //                ApplyVideoObjectRoute(elemRoute, (Hashtable)htVideoObjects[item]);
        //            }
        //        }
        //    }

        //    catch (Exception ex)
        //    {
        //        string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
        //        ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
        //    }
        //}

        #endregion
        #region writer settings
        private void LoadWriterSettings(string settings)
        {
            try
            {
                XmlDocument xdoc = new XmlDocument();
                xdoc.LoadXml(settings);
                XmlNodeList nodes = xdoc.GetElementsByTagName("Settings");
                if (nodes.Count > 0)
                {
                    foreach (XmlNode node in nodes)
                    {
                        int Videoid = Convert.ToInt32(node.ChildNodes[1].InnerText);
                        int Audioid = Convert.ToInt32(node.ChildNodes[3].InnerText);
                        string OpFormat = node.ChildNodes[4].InnerText;
                        string Videocodec = node.ChildNodes[6].InnerText.Trim();
                        string Audiocodec = node.ChildNodes[5].InnerText.Trim();
                        mFormatControl1.comboBoxVideo.SelectedIndex = Videoid;
                        mFormatControl1.comboBoxAudio.SelectedIndex = Audioid;
                        string VideoSettings = string.Empty;
                        if (node.ChildNodes.Count >= 6)
                            VideoSettings = node.ChildNodes[7].InnerText.Trim();

                        (((MControls.ListViewEx)(mConfigList1)).Columns[1].ListView).Items[0].SubItems[1].Text = OpFormat;//o/p format
                        (((MControls.ListViewEx)(mConfigList1)).Columns[1].ListView).Items[1].SubItems[1].Text = Audiocodec;//audiocodec

                        if (Videocodec != "")
                        { (((MControls.ListViewEx)(mConfigList1)).Columns[1].ListView).Items[2].SubItems[1].Text = Videocodec; }//videocodec   }

                        IMAttributes pConfigProps;
                        m_objWriter.ConfigSet("format", OpFormat, out pConfigProps);
                        int bHave = 0; string strExtensions = string.Empty;
                        pConfigProps.AttributesHave("extensions", out bHave, out strExtensions);

                        string[] arr = strExtensions.Split(',');
                        outputFormat = arr[0]; //set output format
                        comboBoxProps.Text = VideoSettings;
                    }
                }
            }
            catch
            {

            }
        }
        #endregion
        private void StopWriter(bool isbtnStop)
        {
            try
            {
                frameCount = 0;
                EnableORDisableControls(true);
                m_objComposer.OnFrame -= new IMEvents_OnFrameEventHandler(m_objComposer_OnFrame);
                CancelbwRoute = true;
                VidTimer.Stop();
                m_objWriter.ObjectClose();
                m_objWriterLive.ObjectClose();
                _isVideoSaved = false;
                if (!isbtnStop)
                {
                    playerOutput = processVideoTemp + "Player" + System.IO.Path.GetExtension(strCapturePath_or_URL);
                    File.Copy(strCapturePath_or_URL, playerOutput, true);
                    if (MessageBox.Show("Video created successfully!\n\rWould you like to save it now?", "Live Video Editor", MessageBoxButton.YesNo, MessageBoxImage.Question, MessageBoxResult.No) == MessageBoxResult.Yes)
                    {
                        btnSaveFile.IsEnabled = false;
                        btnSaveFile_Click(null, null);
                    }
                }
                txtPercentage.Text = "0%";
                txtTimer.Text = "0 sec";
                pbProgress.Value = 0;
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }
        int selScene = 0;
        string playerOutput = string.Empty;
        private void btnSaveFile_Click(object sender, RoutedEventArgs e)
        {
            if (System.IO.File.Exists(strCapturePath_or_URL))
            {
                bs.Show();
                selScene = Convert.ToInt32(cmbScene.SelectedValue);
                bwSaveVideos.RunWorkerAsync();
            }
            else
                MessageBox.Show("Video not found!\n", "Live Video Editor", MessageBoxButton.OK, MessageBoxImage.Information, MessageBoxResult.OK);
        }

        private void SaveFiles()
        {
            try
            {
                VideoScene objScene = lstVideoScene.Where(o => o.SceneId == selScene).FirstOrDefault();
                CameraInfo camera = (new CameraBusiness()).GetLocationWiseCameraDetails(objScene.LocationId);
                if (camera == null)
                {
                    MessageBox.Show("There is no camera with live stream settings!\n", "Live Video Editor", MessageBoxButton.OK, MessageBoxImage.Information, MessageBoxResult.OK);
                    return;
                }
                string cameraFolderpath = ConfigManager.DigiFolderPath + "Camera\\" + camera.DG_CameraFolder + "\\";       //.Replace("\\\\DigiImages\\\\", "\\DigiImages\\");
                if (System.IO.Directory.Exists(cameraFolderpath))
                {
                    if (System.IO.File.Exists(strCapturePath_or_URL))
                        File.Move(strCapturePath_or_URL, cameraFolderpath + System.IO.Path.GetFileName(strCapturePath_or_URL.Replace(".", "_PR.")));
                    if (System.IO.File.Exists(strCapturePath_or_URL.Replace(".", "_GS.")))
                        File.Move(strCapturePath_or_URL.Replace(".", "_GS."), cameraFolderpath + System.IO.Path.GetFileName(strCapturePath_or_URL.Replace(".", "_GS.")));
                    //save video settings.


                }
                MessageBox.Show("Video moved to camera folder successfully!\n", "Live Video Editor", MessageBoxButton.OK, MessageBoxImage.Information, MessageBoxResult.OK);
                _isVideoSaved = true;
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                MessageBox.Show("Video can not be moved!\n Please check network accessibility!\n", "Live Video Editor", MessageBoxButton.OK, MessageBoxImage.Information, MessageBoxResult.OK);
            }
        }

        #region HideShow Settings Panel
        Boolean ShowHideSet = false;
        private void btnShowHideSettings_Click(object sender, RoutedEventArgs e)
        {
            if (!ShowHideSet)
                ShowHideSet = true;
            else
                ShowHideSet = false;

            ShowHideSettings(ShowHideSet);
        }

        private void ShowHideSettings(bool flag)
        {
            if (flag)
            {
                stkSettingsRightPanel.Visibility = Visibility.Visible;
                stkSettingsBottomPanel.Visibility = Visibility.Visible;
            }
            else
            {
                stkSettingsRightPanel.Visibility = Visibility.Collapsed;
                stkSettingsBottomPanel.Visibility = Visibility.Collapsed;
            }
        }
        #endregion HideShow Settings Panel

        #region Expanders
        private void CollapseOthers(Expander exp)
        {
            if (exp != ExpSceneSettings)
            {
                ExpSceneSettings.IsExpanded = false;
            }
            if (exp != ExpRecordSettings)
            {
                ExpRecordSettings.IsExpanded = false;
            }
            if (exp != ExpRecordProgress)
            {
                ExpRecordProgress.IsExpanded = false;
            }
            if (exp != ExpPreview)
            {
                ExpPreview.IsExpanded = false;
            }
            if (exp != ExpEnableAudio)
            {
                ExpEnableAudio.IsExpanded = false;
            }
            if (exp != ExpQuickSettings)
            {
                ExpQuickSettings.IsExpanded = false;
            }

        }
        private void ExpRecordSettings_Expanded(object sender, RoutedEventArgs e)
        {
            Expander exp = (Expander)sender;
            CollapseOthers(exp);
        }
        private void ExpSceneSettings_Expanded(object sender, RoutedEventArgs e)
        {
            Expander exp = (Expander)sender;
            CollapseOthers(exp);
        }

        private void ExpPreview_Expanded(object sender, RoutedEventArgs e)
        {
            Expander exp = (Expander)sender;
            CollapseOthers(exp);
        }

        private void ExpRecordProgress_Expanded(object sender, RoutedEventArgs e)
        {
            Expander exp = (Expander)sender;
            CollapseOthers(exp);
        }
        #endregion Expanders
        private void btnPauseProcess_Click(object sender, RoutedEventArgs e)
        {
            eMState eState;
            m_objWriter.ObjectStateGet(out eState);
            if (eState == eMState.eMS_Paused)
            {
                m_objComposer.OnFrame += new IMEvents_OnFrameEventHandler(m_objComposer_OnFrame);
                m_objComposer.FilePlayStart();
                m_objWriter.ObjectStart(null);
                m_objWriter.ObjectStateGet(out eState);
                m_objWriterLive.ObjectStart(null);

                txbPause.Text = "Pause";
            }
            else
            {
                m_objComposer.OnFrame -= new IMEvents_OnFrameEventHandler(m_objComposer_OnFrame);
                m_objComposer.FilePlayPause(0);
                m_objWriter.WriterSkip(0);
                m_objWriterLive.WriterSkip(0);
                txbPause.Text = "Continue";
            }
        }
        private string GetVideoName()
        {
            const string chars = "0123456789";
            var random = new Random();
            return "live" + new string(Enumerable.Repeat(chars, 5).Select(s => s[random.Next(s.Length)]).ToArray());
            //byte[] buffer = Guid.NewGuid().ToByteArray();
            //return "live-" + BitConverter.ToUInt32(buffer, 6).ToString();
        }

        private void InvalidatePropertyChange(Slider sdr, double timeForChange)
        {
            if (SelectedTreeElement != null)
            {
                if (sdr == sldXAxis)
                    ApplyPropertyChange(SelectedTreeElement, "x", Convert.ToString(sdr.Value), timeForChange);
                if (sdr == sldYAxis)
                    ApplyPropertyChange(SelectedTreeElement, "y", Convert.ToString(sdr.Value), timeForChange);
                if (sdr == sldHeight)
                    ApplyPropertyChange(SelectedTreeElement, "h", Convert.ToString(sdr.Value), timeForChange);
                if (sdr == sldWidth)
                    ApplyPropertyChange(SelectedTreeElement, "w", Convert.ToString(sdr.Value), timeForChange);
                if (sdr == sldRotation)
                    ApplyPropertyChange(SelectedTreeElement, "r", Convert.ToString(sdr.Value), timeForChange);
                if (sdr == sldCropX)
                    ApplyPropertyChange(SelectedTreeElement, "sx", Convert.ToString(sdr.Value), timeForChange);
                if (sdr == sldCropY)
                    ApplyPropertyChange(SelectedTreeElement, "sy", Convert.ToString(sdr.Value), timeForChange);
                if (sdr == sldCropH)
                    ApplyPropertyChange(SelectedTreeElement, "sh", Convert.ToString(sdr.Value), timeForChange);
                if (sdr == sldCropW)
                    ApplyPropertyChange(SelectedTreeElement, "sw", Convert.ToString(sdr.Value), timeForChange);
            }
        }
        private void InvalidatePropertyChange(TextBox textB, double timeForChange)
        {
            if (SelectedTreeElement != null)
            {
                if (textB == txtAudioGain)
                    ApplyPropertyChange(SelectedTreeElement, "audio_gain", Convert.ToString(txtAudioGain.Text), timeForChange);
                if (textB == txtStreamId)
                    ApplyPropertyChange(SelectedTreeElement, "stream_id", Convert.ToString(txtStreamId.Text), timeForChange);
            }
        }
        private void InvalidatePropertyChange(ComboBox cmb, double timeForChange)
        {
            if (SelectedTreeElement != null)
            {
                if (cmb == drpPosition)
                    ApplyPropertyChange(SelectedTreeElement, "pos", Convert.ToString(drpPosition.SelectedValue), timeForChange);
                if (cmb == drpCropP)
                    ApplyPropertyChange(SelectedTreeElement, "spos", Convert.ToString(drpCropP.SelectedValue), timeForChange);
            }
        }
        private void ApplyPropertyChange(MElement SelectedElem, string attribute, string value, double timeForChange)
        {
            SelectedElem.ElementMultipleSet(attribute + "=" + value, timeForChange);
        }

        private void btnPreviewVideo_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (File.Exists(playerOutput))
                {
                    SetVisibility(false);
                    popVideoPlayer.vsMediaFileName = playerOutput;
                    popVideoPlayer.SetParent(this);
                    popVideoPlayer.Visibility = Visibility.Visible;
                }
                else
                {
                    MessageBox.Show("No video available to preview!", "Live Video Editor", MessageBoxButton.OK, MessageBoxImage.Information, MessageBoxResult.OK);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }
        bool isRightPnlVisible = false;
        public void SetVisibility(bool IsVisible)
        {
            if (IsVisible)
            {
                winMPreviewComposer.Visibility = Visibility.Visible;
                winMStreamsList.Visibility = Visibility.Visible;
                winMConfigListl.Visibility = Visibility.Visible;
                winMFormatControl.Visibility = Visibility.Visible;
                winMPreviewGreenScreen.Visibility = Visibility.Visible;
                if (isRightPnlVisible == true)
                {
                    stkSettingsRightPanel.Visibility = Visibility.Visible;
                    isRightPnlVisible = false;
                }

            }
            else
            {
                winMPreviewComposer.Visibility = Visibility.Hidden;
                winMStreamsList.Visibility = Visibility.Hidden;
                winMConfigListl.Visibility = Visibility.Hidden;
                winMFormatControl.Visibility = Visibility.Hidden;
                winMPreviewGreenScreen.Visibility = Visibility.Hidden;
                if (stkSettingsRightPanel.Visibility == Visibility.Visible)
                {
                    isRightPnlVisible = true;
                    stkSettingsRightPanel.Visibility = Visibility.Hidden;
                }

            }
        }
        public void EnableORDisableControls(bool IsEnabled)
        {

            winMPreviewComposer.IsEnabled = IsEnabled;
            winMStreamsList.IsEnabled = IsEnabled;
            winMConfigListl.IsEnabled = IsEnabled;
            winMFormatControl.IsEnabled = IsEnabled;
            winMPreviewGreenScreen.IsEnabled = IsEnabled;
            btnStartProcess.IsEnabled = IsEnabled;
            btnPreviewVideo.IsEnabled = IsEnabled;
            cmbScene.IsEnabled = IsEnabled;
            btnLoadScene.IsEnabled = IsEnabled;
            ExpSceneSettings.IsEnabled = IsEnabled;
            btnClose.IsEnabled = IsEnabled;
            buttonChromaProps.IsEnabled = IsEnabled;
            btnSaveFile.IsEnabled = IsEnabled;
            btnPauseProcess.IsEnabled = !IsEnabled;
            btnStopProcess.IsEnabled = !IsEnabled;
            buttonCameraSettings.IsEnabled = IsEnabled;
        }
        private void GetElementInformation(string strAttributes)
        {
            try
            {
                Hashtable htAttrib = new Hashtable();
                XmlDocument sdoc = new XmlDocument();
                sdoc.LoadXml(strAttributes);
                XmlNode xnode = sdoc.FirstChild;
                for (int i = 0; i < xnode.Attributes.Count; i++)
                {
                    string name = Convert.ToString(xnode.Attributes[i].Name);
                    string value = Convert.ToString(xnode.Attributes[i].Value);
                    if (!htAttrib.ContainsKey(name))
                    {
                        htAttrib.Add(name, value);
                    }
                }
                if (htAttrib.Count > 0)
                {
                    PopulateProperties(htAttrib);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }
        private void PopulateProperties(Hashtable htAttrib)
        {
            txtXAxis.Text = htAttrib.ContainsKey("x") ? htAttrib["x"].ToString() : "0.0";
            txtYAxis.Text = htAttrib.ContainsKey("y") ? htAttrib["y"].ToString() : "0.0";
            txtHeight.Text = htAttrib.ContainsKey("h") ? htAttrib["h"].ToString() : "0.0";
            txtWidth.Text = htAttrib.ContainsKey("w") ? htAttrib["w"].ToString() : "0.0";
            txtRotation.Text = htAttrib.ContainsKey("r") ? htAttrib["r"].ToString() : "0.0";
            txtCropX.Text = htAttrib.ContainsKey("sx") ? htAttrib["sx"].ToString() : "1.0";
            txtCropY.Text = htAttrib.ContainsKey("sy") ? htAttrib["sy"].ToString() : "1.0";
            txtCropH.Text = htAttrib.ContainsKey("sh") ? htAttrib["sh"].ToString() : "1.0";
            txtCropW.Text = htAttrib.ContainsKey("sw") ? htAttrib["sw"].ToString() : "1.0";
            txtAudioGain.Text = htAttrib.ContainsKey("audio_gain") ? htAttrib["audio_gain"].ToString() : "+0.0";
            txtStreamId.Text = htAttrib.ContainsKey("stream_id") ? htAttrib["stream_id"].ToString() : "";

            drpPosition.SelectedValue = htAttrib.ContainsKey("pos") ? htAttrib["pos"].ToString() : "bottom-left";
            drpCropP.SelectedValue = htAttrib.ContainsKey("spos") ? htAttrib["spos"].ToString() : "bottom-left";

        }
        private List<string> FillPositionList()
        {
            List<string> Positions = new List<string>();
            Positions.Add("center");
            Positions.Add("right");
            Positions.Add("left");
            Positions.Add("top");
            Positions.Add("bottom");
            Positions.Add("bottom-right");
            Positions.Add("bottom-left");
            Positions.Add("top-right");
            Positions.Add("top-left");
            return Positions;
        }
        private void FillPositionDropDown()
        {
            try
            {

                List<string> Positions = FillPositionList();
                drpPosition.Items.Clear();
                drpPosition.ItemsSource = Positions;
                drpCropP.Items.Clear();
                drpCropP.ItemsSource = Positions;
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }
        private void sldXAxis_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            InvalidatePropertyChange((Slider)sender, 0.0);
        }

        private void sldYAxis_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            InvalidatePropertyChange((Slider)sender, 0.0);
        }

        private void sldHeight_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            InvalidatePropertyChange((Slider)sender, 0.0);
        }

        private void sldWidth_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            InvalidatePropertyChange((Slider)sender, 0.0);
        }

        private void sldRotation_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            InvalidatePropertyChange((Slider)sender, 0.0);
        }

        private void sldCropX_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            InvalidatePropertyChange((Slider)sender, 0.0);
        }

        private void sldCropY_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            InvalidatePropertyChange((Slider)sender, 0.0);
        }

        private void sldCropW_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            InvalidatePropertyChange((Slider)sender, 0.0);
            sldWidth.Value = sldCropW.Value;
        }

        private void sldCropH_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            InvalidatePropertyChange((Slider)sender, 0.0);
            sldHeight.Value = sldCropH.Value;
        }

        private void txtAudioGain_LostFocus(object sender, RoutedEventArgs e)
        {
            InvalidatePropertyChange((TextBox)sender, 0.0);
        }

        private void txtStreamId_LostFocus(object sender, RoutedEventArgs e)
        {
            InvalidatePropertyChange((TextBox)sender, 0.0);
        }

        private void drpPosition_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            InvalidatePropertyChange((ComboBox)sender, 0.0);
        }
        private void drpCropP_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            InvalidatePropertyChange((ComboBox)sender, 0.0);
        }

        #region Quick Settings
        private void btnFullScreen_Checked(object sender, RoutedEventArgs e)
        {
            ///This code works fine
            if (mPreviewComposerControl.m_pPreview != null)
            {
                // Enable full screen (use -1 for auto-select monitor)
                if ((Boolean)btnFullScreen.IsChecked)
                {
                    mPreviewComposerControl.m_pPreview.PreviewFullScreen("", 1, -1);
                    btnFullScreen.IsChecked = false;
                }
                else
                    mPreviewComposerControl.m_pPreview.PreviewFullScreen("", 0, -1);
            }
        }
        private void sldVolume_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            // Volume in dB
            // 0 - full volume, -100 silence
            double dblPos = (double)sldVolume.Value / sldVolume.Maximum;
            if (mPreviewComposerControl.m_pPreview != null)
                mPreviewComposerControl.m_pPreview.PreviewAudioVolumeSet("", -1, -30 * (1 - dblPos));
        }
        private void btnAspectRatio_Click(object sender, RoutedEventArgs e)
        {
            if (mPreviewComposerControl.m_pPreview != null)
                ((IMProps)mPreviewComposerControl.m_pPreview).PropsSet("maintain_ar", (Boolean)btnAspectRatio.IsChecked ? "none" : "letter-box");
        }
        private void btnAudio_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (mPreviewComposerControl.m_pPreview != null)
                    mPreviewComposerControl.m_pPreview.PreviewEnable("", (Boolean)btnAudio.IsChecked ? 0 : 1, (Boolean)btnVideoPreview.IsChecked ? 0 : 1);
            }
            catch
            {

            }
        }
        private void btnVideoPreview_Click(object sender, RoutedEventArgs e)
        {
            if (mPreviewComposerControl.m_pPreview != null)
                mPreviewComposerControl.m_pPreview.PreviewEnable("", (Boolean)btnAudio.IsChecked ? 0 : 1, (Boolean)btnVideoPreview.IsChecked ? 0 : 1);
        }
        #endregion Quick Settings
        private void ExpEnableAudio_Expanded(object sender, RoutedEventArgs e)
        {
            Expander exp = (Expander)sender;
            CollapseOthers(exp);
        }

        private void chkEnableAudio_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                CheckBox chkBox = (CheckBox)sender;
                int pitem = 0;
                MItem mitem;
                IMElements element = (IMElements)m_objComposer;
                m_objComposer.StreamsGet(chkBox.Tag.ToString(), out pitem, out mitem);

                IMProps m_pProps = (IMProps)mitem;
                if (mitem != null)
                {
                    if (chkBox.IsChecked == true)
                    {
                        m_pProps.PropsSet("object::audio_gain", "0");
                    }
                    else
                    {
                        m_pProps.PropsSet("object::audio_gain", "-100");
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }

        private void ExpQuickSettings_Expanded(object sender, RoutedEventArgs e)
        {
            Expander exp = (Expander)sender;
            CollapseOthers(exp);
        }
        private void ClearAllTempVideos()
        {
            DirectoryInfo dir = new DirectoryInfo(processVideoTemp);
            try
            {
                foreach (var item in dir.GetFiles())
                {
                    try
                    {
                        item.Delete();
                    }
                    catch { }
                }
            }
            catch
            { }
        }

        #region Common Methods For Locking
        /// <summary>
        /// Enables the disable task manager.
        /// </summary>
        /// <param name="enable">if set to <c>true</c> [enable].</param>
        private static void EnableDisableTaskManager(bool enable)
        {
            Microsoft.Win32.RegistryKey HKCU = Microsoft.Win32.Registry.CurrentUser;
            Microsoft.Win32.RegistryKey key = HKCU.CreateSubKey(@"Software\Microsoft\Windows\CurrentVersion\Policies\System");
            key.SetValue("DisableTaskMgr", enable ? 0 : 1, Microsoft.Win32.RegistryValueKind.DWord);
        }

        /// <summary>
        /// Lows the level keyboard proc.
        /// </summary>
        /// <param name="nCode">The asynchronous code.</param>
        /// <param name="wParam">The forward parameter.</param>
        /// <param name="lParam">The calculate parameter.</param>
        /// <returns></returns>
        private int LowLevelKeyboardProc(int nCode, int wParam, ref KBDLLHOOKSTRUCT lParam)
        {
            bool blnEat = false;
            switch (wParam)
            {
                case 256:
                case 257:
                case 260:
                case 261:
                    //Alt+Tab, Alt+Esc, Ctrl+Esc, Windows Key
                    if (((lParam.vkCode == 9) && (lParam.flags == 32)) ||
                    ((lParam.vkCode == 27) && (lParam.flags == 32)) || ((lParam.vkCode ==
                    27) && (lParam.flags == 0)) || ((lParam.vkCode == 91) && (lParam.flags
                    == 1)) || ((lParam.vkCode == 92) && (lParam.flags == 1)) || ((true) &&
                    (lParam.flags == 32)))
                    {
                        blnEat = true;
                    }
                    break;
            }
            try
            {
                if (blnEat)
                    return 1;
                else
                {
                    try
                    {
                        int Hook = CallNextHookEx(0, nCode, wParam, ref lParam);
                        return Hook;
                    }
                    catch (Exception)
                    {
                        return 1;
                    }

                }
            }
            catch (Exception ex)
            {
                return 1;
            }

        }
        static LowLevelKeyboardProcDelegate mHookProc;
        private void KeyboardHook(object sender, EventArgs e)
        {
            // NOTE: ensure delegate can't be garbage collected
            mHookProc = new LowLevelKeyboardProcDelegate(LowLevelKeyboardProc);
            // Get handle to main .exe
            IntPtr hModule = GetModuleHandle(IntPtr.Zero);
            // Hook
            intLLKey = SetWindowsHookEx(WH_KEYBOARD_LL, mHookProc, hModule, 0);
            if (intLLKey == IntPtr.Zero)
            {
                MessageBox.Show("Failed to set hook,error = " + Marshal.GetLastWin32Error().ToString());

            }

        }

        /// <summary>
        /// Releases the keyboard hook.
        /// </summary>
        private void ReleaseKeyboardHook()
        {
            intLLKey = UnhookWindowsHookEx(intLLKey);
        }
        #endregion

        private void btnMinimize_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                ReleaseKeyboardHook();
                EnableDisableTaskManager(true);
                Taskbar.Show();
                this.WindowState = WindowState.Minimized;
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }
        private MElement GetGuestVideoElement(string comparedID)
        {
            MElement GuestElement = null;
            int tempCount = 0;
            m_objComposer.ElementsGetCount(out tempCount);

            for (int i = 0; i < tempCount; i++)
            {
                MElement tmpElem;
                m_objComposer.ElementsGetByIndex(i, out tmpElem);
                if (tmpElem != null)
                {
                    int tempCount2 = 0;
                    ((IMElements)tmpElem).ElementsGetCount(out tempCount2);
                    for (int k = 0; k < tempCount2; k++)
                    {
                        MElement tmpChildElem;
                        ((IMElements)tmpElem).ElementsGetByIndex(k, out tmpChildElem);

                        if (tmpChildElem != null)
                        {
                            string strID; int have = 0;
                            tmpChildElem.AttributesHave("stream_id", out have, out strID);
                            ///  tmpChildElem.("stream_id", out strID);
                            if (strID == comparedID)
                            {
                                GuestElement = tmpChildElem;
                                break;
                            }
                        }
                    }

                }
            }
            return GuestElement;
        }
        private void ApplyFlip(MElement pElement, int FlipMode)
        {
            //MElement elemRoute = GetMElement(LiveVideoObjectID);
            if (FlipMode == 1)
                pElement.ElementMultipleSet("rh=180", 0.0);
            else if (FlipMode == 2)
                pElement.ElementMultipleSet("rv=180", 0.0);
        }

        private void Window_Closing(object sender, CancelEventArgs e)
        {
            try
            {
                CloseApplication();
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }

        private static void CloseApplication()
        {
            Process thisProc = Process.GetCurrentProcess();
            // Check how many total processes have the same name as the current one
            if (Process.GetProcessesByName(thisProc.ProcessName).Length >= 1)
            {
                System.Windows.Application.Current.Shutdown();
                thisProc.Kill();
            }
        }

        private void btnReset_Click(object sender, RoutedEventArgs e)
        {
            GetElementInformation(strDefaultSceneSettings);
        }

        private void buttonCameraSettings_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                GetSetCamera();
                GC.Collect();
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }

        private void GetSetCamera()
        {
            int nFiles = 0;
            m_pMixerStreams.StreamsGetCount(out nFiles);
            for (int i = 0; i < nFiles; i++)
            {
                MItem pItem;
                string strStreamID;
                m_pMixerStreams.StreamsGetByIndex(i, out strStreamID, out pItem);

                // Get type
                eMItemType eItemType;
                pItem.ItemTypeGet(out eItemType);

                if (eItemType == eMItemType.eMPIT_Live)
                {
                    try
                    {
                        // TODO: Make non-modal
                        FormLive formLive = new FormLive();
                        formLive.m_pDevice = (IMDevice)pItem;
                        formLive.ShowDialog();
                    }
                    catch (System.Exception) { }
                    Marshal.ReleaseComObject(pItem);
                    break;
                }
                Marshal.ReleaseComObject(pItem);
            }

        }
               
        #region Added by ajay on 25 April 16 
        static string filename = string.Empty;
        private void BtnSaveSetting_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                string selSceneName = Convert.ToString(((System.Collections.Generic.KeyValuePair<int, string>)cmbScene.SelectedItem).Value);
                string filename = System.IO.Path.Combine(ConfigManager.DigiFolderPath, "Profiles", selSceneName, "interactivesettings.xml");
                System.Windows.Forms.TreeNodeCollection collection =
                   (((System.Windows.Forms.TreeView)winMElementsTree1.Child).Nodes);
                var RootNodename = string.Empty;
                var SubRootNodename = string.Empty;
                foreach (var item in collection)
                {
                    if (((System.Windows.Forms.TreeNode)item).Nodes.Count > 0)
                    {
                        foreach (var subnode in ((System.Windows.Forms.TreeNode)item).Nodes)
                        {
                            if (((System.Windows.Forms.TreeNode)subnode).IsSelected)
                            {
                                RootNodename = ((System.Windows.Forms.TreeNode)item).Text;
                                SubRootNodename = ((System.Windows.Forms.TreeNode)subnode).Text;
                            }
                        }
                    }
                }
                XmlDocument xml = new XmlDocument();
                string Subrootnode = Regex.Replace(SubRootNodename, @"[^0-9a-zA-Z]+", "");
                string filename1 = System.IO.Path.Combine(ConfigManager.DigiFolderPath, "Profiles", selSceneName, "interactivesettings.xml");

                //string filename1 = System.IO.Path.Combine(@"D:\Ajay\RnD\RnD", "interactivesettings.xml");
                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.Load(filename1);
                XmlNodeList nodes = xmlDoc.SelectNodes("/Settings/" + RootNodename);
                foreach (XmlNode node in nodes)
                {
                    foreach (XmlElement item in node)
                    {
                        if (((System.Xml.XmlElement)item).Name == Subrootnode)
                        {
                            node.RemoveChild(item);
                        }
                    }
                }
                xmlDoc.Save(filename1);
                XmlDocument xmlDoc1 = new XmlDocument();
                xmlDoc1.Load(filename1);
                XmlNode RootNode = xmlDoc1.SelectSingleNode("/Settings/" + RootNodename);
                //create a node inside root node
                XmlElement SubRootNode = xmlDoc1.CreateElement(Regex.Replace(SubRootNodename, @"[^0-9a-zA-Z]+", ""));
                XmlElement XAxis = xmlDoc1.CreateElement("XAxis");
                XAxis.InnerText = txtXAxis.Text;
                SubRootNode.AppendChild(XAxis);

                XmlElement YAxis = xmlDoc1.CreateElement("YAxis");
                YAxis.InnerText = txtYAxis.Text;
                SubRootNode.AppendChild(YAxis);

                XmlElement AudioGain = xmlDoc1.CreateElement("AudioGain");
                AudioGain.InnerText = txtAudioGain.Text;
                SubRootNode.AppendChild(AudioGain);

                XmlElement CropH = xmlDoc1.CreateElement("CropH");
                CropH.InnerText = txtCropH.Text;
                SubRootNode.AppendChild(CropH);


                XmlElement CropW = xmlDoc1.CreateElement("CropW");
                CropW.InnerText = txtCropW.Text;
                SubRootNode.AppendChild(CropW);

                XmlElement CropX = xmlDoc1.CreateElement("CropX");
                CropX.InnerText = txtCropX.Text;
                SubRootNode.AppendChild(CropX);

                XmlElement CropY = xmlDoc1.CreateElement("CropY");
                CropY.InnerText = txtCropY.Text;
                SubRootNode.AppendChild(CropY);

                XmlElement Height = xmlDoc1.CreateElement("Height");
                Height.InnerText = txtHeight.Text;
                SubRootNode.AppendChild(Height);

                XmlElement Width = xmlDoc1.CreateElement("Width");
                Width.InnerText = txtWidth.Text;
                SubRootNode.AppendChild(Width);

                XmlElement Percentage = xmlDoc1.CreateElement("Percentage");
                Percentage.InnerText = txtPercentage.Text;
                SubRootNode.AppendChild(Percentage);

                XmlElement Rotation = xmlDoc1.CreateElement("Rotation");
                Rotation.InnerText = txtRotation.Text;
                SubRootNode.AppendChild(Rotation);

                XmlElement Status = xmlDoc1.CreateElement("Status");
                Status.InnerText = txtStatus.Text;
                SubRootNode.AppendChild(Status);

                XmlElement CropP = xmlDoc1.CreateElement("CropP");
                CropP.InnerText = drpCropP.Text;
                SubRootNode.AppendChild(CropP);

                XmlElement Position = xmlDoc1.CreateElement("Position");
                Position.InnerText = drpPosition.Text;
                SubRootNode.AppendChild(Position);


                XmlElement StreamId = xmlDoc1.CreateElement("StreamId");
                StreamId.InnerText = txtStreamId.Text;
                SubRootNode.AppendChild(StreamId);
                RootNode.AppendChild(SubRootNode);
                xmlDoc1.DocumentElement.AppendChild(RootNode);
                xmlDoc1.Save(filename1);

                //save recording settings 

                string SettingXML = string.Empty;
                string AudioCodec = string.Empty;
                string VideoCodec = string.Empty;
                string AudioFormat = mFormatControl1.comboBoxAudio.SelectedItem.ToString();
                string VideoFormat = mFormatControl1.comboBoxVideo.SelectedItem.ToString();
                string OutputFormat = (((MControls.ListViewEx)(mConfigList1)).Columns[1].ListView).Items[0].SubItems[1].Text;
                if ((((MControls.ListViewEx)(mConfigList1)).Columns[1].ListView).Items[1].SubItems[1].Text != null)
                {
                    AudioCodec = (((MControls.ListViewEx)(mConfigList1)).Columns[1].ListView).Items[1].SubItems[1].Text;
                }
                if ((((MControls.ListViewEx)(mConfigList1)).Columns[1].ListView).Items.Count > 2)
                {
                    VideoCodec = (((MControls.ListViewEx)(mConfigList1)).Columns[1].ListView).Items[2].SubItems[1].Text;
                }
                string strConfig;
                m_objWriter.ConfigGetAll(1, out strConfig);
                if (!(strConfig.Length > 20))
                {
                    string configSettings = "audio::bitrate=256K video::bitrate=30M video::codec=mpeg4 gop_size=1 qmin=1 qscale=1 v422=true";
                    strConfig = configSettings;
                }
                SettingXML = getSettingXml(AudioFormat, VideoFormat, OutputFormat, AudioCodec, VideoCodec, strConfig);
                VideoScene vScene = new VideoScene();
                vScene.Settings = SettingXML;
                vScene.SceneId = (int)cmbScene.SelectedValue;
                vScene.Name = cmbScene.Text;
                vScene.Settings = SettingXML;
                //////added by latika
                QuickSettings vSetting = new QuickSettings();
             
                vSetting.ScenID = Convert.ToInt32(cmbScene.SelectedValue);
                vSetting.Volume = Convert.ToInt32(sldVolume.Value);
                vSetting.AspectRatio =Convert.ToBoolean(btnAspectRatio.IsChecked);
                vSetting.Video = Convert.ToBoolean(btnVideoPreview.IsChecked);
                vSetting.Audio = Convert.ToBoolean(btnAudio.IsChecked);
                vSetting.FullScreen = Convert.ToBoolean(btnFullScreen.IsChecked);
                vSetting.LocationID = locationId;
                vSetting.SubStorID = subStoreId;
                bool updated1 = SaveUpdLiveVideoQuickSettings(vSetting);

                /////ended by latika
                bool updated = UpdateOtherSettings(vScene);
               



                MessageBox.Show("Settings saved successfully");
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }


        #endregion

        public class Interactive
        {
            public String title;
        }
        public void ReadXMLFile(string InteractivePath)
        {
            // First write something so that there is something to read ...  
            var b = new Interactive { title = "Serialization Overview" };
            var writer = new System.Xml.Serialization.XmlSerializer(typeof(Interactive));
            var wfile = new System.IO.StreamWriter(InteractivePath);
            writer.Serialize(wfile, b);
            wfile.Close();

            // Now we can read the serialized book ...  
            System.Xml.Serialization.XmlSerializer reader =
                new System.Xml.Serialization.XmlSerializer(typeof(Interactive));
            System.IO.StreamReader file = new System.IO.StreamReader(InteractivePath);
            Interactive overview = (Interactive)reader.Deserialize(file);
            file.Close();

        }


        private string getSettingXml(string AudioFormat, string VideoFormat, string OutputFormat, string AudioCodec, string VideoCodec, string strConfig)
        {
            string outSettingXML = string.Empty;
            string VideoSelectedIndex = mFormatControl1.comboBoxVideo.SelectedIndex.ToString();
            string AudioSelectedIndex = mFormatControl1.comboBoxAudio.SelectedIndex.ToString();
            outSettingXML += "<Settings>";
            outSettingXML += "<VideoResolution>";
            outSettingXML += VideoFormat;
            outSettingXML += "</VideoResolution>";
            outSettingXML += "<VideoResolutionSelectedIndex>";
            outSettingXML += VideoSelectedIndex;
            outSettingXML += "</VideoResolutionSelectedIndex>";
            outSettingXML += "<AudioResolution>";
            outSettingXML += AudioFormat;
            outSettingXML += "</AudioResolution>";
            outSettingXML += "<AudioResolutionSelectedIndex>";
            outSettingXML += AudioSelectedIndex;
            outSettingXML += "</AudioResolutionSelectedIndex>";
            outSettingXML += "<OutputFormat>";
            outSettingXML += OutputFormat;
            outSettingXML += "</OutputFormat>";
            outSettingXML += "<AudioCodec>";
            outSettingXML += AudioCodec;
            outSettingXML += "</AudioCodec>";
            outSettingXML += "<VideoCodec>";
            outSettingXML += VideoCodec;
            outSettingXML += "</VideoCodec>";
            //outSettingXML += "<strConfig>";
            //outSettingXML += UpdateBitRates(); //strConfig;
            //outSettingXML += "</strConfig>";
            outSettingXML += "</Settings>";
            return outSettingXML;

        }

    }
    #region Class TO disable Taskbar
    public class Taskbar
    {
        [DllImport("user32.dll")]
        private static extern int GetWindowText(IntPtr hWnd, StringBuilder text, int count);
        [DllImport("user32.dll", CharSet = CharSet.Auto)]
        private static extern bool EnumThreadWindows(int threadId, EnumThreadProc pfnEnum, IntPtr lParam);
        [DllImport("user32.dll", SetLastError = true)]
        private static extern System.IntPtr FindWindow(string lpClassName, string lpWindowName);
        [DllImport("user32.dll", SetLastError = true)]
        private static extern IntPtr FindWindowEx(IntPtr parentHandle, IntPtr childAfter, string className, string windowTitle);
        [DllImport("user32.dll")]
        private static extern IntPtr FindWindowEx(IntPtr parentHwnd, IntPtr childAfterHwnd, IntPtr className, string windowText);
        [DllImport("user32.dll")]
        private static extern int ShowWindow(IntPtr hwnd, int nCmdShow);
        [DllImport("user32.dll")]
        private static extern uint GetWindowThreadProcessId(IntPtr hwnd, out int lpdwProcessId);

        private const int SW_HIDE = 0;
        private const int SW_SHOW = 5;

        private const string VistaStartMenuCaption = "Start";
        private static IntPtr vistaStartMenuWnd = IntPtr.Zero;
        private delegate bool EnumThreadProc(IntPtr hwnd, IntPtr lParam);

        /// <summary>
        /// Show the taskbar.
        /// </summary>
        public static void Show()
        {
            SetVisibility(true);
        }

        /// <summary>
        /// Hide the taskbar.
        /// </summary>
        public static void Hide()
        {
            SetVisibility(false);
        }

        /// <summary>
        /// Sets the visibility of the taskbar.
        /// </summary>
        public static bool Visible
        {
            set { SetVisibility(value); }
        }

        /// <summary>
        /// Hide or show the Windows taskbar and startmenu.
        /// </summary>
        /// <param name="show">true to show, false to hide</param>
        private static void SetVisibility(bool show)
        {
            // get taskbar window
            IntPtr taskBarWnd = FindWindow("Shell_TrayWnd", null);

            // try it the WinXP way first...
            IntPtr startWnd = FindWindowEx(taskBarWnd, IntPtr.Zero, "Button", "Start");

            if (startWnd == IntPtr.Zero)
            {
                // try an alternate way, as mentioned on CodeProject by Earl Waylon Flinn
                startWnd = FindWindowEx(IntPtr.Zero, IntPtr.Zero, (IntPtr)0xC017, "Start");
            }

            if (startWnd == IntPtr.Zero)
            {
                // ok, let's try the Vista easy way...
                startWnd = FindWindow("Button", null);

                if (startWnd == IntPtr.Zero)
                {
                    // no chance, we need to to it the hard way...
                    startWnd = GetVistaStartMenuWnd(taskBarWnd);
                }
            }

            ShowWindow(taskBarWnd, show ? SW_SHOW : SW_HIDE);
            ShowWindow(startWnd, show ? SW_SHOW : SW_HIDE);
        }

        /// <summary>
        /// Returns the window handle of the Vista start menu orb.
        /// </summary>
        /// <param name="taskBarWnd">windo handle of taskbar</param>
        /// <returns>window handle of start menu</returns>
        private static IntPtr GetVistaStartMenuWnd(IntPtr taskBarWnd)
        {
            // get process that owns the taskbar window
            int procId;
            GetWindowThreadProcessId(taskBarWnd, out procId);

            Process p = Process.GetProcessById(procId);
            if (p != null)
            {
                // enumerate all threads of that process...
                foreach (ProcessThread t in p.Threads)
                {
                    EnumThreadWindows(t.Id, MyEnumThreadWindowsProc, IntPtr.Zero);
                }
            }
            return vistaStartMenuWnd;
        }

        /// <summary>
        /// Callback method that is called from 'EnumThreadWindows' in 'GetVistaStartMenuWnd'.
        /// </summary>
        /// <param name="hWnd">window handle</param>
        /// <param name="lParam">parameter</param>
        /// <returns>true to continue enumeration, false to stop it</returns>
        private static bool MyEnumThreadWindowsProc(IntPtr hWnd, IntPtr lParam)
        {
            StringBuilder buffer = new StringBuilder(256);
            if (GetWindowText(hWnd, buffer, buffer.Capacity) > 0)
            {
                Console.WriteLine(buffer);
                if (buffer.ToString() == VistaStartMenuCaption)
                {
                    vistaStartMenuWnd = hWnd;
                    return false;
                }
            }
            return true;
        }





    }
    #endregion
}
