﻿using DigiPhoto.IMIX.Business;
using DigiPhoto.IMIX.Model;
using FrameworkHelper;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace LiveVideoEditing
{
    /// <summary>
    /// Interaction logic for VideoSlotsTime.xaml
    /// </summary>
    public partial class VideoPlayer : UserControl
    {
        private UIElement _parent;
        DispatcherTimer timer = new DispatcherTimer();
        public string vsMediaFileName {get;set;}
        public BitmapImage imagesource { get; set; }
        public string Title { get; set; }
        private FileStream memoryFileStream;
        bool isMuted = false;
        MLMediaPlayer mplayer;
        public void SetParent(UIElement parent)
        {
            _parent = (UIElement)parent;
            if (!string.IsNullOrEmpty(vsMediaFileName))
            {
               MediaStop();
               MediaPlay();
            }
        }
        public VideoPlayer()
        {
            InitializeComponent();      
            mplayer = new MLMediaPlayer(vsMediaFileName, "VideoPlayer");
        }

        private void btnClose_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                MediaStop();
                imagesource = null;
                vsMediaFileName = string.Empty;
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
            HideHandlerDialog();
        }

        private void HideHandlerDialog()
        {
            Visibility = Visibility.Collapsed;
            _parent.IsEnabled = true;
            ((MLLiveCapture)_parent).SetVisibility(true);
        }
        #region MediaPlayer
        void MediaStop()
        {
            if (mplayer != null)
            {
                mplayer.MediaStop();
                mplayer = null;
            }
            gdMediaPlayer.Children.Clear();
            gdMediaPlayer.Children.Remove(mplayer);
        }
       
        void MediaPlay()
        {
            MediaStop();
            if (mplayer != null)
            {
                mplayer.Dispose();
            }
            gdMediaPlayer.Dispatcher.BeginInvoke(new Action(
                () =>
                {
                    mplayer = new MLMediaPlayer(vsMediaFileName, "Search");
                    gdMediaPlayer.BeginInit();
                    gdMediaPlayer.Children.Clear();
                    gdMediaPlayer.Children.Add(mplayer);
                    gdMediaPlayer.EndInit();
                }));
        }

        #endregion

        private void UserControl_Unloaded(object sender, RoutedEventArgs e)
        {
            MediaClose();
            btnClose.Click -= new RoutedEventHandler(btnClose_Click);
        }
        void MediaClose()
        {
            if (mplayer != null)
            {
                mplayer.MediaClose();              
                mplayer = null;
            }
            gdMediaPlayer.Children.Clear();
            gdMediaPlayer.Children.Remove(mplayer);
        }
    }
}
