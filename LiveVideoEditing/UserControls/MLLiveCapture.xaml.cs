﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using MControls;
using MPLATFORMLib;
using FrameworkHelper;
using DigiPhoto.Common;
using System.IO;
using System.Runtime.InteropServices;
using DigiPhoto.IMIX.Model;
using DigiPhoto.IMIX.Business;
using System.Windows.Threading;
using LiveCapture.Interop;
using FrameworkHelper.Common;
using DigiPhoto;

namespace LiveVideoEditing
{
    /// <summary>
    /// Interaction logic for MLLiveCapture.xaml
    /// </summary>
    public partial class MLLiveCapture : UserControl
    {
        public UIElement _parent;
        private bool _result = false;
        MComposerClass m_objComposer;
        MCHROMAKEYLib.IMPersist persist;
        private IMPersist _mPersist;
        /// /Writer
        public MLiveClass m_objLive;
        public MWriterClass m_objWriter;
        IMConfig m_pConfigRoot;
        // For extern object
        public MPreviewClass m_objExtPreview;
        private IMPersist m_pMPersist;
        // Called if user change playlist selection
        public event EventHandler OnLoad;
        // int loopCount = 0;

        /// <summary>
        /// Images
        /// </summary>
        public List<VideoPage> lstGuestItem = new List<VideoPage>();
        public List<TemplateListItems> lstTemplate = new List<TemplateListItems>();
        public string strRFID = string.Empty;
        public int productId = 0;
        int VideoLength = 10;
        string outputFormat = "mp4";
        System.ComponentModel.BackgroundWorker bwVideoEditing = new System.ComponentModel.BackgroundWorker();
        BusyWindow bs = new BusyWindow();
        string processVideoTemp = Environment.CurrentDirectory + "\\DigiProcessVideoTemp\\";
        bool isCreateNew = false;
        bool IsProcessedVideoEditing = false;
        ProcessedVideoInfo objProcVideoInfo = new ProcessedVideoInfo();
        List<PanProperty> PanPropertyLst = new List<PanProperty>();
        List<TransitionProperty> TransitionPropertyLst = new List<TransitionProperty>();
        List<SlotProperty> borderslotlist = new List<SlotProperty>();
        List<SlotProperty> LogoSlotList = new List<SlotProperty>();
        DispatcherTimer VidTimer;
        public MLLiveCapture()
        {
            InitializeComponent();
            VidTimer = new DispatcherTimer();

            VidTimer.Tick += new EventHandler(VidTimer_Tick);
            try
            {
                m_objLive = new MLiveClass();
                m_objComposer = new MComposerClass();
                m_objWriter = new MWriterClass();
                m_objExtPreview = new MPreviewClass();
            }
            catch (Exception exception)
            {
                MessageBox.Show("Can't create a MPlatform's object: " + exception.ToString(), "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            //  mPersistControl1.SetControlledObject(m_objComposer);
            mMixerList1.SetControlledObject(m_objComposer);
            mElementsTree1.SetControlledObject(m_objComposer);
            mPreviewComposerControl.SetControlledObject(m_objComposer);
            //mFileState1.SetControlledObject(m_objComposer);
            mFormatControl1.SetControlledObject(m_objComposer);
            
            //mMixerBackground1.SetControlledObject(m_objComposer);

            mScenesCombo1.SetControlledObject(m_objComposer);
            SetWriterControlledObject(m_objWriter);
            mFormatControl1.SetControlledObject(m_objLive);
            mConfigList1.SetControlledObject(m_objWriter);
            mConfigList1.OnConfigChanged += new EventHandler(mConfigList1_OnConfigChanged);
            //// mFileSeeking1.SetControlledObject(m_objComposer);          
            //// mRenderersList1.SetSourceObject(m_objComposer);          
            //// mAudioMeter1.SetControlledObject(m_objComposer);
            //// mAudioMeter1.SizeChanged += new EventHandler(mAudioMeter1_SizeChanged);         
            mScenesCombo1.OnActiveSceneChange += new EventHandler(mScenesCombo1_OnActiveSceneChange);
            mMixerList1.OnMixerSelChanged += new EventHandler(mMixerList1_OnMixerSelChanged);
            
            // mPersistControl1.OnLoad += new EventHandler(mPersistControl1_OnLoad);

            string strMax;
            m_objWriter.PropsGet("max_duration", out strMax);
            decimal dMax = 0;
            //if (strMax != null && decimal.TryParse(strMax, out dMax))
            //  numericMaxDuration.Value = dMax;

            //// Fill Senders
            FillSenders((IMSenders)m_objWriter);

            // Update config and enable/disable URL field
            mConfigList1_OnConfigChanged(null, EventArgs.Empty);

            mFormatControl1.comboBoxVideo.SelectedIndex = 0;
            mFormatControl1.comboBoxAudio.SelectedIndex = 0;

            mAttributesList1.ElementDescriptors = MHelpers.MComposerElementDescriptors;
            mElementsTree1.ElementDescriptors = MHelpers.MComposerElementDescriptors;
            m_objComposer.ObjectStart(null);
            MPersistSetControlledObject(m_objComposer);
            bwVideoEditing.DoWork += new System.ComponentModel.DoWorkEventHandler(bwVideoEditing_DoWork);
            bwVideoEditing.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(bwVideoEditing_RunWorkerCompleted);

        }
        void loadScene()
        {
            string filePath = @"E:\Scene.xml";
            //_mPersist.PersistLoad("", filePath, "");
            mMixerList1.LoadItem(null, filePath);

            //mMixerList1.UpdateList(false);
            //mElementsTree1.UpdateTree(false);
        }
        void mPersistControl1_OnLoad(object sender, EventArgs e)
        {
            mMixerList1.UpdateList(false);
            mElementsTree1.UpdateTree(false);
        }
        private void LoadInputFile()
        {
            foreach (var item in lstTemplate)
            {
                string path = System.IO.Path.Combine(item.FilePath);
                mMixerList1.LoadItem(null, path);
            }
            foreach (var item in lstGuestItem)
            {
                if (item.MediaType == 602 || item.MediaType == 607)
                {
                    string path = System.IO.Path.Combine(item.DropVideoPath);
                    mMixerList1.LoadItem(null, path);
                }
                else if (item.MediaType == 601)
                {
                    string path = System.IO.Path.Combine(item.DropVideoPath);
                    mMixerList1.LoadItem(null, path);
                }
            }
        }
        public Object MPersistSetControlledObject(Object pObject)
        {
            Object pOld = (Object)m_pMPersist;
            try
            {
                m_pMPersist = (IMPersist)pObject;
            }
            catch (System.Exception) { }

            return pOld;
        }
        public bool ShowPanHandlerDialog()
        {
            LoadInputFile();
            loadScene();
            Visibility = Visibility.Visible;
            _parent.IsEnabled = false;

            return _result;
        }
        private void HideHandlerDialog()
        {
            if (m_objComposer != null)
                m_objComposer.ObjectClose();
            if (m_objLive != null) m_objLive.ObjectClose();
            if (m_objWriter != null) m_objWriter.ObjectClose();
            if (m_objExtPreview != null) m_objExtPreview.ObjectClose();
            mMixerList1.ClearList();
            m_objLive = null;
            m_objComposer = null;
            m_objWriter = null;
            m_objExtPreview = null;
            
            Visibility = Visibility.Collapsed;
            _parent.IsEnabled = true;
        }
        public void SetParent(UIElement parent)
        {
            _parent = (UIElement)parent;
        }
        private void btnClose_Click(object sender, RoutedEventArgs e)
        {
            HideHandlerDialog();
        }
        public Object SetWriterControlledObject(Object pObject)
        {
            Object pOld = (Object)m_pConfigRoot;
            try
            {
                m_pConfigRoot = (IMConfig)pObject;
            }
            catch (System.Exception) { }
            return pOld;
        }
        void mScenesCombo1_OnActiveSceneChange(object sender, EventArgs e)
        {
            mElementsTree1.UpdateTree(false);
        }
        private void mElementsTree1_AfterSelect(object sender, System.Windows.Forms.TreeViewEventArgs e)
        {
            try
            {
                MElement pElement = (MElement)e.Node.Tag;
                mAttributesList1.SetControlledObject(pElement);
                string strType, strXML;
                pElement.ElementGet(out strType, out strXML);
                bool bDefElement = false;
                foreach (string strDefElement in MHelpers.strDefaultElements)
                {
                    if (strType.Contains(strDefElement))
                    {
                        mPreviewComposerControl.SetEditElement(pElement);
                        bDefElement = true;
                        break;
                    }
                }
                if (!bDefElement)
                {
                    mPreviewComposerControl.SetEditElement(null);
                    // textBoxXML.ReadOnly = true;
                }
                //else
                //{
                //    textBoxXML.ReadOnly = false;
                //}
                //textBoxXML.Text = strXML;
            }
            catch (System.Exception) { }
        }
        void mConfigList1_OnConfigChanged(object sender, EventArgs e)
        {
            // Update config string 
            string strConfig;
            m_objWriter.ConfigGetAll(1, out strConfig);
            comboBoxProps.Text = strConfig;
            // Check if format support network streaming
            string strFormat;
            IMAttributes pFormatConfig;
            m_objWriter.ConfigGet("format", out strFormat, out pFormatConfig);
            int bNetwork = 0;
            try
            {
                pFormatConfig.AttributesBoolGet("network", out bNetwork);
            }
            catch (System.Exception) { }

            //comboBoxURL.Enabled = (bNetwork != 0);
            // The next lines return full XML
            // string strConfigXML;
            // m_objWriter.ConfigGetAll(0, out strConfigXML);
        }
        private void btnStartProcess_Click(object sender, RoutedEventArgs e)
        {
            VidTimer.Start();
            ExtractThumbnail();
            Object pSource = m_objLive;
            Object pSender = null;
            try
            {
                m_objWriter.SendersGet("MComposer", out pSender);
            }
            catch (Exception ex)
            { }
            if (m_objLive != null)
            {
                pSource = pSender;
            } 
            else
                m_objExtPreview.ObjectClose();
            m_objExtPreview.ObjectStart(pSender);
            // mPreviewControl1.SetControlledObject(m_objExtPreview);
            mFormatControl1.SetControlledObjectForMWriter(pSender);

            eMState eState;
            m_objWriter.ObjectStateGet(out eState);
            if (eState == eMState.eMS_Paused)
            {
                // Continue capture
                m_objWriter.ObjectStart(pSource);
            }
            if (eState == eMState.eMS_Running)
            {
                m_objWriter.WriterNameSet("", "");
            }
            else
            {
                string strCapturePath_or_URL = "";
                string strFormat;
                IMAttributes pFormatConfig;
                m_objWriter.ConfigGet("format", out strFormat, out pFormatConfig);
                int bNetwork = 0;
                try
                {
                    pFormatConfig.AttributesBoolGet("network", out bNetwork);
                }
                catch (System.Exception) { }
                int bHave = 0;
                string strExtensions;
                pFormatConfig.AttributesHave("extensions", out bHave, out strExtensions);
                string tempFile = processVideoTemp + "Output." + outputFormat;
                strCapturePath_or_URL = tempFile;
                try
                {
                    //  m_objWriter.WriterNameSet(strCapturePath_or_URL, comboBoxProps.Text != "" ? comboBoxProps.Text : "video::bitrate=1M audio::bitrate=64K");
                    m_objWriter.WriterNameSet(strCapturePath_or_URL, "audio::bitrate=256K video::bitrate=30M video::codec=mpeg4 gop_size=1 qmin=1 qscale=1 v422=true");
                    m_objWriter.ObjectStart(pSource);
                }
                catch (System.Exception ex)
                {
                    MessageBox.Show("Error start capture to '" + strCapturePath_or_URL + "' file." +
                    "\r\nFile Format: " + strFormat + "\r\n\r\n" + ex.Message);
                    return;
                }
            }
        }

        private void btnStopProcess_Click(object sender, RoutedEventArgs e)
        {
            VidTimer.Stop();
            m_objWriter.ObjectClose();
        }

        private void buttonChromaProps_Click(object sender, RoutedEventArgs e)
        {
            if (mMixerList1.SelectedItem != null)
            {
                MCHROMAKEYLib.MChromaKey objChromaKey = GetChromakeyFilter(mMixerList1.SelectedItem);
                // (objChromaKey as IMPersist).PersistLoad("", "D:\\hhhnew.xml", "");
                if (objChromaKey != null)
                {
                    FormChromaKey formCK = new FormChromaKey(objChromaKey);
                    formCK.ShowDialog();
                }
            }
        }
        private MCHROMAKEYLib.MChromaKey GetChromakeyFilter(object source)
        {
            MCHROMAKEYLib.MChromaKey pChromaKey = null;
            try
            {
                int nCount = 0;
                IMPlugins pPlugins = (IMPlugins)source;
                pPlugins.PluginsGetCount(out nCount);
                for (int i = 0; i < nCount; i++)
                {
                    object pPlugin;
                    long nCBCookie;
                    pPlugins.PluginsGetByIndex(i, out pPlugin, out nCBCookie);
                    try
                    {
                        pChromaKey = (MCHROMAKEYLib.MChromaKey)pPlugin;
                        break;
                    }
                    catch { }
                }
            }
            catch { }
            return pChromaKey;
        }
        private void mMixerList1_OnMixerSelChanged(object sender, EventArgs e)
        {
            System.Windows.Forms.ListView listView = (System.Windows.Forms.ListView)sender;
            if (listView.SelectedItems.Count > 0)
            {
                //  mFileSeeking1.Enabled = true;
                ///   mFileSeeking1.SetControlledObject(listView.SelectedItems[0].Tag);

                //  mFileState1.Enabled = true;
                //   mFileState1.SetControlledObject(listView.SelectedItems[0].ToString());

                MCHROMAKEYLib.MChromaKey objChromaKey = GetChromakeyFilter(listView.SelectedItems[0].ToString());
                //  ListView.SelectedListViewItemCollection coll = listView.SelectedItems;
                //   checkBoxCK.CheckedChanged -= new System.EventHandler(this.checkBoxCK_CheckedChanged);
                checkBoxCK.IsChecked = objChromaKey == null ? false : true;
                //    checkBoxCK.Checked += new System.EventHandler(this.checkBoxCK_Checked);

                buttonChromaProps.IsEnabled = checkBoxCK.IsChecked == null ? false : true; ;
            }
            else
            {
                //   mFileSeeking1.Enabled = true;
                //  mFileSeeking1.SetControlledObject(m_objComposer);

                checkBoxCK.IsChecked = false; ;
                buttonChromaProps.IsEnabled = false;

                // mFileState1.Enabled = true;
                // mFileState1.SetControlledObject(m_objComposer);
            }


        }
        private void LoadChromaPlugin(bool onloadChroma)
        {
            if (mMixerList1.SelectedItem != null)
            {
                IMPlugins pPlugins = mMixerList1.SelectedItem as IMPlugins;
                int nCount;
                long nCBCookie;
                object pPlugin = null;
                bool isCK = false;
                pPlugins.PluginsGetCount(out nCount);
                for (int i = 0; i < nCount; i++)
                {
                    pPlugins.PluginsGetByIndex(i, out pPlugin, out nCBCookie);

                    if (pPlugin.GetType().Name == "CoMChromaKeyClass" || pPlugin.GetType().Name == "MChromaKeyClass")
                    {
                        isCK = true;
                        break;
                    }
                }
                if ((checkBoxCK.IsChecked == true && isCK == false) || (onloadChroma && isCK == false))
                {
                    pPlugins.PluginsAdd(new MCHROMAKEYLib.MChromaKey(), 0);
                }
                //else if ((checkBoxCK.Checked == false && isCK == true)|| (!onloadChroma))
                //{
                //    pPlugins.PluginsRemove(pPlugin);
                //}
                buttonChromaProps.IsEnabled = checkBoxCK.IsChecked == null ? false : true;
            }
        }
        private void checkBoxCK_Checked(object sender, RoutedEventArgs e)
        {
            if (checkBoxCK.IsChecked == true)
            {
                LoadChromaPlugin(false);
            }
        }
        private void FillSenders(IMSenders pSenders)
        {
            comboBoxExtSources.Items.Clear();
            comboBoxExtSources.Items.Add("<Live Source>");

            int nCount = 0;
            pSenders.SendersGetCount(out nCount);
            for (int i = 0; i < nCount; i++)
            {
                string strName;
                M_VID_PROPS vidProps;
                M_AUD_PROPS audProps;
                pSenders.SendersGetByIndex(i, out strName, out vidProps, out audProps);

                comboBoxExtSources.Items.Add(strName);
            }

            comboBoxExtSources.SelectedIndex = 0;
        }

        private void btnSaveFile_Click(object sender, RoutedEventArgs e)
        {
            objProcVideoInfo.VideoLength = VideoLength;

            if (IsProcessedVideoEditing)
            {
                if (MessageBox.Show("Click 'Yes' to overwrite or click 'No' to save a new video.", "IMIX", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.No)
                {
                    isCreateNew = true;
                }
            }
            bs.Show();
            bwVideoEditing.RunWorkerAsync();
        }
        private void bwVideoEditing_DoWork(object Sender, System.ComponentModel.DoWorkEventArgs e)
        {
            VideoElements ve = new VideoElements();
            try
            {
                VideoProcessingClass obj = new VideoProcessingClass();
                string outputFile = string.Empty;
                string firstRFID = string.Empty;
                string videoFileTosave = string.Empty;
                string videoThumbnil = string.Empty;
                int processedVideoId = 0;
                string thumbnailPath = LoginUser.DigiFolderThumbnailPath + DateTime.Now.ToString("yyyyMMdd");
                if (!Directory.Exists(thumbnailPath))
                {
                    Directory.CreateDirectory(thumbnailPath);
                }
                //    if ((IsProcessedVideoEditing && isCreateNew) || !IsProcessedVideoEditing)
                {
                    string pathToSave = LoginUser.DigiFolderProcessedVideosPath + DateTime.Now.ToString("yyyyMMdd");
                    if (!Directory.Exists(pathToSave))
                    {
                        Directory.CreateDirectory(pathToSave);
                    }
                    VideoPage objpage = lstGuestItem.Where(o => o.MediaType != 0).OrderBy(o => o.PageNo).FirstOrDefault();
                    if (objpage != null)
                    {

                        outputFile = objpage.Name + "_" + DateTime.Now.ToString("HH:mm").Replace(":", "") + "." + outputFormat;
                        //TO get guest video Id                          
                        firstRFID = objpage.Name;
                        videoFileTosave = pathToSave + "\\" + outputFile;
                        File.Copy(processVideoTemp + "Output." + outputFormat, videoFileTosave);
                        // CopyVideoToDisplayFolder(videoFileTosave);
                        // if (isGroup)
                        {
                            //Save Processed Videos In group
                            LstMyItems _myitem = new LstMyItems();
                            _myitem.MediaType = 3;
                            _myitem.VideoLength = objProcVideoInfo.VideoLength;
                            _myitem.FilePath = thumbnailPath + "\\" + outputFile.Replace(".avi", ".jpg").Replace(".mp4", ".jpg").Replace(".wmv", ".jpg");
                            _myitem.Name = objpage.Name;
                            _myitem.IsPassKeyVisible = Visibility.Collapsed;
                            _myitem.PhotoId = objpage.PhotoId;
                            _myitem.FileName = outputFile;
                            _myitem.CreatedOn = DateTime.Now;
                            _myitem.IsLocked = Visibility.Visible;
                            _myitem.HotFolderPath = LoginUser.DigiFolderPath;
                            if (RobotImageLoader.GroupImages.Count > 0)
                                _myitem.BmpImageGroup = RobotImageLoader.GroupImages[0].BmpImageGroup;
                            //_myitem.BmpImageGroup = new BitmapImage(new Uri(@"/images/view-accept.png", UriKind.Relative));
                            //_myitem.FrameBrdr = ((LstMyItems)(lstImages.SelectedItem)).FrameBrdr;
                            RobotImageLoader.GroupImages.Add(_myitem);
                            RobotImageLoader.ViewGroupedImagesCount.Add(_myitem.Name);
                        }
                    }

                    else// case of live video capture
                    {
                        VideoPage vp = lstGuestItem[0];
                        outputFile = vp.Name + "_" + DateTime.Now.ToString("HH:mm").Replace(":", "") + "." + "avi";
                        firstRFID = vp.Name;
                        videoFileTosave = pathToSave + "\\" + outputFile;
                        File.Copy(processVideoTemp + "Output.avi", videoFileTosave, true);
                        //  obj.ExtractThumbnailFromVideo(videoFileTosave, 4, 4, processVideoTemp + "Output.jpg");
                        //Dispatcher.Invoke(new Action(() =>
                        //                  ThumbnailExtractor.ExtractThumbnailFromVideo(videoFileTosave, 4, 4, processVideoTemp + "Output.jpg", MediaPlayerKey, UserName, EmailId)
                        //                ));
                    }
                    videoThumbnil = thumbnailPath + "\\" + System.IO.Path.GetFileNameWithoutExtension(outputFile) + ".jpg";
                    ve.ResizeWPFImage(processVideoTemp + "Output.jpg", 210, videoThumbnil);
                }
                //else
                //{
                //    videoFileTosave = LoginUser.DigiFolderProcessedVideosPath + "\\" + Convert.ToDateTime(lstProcessedVideo.CreatedOn).ToString("yyyyMMdd") + "\\" + lstProcessedVideo.OutputVideoFileName;
                //    outputFile = lstProcessedVideo.OutputVideoFileName;
                //    //if (File.Exists(videoFileTosave))
                //    //{
                //    //    File.Delete(videoFileTosave);
                //    //}
                //    File.Copy(processVideoTemp + "Output." + outputFormat, videoFileTosave, true);
                //    //Dispatcher.Invoke(new Action(() =>
                //    //                     ThumbnailExtractor.ExtractThumbnailFromVideo(videoFileTosave, 4, 4, processVideoTemp + "Output.jpg", MediaPlayerKey, UserName, EmailId)
                //    //                   ));
                //    videoThumbnil = LoginUser.DigiFolderThumbnailPath + "\\" + Convert.ToDateTime(lstProcessedVideo.CreatedOn).ToString("yyyyMMdd") + "\\" + System.IO.Path.GetFileNameWithoutExtension(outputFile) + ".jpg";
                //    System.GC.Collect();
                //    System.GC.WaitForPendingFinalizers();
                //    File.Delete(videoThumbnil);
                //    ResizeWPFImage(processVideoTemp + "Output.jpg", 210, videoThumbnil);
                //    processedVideoId = ProcessedVideoID;
                //}
                VideoEffects ob = new VideoEffects();

                string effects = ob.GetVideoEffectsXML(objProcVideoInfo, PanPropertyLst, TransitionPropertyLst, borderslotlist, LogoSlotList);
                SaveProcessedVideoDetails(effects, firstRFID, outputFile, processedVideoId);
                //  IsProcessedVideoSaved = true;
            }

            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }

        private void bwVideoEditing_RunWorkerCompleted(object Sender, System.ComponentModel.RunWorkerCompletedEventArgs e)
        {
            try
            {
                bs.Hide();
                MessageBox.Show("\nProcessed video has been saved successfully!", "IMIX", MessageBoxButton.OK, MessageBoxImage.Information);
                HideHandlerDialog();
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }
        private void SaveProcessedVideoDetails(string effects, string firstRFID, string outputFile, int processedVideoId)
        {
            try
            {
                objProcVideoInfo.Effects = effects;
                objProcVideoInfo.CreatedBy = LoginUser.UserId;
                objProcVideoInfo.CreatedOn = System.DateTime.Now;
                objProcVideoInfo.OutputVideoFileName = outputFile;
                objProcVideoInfo.ProcessedVideoId = 0;
                objProcVideoInfo.ProductId = productId;
                objProcVideoInfo.FirstMediaRFID = firstRFID;
                objProcVideoInfo.SubStoreId = ConfigManager.SubStoreId;
                objProcVideoInfo.VideoId = processedVideoId;


                ProcessedVideoBusiness objPVBuss = new ProcessedVideoBusiness();
                List<ProcessedVideoDetailsInfo> lstPVDetails = new List<ProcessedVideoDetailsInfo>();// PrintOrderPageList.ToList();

                //For DropList Items             
                int a = 0;
                foreach (VideoPage item in lstGuestItem.Where(o => o.PhotoId != null && o.PhotoId != 0).OrderBy(o => o.PageNo))
                {
                    ProcessedVideoDetailsInfo objinfo = new ProcessedVideoDetailsInfo();
                    objinfo.MediaType = item.MediaType;
                    objinfo.MediaId = item.PhotoId;
                    objinfo.JoiningOrder = a + 1;
                    objinfo.FrameTime = 0;
                    objinfo.DisplayTime = item.ImageDisplayTime;
                    objinfo.StartTime = item.videoStartTime;
                    objinfo.EndTime = item.videoEndTime;

                    lstPVDetails.Add(objinfo);
                    a++;

                }
                //For Template Items
                List<TemplateListItems> lstCheckedtemplates = lstTemplate.Where(o => o.IsChecked == true).ToList();
                foreach (TemplateListItems objchecked in lstCheckedtemplates)
                {
                    ProcessedVideoDetailsInfo objinfo = new ProcessedVideoDetailsInfo();
                    objinfo.MediaType = objchecked.MediaType;
                    objinfo.MediaId = objchecked.Item_ID;
                    objinfo.JoiningOrder = 0;
                    objinfo.FrameTime = objchecked.InsertTime;
                    objinfo.StartTime = objchecked.StartTime;
                    objinfo.EndTime = objchecked.EndTime;
                    lstPVDetails.Add(objinfo);
                }
                objPVBuss.SaveProcessedVideoAndDetails(objProcVideoInfo, lstPVDetails);
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }
        private void ExtractThumbnail()
        {
            MFrame mf1;
            m_objComposer.ObjectFrameGet(out mf1, "");
            string path_temp = processVideoTemp + "Output.jpg";
            if (File.Exists(path_temp))
            {
                File.Delete(path_temp);
            }
            mf1.FrameVideoSaveToFile(path_temp);
            Marshal.ReleaseComObject(mf1);

        }
        private void VidTimer_Tick(object sender, EventArgs e)
        {
            decimal vidLength = 0;
            txtStatus.Text = UpdateStatus(m_objWriter, out vidLength);

            eMState eState;
            m_objWriter.ObjectStateGet(out eState);
            // if (eState == eMState.eMS_Running)
            // txtStatus.Background = System.Drawing.(256, 256, 256);
            //else if (eState == eMState.eMS_Paused)
            //    txtStatus.Background = Color.FromRgb(222, 30, 30);
            //else
            //    txtStatus.Background = Color.FromRgb(50, 100, 180);
        }
        string UpdateStatus(MWriterClass pCapture, out decimal VidLength)
        {
            VidLength = 0; string strVidlen;
            string strRes = "";
            try
            {
                eMState eState;
                pCapture.ObjectStateGet(out eState);
                string strPid;
                m_objWriter.PropsGet("stat::last::server_pid", out strPid);
                string strSkip;
                pCapture.PropsGet("stat::skip", out strSkip);
                strRes = " State: " + eState + (strSkip != null ? " Skip rest:" + DblStrTrim(strSkip, 3) : "") + " Server PID:" + strPid + "\r\n";

                string sFile;
                pCapture.WriterNameGet(out sFile);
                strRes += " Path: " + sFile + "\r\n";

                {
                    strRes += "TOTAL:\r\n";
                    string strBuffer;
                    pCapture.PropsGet("stat::buffered", out strBuffer);
                    string strFrames;
                    pCapture.PropsGet("stat::frames", out strFrames);

                    string strFPS;
                    pCapture.PropsGet("stat::fps", out strFPS);

                    string strTime;
                    pCapture.PropsGet("stat::video_time", out strTime);

                    string strBreaks = "0";
                    pCapture.PropsGet("stat::breaks", out strBreaks);
                    string strDropped = "0";
                    pCapture.PropsGet("stat::frames_dropped", out strDropped);

                    m_objWriter.PropsGet("stat::video_len", out strVidlen);
                    VidLength = Convert.ToDecimal(strVidlen);
                    string strAVtime;
                    m_objWriter.PropsGet("stat::av_sync_time", out strAVtime);
                    string strAVlen;
                    m_objWriter.PropsGet("stat::av_sync_len", out strAVlen);
                    strRes += "  Buffers:" + strBuffer + " Frames:" + strFrames + " Fps:" + DblStrTrim(strFPS, 3) + " Dropped:" + strDropped +
                    " Breaks:" + strBreaks + "  Video time:" + DblStrTrim(strTime, 3) + "\r\n" + " Video Len:" + strVidlen + " AV Sync time:" +
                    strAVtime + " AV sync len:" + strAVlen + "\r\n";

                    string strSamples;
                    pCapture.PropsGet("stat::samples", out strSamples);
                    pCapture.PropsGet("stat::audio_time", out strTime);

                    strRes += "  Samples:" + strSamples + " Audio time:" + DblStrTrim(strTime, 3) + "\r\n";
                }
                {
                    strRes += "LAST:\r\n";

                    string strFrames;
                    pCapture.PropsGet("stat::last::frames", out strFrames);

                    string strFPS = "";
                    pCapture.PropsGet("stat::last::fps", out strFPS);

                    string strTime = "";
                    pCapture.PropsGet("stat::last::video_time", out strTime);

                    string strBreaks;
                    pCapture.PropsGet("stat::breaks", out strBreaks);

                    strRes += "  Frames:" + strFrames + " Breaks:" + strBreaks +
                        " Video time:" + DblStrTrim(strTime, 3) + "\r\n";

                    string strSamples;
                    pCapture.PropsGet("stat::last::samples", out strSamples);
                    pCapture.PropsGet("stat::last::audio_time", out strTime);

                    strRes += "  Samples:" + strSamples + " Audio time:" + DblStrTrim(strTime, 3);
                }
                return strRes;
            }
            catch (Exception ex)
            {
                return strRes;
            }
        }
        string DblStrTrim(string str, int nPeriod)
        {
            if (str == null)
                return str = "0";
            int nDot = str.LastIndexOf('.');
            int nPeriodHave = nDot >= 0 ? str.Length - nDot - 1 : 0;
            if (nPeriodHave > nPeriod)
            {
                return str.Substring(0, nDot + nPeriod + 1);
            }
            // Add zeroes
            if (nDot < 0)
                str += ".";

            for (int i = nPeriodHave; i < nPeriod; i++)
            {
                str += "0";
            }
            return str;
        }
    }
}
