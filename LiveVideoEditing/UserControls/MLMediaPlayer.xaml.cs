﻿using DigiPhoto.IMIX.Business;
using FrameworkHelper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using MPLATFORMLib;
using MControls;
namespace LiveVideoEditing
{
    /// <summary>
    /// Interaction logic for MediaPlayerControl.xaml
    /// </summary>

    public partial class MLMediaPlayer : UserControl
    {
        MFileClass mFile;
        string vsMediaFileName = "";
        private System.Windows.Threading.DispatcherTimer dispatcherTimer;
        BackgroundWorker bwCopyFiles = new BackgroundWorker();
        Boolean IsMute = false;
        MPreview previewThird = new MPreviewClass();
        int ScreenCount = 1;
        public MLMediaPlayer(string fileName, string type, string ReplayFilePath, bool isReplay, bool isImage)
        {
            InitializeComponent();
            dispatcherTimer = new System.Windows.Threading.DispatcherTimer();
            dispatcherTimer.Tick += DispatcherTimerTick;
            dispatcherTimer.Interval = new TimeSpan(0, 0, 0, 0, 100);
            dispatcherTimer.Start();

            try
            {
                if (mFile == null)
                    mFile = new MFileClass();
            }
            catch (Exception exception)
            {
                return;
            }

            vsMediaFileName = fileName;
            MPreviewControl1.SetControlledObject(mFile);
            MFileSeeking1.SetControlledObject(mFile);
            mFile.FileNameSet(vsMediaFileName, "loop=true");
            if (isImage)
            {
                if (type == "VideoEditor")
                {

                    mpGrid.Height = 405;
                    lowerGrid.Visibility = Visibility.Collapsed;
                }
                return;
            }
            if (isReplay)
            {

                replayFilePath = ReplayFilePath;
            }

        }
        private void CreateSecondaryPreview()
        {
            MPreviewControl1.m_pPreview.PreviewWindowSet("", MPreviewControl1.panelPreview.Handle.ToInt32());
            previewThird.PreviewWindowSet("", MPreviewControl2.panelPreview.Handle.ToInt32());
            previewThird.PreviewEnable("", 1, 1);
            mFile.ObjectStart(null);
            ((IMObject)previewThird).ObjectStart(mFile);
            mFile.FilePlayStart();
            previewThird.PreviewFullScreen("", 1, 1);
        }
        void MyPlaylist_OnEvent(string bsChannelID, string bsEventName, string bsEventParam, object pEventObject)
        {
            UpdateSeekControl();
        }
        private void myPlaylist_OnFrame(string bschannelid, object pmframe)
        {
            MFileSeeking1.UpdatePos();
            Marshal.ReleaseComObject(pmframe);
        }
        void UpdateSeekControl()
        {
            MFileSeeking1.SetControlledObject(mFile);
        }


        public MLMediaPlayer(string fileName, string type)
        {
            InitializeComponent();
            dispatcherTimer = new System.Windows.Threading.DispatcherTimer();
            dispatcherTimer.Tick += DispatcherTimerTick;
            dispatcherTimer.Interval = new TimeSpan(0, 0, 0, 0, 100);
            dispatcherTimer.Start();
            try
            {
                if (mFile == null)
                    mFile = new MFileClass();
            }
            catch (Exception exception)
            {
                return;
            }
            try
            {
                if (string.IsNullOrEmpty(fileName))
                    return;
                vsMediaFileName = fileName;
                MPreviewControl1.SetControlledObject(mFile);
                mFile.FileNameSet(vsMediaFileName, "loop=true");
                if (!string.IsNullOrEmpty(vsMediaFileName))
                {
                    MediaPlay();

                    ScreenCount = System.Windows.Forms.Screen.AllScreens.Count();
                    if (ScreenCount > 1)
                    {
                        CreateSecondaryPreview();
                    }
                }

            }
            catch (Exception ex) { }

        }
        void DispatcherTimerTick(object sender, EventArgs e)
        {
            UpdateState();
        }
        public void UpdateState()
        {
            try
            {
                eMState eState;
                double dblTime;
                mFile.FileStateGet(out eState, out dblTime);
            }
            catch
            { }
        }
        string replayFilePath = string.Empty;

        private void btReplay_Click(object sender, RoutedEventArgs e)
        {

        }

        private void btStop_Click(object sender, RoutedEventArgs e)
        {
            MediaStop();
        }

        public void MediaStop()
        {
            try
            {
                if (mFile != null)
                {
                    mFile.FilePosSet(0, 0);
                    mFile.FilePlayStop(0);
                    mFile.OnFrame -= new IMEvents_OnFrameEventHandler(myPlaylist_OnFrame);
                    mFile.OnEvent -= new IMEvents_OnEventEventHandler(MyPlaylist_OnEvent);
                }
            }
            catch
            { }
        }
        public void MediaClose()
        {
            try
            {
                if (mFile != null)
                {
                    mFile.OnFrame -= new IMEvents_OnFrameEventHandler(myPlaylist_OnFrame);
                    mFile.OnEvent -= new IMEvents_OnEventEventHandler(MyPlaylist_OnEvent);
                    dispatcherTimer.Tick -= DispatcherTimerTick;
                    Marshal.ReleaseComObject(mFile);
                    Dispose();
                }
            }
            catch (Exception ex)
            { }
        }
        private void btStart_Click(object sender, RoutedEventArgs e)
        {
            MediaPlay();
        }

        private void MediaPlay()
        {
            try
            {
                if (mFile != null)
                {
                    mFile.OnFrame += new IMEvents_OnFrameEventHandler(myPlaylist_OnFrame);
                    mFile.OnEvent += new IMEvents_OnEventEventHandler(MyPlaylist_OnEvent);
                    // Check for emty file
                    string strPath;
                    mFile.FileNameGet(out strPath);
                    if (strPath != null)
                    {
                        mFile.FilePlayStart();
                        sldVolume_ValueChanged(null, null);
                    }
                }
            }
            catch
            { }
        }

        private void btPause_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (mFile != null)
                    mFile.FilePlayPause(0);
            }
            catch
            { }
        }
        private void UserControl_Unloaded(object sender, RoutedEventArgs e)
        {
            if (ScreenCount > 1)
                previewThird.PreviewFullScreen("", 0, -1);
            MediaStop();
            mFile.ObjectClose();
            System.Runtime.InteropServices.Marshal.ReleaseComObject(mFile);
        }

        public void Dispose()
        {
            GC.Collect();
        }

        private void btMute_Click(object sender, RoutedEventArgs e)
        {
            IsMute = IsMute ? false : true;
            IMProps m_pProps = (IMProps)mFile;
            if (!IsMute)
            {
                m_pProps.PropsSet("object::audio_gain", "1");
                imgMute.Source = new BitmapImage(new Uri(@"/images/mute.png", UriKind.Relative));
                btMute.ToolTip = "Mute";
            }
            else
            {
                m_pProps.PropsSet("object::audio_gain", "-100");
                imgMute.Source = new BitmapImage(new Uri(@"/images/mute-on.png", UriKind.Relative));
                btMute.ToolTip = "Unmute";
            }
        }
        private void btnFullScreen_Click(object sender, RoutedEventArgs e)
        {
            if (MPreviewControl1.m_pPreview != null)
            {
                MPreviewControl1.m_pPreview.PreviewFullScreen("", 1, -1);
            }
        }
        private void sldVolume_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            double dblPos = (double)sldVolume.Value / sldVolume.Maximum;
            if (MPreviewControl1.m_pPreview != null)
            {
                Thread.Sleep(100);
                MPreviewControl1.m_pPreview.PreviewAudioVolumeSet("", -1, -30 * (1 - dblPos));
            }
        }

    }
}
