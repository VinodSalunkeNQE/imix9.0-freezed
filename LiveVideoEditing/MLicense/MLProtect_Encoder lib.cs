//---------------------------------------------------------------------------
// MLProtect_Encoderlib.cs : Personal protection code for the MediaLooks License system
//---------------------------------------------------------------------------
// Copyright (c) 2012, MediaLooks Soft
// www.medialooks.com (dev@medialooks.com)
//
// Authors: MediaLooks Team
// Version: 3.0.0.0
//
//---------------------------------------------------------------------------
// CONFIDENTIAL INFORMATION
//
// This file is Intellectual Property (IP) of MediaLooks Soft and is
// strictly confidential. You can gain access to this file only if you
// sign a License Agreement and a Non-Disclosure Agreement (NDA) with
// MediaLooks Soft If you had not signed any of these documents, please
// contact <dev@medialooks.com> immediately.
//
//---------------------------------------------------------------------------
// Usage:
//
// 1. Copy MLProxy.dll from the license package to any folder and register it
//    in the system registry by using regsvr32.exe utility
//
// 2. Add the reference to MLProxy.dll in the C# project
//
// 3. Add this file to the project
//
// 4. Call EncoderlibLic.IntializeProtection() method before creating any Medialooks 
//    objects (for e.g. in Form_Load event handler)
//
// 5. Compile the project
//
// IMPORTANT: If your application is running for a period longer than 24 
//            hours, the IntializeProtection() call should be repeated 
//            periodically  (every 24 hours or less).
//
// IMPORTANT: If your have several Medialooks products, don't forget to initialize
//            protection for all of them. For e.g.
//
//             MPlatformLic.IntializeProtection();
//             MDecodersLic.IntializeProtection();
//             etc.

using System;

    public class EncoderlibLic
    {        
        private static MLPROXYLib.CoMLProxyClass m_objMLProxy;
        private static string strLicInfo = @"[MediaLooks]
License.ProductName=Encoder lib
License.IssuedTo=Digiphoto Entertainment Imaging (DIGI PHOTO STUDIO)
License.CompanyID=9935
License.UUID={A22853AE-4178-4613-AF5F-F3AF2F9557BA}
License.Key={D81E2815-FAF3-7B3D-ABD9-67764DF2CA83}
License.Name=MRenderer Module
License.UpdateExpirationDate=June 22, 2017
License.Edition=MWriter
License.AllowedModule=*.*
License.Signature=828DDBDD6FC4619D5F547D588CA6F9D9711C1E9061D9B3B2565AEE5764DEA31AB0729B59414AC8E810BBE03C64C1D260B56466276B07954F5E964AB3CFC162736ED06542E5CA790349459138C408E9E362B98E823316EE203B85E888243D1D5FAD733FF93C0A6F3B794ADEE5FF758322BC136E85A75A6469737643DD7BD32247

[MediaLooks]
License.ProductName=Encoder lib
License.IssuedTo=Digiphoto Entertainment Imaging (DIGI PHOTO STUDIO)
License.CompanyID=9935
License.UUID={825D2A96-6A05-4C75-A699-0906D521B06A}
License.Key={68909C79-A451-A1DE-3500-60E62776C7A2}
License.Name=MWriter Module
License.UpdateExpirationDate=June 22, 2017
License.Edition=MWriter
License.AllowedModule=*.*
License.Signature=0A89F3C277E3C40D03E62B0FA49FC1CE6DD4CFF2F3DEDC9AB5E1590A529BA2AD5E31E94001EEBA00DAB22A5AEFA617114BF0B010F1A911FDB22469E304C58EE51FF560D2C63A5C850959ADF739AC5DBD733F3B10AC79E96C0FC4569417D3AE0AF4ADCD9F9B622DBF586B52163FEDFF8984691E34474A99A504AE1B76FD808A1E

";

		//License initialization
        public static void IntializeProtection()
        {
            if (m_objMLProxy == null)
            {
                // Create MLProxy object 
                m_objMLProxy = new MLPROXYLib.CoMLProxyClass();
                m_objMLProxy.PutString(strLicInfo);                
            }
           UpdatePersonalProtection();
        }

        private static void UpdatePersonalProtection()
        {
            ////////////////////////////////////////////////////////////////////////
            // MediaLooks License secret key
            // Issued to: Digiphoto Entertainment Imaging (DIGI PHOTO STUDIO)
            const long _Q1_ = 62370083;
            const long _P1_ = 53855743;
            const long _Q2_ = 52173661;
            const long _P2_ = 55443887;

            try
            {

                int nFirst = 0;
                int nSecond = 0;
                m_objMLProxy.GetData(out nFirst, out  nSecond);

                // Calculate First * Q1 mod P1
                long llFirst = (long)nFirst * _Q1_ % _P1_;
                // Calculate Second * Q2 mod P2
                long llSecond = (long)nSecond * _Q2_ % _P2_;

                uint uRes = SummBits((uint)(llFirst + llSecond));

                // Calculate check value
                long llCheck = (long)(nFirst - 29) * (nFirst - 23) % nSecond;
                // Calculate return value
                int nRand = new Random().Next(0x7FFF);
                int nValue = (int)llCheck + (int)nRand * (uRes > 0 ? 1 : -1);

                m_objMLProxy.SetData(nFirst, nSecond, (int)llCheck, nValue);

            }
            catch (System.Exception) { }

        }

        private static uint SummBits(uint _nValue)
        {
            uint nRes = 0;
            while (_nValue > 0)
            {
                nRes += (_nValue & 1);
                _nValue >>= 1;
            }

            return nRes % 2;
        }
    }