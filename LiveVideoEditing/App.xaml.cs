﻿using FrameworkHelper;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using DigiPhoto.Cache.DataCache;
using System.Runtime.InteropServices;

namespace LiveVideoEditing
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        #region Show Hide Taskbar
        private delegate int LowLevelKeyboardProcDelegate(int nCode, int wParam, ref KBDLLHOOKSTRUCT lParam);
        private struct KBDLLHOOKSTRUCT
        {
            public int vkCode;
            int scanCode;
            public int flags;
            int time;
            int dwExtraInfo;
        }
        [DllImport("kernel32.dll")]
        private static extern IntPtr GetModuleHandle(IntPtr path);
        [DllImport("user32.dll", EntryPoint = "SetWindowsHookExA", CharSet = CharSet.Ansi)]
        private static extern IntPtr SetWindowsHookEx(
            int idHook,
            LowLevelKeyboardProcDelegate lpfn,
            IntPtr hMod,
            int dwThreadId);
        private IntPtr intLLKey;
        const int WH_KEYBOARD_LL = 13;
        [DllImport("user32.dll", EntryPoint = "CallNextHookEx", CharSet = CharSet.Ansi)]
        private static extern int CallNextHookEx(
            int hHook, int nCode,
            int wParam, ref KBDLLHOOKSTRUCT lParam);
        [DllImport("user32.dll")]
        private static extern IntPtr UnhookWindowsHookEx(IntPtr hHook);


        [DllImport("user32", EntryPoint = "SystemParametersInfo", CharSet = CharSet.Auto, SetLastError = true)]
        private static extern int SystemParametersInfo(int uiAction, int uiParam, int pvParam, int fWinIni);

        private const Int32 SPI_SETSCREENSAVETIMEOUT = 15;
        #endregion Show Hide Taskbar
        private void Application_Startup(object sender, StartupEventArgs e)
        {
            //DataCacheFactory.Register();
            try
            {
                DataCacheFactory.Register();
                // Get Reference to the current Process
                Process thisProc = Process.GetCurrentProcess();
                // Check how many total processes have the same name as the current one
                if (Process.GetProcessesByName(thisProc.ProcessName).Length > 1)
                {
                    // If there is more than one, then it is already running.
                    MessageBox.Show("LiveVideoEditing is already running.");
                    Application.Current.Shutdown();
                    return;
                }
              //  Taskbar.Show();
                StartupUri = new System.Uri("MLLiveCapture.xaml", UriKind.Relative);
#if !DEBUG
                Taskbar.Hide();
                EnableDisableTaskManager(false);
                KeyboardHook(this, e);
#endif
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogFileWrite("Error Message: " + ex.Message + " Error Stack Trace: " + ex.StackTrace);
            }
        }
        #region Common Methods For Locking
        private static void EnableDisableTaskManager(bool enable)
        {
            Microsoft.Win32.RegistryKey HKCU = Microsoft.Win32.Registry.CurrentUser;
            Microsoft.Win32.RegistryKey key = HKCU.CreateSubKey(@"Software\Microsoft\Windows\CurrentVersion\Policies\System");
            key.SetValue("DisableTaskMgr", enable ? 0 : 1, Microsoft.Win32.RegistryValueKind.DWord);
        }

        private int LowLevelKeyboardProc(int nCode, int wParam, ref KBDLLHOOKSTRUCT lParam)
        {
            bool blnEat = false;
            switch (wParam)
            {
                case 256:
                case 257:
                case 260:
                case 261:
                    //Alt+Tab, Alt+Esc, Ctrl+Esc, Windows Key
                    if (((lParam.vkCode == 9) && (lParam.flags == 32)) ||
                    ((lParam.vkCode == 27) && (lParam.flags == 32)) || ((lParam.vkCode ==
                    27) && (lParam.flags == 0)) || ((lParam.vkCode == 91) && (lParam.flags
                    == 1)) || ((lParam.vkCode == 92) && (lParam.flags == 1)) || ((true) &&
                    (lParam.flags == 32)))
                    {
                        blnEat = true;
                    }
                    break;
            }
            try
            {
                if (blnEat)
                    return 1;
                else
                {
                    try
                    {
                        int Hook = CallNextHookEx(0, nCode, wParam, ref lParam);
                        return Hook;
                    }
                    catch (Exception ex)
                    {
                        string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                        ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                        return 1;
                    }
                    return 1;
                }
            }
            catch (Exception ex)
            {
                return 1;
            }

        }
        static LowLevelKeyboardProcDelegate mHookProc;
        private void KeyboardHook(object sender, EventArgs e)
        {
            // NOTE: ensure delegate can't be garbage collected
            mHookProc = new LowLevelKeyboardProcDelegate(LowLevelKeyboardProc);
            // Get handle to main .exe
            IntPtr hModule = GetModuleHandle(IntPtr.Zero);
            // Hook
            intLLKey = SetWindowsHookEx(WH_KEYBOARD_LL, mHookProc, hModule, 0);
            if (intLLKey == IntPtr.Zero)
            {
                MessageBox.Show("Failed to set hook,error = " + Marshal.GetLastWin32Error().ToString());

            }

        }

        private void ReleaseKeyboardHook()
        {
            intLLKey = UnhookWindowsHookEx(intLLKey);
        }
        #endregion
    }
}
