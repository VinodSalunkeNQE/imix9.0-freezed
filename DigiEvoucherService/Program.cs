﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;

namespace DigiEvoucherService
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main()
        {
            ServiceBase[] ServicesToRun;
            ServicesToRun = new ServiceBase[]
            {
                new EvoucherService()
            };
            ServiceBase.Run(ServicesToRun);
            ////for debug
            // EvoucherService evsrv = new EvoucherService();
            // evsrv.EvoucherPullFromIMS();

        }
    }
}
