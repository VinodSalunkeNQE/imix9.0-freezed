﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using DigiPhoto.DataLayer;
using DigiPhoto.IMIX.Business;
using DigiPhoto.IMIX.Model;
using DigiPhoto.Cache;
using FrameworkHelper;
using DigiPhoto.Cache.DataCache;
using DigiPhoto.DataLayer.Model;

namespace DigiEvoucherService
{
    public partial class EvoucherService : ServiceBase
    {
        static System.Timers.Timer timer;
        string Venue;
        //IMSEvoucherService.EVoucherServiceClient serviceNew;
        IMSEvoucherManager.EVoucherServiceClient serviceNew;
        EvoucherManager ObjEvoucher;
        IFormatProvider culture = new System.Globalization.CultureInfo("en-US", true);

        public EvoucherService()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            try
            {

                ServicePosInfoBusiness svcPosinfoBusiness = new ServicePosInfoBusiness();
                string ret = svcPosinfoBusiness.ServiceStart(false);
                //ret = 1;

                if (string.IsNullOrEmpty(ret))
                {
                    DataCacheFactory.Register();
                    //GetSubStoreId();
                    StartScheduler();
                    //GetConfigData();
                }
                else
                {
                    throw new Exception("Already Started");

                }


                //ServicePosInfoBusiness svcPosinfoBusiness = new ServicePosInfoBusiness();
                //svcPosinfoBusiness.ServiceStart(false);

            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                ExitCode = 13816;
                this.OnStop();
            }

        }



        public void StartScheduler()
        {
            try
            {

                timer = new System.Timers.Timer();
                timer.Interval = new TimeSpan(0, 0, 10).TotalMilliseconds;
                timer.AutoReset = true;
                timer.Elapsed += new System.Timers.ElapsedEventHandler(this.timer_Elapsed);
                timer.Start();
                timer.Enabled = true;
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogFileWrite(ex.Message);
            }
        }
        private void timer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            try
            {
                timer.Stop();
                timer.Enabled = false;
                EvoucherPullFromIMS();
                //ErrorHandler.ErrorHandler.LogFileWrite("Timer hits at " + ' ' + DateTime.Now.ToString());

                timer.Start();
                timer.Enabled = true;

            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                timer.Start();
            }
            finally
            {
                timer.Start();
            }
        }
        protected override void OnStop()
        {
            ServicePosInfoBusiness svcPosinfoBusiness = new ServicePosInfoBusiness();
            svcPosinfoBusiness.StopService(false);
        }
        public void EvoucherPullFromIMS()
        {

            ObjEvoucher = new EvoucherManager();
            ConfigBusiness conBiz = new ConfigBusiness();
            bool IsEvoucherActive = false;
            try
            {
                var EvoucherMst = (ConfigManager.IMIXConfigurations.ToDictionary(k => k.Key, k => k.Value).Where(k => k.Key.Equals((int)conBiz.GETEvoucherID()))).ToList();
                IsEvoucherActive = Convert.ToBoolean(EvoucherMst[0].Value);
            }
            catch (Exception e) { ErrorHandler.ErrorHandler.LogFileWrite(Environment.NewLine + " Evoucher Service -UpdateBarcodeNofOFUsers-UpdateStatus :" + e.Message + " at  " + ' ' + DateTime.Now.ToString()); }

            if (IsEvoucherActive)
            {

                serviceNew = new IMSEvoucherManager.EVoucherServiceClient();

                Venue = conBiz.GETVenue();
                //Venue = "ATLANTIS LATIKA 7011";
                UpdateBarcodeNofOFUsers();
                EVoucherMstData();

                EVoucherBarcodes();
                ProductDiscountDetails();
                EVoucherTourList();




            }
        }
        public void UpdateBarcodeNofOFUsers()
        {
            try
            {
                EvoucherManager EvMs = new EvoucherManager();
                List<EVoucherBarcodes> lstBrlist = EvMs.UpdEvoucherBarcodeNofUsedList();
                if (lstBrlist.Count > 0)
                {

                    // serviceNew.UPD_EVBarcodeNofUsed()
                    IMSEvoucherManager.UpdateEVBarcodeNofUsed listc = new IMSEvoucherManager.UpdateEVBarcodeNofUsed();

                    List<IMSEvoucherManager.UpdateEVBarcodeNofUsed> lstProd = lstBrlist.AsEnumerable().Select(x => new IMSEvoucherManager.UpdateEVBarcodeNofUsed
                    {
                        EVoucherBarcodeID = x.EVoucherBarcodeID,
                        EVoucherID = x.EVoucherID,
                        NoofTimeUsed = x.NoofTimeUsed,
                        OrderIDs = x.OrderNos
                    }).ToList();
                    try
                    {
                        var UpdStatus = serviceNew.UPD_BarcodeNofUsedList(lstProd.ToArray(), Venue);
                    }
                    catch (Exception e) { ErrorHandler.ErrorHandler.LogFileWrite(Environment.NewLine + " Evoucher Service -UpdateBarcodeNofOFUsers-UpdateStatus :" + e.Message + " at  " + ' ' + DateTime.Now.ToString()); }
                }
            }
            catch (Exception e) { ErrorHandler.ErrorHandler.LogFileWrite(Environment.NewLine + " Evoucher Service -UpdateBarcodeNofOFUsers outer block :" + e.Message + " at  " + ' ' + DateTime.Now.ToString()); }

        }
        public void EVoucherMstData()
        {
            try
            {
                var p1 = serviceNew.GET_EVouchersMastersData(Venue).ToList();
                //var p2 = serviceNew.GET_EVoucherStatus("KidZania NCR").ToList();
                //var p3 = serviceNew.GET_EVoucherProductDiscountDetails("KidZania NCR").ToList();
                if (p1.Count > 0)
                {
                    DataTable dtEvoucherMaster = new DataTable();

                    dtEvoucherMaster.Columns.Add("EVoucherID");
                    dtEvoucherMaster.Columns.Add("CampTourID");
                    dtEvoucherMaster.Columns.Add("NoofVouchers");
                    dtEvoucherMaster.Columns.Add("CountryID");
                    dtEvoucherMaster.Columns.Add("VoucherValidity");
                    dtEvoucherMaster.Columns.Add("StartDate");
                    dtEvoucherMaster.Columns.Add("EndDate");
                    dtEvoucherMaster.Columns.Add("CreatedBy");
                    dtEvoucherMaster.Columns.Add("CreatedDate");
                    dtEvoucherMaster.Columns.Add("Discount");
                    dtEvoucherMaster.Columns.Add("UsageLimit");
                    dtEvoucherMaster.Columns.Add("isActive");
                    dtEvoucherMaster.Columns.Add("IsMoved");
                    dtEvoucherMaster.Columns.Add("ApprovedStatus");
                    dtEvoucherMaster.Columns.Add("ApprovedBy");
                    dtEvoucherMaster.Columns.Add("ApprovedDt");
                    dtEvoucherMaster.Columns.Add("IsAllVenue");
                    dtEvoucherMaster.Columns.Add("IsAllProduct");


                    //dtEvoucherMaster.Rows.Add((p1.ToArray().OrderByDescending(p => p.EVoucherCreatedDate)));
                    //sourceDate.ToString("yyyy-MM-dd")
                    //dtEvoucherMaster.Rows.Add(query.ToArray().OrderByDescending(p => p.EVoucherCreatedDate).ToArray());
                    DataRow row;
                    foreach (var oMasterdt in p1)
                    {
                        row = dtEvoucherMaster.NewRow();
                        row["EVoucherID"] = oMasterdt.EVoucherID;
                        row["CampTourID"] = oMasterdt.CampTourID;
                        row["NoofVouchers"] = oMasterdt.NoofVouchers;
                        row["CountryID"] = oMasterdt.CountryID;
                        row["VoucherValidity"] = oMasterdt.VoucherValidity;
                        row["StartDate"] = Convert.ToDateTime(oMasterdt.StartDate).ToString("yyyy-MM-dd");
                        row["EndDate"] = Convert.ToDateTime(oMasterdt.EndDate).ToString("yyyy-MM-dd");
                        row["CreatedBy"] = oMasterdt.CreatedBy;
                        row["CreatedDate"] = Convert.ToDateTime(oMasterdt.CreatedDate).ToString("yyyy-MM-dd");
                        row["Discount"] = oMasterdt.Discount;
                        row["UsageLimit"] = oMasterdt.UsageLimit;
                        row["isActive"] = oMasterdt.isActive;
                        row["IsMoved"] = oMasterdt.IsMoved;
                        row["ApprovedStatus"] = oMasterdt.ApprovedStatus;
                        row["ApprovedBy"] = oMasterdt.ApprovedBy;
                        row["ApprovedDt"] = Convert.ToDateTime(oMasterdt.ApprovedDt).ToString("yyyy-MM-dd");
                        row["IsAllVenue"] = oMasterdt.IsAllVenue;
                        row["IsAllProduct"] = oMasterdt.IsAllProduct;

                        dtEvoucherMaster.Rows.Add(row);
                    }
                    bool rtnval = ObjEvoucher.SaveEVoucherMstData(dtEvoucherMaster);
                    if (rtnval)
                    {
                        IMSEvoucherManager.EvoucherLst2 listc = new IMSEvoucherManager.EvoucherLst2();

                        List<IMSEvoucherManager.EvoucherLst2> Evchlist = dtEvoucherMaster.AsEnumerable().Select(x => new IMSEvoucherManager.EvoucherLst2
                        {
                            EvoucherID = Convert.ToInt64((x["EVoucherID"]))
                        }).ToList();


                        try
                        {
                            var p2 = serviceNew.UPD_EVoucherMstDataStatusByList(Evchlist.ToArray(), Venue);
                        }
                        catch (Exception e) { ErrorHandler.ErrorHandler.LogFileWrite(Environment.NewLine + " Evoucher Service -EVoucherMstData :" + e.Message + " at  " + ' ' + DateTime.Now.ToString()); }
                    }
                }
            }
            catch (Exception e) { ErrorHandler.ErrorHandler.LogFileWrite(Environment.NewLine + " Evoucher Service -EVoucherMstData :" + e.Message + " at  " + ' ' + DateTime.Now.ToString()); }
        }
        public void ProductDiscountDetails()
        {
            try
            {
                var p3 = serviceNew.GET_EVoucherProductDetails(Venue).ToList();
                if (p3.Count > 0)
                {
                    DataTable dtDetails = new DataTable();
                    dtDetails.Columns.Add("EVoucherProductID");
                    dtDetails.Columns.Add("EVoucherID");
                    dtDetails.Columns.Add("VenueID");
                    dtDetails.Columns.Add("ProductName");
                    dtDetails.Columns.Add("Code");
                    dtDetails.Columns.Add("ProductID");
                    dtDetails.Columns.Add("IsMoved");


                    //dtDetails.Rows.Add((p1.ToArray().OrderByDescending(p => p.EVoucherCreatedDate)));

                    //dtDetails.Rows.Add(query.ToArray().OrderByDescending(p => p.EVoucherCreatedDate).ToArray());
                    DataRow row;
                    foreach (var oMasterdt in p3)
                    {
                        row = dtDetails.NewRow();


                        row["EVoucherProductID"] = oMasterdt.ID;
                        row["EVoucherID"] = oMasterdt.EVoucherID;
                        row["VenueID"] = oMasterdt.VenueID;
                        row["ProductName"] = oMasterdt.ProductName;
                        row["Code"] = oMasterdt.ProductCode;
                        row["ProductID"] = oMasterdt.ProductID;
                        row["IsMoved"] = oMasterdt.IsMoved;
                        dtDetails.Rows.Add(row);
                    }
                    bool returnval = ObjEvoucher.SaveEVoucherDetails(dtDetails);
                    if (returnval)
                    {
                        IMSEvoucherManager.EvoucherProductDetails listc = new IMSEvoucherManager.EvoucherProductDetails();

                        List<IMSEvoucherManager.EvoucherProductDetails> lstProd = dtDetails.AsEnumerable().Select(x => new IMSEvoucherManager.EvoucherProductDetails
                        {
                            ID = Convert.ToInt64((x["EVoucherProductID"]))
                        }).ToList();
                        try
                        {
                            var UpdStatus = serviceNew.UPD_EvoucherProductDetailsByList(lstProd.ToArray(), Venue);
                        }
                        catch (Exception e) { ErrorHandler.ErrorHandler.LogFileWrite(Environment.NewLine + " Evoucher Service-ProductDiscountDetails : " + e.Message + " at  " + ' ' + DateTime.Now.ToString()); }
                    }
                }
            }
            catch (Exception e) { ErrorHandler.ErrorHandler.LogFileWrite(Environment.NewLine + " Evoucher Service- ProductDiscountDetails : " + e.Message + " at  " + ' ' + DateTime.Now.ToString()); }
        }

        public void EVoucherBarcodes()
        {
            try
            {
                DataTable dtDetails = new DataTable();
                var p3 = serviceNew.GET_EVoucherBarcodeList_New(Venue).ToList(); ///("KidZania NCR").ToList();

                if (p3.Count > 0)
                {
                    //dtDetails.Columns.Add("EVoucherProductID");
                    dtDetails.Columns.Add("EVoucherBarcodeID");
                    dtDetails.Columns.Add("EVoucherID");
                    dtDetails.Columns.Add("Barcode");
                    dtDetails.Columns.Add("Redeem");
                    dtDetails.Columns.Add("IsMoved");
                    dtDetails.Columns.Add("NoofTimeUsed");
                    dtDetails.Columns.Add("OrderIDs");


                    //dtDetails.Rows.Add((p1.ToArray().OrderByDescending(p => p.EVoucherCreatedDate)));

                    //dtDetails.Rows.Add(query.ToArray().OrderByDescending(p => p.EVoucherCreatedDate).ToArray());
                    DataRow row;
                    foreach (var oMasterdt in p3)
                    {
                        row = dtDetails.NewRow();
                        row["EVoucherBarcodeID"] = oMasterdt.EVoucherBarcodeID;
                        row["EVoucherID"] = oMasterdt.EVoucherID;
                        row["Barcode"] = oMasterdt.Barcode;
                        row["IsMoved"] = oMasterdt.IsMoved;
                        row["NoofTimeUsed"] = oMasterdt.NoofTimeUsed;
                        row["OrderIDs"] = oMasterdt.OrderIDs;
                        dtDetails.Rows.Add(row);
                    }
                    bool returnval = ObjEvoucher.SaveEVoucherBarcodes(dtDetails);
                    if (returnval)
                    {
                        IMSEvoucherManager.EVoucherBarcodeslst listc = new IMSEvoucherManager.EVoucherBarcodeslst();

                        List<IMSEvoucherManager.EVoucherBarcodeslst> lstProd = dtDetails.AsEnumerable().Select(x => new IMSEvoucherManager.EVoucherBarcodeslst
                        {
                            EVoucherBarcodeID = Convert.ToInt64((x["EVoucherBarcodeID"]))
                        }).ToList();
                        try
                        {
                            var UpdStatus = serviceNew.UPD_EVoucherBarcodeslstByList(lstProd.ToArray(), Venue);
                        }
                        catch (Exception e) { ErrorHandler.ErrorHandler.LogFileWrite(Environment.NewLine + " Evoucher Service -EVoucherBarcodes :" + e.Message + " at  " + ' ' + DateTime.Now.ToString()); }

                    }

                }
            }
            catch (Exception e) { ErrorHandler.ErrorHandler.LogFileWrite(Environment.NewLine + " Evoucher Service-EVoucherBarcodes :" + e.Message + " at  " + ' ' + DateTime.Now.ToString()); }
        }
        public void EVoucherTourList()
        {
            try
            {
                DataTable dtDetails = new DataTable();
                var p3 = serviceNew.GET_TourOperatorMaster(Venue).ToList();

                if (p3.Count > 0)
                {
                    //dtDetails.Columns.Add("EVoucherProductID");
                    dtDetails.Columns.Add("TourID");
                    dtDetails.Columns.Add("TourOperatorName");
                    dtDetails.Columns.Add("Country");
                    dtDetails.Columns.Add("IsActive");
                    dtDetails.Columns.Add("TypeID");
                    dtDetails.Columns.Add("IsMoved");


                    //dtDetails.Rows.Add((p1.ToArray().OrderByDescending(p => p.EVoucherCreatedDate)));

                    //dtDetails.Rows.Add(query.ToArray().OrderByDescending(p => p.EVoucherCreatedDate).ToArray());
                    DataRow row;
                    foreach (var oMasterdt in p3)
                    {
                        row = dtDetails.NewRow();
                        row["TourID"] = oMasterdt.TourID;
                        row["TourOperatorName"] = oMasterdt.TourOperatorName;
                        row["Country"] = oMasterdt.CountryName;
                        row["IsActive"] = oMasterdt.IsActive;
                        row["TypeID"] = oMasterdt.TypeID;
                        row["IsMoved"] = oMasterdt.IsMoved;
                        dtDetails.Rows.Add(row);
                    }
                    bool returnval = ObjEvoucher.SaveTourOperatorMaster(dtDetails);
                    if (returnval)
                    {
                        IMSEvoucherManager.TourIDList listc = new IMSEvoucherManager.TourIDList();

                        List<IMSEvoucherManager.TourIDList> lstProd = dtDetails.AsEnumerable().Select(x => new IMSEvoucherManager.TourIDList
                        {
                            TourID = Convert.ToInt64((x["TourID"]))
                        }).ToList();
                        try
                        {
                            var UpdStatus = serviceNew.UPD_TourList(lstProd.ToArray(), Venue);
                        }
                        catch (Exception e) { ErrorHandler.ErrorHandler.LogFileWrite(Environment.NewLine + " Evoucher Service -EVoucherTourList :" + e.Message + " at  " + ' ' + DateTime.Now.ToString()); }

                    }

                }
            }
            catch (Exception e) { ErrorHandler.ErrorHandler.LogFileWrite(Environment.NewLine + " Evoucher Service-EVoucherTourList :" + e.Message + " at  " + ' ' + DateTime.Now.ToString()); }
        }
    }

}
