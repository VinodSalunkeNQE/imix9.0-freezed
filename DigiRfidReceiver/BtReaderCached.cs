﻿using Baracoda.Cameleon.PC.Modularity.Models;

namespace DigiRfidService
{
    /// <summary>
    /// Data container for storing Bluetooth reader information
    /// </summary>
    public class BtReaderCached : DataModel
    {
        /// <summary>
        /// Reader ID
        /// </summary>
        public string Id
        {
            get { return id; }
            set
            {
                if (string.Equals(id, value)) return;
                id = value;
                SendPropertyChanged(()=>Id);
            }
        }

        /// <summary>
        /// Reader name
        /// </summary>
        public string Name
        {
            get { return name; }
            set
            {
                if (string.Equals(name, value)) return;
                name = value;
                SendPropertyChanged(()=>Name);
            }
        }

        private string id;
        private string name;
    }
}
