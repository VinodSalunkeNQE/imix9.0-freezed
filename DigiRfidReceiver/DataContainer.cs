﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DigiRfidService
{
    public class DataContainer
    {
        /// <summary>
        /// The identifier of the device from which the data is incoming
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// The RFID/Tag Id
        /// </summary>
        public string TagId { get; set; }

        /// <summary>
        /// The time the data came
        /// </summary>
        public DateTime Time { get; set; }

        /// <summary>
        /// The content of the data
        /// </summary>
        public string Content { get; set; }
    }
}
