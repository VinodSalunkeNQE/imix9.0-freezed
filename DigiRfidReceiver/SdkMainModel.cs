﻿using Baracoda.Cameleon.PC.Common;
using Baracoda.Cameleon.PC.Modularity.DataStructures;
using Baracoda.Cameleon.PC.Modularity.GuiFeatures;
using Baracoda.Cameleon.PC.Modularity.ViewModels;
using Baracoda.Cameleon.PC.Readers;
using Baracoda.Cameleon.PC.Readers.DataParsers;
//using Baracoda.Sdk.Full.Wpf.Common.Data;
//using Baracoda.Sdk.Full.Wpf.Common.Model;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Windows.Input;
using System.Linq;
using Baracoda.Cameleon.PC.Common.Operations;
using FrameworkHelper.RfidLib;

namespace DigiRfidService
{
    /// <summary>
    /// This example shows how to perform the following tasks:
    /// - search for / connect to / disconnect from Bluetooth readers
    /// - listen for incoming data (barcodes / RFID tag IDs)
    /// - list the data
    /// </summary>
    public class SdkMainModel : SdkModelBase
    {
        /// <summary>
        /// Fired when Bluetooth inquiry is complete
        /// </summary>
        public event EventHandler<ReadersAddedEventArgs> InquiryComplete;

        public event EventHandler<DataEventArgs> DataReceived;

        public int DataReceivedEventCount = 0;
        /// <summary>
        /// Creates new instance of this class
        /// </summary>
        public SdkMainModel()
        {
            PinCode = "0000";
            InitBt();
        }

        /// <summary>
        /// The pin code for readers
        /// </summary>
        public string PinCode { get; set; }


        /// <summary>
        /// The selected Bluetooth reader
        /// </summary>
        public BaracodaBtReader Selected
        {
            get { return selected; }
            set
            {
                if (selected == value) return;
                if (selected != null)
                    selected.ConnectionStateChanged -= OnConnectionChanged;
                selected = value;
                if (selected != null)
                {
                    selected.ConnectionStateChanged += OnConnectionChanged;
                    selected.IsManualAckRequired = true;
                }
                IsSelectedConnected = Selected != null && Selected.IsConnected;
                SendPropertyChanged(() => Selected);
            }
        }

        public bool IsSelectedConnected
        {
            get { return isSelectedConnected; }
            private set
            {
                if (isSelectedConnected == value) return;
                isSelectedConnected = value;
                SendPropertyChanged(() => IsSelectedConnected);
            }
        }


        void InitBt()
        {
            IsBusy = true;
            // start Bluetooth initialization
            BtMan.CreateAndInitialize(OnBtInit);
        }

        void OnBtInit()
        {
            IsBtAvailable = BtMan.Instance.IsBluetoothAvailable;
            IsBusy = false;
        }

        internal void LookForReaders()
        {
            if (!BtMan.Instance.IsBluetoothAvailable) return;
            if (IsBusy == true)
            return;
            IsBusy = true;
            // start inquiry on the separate thread
            ThreadPool.QueueUserWorkItem(x => StartInquiry());
        }

        void StartInquiry()
        {
            var btManager = BtMan.Instance;
            try
            {
                if (!btManager.IsBluetoothAvailable)
                {
                    //SendMessage("Cannot start inquiry when Bluetooth is not available");
                    string errorMessage = "Cannot start inquiry when Bluetooth is not available";
                    ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                    return;
                }

                queue.Clear();
                btManager.BtInquiry.DeviceInquiryFinished += OnInquiryFinished;
                try
                {
                    // use Baracoda class of device (0x000500)
                    btManager.BtInquiry.StartDeviceInquiry(true, new byte[] { 0x00, 0x05, 0x00 });
                }
                catch (BaracodaException x)
                {
                    btManager.BtInquiry.DeviceInquiryFinished -= OnInquiryFinished;
                    string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(x);
                    ErrorHandler.ErrorHandler.LogFileWrite("StartInquiry()" + errorMessage);
                    IsBusy = false;

                }
            }
            catch (Exception ex)
            {
                btManager.BtInquiry.DeviceInquiryFinished -= OnInquiryFinished;
                string errorMessage = "Another enquiry process is going on. ";//ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                if (IsBusy == false)
                    IsBusy = true;
                //string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                //ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }

        void OnInquiryFinished(object sender, DeviceInquiryEventArgs e)
        {
            try
            {
                var btManager = BtMan.Instance;

                // at least one device responded
                if (e.State == InquiryResult.DeviceResponded)
                {
                    // go through each of the devices
                    for (var i = 0; i < e.Addresses.Count; i++)
                    {
                        var id = e.Addresses[i].ToString();
                        var cached = new BtReaderCached
                        {
                            Id = id,
                            Name = string.IsNullOrEmpty(e.Names[i])
                                        ? id.Substring(id.Length - 5, 5)
                                        : e.Names[i]
                        };

                        // remember Address/Name pair
                        queue.Enqueue(cached);
                    }
                }

                // inquiry is complete, process the stored pairs
                else if (e.State == InquiryResult.Complete)
                {
                    btManager.BtInquiry.DeviceInquiryFinished -= OnInquiryFinished;

                    var addedReaders = new List<BaracodaBtReader>();
                    foreach (var entry in queue.ToList())
                    {
                        // check if the reader is not already enlisted
                        var hasId = false;
                        foreach (var reader in Readers)
                        {


                            if (string.Equals(reader.Id, entry.Id) && reader.IsConnected)
                            {
                                DataReceivedEventCount = DataReceived.GetInvocationList().Count();
                                hasId = true;
                                //Write log
                                //string errorMessage = DateTime.Now.ToString() + " Device Found: " + reader.Id;
                                //ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                                break;
                            }

                        }

                        if (!hasId)
                        {
                            // we have a new device
                            var btDevice = btManager.BtFactory.CreateBtDevice();

                            btDevice.Name = entry.Name;
                            btDevice.Address = new BtAddress(entry.Id);
                            btDevice.Scn = 1;

                            var btReader = new BaracodaBtReader(btDevice) { Name = btDevice.Name };
                            addedReaders.Add(btReader);

                        }
                    }

                    if (addedReaders.Count > 0)
                    {


                        // cast to BaracodaReaderBase
                        var castReaders = addedReaders.Cast<BaracodaReaderBase>().ToList();

                        // store new readers on the list
                        Readers.AddRange(castReaders);

                        // notify listeners
                        var ev = InquiryComplete;
                        if (ev != null)
                            ev(this, new ReadersAddedEventArgs { AddedReaders = castReaders });
                    }
                    IsBusy = false;

                }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
            finally
            {

            }
        }

        void OnDataReceived(object sender, DataReceivedEventArgs e)
        {
            try
            {
                var device = (BaracodaBtReader)sender;
                if (device == null)
                    return;

                if (e.NeedsAck)
                    device.AckData(true, e.Id);

                var item = new DataContainer
                {
                    Id = device.Retrieved.SerialNumber,// device.Address.ToString()/*device.Retrieved.SerialNumber*/,
                    Time = e.ReceivedTime,
                    Content = e.Text
                };
                //Write log
                string errorMessage = DateTime.Now.ToString() + " Data Received from device " + device.Address.ToString() + " : " + e.Text;
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                // notify listeners
                var ev = DataReceived;
                if (ev != null)
                    ev(this, new DataEventArgs { RfidData = item });

                //OnGui.Run(() => DataList.Add(item));
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }

        /// <summary>
        /// Pair selected device
        /// </summary>
        public void Pair()
        {
            IsBusy = true;
            BtMan.Instance.Pair((BtDevice)Selected.BaseDevice, PinCode, OnPairingResult);
        }

        void OnPairingResult(OperationResult result)
        {
            //SendMessage(string.Format("Pairing status: {0}", result.Status));
            //if (result.Message != null)
            //    SendMessage(result.Message.Content);

            IsBusy = false;
        }


        internal void Connect()
        {
            var reader = Selected;
            if (reader != null)
            {
                reader.DataReceived += OnDataReceived;
                reader.Connect();
                string errorMessage = DateTime.Now.ToString() + " Device Connected: " + reader.Id;
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }

        internal void Disconnect()
        {
            var reader = Selected;
            if (reader != null)
            {
                string errorMessage = DateTime.Now.ToString() + " Device Disconnected: " + reader.Id;
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                reader.Disconnect();
            }
        }

        void OnConnectionChanged(object sender, ConnectionStateEventArgs e)
        {
            string errorMessage = string.Empty;
            switch (e.State)
            {
                case ConnectionState.Connected:
                    IsSelectedConnected = true;
                    IsBusy = false;
                    //Write log
                    // errorMessage = DateTime.Now.ToString() + " Device Connected: " +((BaracodaBtReader)sender).Id;
                    //ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                    break;
                case ConnectionState.Disconnected:
                    IsSelectedConnected = false;
                    IsBusy = false;

                    break;
                case ConnectionState.Connecting:
                    IsSelectedConnected = false;
                    IsBusy = true;
                    break;
            }
        }

        /// <summary>
        /// Forces this model to release resources
        /// </summary>
        public void CloseForced()
        {
            BtMan.Instance.Dispose();
        }

        private bool isSelectedConnected;
        private BaracodaBtReader selected;
        private readonly Queue<BtReaderCached> queue = new Queue<BtReaderCached>();
    }
}
