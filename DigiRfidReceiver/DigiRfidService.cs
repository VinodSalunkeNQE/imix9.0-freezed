﻿using Baracoda.Cameleon.PC.Readers;
using DigiPhoto;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
//using System.Data;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using DigiPhoto.IMIX.Model;
using DigiPhoto.IMIX.Business;
using Baracoda.Cameleon.PC.Common;
using DigiPhoto.DataLayer;
using DigiPhoto.DataLayer.Model;
using FrameworkHelper.RfidLib;
using System.Data;

namespace DigiRfidService
{
    partial class DigiRfidService : ServiceBase
    {
        static System.Timers.Timer timer;
        static int _timerMiliSeconds = 2000;
        private SdkMainModel model;
        List<BaracodaBtReader> readerList = new List<BaracodaBtReader>();
        RfidBusiness rfidBusiness = null;
        bool IsRfidEnabled = false;
        int SubStoreId = 0;
        List<string> deviceList = null;
        DateTime lastRFIDFlushTime = DateTime.Now;
        int TagMinLength = 0;
        public DigiRfidService()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {

            try
            {

                ServicePosInfoBusiness svcPosinfoBusiness = new ServicePosInfoBusiness();
                string ret = svcPosinfoBusiness.ServiceStart(false);

                if (string.IsNullOrEmpty(ret))
                {
                rfidBusiness = new RfidBusiness();
                StartScheduler();
                deviceList = new List<string>();
                GetConfigData();
                GetDeviceList();
                model = new SdkMainModel();
                model.InquiryComplete += OnModelInquiryComplete;
                model.DataReceived += OnDataReceived;
                TagMinLength = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["TagMinLength"]);
                }
                else
                {
                    throw new Exception("Already Started");  
                   
                }


               
              

            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                ExitCode = 13816;
                this.Stop();
            }
        }
///////////////////////////////created by latika for testing the functionality
        public void startFunc()
        {
            try
            {

                ServicePosInfoBusiness svcPosinfoBusiness = new ServicePosInfoBusiness();
                string ret = svcPosinfoBusiness.ServiceStart(false);

                if (string.IsNullOrEmpty(ret))
                {
                    rfidBusiness = new RfidBusiness();
                    StartScheduler();
                    deviceList = new List<string>();
                    GetConfigData();
                    GetDeviceList();
                    model = new SdkMainModel();
                    model.InquiryComplete += OnModelInquiryComplete;
                    model.DataReceived += OnDataReceived;
                    TagMinLength = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["TagMinLength"]);
                    //AssociateRFIDtoPhotosAdvance();
                    AssociateRFIDtoPhotosAdvanceTableWorkFlow();

                }
                else
                {
                    throw new Exception("Already Started");

                }

            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                ExitCode = 13816;
                this.Stop();
            }
        }
/////END
       // string SbStoreId = "2";
        DirectoryInfo _objHotFolder = new DirectoryInfo(System.IO.Path.Combine("D:\\DigiImages", "MobileTags"));
        ///D:\DigiImages\MobileTags
        DirectoryInfo _objSubHotFolder = new DirectoryInfo(System.IO.Path.Combine("D:\\DigiImages", "MobileTags", "Processed"));

        protected override void OnStop()
        {
            try
            {
                timer.Stop();
                timer = null;
                if (model != null)
                {
                    //model.Selected.Disconnect();
                    model.Disconnect();
                    model = null;
                }
                ServicePosInfoBusiness svcPosinfoBusiness = new ServicePosInfoBusiness();
                svcPosinfoBusiness.StopService(false);
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                //timer.Start();
            }
        }
        private void timer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            try
            {
                timer.Stop();
                timer.Enabled = false;
                //Start Search BlueTooth Devices
                //if (IsRfidEnabled)
                //{

                //Flush RFID Tags in every 20 Seconds.
                if (DateTime.Now.Subtract(lastRFIDFlushTime).Minutes > 20)
                {
                    try
                    {
                        rfidBusiness.ArchiveRFIDTags(SubStoreId);
                        lastRFIDFlushTime = DateTime.Now;
                    }
                    catch (Exception ex)
                    {
                        string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                        ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                        SaveEmailDetails(ex.Message);
                    }
                }

                if (model != null)
                {
                    model.LookForReaders();
                }

                //AssociateRFIDtoPhotosAdvance();
                AssociateRFIDtoPhotosAdvanceTableWorkFlow();///changed by latika for table workflow

                //}
                //End Search BlueTooth Devices
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                //timer.Start();
            }
            finally
            {
                timer.Start();
            }
        }

        private bool SaveEmailDetails(string exceptionMsg)
        {
            //DigiPhotoDataServices _objDataLayer = new DigiPhotoDataServices();
            SubStoresInfo _objSubstore = (new StoreSubStoreDataBusniess()).GetSubstoreData(SubStoreId);

            exceptionMsg += ", Sub Store: " + _objSubstore.DG_SubStore_Name;

            //string DGconn = ConfigurationManager.ConnectionStrings["DigiConnectionString"].ConnectionString;
            ConfigBusiness buss = new ConfigBusiness();
            Dictionary<long, string> iMIXConfigurations = new Dictionary<long, string>();
            iMIXConfigurations = buss.GetConfigurations(iMIXConfigurations, SubStoreId);
            string ToAddress = iMIXConfigurations.Where(k => k.Key == (int)ConfigParams.RfidFlushReceiverEmail).Count() > 0 ? iMIXConfigurations[(int)ConfigParams.RfidFlushReceiverEmail] : string.Empty;
            if (!string.IsNullOrEmpty(ToAddress))
            {
                bool success = buss.SaveEmailDetails(ToAddress, null, null, exceptionMsg, "RFID FLUSH");
            }
            return true;
        }
        private void StartScheduler()
        {
            try
            {
                timer = new System.Timers.Timer();
                timer.Interval = _timerMiliSeconds;
                timer.AutoReset = true;
                timer.Elapsed += new System.Timers.ElapsedEventHandler(this.timer_Elapsed);
                timer.Start();
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }

        void OnModelInquiryComplete(object sender, ReadersAddedEventArgs e)
        {
            try
            {
                readerList = e.AddedReaders.Cast<BaracodaBtReader>().ToList();
                if (model != null)
                {
                    foreach (BaracodaBtReader reader in readerList)
                    {
                        if (deviceList.Contains(reader.Retrieved.SerialNumber, StringComparer.OrdinalIgnoreCase))
                        {
                            if (model.Selected == null || model.Selected.Id != reader.Id || model.Selected.IsConnected == false)
                            {
                                model.Selected = reader;// readerList.FirstOrDefault();
                                model.Connect();
                                model.Selected.ConnectionStateChanged += OnSelectedConnectionChanged;
                            }
                        }
                        else
                        {
                            string errorMessage = "Device is not registered :" + reader.Retrieved.SerialNumber;
                            ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }
        void OnSelectedConnectionChanged(object sender, ConnectionStateEventArgs e)
        {
            if (e.State == Baracoda.Cameleon.PC.Common.ConnectionState.Connecting)
                return;
            //if (e.State == Baracoda.Cameleon.PC.Common.ConnectionState.Disconnected)
            //model.Readers.RemoveAll(o => string.Compare(o.Id, ((BaracodaBtReader)sender).BaseDevice.Id , true) == 0);
        }

        void OnDataReceived(object sender, DataEventArgs e)
        {
            if (e.RfidData != null)
                SaveIntoDatabase(e.RfidData);
        }

        void SaveIntoDatabase(DataContainer tagData)
        {
            try
            {
                string tagId = tagData.Content != null && tagData.Content.Split(']').Length > 1 && tagData.Content.Split(']')[1].Length >= TagMinLength ? tagData.Content.Split(']')[1] : string.Empty;
                string Time = tagData.Content != null && tagData.Content.Split('[').Length > 0 ? tagData.Content.Split('[')[0] : string.Empty;
                DateTime? ScannedTime = new DateTime();
                try
                {
                    if (!string.IsNullOrEmpty(Time) && Time.Length >= 12)
                    {
                        System.Globalization.DateTimeFormatInfo objFormat = new System.Globalization.DateTimeFormatInfo();
                        //objFormat.ShortDatePattern = @"dd/MM/yyyy HH:mm:ss";///changed by latika for table workflow
                        ScannedTime = Convert.ToDateTime((Time.Substring(4, 2) + "-" + Time.Substring(2, 2) + "-" + Time.Substring(0, 2) + " " + Time.Substring(6, 2) + ":" + Time.Substring(8, 2) + ":" + Time.Substring(10, 2)), objFormat);
                    }
                    else
                        ScannedTime = null;
                }
                catch (Exception ex)
                {
                    ScannedTime = null;
                    string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                    ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                }
                if (tagId == null)
                    tagId = string.Empty;
                RFIDTagInfo rfidTag = new RFIDTagInfo();
                rfidTag.SerialNo = tagData.Id;
                rfidTag.TagId = tagId;
                rfidTag.RawData = tagData.Content;
				////////created by latika for RFID Service
                rfidTag.ScanningTime =Convert.ToDateTime(ScannedTime);
				rfidTag.ScanningDatetime = ScannedTime.ToString();
				///////end
                //rfidTag.ScanningTime = tagData.Time;
                rfidBusiness.SaveRfidTag(rfidTag);
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }

        void GetSubStoreId()
        {
            //Worker Process
            string pathtosave = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            if (File.Exists(pathtosave + "\\ss.dat"))
            {
                string line;
                string SbStoreId;
                using (StreamReader reader = new StreamReader(pathtosave + "\\ss.dat"))
                {
                    line = reader.ReadLine();
                    SbStoreId = DigiPhoto.CryptorEngine.Decrypt(line, true);
                    SubStoreId = Convert.ToInt32(SbStoreId.Split(',')[0]);
                }
            }
        }
        void GetConfigData()
        {
            try
            {
                GetSubStoreId();
                if (SubStoreId > 0)
                {
                    //DigiPhotoDataServices _objDataLayer = new DigiPhotoDataServices();
                    List<long> filter = new List<long>();
                    filter.Add((long)DigiPhoto.DataLayer.ConfigParams.IsEnabledRFID);
                    //List<iMIXConfigurationValue> _objdata = _objDataLayer.GetNewConfigValues(SubStoreId).Where(o => filter.Contains(o.IMIXConfigurationMasterId)).ToList();
                    //foreach (iMIXConfigurationValue item in _objdata)
                    List<iMIXConfigurationInfo> _objdata = (new ConfigBusiness()).GetNewConfigValues(SubStoreId).Where(o => filter.Contains(o.IMIXConfigurationMasterId)).ToList();
                    foreach (iMIXConfigurationInfo item in _objdata)
                    {
                        switch ((int)item.IMIXConfigurationMasterId)
                        {
                            case (int)ConfigParams.IsEnabledRFID:
                                if (!string.IsNullOrEmpty(item.ConfigurationValue))
                                    IsRfidEnabled = Convert.ToBoolean(item.ConfigurationValue);
                                break;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }

        void GetDeviceList()
        {
            DeviceManager deviceManager = new DeviceManager();
            deviceList = deviceManager.GetDeviceList().Select(o => o.SerialNo).ToList();
        }

        private void AssociateRFIDtoPhotosAdvance()
        {
		//////////////created by latika for Table workflow changes
            AssociateRFIDtoPhotosAdvanceTableWorkFlow();
            //List<RFIDField> lstSubstore = new List<RFIDField>();
            //lstSubstore = GetSubStoreList();
            //DataTable dtlst = new DataTable();
            //string SerialNo = string.Empty;

            //foreach (RFIDField item in lstSubstore)
            //{
            //    string hotfolder = item.HotFolderpath;
            //    DirectoryInfo _objHotFolder = new DirectoryInfo(System.IO.Path.Combine(item.HotFolderpath, "MobileTags"));

            //    if (_objHotFolder.Exists)
            //    {
            //        FileInfo[] fileEntries = _objHotFolder.GetFiles("*.txt");
            //        DirectoryInfo _objSubHotFolder = new DirectoryInfo(System.IO.Path.Combine(item.HotFolderpath, "MobileTags", "Processed"));
            //        if (!_objSubHotFolder.Exists)
            //            _objSubHotFolder.Create();

            //        try
            //        {
            //            foreach (FileInfo filename in fileEntries)
            //            {
            //                if (filename.Name.Contains('_'))
            //                {
            //                    if (filename.Name.Contains(item.SerialNo))
            //                    {
            //                        string file = filename.FullName;//.DirectoryName + "\\" + filename.Name.ToString();
            //                        var pos = file.IndexOf("MobileTags");
            //                        string des = file.Substring(pos + 11, file.Length - pos - 11);
            //                        string txtname = des;
            //                        des = _objSubHotFolder.FullName + "\\" + des;
            //                        var SerialNumber = txtname.Split('_');
            //                        SerialNo = SerialNumber[0];
            //                        List<RFIDTagInfo> rfidTagGlobal = new List<RFIDTagInfo>();
            //                        using (StreamReader r = new StreamReader(file))
            //                        {
            //                            string line;
            //                            while ((line = r.ReadLine()) != null)
            //                            {
            //                                if (!String.IsNullOrWhiteSpace(line))
            //                                {
            //                                    string[] result = line.Split(',');
            //                                    // YYYYMMDDHHMMSS 20160329171041
            //                                    string dateTime = result[1];
            //                                    string year = dateTime.Substring(0, 4);
            //                                    string month = dateTime.Substring(4, 2);
            //                                    string day = dateTime.Substring(6, 2);
            //                                    string hour = dateTime.Substring(8, 2);
            //                                    string min = dateTime.Substring(10, 2);
            //                                    string sec = dateTime.Substring(12, 2);
            //                                    string spantime = year + "/" + month + "/" + day + " " + hour + ":" + min + ":" + sec;
            //                                    rfidTagGlobal.Add(new RFIDTagInfo { TagId = result[0], ScanningTime = Convert.ToDateTime(spantime) });
            //                                    result = null;
            //                                }
            //                            }
            //                        }
            //                        // Convert list to DataTable.
            //                        dtlst = ListToDataTable(rfidTagGlobal);
            //                        SaveTable(dtlst, SerialNo);
            //                        File.Move(file, des);
            //                    }
            //                }
            //            }
            //        }
            //        catch (Exception ex)
            //        {
            //            string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
            //            errorMessage = errorMessage + "Failing for this file : " + SerialNo;
            //            ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            //        }
            //    }
            //}
			////////end
        }
        private List<RFIDField> GetSubStoreList()
        {
            List<RFIDField> lst = new List<RFIDField>();
            try
            {

                return lst = rfidBusiness.GetSubStoreList();
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
            return lst;
        }
        private bool SaveTable(DataTable dt_Rfid, string SerailNo)
        {
            try
            {
                rfidBusiness.AssociateRFIDtoPhotosAdvance(dt_Rfid, SerailNo);
                return true;
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
            return false;
        }

        private static DataTable ListToDataTable<T>(List<T> items)
        {
            DataTable dataTable = new DataTable(typeof(T).Name);
            try
            {
                //Get all the properties
                PropertyInfo[] Props = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);
                foreach (PropertyInfo prop in Props)
                {
                    if (prop.Name == "TagId" || prop.Name == "ScanningTime" || prop.Name == "RawData")
                    {
                        //Setting column names as Property names
                        dataTable.Columns.Add(prop.Name);
                    }
                }
                foreach (T item in items)
                {
                    var values = new object[3];
                    values[0] = Props[2].GetValue(item, null);
                    values[1] = Props[3].GetValue(item, null);
                    values[2] = Props[5].GetValue(item, null);
                    dataTable.Rows.Add(values);
                }
                return dataTable;
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
            return dataTable;
        }
		//////////////created by latika for table flow changes
        private void AssociateRFIDtoPhotosAdvanceTableWorkFlow()
        {
            List<RFIDField> lstSubstore = new List<RFIDField>();
            lstSubstore = GetSubStoreList();
            DataTable dtlst = new DataTable();
            string SerialNo = string.Empty;

            foreach (RFIDField item in lstSubstore)
            {
                string hotfolder = item.HotFolderpath;
                DirectoryInfo _objHotFolder = new DirectoryInfo(System.IO.Path.Combine(item.HotFolderpath, "MobileTags"));
                ///D:\DigiImages\MobileTags
                if (_objHotFolder.Exists)
                {
                    FileInfo[] fileEntries = _objHotFolder.GetFiles("*.txt");
                    DirectoryInfo _objSubHotFolder = new DirectoryInfo(System.IO.Path.Combine(item.HotFolderpath, "MobileTags", "Processed"));
                    if (!_objSubHotFolder.Exists)
                        _objSubHotFolder.Create();

                    try
                    {
                        foreach (FileInfo filename in fileEntries)
                        {
                            if (filename.Name.Contains('_'))
                            {
                                if (filename.Name.Contains(item.SerialNo))
                                {
                                    string file = filename.FullName;//.DirectoryName + "\\" + filename.Name.ToString();
                                    var pos = file.IndexOf("MobileTags");
                                    string des = file.Substring(pos + 11, file.Length - pos - 11);
                                    string txtname = des;
                                    des = _objSubHotFolder.FullName + "\\" + des;
                                    var SerialNumber = txtname.Split('_');
                                    SerialNo = SerialNumber[0];
                                    List<RFIDTagInfo> rfidTagGlobal = new List<RFIDTagInfo>();
                                    using (StreamReader r = new StreamReader(file))
                                    {
                                        string line;
                                        while ((line = r.ReadLine()) != null)
                                        {
                                            if (!String.IsNullOrWhiteSpace(line))
                                            {
                                                string[] result = line.Split(',');
                                                // YYYYMMDDHHMMSS 20160329171041
                                                string dateTime = result[1];
                                                string year = dateTime.Substring(0, 4);
                                                string month = dateTime.Substring(4, 2);
                                                string day = dateTime.Substring(6, 2);
                                                string hour = dateTime.Substring(8, 2);
                                                string min = dateTime.Substring(10, 2);
                                                string sec = dateTime.Substring(12, 2);
                                                //string spantime = year + "/" + month + "/" + day + " " + hour + ":" + min + ":" + sec;
                                               
                                                string spantime = year + "-" + month + "-" + day + " " + hour + ":" + min + ":" + sec;
                                                DateTime dt = Convert.ToDateTime(spantime);
                                                ///////made change by latika ruke for remove ATP QRCode url from tag
                                                string[] strTag = result[0].Split('=');
                                                string tagid = string.Empty;
                                                if (strTag.Length > 1) { tagid = strTag[strTag.Length - 1]; }
                                                else tagid = result[0];
                                                ///end
                                                if (result.Length > 2)
                                                {
                                                    
                                                    rfidTagGlobal.Add(new RFIDTagInfo { TagId = tagid,  ScanningDatetime = (spantime), TableName = result[2], Name = DigiPhoto.CryptorEngine.Encrypt(result[3], true), Email = DigiPhoto.CryptorEngine.Encrypt(result[4], true), Contacts = DigiPhoto.CryptorEngine.Encrypt(result[5], true)});
                                                }
                                                else { rfidTagGlobal.Add(new RFIDTagInfo { TagId = tagid, ScanningDatetime = (spantime) }); }
                                                result = null;
                                            }
                                        }
                                    }
                                    // Convert list to DataTable.
                                    dtlst = ListToDataTableWorkFlow(rfidTagGlobal);
                                    SaveTableTablwFlow(dtlst, SerialNo);
                                    File.Move(file, des);
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                        errorMessage = errorMessage + "Failing for this file : " + SerialNo;
                        ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                    }
                }
            }
        }

        private bool SaveTableTablwFlow(DataTable dt_Rfid, string SerailNo)
        {
            try
            {
                rfidBusiness.AssociateRFIDtoPhotosAdvanceTableFlow(dt_Rfid, SerailNo);
                return true;
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
            return false;
        }
        private static DataTable ListToDataTableWorkFlow<T>(List<T> items)
        {
            DataTable dataTable = new DataTable(typeof(T).Name);
            try
            {
                //Get all the properties
                PropertyInfo[] Props = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);
                foreach (PropertyInfo prop in Props)
                {
                    if (prop.Name == "TagId" || prop.Name == "ScanningTime" || prop.Name == "RawData" || prop.Name == "TableName" || prop.Name == "Name" || prop.Name == "Email" || prop.Name == "Contacts" || prop.Name == "ScanningDatetime")
                    {
                        //Setting column names as Property names
                        dataTable.Columns.Add(prop.Name);
                    }
                }
                foreach (T item in items)
                {
                    var values = new object[8];
                    values[0] = Props[2].GetValue(item, null);
                    values[1] = Props[3].GetValue(item, null);
                    values[2] = Props[5].GetValue(item, null);
                    values[3] = Props[10].GetValue(item, null);
                    values[4] = Props[11].GetValue(item, null);
                    values[5] = Props[12].GetValue(item, null);
                    values[6] = Props[13].GetValue(item, null);
                    values[7] = Props[14].GetValue(item, null);
                    dataTable.Rows.Add(values);
                }
                return dataTable;
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
            return dataTable;
        }
/////////////////////end by latika
    }
}
