﻿using Baracoda.Cameleon.PC.Readers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DigiRfidService
{
    //public class MessageEventArgs : EventArgs
    //{
    //    /// <summary>
    //    /// The message
    //    /// </summary>
    //    public string Message { get; set; }
    //}


    public class ReadersAddedEventArgs : EventArgs
    {
        /// <summary>
        /// The list of added readers
        /// </summary>
        public IEnumerable<BaracodaReaderBase> AddedReaders { get; set; }
    }

    public class DataEventArgs : EventArgs
    {
        /// <summary>
        /// The list of added readers
        /// </summary>
        public DataContainer RfidData { get; set; }
    }
}
