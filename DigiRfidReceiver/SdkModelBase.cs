﻿using Baracoda.Cameleon.PC.Common;
using Baracoda.Cameleon.PC.Modularity.DataStructures;
using Baracoda.Cameleon.PC.Modularity.GuiFeatures;
using Baracoda.Cameleon.PC.Modularity.Models;
using Baracoda.Cameleon.PC.Modularity.ViewModels;
using Baracoda.Cameleon.PC.Modularity.Windows;
using Baracoda.Cameleon.PC.Readers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Windows;
using System.Windows.Input;
using Baracoda;
using FrameworkHelper.RfidLib;

namespace DigiRfidService
{
    /// <summary>
    /// The base class for most of the examples
    /// </summary>
    public class SdkModelBase : DataModel
    {
        /// <summary>
        /// Is the model busy?
        /// </summary>
        public bool IsBusy
        {
            get { return isBusy; }
            set
            {
                if (isBusy == value) return;
                isBusy = value;
                SendPropertyChanged(() => IsBusy);
            }
        }

        /// <summary>
        /// Is Bluetooth stack available?
        /// </summary>
        public bool IsBtAvailable
        {
            get { return isBtAvailable; }
            protected set
            {
                if (isBtAvailable == value) return;
                isBtAvailable = value;
                SendPropertyChanged(() => IsBtAvailable);
            }
        }

        /// <summary>
        /// The list of readers
        /// </summary>
        public List<BaracodaReaderBase> Readers
        {
            get { return readers ?? (readers = new List<BaracodaReaderBase>()); }
        }

        /// <summary>
        /// The queue of readers being added during device inquiry
        /// </summary>
        protected Queue<BtReaderCached> Queue
        {
            get { lock (locker) return queue ?? (queue = new Queue<BtReaderCached>()); }
        }

        /// <summary>
        /// Utility method to handle all exceptions
        /// </summary>
        /// <param name="x">The exception</param>
        protected void OnException(Exception x)
        {
            string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(x);
            ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
        }

        /*
        /// <summary>
        /// Fired when Bluetooth inquiry is complete
        /// </summary>
        public event EventHandler<ReadersAddedEventArgs> InquiryComplete;

        /// <summary>
        /// Starts the Bluetooth inquiry
        /// </summary>
        protected void StartInquiry()
        {
            var btManager = BtMan.Instance;
            if (!btManager.IsBluetoothAvailable)
            {
                string errorMessage = "Cannot start inquiry when Bluetooth is not available";
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                //SendMessage("Cannot start inquiry when Bluetooth is not available");
                return;
            }
            Queue.Clear();

            btManager.BtInquiry.DeviceInquiryFinished += OnInquiryFinished;

            try
            {
                btManager.BtInquiry.StartDeviceInquiry(true, new byte[] { 0x00, 0x05, 0x00 });
            }
            catch (BaracodaException x)
            {
                btManager.BtInquiry.DeviceInquiryFinished -= OnInquiryFinished;

                OnException(x);
                IsBusy = false;

            }
        }

        void OnInquiryFinished(object sender, DeviceInquiryEventArgs e)
        {
            var btManager = BtMan.Instance;

            // at least one device responded
            if (e.State == InquiryResult.DeviceResponded)
            {
                // go through each of the devices
                for (var i = 0; i < e.Addresses.Count; i++)
                {
                    var id = e.Addresses[i].ToString();
                    var cached = new BtReaderCached
                    {
                        Id = id,
                        Name = string.IsNullOrEmpty(e.Names[i])
                                    ? id.Substring(id.Length - 5, 5)
                                    : e.Names[i]
                    };

                    // remember Address/Name pair
                    Queue.Enqueue(cached);
                }
            }

            // inquiry is complete, process the stored pairs
            else if (e.State == InquiryResult.Complete)
            {
                btManager.BtInquiry.DeviceInquiryFinished -= OnInquiryFinished;

                var addedReaders = new List<BaracodaBtReader>();
                foreach (var entry in Queue.ToList())
                {
                    // check if the reader is not already enlisted
                    var hasId = false;
                    foreach (var reader in Readers)
                    {
                        if (string.Equals(reader.Id, entry.Id))
                        {
                            hasId = true;
                            break;
                        }
                    }

                    if (!hasId)
                    {
                        // we have a new device
                        var btDevice = btManager.BtFactory.CreateBtDevice();

                        btDevice.Name = entry.Name;
                        btDevice.Address = new BtAddress(entry.Id);
                        btDevice.Scn = 1;

                        var btReader = new BaracodaBtReader(btDevice) { Name = btDevice.Name };
                        addedReaders.Add(btReader);
                    }
                }

                if (addedReaders.Count > 0)
                {
                    // cast to BaracodaReaderBase
                    var castReaders = addedReaders.Cast<BaracodaReaderBase>().ToList();

                    // store new readers on the list
                    Readers.AddRange(castReaders);

                    // notify listeners
                    var ev = InquiryComplete;
                    if (ev != null)
                        ev(this, new ReadersAddedEventArgs { AddedReaders = castReaders });
                }

                IsBusy = false;
            }
        }
        */
        /// <summary>
        /// Splits the string to substrings.
        /// </summary>
        /// <param name="text">The string that be split.</param>
        protected static IEnumerable<string> SplitNum(string text)
        {
            return SplitNum(text, 80);
        }

        /// <summary>
        /// Splits the string to substrings.
        /// </summary>
        /// <param name="text">The string that be split.</param>
        /// <param name="number">Length of the substring.</param>
        protected static IEnumerable<string> SplitNum(string text, int number)
        {
            var r = new List<string>();
            var i = 0;
            while (i < text.Length)
            {
                var sb = new StringBuilder();
                var j = 0;
                while (j < number && i < text.Length)
                {
                    sb.Append(text[i]);
                    j++;
                    i++;
                }
                r.Add(sb.ToString());
            }
            return r;
        }

        static string ProgramFile(PathContainer p)
        {
            var start = System.IO.Path.Combine(
                Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData),
                Assembly.GetEntryAssembly().FullName);
            p.Parts.Insert(0, start);
            return p.Finalize();
        }

        protected readonly Lazy<string> ReadersPathLazy = new Lazy<string>(() =>
            ProgramFile(Build.SubFolder("Configuration").Path("readers.data")));

        private bool isBusy = true;
        private bool isBtAvailable = true;
        private readonly object locker = new object();
        private Queue<BtReaderCached> queue;
        private List<BaracodaReaderBase> readers;
    }
}
