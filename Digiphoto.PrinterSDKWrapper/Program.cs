﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Description;
using System.ServiceModel.Web;
using System.Text;
using System.Management;
using System.Runtime.InteropServices;

namespace Digiphoto.PrinterSDKWrapper
{
    public class Program
    {
        static void Main(string[] args)
        {          
            WebServiceHost host = new WebServiceHost(typeof(PrinterApi), new Uri("http://" + Environment.MachineName + ":8000"));
            host.Description.Endpoints.Clear();
            ServiceEndpoint ep = host.AddServiceEndpoint(typeof(IPrinterApi), new WebHttpBinding(), "");
            ServiceDebugBehavior stp = host.Description.Behaviors.Find<ServiceDebugBehavior>();
            stp.HttpHelpPageEnabled = false;
            host.Open();
            AddInsetUSBHandler();
            AddRemoveUSBHandler();
            Console.WriteLine("Service is up and running");
            Console.WriteLine("Press enter to quit ");
            Console.ReadLine();
            host.Close();
        }


        static ManagementEventWatcher w = null;
        public static void AddRemoveUSBHandler()
        {
            WqlEventQuery q;
            ManagementScope scope = new ManagementScope("root\\CIMV2");
            scope.Options.EnablePrivileges = true;
            try
            {
                q = new WqlEventQuery();
                q.EventClassName = "__InstanceDeletionEvent";
                q.WithinInterval = new TimeSpan(0, 0, 3);
                q.Condition = @"TargetInstance ISA 'Win32_USBHub'";
                w = new ManagementEventWatcher(scope, q);
                w.EventArrived += new EventArrivedEventHandler(USBRemoved);
                w.Start();
            }

            catch (Exception e)
            {

                Console.WriteLine(e.Message);
                if (w != null)
                    w.Stop();
            }
        }

        static void AddInsetUSBHandler()
        {
            WqlEventQuery q;
            ManagementScope scope = new ManagementScope("root\\CIMV2");
            scope.Options.EnablePrivileges = true;

            try
            {
                q = new WqlEventQuery();
                q.EventClassName = "__InstanceCreationEvent";
                q.WithinInterval = new TimeSpan(0, 0, 3);
                q.Condition = @"TargetInstance ISA 'Win32_USBHub'";
                w = new ManagementEventWatcher(scope, q);
                w.EventArrived += new EventArrivedEventHandler(USBAdded);
                w.Start();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                if (w != null)
                    w.Stop();
            }
        }

        public static void USBAdded(object sender, EventArgs e)
        {
            Console.WriteLine("A USB device inserted");
        }

        public static void USBRemoved(object sender, EventArgs e)
        {
            Console.WriteLine("A USB device removed");
        }
    }
}
