﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace Digiphoto.PrinterSDKWrapper
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "PrinterApi" in both code and config file together.
    public class PrinterApi : IPrinterApi
    {
        //We need 2 separate function in same self-hosted web service for 6x8, 8x10 printer’s display count.

        public List<Printer6850> Get6850PrintersInfo()
        {
            try
            {
                return Printers.Load6850Printers();
            }
            catch (Exception ex)
            {
                return new List<Printer6850>() { new Printer6850(){ ErrorMessage=ex.Message+Environment
                .NewLine+ex.InnerException} };
            }
            // return "Calling Get for you " + inputMessage;
        }

        public List<Printer8810> Get8810PrintersInfo()
        {
            try
            {
                List<Printer8810> lst = new List<Printer8810>();
                foreach (var item in Printers.Load8810Printers())
                    lst.Add(item);
                return lst;
            }
            catch (Exception ex)
            {
                return new List<Printer8810>() { new Printer8810(){ ErrorMessage=ex.Message+Environment
                .NewLine+ex.InnerException} };
            }
            // return "Calling Get for you " + inputMessage;
        }

        public string GetMessage(string inputMessage)
        {
            try
            {
                //PrinterStatus(Printer6850 printer6850)
                Printers.LoadPrinters();
                return Printers.PrinterStatus(new Printer6850() { PrinterID = 0 });
            }
            catch (Exception ex)
            {
                return ex.Message + ":" + ex.StackTrace;
            }
            // return "Calling Get for you " + inputMessage;
        }

        public string LoadPrinters()
        {
            try
            {
                Printers.LoadPrinters();
                return "True";
            }
            catch (Exception ex)
            {
                return ex.Message + ":" + ex.StackTrace;
            }
        }

        public string GetPtinterStatus(int printerID)
        {
            try
            {
                return Printers.PrinterStatus(new Printer6850() { PrinterID = printerID });
            }
            catch (Exception ex)
            {
                return ex.Message + ":" + ex.StackTrace;
            }
        }



        public string PostMessage(string inputMessage)
        {
            return "Calling Post for you " + inputMessage;
        }
    }
}
