﻿using System;
using System.Runtime.InteropServices;


namespace Digiphoto.PrinterSDKWrapper
{
    public class Kodak6850PrinterAPIs : IDisposable
    {
        #region DLL Import

        /// <summary>
        /// Open the connection.
        /// </summary>
        /// <returns></returns>
        [DllImport("6850\\DLLS\\DigiPhoto.6850SDK.dll", CallingConvention = CallingConvention.Cdecl)]
        static extern int OpenConnection();

        /// <summary>
        /// Close the connection.
        /// </summary>
        [DllImport("6850\\DLLS\\DigiPhoto.6850SDK.dll", CallingConvention = CallingConvention.Cdecl)]
        static extern void CloseConnection();
        /// <summary>
        /// Gets the printer current status.
        /// </summary>
        /// <param name="i">The i.</param>
        /// <returns></returns>
        [DllImport("6850\\DLLS\\DigiPhoto.6850SDK.dll", CallingConvention = CallingConvention.Cdecl)]
        static extern int GetPrinterCurrentStatus(int printerID);
        /// <summary>
        /// Print Iamge.
        /// </summary>
        /// <param name="printerid">The printerid.</param>
        /// <param name="path">Image path.</param>
        [DllImport("6850\\DLLS\\DigiPhoto.6850SDK.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        static extern void SetPrint(int printerid, string imagePath);

        /// <summary>
        /// Gets the printer printed image count.
        /// </summary>
        /// <param name="printerid">The printerid.</param>
        [DllImport("6850\\DLLS\\DigiPhoto.6850SDK.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        static extern int GetPrinterPaperCount(int printerid);

        /// <summary>
        /// Gets the printers count.
        /// </summary>
        /// <param name="printerid">The printerid.</param>
        /// <returns></returns>
        [DllImport("6850\\DLLS\\DigiPhoto.6850SDK.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        static extern int GetPrintersCount();

        [DllImport("6850\\DLLS\\DigiPhoto.6850SDK.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr GetPrinterSerialNumbers();
        #endregion


        /// <summary>
        /// Open the SDK Connection.
        /// </summary>
        /// <returns></returns>
        public bool OpenSDKConnection()
        {
         int i=   OpenConnection();
            return true;
        }


        /// <summary>
        /// Initializes a new instance of the <see cref="Kodak6850PrinterAPIs"/> class.       
        /// </summary>
        public Kodak6850PrinterAPIs()
        {
            /// Open 6850 SDK
            this.OpenSDKConnection();
        }

        /// <summary>
        /// Close the SDK connection.
        /// </summary>
        public void CloseSDKConnection() { CloseConnection(); }

        public void Dispose()
        {
            //Dispose 6850 SDK Connection
            CloseSDKConnection();
        }

        /// <summary>
        /// Get 6850 printer status.
        /// </summary>
        /// <returns></returns>
        public PrinterStatus PrinterStatus(int printerID)
        {
            int PrinterStatus = -1;
            try
            {                
                return new Utilities().GetPrinterStatus(GetPrinterCurrentStatus(printerID));
            }
            catch (Exception ex)
            {
              
            }
            return new PrinterStatus() { Status = "kRESULT_UNKNOWN", Value = PrinterStatus, Description = "unknown" };
        }
        /// <summary>
        /// Print image count.
        /// </summary>
        /// <param name="printerid">printerid.</param>
        /// <returns></returns>
        public int PrinterImageCount(int printerid)
        {
            return GetPrinterPaperCount(printerid);
        }

        /// <summary>
        ///Get All 6850 Printer serial numbers.
        /// </summary>
        /// <returns></returns>
        public string PrinterSerialNumbers()
        {
            IntPtr ptr = GetPrinterSerialNumbers();
            byte[] result = new byte[1024];
            Marshal.Copy(ptr, result, 0, result.Length);
            int arrayLength = 0;
            for (int i = 0; i < result.Length; i++)
            {
                if (result[i] != 255)
                    ++arrayLength;
            }
            char[] oct = new char[arrayLength];
            for (int i = 0; i < arrayLength; i++)
            {
                oct[i] = (char)result[i];
            }
            return new string(oct);
        }

        /// <summary>
        ///Get No of Printers connected.
        /// </summary>
        /// <returns></returns>
        public int PrinterCount()
        {
            return GetPrintersCount();
        }
        /// <summary>
        /// Prints the image.
        /// </summary>
        /// <param name="printerID">The printer identifier.</param>
        /// <param name="filePath">The image file path.</param>
        /// <param name="printerSize">Size of the printer.</param>
        public string Print(int printerID, string imagePath, PrintSize printerSize, int width, int height, PrintImageAttribute printImageAttribute)
        {
            //check status
            if (!Printers.Printer6850[printerID].IsPrinting && PrinterStatus(printerID).Value == 0) // 0 print is ready 
            {
                //create ini file
                string ConfigFilePath = AppDomain.CurrentDomain.BaseDirectory + "Utilities\\" + printerSize.ToString() + "_" + printerID + ".ini";
                Utilities u = new Utilities();
                //convert image to raw file
                u.ConvertRawFile(imagePath, ConfigFilePath, printImageAttribute);
                //send the print job
                SetPrint(printerID, ConfigFilePath);
                return "Printing";
            }
            else if (Printers.Printer6850[printerID].IsPrinting)
                return "Printer Busy";
            else
                return PrinterStatus(printerID).Description;

        }
    }
}
