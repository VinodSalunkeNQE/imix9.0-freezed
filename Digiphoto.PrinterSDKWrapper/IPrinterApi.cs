﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace Digiphoto.PrinterSDKWrapper
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IPrinterApi" in both code and config file together.
    [ServiceContract]
    public interface IPrinterApi
    {
        [OperationContract]
        [WebGet]
        string GetMessage(string inputMessage);

        [OperationContract]
        [WebGet]
        List<Printer6850> Get6850PrintersInfo();

        [OperationContract]
        [WebGet]
        List<Printer8810> Get8810PrintersInfo();

        [OperationContract]
        [WebGet]
        string LoadPrinters();

        [OperationContract]
        [WebGet]
        string GetPtinterStatus(int printerID);


        [OperationContract]
        [WebInvoke]
        string PostMessage(string inputMessage);
    }
}
