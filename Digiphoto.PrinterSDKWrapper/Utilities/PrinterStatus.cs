﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Digiphoto.PrinterSDKWrapper
{
    /// <summary>
    /// Printer Status Information
    /// </summary>
    public class PrinterStatus
    {
        private string _status;
        public string Status
        {
            get { return _status; }
            set { this._status = value; }

        }

        private int _value;
        public int Value
        {
            get { return _value; }
            set { this._value = value; }

        }

        private string _description;
        public string Description
        {
            get { return _description; }
            set { this._description = value; }

        }



    }

}
