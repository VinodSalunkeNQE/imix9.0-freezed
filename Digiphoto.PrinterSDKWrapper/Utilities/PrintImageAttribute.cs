﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Digiphoto.PrinterSDKWrapper
{
    /// <summary>
    ///Print Image Attribute
    /// </summary>
    public class PrintImageAttribute
    {

        private string _imageName;
        public string ImageName
        {
            get { return _imageName; }
            set { this._imageName = value; }

        }
        private int _imageWidth;
        public int ImageWidth
        {
            get { return _imageWidth; }
            set { this._imageWidth = value; }

        }
        private int _imageHeight;
        public int ImageHeight
        {
            get { return _imageHeight; }
            set { this._imageHeight = value; }

        }

        private int _imageRGBBGR;
        public int ImageRGBBGR
        {
            get { return _imageRGBBGR; }
            set { this._imageRGBBGR = value; }
        }

        private int _imageCopies;
        public int ImageCopies
        {
            get { return _imageCopies; }
            set { this._imageCopies = value; }
        }

        private int _imagePrintType;
        public int ImagePrintType
        {
            get { return _imagePrintType; }
            set { this._imagePrintType = value; }
        }

        private int _imageRotation;
        public int ImageRotation
        {
            get { return _imageRotation; }
            set { this._imageRotation = value; }
        }
    }
}
