﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Text;
using System.Threading;

namespace Digiphoto.PrinterSDKWrapper
{
    public enum PrintSize
    {
        FourBySix = 0,
        SixByEight = 1
    };

    public static class Printers
    {
        public static List<Printer6850> Printer6850 = new List<Printer6850>();
        public static List<Printer6850> PreviousPrinter6850 = new List<Printer6850>();

        public static List<Printer8810> Printer8810 = new List<Printer8810>();
        /// <summary>
        /// Print the image
        /// </summary>
        /// <param name="image">image</param>
        /// <param name="printer6850">printer6850</param>
        /// <param name="printImageAttribute">printImageAttribute (Size)</param>
        public static void Print(this Image image, Printer6850 printer6850, PrintImageAttribute printImageAttribute)
        {
            using (Kodak6850PrinterAPIs api = new Kodak6850PrinterAPIs())
            {
                ChangePrinterStatus(printer6850.PrinterID, api.PrinterImageCount(printer6850.PrinterID));
                api.Print(printer6850.PrinterID, printImageAttribute.ImageName, PrintSize.FourBySix, printImageAttribute.ImageWidth, printImageAttribute.ImageHeight, printImageAttribute);
            }
        }
        /// <summary>
        ///  Print the image
        /// </summary>
        /// <param name="image"></param>
        /// <param name="printSize"></param>
        /// <param name="noOfCopies"></param>
        public static void Print(this Image image, PrintSize printSize, int noOfCopies)
        {

        }
        /// <summary>
        /// Print the image
        /// </summary>
        /// <param name="img"></param>
        /// <param name="printer6850"></param>
        /// <param name="printSize"></param>
        /// <param name="noOfCopies"></param>
        public static void Print(this Image image, Printer6850 printer6850, PrintSize printSize, int noOfCopies)
        {

        }
        /// <summary>
        /// Print the image
        /// </summary>
        /// <param name="image"></param>
        /// <param name="PrinterID"></param>
        /// <param name="printSize"></param>
        /// <param name="noOfCopies"></param>
        public static void Print(this Image image, int PrinterID, PrintSize printSize, int noOfCopies)
        {

        }
        /// <summary>
        /// Print the image
        /// </summary>
        /// <param name="image"></param>
        /// <param name="printerSerialNumber"></param>
        /// <param name="printSize"></param>
        /// <param name="noOfCopies"></param>
        public static void Print(this Image image, string printerSerialNumber, PrintSize printSize, int noOfCopies)
        {

        }
        /// <summary>
        /// Print the image
        /// </summary>
        /// <param name="image"></param>
        /// <param name="printImageAttribute"></param>
        public static void Print(this Image image, PrintImageAttribute printImageAttribute)
        {

        }
        /// <summary>
        /// Print the image
        /// </summary>
        /// <param name="image"></param>
        /// <param name="PrinterID"></param>
        /// <param name="printImageAttribute"></param>
        public static string Print(this Image image, int printerID, PrintImageAttribute printImageAttribute)
        {
            string result = "Unknown Result";
            using (Kodak6850PrinterAPIs api = new Kodak6850PrinterAPIs())
            {
                result = api.Print(printerID, printImageAttribute.ImageName, PrintSize.FourBySix, printImageAttribute.ImageWidth, printImageAttribute.ImageHeight, printImageAttribute);
            }

            return result;
        }

        public static int ImageCount(Printer6850 printer6850)
        {
            int result = 0;
            using (Kodak6850PrinterAPIs api = new Kodak6850PrinterAPIs())
            {
                result = api.PrinterImageCount(printer6850.PrinterID);
            }

            return result;
        }

        public static int PrinterCount()
        {
            int result = 0;
            using (Kodak6850PrinterAPIs api = new Kodak6850PrinterAPIs())
            {
                result = api.PrinterCount();
            }

            return result;
        }

        public static string PrinterStatus(Printer6850 printer6850)
        {
            string result = string.Empty;
            using (Kodak6850PrinterAPIs api = new Kodak6850PrinterAPIs())
            {
                result = api.PrinterStatus(printer6850.PrinterID).Description;
            }

            return result;
        }



        /// <summary>
        /// Load 6850 the printers and retrn no of printer connected to the system.
        /// </summary>
        /// <returns></returns>
        public static List<Printer6850> Load6850Printers()
        {
            Printer6850 = new List<Printer6850>();
            using (Kodak6850PrinterAPIs api = new Kodak6850PrinterAPIs())
            {
                int printCount = api.PrinterCount();
                printCount = api.PrinterCount();
                if (printCount > 0)
                {
                    string PrinterSerialNumbers = api.PrinterSerialNumbers();
                    for (int i = 0; i < printCount; i++)
                    {
                        string PrinterSerialNumber = PrinterSerialNumbers.Substring((i * 8), 8);
                        int PrintCount = api.PrinterImageCount(i);
                        string PrinterStatus = api.PrinterStatus(i).Status.ToString();
                        AddPrinter(new Printer6850() { PrinterID = i, PrinterSerialNumber = PrinterSerialNumber, ImageCount = PrintCount, PrinterStatus = PrinterStatus });
                    }
                }
            }
            return Printer6850;

        }

        /// <summary>
        /// Load 8810 the printers and retrn no of printer connected to the system.
        /// </summary>
        /// <returns></returns>
        public static List<Printer8810> Load8810Printers()
        {
            int printCount = 0;
            Printer8810 = new List<Printer8810>();
            using (Kodak8810PrinterAPIs api = new Kodak8810PrinterAPIs())
            {
                printCount = api.PrinterCount();
                if (printCount > 0)
                {
                    string PrinterSerialNumbers = api.PrinterSerialNumbers();
                    for (int i = 0; i < printCount; i++)
                    {
                        string PrinterSerialNumber = PrinterSerialNumbers.Substring((i * 8), 8);
                        int PrintCount = api.PrinterImageCount(i);
                        string PrinterStatus = api.PrinterStatus(i).Status.ToString();
                        AddPrinter(new Printer8810() { PrinterID = i, PrinterSerialNumber = PrinterSerialNumber, ImageCount = PrintCount, PrinterStatus = PrinterStatus });
                    }
                }
            }
            return Printer8810;
        }

        /// <summary>
        /// Loads all connected printers.
        /// </summary>
        public static void LoadPrinters()
        {
            //if (Printer6850 == null)
            Printer6850 = new List<Printer6850>();
            //PreviousPrinter6850=new List<Printer6850>();
            //foreach (var item in Printer6850)
            //{
            //    PreviousPrinter6850.Add(item);
            //}
            //if (Printer6850 != null)
            //{
            //    foreach (var item in Printer6850)
            //        item.IsOnline = false;
            //}
            using (Kodak6850PrinterAPIs api = new Kodak6850PrinterAPIs())
            {
                int printCount = api.PrinterCount();
                string PrinterSerialNumbers = api.PrinterSerialNumbers();
                for (int i = 0; i < printCount; i++)
                {
                    string PrinterSerialNumber = PrinterSerialNumbers.Substring((i * 8), 8);
                    if (Printer6850 != null && Printer6850.Count > 0 && Printer6850.Where(s => s.PrinterSerialNumber == PrinterSerialNumber).Count() > 0)
                        Printer6850.Where(s => s.PrinterSerialNumber == PrinterSerialNumber).FirstOrDefault().IsOnline = true;
                    else
                        AddPrinter(new Printer6850() { PrinterID = i, PrinterSerialNumber = PrinterSerialNumber, IsOnline = true });
                }
            }

            Printer8810 = new List<Printer8810>();
            using (Kodak8810PrinterAPIs api = new Kodak8810PrinterAPIs())
            {
                int printCount = api.PrinterCount();
                string PrinterSerialNumbers = api.PrinterSerialNumbers();
                for (int i = 0; i < printCount; i++)
                {
                    string PrinterSerialNumber = PrinterSerialNumbers.Substring((i * 8), 8);
                    AddPrinter(new Printer8810() { PrinterID = i, PrinterSerialNumber = PrinterSerialNumber });
                }
            }
            //PrinterCount()

        }
        /// <summary>
        /// Print the image
        /// </summary>
        /// <param name="image"></param>
        /// <param name="printerSerialNumber"></param>
        /// <param name="printImageAttribute"></param>
        public static void Print(this Image image, string printerSerialNumber, PrintImageAttribute printImageAttribute)
        {

        }

        /// <summary>
        /// Add Printer
        /// </summary>
        /// <param name="printer6850"></param>
        public static void AddPrinter(Printer8810 printer8810)
        {
            Printer8810.Add(printer8810);
        }

        public static void AddPrinter(Printer6850 printer6850)
        {
            Printer6850.Add(printer6850);
        }

        public static void AddPrinter(int printerID)
        {

            Printer8810.Add(new Printer8810() { PrinterID = printerID });
        }

        /// <summary>
        /// Remove Printer
        /// </summary>
        /// <param name="printer6850"></param>
        public static void RemovePrinter(Printer8810 printer8810)
        {
            if (Printer8810.Contains(printer8810))
                Printer8810.Remove(printer8810);
        }


        /// <summary>
        /// Change Printer Status
        /// </summary>
        /// <param name="printerID"></param>
        /// <param name="printCount"></param>
        public static void ChangePrinterStatus(int printerID, int printCount)
        {
            if (Printer8810[printerID] != null)
                Printer8810[printerID].ChangePrinterStatus(printerID, printCount);
            else
                throw new NullReferenceException("Printer is not available.");
        }

        public static string PrinterSerialNumber(Printer8810 printer8810)
        {
            try
            {
                return Printer6850.Where(s => s.PrinterID == printer8810.PrinterID).FirstOrDefault().PrinterSerialNumber;
            }
            catch (Exception ex)
            {

            }
            return "";
        }

        public static string PrinterSerialNumber(Printer6850 printer6850)
        {
            try
            {
                return Printer6850.Where(s => s.PrinterID == printer6850.PrinterID).FirstOrDefault().PrinterSerialNumber;
            }
            catch (Exception ex)
            {

            }
            return "";
        }


    }

    public class Printer8810 : IPrinter
    {
        public Printer8810()
        {
            ErrorMessage = string.Empty;            
            PrinterSerialNumber = string.Empty;
            PrinterStatus = string.Empty;
        }
        // public delegate void LongTimeTask_Delegate(string s);

        private int _imageOldCount;
        private int _imageNewCount;

        private int _imageCount;
        public int ImageCount
        {
            get { return _imageCount; }
            set { _imageCount = value; }
        }
        public string ErrorMessage { get; set; }

        public int ImageOldCount
        {
            get { return _imageOldCount; }
            set { _imageOldCount = value; }
        }

        public int ImageNewCount
        {
            get { return _imageNewCount; }
            set { _imageNewCount = value; }
        }


        private string _printerStatus;
        public string PrinterStatus
        {
            get { return _printerStatus; }
            set { _printerStatus = value; }
        }

        private int _printerID;
        public int PrinterID
        {
            get { return _printerID; }
            set { _printerID = value; }
        }

        private string _printerSerialNumber;
        public string PrinterSerialNumber
        {
            get { return _printerSerialNumber; }
            set { _printerSerialNumber = value; }
        }

        private bool _isPrinting;
        public bool IsPrinting
        {
            get { return _isPrinting; }
            set { _isPrinting = value; }
        }

        public void ChangePrinterStatus(int printerID, int printCount)
        {
            _printerID = printerID;
            _imageOldCount = printCount;
            _isPrinting = true;
            Action example = new Action(GetImageCount);
            IAsyncResult ia = example.BeginInvoke(new AsyncCallback(completed), null);
        }

        public string GetPrinterSerialNumber(int printerID)
        {
            string result = "";
            using (Kodak8810PrinterAPIs api = new Kodak8810PrinterAPIs())
            {
                result = api.PrinterSerialNumbers();
            }
            return result;
        }

        public void GetImageCount()
        {
            int y = _imageOldCount + 1;
            int z = _imageOldCount;
            do
            {
                using (Kodak8810PrinterAPIs api = new Kodak8810PrinterAPIs())
                {
                    z = api.PrinterImageCount(_printerID);
                    Thread.Sleep(1000);
                }
            }
            while (z < y);
        }

        public void completed(IAsyncResult ar)
        {
            Action example = (ar as AsyncResult).AsyncDelegate as Action;
            try
            {
                example.EndInvoke(ar);
                _isPrinting = false;
            }
            catch (ApplicationException ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
    }

    public class Printer6850 : IPrinter
    {
        public Printer6850()
        {
            ErrorMessage = string.Empty;
            PrinterSerialNumber = string.Empty;
            PrinterStatus = string.Empty;
        }
        // public delegate void LongTimeTask_Delegate(string s);

        private int _imageOldCount;
        private int _imageNewCount;

        private int _imageCount;

        public int ImageCount
        {
            get { return _imageCount; }
            set { _imageCount = value; }
        }

        public string ErrorMessage { get; set; }
        public int ImageOldCount
        {
            get { return _imageOldCount; }
            set { _imageOldCount = value; }
        }

        public int ImageNewCount
        {
            get { return _imageNewCount; }
            set { _imageNewCount = value; }
        }


        private string _printerStatus;
        public string PrinterStatus
        {
            get { return _printerStatus; }
            set { _printerStatus = value; }
        }

        private int _printerID;
        public int PrinterID
        {
            get { return _printerID; }
            set { _printerID = value; }
        }

        private bool _isOnline;
        public bool IsOnline
        {
            get { return _isOnline; }
            set { _isOnline = value; }
        }

        private string _printerSerialNumber;
        public string PrinterSerialNumber
        {
            get { return _printerSerialNumber; }
            set { _printerSerialNumber = value; }
        }

        private bool _isPrinting;
        public bool IsPrinting
        {
            get { return _isPrinting; }
            set { _isPrinting = value; }
        }

        public void ChangePrinterStatus(int printerID, int printCount)
        {
            _printerID = printerID;
            _imageOldCount = printCount;
            _isPrinting = true;
            Action example = new Action(GetImageCount);
            IAsyncResult ia = example.BeginInvoke(new AsyncCallback(completed), null);
        }

        public string GetPrinterSerialNumber(int printerID)
        {
            string result = "";
            using (Kodak6850PrinterAPIs api = new Kodak6850PrinterAPIs())
            {
                result = api.PrinterSerialNumbers();
            }
            return result;
        }

        public void GetImageCount()
        {
            int y = _imageOldCount + 1;
            int z = _imageOldCount;
            do
            {
                using (Kodak6850PrinterAPIs api = new Kodak6850PrinterAPIs())
                {
                    z = api.PrinterImageCount(_printerID);
                    Thread.Sleep(1000);
                }
            }
            while (z < y);
        }

        public void completed(IAsyncResult ar)
        {
            Action example = (ar as AsyncResult).AsyncDelegate as Action;
            try
            {
                example.EndInvoke(ar);
                _isPrinting = false;
            }
            catch (ApplicationException ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
    }
}
