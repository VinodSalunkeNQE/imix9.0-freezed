﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Digiphoto.PrinterSDKWrapper
{
    interface IPrinter
    {


        int ImageOldCount
        {
            get;
            set;
        }

        int ImageNewCount
        {
            get;
            set;
        }



        string PrinterStatus
        {
            get;
            set;
        }


        int PrinterID
        {
            get;
            set;
        }


        string PrinterSerialNumber
        {
            get;
            set;
        }


        bool IsPrinting
        {
            get;
            set;
        }

        void ChangePrinterStatus(int printerID, int printCount);

        string GetPrinterSerialNumber(int printerID);

        void GetImageCount();

        void completed(IAsyncResult ar);
    }
}
