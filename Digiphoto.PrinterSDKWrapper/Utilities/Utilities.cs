﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;

namespace Digiphoto.PrinterSDKWrapper
{

    public enum RGBBGR
    {
        FourBySix = 0,
        SixByEight = 1
    };
    /// <summary>
    /// Provide information about printer status details,printer error details and so on.,
    /// </summary>
    public class Utilities : IDisposable
    {
        #region PrintStatus

        public List<PrinterStatus> PrinterStatus = new List<PrinterStatus>(){
            new PrinterStatus(){ Status="kRESULT_READY",Value=0, Description="The printer is ready"},
            new PrinterStatus(){ Status="kRESULT_MUTEX_Wait",Value=1500, Description="Printer is used by another thread.."},
            new PrinterStatus(){ Status="kRESULT_THREAD_Max",Value=1501, Description="Cannot generate new thread."},
            new PrinterStatus(){ Status="kRESULT_NOTCONNECT",Value=2000, Description="The printer cannot be recognized."},
            new PrinterStatus(){ Status="kRESULT_BUSY",Value=2100 , Description="The printer is busy in printing."},
            new PrinterStatus(){ Status="kRESULT_WAIT_WindingRibbonErr",Value=2202, Description="The ink ribbon is being taken up."},
            new PrinterStatus(){ Status="kRESULT_WAIT_OffLineErr",Value=2204, Description="The operator panel is being used."},
            new PrinterStatus(){ Status="kRESULT_WAIT_InitializeErr",Value=2205, Description="The printer is initializing at the power-on time. "},
            new PrinterStatus(){ Status="kRESULT_STATUS_CoverOpenErr",Value=2401, Description="Cover Open."},
            new PrinterStatus(){ Status="kRESULT_STATUS_PaperCoverOpenErr",Value=2402, Description="Paper Cover Open. "},
            new PrinterStatus(){ Status="kRESULT_STATUS_IncorrectRibbonErr",Value=2403, Description="Incorrect Ribbon."},
            new PrinterStatus(){ Status="kRESULT_STATUS_IncorrectPaperErr",Value=2404, Description="Incorrect Paper."},
            new PrinterStatus(){ Status="kRESULT_STATUS_RibbonEmptyErr",Value=2405, Description="Ribbon Empty."},
            new PrinterStatus(){ Status="kRESULT_STATUS_PaperEmptyErr",Value=2408, Description="Paper Empty."},
            new PrinterStatus(){ Status="kRESULT_STATUS_ReadyLoadingErr",Value=2412, Description="Ready Loading."},
            new PrinterStatus(){ Status="kRESULT_STATUS_TakeOutPaperErr",Value=2413, Description="Take Out Paper."},
            new PrinterStatus(){ Status="kRESULT_SVCCALL_UNKOWN ",Value=2504, Description="Undefined error"},
            new PrinterStatus(){ Status="kRESULT_USB_CommErr ",Value=2600, Description="USB communication error"},
            new PrinterStatus(){ Status="kRESULT_OPECALL_PaperJamErr",Value=3001, Description="Receiver Entrance ->Handling to Receiver Empty Sensor, Receiver Empty Sensor is not turning ON."},
            new PrinterStatus(){ Status="kRESULT_OPECALL_PaperJamErr",Value=3002, Description="Receiver Empty Sensor ->Handling to Receiver Edge Sensor, Receiver Edge Sensor is not turning ON."},
            new PrinterStatus(){ Status="kRESULT_OPECALL_PaperJamErr",Value=3003, Description="Receiver Edge Sensor ->Handling to Print Position Sensor, Slow up/down feed error 1 occurred."},
            new PrinterStatus(){ Status="kRESULT_OPECALL_PaperJamErr",Value=3004, Description="Receiver Edge Sensor ->Handling to Print Position Sensor, Slow up/down feed error 2 occurred."},
            new PrinterStatus(){ Status="kRESULT_OPECALL_PaperJamErr",Value=3005, Description="Receiver Edge Sensor ->Handling to Print Position Sensor, Print Position Sensor is not turning ON."},
            new PrinterStatus(){ Status="kRESULT_OPECALL_PaperJamErr",Value=3011, Description="Home Position ->Handling to Print Position Sensor, Print Position Sensor is not turning Off."},
            new PrinterStatus(){ Status="kRESULT_OPECALL_PaperJamErr",Value=3012, Description="Print Position Sensor->Handling to Edge Sensor, Edge sensor is not turning Off."},
            new PrinterStatus(){ Status="kRESULT_OPECALL_PaperJamErr",Value=3013, Description="Receiver Edge Sensor->Handling to Print Position Sensor, Slow up/down feed error 1 occurred."},
            new PrinterStatus(){ Status="kRESULT_OPECALL_PaperJamErr",Value=3014, Description="Receiver Edge Sensor->Handling to Print Position Sensor, Slow up/down feed error 2 occurred."},
            new PrinterStatus(){ Status="kRESULT_OPECALL_PaperJamErr",Value=3015, Description="Receiver Edge Sensor->Handling to Print Position Sensor, Print Position Sensor does not turn on. "},
            new PrinterStatus(){ Status="kRESULT_OPECALL_PaperJamErr",Value=3016, Description="Receiver Type Check-> Handling to Home Position, Slow up/down feed error 1 occurred.."},
            new PrinterStatus(){ Status="kRESULT_OPECALL_PaperJamErr",Value=3017, Description="Receiver Type Check-> Handling to Home Position, Slow up/down feed error 2 occurred"},
            new PrinterStatus(){ Status="kRESULT_OPECALL_PaperJamErr",Value=3018, Description="Receiver Type Check-> Handling to Home Position, Print Position Sensor is not turning Off."},
            new PrinterStatus(){ Status="kRESULT_OPECALL_PaperJamErr",Value=3021, Description="Handling to Print Position Sensor, Print Position Sensor is not turning Off. "},
            new PrinterStatus(){ Status="kRESULT_OPECALL_PaperJamErr",Value=3022, Description="Handling to Edge Sensor, Edge Sensor is not turning Off."},
            new PrinterStatus(){ Status="kRESULT_OPECALL_PaperJamErr",Value=3023, Description="Handling to Receiver Empty Sensor, Handling Empty Sensor is not turning Off."},
            new PrinterStatus(){ Status="kRESULT_OPECALL_PaperJamErr",Value=3031, Description="Handling to Print Position Sensor, Print Position Sensor is not turning Off until end of remaining paper passes. "},
            new PrinterStatus(){ Status="kRESULT_OPECALL_PaperJamErr",Value=3041, Description="Home Position ->Handling to Print Position Sensor, Print Position Sensor is not turning On."},
            new PrinterStatus(){ Status="kRESULT_OPECALL_PaperJamErr",Value=3051, Description="Skew correction Process  -> Handling to Home Position, Slow up/down feed error 1 occurred."},
            new PrinterStatus(){ Status="kRESULT_OPECALL_PaperJamErr",Value=3052, Description="Skew correction Process -> Handling to Home Position.  Slow up/down feed error 2 occurred."},
            new PrinterStatus(){ Status="kRESULT_OPECALL_PaperJamErr",Value=3053, Description="Skew correction Process -> Handling to Home position, Print Position Sensor is not turning OFF."},
            new PrinterStatus(){ Status="kRESULT_OPECALL_PaperJamErr",Value=3061, Description="After 1 color print, Edge Sensor is turning Off."},
            new PrinterStatus(){ Status="kRESULT_OPECALL_PaperJamErr",Value=3062, Description="After YMC print, after paper rewind, Print Position Sensor is turning Off."},
            new PrinterStatus(){ Status="kRESULT_OPECALL_PaperJamErr",Value=3064, Description="Cut->Print start position, Slow up/down feed error occurred."},
            new PrinterStatus(){ Status="kRESULT_OPECALL_PaperJamErr",Value=3065, Description="Cut->At the time of finishing feeding to print start position, Print Position Sensor is turning Off. (except for5x3.5)"},
            new PrinterStatus(){ Status="kRESULT_OPECALL_PaperJamErr",Value=3066, Description="Cut->At the time of finishing feeding to print start position, Print Position Sensor is turning Off. (for5x3.5) "},
            new PrinterStatus(){ Status="kRESULT_OPECALL_PaperJamErr",Value=3067, Description="Skew correction Process -> Handling to Print Start Position, Slow up/down feed error 1 occurred."},
            new PrinterStatus(){ Status="kRESULT_OPECALL_PaperJamErr",Value=3068, Description="Skew correction Process -> Handling to Print Start Position, Slow up/down feed error 2 occurred."},
            new PrinterStatus(){ Status="kRESULT_OPECALL_PaperJamErr",Value=3069, Description="Skew correction Process -> Print Position Sensor is not turning Off. "},
            new PrinterStatus(){ Status="kRESULT_OPECALL_PaperJamErr",Value=3071, Description="Home Position->Handling to Print Position Sensor, Print Position sensor is not turning ON."},
            new PrinterStatus(){ Status="kRESULT_OPECALL_PaperJamErr",Value=3081, Description="Paper Edge Sensor detect OFF while printer is Ready."},
            new PrinterStatus(){ Status="kRESULT_OPECALL_PaperJamErr",Value=3083, Description="Paper Empty Sensor detect OFF while printer is Ready."},
            new PrinterStatus(){ Status="kRESULT_OPECALL_PaperJamErr",Value=3084, Description="Paper size Sensor detect OFF while printer is Ready. (Using 6 inch paper)."},
            new PrinterStatus(){ Status="kRESULT_OPECALL_PaperJamErr",Value=3091, Description="Paper Edge Sensor sense ON, while printer is ready. "},
            new PrinterStatus(){ Status="kRESULT_OPECALL_PaperJamErr",Value=3092, Description="Print Position Sensor sense ON, while printer is ready. "},
            new PrinterStatus(){ Status="kRESULT_OPECALL_PaperJamErr",Value=3093, Description="Paper Empty Sensor sense ON, while printer is ready."},
            new PrinterStatus(){ Status="kRESULT_OPECALL_PaperJamErr",Value=3094, Description="Paper size Sensor sense ON, while printer is ready."}
                
        };
        #endregion
        public PrinterStatus GetPrinterStatus(int value)
        {
            return PrinterStatus.Where(s => s.Value == value).First();
        }


        /// <summary>
        /// Creates a new Image containing the same image only rotated
        /// </summary>
        /// <param name="image">The <see cref="System.Drawing.Image"/> to rotate</param>
        /// <param name="angle">The amount to rotate the image, clockwise, in degrees</param>
        /// <returns>A new <see cref="System.Drawing.Bitmap"/> of the same size rotated.</returns>
        /// <exception cref="System.ArgumentNullException">Thrown if <see cref="image"/> is null.</exception>
        public static Bitmap RotateImage(Image image, float angle)
        {
            return RotateImage(image, new PointF((float)image.Width / 2, (float)image.Height / 2), angle);
        }

        /// <summary>
        /// Creates a new Image containing the same image only rotated
        /// </summary>
        /// <param name="image">The <see cref="System.Drawing.Image"/> to rotate</param>
        /// <param name="offset">The position to rotate from.</param>
        /// <param name="angle">The amount to rotate the image, clockwise, in degrees</param>
        /// <returns>A new <see cref="System.Drawing.Bitmap"/> of the same size rotated.</returns>
        /// <exception cref="System.ArgumentNullException">Thrown if <see cref="image"/> is null.</exception>
        public static Bitmap RotateImage(Image image, PointF offset, float angle)
        {
            if (image == null)
                throw new ArgumentNullException("image");

            //create a new empty bitmap to hold rotated image
            Bitmap rotatedBmp = new Bitmap(image.Width, image.Height);
            rotatedBmp.SetResolution(image.HorizontalResolution, image.VerticalResolution);

            //make a graphics object from the empty bitmap
            Graphics g = Graphics.FromImage(rotatedBmp);

            //Put the rotation point in the center of the image
            g.TranslateTransform(offset.X, offset.Y);

            //rotate the image
            g.RotateTransform(angle);

            //move the image back
            g.TranslateTransform(-offset.X, -offset.Y);

            //draw passed in image onto graphics object
            g.DrawImage(image, new PointF(0, 0));

            return rotatedBmp;
        }


        /// <summary>
        /// Convert JPEG To Raw File
        /// </summary>
        /// <param name="imagePath"></param>
        /// <param name="configFilePath"></param>
        /// <returns></returns>
        public bool ConvertRawFile(string imagePath, string configFilePath, PrintImageAttribute printImageAttribute)
        {

            string pathToJpeg = imagePath;// @"D:\DigiPhoto\Projects\DevelopmentDoc\SDK\68XX\EK68xx SDK(Ver1.0.1.55)_20140908\CHCUSBDLL_v10155\C#SDK\Resources\HumanRGB.jpg";
            string rawImagePath = Path.GetFileNameWithoutExtension(imagePath) + ".raw";
            if (File.Exists(rawImagePath))
            {
                File.Delete(rawImagePath);
            }
            Image img = Image.FromFile(pathToJpeg);
            if (printImageAttribute.ImageRotation == 90)
                img.RotateFlip(RotateFlipType.Rotate90FlipY);
            // Bitmap oldBitmap = new Bitmap(img);
            // oldBitmap.RotateFlip(RotateFlipType.Rotate90FlipX);
            // img = Utilities.RotateImage(img, 90);
            Bitmap myBitmap;
            myBitmap = new Bitmap(img, printImageAttribute.ImageWidth, printImageAttribute.ImageHeight);// 1844, 1240);
            File.WriteAllBytes(rawImagePath, ConvertBitmap(myBitmap));
            //Create Config file for path
            CreateConfigFile(configFilePath, rawImagePath, printImageAttribute);
            return true;
        }

        public string CreateConfigFile(string configFilePath, string rawImagePath, PrintImageAttribute printImageAttribute)
        {
            String TemplateData;
            using (StreamReader sr = new StreamReader(@"D:\DigiPhoto\Projects\DevelopmentDoc\SDK\68XX\EK68xx SDK(Ver1.0.1.55)_20140908\CHCUSBDLL_v10155\C#SDK\68XXWrapper\68XXWrapper\PrinterSDKWrapper\Utilities\Template6850P4X6.ini"))
            {
                // Read the stream to a string, and write the string to the console.
                TemplateData = sr.ReadToEnd();
                //Console.WriteLine(line);
            }
            // rawImagePath = @"D:\DigiPhoto\Projects\DevelopmentDoc\SDK\68XX\EK68xx SDK(Ver1.0.1.55)_20140908\CHCUSBDLL_v10155\SampleCode\PhotoShopTest.raw";

            //media type need to be check
            TemplateData = TemplateData.Replace("<ImageName>", rawImagePath);
            TemplateData = TemplateData.Replace("<ImageSIZE>", printImageAttribute.ImageWidth + "," + printImageAttribute.ImageHeight);
            TemplateData = TemplateData.Replace("<RGBBGR>", printImageAttribute.ImageRGBBGR.ToString());
            //TemplateData = TemplateData.Replace("<RGBBGR>", "4");
            TemplateData = TemplateData.Replace("<COPIES>", printImageAttribute.ImageCopies.ToString());
            TemplateData = TemplateData.Replace("<PRINT_TYPE>", printImageAttribute.ImagePrintType.ToString());
            // TemplateData.Replace("<Media type>", "1");
            // Append text to an existing file named "WriteLines.txt".

            if (File.Exists(configFilePath))
            {
                File.Delete(configFilePath);
            }

            using (StreamWriter outputFile = new StreamWriter(configFilePath, true))
            {
                outputFile.WriteLine(TemplateData);
            }
            return configFilePath;
        }
        /// <summary>
        /// Convert a bitmap to a byte array
        /// </summary>
        /// <param name="bitmap">image to convert</param>
        /// <returns>image as bytes</returns>
        public byte[] ConvertBitmap(Bitmap bitmap)
        {
            BitmapData raw = null;  //used to get attributes of the image
            byte[] rawImage = null; //the image as a byte[]

            try
            {
                //Freeze the image in memory
                raw = bitmap.LockBits(
                    new Rectangle(0, 0, (int)bitmap.Width, (int)bitmap.Height),
                    ImageLockMode.ReadOnly,
                    PixelFormat.Format24bppRgb
                );

                int size = raw.Height * raw.Stride;
                rawImage = new byte[size];



                //Copy the image into the byte[]
                System.Runtime.InteropServices.Marshal.Copy(raw.Scan0, rawImage, 0, size);
            }
            finally
            {
                if (raw != null)
                {
                    //Unfreeze the memory for the image
                    bitmap.UnlockBits(raw);
                }
            }
            return rawImage;
        }

        public void Dispose()
        {
            throw new NotImplementedException();
        }
    }
}
