﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;

namespace Digiphoto.PrinterSDKWrapper
{
    public class Kodak8810PrinterAPIs : IDisposable
    {
        //#region Delegates
        //public delegate bool PrinterStatus(int value);
        //[DllImport("8810\\DLLS\\SDK8810.dll", CallingConvention = CallingConvention.Cdecl)]
        //static extern int OpenConnection(PrinterStatus ps);
        //#endregion

        #region DLL Import

        /// <summary>
        /// Open the connection.
        /// </summary>
        /// <returns></returns>
        [DllImport("8810\\DLLS\\DigiPhoto.8810SDK.dll", CallingConvention = CallingConvention.Cdecl)]
        static extern int OpenConnection();

        /// <summary>
        /// Close the connection.
        /// </summary>
        [DllImport("8810\\DLLS\\DigiPhoto.8810SDK.dll", CallingConvention = CallingConvention.Cdecl)]
        static extern void CloseConnection();
        /// <summary>
        /// Gets the printer current status.
        /// </summary>
        /// <param name="i">The i.</param>
        /// <returns></returns>
        [DllImport("8810\\DLLS\\DigiPhoto.8810SDK.dll", CallingConvention = CallingConvention.Cdecl)]
        static extern int GetPrinterCurrentStatus(int printerID);
        /// <summary>
        /// Print Iamge.
        /// </summary>
        /// <param name="printerid">The printerid.</param>
        /// <param name="path">Image path.</param>
        [DllImport("8810\\DLLS\\DigiPhoto.8810SDK.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        static extern void SetPrint(int printerid, string imagePath);

        /// <summary>
        /// Gets the printer printed image count.
        /// </summary>
        /// <param name="printerid">The printerid.</param>
        [DllImport("8810\\DLLS\\DigiPhoto.8810SDK.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        static extern int GetPrinterPaperCount(int printerid);

        /// <summary>
        /// Gets the printers count.
        /// </summary>
        /// <param name="printerid">The printerid.</param>
        /// <returns></returns>
        [DllImport("8810\\DLLS\\DigiPhoto.8810SDK.dll", CallingConvention = CallingConvention.Cdecl)]
        static extern int GetPrintersCount();

        [DllImport("8810\\DLLS\\DigiPhoto.8810SDK.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr GetPrinterSerialNumbers();
        #endregion

        public Kodak8810PrinterAPIs()
        {
            bool tyr = false;
            try
            {
                tyr = this.OpenSDKConnection();
            }
            catch (Exception ex)
            {

            }
        }


        #region SDK APIs
        /// <summary>
        /// Open the SDK Connection.
        /// </summary>
        /// <returns></returns>
        public bool OpenSDKConnection()
        {
            OpenConnection();
            return true;
        }
        /// <summary>
        /// Close the SDK connection.
        /// </summary>
        public void CloseSDKConnection() { CloseConnection(); }

        /// <summary>
        /// Get 8810 printer status.
        /// </summary>
        /// <returns></returns>
        public PrinterStatus PrinterStatus(int printerID)
        {
            int PrinterStatus = -1;
            try
            {
                PrinterStatus = GetPrinterCurrentStatus(printerID);
                return new Utilities().GetPrinterStatus(PrinterStatus);
            }
            catch (Exception ex)
            {

            }
            return new PrinterStatus() { Status = "kRESULT_UNKNOWN", Value = PrinterStatus, Description = "unknown" };
        }
        /// <summary>
        /// Print image count.
        /// </summary>
        /// <param name="printerid">printerid.</param>
        /// <returns></returns>
        public int PrinterImageCount(int printerid)
        {
            return GetPrinterPaperCount(printerid);
        }

        public string PrinterSerialNumbers()
        {
            IntPtr ptr = GetPrinterSerialNumbers();
            byte[] result = new byte[1024];
            Marshal.Copy(ptr, result, 0, result.Length);
            int arrayLength = 0;
            for (int i = 0; i < result.Length; i++)
            {
                if (result[i] != 255)
                    ++arrayLength;
            }
            char[] oct = new char[arrayLength];
            for (int i = 0; i < arrayLength; i++)
            {
                oct[i] = (char)result[i];
            }
            return new string(oct);
        }

        public int PrinterCount()
        {
            return GetPrintersCount();
        }
        /// <summary>
        /// Prints the image.
        /// </summary>
        /// <param name="printerID">The printer identifier.</param>
        /// <param name="filePath">The image file path.</param>
        /// <param name="printerSize">Size of the printer.</param>
        public string Print(int printerID, string imagePath, PrintSize printerSize, int width, int height, PrintImageAttribute printImageAttribute)
        {
            //check status
            if (!Printers.Printer8810[printerID].IsPrinting && PrinterStatus(printerID).Value == 0) // 0 print is ready 
            {
                string ConfigFilePath = AppDomain.CurrentDomain.BaseDirectory + "Utilities\\" + printerSize.ToString() + "_" + printerID + ".ini";
                Utilities u = new Utilities();
                u.ConvertRawFile(imagePath, ConfigFilePath, printImageAttribute);//1844, 1240);//);
                // Printers.Printer8810[printerID].ChangePrinterStatus(printerID, PrinterImageCount(printerID));
                SetPrint(printerID, ConfigFilePath);
                return "Printing";
            }
            else if (Printers.Printer8810[printerID].IsPrinting)
                return "Printer Busy";
            else
                return PrinterStatus(printerID).Description;
        }



        #endregion
        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            //this.CloseSDKConnection();
        }
    }
}
