﻿using DigiPhoto;
using DigiPhoto.Cache.DataCache;
using DigiPhoto.DataLayer;
using DigiPhoto.DataLayer.Model;
using DigiPhoto.IMIX.Business;
using FrameworkHelper.RfidLib;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using DigiPhoto.IMIX.Model;

namespace DigiRFIDAssociationService
{
    public partial class AssociationService : ServiceBase
    {
        bool IsRfidEnabled = false;
        RfidBusiness rfidBusiness = new RfidBusiness();
        //int SubStoreId = 0;
        static System.Timers.Timer timer;
        static int _timerMiliSeconds = 5000;
        public AssociationService()
        {
            InitializeComponent();
        }
        protected override void OnStart(string[] args)
        {
            try
            {

                ServicePosInfoBusiness svcPosinfoBusiness = new ServicePosInfoBusiness();
                string ret = svcPosinfoBusiness.ServiceStart(false);
                //ret = 1;

                if (string.IsNullOrEmpty(ret))
                {
                DataCacheFactory.Register();
                //GetSubStoreId();
                StartScheduler();
                //GetConfigData();
                }
                else
                {
                    throw new Exception("Already Started");       
                   
                }

               
                //ServicePosInfoBusiness svcPosinfoBusiness = new ServicePosInfoBusiness();
                //svcPosinfoBusiness.ServiceStart(false);
                
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                ExitCode = 13816;
                this.OnStop();
            }
        }
        private void StartScheduler()
        {
            try
            {
                timer = new System.Timers.Timer();
                timer.Interval = _timerMiliSeconds;
                timer.AutoReset = true;
                timer.Elapsed += new System.Timers.ElapsedEventHandler(this.timer_Elapsed);
                timer.Start();
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        private static DataTable ListToDataTable<T>(List<T> items)
        {
            DataTable dataTable = new DataTable(typeof(T).Name);
            try
            {
                //Get all the properties
                PropertyInfo[] Props = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);
                foreach (PropertyInfo prop in Props)
                {
                    if (prop.Name == "TagId" || prop.Name == "ScanningTime" || prop.Name == "RawData")
                    {
                        //Setting column names as Property names
                        dataTable.Columns.Add(prop.Name);
                    }
                }
                foreach (T item in items)
                {
                    var values = new object[3];
                    values[0] = Props[2].GetValue(item, null);
                    values[1] = Props[3].GetValue(item, null);
                    values[2] = Props[5].GetValue(item, null);
                    dataTable.Rows.Add(values);
                }
                return dataTable;
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
            return dataTable;
        }

        private bool SaveTable(DataTable dt_Rfid, string SerailNo)
        {
            try
            {
                rfidBusiness.AssociateRFIDtoPhotosAdvance(dt_Rfid, SerailNo);
                return true;
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
            return false;
        }

        private List<RFIDField> GetSubStoreList()
        {
            List<RFIDField> lst = new List<RFIDField>();
            try
            {

                return lst = rfidBusiness.GetSubStoreList();
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
            return lst;
        }

        private void AssociateRFIDtoPhotosAdvance()
        {
            //List<RFIDField> lstSubstore = new List<RFIDField>();
            //lstSubstore = GetSubStoreList();
            //DataTable dtlst = new DataTable();
            //string SerialNo = string.Empty;

            //foreach (RFIDField item in lstSubstore)
            //{
            //    string hotfolder = item.HotFolderpath;
            //    DirectoryInfo _objHotFolder = new DirectoryInfo(System.IO.Path.Combine(item.HotFolderpath, "MobileTags"));

            //    if (_objHotFolder.Exists)
            //    {
            //        FileInfo[] fileEntries = _objHotFolder.GetFiles("*.txt");
            //        DirectoryInfo _objSubHotFolder = new DirectoryInfo(System.IO.Path.Combine(item.HotFolderpath, "MobileTags", "Processed"));
            //        if (!_objSubHotFolder.Exists)
            //            _objSubHotFolder.Create();

            //        try
            //        {
            //            foreach (FileInfo filename in fileEntries)
            //            {
            //                if (filename.Name.Contains('_'))
            //                {
            //                    if (filename.Name.Contains(item.SerialNo))
            //                    {
            //                        string file = filename.FullName;//.DirectoryName + "\\" + filename.Name.ToString();
            //                        var pos = file.IndexOf("MobileTags");
            //                        string des = file.Substring(pos + 11, file.Length - pos - 11);
            //                        string txtname = des;
            //                        des = _objSubHotFolder.FullName + "\\" + des;
            //                        var SerialNumber = txtname.Split('_');
            //                        SerialNo = SerialNumber[0];
            //                        List<RFIDTagInfo> rfidTagGlobal = new List<RFIDTagInfo>();
            //                        using (StreamReader r = new StreamReader(file))
            //                        {
            //                            string line;
            //                            while ((line = r.ReadLine()) != null)
            //                            {
            //                                if (!String.IsNullOrWhiteSpace(line))
            //                                {
            //                                    string[] result = line.Split(',');
            //                                    // YYYYMMDDHHMMSS 20160329171041
            //                                    string dateTime = result[1];
            //                                    string year = dateTime.Substring(0, 4);
            //                                    string month = dateTime.Substring(4, 2);
            //                                    string day = dateTime.Substring(6, 2);
            //                                    string hour = dateTime.Substring(8, 2);
            //                                    string min = dateTime.Substring(10, 2);
            //                                    string sec = dateTime.Substring(12, 2);
            //                                    string spantime = year + "/" + month + "/" + day + " " + hour + ":" + min + ":" + sec;
            //                                    rfidTagGlobal.Add(new RFIDTagInfo { TagId = result[0], ScanningTime = Convert.ToDateTime(spantime) });
            //                                    result = null;
            //                                }
            //                            }
            //                        }
            //                        // Convert list to DataTable.
            //                        dtlst = ListToDataTable(rfidTagGlobal);
            //                        SaveTable(dtlst, SerialNo);
            //                        //first, delete target file if exists, as File.Move() does not support overwrite
            //                        if (File.Exists(des))
            //                        {
            //                            File.Delete(des);
            //                        }
            //                        File.Move(file, des);
            //                    }
            //                }
            //            }
            //        }
            //        catch (Exception ex)
            //        {
            //            string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
            //            errorMessage = errorMessage + "Failing for this file : " + SerialNo;
            //            ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            //        }
            //    }
            //}
            AssociateRFIDtoPhotosAdvanceTableWorkFlow();
        }

        private void AssociateRFIDtoPhotosAdvanceTableWorkFlow()
        {
            List<RFIDField> lstSubstore = new List<RFIDField>();
            lstSubstore = GetSubStoreList();
            DataTable dtlst = new DataTable();
            string SerialNo = string.Empty;

            foreach (RFIDField item in lstSubstore)
            {
                string hotfolder = item.HotFolderpath;
                DirectoryInfo _objHotFolder = new DirectoryInfo(System.IO.Path.Combine(item.HotFolderpath, "MobileTags"));

                if (_objHotFolder.Exists)
                {
                    FileInfo[] fileEntries = _objHotFolder.GetFiles("*.txt");
                    DirectoryInfo _objSubHotFolder = new DirectoryInfo(System.IO.Path.Combine(item.HotFolderpath, "MobileTags", "Processed"));
                    if (!_objSubHotFolder.Exists)
                        _objSubHotFolder.Create();

                    try
                    {
                        foreach (FileInfo filename in fileEntries)
                        {
                            if (filename.Name.Contains('_'))
                            {
                                if (filename.Name.Contains(item.SerialNo))
                                {
                                    string file = filename.FullName;//.DirectoryName + "\\" + filename.Name.ToString();
                                    var pos = file.IndexOf("MobileTags");
                                    string des = file.Substring(pos + 11, file.Length - pos - 11);
                                    string txtname = des;
                                    des = _objSubHotFolder.FullName + "\\" + des;
                                    var SerialNumber = txtname.Split('_');
                                    SerialNo = SerialNumber[0];
                                    List<RFIDTagInfo> rfidTagGlobal = new List<RFIDTagInfo>();
                                    using (StreamReader r = new StreamReader(file))
                                    {
                                        string line;
                                        while ((line = r.ReadLine()) != null)
                                        {
                                            if (!String.IsNullOrWhiteSpace(line))
                                            {
                                                string[] result = line.Split(',');
                                                // YYYYMMDDHHMMSS 20160329171041
                                                string dateTime = result[1];
                                                string year = dateTime.Substring(0, 4);
                                                string month = dateTime.Substring(4, 2);
                                                string day = dateTime.Substring(6, 2);
                                                string hour = dateTime.Substring(8, 2);
                                                string min = dateTime.Substring(10, 2);
                                                string sec = dateTime.Substring(12, 2);
                                                //string spantime = year + "/" + month + "/" + day + " " + hour + ":" + min + ":" + sec;

                                                string spantime = year + "-" + month + "-" + day + " " + hour + ":" + min + ":" + sec;
                                                DateTime dt = Convert.ToDateTime(spantime);
                                                ///////made change by latika ruke for remove ATP QRCode url from tag
                                                string[] strTag = result[0].Split('=');
                                                string tagid = string.Empty;
                                                if (strTag.Length > 1) { tagid = strTag[strTag.Length - 1]; }
                                                else tagid = result[0];
                                                ///end
                                                if (result.Length > 2)
                                                {

                                                    rfidTagGlobal.Add(new RFIDTagInfo { TagId = tagid, ScanningDatetime = (spantime), TableName = result[2], Name = DigiPhoto.CryptorEngine.Encrypt(result[3], true), Email = DigiPhoto.CryptorEngine.Encrypt(result[4], true), Contacts = DigiPhoto.CryptorEngine.Encrypt(result[5], true) });
                                                }
                                                else { rfidTagGlobal.Add(new RFIDTagInfo { TagId = tagid, ScanningDatetime = (spantime) }); }
                                                result = null;
                                            }
                                        }
                                    }
                                    // Convert list to DataTable.
                                    dtlst = ListToDataTableWorkFlow(rfidTagGlobal);
                                    SaveTableTablwFlow(dtlst, SerialNo);
                                    File.Move(file, des);
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                        errorMessage = errorMessage + "Failing for this file : " + SerialNo;
                        ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                    }
                }
            }
        }
        private void timer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            try
            {
                timer.Stop();
                //if(IsRfidEnabled)
                //{
                //AssociateRFIDtoPhotosAdvance();
                AssociateRFIDtoPhotosAdvanceTableWorkFlow();
                    rfidBusiness.AssociateRFIDtoPhotos();
                //}
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
            finally
            {
                timer.Start();
            }
        }
        protected override void OnStop()
        {
            timer.Stop();
            timer = null;
            ServicePosInfoBusiness svcPosinfoBusiness = new ServicePosInfoBusiness();
            svcPosinfoBusiness.StopService(false);
        }
        public void CallAction()
        {
            AssociateRFIDtoPhotosAdvanceTableWorkFlow();
            //AssociateRFIDtoPhotosAdvance();
            //  rfidBusiness.AssociateRFIDtoPhotos();
        }
        //void GetConfigData()
        //{
        //    try
        //    {
        //        //GetSubStoreId();
        //        if (SubStoreId > 0)
        //        {
        //            //DigiPhotoDataServices _objDataLayer = new DigiPhotoDataServices();
        //            List<long> filter = new List<long>();
        //            filter.Add((long)DigiPhoto.DataLayer.ConfigParams.IsEnabledRFID);                    
        //            //List<iMIXConfigurationValue> _objdata = _objDataLayer.GetNewConfigValues(SubStoreId).Where(o => filter.Contains(o.IMIXConfigurationMasterId)).ToList();
        //            List<iMIXConfigurationInfo> _objdata = (new DigiPhoto.IMIX.Business.ConfigBusiness()).GetNewConfigValues(SubStoreId).Where(o => filter.Contains(o.IMIXConfigurationMasterId)).ToList();
        //            foreach (iMIXConfigurationInfo item in _objdata)
        //            {
        //                switch ((int)item.IMIXConfigurationMasterId)
        //                {
        //                    case (int)ConfigParams.IsEnabledRFID:
        //                        IsRfidEnabled = Convert.ToBoolean(item.ConfigurationValue);
        //                        break;                           
        //                }
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
        //        ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
        //    }
        //}
        //void GetSubStoreId()
        //{
        //    //Worker Process
        //    string pathtosave = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
        //    if (File.Exists(pathtosave + "\\ss.dat"))
        //    {
        //        string line;
        //        string SbStoreId;
        //        using (StreamReader reader = new StreamReader(pathtosave + "\\ss.dat"))
        //        {
        //            line = reader.ReadLine();
        //            SbStoreId = DigiPhoto.CryptorEngine.Decrypt(line, true);
        //            SubStoreId = Convert.ToInt32(SbStoreId.Split(',')[0]);
        //        }
        //    }
        //}

        private bool SaveTableTablwFlow(DataTable dt_Rfid, string SerailNo)
        {
            try
            {
                rfidBusiness.AssociateRFIDtoPhotosAdvanceTableFlow(dt_Rfid, SerailNo);
                return true;
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
            return false;
        }
        private static DataTable ListToDataTableWorkFlow<T>(List<T> items)
        {
            DataTable dataTable = new DataTable(typeof(T).Name);
            try
            {
                //Get all the properties
                PropertyInfo[] Props = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);
                foreach (PropertyInfo prop in Props)
                {
                    if (prop.Name == "TagId" || prop.Name == "ScanningTime" || prop.Name == "RawData" || prop.Name == "TableName" || prop.Name == "Name" || prop.Name == "Email" || prop.Name == "Contacts" || prop.Name == "ScanningDatetime")
                    {
                        //Setting column names as Property names
                        dataTable.Columns.Add(prop.Name);
                    }
                }
                foreach (T item in items)
                {
                    var values = new object[8];
                    values[0] = Props[2].GetValue(item, null);
                    values[1] = Props[3].GetValue(item, null);
                    values[2] = Props[5].GetValue(item, null);
                    values[3] = Props[10].GetValue(item, null);
                    values[4] = Props[11].GetValue(item, null);
                    values[5] = Props[12].GetValue(item, null);
                    values[6] = Props[13].GetValue(item, null);
                    values[7] = Props[14].GetValue(item, null);
                    dataTable.Rows.Add(values);
                }
                return dataTable;
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
            return dataTable;
        }
    }
}
