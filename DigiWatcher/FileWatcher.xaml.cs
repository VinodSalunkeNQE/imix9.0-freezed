﻿using System;
using System.Collections.Generic;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Windows;
using System.Windows.Media.Imaging;
using System.IO;
using System.Drawing.Imaging;
using System.Drawing;
using System.Windows.Threading;
using System.Xml;
using DigiPhoto.DataLayer;
using DigiPhoto;
using System.Runtime.InteropServices;
using DigiPhoto.Common;
using LevDan.Exif;
using ExifLib;
using System.Diagnostics;
using System.ServiceProcess;
using BarcodeReaderLib;
using System.Reflection;
using DigiPhoto.DataSync.Controller;
using DigiPhoto.DigiSync.Model;
using DigiPhoto.DigiSync.ServiceLibrary.Interface;
using DigiAuditLogger;
using FrameworkHelper;
using DigiPhoto.IMIX.Model;
using DigiPhoto.IMIX.Business;
using System.Threading.Tasks;
using DigiPhoto.Utility.Repository.ValueType;
using System.Security.Cryptography;
using System.Text;
using System.Configuration;
using System.Threading;
using System.Data;
using System.Runtime.CompilerServices;
using System.ComponentModel;
using log4net;
//using ClosedXML.Excel;

namespace DigiWatcher
{
    public partial class FileWatcher : Window
    {

        #region Declaration
        private static ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        string[] mediaExtensions = new[] { ".jpg", ".avi", ".mp4", ".wmv", ".mov", ".3gp", ".3g2", ".m2v", ".m4v", ".flv", ".mpeg", ".ffmpeg" };
        public delegate void NextPrimeDelegate();
        static bool isrotated = false;
        ReadImageMetaData _objmetdata;
        public static Int32 counter;
        private DispatcherTimer timer;
        private DispatcherTimer progressTimer;
        private List<SemiOrderSettings> lstDG_SemiOrder_Settings;
        private SemiOrderSettings objDG_SemiOrder_Settings;
        string Directoryname = string.Empty;
        string DirectoryNameWithDate = string.Empty;
        string _thumbnailFilePathDate = string.Empty;
        string _bigThumbnailFilePathDate = string.Empty;
        string _minifiedFilePathDate = string.Empty;
        private String _BorderFolder;
        private bool is_SemiOrder;
        //DigiPhotoDataServices objDigiPhotoDataServices;
        string defaultBrightness = string.Empty;
        string defaultContrast = string.Empty;
        string UniqueCode = string.Empty;
        string codeFormat = string.Empty;
        int substoreId = 0;
        //int LocationID = 0;
        int mediaType = 1;
        bool IsBarcodeActive = false;
        bool IsMobileRfidEnabled = false;
        Int32 MappingType = 0;
        Int32 scanType = 0;
        Int32 GapTimeInSec = 0;
        Boolean IsAnonymousCodeActive = false;
        Boolean IsAutoPurchaseActive = false;
        string QRWebURLReplace = string.Empty;
        List<ImgInfoPostScanList> lstImgInfoPostScanList = null;
        List<ImgInfoPostScanList> lstImgInfoPreScanList = null;
        Int32 PhotoGrapherId = 0;
        List<PreScanWatcher> PreScanWatcherList = null;
        CardLimitInfo objCardLimit = new CardLimitInfo();
        bool IsOnline = false;
        int CardImageLimit = 0;
        int CardImageSold = 0;
        public bool IsQrCodeUsed = false;
        int rest = 0;
        DateTime lastOrderChecktime;
        DateTime lastmemoryUpdateTime = DateTime.Now;
        int? RfidScanType = null;
        bool isSpecCropActive = false;
        string GraphicInfo = string.Empty;
        bool IsSpecBrightActive = false;
        double SpecBright = 0;
        double SpecContrast = 1;
        public string DownloadFolderPath;
        public string ArchivedFolderPath;
        Boolean isResetDPIRequired = false;
        bool isAutoVideoProcessingActive = false;
        public static int videoCount = 0;
        bool _IsAdvancedVideoEditActive = false;
        string videoFramePositions = string.Empty;
        string cropVideoFrameRatio = string.Empty;
        bool IsGreenScreenWorkFlow = false;
        public bool IsAutoColorCorrectionActive = false;
        bool IsCorrectionAtDownloadActive = false;
        bool IsContrastCorrectionActive = false;
        bool IsGammaCorrectionActive = false;
        bool IsNoiseReductionActive = false;
        public List<String> FilesToDelete = new List<string>();
        bool isVideoProcessed = true;
        VideoSceneBusiness VBusiness = new VideoSceneBusiness();
        List<Watchersetting> watchersetting = new List<DigiPhoto.IMIX.Model.Watchersetting>();
        List<ConfigurationInfo> ConfigurationData = null;
        List<iMixConfigurationLocationInfoList> NewConfigList = null;
        List<SemiOrderSettingsList> SemiOrderSettingList = null;
        List<UsersInfo> UserInfoList = null;

        string OriginalFileName = string.Empty;
        int ParentImageID = -1;
        //int imagenameCntr = 0;
        private List<iMixConfigurationLocationInfo> _lstLocationWiseConfigParams;
        private List<iMixConfigurationLocationInfo> _lstPreviewWallLocation;
        List<ImgCopyLocation> imgCopyLocationlst = new List<ImgCopyLocation>();
        private string _previewWallSubStore;
        private string _copyFolderName;
        private string _locationName;
        private string screen = string.Empty;
        string GumBallRideFolderPath = string.Empty;
        string _subStoreName = string.Empty;
        string GumBallRidetxtContent = string.Empty;
        List<long> GumRideList;
        List<iMixConfigurationLocationInfo> LstRideConfigValueLocationWise = new List<iMixConfigurationLocationInfo>();
        List<iMixConfigurationLocationInfo> LstGumBallRideConfigValueLocationWise = new List<iMixConfigurationLocationInfo>();
        List<iMixConfigurationLocationInfo> GumBallConfigurationValueList = new List<iMixConfigurationLocationInfo>();
        List<long> GumBallActiveLocationList = new List<long>();
        string isVisibleZeroScoreOnImage = "False";

        string gumballScoreSeperater = ",";
        string gumballScore = string.Empty;

        public string _QRCode = null;

        string picNameMinified = string.Empty;

        Int32 _QRCodeLenth = 8;
        //DataTable threadSummary;
        private int MaxThreadCount;
        private string restartTime;
        private bool logprint = true;
        int ImageProcessingCount = 0;
        AuditLogBusiness auditLogBusiness = new AuditLogBusiness();
        PhotoBusiness photoBusiness = new PhotoBusiness();

        public string ImageMetaData
        {
            get;
            set;
        }
        string ProductNamePreference = string.Empty;

        #endregion

        #region Constructor
        /// <summary>
        /// Initializes a new instance of the <see cref="FileWatcher"/> class.
        /// </summary>
        public FileWatcher()
        {
            try
            {
                log4net.Config.XmlConfigurator.Configure();
                log.Info("Watcher Service Started");

                if (ConfigurationManager.AppSettings.AllKeys.Contains("MaxThreadCount") == true)
                {
                    MaxThreadCount = Convert.ToInt32(ConfigurationManager.AppSettings["MaxThreadCount"]);
                }
                if (ConfigurationManager.AppSettings.AllKeys.Contains("RestartAppTime") == true)
                {
                    restartTime = ConfigurationManager.AppSettings["RestartAppTime"];
                }
                if (ConfigurationManager.AppSettings.AllKeys.Contains("Logprint") == true)
                {
                    logprint = Convert.ToBoolean(ConfigurationManager.AppSettings["Logprint"]);
                }
                ServicePosInfoBusiness svcPosinfoBusiness = new ServicePosInfoBusiness();
                string ret = svcPosinfoBusiness.ServiceStart(true);
                //ServicePosInfoBusiness svcPosinfoBusiness = new ServicePosInfoBusiness();
                // int ret = svcPosinfoBusiness.ServiceStart(false);

                if (!string.IsNullOrEmpty(ret))
                {
                    MessageBox.Show("This application is already running on " + ret);
                    Environment.Exit(0);
                    // return;
                }
                //ShowMessage("File Watcher Step 3. startup :");
                InitializeComponent();

                this.Left = 0;
                this.Top = 0;
                ConfigurationData = new List<ConfigurationInfo>();
                lstImgInfoPostScanList = new List<ImgInfoPostScanList>();
                lstImgInfoPreScanList = new List<ImgInfoPostScanList>();
                PreScanWatcherList = new List<PreScanWatcher>();
                NewConfigList = new List<iMixConfigurationLocationInfoList>();
                SemiOrderSettingList = new List<SemiOrderSettingsList>();
                UserInfoList = new List<UsersInfo>();
                RobotImageLoader.GetConfigData();
                substoreId = LoginUser.SubStoreId;
                SetNewConfigValues();
                GetStoreName();
                Directoryname = LoginUser.DigiFolderPath;
                _subStoreName = LoginUser.SubstoreName;
                SetGumRideMasterId();
                //Create Datewise folder to store image, thumbnail and bigthumbnail
                DirectoryNameWithDate = Path.Combine(Directoryname, DateTime.Now.ToString("yyyyMMdd"));
                _thumbnailFilePathDate = Path.Combine(Directoryname, "Thumbnails", DateTime.Now.ToString("yyyyMMdd"));
                _bigThumbnailFilePathDate = Path.Combine(Directoryname, "Thumbnails_Big", DateTime.Now.ToString("yyyyMMdd"));
                _minifiedFilePathDate = Path.Combine(Directoryname, "Minified_Images", DateTime.Now.ToString("yyyyMMdd"));
                if (!Directory.Exists(DirectoryNameWithDate))
                    Directory.CreateDirectory(DirectoryNameWithDate);
                if (!Directory.Exists(_thumbnailFilePathDate))
                    Directory.CreateDirectory(_thumbnailFilePathDate);
                if (!Directory.Exists(_bigThumbnailFilePathDate))
                    Directory.CreateDirectory(_bigThumbnailFilePathDate);
                if (!Directory.Exists(_minifiedFilePathDate))
                    Directory.CreateDirectory(_minifiedFilePathDate);
                //Function will fill all substore configuration
                GetConfigDataList();
                //ShowMessage("File Watcher Step 4. startup :");
                StoreSubStoreDataBusniess storeObj = new StoreSubStoreDataBusniess();
                QRWebURLReplace = storeObj.GetQRCodeWebUrl();
                //objDigiPhotoDataServices = new DigiPhotoDataServices();
                //QRWebURLReplace = objDigiPhotoDataServices.GetQRCodeWebUrl();
                //ShowMessage("Semi Order Setting Completed");

                DigiPhoto.IMIX.Model.StoreInfo store = storeObj.GetStore();
                if (store.RunApplicationsSubStoreLevel)
                {
                    string SubStoreName = string.Empty;
                    //Set current SubStoreId
                    try
                    {
                        //ShowMessage("File Watcher Step 5. startup :");
                        //string pathtosave = Environment.CurrentDirectory;
                        //ShowMessage(" Environment.CurrentDirectory:" + Environment.CurrentDirectory);
                        string pathtosave = @"C:\Program Files (x86)\iMix"; //Bhavin change during FileWatcher Restart Utility 
                        //ShowMessage("File Watcher Step 5-1. startup :" + pathtosave);
                        //Bhavin edit
                        if (File.Exists(pathtosave + "\\ss.dat"))
                        {
                           // ShowMessage("File Watcher Step 5-2. startup :");
                            string line;
                            using (StreamReader reader = new StreamReader(pathtosave + "\\ss.dat"))
                            {
                                line = reader.ReadLine();
                                //ShowMessage("File Watcher Step 5-3. startup :");
                                string subID = DigiPhoto.CryptorEngine.Decrypt(line, true);
                                int subStoreId = (subID.Split(',')[0]).ToInt32();
                                SubStoreName = (new StoreSubStoreDataBusniess()).GetSubstoreNameById(subStoreId);
                                if (string.IsNullOrEmpty(SubStoreName))
                                {
                                    SubStoreName = "NQDEVENV";
                                    ShowMessage("FileWatcher Line 268:Site not found.Please update site details with respective site code.");
                                }
                                QRCodeLength(subStoreId);
                                _lstLocationWiseConfigParams = new List<iMixConfigurationLocationInfo>();
                                _lstLocationWiseConfigParams = (new ConfigBusiness()).GetLocationWiseConfigParams(subStoreId);

                                _lstPreviewWallLocation = _lstLocationWiseConfigParams.Where(s => s.IMIXConfigurationMasterId == Convert.ToDouble(ConfigParams.NoOfScreenPW)).ToList();
                                GumBallConfigurationValueList = _lstLocationWiseConfigParams.Where(y => GumRideList.Contains(y.IMIXConfigurationMasterId)).ToList();
                                GumBallActiveLocationList = GumBallConfigurationValueList.Where(n => n.IMIXConfigurationMasterId == (long)ConfigParams.IsGumRideActive
                        && n.ConfigurationValue.ToUpper() == "TRUE").Select(x => Convert.ToInt64(x.LocationId)).ToList();
                                if (_lstPreviewWallLocation != null && _lstPreviewWallLocation.Count > 0)
                                {
                                    ImgCopyLocation imgCopyLocation;
                                    foreach (iMixConfigurationLocationInfo PreviewWallLocation in _lstPreviewWallLocation)
                                    {
                                        imgCopyLocation = new ImgCopyLocation();
                                        imgCopyLocation.locID = PreviewWallLocation.LocationId;
                                        imgCopyLocation.counter = 1;
                                        imgCopyLocation.range = Convert.ToInt32(PreviewWallLocation.ConfigurationValue);
                                        imgCopyLocationlst.Add(imgCopyLocation);
                                    }
                                }
                                _previewWallSubStore = Directoryname + "\\PreviewWall\\" + SubStoreName;
                                //Added by Suraj . For Check Configuration Sequvetional or Parllel.
                                string ConfigValue = photoBusiness.GetFileWatcherConfigValues(subStoreId);
                                if (ConfigValue != "True")
                                {
                                    MessageBox.Show("Please select the Confguration from Config Utility.");
                                    Application.Current.Shutdown();
                                }
                                //End Changes.
                                //ShowMessage("File Watcher Step 6. startup :");
                            }
                        }
                        else
                        {
                            MessageBox.Show("Please select substore from Configuration Section in Imix for this machine.");
                            Application.Current.Shutdown();
                        }
                    }
                    catch (Exception ex)
                    {
                        ErrorHandler.ErrorHandler.LogError(ex);
                    }
                    DownloadFolderPath = Directoryname + "\\Download\\" + SubStoreName;
                    ArchivedFolderPath = Directoryname + "\\Archived\\" + SubStoreName;
                    if (!Directory.Exists(ArchivedFolderPath))
                    {
                        Directory.CreateDirectory(ArchivedFolderPath);
                    }
                }
                else
                {
                    DownloadFolderPath = Directoryname + "\\Download";
                    ArchivedFolderPath = Directoryname + "\\Archived";
                }
                //ShowMessage("File Watcher Step 7. Constructer :");
                DeleteArchviedFiles();
                Task t2 = new Task(DeleteTempFiles);
                t2.Start();
                //ShowMessage("File Watcher Step 8. Constructer :");
                txtProcessing.Text = string.Empty;
                lastOrderChecktime = DateTime.Now;
                timer = new DispatcherTimer();
                timer.Interval = TimeSpan.FromSeconds(1);
                timer.Tick += timer1_Tick;
                timer.Start();
                //ShowMessage("File Watcher Step 9. Constructer :");
                //If Watcher is running on server.
                //if (!store.RunApplicationsSubStoreLevel)
                //{
                try
                {
                    bool isProcessFound = false;
                    Process[] processes = Process.GetProcesses();
                    foreach (Process process in processes)
                    {
                        if (process.ProcessName == "DigiWifiImageProcessing")
                        {
                            try
                            {
                                isProcessFound = true;
                                ServiceController controller1 = new ServiceController("DigiWifiImageProcessing");
                                if (controller1.Status == ServiceControllerStatus.Stopped)
                                {
                                    //ShowMessage("339 : ServiceControllerStatus.Stopped");
                                    ServiceController controller = new ServiceController("DigiWifiImageProcessing");
                                    controller.Start();
                                }
                                else
                                {
                                    ShowMessage("controller1.Status:" + controller1.Status.ToString());
                                }
                            }
                            catch (Exception ex)
                            {
                                //handle any exception here
                                ShowMessage("Wifi service start error");
                                ErrorHandler.ErrorHandler.LogError(ex);
                            }
                        }
                    }
                    if (!isProcessFound)
                    {
                        ServiceController controller = new ServiceController("DigiWifiImageProcessing");
                        controller.Start();
                        ShowMessage("Line no 360: DigiWifiImageProcessing services started");
                    }

                }
                catch (Exception ex)
                {
                    ErrorHandler.ErrorHandler.LogError(ex);
                }
                //}
                //ShowMessage("File Watcher Step 10. Constructer :");
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
            }
        }
        #endregion

        private void DeleteArchviedFiles()
        {
            DirectoryInfo drinfo = new DirectoryInfo(ArchivedFolderPath);
            try
            {
                foreach (var item in drinfo.GetFiles())
                {
                    try
                    {
                        item.Delete();
                    }
                    catch { }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ShowMessage(errorMessage);
            }
            finally
            {
                GC.Collect();
            }
        }
        private void DeleteTempFiles()
        {

            var tempThumbnailsPath = Path.Combine(Directoryname, "Thumbnails\\Temp");

            if (!Directory.Exists(tempThumbnailsPath))
                Directory.CreateDirectory(tempThumbnailsPath);

            DirectoryInfo dir = new DirectoryInfo(tempThumbnailsPath);//Directoryname + "\\Thumbnails\\Temp");
            FileInfo[] FInfoLst = dir.GetFiles("*jpg", SearchOption.AllDirectories);
            try
            {
                foreach (FileInfo FInfo in FInfoLst)
                {
                    try
                    {
                        FInfo.Delete();
                    }
                    catch { }
                }

                if (!Directory.Exists(Path.Combine(Directoryname, "Download", "CorruptImages")))
                    Directory.CreateDirectory(Path.Combine(Directoryname, "Download", "CorruptImages"));
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ShowMessage(errorMessage);
                //ShowMessage("File Watcher Step 14- Catch block. DeleteTemp Files  :" + DateTime.Now.ToString("MM / dd / yyyy HH: mm:ss"));
            }
            finally
            {
                GC.Collect();
            }
        }

        #region SemiOrder

        /// <summary>
        /// Gets the semi order settings.
        /// </summary>
        /// <returns></returns>
        private async Task<List<SemiOrderSettings>> GetSemiOrderSettings(int substoreId, int LocationId)
        {
            try
            {
                List<SemiOrderSettings> objDG_SemiOrder_Settings = new List<SemiOrderSettings>();

                if (lstDG_SemiOrder_Settings != null && lstDG_SemiOrder_Settings.Count > 0)
                {
                    objDG_SemiOrder_Settings = lstDG_SemiOrder_Settings.Where(x => x.DG_LocationId == LocationId).ToList();
                }
                else
                {
                    lstDG_SemiOrder_Settings = (new SemiOrderBusiness()).GetSemiOrderSetting(null, 0);
                    objDG_SemiOrder_Settings = lstDG_SemiOrder_Settings.Where(x => x.DG_LocationId == LocationId).ToList();
                }
                return objDG_SemiOrder_Settings;
            }
            catch (Exception ex)
            {
                ShowMessage("Exception : Get Semi Order Settings : Msg : " + ex.Message);
                return null;
            }
        }
        private async Task SaveSpecPrintEffectsIntoDb(int photoId, int substoreId, int locationId, SemiOrderSettings objDG_SemiOrder_Settings, string dateFolderPath
            , string fileName, string GumBallRidetxtContent, string ImageEffect, int LocationID)
        {
            //objDigiPhotoDataServices = new DigiPhotoDataServices();
            PhotoBusiness phBusiness = new PhotoBusiness();
            if (objDG_SemiOrder_Settings.DG_SemiOrder_Settings_AutoBright == false && !string.IsNullOrEmpty(defaultBrightness))
            {
                objDG_SemiOrder_Settings.DG_SemiOrder_Settings_AutoBright_Value = Convert.ToDouble(defaultBrightness);
            }

            if (objDG_SemiOrder_Settings.DG_SemiOrder_Settings_AutoContrast == false && !string.IsNullOrEmpty(defaultBrightness))
            {
                objDG_SemiOrder_Settings.DG_SemiOrder_Settings_AutoContrast_Value = Convert.ToDouble(defaultContrast);
            }
            //int prdId = 0;
            //if (objDG_SemiOrder_Settings.DG_SemiOrder_ProductTypeId.Contains(','))
            //    prdId = 1;
            //else
            //    prdId = Convert.ToInt32(objDG_SemiOrder_Settings.DG_SemiOrder_ProductTypeId);

            //if ((bool)objDG_SemiOrder_Settings.DG_SemiOrder_Settings_IsImageBG)
            //{
            //    //phBusiness.SaveIsGreenPhotos(photoId, true);
            //    SaveXml(Convert.ToString(objDG_SemiOrder_Settings.DG_SemiOrder_Settings_AutoBright_Value),
            //        Convert.ToString(objDG_SemiOrder_Settings.DG_SemiOrder_Settings_AutoContrast_Value),
            //        Convert.ToString(objDG_SemiOrder_Settings.DG_SemiOrder_Settings_ImageFrame),
            //        objDG_SemiOrder_Settings.DG_SemiOrder_Settings_ImageFrame_Vertical,
            //        objDG_SemiOrder_Settings.ProductName,
            //        objDG_SemiOrder_Settings.DG_SemiOrder_BG, objDG_SemiOrder_Settings.DG_SemiOrder_Graphics_layer,
            //        photoId, objDG_SemiOrder_Settings.DG_SemiOrder_Image_ZoomInfo,
            //        (bool)objDG_SemiOrder_Settings.DG_SemiOrder_IsCropActive,
            //        prdId, dateFolderPath, true, fileName, GumBallRidetxtContent, objDG_SemiOrder_Settings.TextLogos);
            //}
            //else
            //{
            //    SaveXml(Convert.ToString(objDG_SemiOrder_Settings.DG_SemiOrder_Settings_AutoBright_Value),
            //        Convert.ToString(objDG_SemiOrder_Settings.DG_SemiOrder_Settings_AutoContrast_Value),
            //        Convert.ToString(objDG_SemiOrder_Settings.DG_SemiOrder_Settings_ImageFrame),
            //        objDG_SemiOrder_Settings.DG_SemiOrder_Settings_ImageFrame_Vertical,
            //        objDG_SemiOrder_Settings.ProductName, string.Empty, objDG_SemiOrder_Settings.DG_SemiOrder_Graphics_layer,
            //        photoId, objDG_SemiOrder_Settings.DG_SemiOrder_Image_ZoomInfo,
            //        (bool)objDG_SemiOrder_Settings.DG_SemiOrder_IsCropActive,
            //        prdId, dateFolderPath, false, fileName, GumBallRidetxtContent, objDG_SemiOrder_Settings.TextLogos);
            //}

            await SaveXml(objDG_SemiOrder_Settings, photoId, dateFolderPath, fileName, GumBallRidetxtContent, ImageEffect, LocationID);

        }

        #endregion

        #region watcher
        private void CopyImgToDisplayFolder(string imgSourceFilePath, string RFID, Int32 photoId, int LocationID)
        {
            ImgCopyLocation targetFolderLocation = imgCopyLocationlst.FirstOrDefault(s => s.locID == LocationID);
            if (targetFolderLocation.counter <= targetFolderLocation.range)
            {
                screen = "Display" + targetFolderLocation.counter;
                imgCopyLocationlst.Where(s => s.locID == LocationID).ToList().ForEach(s => s.counter++);
            }
            else
            {
                screen = "Display1";
                imgCopyLocationlst.Where(s => s.locID == LocationID).ToList().ForEach(s => s.counter = 2);
            }
            if (Directory.Exists(_previewWallSubStore + "\\" + _locationName + "\\" + screen))
            {
                _copyFolderName = Path.Combine(_previewWallSubStore, _locationName, screen, (RFID + "@" + photoId + ".jpg"));
                File.Copy(imgSourceFilePath, _copyFolderName, true);
            }
        }

        //string ImageEffect;
        List<string> _objFileList = new List<string>();
        //int needrotaion = 0; // Commented by Suraj for Rotate Image Issue.

        /// <summary>
        /// Gets the rotation value.
        /// </summary>
        /// <param name="orientation">The orientation.</param>
        /// <returns></returns>
        private static int GetRotationValue(string orientation)
        {
            switch (int.Parse(orientation))
            {
                case 1:
                    return 0;

                case 2:
                    return 0;

                case 3:
                    return 180;

                case 4:
                    return 180;

                case 5:
                    return 90;

                case 6:
                    return 90;

                case 7:
                    return 270;

                case 8:
                    return 270;

                default:
                    return 0;
            }
        }

        private void rotateImageLocationWise(string rotatePath)
        {
            try
            {
                ImageMagickObject.MagickImage jmagic = new ImageMagickObject.MagickImage();
                object[] o = new object[] { "-rotate", System.Configuration.ConfigurationManager.AppSettings["RotationAngle"], rotatePath };
                jmagic.Mogrify(o);
                o = null;
                isrotated = true;
                jmagic = null;
            }
            catch (Exception ex)
            {
                ShowMessage("Exception - rotateImageLocationWise :" + ex.Message);
                ErrorHandler.ErrorHandler.LogError(ex);
            }
        }

        //string FileNameDownload = string.Empty;
        bool IsFrame = false;

        private async Task EventRaised(FileInfo file, long displayOrderNumber)
        {
            int imagenameCntr = 0;  //Added By Bhavin on 29 Nov 2021 for Image Sequence Number Issue
            int LocationID = 0;
            string ImageEffect;
            int photoId;
            string threadIdentity = Guid.NewGuid().ToString();
            int threadId = Thread.CurrentThread.ManagedThreadId;
            //ErrorHandler.ErrorHandler.LogFileWrite("551 : " + threadIdentity + "  : ");
            DateTime startTime = DateTime.Now;
            //ShowMessage(threadId.ToString());
            IsFrame = false;
            GumBallRidetxtContent = string.Empty;

            string _mediaType = string.Empty;
            if (file.Name.Contains('#'))
            {
                _mediaType = file.Name;
                _mediaType = _mediaType.Split('#')[1].Split('@')[0];
                IsFrame = true;
            }
            string massage = threadIdentity + "  : media Type : " + _mediaType;
            //ShowMessage(threadId.ToString());
            DateTime? CaptureDate = new DateTime();
            bool isResetDPIRequired = false;
            PhotoGrapherId = Convert.ToInt32(file.Name.Split('@')[1].ToString().Split('.')[0]);
            //Get cahced data if available otherwise load from DB
            //ShowMessage(threadId.ToString());
            UsersInfo userInfo = FillUserData(PhotoGrapherId);
            LocationID = userInfo.DG_Location_pkey;
            GumBallRideFolderPath = Path.Combine(Directoryname, "GumBallRide", _subStoreName, _locationName, DateTime.Now.ToString("yyyyMMdd"));

            if (!Directory.Exists(GumBallRideFolderPath))
                Directory.CreateDirectory(GumBallRideFolderPath);
            //ShowMessage(threadId.ToString());
            FileInfo txtFile = new FileInfo(Path.Combine(DownloadFolderPath, _locationName, file.Name.Split('.')[0] + ".txt"));
            PhotoBusiness phBusiness = new PhotoBusiness();
            string picname = string.Empty; //made global by Vinod_11May2020
            try
            {
                string CameraManufacture = "'" + "##" + "'";
                string CameraModel = "'" + "##" + "'";
                string orientation = "'" + "##" + "'";
                string HorizontalResolution = "'" + "##" + "'";
                string VerticalResolution = "'" + "##" + "'";
                string Datetaken = "'" + "##" + "'";
                string dimension = "'" + "##" + "'";
                string ISOSpeedRatings = "'" + "##" + "'";
                string ExposureMode = "'" + "##" + "'";
                string Sharpness = "'" + "##" + "'";
                string GPSLatitude = "'" + "##" + "'";
                string GPSLongitude = "'" + "##" + "'";
                string ExposureTime = "'" + "##" + "'";         //Shutter speed
                string ApertureValue = "'" + "##" + "'";
                if (file.Name != "Thumbs.db")
                {
                    _objmetdata = new ReadImageMetaData();
                    //ShowMessage(threadId.ToString());
                    //Get cached data
                    GetConfigData(substoreId);
                    //Get cahced data if available otherwise load from DB
                    await SetNewConfigLocationValues(LocationID);
                    ErrorHandler.ErrorHandler.LogFileWrite("Sub-Store ID : " + substoreId + "  Location ID  : " + LocationID + "  ");
                    //Get cahced data if available otherwise load from DB
                    List<SemiOrderSettings> lstobjDG_SemiOrder_Settings;
                    if (is_SemiOrder)
                        lstobjDG_SemiOrder_Settings = await GetSemiOrderSettings(substoreId, LocationID);
                    else
                        lstobjDG_SemiOrder_Settings = null;

                    if (_IsAdvancedVideoEditActive)
                        isVideoProcessed = false;
                    else if (isAutoVideoProcessingActive)
                        isVideoProcessed = false;
                    else isVideoProcessed = true;

                    if (IsGreenScreenWorkFlow)
                        isVideoProcessed = true;
                    //here i will go to loop 
                    //ShowMessage(threadId.ToString());
                    ErrorHandler.ErrorHandler.LogFileWrite("Sub-Store ID : " + substoreId + "  Location ID  : " + LocationID + "  ");
                    try
                    {
                        int photoCount = phBusiness.PhotoCountCurrentDate(RFID: file.Name.Split('@')[0].ToString(), photographerID: Convert.ToInt32(file.Name.Split('@')[1].ToString().Split('.')[0]), mediaType: 1);
                        if ((lstobjDG_SemiOrder_Settings == null || lstobjDG_SemiOrder_Settings.Count() == 0) || (IsFrame & _mediaType == "3"))
                        {
                            ImageEffect = "<image brightness = '" + defaultBrightness + "' contrast = '" + defaultContrast + "' Crop='##' colourvalue = '##' rotate='##' ><effects sharpen='##' greyscale='0' digimagic='0' sepia='0' defog='0' underwater='0' emboss='0' invert = '0' granite='0' hue ='##' cartoon = '0'></effects></image>";
                            CustomBusineses custObj = new CustomBusineses();
                            DateTime curDate = custObj.ServerDateTime();
                            //ShowMessage(threadId.ToString());
                            //string picname = string.Empty;
                            //if (photoCount > 0)
                            //    picname = curDate.Day.ToString() + curDate.Month.ToString() + file.Name.Split('@')[0].ToString() + "(" + photoCount + ")" + ".jpg";
                            //else
                            //    picname = curDate.Day.ToString() + curDate.Month.ToString() + file.Name.Split('@')[0].ToString() + ".jpg";

                            if (photoCount > 0)
                            {
                                //picname = curDate.Day.ToString() + curDate.Month.ToString() + file.Name.Split('@')[0].ToString() + "(" + photoCount + ")" + ".jpg";
                                //Added by Vins_19Feb2019  to resolve same photo series issue for different location
                                picname = curDate.Day.ToString() + curDate.Month.ToString() + Convert.ToString(PhotoGrapherId) + file.Name.Split('@')[0].ToString() + "(" + photoCount + ")" + ".jpg";
                            }
                            else
                            {
                                //picname = curDate.Day.ToString() + curDate.Month.ToString() + file.Name.Split('@')[0].ToString() + ".jpg"; //Commented by Vins
                                //Added by Vins_19Feb2019  to resolve same photo series issue for different location
                                picname = curDate.Day.ToString() + curDate.Month.ToString() + Convert.ToString(PhotoGrapherId) + file.Name.Split('@')[0].ToString() + ".jpg";
                            }
                            //ShowMessage(threadId.ToString());
                            if (picname.Contains('#'))
                            {
                                picname = picname = picname.Split('#')[0] + "_" + _mediaType + ".jpg";
                                isVideoProcessed = true;
                            }
                            string filename = file.Name.Split('@')[0].ToString();
                            string rfid = file.Name.Split('@')[0].ToString();
                            //ShowMessage(threadId.ToString());
                            if (filename.Contains('_'))
                            {
                                filename = filename.Split('_')[0].ToString();
                                rfid = filename.Split('_')[0].ToString();
                                isVideoProcessed = true;
                            }
                            string txtFileName = curDate.Day.ToString() + curDate.Month.ToString() + txtFile.Name.Split('@')[0].ToString() + ".txt";
                            int needrotaion = 0;  // Added by Suraj For Rotate Image Issue
                            string renderedTag = string.Empty;
                            try
                            {
                                #region Image Meta Data
                                ExifTagCollection exif = new ExifTagCollection(file.FullName);

                                #region Auto-Rotate
                                //Start:Code for Auto-Rotate
                                ExifReader reader = null;
                                //string renderedTag = "";
                                try
                                {
                                    reader = new ExifReader(file.FullName);
                                    foreach (ushort tagID in Enum.GetValues(typeof(ExifTags)))
                                    {
                                        object val;
                                        if (reader.GetTagValue(tagID, out val))
                                        {
                                            renderedTag = val.ToString();
                                        }
                                    }
                                }
                                catch (Exception ex)
                                {
                                    //ErrorHandler.ErrorHandler.LogError(ex);
                                    auditLogBusiness.InsertExceptionLog(Convert.ToString(Guid.NewGuid()), 0, DateTime.Now, DateTime.Now, "EventRaised", 0, ex, "689");
                                }
                                finally
                                {
                                    if (reader != null)
                                        reader.Dispose();
                                }
                                //FileNameDownload = Directoryname + "\\Download\\" + file.Name;
                                ErrorHandler.ErrorHandler.LogFileWrite("PhotoId : " + file.FullName + " :  renderedTag : " + renderedTag);
                                ShowMessage(threadId.ToString());
                                if (!string.IsNullOrEmpty(renderedTag))
                                {
                                    needrotaion = GetRotationValue(renderedTag);
                                    isrotated = true;
                                    ErrorHandler.ErrorHandler.LogFileWrite("PhotoId : " + file.FullName + " :  renderedTag : " + renderedTag + "695");
                                }
                                else
                                {
                                    needrotaion = 0;
                                    isrotated = false;
                                }

                                //End:Code for Auto-Rotate
                                #endregion

                                if (exif.Where(o => o.Id == 36867).Count() > 0)
                                {
                                    try
                                    {
                                        System.Globalization.DateTimeFormatInfo objFormat = new System.Globalization.DateTimeFormatInfo();
                                        objFormat.ShortDatePattern = @"yyyy/MM/dd HH:mm:ss";
                                        string datePart = exif[36867].Value.Split(' ').First();
                                        datePart = datePart.Replace(':', '/');
                                        string timePart = exif[36867].Value.Split(' ').Last();
                                        CaptureDate = Convert.ToDateTime(datePart + " " + timePart, objFormat);
                                    }
                                    catch (Exception ex)
                                    {
                                        CaptureDate = null;
                                        ErrorHandler.ErrorHandler.LogError(ex);
                                    }
                                }
                                else if (file.CreationTime != null)
                                {
                                    try
                                    {
                                        System.Globalization.DateTimeFormatInfo objFormat = new System.Globalization.DateTimeFormatInfo();
                                        objFormat.ShortDatePattern = @"yyyy/MM/dd HH:mm:ss";
                                        string datePart = Convert.ToString(file.CreationTime.Date).Split(' ').First();
                                        datePart = datePart.Replace(':', '/');
                                        string timePart = Convert.ToString(file.CreationTime.TimeOfDay);
                                        CaptureDate = Convert.ToDateTime(datePart + " " + timePart, objFormat);
                                    }
                                    catch (Exception ex)
                                    {
                                        CaptureDate = null;
                                        string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                                        ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                                    }
                                }
                                else
                                    CaptureDate = null;
                                ShowMessage(threadId.ToString());
                                foreach (LevDan.Exif.ExifTag tag in exif)
                                {
                                    if (tag.Id == 271)
                                    {
                                        CameraManufacture = "'" + exif[271].Value + "'";
                                    }
                                    else if (tag.Id == 272)
                                    {
                                        CameraModel = "'" + exif[272].Value + "'";
                                    }
                                    else if (tag.Id == 274)
                                    {
                                        orientation = "'" + exif[274].Value + "'";
                                    }
                                    else if (tag.Id == 282)
                                    {
                                        HorizontalResolution = "'" + exif[282].Value + "'";
                                    }
                                    else if (tag.Id == 283)
                                    {
                                        VerticalResolution = "'" + exif[283].Value + "'";
                                    }
                                    else if (tag.Id == 36867)
                                    {
                                        Datetaken = "'" + exif[36867].Value + "'";
                                    }
                                    else if (tag.Id == 40962 || tag.Id == 40963)
                                    {
                                        dimension = "'" + exif[40962].Value + " x " + exif[40963].Value + "'";
                                    }
                                    else if (tag.Id == 34855)
                                    {
                                        ISOSpeedRatings = "'" + exif[34855].Value + "'";
                                    }
                                    else if (tag.Id == 41986)
                                    {
                                        ExposureMode = "'" + exif[41986].Value + "'";
                                    }
                                    else if (tag.Id == 41994)
                                    {
                                        Sharpness = "'" + exif[41994].Value + "'";
                                    }
                                    else if (tag.Id == 2)
                                    {
                                        GPSLatitude = "'" + ConvertDegreeAngleToDouble(tag.Value.Split(' ')[0].Replace('°', ' ').Trim(), tag.Value.Split(' ')[1].Replace('\'', ' ').Trim(), tag.Value.Split(' ')[2].Replace('\"', ' ').Trim()) + "'";
                                    }
                                    else if (tag.Id == 4)
                                    {
                                        GPSLongitude = "'" + ConvertDegreeAngleToDouble(tag.Value.Split(' ')[0].Replace('°', ' ').Trim(), tag.Value.Split(' ')[1].Replace('\'', ' ').Trim(), tag.Value.Split(' ')[2].Replace('\"', ' ').Trim()) + "'";
                                    }
                                    else if (tag.Id == 33434)
                                    {
                                        ExposureTime = "'" + exif[33434].Value + "'";
                                    }
                                    else if (tag.Id == 37381)
                                    {
                                        ApertureValue = "'" + exif[37381].Value + "'";
                                    }
                                }
                                ImageMetaData = "<image Dimensions=" + dimension + " CameraManufacture=" + CameraManufacture + " HorizontalResolution=" + HorizontalResolution + " VerticalResolution=" + VerticalResolution + " CameraModel=" + CameraModel + " ISO-SpeedRating=" + ISOSpeedRatings + " DateTaken=" + Datetaken + " ExposureMode=" + ExposureMode + " Sharpness=" + Sharpness + " Orientation=" + orientation + " GPSLatitude=" + GPSLatitude + " GPSLongitude=" + GPSLongitude + " ExposureTime=" + ExposureTime + " ApertureValue=" + ApertureValue + " ></image>";
                            }
                            catch (System.ArgumentException aex)
                            {
                                file.MoveTo(Path.Combine(Directoryname, "Download", "CorruptImages", file.Name));
                                if (txtFile.Exists)
                                    txtFile.MoveTo(Path.Combine(Directoryname, "Download", "CorruptImages", txtFile.Name));
                                return;
                            }
                            catch (Exception ex)
                            {
                                ErrorHandler.ErrorHandler.LogError(ex);
                                ImageMetaData = "<image Dimensions=" + "##" + " CameraManufacture=" + "##" + " HorizontalResolution=" + "##" + " VerticalResolution=" + "##" + " CameraModel=" + "##" + " ISO-SpeedRating=" + "##" + " DateTaken=" + "##" + " ExposureMode=" + "##" + " Sharpness=" + "##" + " Orientation=" + "##" + " GPSLatitude=##" + " GPSLongitude=##" + " ExposureTime=##" + " ApertureValue=##" + "></image>";
                            }
                            #endregion
                            //ShowMessage(threadId.ToString());
                            bool IsImageCorrupt = false;
                            HorizontalResolution = HorizontalResolution.Replace("'", "");
                            VerticalResolution = VerticalResolution.Replace("'", "");
                            Int32 HorizontalDPI = 0;
                            Int32 VerticalDPI = 0;
                            if (HorizontalResolution.Contains("##") || VerticalResolution.Contains("##"))
                            {
                                BitmapImage testbmp = new BitmapImage();
                                using (FileStream fileStream = File.OpenRead(file.FullName))
                                {
                                    MemoryStream ms = new MemoryStream();
                                    fileStream.CopyTo(ms);
                                    ms.Seek(0, SeekOrigin.Begin);
                                    fileStream.Close();
                                    testbmp.BeginInit();
                                    testbmp.StreamSource = ms;
                                    testbmp.EndInit();
                                    testbmp.Freeze();
                                }
                                HorizontalDPI = Convert.ToInt32(testbmp.DpiX);
                                VerticalDPI = Convert.ToInt32(testbmp.DpiY);
                            }
                            else
                            {
                                //Imagica India was returning HorizontalResolution - 0.72 and VerticalResolution - 0.72
                                if (!Int32.TryParse(HorizontalResolution, out HorizontalDPI))
                                    HorizontalDPI = 72;

                                if (!Int32.TryParse(VerticalResolution, out VerticalDPI))
                                    VerticalDPI = 72;
                            }
                            //ShowMessage(threadId.ToString());
                            if (ConfigurationManager.AppSettings.AllKeys.Contains("LocationIds"))
                            {
                                string[] LocationIds = System.Configuration.ConfigurationManager.AppSettings["LocationIds"].Split(',');
                                // ErrorHandler.ErrorHandler.LogFileWrite("LocationID1: " + LocationID.ToString());
                                if (LocationIds.Contains(LocationID.ToString()))
                                    rotateImageLocationWise(file.FullName);
                                else
                                {
                                    // RotateImage(file.FullName);  // Commented by Suraj for Rotate image Issue
                                    RotateImage(file.FullName, needrotaion);
                                }
                            }
                            else
                            {
                                //RotateImage(file.FullName); // Commented by Suraj for Rotate image Issue
                                RotateImage(file.FullName, needrotaion);
                            }
                            isResetDPIRequired = CheckResetDPIRequired(HorizontalDPI, VerticalDPI, Convert.ToString(PhotoGrapherId));
                            //ShowMessage(threadId.ToString());
                            if (isResetDPIRequired)
                                ResetLowDPI(file.FullName, Path.Combine(DirectoryNameWithDate, picname));
                            else if (IsAutoColorCorrectionActive && IsCorrectionAtDownloadActive)
                                AutoCorrectImageWIthCopy(file.FullName, Path.Combine(DirectoryNameWithDate, picname));
                            else
                            {
                                file.CopyTo(Path.Combine(DirectoryNameWithDate, picname), true);
                                // file.CopyTo(Path.Combine(DirectoryNameWithDate, "m_" + picname), true); //minified image original one
                            }
                            //ShowMessage(threadId.ToString());
                            if (txtFile.Exists)
                            {
                                try
                                {
                                    gumballScoreSeperater = GetgumballScoreSeperater(LocationID);
                                    //if (ConfigurationManager.AppSettings.AllKeys.Contains("GumballScoreSeperater"))
                                    //    gumballScoreSeperater = Convert.ToChar(ConfigurationManager.AppSettings["GumballScoreSeperater"].ToString());
                                    gumballScore = File.ReadAllText(txtFile.FullName);
                                    GumBallRidetxtContent = gumballScore.Replace(gumballScoreSeperater, ",");
                                    //GumBallRidetxtContent = File.ReadAllText(txtFile.FullName);
                                    if (!File.Exists(Path.Combine(GumBallRideFolderPath, txtFileName)))
                                        txtFile.MoveTo(Path.Combine(GumBallRideFolderPath, txtFileName));
                                    else
                                    {
                                        File.Delete(Path.Combine(GumBallRideFolderPath, txtFileName));
                                        txtFile.MoveTo(Path.Combine(GumBallRideFolderPath, txtFileName));
                                    }
                                }
                                catch (Exception ex)
                                {
                                    string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                                    ErrorHandler.ErrorHandler.LogFileWrite(errorMessage + txtFile.Name);
                                }
                            }
                            //ShowMessage(threadId.ToString());
                            isResetDPIRequired = false;
                            if (is_SemiOrder && objDG_SemiOrder_Settings != null)
                            {
                                //RotateImageIfRequired(Path.Combine(DirectoryNameWithDate, picname));
                            }
                            else
                            {
                                //IsImageCorrupt = ResizeWPFImage(Path.Combine(DirectoryNameWithDate, picname), 210, Path.Combine(_thumbnailFilePathDate, picname), Path.Combine(DirectoryNameWithDate, picname));
                                IsImageCorrupt = CompressToMinifiedImage(0.25, Path.Combine(DirectoryNameWithDate, picname), Path.Combine(_thumbnailFilePathDate, picname), string.Empty);
                                if (IsImageCorrupt)
                                {
                                    ErrorHandler.ErrorHandler.LogFileWrite(file.Name + " is corrupt image and is moved to CorruptImages folder");
                                    File.Delete(Path.Combine(DirectoryNameWithDate, picname));
                                    file.MoveTo(Path.Combine(Directoryname, "Download", "CorruptImages", file.Name));
                                    if (File.Exists(Path.Combine(GumBallRideFolderPath, txtFileName)))
                                        File.Delete(Path.Combine(GumBallRideFolderPath, txtFileName));
                                    return;
                                }
                                //ResizeWPFImage(Path.Combine(DirectoryNameWithDate, picname), 1200, Path.Combine(_bigThumbnailFilePathDate, picname));
                                CompressToMinifiedImage(0.50, Path.Combine(DirectoryNameWithDate, picname), Path.Combine(_bigThumbnailFilePathDate, picname), Path.Combine(_minifiedFilePathDate, "m_" + picname));
                            }
                            //ShowMessage(threadId.ToString());
                            LocationBusniess locBuiness = new LocationBusniess();

                            photoId = phBusiness.SetPhotoDetails(substoreId, filename, picname.ToString(), curDate, file.Name.Split('@')[1].ToString().Split('.')[0], ImageMetaData, LocationID, "test", ImageEffect, CaptureDate, RfidScanType, 1, (new CharacterBusiness()).GetCharacterId(file.Name.Split('@')[1].ToString().Split('.')[0]), 0, isVideoProcessed, 1, -1, picname.ToString(), 0, false, displayOrderNumber);
                            //photoId = phBusiness.SetPhotoDetails(substoreId, filename, picname.ToString(), curDate, file.Name.Split('@')[1].ToString().Split('.')[0], ImageMetaData, LocationID, "test", ImageEffect, CaptureDate, RfidScanType, Isimageprocessed, (new CharacterBusiness()).GetCharacterId(file.Name.Split('@')[1].ToString().Split('.')[0]), 0, isVideoProcessed, 1, -1, picname.ToString(), 0, false, displayOrderNumber);
                            auditLogBusiness.InsertExceptionLog(threadIdentity, threadId, startTime, DateTime.Now, file.Name, photoId, null, "927");
                            string imgPath = Path.Combine(DirectoryNameWithDate, picname);
                            //ShowMessage(threadId.ToString());
                            if (IsMobileRfidEnabled)
                            {
                                await MobileRfidAssociationCheck(photoId, Path.GetFileNameWithoutExtension(file.FullName));
                            }
                            auditLogBusiness.InsertExceptionLog(threadIdentity, threadId, startTime, DateTime.Now, file.Name, photoId, null, "933");
                            //ShowMessage(threadId.ToString());
                            if (IsBarcodeActive)
                            {
                                BarcodeAssociationCheck(photoId, imgPath);
                            }
                            bool isCodeType = await IsCodeType(photoId);
                            string Message = "936 : " + threadIdentity + "   is_SemiOrder : " + is_SemiOrder.ToString() + " IsCodeType : " + isCodeType.ToString() + "  : " + _mediaType.ToString() + " :photoId : " + photoId + " : " + substoreId + " : " + LocationID + " : " + objDG_SemiOrder_Settings + " : " + DirectoryNameWithDate + " : " + picname + " : " + GumBallRidetxtContent;
                            auditLogBusiness.InsertExceptionLog(threadIdentity, threadId, startTime, DateTime.Now, file.Name, photoId, null, Message);
                            //ShowMessage(threadId.ToString());
                            if (is_SemiOrder && objDG_SemiOrder_Settings != null && !isCodeType)
                            {
                                auditLogBusiness.InsertExceptionLog(threadIdentity, threadId, startTime, DateTime.Now, file.Name, photoId, null, "944");
                                if (_mediaType != "3")
                                    await SaveSpecPrintEffectsIntoDb(photoId, substoreId, LocationID, objDG_SemiOrder_Settings, DirectoryNameWithDate, picname, GumBallRidetxtContent, ImageEffect, LocationID);
                            }
                            //ShowMessage(threadId.ToString());
                            var lstconfig = _lstLocationWiseConfigParams.Where(s => s.LocationId == LocationID && s.IMIXConfigurationMasterId == Convert.ToInt64(ConfigParams.IsPreviewEnabledPW)
                                               && s.ConfigurationValue.ToUpper() == "TRUE").ToList();
                            if (lstconfig != null && lstconfig.Count() > 0)
                            {
                                CopyImgToDisplayFolder(Path.Combine(DirectoryNameWithDate, picname), filename, photoId, LocationID);
                            }
                            //ShowMessage(threadId.ToString());
                            //try
                            //{
                            //    DispatcherFrame frame = new DispatcherFrame();
                            //    mainImage.Dispatcher.BeginInvoke(
                            //        System.Windows.Threading.DispatcherPriority.Input
                            //        , new System.Windows.Threading.DispatcherOperationCallback(delegate
                            //        {
                            //            FileInfo infofile = new FileInfo(Path.Combine(DirectoryNameWithDate, picname));
                            //            infofile.CopyTo(Directoryname + "\\Thumbnails\\Temp\\" + picname, true);
                            //            mainImage.Source =
                            //                 new BitmapImage
                            //                     (new Uri(Directoryname + "\\Thumbnails\\Temp\\" + picname));
                            //            frame.Continue = false;
                            //            counter++;
                            //            UpdateTotalCounter();
                            //            return null;

                            //        }), null);
                            //    Dispatcher.PushFrame(frame);
                            //    //if (this.Dispatcher.HasShutdownFinished)
                            //    //{

                            //    //}
                            //}
                            //catch (IOException IOex)
                            //{
                            //    string errorMessage = "Got IO Exception after Dispatcher Frame for file " + file.FullName;
                            //    ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                            //    for (int i = 0; i < FilesToDelete.Count; i++)
                            //    {
                            //        if (File.Exists(FilesToDelete[i]))
                            //        {
                            //            File.Move(FilesToDelete[i], Path.Combine(ArchivedFolderPath, Path.GetFileName(FilesToDelete[i])));
                            //            //File.Delete(FilesToDelete[i]);
                            //            FilesToDelete.RemoveAt(i);
                            //        }
                            //    }
                            //    FilesToDelete.Add(file.FullName.ToLower());
                            //}
                            //catch (Exception ex)
                            //{
                            //    ErrorHandler.ErrorHandler.LogError(ex);
                            //}


                            if (is_SemiOrder)
                            {
                                if (objDG_SemiOrder_Settings != null && !isCodeType)
                                {
                                    if (is_SemiOrder == true && objDG_SemiOrder_Settings.DG_SemiOrder_Environment == true)
                                    {

                                    }
                                    else if (is_SemiOrder == true && objDG_SemiOrder_Settings.DG_SemiOrder_Environment == false && (bool)objDG_SemiOrder_Settings.DG_SemiOrder_IsPrintActive)
                                    {
                                        string[] prdIdList = objDG_SemiOrder_Settings.DG_SemiOrder_ProductTypeId.Split(',');
                                        foreach (string prodList in prdIdList)
                                        {
                                            string SyncCode = CommonUtility.GetUniqueSynccode(Convert.ToInt32(ApplicationObjectEnum.OrderDetails).ToString().PadLeft(2, '0'), LoginUser.countrycode, LoginUser.Storecode, substoreId.ToString());
                                            int OrderDetailId = (new OrderBusiness()).AddSemiOrderDetails(photoId, prodList, LocationID, SyncCode, false, substoreId);
                                        }
                                    }
                                }
                            }
                            //ShowMessage(threadId.ToString());
                        }
                        else
                        {

                            //semiorder
                            if (IsGreenScreenWorkFlow)
                                isVideoProcessed = true;
                            //string picname = string.Empty; //Commented by Vinod_11May2020
                            string txtFileName = string.Empty;
                            bool isRotatedFirstTime = false; //flag to check if already rotated
                            //ShowMessage(threadId.ToString());
                            foreach (SemiOrderSettings objDG_SemiOrder_Settings in lstobjDG_SemiOrder_Settings)
                            {
                                
                                ShowMessage("Line 1125 Image counter:" + imagenameCntr);
                                ImageEffect = "<image brightness = '" + defaultBrightness + "' contrast = '" + defaultContrast + "' Crop='##' colourvalue = '##' rotate='##' ><effects sharpen='##' greyscale='0' digimagic='0' sepia='0' defog='0' underwater='0' emboss='0' invert = '0' granite='0' hue ='##' cartoon = '0'></effects></image>";
                                CustomBusineses custObj = new CustomBusineses();
                                DateTime curDate = custObj.ServerDateTime();
                                if (string.IsNullOrEmpty(picname))
                                    if (photoCount > 0)
                                        picname = curDate.Day.ToString() + curDate.Month.ToString() + file.Name.Split('@')[0].ToString() + "(" + photoCount + ")" + ".jpg";
                                    else
                                        picname = curDate.Day.ToString() + curDate.Month.ToString() + file.Name.Split('@')[0].ToString() + ".jpg";
                                //ShowMessage(threadId.ToString());
                                if (picname.Contains('#'))
                                {
                                    picname = picname = picname.Split('#')[0] + "_" + _mediaType + ".jpg";
                                    IsFrame = true;
                                    isVideoProcessed = true;
                                }
                                string filename = file.Name.Split('@')[0].ToString();
                                string rfid = file.Name.Split('@')[0].ToString();
                                if (filename.Contains('_'))
                                {
                                    filename = filename.Split('_')[0].ToString();
                                    rfid = filename.Split('_')[0].ToString();
                                    isVideoProcessed = true;
                                }
                                //ShowMessage(threadId.ToString());
                                if (string.IsNullOrEmpty(txtFileName))
                                    txtFileName = curDate.Day.ToString() + curDate.Month.ToString() + txtFile.Name.Split('@')[0].ToString() + ".txt";

                                string renderedTag = "";
                                int needrotaion = 0;  // Added by Suraj For Rotate Image Issue
                                try
                                {
                                    #region Image Meta Data
                                    ExifTagCollection exif = new ExifTagCollection(file.FullName);
                                    #region Auto-Rotate
                                    //Start:Code for Auto-Rotate
                                    ExifReader reader = null;
                                    //string renderedTag = "";
                                    try
                                    {
                                        reader = new ExifReader(file.FullName);
                                        foreach (ushort tagID in Enum.GetValues(typeof(ExifTags)))
                                        {
                                            object val;
                                            if (reader != null)
                                            {
                                                if (reader.GetTagValue(tagID, out val))
                                                {
                                                    renderedTag = val.ToString();
                                                }
                                            }
                                        }
                                    }
                                    catch (Exception ex)
                                    {
                                        ErrorHandler.ErrorHandler.LogError(ex);
                                    }
                                    finally
                                    {
                                        if (reader != null)
                                            reader.Dispose();
                                    }
                                    ErrorHandler.ErrorHandler.LogFileWrite("PhotoId : " + file.FullName + " :  renderedTag : " + renderedTag);
                                    ShowMessage("Line 1188 Image counter:" + imagenameCntr);
                                    //FileNameDownload = Directoryname + "\\Download\\" + file.Name;
                                    if (!string.IsNullOrEmpty(renderedTag))
                                    {
                                        needrotaion = GetRotationValue(renderedTag);
                                        isrotated = true;
                                        ErrorHandler.ErrorHandler.LogFileWrite("PhotoId : " + file.FullName + " :  renderedTag : " + renderedTag + "1092");
                                    }
                                    else
                                    {
                                        needrotaion = 0;
                                        isrotated = false;
                                    }
                                    //End:Code for Auto-Rotate
                                    #endregion
                                    if (exif.Where(o => o.Id == 36867).Count() > 0)
                                    {
                                        try
                                        {
                                            System.Globalization.DateTimeFormatInfo objFormat = new System.Globalization.DateTimeFormatInfo();
                                            objFormat.ShortDatePattern = @"yyyy/MM/dd HH:mm:ss";
                                            string datePart = exif[36867].Value.Split(' ').First();
                                            datePart = datePart.Replace(':', '/');
                                            string timePart = exif[36867].Value.Split(' ').Last();
                                            CaptureDate = Convert.ToDateTime(datePart + " " + timePart, objFormat);
                                        }
                                        catch (Exception ex)
                                        {
                                            CaptureDate = null;
                                            ErrorHandler.ErrorHandler.LogError(ex);
                                        }
                                    }
                                    else if (file.CreationTime != null)
                                    {
                                        try
                                        {
                                            System.Globalization.DateTimeFormatInfo objFormat = new System.Globalization.DateTimeFormatInfo();
                                            objFormat.ShortDatePattern = @"yyyy/MM/dd HH:mm:ss";
                                            string datePart = Convert.ToString(file.CreationTime.Date).Split(' ').First();
                                            datePart = datePart.Replace(':', '/');
                                            string timePart = Convert.ToString(file.CreationTime.TimeOfDay);
                                            CaptureDate = Convert.ToDateTime(datePart + " " + timePart, objFormat);
                                        }
                                        catch (Exception ex)
                                        {
                                            CaptureDate = null;
                                            string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                                            ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                                        }
                                    }
                                    else
                                        CaptureDate = null;
                                    foreach (LevDan.Exif.ExifTag tag in exif)
                                    {
                                        if (tag.Id == 271)
                                        {
                                            CameraManufacture = "'" + exif[271].Value + "'";
                                        }
                                        else if (tag.Id == 272)
                                        {
                                            CameraModel = "'" + exif[272].Value + "'";
                                        }
                                        else if (tag.Id == 274)
                                        {
                                            orientation = "'" + exif[274].Value + "'";
                                        }
                                        else if (tag.Id == 282)
                                        {
                                            HorizontalResolution = "'" + exif[282].Value + "'";
                                        }
                                        else if (tag.Id == 283)
                                        {
                                            VerticalResolution = "'" + exif[283].Value + "'";
                                            //ErrorHandler.ErrorHandler.LogError("VerticalResolution =" + VerticalResolution);
                                            //ErrorHandler.ErrorHandler.LogError("IsRotation =" + Convert.ToString(isrotated));
                                        }
                                        else if (tag.Id == 36867)
                                        {
                                            Datetaken = "'" + exif[36867].Value + "'";
                                        }
                                        else if (tag.Id == 40962 || tag.Id == 40963)
                                        {
                                            dimension = "'" + exif[40962].Value + " x " + exif[40963].Value + "'";
                                        }
                                        else if (tag.Id == 34855)
                                        {
                                            ISOSpeedRatings = "'" + exif[34855].Value + "'";
                                        }
                                        else if (tag.Id == 41986)
                                        {
                                            ExposureMode = "'" + exif[41986].Value + "'";
                                        }
                                        else if (tag.Id == 41994)
                                        {
                                            Sharpness = "'" + exif[41994].Value + "'";
                                        }
                                        else if (tag.Id == 2)
                                        {
                                            GPSLatitude = "'" + ConvertDegreeAngleToDouble(tag.Value.Split(' ')[0].Replace('°', ' ').Trim(), tag.Value.Split(' ')[1].Replace('\'', ' ').Trim(), tag.Value.Split(' ')[2].Replace('\"', ' ').Trim()) + "'";
                                        }
                                        else if (tag.Id == 4)
                                        {
                                            GPSLongitude = "'" + ConvertDegreeAngleToDouble(tag.Value.Split(' ')[0].Replace('°', ' ').Trim(), tag.Value.Split(' ')[1].Replace('\'', ' ').Trim(), tag.Value.Split(' ')[2].Replace('\"', ' ').Trim()) + "'";
                                        }
                                        else if (tag.Id == 33434)
                                        {
                                            ExposureTime = "'" + exif[33434].Value + "'";
                                        }
                                        else if (tag.Id == 37381)
                                        {
                                            ApertureValue = "'" + exif[37381].Value + "'";
                                        }
                                    }
                                    if (!HorizontalResolution.Contains("'"))
                                        HorizontalResolution = "'" + HorizontalResolution + "'";
                                    if (!VerticalResolution.Contains("'"))
                                        VerticalResolution = "'" + VerticalResolution + "'";
                                    ImageMetaData = "<image Dimensions=" + dimension + " CameraManufacture=" + CameraManufacture + " HorizontalResolution=" + HorizontalResolution + " VerticalResolution=" + VerticalResolution + " CameraModel=" + CameraModel + " ISO-SpeedRating=" + ISOSpeedRatings + " DateTaken=" + Datetaken + " ExposureMode=" + ExposureMode + " Sharpness=" + Sharpness + " Orientation=" + orientation + " GPSLatitude=" + GPSLatitude + " GPSLongitude=" + GPSLongitude + " ExposureTime=" + ExposureTime + " ApertureValue=" + ApertureValue + " ></image>";
                                }
                                catch (System.ArgumentException aex)
                                {
                                    file.MoveTo(Path.Combine(Directoryname, "Download", "CorruptImages", file.Name));
                                    if (txtFile.Exists)
                                        txtFile.MoveTo(Path.Combine(Directoryname, "Download", "CorruptImages", txtFile.Name));
                                    return;
                                }
                                catch (Exception ex)
                                {
                                    ErrorHandler.ErrorHandler.LogError(ex);
                                    ImageMetaData = "<image Dimensions=" + "##" + " CameraManufacture=" + "##" + " HorizontalResolution=" + "##" + " VerticalResolution=" + "##" + " CameraModel=" + "##" + " ISO-SpeedRating=" + "##" + " DateTaken=" + "##" + " ExposureMode=" + "##" + " Sharpness=" + "##" + " Orientation=" + "##" + " GPSLatitude=##" + " GPSLongitude=##" + " ExposureTime=##" + " ApertureValue=##" + "></image>";
                                }
                                #endregion
                                //ShowMessage(threadId.ToString());
                                bool IsImageCorrupt = false;
                                HorizontalResolution = HorizontalResolution.Replace("'", "");
                                VerticalResolution = VerticalResolution.Replace("'", "");
                                Int32 HorizontalDPI = 0;
                                Int32 VerticalDPI = 0;
                                if (HorizontalResolution.Contains("##") || VerticalResolution.Contains("##"))
                                {
                                    BitmapImage testbmp = new BitmapImage();
                                    using (FileStream fileStream = File.OpenRead(file.FullName))
                                    {
                                        MemoryStream ms = new MemoryStream();
                                        fileStream.CopyTo(ms);
                                        ms.Seek(0, SeekOrigin.Begin);
                                        fileStream.Close();
                                        testbmp.BeginInit();
                                        testbmp.StreamSource = ms;
                                        testbmp.EndInit();
                                        testbmp.Freeze();
                                    }
                                    HorizontalDPI = Convert.ToInt32(testbmp.DpiX);
                                    VerticalDPI = Convert.ToInt32(testbmp.DpiY);
                                }
                                else
                                {
                                    //Imagica India was returning HorizontalResolution - 0.72 and VerticalResolution - 0.72
                                    if (!Int32.TryParse(HorizontalResolution, out HorizontalDPI))
                                        HorizontalDPI = 72;

                                    if (!Int32.TryParse(VerticalResolution, out VerticalDPI))
                                        VerticalDPI = 72;
                                }
                                ShowMessage("Line 1352 Image counter:" + imagenameCntr);
                                string[] LocationIds = System.Configuration.ConfigurationManager.AppSettings["LocationIds"].Split(',');
                                if (LocationIds.Contains(LocationID.ToString()))
                                {
                                    if (!isRotatedFirstTime)
                                    {
                                        rotateImageLocationWise(file.FullName);
                                        isRotatedFirstTime = true;
                                    }

                                }
                                else
                                {
                                    if (!isRotatedFirstTime)
                                    {
                                        //RotateImage(file.FullName); // Commented by Suraj for Rotate image Issue
                                        RotateImage(file.FullName, needrotaion);
                                        isRotatedFirstTime = true;
                                    }
                                }

                                //ShowMessage(threadId.ToString());
                                isResetDPIRequired = CheckResetDPIRequired(HorizontalDPI, VerticalDPI, Convert.ToString(PhotoGrapherId));
                                //needs to change here also
                                if (isResetDPIRequired)
                                    ResetLowDPI(file.FullName, Path.Combine(DirectoryNameWithDate, picname));
                                else if (IsAutoColorCorrectionActive && IsCorrectionAtDownloadActive)
                                    AutoCorrectImageWIthCopy(file.FullName, Path.Combine(DirectoryNameWithDate, picname));
                                else
                                    file.CopyTo(Path.Combine(DirectoryNameWithDate, picname), true);
                                //ShowMessage(threadId.ToString());
                                string FilePath = Path.Combine(GumBallRideFolderPath, txtFileName);
                                if (txtFile.Exists)
                                {
                                    try
                                    {
                                        gumballScoreSeperater = GetgumballScoreSeperater(LocationID);
                                        //if (ConfigurationManager.AppSettings.AllKeys.Contains("GumballScoreSeperater"))
                                        //    gumballScoreSeperater = Convert.ToChar(ConfigurationManager.AppSettings["GumballScoreSeperater"].ToString());
                                        gumballScore = File.ReadAllText(txtFile.FullName);
                                        GumBallRidetxtContent = gumballScore.Replace(gumballScoreSeperater, ",");
                                        //GumBallRidetxtContent = File.ReadAllText(txtFile.FullName);
                                        if (!File.Exists(FilePath))
                                            txtFile.MoveTo(FilePath);
                                        else
                                        {
                                            File.Delete(FilePath);
                                            txtFile.MoveTo(FilePath);
                                        }
                                    }
                                    catch (Exception ex)
                                    {
                                        string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                                        ErrorHandler.ErrorHandler.LogFileWrite(errorMessage + txtFile.Name);
                                    }
                                }
                                //ShowMessage(threadId.ToString());
                                isResetDPIRequired = false;
                                if (is_SemiOrder && lstobjDG_SemiOrder_Settings != null)
                                {
                                    //uncommented below code on 26 Feb by Ajay to resolve the rotation issue.
                                    if (!string.IsNullOrEmpty(renderedTag) && renderedTag != "8")
                                    {
                                        RotateImageIfRequired(Path.Combine(DirectoryNameWithDate, picname));
                                    }
                                    CompressToMinifiedImage(0.25, Path.Combine(DirectoryNameWithDate, picname), Path.Combine(_thumbnailFilePathDate, picname), string.Empty);
                                    CompressToMinifiedImage(0.50, Path.Combine(DirectoryNameWithDate, picname), Path.Combine(_bigThumbnailFilePathDate, picname), Path.Combine(_minifiedFilePathDate, "m_" + picname));
                                }
                                else
                                {
                                    //IsImageCorrupt = ResizeWPFImage(Path.Combine(DirectoryNameWithDate, picname), 210, Path.Combine(_thumbnailFilePathDate, picname), Path.Combine(DirectoryNameWithDate, picname));
                                    IsImageCorrupt = CompressToMinifiedImage(0.25, Path.Combine(DirectoryNameWithDate, picname), Path.Combine(_thumbnailFilePathDate, picname), string.Empty);
                                    if (IsImageCorrupt)
                                    {
                                        //ErrorHandler.ErrorHandler.LogFileWrite(file.Name + " is corrupt image and is moved to CorruptImages folder");
                                        File.Delete(Path.Combine(DirectoryNameWithDate, picname));
                                        file.MoveTo(Path.Combine(Directoryname, "Download", "CorruptImages", file.Name));
                                        if (File.Exists(Path.Combine(GumBallRideFolderPath, txtFileName)))
                                            File.Delete(Path.Combine(GumBallRideFolderPath, txtFileName));
                                        return;
                                    }
                                    //ResizeWPFImage(Path.Combine(DirectoryNameWithDate, picname), 1200, Path.Combine(_bigThumbnailFilePathDate, picname));
                                    CompressToMinifiedImage(0.50, Path.Combine(DirectoryNameWithDate, picname), Path.Combine(_bigThumbnailFilePathDate, picname), Path.Combine(_minifiedFilePathDate, "m_" + picname));
                                }
                                ShowMessage("Line 1436 Image counter:" + imagenameCntr);
                                LocationBusniess locBuiness = new LocationBusniess();

                                //try
                                //{
                                //    foreach (SemiOrderSettings objDG_SemiOrder_Settings in lstobjDG_SemiOrder_Settings)
                                //{
                                //        imagenameCntr = imagenameCntr + 1;

                                //int Isimageprocessed = 1;
                                //if (is_SemiOrder && objDG_SemiOrder_Settings != null) // Added by Suraj For Check Location Spec.
                                //{
                                //    Isimageprocessed = 0;
                                //}

                                if (ParentImageID == -1)
                                {

                                    // string filenametobesaved = picname.Substring(0, picname.Length - 4) + "_" + imagenameCntr + ".jpg";
                                    string filenametobesaved = picname;

                                    OriginalFileName = picname;
                                    photoId = phBusiness.SetPhotoDetails(substoreId, filename, filenametobesaved, curDate, file.Name.Split('@')[1].ToString().Split('.')[0], ImageMetaData, LocationID, "test", ImageEffect, CaptureDate, RfidScanType, 3, (new CharacterBusiness()).GetCharacterId(file.Name.Split('@')[1].ToString().Split('.')[0]), 0, isVideoProcessed, 1, ParentImageID, OriginalFileName, objDG_SemiOrder_Settings.DG_SemiOrder_Settings_Pkey, false, displayOrderNumber);
                                    ParentImageID = photoId;
                                    auditLogBusiness.InsertExceptionLog(threadIdentity, threadId, startTime, DateTime.Now, file.Name, photoId, null, "1352");
                                }
                                else
                                {
                                    string filenametobesaved = picname;//.Substring(0, picname.Length - 4) + "_" + imagenameCntr + ".jpg";

                                    photoId = phBusiness.SetPhotoDetails(substoreId, filename, filenametobesaved, curDate, file.Name.Split('@')[1].ToString().Split('.')[0], ImageMetaData, LocationID, "test", ImageEffect, CaptureDate, RfidScanType, 3, (new CharacterBusiness()).GetCharacterId(file.Name.Split('@')[1].ToString().Split('.')[0]), 0, isVideoProcessed, 1, ParentImageID, OriginalFileName, objDG_SemiOrder_Settings.DG_SemiOrder_Settings_Pkey, false, displayOrderNumber);
                                    auditLogBusiness.InsertExceptionLog(threadIdentity, threadId, startTime, DateTime.Now, file.Name, photoId, null, "1359");
                                }
                                string imgPath = Path.Combine(DirectoryNameWithDate, picname);
                                ShowMessage("Line 1470 Image counter:" + imagenameCntr);
                                if (IsMobileRfidEnabled)
                                {
                                    await MobileRfidAssociationCheck(photoId, Path.GetFileNameWithoutExtension(file.FullName));
                                }
                                ShowMessage("Line 1475 Image counter:" + imagenameCntr);
                                auditLogBusiness.InsertExceptionLog(threadIdentity, threadId, startTime, DateTime.Now, file.Name, photoId, null, "1363");
                                if (IsBarcodeActive)
                                {
                                    BarcodeAssociationCheck(photoId, imgPath);
                                }
                                auditLogBusiness.InsertExceptionLog(threadIdentity, threadId, startTime, DateTime.Now, file.Name, photoId, null, "1372");
                                bool isCodeType = await IsCodeType(photoId);
                                string Message = "1374 :" + threadIdentity + "   is_SemiOrder : " + is_SemiOrder.ToString() + " IsCodeType : " + isCodeType.ToString() + "  : " + _mediaType.ToString() + " :photoId : " + photoId + " : " + substoreId + " : " + LocationID + " : " + objDG_SemiOrder_Settings + " : " + DirectoryNameWithDate + " : " + picname + " : " + GumBallRidetxtContent;
                                auditLogBusiness.InsertExceptionLog(threadIdentity, threadId, startTime, DateTime.Now, file.Name, photoId, null, Message);
                                ShowMessage("Line 1485 Image counter:" + imagenameCntr);
                                if (is_SemiOrder && objDG_SemiOrder_Settings != null && !isCodeType)
                                {
                                    auditLogBusiness.InsertExceptionLog(threadIdentity, threadId, startTime, DateTime.Now, file.Name, photoId, null, "1378");
                                    if (Convert.ToString(_mediaType) != "3")
                                    {
                                        await SaveSpecPrintEffectsIntoDb(photoId, substoreId, LocationID, objDG_SemiOrder_Settings, DirectoryNameWithDate, picname, GumBallRidetxtContent, ImageEffect, LocationID);
                                    }
                                }
                                var lstconfiglst = _lstLocationWiseConfigParams.Where(s => s.LocationId == LocationID && s.IMIXConfigurationMasterId == Convert.ToInt64(ConfigParams.IsPreviewEnabledPW)
                                               && s.ConfigurationValue.ToUpper() == "TRUE").ToList();
                                if (lstconfiglst != null && lstconfiglst.Count > 0)
                                {
                                    var lstconfig = _lstLocationWiseConfigParams.Where(s => s.LocationId == LocationID && s.IMIXConfigurationMasterId == Convert.ToInt64(ConfigParams.IsSpecImgPW) && s.ConfigurationValue.ToUpper() == "FALSE").ToList();
                                    if (lstconfig != null && lstconfig.Count() > 0)
                                        CopyImgToDisplayFolder(Path.Combine(DirectoryNameWithDate, picname), filename, photoId, LocationID);
                                }
                                ShowMessage("Line 1501 Image counter:" + imagenameCntr);
                                //try
                                //{
                                //    ErrorHandler.ErrorHandler.LogFileWrite("1462");
                                //    DispatcherFrame frame = new DispatcherFrame();
                                //    mainImage.Dispatcher.BeginInvoke(
                                //        System.Windows.Threading.DispatcherPriority.Input
                                //        , new System.Windows.Threading.DispatcherOperationCallback(delegate
                                //        {
                                //            FileInfo infofile = new FileInfo(Path.Combine(DirectoryNameWithDate, picname));
                                //            infofile.CopyTo(Directoryname + "\\Thumbnails\\Temp\\" + picname, true);
                                //            mainImage.Source =
                                //                 new BitmapImage
                                //                     (new Uri(Directoryname + "\\Thumbnails\\Temp\\" + picname));
                                //            frame.Continue = false;
                                //            counter++;
                                //            UpdateTotalCounter();
                                //            return null;

                                //        }), null);
                                //    Dispatcher.PushFrame(frame);
                                //    if (this.Dispatcher.HasShutdownFinished)
                                //    {

                                //    }
                                //}
                                //catch (IOException IOex)
                                //{
                                //    string errorMessage = "Got IO Exception after Dispatcher Frame for file " + file.FullName;
                                //    ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                                //    for (int i = 0; i < FilesToDelete.Count; i++)
                                //    {
                                //        if (File.Exists(FilesToDelete[i]))
                                //        {
                                //            File.Move(FilesToDelete[i], Path.Combine(ArchivedFolderPath, Path.GetFileName(FilesToDelete[i])));
                                //            //File.Delete(FilesToDelete[i]);
                                //            FilesToDelete.RemoveAt(i);
                                //        }
                                //    }
                                //    FilesToDelete.Add(file.FullName.ToLower());
                                //}
                                //catch (Exception ex)
                                //{
                                //    ErrorHandler.ErrorHandler.LogError(ex);
                                //}


                                if (is_SemiOrder && objDG_SemiOrder_Settings != null && !isCodeType)
                                {
                                    if (is_SemiOrder && objDG_SemiOrder_Settings.DG_SemiOrder_Environment == true)
                                    {

                                    }
                                    else if (is_SemiOrder && objDG_SemiOrder_Settings.DG_SemiOrder_Environment == false && (bool)objDG_SemiOrder_Settings.DG_SemiOrder_IsPrintActive)
                                    {
                                        string[] prdIdList = objDG_SemiOrder_Settings.DG_SemiOrder_ProductTypeId.Split(',');
                                        foreach (string prodList in prdIdList)
                                        {
                                            string SyncCode = CommonUtility.GetUniqueSynccode(Convert.ToInt32(ApplicationObjectEnum.OrderDetails).ToString().PadLeft(2, '0'), LoginUser.countrycode, LoginUser.Storecode, substoreId.ToString());
                                            new OrderBusiness().AddSemiOrderDetails(photoId, prodList, LocationID, SyncCode, false, substoreId);
                                        }
                                    }
                                }

                                imagenameCntr = imagenameCntr + 1;
                                picname = picname.Substring(0, picname.Length - 4) + "_" + imagenameCntr + ".jpg";
                                ShowMessage("Watcher completed for picname:" + picname);
                            }
                        }
                        try
                        {
                            //.Substring(0, picname.Length - 4) + "_" + imagenameCntr + ".jpg"
                            //ShowMessage(threadId.ToString());
                            string img = System.IO.Path.Combine(ArchivedFolderPath, file.Name);
                            ErrorHandler.ErrorHandler.LogFileWrite("File Move path : " + img);
                            string fileNameArchive = string.Empty;
                            if (file.Name.Contains('#'))
                            {
                                fileNameArchive = file.Name.Split('#')[0] + "_1" + ".jpg";
                                img = System.IO.Path.Combine(ArchivedFolderPath, fileNameArchive);
                            }
                            if (File.Exists(img))
                                File.Delete(img);
                            file.MoveTo(img);
                            //ShowMessage(threadId.ToString());
                        }
                        catch (IOException IOex)
                        {
                            string errorMessage = "Got IO Exception after Archived movement for file " + file.FullName;
                            ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                            for (int i = 0; i < FilesToDelete.Count; i++)
                            {
                                if (File.Exists(FilesToDelete[i]))
                                {
                                    File.Move(FilesToDelete[i], Path.Combine(ArchivedFolderPath, Path.GetFileName(FilesToDelete[i])));
                                    //File.Delete(FilesToDelete[i]);
                                    FilesToDelete.RemoveAt(i);
                                }
                            }
                            FilesToDelete.Add(file.FullName.ToLower());
                        }
                        ParentImageID = -1;
                        OriginalFileName = string.Empty;
                        imagenameCntr = 0;
                    }
                    catch (Exception ex)
                    {
                        ParentImageID = -1;
                        OriginalFileName = string.Empty;
                        imagenameCntr = 0;
                        ErrorHandler.ErrorHandler.LogError(ex);
                    }
                }
            }
            catch (System.NotSupportedException sex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(sex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage + "File not supported " + file.FullName);
                file.MoveTo(Path.Combine(Directoryname, "Download", "CorruptImages", file.Name));
                if (txtFile.Exists)
                    txtFile.MoveTo(Path.Combine(Directoryname, "Download", "CorruptImages", txtFile.Name));
            }
            catch (IOException IOex)
            {
                string errorMessage = "Got IO Exception in outer catch for file " + file.FullName;
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                for (int i = 0; i < FilesToDelete.Count; i++)
                {
                    if (File.Exists(FilesToDelete[i]))
                    {
                        File.Move(FilesToDelete[i], Path.Combine(ArchivedFolderPath, Path.GetFileName(FilesToDelete[i])));
                        //File.Delete(FilesToDelete[i]);
                        FilesToDelete.RemoveAt(i);
                    }
                }
                FilesToDelete.Add(file.FullName.ToLower());
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
            }
            finally
            {
                if (DateTime.Now.Subtract(lastmemoryUpdateTime).Seconds > 5)
                {
                    MemoryManagement.FlushMemory();
                    lastmemoryUpdateTime = DateTime.Now;
                }
                //row["End Time"] = Convert.ToString(DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss:ffff"));
                //threadSummary.Rows.Add(row);
                //UpdateTotalCounter();
                AuditLogBusiness auditLogBusiness = new AuditLogBusiness();
                auditLogBusiness.InsertAuditLog(threadIdentity, threadId, startTime, DateTime.Now, file.Name);
            }
        }

        /// <summary>
        /// Added by Vinod for SET3
        /// </summary>
        /// <param name="scaleFactor"></param>
        /// <param name="sourcePath"></param>
        /// <param name="targetPath"></param>
        private bool CompressToMinifiedImage(double scaleFactor, string sourcePath, string targetPath, string targetMinifiedPath)
        {
            try
            {
                using (FileStream fileStream = File.OpenRead(sourcePath))
                {
                    using (var image = System.Drawing.Image.FromStream(fileStream))
                    {
                        var newWidth = (int)(image.Width * scaleFactor);
                        var newHeight = (int)(image.Height * scaleFactor);
                        var thumbnailImg = new Bitmap(newWidth, newHeight);
                        var thumbGraph = Graphics.FromImage(thumbnailImg);
                        thumbGraph.CompositingQuality = CompositingQuality.HighQuality;
                        thumbGraph.SmoothingMode = SmoothingMode.HighQuality;
                        thumbGraph.InterpolationMode = InterpolationMode.HighQualityBicubic;
                        var imageRectangle = new System.Drawing.Rectangle(0, 0, newWidth, newHeight);
                        thumbGraph.DrawImage(image, imageRectangle);
                        thumbnailImg.Save(targetPath, image.RawFormat);
                        if (targetMinifiedPath != string.Empty)
                            thumbnailImg.Save(targetMinifiedPath, image.RawFormat);
                    }
                }

                return false;
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogFileWrite("CompressToMinifiedImage:: Message: " + ex.Message + " StackTrace: " + ex.StackTrace + " InnerException: " + ex.InnerException + " Source: " + ex.Source);
                return true;//for corrupted image
            }
        }

        private string GetgumballScoreSeperater(int LocationID)
        {
            if (GumBallActiveLocationList != null && GumBallActiveLocationList.Count > 0)
                LstRideConfigValueLocationWise = GumBallConfigurationValueList.Where(x => x.LocationId.Equals(LocationID) && GumBallActiveLocationList.Contains(x.LocationId)).ToList();
            if (LstRideConfigValueLocationWise != null && LstRideConfigValueLocationWise.Count > 0)
            {
                gumballScoreSeperater = LstRideConfigValueLocationWise.Where(x => x.IMIXConfigurationMasterId.Equals(Convert.ToInt32(ConfigParams.GumballScoreSeperater))).Select(x => x.ConfigurationValue).FirstOrDefault();
            }
            if (string.IsNullOrEmpty(gumballScoreSeperater))
                gumballScoreSeperater = ",";
            return gumballScoreSeperater;
        }
        private async Task MobileRfidAssociationCheck(int photoId, string imageName)
        {
            try
            {
                string tagId = string.Empty;
                if (imageName.Split('@').Length > 2)
                    tagId = imageName.Split('@')[2];
                if (!string.IsNullOrEmpty(tagId))
                {
                    tagId = "2222" + tagId;
                    new AssociateImageBusiness().AssociateMobileImage(tagId, photoId);
                }
            }
            catch (Exception ex)
            {
                auditLogBusiness.InsertExceptionLog(Convert.ToString(Guid.NewGuid()), 0, DateTime.Now, DateTime.Now, "MobileRfidAssociationCheck", photoId, ex, "1673");
                //ErrorHandler.ErrorHandler.LogError(ex);
            }
        }
        private async Task<bool> IsCodeType(int PhotoId)
        {
            try
            {
                //ErrorHandler.ErrorHandler.LogFileWrite("1621 : IsCodeType  : "+PhotoId);
                //objDigiPhotoDataServices = new DigiPhotoDataServices();
                PhotoBusiness phBusiness = new PhotoBusiness();
                return phBusiness.CheckIsCodeType(PhotoId);
            }
            catch (Exception ex)
            {
                auditLogBusiness.InsertExceptionLog(Convert.ToString(Guid.NewGuid()), 0, DateTime.Now, DateTime.Now, "IsCodeType", PhotoId, ex, "1632");
                //ErrorHandler.ErrorHandler.LogError(ex);
                return false;
            }
        }
        private void BarcodeAssociationCheck(int PhotoId, string ImagePath)
        {
            try
            {
                PhotoBusiness phBusiness = new PhotoBusiness();
                //objDigiPhotoDataServices = new DigiPhotoDataServices();
                if (scanType == Convert.ToInt32(ScanType.PreScan))
                {
                    bool isUniqueCodeFound = SetUniqueCodeInfoForPreScan(PhotoId, ImagePath);
                    if (isUniqueCodeFound)
                    {
                        var item = lstImgInfoPreScanList.Where(x => x.PhotoGrapherId == PhotoGrapherId).FirstOrDefault();
                        if (item != null && !String.IsNullOrEmpty(item.Barcode))
                        {
                            phBusiness.SetImageAssociationInfo(PhotoId, item.Format, item.Barcode, IsAnonymousCodeActive);
                        }
                    }
                }
                else if (scanType == Convert.ToInt32(ScanType.PostScan))
                {
                    bool isUniqueCodeFound = SetUniqueCodeInfoForPostScan(PhotoId, ImagePath);
                    if (isUniqueCodeFound)
                    {
                        var item = lstImgInfoPostScanList.Where(x => x.PhotoGrapherId == PhotoGrapherId).FirstOrDefault();
                        if (item != null && !String.IsNullOrEmpty(item.Barcode))
                        {
                            phBusiness.SetImageAssociationInfoForPostScan(item.ListImgPhotoId, item.Format, item.Barcode, IsAnonymousCodeActive);
                            AutoPurchasePostScan(item);
                            int ind = lstImgInfoPostScanList.FindIndex(x => x.PhotoGrapherId == PhotoGrapherId);
                            if (ind >= 0 && ind < lstImgInfoPostScanList.Count)
                                lstImgInfoPostScanList.RemoveAt(lstImgInfoPostScanList.FindIndex(x => x.PhotoGrapherId == PhotoGrapherId));
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                auditLogBusiness.InsertExceptionLog(Convert.ToString(Guid.NewGuid()), 0, DateTime.Now, DateTime.Now, "BarcodeAssociationCheck", PhotoId, ex, "1673");
                //ErrorHandler.ErrorHandler.LogError(ex);
            }
        }
        private void AutoPurchasePostScan(ImgInfoPostScanList item)
        {
            try
            {
                CardBusiness cardBusiness = new CardBusiness();
                if (IsAutoPurchaseActive && !IsAnonymousCodeActive)
                {
                    if (cardBusiness.IsValidPrepaidCodeType(item.Barcode, 405))
                    {
                        if (ReadSystemOnlineStatus())
                        {
                            try
                            {
                                ServiceProxy<IDataSyncService>.Use(client =>
                                {
                                    objCardLimit = client.CheckCardLimit(item.Barcode);
                                });
                                IsOnline = true;
                            }
                            catch (Exception ex)
                            {
                                ErrorHandler.ErrorHandler.LogError(ex);
                                IsOnline = false;
                            }
                        }

                        if (IsOnline)
                        {
                            if (objCardLimit.ValidCard)
                            {
                                CardImageLimit = objCardLimit.Allowed;
                                CardImageSold = objCardLimit.Associated;
                            }
                        }
                        else
                        {
                            CardImageLimit = cardBusiness.GetCardImageLimit(item.Barcode);
                            CardImageSold = cardBusiness.GetCardImageSold(item.Barcode);
                        }
                        //Set flag to set the package price.
                        if (CardImageSold > 0)
                            IsQrCodeUsed = true;
                        else
                            IsQrCodeUsed = false;

                        if (CardImageSold < CardImageLimit)
                        {
                            rest = CardImageLimit - CardImageSold;
                            rest = rest < 0 ? 0 : rest;
                        }
                        else
                        {
                            rest = 0;
                        }
                        string images = string.Empty;
                        if (scanType == 501)
                            images = string.Join(",", item.ListImgPhotoId.Skip(1).Take(rest));
                        else if (scanType == 502)
                            images = string.Join(",", item.ListImgPhotoId.Take(rest));

                        if (!String.IsNullOrWhiteSpace(images))
                        {
                            iMixImageCardTypeInfo imgCardTypeInfo = cardBusiness.GetCardTypeList().Where(d => d.CardIdentificationDigit == item.Barcode.Substring(0, 4)).FirstOrDefault();
                            int pkgId = imgCardTypeInfo.PackageId == null ? 0 : (int)imgCardTypeInfo.PackageId;
                            decimal pkgPrice = 0;
                            ProductBusiness prodBusiness = new ProductBusiness();
                            if (CardImageSold == 0)
                            {

                                pkgPrice = (Decimal)prodBusiness.GetProductPricing(pkgId);
                            }

                            int PaymentMode = 0;
                            CurrencyBusiness curBusiness = new CurrencyBusiness();
                            OrderBusiness ordBusiness = new OrderBusiness();
                            DigiPhoto.IMIX.Model.CurrencyInfo currency = curBusiness.GetCurrencyList().Where(g => g.DG_Currency_Default == true).FirstOrDefault();
                            int prodId = prodBusiness.GetPackagDetails(pkgId).Where(k => k.DG_Product_Quantity > 0).FirstOrDefault().DG_Orders_ProductType_pkey;
                            string PaymentDetails = "<Payment Mode = 'prepaid' Amount = '" + pkgPrice.ToString() + "' OrignalAmount = '" + pkgPrice.ToString() + "' CurrencyID = '" + currency.DG_Currency_pkey.ToString() + "' CurrencySyncCode = '" + currency.SyncCode.ToString() + "'/>";
                            string OrderNumber = GenerateOrderNumber();
                            string SyncCode = CommonUtility.GetUniqueSynccode(Convert.ToInt32(ApplicationObjectEnum.Order).ToString().PadLeft(2, '0'), LoginUser.countrycode, LoginUser.Storecode, "14");

                            DigiPhoto.IMIX.Model.OrderInfo orderInfo = ordBusiness.GenerateOrder(OrderNumber, pkgPrice, pkgPrice, PaymentDetails, PaymentMode, 0, null, 3, curBusiness.GetDefaultCurrency(), "0", SyncCode, LoginUser.Storecode);

                            int OrderID = orderInfo.DG_Orders_pkey;
                            AuditLog.AddUserLog(3, (int)FrameworkHelper.ActionType.GenerateOrder, "Create Order of No :" + OrderNumber + " of total Amount " + currency.DG_Currency_Symbol + " " + pkgPrice.ToString("0.000"));
                            string OrderDetailsSyncCode = CommonUtility.GetUniqueSynccode(Convert.ToInt32(ApplicationObjectEnum.OrderDetails).ToString().PadLeft(2, '0'), LoginUser.countrycode, LoginUser.Storecode, substoreId.ToString());
                            int ParentID = ordBusiness.SaveOrderLineItems(pkgId, OrderID, "", 1, null, 0, pkgPrice, pkgPrice, pkgPrice, -1, substoreId, 0, null, OrderDetailsSyncCode, null, null);
                            ordBusiness.SaveOrderLineItems(prodId, OrderID, images, 1, null, 0, 0, 0, 0, ParentID, substoreId, imgCardTypeInfo.ImageIdentificationType, item.Barcode, OrderDetailsSyncCode, null, null);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
            }
        }
        private void RotateImageIfRequired(string sourceImage)
        {
            //try
            //{
            //    BitmapImage bitmapImage = new BitmapImage();
            //    using (FileStream fileStream = File.OpenRead(sourceImage.ToString()))
            //    {
            //        MemoryStream ms = new MemoryStream();
            //        fileStream.CopyTo(ms);
            //        fileStream.Close();
            //        ms.Seek(0, SeekOrigin.Begin);
            //        bitmapImage.BeginInit();
            //        bitmapImage.StreamSource = ms;
            //        ImageMagickObject.MagickImage jmagic = new ImageMagickObject.MagickImage();

            //        if (needrotaion > 0)
            //        {
            //            if (needrotaion == 0)
            //            {
            //                bitmapImage.Rotation = Rotation.Rotate0;
            //                isrotated = false;
            //            }
            //            else if (needrotaion == 90)
            //            {
            //                bitmapImage.Rotation = Rotation.Rotate90;

            //                object[] o = new object[] { "-rotate", " 90 ", sourceImage };
            //                jmagic.Mogrify(o);
            //                o = null;
            //                isrotated = true;
            //            }
            //            else if (needrotaion == 180)
            //            {
            //                bitmapImage.Rotation = Rotation.Rotate180;

            //                object[] o = new object[] { "-rotate", " 180 ", sourceImage };
            //                jmagic.Mogrify(o);
            //                o = null;
            //                isrotated = false;
            //            }
            //            else if (needrotaion == 270)
            //            {
            //                bitmapImage.Rotation = Rotation.Rotate270;

            //                object[] o = new object[] { "-rotate", " 270 ", sourceImage };
            //                jmagic.Mogrify(o);
            //                o = null;
            //                isrotated = true;
            //            }
            //        }

            //        jmagic = null;
            //        bitmapImage.EndInit();
            //        bitmapImage.Freeze();
            //        fileStream.Close();
            //    }
            //}
            //catch (Exception ex)
            //{
            //    string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
            //    ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            //}
        }
        private void CompressToMinifiedImage(System.IO.Stream sourceImage, int maxHeight, string saveToPath)
        {
            try
            {
                using (var image = System.Drawing.Image.FromStream(sourceImage))
                {
                    decimal ratio = Convert.ToDecimal(image.Width) / Convert.ToDecimal(image.Height);
                    int newWidth = Convert.ToInt32(maxHeight * ratio);
                    int newHeight = maxHeight;

                    var thumbnailImg = new Bitmap(newWidth, newHeight);
                    var thumbGraph = Graphics.FromImage(thumbnailImg);
                    thumbGraph.CompositingQuality = CompositingQuality.HighQuality;
                    thumbGraph.SmoothingMode = SmoothingMode.HighQuality;
                    thumbGraph.InterpolationMode = InterpolationMode.HighQualityBicubic;
                    var imageRectangle = new System.Drawing.Rectangle(0, 0, newWidth, newHeight);
                    thumbGraph.DrawImage(image, imageRectangle);
                    thumbnailImg.Save(saveToPath, image.RawFormat);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
            finally
            {
                if (DateTime.Now.Subtract(lastmemoryUpdateTime).Seconds > 15)
                {
                    MemoryManagement.FlushMemory();
                    lastmemoryUpdateTime = DateTime.Now;
                }
            }
        }
        /// <summary>
        /// Resizes the WPF image.
        /// </summary>
        /// <param name="sourceImage">The source image.</param>
        /// <param name="maxHeight">The maximum height.</param>
        /// <param name="saveToPath">The save automatic path.</param>
        private void ResizeWPFImage(string sourceImage, int maxHeight, string saveToPath)
        {
            try
            {
                BitmapImage bi = new BitmapImage();
                BitmapImage bitmapImage = new BitmapImage();
                using (FileStream fileStream = File.OpenRead(sourceImage.ToString()))
                {
                    MemoryStream ms = new MemoryStream();
                    fileStream.CopyTo(ms);
                    ms.Seek(0, SeekOrigin.Begin);
                    fileStream.Close();
                    bi.BeginInit();
                    bi.StreamSource = ms;
                    bi.EndInit();
                    bi.Freeze();
                    decimal ratio = Convert.ToDecimal(bi.Width) / Convert.ToDecimal(bi.Height);

                    int newWidth = Convert.ToInt32(maxHeight * ratio);
                    int newHeight = maxHeight;

                    ms.Seek(0, SeekOrigin.Begin);
                    bitmapImage.BeginInit();
                    bitmapImage.StreamSource = ms;
                    bitmapImage.DecodePixelWidth = newWidth;
                    bitmapImage.DecodePixelHeight = newHeight;

                    bitmapImage.EndInit();
                    bitmapImage.Freeze();
                }

                using (var fileStreamForSave = new FileStream(saveToPath, FileMode.Create, FileAccess.ReadWrite))
                {
                    JpegBitmapEncoder encoder = new JpegBitmapEncoder();
                    encoder.QualityLevel = 94;
                    encoder.Frames.Add(BitmapFrame.Create(bitmapImage));
                    encoder.Save(fileStreamForSave);
                }
                bi = null;
                bitmapImage = null;
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
            }
            finally
            {
                if (DateTime.Now.Subtract(lastmemoryUpdateTime).Seconds > 15)
                {
                    MemoryManagement.FlushMemory();
                    lastmemoryUpdateTime = DateTime.Now;
                }
            }
        }
        /// <summary>
        /// Resizes the WPF image.
        /// </summary>
        /// <param name="sourceImage">The source image.</param>
        /// <param name="maxHeight">The maximum height.</param>
        /// <param name="saveToPath">The save automatic path.</param>
        /// <param name="rotatePath">The rotate path.</param>
        private bool ResizeWPFImage(string sourceImage, int maxHeight, string saveToPath, string rotatePath)
        {
            bool IsImageCorrupt = false;
            try
            {
                BitmapImage bi = new BitmapImage();
                BitmapImage bitmapImage = new BitmapImage();
                using (FileStream fileStream = File.OpenRead(sourceImage.ToString()))
                {
                    MemoryStream ms = new MemoryStream();
                    fileStream.CopyTo(ms);
                    ms.Seek(0, SeekOrigin.Begin);
                    fileStream.Close();
                    bi.BeginInit();
                    bi.StreamSource = ms;
                    bi.EndInit();
                    bi.Freeze();

                    decimal ratio = 0;
                    int newWidth = 0;
                    int newHeight = 0;

                    if (bi.Width >= bi.Height)
                    {
                        ratio = Convert.ToDecimal(bi.Width) / Convert.ToDecimal(bi.Height);
                        newWidth = maxHeight;
                        newHeight = Convert.ToInt32(maxHeight / ratio);
                    }
                    else
                    {
                        ratio = Convert.ToDecimal(bi.Height) / Convert.ToDecimal(bi.Width);
                        newHeight = maxHeight;
                        newWidth = Convert.ToInt32(maxHeight / ratio);
                    }

                    ms.Seek(0, SeekOrigin.Begin);
                    bitmapImage.BeginInit();
                    bitmapImage.StreamSource = ms;
                    bitmapImage.DecodePixelWidth = newWidth;
                    bitmapImage.DecodePixelHeight = newHeight;

                    bitmapImage.EndInit();
                    bitmapImage.Freeze();
                    fileStream.Close();
                }

                using (var fileStreamForSave = new FileStream(saveToPath, FileMode.Create, FileAccess.ReadWrite))
                {
                    JpegBitmapEncoder encoder = new JpegBitmapEncoder();
                    encoder.QualityLevel = 94;
                    encoder.Frames.Add(BitmapFrame.Create(bitmapImage));
                    encoder.Save(fileStreamForSave);
                    fileStreamForSave.Close();
                }
                bi = null;
                bitmapImage = null;
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
                IsImageCorrupt = true;
            }
            return IsImageCorrupt;
        }

        /// <summary>
        /// Resizes the and save high quality image.
        /// </summary>
        /// <param name="image">The image.</param>
        /// <param name="pathToSave">The path automatic save.</param>
        /// <param name="quality">The quality.</param>
        /// <param name="height">The height.</param>
        /// <exception cref="System.ArgumentOutOfRangeException"></exception>
        private void ResizeAndSaveHighQualityImage(System.Drawing.Image image, string pathToSave, int quality, int height)
        {
            try
            {

                // the resized result bitmap
                decimal ratio = Convert.ToDecimal(image.Width) / Convert.ToDecimal(image.Height);
                int width = Convert.ToInt32(height * ratio);

                using (Bitmap result = new Bitmap(width, height))
                {
                    // get the graphics and draw the passed image to the result bitmap
                    using (Graphics grphs = Graphics.FromImage(result))
                    {
                        grphs.CompositingQuality = System.Drawing.Drawing2D.CompositingQuality.HighQuality;
                        grphs.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
                        grphs.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
                        grphs.DrawImage(image, 0, 0, result.Width, result.Height);
                    }

                    // check the quality passed in
                    if ((quality < 0) || (quality > 100))
                    {
                        string error = string.Format("quality must be 0, 100", quality);
                        throw new ArgumentOutOfRangeException(error);
                    }

                    EncoderParameter qualityParam = new EncoderParameter(System.Drawing.Imaging.Encoder.Quality, quality);
                    string lookupKey = "image/jpeg";
                    var jpegCodec =
                        ImageCodecInfo.GetImageEncoders().Where(i => i.MimeType.Equals(lookupKey)).FirstOrDefault();

                    //create a collection of EncoderParameters and set the quality parameter
                    var encoderParams = new EncoderParameters(1);
                    encoderParams.Param[0] = qualityParam;
                    //save the image using the codec and the encoder parameter
                    result.Save(pathToSave, jpegCodec, encoderParams);
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
            }
        }
        #endregion
        #region Events
        /// <summary>
        /// Handles the Tick event of the timer1 control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private async void timer1_Tick(object sender, EventArgs e)
        {   //string s1 = DateTime.Now.ToString("HH:mm");
            //if (DateTime.Now.ToString("HH:mm") == restartTime)
            //{
            //    RestartApplication();
            //}

            timer.Stop();
            ShowMessage("File Watcher Step 11. Timer Tick :");
            var tasks = new List<Task>();
            try
            {
                if (string.IsNullOrEmpty(DownloadFolderPath))
                {
                    return;
                }
                DirectoryNameWithDate = Path.Combine(Directoryname, DateTime.Now.ToString("yyyyMMdd"));
                _thumbnailFilePathDate = Path.Combine(Directoryname, "Thumbnails", DateTime.Now.ToString("yyyyMMdd"));
                _bigThumbnailFilePathDate = Path.Combine(Directoryname, "Thumbnails_Big", DateTime.Now.ToString("yyyyMMdd"));
                _minifiedFilePathDate = Path.Combine(Directoryname, "Minified_Images", DateTime.Now.ToString("yyyyMMdd"));
                if (!Directory.Exists(DirectoryNameWithDate))
                    Directory.CreateDirectory(DirectoryNameWithDate);
                if (!Directory.Exists(_thumbnailFilePathDate))
                    Directory.CreateDirectory(_thumbnailFilePathDate);
                if (!Directory.Exists(_bigThumbnailFilePathDate))
                    Directory.CreateDirectory(_bigThumbnailFilePathDate);
                if (!Directory.Exists(_minifiedFilePathDate))
                    Directory.CreateDirectory(_minifiedFilePathDate);

                ShowMessage("File Watcher Step 12. Timer Tick : 2141");
                DirectoryInfo _objnew = new DirectoryInfo(DownloadFolderPath);
                if (_objnew.Exists)
                {
                    FileInfo[] lstFileInfo;
                    lstFileInfo = _objnew.EnumerateFiles()
                                           .Where(f => mediaExtensions.Contains(f.Extension.ToLower()) && f.Name.Contains("@")).OrderBy(f => f.CreationTime).Take(MaxThreadCount).ToArray();
                    List<FileInfo> downloadedFiles = lstFileInfo.Where(x => !FilesToDelete.Contains(x.FullName.ToLower())).OrderBy(x => x.CreationTime).ToList();
                    ShowMessage("File Watcher Step 13. Timer Tick : 2149");
                    if (downloadedFiles.Count > 0)
                    {
                        this.Dispatcher.Invoke(() =>
                        {
                            ImgGrd.Visibility = Visibility.Visible;
                            txtProcessing.Text = "Processing...";
                            UpdateTotalCounter();
                        });
                        ShowMessage("File Watcher Step 13-2. Timer Tick : 2158");
                        List<FileInfoViewModel> filesList = new List<FileInfoViewModel>();
                        long displayOrderNumber = 0;
                        int batchCount = MaxThreadCount;

                        if (downloadedFiles.Count < MaxThreadCount)
                        {
                            batchCount = downloadedFiles.Count;
                        }

                        displayOrderNumber = photoBusiness.GetPhotosDisplayOrder(batchCount);

                        foreach (var item in downloadedFiles)
                        {
                            displayOrderNumber += 1;
                            filesList.Add(new FileInfoViewModel() { FileInfo = item, DisplayOrder = displayOrderNumber });
                        }
                        ShowMessage("File Watcher Step 14. Timer Tick : 2175");
                        tasks.Add(Task.Run(() =>
                        {
                            Parallel.ForEach(filesList, item =>
                            {
                                ErrorHandler.ErrorHandler.LogFileWrite("File Name :" + item.FileInfo.Name);
                                if (item.FileInfo.Extension.ToLower().Equals(".jpg"))
                                {
                                    if (!IsFileLocked(item.FileInfo))
                                    {
                                        EventRaised(item.FileInfo, item.DisplayOrder);
                                    }
                                }
                                else
                                {
                                    TimerFunction(item.FileInfo, item.DisplayOrder);
                                }
                            });
                        }));
                        ShowMessage("File Watcher Step 15. Timer Tick : 2194");
                    }
                    else
                    {
                        ImgGrd.Visibility = Visibility.Hidden;
                    }
                    ShowMessage("File Watcher Step 16. Timer Tick :");
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
            }
            finally
            {
                await Task.WhenAll(tasks);
                UpdateTotalCounter();
                timer.Start();
                ShowMessage("File Watcher Step 17. Timer Tick :");
            }
        }
        public async Task TimerFunction(FileInfo file, long displayOrderNumber)
        {
            try
            {
                ShowMessage("File Wathcer TimerFunction Step 1. Line Number : 2219");
                int LocationID = 0;
                int photoId;
                string _videoFileName = Path.GetFileNameWithoutExtension(file.Name);
                //Process each video media file
                //1) Move Video File,  2) Create Video Thumbnail, 3) Add entry in DG_photos.
                string userId = file.Name.Split('@')[1].Split('.')[0]; //user id for processed video
                bool IsGreenScreenVideo = false;
                bool IsProcessedVideo = false;
                string fileName = file.Name.Split('@')[0];
                string downloadpath = file.DirectoryName + "\\";
                PhotoBusiness photoBiz = new PhotoBusiness();
                ShowMessage("File Wathcer TimerFunction Step 2. Line Number : 2231");
                if (userId.Contains("_PR"))
                {
                    IsProcessedVideo = true;

                    userId = userId.Split('_')[0]; //user id for greenscreen

                }
                if (userId.Contains("_GS"))
                {
                    IsGreenScreenVideo = true;
                    userId = userId.Split('_')[0]; //user id for greenscreen
                }
                int vidCount = photoBiz.PhotoCountCurrentDate(RFID: fileName, photographerID: Convert.ToInt32(userId), mediaType: 2);
                UsersInfo userInfo = (new UserBusiness().GetUserDetailByUserId(userId.ToInt32(), string.Empty, 0).FirstOrDefault());
                LocationID = userInfo.DG_Location_pkey;
                int substoreId = userInfo.DG_Substore_ID;
                ShowMessage("File Wathcer TimerFunction Step 3. Line Number : 2248");
                SetNewConfigLocationValues(LocationID);
                DateTime curDate = (new CustomBusineses()).ServerDateTime();
                ShowMessage("File Wathcer TimerFunction Step 4. Line Number : 2251");
                string videoFileName = string.Empty;
                string rfid = file.Name.Split('@')[0];

                if (IsGreenScreenVideo)
                {
                    mediaType = 2;
                    if (vidCount > 0)
                        videoFileName = curDate.Day.ToString() + curDate.Month.ToString() + file.Name.Split('@')[0] + "_GS" + "(" + vidCount + ")" + file.Extension;
                    else
                        videoFileName = curDate.Day.ToString() + curDate.Month.ToString() + file.Name.Split('@')[0] + "_GS" + file.Extension;
                    string videoSavePath = Path.Combine(Directoryname, "Videos", DateTime.Now.ToString("yyyyMMdd"));
                    if (!Directory.Exists(videoSavePath))
                        Directory.CreateDirectory(videoSavePath);
                    file.MoveTo(Path.Combine(videoSavePath, videoFileName));
                    ShowMessage("File Wathcer TimerFunction Step 5. Line Number : 2266");
                    FileInfo fi = new FileInfo(Path.Combine(videoSavePath, videoFileName));
                    //Create Minified Video by Suraj ....
                    CreateMinifiedVideo(Path.Combine(videoSavePath, videoFileName), videoSavePath, videoFileName);
                    // End Changes ...
                    //fi.CopyTo(Path.Combine(videoSavePath, "m_" + videoFileName)); //Create minified video

                }
                else if (IsProcessedVideo)
                {
                    ShowMessage("File Wathcer TimerFunction Step 6. Line Number : 2276");
                    mediaType = 3;
                    int processedVidCount = photoBiz.PhotoCountCurrentDate(RFID: fileName, photographerID: Convert.ToInt32(userId), mediaType: mediaType);
                    if (processedVidCount > 0)
                        videoFileName = curDate.Day.ToString() + curDate.Month.ToString() + file.Name.Split('@')[0] + "(" + processedVidCount + ")" + file.Extension;
                    else
                        videoFileName = curDate.Day.ToString() + curDate.Month.ToString() + file.Name.Split('@')[0] + file.Extension;
                    string videoSavePath = Path.Combine(Directoryname, "ProcessedVideos", DateTime.Now.ToString("yyyyMMdd"));
                    if (!Directory.Exists(videoSavePath))
                        Directory.CreateDirectory(videoSavePath);
                    file.MoveTo(Path.Combine(videoSavePath, videoFileName));
                    FileInfo fi = new FileInfo(Path.Combine(videoSavePath, videoFileName));
                    //Create Minified Video by Suraj ....
                    CreateMinifiedVideo(Path.Combine(videoSavePath, videoFileName), videoSavePath, videoFileName);
                    // End Changes ...
                    //fi.CopyTo(Path.Combine(videoSavePath, "m_" + videoFileName)); //Create minified video
                }
                else
                {
                    ShowMessage("File Wathcer TimerFunction Step 7. Line Number : 2295");
                    mediaType = 2;
                    if (vidCount > 0)
                        videoFileName = curDate.Day.ToString() + curDate.Month.ToString() + file.Name.Split('@')[0] + "(" + vidCount + ")" + file.Extension;
                    else
                        videoFileName = curDate.Day.ToString() + curDate.Month.ToString() + file.Name.Split('@')[0] + file.Extension;
                    string videoSavePath = Path.Combine(Directoryname, "Videos", DateTime.Now.ToString("yyyyMMdd"));
                    if (!Directory.Exists(videoSavePath))
                        Directory.CreateDirectory(videoSavePath);
                    file.MoveTo(Path.Combine(videoSavePath, videoFileName));
                    FileInfo fi = new FileInfo(Path.Combine(videoSavePath, videoFileName));
                    // Create Minified Video By Suraj....
                    CreateMinifiedVideo(Path.Combine(videoSavePath, videoFileName), videoSavePath, videoFileName);
                    //End Changes ...
                    //fi.CopyTo(Path.Combine(videoSavePath, "m_" + videoFileName)); //Create minified video
                }
                ShowMessage("File Wathcer TimerFunction Step 8. Line Number : 2311");
                //Save Video Thumbnail.
                long videoLength;
                string filSavePath = Path.Combine(_thumbnailFilePathDate, Path.GetFileNameWithoutExtension(file.FullName) + ".jpg");
                string minifidFilePath = Path.Combine(_thumbnailFilePathDate, "m_" + Path.GetFileNameWithoutExtension(file.FullName) + ".jpg");
                ShowMessage("File Wathcer TimerFunction Step 9. Line Number : 2316");
                //ThumbnailExtractor.ExtractThumbnailFromVideo(file.FullName, 4, 4, filSavePath, _VisioForgeLicenseKey, _VisioForgeUserName, _VisioForgeEmailID);
                string FilePath = FrameworkHelper.Common.MLHelpers.ExtractThumbnail(file.FullName);
                if (!string.IsNullOrEmpty(FilePath))
                {
                    FrameworkHelper.Common.MLHelpers.ResizeImage(FilePath, 210, filSavePath);
                    File.Copy(filSavePath, minifidFilePath);
                }
                ShowMessage("File Wathcer TimerFunction Step 10. Line Number : 2324");
                //Extract video frames by reading from XML and save it into Frame Folder
                if (IsFrameExtractionEnabled && !IsProcessedVideo)
                {
                    List<double> framesec = ReadXml();

                    if (framesec != null || framesec.Count > 0)
                    {
                        List<string> FrameFilePath = FrameworkHelper.Common.MLHelpers.ExtractFrame(file.FullName, framesec, userId, fileName, cropVideoFrameRatio);

                        //Move File into download folder
                        foreach (string derivedFrame in FrameFilePath)
                        {
                            string filename = Path.GetFileName(derivedFrame);
                            File.Move(derivedFrame, downloadpath + filename);
                        }
                    }
                }
                ShowMessage("File Wathcer TimerFunction Step 11. Line Number : 2342");
                watchersetting = VBusiness.GetSettingWatcher(LocationID);

                int VideoSceneCount = watchersetting.Where(e => e.SceneName != "" && (e.IsMixerScene == null || e.IsMixerScene == false)).Count();
                int ProfileNameCount = watchersetting.Where(e => e.SceneName != "" && e.IsMixerScene == true).Count();
                videoLength = Convert.ToInt64(FrameworkHelper.Common.MLHelpers.VideoLength);
                isVideoProcessed = true;
                ShowMessage("File Wathcer TimerFunction Step 12. Line Number : 2349");
                if (_IsAdvancedVideoEditActive)
                {

                    if (!IsProcessedVideo)     //Specify the GreenscreenVideo and guest video
                        if (VideoSceneCount > 0)  //look in the Videoscene table for scene name
                            isVideoProcessed = false;

                }
                if (isAutoVideoProcessingActive && isVideoProcessed == true)
                    if (!IsProcessedVideo)
                        if (ProfileNameCount > 0)
                            isVideoProcessed = false;
                ShowMessage("File Wathcer TimerFunction Step 13. Line Number : 2362");
                int? charId = (new CharacterBusiness()).GetCharacterId(userId);
                photoId = photoBiz.SetPhotoDetails(substoreId, rfid, videoFileName, curDate, userId, null, LocationID, "", "", null, RfidScanType, 1, charId, videoLength, isVideoProcessed, mediaType, photoDisplayOrder: displayOrderNumber);
                videoCount++;
                ShowMessage("File Wathcer TimerFunction Step 14. Line Number : 2366");
                //UpdateTotalCounter();
                if (IsMobileRfidEnabled)
                {
                    await MobileRfidAssociationCheck(photoId, _videoFileName);
                }
                ShowMessage("File Wathcer TimerFunction Step 15. Line Number : 2372");
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
            finally
            {
                MemoryManagement.FlushMemory();
            }
            //file.MoveTo(System.IO.Path.Combine(Directoryname, "Archived", file.Name));
        }
        // Create MinifiedVideos Function is created by Suraj.
        private void CreateMinifiedVideo(string sourceFilePath, string tragetFilePath, string minifiedFileName)
        {
            try
            {
                string file = sourceFilePath;

                ErrorHandler.ErrorHandler.LogFileWrite("Minified Video Crate Start");
                string command = "C:\\WinFF\\ffmpeg.exe -i " + @file + " -vf scale=480:-2,setsar=1:1 -c:v libx264 -c:a copy -crf 20 " + @tragetFilePath + "\\m_" + minifiedFileName;
                //string command = "C:\\Program Files (x86)\\iMix\\WinFF\\ffmpeg.exe -i " + @file + " -vf scale=480:-2,setsar=1:1 -c:v libx264 -c:a copy -crf 20 " + @tragetFilePath + "\\m_" + minifiedFileName;  // For deployment.
                ErrorHandler.ErrorHandler.LogFileWrite("Line Number : 2422  Command :" + command);
                ProcessStartInfo procStartInfo = new ProcessStartInfo("cmd", "/c " + command);

                procStartInfo.RedirectStandardOutput = false;
                procStartInfo.UseShellExecute = false;
                procStartInfo.CreateNoWindow = true;

                // wrap IDisposable into using (in order to release hProcess) 
                using (Process process = new Process())
                {
                    process.StartInfo = procStartInfo;
                    process.Start();
                    ErrorHandler.ErrorHandler.LogFileWrite("Line Number : 2434  Command :" + process.StandardOutput.ReadToEnd());
                    process.WaitForExit();
                    process.WaitForExit();

                    ///and only then read the result
                    //string result = process.StandardOutput.ReadToEnd();
                    //Console.WriteLine(result);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }
        // End Changes
        private List<double> ReadXml()
        {
            if (videoFramePositions != "Empty")
            {
                var xmlDocument = new XmlDocument();
                xmlDocument.LoadXml(videoFramePositions);
                XmlNodeList itemNodes = xmlDocument.SelectNodes("Frames/frame");
                List<double> frameSec = new List<double>();
                foreach (XmlNode loc in itemNodes)
                {
                    frameSec.Add(Convert.ToDouble(loc.InnerText));
                }
                return frameSec;
            }
            return null;
        }

        #region Old UpdateTotalCounter method.
        //private void UpdateTotalCounter()
        //{
        //    try
        //    {
        //        DirectoryInfo drinfo = new DirectoryInfo(DownloadFolderPath);
        //        int pending = 0;
        //        int archived = 0;
        //        if (drinfo.Exists)
        //        {
        //            pending = drinfo.EnumerateFiles().Count(f => mediaExtensions.Contains(f.Extension.ToLower()) && f.Name.Contains("@"));
        //        }
        //        ErrorHandler.ErrorHandler.LogFileWrite("Update Counter  :   Download Info   : 2442");
        //        DirectoryInfo drinfoArchived = new DirectoryInfo(ArchivedFolderPath);
        //        if (drinfoArchived.Exists)
        //        {
        //            archived = drinfoArchived.EnumerateFiles().Count(f => mediaExtensions.Contains(f.Extension.ToLower()) && f.Name.Contains("@"));
        //        }
        //        int totalFiles = pending + archived + videoCount;
        //        ErrorHandler.ErrorHandler.LogFileWrite("Update Counter  :   Archived Info   : 2449");
        //        if (totalFiles > 0)
        //        {
        //            txbtotalfiles.Text = totalFiles.ToString();
        //            prgbar.Maximum = totalFiles;
        //            txbProcessed.Text = (archived + videoCount).ToString();// counter.ToString();
        //            prgbar.Value = archived + videoCount;// counter;
        //        }
        //        ErrorHandler.ErrorHandler.LogFileWrite("Update Counter  :   Total Count    : 2457");
        //        if (IsAutoPurchaseActive)
        //        {
        //            if (lstImgInfoPreScanList.Count > 0 && (DateTime.Now - lastOrderChecktime).TotalSeconds > GapTimeInSec)
        //            {
        //                lstImgInfoPreScanList.ForEach(a =>
        //                {
        //                    if ((DateTime.Now - a.LastImgTime).TotalSeconds > GapTimeInSec)
        //                    {
        //                        AutoPurchasePostScan(a);
        //                        lstImgInfoPreScanList.Remove(a);
        //                    }
        //                }
        //                );
        //                lastOrderChecktime = DateTime.Now;
        //            }
        //        }
        //        ErrorHandler.ErrorHandler.LogFileWrite("Update Counter  :   Auto Purchase   : 2474");
        //    }
        //    catch(Exception ex)
        //    {
        //        ErrorHandler.ErrorHandler.LogFileWrite("Update Counter  :   Error Message  : "+ex.Message + "  Stack Trace   : "+ ex.StackTrace);
        //    }
        //}
        #endregion
        private void UpdateTotalCounter()
        {
            try
            {
                //ErrorHandler.ErrorHandler.LogFileWrite("Update Counter  :   Download Info   : 2496");
                DirectoryInfo drinfo = new DirectoryInfo(DownloadFolderPath);
                int pending = 0;
                int archived = 0;
                if (drinfo.Exists)
                {
                    pending = drinfo.EnumerateFiles().Count(f => mediaExtensions.Contains(f.Extension.ToLower()) && f.Name.Contains("@"));
                }
                //ErrorHandler.ErrorHandler.LogFileWrite("Update Counter  :   Download Info   : 2504");
                DirectoryInfo drinfoArchived = new DirectoryInfo(ArchivedFolderPath);
                if (drinfoArchived.Exists)
                {
                    //ErrorHandler.ErrorHandler.LogFileWrite("Update Counter  :   Download Info   : 2507");
                    archived = drinfoArchived.EnumerateFiles().Count(f => mediaExtensions.Contains(f.Extension.ToLower()) && f.Name.Contains("@"));
                    ImageProcessingCount += archived;
                    //ErrorHandler.ErrorHandler.LogFileWrite("Update Counter  :   Download Info   : 2510");
                    DirectoryInfo archivedFiles = new DirectoryInfo(ArchivedFolderPath);
                    foreach (var item in archivedFiles.GetFiles())
                    {
                        try
                        {
                            if (!IsFileLocked(item))
                            {
                                item.Delete();
                            }
                        }
                        catch (Exception ex)
                        {
                            ErrorHandler.ErrorHandler.LogFileWrite("Exception : Update Counter  :   Archived Delete   : Masg : " + ex.Message);
                        }
                    }
                }
                //int totalFiles = pending + archived + videoCount;
                int totalFiles = pending + ImageProcessingCount + videoCount;
                //ErrorHandler.ErrorHandler.LogFileWrite("Update Counter  :   Archived Info   : 2529");
                if (totalFiles > 0)
                {
                    //ErrorHandler.ErrorHandler.LogFileWrite("Update Counter  :   Download Info   : 2533");
                    txbtotalfiles.Text = totalFiles.ToString();
                    prgbar.Maximum = totalFiles;
                    txbProcessed.Text = (ImageProcessingCount + videoCount).ToString();//(archived + videoCount).ToString();
                    prgbar.Value = ImageProcessingCount + videoCount;//archived + videoCount;
                }
                //ErrorHandler.ErrorHandler.LogFileWrite("Update Counter  :   Total Count    : 2537");
                if (IsAutoPurchaseActive)
                {
                    if (lstImgInfoPreScanList.Count > 0 && (DateTime.Now - lastOrderChecktime).TotalSeconds > GapTimeInSec)
                    {
                        lstImgInfoPreScanList.ForEach(a =>
                        {
                            if ((DateTime.Now - a.LastImgTime).TotalSeconds > GapTimeInSec)
                            {
                                AutoPurchasePostScan(a);
                                lstImgInfoPreScanList.Remove(a);
                            }
                        }
                        );
                        lastOrderChecktime = DateTime.Now;
                    }
                }
                //ErrorHandler.ErrorHandler.LogFileWrite("Update Counter  :   Auto Purchase   : 2474");
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogFileWrite("Update Counter  :   Error Message  : " + ex.Message + "  Stack Trace   : " + ex.StackTrace);
            }
        }
        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            Process[] processes = Process.GetProcesses();
            foreach (Process process in processes)
            {
                if (process.ProcessName == "DigiWifiImageProcessing")
                {
                    try
                    {
                        ServiceController controller = new ServiceController("DigiWifiImageProcessing");
                        controller.Stop();
                        controller.WaitForStatus(ServiceControllerStatus.Stopped);
                    }
                    catch (Exception)
                    {
                        //handle any exception here 
                    }
                }
            }
            try
            {
                for (int i = 0; i < FilesToDelete.Count; i++)
                {
                    if (File.Exists(FilesToDelete[i]))
                    {
                        File.Move(FilesToDelete[i], Path.Combine(ArchivedFolderPath, Path.GetFileName(FilesToDelete[i])));
                        FilesToDelete.RemoveAt(i);
                    }
                }
            }
            catch (Exception)
            { }
            //try
            //{
            //    timer.Stop();
            //    DirectoryInfo drinfo = new DirectoryInfo(ArchivedFolderPath);
            //    FileInfo[] FInfoLst = drinfo.GetFiles();
            //    if (FInfoLst.Count() > 0)
            //    {
            //        MessageBox.Show("Archived Files are deleting....");
            //        DeleteTempFiles();
            //    }
            //}
            //catch (Exception ex)
            //{
            //    ErrorHandler.ErrorHandler.LogFileWrite("File Watcher :Window_Closing  : exMessage : " + ex.Message);
            //}

            foreach (Process process in processes)
            {
                if (process.ProcessName == "DigiWatcher")
                {
                    try
                    {
                        ServicePosInfoBusiness svcPosinfoBusiness = new ServicePosInfoBusiness();
                        svcPosinfoBusiness.StopService(true);
                        //MessageBox.Show("watcher stopped");
                        process.Kill();
                    }
                    catch (Exception)
                    {
                        //handle any exception here 
                    }
                }
            }
        }
        #endregion

        #region Common Methods

        private void AutoCorrectImageWIthCopy(string InputFilePath, string OutputFilePath)
        {
            try
            {
                using (FileStream fs = File.OpenRead(InputFilePath))
                {
                    using (MemoryStream ms = new MemoryStream())
                    {
                        fs.CopyTo(ms);
                        ms.Seek(0, SeekOrigin.Begin);

                        using (ImageMagick.MagickImage image = new ImageMagick.MagickImage(ms))
                        {
                            if (IsContrastCorrectionActive)
                                image.Contrast();
                            if (IsGammaCorrectionActive)
                                image.AutoGamma();// auto-gamma correction
                            if (IsNoiseReductionActive)
                                image.ReduceNoise();
                            //image.Normalize();
                            IsAutoColorCorrectionActive = false;
                            image.Write(OutputFilePath);
                            image.Dispose();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogFileWrite(ex.Message);
            }
        }

        /// <summary>
        /// Gets the configuration data.
        /// </summary>
        private void GetConfigData(int subStoreId)
        {
            try
            {
                ConfigurationInfo config = ConfigurationData.Where(x => x.DG_Substore_Id == subStoreId).FirstOrDefault();
                is_SemiOrder = Convert.ToBoolean(config.DG_SemiOrderMain);
                _BorderFolder = config.DG_Frame_Path.ToString();
                Directoryname = config.DG_Hot_Folder_Path;
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
            }
        }
        private UsersInfo FillUserData(int photographerid)
        {
            try
            {
                if (UserInfoList == null)
                {
                    ErrorHandler.ErrorHandler.LogFileWrite("UserInfoList is null");
                }
                ErrorHandler.ErrorHandler.LogFileWrite("Fill User data Is called  2692");
                UsersInfo userInfo = new UsersInfo();
                var result = UserInfoList.Where(x => x.DG_User_pkey == PhotoGrapherId);
                if (result != null && result.Count() > 0)
                {
                    userInfo = result.FirstOrDefault();
                }
                else
                {
                    if (userInfo != null)
                    {
                        userInfo = (new UserBusiness().GetUserDetailByUserId(PhotoGrapherId, string.Empty, 0).FirstOrDefault());
                        if (!UserInfoList.Contains(userInfo))
                        {
                            UserInfoList.Add(userInfo);
                        }
                    }
                    else
                    {
                        ErrorHandler.ErrorHandler.LogFileWrite("userInfo is null ");
                    }
                }

                //LocationID = userInfo.DG_Location_pkey;
                substoreId = userInfo.DG_Substore_ID;
                _locationName = userInfo.DG_Location_Name;
                return userInfo;
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
                ErrorHandler.ErrorHandler.LogFileWrite("FillUserData Inside Exception  ------------- ");
                return null;
            }
        }
        private void GetConfigDataList()
        {
            ConfigBusiness confObj = new ConfigBusiness();
            try
            {
                ErrorHandler.ErrorHandler.LogFileWrite("ConfigurationData Started");
                ConfigurationData = confObj.GetConfigurationData();
                if (ConfigurationData.Count > 0)
                {
                    ErrorHandler.ErrorHandler.LogFileWrite("ConfigurationData End" + ConfigurationData[0].DG_BG_Path);

                }
                ErrorHandler.ErrorHandler.LogFileWrite("ConfigurationData End 0 count");
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
            }
            finally { confObj = null; }
        }
        private static RotateFlipType OrientationToFlipType(string orientation)
        {
            switch (int.Parse(orientation))
            {
                case 1:
                    return RotateFlipType.RotateNoneFlipNone;
                    break;
                case 2:
                    return RotateFlipType.RotateNoneFlipX;
                    break;
                case 3:
                    return RotateFlipType.Rotate180FlipNone;
                    break;
                case 4:
                    return RotateFlipType.Rotate180FlipX;
                    break;
                case 5:
                    return RotateFlipType.Rotate90FlipX;
                    break;
                case 6:
                    {
                        return RotateFlipType.Rotate90FlipNone;
                        isrotated = true;
                        break;
                    }
                case 7:
                    return RotateFlipType.Rotate270FlipX;
                    break;
                case 8:
                    {
                        return RotateFlipType.Rotate270FlipNone;
                        isrotated = true;
                    }
                    break;
                default:
                    return RotateFlipType.RotateNoneFlipNone;
            }
        }
        private bool SetUniqueCodeInfoForPreScan(int PhotoId, string BigThumbnailPath)
        {
            bool isCode = false;
            ImgInfoPostScanList item = null;
            //objDigiPhotoDataServices = new DigiPhotoDataServices();
            PhotoBusiness phBusiness = new PhotoBusiness();
            try
            {
                BarcodeReaderLib.barcode barcodeInfo = GetBarcodeInfo(BigThumbnailPath);

                if (barcodeInfo != null && ((int)barcodeInfo.BarcodeType == (int)BarcodeTypes.Code128 || (int)barcodeInfo.BarcodeType == (int)BarcodeTypes.Code39 || (int)barcodeInfo.BarcodeType == (int)BarcodeTypes.QRCode))
                {
                    UniqueCode = barcodeInfo.Text;
                    if (!String.IsNullOrWhiteSpace(QRWebURLReplace))
                        UniqueCode = UniqueCode.Replace(QRWebURLReplace, "");

                    codeFormat = barcodeInfo.BarcodeType.ToString();

                    if (lstImgInfoPreScanList != null)
                        item = lstImgInfoPreScanList.Where(x => x.PhotoGrapherId == PhotoGrapherId).FirstOrDefault();

                    if (item != null)
                    {
                        AutoPurchasePostScan(item);
                        int ind = lstImgInfoPreScanList.FindIndex(x => x.PhotoGrapherId == PhotoGrapherId);
                        if (ind >= 0 && ind < lstImgInfoPreScanList.Count)
                            lstImgInfoPreScanList.RemoveAt(lstImgInfoPreScanList.FindIndex(x => x.PhotoGrapherId == PhotoGrapherId));
                    }

                    phBusiness.DeletePhotoByPhotoId(PhotoId);
                    ImgInfoPostScanList imgInfoPreScanList = new ImgInfoPostScanList();
                    imgInfoPreScanList.Barcode = UniqueCode;
                    imgInfoPreScanList.Format = codeFormat;
                    imgInfoPreScanList.PhotoGrapherId = PhotoGrapherId;
                    imgInfoPreScanList.ListImgPhotoId.Add(PhotoId);
                    imgInfoPreScanList.LastImgTime = DateTime.Now;
                    lstImgInfoPreScanList.Add(imgInfoPreScanList);

                    isCode = false;
                }
                else
                {
                    if (lstImgInfoPreScanList != null)
                        item = lstImgInfoPreScanList.Where(x => x.PhotoGrapherId == PhotoGrapherId).FirstOrDefault();
                    if (item != null)
                    {
                        DateTime dt = DateTime.Now;
                        if ((dt - item.LastImgTime).TotalSeconds <= GapTimeInSec)
                        {
                            foreach (var obj in lstImgInfoPreScanList.Where(x => x.PhotoGrapherId == PhotoGrapherId))
                            {
                                obj.ListImgPhotoId.Add(PhotoId);
                                obj.LastImgTime = dt;
                            }
                        }
                        else
                        {
                            AutoPurchasePostScan(item);
                            int ind = lstImgInfoPreScanList.FindIndex(x => x.PhotoGrapherId == PhotoGrapherId);
                            if (ind >= 0 && ind < lstImgInfoPreScanList.Count)
                                lstImgInfoPreScanList.RemoveAt(lstImgInfoPreScanList.FindIndex(x => x.PhotoGrapherId == PhotoGrapherId));

                            ImgInfoPostScanList imgInfoPreScanList = new ImgInfoPostScanList();
                            imgInfoPreScanList.Barcode = "1111";
                            imgInfoPreScanList.Format = "Lost";
                            imgInfoPreScanList.PhotoGrapherId = PhotoGrapherId;
                            imgInfoPreScanList.ListImgPhotoId.Add(PhotoId);
                            imgInfoPreScanList.LastImgTime = DateTime.Now;
                            lstImgInfoPreScanList.Add(imgInfoPreScanList);
                        }
                    }
                    else
                    {
                        ImgInfoPostScanList imgInfoPreScanList = new ImgInfoPostScanList();
                        imgInfoPreScanList.Barcode = "1111";
                        imgInfoPreScanList.Format = "Lost";
                        imgInfoPreScanList.PhotoGrapherId = PhotoGrapherId;
                        imgInfoPreScanList.ListImgPhotoId.Add(PhotoId);
                        imgInfoPreScanList.LastImgTime = DateTime.Now;
                        lstImgInfoPreScanList.Add(imgInfoPreScanList);
                    }
                    isCode = true;
                }
            }

            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
            }
            return isCode;
        }
        private bool SetUniqueCodeInfoForPostScan(int PhotoId, string BigThumbnailPath)
        {
            bool isCode = true;
            // objDigiPhotoDataServices = new DigiPhotoDataServices();
            PhotoBusiness phBusines = new PhotoBusiness();
            try
            {
                BarcodeReaderLib.barcode barcodeInfo = GetBarcodeInfo(BigThumbnailPath);

                if (barcodeInfo != null && ((int)barcodeInfo.BarcodeType == (int)BarcodeTypes.Code128 || (int)barcodeInfo.BarcodeType == (int)BarcodeTypes.Code39 || (int)barcodeInfo.BarcodeType == (int)BarcodeTypes.QRCode))
                {
                    UniqueCode = barcodeInfo.Text;

                    codeFormat = barcodeInfo.BarcodeType.ToString();
                    var item = lstImgInfoPostScanList.Where(x => x.PhotoGrapherId == PhotoGrapherId).FirstOrDefault();
                    if (item != null)
                    {
                        foreach (var obj in lstImgInfoPostScanList.Where(x => x.PhotoGrapherId == PhotoGrapherId))
                        {
                            obj.Format = codeFormat;
                            if (!String.IsNullOrWhiteSpace(QRWebURLReplace))
                                obj.Barcode = UniqueCode.Replace(QRWebURLReplace, "");
                            else
                                obj.Barcode = UniqueCode;
                        }
                        phBusines.DeletePhotoByPhotoId(PhotoId);
                    }
                    isCode = true;
                }
                else
                {
                    if (lstImgInfoPostScanList.Count <= 0)
                    {
                        ImgInfoPostScanList frstList = new ImgInfoPostScanList();
                        frstList.ListImgPhotoId.Add(PhotoId);
                        frstList.PhotoGrapherId = PhotoGrapherId;
                        frstList.LastImgTime = DateTime.Now;
                        lstImgInfoPostScanList.Add(frstList);
                    }
                    else
                    {
                        var item = lstImgInfoPostScanList.Where(x => x.PhotoGrapherId == PhotoGrapherId).FirstOrDefault();
                        if (item == null)
                        {
                            ImgInfoPostScanList frstList = new ImgInfoPostScanList();
                            frstList.ListImgPhotoId.Add(PhotoId);
                            frstList.PhotoGrapherId = PhotoGrapherId;
                            frstList.LastImgTime = DateTime.Now;
                            lstImgInfoPostScanList.Add(frstList);
                            isCode = false;
                        }
                        else
                        {
                            DateTime dt = DateTime.Now;
                            if ((dt - item.LastImgTime).TotalSeconds <= GapTimeInSec)
                            {
                                foreach (var obj in lstImgInfoPostScanList.Where(x => x.PhotoGrapherId == PhotoGrapherId))
                                {
                                    obj.ListImgPhotoId.Add(PhotoId);
                                    obj.LastImgTime = dt;
                                }
                            }
                            else
                            {
                                //objDigiPhotoDataServices = new DigiPhotoDataServices();
                                phBusines.SetImageAssociationInfoForPostScan(item.ListImgPhotoId, "Lost", "1111", IsAnonymousCodeActive);
                                foreach (var obj in lstImgInfoPostScanList.Where(x => x.PhotoGrapherId == PhotoGrapherId))
                                {
                                    obj.ListImgPhotoId.Clear();
                                    obj.ListImgPhotoId.Add(PhotoId);
                                    obj.LastImgTime = dt;
                                }
                            }
                        }
                    }
                    isCode = false;
                }
            }
            catch { }

            return isCode;
        }
        private BarcodeReaderLib.barcode GetBarcodeInfo(string imgpath)
        {
            BarcodeReaderLib.BarcodeList BarCodeList = null;
            BarcodeReaderLib.BarcodeList QRCodeList = null;
            BarcodeReaderLib.barcode barcodeInfo = null;

            switch (MappingType)
            {
                case 401:
                    BarcodeDecoder barcodeDecoder = new BarcodeReaderLib.BarcodeDecoder();
                    barcodeDecoder.BarcodeTypes = (int)(BarcodeReaderLib.EBarcodeTypes.QRCode);// | BarcodeReaderLib.EBarcodeTypes.QRCodeUnrecognized);
                    barcodeDecoder.DecodeFile(imgpath);
                    QRCodeList = barcodeDecoder.Barcodes;
                    if (QRCodeList.length > 0)
                        barcodeInfo = QRCodeList.item(0);
                    break;

                case 402:
                    BarcodeDecoder barcodeDecoder1 = new BarcodeReaderLib.BarcodeDecoder();
                    barcodeDecoder1.LinearFindBarcodes = 1;
                    barcodeDecoder1.BarcodeTypes = (int)(BarcodeReaderLib.EBarcodeTypes.Code128) | (int)(BarcodeReaderLib.EBarcodeTypes.Code39);
                    barcodeDecoder1.LinearShowSymbologyID = false;
                    barcodeDecoder1.LinearShowCheckDigit = false;
                    barcodeDecoder1.DecodeFile(imgpath);
                    BarCodeList = barcodeDecoder1.Barcodes;
                    if (BarCodeList.length > 0)
                        barcodeInfo = BarCodeList.item(0);
                    break;

                case 405:
                    BarcodeDecoder barcodeDecoder2 = new BarcodeReaderLib.BarcodeDecoder();
                    barcodeDecoder2.BarcodeTypes = (int)(BarcodeReaderLib.EBarcodeTypes.QRCode);// | BarcodeReaderLib.EBarcodeTypes.QRCodeUnrecognized);
                    barcodeDecoder2.DecodeFile(imgpath);
                    QRCodeList = barcodeDecoder2.Barcodes;
                    BarcodeDecoder barcodeDecoder3 = new BarcodeReaderLib.BarcodeDecoder();
                    barcodeDecoder3.LinearFindBarcodes = 1;
                    barcodeDecoder3.BarcodeTypes = (int)(BarcodeReaderLib.EBarcodeTypes.Code128) | (int)(BarcodeReaderLib.EBarcodeTypes.Code39);//.LinearUnrecognized);
                    barcodeDecoder3.LinearShowCheckDigit = false;
                    barcodeDecoder3.LinearShowSymbologyID = false;
                    barcodeDecoder3.DecodeFile(imgpath);
                    BarCodeList = barcodeDecoder3.Barcodes;

                    if (QRCodeList.length == 1 && BarCodeList.length == 1)
                    {
                        barcodeInfo = QRCodeList.item(0);
                    }
                    else if (QRCodeList.length == 1 && BarCodeList.length < 1)
                    {
                        barcodeInfo = QRCodeList.item(0);
                    }
                    else if (QRCodeList.length < 1 && BarCodeList.length == 1)
                    {
                        barcodeInfo = BarCodeList.item(0);
                    }
                    break;

                default:
                    break;
            }
            return barcodeInfo;
        }
        private void SetNewConfigValues()
        {
            //objDigiPhotoDataServices = new DigiPhotoDataServices();
            ConfigBusiness configBusiness = new ConfigBusiness();
            bool IsRfidEnabled = false;
            List<iMIXConfigurationInfo> _objNewConfigList = configBusiness.GetNewConfigValues(substoreId);
            foreach (iMIXConfigurationInfo _objNewConfig in _objNewConfigList)
            {
                switch (_objNewConfig.IMIXConfigurationMasterId)
                {
                    case (int)ConfigParams.IsBarcodeActive:
                        IsBarcodeActive = string.IsNullOrWhiteSpace(_objNewConfig.ConfigurationValue) ? false : Convert.ToBoolean(_objNewConfig.ConfigurationValue);
                        break;
                    case (int)ConfigParams.DefaultMappingCode:
                        MappingType = Convert.ToInt32(_objNewConfig.ConfigurationValue);
                        break;
                    case (int)ConfigParams.ScanType:
                        scanType = Convert.ToInt32(_objNewConfig.ConfigurationValue);
                        break;
                    case (int)ConfigParams.LostImgTimeGap:
                        GapTimeInSec = Convert.ToInt32(_objNewConfig.ConfigurationValue);
                        break;
                    case (int)ConfigParams.IsAnonymousQrCodeEnabled:
                        IsAnonymousCodeActive = string.IsNullOrWhiteSpace(_objNewConfig.ConfigurationValue) ? false : Convert.ToBoolean(_objNewConfig.ConfigurationValue);
                        break;
                    case (int)ConfigParams.Contrast:
                        defaultContrast = _objNewConfig.ConfigurationValue != null ? _objNewConfig.ConfigurationValue : "1";
                        break;
                    case (int)ConfigParams.Brightness:
                        defaultBrightness = _objNewConfig.ConfigurationValue != null ? _objNewConfig.ConfigurationValue : "0";
                        break;
                    case (int)ConfigParams.IsPrepaidAutoPurchaseActive:
                        IsAutoPurchaseActive = string.IsNullOrWhiteSpace(_objNewConfig.ConfigurationValue) ? false : Convert.ToBoolean(_objNewConfig.ConfigurationValue);
                        break;
                    case (int)ConfigParams.IsEnabledRFID:
                        IsRfidEnabled = string.IsNullOrWhiteSpace(_objNewConfig.ConfigurationValue) ? false : Convert.ToBoolean(_objNewConfig.ConfigurationValue);
                        break;
                    case (int)ConfigParams.RFIDScanType:
                        RfidScanType = string.IsNullOrWhiteSpace(_objNewConfig.ConfigurationValue) ? 0 : Convert.ToInt32(_objNewConfig.ConfigurationValue);
                        break;
                    case (int)ConfigParams.IsGreenScreenFlow:
                        IsGreenScreenWorkFlow = string.IsNullOrWhiteSpace(_objNewConfig.ConfigurationValue) ? false : Convert.ToBoolean(_objNewConfig.ConfigurationValue);
                        break;
                    //  case (int)ConfigParams.MediaPlayerLicenseKey:
                    //     _VisioForgeLicenseKey = _objNewConfig.ConfigurationValue != null ? _objNewConfig.ConfigurationValue : string.Empty;
                    //    break;

                    default:
                        break;
                }
            }
            RfidScanType = IsRfidEnabled == true ? RfidScanType : null;
        }
        bool IsFrameExtractionEnabled = false;
        private async Task SetNewConfigLocationValues(int locationId)
        {
            try
            {
                bool IsRfidEnabled = false;
                List<iMixConfigurationLocationInfo> _objNewConfigList = new List<iMixConfigurationLocationInfo>();
                try
                {
                    ErrorHandler.ErrorHandler.LogFileWrite("Sub-Store ID : " + substoreId + "  Location ID  : " + locationId + "  : 2990");
                    iMixConfigurationLocationInfoList result = null;
                    if (NewConfigList.Count > 0)
                    {
                        result = NewConfigList.Where(x => x.SubstoreId == substoreId && x.LocationId == locationId).FirstOrDefault();
                        ErrorHandler.ErrorHandler.LogFileWrite("Sub-Store ID : " + substoreId + "  Location ID  : " + locationId + "  : 2994");
                    }
                    if (result != null)
                    {
                        _objNewConfigList = result.iMixConfigurationLocationList;
                        ErrorHandler.ErrorHandler.LogFileWrite("Sub-Store ID : " + substoreId + "  Location ID  : " + locationId + "  : 3000");
                    }
                    else
                    {
                        ErrorHandler.ErrorHandler.LogFileWrite("Sub-Store ID : " + substoreId + "  Location ID  : " + locationId + "  : 3003");
                        ConfigBusiness configBusiness = new ConfigBusiness();
                        _objNewConfigList = configBusiness.GetConfigLocation(locationId, substoreId);
                        iMixConfigurationLocationInfoList configList = new iMixConfigurationLocationInfoList();
                        configList.iMixConfigurationLocationList = _objNewConfigList;
                        configList.SubstoreId = substoreId;
                        configList.LocationId = locationId;
                        NewConfigList.Add(configList);
                        ErrorHandler.ErrorHandler.LogFileWrite("Sub-Store ID : " + substoreId + "  Location ID  : " + locationId + "  : 3011");
                    }
                }
                catch (Exception ex)
                {
                    ErrorHandler.ErrorHandler.LogFileWrite("Exception : Error Massage  : " + ex.Message + "  StackTrace : " + ex.StackTrace);
                    ConfigBusiness configBusiness = new ConfigBusiness();
                    _objNewConfigList = configBusiness.GetConfigLocation(locationId, substoreId);
                    iMixConfigurationLocationInfoList configList = new iMixConfigurationLocationInfoList();
                    configList.iMixConfigurationLocationList = _objNewConfigList;
                    configList.SubstoreId = substoreId;
                    configList.LocationId = locationId;
                    NewConfigList.Add(configList);
                }

                foreach (iMixConfigurationLocationInfo _objNewConfig in _objNewConfigList)
                {
                    switch (_objNewConfig.IMIXConfigurationMasterId)
                    {
                        case (int)ConfigParams.IsBarcodeActive:
                            IsBarcodeActive = string.IsNullOrWhiteSpace(_objNewConfig.ConfigurationValue) ? false : Convert.ToBoolean(_objNewConfig.ConfigurationValue);
                            break;
                        case (int)ConfigParams.DefaultMappingCode:
                            MappingType = Convert.ToInt32(_objNewConfig.ConfigurationValue);
                            break;
                        case (int)ConfigParams.ScanType:
                            scanType = Convert.ToInt32(_objNewConfig.ConfigurationValue);
                            break;
                        case (int)ConfigParams.LostImgTimeGap:
                            GapTimeInSec = Convert.ToInt32(_objNewConfig.ConfigurationValue);
                            break;
                        case (int)ConfigParams.IsAnonymousQrCodeEnabled:
                            IsAnonymousCodeActive = string.IsNullOrWhiteSpace(_objNewConfig.ConfigurationValue) ? false : Convert.ToBoolean(_objNewConfig.ConfigurationValue);
                            break;
                        case (int)ConfigParams.Contrast:
                            defaultContrast = _objNewConfig.ConfigurationValue != null ? _objNewConfig.ConfigurationValue : "1";
                            break;
                        case (int)ConfigParams.Brightness:
                            defaultBrightness = _objNewConfig.ConfigurationValue != null ? _objNewConfig.ConfigurationValue : "0";
                            break;
                        case (int)ConfigParams.IsPrepaidAutoPurchaseActive:
                            IsAutoPurchaseActive = string.IsNullOrWhiteSpace(_objNewConfig.ConfigurationValue) ? false : Convert.ToBoolean(_objNewConfig.ConfigurationValue);
                            break;
                        case (int)ConfigParams.IsEnabledRFID:
                            IsRfidEnabled = string.IsNullOrWhiteSpace(_objNewConfig.ConfigurationValue) ? false : Convert.ToBoolean(_objNewConfig.ConfigurationValue);
                            break;
                        case (int)ConfigParams.IsMobileRfidEnabled:
                            IsMobileRfidEnabled = string.IsNullOrWhiteSpace(_objNewConfig.ConfigurationValue) ? false : Convert.ToBoolean(_objNewConfig.ConfigurationValue);
                            break;
                        case (int)ConfigParams.RFIDScanType:
                            //if (IsRfidEnabled == true && !string.IsNullOrWhiteSpace(_objNewConfig.ConfigurationValue))
                            if (!string.IsNullOrWhiteSpace(_objNewConfig.ConfigurationValue))
                                RfidScanType = Convert.ToInt32(_objNewConfig.ConfigurationValue);
                            else
                                RfidScanType = null;
                            break;
                        case (int)ConfigParams.IsEnableAutoVidProcessing:

                            isAutoVideoProcessingActive = string.IsNullOrWhiteSpace(_objNewConfig.ConfigurationValue) ? false : Convert.ToBoolean(_objNewConfig.ConfigurationValue);
                            break;
                        case (int)ConfigParams.IsAutoColorCorrection:
                            IsAutoColorCorrectionActive = string.IsNullOrWhiteSpace(_objNewConfig.ConfigurationValue) ? false : Convert.ToBoolean(_objNewConfig.ConfigurationValue);
                            break;
                        case (int)ConfigParams.IsCorrectAtDownloadActive:
                            IsCorrectionAtDownloadActive = string.IsNullOrWhiteSpace(_objNewConfig.ConfigurationValue) ? false : Convert.ToBoolean(_objNewConfig.ConfigurationValue);
                            break;
                        case (int)ConfigParams.IsContrastCorrection:
                            IsContrastCorrectionActive = string.IsNullOrWhiteSpace(_objNewConfig.ConfigurationValue) ? false : Convert.ToBoolean(_objNewConfig.ConfigurationValue);
                            break;
                        case (int)ConfigParams.IsGammaCorrection:
                            IsGammaCorrectionActive = string.IsNullOrWhiteSpace(_objNewConfig.ConfigurationValue) ? false : Convert.ToBoolean(_objNewConfig.ConfigurationValue);
                            break;
                        case (int)ConfigParams.IsNoiseReduction:
                            IsNoiseReductionActive = string.IsNullOrWhiteSpace(_objNewConfig.ConfigurationValue) ? false : Convert.ToBoolean(_objNewConfig.ConfigurationValue);
                            break;
                        case (int)ConfigParams.IsAdvancedVideoEditActive:
                            _IsAdvancedVideoEditActive = string.IsNullOrWhiteSpace(_objNewConfig.ConfigurationValue) ? false : Convert.ToBoolean(_objNewConfig.ConfigurationValue);
                            break;
                        case (int)ConfigParams.IsAutoVideoFrameExtEnabled:
                            IsFrameExtractionEnabled = string.IsNullOrWhiteSpace(_objNewConfig.ConfigurationValue) ? false : Convert.ToBoolean(_objNewConfig.ConfigurationValue);
                            break;
                        case (int)ConfigParams.VideoFramePositions:
                            videoFramePositions = _objNewConfig.ConfigurationValue != null ? _objNewConfig.ConfigurationValue : "Empty";
                            break;
                        case (int)ConfigParams.VideoFrameCropRatio:
                            cropVideoFrameRatio = _objNewConfig.ConfigurationValue != null ? _objNewConfig.ConfigurationValue : "None";
                            break;
                        default:
                            break;
                    }
                }
                RfidScanType = IsRfidEnabled == true ? RfidScanType : null;
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogFileWrite("Exception : Error Massage  : " + ex.Message + "  StackTrace : " + ex.StackTrace);
            }
        }
        /// <summary>
        /// Gets the bitmap image from path.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <returns></returns>
        public BitmapImage GetBitmapImageFromPath(string value)
        {
            BitmapImage bi = new BitmapImage();
            try
            {
                if (value != null)
                {
                    using (FileStream fileStream = File.OpenRead(value.ToString()))
                    {
                        MemoryStream ms = new MemoryStream();
                        fileStream.CopyTo(ms);
                        ms.Seek(0, SeekOrigin.Begin);
                        fileStream.Close();
                        bi.BeginInit();
                        bi.StreamSource = ms;
                        bi.EndInit();
                    }
                }
                else
                {
                    bi = new BitmapImage();
                }
                return bi;
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
                return bi;
            }
        }
        /// <summary>
        /// Saves the XML.
        /// </summary>
        /// <param name="bvalue">The bvalue.</param>
        /// <param name="childnode">if set to <c>true</c> [childnode].</param>
        /// <param name="rfid">The rfid.</param>
        /// <param name="cvalue">The cvalue.</param>
        /// <param name="fvalue">The fvalue.</param>
        /// <param name="fvvalue">The fvvalue.</param>
        /// <param name="BG">The debug.</param>
        /// <param name="glayer">The glayer.</param>
        /// <param name="photoId">The photo unique identifier.</param>
        /// <param name="ZoomDetails">The zoom details.</param>
        /// 
        public async Task SaveXml(SemiOrderSettings semiOrderSettings, int photoId, string filedatepath, string fileName, string GumBallRidetxtContent, string ImageEffect, int LocationID)
        {
            try
            {

                auditLogBusiness.InsertExceptionLog(Convert.ToString(Guid.NewGuid()), 0, DateTime.Now, DateTime.Now, "SaveXml", photoId, null, "3112");
                _QRCode = null;
                string layeringdata = "";
                StringBuilder PlayerXml = new StringBuilder();
                string[] NoOfPlayers;
                NoOfPlayers = GumBallRidetxtContent.Split(',');
                int ProductId = 0;
                System.Xml.XmlDocument Xdocument = new System.Xml.XmlDocument();

                try
                {
                    if (semiOrderSettings.DG_SemiOrder_ProductTypeId.Contains(','))
                        ProductId = 1;
                    else
                        ProductId = Convert.ToInt32(semiOrderSettings.DG_SemiOrder_ProductTypeId);
                    Xdocument.LoadXml(ImageEffect);

                    System.Xml.XmlNodeList list = Xdocument.SelectNodes("//image");

                    foreach (System.Xml.XmlElement XElement in list)
                    {
                        XElement.SetAttribute("brightness", Convert.ToString(semiOrderSettings.DG_SemiOrder_Settings_AutoBright_Value));
                    }

                    foreach (System.Xml.XmlElement XElement in list)
                    {
                        XElement.SetAttribute("contrast", Convert.ToString(semiOrderSettings.DG_SemiOrder_Settings_AutoContrast_Value));
                    }

                    if (semiOrderSettings.DG_SemiOrder_IsCropActive.ToBoolean())
                    {
                        foreach (System.Xml.XmlElement XElement in list)
                        {
                            if (ProductId == 1)
                                XElement.SetAttribute("Crop", "6 * 8");
                            else if (ProductId == 2 || ProductId == 3)
                                XElement.SetAttribute("Crop", "8 * 10");
                            else if (ProductId == 30 || ProductId == 5 || ProductId == 104)
                                XElement.SetAttribute("Crop", "4 * 6");
                            else if (ProductId == 98)
                                XElement.SetAttribute("Crop", "3 * 3");
                        }
                    }

                    if (semiOrderSettings.ProductName.Contains("6 * 8"))
                        ProductNamePreference = "1";
                    else if (semiOrderSettings.ProductName.Contains("8 * 10"))
                        ProductNamePreference = "2";
                    else if (semiOrderSettings.ProductName.Contains("4 * 6") || semiOrderSettings.ProductName.Contains("(4x6) & QR") || semiOrderSettings.ProductName.Contains("4 Small Wallets"))
                        ProductNamePreference = "30";

                    else if (semiOrderSettings.ProductName.Contains("6 * 20"))
                        ProductNamePreference = "125";
                    else if (semiOrderSettings.ProductName.Contains("6 * 14"))
                        ProductNamePreference = "123";
                    else if (semiOrderSettings.ProductName.Contains("8 * 18"))
                        ProductNamePreference = "122";
                    else if (semiOrderSettings.ProductName.Contains("8 * 26"))
                        ProductNamePreference = "121";
                    else if (semiOrderSettings.ProductName.Contains("8 * 24"))
                        ProductNamePreference = "120";

                    else
                        ProductNamePreference = "2";
                }
                catch (Exception ex)
                {
                    auditLogBusiness.InsertExceptionLog(Convert.ToString(Guid.NewGuid()), 0, DateTime.Now, DateTime.Now, "SaveXml", photoId, ex, "3189");
                }


                #region Gumball Code
                bool IsGumballShow = false;
                try
                {
                    if (GumBallActiveLocationList != null && GumBallActiveLocationList.Count > 0)
                        LstGumBallRideConfigValueLocationWise = GumBallConfigurationValueList.Where(x => x.LocationId.Equals(LocationID) && GumBallActiveLocationList.Contains(x.LocationId)).ToList();

                    if (LstGumBallRideConfigValueLocationWise != null && LstGumBallRideConfigValueLocationWise.Count > 0)
                    {
                        if (!string.IsNullOrWhiteSpace(NoOfPlayers[0]) && NoOfPlayers.Length != 1)
                        {
                            string[] playerPosition = GetAllPlayerPositions(LstGumBallRideConfigValueLocationWise.Where(x => x.IMIXConfigurationMasterId.Equals(Convert.ToInt32(ConfigParams.GumRidePosition))).Select(x => x.ConfigurationValue).FirstOrDefault());
                            string[] playerMargin = GetAllPlayerMargins(LstGumBallRideConfigValueLocationWise.Where(x => x.IMIXConfigurationMasterId.Equals(Convert.ToInt32(ConfigParams.GumRideMargin))).Select(x => x.ConfigurationValue).FirstOrDefault());

                            //--------Start----Nilesh-----------Dynamically Add Img----------24th Feb 2018--------
                            string[] playerBGImgPath = GetAllPlayerBGImg(LstGumBallRideConfigValueLocationWise.Where(x => x.IMIXConfigurationMasterId.Equals(Convert.ToInt32(ConfigParams.GumBGImagePath))).Select(x => x.ConfigurationValue).FirstOrDefault());
                            string[] playerBGImgHeigthWidth = GetAllPlayerBGImgHeightWidth(LstGumBallRideConfigValueLocationWise.Where(x => x.IMIXConfigurationMasterId.Equals(Convert.ToInt32(ConfigParams.GumBGImgHeightWidth))).Select(x => x.ConfigurationValue).FirstOrDefault());
                            string[] playerBGImgScorePostition = GetAllPlayerScoreBGImgPosition(LstGumBallRideConfigValueLocationWise.Where(x => x.IMIXConfigurationMasterId.Equals(Convert.ToInt32(ConfigParams.GumBGImgPosition))).Select(x => x.ConfigurationValue).FirstOrDefault());

                            //--------End----Nilesh-----------Dynamically Add Img----------24th Feb 2018--------

                            isVisibleZeroScoreOnImage = LstGumBallRideConfigValueLocationWise.Where(x => x.IMIXConfigurationMasterId.Equals(Convert.ToInt32(ConfigParams.IsVisibleGumBallZeroScoreOnImage))).Select(x => x.ConfigurationValue).FirstOrDefault();
                            int playercount = NoOfPlayers.Length;
                            if (playercount > 6)
                                playercount = 6;
                            for (int i = 0; i < playercount; i++)
                            {
                                try
                                {
                                    string playerPos = !String.IsNullOrEmpty(playerPosition[i]) ? playerPosition[i] : string.Empty;
                                    string playerMrg = !String.IsNullOrEmpty(playerMargin[i]) ? playerMargin[i] : string.Empty;
                                    //--------Start----Nilesh-----------Dynamically Add Img----------24th Feb 2018--------
                                    string playerimgpath = !String.IsNullOrEmpty(playerBGImgPath[i]) ? playerBGImgPath[i] : string.Empty;
                                    string playerimgHW = !String.IsNullOrEmpty(playerBGImgHeigthWidth[i]) ? playerBGImgHeigthWidth[i] : string.Empty;
                                    string playerScorePs = !String.IsNullOrEmpty(playerBGImgScorePostition[i]) ? playerBGImgScorePostition[i] : string.Empty;

                                    //--------End----Nilesh-----------Dynamically Add Img----------24th Feb 2018--------

                                    string[] fontcolorArray = LstGumBallRideConfigValueLocationWise.Where(x => x.IMIXConfigurationMasterId.Equals(Convert.ToInt32(ConfigParams.GumRideFontColor))).Select(x => x.ConfigurationValue).FirstOrDefault().Split(',');
                                    string[] BGcolorArray = LstGumBallRideConfigValueLocationWise.Where(x => x.IMIXConfigurationMasterId.Equals(Convert.ToInt32(ConfigParams.GumRideBackgrondColor))).Select(x => x.ConfigurationValue).FirstOrDefault().Split(',');
                                    if (isVisibleZeroScoreOnImage.ToUpper() == "FALSE")
                                        if (NoOfPlayers[i].Equals("0"))
                                            continue;
                                    PlayerXml.Append("<Gumball player='" + (i + 1) + "' zindex='7' text='Player" + (i + 1) + "=" + NoOfPlayers[i] + "' foreground='" + fontcolorArray[i] + "' fontfamily='" + LstGumBallRideConfigValueLocationWise.Where(x => x.IMIXConfigurationMasterId.Equals(Convert.ToInt32(ConfigParams.GumRideFontFamily))).
                                    Select(x => x.ConfigurationValue).FirstOrDefault() + "' fontsize='" + LstGumBallRideConfigValueLocationWise.Where(x => x.IMIXConfigurationMasterId.Equals(Convert.ToInt32(ConfigParams.GumRideFontSize))).
                                    Select(x => x.ConfigurationValue).FirstOrDefault() + "' fontweight='" + LstGumBallRideConfigValueLocationWise.Where(x => x.IMIXConfigurationMasterId.Equals(Convert.ToInt32(ConfigParams.GumRideFontWeight))).
                                    Select(x => x.ConfigurationValue).FirstOrDefault() + "' fontStyle='" + LstGumBallRideConfigValueLocationWise.Where(x => x.IMIXConfigurationMasterId.Equals(Convert.ToInt32(ConfigParams.GumRideFontStyle))).
                                    Select(x => x.ConfigurationValue).FirstOrDefault() + "' background='" + BGcolorArray[i] + "' "
                                   + " position='" + playerPos + "'" + " margin='" + playerMrg + "'"
                                    + " BGImgPath='" + playerimgpath + "'"
                                    + " BGImgHeightWidth='" + playerimgHW + "'"
                                    + " BGImgScorePosition='" + playerScorePs + "'"
                                    +
                                    " />");
                                }
                                catch (Exception ex)
                                {
                                    auditLogBusiness.InsertExceptionLog(Convert.ToString(Guid.NewGuid()), 0, DateTime.Now, DateTime.Now, "SaveXml", photoId, ex, "3234");
                                }
                            }
                        }
                        if (LstGumBallRideConfigValueLocationWise.Where(x => x.IMIXConfigurationMasterId.Equals(Convert.ToInt32(ConfigParams.IsGumPlayerScoreVisible))).Select(x => x.ConfigurationValue).FirstOrDefault().ToUpper() == "TRUE")
                        {
                            IsGumballShow = true;
                        }
                    }
                }
                catch (Exception ex)
                {
                    auditLogBusiness.InsertExceptionLog(Convert.ToString(Guid.NewGuid()), 0, DateTime.Now, DateTime.Now, "SaveXml", photoId, ex, "3312");
                }
                #endregion

                PhotoBusiness phBusiness = new PhotoBusiness();
                string fname = string.Empty;
                string[] zoomdetails = null;
                string graphicLayer = string.Empty;
                string textLogo = string.Empty;
                string background = string.Empty;
                bool IsCropActive = false;
                bool IsChromaActive = false;
                BitmapImage _objPhoto = GetBitmapImageFromPath(Path.Combine(filedatepath, fileName));
                //commented by ajay sinha on 16 july 2019 to resolve applying border to the normal- horizontal  image to  vertical.
                //if (_objPhoto.Height > _objPhoto.Width)
                try
                {
                    if ((_objPhoto.Height > _objPhoto.Width) || !String.IsNullOrWhiteSpace(semiOrderSettings.VerticalCropValues))
                    {
                        fname = Convert.ToString(semiOrderSettings.ImageFrame_Vertical);
                        zoomdetails = semiOrderSettings.ZoomInfo_Vertical.Split(',');
                        graphicLayer = semiOrderSettings.Graphics_layer_Vertical;
                        textLogo = semiOrderSettings.TextLogo_Vertical;
                        background = semiOrderSettings.Background_Vertical;

                        string Message1 = "3230 : " + "Photoid : " + photoId + "   Background : " + background + "  :  " + semiOrderSettings.DG_SemiOrder_Settings_IsImageBG.Value.ToString() + "  : " + photoId.ToString();
                        auditLogBusiness.InsertExceptionLog(Convert.ToString(Guid.NewGuid()), 0, DateTime.Now, DateTime.Now, "SaveXml", photoId, null, Message1);
                        if (semiOrderSettings.DG_SemiOrder_Settings_IsImageBG.Value && !String.IsNullOrWhiteSpace(background))
                            IsChromaActive = true;

                        if (semiOrderSettings.DG_SemiOrder_IsCropActive.ToBoolean() == true)
                            IsCropActive = String.IsNullOrWhiteSpace(semiOrderSettings.VerticalCropValues) == true ? false : true;
                    }
                    else
                    {
                        fname = Convert.ToString(semiOrderSettings.ImageFrame_Horizontal);
                        zoomdetails = semiOrderSettings.ZoomInfo_Horizontal.Split(',');
                        graphicLayer = semiOrderSettings.Graphics_layer_Horizontal;
                        textLogo = semiOrderSettings.TextLogo_Horizontal;
                        background = semiOrderSettings.Background_Horizontal;

                        if (semiOrderSettings != null && semiOrderSettings.DG_SemiOrder_Settings_IsImageBG != null)
                        {
                            string Message2 = "3274 : " + "Photoid : " + photoId + "   Background : " + background + "  :  " + semiOrderSettings?.DG_SemiOrder_Settings_IsImageBG.Value;
                            auditLogBusiness.InsertExceptionLog(Convert.ToString(Guid.NewGuid()), 0, DateTime.Now, DateTime.Now, "SaveXml", photoId, null, Message2);
                        }
                        else
                        {
                            auditLogBusiness.InsertExceptionLog(Convert.ToString(Guid.NewGuid()), 0, DateTime.Now, DateTime.Now, "SaveXml", photoId, null, "3288");
                        }
                        if (semiOrderSettings.DG_SemiOrder_Settings_IsImageBG.Value && !String.IsNullOrWhiteSpace(background))
                            IsChromaActive = true;
                        if (semiOrderSettings.DG_SemiOrder_IsCropActive.ToBoolean() == true)
                            IsCropActive = String.IsNullOrWhiteSpace(semiOrderSettings.HorizontalCropValues) == true ? false : true;
                    }
                }
                catch (Exception ex)
                {
                    auditLogBusiness.InsertExceptionLog(Convert.ToString(Guid.NewGuid()), 0, DateTime.Now, DateTime.Now, "SaveXml", photoId, ex, "3312");
                }
                string zoomFactor = "1";
                string canvasleft = "0";
                string canvastop = "0";
                string scalecentrex = "-1";
                string scalecentrey = "-1";
                // Vertical Layering Effects
                try
                {
                    if (zoomdetails.Count() > 1)
                    {
                        zoomFactor = zoomdetails[0];
                        canvasleft = zoomdetails[1];
                        canvastop = zoomdetails[2];
                        scalecentrex = zoomdetails[3];
                        scalecentrey = zoomdetails[4];
                    }
                    layeringdata = "<photo producttype='" + ProductNamePreference + "' zoomfactor='" + zoomFactor + "' border='" + fname + "' bg= '" + background + "' canvasleft='" + canvasleft + "' canvastop='" + canvastop + "' scalecentrex='" + scalecentrex + "' scalecentrey='" + scalecentrey + "'>" + graphicLayer + PlayerXml + textLogo + "</photo>";
                    ImageEffect = Xdocument.InnerXml.ToString();
                }
                catch (Exception ex)
                {
                    auditLogBusiness.InsertExceptionLog(Convert.ToString(Guid.NewGuid()), 0, DateTime.Now, DateTime.Now, "SaveXml", photoId, ex, "3320");
                }

                if (ProductId == 104 || semiOrderSettings.ProductName.Contains("(4x6) & QR") || ProductId == 124 || semiOrderSettings.ProductName.Contains("(4*6) & QR"))//Added by VINS
                {
                    _QRCode = GenerateQRCode.GetNextQRCode(_QRCodeLenth);
                }

                phBusiness.UpdateEffectsSpecPrint(photoId, layeringdata, ImageEffect, IsChromaActive, IsCropActive, IsGumballShow, GumBallRidetxtContent, _QRCode);
                GumBallRidetxtContent = string.Empty;
                PlayerXml.Length = 0;
            }
            catch (Exception ex)
            {
                auditLogBusiness.InsertExceptionLog(Convert.ToString(Guid.NewGuid()), 0, DateTime.Now, DateTime.Now, "SaveXml", photoId, ex, "3312");
            }
        }

        private Int32 QRCodeLength(Int32 subStoreId)
        {
            List<long> filterValues = new List<long>();
            filterValues.Add((Int32)ConfigParams.QRCodeLengthSetting);
            ConfigBusiness conBiz = new ConfigBusiness();
            iMIXConfigurationInfo ConfigValuesList = conBiz.GetNewConfigValues(subStoreId).Where(o => o.IMIXConfigurationMasterId == (Int32)ConfigParams.QRCodeLengthSetting).FirstOrDefault();
            if (ConfigValuesList != null)
                _QRCodeLenth = ConfigValuesList.ConfigurationValue != null ? Convert.ToInt32(ConfigValuesList.ConfigurationValue) : 0;
            return _QRCodeLenth;
        }
        private string[] GetAllPlayerPositions(string allPos)
        {
            string[] singlePos = allPos.Split(',');
            if (singlePos != null && singlePos.Length > 0)
                return singlePos;
            else
                return null;
        }
        private string[] GetAllPlayerMargins(string allMargins)
        {
            string[] singleMargin = allMargins.Split(':');
            if (singleMargin != null && singleMargin.Length > 0)
                return singleMargin;
            else
                return null;
        }
        /// <summary>
        /// Determines whether [is file locked] [the specified file].
        /// </summary>
        /// <param name="file">The file.</param>
        /// <returns></returns>
        protected bool IsFileLocked(FileInfo file)
        {
            FileStream stream = null;

            try
            {
                stream = file.Open(FileMode.Open, FileAccess.ReadWrite, FileShare.None);
            }
            catch (IOException)
            {
                //the file is unavailable because it is: 
                //still being written to 
                //or being processed by another thread 
                //or does not exist (has already been processed) 
                return true;
            }
            finally
            {
                if (stream != null)
                {
                    stream.Close();
                    stream.Dispose();
                }
            }

            //file is not locked 
            return false;
        }
        private bool ReadSystemOnlineStatus()
        {
            bool status = false;
            try
            {
                string pathtosave = System.IO.Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
                if (File.Exists(pathtosave + "\\Config.dat"))
                {
                    string OnlineStatus;
                    using (StreamReader reader = new StreamReader(pathtosave + "\\Config.dat"))
                    {
                        OnlineStatus = reader.ReadLine();
                        status = Convert.ToBoolean(OnlineStatus.Split(',')[1]);
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
            }
            return status;
        }
        private string GenerateOrderNumber()
        {
            string orderNumber = LoginUser.OrderPrefix + "-";
            string uniqueNumber = string.Empty;
            try
            {
                using (RNGCryptoServiceProvider rng = new RNGCryptoServiceProvider())
                {
                    // Buffer storage.
                    byte[] data = new byte[4];

                    // Ten iterations.                     
                    {
                        // Fill buffer.
                        rng.GetBytes(data);

                        // Convert to int 32.
                        int value = BitConverter.ToInt32(data, 0);
                        if (value < 0)
                            value = -value;
                        if (value.ToString().Length > 10)
                        {
                            uniqueNumber = value.ToString().Substring(0, 10);
                        }
                        else
                        {
                            uniqueNumber = value.ToString();
                        }
                    }
                }
            }
            catch (Exception ex)
            { ErrorHandler.ErrorHandler.LogFileWrite(ex.Message); }
            orderNumber = orderNumber + uniqueNumber;
            return orderNumber;
        }
        private void GetStoreName()
        {
            StoreSubStoreDataBusniess storeObj = new StoreSubStoreDataBusniess();
            List<DigiPhoto.IMIX.Model.StoreInfo> objstore = storeObj.GetStoreName();
            LoginUser.countrycode = objstore.FirstOrDefault().CountryCode;
            LoginUser.Storecode = objstore.FirstOrDefault().StoreCode;
        }
        public string ConvertDegreeAngleToDouble(string degrees, string minutes, string seconds)
        {
            //Decimal degrees = 
            //   whole number of degrees, 
            //   plus minutes divided by 60, 
            //   plus seconds divided by 3600

            return (Convert.ToDouble(degrees) + (Convert.ToDouble(minutes) / 60) + (Convert.ToDouble(seconds) / 3600)).ToString();
        }
        private void ResetLowDPI(string InputFilePath, string OutputFilePath)
        {
            try
            {
                System.Drawing.Image newImage = System.Drawing.Image.FromFile(InputFilePath);
                System.Drawing.Image outImage = ResetDPI.ScaleByHeightAndResolution(newImage, 2136, 300);

                if (IsAutoColorCorrectionActive && IsCorrectionAtDownloadActive)
                {
                    using (MemoryStream ms = new MemoryStream())
                    {
                        outImage.Save(ms, System.Drawing.Imaging.ImageFormat.Jpeg);
                        ms.Seek(0, SeekOrigin.Begin);

                        using (ImageMagick.MagickImage image = new ImageMagick.MagickImage(ms))
                        {
                            if (IsContrastCorrectionActive)
                                image.Contrast();
                            if (IsGammaCorrectionActive)
                                image.AutoGamma();// auto-gamma correction
                            if (IsNoiseReductionActive)
                                image.ReduceNoise();
                            //image.Normalize();
                            IsAutoColorCorrectionActive = false;
                            image.Write(OutputFilePath);
                            image.Dispose();
                        }
                    }
                }
                else
                {
                    outImage.Save(OutputFilePath);
                }
                newImage.Dispose();
                outImage.Dispose();
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogFileWrite(ex.Message);
            }
        }

        // Commented by Suraj for Rotate Image Issue.
        //private void RotateImage(string rotatePath)
        //{
        //    try
        //    {
        //        ImageMagickObject.MagickImage jmagic = new ImageMagickObject.MagickImage();

        //        if (needrotaion > 0)
        //        {
        //            if (needrotaion == 90)
        //            {
        //                object[] o = new object[] { "-rotate", " 90 ", rotatePath };
        //                jmagic.Mogrify(o);
        //                o = null;
        //                isrotated = true;
        //            }
        //            else if (needrotaion == 180)
        //            {
        //                object[] o = new object[] { "-rotate", " 180 ", rotatePath };
        //                jmagic.Mogrify(o);
        //                o = null;
        //                isrotated = false;
        //            }
        //            else if (needrotaion == 270)
        //            {
        //                object[] o = new object[] { "-rotate", " 270 ", rotatePath };
        //                jmagic.Mogrify(o);
        //                o = null;
        //                isrotated = true;
        //            }
        //        }
        //        jmagic = null;
        //    }
        //    catch (Exception ex)
        //    {
        //        ErrorHandler.ErrorHandler.LogError(ex);
        //    }
        //}

        private void RotateImage(string rotatePath, int needrotaion)
        {
            try
            {
                ErrorHandler.ErrorHandler.LogFileWrite(" RotateImage  :  FilePath : " + rotatePath + " Rotaion : " + needrotaion + "  : Fun Start:");
                ImageMagickObject.MagickImage jmagic = new ImageMagickObject.MagickImage();

                if (needrotaion > 0)
                {
                    if (needrotaion == 90)
                    {
                        ErrorHandler.ErrorHandler.LogFileWrite(" RotateImage  :  FilePath : " + rotatePath + " Rotaion : " + needrotaion + "  line : 3599");
                        object[] o = new object[] { "-rotate", " 90 ", rotatePath };
                        jmagic.Mogrify(o);
                        o = null;
                        isrotated = true;
                        ErrorHandler.ErrorHandler.LogFileWrite(" RotateImage  :  FilePath : " + rotatePath + " Rotaion : " + needrotaion + "  line : 3603");
                    }
                    else if (needrotaion == 180)
                    {
                        ErrorHandler.ErrorHandler.LogFileWrite(" RotateImage  :  FilePath : " + rotatePath + " Rotaion : " + needrotaion + "  line : 3608");
                        object[] o = new object[] { "-rotate", " 180 ", rotatePath };
                        jmagic.Mogrify(o);
                        o = null;
                        isrotated = false;
                        ErrorHandler.ErrorHandler.LogFileWrite(" RotateImage  :  FilePath : " + rotatePath + " Rotaion : " + needrotaion + "  line : 3611");
                    }
                    else if (needrotaion == 270)
                    {
                        ErrorHandler.ErrorHandler.LogFileWrite(" RotateImage  :  FilePath : " + rotatePath + " Rotaion : " + needrotaion + "  line : 3617");
                        object[] o = new object[] { "-rotate", " 270 ", rotatePath };
                        jmagic.Mogrify(o);
                        o = null;
                        isrotated = true;
                        ErrorHandler.ErrorHandler.LogFileWrite(" RotateImage  :  FilePath : " + rotatePath + " Rotaion : " + needrotaion + "  line : 3619");
                    }
                }
                jmagic = null;
                ErrorHandler.ErrorHandler.LogFileWrite(" RotateImage  :  FilePath : " + rotatePath + " Rotaion : " + needrotaion + "  Stop");
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
            }
        }
        private bool CheckResetDPIRequired(int hr, int vr, string PhotographerId)
        {
            try
            {
                bool cameraSetting = (new CameraBusiness()).GetIsResetDPIRequired(Convert.ToInt32(PhotographerId));
                if ((hr != 300 || vr != 300) && cameraSetting)
                    return true;
                else
                    return false;
            }
            catch
            {
                return false;
            }
        }
        #endregion

        //void MainWindow_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        //{
        //    ServicePosInfoBusiness svcPosinfoBusiness = new ServicePosInfoBusiness();            
        //    svcPosinfoBusiness.StopService(true);
        //}

        //void MainWindow_Loaded(object sender, System.Windows.RoutedEventArgs e)
        //{           
        //    ServicePosInfoBusiness svcPosinfoBusiness = new ServicePosInfoBusiness();
        //    svcPosinfoBusiness.ServiceStart(true);
        //    MessageBox.Show("called");
        //}
        private void SetGumRideMasterId()
        {
            GumRideList = new List<long>();
            GumRideList.Add(Convert.ToInt64(ConfigParams.GumRideAvailableLocations));
            GumRideList.Add(Convert.ToInt64(ConfigParams.GumRideFontSize));
            GumRideList.Add(Convert.ToInt64(ConfigParams.GumRideFontColor));
            GumRideList.Add(Convert.ToInt64(ConfigParams.GumRideFontWeight));
            GumRideList.Add(Convert.ToInt64(ConfigParams.GumRideBackgrondColor));
            GumRideList.Add(Convert.ToInt64(ConfigParams.GumRidePosition));
            GumRideList.Add(Convert.ToInt64(ConfigParams.GumRideMargin));
            GumRideList.Add(Convert.ToInt64(ConfigParams.GumRideFilePath));
            GumRideList.Add(Convert.ToInt64(ConfigParams.IsGumRideActive));
            GumRideList.Add(Convert.ToInt64(ConfigParams.IsGumPlayerScoreVisible));
            GumRideList.Add(Convert.ToInt64(ConfigParams.GumRideFontStyle));
            GumRideList.Add(Convert.ToInt64(ConfigParams.GumRideFontFamily));
            GumRideList.Add(Convert.ToInt64(ConfigParams.IsVisibleGumBallZeroScore));
            GumRideList.Add(Convert.ToInt64(ConfigParams.IsVisibleGumBallZeroScoreOnImage));
            GumRideList.Add(Convert.ToInt64(ConfigParams.GumRideInputPhotoPath));
            GumRideList.Add(Convert.ToInt64(ConfigParams.GumballScoreSeperater));
            GumRideList.Add(Convert.ToInt64(ConfigParams.IsPrefixActiveFlow));
            GumRideList.Add(Convert.ToInt64(ConfigParams.PrefixPhotoName));
            GumRideList.Add(Convert.ToInt64(ConfigParams.PrefixScoreFileName));
            //------Start------Nilesh-----Dynamically add img BG for Gumball-------24th Feb2018-----
            GumRideList.Add(Convert.ToInt64(ConfigParams.GumBGImagePath));
            GumRideList.Add(Convert.ToInt64(ConfigParams.GumBGImgHeightWidth));
            GumRideList.Add(Convert.ToInt64(ConfigParams.GumBGImgPosition));
            //------End------Nilesh-----Dynamically add img BG for Gumball-------24th Feb2018-----
        }
        private string[] GetAllPlayerBGImg(string allBGImg)
        {
            string[] singleBGImg = allBGImg.Split(',');
            if (singleBGImg != null && singleBGImg.Length > 0)
                return singleBGImg;
            else
                return null;
        }

        private string[] GetAllPlayerBGImgHeightWidth(string allBGImgHeightWidth)
        {
            string[] singleBGImgHeightWidth = allBGImgHeightWidth.Split(',');
            if (singleBGImgHeightWidth != null && singleBGImgHeightWidth.Length > 0)
                return singleBGImgHeightWidth;
            else
                return null;
        }

        private string[] GetAllPlayerScoreBGImgPosition(string allScorePost)
        {
            string[] singleBGImgHeightWidth = allScorePost.Split(',');
            if (singleBGImgHeightWidth != null && singleBGImgHeightWidth.Length > 0)
                return singleBGImgHeightWidth;
            else
                return null;
        }

        private void ShowMessage(string message, [CallerLineNumber] int lineNumber = 0, [CallerMemberName] string caller = null)
        {
            if (logprint)
            {
                log.Error(message);
            }            
        }

        private void RestartApplication()
        {
            Process thisProc = Process.GetCurrentProcess();
            System.Windows.Forms.Application.Restart();
            thisProc.Kill();
        }
    }
    public class MemoryManagement
    {
        /// <summary>
        /// Sets the size of the process working set.
        /// </summary>
        /// <param name="process">The process.</param>
        /// <param name="minimumWorkingSetSize">Minimum size of the working set.</param>
        /// <param name="maximumWorkingSetSize">Maximum size of the working set.</param>
        /// <returns></returns>
        [DllImportAttribute("kernel32.dll", EntryPoint = "SetProcessWorkingSetSize", ExactSpelling = true, CharSet =
        CharSet.Ansi, SetLastError = true)]

        private static extern int SetProcessWorkingSetSize(IntPtr process, int minimumWorkingSetSize, int
        maximumWorkingSetSize);

        /// <summary>
        /// Flushes the memory.
        /// </summardy>
        public static void FlushMemory()
        {
            GC.Collect();
            GC.WaitForPendingFinalizers();
            if (Environment.OSVersion.Platform == PlatformID.Win32NT)
            {
                SetProcessWorkingSetSize(System.Diagnostics.Process.GetCurrentProcess().Handle, -1, -1);
            }
            //GC.Collect();
        }

    }
    public class ImgCopyLocation
    {
        public Int32 locID { get; set; }
        public string locName { get; set; }
        public Int32 counter { get; set; }
        public Int32 range { get; set; }

    }

}
