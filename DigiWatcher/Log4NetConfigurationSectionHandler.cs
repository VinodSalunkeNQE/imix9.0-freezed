﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DigiWatcher
{
    public class Log4NetConfigurationSectionHandler : ConfigurationSection
    {
        //First constructor
        public Log4NetConfigurationSectionHandler() { }

        //Second constructor
        public Log4NetConfigurationSectionHandler(string SectionName, string Key, string Serial)
        {
            this.SectionName = SectionName; //this.Key = Key; this.Serial = Serial;
        }

        //First property: SectionName

        private string _SectionName;
        public string SectionName
        {
            get
            { return _SectionName; }
            set
            { _SectionName = value; }
        }
    }
}
