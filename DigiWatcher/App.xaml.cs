﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Windows;
using System.Diagnostics;
using System.Windows.Media.Animation;
using DigiPhoto.Cache.DataCache;
using Newtonsoft.Json;
using System.Windows.Documents;
using System.Web;
using System.Web.Script;
using System.Web.Script.Serialization;
using Newtonsoft.Json.Linq;
using System.IO;
using System.Xml;

namespace DigiWatcher
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        #region ADDED bY AJAY
        private List jsonAsList = new List();
        #endregion
        private void Application_Startup(object sender, StartupEventArgs e)
        {
            ErrorHandler.ErrorHandler.LogFileWrite("File Watcher Step 1. startup :" + DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss"));
            DataCacheFactory.Register();
            Timeline.DesiredFrameRateProperty.OverrideMetadata(typeof(Timeline),
                               new FrameworkPropertyMetadata { DefaultValue = 5 });
            // Get Reference to the current Process
            Process thisProc = Process.GetCurrentProcess();
            // Check how many total processes have the same name as the current one
            if (Process.GetProcessesByName(thisProc.ProcessName).Length > 1)
            {
                // If there is more than one, then it is already running.
                MessageBox.Show("Digiwatcher is already running.");
                Application.Current.Shutdown();
                return;
            }
            ErrorHandler.ErrorHandler.LogFileWrite("File Watcher Step 2. startup :" + DateTime.Now.ToString("MM / dd / yyyy HH: mm:ss"));
            string configFile = File.ReadAllText(System.IO.Directory.GetCurrentDirectory() + @"\DigiWatcher.exe.config");
            if (!configFile.Contains("<section name=\"log4net\" type=\"Log4net.Log4NetConfigurationSectionHandler, Log4net\" />"))
            {
                addLog4NetToFileWatcher();
            }
            #region Add Keys Dynamically by AJay on  1 June
            //READ THE CONFIG FILE.
            //string ConfigPath = @"C:\Program Files (x86)\iMix";
            //var directory = ConfigPath + "\\ConfiDetails\\ConfigDetailsJSON-Digiwatcher.json";
            //string jrf = System.IO.File.ReadAllText(directory);
            //JavaScriptSerializer serializer = new JavaScriptSerializer();
            //jsonAsList = serializer.Deserialize<List>(jrf.ToString());
            //var jsonObj = JsonConvert.DeserializeObject<JObject>(jrf).First.First;
            //System.Configuration.Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);

            //if (ConfigurationManager.AppSettings.AllKeys.Contains("LocationIds") == false)
            //{           
            //    string LocationIds = Convert.ToString(jsonObj["LocationIds"]);

            //    config.AppSettings.Settings.Add("LocationIds", LocationIds);
            //    // Save the changes in App.config file.
            //    config.Save(ConfigurationSaveMode.Full);
            //    ConfigurationManager.RefreshSection("appSettings");
            //}

            //if (ConfigurationManager.AppSettings.AllKeys.Contains("GumballScoreSeperater") == false)
            //{              
            //    string GumballScoreSeperater = Convert.ToString(jsonObj["GumballScoreSeperater"]);                
            //    config.AppSettings.Settings.Add("GumballScoreSeperater", GumballScoreSeperater);
            //    // Save the changes in App.config file.
            //    config.Save(ConfigurationSaveMode.Full);
            //    ConfigurationManager.RefreshSection("appSettings");
            //}

            //if (ConfigurationManager.AppSettings.AllKeys.Contains("RotationAngle") == false)
            //{
            //    string RotationAngle = Convert.ToString(jsonObj["RotationAngle"]);
            //    config.AppSettings.Settings.Add("RotationAngle", RotationAngle);
            //    // Save the changes in App.config file.
            //    config.Save(ConfigurationSaveMode.Full);
            //    ConfigurationManager.RefreshSection("appSettings");
            //}

            System.Configuration.Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            if (ConfigurationManager.AppSettings.AllKeys.Contains("MaxThreadCount") == false)
            {
                config.AppSettings.Settings.Add("MaxThreadCount", "25");
            }

            // Save the changes in App.config file.
            config.Save(ConfigurationSaveMode.Full);
            ConfigurationManager.RefreshSection("appSettings");

            #endregion
        }

        private void addLog4NetToFileWatcher()
        {
            try
            {
                ExeConfigurationFileMap fileMap = new ExeConfigurationFileMap();
                fileMap.ExeConfigFilename = System.IO.Directory.GetCurrentDirectory() + @"\DigiWatcher.exe.config";
                // Open another config file
                Configuration config = ConfigurationManager.OpenMappedExeConfiguration(fileMap, ConfigurationUserLevel.None);
                DigiWatcher.Log4NetConfigurationSectionHandler oCostomSection;
                oCostomSection = new DigiWatcher.Log4NetConfigurationSectionHandler();
                oCostomSection.SectionName = "log4net";
                config.Sections.Add(oCostomSection.SectionName, oCostomSection);
                ////Save the given section in the configuration file
                oCostomSection.SectionInformation.ForceSave = true;
                config.Save(ConfigurationSaveMode.Modified);
                /* This variable represent the configuration file*/
                var configContext = new ExeConfigurationFileMap();
                //Setting its path

                (configContext as ExeConfigurationFileMap).ExeConfigFilename = System.IO.Directory.GetCurrentDirectory() + @"\DigiWatcher.exe.config";
                // This oParent variable represents the configuration
                var oParent = ConfigurationManager.OpenMappedExeConfiguration((configContext as ExeConfigurationFileMap), ConfigurationUserLevel.None);

                //Load the configuration file into the xmlDocument
                var xmlDocument = new XmlDataDocument();
                xmlDocument.Load((oParent as Configuration).FilePath);
                xmlDocument.Save((oParent as Configuration).FilePath);
                XmlNode Root1 = xmlDocument.SelectSingleNode("//log4net");
                var section1 = xmlDocument.CreateElement("root");
                //Add the 'section' node to Root nodes childs
                Root1.AppendChild(section1);
                XmlNode Root2 = xmlDocument.SelectSingleNode("//log4net");
                //Create the node element with the name given by the user
                var section2 = xmlDocument.CreateElement("appender");
                section2.SetAttribute("name", "LogFileAppender");
                section2.SetAttribute("type", "log4net.Appender.RollingFileAppender");
                //Add the 'section' node to Root nodes childs
                Root2.AppendChild(section2);
                //level value all 
                var nodeRegionValue = xmlDocument.CreateElement("level");
                nodeRegionValue.SetAttribute("value", "ALL");
                xmlDocument.SelectSingleNode("//log4net/root").AppendChild(nodeRegionValue);
                xmlDocument.Save((oParent as Configuration).FilePath);
                //appender-ref
                var nodeRegionAppender = xmlDocument.CreateElement("appender-ref");
                nodeRegionAppender.SetAttribute("ref", "LogFileAppender");
                xmlDocument.SelectSingleNode("//log4net/root").AppendChild(nodeRegionAppender);
                xmlDocument.Save((oParent as Configuration).FilePath);
                //appender 
                //param 
                var nodeRegionAppenderParam = xmlDocument.CreateElement("param");
                nodeRegionAppenderParam.SetAttribute("name", "AppendToFile");
                nodeRegionAppenderParam.SetAttribute("type", "true");
                xmlDocument.SelectSingleNode("//log4net/appender").AppendChild(nodeRegionAppenderParam);
                //rollingstyle
                var nodeRegionAppenderRoll = xmlDocument.CreateElement("rollingStyle");
                nodeRegionAppenderRoll.SetAttribute("value", "Size");
                xmlDocument.SelectSingleNode("//log4net/appender").AppendChild(nodeRegionAppenderRoll);
                //maxroll
                var nodeRegionAppenderMaxRoll = xmlDocument.CreateElement("maxSizeRollBackups");
                nodeRegionAppenderMaxRoll.SetAttribute("value", "10");
                xmlDocument.SelectSingleNode("//log4net/appender").AppendChild(nodeRegionAppenderMaxRoll);
                //maxfilesize
                var nodeRegionAppenderMaxFile = xmlDocument.CreateElement("maximumFileSize");
                nodeRegionAppenderMaxFile.SetAttribute("value", "1000KB");
                xmlDocument.SelectSingleNode("//log4net/appender").AppendChild(nodeRegionAppenderMaxFile);
                //static file name
                var nodeRegionAppenderstaticFile = xmlDocument.CreateElement("staticLogFileName");
                nodeRegionAppenderstaticFile.SetAttribute("value", "true");
                xmlDocument.SelectSingleNode("//log4net/appender").AppendChild(nodeRegionAppenderstaticFile);
                //layout
                XmlNode Root3 = xmlDocument.SelectSingleNode("//log4net/appender");
                //Create the node element with the name given by the user
                var section3 = xmlDocument.CreateElement("layout");
                section3.SetAttribute("type", "log4net.Layout.PatternLayout");
                //Add the 'section' node to Root nodes childs
                Root3.AppendChild(section3);
                //layout param
                var nodeRegionLayoutParam = xmlDocument.CreateElement("param");
                nodeRegionLayoutParam.SetAttribute("name", "ConversionPattern");
                nodeRegionLayoutParam.SetAttribute("value", "%-5p %location %d{yyyy-MM-dd hh:mm:ss.fff} – %m%n");
                xmlDocument.SelectSingleNode("//log4net/appender/layout").AppendChild(nodeRegionLayoutParam);
                xmlDocument.Save((oParent as Configuration).FilePath);

                string replacetext = "<section name=\"log4net\" type=\"Log4net.Log4NetConfigurationSectionHandler, Log4net\" />";
                string text = File.ReadAllText(System.IO.Directory.GetCurrentDirectory() + @"\DigiWatcher.exe.config");
                text = text.Replace("<section name=\"log4net\" type=\"DigiWatcher.Log4NetConfigurationSectionHandler, DigiWatcher, Version=8.1.0.0, Culture=neutral, PublicKeyToken=null\" />", replacetext);
                File.WriteAllText(System.IO.Directory.GetCurrentDirectory() + @"\DigiWatcher.exe.config", text);
            }
            catch (Exception ex)
            {

                // throw;
            }
        }

    }
}
