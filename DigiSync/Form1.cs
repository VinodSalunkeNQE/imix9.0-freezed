﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Windows.Threading;
using System.Threading;


namespace DigiSync
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

       
        int storeId = 0;
        /// <summary>
        /// Handles the Click event of the btnSave control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {

                ////////////Checking validation for required fields//////////
                if (Checkvalidation())
                {

                    if (CommonClass.SetStoreData(txtStoreName.Text, storeId))
                    {
                        MessageBox.Show("Record saved successfully");
                        LoadStoreData();
                        GetStoreCombo();
                        UpdateFont();
                        txtStoreName.Text = "";
                        txtStoreKey.Text = "";
                        storeId = 0;

                    }
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        /// <summary>
        /// Loads the store data.
        /// </summary>
        private void LoadStoreData()
        {
            dgStore.DataSource = CommonClass.GetStoreList();
            UpdateFont();
        }
        /// <summary>
        /// Checkvalidations this instance.
        /// </summary>
        /// <returns></returns>
        private bool Checkvalidation()
        {
            if (string.IsNullOrEmpty(txtStoreName.Text))
            {
                MessageBox.Show("Please enter storename");
                return false;
            }

            else
            {
                return true;
            }

        }
        /// <summary>
        /// Handles the Click event of the btnclear control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void btnclear_Click(object sender, EventArgs e)
        {
            txtStoreName.Text = "";
            txtStoreKey.Text = "";
            storeId = 0;
        }

        /// <summary>
        /// My timer
        /// </summary>
        public DispatcherTimer MyTimer;
        /// <summary>
        /// Handles the Load event of the Form1 control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void Form1_Load(object sender, EventArgs e)
        {
            try
            {
                LoadStoreData();
                GetStoreCombo();
   
                UpdateFont();
            }
            catch (Exception ex)
            {
            }
        }

        /// <summary>
        /// Handles the CellDoubleClick event of the dgStore control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="DataGridViewCellEventArgs"/> instance containing the event data.</param>
        private void dgStore_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                DataTable dt = CommonClass.GetStoreDetailsbyAppId(dgStore.Rows[e.RowIndex].Cells[1].Value.ToString());
                if (dt.Rows.Count > 0)
                {
                    txtStoreName.Text = dt.Rows[0]["DG_Store_Name"].ToString();
                    txtStoreKey.Text = dt.Rows[0]["ApplicationID"].ToString();
                    storeId = Convert.ToInt32(dt.Rows[0]["DG_Store_pkey"].ToString());
                }
            }
            catch (Exception ex)
            {
            }
        }

        /// <summary>
        /// Gets the store combo.
        /// </summary>
        public void GetStoreCombo()
        {
            cmbStoreName.DataSource = CommonClass.GetStoreList();

        }

        /// <summary>
        /// Handles the Click event of the btnSearch control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                dgvSynchronizationLog.DataSource = CommonClass.GetSyndata(dateTimePicker1.Text.ToString(),cmbStoreName.SelectedValue.ToString());
                UpdateFont();
            }
            catch (Exception ex)
            {
            }
        }

        /// <summary>
        /// Updates the font.
        /// </summary>
        private void UpdateFont()
        {
            //Change cell font
            foreach (DataGridViewColumn c in dgStore.Columns)
            {
                c.DefaultCellStyle.Font = new Font("Arial", 15F, GraphicsUnit.Pixel);
            }
            foreach (DataGridViewColumn c in dgvLastSync.Columns)
            {
                c.DefaultCellStyle.Font = new Font("Arial", 15F, GraphicsUnit.Pixel);
            }
            foreach (DataGridViewColumn c in dgvSynchronizationLog.Columns)
            {
                c.DefaultCellStyle.Font = new Font("Arial", 15F, GraphicsUnit.Pixel);
            }
        }

        /// <summary>
        /// Handles the SelectedIndexChanged event of the tbcStore control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void tbcStore_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (tbcStore.SelectedIndex == 2)
            {
                dgvLastSync.DataSource = CommonClass.GetLastSyncdata();
            }
        }
    }
}
