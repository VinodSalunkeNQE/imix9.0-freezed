﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Configuration;
using Microsoft.ApplicationBlocks.Data;
using System.Data.SqlClient;

namespace DigiSync
{
    /// <summary>
    /// Class that contains common method for the application 
    /// </summary>
    class CommonClass
    {
        /// <summary>
        /// Setting the connectionstring that is defines in webconfig files.
        /// </summary>
        static readonly String strConn = ConfigurationManager.ConnectionStrings["DigiConnectionString"].ConnectionString;
        /// <summary>
        /// Gets the location data.
        /// </summary>
        /// <param name="AppId">The application unique identifier.</param>
        /// <param name="username">The username.</param>
        /// <param name="password">The password.</param>
        /// <param name="serverip">The serverip.</param>
        /// <returns></returns>
        public static bool GetLocationData(string AppId,string username,string password,string serverip)
        {
            try
            {
                SqlParameter[] sparam = { new SqlParameter("@ApplicationId", AppId), new SqlParameter("@serverIP", serverip), new SqlParameter("@username", username), new SqlParameter("@password", password)};
                object i = SqlHelper.ExecuteNonQuery(strConn, "SetDataToCentralServer", sparam);
                if (Convert.ToInt32(i) > 0)
                    return true;
                else
                    return false;
            }
            catch (Exception ex)
            {
               // ErrorHandler.ErrorHandler.LogFileWrite( DateTime.Now.ToString() + ":- ServerIP:- " + serverip  + "ApplicationID :- " + AppId + " Error :- " + ex.Message);
                return false;
            }
        }
        /// <summary>
        /// Sets the store data.
        /// </summary>
        /// <param name="StoreName">Name of the store.</param>
        /// <param name="storeId">The store unique identifier.</param>
        /// <returns></returns>
        public static bool SetStoreData(string StoreName,int storeId)
        {
            try
            {
                SqlParameter[] sparam = { new SqlParameter("@StoreName", StoreName),
                                          new SqlParameter("@StoreID",storeId)
                                        };
                object i = SqlHelper.ExecuteScalar(strConn, "SetStoreData", sparam);
                if (Convert.ToInt32(i) > 0)
                    return true;
                else
                    return false;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        /// <summary>
        /// Gets the store list.
        /// </summary>
        /// <returns></returns>
        public static DataTable GetStoreList()
        {
            try
            {
                DataSet ds = new DataSet();
                ds = SqlHelper.ExecuteDataset(strConn, CommandType.StoredProcedure, "GetStoreList");
                if (ds.Tables.Count > 0)
                    return ds.Tables[0];
                else
                    return null;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        /// <summary>
        /// Gets the store detailsby application unique identifier.
        /// </summary>
        /// <param name="appId">The application unique identifier.</param>
        /// <returns></returns>
        public static DataTable GetStoreDetailsbyAppId(string appId)
        {
            try
            {
                DataSet ds = new DataSet();
                SqlParameter[] spparam = {new SqlParameter("@AppId",appId) };
                ds = SqlHelper.ExecuteDataset(strConn, "GetStoreListAppIdWise", spparam);
                if (ds.Tables.Count > 0)
                    return ds.Tables[0];
                else
                    return null;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        /// <summary>
        /// Gets the allstore data.
        /// </summary>
        /// <returns></returns>
        public static DataTable GetAllstoreData()
        {
            try
            {
                DataSet ds = new DataSet();
                ds = SqlHelper.ExecuteDataset(strConn, CommandType.StoredProcedure, "GetAllStoreData");
                if (ds.Tables.Count > 0)
                    return ds.Tables[0];
                else
                    return null;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        /// <summary>
        /// Gets the syndata.
        /// </summary>
        /// <param name="syndate">The syndate.</param>
        /// <param name="StoreID">The store unique identifier.</param>
        /// <returns></returns>
        public static DataTable GetSyndata(string syndate,string StoreID)
        {
            try
            {
                DataSet ds = new DataSet();
                SqlParameter[] sparam = { new SqlParameter("@Datetime", syndate), new SqlParameter("@StoreId", StoreID) };
                ds = SqlHelper.ExecuteDataset(strConn,"getSyncLog",sparam);
                if (ds.Tables.Count > 0)
                    return ds.Tables[0];
                else
                    return null;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        /// <summary>
        /// Gets the last syncdata.
        /// </summary>
        /// <returns></returns>
        public static DataTable GetLastSyncdata()
        {
            try
            {
                DataSet ds = new DataSet();

                ds = SqlHelper.ExecuteDataset(strConn, "GetLastSynStatus");
                if (ds.Tables.Count > 0)
                    return ds.Tables[0];
                else
                    return null;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
    }
}
