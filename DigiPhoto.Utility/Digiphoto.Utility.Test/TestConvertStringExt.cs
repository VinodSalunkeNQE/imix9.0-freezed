﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using DigiPhoto.Utility.Repository.ValueType;
using DigiPhoto.Utility.Repository.Data;

namespace Digiphoto.Utility.Test
{
    [TestClass]
    public class TestConvertStringExt
    {
        [TestMethod]
        public void TestConvertStringToInt16()
        {
            string obj = string.Empty;
            obj = "1";
            var expected = 1;
            Int16 actual = obj.ToInt16();
            
            Assert.AreEqual(expected, actual);

        }

        [TestMethod]
        public void TestNotConvertStringToInt16()
        {
            string obj = string.Empty;
            obj = "123456789123456789";
            var expected = 0;
            var actual = obj.ToInt16();

            Assert.AreEqual(expected, actual);

        }

        [TestMethod]
        public void TestConvertStringToInt32()
        {
            string obj = string.Empty;
            obj = "10000";
            var expected = 10000;
            var actual = obj.ToInt32();
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void TestNotConvertStringToInt32()
        {
            string obj = string.Empty;
            obj = null;
            var expected = ClientConstant.NullInt32;
            var actual = obj.ToInt32();

            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void TestConvertStringToInt64()
        {
            string obj = string.Empty;
            obj = "34345345345";
            var expected = 34345345345;
            var actual = obj.ToInt64();
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void TestNotConvertStringToInt64()
        {
            string obj = string.Empty;
            obj = null;
            var expected = ClientConstant.NullInt64;
            var actual = obj.ToInt64();

            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void TestConvertStringToDateTime()
        {
            string obj = string.Empty;
            obj = "3/3/2015 12:00:00 PM";

            var expected = "3/3/2015 12:00:00 PM";
            
            var actual = obj.ToDateTime();
            Assert.AreEqual(expected, actual.ToString());
            
        }

        [TestMethod]
        public void TestNotConvertStringToDateTime()
        {
            string obj = string.Empty;
            obj = null;
            var expected = ClientConstant.NullDate;
            var actual = obj.ToDateTime();

            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void TestConvertStringToDecimal()
        {
            string obj = string.Empty;
            obj = "1.34";
            var expected = 1.34m;
            var actual = obj.ToDecimal();
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void TestNotConvertStringToDecimal()
        {
            string obj = string.Empty;
            obj = null;
            var expected = ClientConstant.NullDecimal;
            var actual = obj.ToDecimal();

            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void TestConvertStringToBoolean()
        {
            string obj = string.Empty;
            obj = "false";
            var expected = ClientConstant.NullBoolean;
            var actual = obj.ToBoolean();

            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void TestNotConvertStringToBoolean()
        {
            string obj = string.Empty;
            obj = "1234";
            var expected = ClientConstant.NullBoolean;
            var actual = obj.ToBoolean();

            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void TestConvertStringToByte()
        {
            string obj = string.Empty;
            obj = null;
            var expected = ClientConstant.NullByte;
            var actual = obj.ToByte();

            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void TestNotConvertStringToByte()
        {
            string obj = string.Empty;
            obj = null;
            var expected = ClientConstant.NullByte;
            var actual = obj.ToByte();

            Assert.AreEqual(expected, actual);
        }

        //ToChar
        [TestMethod]
        public void TestConvertStringToChar()
        {
            string obj = string.Empty;
            obj = "122";
            var expected = ClientConstant.NullChar;
            var actual = obj.ToChar();

            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void TestNotConvertStringToChar()
        {
            string obj = string.Empty;
            obj = null;
            var expected = ClientConstant.NullChar;
            var actual = obj.ToChar();

            Assert.AreEqual(expected, actual);
        }

        //ToDouble
        [TestMethod]
        public void TestConvertStringToDouble()
        {
            string obj = string.Empty;
            obj = "12.4";
            var expected = 12.4;
            var actual = obj.ToDouble();

            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void TestNotConvertStringToDouble()
        {
            string obj = string.Empty;
            obj = null;
            var expected = ClientConstant.NullDouble;
            var actual = obj.ToDouble();

            Assert.AreEqual(expected, actual);
        }
        //ToSByte
        [TestMethod]
        public void TestConvertStringToSByte()
        {
            string obj = string.Empty;
            obj = "123";

            var expected = (SByte)123;
            var actual = obj.ToSByte();

            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void TestNotConvertStringToSByte()
        {
            string obj = string.Empty;
            obj = null;
            var expected = ClientConstant.NullSByte;
            var actual = obj.ToSByte();

            Assert.AreEqual(expected, actual);
        }

        //ToUInt16
        [TestMethod]
        public void TestConvertStringToUInt16()
        {
            string obj = string.Empty;
            obj = "1";
            var expected = 1;
            var actual = obj.ToUInt16();

            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void TestNotConvertStringToUInt16()
        {
            string obj = string.Empty;
            obj = null;
            var expected = ClientConstant.NullUInt16;
            var actual = obj.ToUInt16();

            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void TestConvertStringToUInt32()
        {
            string obj = string.Empty;
            obj = "1";
            var expected =(ushort) 1;
            var actual = obj.ToUInt32();

            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void TestNotConvertStringToUInt32()
        {
            string obj = string.Empty;
            obj = null;
            var expected = ClientConstant.NullUInt32;
            var actual = obj.ToUInt32();

            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void TestConvertStringToUInt64()
        {
            string obj = string.Empty;
            obj = "1";
            var expected = (UInt64)1;
            var actual = obj.ToUInt64();

            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void TestNotConvertStringToUInt64()
        {
            string obj = string.Empty;
            obj = null;
            var expected = ClientConstant.NullUInt64;
            var actual = obj.ToUInt64();

            Assert.AreEqual(expected, actual);
        }
    }
}
