﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using DigiPhoto.Utility.Repository.ValueType;
using DigiPhoto.Utility.Repository.Data;

namespace Digiphoto.Utility.Test
{
    [TestClass]
    public class TestConvertObjectExt
    {
        [TestMethod]
        public void TestConvertObjectToInt16()
        {
            object obj = new object();
            obj = 1;
            var expected = 1;
            Int16 actual = obj.ToInt16();

            Assert.AreEqual(expected, actual);

        }

        [TestMethod]
        public void TestNotConvertObjectToInt16()
        {
            object obj = new object();
            obj = null;
            var expected = 0;
            var actual = obj.ToInt16();

            Assert.AreEqual(expected, actual);

        }

        [TestMethod]
        public void TestConvertObjectToInt32()
        {
            object obj = new object();
            obj = 10000;

            var expected = 10000;

            var actual = obj.ToInt32();

            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void TestNotConvertObjectToInt32()
        {
            object obj = new object();
            obj = null;
            var expected = ClientConstant.NullInt32;
            var actual = obj.ToInt32();

            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void TestConvertObjectToInt64()
        {
            object obj = new object();
            obj = Int64.MinValue;
            var expected = Int64.MinValue;
            var actual = obj.ToInt64();
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void TestNotConvertObjectToInt64()
        {
            object obj = new object();
            obj = null;
            var expected = ClientConstant.NullInt64;
            var actual = obj.ToInt64();

            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void TestConvertObjectToDateTime()
        {
            object obj = new object();
            obj = "2015-03-14";
            var expected = new DateTime(2015, 3, 14);

            var actual = obj.ToDateTime();
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void TestNotConvertObjectToDateTime()
        {
            object obj = new object();
            obj = null;
            var expected = ClientConstant.NullDate;
            var actual = obj.ToDateTime();

            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void TestConvertObjectToDecimal()
        {
            object obj = new object();
            obj = 1.34;
            var expected = Convert.ToDecimal(1.34);
            var actual = obj.ToDecimal();
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void TestNotConvertObjectToDecimal()
        {
            object obj = new object();
            obj = null;
            var expected = ClientConstant.NullDecimal;
            var actual = obj.ToDecimal();

            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void TestConvertObjectToBoolean()
        {
            object obj = new object();
            obj = null;
            var expected = ClientConstant.NullBoolean;
            var actual = obj.ToBoolean();

            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void TestNotConvertObjectToBoolean()
        {
            object obj = new object();
            obj = 1;
            obj.ToBoolean();
        }

        [TestMethod]
        public void TestConvertObjectToByte()
        {
            object obj = new object();
            obj = null;
            var expected = ClientConstant.NullByte;
            var actual = obj.ToByte();

            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void TestNotConvertObjectToByte()
        {
            object obj = new object();
            obj = 1;
            obj.ToByte();
        }

        //ToChar
        [TestMethod]
        public void TestConvertObjectToChar()
        {
            object obj = new object();
            obj = new char[2] { 'A', 'B' };
            var expected = ClientConstant.NullChar;
            var actual = obj.ToChar();

            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void TestNotConvertObjectToChar()
        {
            object obj = new object();
            obj = null;
            var expected = ClientConstant.NullChar;
            var actual = obj.ToChar();

            Assert.AreEqual(expected, actual);
        }

        //ToDouble
        [TestMethod]
        public void TestConvertObjectToDouble()
        {
            object obj = new object();
            obj = 1;
            obj.ToDouble();
        }

        [TestMethod]
        public void TestNotConvertObjectToDouble()
        {
            object obj = new object();
            obj = null;
            var expected = ClientConstant.NullDouble;
            var actual = obj.ToDouble();

            Assert.AreEqual(expected, actual);
        }
        //ToSByte
        [TestMethod]
        public void TestConvertObjectToSByte()
        {
            object obj = new object();
            obj = 1;
            obj.ToSByte();
        }

        [TestMethod]
        public void TestNotConvertObjectToSByte()
        {
            object obj = new object();
            obj = null;
            var expected = ClientConstant.NullSByte;
            var actual = obj.ToSByte();

            Assert.AreEqual(expected, actual);
        }

        //ToUInt16
        [TestMethod]
        public void TestConvertObjectToUInt16()
        {
            object obj = new object();
            obj = 1;
            obj.ToUInt16();
        }

        [TestMethod]
        public void TestNotConvertObjectToUInt16()
        {
            object obj = new object();
            obj = null;
            var expected = ClientConstant.NullUInt16;
            var actual = obj.ToUInt16();

            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void TestConvertObjectToUInt32()
        {
            object obj = new object();
            obj = 1;
            obj.ToUInt32();
        }

        [TestMethod]
        public void TestNotConvertObjectToUInt32()
        {
            object obj = new object();
            obj = null;
            var expected = ClientConstant.NullUInt32;
            var actual = obj.ToUInt32();

            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void TestConvertObjectToUInt64()
        {
            object obj = new object();
            obj = 1;
            obj.ToUInt64();
        }

        [TestMethod]
        public void TestNotConvertObjectToUInt64()
        {
            object obj = new object();
            obj = null;
            var expected = ClientConstant.NullUInt64;
            var actual = obj.ToUInt64();

            Assert.AreEqual(expected, actual);
        }
    }
}
