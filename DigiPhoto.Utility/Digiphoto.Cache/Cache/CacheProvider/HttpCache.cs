﻿using DigiPhoto.Cache.Data;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Caching;

namespace DigiPhoto.Cache.DataCache
{
    public abstract class HttpCache<T> : CacheProvider<System.Web.Caching.Cache, T>
    {
        private TimeSpan _expiryCacheSpan = new TimeSpan();
        private static Object _syncObject = new Object();
        public HttpCache()
        {
        }

        protected override System.Web.Caching.Cache InitCache()
        {
            _expiryCacheSpan = TimeSpan.FromMinutes(RepositoryData.CacheExpirationInMinute);
            return HttpRuntime.Cache;
        }

        public override void AddToCache(string key, T value)
        {
            lock (_syncObject)
            {
                int duration = RepositoryData.CacheExpirationInMinute;
                if (duration == 0)
                    duration = 30;
                AddToCache(key, value, duration);
            }
        }

        public override void AddToCache(string key, T value, int duration)
        {
            lock (_syncObject)
            {
                TimeSpan expiryCache = TimeSpan.FromMinutes(duration);
                base.Cache.Insert(key, value, null, System.Web.Caching.Cache.NoAbsoluteExpiration, expiryCache,
                    CacheItemPriority.Normal, RefreshDataCallback);
            }
        }

        private void RefreshDataCallback(string key, object value, CacheItemRemovedReason removedReason)
        {
            RefreshCacheData();
        }

        protected virtual void RefreshCacheData()
        {

        }

        public override void RemoveFromCache(string key)
        {
            base.Cache.Remove(key);
        }

        public override bool Contains(string key)
        {
            return base.Cache.Get(key) != null;
        }

        public override int Count()
        {
            return base.Cache.Count;
        }

        public override bool GetFromCache(string key, out T value)
        {
            try
            {
                if (base.Cache[key] == null)
                {
                    value = default(T);
                    return false;
                }
                value = (T)base.Cache[key];
            }
            catch
            {
                value = default(T);
                return false;
            }

            return true;
        }

        public override IEnumerable<KeyValuePair<string, object>> GetAll()
        {
            foreach (DictionaryEntry item in base.Cache)
            {
                yield return new KeyValuePair<string, object>(item.Key as string, item.Value);
            }
        }
    }
}

