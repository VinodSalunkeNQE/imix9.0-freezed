﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DigiPhoto.Cache.DataCache
{
    public class HashTableCache<T> : CacheProvider<Hashtable, T>
    {
        //TimeSpan expiryCacheSpan = new TimeSpan();
        private static Object syncObject = new Object();
        private static Hashtable _hashData = null;
        protected override Hashtable InitCache()
        {
            lock (syncObject)
            {
                if (_hashData == null)
                {
                    _hashData = new Hashtable();
                    base.Cache = _hashData;
                }
                else if (base.Cache == null)
                    base.Cache = _hashData;
            }
            return base.Cache;
        }

        public override bool GetFromCache(string key, out T value)
        {
            try
            {
                if (base.Cache[key] == null)
                {
                    value = default(T);
                    return false;
                }
                value = (T)base.Cache[key];
            }
            catch
            {
                value = default(T);
                return false;
            }

            return true;
        }

        public override void AddToCache(string key, T value)
        {
            lock (syncObject)
            {
                if (!base.Cache.ContainsKey(key))
                {
                    base.Cache.Add(key, value);
                }
                
            }
        }

        public override void AddToCache(string key, T value, int duration)
        {
            lock (syncObject)
            {
                if (!base.Cache.ContainsKey(key))
                {
                    TimeSpan expiryCache = TimeSpan.FromMinutes(duration);
                    base.Cache.Add(key, value);
                }
            }

        }

        public override void RemoveFromCache(string key)
        {
            base.Cache.Remove(key);
        }

        public override bool Contains(string key)
        {
            return base.Cache.ContainsKey(key);
        }

        public override int Count()
        {
            return base.Cache.Count;
        }

        public override IEnumerable<KeyValuePair<string, object>> GetAll()
        {
            foreach (DictionaryEntry item in base.Cache)
            {
                yield return new KeyValuePair<string, object>(item.Key as string, item.Value);
            }
        }
    }
}
