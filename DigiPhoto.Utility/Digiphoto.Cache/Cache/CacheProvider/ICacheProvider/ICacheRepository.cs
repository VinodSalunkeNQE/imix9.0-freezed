﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DigiPhoto.Cache.Repository
{
    public interface ICacheRepository
    {
        /// <summary>
        /// GetData
        /// </summary>
        /// <returns>object</returns>
        object GetData();

        /// <summary>
        /// CleanData
        /// </summary>
        void CleanData();

        void RemoveFromCache();

    }
}
