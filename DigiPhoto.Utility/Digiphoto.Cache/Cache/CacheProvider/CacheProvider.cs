﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DigiPhoto.Cache.DataCache
{
    public abstract class CacheProvider<TCache, T> : ICacheProvider<string, T>
    {
        private readonly int defaultDurationInMinutes = 30;
        protected TCache Cache;
        public int CacheDuration { get; set; }

        public CacheProvider()
        {
            CacheDuration = defaultDurationInMinutes;
            Cache = InitCache();
        }
        public CacheProvider(int durationInMinutes)
        {
            CacheDuration = durationInMinutes;
            Cache = InitCache();
        }

        protected abstract TCache InitCache();
        public abstract bool GetFromCache(string key, out T value);
        public abstract void AddToCache(string key, T value);
        public abstract void AddToCache(string key, T value, int duration);
        public abstract void RemoveFromCache(string key);
        public abstract bool Contains(string key);
        public abstract int Count();
        public abstract IEnumerable<KeyValuePair<string, object>> GetAll();

    }
}
