﻿using DigiPhoto.Cache.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace Digiphoto.Cache.Cache
{
    public class CacheHelper
    {
        public static void ClearAllCache()
        {
            var instances = from t in Assembly.GetExecutingAssembly().GetTypes()
                            where t.GetInterfaces().Contains(typeof(ICacheRepository))
                                     && t.GetConstructor(Type.EmptyTypes) != null
                            select Activator.CreateInstance(t) as ICacheRepository;

            foreach (var instance in instances)
            {
                instance.RemoveFromCache();
            }
        }
    }
}
