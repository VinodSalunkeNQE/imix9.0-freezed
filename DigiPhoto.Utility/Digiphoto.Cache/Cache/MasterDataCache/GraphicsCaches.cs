﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DigiPhoto.Cache.Repository;
using DigiPhoto.Cache.DataCache;
using DigiPhoto.IMIX.Model;
using DigiPhoto.IMIX.DataAccess;

namespace DigiPhoto.Cache.MasterDataCache
{
    public class GraphicsCaches : HashTableCache<List<GraphicsInfo>>, ICacheRepository
    {
        private string _cacheKey = string.Empty;
        public GraphicsCaches()
        {
            _cacheKey = "GraphicsInfo";
        }
        public object GetData()
        {
            List<GraphicsInfo> objList = null;
            if (this.Contains(_cacheKey))
            {
                objList = new List<GraphicsInfo>();
                this.GetFromCache(_cacheKey, out objList);
            }
            else
            {
                //TBD
                //objList = (new ConfigurationDao().GetAllConfig());
                this.AddToCache(_cacheKey, objList);
            }

            return objList;
        }
        public void RemoveFromCache()
        {
            this.RemoveFromCache(_cacheKey);
        }
        public void CleanData()
        {
            this.CleanData();
        }
       
    }
}
