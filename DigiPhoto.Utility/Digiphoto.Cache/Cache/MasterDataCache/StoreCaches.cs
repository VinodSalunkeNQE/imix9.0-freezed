﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DigiPhoto.Cache.Repository;
using DigiPhoto.Cache.DataCache;
using DigiPhoto.IMIX.Model;
using DigiPhoto.IMIX.DataAccess;

namespace DigiPhoto.Cache.MasterDataCache
{
    public class StoreCaches : HashTableCache<List<StoreInfo>>, ICacheRepository
    {
        private string _cacheKey = string.Empty;
        public StoreCaches()
        {
            _cacheKey = "StoreInfo";
        }
        public object GetData()
        {
            List<StoreInfo> objList = null;
            if (this.Contains(_cacheKey))
            {
                objList = new List<StoreInfo>();
                this.GetFromCache(_cacheKey, out objList);
            }
            else
            {
                objList = (new StoreDao().Select());
                this.AddToCache(_cacheKey, objList);
            }

            return objList;
        }
        public void RemoveFromCache()
        {
            this.RemoveFromCache(_cacheKey);
        }
        public void CleanData()
        {
            this.CleanData();
        }
       
    }
}
