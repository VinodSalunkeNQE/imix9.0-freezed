﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DigiPhoto.Cache.Repository;
using DigiPhoto.Cache.DataCache;
using DigiPhoto.IMIX.Model;
using DigiPhoto.IMIX.DataAccess;

namespace DigiPhoto.Cache.MasterDataCache
{
    public class BorderCaches : HashTableCache<List<BorderInfo>>, ICacheRepository
    {
        private string _cacheKey = string.Empty;
        public BorderCaches()
        {
            _cacheKey = "BorderInfo";
        }
        public object GetData()
        {
            List<BorderInfo> objList = null;
            if (this.Contains(_cacheKey))
            {
                objList = new List<BorderInfo>();
                this.GetFromCache(_cacheKey, out objList);
            }
            else
            {
                objList = (new BorderDao().SelectBorder());
                this.AddToCache(_cacheKey, objList);
            }

            return objList;
        }
        public void RemoveFromCache()
        {
            this.RemoveFromCache(_cacheKey);
        }
        public void CleanData()
        {
            this.CleanData();
        }
       
    }
}
