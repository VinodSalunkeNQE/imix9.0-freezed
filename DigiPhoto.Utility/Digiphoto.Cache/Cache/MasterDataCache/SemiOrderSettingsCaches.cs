﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DigiPhoto.Cache.Repository;
using DigiPhoto.Cache.DataCache;
using DigiPhoto.IMIX.Model;
using DigiPhoto.IMIX.DataAccess;

namespace DigiPhoto.Cache.MasterDataCache
{
    public class SemiOrderSettingsCaches : HashTableCache<List<SemiOrderSettings>>, ICacheRepository
    {
        private string _cacheKey = string.Empty;
        public SemiOrderSettingsCaches()
        {
            _cacheKey = "SemiOrderSettings";
        }
        public object GetData()
        {
            List<SemiOrderSettings> objList = null;
            if (this.Contains(_cacheKey))
            {
                objList = new List<SemiOrderSettings>();
                this.GetFromCache(_cacheKey, out objList);
            }
            else
            {
                objList = (new SemiOrderSettingsDao().GetSemiOrderSettings());
                this.AddToCache(_cacheKey, objList);
            }

            return objList;
        }
        public void RemoveFromCache()
        {
            this.RemoveFromCache(_cacheKey);
        }
        public void CleanData()
        {
            this.CleanData();
        }
       
    }
}
