﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DigiPhoto.Cache.Repository;
using DigiPhoto.Cache.DataCache;
using DigiPhoto.IMIX.Model;
using DigiPhoto.IMIX.DataAccess;

namespace DigiPhoto.Cache.MasterDataCache
{
    public class BackgroundCache : HashTableCache<List<BackGroundInfo>>, ICacheRepository
    {
        private string _cacheKey = string.Empty;
        public BackgroundCache()
        {
            _cacheKey = "BackgroundInfo";
        }
        public object GetData()
        {
            List<BackGroundInfo> objList = null;
            if (this.Contains(_cacheKey))
            {
                objList = new List<BackGroundInfo>();
                this.GetFromCache(_cacheKey, out objList);
            }
            else
            {
                objList = (new BackGroundDao().SelectBackground());
                this.AddToCache(_cacheKey, objList);
            }
            return objList;
        }
        public void RemoveFromCache()
        {
            this.RemoveFromCache(_cacheKey);
        }
        public void CleanData()
        {
            this.CleanData();
        }
       
    }
}
