﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DigiPhoto.Cache.Repository;
using DigiPhoto.Cache.DataCache;
using DigiPhoto.IMIX.Model;
using DigiPhoto.IMIX.DataAccess;

namespace DigiPhoto.Cache.MasterDataCache
{
    public class CurrencyCache : HashTableCache<List<CurrencyInfo>>, ICacheRepository
    {
        private string _cacheKey = string.Empty;
        public CurrencyCache()
        {
            _cacheKey = "CurrencyInfo";
        }
        public object GetData()
        {
            List<CurrencyInfo> objList = null;
            if (this.Contains(_cacheKey))
            {
                objList = new List<CurrencyInfo>();
                this.GetFromCache(_cacheKey, out objList);
            }
            else
            {
                objList = (new CurrencyDao().SelectCurrency());
                this.AddToCache(_cacheKey, objList);
            }

            return objList;
        }
        public void RemoveFromCache()
        {
            this.RemoveFromCache(_cacheKey);
        }
        public void CleanData()
        {
            this.CleanData();
        }
       
    }
}
