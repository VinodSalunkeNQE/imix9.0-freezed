﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DigiPhoto.Cache.Repository;
using DigiPhoto.Cache.DataCache;
using DigiPhoto.IMIX.Model;
using DigiPhoto.IMIX.DataAccess;

namespace DigiPhoto.Cache.MasterDataCache
{
    public class ConfigurationCache : HashTableCache<List<ConfigurationInfo>>, ICacheRepository
    {
        private string _cacheKey = string.Empty;
        public ConfigurationCache()
        {
            _cacheKey = "ConfigurationInfo";
        }
        public object GetData()
        {
            List<ConfigurationInfo> objList = null;
            if (this.Contains(_cacheKey))
            {
                objList = new List<ConfigurationInfo>();
                this.GetFromCache(_cacheKey, out objList);
            }
            else
            {
                //ConfigDao obj = new ConfigDao();
                //obj.GetAllConfigurationSetting();
                //objList = obj.Data;
                objList = (new ConfigurationDao().GetAllConfigurationSetting());
                this.AddToCache(_cacheKey, objList);
            }

            return objList;
        }
        public void CleanData()
        {

            this.InitCache();
            GC.Collect();
        }
        public void RemoveFromCache()
        {
            this.RemoveFromCache(_cacheKey);
        }
        //protected override void RefreshCacheData()
        //{
        //    this.CleanData();
        //    this.GetData();
        //}

       
    }
}
