﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DigiPhoto.Cache.Repository;
using DigiPhoto.Cache.DataCache;
using DigiPhoto.IMIX.Model;

namespace DigiPhoto.Cache.MasterDataCache
{
    public class UserCache : HashTableCache<List<UserInfo>>, ICacheRepository
    {
        private string _cacheKey = string.Empty;
        public UserCache()
        {
            _cacheKey = "UserInfo";
        }
        public object GetData()
        {
            List<UserInfo> objList = null;
            if (this.Contains(_cacheKey))
            {
                objList = new List<UserInfo>();
                this.GetFromCache(_cacheKey, out objList);
            }
            else
            {
                //objList = get all userdata TBD
                this.AddToCache(_cacheKey, objList);
            }

            return objList;
        }
        public void RemoveFromCache()
        {
            this.RemoveFromCache(_cacheKey);
        }
        public void CleanData()
        {
            this.CleanData();
        }

    }
}
