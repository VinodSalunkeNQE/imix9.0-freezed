﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DigiPhoto.Cache.Repository;
using DigiPhoto.Cache.DataCache;
using DigiPhoto.IMIX.Model;
using DigiPhoto.IMIX.DataAccess;

namespace DigiPhoto.Cache.MasterDataCache
{
    public class ConfigCache : HashTableCache<List<ConfigInfo>>, ICacheRepository
    {
        private string _cacheKey = string.Empty;
        public ConfigCache()
        {
            _cacheKey = "ConfigInfo";
        }
        public object GetData()
        {
            List<ConfigInfo> objList = null;
            if (this.Contains(_cacheKey))
            {
                objList = new List<ConfigInfo>();
                this.GetFromCache(_cacheKey, out objList);
            }
            else
            {
                objList = (new ConfigurationDao().GetAllConfig());
                this.AddToCache(_cacheKey, objList);
            }

            return objList;
        }
        public void RemoveFromCache()
        {
            this.RemoveFromCache(_cacheKey);
        }
        public void CleanData()
        {
            this.CleanData();
        }
       
    }
}
