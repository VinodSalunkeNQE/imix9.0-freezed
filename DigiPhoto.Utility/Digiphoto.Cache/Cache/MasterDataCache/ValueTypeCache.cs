﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DigiPhoto.Cache.Repository;
using DigiPhoto.Cache.DataCache;
using DigiPhoto.IMIX.Model;
using DigiPhoto.IMIX.DataAccess;

namespace DigiPhoto.Cache.MasterDataCache
{
    public class ValueTypeCache : HashTableCache<List<ValueTypeInfo>>, ICacheRepository
    {
        private string _cacheKey = string.Empty;
        public ValueTypeCache()
        {
            _cacheKey = "ValueTypeInfo";
        }
        public object GetData()
        {
            List<ValueTypeInfo> objList = null;
            if (this.Contains(_cacheKey))
            {
                objList = new List<ValueTypeInfo>();
                this.GetFromCache(_cacheKey, out objList);
            }
            else
            {
                objList = (new ValueTypeDao().SelectValueTypeList());
                this.AddToCache(_cacheKey, objList);
            }

            return objList;
        }
        public void RemoveFromCache()
        {
            this.RemoveFromCache(_cacheKey);
        }
        public void CleanData()
        {
            this.CleanData();
        }
       
    }
}
