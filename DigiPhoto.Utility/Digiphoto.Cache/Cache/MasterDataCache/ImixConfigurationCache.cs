﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DigiPhoto.Cache.Repository;
using DigiPhoto.Cache.DataCache;
using DigiPhoto.IMIX.Model;
using DigiPhoto.IMIX.DataAccess;

namespace DigiPhoto.Cache.MasterDataCache
{
    public class ImixConfigurationCache : HashTableCache<List<iMIXConfigurationInfo>>, ICacheRepository
    {
        private string _cacheKey = string.Empty;
        public ImixConfigurationCache()
        {
            _cacheKey = "iMIXConfigurationInfo";
        }
        public object GetData()
        {
            List<iMIXConfigurationInfo> objList = null;
            if (this.Contains(_cacheKey))
            {
                objList = new List<iMIXConfigurationInfo>();
                this.GetFromCache(_cacheKey, out objList);
            }
            else
            {
                objList = (new ConfigurationDao().GetAllNewConfigValues());
                this.AddToCache(_cacheKey, objList);
            }

            return objList;
        }
        public void CleanData()
        {
            this.InitCache();
            GC.Collect();
        }
        public void RemoveFromCache()
        {
            this.RemoveFromCache(_cacheKey);
        }
        //protected override void RefreshCacheData()
        //{
        //    this.CleanData();
        //    this.GetData();
        //}

       
    }
}
