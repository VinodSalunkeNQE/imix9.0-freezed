﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DigiPhoto.Cache.Repository;
using DigiPhoto.Cache.DataCache;
using DigiPhoto.IMIX.Model;
using DigiPhoto.IMIX.DataAccess;

namespace DigiPhoto.Cache.MasterDataCache
{
    public class SceneCache : HashTableCache<List<SceneInfo>>, ICacheRepository
    {
        private string _cacheKey = string.Empty;
        public SceneCache()
        {
            _cacheKey = "Sceneinfo";
        }
        public object GetData()
        {
            List<SceneInfo> objList = null;
            if (this.Contains(_cacheKey))
            {
                objList = new List<SceneInfo>();
                this.GetFromCache(_cacheKey, out objList);
            }
            else
            {
                objList = (new SceneDao().GetSceneDetails());
                this.AddToCache(_cacheKey, objList);
            }
            return objList;
        }
        public void RemoveFromCache()
        {
            this.RemoveFromCache(_cacheKey);
        }
        public void CleanData()
        {
            this.CleanData();
        }
       
    }
}
