﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DigiPhoto.Cache.Repository;
using DigiPhoto.Cache.DataCache;
using DigiPhoto.IMIX.Model;

namespace DigiPhoto.Cache.MasterDataCache
{
    public class LocationCache : HashTableCache<List<LocationInfo>>, ICacheRepository
    {
        private string _cacheKey = string.Empty;
        public LocationCache()
        {
            _cacheKey = "LocationInfo";
        }
        public object GetData()
        {
            List<LocationInfo> objList = null;
            if (this.Contains(_cacheKey))
            {
                objList = new List<LocationInfo>();
                this.GetFromCache(_cacheKey, out objList);
            }
            else
            {
                //TBD
                //objList =  //get All Locations TBD
                this.AddToCache(_cacheKey, objList);
            }

            return objList;
        }
        public void RemoveFromCache()
        {
            this.RemoveFromCache(_cacheKey);
        }
        public void CleanData()
        {
            this.CleanData();
        }

    }
}
