﻿using DigiPhoto.Cache.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DigiPhoto.Cache.MasterDataCache;

namespace DigiPhoto.Cache.DataCache
{
    internal class CacheFactory<T>
    {
        private CacheFactory() {}     
     
        private static Dictionary<string, Func<T>> _dict = new Dictionary<string, Func<T>>();

        public static Dictionary<string, Func<T>> RegisteredFactory
        {
            get
            {
                return _dict;
            }
        }

        public static T Create(string id)
        {
            Func<T> constructor = null;
            if (_dict.TryGetValue(id, out constructor))
                return constructor();

            throw new ArgumentException("No type registered for this id");
        }

        public static void Register(string fullName, Func<T> ctor)
        {
            _dict.Add(fullName, ctor);
        }
    }
    public static class DataCacheFactory
    {
        public static ICacheRepository GetFactory<T>(string cacheType)
        {
            string type = typeof(T).FullName;
            var repositiry = CacheFactory<T>.Create(cacheType);
            return repositiry as ICacheRepository;
        }

        public static void Register()
        {
            CacheFactory<ICacheRepository>.Register(typeof(BackgroundCache).FullName, () => new BackgroundCache());
            CacheFactory<ICacheRepository>.Register(typeof(BorderCaches).FullName, () => new BorderCaches());
            CacheFactory<ICacheRepository>.Register(typeof(CurrencyCache).FullName, () => new CurrencyCache());
            CacheFactory<ICacheRepository>.Register(typeof(DiscountCache).FullName, () => new DiscountCache());
            CacheFactory<ICacheRepository>.Register(typeof(GraphicsCaches).FullName, () => new GraphicsCaches());
            CacheFactory<ICacheRepository>.Register(typeof(SemiOrderSettingsCaches).FullName, () => new SemiOrderSettingsCaches());
            CacheFactory<ICacheRepository>.Register(typeof(StoreCaches).FullName, () => new StoreCaches());
            CacheFactory<ICacheRepository>.Register(typeof(ValueTypeCache).FullName, () => new ValueTypeCache());
            CacheFactory<ICacheRepository>.Register(typeof(ConfigCache).FullName, () => new ConfigCache());
            CacheFactory<ICacheRepository>.Register(typeof(ConfigurationCache).FullName, () => new ConfigurationCache());
            CacheFactory<ICacheRepository>.Register(typeof(ImixConfigurationCache).FullName, () => new ImixConfigurationCache());
            CacheFactory<ICacheRepository>.Register(typeof(UserCache).FullName, () => new UserCache());
            CacheFactory<ICacheRepository>.Register(typeof(LocationCache).FullName, () => new LocationCache());
            CacheFactory<ICacheRepository>.Register(typeof(SceneCache).FullName, () => new SceneCache());
            CacheFactory<ICacheRepository>.Register(typeof(CharacterCache).FullName, () => new CharacterCache());
        }

        //public static void LoadData()
        //{
        //    ICacheRepository oCache = DataCacheFactory.GetFactory<ICacheRepository>(typeof(ConfigurationCache).FullName);
        //    oCache.GetData();
        //}

    }
}