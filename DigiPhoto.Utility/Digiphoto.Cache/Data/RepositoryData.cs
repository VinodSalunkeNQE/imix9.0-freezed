﻿using DigiPhoto.Cache.Data;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DigiPhoto.Cache.Data
{
    public sealed class RepositoryData
    {
        private RepositoryData() { }

        private static int _cacheExpirationInMinute = ClientConstant.NullInt;
        public static int CacheExpirationInMinute
        {
            get
            {
                if (_cacheExpirationInMinute == ClientConstant.NullInt)
                {
                    int inMin;
                    int.TryParse(ConfigurationManager.AppSettings["CacheExpirationInMinutes"], out inMin);
                    return (inMin <= 0) ? 10 : inMin;
                }
                else
                {
                    return _cacheExpirationInMinute;
                }
            }
            set
            {
                _cacheExpirationInMinute = value;
            }
        }

    }
}
