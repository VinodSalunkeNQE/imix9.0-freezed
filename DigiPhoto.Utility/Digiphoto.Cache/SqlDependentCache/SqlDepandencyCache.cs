﻿using DigiPhoto.IMIX.DataAccess;
using DigiPhoto.IMIX.Model;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Runtime.Caching;
using System.Text;

namespace Digiphoto.Cache.SqlDependentCache
{
    public class SqlDepandencyCache
    {
        private static ObjectCache _memoryCache;
        //private static SqlDependency dependency;
        //private static SqlDependency dependencyNewConfig;
        private static SqlChangeMonitor sqlChangeMonitor;
        private static SqlChangeMonitor sqlChangeMonitorNewConfig;
        private static CacheItemPolicy policy;
        private static CacheItemPolicy policyNew;
        private const string ConfigurationCacheKey = "ConfigurationCache";
        private const string NewConfigCacheKey = "NewConfigCache";
        private static object objLock = new object();
        private static object objLockNewConfig = new object();
        private SqlDepandencyCache()
        {

        }

        private static void InitializeCache()
        {
            try
            {
                if (_memoryCache == null)
                {
                    //SqlDependency.Stop(ConnectionString);
                    //SqlDependency.Stop(ConnectionString, QueueName);
                    //SqlDependency.Start(ConnectionString, QueueName);
                    _memoryCache = MemoryCache.Default;
                }
            }
            catch (Exception e)
            {

            }
        }

        private static Int32 RefreshCacheTimeInMinitues
        {
            get
            {
                try
                {
                    if (ConfigurationManager.AppSettings.AllKeys.Contains("RefreshCacheTimeInMinitues") == true)
                    {
                        return (Convert.ToInt32(Convert.ToString(ConfigurationManager.AppSettings["RefreshCacheTimeInMinitues"])));
                    }
                    else
                        return 10;
                }
                catch (Exception ex)
                {
                    return 10;
                }
            }
        }
        //private static string ConnectionString
        //{
        //    get { return ConfigurationManager.ConnectionStrings["DigiConnectionString"].ConnectionString; }
        //}

        //private static string QueueName
        //{
        //    get { return ConfigurationManager.AppSettings["QueueName"].ToString(); }
        //}

        //private static string QueueServiceName
        //{
        //    get { return ConfigurationManager.AppSettings["QueueServiceName"].ToString(); }
        //}

        //private static string DatabaseName
        //{
        //    get { return ConfigurationManager.AppSettings["DatabaseName"].ToString(); }
        //}

        private static CacheItemPolicy GetCachePolicy(SqlChangeMonitor sqlChangeMonitor)
        {
            if (policy == null)
                policy = new CacheItemPolicy { };
            //policy.UpdateCallback = RefreshCacheData;
            // policy.SlidingExpiration = new TimeSpan(0, 0, RefreshCacheTimeInMinitues, 0);
            policy.AbsoluteExpiration = DateTimeOffset.Now.AddMinutes(RefreshCacheTimeInMinitues);
            //policy.ChangeMonitors.Add(sqlChangeMonitor);

            return policy;

        }

        private static CacheItemPolicy GetCachePolicyNew(SqlChangeMonitor sqlChangeMonitor)
        {
            if (policyNew == null)
                policyNew = new CacheItemPolicy { };

            //policyNew.UpdateCallback = RefreshCacheDataNew;
            //policyNew.SlidingExpiration = new TimeSpan(0, 0, RefreshCacheTimeInMinitues, 0);

            policyNew.AbsoluteExpiration = DateTimeOffset.Now.AddMinutes(RefreshCacheTimeInMinitues);
            //policyNew.ChangeMonitors.Add(sqlChangeMonitor);

            return policyNew;

        }

        private static void RefreshCacheDataNew(CacheEntryUpdateArguments e)
        {
            try
            {
                var key = e.Key;
                _memoryCache.Remove(key);

                var oConfigurationList = (new ConfigurationDao().GetAllNewConfigValues());
                e.UpdatedCacheItem = new CacheItem(key, oConfigurationList);
                e.UpdatedCacheItemPolicy = GetCachePolicyNew(sqlChangeMonitor);

                //_memoryCache.Set(NewConfigCacheKey, oConfigurationList, policyNew);
            }
            catch (Exception ex)
            {

            }

        }
        private static void RefreshCacheData(CacheEntryUpdateArguments e)
        {

            var key = e.Key;
            _memoryCache.Remove(key);

            //_memoryCache.Set(key, (new ConfigurationDao().GetAllConfigurationSetting()), GetCachePolicy(sqlChangeMonitor));
            var oConfigurationList = (new ConfigurationDao().GetAllConfigurationSetting());
            e.UpdatedCacheItem = new CacheItem(key, oConfigurationList);
            e.UpdatedCacheItemPolicy = GetCachePolicy(sqlChangeMonitor);

            //_memoryCache.Set(ConfigurationCacheKey, oConfigurationList, policy);



        }
        static void dependency_OnChange(object sender, SqlNotificationEventArgs e)
        {
            SqlDependency dependency = sender as SqlDependency;
            dependency.OnChange -= new OnChangeEventHandler(dependency_OnChange);
        }
        static void dependency_OnChangeNewConfig(object sender, SqlNotificationEventArgs e)
        {
            SqlDependency dependency = sender as SqlDependency;
            dependency.OnChange -= new OnChangeEventHandler(dependency_OnChangeNewConfig);
        }


        public static List<ConfigurationInfo> GetAllConfigurationSetting()
        {
            lock (objLock)
            {
                InitializeCache();

                if (_memoryCache.Contains(ConfigurationCacheKey))
                {
                    return _memoryCache.Get(ConfigurationCacheKey) as List<ConfigurationInfo>;
                }

                List<ConfigurationInfo> oList = (new ConfigurationDao().GetAllConfigurationSetting());
                //                var oConfigurationList = new List<ConfigurationInfo>();
                //                using (var connection = new SqlConnection(ConnectionString))
                //                {
                //                    connection.Open();

                //                    using (var command = new SqlCommand(@"
                //                    SELECT DG_Config_pkey, DG_Hot_Folder_Path, DG_Frame_Path, DG_BG_Path, DG_IsCompression, DG_Mod_Password, DG_NoOfPhotos, DG_Graphics_Path,   
                //                                          DG_Watermark, DG_SemiOrder, DG_HighResolution, DG_AllowDiscount, DG_EnableDiscountOnTotal, WiFiStartingNumber, FolderStartingNumber, IsAutoLock,   
                //                                          PosOnOff, DG_Substore_Id, DG_NoOfBillReceipt, DG_NoOfPhotoIdSearch, IntervalCount, IsRecursive, intervalType, IsDeleteFromUSB,  
                //                                               DG_SemiOrderMain, DG_ReceiptPrinter, DG_IsAutoRotate, DG_IsEnableGroup, DG_ChromaColor,   
                //                                          DG_ChromaTolerance, DG_DbBackupPath, DG_CleanupTables, DG_HfBackupPath, DG_ScheduleBackup, DG_IsBackupScheduled, DG_Brightness, DG_Contrast,   
                //                                          DG_PageCountGrid, DG_PageCountPreview, DG_MktImgPath, DG_MktImgTimeInSec, EK_SampleImagePath, EK_DisplayDuration, EK_ScreenStartTime,   
                //                                          EK_IsScreenSaverActive, DG_CleanUpDaysBackUp ,  FtpIP,FtpUid,FtpPwd,FtpFolder      
                //                    FROM dbo.DG_Configuration", connection))
                //                    {
                //                        command.Notification = null;
                //                        string option = "Service=" + QueueServiceName + "; local database=" + DatabaseName;
                //                        dependency = new SqlDependency(command, option, 0);

                //                        sqlChangeMonitor = new SqlChangeMonitor(dependency);
                //                        dependency.OnChange += dependency_OnChange;

                //                        if (connection.State == ConnectionState.Closed)
                //                            connection.Open();

                //                        var reader = command.ExecuteReader(CommandBehavior.CloseConnection);
                //                        oList = (new ConfigurationDao()).MapConfigurationInfo(reader);

                //                    }

                //                }

                policy = GetCachePolicy(sqlChangeMonitor);
                if (_memoryCache.Contains(ConfigurationCacheKey))
                    _memoryCache.Remove(ConfigurationCacheKey);
                _memoryCache.Add(ConfigurationCacheKey, oList, policy);
                return oList;
            }
        }
        public static void ClearCacheAllConfigurationSetting()
        {
            if (_memoryCache.Contains(ConfigurationCacheKey))
            {
                _memoryCache.Remove(ConfigurationCacheKey);
            }

        }
        public static void ClearCacheNewConfigValues()
        {
            if (_memoryCache.Contains(NewConfigCacheKey))
            {
                _memoryCache.Remove(NewConfigCacheKey);
            }
        }

        public static List<iMIXConfigurationInfo> GetNewConfigValues()
        {
            lock (objLockNewConfig)
            {
                InitializeCache();

                if (_memoryCache.Contains(NewConfigCacheKey))
                {
                    if(_memoryCache.Get(NewConfigCacheKey) as List<iMIXConfigurationInfo>!=null)
                    {
                        return _memoryCache.Get(NewConfigCacheKey) as List<iMIXConfigurationInfo>;
                    }
                }

                var oConfigurationList = (new ConfigurationDao().GetAllNewConfigValues());// new List<iMIXConfigurationInfo>();
                //                using (var connection = new SqlConnection(ConnectionString))
                //                {
                //                    connection.Open();

                //                    using (var command = new SqlCommand(@"
                //                    SELECT IMIXConfigurationValueId ,IMIXConfigurationMasterId ,ConfigurationValue ,SubstoreId ,SyncCode ,IsSynced  
                //                    FROM dbo.iMIXConfigurationValue", connection))
                //                    {
                //                        command.Notification = null;
                //                        string option = "Service=" + QueueServiceName + "; local database=" + DatabaseName;
                //                        dependencyNewConfig = new SqlDependency(command, option, 0);

                //                        sqlChangeMonitorNewConfig = new SqlChangeMonitor(dependencyNewConfig);
                //                        dependencyNewConfig.OnChange += dependency_OnChangeNewConfig;

                //                        if (connection.State == ConnectionState.Closed)
                //                            connection.Open();

                //                        var reader = command.ExecuteReader(CommandBehavior.CloseConnection);
                //                        oConfigurationList = (new ConfigurationDao()).MapNewConfigValuesBase(reader);

                //                    }

                //                }

                policyNew = GetCachePolicyNew(sqlChangeMonitorNewConfig);
                if (_memoryCache.Contains(NewConfigCacheKey))
                    _memoryCache.Remove(NewConfigCacheKey);
                _memoryCache.Add(NewConfigCacheKey, oConfigurationList, policyNew);
                return oConfigurationList;
            }
        }
		////changed by latika for table workflow
        public static List<iMIXRFIDTableWorkflowInfo> GetTableworkFlowinfo(int Locationid)
        {
            lock (objLockNewConfig)
            {
                InitializeCache();
                ClearCacheNewConfigValues();
                if (_memoryCache.Contains(NewConfigCacheKey))
                {
                    return _memoryCache.Get(NewConfigCacheKey) as List<iMIXRFIDTableWorkflowInfo>;
                }

                var oConfigurationList = (new ConfigurationDao().GetTableworkFlowinfo(Locationid));// new List<iMIXConfigurationInfo>();
                policyNew = GetCachePolicyNew(sqlChangeMonitorNewConfig);
                if (_memoryCache.Contains(NewConfigCacheKey))
                    _memoryCache.Remove(NewConfigCacheKey);
                _memoryCache.Add(NewConfigCacheKey, oConfigurationList, policyNew);
                return oConfigurationList;
            }
        }
		////end
        public static void UpdateCacheAllConfigurationSetting()
        {
            if (_memoryCache.Contains(ConfigurationCacheKey))
            {
                _memoryCache.Remove(ConfigurationCacheKey);
            }

        }
        public static void UpdateCacheNewConfigValues()
        {
            if (_memoryCache.Contains(NewConfigCacheKey))
            {
                _memoryCache.Remove(NewConfigCacheKey);
            }
        }
        public static List<iMIXStoreConfigurationInfo> GetAllReportConfiguration()
        {
            lock (objLock)
            {
                InitializeCache();

                //if (_memoryCache.Contains(ConfigurationCacheKey))
                //{
                //    return _memoryCache.Get(ConfigurationCacheKey) as List<iMIXStoreConfigurationInfo>;
                //}

                List<iMIXStoreConfigurationInfo> oList = (new ConfigurationDao().GetAllReportConfiguration());

                //policy = GetCachePolicy(sqlChangeMonitor);
                //if (_memoryCache.Contains(ConfigurationCacheKey))
                //    _memoryCache.Remove(ConfigurationCacheKey);
                //_memoryCache.Add(ConfigurationCacheKey, oList, policy);
                return oList;
            }
        }
    }
}
