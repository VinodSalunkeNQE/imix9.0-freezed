﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DigiPhoto.Utility.Repository.ValueType
{
    public static class ConvertByteExtension
    {
        // /// <summary>
        // /// Convert Byte to String
        // /// </summary>
        // /// <param name="input"></param>
        // /// <param name="throwExceptionIfFailed"></param>
        // /// <returns></returns>
        //public static string ToString(this Byte input, bool throwExceptionIfFailed = false)
        // {
        //     String result = "";

        //     result = input.ToString();

        //     if (throwExceptionIfFailed)
        //         throw new FormatException(string.Format("'{0}' cannot be converted as Int16", input));

        //     return result;
        // }


        /// <summary>
        /// Convert Byte to Int16
        /// </summary>
        /// <param name="input"></param>
        /// <param name="throwExceptionIfFailed"></param>
        /// <returns></returns>
        public static Int16 ToInt16(this Byte input, bool throwExceptionIfFailed = false)
        {
            Int16 result = 0;

            var valid = Int16.TryParse(input.ToString(), out result);
            if (!valid)
                if (throwExceptionIfFailed)
                    throw new FormatException(string.Format("'{0}' cannot be converted as Int16", input));

            return result;
        }

        /// <summary>
        /// Convert Byte to Int32
        /// </summary>
        /// <param name="input"></param>
        /// <param name="throwExceptionIfFailed"></param>
        /// <returns></returns>
        public static Int32 ToInt32(this Byte input, bool throwExceptionIfFailed = false)
        {
            Int32 result = 0;

            var valid = Int32.TryParse(input.ToString(), out result);
            if (!valid)
                if (throwExceptionIfFailed)
                    throw new FormatException(string.Format("'{0}' cannot be converted as Int32", input));

            return result;
        }


        /// <summary>
        /// Byte to Int64
        /// </summary>
        /// <param name="input"></param>
        /// <param name="throwExceptionIfFailed"></param>
        /// <returns></returns>
        public static Int64 ToInt64(this Byte input, bool throwExceptionIfFailed = false)
        {
            Int64 result = 0;
            var valid = Int64.TryParse(input.ToString(), out result);
            if (!valid)
                if (throwExceptionIfFailed)
                    throw new FormatException(string.Format("'{0}' cannot be converted as Int64", input));

            return result;
        }

        /// <summary>
        /// Byte to Boolean
        /// </summary>
        /// <param name="input"></param>
        /// <param name="throwExceptionIfFailed"></param>
        /// <returns></returns>
        public static Boolean ToBoolean(this Byte input, bool throwExceptionIfFailed = false)
        {
            Boolean result;
            var valid = Boolean.TryParse(input.ToString(), out result);
            if (!valid)
                if (throwExceptionIfFailed)
                    throw new FormatException(string.Format("'{0}' cannot be converted as boolean", input));

            return result;
        }

        /// <summary>
        /// Byte to ToChar
        /// </summary>
        /// <param name="input"></param>
        /// <param name="throwExceptionIfFailed"></param>
        /// <returns></returns>
        public static Char ToChar(this Byte input, bool throwExceptionIfFailed = false)
        {
            Char result;

            var valid = Char.TryParse(input.ToString(), out result);
            if (!valid)
                if (throwExceptionIfFailed)
                    throw new FormatException(string.Format("'{0}' cannot be converted as char", input));

            return result;
        }

        /// <summary>
        /// Byte to Decimal
        /// </summary>
        /// <param name="input"></param>
        /// <param name="throwExceptionIfFailed"></param>
        /// <returns></returns>
        public static Decimal ToDecimal(this Byte input, bool throwExceptionIfFailed = false)
        {
            Decimal result;
            var valid = Decimal.TryParse(input.ToString(), out result);
            if (!valid)
                if (throwExceptionIfFailed)
                    throw new FormatException(string.Format("'{0}' cannot be converted as Decimal", input));

            return result;
        }

        /// <summary>
        /// Byte to ToSByte
        /// </summary>
        /// <param name="input"></param>
        /// <param name="throwExceptionIfFailed"></param>
        /// <returns></returns>
        public static SByte ToSByte(this Byte input, bool throwExceptionIfFailed = false)
        {
            SByte result;
            var valid = SByte.TryParse(input.ToString(), out result);
            if (!valid)
                if (throwExceptionIfFailed)
                    throw new FormatException(string.Format("'{0}' cannot be converted as SByte", input));

            return result;
        }

        /// <summary>
        /// Byte to ToSingle
        /// </summary>
        /// <param name="input"></param>
        /// <param name="throwExceptionIfFailed"></param>
        /// <returns></returns>
        public static Single ToSingle(this Byte input, bool throwExceptionIfFailed = false)
        {
            Single result;
            var valid = Single.TryParse(input.ToString(), out result);
            if (!valid)
                if (throwExceptionIfFailed)
                    throw new FormatException(string.Format("'{0}' cannot be converted as Single", input));

            return result;
        }

        /// <summary>
        /// Byte to Double
        /// </summary>
        /// <param name="input"></param>
        /// <param name="throwExceptionIfFailed"></param>
        /// <returns></returns>
        public static Double ToDouble(this Byte input, bool throwExceptionIfFailed = false)
        {
            Double result;
            var valid = Double.TryParse(input.ToString(), out result);
            if (!valid)
                if (throwExceptionIfFailed)
                    throw new FormatException(string.Format("'{0}' cannot be converted as Double", input));

            return result;
        }

        /// <summary>
        /// Byte to ToUInt16
        /// </summary>
        /// <param name="input"></param>
        /// <param name="throwExceptionIfFailed"></param>
        /// <returns></returns>
        public static UInt16 ToUInt16(this Byte input, bool throwExceptionIfFailed = false)
        {
            UInt16 result;
            var valid = UInt16.TryParse(input.ToString(), out result);
            if (!valid)
                if (throwExceptionIfFailed)
                    throw new FormatException(string.Format("'{0}' cannot be converted as UInt16", input));

            return result;
        }

        /// <summary>
        /// Byte to ToUInt32
        /// </summary>
        /// <param name="input"></param>
        /// <param name="throwExceptionIfFailed"></param>
        /// <returns></returns>
        public static UInt32 ToUInt32(this Byte input, bool throwExceptionIfFailed = false)
        {
            UInt32 result;
            var valid = UInt32.TryParse(input.ToString(), out result);
            if (!valid)
                if (throwExceptionIfFailed)
                    throw new FormatException(string.Format("'{0}' cannot be converted as UInt32", input));

            return result;
        }

        /// <summary>
        /// Byte to ToUInt64
        /// </summary>
        /// <param name="input"></param>
        /// <param name="throwExceptionIfFailed"></param>
        /// <returns></returns>
        public static UInt64 ToUInt64(this Byte input, bool throwExceptionIfFailed = false)
        {
            UInt64 result;
            var valid = UInt64.TryParse(input.ToString(), out result);
            if (!valid)
                if (throwExceptionIfFailed)
                    throw new FormatException(string.Format("'{0}' cannot be converted as UInt64", input));

            return result;
        }
    }
}
