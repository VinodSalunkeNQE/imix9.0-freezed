﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DigiPhoto.Utility.Repository.ValueType
{
    public static class ConvertEnumExtension
    {
        /// <summary>
        /// Parse String To Enum
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="enumString"></param>
        /// <param name="ignoreCase"></param>
        /// <param name="throwExceptionIfFailed"></param>
        /// <returns></returns>
        public static T ParseStringToEnum<T>(this string enumString, bool ignoreCase = true, bool throwExceptionIfFailed = false)
        where T : struct
        {
            T result = default(T);

            if (!typeof(T).IsEnum || String.IsNullOrEmpty(enumString))
            {
                throw new InvalidOperationException("Invalid Enum Type or Input String 'inString'. " +
                  typeof(T).ToString() + "  must be an Enum");
            }

            bool success = Enum.TryParse<T>(enumString, ignoreCase, out result);
            if (!success && throwExceptionIfFailed)
            {
                throw new InvalidOperationException("Invalid Cast");
            }

            return result;
        }

        /// <summary>
        /// Parse Enum To String
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="enumString"></param>
        /// <param name="ignoreCase"></param>
        /// <param name="throwExceptionIfFailed"></param>
        /// <returns></returns>
        public static string ParseEnumToString<T>(this string enumString, bool ignoreCase = true, bool throwExceptionIfFailed = false)
        where T : struct
        {
            T result = default(T);
            string parsedString = string.Empty;
            if (!typeof(T).IsEnum || String.IsNullOrEmpty(enumString))
            {
                throw new InvalidOperationException("Invalid Enum Type or Input String 'inString'. " +
                  typeof(T).ToString() + "  must be an Enum");
            }

            bool success = Enum.TryParse<T>(enumString, ignoreCase, out result);
            if (!success && throwExceptionIfFailed)
            {
                throw new InvalidOperationException("Invalid Cast");
            }

            parsedString = result.ToString();

            return parsedString;
        }

        /// <summary>
        /// ParseEnumToInt
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="enumString"></param>
        /// <param name="ignoreCase"></param>
        /// <param name="throwExceptionIfFailed"></param>
        /// <returns></returns>
        public static T ParseInt16ToEnum<T>(this Int16 enumInt, bool ignoreCase = true, bool throwExceptionIfFailed = false)
        where T : struct
        {
            T result = default(T);
            if (!typeof(T).IsEnum || enumInt == DigiPhoto.Utility.Repository.Data.ClientConstant.NullInt)
            {
                throw new InvalidOperationException("Invalid Enum Type or Input String 'inString'. " +
                  typeof(T).ToString() + "  must be an Enum");
            }

            bool success = Enum.TryParse<T>(enumInt.ToString(), ignoreCase, out result);
            if (!success && throwExceptionIfFailed)
            {
                throw new InvalidOperationException("Invalid Cast");
            }

            return result;
        }

        /// <summary>
        /// ParseInt32ToEnum
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="enumInt"></param>
        /// <param name="ignoreCase"></param>
        /// <param name="throwExceptionIfFailed"></param>
        /// <returns></returns>
        public static T ParseInt32ToEnum<T>(this Int32 enumInt, bool ignoreCase = true, bool throwExceptionIfFailed = false)
        where T : struct
        {
            T result = default(T);
            if (!typeof(T).IsEnum || enumInt == DigiPhoto.Utility.Repository.Data.ClientConstant.NullInt32)
            {
                throw new InvalidOperationException("Invalid Enum Type or Input String 'inString'. " +
                  typeof(T).ToString() + "  must be an Enum");
            }

            bool success = Enum.TryParse<T>(enumInt.ToString(), ignoreCase, out result);
            if (!success && throwExceptionIfFailed)
            {
                throw new InvalidOperationException("Invalid Cast");
            }

            return result;
        }

        /// <summary>
        /// ParseInt64ToEnum
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="enumInt"></param>
        /// <param name="ignoreCase"></param>
        /// <param name="throwExceptionIfFailed"></param>
        /// <returns></returns>
        public static T ParseInt64ToEnum<T>(this Int64 enumInt, bool ignoreCase = true, bool throwExceptionIfFailed = false)
        where T : struct
        {
            T result = default(T);
            if (!typeof(T).IsEnum || enumInt == DigiPhoto.Utility.Repository.Data.ClientConstant.NullInt64)
            {
                throw new InvalidOperationException("Invalid Enum Type or Input String 'inString'. " +
                  typeof(T).ToString() + "  must be an Enum");
            }

            bool success = Enum.TryParse<T>(enumInt.ToString(), ignoreCase, out result);
            if (!success && throwExceptionIfFailed)
            {
                throw new InvalidOperationException("Invalid Cast");
            }

            return result;
        }

        /// <summary>
        /// ParseEnumToInt16
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="enumInt"></param>
        /// <param name="ignoreCase"></param>
        /// <param name="throwExceptionIfFailed"></param>
        /// <returns></returns>
        public static Int16 ParseEnumToInt16<T>(this Int16 enumInt, bool ignoreCase = true, bool throwExceptionIfFailed = false)
        where T : struct
        {
            T result = default(T);
            Int16 parsedInt16 = DigiPhoto.Utility.Repository.Data.ClientConstant.NullInt;
            if (!typeof(T).IsEnum || enumInt == DigiPhoto.Utility.Repository.Data.ClientConstant.NullInt)
            {
                throw new InvalidOperationException("Invalid Enum Type or Input String 'inString'. " +
                  typeof(T).ToString() + "  must be an Enum");
            }

            bool success = Enum.TryParse<T>(enumInt.ToString(), ignoreCase, out result);
            if (!success && throwExceptionIfFailed)
            {
                throw new InvalidOperationException("Invalid Cast");
            }

            parsedInt16 = result.ToInt16();
            return parsedInt16;
        }

        /// <summary>
        /// ParseEnumToInt32
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="enumInt"></param>
        /// <param name="ignoreCase"></param>
        /// <param name="throwExceptionIfFailed"></param>
        /// <returns></returns>
        public static Int32 ParseEnumToInt32<T>(this Int32 enumInt, bool ignoreCase = true, bool throwExceptionIfFailed = false)
        where T : struct
        {
            T result = default(T);
            Int32 parsedInt32 = DigiPhoto.Utility.Repository.Data.ClientConstant.NullInt32;
            if (!typeof(T).IsEnum || enumInt == DigiPhoto.Utility.Repository.Data.ClientConstant.NullInt32)
            {
                throw new InvalidOperationException("Invalid Enum Type or Input String 'inString'. " +
                  typeof(T).ToString() + "  must be an Enum");
            }

            bool success = Enum.TryParse<T>(enumInt.ToString(), ignoreCase, out result);
            if (!success && throwExceptionIfFailed)
            {
                throw new InvalidOperationException("Invalid Cast");
            }

            parsedInt32 = result.ToInt32();
            return parsedInt32;
        }

        /// <summary>
        /// ParseEnumToInt64
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="enumInt"></param>
        /// <param name="ignoreCase"></param>
        /// <param name="throwExceptionIfFailed"></param>
        /// <returns></returns>
        public static Int64 ParseEnumToInt64<T>(this Int64 enumInt, bool ignoreCase = true, bool throwExceptionIfFailed = false)
        where T : struct
        {
            T result = default(T);
            Int64 parsedInt64 = DigiPhoto.Utility.Repository.Data.ClientConstant.NullInt64;
            if (!typeof(T).IsEnum || enumInt == DigiPhoto.Utility.Repository.Data.ClientConstant.NullInt64)
            {
                throw new InvalidOperationException("Invalid Enum Type or Input String 'inString'. " +
                  typeof(T).ToString() + "  must be an Enum");
            }

            bool success = Enum.TryParse<T>(enumInt.ToString(), ignoreCase, out result);
            if (!success && throwExceptionIfFailed)
            {
                throw new InvalidOperationException("Invalid Cast");
            }

            parsedInt64 = result.ToInt64();
            return parsedInt64;
        }
        

    }
}
