﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DigiPhoto.Utility.Repository.ValueType
{
   public static class ConvertUInt64Extension
    {
       // /// <summary>
       // /// Convert UInt64 to String
       // /// </summary>
       // /// <param name="input"></param>
       // /// <param name="throwExceptionIfFailed"></param>
       // /// <returns></returns>
       //public static string ToString(this UInt64 input, bool throwExceptionIfFailed = false)
       // {
       //     string result = "";
       //     result = input.ToString();

       //     if (throwExceptionIfFailed)
       //         throw new FormatException(string.Format("'{0}' cannot be converted as Int16", input));

       //     return result;  
       // }

       /// <summary>
       /// Convert UInt64 to Int16
       /// </summary>
       /// <param name="input"></param>
       /// <param name="throwExceptionIfFailed"></param>
       /// <returns></returns>
       public static Int16 ToInt16(this UInt64 input, bool throwExceptionIfFailed = false)
       {
           Int16 result = 0;

           var valid = Int16.TryParse(input.ToString(), out result);
           if (!valid)
               if (throwExceptionIfFailed)
                   throw new FormatException(string.Format("'{0}' cannot be converted as Int16", input));

           return result;
       }

        /// <summary>
       /// Convert UInt64 to Int32
        /// </summary>
        /// <param name="input"></param>
        /// <param name="throwExceptionIfFailed"></param>
        /// <returns></returns>
       public static Int32 ToInt32(this UInt64 input, bool throwExceptionIfFailed = false)
        {
            Int32 result = 0;

            var valid = Int32.TryParse(input.ToString(), out result);
            if (!valid)
                if (throwExceptionIfFailed)
                    throw new FormatException(string.Format("'{0}' cannot be converted as Int16", input));

            return result;
        }


        /// <summary>
       /// UInt64 to Int64
        /// </summary>
        /// <param name="input"></param>
        /// <param name="throwExceptionIfFailed"></param>
        /// <returns></returns>
       public static Int64 ToInt64(this UInt64 input, bool throwExceptionIfFailed = false)
        {
            Int64 result = 0;
            var valid = Int64.TryParse(input.ToString(), out result);
            if (!valid)
                if (throwExceptionIfFailed)
                    throw new FormatException(string.Format("'{0}' cannot be converted as Int16", input));

            return result;
        }

        /// <summary>
       /// UInt64 to Boolean
        /// </summary>
        /// <param name="input"></param>
        /// <param name="throwExceptionIfFailed"></param>
        /// <returns></returns>
       public static Boolean ToBoolean(this UInt64 input, bool throwExceptionIfFailed = false)
        {
            Boolean result;
            var valid = Boolean.TryParse(input.ToString(), out result);
            if (!valid)
                if (throwExceptionIfFailed)
                    throw new FormatException(string.Format("'{0}' cannot be converted as Int16", input));

            return result;
        }

        /// <summary>
       /// UInt64 to ToByte
        /// </summary>
        /// <param name="input"></param>
        /// <param name="throwExceptionIfFailed"></param>
        /// <returns></returns>
       public static Byte ToByte(this UInt64 input, bool throwExceptionIfFailed = false)
        {
            Byte result;
            var valid = Byte.TryParse(input.ToString(), out result);
            if (!valid)
                if (throwExceptionIfFailed)
                    throw new FormatException(string.Format("'{0}' cannot be converted as Int16", input));

            return result;
        }

        /// <summary>
       /// UInt64 to ToChar
        /// </summary>
        /// <param name="input"></param>
        /// <param name="throwExceptionIfFailed"></param>
        /// <returns></returns>
       public static Char ToChar(this UInt64 input, bool throwExceptionIfFailed = false)
        {
            Char result;
            var valid = Char.TryParse(input.ToString(), out result);
            if (!valid)
                if (throwExceptionIfFailed)
                    throw new FormatException(string.Format("'{0}' cannot be converted as Int16", input));

            return result;
        }

        /// <summary>
       /// UInt64 to Decimal
        /// </summary>
        /// <param name="input"></param>
        /// <param name="throwExceptionIfFailed"></param>
        /// <returns></returns>
       public static Decimal ToDecimal(this UInt64 input, bool throwExceptionIfFailed = false)
        {
            Decimal result;
            var valid = Decimal.TryParse(input.ToString(), out result);
            if (!valid)
                if (throwExceptionIfFailed)
                    throw new FormatException(string.Format("'{0}' cannot be converted as Int16", input));

            return result;
        }

        /// <summary>
       /// UInt64 to ToSByte
        /// </summary>
        /// <param name="input"></param>
        /// <param name="throwExceptionIfFailed"></param>
        /// <returns></returns>
       public static SByte ToSByte(this UInt64 input, bool throwExceptionIfFailed = false)
        {
            SByte result;
            var valid = SByte.TryParse(input.ToString(), out result);
            if (!valid)
                if (throwExceptionIfFailed)
                    throw new FormatException(string.Format("'{0}' cannot be converted as Int16", input));

            return result;
        }

        /// <summary>
       /// UInt64 to ToSingle
        /// </summary>
        /// <param name="input"></param>
        /// <param name="throwExceptionIfFailed"></param>
        /// <returns></returns>
       public static Single ToSingle(this UInt64 input, bool throwExceptionIfFailed = false)
        {
            Single result;
            var valid = Single.TryParse(input.ToString(), out result);
            if (!valid)
                if (throwExceptionIfFailed)
                    throw new FormatException(string.Format("'{0}' cannot be converted as Int16", input));

            return result;
        }

        /// <summary>
       /// UInt64 to Double
        /// </summary>
        /// <param name="input"></param>
        /// <param name="throwExceptionIfFailed"></param>
        /// <returns></returns>
       public static Double ToDouble(this UInt64 input, bool throwExceptionIfFailed = false)
        {
            Double result;
            var valid = Double.TryParse(input.ToString(), out result);
            if (!valid)
                if (throwExceptionIfFailed)
                    throw new FormatException(string.Format("'{0}' cannot be converted as Int16", input));

            return result;
        }

        /// <summary>
       /// UInt64 to ToUInt16
        /// </summary>
        /// <param name="input"></param>
        /// <param name="throwExceptionIfFailed"></param>
        /// <returns></returns>
       public static UInt64 ToUInt16(this UInt64 input, bool throwExceptionIfFailed = false)
        {
            UInt16 result;
            var valid = UInt16.TryParse(input.ToString(), out result);
            if (!valid)
                if (throwExceptionIfFailed)
                    throw new FormatException(string.Format("'{0}' cannot be converted as Int16", input));

            return result;
        }

        /// <summary>
        /// UInt64 to ToUInt32
        /// </summary>
        /// <param name="input"></param>
        /// <param name="throwExceptionIfFailed"></param>
        /// <returns></returns>
       public static UInt32 ToUInt64(this UInt64 input, bool throwExceptionIfFailed = false)
        {
            UInt32 result;
            var valid = UInt32.TryParse(input.ToString(), out result);
            if (!valid)
                if (throwExceptionIfFailed)
                    throw new FormatException(string.Format("'{0}' cannot be converted as Int16", input));

            return result;
        }
    }
}
