﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DigiPhoto.Utility.Repository.ValueType
{
    public static class ConvertUInt32Extension
    {
        ///// <summary>
        ///// Convert UInt32 to String
        ///// </summary>
        ///// <param name="input"></param>
        ///// <param name="throwExceptionIfFailed"></param>
        ///// <returns></returns>
        //public static string ToString(this UInt32 input, bool throwExceptionIfFailed = false)
        //{
        //    string result = "";
        //    result = input.ToString();

        //    if (throwExceptionIfFailed)
        //        throw new FormatException(string.Format("'{0}' cannot be converted as Int16", input));

        //    return result;  
        //}


        /// <summary>
        /// Convert UInt32 to Int16
        /// </summary>
        /// <param name="input"></param>
        /// <param name="throwExceptionIfFailed"></param>
        /// <returns></returns>
        public static Int16 ToInt16(this UInt32 input, bool throwExceptionIfFailed = false)
        {
            Int16 result = 0;

            var valid = Int16.TryParse(input.ToString(), out result);
            if (!valid)
                if (throwExceptionIfFailed)
                    throw new FormatException(string.Format("'{0}' cannot be converted as Int16", input));

            return result;
        }

        /// <summary>
        /// Convert UInt32 to Int32
        /// </summary>
        /// <param name="input"></param>
        /// <param name="throwExceptionIfFailed"></param>
        /// <returns></returns>
        public static Int32 ToInt32(this UInt32 input, bool throwExceptionIfFailed = false)
        {
            Int32 result = 0;

            var valid = Int32.TryParse(input.ToString(), out result);
            if (!valid)
                if (throwExceptionIfFailed)
                    throw new FormatException(string.Format("'{0}' cannot be converted as Int16", input));

            return result;
        }


        /// <summary>
        /// UInt32 to ToInt64
        /// </summary>
        /// <param name="input"></param>
        /// <param name="throwExceptionIfFailed"></param>
        /// <returns></returns>
        public static Int64 ToInt64(this UInt32 input, bool throwExceptionIfFailed = false)
        {
            Int64 result = 0;
            var valid = Int64.TryParse(input.ToString(), out result);
            if (!valid)
                if (throwExceptionIfFailed)
                    throw new FormatException(string.Format("'{0}' cannot be converted as Int16", input));

            return result;
        }

        /// <summary>
        /// UInt32 to Boolean
        /// </summary>
        /// <param name="input"></param>
        /// <param name="throwExceptionIfFailed"></param>
        /// <returns></returns>
        public static Boolean ToBoolean(this UInt32 input, bool throwExceptionIfFailed = false)
        {
            Boolean result;
            var valid = Boolean.TryParse(input.ToString(), out result);
            if (!valid)
                if (throwExceptionIfFailed)
                    throw new FormatException(string.Format("'{0}' cannot be converted as Int16", input));

            return result;
        }

        /// <summary>
        /// UInt32 to ToByte
        /// </summary>
        /// <param name="input"></param>
        /// <param name="throwExceptionIfFailed"></param>
        /// <returns></returns>
        public static Byte ToByte(this UInt32 input, bool throwExceptionIfFailed = false)
        {
            Byte result;
            var valid = Byte.TryParse(input.ToString(), out result);
            if (!valid)
                if (throwExceptionIfFailed)
                    throw new FormatException(string.Format("'{0}' cannot be converted as Int16", input));

            return result;
        }

        /// <summary>
        /// UInt32 to ToChar
        /// </summary>
        /// <param name="input"></param>
        /// <param name="throwExceptionIfFailed"></param>
        /// <returns></returns>
        public static Char ToChar(this UInt32 input, bool throwExceptionIfFailed = false)
        {
            Char result;
            var valid = Char.TryParse(input.ToString(), out result);
            if (!valid)
                if (throwExceptionIfFailed)
                    throw new FormatException(string.Format("'{0}' cannot be converted as Int16", input));

            return result;
        }

        /// <summary>
        /// UInt32 to Decimal
        /// </summary>
        /// <param name="input"></param>
        /// <param name="throwExceptionIfFailed"></param>
        /// <returns></returns>
        public static Decimal ToDecimal(this UInt32 input, bool throwExceptionIfFailed = false)
        {
            Decimal result;
            var valid = Decimal.TryParse(input.ToString(), out result);
            if (!valid)
                if (throwExceptionIfFailed)
                    throw new FormatException(string.Format("'{0}' cannot be converted as Int16", input));

            return result;
        }

        /// <summary>
        /// UInt32 to ToSByte
        /// </summary>
        /// <param name="input"></param>
        /// <param name="throwExceptionIfFailed"></param>
        /// <returns></returns>
        public static SByte ToSByte(this UInt32 input, bool throwExceptionIfFailed = false)
        {
            SByte result;
            var valid = SByte.TryParse(input.ToString(), out result);
            if (!valid)
                if (throwExceptionIfFailed)
                    throw new FormatException(string.Format("'{0}' cannot be converted as Int16", input));

            return result;
        }

        /// <summary>
        /// UInt32 to ToSingle
        /// </summary>
        /// <param name="input"></param>
        /// <param name="throwExceptionIfFailed"></param>
        /// <returns></returns>
        public static Single ToSingle(this UInt32 input, bool throwExceptionIfFailed = false)
        {
            Single result;
            var valid = Single.TryParse(input.ToString(), out result);
            if (!valid)
                if (throwExceptionIfFailed)
                    throw new FormatException(string.Format("'{0}' cannot be converted as Int16", input));

            return result;
        }

        /// <summary>
        /// UInt32 to Double
        /// </summary>
        /// <param name="input"></param>
        /// <param name="throwExceptionIfFailed"></param>
        /// <returns></returns>
        public static Double ToDouble(this UInt32 input, bool throwExceptionIfFailed = false)
        {
            Double result;
            var valid = Double.TryParse(input.ToString(), out result);
            if (!valid)
                if (throwExceptionIfFailed)
                    throw new FormatException(string.Format("'{0}' cannot be converted as Int16", input));

            return result;
        }

        /// <summary>
        /// UInt32 to ToUInt16
        /// </summary>
        /// <param name="input"></param>
        /// <param name="throwExceptionIfFailed"></param>
        /// <returns></returns>
        public static UInt16 ToUInt16(this UInt32 input, bool throwExceptionIfFailed = false)
        {
            UInt16 result;
            var valid = UInt16.TryParse(input.ToString(), out result);
            if (!valid)
                if (throwExceptionIfFailed)
                    throw new FormatException(string.Format("'{0}' cannot be converted as Int16", input));

            return result;
        }

        /// <summary>
        /// UInt32 to ToUInt64
        /// </summary>
        /// <param name="input"></param>
        /// <param name="throwExceptionIfFailed"></param>
        /// <returns></returns>
        public static UInt64 ToUInt64(this UInt32 input, bool throwExceptionIfFailed = false)
        {
            UInt64 result;
            var valid = UInt64.TryParse(input.ToString(), out result);
            if (!valid)
                if (throwExceptionIfFailed)
                    throw new FormatException(string.Format("'{0}' cannot be converted as Int16", input));

            return result;
        }
    }
}
