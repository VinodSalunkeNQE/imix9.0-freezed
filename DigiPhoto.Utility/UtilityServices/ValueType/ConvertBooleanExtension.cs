﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DigiPhoto.Utility.Repository.ValueType
{
    public static class ConvertBooleanExtension
    {
        ///// <summary>
        ///// Convert Boolean to String
        ///// </summary>
        ///// <param name="input"></param>
        ///// <param name="throwExceptionIfFailed"></param>
        ///// <returns></returns>
        //public static string ToString(this Boolean input, bool throwExceptionIfFailed = false)
        //{
        //    String result = "";

        //    result = input.ToString();

        //    if (throwExceptionIfFailed)
        //        throw new FormatException(string.Format("'{0}' cannot be converted as Int16", input));

        //    return result;
        //}


        /// <summary>
        /// Convert Boolean to Int16
        /// </summary>
        /// <param name="input"></param>
        /// <param name="throwExceptionIfFailed"></param>
        /// <returns></returns>
        public static Int16 ToInt16(this Boolean input, bool throwExceptionIfFailed = false)
        {
            Int16 result = 0;

            var valid = Int16.TryParse(input.ToString(), out result);
            if (!valid)
                if (throwExceptionIfFailed)
                    throw new FormatException(string.Format("'{0}' cannot be converted as Int16", input));

            return result;
        }


        /// <summary>
        /// Convert Boolean to Int32
        /// </summary>
        /// <param name="input"></param>
        /// <param name="throwExceptionIfFailed"></param>
        /// <returns></returns>
        public static Int32 ToInt32(this Boolean input, bool throwExceptionIfFailed = false)
        {
            Int32 result = 0;

            var valid = Int32.TryParse(input.ToString(), out result);
            if (!valid)
                if (throwExceptionIfFailed)
                    throw new FormatException(string.Format("'{0}' cannot be converted as Int16", input));

            return result;
        }



        /// <summary>
        /// Boolean to Int64
        /// </summary>
        /// <param name="input"></param>
        /// <param name="throwExceptionIfFailed"></param>
        /// <returns></returns>
        public static Int64 ToInt64(this Boolean input, bool throwExceptionIfFailed = false)
        {
            Int64 result = 0;
            var valid = Int64.TryParse(input.ToString(), out result);
            if (!valid)
                if (throwExceptionIfFailed)
                    throw new FormatException(string.Format("'{0}' cannot be converted as Int16", input));

            return result;
        }

        /// <summary>
        /// Boolean to Single
        /// </summary>
        /// <param name="input"></param>
        /// <param name="throwExceptionIfFailed"></param>
        /// <returns></returns>
        public static Single ToSingle(this Boolean input, bool throwExceptionIfFailed = false)
        {
            Single result;
            var valid = Single.TryParse(input.ToString(), out result);
            if (!valid)
                if (throwExceptionIfFailed)
                    throw new FormatException(string.Format("'{0}' cannot be converted as Int16", input));

            return result;
        }

        /// <summary>
        /// Boolean to ToChar
        /// </summary>
        /// <param name="input"></param>
        /// <param name="throwExceptionIfFailed"></param>
        /// <returns></returns>
        public static Char ToChar(this Boolean input, bool throwExceptionIfFailed = false)
        {
            Char result;

            var valid = Char.TryParse(input.ToString(), out result);
            if (!valid)
                if (throwExceptionIfFailed)
                    throw new FormatException(string.Format("'{0}' cannot be converted as Int16", input));

            return result;
        }

        /// <summary>
        /// Boolean to Decimal
        /// </summary>
        /// <param name="input"></param>
        /// <param name="throwExceptionIfFailed"></param>
        /// <returns></returns>
        public static Decimal ToDecimal(this Boolean input, bool throwExceptionIfFailed = false)
        {
            Decimal result;
            var valid = Decimal.TryParse(input.ToString(), out result);
            if (!valid)
                if (throwExceptionIfFailed)
                    throw new FormatException(string.Format("'{0}' cannot be converted as Int16", input));

            return result;
        }

        /// <summary>
        /// Boolean to ToSByte
        /// </summary>
        /// <param name="input"></param>
        /// <param name="throwExceptionIfFailed"></param>
        /// <returns></returns>
        public static SByte ToSByte(this Boolean input, bool throwExceptionIfFailed = false)
        {
            SByte result;
            var valid = SByte.TryParse(input.ToString(), out result);
            if (!valid)
                if (throwExceptionIfFailed)
                    throw new FormatException(string.Format("'{0}' cannot be converted as Int16", input));

            return result;
        }

        /// <summary>
        /// Boolean to ToByte
        /// </summary>
        /// <param name="input"></param>
        /// <param name="throwExceptionIfFailed"></param>
        /// <returns></returns>
        public static Byte ToByte(this Boolean input, bool throwExceptionIfFailed = false)
        {
            Byte result;
            var valid = Byte.TryParse(input.ToString(), out result);
            if (!valid)
                if (throwExceptionIfFailed)
                    throw new FormatException(string.Format("'{0}' cannot be converted as Int16", input));

            return result;
        }

        /// <summary>
        /// Boolean to Double
        /// </summary>
        /// <param name="input"></param>
        /// <param name="throwExceptionIfFailed"></param>
        /// <returns></returns>
        public static Double ToDouble(this Boolean input, bool throwExceptionIfFailed = false)
        {
            Double result;
            var valid = Double.TryParse(input.ToString(), out result);
            if (!valid)
                if (throwExceptionIfFailed)
                    throw new FormatException(string.Format("'{0}' cannot be converted as Int16", input));

            return result;
        }

        /// <summary>
        /// Boolean to ToUInt16
        /// </summary>
        /// <param name="input"></param>
        /// <param name="throwExceptionIfFailed"></param>
        /// <returns></returns>
        public static UInt16 ToUInt16(this Boolean input, bool throwExceptionIfFailed = false)
        {
            UInt16 result;
            var valid = UInt16.TryParse(input.ToString(), out result);
            if (!valid)
                if (throwExceptionIfFailed)
                    throw new FormatException(string.Format("'{0}' cannot be converted as Int16", input));

            return result;
        }

        /// <summary>
        /// Boolean to ToUInt32
        /// </summary>
        /// <param name="input"></param>
        /// <param name="throwExceptionIfFailed"></param>
        /// <returns></returns>
        public static UInt32 ToUInt32(this Boolean input, bool throwExceptionIfFailed = false)
        {
            UInt32 result;
            var valid = UInt32.TryParse(input.ToString(), out result);
            if (!valid)
                if (throwExceptionIfFailed)
                    throw new FormatException(string.Format("'{0}' cannot be converted as Int16", input));

            return result;
        }

        /// <summary>
        /// Boolean to ToUInt64
        /// </summary>
        /// <param name="input"></param>
        /// <param name="throwExceptionIfFailed"></param>
        /// <returns></returns>
        public static UInt64 ToUInt64(this Boolean input, bool throwExceptionIfFailed = false)
        {
            UInt64 result;
            var valid = UInt64.TryParse(input.ToString(), out result);
            if (!valid)
                if (throwExceptionIfFailed)
                    throw new FormatException(string.Format("'{0}' cannot be converted as Int16", input));

            return result;
        }
    }
}
