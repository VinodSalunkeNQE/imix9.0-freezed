﻿using DigiPhoto.Utility.Repository.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DigiPhoto.Utility.Repository.ValueType
{
    public static class ConvertStringExtension
    {

        ///// <summary>
        ///// Convert string to Int16
        ///// </summary>
        ///// <param name="input">string</param>
        ///// <param name="throwExceptionIfFailed">bool</param>
        ///// <returns>Int16</returns>
        //public static Int16 ToInt16(this string input, bool throwExceptionIfFailed = false)
        //{
        //    Int16 result = ClientConstant.NullInt;
        //    if (string.IsNullOrEmpty(input))
        //        return result;

        //    var valid = Int16.TryParse(input, out result);
        //    if (!valid)
        //        if (throwExceptionIfFailed)
        //            throw new FormatException(string.Format("'{0}' cannot be converted as Int16", input));

        //    return result;
        //}

        /// <summary>
        /// Convert string to Int32
        /// </summary>
        /// <param name="input">string</param>
        /// <param name="throwExceptionIfFailed">bool</param>
        /// <returns>Int32</returns>
        public static Int32 ToInt32(this string input, bool throwExceptionIfFailed = false)
        {
            Int32 result = ClientConstant.NullInt32;

            if (string.IsNullOrEmpty(input))
                return result;

            var valid = Int32.TryParse(input, out result);
            if (!valid)
                if (throwExceptionIfFailed)
                    throw new FormatException(string.Format("'{0}' cannot be converted as Int32", input));

            return result;
        }

        /// <summary>
        /// Convert string to Int64
        /// </summary>
        /// <param name="input">string</param>
        /// <param name="throwExceptionIfFailed">bool</param>
        /// <returns>Int64</returns>
        public static Int64 ToInt64(this string input, bool throwExceptionIfFailed = false)
        {
            Int64 result = ClientConstant.NullInt64;

            if (string.IsNullOrEmpty(input))
                return result;

            var valid = Int64.TryParse(input, out result);
            if (!valid)
                if (throwExceptionIfFailed)
                    throw new FormatException(string.Format("'{0}' cannot be converted as Int64", input));

            return result;
        }

        /// <summary>
        /// Convet string ToDate
        /// </summary>
        /// <param name="input">string</param>
        /// <param name="throwExceptionIfFailed">bool</param>
        /// <returns>DateTime</returns>
        public static DateTime ToDateTime(this string input, bool throwExceptionIfFailed = false)
        {
            DateTime result= ClientConstant.NullDate;

            if (string.IsNullOrEmpty(input))
                return result;

            var valid = DateTime.TryParse(input, out result);
            if (!valid)
                if (throwExceptionIfFailed)
                    throw new FormatException(string.Format("'{0}' cannot be converted as DateTime", input));
            return result;
        }

        /// <summary>
        /// string to ToDecimal
        /// </summary>
        /// <param name="input">string</param>
        /// <param name="throwExceptionIfFailed">bool</param>
        /// <returns>Decimal</returns>
        public static Decimal ToDecimal(this string input, bool throwExceptionIfFailed = false)
        {
            Decimal result = ClientConstant.NullDecimal;

            if (string.IsNullOrEmpty(input))
                return result;

            var valid = Decimal.TryParse(input, out result);
            if (!valid)
                if (throwExceptionIfFailed)
                    throw new FormatException(string.Format("'{0}' cannot be converted as Decimal", input));
            return result;
        }

        /// <summary>
        /// ToBoolean
        /// </summary>
        /// <param name="input">string</param>
        /// <param name="throwExceptionIfFailed">bool</param>
        /// <returns>Boolean</returns>
        public static Boolean ToBoolean(this string input, bool throwExceptionIfFailed = false)
        {
            Boolean result = ClientConstant.NullBoolean;

            if (string.IsNullOrEmpty(input))
                return result;

            var valid = Boolean.TryParse(input, out result);
            if (!valid)
                if (throwExceptionIfFailed)
                    throw new FormatException(string.Format("'{0}' cannot be converted as Boolean", input));
            return result;
        }
        /// <summary>
        /// added by ajit dhama
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static Boolean ToBool(this string str)
        {
            String cleanValue = (str ?? "").Trim();
            if (String.Equals(cleanValue, "False", StringComparison.OrdinalIgnoreCase))
                return false;
            return
                (String.Equals(cleanValue, "True", StringComparison.OrdinalIgnoreCase)) ||
                (cleanValue != "0");
        }
        /// <summary>
        /// ToByte
        /// </summary>
        /// <param name="input">string</param>
        /// <param name="throwExceptionIfFailed">bool</param>
        /// <returns>Byte</returns>
        public static Byte ToByte(this string input, bool throwExceptionIfFailed = false)
        {
            Byte result = ClientConstant.NullByte;
            
            if (string.IsNullOrEmpty(input))
                return result;

            var valid = Byte.TryParse(input, out result);
            if (!valid)
                if (throwExceptionIfFailed)
                    throw new FormatException(string.Format("'{0}' cannot be converted as Byte", input));
            return result;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="input">string</param>
        /// <param name="throwExceptionIfFailed">bool</param>
        /// <returns>Char</returns>
        public static Char ToChar(this string input, bool throwExceptionIfFailed = false)
        {
            Char result = ClientConstant.NullChar;

            if (string.IsNullOrEmpty(input))
                return result;

            var valid = Char.TryParse(input, out result);
            if (!valid)
                if (throwExceptionIfFailed)
                    throw new FormatException(string.Format("'{0}' cannot be converted as Char", input));
            return result;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="input">string</param>
        /// <param name="throwExceptionIfFailed">bool</param>
        /// <returns>Double</returns>
        public static Double ToDouble(this string input, bool throwExceptionIfFailed = false)
        {
            Double result = ClientConstant.NullDouble;

            if (string.IsNullOrEmpty(input))
                return result;

            var valid = Double.TryParse(input, out result);
            if (!valid)
                if (throwExceptionIfFailed)
                    throw new FormatException(string.Format("'{0}' cannot be converted as Double", input));
            return result;
        }

        /// <summary>
        /// ToSByte
        /// </summary>
        /// <param name="input">string</param>
        /// <param name="throwExceptionIfFailed">bool</param>
        /// <returns>SByte</returns>
        public static SByte ToSByte(this string input, bool throwExceptionIfFailed = false)
        {
            SByte result = ClientConstant.NullSByte;

            if (string.IsNullOrEmpty(input))
                return result;

            var valid = SByte.TryParse(input, out result);
            if (!valid)
                if (throwExceptionIfFailed)
                    throw new FormatException(string.Format("'{0}' cannot be converted as SByte", input));
            return result;
        }


        /// <summary>
        /// ToSingle
        /// </summary>
        /// <param name="input">string</param>
        /// <param name="throwExceptionIfFailed">bool</param>
        /// <returns>Single</returns>
        public static Single ToSingle(this string input, bool throwExceptionIfFailed = false)
        {
            Single result = ClientConstant.NullSingle;

            if (string.IsNullOrEmpty(input))
                return result;

            var valid = Single.TryParse(input, out result);
            if (!valid)
                if (throwExceptionIfFailed)
                    throw new FormatException(string.Format("'{0}' cannot be converted as Single", input));
            return result;
        }

        /// <summary>
        /// ToUInt16
        /// </summary>
        /// <param name="input">string</param>
        /// <param name="throwExceptionIfFailed">bool</param>
        /// <returns>UInt16</returns>
        public static UInt16 ToUInt16(this string input, bool throwExceptionIfFailed = false)
        {
            UInt16 result = ClientConstant.NullUInt16;
            
            if (string.IsNullOrEmpty(input))
                return result;

            var valid = UInt16.TryParse(input, out result);
            if (!valid)
                if (throwExceptionIfFailed)
                    throw new FormatException(string.Format("'{0}' cannot be converted as UInt16", input));
            return result;
        }

        /// <summary>
        /// ToUInt32
        /// </summary>
        /// <param name="input">string</param>
        /// <param name="throwExceptionIfFailed">bool</param>
        /// <returns>UInt32</returns>
        public static UInt32 ToUInt32(this string input, bool throwExceptionIfFailed = false)
        {
            UInt32 result = ClientConstant.NullUInt32;

            if (string.IsNullOrEmpty(input))
                return result;

            var valid = UInt32.TryParse(input, out result);
            if (!valid)
                if (throwExceptionIfFailed)
                    throw new FormatException(string.Format("'{0}' cannot be converted as UInt32", input));
            return result;
        }

        /// <summary>
        /// ToUInt64
        /// </summary>
        /// <param name="input">string</param>
        /// <param name="throwExceptionIfFailed">bool</param>
        /// <returns>UInt64</returns>
        public static UInt64 ToUInt64(this string input, bool throwExceptionIfFailed = false)
        {
            UInt64 result = ClientConstant.NullUInt64;

            if (string.IsNullOrEmpty(input))
                return result;

            var valid = UInt64.TryParse(input, out result);
            if (!valid)
                if (throwExceptionIfFailed)
                    throw new FormatException(string.Format("'{0}' cannot be converted as UInt64", input));
            return result;
        }
        
    }
}
