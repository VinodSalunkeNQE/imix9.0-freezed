﻿using DigiPhoto.Utility.Repository.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DigiPhoto.Utility.Repository.ValueType
{
    public static class ConvertObjectExtension
    {
        public static bool IsNull(this Object input)
        {
            return input == null;
        }

        /// <summary>
        /// Converts an object safely to integer or assigns 0
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static Int16 ToInt16(this Object input, bool throwExceptionIfFailed = false)
        {
            Int16 result = 0;

            if (input.IsNull() ||input.GetType() == typeof(DBNull))
                return result;

            if (input.GetType() == typeof(Int16))
            {
                result = ((Int16?)input).Value;
                return result;
            }

            var valid = Int16.TryParse(input.ToString(), out result);
            if (!valid)
                if (throwExceptionIfFailed)
                    throw new FormatException(string.Format("'{0}' cannot be converted as Int16", input));
            return result;
        }

        public static Int32 ToInt32(this Object input, bool throwExceptionIfFailed = false)
        {
            Int32 result = ClientConstant.NullInt32;

            if (input == null || input.GetType() == typeof(DBNull))
                return result;

            if (input.GetType() == typeof(Int32))
            {
                result = ((Int32?)input).Value;
                return result;
            }

            var valid = Int32.TryParse(input.ToString(), out result);
            if (!valid)
                if (throwExceptionIfFailed)
                    throw new FormatException(string.Format("'{0}' cannot be converted as Int16", input));
            return result;
        }

        public static Int64 ToInt64(this Object input, bool throwExceptionIfFailed = false)
        {
            Int64 result = ClientConstant.NullInt64;

            if (input == null || input.GetType() == typeof(DBNull))
                return result;

            if (input.GetType() == typeof(Int64))
            {
                result = ((Int64?)input).Value;
                return result;
            }

            var valid = Int64.TryParse(input.ToString(), out result);
            if (!valid)
                if (throwExceptionIfFailed)
                    throw new FormatException(string.Format("'{0}' cannot be converted as Int16", input));
            return result;
        }

        /// <summary>
        /// Convet string ToDate
        /// </summary>
        /// <param name="input"></param>
        /// <param name="throwExceptionIfFailed"></param>
        /// <returns></returns>
        public static DateTime ToDateTime(this Object input, bool throwExceptionIfFailed = false)
        {
            DateTime result = ClientConstant.NullDate;

            if (input == null || input.GetType() == typeof(DBNull))
                return result;

            if (input.GetType() == typeof(DateTime?))
            {
                result = ((DateTime?)input).Value;
                return result;
            }

            var valid = DateTime.TryParse(input.ToString(), out result);
            if (!valid)
                if (throwExceptionIfFailed)
                    throw new FormatException(string.Format("'{0}' cannot be converted as DateTime", input));
            return result;
        }

        /// <summary>
        /// string to ToDecimal
        /// </summary>
        /// <param name="input"></param>
        /// <param name="throwExceptionIfFailed"></param>
        /// <returns></returns>
        public static Decimal ToDecimal(this Object input, bool throwExceptionIfFailed = false)
        {
            Decimal result = ClientConstant.NullDecimal;

            if (input == null || input.GetType() == typeof(DBNull))
                return result;

            if (input.GetType() == typeof(Decimal?))
            {
                result = ((Decimal?)input).Value;
                return result;
            }

            var valid = Decimal.TryParse(input.ToString(), out result);
            if (!valid)
                if (throwExceptionIfFailed)
                    throw new FormatException(string.Format("'{0}' cannot be converted as Decimal", input));

            return result;
        }

        public static Boolean ToBoolean(this Object input, bool throwExceptionIfFailed = false)
        {
            Boolean result = ClientConstant.NullBoolean;

            if (input == null || input.GetType() == typeof(DBNull))
                return result;

            if (input.GetType() == typeof(Boolean?))
            {
                result = ((Boolean?)input).Value;
                return result;
            }

            var valid = Boolean.TryParse(input.ToString(), out result);
            if (!valid)
                if (throwExceptionIfFailed)
                    throw new FormatException(string.Format("'{0}' cannot be converted as Boolean", input));
            return result;
        }

        public static Byte ToByte(this Object input, bool throwExceptionIfFailed = false)
        {
            Byte result = ClientConstant.NullByte;

            if (input == null || input.GetType() == typeof(DBNull))
                return result;

            if (input.GetType() == typeof(Boolean?))
            {
                result = ((Byte?)input).Value;
                return result;
            }

            var valid = Byte.TryParse(input.ToString(), out result);
            if (!valid)
                if (throwExceptionIfFailed)
                    throw new FormatException(string.Format("'{0}' cannot be converted as Byte", input));
            return result;
        }

        public static Char ToChar(this Object input, bool throwExceptionIfFailed = false)
        {
            Char result = ClientConstant.NullChar;

            if (input == null || input.GetType() == typeof(DBNull))
                return result;

            if (input.GetType() == typeof(Char?))
            {
                result = ((Char?)input).Value;
                return result;
            }

            var valid = Char.TryParse(input.ToString(), out result);
            if (!valid)
                if (throwExceptionIfFailed)
                    throw new FormatException(string.Format("'{0}' cannot be converted as Char", input));
            return result;
        }

        public static Double ToDouble(this Object input, bool throwExceptionIfFailed = false)
        {
            Double result = ClientConstant.NullDouble;

            if (input == null || input.GetType() == typeof(DBNull))
                return result;

            if (input.GetType() == typeof(Double?))
            {
                result = ((Double?)input).Value;
                return result;
            }

            var valid = Double.TryParse(input.ToString(), out result);
            if (!valid)
                if (throwExceptionIfFailed)
                    throw new FormatException(string.Format("'{0}' cannot be converted as Double", input));
            return result;
        }

        public static SByte ToSByte(this Object input, bool throwExceptionIfFailed = false)
        {
            SByte result = ClientConstant.NullSByte;

            if (input == null || input.GetType() == typeof(DBNull))
                return result;

            if (input.GetType() == typeof(SByte?))
            {
                result = ((SByte?)input).Value;
                return result;
            }

            var valid = SByte.TryParse(input.ToString(), out result);
            if (!valid)
                if (throwExceptionIfFailed)
                    throw new FormatException(string.Format("'{0}' cannot be converted as SByte", input));
            return result;
        }


        public static Single ToSingle(this Object input, bool throwExceptionIfFailed = false)
        {
            Single result = ClientConstant.NullSingle;

            if (input == null || input.GetType() == typeof(DBNull))
                return result;

            if (input.GetType() == typeof(SByte?))
            {
                result = ((SByte?)input).Value;
                return result;
            }

            var valid = Single.TryParse(input.ToString(), out result);
            if (!valid)
                if (throwExceptionIfFailed)
                    throw new FormatException(string.Format("'{0}' cannot be converted as Single", input));
            return result;
        }

        public static UInt16 ToUInt16(this Object input, bool throwExceptionIfFailed = false)
        {
            UInt16 result = ClientConstant.NullUInt16;

            if (input == null || input.GetType() == typeof(DBNull))
                return result;

            if (input.GetType() == typeof(UInt16?))
            {
                result = ((UInt16?)input).Value;
                return result;
            }

            var valid = UInt16.TryParse(input.ToString(), out result);
            if (!valid)
                if (throwExceptionIfFailed)
                    throw new FormatException(string.Format("'{0}' cannot be converted as UInt16", input));
            return result;
        }

        public static UInt32 ToUInt32(this Object input, bool throwExceptionIfFailed = false)
        {
            UInt32 result = ClientConstant.NullUInt16;

            if (input == null || input.GetType() == typeof(DBNull))
                return result;

            if (input.GetType() == typeof(UInt32?))
            {
                result = ((UInt32?)input).Value;
                return result;
            }

            var valid = UInt32.TryParse(input.ToString(), out result);
            if (!valid)
                if (throwExceptionIfFailed)
                    throw new FormatException(string.Format("'{0}' cannot be converted as UInt32", input));
            return result;
        }

        public static UInt64 ToUInt64(this Object input, bool throwExceptionIfFailed = false)
        {
            UInt64 result = ClientConstant.NullUInt16;

            if (input == null || input.GetType() == typeof(DBNull))
                return result;

            if (input.GetType() == typeof(UInt64?))
            {
                result = ((UInt64?)input).Value;
                return result;
            }

            var valid = UInt64.TryParse(input.ToString(), out result);
            if (!valid)
                if (throwExceptionIfFailed)
                    throw new FormatException(string.Format("'{0}' cannot be converted as UInt64", input));
            return result;
        }
    }
}
