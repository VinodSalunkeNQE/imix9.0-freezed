﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DigiPhoto.Utility.Repository.ValueType
{
    public static class ConvertDoubleExtension
    {
        ///// <summary>
        ///// Convert Double to String
        ///// </summary>
        ///// <param name="input"></param>
        ///// <param name="throwExceptionIfFailed"></param>
        ///// <returns></returns>
        //public static string ToString(this Double input, bool throwExceptionIfFailed = false)
        //{
        //    string result = "";
        //    result = input.ToString();

        //    if (throwExceptionIfFailed)
        //        throw new FormatException(string.Format("'{0}' cannot be converted as Int16", input));

        //    return result;
        //}

        /// <summary>
        /// Convert Double to Int16
        /// </summary>
        /// <param name="input"></param>
        /// <param name="throwExceptionIfFailed"></param>
        /// <returns></returns>
        public static Int16 ToInt16(this Double input, bool throwExceptionIfFailed = false)
        {
            Int16 result = 0;

            var valid = Int16.TryParse(input.ToString(), out result);
            if (!valid)
                if (throwExceptionIfFailed)
                    throw new FormatException(string.Format("'{0}' cannot be converted as Int16", input));

            return result;
        }

        /// <summary>
        /// Convert Double to Int32
        /// </summary>
        /// <param name="input"></param>
        /// <param name="throwExceptionIfFailed"></param>
        /// <returns></returns>
        public static Int32 ToInt32(this Double input, bool throwExceptionIfFailed = false)
        {
            Int32 result = 0;

            var valid = Int32.TryParse(input.ToString(), out result);
            if (!valid)
                if (throwExceptionIfFailed)
                    throw new FormatException(string.Format("'{0}' cannot be converted as Int16", input));

            return result;
        }


        /// <summary>
        /// Double to Int64
        /// </summary>
        /// <param name="input"></param>
        /// <param name="throwExceptionIfFailed"></param>
        /// <returns></returns>
        public static Int64 Tont64(this Double input, bool throwExceptionIfFailed = false)
        {
            Int64 result = 0;
            var valid = Int64.TryParse(input.ToString(), out result);
            if (!valid)
                if (throwExceptionIfFailed)
                    throw new FormatException(string.Format("'{0}' cannot be converted as Int16", input));

            return result;
        }

        /// <summary>
        /// Double to Boolean
        /// </summary>
        /// <param name="input"></param>
        /// <param name="throwExceptionIfFailed"></param>
        /// <returns></returns>
        public static Boolean ToBoolean(this Double input, bool throwExceptionIfFailed = false)
        {
            Boolean result;
            var valid = Boolean.TryParse(input.ToString(), out result);
            if (!valid)
                if (throwExceptionIfFailed)
                    throw new FormatException(string.Format("'{0}' cannot be converted as Int16", input));

            return result;
        }

        /// <summary>
        /// Double to ToByte
        /// </summary>
        /// <param name="input"></param>
        /// <param name="throwExceptionIfFailed"></param>
        /// <returns></returns>
        public static Byte ToByte(this Double input, bool throwExceptionIfFailed = false)
        {
            Byte result;
            var valid = Byte.TryParse(input.ToString(), out result);
            if (!valid)
                if (throwExceptionIfFailed)
                    throw new FormatException(string.Format("'{0}' cannot be converted as Int16", input));

            return result;
        }

        /// <summary>
        /// Double to ToChar
        /// </summary>
        /// <param name="input"></param>
        /// <param name="throwExceptionIfFailed"></param>
        /// <returns></returns>
        public static Char ToChar(this Double input, bool throwExceptionIfFailed = false)
        {
            Char result;
            var valid = Char.TryParse(input.ToString(), out result);
            if (!valid)
                if (throwExceptionIfFailed)
                    throw new FormatException(string.Format("'{0}' cannot be converted as Int16", input));

            return result;
        }

        /// <summary>
        /// Double to Decimal
        /// </summary>
        /// <param name="input"></param>
        /// <param name="throwExceptionIfFailed"></param>
        /// <returns></returns>
        public static Decimal ToDecimal(this Double input, bool throwExceptionIfFailed = false)
        {
            Decimal result;
            var valid = Decimal.TryParse(input.ToString(), out result);
            if (!valid)
                if (throwExceptionIfFailed)
                    throw new FormatException(string.Format("'{0}' cannot be converted as Int16", input));

            return result;
        }

        /// <summary>
        /// Double to ToSByte
        /// </summary>
        /// <param name="input"></param>
        /// <param name="throwExceptionIfFailed"></param>
        /// <returns></returns>
        public static SByte ToSByte(this Double input, bool throwExceptionIfFailed = false)
        {
            SByte result;
            var valid = SByte.TryParse(input.ToString(), out result);
            if (!valid)
                if (throwExceptionIfFailed)
                    throw new FormatException(string.Format("'{0}' cannot be converted as Int16", input));

            return result;
        }

        /// <summary>
        /// Double to ToSingle
        /// </summary>
        /// <param name="input"></param>
        /// <param name="throwExceptionIfFailed"></param>
        /// <returns></returns>
        public static Single ToSingle(this Double input, bool throwExceptionIfFailed = false)
        {
            Single result;
            var valid = Single.TryParse(input.ToString(), out result);
            if (!valid)
                if (throwExceptionIfFailed)
                    throw new FormatException(string.Format("'{0}' cannot be converted as Int16", input));

            return result;
        }

        /// <summary>
        /// Double to ToUInt16
        /// </summary>
        /// <param name="input"></param>
        /// <param name="throwExceptionIfFailed"></param>
        /// <returns></returns>
        public static UInt16 ToUInt16(this Double input, bool throwExceptionIfFailed = false)
        {
            UInt16 result;
            var valid = UInt16.TryParse(input.ToString(), out result);
            if (!valid)
                if (throwExceptionIfFailed)
                    throw new FormatException(string.Format("'{0}' cannot be converted as Int16", input));

            return result;
        }

        /// <summary>
        /// Double to ToUInt32
        /// </summary>
        /// <param name="input"></param>
        /// <param name="throwExceptionIfFailed"></param>
        /// <returns></returns>
        public static UInt32 ToUInt32(this Double input, bool throwExceptionIfFailed = false)
        {
            UInt32 result;
            var valid = UInt32.TryParse(input.ToString(), out result);
            if (!valid)
                if (throwExceptionIfFailed)
                    throw new FormatException(string.Format("'{0}' cannot be converted as Int16", input));

            return result;
        }

        /// <summary>
        /// Double to ToUInt64
        /// </summary>
        /// <param name="input"></param>
        /// <param name="throwExceptionIfFailed"></param>
        /// <returns></returns>
        public static UInt64 ToUInt64(this Double input, bool throwExceptionIfFailed = false)
        {
            UInt64 result;
            var valid = UInt64.TryParse(input.ToString(), out result);
            if (!valid)
                if (throwExceptionIfFailed)
                    throw new FormatException(string.Format("'{0}' cannot be converted as Int16", input));

            return result;
        }
    }
}
