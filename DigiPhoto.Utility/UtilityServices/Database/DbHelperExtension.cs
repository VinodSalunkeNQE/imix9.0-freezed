﻿using DigiPhoto.Utility.Repository.Data;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MSB.DAL.DAO.Shared
{
    public static class DbHelperExtension
    {
        private static Hashtable _DataReaderColumns = new Hashtable();

        /// <summary>
        /// HasColumn
        /// </summary>
        /// <param name="dr">IDataRecord</param>
        /// <param name="columnName">string</param>
        /// <returns>bool</returns>
        public static bool HasColumn(this IDataRecord dr, string columnName)
        {
            for (int i = 0; i < dr.FieldCount; i++)
            {
                if (dr.GetName(i).Equals(columnName, StringComparison.InvariantCultureIgnoreCase))
                    return true;
            }
            return false;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dr">IDataRecord</param>
        /// <param name="columnName">string</param>
        /// <returns>int</returns>
        public static int? GetColumnIndex(this IDataRecord dr, string columnName)
        {
            for (int i = 0; i < dr.FieldCount; i++)
            {
                if (dr.GetName(i).Equals(columnName, StringComparison.InvariantCultureIgnoreCase))
                    return i;
            }
            return null;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dr">IDataRecord</param>
        /// <param name="columnName">string</param>
        /// <param name="ht">Hashtable</param>
        /// <returns>int</returns>
        public static int? GetColumnIndex(this IDataRecord dr, string columnName, Hashtable ht)
        {
            int? ordinal = null;
            if (ht != null)
            {
                if (ht.Contains(columnName))
                    ordinal = (int?)ht[columnName];
                else
                {
                    ordinal = GetColumnIndex(dr, columnName);
                    ht.Add(columnName, ordinal);
                }
            }
            else
                ordinal = GetColumnIndex(dr, columnName);
            return ordinal;
        }

        /// <summary>
        /// GetDataReaderOrdinal
        /// </summary>
        /// <param name="dr">IDataRecord</param>
        /// <param name="columnName">string</param>
        /// <returns>int</returns>
        public static int? GetDataReaderOrdinal(this IDataRecord dr, string columnName)
        {
            Hashtable ht = null;
            if (_DataReaderColumns.Contains(dr))
                ht = (Hashtable)_DataReaderColumns[dr];
            else
            {
                ht = new Hashtable();
                _DataReaderColumns.Add(dr, ht);
            }
            int? ordinal = GetColumnIndex(dr, columnName, ht);
            return ordinal;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dr">IDataRecord</param>
        public static void ClearColumnHash(this IDataRecord dr)
        {
            if (_DataReaderColumns.Contains(dr))
            {
                Hashtable ht = (Hashtable)_DataReaderColumns[dr];
                ht.Clear();
                _DataReaderColumns.Remove(dr);
            }
        }

        #region Helper Functions

        /// <summary>
        /// GetInt16Value
        /// </summary>
        /// <param name="dr">IDataRecord</param>
        /// <param name="columnName">string</param>
        /// <returns>Int16</returns>
        public static Int16 GetInt16Value(this IDataRecord dr, string columnName)
        {
            Int16 value = ClientConstant.NullInt;
            try
            {
                int? ordinal = dr.GetDataReaderOrdinal(columnName);
                if (ordinal != null)
                    if (!dr.IsDBNull(ordinal.Value)) value = (Convert.ToInt16(dr.GetValue(ordinal.Value)));
            }
            catch (Exception) { }
            return value;
        }

        /// <summary>
        /// GetInt32Value
        /// </summary>
        /// <param name="dr">IDataRecord</param>
        /// <param name="columnName">string</param>
        /// <returns>Int32</returns>
        public static Int32 GetInt32Value(this IDataRecord dr, string columnName)
        {
            Int32 value = ClientConstant.NullInt;
            try
            {
                int? ordinal = dr.GetDataReaderOrdinal(columnName);
                if (ordinal != null)
                    if (!dr.IsDBNull(ordinal.Value)) value = ((System.Int32)(dr.GetValue(ordinal.Value)));
            }
            catch (Exception) { }
            return value;
        }

        /// <summary>
        /// GetInt64Value
        /// </summary>
        /// <param name="dr">IDataRecord</param>
        /// <param name="columnName">string</param>
        /// <returns>Int64</returns>
        public static Int64 GetInt64Value(this IDataRecord dr, string columnName)
        {
            Int64 value = ClientConstant.NullInt;
            try
            {
                int? ordinal = dr.GetDataReaderOrdinal(columnName);
                if (ordinal != null)
                    if (!dr.IsDBNull(ordinal.Value)) value = ((System.Int64)(dr.GetValue(ordinal.Value)));
            }
            catch (Exception) { }
            return value;
        }

        /// <summary>
        /// GetDecimalValue
        /// </summary>
        /// <param name="dr">IDataRecord</param>
        /// <param name="columnName">string</param>
        /// <returns>decimal</returns>
        public static decimal GetDecimalValue(this IDataRecord dr, string columnName)
        {
            decimal value = ClientConstant.DefaultZeroDecimal;
            try
            {
                int? ordinal = dr.GetDataReaderOrdinal(columnName);
                if (ordinal != null)
                    if (!dr.IsDBNull(ordinal.Value)) value = ((System.Decimal)(dr.GetValue(ordinal.Value)));
            }
            catch (Exception) { }
            return value;
        }


        /// <summary>
        /// GetDoubleValue() is used to typecast Double value 
        /// </summary>
        /// <param name="dr">IDataRecord</param>
        /// <param name="columnName">string</param>
        /// <returns>double</returns>
        public static double GetDoubleValue(this IDataRecord dr, string columnName)
        {
            double value = ClientConstant.NullDouble;
            try
            {
                int? ordinal = dr.GetDataReaderOrdinal(columnName);
                if (ordinal != null)
                    if (!dr.IsDBNull(ordinal.Value)) value = ((System.Double)(dr.GetValue(ordinal.Value)));
            }
            catch (Exception) { }
            return value;
        }

        /// <summary>
        /// GetStringValue
        /// </summary>
        /// <param name="dr">IDataRecord</param>
        /// <param name="columnName">string</param>
        /// <returns>string</returns>
        public static string GetStringValue(this IDataRecord dr, string columnName)
        {
            string value = string.Empty;
            try
            {
                int? ordinal = dr.GetDataReaderOrdinal(columnName);
                if (ordinal != null)
                    if (!dr.IsDBNull(ordinal.Value)) value = ((System.String)(dr.GetValue(ordinal.Value)));
            }
            catch (Exception) { }
            return value;
        }

        /// <summary>
        /// GetDateValue
        /// </summary>
        /// <param name="dr">IDataRecord</param>
        /// <param name="columnName">string</param>
        /// <returns>DateTime</returns>
        public static DateTime GetDateValue(this IDataRecord dr, string columnName)
        {
            DateTime value = ClientConstant.NullDate;
            try
            {
                int? ordinal = dr.GetDataReaderOrdinal(columnName);
                if (ordinal != null)
                    if (!dr.IsDBNull(ordinal.Value)) value = ((System.DateTime)(dr.GetValue(ordinal.Value)));
            }
            catch (Exception) { }
            return value;
        }

        /// <summary>
        /// GetBooleanValue
        /// </summary>
        /// <param name="dr">IDataRecord</param>
        /// <param name="columnName">string</param>
        /// <returns>bool</returns>
        public static bool GetBooleanValue(this IDataRecord dr, string columnName)
        {
            bool value = ClientConstant.NullBoolean;
            try
            {
                int? ordinal = dr.GetDataReaderOrdinal(columnName);
                if (ordinal != null)
                    if (!dr.IsDBNull(ordinal.Value)) value = ((System.Boolean)(dr.GetValue(ordinal.Value)));
            }
            catch (Exception) { }
            return value;
        }

        /// <summary>
        /// GetCharValue
        /// </summary>
        /// <param name="dr">IDataRecord</param>
        /// <param name="columnName">string</param>
        /// <returns>char</returns>
        public static char GetCharValue(this IDataRecord dr, string columnName)
        {
            char value = ' ';
            try
            {
                int? ordinal = dr.GetDataReaderOrdinal(columnName);
                if (ordinal != null)
                    if (!dr.IsDBNull(ordinal.Value)) value = (Convert.ToChar(dr.GetValue(ordinal.Value)));
            }
            catch (Exception) { }
            return value;
        }

        /// <summary>
        /// GetByteValue
        /// </summary>
        /// <param name="dr">IDataRecord</param>
        /// <param name="columnName">string</param>
        /// <returns>byte</returns>
        public static byte GetByteValue(this IDataRecord dr, string columnName)
        {
            byte value = ClientConstant.NullByte;
            try
            {
                int? ordinal = dr.GetDataReaderOrdinal(columnName);
                if (ordinal != null)
                    if (!dr.IsDBNull(ordinal.Value)) value = ((byte)(dr.GetValue(ordinal.Value)));
            }
            catch (Exception) { }
            return value;
        }

        /// <summary>
        /// GetTimeSpanValue
        /// </summary>
        /// <param name="dr">IDataRecord</param>
        /// <param name="columnName">string</param>
        /// <returns>TimeSpan</returns>
        public static TimeSpan GetTimeSpanValue(this IDataRecord dr, string columnName)
        {
            TimeSpan value = ClientConstant.NullTimespan;
            try
            {
                int? ordinal = dr.GetDataReaderOrdinal(columnName);
                if (ordinal != null)
                    if (!dr.IsDBNull(ordinal.Value)) value = ((TimeSpan)(dr.GetValue(ordinal.Value)));
            }
            catch (Exception) { }
            return value;
        }

        /// <summary>
        /// GetNullableBooleanValue
        /// </summary>
        /// <param name="dr">IDataRecord</param>
        /// <param name="columnName">string</param>
        /// <returns>bool</returns>
        public static bool? GetNullableBooleanValue(this IDataRecord dr, string columnName)
        {
            bool? value = null;
            try
            {
                int? ordinal = dr.GetDataReaderOrdinal(columnName);
                if (ordinal != null)
                    if (!dr.IsDBNull(ordinal.Value)) value = ((System.Boolean)(dr.GetValue(ordinal.Value)));
            }
            catch (Exception) { }
            return value;
        }
        #endregion
    }
}
