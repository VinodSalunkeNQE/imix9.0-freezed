﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace MSB.DAL.DAO.Shared
{
    /// <summary>
    /// DbUdtHelper
    /// </summary>
    public class DbUdtHelper
    {
        private static volatile DbUdtHelper instance;
        private static object syncRoot = new Object();

        private DbUdtHelper() { }

        /// <summary>
        /// Instance
        /// </summary>
        public static DbUdtHelper Instance
        {
            get
            {
                if (instance == null)
                {
                    lock (syncRoot)
                    {
                        if (instance == null)
                            instance = new DbUdtHelper();
                    }
                }

                return instance;
            }
        }

        /// <summary>
        /// Generic ListToDataTable
        /// </summary>
        /// <typeparam name="T">Generic</typeparam>
        /// <param name="list">List of generic</param>
        /// <returns>DataTable</returns>
        public DataTable ListToDataTable<T>(List<T> list)
        {
            DataTable dt = new DataTable();

            foreach (PropertyInfo info in typeof(T).GetProperties())
            {
                dt.Columns.Add(new DataColumn(info.Name, info.PropertyType));
            }
            if (list == null)
                return dt;
            foreach (T t in list)
            {
                DataRow row = dt.NewRow();
                foreach (PropertyInfo info in typeof(T).GetProperties())
                {
                    row[info.Name] = info.GetValue(t, null);
                    switch (info.PropertyType.Name.ToUpper())
                    {
                        case "INT32":
                            row[info.Name] = DbHelper.GetIntDbValue((int)info.GetValue(t, null));
                            break;
                        case "BYTE":
                            row[info.Name] = DbHelper.GetByteDbValue((byte)info.GetValue(t, null));
                            break;
                        case "DATETIME":
                            row[info.Name] = Convert.ToDateTime(info.GetValue(t, null)).ToShortDateString();
                            break;
                        case "STRING":
                            row[info.Name] = DbHelper.GetStringDbValue((string)info.GetValue(t, null));
                            break;
                        default:
                            row[info.Name] = info.GetValue(t, null);
                            break;
                    }
                }
                dt.Rows.Add(row);
            }
            return dt;
        }
    }
}
