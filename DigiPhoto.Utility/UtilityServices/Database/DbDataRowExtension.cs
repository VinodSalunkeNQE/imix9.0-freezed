﻿using DigiPhoto.Utility.Repository.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MSB.DAL.DAO.Shared
{
    /// <summary>
    /// DbDataRowExtension
    /// </summary>
    public static class DbDataRowExtension
    {
        /// <summary>
        /// GetDataColumnIndex
        /// </summary>
        /// <param name="dr">DataRow</param>
        /// <param name="columnName">string</param>
        /// <returns></returns>
        public static int? GetDataColumnIndex(this DataRow dr, string columnName)
        {
            bool hasColumn = dr.Table.Columns.Contains(columnName);
            int? ordinal = null;
            if (hasColumn)
            {
                ordinal = dr.Table.Columns[columnName].Ordinal;
            }
            return ordinal;
        }

        /// <summary>
        /// GetDataRowDecimalValue
        /// </summary>
        /// <param name="dr">DataRow</param>
        /// <param name="columnName">string</param>
        /// <returns></returns>
        public static decimal GetDataRowDecimalValue(this DataRow dr, string columnName)
        {
            decimal value = ClientConstant.DefaultZeroDecimal;
            try
            {
                int? ordinal = dr.GetDataColumnIndex(columnName);
                if (ordinal != null)
                {
                    if (dr[ordinal.Value] != null)
                    {
                        bool result = Decimal.TryParse((dr[ordinal.Value]).ToString(), out value);
                    }
                }
            }
            catch (Exception) { }
            return value;
        }
    }
}
