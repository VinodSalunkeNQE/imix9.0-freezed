﻿using DigiPhoto.Utility.Repository.Data;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MSB.DAL.DAO.Shared
{
    /// <summary>
    /// Database Helper
    /// </summary>
    public class DbHelper
    {

        private DbHelper()
        {

        }


        #region "Convert Values"

        /// <summary>
        /// ConvertStringToBoolean
        /// </summary>
        /// <param name="value">object</param>
        /// <returns>bool</returns>
        public static bool ConvertStringToBoolean(object value)
        {
            bool returnVal = false;
            if (value != null)
            {
                value = (value.ToString().Equals("Y")) ? "true" : "false";

                if (value.ToString().Equals("true"))
                    returnVal = true;
                else
                    returnVal = false;
            }

            
            return returnVal;
        }

        /// <summary>
        /// ConvertObjectToString
        /// </summary>
        /// <param name="value">object</param>
        /// <returns>string</returns>
        public static string ConvertObjectToString(object value)
        {
            string returnVal = string.Empty;
            if (value != null)
            {
                returnVal = value.ToString();
            }
            return returnVal;
        }

        /// <summary>
        /// FormatDecimalValue
        /// </summary>
        /// <param name="value">decimal</param>
        /// <param name="roundTo">int</param>
        /// <returns>Decimal</returns>
        public static Decimal FormatDecimalValue(decimal value, int roundTo)
        {
            decimal dec = Math.Round(value, roundTo);
            return dec;
        }

        /// <summary>
        /// ConvertStringToInteger
        /// </summary>
        /// <param name="value">string</param>
        /// <returns>int</returns>
        public static int ConvertStringToInteger(string value)
        {
            int returnVal = ClientConstant.NullInt;
            if (value != null)
            {
                bool result = Int32.TryParse(value.ToString(), out returnVal);
                returnVal = result ? returnVal : ClientConstant.NullInt;
            }
            return returnVal;
        }


        /// <summary>
        /// ConvertObjectToDecimal
        /// </summary>
        /// <param name="value">object</param>
        /// <returns>Decimal</returns>
        public static Decimal ConvertObjectToDecimal(object value)
        {
            Decimal returnVal = ClientConstant.DefaultZeroDecimal;
            if (value != null)
            {
                bool result = Decimal.TryParse(value.ToString(), out returnVal);
                returnVal = result ? returnVal : ClientConstant.DefaultZeroDecimal;
            }
            return returnVal;
        }

        /// <summary>
        /// ConvertAndRoundValue
        /// </summary>
        /// <param name="value">object</param>
        /// <returns>Decimal</returns>
        public static Decimal ConvertAndRoundValue(object value)
        {
            Decimal returnVal = ClientConstant.DefaultZeroDecimal;
            if (value != null)
            {
                bool result = Decimal.TryParse(value.ToString(), out returnVal);
                returnVal = result ? Math.Round(returnVal, 1) : ClientConstant.DefaultZeroDecimal;
            }
            return returnVal;
        }

        /// <summary>
        /// ConvertAndRoundValue
        /// </summary>
        /// <param name="value">object</param>
        /// <param name="fractionalVal">short</param>
        /// <returns></returns>
        public static Decimal ConvertAndRoundValue(object value, short fractionalVal)
        {
            Decimal returnVal = ClientConstant.DefaultZeroDecimal;
            if (value != null)
            {
                bool result = Decimal.TryParse(value.ToString(), out returnVal);
                returnVal = result ? Math.Round(returnVal, fractionalVal) : ClientConstant.DefaultZeroDecimal;
            }
            return returnVal;
        }

        /// <summary>
        /// ConvertStringToDecimal
        /// </summary>
        /// <param name="value">object</param>
        /// <returns>Decimal</returns>
        public static Decimal ConvertStringToDecimal(object value)
        {
            Decimal returnVal = ClientConstant.DefaultZeroDecimal;
            if (value != null)
            {
                bool result = Decimal.TryParse(value.ToString(), out returnVal);
                returnVal = result ? returnVal : ClientConstant.DefaultZeroDecimal;
            }
            return returnVal;
        }

        /// <summary>
        /// GetDateDbValue
        /// </summary>
        /// <param name="objDate">DateTime</param>
        /// <returns>object</returns>
        public static object GetDateDbValue(DateTime objDate)
        {
            if (ClientConstant.NullDate == objDate || ClientConstant.MinimunDateValue == objDate || ClientConstant.NullDbDate == objDate)
                return DBNull.Value;
            else
                return objDate;
        }

        /// <summary>
        /// GetDateDbValue
        /// </summary>
        /// <param name="objDate">DateTime</param>
        /// <returns>object</returns>
        public static object GetDateDbValue(DateTime? objDate)
        {
            if (ClientConstant.NullDate == objDate || ClientConstant.MinimunDateValue == objDate || ClientConstant.NullDbDate == objDate)
                return DBNull.Value;
            else
                return objDate;
        }

        /// <summary>
        /// GetDateDbMaxValue
        /// </summary>
        /// <param name="objDate">DateTime</param>
        /// <returns>object</returns>
        public static object GetDateDbMaxValue(DateTime objDate)
        {
            if (ClientConstant.NullDate == objDate || ClientConstant.MinimunDateValue == objDate || ClientConstant.NullDbDate == objDate)
                return ClientConstant.MaxDbDate;
            else
                return objDate;
        }

        /// <summary>
        /// GetIntDbValue
        /// </summary>
        /// <param name="objLong">long</param>
        /// <returns>object</returns>
        public static object GetIntDbValue(long objLong)
        {
            if (ClientConstant.NullInt == objLong)
                return DBNull.Value;
            else
                return objLong;
        }

        /// <summary>
        /// GetByteDbValue
        /// </summary>
        /// <param name="objByte">byte</param>
        /// <returns>object</returns>
        public static object GetByteDbValue(byte objByte)
        {
            if (ClientConstant.NullByte == objByte)
                return DBNull.Value;
            else
                return objByte;
        }

        /// <summary>
        /// GetStringDbValue
        /// </summary>
        /// <param name="strValue"></param>
        /// <returns>string</returns>
        public static object GetStringDbValue(string strValue)
        {
            if (strValue == string.Empty)
                return DBNull.Value;
            else
                return strValue;
        }

        /// <summary>
        /// GetDecimalDbValue
        /// </summary>
        /// <param name="objDecimal">decimal</param>
        /// <returns>object</returns>
        public static object GetDecimalDbValue(decimal objDecimal)
        {
            if (ClientConstant.NullDecimal == objDecimal)
                return DBNull.Value;
            else
                return objDecimal;
        }

        /// <summary>
        /// CheckDbNullDate
        /// </summary>
        /// <param name="obj">object</param>
        /// <returns>DateTime</returns>
        public static DateTime CheckDbNullDate(object obj)
        {
            if (obj == DBNull.Value || obj == null)
                return ClientConstant.NullDbDate;
            else
                return Convert.ToDateTime(obj);
        }

        /// <summary>
        /// CheckDbNullLong
        /// </summary>
        /// <param name="obj">object</param>
        /// <returns>long</returns>
        public static long CheckDbNullLong(object obj)
        {
            if (obj == DBNull.Value || obj == null)
                return ClientConstant.NullInt;
            else
                return Convert.ToInt64(obj);
        }

        /// <summary>
        /// CheckDbNullInt
        /// </summary>
        /// <param name="obj">object</param>
        /// <returns>int</returns>
        public static int CheckDbNullInt(object obj)
        {
            if (obj == DBNull.Value || obj == null)
                return ClientConstant.NullInt;
            else
                return Convert.ToInt32(obj);
        }

        /// <summary>
        /// CheckDbNullByte
        /// </summary>
        /// <param name="obj">object</param>
        /// <returns>byte</returns>
        public static byte CheckDbNullByte(object obj)
        {
            if (obj == DBNull.Value || obj == null)
                return ClientConstant.NullByte;
            else
                return Convert.ToByte(obj);
        }

        /// <summary>
        /// CheckDbNullShort
        /// </summary>
        /// <param name="obj">object</param>
        /// <returns>short</returns>
        public static short CheckDbNullShort(object obj)
        {
            if (obj == DBNull.Value || obj == null)
                return ClientConstant.NullInt;
            else
                return Convert.ToInt16(obj);
        }

        /// <summary>
        /// CheckDbNullString
        /// </summary>
        /// <param name="obj">object</param>
        /// <returns>string</returns>
        public static string CheckDbNullString(object obj)
        {
            if (obj == DBNull.Value || obj == null)
                return string.Empty;
            else
                return Convert.ToString(obj);
        }

        #endregion


        #region Convert from List to Data Table

        /// <summary>
        /// ConvertStringIdListToDataTable
        /// </summary>
        /// <param name="idList">List of string</param>
        /// <returns>DataTable</returns>
        public static DataTable ConvertStringIdListToDataTable(List<string> idList)
        {
            var dt = new DataTable("udt_StringIDTable");
            dt.Columns.Add("ID");
            if (idList != null)
            {
                foreach (var t in idList)
                {
                    if (!string.IsNullOrEmpty(t))
                    {
                        var dr = dt.NewRow();
                        dr[0] = t;
                        dt.Rows.Add(dr);
                    }

                }
            }
            return dt;
        }

        
        /// <summary>
        /// ConvertIdListToDataTable
        /// </summary>
        /// <param name="idList">List of integer</param>
        /// <returns>DataTable</returns>
        public static DataTable ConvertIdListToDataTable(List<int> idList)
        {
            var dt = new DataTable("udt_IDTable");
            dt.Columns.Add("ID");
            if (idList != null)
            {
                foreach (var t in idList)
                {
                    if (t > 0 && t != ClientConstant.NullInt)
                    {
                        var dr = dt.NewRow();
                        dr[0] = t;
                        dt.Rows.Add(dr);
                    }

                }
            }
            return dt;
        }


        #endregion


        #region Transform Data Table
        /// <summary>
        /// GenerateTransposedTable
        /// </summary>
        /// <param name="inputTable">DataTable</param>
        /// <returns>DataTable</returns>
        public static DataTable GenerateTransposedTable(DataTable inputTable)
        {
            DataTable outputTable = new DataTable();

            // Add columns by looping rows
            // Header row's first column is same as in inputTable
            outputTable.Columns.Add(inputTable.Columns[0].ColumnName.ToString());
            // Header row's second column onwards, 'inputTable's first column taken
            foreach (DataRow inRow in inputTable.Rows)
            {
                string newColName = inRow[0].ToString();
                outputTable.Columns.Add(newColName);
            }

            // Add rows by looping columns        
            for (int rowCount = 1; rowCount <= inputTable.Columns.Count - 1; rowCount++)
            {
                DataRow newRow = outputTable.NewRow();
                // First column is inputTable's Header row's second column
                newRow[0] = inputTable.Columns[rowCount].ColumnName.ToString();
                for (int cCount = 0; cCount <= inputTable.Rows.Count - 1; cCount++)
                {
                    string colValue = inputTable.Rows[cCount][rowCount].ToString();
                    newRow[cCount + 1] = colValue;
                }
                outputTable.Rows.Add(newRow);
            }

            return outputTable;
        }
        #endregion


        
    }
}
