﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DigiPhoto.Utility.Repository.Data
{
    public sealed class ClientConstant
    {
        public const short NullInt = short.MinValue;
        public const int NullInt32 = int.MinValue;
        public const long NullInt64 = long.MinValue;

        public static readonly DateTime NullDate = DateTime.MinValue;
        public const byte NullByte = byte.MinValue;
        public const decimal NullDecimal = short.MinValue;
        public const short NullShort = short.MinValue;
        public const decimal DefaultZeroDecimal = 0.00M;
        public const String YesString = "Y";
        public const String NoString = "N";
        public const string YesCompleteString = "Yes";
        public const string NoCompleteString = "No";
        public static DateTime MinimunDateValue = new DateTime(1753, 1, 1);
        public const char SeperatorComma = ',';
        public const bool NullBoolean = false;

        public static TimeSpan NullTimespan = new TimeSpan(0, 0, 0);

        // SQL Date Min Value
        public static readonly DateTime NullDbDate = new DateTime(1900, 1, 1);
        public const double NullDouble = double.MinValue;
        public const SByte NullSByte = SByte.MinValue;
        public const Single NullSingle = Single.MinValue;
        public const UInt16 NullUInt16 = UInt16.MinValue;
        public const UInt32 NullUInt32 = UInt32.MinValue;
        public const UInt64 NullUInt64 = UInt64.MinValue;

        //SQL Date MAX Value
        public static readonly DateTime MaxDbDate = new DateTime(2079, 6, 6, 23, 59, 00);

        public const Char NullChar = Char.MinValue;
        public const string DecimalValueEffectiveSignificantDigitFormat = "{0:0.######}";

        public static readonly string SelectString = "--Select--";
    }
}
