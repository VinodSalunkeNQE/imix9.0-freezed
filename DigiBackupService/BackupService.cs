﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Collections;
using System.Data.SqlClient;
using System.IO;
using System.Configuration;
using DigiPhoto.DataLayer;
using System.Globalization;
using DigiPhoto;
using System.Reflection;
using DigiPhoto.Utility.Repository.ValueType;
using DigiPhoto.IMIX.Business;
using DigiPhoto.IMIX.Model;
using FrameworkHelper.Common;

namespace DigiBackupService
{
    public partial class BackupService : ServiceBase
    {
        static System.Timers.Timer timer;
        static string _connstr = "";//ConfigurationManager.ConnectionStrings["DigiConnectionString"].ConnectionString;
        static string _databasename = "";//ConfigurationManager.AppSettings["DatabaseName"];
        static string _username = "";//ConfigurationManager.AppSettings["UserName"];
        static string _pass = "";//ConfigurationManager.AppSettings["UserPass"];
        static string _server = "";//ConfigurationManager.AppSettings["ServerName"];
        static Hashtable htSubStoreConfigurations = new Hashtable();
        static int _timerMiliSeconds = 15000; //15000
        static List<SubStoreBackupConfigurationInfo> lstBackupCofig = new List<SubStoreBackupConfigurationInfo>();
        static int maxBackupDays = 7;
        static int NasBackupDays = Convert.ToInt32(ConfigurationManager.AppSettings["NasBackupDays"]);

        public BackupService()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            //Read Connection String Initially (on service start).
            try
            {
                ErrorHandler.ErrorHandler.LogFileWrite("Backup Service: started ");
                this.RequestAdditionalTime(2 * 60 * 1000);
                ServicePosInfoBusiness svcPosinfoBusiness = new ServicePosInfoBusiness();
                string ret = svcPosinfoBusiness.ServiceStart(false);

                //ret = 1;
                if (string.IsNullOrEmpty(ret))
                {
                    StartScheduler();
                }
                else
                {
                    throw new Exception("Already Started");
                }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                this.ExitCode = 13816;
                this.Stop();
            }
        }

        private static void fillActiveSubStores()
        {
            try
            {
                List<int> SubstoreList = GetActiveSubstoreList();
                for (int counter = 0; counter < SubstoreList.Count; counter++)
                {
                    int subStoreId = SubstoreList[counter];
                    SubStoreBackupConfigurationInfo subStoreConfigInfo = GetBackupConfigData(subStoreId);
                    if (subStoreConfigInfo != null)
                    {
                        lstBackupCofig.Add(subStoreConfigInfo);
                        // htSubStoreConfigurations.Add(subStoreId, subStoreConfigInfo);
                    }
                }
                if (lstBackupCofig.Count > 0)
                {
                    // Delete all the folder older than max retention period of images in DigiImages
                    maxBackupDays = lstBackupCofig.Max(x => x.FailedOnlineOrderCleanupdays);
                    maxBackupDays = maxBackupDays == 0 ? 7 : maxBackupDays;
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
            }
        }


        public void StartScheduler()
        {
            try
            {
                timer = new System.Timers.Timer();
                timer.Interval = _timerMiliSeconds;
                timer.AutoReset = true;
                timer.Elapsed += new System.Timers.ElapsedEventHandler(this.timer_Elapsed);
                timer.Start();
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }

        public void testBackucp()
        {
            int subStoreId = 0;
            int Backupstatus = 0;
            string bcMsg = "";
            int errorId = 0;
            bool isDiskFull = false;
            bool driveNotExists = false;
            bool isBackup = false;
            try
            {
                //timer.Stop();
                //timer.Enabled = false;
                if (htSubStoreConfigurations == null || htSubStoreConfigurations.Count == 0)
                {
                    fillConfigServerVariables();
                    fillActiveSubStores();
                }

                ErrorHandler.ErrorHandler.LogFileWrite("GetNextBackupScheduled: started ");
                BackupHistoryInfo backupHistory = GetNextBackupScheduled();
                ErrorHandler.ErrorHandler.LogFileWrite("GetNextBackupScheduled: end ");

                // subStoreId = subStoreConfig.Key.ToInt32();
                //Get Next Backup Scheduled for this SubStore.

                if (backupHistory != null)
                {
                    SubStoreBackupConfigurationInfo currentSubStoreConfig = GetBackupConfigData(backupHistory.SubStoreId);
                    if (backupHistory.ScheduleDate < DateTime.Now)
                    {
                        try
                        {
                            subStoreId = backupHistory.SubStoreId;
                            int status = Convert.ToInt32(Digibackup.Inprogress);
                            GetsetbackupHistoryData(backupHistory.BackupId, status);
                            //Create new backup schedule(Error or Success) based on new recursive configuration.
                            SetBackupConfigData(subStoreId);
                            if (!Directory.Exists(currentSubStoreConfig.DbBackupPath) || !Directory.Exists(currentSubStoreConfig.HfBackupPath))
                            {
                                driveNotExists = true;
                            }
                            if (FrameworkHelper.Common.clsBackup.IsMemoryAvailable(currentSubStoreConfig.HotFolderPath, currentSubStoreConfig.HfBackupPath, subStoreId) && !driveNotExists)
                            {
                                string destinationFld = clsBackup.CreateBackupFolderExits(currentSubStoreConfig.HfBackupPath);
                                isBackup = ExecuteBackupUtility(currentSubStoreConfig, ref errorId, backupHistory.BackupId, subStoreId, backupHistory, destinationFld);
                                if (errorId == (int)BackupErrorMessage.DestinationDiskFull)
                                    isDiskFull = true;
                                else if (errorId == (int)BackupErrorMessage.DatabaseBackupAlreadyExists)
                                    GetsetbackupHistoryErrormessage(backupHistory.BackupId, "Database Backup already exists for today.", Convert.ToInt32(Digibackup.Completed));
                                try
                                {
                                    if (isBackup)
                                    {
                                        ErrorHandler.ErrorHandler.LogFileWrite("clsBackup.CleanLeftOverFolders 1: started ");
                                        clsBackup.CleanLeftOverFolders(currentSubStoreConfig.HotFolderPath, destinationFld, currentSubStoreConfig.CleanUpDaysBackUp);
                                        ErrorHandler.ErrorHandler.LogFileWrite("clsBackup.CleanLeftOverFolders 2: end ");
                                    }
                                }
                                catch (Exception ex)
                                {
                                    string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                                    ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                                }
                            }
                            else
                            {
                                if (driveNotExists)
                                    bcMsg = "Drive does not exist.";
                                else
                                {
                                    isDiskFull = true;
                                    bcMsg = "There is not enough space in the disk.";
                                }
                            }
                            if (isDiskFull || driveNotExists)
                            {
                                SaveEmailDetails(subStoreId, bcMsg,false);
                                GetsetbackupHistoryErrormessage(backupHistory.BackupId, bcMsg, Convert.ToInt32(Digibackup.failed));
                            }
                            ErrorHandler.ErrorHandler.LogFileWrite(bcMsg);

                            if (isBackup)
                            {
                                Backupstatus = Convert.ToInt32(Digibackup.Completed);
                                GetsetbackupHistoryEndDate(backupHistory.BackupId, Backupstatus);
                            }
                            else if (!isBackup)
                            {
                                bcMsg = "Backup is not heppned due to SQL server versioning issue/ Hotfolder path access/ folder path permission issue.";
                                GetsetbackupHistoryErrormessage(backupHistory.BackupId, bcMsg, Convert.ToInt32(Digibackup.failed));
                            }
                            else if ((errorId == (int)BackupErrorMessage.DestinationDiskFull) || isDiskFull || driveNotExists)
                            {
                                bcMsg = "Backup is not heppned due to either disk is full or drive does not exist";
                                GetsetbackupHistoryErrormessage(backupHistory.BackupId, bcMsg, Convert.ToInt32(Digibackup.failed));
                            }

                            GC.Collect();
                        }
                        catch (Exception ex)
                        {
                            string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                            ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                            int status = Convert.ToInt32(Digibackup.failed);
                            GetsetbackupHistoryErrormessage(backupHistory.BackupId, errorMessage, status);
                            //Create new backup schedule(Error or Success) based on new recursive configuration.
                            SetBackupConfigData(subStoreId);
                            SaveEmailDetails(subStoreId, errorMessage,false);
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
            finally
            {
                //timer.Start();
            }
        }

        public void timer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            int subStoreId = 0;
            int Backupstatus = 0;
            string bcMsg = "";
            int errorId = 0;
            bool isDiskFull = false;
            bool driveNotExists = false;
            bool isBackup = false;
            bool isIssueExist = false;

            try
            {
                ErrorHandler.ErrorHandler.LogFileWrite("timer_Elapsed: Backup started " + System.DateTime.Now);
                timer.Stop();
                timer.Enabled = false;
                if (htSubStoreConfigurations == null || htSubStoreConfigurations.Count == 0)
                {
                    fillConfigServerVariables();
                    fillActiveSubStores();
                }

                BackupHistoryInfo backupHistory = GetNextBackupScheduled();
                //Get Next Backup Scheduled for this SubStore.

                if (backupHistory != null)
                {
                    SubStoreBackupConfigurationInfo currentSubStoreConfig = GetBackupConfigData(backupHistory.SubStoreId);
                    if (backupHistory.ScheduleDate < DateTime.Now)
                    {
                        try
                        {
                            subStoreId = backupHistory.SubStoreId;
                            int status = Convert.ToInt32(Digibackup.Inprogress);
                            GetsetbackupHistoryData(backupHistory.BackupId, status);
                            //Create new backup schedule(Error or Success) based on new recursive configuration.
                            SetBackupConfigData(subStoreId);
                            if (!Directory.Exists(currentSubStoreConfig.DbBackupPath) || !Directory.Exists(currentSubStoreConfig.HfBackupPath))
                            {
                                ErrorHandler.ErrorHandler.LogFileWrite("DbBackupPath : " + currentSubStoreConfig.DbBackupPath + " " + " HfBackupPath : " + currentSubStoreConfig.HfBackupPath);
                                ErrorHandler.ErrorHandler.LogFileWrite("Drive does not exist.");
                                driveNotExists = true;
                                bcMsg = "Drive does not exist.";
                                isIssueExist = true;
                                SaveEmailDetails(subStoreId, bcMsg,false);
                                goto UpdateBackupHistory;

                            }
                            if (FrameworkHelper.Common.clsBackup.IsMemoryAvailable(currentSubStoreConfig.HotFolderPath, currentSubStoreConfig.HfBackupPath, subStoreId) && !driveNotExists)
                            {
                                string destinationFld = clsBackup.CreateBackupFolderExits(currentSubStoreConfig.HfBackupPath);
                                //ErrorHandler.ErrorHandler.LogFileWrite("ExecuteBackupUtility Start : ");
                                isBackup = ExecuteBackupUtility(currentSubStoreConfig, ref errorId, backupHistory.BackupId, subStoreId, backupHistory, destinationFld);
                                //ErrorHandler.ErrorHandler.LogFileWrite("ExecuteBackupUtility End : ");
                                if (errorId == (int)BackupErrorMessage.DestinationDiskFull)
                                    isDiskFull = true;
                                else if (errorId == (int)BackupErrorMessage.DatabaseBackupAlreadyExists)
                                    GetsetbackupHistoryErrormessage(backupHistory.BackupId, "Database Backup already exists for today.", Convert.ToInt32(Digibackup.Completed));
                                try
                                {
                                    if (isBackup)
                                    {
                                        clsBackup.CleanLeftOverFolders(currentSubStoreConfig.HotFolderPath, destinationFld, currentSubStoreConfig.CleanUpDaysBackUp);
                                        Backupstatus = Convert.ToInt32(Digibackup.Completed);
                                        GetsetbackupHistoryEndDate(backupHistory.BackupId, Backupstatus);
                                        bcMsg = "Backup executed successfully";
                                        SaveEmailDetails(subStoreId, bcMsg,true);
                                    }
                                }
                                catch (Exception ex)
                                {
                                    string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                                    ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                                }
                            }
                            else
                            {

                                if (isDiskFull || driveNotExists)
                                {
                                    isIssueExist = true;
                                    bcMsg = "Either disk is full or drive does not exist.";
                                    //SaveEmailDetails(subStoreId, bcMsg,false);
                                    goto UpdateBackupHistory;
                                }
                                else if ((errorId == (int)BackupErrorMessage.DestinationDiskFull) || isDiskFull)
                                {
                                    isIssueExist = true;
                                    bcMsg = "Backup is not heppned due to either disk is full or SQL server versioning issue or folder path permission issue";
                                    //SaveEmailDetails(subStoreId, bcMsg,false);
                                    goto UpdateBackupHistory;
                                }
                                else if (!isBackup)
                                {
                                    isIssueExist = true;
                                    bcMsg = "Backup is not heppned due to SQL server versioning issue or folder path permission issue.";
                                    //SaveEmailDetails(subStoreId, bcMsg,false);
                                    goto UpdateBackupHistory;
                                }

                            }
                            UpdateBackupHistory:
                            if (isIssueExist)
                                GetsetbackupHistoryErrormessage(backupHistory.BackupId, bcMsg, Convert.ToInt32(Digibackup.failed));
                            SaveEmailDetails(subStoreId, bcMsg, false);
                            GC.Collect();
                            ErrorHandler.ErrorHandler.LogFileWrite("timer_Elapsed: Backup Completed " + System.DateTime.Now);
                            
                        }
                        catch (Exception ex)
                        {
                            string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                            ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                            int status = Convert.ToInt32(Digibackup.failed);
                            GetsetbackupHistoryErrormessage(backupHistory.BackupId, errorMessage, status);
                            //Create new backup schedule(Error or Success) based on new recursive configuration.
                            SetBackupConfigData(subStoreId);
                            SaveEmailDetails(subStoreId, errorMessage,false);
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogFileWrite("timer_Elapsed : Catch");
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                SaveEmailDetails(subStoreId, errorMessage, false);
            }
            finally
            {
                timer.Start();
            }
        }
        /// <summary>
        /// DoBackupNowDaily
        /// </summary>
        /// <param name="subStoreId"></param>
        /// <param name="_imgSource"></param>
        /// <param name="destinationId"></param>
        /// <param name="excludeBackUpDays"></param>
        private static void DoBackupNowDaily(Int32 subStoreId, string _imgSource, string destinationId, int excludeBackUpDays)
        {
            try
            {
               // ErrorHandler.ErrorHandler.LogFileWrite("DoBackupNowDaily: started ");
                string _destinationPathDaily = string.Empty;
                string _filePathDate = string.Empty;
                string _thumbnailFilePathDate = string.Empty;
                string _bigThumbnailFilePathDate = string.Empty;
                string _destinationPathDailySite = string.Empty;
                string _filePathDateSource = string.Empty;
                string _thumbnailFilePathDateSource = string.Empty;
                string _bigThumbnailFilePathDateSource = string.Empty;
                string _VideoFilePathDate = string.Empty;
                string _ProcessedVideoFilePathDate = string.Empty;
                string _VideoFilePathDateSource = string.Empty;
                string _ProcessedVideoFilePathDateSource = string.Empty;
                iMIXConfigurationInfo _objdata = (new ConfigBusiness()).GetNewConfigValues(subStoreId)
                    .Where(o => o.IMIXConfigurationMasterId == 95).FirstOrDefault();
                if (_objdata != null && !string.IsNullOrEmpty(_objdata.ConfigurationValue))
                {
                    _destinationPathDaily = _objdata.ConfigurationValue.ToString();
                    _destinationPathDaily = FrameworkHelper.Common.clsBackup.CreateBackupFolderExits(_destinationPathDaily);
                    DailyImageBackupInNAS(_imgSource, _destinationPathDaily);
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogFileWrite("DoBackupNowDaily catch ");
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="_imgSource"></param>
        /// <param name="_destinationPathDaily"></param>
        /// <param name="excludeBackUpDays"></param>
        private static void DailyImageBackupInNAS(string _imgSource, string _destinationPathDaily)
        {
            string _filePathDate = string.Empty;
            string _filePathDateSource = string.Empty;
            string _VideoFilePathDate = string.Empty;
            string _ProcessedVideoFilePathDate = string.Empty;
            string _VideoFilePathDateSource = string.Empty;
            string _ProcessedVideoFilePathDateSource = string.Empty;
            try
            {
                if (!String.IsNullOrEmpty(_destinationPathDaily) && !String.IsNullOrWhiteSpace(_destinationPathDaily))
                {
                    if (Directory.Exists(_destinationPathDaily))
                    {
                        if (!String.IsNullOrEmpty(_imgSource))
                        {
                            if (Directory.Exists(_imgSource))
                            {
                                _filePathDate = System.IO.Path.Combine(_destinationPathDaily, DateTime.Now.AddDays(-NasBackupDays).ToString("yyyyMMdd"));
                                _filePathDateSource = System.IO.Path.Combine(_imgSource, DateTime.Now.AddDays(-NasBackupDays).ToString("yyyyMMdd"));
                                CopyFiles(_filePathDateSource, _filePathDate);
                                _VideoFilePathDate = System.IO.Path.Combine(_destinationPathDaily, "Videos", DateTime.Now.AddDays(-NasBackupDays).ToString("yyyyMMdd"));
                                _VideoFilePathDateSource = System.IO.Path.Combine(_imgSource, "Videos", DateTime.Now.AddDays(-NasBackupDays).ToString("yyyyMMdd"));
                                CopyFiles(_VideoFilePathDateSource, _VideoFilePathDate);
                                _ProcessedVideoFilePathDate = System.IO.Path.Combine(_destinationPathDaily, "ProcessedVideos", DateTime.Now.AddDays(-NasBackupDays).ToString("yyyyMMdd"));
                                _ProcessedVideoFilePathDateSource = System.IO.Path.Combine(_imgSource, "ProcessedVideos", DateTime.Now.AddDays(-NasBackupDays).ToString("yyyyMMdd"));
                                CopyFiles(_ProcessedVideoFilePathDateSource, _ProcessedVideoFilePathDate);
                            }
                        }
                    }
                }
            }
            catch
            {
                ErrorHandler.ErrorHandler.LogFileWrite("DailyImageBackupInNAS: catch ");
            }
        }


        private static void CopyFiles(string source, string destination)
        {

            if (Directory.Exists(source))
            {
                if (!Directory.Exists(destination))
                {
                    Directory.CreateDirectory(destination);
                }
                CopyAllFile(new DirectoryInfo(source), new DirectoryInfo(destination));
            }
        }

        public static void CopyAllFile(DirectoryInfo source, DirectoryInfo target)
        {
            // Copy each file into the new directory.
            foreach (FileInfo fi in source.GetFiles())
            {
                if (!File.Exists(target + @"\" + fi.Name))
                    fi.CopyTo(System.IO.Path.Combine(target.FullName, fi.Name));

            }
        }
        private bool SaveEmailDetails(int subStoreID, string exceptionMsg,bool status)
        {
            ConfigBusiness buss = new ConfigBusiness();
            Dictionary<long, string> iMIXConfigurations = new Dictionary<long, string>();
            iMIXConfigurations = buss.GetConfigurations(iMIXConfigurations, subStoreID);
            string ToAddress = iMIXConfigurations.Where(x => x.Key == (int)ConfigParams.BackUpEmailAddress).FirstOrDefault().Value;
            if (ToAddress != null)
            {
                if(status)
                {
                    bool success = buss.SaveEmailDetails(ToAddress, string.Empty, string.Empty, exceptionMsg, "BACKUP DONE SUCCESSFULLY" , subStoreID);
                }
                else
                {
                    bool success = buss.SaveEmailDetails(ToAddress, string.Empty, string.Empty, exceptionMsg, "BACKUP FAILED!", subStoreID);
                }
               
            }
            return true;
        }
        private bool SaveEmailDetails(int subStoreID, string exceptionMsg)
        {
            ConfigBusiness buss = new ConfigBusiness();
            Dictionary<long, string> iMIXConfigurations = new Dictionary<long, string>();
            iMIXConfigurations = buss.GetConfigurations(iMIXConfigurations, subStoreID);
            string ToAddress = iMIXConfigurations.Where(x => x.Key == (int)ConfigParams.BackUpEmailAddress).FirstOrDefault().Value;
            if (ToAddress != null)
            {
                bool success = buss.SaveEmailDetails(ToAddress, string.Empty, string.Empty, exceptionMsg, "BACKUP FAILED", subStoreID);
            }
            return true;
        }
        private static void fillConfigServerVariables()
        {
            try
            {
                string appConString = "";
                _connstr = System.Configuration.ConfigurationManager.ConnectionStrings["DigiConnectionString"].ConnectionString;
                appConString = _connstr;

                ErrorHandler.ErrorHandler.LogFileWrite("fillConfigServerVariables: appConString " + appConString);

                string[] strVar = appConString.Split(';');
                Hashtable htCon = new Hashtable();
                if (strVar.Length != 0)
                {
                    for (int k = 0; k <= strVar.Length - 1; k++)
                    {
                        string[] tempK = strVar[k].Split('=');
                        if (tempK.Length != 0)
                        {
                            if (!htCon.ContainsKey(tempK[0]))
                            {
                                htCon.Add(tempK[0], tempK[1]);
                            }
                        }
                    }
                }
                ErrorHandler.ErrorHandler.LogFileWrite("fillConfigServerVariables: assign vals ");

                _databasename = htCon.ContainsKey("Initial Catalog") ? htCon["Initial Catalog"].ToString() : "";
                ErrorHandler.ErrorHandler.LogFileWrite("fillConfigServerVariables: assign vals _databasename" + _databasename);
                //Author Bhavin Udani. Created on 8 April 2021. Change Of "User ID" to "User Id"
                _username = htCon.ContainsKey("User Id") ? htCon["User Id"].ToString() : "";
                ErrorHandler.ErrorHandler.LogFileWrite("fillConfigServerVariables: assign vals _username" + _username);
                _pass = htCon.ContainsKey("Password") ? htCon["Password"].ToString() : "";
                ErrorHandler.ErrorHandler.LogFileWrite("fillConfigServerVariables: assign vals _pass" + _pass);
                _server = htCon.ContainsKey("Data Source") ? htCon["Data Source"].ToString() : "";
                ErrorHandler.ErrorHandler.LogFileWrite("fillConfigServerVariables: start " + _databasename + " , " + _username + " , " + _pass + " , " + _server);
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogFileWrite("fillConfigServerVariables: catch ");
                ErrorHandler.ErrorHandler.LogError(ex);
            }

        }
        private static void HotFolderBackup(SubStoreBackupConfigurationInfo currentSubStoreConfig, int SubstoreId, string destinationFld)
        {
            try
            {
                bool isTableCleanup = clsBackup.EmptyTables(currentSubStoreConfig.CleanUpDaysBackUp, currentSubStoreConfig.CleanUpDaysOldBackup, SubstoreId, currentSubStoreConfig.FailedOnlineOrderCleanupdays);
                if (isTableCleanup)
                {
                    if ((!String.IsNullOrEmpty(currentSubStoreConfig.HotFolderPath)) && (!String.IsNullOrEmpty(currentSubStoreConfig.HfBackupPath) && !String.IsNullOrWhiteSpace(currentSubStoreConfig.HfBackupPath)))
                    {
                        if (Directory.Exists(currentSubStoreConfig.HfBackupPath))
                        {
                            int delImgCount = 0;
                            int tableCount;
                            if (!String.IsNullOrEmpty(currentSubStoreConfig.HotFolderPath))
                            {
                                DoBackupNowDaily(SubstoreId, currentSubStoreConfig.HotFolderPath, destinationFld, currentSubStoreConfig.CleanUpDaysBackUp);
                                tableCount = clsBackup.GetPhotoArchived();
                                if (tableCount > 0)
                                {
                                    delImgCount += clsBackup.CopyDirectoryFiles(destinationFld, currentSubStoreConfig.HotFolderPath);
                                }
                                bool statusUpdated = clsBackup.UpdateArchivedPhotoDetails();
                            }
                        }
                        else
                        {
                            throw new Exception("Drive Not Exists");
                        }
                    }
                }
                else
                {
                    throw new Exception("Image Destination Path is Null");
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogFileWrite("DB backup CompleteDBBackup : catch " + ex.Message);
            }
        }

        /// <summary>
        /// This function is used to take hot folder back and database back as per config utility and then delete from 
        /// hotfolder backup path and deletion from database to selected table as config utility
        /// 
        /// MOdified by:Manoj Gupta
        /// Modified Date:21/02/2018
        /// Reason:Now this function will work as usual, just removed database cleanup as per configration
        /// </summary>
        /// <param name="SubstoreId"></param>
        /// <param name="errorID"></param>
        /// <returns></returns>
        private static Boolean ExecuteBackupUtility(SubStoreBackupConfigurationInfo currentSubStoreConfig, ref int errorID, int BackupId, int SubstoreId, BackupHistoryInfo BackupHistory, string destinationPath)
        {
            int isDBBackupDone = 0;
            Boolean isBackup = false;
            try
            {
                //ErrorHandler.ErrorHandler.LogFileWrite("ExecuteBackupUtility 1: started ");
                //  SubStoreBackupConfigurationInfo currentSubStoreConfig = htSubStoreConfigurations[SubstoreId] as SubStoreBackupConfigurationInfo;
                if (currentSubStoreConfig == null)
                    throw new Exception("Backup configuration not found.");

                //bool isTableCleanup;
                if (!String.IsNullOrEmpty(currentSubStoreConfig.DbBackupPath) && !String.IsNullOrWhiteSpace(currentSubStoreConfig.DbBackupPath))
                {
                    if (Directory.Exists(currentSubStoreConfig.DbBackupPath))
                    {
                        if (clsBackup.IsDbBackupDone(SubstoreId))
                        {
                            isDBBackupDone = CompleteDBBackup(currentSubStoreConfig, ref errorID);

                            if (isDBBackupDone == 1)
                            {
                                HotFolderBackup(currentSubStoreConfig, SubstoreId, destinationPath);
                                isBackup = true;
                            }
                            else
                            {
                                GetsetbackupHistoryData(BackupHistory.BackupId, Convert.ToInt32(Digibackup.scheduled));
                                isBackup = false;
                            }
                        }
                        else
                        {
                            isBackup = true;
                            HotFolderBackup(currentSubStoreConfig, SubstoreId, destinationPath);
                        }
                    }
                    else
                    {
                        isBackup = false;
                        throw new Exception("Drive Not Exists");
                    }
                }
                else
                {
                    isBackup = false;
                    throw new Exception("Destination Path is not correct");
                }
                return isBackup;
            }
            catch (Exception ex)
            {
                isBackup = false;
                ErrorHandler.ErrorHandler.LogFileWrite("ExecuteBackupUtility : catch " + ex.StackTrace);
                throw;
            }

        }

        private static int CompleteDBBackup(SubStoreBackupConfigurationInfo currentSubStoreConfig, ref int errorID)
        {
            string strMsg = "";
            bool isDBBackupComplete = false;
            int returnValue = 0;
            try
            {
                try
                {
                    //ErrorHandler.ErrorHandler.LogFileWrite("CleanOldBackupData 1: started ");
                    CleanOldBackupData(currentSubStoreConfig);
                }
                catch (Exception ex)
                {
                    string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                    ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                }
                //int errorID = 0;
                isDBBackupComplete = clsBackup.BackupDatabase(_databasename, _username, _pass, _server, currentSubStoreConfig.DbBackupPath, ref errorID);
                if (!isDBBackupComplete && errorID == (int)BackupErrorMessage.DestinationDiskFull)
                {
                    strMsg = "There is not enough space in the disk.";
                    return returnValue;
                }
                if (isDBBackupComplete)
                {
                    returnValue = 1;
                    strMsg += "- Database backup complete! \n\r\n\r";
                    return returnValue;
                }
                else
                {
                    //throw new Exception("DB Backup operation could not be executed");
                    strMsg += "DB Backup operation could not be executed";
                    return returnValue;
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogFileWrite("DB backup CompleteDBBackup : catch " + ex.Message);
                return returnValue;
            }
        }

        protected override void OnStop()
        {
            timer.Stop();
            timer = null;
            ServicePosInfoBusiness svcPosinfoBusiness = new ServicePosInfoBusiness();
            svcPosinfoBusiness.StopService(false);
        }

        public static SubStoreBackupConfigurationInfo GetBackupConfigData(int subStorId)
        {
            //fillConfigServerVariables();          //Commented by Anand, not required for every timer tick
            SubStoreBackupConfigurationInfo subStoreBackupConfig = null;
            using (SqlConnection con = new SqlConnection(_connstr))
            {
                using (SqlCommand cmd = new SqlCommand("DG_GetBackupConfigDetails", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("@SubStoreId", SqlDbType.Int).Value = subStorId;
                    con.Open();
                    //cmd.ExecuteNonQuery();

                    using (SqlDataReader rdr = cmd.ExecuteReader(CommandBehavior.CloseConnection))
                    {
                        //if (rdr.HasRows)
                        while (rdr.Read())
                        {
                            //rdr.Read();
                            subStoreBackupConfig = new SubStoreBackupConfigurationInfo();
                            subStoreBackupConfig.DbBackupPath = rdr["DG_DbBackupPath"].ToString();
                            subStoreBackupConfig.HotFolderPath = rdr["DG_Hot_Folder_Path"].ToString();
                            subStoreBackupConfig.HfBackupPath = rdr["DG_HfBackupPath"].ToString();
                            subStoreBackupConfig.CleanupTables = rdr["DG_CleanupTables"].ToString();
                            subStoreBackupConfig.CleanUpDaysBackUp = rdr["DG_CleanUpDaysBackUp"].ToInt32();
                            subStoreBackupConfig.CleanUpDaysOldBackup = rdr["DG_CleanUpDaysOldBackup"].ToInt32();
                            subStoreBackupConfig.FailedOnlineOrderCleanupdays = rdr["DG_FailedOnlineOrderCleanUpDays"].ToInt32();
                        }
                    }
                }
            }
            return subStoreBackupConfig;
        }
        
        /// <summary>
        /// Clean backup folder which is greater than old cleanup days
        /// </summary>
        /// <param name="currentSubStoreConfig"></param>
        public static void CleanOldBackupData(SubStoreBackupConfigurationInfo currentSubStoreConfig)
        {
            if (Directory.Exists(currentSubStoreConfig.HfBackupPath))
            {
                DirectoryInfo imgDirInfo = new DirectoryInfo(currentSubStoreConfig.HfBackupPath);
                imgDirInfo.EnumerateFiles("*", SearchOption.AllDirectories).Where(c => c.CreationTime < (DateTime.Now.AddDays(-(currentSubStoreConfig.CleanUpDaysOldBackup + currentSubStoreConfig.CleanUpDaysBackUp)))).ToList().ForEach(d => d.Delete());

                DirectoryInfo[] dirInfoList = imgDirInfo.EnumerateDirectories("*", SearchOption.TopDirectoryOnly).Where(c => c.LastWriteTime < (DateTime.Now.AddDays(-currentSubStoreConfig.CleanUpDaysOldBackup))).ToArray();//ToList().ForEach(d => d.Delete());
                foreach (DirectoryInfo dir in dirInfoList)
                {
                    RecursiveDelete(dir);
                }
            }

            if (Directory.Exists(currentSubStoreConfig.DbBackupPath))
            {
                DirectoryInfo dirInfo = new DirectoryInfo(currentSubStoreConfig.DbBackupPath);
                dirInfo.EnumerateFiles("*", SearchOption.AllDirectories).Where(c => c.CreationTime < (DateTime.Now.AddDays(-currentSubStoreConfig.CleanUpDaysOldBackup))).ToList().ForEach(d => d.Delete());

            }
        }

        /// <summary>
        /// delete directory recursivly
        /// </summary>
        /// <param name="baseDir"></param>
        public static void RecursiveDelete(DirectoryInfo baseDir)
        {
            if (!baseDir.Exists)
                return;

            foreach (var dir in baseDir.EnumerateDirectories())
            {
                RecursiveDelete(dir);
            }
            baseDir.Delete(true);
        }

        public static List<int> GetActiveSubstoreList()
        {
            List<int> SubstoreIds = new List<int>();
            //fillConfigServerVariables();          //Commented by Anand, not required for every timer tick
            using (SqlConnection con = new SqlConnection(_connstr))
            {
                using (SqlCommand cmd = new SqlCommand("DG_GetActiveSubstoreIds", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;

                    //cmd.Parameters.Add("@SubStoreId", SqlDbType.VarChar).Value = 1; //LoginUser.SubStoreId;
                    con.Open();
                    //cmd.ExecuteNonQuery();

                    using (SqlDataReader rdr = cmd.ExecuteReader(CommandBehavior.CloseConnection))
                    {
                        if (rdr.HasRows)
                        {
                            while (rdr.Read())
                            {
                                SubstoreIds.Add(rdr["SubstoreId"].ToInt32());
                            }
                        }
                    }
                }
            }
            return SubstoreIds;
        }

        public static void SetBackupConfigData(int SubstorId)
        {
            try
            {
                using (SqlConnection con = new SqlConnection(_connstr))
                {
                    using (SqlCommand cmd = new SqlCommand("DG_UpdateBackupComplete", con))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add("@SubStoreId", SqlDbType.VarChar).Value = SubstorId;
                        con.Open();
                        cmd.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogFileWrite("DB backup SetBackupConfigData : catch " + ex.Message);
            }
        }

        public static void GetsetbackupHistoryData(int backupId, int status)
        {
            using (SqlConnection con = new SqlConnection(_connstr))
            {
                using (SqlCommand cmd = new SqlCommand("DG_UpdatebackupstartTime", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("@BackupId", SqlDbType.Int).Value = backupId;
                    cmd.Parameters.Add("@status", SqlDbType.Int).Value = status;
                    con.Open();
                    cmd.ExecuteNonQuery();
                }
            }
        }

        public static void GetsetbackupHistoryEndDate(int backupId, int Backupstatus)
        {
            using (SqlConnection con = new SqlConnection(_connstr))
            {
                using (SqlCommand cmd = new SqlCommand("DG_UpdatebackupEndTime", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add("@BackupId", SqlDbType.VarChar).Value = backupId;
                    cmd.Parameters.Add("@status", SqlDbType.VarChar).Value = Backupstatus;
                    con.Open();
                    cmd.ExecuteNonQuery();
                }
            }
        }

        public static void GetsetbackupHistoryErrormessage(int backupId, string errorMessage, int Backupstatus)
        {
            using (SqlConnection con = new SqlConnection(_connstr))
            {
                using (SqlCommand cmd = new SqlCommand("DG_UpdatebackupErrormessage", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("@BackupId", SqlDbType.VarChar).Value = backupId;
                    cmd.Parameters.Add("@errorMessage", SqlDbType.VarChar).Value = errorMessage;
                    cmd.Parameters.Add("@status", SqlDbType.VarChar).Value = Backupstatus;
                    con.Open();
                    cmd.ExecuteNonQuery();
                }
            }
        }

        public static BackupHistoryInfo GetNextBackupScheduled()
        {
            BackupHistoryInfo backupHistory = null;
            using (SqlConnection con = new SqlConnection(_connstr))
            {
                using (SqlCommand cmd = new SqlCommand("DG_NextScheduledBackup", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    con.Open();
                    SqlDataReader rdr = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                    while (rdr.Read())
                    {
                        //rdr.Read();
                        backupHistory = new BackupHistoryInfo();
                        backupHistory.BackupId = rdr["BackupId"].ToInt32();
                        backupHistory.ScheduleDate = rdr["ScheduleDate"].ToDateTime();
                        backupHistory.SubStoreId = rdr["SubStoreId"].ToInt32();
                    }
                }
            }
            return backupHistory;
        }
    }
    public class BackupHistoryInfo
    {
        public int BackupId { get; set; }
        public DateTime ScheduleDate { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public int Status { get; set; }
        public string ErrorMessage { get; set; }
        public int SubStoreId { get; set; }
        public bool isBackupDone { get; set; }
    }

    public class SubStoreBackupConfigurationInfo
    {
        public string DbBackupPath { get; set; }
        public string HotFolderPath { get; set; }
        public string HfBackupPath { get; set; }
        public string CleanupTables { get; set; }
        public int CleanUpDaysBackUp { get; set; }
        public int CleanUpDaysOldBackup { get; set; }

        public int FailedOnlineOrderCleanupdays { get; set; }
    }
}
