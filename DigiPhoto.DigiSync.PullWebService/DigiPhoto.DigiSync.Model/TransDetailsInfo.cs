﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DigiPhoto.DigiSync.Model
{
    public class TransDetailsInfo
    {
        public long TransDetailID { get; set; }

        public Int32 SubstoreID { get; set; }

        public DateTime TransDate { get; set; }

        public Int32 PackageID { get; set; }

        public string PackageSyncCode { get; set; }

        public decimal UnitPrice { get; set; }

        public decimal Quantity { get; set; }

        public decimal Discount { get; set; }

        public decimal Total { get; set; }

        public long ClosingFormDetailID { get; set; }
    }
}
