﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DigiPhoto.DigiSync.Model
{
    public class SubStoreInfo
    {
        public long SubStoreId { get; set; }
        public string SubStoreCode { get; set; }
        public string SubStore { get; set; }
        public string Store { get; set; }
        public string Country { get; set; }

        public String SyncCode { get; set; }
    }
}
