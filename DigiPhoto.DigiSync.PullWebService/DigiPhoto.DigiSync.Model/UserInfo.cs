﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DigiPhoto.DigiSync.Model
{
    public class UserInfo
    {
        public long UserId { get; set; }
        public string UserName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MiddleName { get; set; }
        public string EmailAddress { get; set; }
        public string PhoneNumber { get; set; }
        public string MobileNumber { get; set; }
        public DateTime DateOfBirth { get; set; }
        public string Password { get; set; }

        //add new properties for SyncUtility
        public string SyncCode { get; set; }
        public string RoleSyncCode { get; set; }

        public string LocationSyncCode { get; set; }
        public int Location_ID { get; set; }

        public int Role_ID { get; set; }
        public DateTime ?CreatedDateTime { get; set; }
        public bool? IsActive { get; set; }
     
        public string StoreName { get; set; }

        public string CountryName { get; set; }

        public string SubstoreName { get; set; }
        public string SubstoreSyncCode { get; set; }

       

      
      
    }
}
