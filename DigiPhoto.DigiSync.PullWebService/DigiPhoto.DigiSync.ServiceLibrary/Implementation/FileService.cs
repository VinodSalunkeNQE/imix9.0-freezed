﻿using DigiPhoto.DigiSync.Model;
using DigiPhoto.DigiSync.ServiceLibrary.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Configuration;
using log4net;
using System.Drawing;
namespace DigiPhoto.DigiSync.ServiceLibrary.Implementation
{
    [ServiceBehavior]
    public class FileService : IFileService
    {
        protected static ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        public RemoteFileInfo DownloadFile(DownloadRequest request)
        {
            RemoteFileInfo result = new RemoteFileInfo();
            try
            {
                // get some info about the input file
                //string filePath = System.IO.Path.Combine(@"d:\Uploadfiles", request.FilePath);
                System.IO.FileInfo fileInfo = new System.IO.FileInfo(request.FilePath);

                // check if exists
                if (!fileInfo.Exists) throw new System.IO.FileNotFoundException("File not found", request.FilePath);

                // open stream
                System.IO.FileStream stream = new System.IO.FileStream(request.FilePath, System.IO.FileMode.Open, System.IO.FileAccess.Read);

                // return result

                result.FileName = request.FilePath;
                result.Length = fileInfo.Length;
                result.FileByteStream = stream;
            }
            catch (Exception ex)
            {
                log.Error("FileService:DownloadFile:: Message: " + ex.Message + " StackTrace: " + ex.StackTrace + " InnerException: " + ex.InnerException + " Source: " + ex.Source);
                throw new Exception(ex.ToString());
            }
            return result;
        }

        public void UploadFile(RemoteFileInfo request)
        {
            try
            {
                //string uploadFolder = ConfigurationManager.AppSettings["DigiphotoFilePath"];
                FileStream targetStream = null;
                Stream sourceStream = request.FileByteStream;
                string filePath = string.Empty;
                if (!string.IsNullOrEmpty(request.SaveFolderPath))
                {
                    string SaveFolderPath = string.Empty;
                    if (request.SaveFolderPath == "Border")
                        SaveFolderPath = ConfigurationManager.AppSettings["DigiphotoBorderPath"];
                    else if (request.SaveFolderPath == "BorderThumbnails")
                        SaveFolderPath = Path.Combine(ConfigurationManager.AppSettings["DigiphotoBorderPath"], "Thumbnails");
                    else if (request.SaveFolderPath == "BackGround")
                        SaveFolderPath = ConfigurationManager.AppSettings["DigiphotoBackGroundPath"];
                    else if (request.SaveFolderPath == "BackGroundThumbnails")
                        SaveFolderPath = Path.Combine(ConfigurationManager.AppSettings["DigiphotoBackGroundPath"], "Thumbnails");
                    else if (request.SaveFolderPath == "Graphics")
                        SaveFolderPath = ConfigurationManager.AppSettings["DigiphotoGraphicsPath"];

                    //string dirPath = Path.Combine(uploadFolder, SaveFolderPath);
                    //if (!Directory.Exists(dirPath))
                    //{
                    //    Directory.CreateDirectory(dirPath);
                    //}
                    //string dirPath = Path.Combine(uploadFolder, SaveFolderPath);
                    if (!Directory.Exists(SaveFolderPath))
                    {
                        Directory.CreateDirectory(SaveFolderPath);
                    }
                    //filePath = Path.Combine(uploadFolder, SaveFolderPath, request.FileName);
                    filePath = Path.Combine(SaveFolderPath, request.FileName);
                    using (targetStream = new FileStream(filePath, FileMode.Create, FileAccess.Write, FileShare.None))
                    {
                        //read from the input stream in 6K chunks
                        //and save to output stream
                        const int bufferLen = 65000;
                        byte[] buffer = new byte[bufferLen];
                        int count = 0;
                        while ((count = sourceStream.Read(buffer, 0, bufferLen)) > 0)
                        {
                            targetStream.Write(buffer, 0, count);
                        }
                        targetStream.Close();
                        sourceStream.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("FileService:UploadFile:: Message: " + ex.Message + " StackTrace: " + ex.StackTrace + " InnerException: " + ex.InnerException + " Source: " + ex.Source);
                throw new Exception(ex.ToString());
            }

        }

        public void UploadFileBytes(RemoteFileBytesInfo request)
        {
            //log.Info("Recieved bytes [" + System.Text.Encoding.Default.GetString(request.Filebytes) + "]");
            log.Info("==============================================");
            log.Info("FileName - " + request.FileName);
            log.Info("SaveFolderPath - " + request.SaveFolderPath);
            //log.Info("bytes - " + request.bytes);
            //log.Info("FileBytes - " + request.FileBytes.Length);
            log.Info("==============================================");
            try
            {
                byte[] byteArray = null;
                if (request.FileBytes != null && request.FileBytes.Length > 0)
                {
                    byteArray = request.FileBytes;
                }
                else
                {
                    byteArray = Convert.FromBase64String(request.bytes);
                }
                string uploadFolder = ConfigurationManager.AppSettings["DigiphotoQRClaimImagesPath"];
                FileStream targetStream = null;
                //Stream sourceStream = request.FileByteStream;
                string filePath = string.Empty;
                if (!string.IsNullOrEmpty(request.SaveFolderPath))
                {
                    //string dirPath = Path.Combine(uploadFolder, request.SaveFolderPath);
                    string dirPath = System.IO.Path.GetDirectoryName(Path.Combine(uploadFolder, request.SaveFolderPath));
                    if (!Directory.Exists(dirPath))
                    {
                        Directory.CreateDirectory(dirPath);
                    }

                    filePath = Path.Combine(uploadFolder, request.SaveFolderPath);
                    using (targetStream = new FileStream(filePath, FileMode.Create, FileAccess.Write, FileShare.None))
                    {
                        //read from the input stream in 6K chunks
                        //and save to output stream
                        targetStream.Write(byteArray, 0, byteArray.Length);
                        targetStream.Close();
                        //sourceStream.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("FileService:UploadFileBytes:: Message: " + ex.Message + " StackTrace: " + ex.StackTrace + " InnerException: " + ex.InnerException + " Source: " + ex.Source);
                throw new Exception(ex.ToString());
            }

        }

        //We need to download images for zip file creation on loacal system 
        public RemoteFileInfo DownloadFileForRelativeFilePath(DownloadRequest request)
        {
            RemoteFileInfo result = new RemoteFileInfo();
            try
            {
                // get some info about the input file
                string downloadFilePath = Path.Combine(ConfigurationManager.AppSettings["DigiphotoQRClaimImagesPath"], request.FilePath);
                System.IO.FileInfo fileInfo = new System.IO.FileInfo(downloadFilePath);

                // check if exists
                if (!fileInfo.Exists) throw new System.IO.FileNotFoundException("File not found", request.FilePath);

                // open stream
                System.IO.FileStream stream = new System.IO.FileStream(downloadFilePath, System.IO.FileMode.Open, System.IO.FileAccess.Read);

                // return result

                result.FileName = request.FilePath;
                result.Length = fileInfo.Length;
                result.FileByteStream = stream;
            }
            catch (Exception ex)
            {
                log.Error("FileService:DownloadFile:: Message: " + ex.Message + " StackTrace: " + ex.StackTrace + " InnerException: " + ex.InnerException + " Source: " + ex.Source);
                throw new Exception(ex.ToString());
            }
            return result;
        }

        public void CreateFreshThumbnail(DownloadRequest request)
        {
            string FileThumbnailPath = Path.Combine(ConfigurationManager.AppSettings["DigiphotoQRClaimImagesPath"], request.FilePath);
            string filePath = FileThumbnailPath.Replace("/Thumbnails", "");
            if (System.IO.File.Exists(filePath))
            {
                using (var input = new Bitmap(filePath))
                {
                    int width;
                    int height;
                    if (input.Width > input.Height)
                    {
                        width = 128;
                        height = 128 * input.Height / input.Width;
                    }
                    else
                    {
                        height = 128;
                        width = 128 * input.Width / input.Height;
                    }
                    using (var thumb = new Bitmap(width, height))
                    using (var graphic = Graphics.FromImage(thumb))
                    {
                        graphic.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
                        graphic.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;
                        graphic.PixelOffsetMode = System.Drawing.Drawing2D.PixelOffsetMode.HighQuality;
                        input.MakeTransparent(Color.Transparent);
                        graphic.Clear(Color.Transparent);
                        graphic.DrawImage(input, 0, 0, width, height);
                        if (!Directory.Exists(Path.GetDirectoryName(FileThumbnailPath)))
                            Directory.CreateDirectory(Path.GetDirectoryName(FileThumbnailPath));
                        {
                            thumb.Save(FileThumbnailPath, System.Drawing.Imaging.ImageFormat.Png);
                        }
                    }
                }
            }

        }
    }
}