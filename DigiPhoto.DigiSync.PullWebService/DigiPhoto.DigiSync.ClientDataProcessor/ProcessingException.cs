﻿using DigiPhoto.DigiSync.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DigiPhoto.DigiSync.ClientDataProcessor
{
    class ProcessingException : BaseException
    {
        public ProcessingException()
            : base()
        {
        }
        public ProcessingException(string message)
            : base(message)
        {
        }
        public ProcessingException(string message, Exception innerException)
            : base(message, innerException)
        {
        }
    }
}
