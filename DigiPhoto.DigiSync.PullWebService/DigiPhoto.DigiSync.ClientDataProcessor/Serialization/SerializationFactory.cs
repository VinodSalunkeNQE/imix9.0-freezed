﻿using DigiPhoto.DigiSync.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DigiPhoto.DigiSync.ClientDataProcessor.Serialization
{
    public class SerializationFactory
    {
        private static Dictionary<long, Serializer> serializers = new Dictionary<long, Serializer>();

        public static Serializer Create(long applicationObjectId)
        {
            try
            {
                if (!serializers.ContainsKey(applicationObjectId))
                {
                    ApplicationObjectEnum applicationObject = (ApplicationObjectEnum)applicationObjectId;
                    string applicationObjectName = applicationObject.ToString();
                    string className = applicationObjectName + "Serializer";
                    Type type = Type.GetType("DigiPhoto.DigiSync.ClientDataProcessor.Serialization." + className);
                    serializers[applicationObjectId] = Activator.CreateInstance(type) as Serializer;
                }
                return serializers[applicationObjectId];
            }
            catch
            {
                throw;
            }
        }
    }
}
