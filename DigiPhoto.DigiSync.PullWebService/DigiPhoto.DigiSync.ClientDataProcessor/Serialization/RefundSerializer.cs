﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DigiPhoto.DigiSync.ClientDataAccess.DataModels;
using DigiPhoto.DigiSync.Model;
using DigiPhoto.DigiSync.Utility;

namespace DigiPhoto.DigiSync.ClientDataProcessor.Serialization
{
    class RefundSerializer : Serializer
    {
        public override string Serialize(long ObjectValueId)
        {
            try
            {
                using (DigiPhotoContext dbContext = new DigiPhotoContext())
                {
                    string dataXML = string.Empty;

                    var Refunddetails = (from R in dbContext.DG_Refund
                                         join RD in dbContext.DG_RefundDetails on R.DG_RefundId equals RD.DG_RefundMaster_ID into erdset
                                         from erd in erdset.DefaultIfEmpty()
                                         where R.DG_RefundId == ObjectValueId
                                         select new { refund = R, refunddetails = (erd == null ? null : erd) }).FirstOrDefault();

                    var users = (from R in dbContext.DG_Refund
                                 join u in dbContext.DG_Users on R.UserId equals u.DG_User_pkey
                                 where R.DG_RefundId == ObjectValueId
                                 select new { refund = R, myusers = u }).FirstOrDefault();
                    var orders = (from R in dbContext.DG_Refund
                                  join o in dbContext.DG_Orders on R.DG_OrderId equals o.DG_Orders_pkey
                                  where R.DG_RefundId == ObjectValueId
                                  select new { refund = R, myorder = o }).FirstOrDefault();

                    List<RefundDetailInfo> refundeatilslst = (from rf in dbContext.DG_RefundDetails
                                                              where rf.DG_RefundMaster_ID == Refunddetails.refund.DG_RefundId
                                                              select new RefundDetailInfo
                                                              {
                                                                  RefundPhotoId = rf.RefundPhotoId,
                                                                  orderdetailId=rf.DG_LineItemId
                                                              }).ToList();

                    List<RefundDetailInfo> rd = new List<RefundDetailInfo>();
                    foreach (RefundDetailInfo rs in refundeatilslst)
                    {
                        RefundDetailInfo refundinfo = new RefundDetailInfo();
                        DG_Orders_Details orderDetails = dbContext.DG_Orders_Details.Where(o => o.DG_Orders_LineItems_pkey == rs.orderdetailId).FirstOrDefault();
                        if (orderDetails != null)
                        {
                            refundinfo.OrderDetailsSyncCode = orderDetails.SyncCode;
                        }
                        refundinfo.RefundPhotoId = rs.RefundPhotoId;
                        rd.Add(refundinfo);
                    }

                    if (Refunddetails != null && refundeatilslst != null)
                    {
                        RefundDetailInfo RefundInfo = new RefundDetailInfo()
                        {
                            OrderId = Convert.ToInt32(Refunddetails.refund.DG_OrderId),
                            RefundAmount = Convert.ToDecimal(Refunddetails.refund.RefundAmount),
                            RefundDate = Convert.ToDateTime(Refunddetails.refund.RefundDate),
                            UserSynccode = users.myusers.SyncCode,
                            OrderSyncCode = orders.myorder.SyncCode,
                            RefundMode = Convert.ToInt32(Refunddetails.refund.Refund_Mode),
                        };
                        if (Refunddetails.refunddetails != null)
                        {
                           
                            RefundInfo.orderdetailId = Convert.ToInt32(Refunddetails.refunddetails.DG_LineItemId);
                            RefundInfo.RefundReason = Refunddetails.refunddetails.RefundReason;
                            RefundInfo.RefundMasterid = Convert.ToInt32(Refunddetails.refunddetails.DG_RefundMaster_ID);
                            RefundInfo.RefundedAmount = Convert.ToDecimal(Refunddetails.refunddetails.Refunded_Amount);
                        }

                        RefundInfo.refundphotolist = rd;

                        dataXML = CommonUtility.SerializeObject<RefundDetailInfo>(RefundInfo);
                    }
                    return dataXML;
                }
            }
            catch (Exception e)
            {
                Exception outerException = new SerializationException("Error occured in Refund Serialization.", e);
                outerException.Source = "Serialization.RefundSerializer";
                throw outerException;
            }
        }
        public override object Deserialize(string input)
        {
            throw new Exception("Not emplemented");
        }
    }
}
