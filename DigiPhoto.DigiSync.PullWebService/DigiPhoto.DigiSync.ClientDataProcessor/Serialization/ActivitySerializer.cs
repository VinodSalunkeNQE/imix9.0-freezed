﻿using DigiPhoto.DigiSync.ClientDataAccess.DataModels;
using DigiPhoto.DigiSync.Model;
using DigiPhoto.DigiSync.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DigiPhoto.DigiSync.ClientDataProcessor.Serialization
{
    public class ActivitySerializer : Serializer
    {
        public override string Serialize(long ObjectValueId)
        {
            try
            {
                string dataXML = string.Empty;
                using (DigiPhotoContext dbContext = new DigiPhotoContext())
                {
                    var activity = dbContext.DG_Activity.Where(r => r.DG_Acitivity_Action_Pkey == ObjectValueId).FirstOrDefault();

                    if (activity != null)
                    {
                        var user = dbContext.DG_Users.Where(r => r.DG_User_pkey == activity.DG_Acitivity_By).FirstOrDefault();
                        if (user == null)
                        {
                            user = dbContext.DG_Users.Where(r => r.DG_User_Name.ToUpper().Equals("ADMIN")).FirstOrDefault();
                        }
                        ActivityInfo activityInfo = new ActivityInfo()
                        {
                            AcitivityId = activity.DG_Acitivity_Action_Pkey,
                            ActionType = activity.DG_Acitivity_ActionType,
                            AcitivityDate = activity.DG_Acitivity_Date,
                            AcitivityBy = activity.DG_Acitivity_By,
                            AcitivityDescrption = activity.DG_Acitivity_Descrption,
                            ReferenceID = activity.DG_Reference_ID,
                            SyncCode = activity.SyncCode,
                            UserSyncCode = user.SyncCode
                        };
                        dataXML = CommonUtility.SerializeObject<ActivityInfo>(activityInfo);
                    }
                }
                return dataXML;
            }
            catch (Exception e)
            {
                Exception outerException = new SerializationException("Error occured in Activity Serialization.", e);
                outerException.Source = "Serialization.ActivitySerializer";
                throw outerException;
            }

        }

        public override object Deserialize(string input)
        {
            throw new Exception("Not emplemented");
        }
    }
}
