﻿namespace DigiPhoto.DigiSync.ClientDataAccess.DataModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class iMIXconfigurationLocationValue
    {
        public long IMIXConfigurationMasterId { get; set; }

        public string ConfigurationValue { get; set; }

        public int LocationId { get; set; }

        public int SubstoreId { get; set; }
    }
}
