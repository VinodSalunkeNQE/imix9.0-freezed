namespace DigiPhoto.DigiSync.ClientDataAccess.DataModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class DG_PhotoGroup
    {
        [Key]
        public long DG_Group_pkey { get; set; }

        [Required]
        [StringLength(100)]
        public string DG_Group_Name { get; set; }

        public int? DG_Photo_ID { get; set; }

        [StringLength(100)]
        public string DG_Photo_RFID { get; set; }

        public DateTime? DG_CreatedDate { get; set; }

        public int? DG_SubstoreId { get; set; }
    }
}
