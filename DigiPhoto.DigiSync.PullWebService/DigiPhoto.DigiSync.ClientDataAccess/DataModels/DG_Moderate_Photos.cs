namespace DigiPhoto.DigiSync.ClientDataAccess.DataModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class DG_Moderate_Photos
    {
        [Key]
        public int DG_Mod_Photo_pkey { get; set; }

        public int DG_Mod_Photo_ID { get; set; }

        public DateTime DG_Mod_Date { get; set; }

        public int DG_Mod_User_ID { get; set; }
    }
}
