namespace DigiPhoto.DigiSync.ClientDataAccess.DataModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class DG_Configuration
    {
        [Key]
        public int DG_Config_pkey { get; set; }

        [StringLength(500)]
        public string DG_Hot_Folder_Path { get; set; }

        [StringLength(500)]
        public string DG_Frame_Path { get; set; }

        [StringLength(500)]
        public string DG_BG_Path { get; set; }

        [StringLength(20)]
        public string DG_Mod_Password { get; set; }

        public int? DG_NoOfPhotos { get; set; }

        public bool? DG_Watermark { get; set; }

        public bool? DG_SemiOrder { get; set; }

        public bool? DG_HighResolution { get; set; }

        public bool? DG_AllowDiscount { get; set; }

        public bool? DG_EnableDiscountOnTotal { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? WiFiStartingNumber { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? FolderStartingNumber { get; set; }

        public bool? IsAutoLock { get; set; }

        public bool? DG_SemiOrderMain { get; set; }

        public bool? PosOnOff { get; set; }

        [StringLength(200)]
        public string DG_ReceiptPrinter { get; set; }

        public bool? DG_IsAutoRotate { get; set; }

        [StringLength(500)]
        public string DG_Graphics_Path { get; set; }

        public bool? DG_IsCompression { get; set; }

        public bool? DG_IsEnableGroup { get; set; }

        public int? DG_Substore_Id { get; set; }

        public int? DG_NoOfBillReceipt { get; set; }

        [StringLength(10)]
        public string DG_ChromaColor { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? DG_ChromaTolerance { get; set; }

        [StringLength(300)]
        public string DG_DbBackupPath { get; set; }

        [StringLength(500)]
        public string DG_CleanupTables { get; set; }

        [StringLength(300)]
        public string DG_HfBackupPath { get; set; }

        [StringLength(200)]
        public string DG_ScheduleBackup { get; set; }

        public bool? DG_IsBackupScheduled { get; set; }

        public double? DG_Brightness { get; set; }

        public double? DG_Contrast { get; set; }

        public int? DG_PageCountGrid { get; set; }

        public int? DG_PageCountPreview { get; set; }

        public int? DG_NoOfPhotoIdSearch { get; set; }

        public bool? IsRecursive { get; set; }

        public int? IntervalCount { get; set; }

        public int? intervalType { get; set; }

        [StringLength(500)]
        public string DG_MktImgPath { get; set; }

        public int? DG_MktImgTimeInSec { get; set; }

        [StringLength(500)]
        public string EK_SampleImagePath { get; set; }

        public int? EK_DisplayDuration { get; set; }

        public int? EK_ScreenStartTime { get; set; }

        public bool? EK_IsScreenSaverActive { get; set; }

        public bool? IsDeleteFromUSB { get; set; }

        public int? DG_CleanUpDaysBackUp { get; set; }

        [StringLength(50)]
        public string FtpIP { get; set; }

        [StringLength(50)]
        public string FtpUid { get; set; }

        [StringLength(50)]
        public string FtpPwd { get; set; }

        [StringLength(50)]
        public string FtpFolder { get; set; }
        public bool IsSynced { get; set; }
    }
}
