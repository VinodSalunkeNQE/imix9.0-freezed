namespace DigiPhoto.DigiSync.ClientDataAccess.DataModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class DG_Orders
    {
        [Key]
        public int DG_Orders_pkey { get; set; }

        [StringLength(250)]
        public string DG_Orders_Number { get; set; }

        public DateTime? DG_Orders_Date { get; set; }

        public int? DG_Albums_ID { get; set; }

        [StringLength(1)]
        public string DG_Order_Mode { get; set; }

        public int? DG_Orders_UserID { get; set; }

        [Column(TypeName = "money")]
        public decimal? DG_Orders_Cost { get; set; }

        [Column(TypeName = "money")]
        public decimal? DG_Orders_NetCost { get; set; }

        public int? DG_Orders_Currency_ID { get; set; }

        public string DG_Orders_Currency_Conversion_Rate { get; set; }

        public double? DG_Orders_Total_Discount { get; set; }

        public string DG_Orders_Total_Discount_Details { get; set; }

        public int? DG_Orders_PaymentMode { get; set; }

        public string DG_Orders_PaymentDetails { get; set; }

        public bool? DG_Orders_Canceled { get; set; }

        public DateTime? DG_Orders_Canceled_Date { get; set; }

        public string DG_Orders_Canceled_Reason { get; set; }

        public string SyncCode { get; set; }
        public string PosName { get; set; }
    }
}
