namespace DigiPhoto.DigiSync.ClientDataAccess.DataModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class DG_Store
    {
        [Key]
        public int DG_Store_pkey { get; set; }

        [Required]
        [StringLength(150)]
        public string DG_Store_Name { get; set; }

        [Required]
        [StringLength(150)]
        public string Country { get; set; }

        [StringLength(50)]
        public string DG_CentralServerIP { get; set; }

        public Guid? DG_StoreCode { get; set; }

        [StringLength(50)]
        public string DG_CenetralServerUName { get; set; }

        [StringLength(50)]
        public string DG_CenetralServerPassword { get; set; }

        public decimal? DG_PreferredTimeToSyncFrom { get; set; }

        public decimal? DG_PreferredTimeToSyncTo { get; set; }

        public string StoreCode { get; set; }

        public string CountryCode { get; set; }
        public bool IsOnLine { get; set; }
    }
}
