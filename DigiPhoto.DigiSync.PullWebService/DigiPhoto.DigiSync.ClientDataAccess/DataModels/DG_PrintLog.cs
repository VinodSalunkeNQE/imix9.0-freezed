namespace DigiPhoto.DigiSync.ClientDataAccess.DataModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class DG_PrintLog
    {
        public int ID { get; set; }

        public int? PhotoId { get; set; }

        public DateTime? PrintTime { get; set; }

        public int? ProductTypeId { get; set; }

        public int? UserID { get; set; }
    }
}
