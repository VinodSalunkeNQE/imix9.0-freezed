﻿namespace DigiPhoto.DigiSync.DataAccess.DataModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ChangeTrackingDownLoadHistory")]
    public partial class ChangeTrackingDownLoadHistory
    {
        [Required]
        public long ChangeTrackingDownLoadHistoryId { get; set; }
        [Required]
        public long ChangeTrackingId { get; set; }
        [Required]
        public long StoreId { get; set; }
        [Required]
        public string SiteSyncCode { get; set; }
        [Required]
        public long ApplicationObjectId { get; set; }
        [Required]
        public long ObjectValueId { get; set; }
        [Required]
        public Int32 ChangeAction { get; set; }
        [Required]
        public DateTime CreatedDate { get; set; }
        [Required]
        public long CreatedBy { get; set; }
        [Required]
        public DateTime ModifiedDate { get; set; }
        [Required]
        public string EntityCode { get; set; }
        [Required]
        public Int32 ProcessingStatus { get; set; }
    }
}
