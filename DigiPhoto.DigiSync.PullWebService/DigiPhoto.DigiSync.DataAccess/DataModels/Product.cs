namespace DigiPhoto.DigiSync.DataAccess.DataModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Product")]
    public partial class Product
    {
        public Product()
        {
            Backgrounds = new HashSet<Background>();
            Borders = new HashSet<Border>();
            PackageProducts = new HashSet<PackageProduct>();
            ProductPricings = new HashSet<ProductPricing>();
        }

        public long ProductId { get; set; }

        [Required]
        [StringLength(50)]
        public string Name { get; set; }

        [StringLength(500)]
        public string Description { get; set; }

        [Required]
        [StringLength(100)]
        public string Code { get; set; }

        public double? Price { get; set; }

        public int? CurrencyID { get; set; }

        [StringLength(50)]
        public string ProductNumber { get; set; }

        [Required]
        [StringLength(500)]
        public string ImagePath { get; set; }

        public int MaximumQuantity { get; set; }

        public bool ?IsBundled { get; set; }

        public bool? IsDiscountApplied { get; set; }

        public bool? IsAccessory { get; set; }

        public bool? IsPrimary { get; set; }

        public bool ?IsActive { get; set; }

        public DateTime CreatedDateTime { get; set; }

        public DateTime? ModifiedDateTime { get; set; }

        public long CreatedBy { get; set; }

        public long? ModifiedBy { get; set; }

        [StringLength(50)]
        public string SyncCode { get; set; }
        public bool IsSynced { get; set; }
        //[StringLength(50)]
        //public string UserSyncCode { get; set; }

        public virtual ICollection<Background> Backgrounds { get; set; }

        public virtual ICollection<Border> Borders { get; set; }

        public virtual Currency Currency { get; set; }

        public virtual ICollection<PackageProduct> PackageProducts { get; set; }

        public virtual ICollection<ProductPricing> ProductPricings { get; set; }

        public virtual User User { get; set; }

        public virtual User User1 { get; set; }
        public bool? IsPersonalizedAR { get; set; }////latika for AR Personalised changed
    }
}
