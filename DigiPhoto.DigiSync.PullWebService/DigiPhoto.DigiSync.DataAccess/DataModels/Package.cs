namespace DigiPhoto.DigiSync.DataAccess.DataModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Package")]
    public partial class Package
    {
        public Package()
        {
            PackageProducts = new HashSet<PackageProduct>();
        }

        public long PackageId { get; set; }

        [Required]
        [StringLength(1000)]
        public string Name { get; set; }

        [StringLength(1000)]
        public string Description { get; set; }

        [Required]
        [StringLength(500)]
        public string Code { get; set; }

        public double Price { get; set; }

        public int? CurrencyID { get; set; }

        public bool? IsDiscountApplied { get; set; }

        public bool IsActive { get; set; }

        public DateTime CreatedDateTime { get; set; }

        public DateTime? ModifiedDateTime { get; set; }

        public long CreatedBy { get; set; }

        public long? ModifiedBy { get; set; }

        [StringLength(50)]
        public string SyncCode { get; set; }
        public bool IsSynced { get; set; }
        //public string UserSyncCode { get; set; }
        public virtual Currency Currency { get; set; }

        public virtual ICollection<PackageProduct> PackageProducts { get; set; }
        public bool? IsPersonalizedAR { get; set; }////latika for AR Personalised changed

    }
}
