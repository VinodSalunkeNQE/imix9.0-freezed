namespace DigiPhoto.DigiSync.DataAccess.DataModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("OrderDetail")]
    public partial class OrderDetail
    {
        public OrderDetail()
        {
            ProductRefundDetails = new HashSet<ProductRefundDetail>();
        }

        public long OrderDetailId { get; set; }

        public long OrderMasterId { get; set; }

        //public long DigiImagesId { get; set; }

        public long? DiscountTypeId { get; set; }
        public string DiscountDetails { get; set; }

        // public long SubStoreId { get; set; }

        public long? PrinterReferenceId { get; set; }

        public long? ProductId { get; set; }

        public long? PackageId { get; set; }

        [Column(TypeName = "money")]
        public decimal? DiscountAmount { get; set; }

        [Column(TypeName = "money")]
        public decimal? UnitPrice { get; set; }

        public int? Quantity { get; set; }

        [Column(TypeName = "money")]
        public decimal? TotalCost { get; set; }

        [Column(TypeName = "money")]
        public decimal? NetPrice { get; set; }

        public bool IsBurned { get; set; }

        public DateTime CreatedDateTime { get; set; }

        //public virtual DigiImage DigiImage { get; set; }

        //public virtual DigiMasterLocation DigiMasterLocation { get; set; }

        public virtual OrderMaster OrderMaster { get; set; }

        public virtual ICollection<ProductRefundDetail> ProductRefundDetails { get; set; }

        public string PhotoId { get; set; }

        public string PhotoIdUnSold { get; set; }
        public long? LineItemParentId { get; set; }
        public string SyncCode { get; set; }

        public Int32 OrderProcessStatus { get; set; }
    }
}
