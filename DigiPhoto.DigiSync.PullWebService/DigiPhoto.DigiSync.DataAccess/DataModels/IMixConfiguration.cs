namespace DigiPhoto.DigiSync.DataAccess.DataModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("IMixConfiguration")]
    public partial class IMixConfiguration
    {
        public IMixConfiguration()
        {
            IMixConfigurationValues = new HashSet<IMixConfigurationValue>();
        }

        [Key]
        public int ConfigurationId { get; set; }

        [Required]
        [StringLength(50)]
        public string Name { get; set; }

        public bool IsActive { get; set; }

        public int? OrderBy { get; set; }

        public virtual ICollection<IMixConfigurationValue> IMixConfigurationValues { get; set; }
    }
}
