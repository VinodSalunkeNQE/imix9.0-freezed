namespace DigiPhoto.DigiSync.DataAccess.DataModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ProductRefundDetail")]
    public partial class ProductRefundDetail
    {
        public long ProductRefundDetailId { get; set; }

        public long ProductRefundId { get; set; }

        public long OrderDetailId { get; set; }

        [StringLength(250)]
        public string RefundDigiImageId { get; set; }

        [Column(TypeName = "money")]
        public decimal? Amount { get; set; }

        [StringLength(2000)]
        public string Reason { get; set; }

        public virtual OrderDetail OrderDetail { get; set; }

        public virtual ProductRefund ProductRefund { get; set; }
    }
}
