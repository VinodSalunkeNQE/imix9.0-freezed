namespace DigiPhoto.DigiSync.DataAccess.DataModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("OrderMaster")]
    public partial class OrderMaster
    {
        public OrderMaster()
        {
            OrderDetails = new HashSet<OrderDetail>();
            ProductRefunds = new HashSet<ProductRefund>();
        }

        public long OrderMasterId { get; set; }

        [Required]
        [StringLength(250)]
        public string OrderNumber { get; set; }

        public DateTime OrderDate { get; set; }

        public int AlbumId { get; set; }

        [StringLength(1)]
        public string OrderMode { get; set; }

        [Column(TypeName = "money")]
        public decimal? Cost { get; set; }

        [Column(TypeName = "money")]
        public decimal? NetCost { get; set; }

        public int? CurrencyId { get; set; }

        public string CurrencyConversionRate { get; set; }

        public double? TotalDiscount { get; set; }

        public string TotalDiscountDetails { get; set; }

        public long? PaymentMode { get; set; }

        public string PaymentDetails { get; set; }

        public bool? IsCancelled { get; set; }

        public DateTime? CancelledDateTime { get; set; }

        [StringLength(2000)]
        public string Reason { get; set; }

        public long OrderCreatedBy { get; set; }

        public virtual ICollection<OrderDetail> OrderDetails { get; set; }

        public virtual ICollection<ProductRefund> ProductRefunds { get; set; }

        public long SubStoreId { get; set; }

        public string SyncCode { get; set; }
        public string PosName { get; set; }
        public bool IsSynced { get; set; }
    }
}
