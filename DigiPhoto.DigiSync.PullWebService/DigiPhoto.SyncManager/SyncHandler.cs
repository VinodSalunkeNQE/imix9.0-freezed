﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using log4net;
using DigiPhoto.DigiSync.Business;
using DigiPhoto.DigiSync.Model;
using DigiPhoto.DigiSync.Business.Processing;
namespace DigiPhoto.SyncManager
{
    class SyncHandler
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(Program));

        private static SyncHandler SyncHandlerObject = null;

        private SyncHandler()
        {
        }

        public static SyncHandler GetObject()
        {
            if (SyncHandlerObject == null)
            {
                SyncHandlerObject = new SyncHandler();
                return SyncHandlerObject;
            }
            else
            {
                //only one client can get access to server
                return null;
            }
        }

        public void ProcessIncomingChange()
        {
            try
            {
                //IncomingChangeInfo incomingChange = SyncController.GetNextIncomingChange();
                 List<IncomingChangeInfo> incomingChangeList = SyncController.GetNextIncomingChange();
                 if (incomingChangeList != null)
                 {

                         foreach (IncomingChangeInfo incomingChange in incomingChangeList)
                         {
                             try
                             {
                                 Console.WriteLine(DateTime.Now.ToString("dd/MMM/yyyy H:mm:ss") + " - Processing started   for Application Object Id :" + incomingChange.ApplicationObjectId + " IncomingChangeId=>" + incomingChange.IncomingChangeId + ", Entity Code=>" + incomingChange.EntityCode);

                                 if (log.IsInfoEnabled)
                                     log.Info("Processing started for Application Object Id :" + incomingChange.ApplicationObjectId + " IncomingChangeId=>" + incomingChange.IncomingChangeId + ", Entity Code=>" + incomingChange.EntityCode);

                                 if (!string.IsNullOrEmpty(incomingChange.DataXML))
                                 {
                                     if (log.IsInfoEnabled)
                                     {
                                         Console.WriteLine("DataXML=>");
                                         Console.WriteLine(incomingChange.DataXML.Replace("\n", Environment.NewLine));
                                     }
                                 }
                                 //Process
                                 IProcesser processor = ProcessorFactory.Create(incomingChange.ApplicationObjectId);
                                 processor.Process(incomingChange);
                                 Console.WriteLine(DateTime.Now.ToString("dd/MMM/yyyy H:mm:ss") + " - Processing completed for Application Object Id :" + incomingChange.ApplicationObjectId + " IncomingChangeId=>" + incomingChange.IncomingChangeId + ", Entity Code=>" + incomingChange.EntityCode);
                                
                                 if (log.IsInfoEnabled)
                                     log.Info("Processing completed for Application Object Id:" + incomingChange.ApplicationObjectId + " IncomingChangeId=>" + incomingChange.IncomingChangeId + ", Entity Code=>" + incomingChange.EntityCode);
                             }
                             catch(Exception ex)
                             {
                                 Console.WriteLine("EXCEPTION " + DateTime.Now.ToString("dd/MMM/yyyy H:mm:ss") + " - " + ex.Message);
                                 log.Error(ex.Message);
                             }

                         }
                     }
                 
               
            }
            catch (Exception ex)
            {
                Console.WriteLine("EXCEPTION " + DateTime.Now.ToString("dd/MMM/yyyy H:mm:ss") + " - " + ex.Message);
                log.Error(ex.Message);
            }
            finally
            {
                SyncHandlerObject = null;
            }
        }

    }
}
