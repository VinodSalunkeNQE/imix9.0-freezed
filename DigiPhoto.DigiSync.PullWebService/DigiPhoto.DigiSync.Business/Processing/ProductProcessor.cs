﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DigiPhoto.DigiSync.DataAccess.DataModels;
using DigiPhoto.DigiSync.Model;
using DigiPhoto.DigiSync.Utility;
using log4net;
namespace DigiPhoto.DigiSync.Business.Processing
{
    class ProductProcessor : BaseProcessor
    {
        private static ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        protected override void ProcessInsert()
        {
            try
            {
                if (!string.IsNullOrEmpty(ChangeInfo.DataXML))
                {
                    ProductInfo productInfo = CommonUtility.DeserializeXML<ProductInfo>(ChangeInfo.DataXML);
                    using (DigiPhotoWebContext dbContext = new DigiPhotoWebContext())
                    {
                        var digiStore = dbContext.DigiMasterLocations.Where(r => r.Name == ChangeInfo.StoreName && r.Level == 3).FirstOrDefault();
                        int existingProduct = dbContext.Products.Count(r => r.SyncCode == productInfo.SyncCode);

                        var digilocation = dbContext.DigiMasterLocations.FirstOrDefault();

                        if (productInfo.LocationSyncCode == null)
                        {
                             digilocation = dbContext.DigiMasterLocations.Where(r => r.Name == ChangeInfo.StoreName && r.Level == 3).FirstOrDefault();
                        }
                        else
                        {
                             digilocation = dbContext.DigiMasterLocations.Where(r => r.SyncCode == productInfo.LocationSyncCode && r.Level == 4).FirstOrDefault();
                        }
                      
                        
                        if (existingProduct == 0)
                        {
                            var product = new Product();
                            if (productInfo.IsPackage == false)
                            {
                                product.Name = productInfo.Name;
                                product.Description = productInfo.Description;
                                product.Code = productInfo.Code;
                                product.Price = productInfo.Price;
                                product.ImagePath = productInfo.ImagePath;
                                product.ProductNumber = productInfo.ProductNumber;
                                // product.CurrencyID = 74;
                                product.IsAccessory = productInfo.IsAccessory;
                                product.IsActive = productInfo.IsActive;
                                product.IsDiscountApplied = productInfo.IsDiscountApplied;
                                product.IsPrimary = productInfo.IsPrimary;
                                product.SyncCode = productInfo.SyncCode;
                                product.IsBundled = productInfo.IsBundled;
                                product.CreatedDateTime = System.DateTime.Now;
                                product.IsPersonalizedAR = productInfo.IsPersonalizedAR;////latika for AR Personalised changed
                                var user = dbContext.Users.Where(r => r.SyncCode == productInfo.UserSyncCode).FirstOrDefault();
                                if (user != null)
                                {
                                    product.CreatedBy = Convert.ToInt64(user.UserId);
                                }
                                product.IsSynced = true;
                                dbContext.Products.Add(product);
                                dbContext.SaveChanges();

                                long ProductId = product.ProductId;
                                if (ProductId > 0)
                                {

                                    var objectauth = dbContext.ObjectAuthorizations.Where(r => r.ObjectValueId == ProductId && r.DigiMasterLocationId == digilocation.DigiMasterLocationId && r.ApplicationObjectId == ChangeInfo.ApplicationObjectId).FirstOrDefault();

                                    if (objectauth != null)
                                    {
                                        dbContext.ObjectAuthorizations.Remove(objectauth);
                                        dbContext.SaveChanges();

                                    }
                                   
                                        ObjectAuthorization obj = new ObjectAuthorization();
                                        obj.ApplicationObjectId = ChangeInfo.ApplicationObjectId;
                                        obj.ObjectValueId = ProductId;
                                        obj.DigiMasterLocationId = digilocation.DigiMasterLocationId;
                                        dbContext.ObjectAuthorizations.Add(obj);
                                        dbContext.SaveChanges();
                                  
                                                                          

                                }

                            }


                        }

                        int existingPackage = dbContext.Packages.Count(r => r.SyncCode == productInfo.SyncCode);
                        var digilocations = dbContext.DigiMasterLocations.FirstOrDefault();

                        if (productInfo.LocationSyncCode == null)
                        {
                            digilocations = dbContext.DigiMasterLocations.Where(r => r.Name == ChangeInfo.StoreName && r.Level == 3).FirstOrDefault();
                        }
                        else
                        {
                            digilocations = dbContext.DigiMasterLocations.Where(r => r.SyncCode == productInfo.LocationSyncCode && r.Level == 4).FirstOrDefault();
                        }
                       // var digilocations = dbContext.DigiMasterLocations.Where(r => r.SyncCode == productInfo.LocationSyncCode && r.Level == 4).FirstOrDefault();
                        if (existingPackage == 0 && productInfo.IsPackage == true)
                        {

                            var package = new Package();
                            package.Name = productInfo.Name;
                            package.Description = productInfo.Description;
                            package.Code = productInfo.Code;
                            package.Price = Convert.ToDouble(productInfo.Price);
                            //package.CurrencyID = 1;
                            package.IsDiscountApplied = productInfo.IsDiscountApplied;
                            package.IsActive = Convert.ToBoolean(productInfo.IsActive);
                            package.SyncCode = productInfo.SyncCode;
                            package.IsPersonalizedAR = productInfo.IsPersonalizedAR;////latika for AR Personalised changed
                            var user = dbContext.Users.Where(r => r.SyncCode == productInfo.UserSyncCode).FirstOrDefault();
                            if (user != null)
                            {
                                package.CreatedBy = user.UserId;
                            }
                          
                            package.CreatedDateTime = Convert.ToDateTime(productInfo.CreatedDateTime);
                            package.IsSynced = true;
                            dbContext.Packages.Add(package);
                            dbContext.SaveChanges();




                            //adding data to packagedetails table
                            //  PackageProduct packagedetails = new PackageProduct();
                            long productid = 0;
                            long packageid = 0;
                            foreach (PackageProductInfo p in productInfo.packageProductInfoList)
                            {

                                if (dbContext.Packages.Where(r => r.SyncCode == productInfo.SyncCode).FirstOrDefault() != null)
                                    packageid = dbContext.Packages.Where(r => r.SyncCode == productInfo.SyncCode).FirstOrDefault().PackageId;

                                if (dbContext.Products.Where(r => r.SyncCode == p.ProductSyncCode).FirstOrDefault() != null)
                                    productid = dbContext.Products.Where(r => r.SyncCode == p.ProductSyncCode).ToList().FirstOrDefault().ProductId;
                                if (productid > 0 && packageid > 0)
                                {
                                    PackageProduct packagedetails = new PackageProduct()
                                    {
                                        ProductMaxImage = p.ProductMaxImage,
                                        ProductQuantity = p.ProductQuantity,
                                        PackageId = packageid,
                                        ProductId = productid,
                                    };

                                    dbContext.PackageProducts.Add(packagedetails);
                                    dbContext.SaveChanges();
                                    productid = 0;
                                }
                            }

                            long PackageId = package.PackageId;
                            if (PackageId > 0)
                            {

                                var objectauth = dbContext.ObjectAuthorizations.Where(r => r.ObjectValueId == PackageId && r.DigiMasterLocationId == digilocation.DigiMasterLocationId && r.ApplicationObjectId == 5).FirstOrDefault();

                                if (objectauth != null)
                                {
                                    dbContext.ObjectAuthorizations.Remove(objectauth);
                                    dbContext.SaveChanges();

                                }
                                ObjectAuthorization obj = new ObjectAuthorization();
                                obj.ApplicationObjectId = 5;
                                obj.ObjectValueId = PackageId;
                                obj.DigiMasterLocationId = digilocations.DigiMasterLocationId;
                                dbContext.ObjectAuthorizations.Add(obj);
                                dbContext.SaveChanges();

                            }




                        }

                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("ProductProcessor:ProcessInsert:: Message: " + ex.Message + " StackTrace: " + ex.StackTrace + " InnerException: " + ex.InnerException + " Source: " + ex.Source);
                Exception outerException = new ProcessingException("Error occured in Product Processor Insert.", ex);
                outerException.Source = "Business.Processing.ProductProcessor";
                throw outerException;
            }
        }

        protected override void ProcessUpdate()
        {
            try
            {
                if (!string.IsNullOrEmpty(ChangeInfo.DataXML))
                {
                    ProductInfo productInfo = CommonUtility.DeserializeXML<ProductInfo>(ChangeInfo.DataXML);
                    using (DigiPhotoWebContext dbContext = new DigiPhotoWebContext())
                    {
                        var dbProduct = dbContext.Products.Where(r => r.SyncCode == productInfo.SyncCode && productInfo.IsPackage == false).FirstOrDefault();

                        if (dbProduct != null && productInfo.IsPackage == false)
                        {
                            //Update Product Details
                            dbProduct.Name = productInfo.Name;
                            dbProduct.Description = productInfo.Description;
                           // dbProduct.Code = productInfo.Code;
                            dbProduct.Price = productInfo.Price;
                            dbProduct.ImagePath = productInfo.ImagePath;
                            dbProduct.ProductNumber = productInfo.ProductNumber;
                            // dbProduct.CurrencyID = productInfo.CurrencyID;
                            dbProduct.IsAccessory = productInfo.IsAccessory;
                            dbProduct.IsActive = productInfo.IsActive;
                            dbProduct.IsDiscountApplied = productInfo.IsDiscountApplied;
                            dbProduct.IsPrimary = productInfo.IsPrimary;
                            dbProduct.SyncCode = productInfo.SyncCode;
                            dbProduct.IsBundled = productInfo.IsBundled;
                            dbProduct.IsPersonalizedAR = productInfo.IsPersonalizedAR;////latika for AR Personalised changed
                            var user = dbContext.Users.Where(r => r.SyncCode == productInfo.UserSyncCode).FirstOrDefault();
                            if (user != null)
                            {
                                dbProduct.CreatedBy = Convert.ToInt64(user.UserId);

                            }
                            dbProduct.IsSynced = true;
                            dbContext.SaveChanges();

                            //-----------------Saurabh ---------------------------------
                            long productid = dbProduct.ProductId;
                            var digilocation = dbContext.DigiMasterLocations.FirstOrDefault();

                            if (productInfo.LocationSyncCode == null)
                            {
                                digilocation = dbContext.DigiMasterLocations.Where(r => r.Name == ChangeInfo.StoreName && r.Level == 3).FirstOrDefault();
                            }
                            else
                            {
                                digilocation = dbContext.DigiMasterLocations.Where(r => r.SyncCode == productInfo.LocationSyncCode && r.Level == 4).FirstOrDefault();
                            }
                            //var digilocation = dbContext.DigiMasterLocations.Where(r => r.Name == ChangeInfo.StoreName && r.Level == 3).FirstOrDefault();
                           // var digilocation = dbContext.DigiMasterLocations.Where(r => r.SyncCode == productInfo.LocationSyncCode && r.Level == 4).FirstOrDefault();
                            if (productid > 0)
                            {

                                var objectauth = dbContext.ObjectAuthorizations.Where(r => r.ObjectValueId == productid && r.DigiMasterLocationId == digilocation.DigiMasterLocationId && r.ApplicationObjectId == ChangeInfo.ApplicationObjectId).FirstOrDefault();

                                if (objectauth != null)
                                {
                                    dbContext.ObjectAuthorizations.Remove(objectauth);
                                    dbContext.SaveChanges();

                                }
                                ObjectAuthorization obj = new ObjectAuthorization();
                                obj.ApplicationObjectId = ChangeInfo.ApplicationObjectId;
                                obj.ObjectValueId = productid;
                                obj.DigiMasterLocationId = digilocation.DigiMasterLocationId;
                                // obj.CreatedOn = DateTime.Today.Date;
                                dbContext.ObjectAuthorizations.Add(obj);
                                dbContext.SaveChanges();

                            }
                            //-----------------Saurabh ---------------------------------

                        }

                        else
                        {

                            var Products = dbContext.Products.Where(r => r.Name == productInfo.Name && productInfo.IsPackage == false).FirstOrDefault();
                            if (Products == null &&productInfo.IsPackage==false)
                            {

                                dbProduct = new Product();
                                dbProduct.Name = productInfo.Name;
                                dbProduct.Description = productInfo.Description;
                              //  dbProduct.Code = productInfo.Code;
                                dbProduct.Price = productInfo.Price;
                                dbProduct.ImagePath = productInfo.ImagePath;
                                dbProduct.ProductNumber = productInfo.ProductNumber;
                                // dbProduct.CurrencyID = productInfo.CurrencyID;
                                dbProduct.IsAccessory = productInfo.IsAccessory;
                                dbProduct.IsActive = productInfo.IsActive;
                                dbProduct.IsDiscountApplied = productInfo.IsDiscountApplied;
                                dbProduct.IsPrimary = productInfo.IsPrimary;
                                dbProduct.SyncCode = productInfo.SyncCode;
                                dbProduct.IsBundled = productInfo.IsBundled;
                                dbProduct.IsPersonalizedAR = productInfo.IsPersonalizedAR;////latika for AR Personalised changed
                                dbProduct.CreatedDateTime = System.DateTime.Now;
                                var user = dbContext.Users.Where(r => r.SyncCode == productInfo.UserSyncCode).FirstOrDefault();
                                if (user != null)
                                {
                                    dbProduct.CreatedBy = Convert.ToInt64(user.UserId);
                                }
                               
                                dbContext.Products.Add(dbProduct);
                                dbContext.SaveChanges();

                                long productid = dbProduct.ProductId;

                                var digilocation = dbContext.DigiMasterLocations.FirstOrDefault();

                                if (productInfo.LocationSyncCode == null)
                                {
                                    digilocation = dbContext.DigiMasterLocations.Where(r => r.Name == ChangeInfo.StoreName && r.Level == 3).FirstOrDefault();
                                }
                                else
                                {
                                    digilocation = dbContext.DigiMasterLocations.Where(r => r.SyncCode == productInfo.LocationSyncCode && r.Level == 4).FirstOrDefault();
                                }
                                //var digilocation = dbContext.DigiMasterLocations.Where(r => r.Name == ChangeInfo.StoreName && r.Level == 3).FirstOrDefault();
                               // var digilocation = dbContext.DigiMasterLocations.Where(r => r.SyncCode == productInfo.LocationSyncCode && r.Level == 4).FirstOrDefault();
                                if (productid > 0)
                                {

                                    var objectauth = dbContext.ObjectAuthorizations.Where(r => r.ObjectValueId == productid && r.DigiMasterLocationId == digilocation.DigiMasterLocationId && r.ApplicationObjectId == ChangeInfo.ApplicationObjectId).FirstOrDefault();

                                    if (objectauth != null)
                                    {
                                        dbContext.ObjectAuthorizations.Remove(objectauth);
                                        dbContext.SaveChanges();

                                    }
                                    ObjectAuthorization obj = new ObjectAuthorization();
                                    obj.ApplicationObjectId = ChangeInfo.ApplicationObjectId;
                                    obj.ObjectValueId = productid;
                                    obj.DigiMasterLocationId = digilocation.DigiMasterLocationId;
                                    dbContext.ObjectAuthorizations.Add(obj);
                                    dbContext.SaveChanges();

                                }

                            }
                        }


                        var dbPackage = dbContext.Packages.Where(r => r.SyncCode == productInfo.SyncCode).FirstOrDefault();

                        if (dbPackage != null && productInfo.IsPackage == true)
                        {

                            dbPackage.Name = productInfo.Name;
                            dbPackage.Description = productInfo.Description;
                          //  dbPackage.Code = productInfo.Code;
                            dbPackage.Price = Convert.ToDouble(productInfo.Price);
                            //dbPackage.CurrencyID = 1;
                            dbPackage.IsDiscountApplied = productInfo.IsDiscountApplied;
                            dbPackage.IsActive = Convert.ToBoolean(productInfo.IsActive);
                            dbPackage.SyncCode = productInfo.SyncCode;
                            dbPackage.IsPersonalizedAR = productInfo.IsPersonalizedAR;////latika for AR Personalised changed
                            var user = dbContext.Users.Where(r => r.SyncCode == productInfo.UserSyncCode).FirstOrDefault();
                            if (user != null)
                            {
                                dbPackage.CreatedBy = user.UserId;
                            }
                        
                            dbPackage.CreatedDateTime = Convert.ToDateTime(productInfo.CreatedDateTime);

                            dbContext.SaveChanges();

                            //------------------------Saurabh--------------------------

                            if (dbPackage.PackageId > 0)
                            {
                                long packID = dbPackage.PackageId;
                                var objectauth = dbContext.ObjectAuthorizations.Where(r => r.ObjectValueId == dbPackage.PackageId &&  r.ApplicationObjectId == 5).FirstOrDefault();

                                if (objectauth != null)
                                {
                                    dbContext.ObjectAuthorizations.Remove(objectauth);
                                    dbContext.SaveChanges();

                                }
                                var digilocation = dbContext.DigiMasterLocations.FirstOrDefault();

                                if (productInfo.LocationSyncCode == null)
                                {
                                    digilocation = dbContext.DigiMasterLocations.Where(r => r.Name == ChangeInfo.StoreName && r.Level == 3).FirstOrDefault();
                                }
                                else
                                {
                                    digilocation = dbContext.DigiMasterLocations.Where(r => r.SyncCode == productInfo.LocationSyncCode && r.Level == 4).FirstOrDefault();
                                }

                                //var digilocatn = dbContext.DigiMasterLocations.Where(r => r.SyncCode == productInfo.LocationSyncCode && r.Level == 4).FirstOrDefault();
                                ObjectAuthorization obj = new ObjectAuthorization();
                                obj.ApplicationObjectId = 5;
                                obj.ObjectValueId = dbPackage.PackageId;
                                obj.DigiMasterLocationId = digilocation.DigiMasterLocationId;
                                //obj.CreatedOn = DateTime.Today.Date;
                                dbContext.ObjectAuthorizations.Add(obj);
                                dbContext.SaveChanges();

                            }


                             //------------------------Saurabh--------------------------




                            PackageProduct packagedetails = new PackageProduct();

                            long productid = 0;
                            long packageid = 0;
                            int cnt = dbContext.PackageProducts.Count(p => p.PackageId == dbPackage.PackageId);
                            for (int i = 0; i <= cnt - 1; i++)
                            {
                                var dbPackageDetails = dbContext.PackageProducts.Where(r => r.PackageId == dbPackage.PackageId).FirstOrDefault();
                                if (dbPackageDetails != null)
                                {
                                    dbContext.PackageProducts.Remove(dbPackageDetails);
                                    dbContext.SaveChanges();
                                }
                            }

                            foreach (PackageProductInfo p in productInfo.packageProductInfoList)
                            {
                                if (dbContext.Packages.Where(r => r.SyncCode == productInfo.SyncCode).FirstOrDefault() != null)
                                    packageid = dbContext.Packages.Where(r => r.SyncCode == productInfo.SyncCode).FirstOrDefault().PackageId;

                                if (dbContext.Products.Where(r => r.SyncCode == p.ProductSyncCode).FirstOrDefault() != null)
                                    productid = dbContext.Products.Where(r => r.SyncCode == p.ProductSyncCode).FirstOrDefault().ProductId;
                                if (productid > 0 && packageid > 0)
                                {
                                    packagedetails.ProductMaxImage = p.ProductMaxImage;
                                    packagedetails.ProductQuantity = p.ProductQuantity;
                                    packagedetails.PackageId = packageid;
                                    packagedetails.ProductId = productid;
                                    dbContext.PackageProducts.Add(packagedetails);
                                    dbContext.SaveChanges();
                                    productid = 0;
                                }
                            }


                        }

                        else
                        {
                            if (productInfo.IsPackage == true)
                            {
                                dbPackage = new Package();
                                dbPackage.Name = productInfo.Name;
                                dbPackage.Description = productInfo.Description;
                             //   dbPackage.Code = productInfo.Code;
                                dbPackage.Price = Convert.ToDouble(productInfo.Price);
                                // dbPackage.CurrencyID = 1;
                                dbPackage.IsDiscountApplied = productInfo.IsDiscountApplied;
                                dbPackage.IsActive = Convert.ToBoolean(productInfo.IsActive);
                                dbPackage.IsPersonalizedAR = Convert.ToBoolean(productInfo.IsPersonalizedAR);////latika for AR Personalised changed
                                dbPackage.SyncCode = productInfo.SyncCode;
                                var user = dbContext.Users.Where(r => r.SyncCode == productInfo.UserSyncCode).FirstOrDefault();
                                if (user != null)
                                {
                                    dbPackage.CreatedBy = user.UserId;
                                }
                               
                                dbPackage.CreatedDateTime = Convert.ToDateTime(productInfo.CreatedDateTime);
                                dbContext.Packages.Add(dbPackage);
                                dbContext.SaveChanges();


                                var digilocation = dbContext.DigiMasterLocations.FirstOrDefault();

                                if (productInfo.LocationSyncCode == null)
                                {
                                    digilocation = dbContext.DigiMasterLocations.Where(r => r.Name == ChangeInfo.StoreName && r.Level == 3).FirstOrDefault();
                                }
                                else
                                {
                                    digilocation = dbContext.DigiMasterLocations.Where(r => r.SyncCode == productInfo.LocationSyncCode && r.Level == 4).FirstOrDefault();
                                }
                                long PackageId = dbPackage.PackageId;
                               // var digilocatn = dbContext.DigiMasterLocations.Where(r => r.SyncCode == productInfo.LocationSyncCode && r.Level == 4).FirstOrDefault();
                                if (PackageId > 0)
                                {

                                    var objectauth = dbContext.ObjectAuthorizations.Where(r => r.ObjectValueId == PackageId && r.DigiMasterLocationId == digilocation.DigiMasterLocationId && r.ApplicationObjectId == 5).FirstOrDefault();

                                    if (objectauth != null)
                                    {
                                        dbContext.ObjectAuthorizations.Remove(objectauth);
                                        dbContext.SaveChanges();

                                    }
                                    ObjectAuthorization obj = new ObjectAuthorization();
                                    obj.ApplicationObjectId = 5;
                                    obj.ObjectValueId = PackageId;
                                    obj.DigiMasterLocationId = digilocation.DigiMasterLocationId;
                                    dbContext.ObjectAuthorizations.Add(obj);
                                    dbContext.SaveChanges();

                                }

                                PackageProduct packagedetails = new PackageProduct();

                                long productid = 0;
                                long packageid = 0;
                                int cnt = dbContext.PackageProducts.Count(p => p.PackageId == dbPackage.PackageId);
                                for (int i = 0; i <= cnt - 1; i++)
                                {
                                    var dbPackageDetails = dbContext.PackageProducts.Where(r => r.PackageId == dbPackage.PackageId).FirstOrDefault();
                                    if (dbPackageDetails != null)
                                    {
                                        dbContext.PackageProducts.Remove(dbPackageDetails);
                                        dbContext.SaveChanges();
                                    }
                                }

                                foreach (PackageProductInfo p in productInfo.packageProductInfoList)
                                {
                                    if (dbContext.Packages.Where(r => r.SyncCode == productInfo.SyncCode).FirstOrDefault() != null)
                                        packageid = dbContext.Packages.Where(r => r.SyncCode == productInfo.SyncCode).FirstOrDefault().PackageId;

                                    if (dbContext.Products.Where(r => r.SyncCode == p.ProductSyncCode).FirstOrDefault() != null)
                                        productid = dbContext.Products.Where(r => r.SyncCode == p.ProductSyncCode).FirstOrDefault().ProductId;
                                    if (productid > 0 && packageid > 0)
                                    {
                                        packagedetails.ProductMaxImage = p.ProductMaxImage;
                                        packagedetails.ProductQuantity = p.ProductQuantity;
                                        packagedetails.PackageId = packageid;
                                        packagedetails.ProductId = productid;
                                        dbPackage.IsPersonalizedAR = Convert.ToBoolean(productInfo.IsPersonalizedAR);////latika for AR Personalised changed
                                        dbContext.PackageProducts.Add(packagedetails);
                                        dbContext.SaveChanges();
                                        productid = 0;
                                    }
                                }
                            }
                        }


                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("ProductProcessor:ProcessUpdate:: Message: " + ex.Message + " StackTrace: " + ex.StackTrace + " InnerException: " + ex.InnerException + " Source: " + ex.Source);
                Exception outerException = new ProcessingException("Error occured in Package Processor Update.", ex);
                outerException.Source = "Business.Processing.PackageProcessor";
                throw outerException;
            }

        }
        protected override void ProcessDelete()
        {
            try
            {

                using (DigiPhotoWebContext dbContext = new DigiPhotoWebContext())
                {
                    var dbPackage = dbContext.Packages.Where(r => r.SyncCode == ChangeInfo.EntityCode).FirstOrDefault();
                    if (dbPackage != null)
                    {
                        long PackageID = 0;
                        if (dbContext.Packages.Where(r => r.SyncCode == ChangeInfo.EntityCode).FirstOrDefault() != null)
                            PackageID = dbContext.Packages.Where(r => r.SyncCode == ChangeInfo.EntityCode).FirstOrDefault().PackageId;
                        if (PackageID != 0)
                        {

                            int cnt = dbContext.Packages.Count(p => p.PackageId == PackageID);
                            for (int i = 0; i <= cnt - 1; i++)
                            {
                                var dbPackageDetails = dbContext.PackageProducts.Where(r => r.PackageId == PackageID).FirstOrDefault();
                                if (dbPackageDetails != null)
                                {
                                    dbContext.PackageProducts.Remove(dbPackageDetails);
                                    dbContext.SaveChanges();
                                }
                            }
                        }
                        if (dbPackage != null)
                        {
                            dbContext.Packages.Remove(dbPackage);
                            dbContext.SaveChanges();            //If multiple remove operations are done, commit will be at last.
                        }

                        var digimasterlocation = dbContext.ObjectAuthorizations.Where(r => r.ObjectValueId == dbPackage.PackageId && r.ApplicationObjectId == ChangeInfo.ApplicationObjectId).FirstOrDefault();
                        if (digimasterlocation != null)
                        {
                            dbContext.ObjectAuthorizations.Remove(digimasterlocation);
                            dbContext.SaveChanges();

                        }

                    }

                    var dbProduct = dbContext.Products.Where(r => r.SyncCode == ChangeInfo.EntityCode).FirstOrDefault();
                    if (dbProduct != null)
                    {

                        long productid = 0;
                        if (dbContext.Products.Where(r => r.SyncCode == ChangeInfo.EntityCode).FirstOrDefault() != null)
                            productid = dbContext.Products.Where(r => r.SyncCode == ChangeInfo.EntityCode).FirstOrDefault().ProductId;
                        if (productid != 0)
                        {
                            dbContext.Products.Remove(dbProduct);
                            dbContext.SaveChanges();

                        }

                        var digimasterlocation = dbContext.ObjectAuthorizations.Where(r => r.ObjectValueId == dbProduct.ProductId && r.ApplicationObjectId == ChangeInfo.ApplicationObjectId).FirstOrDefault();
                        if (digimasterlocation != null)
                        {
                            dbContext.ObjectAuthorizations.Remove(digimasterlocation);
                            dbContext.SaveChanges();

                        }

                    }

                }
            }
            catch (Exception ex)
            {
                log.Error("ProductProcessor:ProcessDelete:: Message: " + ex.Message + " StackTrace: " + ex.StackTrace + " InnerException: " + ex.InnerException + " Source: " + ex.Source);
                Exception outerException = new ProcessingException("Error occured in Package Processor Delete.", ex);
                outerException.Source = "Business.Processing.PackageProcessor";
                throw outerException;
            }
        }
    }
}
