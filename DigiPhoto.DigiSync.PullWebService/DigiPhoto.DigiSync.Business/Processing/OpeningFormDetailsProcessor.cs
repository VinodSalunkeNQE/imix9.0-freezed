﻿using DigiPhoto.DigiSync.Model;
using DigiPhoto.DigiSync.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using log4net;
using DigiPhoto.DigiSync.DataAccess.DataModels;

namespace DigiPhoto.DigiSync.Business.Processing
{
    public class OpeningFormDetailsProcessor : BaseProcessor
    {
        private static ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        protected override void ProcessInsert()
        {
            try
            {
                if (!string.IsNullOrEmpty(ChangeInfo.DataXML))
                {
                    OpeningFormDetailsInfo openingFormDetailsInfo = CommonUtility.DeserializeXML<OpeningFormDetailsInfo>(ChangeInfo.DataXML);
                    using (DigiPhotoWebContext dbContext = new DigiPhotoWebContext())
                    {
                        long? UserId = null;
                        var subStoreId = dbContext.DigiMasterLocations.Where(s => s.SyncCode == openingFormDetailsInfo.SubStore.SyncCode && s.Level == 4).FirstOrDefault().DigiMasterLocationId;
                        if (openingFormDetailsInfo != null)
                        {
                            if (openingFormDetailsInfo.FilledBySyncCode != null)
                                UserId = dbContext.Users.Where(s => s.SyncCode == openingFormDetailsInfo.FilledBySyncCode).FirstOrDefault().UserId;
                            OpeningFormDetails obj = new OpeningFormDetails();
                            obj.BussinessDate = openingFormDetailsInfo.BussinessDate;
                            obj.StartingNumber6X8 = openingFormDetailsInfo.StartingNumber6X8;
                            obj.StartingNumber8X10 = openingFormDetailsInfo.StartingNumber8X10;
                            obj.Auto6X8OpeningPrinterCount = openingFormDetailsInfo.AutoOpening6X8PrinterCount;
                            obj.Auto8x10OpeningPrinterCount = openingFormDetailsInfo.AutoOpening8x10PrinterCount;
                            obj.PosterStartingNumber = openingFormDetailsInfo.PosterStartingNumber;
                            obj.CashFloatAmount = openingFormDetailsInfo.CashFloatAmount;
                            obj.SubStoreId = Convert.ToInt64(subStoreId);
                            obj.OpeningDate = openingFormDetailsInfo.OpeningDate;
                            obj.FilledBy = UserId;
                            obj.SyncCode = openingFormDetailsInfo.SyncCode;
                            dbContext.OpeningFormDetails.Add(obj);
                            dbContext.SaveChanges();

                            WorkFlowControl workFlowControl = new WorkFlowControl()
                            {
                                SiteId = subStoreId,
                                FormTypeID = 1,  //for closingFormDetails
                                FormID = obj.OpeningFormDetailID,
                                FilledOn = obj.OpeningDate,  //Opening Form filled on.
                                FiledBy = UserId,
                                BussinessDate = obj.BussinessDate  //Opening Form of bussiness date.
                            };
                            dbContext.WorkFlowControls.Add(workFlowControl);
                            dbContext.SaveChanges();
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                log.Error("OpeningFormDetailsProcessor:ProcessInsert:: Message: " + ex.Message + " StackTrace: " + ex.StackTrace + " InnerException: " + ex.InnerException + " Source: " + ex.Source);
                Exception outerException = new ProcessingException("Error occured in OpeningFormDetails Processor Insert.", ex);
                outerException.Source = "Business.Processing.OpeningFormDetailsProcessor";
                throw outerException;
            }
        }
        protected override void ProcessUpdate()
        {
            throw new Exception("Not emplemented");
        }

        protected override void ProcessDelete()
        {
            throw new Exception("Not emplemented");
        }
    }
}
