﻿using DigiPhoto.DigiSync.DataAccess.DataModels;
using DigiPhoto.DigiSync.Model;
using DigiPhoto.DigiSync.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using log4net;
namespace DigiPhoto.DigiSync.Business.Processing
{
    //Created By Kumar Gaurav.............
    public class BorderProcessor : BaseProcessor
    {
        private static ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        protected override void ProcessInsert()
        {
            try
            {
                if (!string.IsNullOrEmpty(ChangeInfo.DataXML))
                {
                    BordersInfo bordersInfo = CommonUtility.DeserializeXML<BordersInfo>(ChangeInfo.DataXML);
                    using (DigiPhotoWebContext dbContext = new DigiPhotoWebContext())
                    {
                        var existingBorders = dbContext.Borders.Count(r => r.SyncCode == bordersInfo.SyncCode);
                        var storeId = (from DMStore in dbContext.DigiMasterLocations
                                       join DMSite in dbContext.DigiMasterLocations
                                       on DMStore.DigiMasterLocationId equals DMSite.ParentDigiMasterLocationId
                                       where DMSite.SyncCode == bordersInfo.SiteCodeSyncCode
                                       select DMStore.DigiMasterLocationId).FirstOrDefault();
                        if (existingBorders == 0)
                        {
                            var border = new Border();
                            border.Name = bordersInfo.Name;
                            border.Code = bordersInfo.Name;
                            border.ProductId = bordersInfo.ProductId;
                            border.CreatedDateTime = bordersInfo.ModifiedDate;
                            border.ModifiedDateTime = bordersInfo.ModifiedDate;
                            border.CreatedBy = 1;
                            border.ModifiedBy = 1;
                            border.IsActive = bordersInfo.IsActive;
                            border.SyncCode = bordersInfo.SyncCode;
                            border.IsSynced = true;
                            border.DisplayName = bordersInfo.Name;
                            dbContext.Borders.Add(border);
                            dbContext.SaveChanges();
                            Int64 borderId = border.BorderId;

                            ObjectAuthorization obj = new ObjectAuthorization();
                            obj.ApplicationObjectId = 1;
                            obj.ObjectValueId = borderId;
                            obj.IsDeleted = 0;
                            obj.DigiMasterLocationId = Convert.ToInt64(storeId);
                            dbContext.ObjectAuthorizations.Add(obj);
                            dbContext.SaveChanges();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("BorderProcessor:ProcessInsert:: Message: " + ex.Message + " StackTrace: " + ex.StackTrace + " InnerException: " + ex.InnerException + " Source: " + ex.Source);
                Exception outerException = new ProcessingException("Error occured in Border Processor Insert.", ex);
                outerException.Source = "Business.Processing.BorderProcessor";
                throw outerException;
            }
        }
        protected override void ProcessUpdate()
        {
            try
            {
                if (!string.IsNullOrEmpty(ChangeInfo.DataXML))
                {
                    BordersInfo bordersInfo = CommonUtility.DeserializeXML<BordersInfo>(ChangeInfo.DataXML);
                    using (DigiPhotoWebContext dbContext = new DigiPhotoWebContext())
                    {
                        var dbBorders = dbContext.Borders.Where(r => r.SyncCode == bordersInfo.SyncCode).FirstOrDefault();
                        if (dbBorders != null)
                        {
                            dbBorders.Name = bordersInfo.Name;
                            dbBorders.Code = bordersInfo.Name;
                            dbBorders.ProductId = bordersInfo.ProductId;
                            dbBorders.ModifiedDateTime = bordersInfo.ModifiedDate;
                            dbBorders.ModifiedBy = 1;
                            dbBorders.IsActive = bordersInfo.IsActive;
                            dbBorders.SyncCode = bordersInfo.SyncCode;
                            dbBorders.IsSynced = true;
                            dbBorders.DisplayName = bordersInfo.Name;
                            dbContext.SaveChanges();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("BorderProcessor:ProcessUpdate:: Message: " + ex.Message + " StackTrace: " + ex.StackTrace + " InnerException: " + ex.InnerException + " Source: " + ex.Source);
                Exception outerException = new ProcessingException("Error occured in Border Processor Update.", ex);
                outerException.Source = "Business.Processing.BorderProcessor";
                throw outerException;
            }
        }

        protected override void ProcessDelete()
        {
            try
            {

                using (DigiPhotoWebContext dbContext = new DigiPhotoWebContext())
                {
                    var dbBorder = dbContext.Borders.Where(r => r.SyncCode == ChangeInfo.EntityCode).FirstOrDefault();
                    if (dbBorder != null)
                    {
                        var objectAuthorizationobj = dbContext.ObjectAuthorizations.Where(x => x.ObjectValueId == dbBorder.BorderId 
                            && x.ApplicationObjectId == 1).ToList();
                        if (objectAuthorizationobj != null && objectAuthorizationobj.Count == 1)
                        {
                            dbContext.Borders.Remove(dbBorder);
                        }
                        dbContext.SaveChanges(); //If multiple remove operations are done, commit will be at last.
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("BorderProcessor:ProcessDelete:: Message: " + ex.Message + " StackTrace: " + ex.StackTrace + " InnerException: " + ex.InnerException + " Source: " + ex.Source);
                Exception outerException = new ProcessingException("Error occured in Border Processor  Delete.", ex);
                outerException.Source = "Business.Processing.BorderProcessor";
                throw outerException;
            }
        }
    }
}
