﻿using DigiPhoto.DigiSync.DataAccess.DataModels;
using DigiPhoto.DigiSync.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity.Validation;
using DigiPhoto.DigiSync.Business.Serialization;
using DigiPhoto.DigiSync.Utility;
using log4net;
using System.Configuration;
using System.Data;
using System.Transactions;

namespace DigiPhoto.DigiSync.Business
{
    public class SyncController
    {
        private static ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        //public static SyncResponseInfo SingIn(SyncRequestInfo syncRequest)
        //{
        //    try
        //    {
        //        using (DigiPhotoWebContext dbContext = new DigiPhotoWebContext())
        //        {
        //            var syncSession = new SyncSession();
        //            syncSession.ClientVenueCode = syncRequest.Venue.VenueCode;
        //            syncSession.ClientSiteCode = syncRequest.Venue.Site.SiteCode;
        //            syncSession.ClientStoreCode = syncRequest.Venue.Site.Store.StoreCode;
        //            syncSession.SignInDate = DateTime.Now;
        //            syncSession.Status = 1;   //TBD
        //            syncSession.StartChangeTrackingId = syncRequest.StartChangeTrackingId;
        //            syncSession.LastSyncOnDate = syncRequest.LastSyncOnDate;
        //            syncSession.SessionTokenId = System.Guid.NewGuid();
        //            dbContext.SyncSessions.Add(syncSession);
        //            dbContext.SaveChanges();

        //            return new SyncResponseInfo() { SessionTokenId = syncSession.SessionTokenId };
        //        }
        //    }
        //    catch (DbEntityValidationException e)
        //    {
        //        foreach (var eve in e.EntityValidationErrors)
        //        {
        //            Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
        //                eve.Entry.Entity.GetType().Name, eve.Entry.State);
        //            foreach (var ve in eve.ValidationErrors)
        //            {
        //                Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
        //                    ve.PropertyName, ve.ErrorMessage);
        //            }
        //        }
        //        throw;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}
        /*
        public static ChangeInfo GetNextChangeInfo1(Guid sessionTokenId, ChangeInfo lastChangeInfo)
        {
            try
            {
                using (DigiPhotoWebContext dbContext = new DigiPhotoWebContext())
                {
                    ChangeInfo change;
                    //This query will be from DB when we need query Location entity data
                    var changeTracking = (from c in dbContext.ChangeTrackings
                                          where c.ChangeTrackingId > lastChangeInfo.ChangeTrackingId
                                          orderby c.ChangeTrackingId, c.ChangeDate
                                          //select new ChangeInfo
                                          //{
                                          //    ApplicationObjectId = c.ApplicationObjectId,
                                          //    ChangeTrackingId = c.ChangeTrackingId,
                                          //    ChangeDate = c.ChangeDate,
                                          //    dataXML = GetChangedDataXML(c.ApplicationObjectId, c.ObjectValueId)
                                          //}
                                          select c
                                ).FirstOrDefault();

                    if (changeTracking != null)
                    {
                        change = new ChangeInfo
                        {
                            ApplicationObjectId = changeTracking.ApplicationObjectId,
                            ObjectValueId = changeTracking.ObjectValueId,
                            ChangeTrackingId = changeTracking.ChangeTrackingId,
                            ChangeDate = changeTracking.ChangeDate,
                            dataXML = GetChangedDataXML(changeTracking.ApplicationObjectId, changeTracking.ObjectValueId),
                            ChangeAction = changeTracking.ChangeAction,
                            ChangeBy = changeTracking.ChangeBy,
                            EntityCode = changeTracking.EntityCode
                        };
                    }
                    else
                    {
                        change = lastChangeInfo;
                    }
                    return change;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
          */
        public static ChangeInfo GetNextChangeInfo(ChangeInfo lastChangeInfo)
        {
            try
            {

                ChangeInfo change;
                ChangeInfo changeTracking = DataAccess.ChangeTraking.GetChangeInfo(lastChangeInfo);
                if (changeTracking != null && changeTracking.ChangeTrackingId > 0)
                {
                    log.Info("Change Tracking Id:" + changeTracking.ChangeTrackingId);
                    DownloadInfo download;
                    string dataXML;
                    GetChangedDataXML(changeTracking.ApplicationObjectId, changeTracking.ObjectValueId, out dataXML, out download);

                    change = new ChangeInfo
                    {
                        ApplicationObjectId = changeTracking.ApplicationObjectId,
                        ObjectValueId = changeTracking.ObjectValueId,
                        ChangeTrackingId = changeTracking.ChangeTrackingId,
                        ChangeDate = changeTracking.ChangeDate,
                        dataXML = dataXML,//GetChangedDataXML(changeTracking.ApplicationObjectId, changeTracking.ObjectValueId),
                        ChangeAction = changeTracking.ChangeAction,
                        ChangeBy = changeTracking.ChangeBy,
                        EntityCode = changeTracking.EntityCode,
                        DownloadDetail = download,
                        IsDeleted=changeTracking.IsDeleted
                    };
                }
                else
                {
                    change = lastChangeInfo;
                }
                return change;
            }
            catch (Exception ex)
            {
                log.Error("GetNextChangeInfo:: Message: " + ex.Message + " StackTrace: " + ex.StackTrace + " InnerException: " + ex.InnerException + " Source: " + ex.Source);

                throw ex;
            }
        }

        #region Sync Service Enhancement - Ashirwad
        public static OnlineOrderStatus GetOnlineOrderStatus(QrCodeStatus qrCode)
        {
            try
            {
                OnlineOrderStatus obj = new OnlineOrderStatus();
                using (DigiPhotoWebContext dbContext = new DigiPhotoWebContext())
                {
                    
                    var imsOrderList = (from DO in dbContext.CustomerQRCodeDetails
                                        where DO.IdentificationCode == qrCode.IdentificationCode
                                        && DO.OrderId==qrCode.OrderNumber
                                        orderby DO.OrderDate descending
                                        select DO).Take(1);

                    obj.OnlineOrderIMSStatus = (int)SyncStatus.NotProcessedinIMS;
                    obj.OnlineOrderIMSStatusText = "Not Processed in IMS";

                    foreach (CustomerQRCodeDetail item in imsOrderList)
                    {
                        if (item.IdentificationCode == qrCode.IdentificationCode)
                        {
                            obj.OnlineOrderIMSStatus = (int)SyncStatus.ProcessedinIMS;
                            obj.OnlineOrderIMSStatusText = "Processed in IMS";                            
                        }
                    }

                    if (obj.OnlineOrderIMSStatus == (int)SyncStatus.ProcessedinIMS)
                    {
                        var objectwebphoto = (from DO in dbContext.WebPhotos
                                              where DO.IdentificationCode == qrCode.IdentificationCode
                                              && DO.OrderNumber == qrCode.OrderNumber
                                              orderby DO.WebPhotoId descending
                                              select DO).Take(1);


                        foreach (WebPhoto item in objectwebphoto)
                        {
                            if (item.Height > 0 && item.Width > 0)
                            {
                                obj.OnlineOrderIMSStatus = (int)SyncStatus.HWupdatedinIMS;
                                obj.OnlineOrderIMSStatusText = "H&W updated in IMS";

                            }
                            else if (item.Height <= 0 && item.Width <= 0)
                            {
                                obj.OnlineOrderIMSStatus = (int)SyncStatus.HWnotupdatedinIMS;
                                obj.OnlineOrderIMSStatusText = "H&W not updated in IMS";
                            }
                        }

                    }

                }

                if (obj.OnlineOrderIMSStatus == (int)SyncStatus.HWupdatedinIMS)
                {
                    long partnerStatus = DataAccess.ChangeTraking.GetOnlinePartnerStatus(qrCode);

                    if (partnerStatus > 0)
                    {
                        obj.OnlineOrderIMSStatus = (int)SyncStatus.CodeisClaimable;
                        obj.OnlineOrderIMSStatusText = "Code is Claimable";
                    }

                }

                return obj;
            }
            catch (Exception ex)
            {
                log.Error("GetOnlineOrderStatus:: Message: " + ex.Message + " StackTrace: " + ex.StackTrace + " InnerException: " + ex.InnerException + " Source: " + ex.Source);

                throw ex;
            }
        }
        #endregion
        private static void GetChangedDataXML(long applicationObjectId, long objectValueId, out string dataXML, out DownloadInfo downloadInfo)
        {
            try
            {
                downloadInfo = null;
                Serializer serializer = SerializationFactory.Create(applicationObjectId);
                dataXML = serializer.Serialize(objectValueId);
                if (serializer is IDownloadable)   //If This serializer has Download
                {
                    IDownloadable downloadable = (IDownloadable)serializer;
                    downloadInfo = downloadable.Download;
                }
            }
            catch (Exception ex)
            {
                using (DigiPhotoWebContext dbContext = new DigiPhotoWebContext())
                {
                    var changeDataList = dbContext.ChangeTrackings.Where(s => s.ApplicationObjectId == applicationObjectId && s.ObjectValueId == objectValueId).ToList();
                    foreach (var changeData in changeDataList)
                    {
                        changeData.SyncStatus = -1;
                    }
                    dbContext.SaveChanges();
                }
                log.Error("GetChangedDataXML:: Message: " + ex.Message + " StackTrace: " + ex.StackTrace + " InnerException: " + ex.InnerException + " Source: " + ex.Source);
                throw ex;
            }
        }
        private static string GetChangedDataXML(long applicationObjectId, long objectValueId)
        {
            try
            {
                Serializer serializer = SerializationFactory.Create(applicationObjectId);
                return serializer.Serialize(objectValueId);
            }
            catch (Exception ex)
            {
                log.Error("GetChangedDataXML:: Message: " + ex.Message + " StackTrace: " + ex.StackTrace + " InnerException: " + ex.InnerException + " Source: " + ex.Source);

                throw ex;
            }
        }
        //public static bool SignOut(Guid SessionTokenId, long endChangeTrackingId)
        //{
        //    try
        //    {
        //        using (DigiPhotoWebContext dbContext = new DigiPhotoWebContext())
        //        {
        //            var session = dbContext.SyncSessions.Where(s => s.SessionTokenId == SessionTokenId).FirstOrDefault();
        //            if (session != null)
        //            {
        //                //Update Role Name
        //                session.SignOutDate = DateTime.Now;
        //                session.EndChangeTrackingId = endChangeTrackingId;
        //                session.Status = 3;                   //Status = Closed TBD
        //                dbContext.SaveChanges();
        //            }
        //            return true;
        //        }
        //    }
        //    catch (DbEntityValidationException ex)
        //    {
        //        log.Error("SignOut:: Message: " + ex.Message + " StackTrace: " + ex.StackTrace + " InnerException: " + ex.InnerException + " Source: " + ex.Source);

        //        foreach (var eve in ex.EntityValidationErrors)
        //        {
        //            Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
        //                eve.Entry.Entity.GetType().Name, eve.Entry.State);
        //            foreach (var ve in eve.ValidationErrors)
        //            {
        //                Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
        //                    ve.PropertyName, ve.ErrorMessage);
        //            }
        //        }
        //        throw;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}
        public static List<IncomingChangeInfo> GetNextIncomingChange()
        {
            try
            {
                List<IncomingChangeInfo> IncomingChangeList = new List<IncomingChangeInfo>();
                var PartnerIds = ConfigurationManager.AppSettings["PartnerIds"];
                //   IncomingChangeInfo IncomingChangeObjectList;
                //if (!string.IsNullOrEmpty(PartnerIds))
                {
                    using (DigiPhotoWebContext dbContext = new DigiPhotoWebContext())
                    {

                        //----Update the ImixPosDetail Processing Status to 2 in order to not process
                        //var IncomingIMixPosDetail = dbContext.IncomingChanges.Where(s => (s.ApplicationObjectId == 20 || s.ApplicationObjectId == 21) && s.ProcessingStatus == 0).ToList();
                        //Application Object ID 21 is removed since it is for CloudinaryException application objectId _ Vinod Salunke_14Jul2018
                        var IncomingIMixPosDetail = dbContext.IncomingChanges.Where(s => (s.ApplicationObjectId == 20) && s.ProcessingStatus == 0).ToList();
                        if (IncomingIMixPosDetail != null && IncomingIMixPosDetail.Count() > 0)
                        {
                            foreach (var IncomingIMixPosDetailobj in IncomingIMixPosDetail)
                            {
                                IncomingIMixPosDetailobj.ProcessingStatus = 2;
                            }
                            dbContext.SaveChanges();
                        }

                        var IncomingChangeObjectList = (from PL in dbContext.PartnerLocations
                                                        join DL in dbContext.DigiMasterLocations on PL.StoreID equals DL.ParentDigiMasterLocationId
                                                        join IC in dbContext.IncomingChanges on DL.SyncCode equals IC.SubStoreCode
                                                        where IC.ProcessingStatus == (int)SyncProcessingStatus.NotProcessed
                                                        && IC.ApplicationObjectId != (int)ApplicationObjectEnum.Activity
                                                        //&& PartnerIds.Contains(PL.PartnerID.ToString())
                                                        && DL.Level == 4
                                                        orderby IC.IncomingChangeId, IC.ChangeRecievedOn
                                                        select IC).Take(100);

                        //var IncomingChangeObjectList = (from c in dbContext.IncomingChanges
                        //                                where c.ProcessingStatus == (int)SyncProcessingStatus.NotProcessed
                        //                                && c.ApplicationObjectId != (int)ApplicationObjectEnum.Activity
                        //                                orderby c.IncomingChangeId, c.ChangeRecievedOn
                        //                                select c
                        //                           ).Take(100);//.FirstOrDefault();

                        if (IncomingChangeObjectList != null && IncomingChangeObjectList.Count() == 0)
                        {
                            //IncomingChangeObjectList = (
                            //                       from c in dbContext.IncomingChanges
                            //                       where c.ProcessingStatus == (int)SyncProcessingStatus.NotProcessed
                            //                       orderby c.IncomingChangeId, c.ChangeRecievedOn
                            //                       select c
                            //                       ).Take(100);//.FirstOrDefault();
                            IncomingChangeObjectList = (from PL in dbContext.PartnerLocations
                                                        join DL in dbContext.DigiMasterLocations on PL.StoreID equals DL.ParentDigiMasterLocationId
                                                        join IC in dbContext.IncomingChanges on DL.SyncCode equals IC.SubStoreCode
                                                        where IC.ProcessingStatus == (int)SyncProcessingStatus.NotProcessed
                                                       // && PartnerIds.Contains(PL.PartnerID.ToString())
                                                        && DL.Level == 4
                                                        orderby IC.IncomingChangeId, IC.ChangeRecievedOn
                                                        select IC).Take(100);
                            if (IncomingChangeObjectList != null && IncomingChangeObjectList.Count() == 0)
                            {
                                IncomingChangeObjectList = (
                                                    from c in dbContext.IncomingChanges
                                                    where c.ProcessingStatus == (int)SyncProcessingStatus.NotProcessed
                                                    orderby c.IncomingChangeId, c.ChangeRecievedOn
                                                    select c
                                                    ).Take(100);//.FirstOrDefault();}
                            }
                        }


                        //IncomingChangeInfo IncomingChange = null;
                        //if (IncomingChangeObject != null)
                        //{

                        //    IncomingChange = new IncomingChangeInfo
                        //    {
                        //        IncomingChangeId = IncomingChangeObject.IncomingChangeId,
                        //        ChangeTrackingId = IncomingChangeObject.ChangeTrackingId,
                        //        ApplicationObjectId = IncomingChangeObject.ApplicationObjectId,
                        //        ObjectValueId = IncomingChangeObject.ObjectValueId,
                        //        ChangeAction = IncomingChangeObject.ChangeAction,
                        //        ChangeDateOnIMIX = IncomingChangeObject.ChangeDateOnIMIX,
                        //        ChangeByOnIMIX = IncomingChangeObject.ChangeByOnIMIX,
                        //        ChangeRecievedOn = IncomingChangeObject.ChangeRecievedOn,
                        //        DataXML = IncomingChangeObject.DataXML,
                        //        StoreName = IncomingChangeObject.StoreName,
                        //        SubStore = new SiteInfo
                        //        {
                        //            SiteCode = IncomingChangeObject.SubStoreCode
                        //        },
                        //        EntityCode = IncomingChangeObject.EntityCode,
                        //        ProcessingStatus = IncomingChangeObject.ProcessingStatus
                        //    };
                        //}

                        IncomingChangeInfo IncomingChange = null;
                        if (IncomingChangeObjectList != null)
                        {

                            foreach (IncomingChange IncomingChangeObject in IncomingChangeObjectList)
                            {
                                IncomingChange = new IncomingChangeInfo
                                                    {
                                                        IncomingChangeId = IncomingChangeObject.IncomingChangeId,
                                                        ChangeTrackingId = IncomingChangeObject.ChangeTrackingId,
                                                        ApplicationObjectId = IncomingChangeObject.ApplicationObjectId,
                                                        ObjectValueId = IncomingChangeObject.ObjectValueId,
                                                        ChangeAction = IncomingChangeObject.ChangeAction,
                                                        ChangeDateOnIMIX = IncomingChangeObject.ChangeDateOnIMIX,
                                                        ChangeByOnIMIX = IncomingChangeObject.ChangeByOnIMIX,
                                                        ChangeRecievedOn = IncomingChangeObject.ChangeRecievedOn,
                                                        DataXML = IncomingChangeObject.DataXML,
                                                        StoreName = IncomingChangeObject.StoreName,
                                                        SubStore = new SiteInfo
                                                        {
                                                            SiteCode = IncomingChangeObject.SubStoreCode
                                                        },
                                                        EntityCode = IncomingChangeObject.EntityCode,
                                                        ProcessingStatus = IncomingChangeObject.ProcessingStatus
                                                    };
                                IncomingChangeList.Add(IncomingChange);
                            }
                        }
                    }
                }
                return IncomingChangeList;
            }
            catch (Exception ex)
            {
                log.Error("GetNextIncomingChange:: Message: " + ex.Message + " StackTrace: " + ex.StackTrace + " InnerException: " + ex.InnerException + " Source: " + ex.Source);

                throw ex;
            }
        }
        public static void UpdateProcessingStatus(long incomingChangeId, int processingStatus)
        {
            try
            {
                using (TransactionScope scope = TransactionManagement.CreateTransactionScope())
                {
                    try
                    {
                        using (DigiPhotoWebContext dbContext = new DigiPhotoWebContext())
                        {
                            var incomingChange = dbContext.IncomingChanges.Where(i => i.IncomingChangeId == incomingChangeId).FirstOrDefault();
                            incomingChange.ProcessingStatus = processingStatus;
                            dbContext.SaveChanges();
                            scope.Complete();
                        }
                    }
                    catch (Exception ex)
                    {
                        log.Error("UpdateProcessingStatus:: Message: " + ex.Message + " StackTrace: " + ex.StackTrace + " InnerException: " + ex.InnerException + " Source: " + ex.Source);
                        throw ex;
                    }
                    //No need of finally block while using  transactionScope with in using()
                    //finally
                    //{
                    //    scope.Dispose();
                    //}

                }
            }
            catch (Exception ex)
            {

                log.Error("UpdateProcessingStatus:: Message: " + ex.Message + " StackTrace: " + ex.StackTrace + " InnerException: " + ex.InnerException + " Source: " + ex.Source);
                throw ex;
            }

        }
        public static bool SaveChangeInfo(ChangeInfo change)
        {
            try
            {
                using (DigiPhotoWebContext dbContext = new DigiPhotoWebContext())
                {
                    var incomingChange = new IncomingChange();
                    incomingChange.ChangeTrackingId = change.ChangeTrackingId;
                    incomingChange.ApplicationObjectId = change.ApplicationObjectId;
                    incomingChange.ObjectValueId = change.ObjectValueId;
                    incomingChange.ChangeAction = change.ChangeAction;
                    incomingChange.DataXML = change.dataXML;
                    incomingChange.ChangeDateOnIMIX = change.ChangeDate;
                    incomingChange.ChangeByOnIMIX = change.ChangeBy;
                    incomingChange.ChangeRecievedOn = DateTime.Now;
                    incomingChange.EntityCode = change.EntityCode;
                    incomingChange.StoreName = change.SubStore.Store.StoreName;
                    incomingChange.SubStoreCode = change.SubStore.SiteCode;
                    dbContext.IncomingChanges.Add(incomingChange);
                    dbContext.SaveChanges();
                    return true;
                }
            }
            catch (DbEntityValidationException ex)
            {
                log.Error("UpdateProcessingStatus:: Message: " + ex.Message + " StackTrace: " + ex.StackTrace + " InnerException: " + ex.InnerException + " Source: " + ex.Source);

                foreach (var eve in ex.EntityValidationErrors)
                {
                    Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                        eve.Entry.Entity.GetType().Name, eve.Entry.State);
                    foreach (var ve in eve.ValidationErrors)
                    {
                        Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                            ve.PropertyName, ve.ErrorMessage);
                    }
                }
                throw;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public static CardLimitInfo CheckCardLimit(string code)
        {
            try
            {
                using (DigiPhotoWebContext dbContext = new DigiPhotoWebContext())
                {
                    CardLimitInfo cardLimit;
                    string cardIdentificationDigit = string.Empty;
                    if (code.Length >= 4)
                    {
                        cardIdentificationDigit = code.Substring(0, 4);
                    }
                    var cardType = dbContext.iMixImageCardTypes.Where(c => c.CardIdentificationDigit == cardIdentificationDigit).FirstOrDefault();
                    int Allowed = 0;
                    if (cardType != null)
                    {
                        Allowed = cardType.MaxImages == null ? 0 : Convert.ToInt32(cardType.MaxImages);
                    }
                    else
                    {
                        return new CardLimitInfo
                        {
                            ValidCard = false
                        };
                    }

                    var orderDetail = dbContext.CustomerQRCodeDetails.Where(c => c.IdentificationCode == code).ToList();
                    if (orderDetail != null)
                    {
                        int associated = orderDetail.Select(p => p.Photos.Split(',').Length).Sum();
                        cardLimit = new CardLimitInfo()
                        {
                            //IsCardPurchased = true,
                            Allowed = Allowed,
                            Associated = associated,
                            ValidCard = true
                        };
                    }
                    else
                    {
                        cardLimit = new CardLimitInfo()
                        {
                            //IsCardPurchased = false,
                            Allowed = Allowed,
                            Associated = 0,
                            ValidCard = true
                        };
                    }
                    return cardLimit;
                }
            }
            catch (Exception ex)
            {
                log.Error("CheckCardLimit:: Message: " + ex.Message + " StackTrace: " + ex.StackTrace + " InnerException: " + ex.InnerException + " Source: " + ex.Source);

                throw ex;
            }
        }
        public static CardLimitInfo CheckAnonymousCardLimit(string code, PackageInfo packageInfo)
        {
            try
            {
                using (DigiPhotoWebContext dbContext = new DigiPhotoWebContext())
                {
                    CardLimitInfo cardLimit;

                    var pkgInfo = (from p in dbContext.Packages
                                              join pp in dbContext.PackageProducts on p.PackageId equals pp.PackageId
                                              where
                                              pp.ProductId == 84 &&
                                              p.Name == packageInfo.PackageName &&
                                              p.Price == packageInfo.PackagePrice &&
                                              pp.ProductMaxImage == packageInfo.MaxImgQuantity
                                              select new 
                                              {
                                                  ProductMaxImage=pp.ProductMaxImage,
                                                  PackageId=pp.PackageId
                                              }).FirstOrDefault();


                    //var pkgInfo = dbContext.Packages1.Where(c => c.Name == packageInfo.PackageName 
                    //    && c.PackagePrice == packageInfo.PackagePrice 
                    //    && c.MaxQuantity == packageInfo.MaxImgQuantity).FirstOrDefault();
                    int Allowed = 0;

                    if (pkgInfo != null)
                    {
                        // Allowed = pkgInfo.MaxQuantity == null ? 0 : Convert.ToInt32(pkgInfo.MaxQuantity);
                        Allowed = pkgInfo.ProductMaxImage == null ? 0 : Convert.ToInt32(pkgInfo.ProductMaxImage);
                    }
                    else
                    {
                        return new CardLimitInfo
                        {
                            ValidCard = true,
                            Allowed = 0,
                            Associated = 0
                        };
                    }

                    var orderDetail = dbContext.CustomerQRCodeDetails.Where(c => c.IdentificationCode == code && c.PackageId == pkgInfo.PackageId).ToList();
                    if (orderDetail != null)
                    {
                        int associated = orderDetail.Select(p => p.Photos.Split(',').Length).Sum();
                        cardLimit = new CardLimitInfo()
                        {
                            Allowed = Allowed,
                            Associated = associated,
                            ValidCard = true
                        };
                    }
                    else
                    {
                        cardLimit = new CardLimitInfo()
                        {
                            Allowed = Allowed,
                            Associated = 0,
                            ValidCard = true
                        };
                    }
                    return cardLimit;
                }
            }
            catch (Exception ex)
            {
                log.Error("CheckAnonymousCardLimit:: Message: " + ex.Message + " StackTrace: " + ex.StackTrace + " InnerException: " + ex.InnerException + " Source: " + ex.Source);

                throw ex;
            }
        }
        public static bool SaveCurrencySyncStatusInfo(CurrencySyncStatusInfo currencySyncStatus)
        {
            try
            {
                using (DigiPhotoWebContext dbContext = new DigiPhotoWebContext())
                {

                    //here get the id of venue base don the sync code
                    var SubStore = dbContext.DigiMasterLocations.Where(r => r.SyncCode == currencySyncStatus.SubStoreSyncCode).FirstOrDefault();
                    var Venue = dbContext.DigiMasterLocations.Where(r => r.DigiMasterLocationId == SubStore.ParentDigiMasterLocationId).FirstOrDefault();
                    var currencyStatus = new CurrencySyncStatus();// IncomingChange();
                    currencyStatus.CurrencySyncStatusID = currencySyncStatus.CurrencySyncStatusID;
                    currencyStatus.CurrencyProfileID = currencySyncStatus.CurrencyProfileID;
                    currencyStatus.VenueID = Venue.DigiMasterLocationId;
                    currencyStatus.SyncStatus = currencySyncStatus.SyncStatus;
                    currencyStatus.CreatedBy = currencySyncStatus.CreatedBy;
                    currencyStatus.CreatedOn = currencySyncStatus.CreatedOn;
                    currencyStatus.CurrencyProfileStatus = currencySyncStatus.CurrencyProfileStatus;
                    //incomingChange.ChangeRecievedOn = currencySyncStatus.Now;
                    //incomingChange.EntityCode = change.EntityCode;
                    //incomingChange.StoreName = change.SubStore.Store.StoreName;
                    //incomingChange.SubStoreCode = change.SubStore.SiteCode;
                    dbContext.CurrencySyncStatuses.Add(currencyStatus);
                    dbContext.SaveChanges();
                    return true;
                }
            }
            catch (DbEntityValidationException ex)
            {
                log.Error("UpdateProcessingStatus:: Message: " + ex.Message + " StackTrace: " + ex.StackTrace + " InnerException: " + ex.InnerException + " Source: " + ex.Source);

                foreach (var eve in ex.EntityValidationErrors)
                {
                    Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                        eve.Entry.Entity.GetType().Name, eve.Entry.State);
                    foreach (var ve in eve.ValidationErrors)
                    {
                        Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                            ve.PropertyName, ve.ErrorMessage);
                    }
                }
                throw;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public static bool updateChangeTrackingHistory(ChangeInfo changeInfo, SiteInfo site, Int32 ProcessingStatus)
        {
            try
            {
                using (DigiPhotoWebContext dbContext = new DigiPhotoWebContext())
                {
                    var changDetails = dbContext.ChangeTrackingDownLoadHistorys.Where(x => x.SiteSyncCode == site.SiteCode &&
                        x.ObjectValueId == changeInfo.ObjectValueId && x.ApplicationObjectId == changeInfo.ApplicationObjectId &&
                        x.ChangeTrackingId == changeInfo.ChangeTrackingId).FirstOrDefault();
                    if (changDetails != null)
                    {
                        changDetails.ProcessingStatus = ProcessingStatus;
                        dbContext.SaveChanges();
                    }
                    return true;
                }
            }
            catch (DbEntityValidationException ex)
            {
                log.Error("updateChangeTrackingHistory:: Message: " + ex.Message + " StackTrace: " + ex.StackTrace + " InnerException: " + ex.InnerException + " Source: " + ex.Source);
                foreach (var eve in ex.EntityValidationErrors)
                {
                    Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                        eve.Entry.Entity.GetType().Name, eve.Entry.State);
                    foreach (var ve in eve.ValidationErrors)
                    {
                        Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                            ve.PropertyName, ve.ErrorMessage);
                    }
                }
                throw;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        //////created by latika start for AR Personalised
        public static int CheckIsPersonalized(int orderID)
        {
            try
            {

                return DataAccess.ChangeTraking.CheckIsPersonalized(orderID); ;
            }
            catch (Exception ex)
            {
                log.Error("CheckIsPersonalized:: Message: " + ex.Message + " StackTrace: " + ex.StackTrace + " InnerException: " + ex.InnerException + " Source: " + ex.Source);

                throw ex;
            }
        }
        //////created by latika end
    }
}
