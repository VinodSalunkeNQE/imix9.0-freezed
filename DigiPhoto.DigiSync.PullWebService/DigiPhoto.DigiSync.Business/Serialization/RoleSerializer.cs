﻿using DigiPhoto.DigiSync.DataAccess.DataModels;
using DigiPhoto.DigiSync.Model;
using DigiPhoto.DigiSync.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DigiPhoto.DigiSync.Business.Serialization
{
    class RoleSerializer : Serializer
    {
        public override string Serialize(long ObjectValueId)
        {
            try
            {
                using (DigiPhotoWebContext dbContext = new DigiPhotoWebContext())
                {
                    string dataXML = string.Empty;
                    var role = dbContext.Roles.Where(r => r.RoleId == ObjectValueId).FirstOrDefault();
                    if (role != null)
                    {
                        List<string> mod = (from modules in dbContext.Modules
                                            join
                                                rolemodules in dbContext.RoleModules on modules.ModuleId equals rolemodules.ModuleId
                                            where rolemodules.RoleId == role.RoleId
                                            select modules.SyncCode).ToList();
                                                                

                        RoleInfo roleInfo = new RoleInfo()
                                            {
                                                RoleId = role.RoleId,
                                                RoleName = role.Name,
                                                IsActive = role.IsActive,
                                                SyncCode = role.SyncCode,
                                                ModulesSyncCodes=mod
                                            };

                        dataXML = CommonUtility.SerializeObject<RoleInfo>(roleInfo);
                    }
                    return dataXML;
                }
            }
            catch(Exception e)
            {
                Exception outerException = new SerializationException("Error occured in Role Serialization.", e);
                outerException.Source = "Business.Serialization.RoleSerializer";
                throw outerException;
            }
        }
        public override object Deserialize(string input)
        {
            throw new Exception("Not emplemented");
        }
    }
}
