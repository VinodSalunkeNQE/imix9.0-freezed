﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Reflection;

namespace ErrorHandler
{
    public class ErrorHandler
    {

        /// <summary>
        /// This method is for prepare the error massage on base of Exception Object
        /// </summary>
        /// <param name="ServiceException"></param>
        /// <returns></returns>

        private static volatile object _object = new Object();

        public static string CreateErrorMessage(Exception ServiceException)
        {
            StringBuilder messageBuilder = new StringBuilder();

            try
            {
                messageBuilder.Append(DateTime.Now.ToString("HH:mm:ss tt"));
                messageBuilder.Append(" :" + " The Exception is:- " + ServiceException.ToString());


                if (ServiceException.InnerException != null)
                {

                    messageBuilder.Append(" InnerException :: " + ServiceException.InnerException.ToString());

                }


                if (ServiceException.StackTrace != null)
                {

                    messageBuilder.Append(" StackTrace :: " + ServiceException.StackTrace.ToString());

                }
                if (ServiceException.Message != null)
                {

                    messageBuilder.Append(" Message is :: " + ServiceException.Message.ToString());

                }



                return messageBuilder.ToString();
            }
            catch
            {
                messageBuilder.Append("Exception:: Unknown Exception.");
                return messageBuilder.ToString();
            }

        }

        /// <summary>
        /// This method is for writting the Log file with message parameter
        /// </summary>
        /// <param name="message"></param>
        public static void LogFileWrite(string message)
        {

            //ErrorLogger.WriteError(message);
            lock (_object)
            {
                LogError(message);
            }

        }

        public static void LogError(string message)
        {
            FileStream fileStream = null;
            StreamWriter streamWriter = null;
            try
            {
                //string root = Environment.CurrentDirectory;
                string root = Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location));
                string logFilePath = root + "\\";
                logFilePath = System.IO.Path.Combine(logFilePath, "DigiLogError");
                logFilePath = logFilePath + "\\" + "DigiLog" + "-" + DateTime.Today.ToString("yyyyMMdd") + "." + "txt";

                if (logFilePath.Equals(""))
                    return;

                #region Create the Log file directory if it does not exists
                DirectoryInfo logDirInfo = null;
                FileInfo logFileInfo = new FileInfo(logFilePath);
                logDirInfo = new DirectoryInfo(logFileInfo.DirectoryName);

                if (!logDirInfo.Exists)
                    logDirInfo.Create();

                if (logFileInfo.Exists)
                {
                    if (logFileInfo.Length > 5000000)//Check if file size is greater than 5MB then rename it and create new one..Vins
                    {
                        logFileInfo.MoveTo(logFilePath.Replace(".txt", DateTime.Now.ToString("yyyyMMddHHmmss") + ".txt"));
                    }
                }
                #endregion Create the Log file directory if it does not exists

                if (!logFileInfo.Exists)
                {
                    fileStream = logFileInfo.Create();
                }
                else
                {
                    fileStream = new FileStream(logFilePath, FileMode.Append);
                }
                streamWriter = new StreamWriter(fileStream);
                streamWriter.WriteLine(message);
                streamWriter.WriteLine(" ");
            }
            finally
            {
                if (streamWriter != null) streamWriter.Close();
                if (fileStream != null) fileStream.Close();
            }
        }

        
        public static void LogError(Exception ServiceException)
        {
            StringBuilder messageBuilder = new StringBuilder();
            try
            {
                messageBuilder.Append(DateTime.Now.ToString("HH:mm:ss tt"));
                messageBuilder.Append(" :" + " The Exception is:- " + ServiceException.ToString());
                if (ServiceException.InnerException != null)
                {
                    messageBuilder.Append(" InnerException :: " + ServiceException.InnerException.ToString());
                }
                if (ServiceException.StackTrace != null)
                {
                    messageBuilder.Append(" StackTrace :: " + ServiceException.StackTrace.ToString());
                }
                if (ServiceException.Message != null)
                {
                    messageBuilder.Append(" Message is :: " + ServiceException.Message.ToString());
                }
                //messageBuilder.ToString();
            }
            catch
            {
                messageBuilder.Append("Exception:: Unknown Exception.");
                //messageBuilder.ToString();
            }

            //ErrorLogger.WriteError(messageBuilder.ToString()); Implemented for log4net
            lock (_object)
            {
                LogError(messageBuilder);
            }

        }

        private static void LogError(StringBuilder messageBuilder)
        {
            FileStream fileStream = null;
            StreamWriter streamWriter = null;
            try
            {
                //string root = Environment.CurrentDirectory;
                string root = Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location));
                string logFilePath = root + "\\";
                logFilePath = System.IO.Path.Combine(logFilePath, "DigiLogError");
                logFilePath = logFilePath + "\\" + "DigiLog" + "-" + DateTime.Today.ToString("yyyyMMdd") + "." + "txt";

                if (logFilePath.Equals(""))
                    return;

                #region Create the Log file directory if it does not exists
                DirectoryInfo logDirInfo = null;
                FileInfo logFileInfo = new FileInfo(logFilePath);
                logDirInfo = new DirectoryInfo(logFileInfo.DirectoryName);

                if (!logDirInfo.Exists)
                    logDirInfo.Create();
                #endregion Create the Log file directory if it does not exists

                if (!logFileInfo.Exists)
                {
                    fileStream = logFileInfo.Create();
                }
                else
                {
                    fileStream = new FileStream(logFilePath, FileMode.Append);
                }
                streamWriter = new StreamWriter(fileStream);
                streamWriter.WriteLine(messageBuilder.ToString());
                streamWriter.WriteLine(" ");
            }
            finally
            {
                if (streamWriter != null) streamWriter.Close();
                if (fileStream != null) fileStream.Close();
            }
        }

        /// <summary>
        /// This method is for Deleting the Log file earlier than 7 days
        /// </summary>
        public static void DeleteLog()
        {
            try
            {
                string root = Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location));
                //string root = Environment.CurrentDirectory;
                string logFilePath = root + "\\";
                logFilePath = System.IO.Path.Combine(logFilePath, "DigiLogError");


                if (logFilePath.Equals(""))
                    return;
                string[] files = Directory.GetFiles(logFilePath);

                foreach (string file in files)
                {
                    FileInfo fi = new FileInfo(file);
                    if (fi.CreationTime < DateTime.Now.AddDays(-7))
                        fi.Delete();
                }


            }
            catch (Exception ex)
            {

            }
        }
    }

}
