﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DigiPhoto.LogEnvelop.Data;
using DigiPhoto.LogEnvelop.Logger;

namespace ErrorHandler
{
    public class ErrorLogger
    {
        public static void WriteException(Exception ex, LoggingLevel level = LoggingLevel.Info)
        {
            ExceptionLogger.Instance().LogException(ex,
                                       LoggingLevel.Info);
        }

        public static void WriteError(string error, LoggingLevel level = LoggingLevel.Info)
        {
            ExceptionLogger.Instance().LogException(error,
                                       LoggingLevel.Info);
        }
    }
}
