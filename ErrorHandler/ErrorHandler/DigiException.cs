﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace ErrorHandler
{
    [Serializable]
   public class DigiException : Exception
    {
       public DigiException()
        : base() {

       
       }
    
    public DigiException(string message)
        : base(message) 
    {

    }
    
    public DigiException(string format, params object[] args)
        : base(string.Format(format, args))
    {
            
    }
    
    public DigiException(string message, Exception innerException)
        : base(message, innerException) 
    {
        
    }
    
    public DigiException(string format, Exception innerException, params object[] args)
        : base(string.Format(format, args), innerException)
    {
    }

    protected DigiException(SerializationInfo info, StreamingContext context)
        : base(info, context) { }
    }
}
