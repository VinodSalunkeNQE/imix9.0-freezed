﻿using RestSharp;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DigiAzureUploadServiceFrist
{
    class APIWrapper
    {
        string _url;
        private static APIWrapper instance = null;
        private static readonly object obj = new object();

        string Client_Authorization = "#$%85RT";

        private APIWrapper()
        {
            if (ConfigurationManager.AppSettings["URL"] != null)
            {
                this._url = Convert.ToString(ConfigurationManager.AppSettings["URL"]);
                if (string.IsNullOrEmpty(this._url))
                    this._url = "https://ubrapi-digiphotoglobal-com-staging.azurewebsites.net/api/v1/imix/";
            }

            if (ConfigurationManager.AppSettings["Authorization"] != null)
            {
                string _authorization = Convert.ToString(ConfigurationManager.AppSettings["Authorization"]);
                if (!string.IsNullOrEmpty(_authorization))
                    this.Client_Authorization = _authorization;
            }
        }
        public static APIWrapper GetInstance
        {
            get
            {
                lock (obj)
                {
                    if (instance == null)
                        instance = new APIWrapper();
                }
                return instance;
            }
        }
        public string PostData(string JsonRequest, string tblName)
        {
            string result;
            try
            {
                JsonRequest = JsonRequest.Replace("[", "");
                JsonRequest = JsonRequest.Replace("]", "");
                string url = _url + tblName;
                RestClient client = new RestClient(url);
                client.Timeout = -1;
                var request = new RestRequest(Method.POST);
                //request.AddHeader("Authorization", "Basic " + this.Client_Authorization);
                request.AddHeader("ApiKey", this.Client_Authorization);
                request.AddHeader("Content-Type", "application/json");
                request.AddParameter("application/x-www-form-urlencoded", JsonRequest, ParameterType.RequestBody);
                request.RequestFormat = DataFormat.Json;
                ErrorHandler.ErrorHandler.LogFileWrite("Json Data For " + tblName + " Json : " + JsonRequest);
                IRestResponse response = client.Execute(request);
                if (tblName == "Photo")
                    //  = Newtonsoft.Json.JsonConvert.DeserializeObject();
                    result = response.StatusCode.ToString() + ";" + response.Content.ToString();
                else if (tblName == "WebPhotos")
                    result = response.StatusCode.ToString() + ";" + response.Content.ToString();
                else
                    result = response.StatusCode.ToString();
                ErrorHandler.ErrorHandler.LogFileWrite(response.Content);

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                result = "Error";
            }
            return result;
        }
    }

    public class photos
    {
        public string FileName { get; set; }
        public string RFID { get; set; }
        public string Frame { get; set; }
        public string Background { get; set; }
        public string Layering { get; set; }
        public string Effects { get; set; }
        public bool IsCroped { get; set; }
        public bool IsGreen { get; set; }
        public string MetaData { get; set; }
        public string Size { get; set; }
        public bool IsRedEye { get; set; }
        public bool IsArchived { get; set; }
        public string LocationId { get; set; }
        public bool Active { get; set; }
        public int CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public string SubStoreId { get; set; }
        public int IMIXPhotoId { get; set; }
        public DateTime CreatedOnMIX { get; set; }
        public string LayeringObjects { get; set; }
        public string Dimention { get; set; }
        public DateTime CaptureDate { get; set; }
        public decimal GpsLatitude { get; set; }
        public decimal GpsLongitude { get; set; }
        public int MediaType { get; set; }
        public bool IsBorder { get; set; }
        public bool IsGraphics { get; set; }
        public bool IsBackground { get; set; }
        public int VideoLength { get; set; }
        public bool IsSyncedtoIMSDB { get; set; }
    }

    public class webphoto
    {
        public int PhotoId { get; set; }
        public string OrderNumber { get; set; }
        public string IdentificationCode { get; set; }
        public string LayeringObjects { get; set; }
        public string Dimention { get; set; }
        public bool EditedOnMobile { get; set; }
        public DateTime EditedDate { get; set; }
        public DateTime LayeringObjectsEditedOn { get; set; }
        public string CloudImageUrl { get; set; }
        public int Height { get; set; }
        public int Width { get; set; }
        public string ThumbnailDimension { get; set; }
        public bool IsPaidImage { get; set; }
        public string CloudSourceImageURL { get; set; }
    }

    public class CloudinaryDtl
    {
        public int PartnerId { get; set; }
        public int WebPhotoID { get; set; }
        public string SourceImageID { get; set; }
        public string CloudinaryPublicID { get; set; }
        public int CloudinaryStatusID { get; set; }
        public int RetryCount { get; set; }
        public string ErrorMessage { get; set; }
        public string AddedBy { get; set; }
        public DateTime CreatedDateTime { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime ModifiedDateTime { get; set; }
        public bool IsActive { get; set; }
    }

    public class iMixImagesAssociation
    {
        public long IMIXImageAssociationId { get; set; }
        public long IMIXCardTypeId { get; set; }
        public long PhotoId { get; set; }
        public string CardUniqueIdentifier { get; set; }
        public string MappedIdentifier { get; set; }
        public DateTime ModifiedDate { get; set; }
        public int IsOrdered { get; set; }
        public int RfidIdentifierId { get; set; }
        public int IsMoved { get; set; }
        public int Nationality { get; set; }
        public string Email { get; set; }
    }
}
