﻿using DigiPhoto.IMIX.Business;
using log4net;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;

using Microsoft.Azure.Management.Fluent;//Install-Package Microsoft.Azure.Management.Fluent -Version 1.32.0
using Microsoft.Azure.Management.ResourceManager.Fluent;
using Microsoft.Azure.Management.ResourceManager.Fluent.Core;

using Microsoft.Azure.KeyVault;
using Microsoft.IdentityModel.Clients.ActiveDirectory;
using RestSharp;//Install-Package RestSharp -Version 106.10.1
using Newtonsoft.Json;

using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using System.IO;
using System.Collections;
using DigiPhoto.DailySales.DataAccess.DataModels;
using DigiPhoto.IMIX.Model;
using DigiPhoto.DailySales.ClientDataAccess.DataModels;
using DigiPhoto.IMIX.DataAccess;
using System.Threading;
using System.Configuration;
using DigiPhoto.IMIX.Model.Base;
using Newtonsoft.Json.Linq;

namespace DigiAzureUploadServiceFrist
{
    public partial class Service1 : ServiceBase
    {
        private static ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        static System.Timers.Timer timer;
        public int partnerID;
        public string countryPrefix;
        public Service1()
        {
            InitializeComponent();
            if (ConfigurationManager.AppSettings["PartnerId"] != null)
            {
                this.partnerID = Convert.ToInt32(ConfigurationManager.AppSettings["PartnerId"]);
                if (string.IsNullOrEmpty(this.partnerID.ToString()))
                    this.partnerID = 34;
            }
            if (ConfigurationManager.AppSettings["CountryPrefix"] != null)
            {
                this.countryPrefix = Convert.ToString(ConfigurationManager.AppSettings["CountryPrefix"]);
                if (string.IsNullOrEmpty(this.countryPrefix))
                    this.countryPrefix = "stagedei";
            }
        }

        protected override void OnStart(string[] args)
        {
            try
            {
                ErrorHandler.ErrorHandler.LogFileWrite("OnStart");
                ServicePosInfoBusiness svcPosinfoBusiness = new ServicePosInfoBusiness();
                string ret = svcPosinfoBusiness.ServiceStart(false);
                ErrorHandler.ErrorHandler.LogFileWrite("line 71:");
                if (string.IsNullOrEmpty(ret))
                {
                    ErrorHandler.ErrorHandler.LogFileWrite("Upload now");

                    ErrorHandler.ErrorHandler.LogFileWrite("UploadToAzureBlob() line 60 start ");
                    StartScheduler();
                }
                else
                {
                    throw new Exception("Already Started");
                }
                ErrorHandler.ErrorHandler.LogFileWrite("Line Number 100:");
            }
            catch (Exception ex)
            {
                log.Error("OnStart:: Message: " + ex.Message + " StackTrace: " + ex.StackTrace + " InnerException: " + ex.InnerException + " Source: " + ex.Source);
                this.Stop();
            }
        }

        protected override void OnStop()
        {
            ServicePosInfoBusiness svcPosinfoBusiness = new ServicePosInfoBusiness();
            svcPosinfoBusiness.StopService(false);
        }
               
        public void SyncOrderToBlob()
        {
            try
            {

                ServicePosInfoBusiness svcPosinfoBusiness = new ServicePosInfoBusiness();
                string ret = svcPosinfoBusiness.ServiceStart(false);

                if (string.IsNullOrEmpty(ret))
                {
                    //this.timer = new System.Timers.Timer();
                    //this.timer.Interval = 1000 * 5;
                    //this.timer.AutoReset = true;
                    //this.timer.Elapsed += new System.Timers.ElapsedEventHandler(this.timer_Elapsed);
                    //this.timer.Enabled = false;
                    //this.timer.Start();
                    //timer.Elapsed += new System.Timers.ElapsedEventHandler(timer_Elapsed);
                    //timer.Start();
                }
                else
                {
                    throw new Exception("Already Started");
                }
            }
            catch (Exception ex)
            {
                log.Error("OnStart:: Message: " + ex.Message + " StackTrace: " + ex.StackTrace + " InnerException: " + ex.InnerException + " Source: " + ex.Source);
                this.Stop();
            }

            //bool IsSyncEnabled = Convert.ToBoolean(System.Configuration.ConfigurationManager.AppSettings["IsSet3Enabled"]);
            //if (!IsSyncEnabled)
            //{
            //    log.Info("Set 3 is not Enabled !");
            //}

            //if (IsSyncEnabled)
            //{
            //    //Pull();
            //    //Push();
            //}
        }

        public void SyncPortalOrderImages()
        {

        }

        public void UploadToAzureBlob()
        {
            try
            {
                ErrorHandler.ErrorHandler.LogFileWrite("UploadToAzureBlob method start ");
                bool isSet3Enabled = false;
                ConfigBusiness conBiz = new ConfigBusiness();
                int set3Enabled = (int)conBiz.GETSet3EnabledID();

                ErrorHandler.ErrorHandler.LogFileWrite("set3Enabled =  " + Convert.ToString(set3Enabled));
                if (set3Enabled > 0)
                {
                    ErrorHandler.ErrorHandler.LogFileWrite("156 ");
                    Dictionary<long, string> iMIXConfigurations = new Dictionary<long, string>();
                    iMIXConfigurations = conBiz.GetConfigurations(iMIXConfigurations, 1);
                    ErrorHandler.ErrorHandler.LogFileWrite("158 ");
                    string val = iMIXConfigurations.Where(c => c.Key == set3Enabled).Select(c => c.Value).FirstOrDefault();
                    ErrorHandler.ErrorHandler.LogFileWrite("161 " + val);
                    if (val == "True")
                    {
                        ErrorHandler.ErrorHandler.LogFileWrite("set3Enabled =  " + Convert.ToString(set3Enabled));
                        isSet3Enabled = true;
                        ErrorHandler.ErrorHandler.LogFileWrite("166 ");
                    }
                    else
                    {
                        ErrorHandler.ErrorHandler.LogFileWrite("169 ");
                        return;
                    }
                }
                else
                {
                    ErrorHandler.ErrorHandler.LogFileWrite("Set3 is not enabled, no need to upload images to azure cloud");

                    return;//Set3 is not enabled, no need to upload images to azure cloud
                }
                //UploadPhotosAndSaveinPhotosIMS();

                StoreSubStoreDataBusniess stoObj = new StoreSubStoreDataBusniess();
                Hashtable htStoreInfo = stoObj.GetStoreDetails();

                /*
            *   The Purpose of this service is to demostrate DEI Phase3 Migration to Azure 
            * 
            */

                bool isSubscribedToEventGrid = false; //This has to be done only once ,save this info to DB
                bool isLifeCyclePolicySet = false; //This has to be done only once ,save this info to DB               
                string DB_Variable_storageaccountname = string.Empty;
                string DB_Variable_connectionsstring = string.Empty;
                string DB_Variable_containerName = string.Empty;
                string strLifeCyclePolicy = string.Empty;
                string strSubscribedEventGrid = string.Empty;
                string strCountry = string.Empty;

                // here country code bhavin
                //htStoreInfo.ContainsKey("Country") = 
                //DB_Variable_storageaccountname = "stagdeiindia";   // commited by Suraj
                DB_Variable_storageaccountname = htStoreInfo.ContainsKey("Country") ? countryPrefix + htStoreInfo["Country"].ToString().ToLower() : "stagdeinewdubai";  // uncommited by Suraj
                //DB_Variable_containerName = "containerinida"; // commited by Suraj
                DB_Variable_containerName = htStoreInfo.ContainsKey("Store") ? htStoreInfo["Store"].ToString().ToLower() : ""; //Store name is nothing but Venue name // uncommited by Suraj
                strCountry = Convert.ToString(htStoreInfo["Country"]).ToLower();
                // Add For Remove Space And Add "-".  As Discuss with Jijo and Sona
                DB_Variable_storageaccountname = DB_Variable_storageaccountname.Replace(" ", "");
                DB_Variable_containerName = DB_Variable_containerName.Replace(" ", "-");
                // End Changes..  By Suraj
                ErrorHandler.ErrorHandler.LogFileWrite("DB_Variable_storageaccountname =  " + Convert.ToString(DB_Variable_storageaccountname));
                ErrorHandler.ErrorHandler.LogFileWrite("DB_Variable_containerName =  " + Convert.ToString(DB_Variable_containerName));
                ErrorHandler.ErrorHandler.LogFileWrite("strCountry =  " + Convert.ToString(strCountry));

                List<string> lstConfg = stoObj.GetStoreAzureConfiguration(DB_Variable_storageaccountname);

                //string path = Path.Combine(Environment.CurrentDirectory, "azureauth.properties");  // uncommited by Suraj
                string path = Path.Combine(@"C:\Program Files (x86)\iMix\AzureUploadService", "azureauth.properties"); // commited by Suraj

                var credentials = SdkContext
                                   .AzureCredentialsFactory
                                   .FromFile(path);

                var azure = Azure
                            .Configure()
                            .WithLogLevel(HttpLoggingDelegatingHandler.Level.Basic)
                            .Authenticate(credentials)
                            .WithDefaultSubscription();
                // var location = Microsoft.Azure.Management.ResourceManager.Fluent.Core.Region.EuropeWest;
                var location = Microsoft.Azure.Management.ResourceManager.Fluent.Core.Region.ChinaEast2;
                string rgName = System.Configuration.ConfigurationManager.AppSettings["resourceGroupName"];

                //Check if Azure configurations are saved in DB, if not create and save it
                if (lstConfg != null && lstConfg.Count() > 0)
                {
                    DB_Variable_storageaccountname = lstConfg[0];
                    DB_Variable_connectionsstring = lstConfg[3];//Azure connection string
                    ErrorHandler.ErrorHandler.LogFileWrite("DB_Variable_storageaccountname =  " + Convert.ToString(DB_Variable_storageaccountname));
                    //Check if Connectionstring is null and then update conn string for storage account in DB table
                    if (lstConfg[2] != "")
                    {
                        isSubscribedToEventGrid = true;
                    }
                    else
                    {
                        Microsoft.Azure.Management.Storage.Fluent.CheckNameAvailabilityResult isAccountAvailableRes = azure.StorageAccounts.CheckNameAvailability(DB_Variable_storageaccountname);//Vins
                                                                                                                                                                                                  //Create Azure storage account if not created earlier
                        if ((bool)isAccountAvailableRes.IsAvailable)
                        {
                            var storageAccount = azure
                                                    .StorageAccounts.Define(DB_Variable_storageaccountname)
                                                    .WithRegion(location)
                                                    .WithNewResourceGroup(rgName)
                                                    .WithGeneralPurposeAccountKindV2()
                                                    .Create();

                            var storageAccountKeys = storageAccount.GetKeys();

                            //Console.WriteLine("Saving Connection Strings to KeyVault.");
                            KeyVaultClient kvClient = new KeyVaultClient(async (authority, resource, scope) =>
                            {
                                var adCredential = new ClientCredential(System.Configuration.ConfigurationManager.AppSettings["AZURE_CLIENT_ID"],
                                                                        System.Configuration.ConfigurationManager.AppSettings["AZURE_CLIENT_SECRET"]);
                                var authenticationContext = new AuthenticationContext(authority, null);
                                return (await authenticationContext.AcquireTokenAsync(resource, adCredential)).AccessToken;
                            });
                            var i = 1;

                            foreach (var batchGroup in storageAccountKeys)
                            {
                                //Save it into Keyvault
                                DB_Variable_connectionsstring = "DefaultEndpointsProtocol=https;AccountName=" + DB_Variable_storageaccountname +
                                    ";AccountKey=" + batchGroup.Value +
                                    ";EndpointSuffix=core.windows.net";
                                /*Note:- EndpointSuffix has to be replaced (Default is kept for now.)
                                    AzureChinaCloud   core.chinacloudapi.cn
                                    AzureCloud        core.windows.net
                                    AzureGermanCloud  core.cloudapi.de
                                    AzureUSGovernment core.usgovcloudapi.net
                                */
                                var secretName = DB_Variable_storageaccountname + "-connectionstring" + i; //Do not change this secretname template 
                                var kvURL = System.Configuration.ConfigurationManager.AppSettings["AZURE_KEY_VAULT_URL"];// Environment.GetEnvironmentVariable("AZURE_KEY_VAULT_URL");
                                i = i + 1;
                                var result = SetSecret(kvClient, kvURL, secretName, DB_Variable_connectionsstring);
                            }

                            // Console.WriteLine("Creating Life cycle Policy");
                            if (isLifeCyclePolicySet == false)
                            {
                                string token = GetAuthorizationHeader().Result;
                                var client = new RestSharp.RestClient("https://management.azure.com/subscriptions/" + System.Configuration.ConfigurationManager.AppSettings["AZURE_SUBSCRIPTION_ID"] +
                                    "/resourceGroups/" + System.Configuration.ConfigurationManager.AppSettings["resourceGroupName"] +
                                    "/providers/Microsoft.Storage/storageAccounts/" + DB_Variable_storageaccountname + "/managementPolicies/default?api-version=2019-06-01");
                                var request = new RestRequest(Method.PUT);
                                request.AddHeader("cache-control", "no-cache");
                                request.AddHeader("authorization", "Bearer " + token);
                                request.AddHeader("accept", "application/json; charset=utf-8");
                                string lifecyclepolicyname = DB_Variable_storageaccountname + "-policy1";
                                //replace with appropriate lifecyclepolicyname like policy1 or venue_name for each venue , since a storage account is for each country 
                                // and each container is a venue you can use the prefixMatch for each venue to demarcate how lifecycle should behave
                                // like for venue1 default delete is 30 (media folder) other venue is (60 days) like that. Place appropriately.
                                //Template prefixMatch :- container-name/media or container-name/transformations
                                //Console.Write("Enter Storage Account Name:- ");
                                //DB_Variable_storageaccountname = "stagedeivins";// Console.ReadLine();
                                //Console.Write("Enter Container Name:- ");
                                string container = DB_Variable_containerName;// Console.ReadLine();
                                                                             //Console.Write("Enter Folder name(media/transformations):- ");
                                string folder = "media";// Console.ReadLine();
                                string prefixMatch = container + "/" + folder;
                                //leave actions in body empty if don't want to apply. a template is given below , replace or remove from `actions` options that are not necessary like snapshort delete option.
                                var body = "{\"properties\":{\"policy\":{\"rules\": [{\"enabled\": true,\"name\": \"" + lifecyclepolicyname + "\",    \"type\": \"Lifecycle\",    \"definition\": { \"filters\": { \"blobTypes\": [   \"blockBlob\" ], \"prefixMatch\": [   \"" + prefixMatch + "\" ] }, \"actions\": { \"baseBlob\": {   \"tierToCool\": {\"daysAfterModificationGreaterThan\": 30   },   \"tierToArchive\": {\"daysAfterModificationGreaterThan\": 90   },   \"delete\": {\"daysAfterModificationGreaterThan\": 1000   } }, \"snapshot\": {   \"delete\": {\"daysAfterCreationGreaterThan\": 30   } } }    }  }]}   } }";

                                request.AddParameter("application/json", body, ParameterType.RequestBody);
                                IRestResponse response = client.Execute(request);

                                strLifeCyclePolicy = response.Content;
                                //Console.WriteLine(response.Content);
                                //Save response to DB_VINS

                                isLifeCyclePolicySet = true;
                            }

                            //-----------------------------------//
                            //Console.WriteLine("Subscribing Event Grid");
                            //Console.Write("Set eventgrid to storage account " + DB_Variable_storageaccountname + " ?: ");
                            //DB_Variable_storageaccountname = "stagedeivins";
                            if (isSubscribedToEventGrid == false)
                            {
                                string token = GetAuthorizationHeader().Result;
                                var client = new RestSharp.RestClient("https://management.azure.com/subscriptions/" + System.Configuration.ConfigurationManager.AppSettings["AZURE_SUBSCRIPTION_ID"] + "/" +
                                    "resourceGroups/" + System.Configuration.ConfigurationManager.AppSettings["resourceGroupName"] + "/providers/Microsoft.Storage/storageaccounts/" + DB_Variable_storageaccountname +
                                    "/providers/Microsoft.EventGrid/eventSubscriptions/" + DB_Variable_storageaccountname + "-events?api-version=2019-01-01");
                                var request = new RestRequest(Method.PUT);//Updates If not exists
                                request.AddHeader("cache-control", "no-cache");
                                request.AddHeader("authorization", "Bearer " + token);
                                request.AddHeader("accept", "application/json; charset=utf-8");
                                //endpointUrl would change for Production.
                                //https://{replace function endpoint url}/runtime/webhooks/EventGrid?functionName={replace function name}&code={replace function code}
                                // Code can be obtained from portal.Use Master key.
                                //var body = "{\"properties\":{\"destination\": {\"endpointType\": \"WebHook\",\"properties\": {\"endpointUrl\": \"https://stagdeieventgrid.azurewebsites.net/runtime/webhooks/EventGrid?functionName=auto_thumbnail&code=lSgwnQcSSUfEnYd7ynxQIO7/WWoWiEthGLDVI2JcZ53V0xH2d8JFJA==\"}}}}";
                                var body = "{\"properties\":{\"destination\": {\"endpointType\": \"WebHook\",\"properties\": {\"endpointUrl\": \"https://stagdeieventgrid.azurewebsites.cn/runtime/webhooks/EventGrid?functionName=auto_thumbnail&code=lSgwnQcSSUfEnYd7ynxQIO7/WWoWiEthGLDVI2JcZ53V0xH2d8JFJA==\"}}}}";

                                request.AddParameter("application/json", body, ParameterType.RequestBody);
                                IRestResponse response = client.Execute(request);

                                strSubscribedEventGrid = response.Content;
                                //Console.WriteLine(response.Content);
                                //Console.ReadKey();
                                isSubscribedToEventGrid = true;
                            }
                            else
                            {
                                //Console.WriteLine("Skipping since already subscribed");
                            }
                            //Save to Database
                            stoObj.SaveStoreAzureDetails(DB_Variable_storageaccountname, strLifeCyclePolicy, strSubscribedEventGrid, DB_Variable_connectionsstring);
                        }//if ended for storage account creation
                        else
                        {  // Storage Account Allready Created need to Create Connection Start Here.
                            var storageAccount = azure
                                                   .StorageAccounts.Define(DB_Variable_storageaccountname)
                                                   .WithRegion(location)
                                                   .WithNewResourceGroup(rgName)
                                                   .WithGeneralPurposeAccountKindV2()
                                                   .Create();

                            var storageAccountKeys = storageAccount.GetKeys();

                            //Console.WriteLine("Saving Connection Strings to KeyVault.");
                            KeyVaultClient kvClient = new KeyVaultClient(async (authority, resource, scope) =>
                            {
                                var adCredential = new ClientCredential(System.Configuration.ConfigurationManager.AppSettings["AZURE_CLIENT_ID"],
                                                                        System.Configuration.ConfigurationManager.AppSettings["AZURE_CLIENT_SECRET"]);
                                var authenticationContext = new AuthenticationContext(authority, null);
                                return (await authenticationContext.AcquireTokenAsync(resource, adCredential)).AccessToken;
                            });
                            var i = 1;

                            foreach (var batchGroup in storageAccountKeys)
                            {
                                //Save it into Keyvault
                                DB_Variable_connectionsstring = "DefaultEndpointsProtocol=https;AccountName=" + DB_Variable_storageaccountname +
                                    ";AccountKey=" + batchGroup.Value +
                                    ";EndpointSuffix=core.windows.net";
                                /*Note:- EndpointSuffix has to be replaced (Default is kept for now.)
                                    AzureChinaCloud   core.chinacloudapi.cn
                                    AzureCloud        core.windows.net
                                    AzureGermanCloud  core.cloudapi.de
                                    AzureUSGovernment core.usgovcloudapi.net
                                */
                                var secretName = DB_Variable_storageaccountname + "-connectionstring" + i; //Do not change this secretname template 
                                var kvURL = System.Configuration.ConfigurationManager.AppSettings["AZURE_KEY_VAULT_URL"];// Environment.GetEnvironmentVariable("AZURE_KEY_VAULT_URL");
                                i = i + 1;
                                var result = SetSecret(kvClient, kvURL, secretName, DB_Variable_connectionsstring);
                            }

                            // Console.WriteLine("Creating Life cycle Policy");
                            if (isLifeCyclePolicySet == false)
                            {
                                string token = GetAuthorizationHeader().Result;
                                var client = new RestSharp.RestClient("https://management.azure.com/subscriptions/" + System.Configuration.ConfigurationManager.AppSettings["AZURE_SUBSCRIPTION_ID"] +
                                    "/resourceGroups/" + System.Configuration.ConfigurationManager.AppSettings["resourceGroupName"] +
                                    "/providers/Microsoft.Storage/storageAccounts/" + DB_Variable_storageaccountname + "/managementPolicies/default?api-version=2019-06-01");
                                var request = new RestRequest(Method.PUT);
                                request.AddHeader("cache-control", "no-cache");
                                request.AddHeader("authorization", "Bearer " + token);
                                request.AddHeader("accept", "application/json; charset=utf-8");
                                string lifecyclepolicyname = DB_Variable_storageaccountname + "-policy1";

                                string container = DB_Variable_containerName;
                                string folder = "media";
                                string prefixMatch = container + "/" + folder;
                                //leave actions in body empty if don't want to apply. a template is given below , replace or remove from `actions` options that are not necessary like snapshort delete option.
                                var body = "{\"properties\":{\"policy\":{\"rules\": [{\"enabled\": true,\"name\": \"" + lifecyclepolicyname + "\",    \"type\": \"Lifecycle\",    \"definition\": { \"filters\": { \"blobTypes\": [   \"blockBlob\" ], \"prefixMatch\": [   \"" + prefixMatch + "\" ] }, \"actions\": { \"baseBlob\": {   \"tierToCool\": {\"daysAfterModificationGreaterThan\": 30   },   \"tierToArchive\": {\"daysAfterModificationGreaterThan\": 90   },   \"delete\": {\"daysAfterModificationGreaterThan\": 1000   } }, \"snapshot\": {   \"delete\": {\"daysAfterCreationGreaterThan\": 30   } } }    }  }]}   } }";

                                request.AddParameter("application/json", body, ParameterType.RequestBody);
                                IRestResponse response = client.Execute(request);

                                strLifeCyclePolicy = response.Content;

                                isLifeCyclePolicySet = true;
                            }

                            if (isSubscribedToEventGrid == false)
                            {
                                string token = GetAuthorizationHeader().Result;
                                var client = new RestSharp.RestClient("https://management.azure.com/subscriptions/" + System.Configuration.ConfigurationManager.AppSettings["AZURE_SUBSCRIPTION_ID"] + "/" +
                                    "resourceGroups/" + System.Configuration.ConfigurationManager.AppSettings["resourceGroupName"] + "/providers/Microsoft.Storage/storageaccounts/" + DB_Variable_storageaccountname +
                                    "/providers/Microsoft.EventGrid/eventSubscriptions/" + DB_Variable_storageaccountname + "-events?api-version=2019-01-01");
                                var request = new RestRequest(Method.PUT);//Updates If not exists
                                request.AddHeader("cache-control", "no-cache");
                                request.AddHeader("authorization", "Bearer " + token);
                                request.AddHeader("accept", "application/json; charset=utf-8");

                                var body = "{\"properties\":{\"destination\": {\"endpointType\": \"WebHook\",\"properties\": {\"endpointUrl\": \"https://stagdeieventgrid.azurewebsites.net/runtime/webhooks/EventGrid?functionName=auto_thumbnail&code=lSgwnQcSSUfEnYd7ynxQIO7/WWoWiEthGLDVI2JcZ53V0xH2d8JFJA==\"}}}}";

                                request.AddParameter("application/json", body, ParameterType.RequestBody);
                                IRestResponse response = client.Execute(request);

                                strSubscribedEventGrid = response.Content;
                                //Console.WriteLine(response.Content);
                                //Console.ReadKey();
                                isSubscribedToEventGrid = true;
                            }
                            else
                            {
                                //Console.WriteLine("Skipping since already subscribed");
                            }
                            //Save to Database
                            stoObj.SaveStoreAzureDetails(DB_Variable_storageaccountname, strLifeCyclePolicy, strSubscribedEventGrid, DB_Variable_connectionsstring);
                        } // Storage Account Allready Created need to Create Connection End Here.
                    }
                }
                else
                {
                    //Save to Database
                    stoObj.SaveStoreAzureDetails(DB_Variable_storageaccountname, strLifeCyclePolicy, strSubscribedEventGrid, DB_Variable_connectionsstring);
                }

                if (isSubscribedToEventGrid == false)
                {
                    //Console.WriteLine();
                    //Console.Write("Event Grid not subcribed. If already subscribed previously (press Y and continue) else (press N and subscribe using previous menu.) ");
                    //string op_2 = Console.ReadLine();
                    //if (op_2 == "y" || op_2 == "Y")
                    //{
                    //    isSubscribedToEventGrid = true;
                    //}
                }
                if (isSubscribedToEventGrid == true)
                {
                    //Console.WriteLine("Uploading Images");
                    if (DB_Variable_connectionsstring == "")
                    {
                        // Console.WriteLine("connectionsstring of storage account is empty. possible options [storage account not created | fetching connectionsstring for already created from your db failed] ");
                        return;
                    }
                    CloudStorageAccount storageAccount_client = CloudStorageAccount.Parse(DB_Variable_connectionsstring);
                    CloudBlobClient blobClient = storageAccount_client.CreateCloudBlobClient();

                    //Console.Write("Enter Venue name:- ");
                    string containerName = DB_Variable_containerName.Replace(" ", string.Empty);// Console.ReadLine();
                    CloudBlobContainer container = blobClient.GetContainerReference(containerName);
                    try
                    {
                        container.CreateIfNotExists();

                    }
                    catch (StorageException ex)
                    {
                        ErrorHandler.ErrorHandler.LogFileWrite(" Message: " + ex.Message + " StackTrace: " + ex.StackTrace + " InnerException: " + ex.InnerException + " Source: " + ex.Source);
                        // Ensure that the DB_Variable_connectionsstring is correct or container name is correct.
                        //Console.WriteLine("Exception " + ex);
                        //Console.ReadLine();
                    }
                    ErrorHandler.ErrorHandler.LogFileWrite(" Line Number : 524");
                    //Generate an ad-hoc SAS URI for the container. The ad-hoc SAS has write , read and list and write permissions.
                    string adHocContainerSAS = GetContainerSasUri(container);

                    UploadPhotosAndSaveinPhotosIMS(Convert.ToString(container.Uri), adHocContainerSAS, containerName, DB_Variable_storageaccountname);

                    //string source_dir = @"D:\DigiImages\20200422";//replace with image folder path //DateTime.Now.ToString("yyyy'-'MM'-'dd'")
                    //string dest_dir = container.Uri + "/media/" + "" + "localsite/2020-05-18/" + adHocContainerSAS; // media is a must , rest can be configured
                    //string include_files = "m_2244748.jpg;m_2244751.jpg"; // Minified has to be prepended with m_ (must) original with o_

                    //ProcessStartInfo startInfo = new ProcessStartInfo();
                    //startInfo.CreateNoWindow = false;
                    //startInfo.UseShellExecute = false;
                    ////startInfo.FileName = "D:\\DEI\\Phase 2\\azcopy.exe";//replace with azcopy path
                    //startInfo.FileName = Path.Combine(Environment.CurrentDirectory, "azcopy_windows_amd64_10.3.4", "azcopy.exe");
                    //startInfo.WindowStyle = ProcessWindowStyle.Hidden;
                    //startInfo.Arguments = "sync \"" + source_dir + "\" \"" + dest_dir + "\"" + " --include-pattern=\"" + include_files + "\"";
                    //try
                    //{
                    //    // Start the process with the info we specified.
                    //    // Call WaitForExit and then the using statement will close.
                    //    using (Process exeProcess = Process.Start(startInfo))
                    //    {
                    //        string output = exeProcess.StandardOutput.ReadToEnd();
                    //        exeProcess.WaitForExit();
                    //        log.Error("Azure: Log info :" + output);
                    //    }
                    //}
                    //catch (Exception ex)
                    //{
                    //    // Log error.
                    //    //Console.WriteLine(ex);
                    //}
                }

                //------------------------------------------//
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogFileWrite("OnStart:: Message: " + ex.Message + " StackTrace: " + ex.StackTrace + " InnerException: " + ex.InnerException + " Source: " + ex.Source);
            }
        }

        static async Task<bool> SetSecret(KeyVaultClient kvClient, string kvURL, string secretName, string secretValue)
        {
            // <setsecret>
            await kvClient.SetSecretAsync($"{kvURL}", secretName, secretValue);
            // </setsecret>

            return true;
        }

        private static async Task<string> GetAuthorizationHeader()
        {
            string applicationId = System.Configuration.ConfigurationManager.AppSettings["AZURE_CLIENT_ID"];
            string password = System.Configuration.ConfigurationManager.AppSettings["AZURE_CLIENT_SECRET"];
            string tenantId = System.Configuration.ConfigurationManager.AppSettings["AZURE_TENANT_ID"];
            ClientCredential cc = new ClientCredential(applicationId, password);
            var context = new AuthenticationContext("https://login.windows.net/" + tenantId);
            var result = await context.AcquireTokenAsync("https://management.azure.com/", cc);

            if (result == null)
            {
                throw new InvalidOperationException("Failed to obtain the JWT token");
            }

            string token = result.AccessToken;

            return token;
        }


        static string GetContainerSasUri(CloudBlobContainer container)
        {
            ErrorHandler.ErrorHandler.LogFileWrite(" Line Number ");
            ErrorHandler.ErrorHandler.LogFileWrite("Line Number : 598  Start Time : " + DateTime.UtcNow.AddMinutes(-5).ToString());
            ErrorHandler.ErrorHandler.LogFileWrite("Line Number : 599  Expiry Time : " + DateTime.UtcNow.AddHours(24).ToString());
            string sasContainerToken;
            SharedAccessBlobPolicy adHocPolicy = new SharedAccessBlobPolicy()
            {
                // Set start time to five minutes before now to avoid clock skew.
                SharedAccessStartTime = DateTime.Now.AddHours(-24),
                SharedAccessExpiryTime = DateTime.Now.AddHours(24),
                Permissions = SharedAccessBlobPermissions.Write | SharedAccessBlobPermissions.List | SharedAccessBlobPermissions.Read
            };
            ErrorHandler.ErrorHandler.LogFileWrite("Line Number : 604  SharedAccessExpiryTime : " + adHocPolicy.SharedAccessExpiryTime.ToString());

            ErrorHandler.ErrorHandler.LogFileWrite("Line Number : 606  SharedAccessStartTime : " + adHocPolicy.SharedAccessStartTime.ToString());
            ErrorHandler.ErrorHandler.LogFileWrite("Line Number : 607  Start Time : " + DateTime.UtcNow.AddMinutes(-5).ToString());
            ErrorHandler.ErrorHandler.LogFileWrite("Line Number : 608  Expiry Time : " + DateTime.UtcNow.AddHours(24).ToString());
            sasContainerToken = container.GetSharedAccessSignature(adHocPolicy, null);

            //Return the URI string for the container, including the SAS token.
            return sasContainerToken;
        }


        static string GetContainerSasUri_old(CloudBlobContainer container)
        {
            string sasContainerToken;
            SharedAccessBlobPolicy adHocPolicy = new SharedAccessBlobPolicy()
            {
                // Set start time to five minutes before now to avoid clock skew.
                SharedAccessStartTime = DateTime.UtcNow.AddMinutes(-5),
                SharedAccessExpiryTime = DateTime.UtcNow.AddHours(24),
                Permissions = SharedAccessBlobPermissions.Write | SharedAccessBlobPermissions.List | SharedAccessBlobPermissions.Read
            };
            sasContainerToken = container.GetSharedAccessSignature(adHocPolicy, null);

            //Return the URI string for the container, including the SAS token.
            return sasContainerToken;
        }

        private void UploadPhotosAndSaveinPhotosIMS(string containerURI, string adHocContainerSAS, string venueName, string countryName)
        {
            bool Flag = true;
            ErrorHandler.ErrorHandler.LogFileWrite("UploadPhotosAndSaveinPhotosIMS is Called  ");
            countryName = countryName.Replace(countryPrefix, "");
            PhotoBusiness phBiz = new PhotoBusiness();
            List<PhotoInfo> lstAllLatest = phBiz.SelectAllPhotosToBlobUpload().OrderByDescending(t => t.DG_Photos_CreatedOn).ToList();
            foreach (PhotoInfo photoitem in lstAllLatest)
            {
                string source_dir = string.Empty;
                string minified_source_dir = string.Empty;
                string siteName = string.Empty;
                bool IsVideoFlag = false;
                ConfigBusiness cnfgBusines = new ConfigBusiness();

                siteName = cnfgBusines.GetSubStoreNameBySuubStoreId((Int32)photoitem.DG_SubStoreId);
                var siteWiseListDateWise = photoitem.DG_Photos_CreatedOn.ToString("yyyy/MM/dd");
                try
                {
                    string include_files = string.Join(";", photoitem.DG_Photos_FileName);
                    string includeMinified_files = "m_" + include_files.Replace(";", ";m_");
                    if (photoitem.DG_Photos_FileName.Contains(".mp4"))
                    {
                        IsVideoFlag = true;
                    }
                    if (IsVideoFlag)
                    {
                        source_dir = photoitem.HotFolderPath + "Videos" + "\\" + photoitem.DG_Photos_CreatedOn.ToString("yyyyMMdd");
                        minified_source_dir = photoitem.HotFolderPath + "Videos" + "\\" + photoitem.DG_Photos_CreatedOn.ToString("yyyyMMdd");
                    }
                    else
                    {
                        bool IsGreenscreen = photoitem.DG_Photos_IsGreen.HasValue ? Convert.ToBoolean(photoitem.DG_Photos_IsGreen) : false;
                        if (IsGreenscreen)
                        {
                            source_dir = photoitem.HotFolderPath + "EditedImages";
                            minified_source_dir = photoitem.HotFolderPath + "Minified_Images" + "\\" + photoitem.DG_Photos_CreatedOn.ToString("yyyyMMdd");
                        }
                        else
                        {
                            source_dir = photoitem.HotFolderPath + photoitem.DG_Photos_CreatedOn.ToString("yyyyMMdd");
                            minified_source_dir = photoitem.HotFolderPath + "Minified_Images" + "\\" + photoitem.DG_Photos_CreatedOn.ToString("yyyyMMdd");
                        }
                    }

                    string dest_dir = containerURI + "/media/" + siteName + "/" + photoitem.DG_Photos_CreatedOn.ToString("yyyyMMdd") + "/" + adHocContainerSAS; // media is a must , rest can be configured                   
                    string minified_dest_dir = containerURI + "/media/" + siteName + "/" + photoitem.DG_Photos_CreatedOn.ToString("yyyyMMdd") + "/" + adHocContainerSAS; // media is a must , rest can be configured

                    // Check Original Images are exist or not.
                    if (checkFileExist(source_dir, include_files))
                    {
                        ErrorHandler.ErrorHandler.LogFileWrite("Original Images are Not Found ");
                        Flag = false;
                        return;
                    }
                    // End Changes

                    ProcessStartInfo startInfo = new ProcessStartInfo();
                    startInfo.CreateNoWindow = false;
                    startInfo.UseShellExecute = false;
                    startInfo.RedirectStandardOutput = true;
                    //startInfo.FileName = Path.Combine(Environment.CurrentDirectory, "azcopy_windows_amd64_10.3.4", "azcopy.exe");
                    //startInfo.FileName = Path.Combine(@"C:\Program Files (x86)\iMix", "azcopy_windows_amd64_10.3.4", "azcopy.exe");  // Added by Suraj.
                    startInfo.FileName = Path.Combine(@"C:\Program Files (x86)\IMIXCopy", "azcopy_windows_amd64_10.10.0", "azcopy.exe");   // Added For File modified since transfer scheduled

                    ErrorHandler.ErrorHandler.LogFileWrite("File Path : " + startInfo.FileName);
                    startInfo.WindowStyle = ProcessWindowStyle.Hidden;
                    startInfo.Arguments = "sync \"" + source_dir + "\" \"" + dest_dir + "\"" + " --include-pattern=\"" + include_files + "\"";
                    try
                    {
                        // Start the process with the info we specified.
                        // Call WaitForExit and then the using statement will close.
                        using (Process exeProcess = Process.Start(startInfo))
                        {
                            string output = exeProcess.StandardOutput.ReadToEnd();
                            exeProcess.WaitForExit();
                            log.Error("Azure: Log info about original :" + output);
                            ErrorHandler.ErrorHandler.LogFileWrite("Azure: Log info about original :" + output);
                        }
                    }
                    catch (Exception ex)
                    {
                        log.Error("Azure: Exceptionlog info about original :" + ex.InnerException.ToString());
                        ErrorHandler.ErrorHandler.LogFileWrite("Azure: Exceptionlog info about original :" + ex.InnerException.ToString());
                    }

                    // Check Minified Images are exist or not.
                    if (checkFileExist(minified_source_dir, includeMinified_files))
                    {
                        ErrorHandler.ErrorHandler.LogFileWrite("Minified Images are Not Found ");
                        Flag = false;
                        return;
                    }

                    //upload minified images now..
                    startInfo.Arguments = "sync \"" + minified_source_dir + "\" \"" + minified_dest_dir + "\"" + " --include-pattern=\"" + includeMinified_files + "\"";
                    try
                    {
                        // Start the process with the info we specified.
                        // Call WaitForExit and then the using statement will close.
                        using (Process exeProcess = Process.Start(startInfo))
                        {
                            string output = exeProcess.StandardOutput.ReadToEnd();
                            exeProcess.WaitForExit();
                            log.Error("Azure: Log info about minified :" + output);
                            ErrorHandler.ErrorHandler.LogFileWrite("Azure: Log info about minified :" + output);
                        }
                    }
                    catch (Exception ex)
                    {
                        log.Error("Azure: Exceptionlog info about minified :" + ex.InnerException.ToString());
                        ErrorHandler.ErrorHandler.LogFileWrite("Azure: Exceptionlog info about minified :" + ex.InnerException.ToString());
                    }
                    ErrorHandler.ErrorHandler.LogFileWrite("Images are uploaded ");
                }
                catch (Exception ex)
                {
                    ErrorHandler.ErrorHandler.LogFileWrite("Error Msg : " + ex.Message + "          Stack Trace : " + ex.StackTrace + "   Inner Exception : " + ex.InnerException);
                    log.Error("Azure: Exceptionlog info about minified :" + ex.InnerException.ToString());
                }
                finally
                {
                    ErrorHandler.ErrorHandler.LogFileWrite("Image uploading is " + Flag.ToString());
                }
                if (Flag)
                {
                    if (UploadMetaDeta(photoitem, countryName, venueName, siteName))
                    {
                        ErrorHandler.ErrorHandler.LogFileWrite("Image MetaDeta is updated into Partner DB ");
                    }
                    else
                    {
                        ErrorHandler.ErrorHandler.LogFileWrite("Image MetaDeta is NOT updated into Partner DB ");
                    }
                }
            }
        }

        private bool UploadMetaDeta(PhotoInfo photoitem, string countryName, string venueName, string siteName)
        {
            APIWrapper wrapper = APIWrapper.GetInstance;
            bool isDataUpload = true;
            int RetryConfigCount = 0;
            PhotoBusiness phBiz = new PhotoBusiness();
            if (ConfigurationManager.AppSettings["RetryCount"] != null)
            {
                RetryConfigCount = Convert.ToInt32(ConfigurationManager.AppSettings["RetryCount"]);
            }

            if (phBiz.GetretryFiledCount(photoitem.DG_Photos_RFID, RetryConfigCount))
            {
                string photoFilePath = string.Empty;

                // Photo WebApi Call
                string LocationSynccode = string.Empty;
                string SubstoreSynccode = string.Empty;

                LocationSynccode = phBiz.GetSynccodeValues("dbo.GetLocationNameSyncCode", Convert.ToInt32(photoitem.DG_Location_Id));
                SubstoreSynccode = phBiz.GetSynccodeValues("dbo.GetSubStoreSyncCode", Convert.ToInt32(photoitem.DG_SubStoreId));
                List<photos> photoinfo = new List<photos>();
                var photo = new photos();

                photo.Active = true;
                photo.RFID = photoitem.DG_Photos_RFID;
                photo.CreatedBy = Convert.ToInt32(photoitem.DG_Photos_UserID);
                photo.CreatedDate = photoitem.DG_Photos_CreatedOn;
                photo.CreatedOnMIX = photoitem.DG_Photos_CreatedOn;
                photo.Effects = string.IsNullOrEmpty(photoitem.DG_Photos_Effects) ? "" : photoitem.DG_Photos_Effects;
                photo.FileName = photoitem.DG_Photos_FileName;
                photo.Frame = photoitem.DG_Photos_Frame;
                photo.Background = photoitem.DG_Photos_Background;
                photo.Layering = string.IsNullOrEmpty(photoitem.DG_Photos_Layering) ? "" : photoitem.DG_Photos_Layering;
                photo.GpsLatitude = Convert.ToDecimal(0.0);
                photo.GpsLongitude = Convert.ToDecimal(0.0);
                photo.IMIXPhotoId = photoitem.DG_Photos_pkey;
                photo.SubStoreId = SubstoreSynccode;
                photoFilePath = System.IO.Path.Combine(photoitem.HotFolderPath, photoitem.DG_Photos_CreatedOn.ToString("yyyyMMdd"), photoitem.DG_Photos_FileName);
                if (photoitem.DG_Photos_FileName.ToLower().Contains(".mp4"))
                {
                    photo.MediaType = 2;
                    photoFilePath = System.IO.Path.Combine(photoitem.HotFolderPath, "Videos", photoitem.DG_Photos_CreatedOn.ToString("yyyyMMdd"), photoitem.DG_Photos_FileName);
                    photo.CaptureDate = photoitem.DG_Photos_CreatedOn;
                }
                else
                {
                    photo.MediaType = 1;
                    photo.CaptureDate = Convert.ToDateTime(photoitem.DateTaken);
                }

                photo.IsCroped = Convert.ToBoolean(photoitem.DG_Photos_IsCroped);
                photo.IsGreen = Convert.ToBoolean(photoitem.DG_Photos_IsGreen);
                photo.MetaData = string.IsNullOrEmpty(photoitem.DG_Photos_MetaData) ? "" : photoitem.DG_Photos_MetaData;
                photo.Size = string.IsNullOrEmpty(photoitem.DG_Photos_Sizes) ? "" : photoitem.DG_Photos_Sizes;
                photo.IsRedEye = Convert.ToBoolean(photoitem.DG_Photos_IsRedEye);
                photo.IsArchived = Convert.ToBoolean(photoitem.DG_Photos_Archive);
                photo.LocationId = LocationSynccode;
                photo.LayeringObjects = string.IsNullOrEmpty(photoitem.DG_Photos_Layering) ? "" : photoitem.DG_Photos_Layering;
                photo.Dimention = "";
                photo.IsBorder = false;
                photo.IsGraphics = false;
                photo.IsBackground = string.IsNullOrEmpty(photoitem.DG_Photos_Background) ? false : Convert.ToBoolean(photoitem.DG_Photos_Background);
                photo.VideoLength = Convert.ToInt32(photoitem.DG_VideoLength);
                photo.IsSyncedtoIMSDB = false;

                photoinfo.Add(photo);

                string jsonrequestphoto = Newtonsoft.Json.JsonConvert.SerializeObject(photoinfo);
                string response = wrapper.PostData(jsonrequestphoto, "Photo");
                string[] result = response.Split(';');
                if (result[0].ToString() == "OK")
                {

                    dynamic obj = JObject.Parse(result[1]);
                    photoitem.DG_Photo_ID = obj.PhotoId;
                    if (isDataUpload)
                        isDataUpload = true;
                }
                else
                    isDataUpload = false;

                // WebPhoto WebApi Call

                List<webphoto> webphotoinfo = new List<webphoto>();
                var webphotos = new webphoto();
                ImageAttribute imageAttribute = GetImageHeightWidth(photoFilePath);

                webphotos.OrderNumber = "NA";
                webphotos.IdentificationCode = "NA";
                webphotos.LayeringObjects = string.IsNullOrEmpty(photoitem.DG_Photos_Layering) ? "" : photoitem.DG_Photos_Layering;
                webphotos.Dimention = string.Empty;
                webphotos.PhotoId = photoitem.DG_Photo_ID;
                webphotos.Height = imageAttribute.Height;
                webphotos.Width = imageAttribute.Width;
                webphotos.ThumbnailDimension = string.IsNullOrEmpty(imageAttribute.Dimension) ? "" : imageAttribute.Dimension;
                webphotos.IsPaidImage = false;
                // Added For Space Issue in Cloud URL
                string CloudSourceImageURL = countryName + "/" + venueName + "/" + siteName + "/" + photoitem.DG_Photos_CreatedOn.ToString("yyyyMMdd") + "/" + "m_" + photoitem.DG_Photos_FileName;
                CloudSourceImageURL = CloudSourceImageURL.Replace(" ", "_");
                webphotos.CloudSourceImageURL = CloudSourceImageURL;
                // End Changes
                //webphotos.CloudSourceImageURL = countryName + "/" + venueName + "/" + siteName + "/" + photoitem.DG_Photos_CreatedOn.ToString("yyyyMMdd") + "/" + "m_" + photoitem.DG_Photos_FileName;
                // Added For Space Issue in Cloud URL
                string CloudImageUrl = countryName + "/" + venueName + "/" + siteName + "/" + photoitem.DG_Photos_CreatedOn.ToString("yyyyMMdd") + "/" + photoitem.DG_Photos_FileName;
                CloudImageUrl = CloudImageUrl.Replace(" ", "_");
                webphotos.CloudImageUrl = CloudImageUrl;
                // End Changes
                //webphotos.CloudImageUrl = countryName + "/" + venueName + "/" + siteName + "/" + photoitem.DG_Photos_CreatedOn.ToString("yyyyMMdd") + "/" + photoitem.DG_Photos_FileName;
                webphotos.EditedOnMobile = false;
                webphotos.EditedDate = DateTime.Now;
                webphotos.LayeringObjectsEditedOn = DateTime.Now;

                webphotoinfo.Add(webphotos);

                string jsonrequestwebphoto = Newtonsoft.Json.JsonConvert.SerializeObject(webphotoinfo);
                string responsewebphoto = wrapper.PostData(jsonrequestwebphoto, "WebPhotos");
                string[] resultwebphoto = responsewebphoto.Split(';');
                Int32 webphotoid = 0;
                if (resultwebphoto[0].ToString() == "OK")
                {
                    dynamic obj = JObject.Parse(resultwebphoto[1]);
                    webphotoid = obj.WebPhotoId;
                    if (isDataUpload)
                        isDataUpload = true;
                }
                else
                    isDataUpload = false;

                // Cloudinary Details WebApi Call

                List<CloudinaryDtl> cloudinary = new List<CloudinaryDtl>();
                var CloudinaryDtlv = new CloudinaryDtl();
                CloudinaryDtlv.PartnerId = partnerID;
                CloudinaryDtlv.WebPhotoID = webphotoid;//webphoto.WebPhotoId;
                CloudinaryDtlv.SourceImageID = photoitem.DG_Photos_FileName;
                // Added For Space Issue in Cloud URL
                string CloudinaryPublicID = countryName + "/" + venueName + "/" + siteName + "/" + photoitem.DG_Photos_CreatedOn.ToString("yyyyMMdd") + "/" + photoitem.DG_Photos_FileName;
                CloudinaryPublicID = CloudinaryPublicID.Replace(" ", "_");
                CloudinaryDtlv.CloudinaryPublicID = CloudinaryPublicID;
                // End Changes
                //CloudinaryDtlv.CloudinaryPublicID = countryName + "/" + venueName + "/" + siteName + "/" + photoitem.DG_Photos_CreatedOn.ToString("yyyyMMdd") + "/" + photoitem.DG_Photos_FileName;
                CloudinaryDtlv.CloudinaryStatusID = 1;
                CloudinaryDtlv.RetryCount = 0;
                CloudinaryDtlv.ErrorMessage = "Minified,thumbnail uploaded successfully";
                CloudinaryDtlv.AddedBy = "webusers";
                CloudinaryDtlv.CreatedDateTime = photoitem.DG_Photos_CreatedOn;
                CloudinaryDtlv.ModifiedBy = "webusers";
                CloudinaryDtlv.ModifiedDateTime = DateTime.Now;
                CloudinaryDtlv.IsActive = true;

                cloudinary.Add(CloudinaryDtlv);

                string jsonrequestCloudinary = Newtonsoft.Json.JsonConvert.SerializeObject(cloudinary);
                if (wrapper.PostData(jsonrequestCloudinary, "CloudinaryDtl") == "OK")
                {
                    if (isDataUpload)
                        isDataUpload = true;
                }
                else
                    isDataUpload = false;

                List<AssociateImageDetails> lstAssociateImages = phBiz.GetAssociateImageDetails(photoitem.DG_Photos_pkey.ToString());
                string jsonrequestImgAssociation = string.Empty;
                foreach (var AssociateImage in lstAssociateImages)
                {
                    List<iMixImagesAssociation> ImageAssociation = new List<iMixImagesAssociation>();
                    var ImgAssociation = new iMixImagesAssociation();
                    ImgAssociation.IMIXImageAssociationId = AssociateImage.IMIXImageAssociationId;
                    ImgAssociation.IMIXCardTypeId = AssociateImage.IMIXCardTypeId;
                    ImgAssociation.PhotoId = AssociateImage.PhotoId;
                    ImgAssociation.CardUniqueIdentifier = AssociateImage.CardUniqueIdentifier;
                    ImgAssociation.MappedIdentifier = AssociateImage.MappedIdentifier;
                    ImgAssociation.ModifiedDate = AssociateImage.ModifiedDate;
                    ImgAssociation.IsOrdered = AssociateImage.IsOrdered;
                    ImgAssociation.RfidIdentifierId = AssociateImage.RfidIdentifierId;
                    ImgAssociation.IsMoved = AssociateImage.IsMoved;
                    ImgAssociation.Nationality = AssociateImage.Nationality;
                    ImgAssociation.Email = AssociateImage.Email;

                    ImageAssociation.Add(ImgAssociation);

                    jsonrequestImgAssociation = Newtonsoft.Json.JsonConvert.SerializeObject(ImageAssociation);
                    if (wrapper.PostData(jsonrequestImgAssociation, "iMixImageAssociation") == "OK")
                    {
                        if (isDataUpload)
                            isDataUpload = true;
                    }
                    else
                        isDataUpload = false;
                }

                try
                {
                    StoreSubStoreDataBusniess stoObj = new StoreSubStoreDataBusniess();
                    if (isDataUpload)
                    {

                        stoObj.UpdateMetaDetaDGPhotos(photoitem.DG_Photos_pkey, true);
                    }
                    else
                    {
                        stoObj.SaveMetaDetaUploadFailureDetails(photoitem.DG_Photos_pkey.ToString(), jsonrequestphoto, jsonrequestwebphoto, jsonrequestCloudinary, jsonrequestImgAssociation, venueName, siteName);
                    }
                }
                catch (Exception ex)
                {
                    ErrorHandler.ErrorHandler.LogFileWrite(ex.Message);
                }
                finally
                {

                }
                photoinfo.Clear();
                webphotoinfo.Clear();
                cloudinary.Clear();
                return isDataUpload;
            }
            else
            {
                log.Error("MetaDeta Upload: MetaDeta Uplo :");
                return isDataUpload;
            }

        }

        private void UploadPhotosAndSaveinPhotosIMS_old(string containerURI, string adHocContainerSAS, string venueName, string countryName)
        {
            //int subStoreId = 0;
            //int venueId = 0;
            bool Flag = true;
            ErrorHandler.ErrorHandler.LogFileWrite("UploadPhotosAndSaveinPhotosIMS is Called  ");
            countryName = countryName.Replace(countryPrefix, "");
            PhotoBusiness phBiz = new PhotoBusiness();
            //List<PhotoInfo> lstAllLatest = phBiz.SelectAllPhotosToBlobUpload().Where(t => t.DG_Photos_CreatedOn.ToString("yyyy-MM-dd") == DateTime.Now.ToString("yyyy-MM-dd")).OrderByDescending(t => t.DG_Photos_CreatedOn).ToList();
            List<PhotoInfo> lstAllLatest = phBiz.SelectAllPhotosToBlobUpload().OrderByDescending(t => t.DG_Photos_CreatedOn).ToList();

            List<PhotoInfo> lstAllLatestVideos = lstAllLatest.Where(x => x.DG_Photos_FileName.ToLower().Contains(".mp4")).ToList();
            List<PhotoInfo> lstAllLatestImages = lstAllLatest.Where(x => x.DG_Photos_FileName.ToLower().Contains(".jpg")).ToList();
            ErrorHandler.ErrorHandler.LogFileWrite("Line Number : 606  ");
            string source_dir = string.Empty;// @"D:\DigiImages\20200422";
            string siteName = string.Empty;
            ConfigBusiness cnfgBusines = new ConfigBusiness();

            var siteWiseList = lstAllLatestImages.GroupBy(x => x.DG_SubStoreId).ToList();
            var siteWiseVideoList = lstAllLatestVideos.GroupBy(x => x.DG_SubStoreId).ToList();
            foreach (var siteWisePhotos in siteWiseList)
            {
                ErrorHandler.ErrorHandler.LogFileWrite("Line Number : 630  ");
                if (!Flag)
                {
                    return;
                }
                siteName = cnfgBusines.GetSubStoreNameBySuubStoreId((Int32)siteWisePhotos.ElementAt(0).DG_SubStoreId);
                //siteName = siteName.Replace(" ", "-");
                ErrorHandler.ErrorHandler.LogFileWrite(siteName);
                ErrorHandler.ErrorHandler.LogFileWrite("Get SubstoreName By Sub store id : 636  ");
                var siteWiseListDateWise = siteWisePhotos.GroupBy(x => x.DG_Photos_CreatedOn.ToString("yyyy/MM/dd")).ToList();
                ErrorHandler.ErrorHandler.LogFileWrite("Line Number : 618  ");
                //string include_files = string.Join(";", VsiteWiseListDateWise.Select(x => x.DG_Photos_FileName.ToString()).ToArray());

                //List<PhotoInfo> siteWiseListDateWise = VsiteWiseListDateWise;// (List<PhotoInfo>)siteWiseList.GroupBy(x => x.DG_Photos_CreatedOn);
                foreach (var sitePhotosDateWise in siteWiseListDateWise)
                {
                    try
                    {
                        // siteName = cnfgBusines.GetSubStoreNameBySuubStoreId((Int32)sitePhotosDateWise[0].DG_SubStoreId);
                        //string dest_dir = containerURI + "/media/" + "" + siteName +  "/2020-05-18/" + adHocContainerSAS; // media is a must , rest can be configured
                        string dest_dir = containerURI + "/media/" + siteName + "/" + sitePhotosDateWise.ElementAt(0).DG_Photos_CreatedOn.ToString("yyyyMMdd") + "/" + adHocContainerSAS; // media is a must , rest can be configured
                        source_dir = sitePhotosDateWise.ElementAt(0).HotFolderPath + sitePhotosDateWise.ElementAt(0).DG_Photos_CreatedOn.ToString("yyyyMMdd");
                        // Changes by Suraj For Minified Images Upload
                        string minified_dest_dir = containerURI + "/media/" + siteName + "/" + sitePhotosDateWise.ElementAt(0).DG_Photos_CreatedOn.ToString("yyyyMMdd") + "/" + adHocContainerSAS; // media is a must , rest can be configured
                        string minified_source_dir = sitePhotosDateWise.ElementAt(0).HotFolderPath + "Minified_Images" + "\\" + sitePhotosDateWise.ElementAt(0).DG_Photos_CreatedOn.ToString("yyyyMMdd");
                        // End Changes By Suraj.
                        string include_files = string.Join(";", sitePhotosDateWise.Select(x => x.DG_Photos_FileName).ToArray());
                        string includeMinified_files = "m_" + include_files.Replace(";", ";m_");

                        // Added code by Suraj....
                        int CountSource = Directory.GetFiles(source_dir, "*.jpg", SearchOption.AllDirectories).Length;
                        int CountMinified = Directory.GetFiles(minified_source_dir, "*.jpg", SearchOption.AllDirectories).Length;
                        ErrorHandler.ErrorHandler.LogFileWrite("Original Images Count: " + CountSource.ToString());
                        ErrorHandler.ErrorHandler.LogFileWrite("Minified Images Count: " + CountMinified.ToString());
                        // Check Original Images are exist or not.
                        if (checkFileExist(source_dir, include_files))
                        {
                            ErrorHandler.ErrorHandler.LogFileWrite("Original Images are Not Found ");
                            Flag = false;
                            return;
                        }
                        // End Changes
                        //ErrorHandler.ErrorHandler.LogFileWrite("Original Images : " + include_files);
                        //ErrorHandler.ErrorHandler.LogFileWrite("Minified Images : " + includeMinified_files);
                        ErrorHandler.ErrorHandler.LogFileWrite("Line Number : 634  ");
                        ProcessStartInfo startInfo = new ProcessStartInfo();
                        startInfo.CreateNoWindow = false;
                        startInfo.UseShellExecute = false;
                        startInfo.RedirectStandardOutput = true;

                        //startInfo.FileName = Path.Combine(Environment.CurrentDirectory, "azcopy_windows_amd64_10.3.4", "azcopy.exe");
                        startInfo.FileName = Path.Combine(@"C:\Program Files (x86)\iMix", "azcopy_windows_amd64_10.3.4", "azcopy.exe");  // Added by Suraj.
                        ErrorHandler.ErrorHandler.LogFileWrite("File Path : " + startInfo.FileName);
                        startInfo.WindowStyle = ProcessWindowStyle.Hidden;
                        startInfo.Arguments = "sync \"" + source_dir + "\" \"" + dest_dir + "\"" + " --include-pattern=\"" + include_files + "\"";
                        try
                        {
                            // Start the process with the info we specified.
                            // Call WaitForExit and then the using statement will close.
                            using (Process exeProcess = Process.Start(startInfo))
                            {
                                string output = exeProcess.StandardOutput.ReadToEnd();
                                exeProcess.WaitForExit();
                                log.Error("Azure: Log info about original :" + output);
                                ErrorHandler.ErrorHandler.LogFileWrite("Azure: Log info about original :" + output);
                            }
                        }
                        catch (Exception ex)
                        {
                            log.Error("Azure: Exceptionlog info about original :" + ex.InnerException.ToString());
                            ErrorHandler.ErrorHandler.LogFileWrite("Azure: Exceptionlog info about original :" + ex.InnerException.ToString());
                        }
                        ErrorHandler.ErrorHandler.LogFileWrite("Line Number : 658  Original Images Uploads ");
                        // Check Minified Images are exist or not.
                        if (checkFileExist(minified_source_dir, includeMinified_files))
                        {
                            ErrorHandler.ErrorHandler.LogFileWrite("Minified Images are Not Found ");
                            Flag = false;
                            return;
                        }
                        //upload minified images now..
                        //ProcessStartInfo startInfo = new ProcessStartInfo();
                        //startInfo.CreateNoWindow = false;
                        //startInfo.UseShellExecute = false;
                        //startInfo.RedirectStandardOutput = true;
                        //startInfo.FileName = Path.Combine(Environment.CurrentDirectory, "azcopy_windows_amd64_10.3.4", "azcopy.exe");
                        //startInfo.WindowStyle = ProcessWindowStyle.Hidden;
                        startInfo.Arguments = "sync \"" + minified_source_dir + "\" \"" + minified_dest_dir + "\"" + " --include-pattern=\"" + includeMinified_files + "\"";
                        try
                        {
                            // Start the process with the info we specified.
                            // Call WaitForExit and then the using statement will close.
                            using (Process exeProcess = Process.Start(startInfo))
                            {
                                string output = exeProcess.StandardOutput.ReadToEnd();
                                exeProcess.WaitForExit();
                                log.Error("Azure: Log info about minified :" + output);
                                ErrorHandler.ErrorHandler.LogFileWrite("Azure: Log info about minified :" + output);
                            }
                        }
                        catch (Exception ex)
                        {
                            log.Error("Azure: Exceptionlog info about minified :" + ex.InnerException.ToString());
                            ErrorHandler.ErrorHandler.LogFileWrite("Azure: Exceptionlog info about minified :" + ex.InnerException.ToString());
                        }
                        // ErrorHandler.ErrorHandler.LogFileWrite("Line Number : 682 Minified Images Upload ");
                    }
                    catch (Exception ex)
                    {
                        ErrorHandler.ErrorHandler.LogFileWrite("Error Msg : " + ex.Message + "          Stack Trace : " + ex.StackTrace + "   Inner Exception : " + ex.InnerException);
                        log.Error("Azure: Exceptionlog info about minified :" + ex.InnerException.ToString());
                    }
                }
            }

            foreach (var siteWisePhotos in siteWiseVideoList)
            {
                ErrorHandler.ErrorHandler.LogFileWrite("Azure: Exceptionlog info about Video upload Started");
                if (!Flag)
                {
                    return;
                }
                siteName = cnfgBusines.GetSubStoreNameBySuubStoreId((Int32)siteWisePhotos.ElementAt(0).DG_SubStoreId);
                ErrorHandler.ErrorHandler.LogFileWrite(siteName);
                ErrorHandler.ErrorHandler.LogFileWrite("Get SubstoreName By Sub store id : 753  ");
                var siteWiseListDateWise = siteWisePhotos.GroupBy(x => x.DG_Photos_CreatedOn.ToString("yyyy/MM/dd")).ToList();
                ErrorHandler.ErrorHandler.LogFileWrite("Get SubstoreName By Sub store id : 755  ");
                foreach (var sitePhotosDateWise in siteWiseListDateWise)
                {
                    // siteName = cnfgBusines.GetSubStoreNameBySuubStoreId((Int32)sitePhotosDateWise[0].DG_SubStoreId);
                    //string dest_dir = containerURI + "/media/" + "" + siteName +  "/2020-05-18/" + adHocContainerSAS; // media is a must , rest can be configured
                    //string dest_dir = containerURI + "/media/" + "" + siteName + "/" + sitePhotosDateWise.ElementAt(0).DG_Photos_CreatedOn.ToString("yyyyMMdd") + "/" + adHocContainerSAS; // media is a must , rest can be configured
                    ErrorHandler.ErrorHandler.LogFileWrite("Get SubstoreName By Sub store id : 761  ");
                    string dest_dir = containerURI + "/media/" + siteName + "/" + sitePhotosDateWise.ElementAt(0).DG_Photos_CreatedOn.ToString("yyyyMMdd") + "/" + adHocContainerSAS; // media is a must , rest can be configured
                    source_dir = sitePhotosDateWise.ElementAt(0).HotFolderPath + "/Videos/" + sitePhotosDateWise.ElementAt(0).DG_Photos_CreatedOn.ToString("yyyyMMdd");

                    string include_Videofiles = string.Join(";", sitePhotosDateWise.Select(x => x.DG_Photos_FileName).ToArray());
                    string includeMinified_Videofiles = "m_" + include_Videofiles.Replace(";", ";m_");
                    ErrorHandler.ErrorHandler.LogFileWrite("Get SubstoreName By Sub store id : 767  ");
                    ProcessStartInfo startInfo = new ProcessStartInfo();
                    startInfo.CreateNoWindow = false;
                    startInfo.UseShellExecute = false;
                    startInfo.RedirectStandardOutput = true;
                    //startInfo.FileName = Path.Combine(Environment.CurrentDirectory, "azcopy_windows_amd64_10.3.4", "azcopy.exe");
                    startInfo.FileName = Path.Combine(@"C:\Program Files (x86)\iMix", "azcopy_windows_amd64_10.3.4", "azcopy.exe"); //Added by Suraj..
                    startInfo.WindowStyle = ProcessWindowStyle.Hidden;
                    ErrorHandler.ErrorHandler.LogFileWrite("azcopy file path  " + startInfo.FileName);
                    ErrorHandler.ErrorHandler.LogFileWrite("Get SubstoreName By Sub store id : 774  ");
                    //Upload original videos
                    source_dir = sitePhotosDateWise.ElementAt(0).HotFolderPath + "Videos\\" + sitePhotosDateWise.ElementAt(0).DG_Photos_CreatedOn.ToString("yyyyMMdd");
                    startInfo.Arguments = "sync \"" + source_dir + "\" \"" + dest_dir + "\"" + " --include-pattern=\"" + include_Videofiles + "\"";
                    try
                    {
                        // Start the process with the info we specified.
                        // Call WaitForExit and then the using statement will close.
                        using (Process exeProcess = Process.Start(startInfo))
                        {
                            string output = exeProcess.StandardOutput.ReadToEnd();
                            exeProcess.WaitForExit();
                            log.Error("Azure: Log info about original videos :" + output);
                        }
                    }
                    catch (Exception ex)
                    {
                        log.Error("Azure: Exceptionlog info about original videos :" + ex.InnerException.ToString());
                    }
                    ErrorHandler.ErrorHandler.LogFileWrite("Azure: Upload Orignal Video : 789");
                    //upload minified videos
                    //Upload original videos
                    source_dir = sitePhotosDateWise.ElementAt(0).HotFolderPath + "Videos\\" + sitePhotosDateWise.ElementAt(0).DG_Photos_CreatedOn.ToString("yyyyMMdd");
                    startInfo.Arguments = "sync \"" + source_dir + "\" \"" + dest_dir + "\"" + " --include-pattern=\"" + includeMinified_Videofiles + "\"";
                    try
                    {
                        // Start the process with the info we specified.
                        // Call WaitForExit and then the using statement will close.
                        using (Process exeProcess = Process.Start(startInfo))
                        {
                            string output = exeProcess.StandardOutput.ReadToEnd();
                            exeProcess.WaitForExit();
                            log.Error("Azure: Log info about minified videos :" + output);
                        }
                    }
                    catch (Exception ex)
                    {
                        log.Error("Azure: Exceptionlog info about minified videos :" + ex.InnerException.ToString());
                    }
                    ErrorHandler.ErrorHandler.LogFileWrite("Azure: Upload Minified Video : 809");
                }
            }

            if (lstAllLatest.Count > 0)
            {
                if (!Flag)
                {
                    return;
                }
                APIWrapper wrapper = APIWrapper.GetInstance;
                bool isDataUpload = true;
                int RetryConfigCount = 0;
                if (ConfigurationManager.AppSettings["RetryCount"] != null)
                {
                    RetryConfigCount = Convert.ToInt32(ConfigurationManager.AppSettings["RetryCount"]);
                }

                foreach (var photoitem in lstAllLatest)
                {
                    if (phBiz.GetretryFiledCount(photoitem.DG_Photos_RFID, RetryConfigCount))
                    {
                        string photoFilePath = string.Empty;

                        // Photo WebApi Call
                        string LocationSynccode = string.Empty;
                        string SubstoreSynccode = string.Empty;

                        LocationSynccode = phBiz.GetSynccodeValues("dbo.GetLocationNameSyncCode", Convert.ToInt32(photoitem.DG_Location_Id));
                        SubstoreSynccode = phBiz.GetSynccodeValues("dbo.GetSubStoreSyncCode", Convert.ToInt32(photoitem.DG_SubStoreId));
                        List<photos> photoinfo = new List<photos>();
                        var photo = new photos();

                        photo.Active = true;
                        photo.RFID = photoitem.DG_Photos_RFID;
                        photo.CreatedBy = Convert.ToInt32(photoitem.DG_Photos_UserID);
                        photo.CreatedDate = photoitem.DG_Photos_CreatedOn;
                        photo.CreatedOnMIX = photoitem.DG_Photos_CreatedOn;
                        photo.Effects = string.IsNullOrEmpty(photoitem.DG_Photos_Effects) ? "" : photoitem.DG_Photos_Effects;
                        photo.FileName = photoitem.DG_Photos_FileName;
                        photo.Frame = photoitem.DG_Photos_Frame;
                        photo.Background = photoitem.DG_Photos_Background;
                        photo.Layering = string.IsNullOrEmpty(photoitem.DG_Photos_Layering) ? "" : photoitem.DG_Photos_Layering;
                        photo.GpsLatitude = Convert.ToDecimal(0.0);
                        photo.GpsLongitude = Convert.ToDecimal(0.0);
                        photo.IMIXPhotoId = photoitem.DG_Photos_pkey;
                        photo.SubStoreId = SubstoreSynccode;
                        photoFilePath = System.IO.Path.Combine(photoitem.HotFolderPath, photoitem.DG_Photos_CreatedOn.ToString("yyyyMMdd"), photoitem.DG_Photos_FileName);
                        if (photoitem.DG_Photos_FileName.ToLower().Contains(".mp4"))
                        {
                            photo.MediaType = 2;
                            photoFilePath = System.IO.Path.Combine(photoitem.HotFolderPath, "Videos", photoitem.DG_Photos_CreatedOn.ToString("yyyyMMdd"), photoitem.DG_Photos_FileName);
                            photo.CaptureDate = photoitem.DG_Photos_CreatedOn;
                        }
                        else
                        {
                            photo.MediaType = 1;
                            photo.CaptureDate = Convert.ToDateTime(photoitem.DateTaken);
                        }

                        photo.IsCroped = Convert.ToBoolean(photoitem.DG_Photos_IsCroped);
                        photo.IsGreen = Convert.ToBoolean(photoitem.DG_Photos_IsGreen);
                        photo.MetaData = string.IsNullOrEmpty(photoitem.DG_Photos_MetaData) ? "" : photoitem.DG_Photos_MetaData;
                        photo.Size = string.IsNullOrEmpty(photoitem.DG_Photos_Sizes) ? "" : photoitem.DG_Photos_Sizes;
                        photo.IsRedEye = Convert.ToBoolean(photoitem.DG_Photos_IsRedEye);
                        photo.IsArchived = Convert.ToBoolean(photoitem.DG_Photos_Archive);
                        photo.LocationId = LocationSynccode;
                        photo.LayeringObjects = string.IsNullOrEmpty(photoitem.DG_Photos_Layering) ? "" : photoitem.DG_Photos_Layering;
                        photo.Dimention = "";
                        photo.IsBorder = false;
                        photo.IsGraphics = false;
                        photo.IsBackground = string.IsNullOrEmpty(photoitem.DG_Photos_Background) ? false : Convert.ToBoolean(photoitem.DG_Photos_Background);
                        photo.VideoLength = Convert.ToInt32(photoitem.DG_VideoLength);
                        photo.IsSyncedtoIMSDB = false;

                        photoinfo.Add(photo);

                        string jsonrequestphoto = Newtonsoft.Json.JsonConvert.SerializeObject(photoinfo);
                        string response = wrapper.PostData(jsonrequestphoto, "Photo");
                        string[] result = response.Split(';');
                        if (result[0].ToString() == "OK")
                        {

                            dynamic obj = JObject.Parse(result[1]);
                            photoitem.DG_Photo_ID = obj.PhotoId;
                            if (isDataUpload)
                                isDataUpload = true;
                        }
                        else
                            isDataUpload = false;

                        // WebPhoto WebApi Call

                        List<webphoto> webphotoinfo = new List<webphoto>();
                        var webphotos = new webphoto();
                        ImageAttribute imageAttribute = GetImageHeightWidth(photoFilePath);

                        webphotos.OrderNumber = "NA";
                        webphotos.IdentificationCode = "NA";
                        webphotos.LayeringObjects = string.IsNullOrEmpty(photoitem.DG_Photos_Layering) ? "" : photoitem.DG_Photos_Layering;
                        webphotos.Dimention = string.Empty;
                        webphotos.PhotoId = photoitem.DG_Photo_ID;
                        webphotos.Height = imageAttribute.Height;
                        webphotos.Width = imageAttribute.Width;
                        webphotos.ThumbnailDimension = string.IsNullOrEmpty(imageAttribute.Dimension) ? "" : imageAttribute.Dimension;
                        webphotos.IsPaidImage = false;
                        webphotos.CloudSourceImageURL = countryName + "/" + venueName + "/" + siteName + "/" + photoitem.DG_Photos_CreatedOn.ToString("yyyyMMdd") + "/" + "m_" + photoitem.DG_Photos_FileName;
                        webphotos.CloudImageUrl = countryName + "/" + venueName + "/" + siteName + "/" + photoitem.DG_Photos_CreatedOn.ToString("yyyyMMdd") + "/" + photoitem.DG_Photos_FileName;
                        webphotos.EditedOnMobile = false;
                        webphotos.EditedDate = DateTime.Now;
                        webphotos.LayeringObjectsEditedOn = DateTime.Now;

                        webphotoinfo.Add(webphotos);

                        string jsonrequestwebphoto = Newtonsoft.Json.JsonConvert.SerializeObject(webphotoinfo);
                        string responsewebphoto = wrapper.PostData(jsonrequestwebphoto, "WebPhotos");
                        string[] resultwebphoto = responsewebphoto.Split(';');
                        Int32 webphotoid = 0;
                        if (resultwebphoto[0].ToString() == "OK")
                        {
                            dynamic obj = JObject.Parse(resultwebphoto[1]);
                            webphotoid = obj.WebPhotoId;
                            if (isDataUpload)
                                isDataUpload = true;
                        }
                        else
                            isDataUpload = false;

                        // Cloudinary Details WebApi Call

                        List<CloudinaryDtl> cloudinary = new List<CloudinaryDtl>();
                        var CloudinaryDtlv = new CloudinaryDtl();
                        CloudinaryDtlv.PartnerId = partnerID;
                        CloudinaryDtlv.WebPhotoID = webphotoid;//webphoto.WebPhotoId;
                        CloudinaryDtlv.SourceImageID = photoitem.DG_Photos_FileName;
                        CloudinaryDtlv.CloudinaryPublicID = countryName + "/" + venueName + "/" + siteName + "/" + photoitem.DG_Photos_CreatedOn.ToString("yyyyMMdd") + "/" + photoitem.DG_Photos_FileName;
                        CloudinaryDtlv.CloudinaryStatusID = 1;
                        CloudinaryDtlv.RetryCount = 0;
                        CloudinaryDtlv.ErrorMessage = "Minified,thumbnail uploaded successfully";
                        CloudinaryDtlv.AddedBy = "webusers";
                        CloudinaryDtlv.CreatedDateTime = photoitem.DG_Photos_CreatedOn;
                        CloudinaryDtlv.ModifiedBy = "webusers";
                        CloudinaryDtlv.ModifiedDateTime = DateTime.Now;
                        CloudinaryDtlv.IsActive = true;

                        cloudinary.Add(CloudinaryDtlv);

                        string jsonrequestCloudinary = Newtonsoft.Json.JsonConvert.SerializeObject(cloudinary);
                        if (wrapper.PostData(jsonrequestCloudinary, "CloudinaryDtl") == "OK")
                        {
                            if (isDataUpload)
                                isDataUpload = true;
                        }
                        else
                            isDataUpload = false;

                        List<AssociateImageDetails> lstAssociateImages = phBiz.GetAssociateImageDetails(photoitem.DG_Photos_pkey.ToString());
                        string jsonrequestImgAssociation = string.Empty;
                        foreach (var AssociateImage in lstAssociateImages)
                        {
                            List<iMixImagesAssociation> ImageAssociation = new List<iMixImagesAssociation>();
                            var ImgAssociation = new iMixImagesAssociation();
                            ImgAssociation.IMIXImageAssociationId = AssociateImage.IMIXImageAssociationId;
                            ImgAssociation.IMIXCardTypeId = AssociateImage.IMIXCardTypeId;
                            ImgAssociation.PhotoId = AssociateImage.PhotoId;
                            ImgAssociation.CardUniqueIdentifier = AssociateImage.CardUniqueIdentifier;
                            ImgAssociation.MappedIdentifier = AssociateImage.MappedIdentifier;
                            ImgAssociation.ModifiedDate = AssociateImage.ModifiedDate;
                            ImgAssociation.IsOrdered = AssociateImage.IsOrdered;
                            ImgAssociation.RfidIdentifierId = AssociateImage.RfidIdentifierId;
                            ImgAssociation.IsMoved = AssociateImage.IsMoved;
                            ImgAssociation.Nationality = AssociateImage.Nationality;
                            ImgAssociation.Email = AssociateImage.Email;

                            ImageAssociation.Add(ImgAssociation);

                            jsonrequestImgAssociation = Newtonsoft.Json.JsonConvert.SerializeObject(ImageAssociation);
                            if (wrapper.PostData(jsonrequestImgAssociation, "iMixImageAssociation") == "OK")
                            {
                                if (isDataUpload)
                                    isDataUpload = true;
                            }
                            else
                                isDataUpload = false;
                        }
                        // DG_Photo Table flag value update

                        try
                        {
                            if (isDataUpload)
                            {
                                StoreSubStoreDataBusniess stoObj = new StoreSubStoreDataBusniess();
                                stoObj.UpdateMetaDetaDGPhotos(photoitem.DG_Photos_pkey, true);
                            }
                            else
                            {

                                StoreSubStoreDataBusniess stoObj = new StoreSubStoreDataBusniess();
                                stoObj.SaveMetaDetaUploadFailureDetails(photoitem.DG_Photo_ID.ToString(), jsonrequestphoto, jsonrequestwebphoto, jsonrequestCloudinary, jsonrequestImgAssociation, venueName, siteName);
                            }
                        }
                        catch (Exception ex)
                        {
                            ErrorHandler.ErrorHandler.LogFileWrite(ex.Message);
                        }
                        photoinfo.Clear();
                        webphotoinfo.Clear();
                        cloudinary.Clear();
                        //ImageAssociation.Clear();
                        // Need write list clear logic.

                    }
                    else
                    {
                        log.Error("MetaDeta Upload: MetaDeta Uplo :");
                    }
                };

                #region old logic 
                //using (DigiPhotoWebContext dbContext = new DigiPhotoWebContext())
                //{
                //    //var subStoreId = dbContext.DigiMasterLocations.Where(s => s.SyncCode == cloudInaryInfo.SubStoreSyncCode && s.Level == 4).FirstOrDefault().DigiMasterLocationId;
                //    //var partnerId = (from DigiLocation in dbContext.DigiMasterLocations
                //    //                 join DigiSubStore in dbContext.DigiMasterLocations
                //    //                 on DigiLocation.DigiMasterLocationId equals DigiSubStore.ParentDigiMasterLocationId
                //    //                 join PtrLocation in dbContext.PartnerLocations
                //    //                 on DigiLocation.DigiMasterLocationId equals PtrLocation.StoreID
                //    //                 where DigiSubStore.DigiMasterLocationId == subStoreId
                //    //                 select PtrLocation.PartnerID).FirstOrDefault();

                //    //if (dbContext.DigiMasterLocations.Where(s => s.SyncCode == lstAllLatest[0].SyncCode && s.Level == 4).FirstOrDefault() != null)
                //    //{
                //    //    subStoreId = dbContext.DigiMasterLocations.Where(s => s.SyncCode == orderDetail.SubStore.SyncCode && s.Level == 4).FirstOrDefault().DigiMasterLocationId;
                //    //    venueId = dbContext.DigiMasterLocations.Where(s => s.SyncCode == orderDetail.SubStore.SyncCode && s.Level == 4).FirstOrDefault().ParentDigiMasterLocationId;
                //    //    //end
                //    //}
                //    //else
                //    //{
                //    //    //throw new Exception((Int32)DigiPhoto.DailySales.Utility.ChangeTrackingProcessingError.SiteMissing + "@@@@ Order failed because site with sync code " + orderDetail.SubStore.SyncCode + " does not exist.");
                //    //}
                //    foreach (var photoitem in lstAllLatest)
                //    {
                //        //if (dbContext.Photos.Any((c => c.IMIXPhotoId == photoitem.DG_Photo_ID && c.FileName == photoitem.DG_Photos_FileName)))
                //        //{
                //        //    //nothing to do
                //        //}
                //        //else
                //        //{
                //        //dbContext.Photos.Where(c => c.IMIXPhotoId == photoitem.DG_Photo_ID && c.FileName == photoitem.DG_Photos_FileName)
                //        var photo = new Photo();
                //        photo.Active = true;
                //        photo.RFID = photoitem.DG_Photos_RFID;

                //        photo.CreatedBy = 1;// photoitem.DG_Photos_UserID;
                //        photo.CreatedDate = photoitem.DG_Photos_CreatedOn;
                //        photo.CreatedOnMIX = photoitem.DG_Photos_CreatedOn;
                //        photo.Effects = photoitem.DG_Photos_Effects;
                //        photo.FileName = photoitem.DG_Photos_FileName;
                //        photo.Frame = photoitem.DG_Photos_Frame;
                //        photo.Background = photoitem.DG_Photos_Background;
                //        photo.Layering = photoitem.DG_Photos_Layering;
                //        photo.GPSLatitude = Convert.ToDecimal(0.0);
                //        photo.GPSLongitude = Convert.ToDecimal(0.0);

                //        photo.IMIXPhotoId = photoitem.DG_Photo_ID;
                //        photo.SubStoreId = Convert.ToInt32(photoitem.DG_SubStoreId);
                //        photo.PhotoId = photoitem.DG_Photo_ID;//No need it's pkey


                //        string photoFilePath = System.IO.Path.Combine(photoitem.HotFolderPath, photoitem.DG_Photos_CreatedOn.ToString("yyyyMMdd"), photoitem.DG_Photos_FileName);
                //        if (photoitem.DG_Photos_FileName.ToLower().Contains(".mp4"))
                //        {
                //            photo.MediaType = 2;
                //            photoFilePath = System.IO.Path.Combine(photoitem.HotFolderPath, "Videos", photoitem.DG_Photos_CreatedOn.ToString("yyyyMMdd"), photoitem.DG_Photos_FileName);
                //        }
                //        else
                //        {
                //            photo.MediaType = 1;
                //            photo.CaptureDate = photoitem.DateTaken;
                //        }

                //        dbContext.Photos.Add(photo);
                //        dbContext.SaveChanges();

                //        ImageAttribute imageAttribute = GetImageHeightWidth(photoFilePath);

                //        var webphoto = new WebPhoto();
                //        webphoto.WebPhotoId = 1;
                //        webphoto.OrderNumber = "NA";
                //        webphoto.IdentificationCode = "NA";
                //        webphoto.LayeringObjects = string.Empty;
                //        webphoto.Dimention = string.Empty;
                //        webphoto.PhotoId = photo.PhotoId;
                //        webphoto.Height = imageAttribute.Height;
                //        webphoto.Width = imageAttribute.Width;
                //        webphoto.ThumbnailDimension = imageAttribute.Dimension;
                //        webphoto.IsPaidImage = false;
                //        webphoto.CloudSourceImageURL = countryName + "/" + venueName + "/" + siteName + "/" + photoitem.DG_Photos_CreatedOn.ToString("yyyyMMdd") + "/" + "m_" + photo.FileName;
                //        dbContext.WebPhotos.Add(webphoto);
                //        dbContext.SaveChanges();

                //        var CloudinaryDtlv = new DigiPhoto.DailySales.DataAccess.DataModels.CloudinaryDtl();
                //        CloudinaryDtlv.PartnerId = 1;
                //        CloudinaryDtlv.WebPhotoID = webphoto.WebPhotoId;
                //        CloudinaryDtlv.SourceImageID = photoitem.DG_Photos_FileName;
                //        CloudinaryDtlv.CloudinaryPublicID = countryName + "/" + venueName + "/" + siteName + "/" + photoitem.DG_Photos_CreatedOn.ToString("yyyyMMdd") + "/" + photo.FileName;
                //        CloudinaryDtlv.CloudinaryStatusID = 1;
                //        CloudinaryDtlv.RetryCount = 0;
                //        CloudinaryDtlv.ErrorMessage = "Minified,thumbnail uploaded successfully";
                //        CloudinaryDtlv.AddedBy = "vinod";
                //        CloudinaryDtlv.CreatedDateTime = photoitem.DG_Photos_CreatedOn;
                //        CloudinaryDtlv.ModifiedBy = "webusers";
                //        CloudinaryDtlv.ModifiedDateTime = DateTime.Now;
                //        CloudinaryDtlv.IsActive = true;
                //        dbContext.CloudinaryDtls.Add(CloudinaryDtlv);
                //        dbContext.SaveChanges();

                //        using (DigiPhotoContext dgdbContext = new DigiPhotoContext())
                //        {
                //            var photoInfo = (
                //                                     from p in dgdbContext.DG_Photos
                //                                     where p.DG_Photos_pkey == photoitem.DG_Photos_pkey
                //                                     select new PhotoInfo
                //                                     {
                //                                         DG_Photo_ID = p.DG_Photos_pkey,
                //                                         DG_Photos_FileName = p.DG_Photos_FileName,
                //                                         DG_Photos_RFID = p.DG_Photos_RFID,
                //                                         DG_Photos_CreatedOn = p.DG_Photos_CreatedOn,
                //                                         DG_Photos_Background = p.DG_Photos_Background,
                //                                         DG_Photos_Frame = p.DG_Photos_Frame,
                //                                         DG_Photos_Layering = null,//p.DG_Photos_Layering,
                //                                         DG_Photos_IsCroped = p.DG_Photos_IsCroped,
                //                                         DG_Photos_IsRedEye = p.DG_Photos_IsRedEye,
                //                                         DG_Photos_IsGreen = p.DG_Photos_IsGreen,
                //                                         DG_Location_Id = p.DG_Location_Id,
                //                                         DateTaken = p.DateTaken,
                //                                         DG_MediaType = p.DG_MediaType,
                //                                         DG_Photos_UserID = p.DG_Photos_UserID,
                //                                         DG_VideoLength = p.DG_VideoLength,
                //                                         DG_SubStoreId = p.DG_SubStoreId,
                //                                         IsUploadedToBlob = true
                //                                     }).ToList();

                //            if (photoInfo != null)
                //            {
                //                var dbPhoto = dgdbContext.DG_Photos.Where(r => r.DG_Photos_pkey == photoitem.DG_Photos_pkey).FirstOrDefault();
                //                if (dbPhoto != null)
                //                {
                //                    dbPhoto.DateTaken = photoInfo[0].DateTaken;
                //                    dbPhoto.DG_Location_Id = photoInfo[0].DG_Location_Id;
                //                    dbPhoto.DG_MediaType = photoInfo[0].DG_MediaType;
                //                    dbPhoto.DG_Photos_Archive = photoInfo[0].DG_Photos_Archive;
                //                    dbPhoto.DG_Photos_Background = photoInfo[0].DG_Photos_Background;
                //                    dbPhoto.DG_Photos_CreatedOn = photoInfo[0].DG_Photos_CreatedOn;
                //                    dbPhoto.DG_Photos_DateTime = photoInfo[0].DG_Photos_DateTime;
                //                    dbPhoto.DG_Photos_FileName = photoInfo[0].DG_Photos_FileName;
                //                    dbPhoto.DG_Photos_Frame = photoInfo[0].DG_Photos_Frame;
                //                    dbPhoto.DG_Photos_IsCroped = photoInfo[0].DG_Photos_IsCroped;
                //                    dbPhoto.DG_Photos_IsGreen = photoInfo[0].DG_Photos_IsGreen;
                //                    dbPhoto.DG_Photos_IsRedEye = photoInfo[0].DG_Photos_IsRedEye;
                //                    dbPhoto.DG_Photos_RFID = photoInfo[0].DG_Photos_RFID;
                //                    dbPhoto.DG_Photos_Sizes = photoInfo[0].DG_Photos_Sizes;
                //                    dbPhoto.DG_Photos_UserID = photoInfo[0].DG_Photos_UserID;
                //                    dbPhoto.DG_SubStoreId = photoInfo[0].DG_SubStoreId;
                //                    dbPhoto.DG_VideoLength = (Int32)photoInfo[0].DG_VideoLength;
                //                    dbPhoto.IsUploadedToBlob = true;
                //                    dgdbContext.SaveChanges();//Update dg_phtoto table

                //                }

                //            }

                //        }
                #endregion





            }
        }

        public static ImageAttribute GetImageHeightWidth(string filePath)
        {
            var imageInfo = new ImageAttribute();
            try
            {
                using (var image = System.Drawing.Image.FromFile(filePath))
                {
                    imageInfo.Width = image.Width;
                    imageInfo.Height = image.Height;
                    imageInfo.ImageSize = image.Size;
                    System.Drawing.Size size = new System.Drawing.Size(image.Width, image.Height);
                    imageInfo.Dimension = ScaleImage(image.Width, image.Height, size);
                }
            }
            catch
            {
                imageInfo.Width = 500;
                imageInfo.Height = 500;
                imageInfo.ImageSize = new Size(500, 500);
            }

            return imageInfo;
        }

        string ScaleImage(int width, int height, Size size)
        {
            throw new NotImplementedException();
        }

        public static string ScaleImage(int imageWidth, int imageHeight, Size size, bool preserveAspectRatio = true)
        {
            string dimension = string.Empty;
            try
            {
                int newWidth;
                int newHeight;
                if (preserveAspectRatio)
                {
                    float percentWidth = (float)size.Width / (float)imageWidth;
                    float percentHeight = (float)size.Height / (float)imageHeight;
                    float percent = (percentHeight < percentWidth) ? percentHeight : percentWidth;
                    newWidth = (int)((float)imageWidth * percent);
                    newHeight = (int)((float)imageHeight * percent);
                }
                else
                {
                    newWidth = size.Width;
                    newHeight = size.Height;
                }
                dimension = newHeight + "," + newWidth;
            }
            catch
            {
                dimension = "400,500";
            }
            return dimension;
        }

        public void StartScheduler()
        {
            try
            {
                ErrorHandler.ErrorHandler.LogFileWrite("Line Number 1245 Start Schedular");
                timer = new System.Timers.Timer();
                timer.Interval = new TimeSpan(0, 0, 10).TotalMilliseconds;
                timer.AutoReset = true;
                timer.Elapsed += new System.Timers.ElapsedEventHandler(this.timer_Elapsed);
                timer.Start();
                timer.Enabled = true;
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogFileWrite(ex.Message);
            }
        }

        private void timer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            try
            {
                ErrorHandler.ErrorHandler.LogFileWrite("Timer trigger 1263 ");
                timer.Stop();
                timer.Enabled = false;
                UploadToAzureBlob();

                ErrorHandler.ErrorHandler.LogFileWrite("Timer hits at " + ' ' + DateTime.Now.ToString());

                timer.Start();
                timer.Enabled = true;

            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                timer.Start();
            }
            finally
            {
                timer.Start();
            }

        }
           

        private bool checkFileExist(string sourcePath, string fileName)
        {
            bool flag = false;
            try
            {

                string[] fileNames = fileName.Split(';');
                ErrorHandler.ErrorHandler.LogFileWrite("File exits Check : " + sourcePath);
                foreach (var file in fileNames)
                {
                    if (File.Exists(sourcePath + "\\" + file))
                    {
                        flag = false;
                    }
                    else
                    {
                        flag = true;
                        break;
                    }
                }
                return flag;
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogFileWrite("File exits Check : " + ex.Message);
                return true;
            }
        }
    }
}
public class ImageAttribute
{
    public int Width { get; set; }
    public int Height { get; set; }
    public string Dimension { get; set; }
    public Size ImageSize { get; set; }
}