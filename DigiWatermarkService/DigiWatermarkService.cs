﻿using DigiPhoto.DataLayer;
using DigiPhoto.IMIX.Business;
using DigiPhoto.IMIX.Model;
using FrameworkHelper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.ServiceProcess;
using System.Text;
using System.Threading;
using System.Threading.Tasks;


namespace DigiWatermarkService
{
    public partial class DigiWatermarkService : ServiceBase
    {

        #region Variables Declarations
        static System.Timers.Timer timer;
        List<long> WaterMarkConfigList = new List<long>();
        List<iMixConfigurationLocationInfo> ListiMixConfigurationLocationInfo;
        List<WaterMarkConfiguration> listWaterMarkConfiguration;
        List<CurrencyInfo> _lstCurrency;
        int _defaultCurrencyId;
        string _billDiscountDetails;
        List<WaterMarkConfiguration> tempList = new List<WaterMarkConfiguration>();
        public String DiscountDetails
        { get { return _billDiscountDetails; } set { _billDiscountDetails = value; } }

        #endregion

        #region Cstr
        public DigiWatermarkService()
        {
            InitializeComponent();
            GetWaterMarkConfigList();
        }
        #endregion

        #region Service StartStop
        protected override void OnStart(string[] args)
        {
            try
            {
                //ErrorHandler.ErrorHandler.LogFileWrite("Timer hits at on start " + ' ' + DateTime.Now.ToString());

                //Thread.Sleep(new TimeSpan(0, 0, 0));

                ServicePosInfoBusiness svcPosinfoBusiness = new ServicePosInfoBusiness();
                string ret = svcPosinfoBusiness.ServiceStart(false);
                //ErrorHandler.ErrorHandler.LogFileWrite("Timer hits after service pos " + ' ' + DateTime.Now.ToString());


                if (string.IsNullOrEmpty(ret))
                {
                    if (!(new PreSoldAutoOnlineOrderBusiness()).chKIsAutoPurchaseActiveOrNot(1))
                    {
                        LoadConfiguration();
                        //ErrorHandler.ErrorHandler.LogFileWrite("Timer hits after loadconfig" + ' ' + DateTime.Now.ToString());
                        StartScheduler();
                    }

                }
                else
                {
                    throw new Exception("Already Started");

                }
            }
            catch (Exception ex)
            {
                ExitCode = 13816;
                this.Stop();
                ErrorHandler.ErrorHandler.LogError(ex);
            }
        }
        public void test()
        {
            ServicePosInfoBusiness svcPosinfoBusiness = new ServicePosInfoBusiness();
            string ret = svcPosinfoBusiness.ServiceStart(false);
            if (!(new PreSoldAutoOnlineOrderBusiness()).chKIsAutoPurchaseActiveOrNot(1))
            {
                LoadConfiguration();
            }

            GetWaterMarkUploadDetails();
            if (tempList.Count > 0)
                UploadWaterMarkImages();
        }
        public void StartScheduler()
        {
            try
            {

                timer = new System.Timers.Timer();
                timer.Interval = new TimeSpan(0, 0, 10).TotalMilliseconds;
                timer.AutoReset = true;
                timer.Elapsed += new System.Timers.ElapsedEventHandler(this.timer_Elapsed);
                timer.Start();
                timer.Enabled = true;
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogFileWrite(ex.Message);
            }
        }
        private void timer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            try
            {
                timer.Stop();
                timer.Enabled = false;
                //ErrorHandler.ErrorHandler.LogFileWrite("Timer hits at " + ' ' + DateTime.Now.ToString());

                GetWaterMarkUploadDetails();
                if (tempList.Count > 0)
                    UploadWaterMarkImages();
                timer.Start();
                timer.Enabled = true;

            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                timer.Start();
            }
            finally
            {
                timer.Start();
            }
        }
        protected override void OnStop()
        {
            ServicePosInfoBusiness svcPosinfoBusiness = new ServicePosInfoBusiness();
            svcPosinfoBusiness.StopService(false);
        }
        #endregion

        #region Get and Process WaterMarksettings
        public void GetWaterMarkConfigList()
        {
            WaterMarkConfigList.Add(Convert.ToInt64(ConfigParams.WaterMarkEnable));
            WaterMarkConfigList.Add(Convert.ToInt64(ConfigParams.WaterMarkLocation));
            WaterMarkConfigList.Add(Convert.ToInt64(ConfigParams.WaterMarkProduct));
            WaterMarkConfigList.Add(Convert.ToInt64(ConfigParams.WaterMarkScheduledOn));
        }
        private void LoadConfiguration()
        {
            try
            {
                ConfigBusiness configBusiness = new ConfigBusiness();
                List<iMixConfigurationLocationInfo> List_iMixConfigurationLocationInfo = new List<iMixConfigurationLocationInfo>();
                listWaterMarkConfiguration = new List<WaterMarkConfiguration>();

                List_iMixConfigurationLocationInfo = configBusiness.GetLocationWiseConfigParams();
                ListiMixConfigurationLocationInfo = List_iMixConfigurationLocationInfo.Where(x => x.LocationId != 0 && WaterMarkConfigList.Contains(x.IMIXConfigurationMasterId)).ToList();
                if (ListiMixConfigurationLocationInfo != null && ListiMixConfigurationLocationInfo.Count > 0)
                    listWaterMarkConfiguration = MapLoctionList(ListiMixConfigurationLocationInfo);
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
            }
        }
        private List<WaterMarkConfiguration> MapLoctionList(List<iMixConfigurationLocationInfo> ListiMixConfigurationLocationInfo)
        {
            List<int> AvailableLocations = new List<int>();
            AvailableLocations = ListiMixConfigurationLocationInfo.Select(x => x.LocationId).Distinct().ToList();
            List<WaterMarkConfiguration> list_WaterMarkConfiguration = new List<WaterMarkConfiguration>();

            foreach (var loc in AvailableLocations)
            {

                WaterMarkConfiguration waterMarkConfiguration = new WaterMarkConfiguration();
                foreach (var item in ListiMixConfigurationLocationInfo.Where(x => x.LocationId == loc))
                {

                    int ScheduleHour = 0;
                    int ScheduleMinute = 0;

                    switch (item.IMIXConfigurationMasterId)
                    {
                        case (int)ConfigParams.WaterMarkLocation:
                            waterMarkConfiguration.LocationId = Convert.ToInt32(item.ConfigurationValue);

                            break;
                        case (int)ConfigParams.WaterMarkEnable:
                            waterMarkConfiguration.EnableWaterMark = item.ConfigurationValue == "True" ? true : false;
                            break;
                        case (int)ConfigParams.WaterMarkProduct:
                            waterMarkConfiguration.ProductId = Convert.ToInt32(item.ConfigurationValue);
                            break;
                        case (int)ConfigParams.WaterMarkScheduledOn:
                            ScheduleHour = TimeSpan.Parse(item.ConfigurationValue).Hours;
                            ScheduleMinute = TimeSpan.Parse(item.ConfigurationValue).Minutes;
                            waterMarkConfiguration.ScheduleTime = new TimeSpan(ScheduleHour, ScheduleMinute, 0);
                            break;
                    }
                    waterMarkConfiguration.SubstoreId = item.SubstoreId;
                }

                waterMarkConfiguration.ProductPrice = GetProductPrice(waterMarkConfiguration.ProductId);
                if (waterMarkConfiguration.EnableWaterMark == true)
                    list_WaterMarkConfiguration.Add(waterMarkConfiguration);
            }
            if (list_WaterMarkConfiguration.Count > 0)
                list_WaterMarkConfiguration = list_WaterMarkConfiguration.OrderBy(x => x.ScheduleTime).ToList();

            return list_WaterMarkConfiguration;
        }
        private double GetProductPrice(int productId)
        {
            ConfigBusiness business = new ConfigBusiness();
            List<ProductTypeInfo> productInfo = new List<ProductTypeInfo>();
            double productPrice = 0.0;

            productInfo = business.GetWaterMarkProduct();
            productPrice = productInfo.Where(x => x.DG_Orders_ProductType_pkey == productId).Select(x => x.DG_Product_Pricing_ProductPrice).FirstOrDefault();

            return productPrice;
        }
        public void GetWaterMarkUploadDetails()
        {
            if (listWaterMarkConfiguration.Count > 0)
            {
                TimeSpan currentTime = new TimeSpan(DateTime.Now.Hour, DateTime.Now.Minute, 0);
                foreach (var item in listWaterMarkConfiguration)
                {
                    if (TimeSpan.Compare(currentTime, item.ScheduleTime) >= 0)
                    {
                        tempList.Add(item);
                    }

                    else
                    { //No location is currently sceduled with current Time
                    }
                }
            }
        }
        private void UploadWaterMarkImages()
        {
            try
            {
                foreach (var Configitem in tempList)
                {
                    ConfigBusiness business = new ConfigBusiness();
                    List<WaterMarkTagsUpload> tagsUploadList = new List<WaterMarkTagsUpload>();

                    tagsUploadList = business.GetCardIdentifier(Configitem.LocationId);
                    if (tagsUploadList.Count > 0)
                    {
                        //stop timer here...
                        var UniquCode = tagsUploadList
                                     .GroupBy(g => new
                                     {
                                         g.CardUniqueIdentifier,
                                         g.ImageIdentificationType
                                     })
                                     .Select(group => new
                                     {
                                         CardUniqueIdentifier = group.Key.CardUniqueIdentifier,
                                         ImageIdentificationType = group.Key.ImageIdentificationType

                                     });
                        foreach (var items in UniquCode)
                        {
                            string photoIDs = string.Join(", ", tagsUploadList.Where(x => x.CardUniqueIdentifier == items.CardUniqueIdentifier).ToList().Select(i => i.PhotoId.ToString()).ToArray());
                            int NumberOfPhotos = tagsUploadList.Where(x => x.CardUniqueIdentifier == items.CardUniqueIdentifier).Select(x => x.PhotoId).Count();
                            string IMIXImageAssociationIds = string.Join(", ", tagsUploadList.Where(x => x.CardUniqueIdentifier == items.CardUniqueIdentifier).ToList().Select(i => i.IMIXImageAssociationId.ToString()).ToArray());
                            ProcessOrder(photoIDs, IMIXImageAssociationIds, NumberOfPhotos, Configitem.ProductPrice, Configitem.SubstoreId, Configitem.ProductId, items.CardUniqueIdentifier, items.ImageIdentificationType);

                            //tempList.First(d => d.LocationId == Configitem.LocationId).IsLocationProcessed =true;
                        }

                    }

                    else
                    {
                        //no more tags on this locaion.
                        // tempList.First(d => d.LocationId == Configitem.LocationId).IsLocationProcessed = true;
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogFileWrite(ex.Message);
            }
            finally
            {
                tempList.Clear();
                //timer starts here.
            }

        }
        #endregion

        #region Genrate Orders
        private void ProcessOrder(string photoIDs, string IMIXImageAssociationIds, int NumberOfPhotos, double ProductPrice, int SubstoreId, int ProductId, string CardUniqueIdentifier, int ImageIdentificationType)
        {
            try
            {
                #region variables

                int subStoreId = SubstoreId;
                bool IsWaterMarked = true;

                String paymentDetails = string.Empty;
                int paymentMode = -1;
                string paymentType = string.Empty;
                double _totalAmount = 0;
                double _totalBillDiscount = 0;

                #endregion
                _lstCurrency = (new CurrencyBusiness()).GetCurrencyOnly();
                _defaultCurrencyId = (new CurrencyBusiness()).GetDefaultCurrency();
                var CurrencySymbol = (from currlist in _lstCurrency
                                      where currlist.DG_Currency_pkey == _defaultCurrencyId
                                      select currlist).FirstOrDefault();

                StoreSubStoreDataBusniess storeObj = new StoreSubStoreDataBusniess();

                DigiPhoto.IMIX.Model.StoreInfo store = storeObj.GetStore();


                StoreInfo objStore = (new StoreSubStoreDataBusniess()).GetStore();


                paymentDetails = "<Payments>";


                paymentMode = (Int32)DigiPhoto.DataLayer.PaymentMode.Cash;
                paymentType = "CASH";
                double Amount = ProductPrice;
                double OrignalAmount = ProductPrice;

                int CurrencyID = _defaultCurrencyId;
                string CurrencySyncCode = CurrencySymbol.SyncCode;
                string currencyCode = CurrencySymbol.DG_Currency_Code;
                _totalAmount = ProductPrice;
                double _totalBillAmount = ProductPrice;
                Amount = Math.Round(ConvertToDefault(CurrencyID, Amount), 3);
                paymentDetails += "<Payment Mode = 'cash' Amount = '" + Amount.ToString("N2") + "' OrignalAmount = '" + OrignalAmount.ToString() + "' MaxImages ='" + NumberOfPhotos.ToString() + "' CurrencyID = '" + CurrencyID.ToString() + "' CurrencyCode = '" + currencyCode + "' CurrencySyncCode = '" + CurrencySyncCode + "'/>";
                paymentDetails += "</Payments>";



                string orderNumber = GenerateOrderNumber();

                string syncCode = CommonUtility.GetUniqueSynccode(Convert.ToInt32(ApplicationObjectEnum.Order).ToString().PadLeft(2, '0'), objStore.CountryCode, objStore.StoreCode, "14");
                string newOrderNo = string.Empty;
                OrderInfo orderInfo = (new OrderBusiness()).GenerateOrder(orderNumber, (decimal)_totalAmount, (decimal)_totalBillAmount, paymentDetails, paymentMode, _totalBillDiscount, DiscountDetails, 1, this._defaultCurrencyId, "0", syncCode, objStore.StoreCode); // 1 for user AP Admin user
                orderNumber = orderInfo.DG_Orders_Number;
                TaxBusiness buss = new TaxBusiness();
                int orderId = orderInfo.DG_Orders_pkey;


                int parentId = -1;
                if (orderId > 0)
                {
                    StringBuilder ItemDetail = new StringBuilder();
                    string images = string.Empty;
                    string photoIDsUnsold = null;
                    String BillLineItems = string.Empty;

                    images = string.Empty;
                    #region Condition If Order is a Package
                    if (IsWaterMarked == true)
                    {
                        photoIDsUnsold = photoIDs;
                        photoIDs = "";
                    }
                    if (subStoreId == 0)
                        subStoreId = 1;
                    string orderDetailsSyncCode = CommonUtility.GetUniqueSynccode(Convert.ToInt32(ApplicationObjectEnum.OrderDetails).ToString().PadLeft(2, '0'), objStore.CountryCode, objStore.StoreCode, 1.ToString());
                    parentId = (new OrderBusiness()).SaveOrderLineItems(ProductId, orderId, "", 1, "", 0, (decimal)_totalAmount, (decimal)_totalAmount, (decimal)_totalAmount, -1, subStoreId, 0, "", orderDetailsSyncCode, null,null);
                    int id = 0;

                    string OrderDetailsSyncCode1 = CommonUtility.GetUniqueSynccode(Convert.ToInt32(ApplicationObjectEnum.OrderDetails).ToString().PadLeft(2, '0'), objStore.CountryCode, objStore.StoreCode, "1");
                    id = (new OrderBusiness()).SaveOrderLineItems(84, orderId, photoIDs, 1, "", 0, 0, 0, 0, parentId, subStoreId, ImageIdentificationType, CardUniqueIdentifier, OrderDetailsSyncCode1,null, photoIDsUnsold);

                    if (BillLineItems != string.Empty)
                        BillLineItems += "," + images;
                    else
                        BillLineItems = images;

                    images = string.Empty;

                    new ConfigBusiness().UpdateOrderStatus(IMIXImageAssociationIds);
                    #endregion
                }
            }
            catch (Exception ee)
            {
                ErrorHandler.ErrorHandler.LogFileWrite(ee.Message);
            }
        }
        private double ConvertToDefault(int currencyid, double ToBeConverted)
        {
            double convertedAmount = 0;

            var amount = (from rate in _lstCurrency
                          where rate.DG_Currency_pkey == currencyid
                          select rate).FirstOrDefault();

            if (amount != null)
                convertedAmount = (amount.DG_Currency_Rate * ToBeConverted);

            return Math.Round(convertedAmount, 3, MidpointRounding.ToEven);
        }
        private string GenerateOrderNumber()
        {
            string orderNumber = "DG-";// +"-";
            string uniqueNumber = string.Empty;
            try
            {
                using (RNGCryptoServiceProvider rng = new RNGCryptoServiceProvider())
                {
                    // Buffer storage.
                    byte[] data = new byte[4];

                    // Ten iterations.                    
                    {
                        // Fill buffer.
                        rng.GetBytes(data);

                        // Convert to int 32.
                        int value = BitConverter.ToInt32(data, 0);
                        if (value < 0)
                            value = -value;
                        if (value.ToString().Length > 10)
                        {
                            uniqueNumber = value.ToString().Substring(0, 10);
                        }
                        else
                        {
                            uniqueNumber = value.ToString();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogFileWrite(ex.Message);
            }
            orderNumber = orderNumber + uniqueNumber;
            return orderNumber;
        }
        #endregion

    }
}