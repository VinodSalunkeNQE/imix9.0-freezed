﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using log4net;
using log4net.Config;

namespace DigiPhoto.SyncManager
{
    class Program
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(Program));
        static void Main(string[] args)
        {
            log4net.Config.XmlConfigurator.Configure();
            if (log.IsInfoEnabled)
                log.Info(String.Format("Sync Manager Started at : {0}  {1}", DateTime.Now.ToLongDateString(), DateTime.Now.ToLongTimeString()));
            Console.WriteLine(String.Format("Sync Manager Started at : {0}  {1}", DateTime.Now.ToLongDateString(), DateTime.Now.ToLongTimeString()));
            while (true)
            {
                SyncHandler syncHandlerObject = SyncHandler.GetObject();
                if (syncHandlerObject != null)
                {
                    syncHandlerObject.ProcessIncomingChange();
                }
            }
        }
    }
}
