namespace DigiPhoto.DigiSync.ClientDataAccess.DataModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class DG_PhotoNumberConfiguration
    {
        [Key]
        public int DG_PhotoSequence_pkey { get; set; }

        [StringLength(50)]
        public string DG_WifiName { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? DG_StartingNumber { get; set; }
    }
}
