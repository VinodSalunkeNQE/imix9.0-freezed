namespace DigiPhoto.DigiSync.ClientDataAccess.DataModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class DG_Permission_Role
    {
        [Key]
        public int DG_Permission_Role_pkey { get; set; }

        public int DG_User_Roles_Id { get; set; }

        public int DG_Permission_Id { get; set; }
    }
}
