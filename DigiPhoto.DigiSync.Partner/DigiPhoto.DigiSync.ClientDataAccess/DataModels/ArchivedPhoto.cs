namespace DigiPhoto.DigiSync.ClientDataAccess.DataModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ArchivedPhotos")]
    public partial class ArchivedPhoto
    {
        public long ArchivedPhotoId { get; set; }

        public int DG_Photos_pkey { get; set; }

        [Required]
        [StringLength(150)]
        public string DG_Photos_FileName { get; set; }

        public DateTime DG_Photos_CreatedOn { get; set; }

        [StringLength(150)]
        public string DG_Photos_RFID { get; set; }

        public int? DG_Photos_UserID { get; set; }

        [StringLength(150)]
        public string DG_Photos_Background { get; set; }

        [StringLength(150)]
        public string DG_Photos_Frame { get; set; }

        public DateTime? DG_Photos_DateTime { get; set; }

        [Column(TypeName = "xml")]
        public string DG_Photos_Layering { get; set; }

        [Column(TypeName = "xml")]
        public string DG_Photos_Effects { get; set; }

        public bool? DG_Photos_IsCroped { get; set; }

        public bool? DG_Photos_IsRedEye { get; set; }

        public bool? DG_Photos_IsGreen { get; set; }

        [Column(TypeName = "xml")]
        public string DG_Photos_MetaData { get; set; }

        public string DG_Photos_Sizes { get; set; }

        public bool? DG_Photos_Archive { get; set; }

        public int? DG_Location_Id { get; set; }

        public int? DG_SubStoreId { get; set; }

        public DateTime CreatedOn { get; set; }

        public bool FileDeleted { get; set; }

        public DateTime? FileDeletedOn { get; set; }
    }
}
