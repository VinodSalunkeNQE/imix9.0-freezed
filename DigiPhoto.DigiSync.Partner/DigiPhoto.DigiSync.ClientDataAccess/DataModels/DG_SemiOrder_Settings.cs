namespace DigiPhoto.DigiSync.ClientDataAccess.DataModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class DG_SemiOrder_Settings
    {
        [Key]
        public int DG_SemiOrder_Settings_Pkey { get; set; }

        public bool? DG_SemiOrder_Settings_AutoBright { get; set; }

        public double? DG_SemiOrder_Settings_AutoBright_Value { get; set; }

        public bool? DG_SemiOrder_Settings_AutoContrast { get; set; }

        public double? DG_SemiOrder_Settings_AutoContrast_Value { get; set; }

        [StringLength(350)]
        public string DG_SemiOrder_Settings_ImageFrame { get; set; }

        public bool? DG_SemiOrder_Settings_IsImageFrame { get; set; }

        public int? DG_SemiOrder_ProductTypeId { get; set; }

        [StringLength(350)]
        public string DG_SemiOrder_Settings_ImageFrame_Vertical { get; set; }

        public bool? DG_SemiOrder_Environment { get; set; }

        [StringLength(350)]
        public string DG_SemiOrder_BG { get; set; }

        public bool? DG_SemiOrder_Settings_IsImageBG { get; set; }

        public string DG_SemiOrder_Graphics_layer { get; set; }

        [StringLength(200)]
        public string DG_SemiOrder_Image_ZoomInfo { get; set; }

        public int? DG_SubStoreId { get; set; }
    }
}
