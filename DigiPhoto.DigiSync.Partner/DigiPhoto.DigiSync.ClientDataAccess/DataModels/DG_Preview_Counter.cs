namespace DigiPhoto.DigiSync.ClientDataAccess.DataModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class DG_Preview_Counter
    {
        public int ID { get; set; }

        public int PhotoId { get; set; }

        public DateTime? PreviewDate { get; set; }
    }
}
