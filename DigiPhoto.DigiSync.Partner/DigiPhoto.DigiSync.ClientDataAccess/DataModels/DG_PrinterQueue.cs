namespace DigiPhoto.DigiSync.ClientDataAccess.DataModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class DG_PrinterQueue
    {
        [Key]
        public int DG_PrinterQueue_Pkey { get; set; }

        public int? DG_PrinterQueue_ProductID { get; set; }

        public string DG_PrinterQueue_Image_Pkey { get; set; }

        public int? DG_Associated_PrinterId { get; set; }

        public int? DG_Order_Details_Pkey { get; set; }

        public bool? DG_SentToPrinter { get; set; }

        public bool? is_Active { get; set; }

        public int? QueueIndex { get; set; }

        public bool? DG_IsSpecPrint { get; set; }

        public DateTime? DG_Print_Date { get; set; }

        public string RotationAngle { get; set; }
    }
}
