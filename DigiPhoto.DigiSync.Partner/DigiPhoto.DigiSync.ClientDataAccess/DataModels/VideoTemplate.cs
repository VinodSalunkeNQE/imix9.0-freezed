namespace DigiPhoto.DigiSync.ClientDataAccess.DataModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("VideoTemplate")]
    public partial class VideoTemplate
    {
        public VideoTemplate()
        {
            VideoSlots = new HashSet<VideoSlot>();
        }

        public long VideoTemplateId { get; set; }

        [Required]
        [StringLength(150)]
        public string Name { get; set; }

        [StringLength(200)]
        public string Description { get; set; }

        [Required]
        [StringLength(50)]
        public string DisplayName { get; set; }

        public long VideoLength { get; set; }

        public int CreatedBy { get; set; }

        public DateTime CreatedOn { get; set; }

        public int? ModifiedBy { get; set; }

        public DateTime? ModifiedOn { get; set; }

        public bool? IsActive { get; set; }

        public virtual ICollection<VideoSlot> VideoSlots { get; set; }
    }
}
