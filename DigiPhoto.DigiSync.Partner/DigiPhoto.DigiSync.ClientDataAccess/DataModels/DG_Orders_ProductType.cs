namespace DigiPhoto.DigiSync.ClientDataAccess.DataModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class DG_Orders_ProductType
    {
        [Key]
        public int DG_Orders_ProductType_pkey { get; set; }

        [StringLength(150)]
        public string DG_Orders_ProductType_Name { get; set; }

        [StringLength(500)]
        public string DG_Orders_ProductType_Desc { get; set; }

        public bool? DG_Orders_ProductType_IsBundled { get; set; }

        public bool? DG_Orders_ProductType_DiscountApplied { get; set; }

        [StringLength(500)]
        public string DG_Orders_ProductType_Image { get; set; }

        public bool DG_IsPackage { get; set; }

        public int DG_MaxQuantity { get; set; }

        public bool? DG_Orders_ProductType_Active { get; set; }

        public bool? DG_IsActive { get; set; }
        public bool? IsPersonalizedAR { get; set; }
        public bool? DG_IsAccessory { get; set; }

        public bool? DG_IsPrimary { get; set; }

        [StringLength(100)]
        public string DG_Orders_ProductCode { get; set; }

        public int? DG_Orders_ProductNumber { get; set; }

        public bool? DG_IsBorder { get; set; }

        [StringLength(50)]
        public string SyncCode { get; set; }
        public bool IsSynced { get; set; }
        public int? DG_SubStore_pkey { get; set; }
        #region Added by Ajay on 13 June 18
        public bool IsPanorama { get; set; } 
        #endregion
    }
}
