﻿namespace DigiPhoto.DigiSync.ClientDataAccess.DataModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("GetDateNow")]
    public partial class GetDateNow
    {
        [Key]
        public DateTime GetDateTimeNow { get; set; }
    }
}
