﻿using DigiPhoto.DigiSync.ClientDataAccess.DataModels;
using DigiPhoto.DigiSync.Model;
using DigiPhoto.DigiSync.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DigiPhoto.DigiSync.ClientDataProcessor
{
    public class RoleProcessor : BaseProcessor
    {
        protected override void ProcessInsert()
        {
            try
            {
                if (!string.IsNullOrEmpty(ChangeInfo.dataXML))
                {
                    RoleInfo roleInfo = CommonUtility.DeserializeXML<RoleInfo>(ChangeInfo.dataXML);

                    using (DigiPhotoContext dbContext = new DigiPhotoContext())
                    {
                        var existingRoleCount = dbContext.DG_User_Roles.Count(r => r.SyncCode == roleInfo.SyncCode);
                        if (existingRoleCount == 0)
                        {
                            var role = new DG_User_Roles();
                            role.DG_User_Role = roleInfo.RoleName;
                            role.SyncCode = roleInfo.SyncCode;
                            role.IsSynced = true;
                            dbContext.DG_User_Roles.Add(role);
                            dbContext.SaveChanges();

                            //role id which is to be used in dg_permissionRole which is just inserted

                            int RoleId = dbContext.DG_User_Roles.Where(r => r.SyncCode == roleInfo.SyncCode).FirstOrDefault().DG_User_Roles_pkey;

                            DG_Permission_Role dbPermissionRole = new DG_Permission_Role();

                            foreach (string s in roleInfo.ModulesSyncCodes)
                            {
                                var value = dbContext.DG_Permissions.Count(r => r.SyncCode == s);
                                if (value != 0)
                                {
                                    int DG_Permission = dbContext.DG_Permissions.Where(r => r.SyncCode == s).FirstOrDefault().DG_Permission_pkey;
                                    dbPermissionRole.DG_Permission_Id = DG_Permission;
                                    dbPermissionRole.DG_User_Roles_Id = RoleId;
                                    dbContext.DG_Permission_Role.Add(dbPermissionRole);
                                    dbContext.SaveChanges();
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Exception outerException = new ProcessingException("Error occured in Role Processor Insert.", e);
                outerException.Source = "ClientDataProcessor.RoleProcessor";
                throw outerException;
            }
        }
        protected override void ProcessUpdate()
        {
            try
            {
                if (!string.IsNullOrEmpty(ChangeInfo.dataXML))
                {
                    RoleInfo roleInfo = CommonUtility.DeserializeXML<RoleInfo>(ChangeInfo.dataXML);
                    using (DigiPhotoContext dbContext = new DigiPhotoContext())
                    {
                        var dbRole = dbContext.DG_User_Roles.Where(r => r.SyncCode == roleInfo.SyncCode).FirstOrDefault();
                        if (dbRole != null)
                        {
                            //Update Role Name
                            dbRole.DG_User_Role = roleInfo.RoleName;
                            dbRole.IsSynced = true;
                            dbContext.SaveChanges();
                            //get the list of modules from rolemodule table corresponding to the given RoleId from web Role
                            //loop  and change d roleid 

                            int RoleId = dbContext.DG_User_Roles.Where(r => r.SyncCode == roleInfo.SyncCode).FirstOrDefault().DG_User_Roles_pkey;

                            dbContext.Database.ExecuteSqlCommand("DELETE FROM DG_Permission_Role WHERE DG_User_Roles_Id = {0}", RoleId);

                            DG_Permission_Role dbPermissionRole = new DG_Permission_Role();

                            foreach (string s in roleInfo.ModulesSyncCodes)
                            {
                                var value = dbContext.DG_Permissions.Count(r => r.SyncCode == s);
                                if (value != 0)
                                {
                                    int DG_Permission = dbContext.DG_Permissions.Where(r => r.SyncCode == s).FirstOrDefault().DG_Permission_pkey;
                                    dbPermissionRole.DG_Permission_Id = DG_Permission;
                                    dbPermissionRole.DG_User_Roles_Id = RoleId;
                                    dbContext.DG_Permission_Role.Add(dbPermissionRole);
                                    dbContext.SaveChanges();
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Exception outerException = new ProcessingException("Error occured in Role Processor Update.", e);
                outerException.Source = "ClientDataProcessor.RoleProcessor";
                throw outerException;
            }
        }
        protected override void ProcessDelete()
        {
            try
            {
                using (DigiPhotoContext dbContext = new DigiPhotoContext())
                {
                    var dbRole = dbContext.DG_User_Roles.Where(r => r.SyncCode == ChangeInfo.EntityCode).FirstOrDefault();
                    if (dbRole != null)
                    {

                        int cnt = dbContext.DG_Permission_Role.Count(r => r.DG_User_Roles_Id == dbRole.DG_User_Roles_pkey);
                        for (int i = 0; i <= cnt - 1; i++)
                        {
                            var dbPermissionRole = dbContext.DG_Permission_Role.Where(r => r.DG_User_Roles_Id == dbRole.DG_User_Roles_pkey).FirstOrDefault();
                            dbContext.DG_Permission_Role.Remove(dbPermissionRole);
                            dbContext.SaveChanges();
                        }

                        dbContext.DG_User_Roles.Remove(dbRole);
                        dbContext.SaveChanges();
                    }
                }

            }
            catch (Exception e)
            {
                Exception outerException = new ProcessingException("Error occured in Role Processor Delete.", e);
                outerException.Source = "ClientDataProcessor.RoleProcessor";
                throw outerException;
            }
        }
    }
}
