﻿using DigiPhoto.DigiSync.ClientDataAccess.DataModels;
using DigiPhoto.DigiSync.Model;
using DigiPhoto.DigiSync.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DigiPhoto.DigiSync.ClientDataProcessor
{
    public class CurrencyProcessor:BaseProcessor
    {
        protected override void ProcessInsert()
        {
            try
            {
                if (!string.IsNullOrEmpty(ChangeInfo.dataXML))
                {
                    CurrencyInfo currencyInfo = CommonUtility.DeserializeXML<CurrencyInfo>(ChangeInfo.dataXML);
                    using (DigiPhotoContext dbContext = new DigiPhotoContext())
                    {
                        var existingCurrencyCount = dbContext.DG_Currency.Count
                            (r => r.SyncCode == currencyInfo.SyncCode || currencyInfo.Name.ToLower().Equals(r.DG_Currency_Name.ToLower()));
                        if (existingCurrencyCount == 0)
                        {
                            var currency = new DG_Currency();
                            currency.DG_Currency_Name = currencyInfo.Name;
                            currency.DG_Currency_Rate = currencyInfo.Rate;
                            currency.DG_Currency_Symbol = currencyInfo.Symbol;
                            currency.DG_Currency_Default = false;// currencyInfo.IsDefault;
                            //currency.DG_Currency_Icon = currencyInfo.CurrencyIcon;
                            currency.DG_Currency_Code = currencyInfo.CurrencyCode;
                            currency.SyncCode = currencyInfo.SyncCode;
                            currency.DG_Currency_IsActive = Convert.ToBoolean(1);
                            currency.DG_Currency_ModifiedBy = Convert.ToInt32(currencyInfo.ModifiedBy);
                            currency.DG_Currency_UpdatedDate = currencyInfo.ModifiedDateTime;
                            currency.IsSynced = true;
                            dbContext.DG_Currency.Add(currency);
                            dbContext.SaveChanges();
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Exception outerException = new ProcessingException("Error occured in Currency Processor Insert.", e);
                outerException.Source = "ClientDataProcessor.CurrencyProcessor";
                throw outerException;
            }
        }
        //}
        protected override void ProcessUpdate()
        {
            try
            {
                if (!string.IsNullOrEmpty(ChangeInfo.dataXML))
                {
                    CurrencyInfo currencyInfo = CommonUtility.DeserializeXML<CurrencyInfo>(ChangeInfo.dataXML);
                    using (DigiPhotoContext dbContext = new DigiPhotoContext())
                    {
                        var dbCurrency = dbContext.DG_Currency.Where(r => r.SyncCode == currencyInfo.SyncCode && currencyInfo.Name.ToLower().Equals(r.DG_Currency_Name.ToLower())).FirstOrDefault();
                        if (dbCurrency != null)
                        {
                            //Update Role Name
                            dbCurrency.DG_Currency_Name = currencyInfo.Name;
                            dbCurrency.DG_Currency_Rate = currencyInfo.Rate;
                            dbCurrency.DG_Currency_Symbol = currencyInfo.Symbol;
                            //dbCurrency.DG_Currency_Default = currencyInfo.IsDefault;
                            dbCurrency.DG_Currency_IsActive = Convert.ToBoolean(1);
                            //dbCurrency.DG_Currency_Icon = currencyInfo.CurrencyIcon;
                            dbCurrency.DG_Currency_Code = currencyInfo.CurrencyCode;
                            dbCurrency.DG_Currency_ModifiedBy = Convert.ToInt32(currencyInfo.ModifiedBy);
                            dbCurrency.DG_Currency_UpdatedDate = currencyInfo.ModifiedDateTime;
                            dbCurrency.IsSynced = true;
                            dbContext.SaveChanges();            //If multiple update operations are done, commit will be at last.
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Exception outerException = new ProcessingException("Error occured in Currency Processor Update.", e);
                outerException.Source = "ClientDataProcessor.CurrencyProcessor";
                throw outerException;
            }
        }
        protected override void ProcessDelete()
        {
            try
            {
               
                    
                    using (DigiPhotoContext dbContext = new DigiPhotoContext())
                    {
                        var dbCurrency = dbContext.DG_Currency.Where(r => r.SyncCode == ChangeInfo.EntityCode).FirstOrDefault();
                        if (dbCurrency != null)
                        {
                            dbContext.DG_Currency.Remove(dbCurrency);
                            dbContext.SaveChanges();            //If multiple remove operations are done, commit will be at last.
                        }
                    }
              
            }
            catch (Exception e)
            {
                Exception outerException = new ProcessingException("Error occured in Currency Processor Delete.", e);
                outerException.Source = "ClientDataProcessor.CurrencyProcessor";
                throw outerException;
            }
        }
    }
}
