﻿using DigiPhoto.DigiSync.ClientDataAccess.DataModels;
using DigiPhoto.DigiSync.Model;
using DigiPhoto.DigiSync.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DigiPhoto.DigiSync.ClientDataProcessor.Serialization
{
    public class DiscountSerializer : Serializer
    {
        public override string Serialize(long ObjectValueId)
        {
            try
            {
                string dataXML = string.Empty;
                using (DigiPhotoContext dbContext = new DigiPhotoContext())
                {
                    var discount = dbContext.DG_Orders_DiscountType.Where(r => r.DG_Orders_DiscountType_Pkey == ObjectValueId).FirstOrDefault();
                    if (discount != null)
                    {
                        DiscountInfo discountInfo = new DiscountInfo()
                            {
                                Name = discount.DG_Orders_DiscountType_Name,
                                Description = discount.DG_Orders_DiscountType_Desc,
                                IsActive = Convert.ToBoolean(discount.DG_Orders_DiscountType_Active),
                                Code = discount.DG_Orders_DiscountType_Code,
                                Secure = discount.DG_Orders_DiscountType_Secure,
                                ItemLevel = discount.DG_Orders_DiscountType_ItemLevel,
                                AsPercentage = discount.DG_Orders_DiscountType_AsPercentage,
                                SyncCode = discount.SyncCode,
                            };

                        dataXML = CommonUtility.SerializeObject<DiscountInfo>(discountInfo);
                    }
                    return dataXML;
                }
            }
            catch (Exception e)
            {
                Exception outerException = new SerializationException("Error occured in Order Serialization.", e);
                outerException.Source = "Serialization.SubStoreSerializer";
                throw outerException;
            }
        }

        public override object Deserialize(string input)
        {
            throw new Exception("Not emplemented");
        }
    }
}
