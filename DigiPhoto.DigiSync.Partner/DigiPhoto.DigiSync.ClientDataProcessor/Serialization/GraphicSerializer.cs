﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DigiPhoto.DigiSync.ClientDataAccess.DataModels;
using DigiPhoto.DigiSync.Model;
using DigiPhoto.DigiSync.Utility;
using System.IO;

namespace DigiPhoto.DigiSync.ClientDataProcessor.Serialization
{
    public class GraphicSerializer : Serializer, IUploadable
    {
        public override string Serialize(long ObjectValueId)
        {
            try
            {
                string dataXML = string.Empty;
                using (DigiPhotoContext dbContext = new DigiPhotoContext())
                {
                    var graphic = dbContext.DG_Graphics.Where(r => r.DG_Graphics_pkey == ObjectValueId).FirstOrDefault();
                    //Code Added by Anis for Graphic issue
                    //var siteSynccode = dbContext.DG_SubStores.Select(s => s.SyncCode.ToString()).FirstOrDefault();
                    var siteSynccode = dbContext.DG_SubStores.Select(s => s.SyncCode).FirstOrDefault();
                    //End
                    if (graphic != null)
                    {
                        GraphicInfo graphicInfo = new GraphicInfo()
                        {
                            Name = graphic.DG_Graphics_Name,
                            DisplayName = graphic.DG_Graphics_Displayname,
                            IsActive = graphic.DG_Graphics_IsActive,
                            SyncCode = graphic.SyncCode,
                            ModifiedDate = System.DateTime.Now,
                            SiteCodeSyncCode = siteSynccode,
                        };
                        if (graphic.CreatedBy.HasValue)
                            graphicInfo.CreatedBy = graphic.CreatedBy.Value;
                        if (graphic.CreatedDate.HasValue)
                            graphicInfo.CreatedDate = graphic.CreatedDate.Value;
                        else
                            graphicInfo.CreatedDate = DateTime.Now;

                        if (graphic.ModifiedBy.HasValue)
                            graphicInfo.ModifiedBy = graphic.ModifiedBy.Value;

                        var configurationData = dbContext.DG_Configuration.FirstOrDefault();
                        if (configurationData != null)
                        {
                            //string Folderpath = ConfigurationManager.AppSettings["DigiphotoBorderPath"].ToString();
                            uploadFile = new UploadFile();
                            uploadFile.UploadFilePath = Path.Combine(configurationData.DG_Graphics_Path, graphic.DG_Graphics_Name);
                            uploadFile.FileType = 3;
                        }
                        dataXML = CommonUtility.SerializeObject<GraphicInfo>(graphicInfo);
                    }

                    return dataXML;
                }
            }
            catch (Exception e)
            {
                Exception outerException = new SerializationException("Error occured in Graphic Serialization.", e);
                outerException.Source = "Serialization.GraphicSerializer";
                throw outerException;
            }
        }
        public override object Deserialize(string input)
        {
            throw new Exception("Not emplemented");
        }

        private UploadFile uploadFile;
        public UploadFile UploadFile
        {
            get
            {
                return uploadFile;
            }
            set
            {
                uploadFile = value;
            }
        }

        private List<UploadInfo> uploadList;
        public List<UploadInfo> UploadList
        {
            get
            {
                return uploadList;
            }
            set
            {
                uploadList = value;
            }
        }

    }
}
