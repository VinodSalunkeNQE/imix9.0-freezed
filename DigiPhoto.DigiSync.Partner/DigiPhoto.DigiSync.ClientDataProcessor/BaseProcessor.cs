﻿using DigiPhoto.DigiSync.ClientDataAccess.DataModels;
using DigiPhoto.DigiSync.Model;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using DigiPhoto.DigiSync.Utility;
using log4net;

namespace DigiPhoto.DigiSync.ClientDataProcessor
{
    public abstract class BaseProcessor : IProcesser
    {
        private static ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private ChangeInfo _ChangeInfo;
        private Int32 _IncomingChangeId;
        protected ChangeInfo ChangeInfo
        {
            get { return _ChangeInfo; }
            set { _ChangeInfo = value; }
        }
        public void Process(ChangeInfo changeInfo)
        {
            this._ChangeInfo = changeInfo;
            try
            {
                PreProcess();

                if (ConfigurationManager.AppSettings["ProcessChange"].Equals("true"))
                {
                    //if (!string.IsNullOrEmpty(_ChangeInfo.dataXML))
                    //{
                    try
                    {
                        DoProcess();
                        UpdateProcessingStatus(_IncomingChangeId, (int)SyncProcessingStatus.Processed);
                    }
                    catch (Exception ex)
                    {
                        UpdateProcessingStatus(_IncomingChangeId, (int)SyncProcessingStatus.Error);
                        log.Error("ClientDataProcessor:BaseProcessor:Process:DoProcess:: Message: " + ex.Message + " StackTrace: " + ex.StackTrace + " InnerException: " + ex.InnerException + " Source: " + ex.Source);
                    }
                    //}
                    PostProcess();
                }
            }
            catch (Exception ex)
            {
                log.Error("ClientDataProcessor:BaseProcessor:Process:: Message: " + ex.Message + " StackTrace: " + ex.StackTrace + " InnerException: " + ex.InnerException + " Source: " + ex.Source);
                throw ex;
            }
        }
        protected virtual void PreProcess()
        {
            //Log this change to DB for future reference. //TBD
            if (ConfigurationManager.AppSettings["LogIncomingChanges"].Equals("true"))
            {
                using (DigiPhotoContext dbContext = new DigiPhotoContext())
                {
                    IncomingChange incomingChnage = new IncomingChange()
                    {
                        ChangeTrackingId = _ChangeInfo.ChangeTrackingId,
                        ApplicationObjectId = _ChangeInfo.ApplicationObjectId,
                        ObjectValueId = _ChangeInfo.ObjectValueId,
                        ChangeAction = _ChangeInfo.ChangeAction,
                        ChangeByOnServer = _ChangeInfo.ChangeBy,
                        ChangeDateOnServer = _ChangeInfo.ChangeDate,
                        ChangeRecievedOn = DateTime.Now,
                        DataXML = string.IsNullOrEmpty(_ChangeInfo.dataXML) ? null : _ChangeInfo.dataXML,
                        SyncSessionId = _ChangeInfo.sessionId
                    };

                    dbContext.IncomingChanges.Add(incomingChnage);
                    dbContext.SaveChanges();
                    _IncomingChangeId = incomingChnage.IncomingChangeId;
                }
            }
        }
        private void DoProcess()
        {
            if (!string.IsNullOrEmpty(_ChangeInfo.dataXML))
            {
                if (this._ChangeInfo.ChangeAction == 1)
                    ProcessInsert();
                else if (this._ChangeInfo.ChangeAction == 2)
                    ProcessUpdate();
                else if (this._ChangeInfo.ChangeAction == 3)
                    ProcessDelete();
            }
            else
            {
                if (this._ChangeInfo.ChangeAction == 3)
                    ProcessDelete();
            }
        }

        protected virtual void ProcessInsert()
        { }
        protected virtual void ProcessUpdate()
        { }
        protected virtual void ProcessDelete()
        { }
        protected virtual void PostProcess()
        {
            //Update Last LastChangeDetails
            using (DigiPhotoContext dbContext = new DigiPhotoContext())
            {
                var dbLastChangeDetails = dbContext.LastChangeDetails.Where(r => r.SubStoreId == _ChangeInfo.SubStore.SubStoreId).FirstOrDefault();

                if (dbLastChangeDetails != null)
                {
                    dbLastChangeDetails.LastChangeDate = DateTime.Now;
                    dbLastChangeDetails.LastChangeTrackingId = _ChangeInfo.ChangeTrackingId;
                    dbLastChangeDetails.SubStoreId = _ChangeInfo.SubStore.SubStoreId;
                    dbContext.SaveChanges();
                }
                else
                {
                    dbLastChangeDetails = new LastChangeDetail();
                    dbLastChangeDetails.LastChangeDate = DateTime.Now;
                    dbLastChangeDetails.LastChangeTrackingId = _ChangeInfo.ChangeTrackingId;
                    dbLastChangeDetails.SubStoreId = _ChangeInfo.SubStore.SubStoreId;
                    dbContext.LastChangeDetails.Add(dbLastChangeDetails);
                    dbContext.SaveChanges();
                }

            }
        }
        public static void UpdateProcessingStatus(long incomingChangeId, int processingStatus)
        {
            try
            {
                using (DigiPhotoContext dbContext = new DigiPhotoContext())
                {
                    var incomingChange = dbContext.IncomingChanges.Where(i => i.IncomingChangeId == incomingChangeId).FirstOrDefault();
                    incomingChange.ProcessingStatus = processingStatus;
                    dbContext.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                log.Error("UpdateProcessingStatus:: Message: " + ex.Message + " StackTrace: " + ex.StackTrace + " InnerException: " + ex.InnerException + " Source: " + ex.Source);
                throw ex;
            }

        }
    }
}
