﻿using DigiPhoto.DigiSync.ClientDataAccess.DataModels;
using DigiPhoto.DigiSync.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using DigiPhoto.DigiSync.Utility;
using DigiPhoto.DigiSync.ClientDataProcessor.Serialization;
using log4net;
using System.Data.Entity.Validation;
using System.Data.SqlClient;

namespace DigiPhoto.DigiSync.ClientDataProcessor.Controller
{
    public class SyncController
    {
        public static bool IsOnline = false;

        private static bool IsResyncActive = false;
        private static DateTime InitialResyncDateTime = DateTime.Now;
        private static Int32 ResyncIntervalCount = 0;
        private static string ResyncIntervalType = string.Empty;
        private static Int32 ResyncMaxRetryCount = 5;
        private static DateTime scheduleSyncTime;

        private static readonly ILog log = LogManager.GetLogger(typeof(SyncController));

        static string hotfolderPath = string.Empty;
        public static long SaveSyncHistory(SyncSessionInfo syncSessionInfo)
        {
            try
            {
                using (DigiPhotoContext dbContext = new DigiPhotoContext())
                {
                    SyncSession syncHistory = new SyncSession()
                    {
                        TokenId = syncSessionInfo.SessionTokenId,
                        StoreCode = syncSessionInfo.ClientStoreCode,
                        SiteCode = syncSessionInfo.ClientSiteCode,
                        VenueCode = syncSessionInfo.ClientVenueCode,
                        SignInDate = syncSessionInfo.SignInDate,
                        Status = syncSessionInfo.Status,
                        StartChangeTrackingId = syncSessionInfo.StartChangeTrackingId
                    };
                    dbContext.SyncSessions.Add(syncHistory);
                    dbContext.SaveChanges();
                    return syncHistory.SyncSessionId;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static void UpdateSyncStatus(SyncSessionInfo syncSessionInfo)
        {
            try
            {
                using (DigiPhotoContext dbContext = new DigiPhotoContext())
                {
                    var dbSyncSession = dbContext.SyncSessions.Where(s => s.SyncSessionId == syncSessionInfo.SyncSessionId).FirstOrDefault();
                    if (dbSyncSession != null)
                    {
                        //Update Role Name
                        dbSyncSession.SignOutDate = syncSessionInfo.SignOutDate;
                        dbSyncSession.Status = syncSessionInfo.Status;
                        dbSyncSession.EndChangeTrackingId = syncSessionInfo.EndChangeTrackingId;
                        dbContext.SaveChanges();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public static ChangeInfo GetLastChangeInfo()
        {
            try
            {

                using (DigiPhotoContext dbContext = new DigiPhotoContext())
                {
                    ChangeInfo lastChange = new ChangeInfo();
                    var syncSession = (from s in dbContext.SyncSessions
                                       orderby s.SyncSessionId descending
                                       select s).FirstOrDefault();
                    if (syncSession != null)
                    {
                        lastChange = new ChangeInfo
                        {
                            ChangeTrackingId = (syncSession.EndChangeTrackingId.HasValue ? syncSession.EndChangeTrackingId.Value : 0),
                            ChangeDate = syncSession.SignOutDate.HasValue ? syncSession.SignOutDate.Value : DateTime.MinValue
                        };
                    }
                    return lastChange;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static List<SiteInfo> GetSites()
        {
            try
            {
                using (DigiPhotoContext dbContext = new DigiPhotoContext())
                {
                    StoreInfo store = (from s in dbContext.DG_Store
                                       select new StoreInfo
                                       {

                                           StoreCode = s.StoreCode,
                                           StoreName = s.DG_Store_Name,
                                           Country = new CountryInfo
                                           {
                                               CountryCode = s.CountryCode,
                                               CountryName = s.Country
                                           }
                                       }).FirstOrDefault();


                    List<SiteInfo> sites = new List<SiteInfo>();
                    SiteInfo site = new SiteInfo();
                    sites = (from s in dbContext.DG_SubStores
                             join c in dbContext.DG_Configuration on s.DG_SubStore_pkey equals c.DG_Substore_Id
                             //join l in dbContext.DG_Location on sl.DG_Location_ID equals l.DG_Location_pkey
                             select new SiteInfo
                             {
                                 SubStoreId = s.DG_SubStore_pkey,
                                 SiteCode = s.SyncCode, // replace by SyncCode
                                 SiteName = s.DG_SubStore_Name,
                                 HotFolderPath = c.DG_Hot_Folder_Path,
                                 DG_SiteCode = s.DG_SubStore_Code,
                                 Store = new StoreInfo
                                 {
                                     StoreCode = store.StoreCode,
                                     StoreName = store.StoreName,
                                     Country = new CountryInfo
                                     {
                                         CountryCode = store.Country.CountryCode,
                                         CountryName = store.Country.CountryName
                                     }
                                 }
                             }
                             ).ToList();
                    return sites;
                }
            }
            catch (Exception ex)
            {
                log.Error("SyncController:GetSites:: Message: " + ex.Message + " StackTrace: " + ex.StackTrace + " InnerException: " + ex.InnerException + " Source: " + ex.Source);
                throw ex;
            }
        }

        public static ChangeInfo GetLastChangeDetail(SiteInfo sites)
        {
            try
            {

                //With Help of SqlHelper fetch authorized change by stored proc

                using (DigiPhotoContext dbContext = new DigiPhotoContext())
                {
                    ChangeInfo lastChange;
                    lastChange = (from s in dbContext.LastChangeDetails
                                  where s.SubStoreId == sites.SubStoreId
                                  orderby s.LastChangeDetailId descending
                                  select new ChangeInfo
                                  {
                                      ChangeTrackingId = s.LastChangeTrackingId,
                                      ChangeDate = s.LastChangeDate,
                                      SubStore = new SiteInfo
                                      {
                                          SiteCode = sites.SiteCode,
                                          SiteName = sites.SiteName,
                                          SubStoreId = s.SubStoreId,
                                          DG_SiteCode = sites.DG_SiteCode,
                                          Store = new StoreInfo
                                          {
                                              StoreCode = sites.Store.StoreCode,
                                              StoreName = sites.Store.StoreName,
                                              Country = new CountryInfo
                                              {
                                                  CountryCode = sites.Store.Country.CountryCode,
                                                  CountryName = sites.Store.Country.CountryName
                                              }
                                          },

                                      }
                                  }).FirstOrDefault();
                    //if (lastChange != null)
                    //{
                    //    lastChange = new ChangeInfo
                    //    {
                    //        ChangeTrackingId = (syncSession.EndChangeTrackingId.HasValue ? syncSession.EndChangeTrackingId.Value : 0),
                    //        ChangeDate = syncSession.SignOutDate.HasValue ? syncSession.SignOutDate.Value : DateTime.MinValue
                    //    };
                    //}
                    return lastChange;
                }
            }
            catch (Exception ex)
            {
                log.Error("SyncController:GetLastChangeDetail:: Message: " + ex.Message + " StackTrace: " + ex.StackTrace + " InnerException: " + ex.InnerException + " Source: " + ex.Source);

                throw ex;
            }
        }
        #region Sync Service Enhancement - Ashirwad
        public static bool? GetOrderCancelStatus(long ObjectValueId)
        {
            try
            {
                using (DigiPhotoContext dbContext = new DigiPhotoContext())
                {
                    var isCancelList = (from DO in dbContext.DG_Orders
                                        where DO.DG_Orders_pkey == ObjectValueId
                                        select DO);

                    foreach (DG_Orders objectOrder in isCancelList)
                    {
                        return objectOrder.DG_Orders_Canceled;
                    }

                    return false;
                }
            }
            catch (Exception ex)
            {
                log.Error("SyncController:GetOrderCancelStatus:: Message: " + ex.Message + " StackTrace: " + ex.StackTrace + " InnerException: " + ex.InnerException + " Source: " + ex.Source);
                throw ex;
            }
        }
        public static List<QrCodeStatus> GetPendingOnlineOrders(int syncDataDelay)
        {

            List<QrCodeStatus> qrCodeList = new List<QrCodeStatus>();
            DateTime changeDateOld = (DateTime.Now.AddDays(Convert.ToDouble(-15)));
            using (DigiPhotoContext dbContext = new DigiPhotoContext())
            {
                List<DG_Orders_Details> qrCodechangeTracking = new List<DG_Orders_Details>();
                qrCodechangeTracking = (from DOD in dbContext.DG_Orders_Details
                                        join DO in dbContext.DG_Orders
                                        on DOD.DG_Orders_ID equals DO.DG_Orders_pkey
                                        join PT in dbContext.DG_Orders_ProductType
                                        on DOD.DG_Orders_Details_ProductType_pkey equals PT.DG_Orders_ProductType_pkey
                                        join QCT in dbContext.ChangeTrackings
                                        on DO.DG_Orders_pkey equals QCT.ObjectValueId
                                        where
                                        QCT.ApplicationObjectId == 14 //&& QCT.ChangeAction==2
                                        && QCT.ChangeDate >= changeDateOld //get last 15 days data
                                        && (QCT.SyncStatus == 3 || QCT.SyncStatus == 11 || QCT.SyncStatus == 12 || QCT.SyncStatus == 13 || QCT.SyncStatus == 14)
                                        && PT.DG_Orders_ProductType_pkey == 84
                                        //&& ct.SyncStatus != 0
                                        select DOD).ToList();

                if (qrCodechangeTracking != null && qrCodechangeTracking.Count() > 0)
                {
                    foreach (DG_Orders_Details changeTracking in qrCodechangeTracking)
                    {
                        QrCodeStatus obj = new QrCodeStatus();
                        obj.IdentificationCode = changeTracking.DG_Order_ImageUniqueIdentifier;
                        var isCancelList = (from DO in dbContext.DG_Orders
                                            where DO.DG_Orders_pkey == changeTracking.DG_Orders_ID
                                            select DO);

                        foreach (DG_Orders objectOrder in isCancelList)
                        {
                            obj.OrderNumber = objectOrder.DG_Orders_Number;
                        }

                        qrCodeList.Add(obj);
                    }

                }


                //            FROM dbo.DG_Orders_Details DOD   WITH(NOLOCK)
                //INNER JOIN dbo.DG_Orders DO WITH(NOLOCK)
                //  ON DO.DG_Orders_pkey = DOD.DG_Orders_ID
                //INNER JOIN dbo.DG_Orders_ProductType PType WITH(NOLOCK)
                //  ON DOD.DG_Orders_Details_ProductType_pkey = PType.DG_Orders_ProductType_pkey
                //INNER JOIN dbo.ChangeTracking CT WITH(NOLOCK)
                //  ON CT.ObjectValueId = DO.DG_Orders_pkey   AND CT.ApplicationObjectId = 14--AND CT.ChangeAction = 1
                //WHERE
                // (@FromDate IS NULL OR DO.DG_Orders_Date >= @FromDate)
                // AND(@ToDate  IS NULL OR  DO.DG_Orders_Date <= @ToDate)

                // AND(@paramQrCode  IS NULL OR  DOD.DG_Order_ImageUniqueIdentifier = @paramQrCode)
                // AND PType.DG_Orders_ProductType_pkey = 84
                // AND CT.ChangeTrackingId in (Select MAX(ChangeTrackingId) FROM dbo.ChangeTracking C  WITH(NOLOCK)
                // WHERE C.ObjectValueId = DO.DG_Orders_pkey AND C.ApplicationObjectId = 14) 
            }



            return qrCodeList;
        }
        #endregion
        public static List<ChangeInfo> GetNextChangeInfo(int syncDataDelay)
        {
            long changeTrackingId = 0;
            try
            {
                using (DigiPhotoContext dbContext = new DigiPhotoContext())
                {

                    List<ChangeInfo> changeList = new List<ChangeInfo>();
                    DateTime dateDelay = System.DateTime.Now;
                    DateTime getDateNow = System.DateTime.Now;//.AddSeconds(-syncDataDelay);
                    var detDateTimeNow = dbContext.Database.SqlQuery<GetDateNow>("Select GETDATE() AS GetDateTimeNow").ToList<GetDateNow>(); //dbContext.GetDateNows(1).Select(x => x.GetDateTimeNow);
                    foreach (var item in detDateTimeNow)
                    {
                        getDateNow = item.GetDateTimeNow;
                    }
                    dateDelay = getDateNow.AddSeconds(-syncDataDelay);


                    /*Process the sync which are in error state  */
                    DateTime getDate = getDateNow.AddMinutes(-180);
                    string ResyncTime = dbContext.iMIXConfigurationValues.Where(c => c.IMIXConfigurationMasterId == 117).Select(S => S.ConfigurationValue).FirstOrDefault();
                    if (!string.IsNullOrEmpty(ResyncTime))
                        getDate = getDateNow.AddMinutes(-Convert.ToInt32(ResyncTime));


                    List<ChangeTracking> lstchangeTracking = (
                     from c in dbContext.ChangeTrackings
                     join AP in dbContext.ApplicationObjects on c.ApplicationObjectId equals AP.ApplicationObjectId
                     where c.ApplicationObjectId != 16 && c.RetryCount < 5
                     && c.SyncStatus < 0 || c.SyncStatus == 2 || (c.SyncStatus > 5 && c.SyncStatus < 11)
                     //&& c.SyncStatus < 0
                     //&& c.SyncDate >= getDate
                     orderby AP.SyncPriority ascending
                     select c).Take(100).ToList();

                    List<ChangeTracking> cloudinarychangeTracking = new List<ChangeTracking>();
                    if (lstchangeTracking != null)
                    {
                        foreach (var updateChange in lstchangeTracking)
                        {
                            cloudinarychangeTracking = (
                                                        from ct in dbContext.ChangeTrackings
                                                        join cd in dbContext.CloudinaryDtls
                                                        on new { ID = ct.ObjectValueId, CODE = ct.EntityCode } equals new { ID = cd.CloudinaryInfoID, CODE = cd.SyncCode }
                                                        join ctt in dbContext.ChangeTrackings
                                                        on cd.OrderId equals ctt.ObjectValueId
                                                        where ctt.ChangeTrackingId == updateChange.ChangeTrackingId
                                                        && ctt.ApplicationObjectId == 14 && ct.ApplicationObjectId == 18
                                                        && ct.SyncStatus != 0
                                                        select ct).ToList();

                            if (cloudinarychangeTracking != null)
                            {
                                foreach (var updatecloudinaryChange in cloudinarychangeTracking)
                                {
                                    updatecloudinaryChange.SyncStatus = 0;
                                }
                            }
                            updateChange.SyncStatus = 0;
                            updateChange.RetryCount = updateChange.RetryCount + 1;
                        }
                        dbContext.SaveChanges();
                    }

                    //Get non Activity ChangeTracking with SyncPriority more than 0.
                    var changeTrackingList = (
                                           from c in dbContext.ChangeTrackings
                                           join AP in dbContext.ApplicationObjects on c.ApplicationObjectId equals AP.ApplicationObjectId
                                           where c.SyncStatus == (int)SyncStatus.NotSynced && c.ChangeDate < dateDelay &&
                                           c.ApplicationObjectId != 16 && c.SyncPriority > 0
                                           orderby AP.SyncPriority, c.ChangeTrackingId, c.ChangeDate
                                           select c
                                         ).Take(100);

                    //Get non Activity ChangeTracking.
                    if (changeTrackingList != null && changeTrackingList.Count() == 0)
                    {
                        changeTrackingList = (
                                               from c in dbContext.ChangeTrackings
                                               join AP in dbContext.ApplicationObjects on c.ApplicationObjectId equals AP.ApplicationObjectId
                                               where c.SyncStatus == (int)SyncStatus.NotSynced && c.ChangeDate < dateDelay
                                               && c.ApplicationObjectId != 16
                                               orderby AP.SyncPriority, c.ChangeTrackingId, c.ChangeDate
                                               select c
                                             ).Take(100);

                        //Get non Activity failed ChangeTracking  as per setting of Resync failed configuration 
                        if (changeTrackingList != null && changeTrackingList.Count() == 0)
                        {
                            if (IsResyncActive)
                            {
                                ResyncHistory resyncHistory = (
                                                      from r in dbContext.ResyncHistorys
                                                      where r.ResyncStatus == 0
                                                      select r
                                                      ).FirstOrDefault();
                                if (resyncHistory != null && resyncHistory.ResyncDatetime != null)
                                {
                                    scheduleSyncTime = resyncHistory.ResyncDatetime;
                                    if (scheduleSyncTime <= System.DateTime.Now)
                                    {
                                        changeTrackingList = (
                                                             from c in dbContext.ChangeTrackings
                                                             join AP in dbContext.ApplicationObjects on c.ApplicationObjectId equals AP.ApplicationObjectId
                                                             where c.SyncStatus < 0
                                                                    && c.ChangeDate < dateDelay
                                                                    && c.ApplicationObjectId != 16
                                                                    && c.SyncDate >= InitialResyncDateTime
                                                                    && c.SyncRetryCount <= ResyncMaxRetryCount
                                                             orderby AP.SyncPriority, c.ChangeTrackingId, c.ChangeDate
                                                             select c
                                                           ).Take(100);

                                        resyncHistory.ResyncStatus = 1;

                                        ResyncHistory resyncHistoryAdd = new ResyncHistory();
                                        if (ResyncIntervalType == "0")
                                            resyncHistoryAdd.ResyncDatetime = scheduleSyncTime.AddHours(Convert.ToDouble(ResyncIntervalCount));
                                        else
                                            resyncHistoryAdd.ResyncDatetime = scheduleSyncTime.AddDays(Convert.ToDouble(ResyncIntervalCount));
                                        resyncHistoryAdd.ResyncStatus = 0;
                                        resyncHistoryAdd.ResyncType = 2;
                                        dbContext.ResyncHistorys.Add(resyncHistoryAdd);
                                        dbContext.SaveChanges();
                                    }
                                }
                            }
                            //No more change tracking found, try for Activity Change Tracking
                            if (changeTrackingList != null && changeTrackingList.Count() == 0)
                            {
                                changeTrackingList = (
                                                   from c in dbContext.ChangeTrackings
                                                   join AP in dbContext.ApplicationObjects on c.ApplicationObjectId equals AP.ApplicationObjectId
                                                   where c.SyncStatus == (int)SyncStatus.NotSynced && c.ChangeDate < dateDelay
                                                   orderby AP.SyncPriority, c.ChangeTrackingId, c.ChangeDate
                                                   select c
                                                 ).Take(100);
                            }
                        }
                    }
                    ChangeInfo change = null;
                    if (changeTrackingList != null && changeTrackingList.Count() > 0)
                    {
                        foreach (ChangeTracking changeTracking in changeTrackingList)
                        {
                            if (changeTracking.ApplicationObjectId == 14)
                                SyncController.UpdateSyncStatus(changeTracking.ChangeTrackingId, (int)SyncStatus.Inprogress);

                            changeTrackingId = changeTracking.ChangeTrackingId;
                            List<UploadInfo> uploadList;
                            UploadFile uploadFile;
                            string dataXML;
                            GetChangedDataXML(changeTracking.ApplicationObjectId, changeTracking.ObjectValueId, out dataXML, out uploadList, out uploadFile);
                            change = new ChangeInfo
                            {
                                ApplicationObjectId = changeTracking.ApplicationObjectId,
                                ObjectValueId = changeTracking.ObjectValueId,
                                ChangeTrackingId = changeTracking.ChangeTrackingId,
                                ChangeDate = changeTracking.ChangeDate,
                                dataXML = dataXML,
                                ChangeAction = changeTracking.ChangeAction,
                                ChangeBy = changeTracking.ChangeBy,
                                EntityCode = changeTracking.EntityCode,
                                UploadList = uploadList,
                                UploadFile = uploadFile
                            };
                            changeList.Add(change);
                        }
                    }
                    return changeList;
                }
            }
            catch (Exception ex)
            {
                SyncController.UpdateSyncStatus(changeTrackingId, (int)SyncStatus.Error);
                //Added by Anand, this will generate record in ChangeTrackingExceptionDtl for this ChangeTrackingID.
                //SyncController.AddChangeTrackingExceptionDtl(changeTrackingId, ex);
                log.Error("SyncController:GetNextChangeInfo:: Message: " + ex.Message + " StackTrace: " + ex.StackTrace + " InnerException: " + ex.InnerException + " Source: " + ex.Source);
                throw ex;
            }
        }

        private static void GetChangedDataXML(long applicationObjectId, long objectValueId, out string dataXML, out List<UploadInfo> uploadList, out UploadFile uploadFile)
        {
            try
            {
                uploadList = null;
                uploadFile = null;
                Serializer serializer = SerializationFactory.Create(applicationObjectId);
                dataXML = serializer.Serialize(objectValueId);
                if (serializer is IUploadable)   //If This serializer has Download
                {
                    IUploadable uploadable = (IUploadable)serializer;
                    uploadList = uploadable.UploadList;
                    uploadFile = uploadable.UploadFile;
                }
            }
            catch (Exception ex)
            {
                log.Error("SyncController:GetChangedDataXML:: Message: " + ex.Message + " StackTrace: " + ex.StackTrace + " InnerException: " + ex.InnerException + " Source: " + ex.Source);

                throw ex;
            }
        }

        //public static DateTime GetReSyncDateTime(DateTime NextRetryDateTime, Int32 SyncRetryCount)
        //{
        //    DateTime reSyncDateTime = DateTime.Now;
        //    if (SyncRetryCount == 0)//First time
        //    {
        //        if (InitialResyncDateTime >= DateTime.Now)
        //        {
        //            reSyncDateTime = InitialResyncDateTime;
        //        }
        //        else
        //        {
        //            if (ResyncIntervalType == "0")
        //            {

        //                reSyncDateTime = InitialResyncDateTime;
        //            }
        //            else
        //            {

        //            }
        //        }
        //    }
        //    else //First time onwards
        //    {
        //        if (ResyncIntervalType == "0")
        //        {
        //            reSyncDateTime = NextRetryDateTime.AddHours(Convert.ToDouble(ResyncIntervalCount));
        //        }
        //        else
        //        {
        //            reSyncDateTime = NextRetryDateTime.AddDays(Convert.ToDouble(ResyncIntervalCount));
        //        }
        //    }
        //    return reSyncDateTime;
        //}
        public static void UpdateSyncStatus(long changeTrackingId, int syncStatus)
        {
            try
            {
                using (DigiPhotoContext dbContext = new DigiPhotoContext())
                {
                    if (changeTrackingId != 0)
                    {
                        var changeTracking = dbContext.ChangeTrackings.Where(c => c.ChangeTrackingId == changeTrackingId).FirstOrDefault();
                        changeTracking.SyncStatus = syncStatus;
                        changeTracking.SyncDate = DateTime.Now;
                        //start update ResyncDateTime and ReSyncCount

                        if (syncStatus < 0)
                        {
                            if (IsResyncActive)
                            {
                                if (string.IsNullOrEmpty(changeTracking.SyncRetryCount.ToString()))
                                    changeTracking.SyncRetryCount = 0;
                                else
                                    changeTracking.SyncRetryCount += 1;
                                //changeTracking.NextRetryDateTime = GetReSyncDateTime(changeTracking.NextRetryDateTime, changeTracking.SyncRetryCount);
                            }
                        }
                        //else
                        //{
                        //    changeTracking.FailedResyncRetryDateTime = DateTime.Now;
                        //    changeTracking.FailedResyncRetryCount = 0;
                        //}
                        //end 
                        if (syncStatus != (Convert.ToInt32(IsOnline)))
                        {
                            var StoreDetails = dbContext.DG_Store.FirstOrDefault();
                            StoreDetails.IsOnLine = (syncStatus == -1 || syncStatus == -2) ? false : true;
                            dbContext.SaveChanges();
                            IsOnline = StoreDetails.IsOnLine;
                        }
                    }
                    else
                    {
                        var StoreDetails = dbContext.DG_Store.FirstOrDefault();
                        StoreDetails.IsOnLine = false;
                        dbContext.SaveChanges();
                        IsOnline = StoreDetails.IsOnLine;
                    }
                    dbContext.SaveChanges();
                }
            }
            catch (DbEntityValidationException e)
            {
                foreach (var eve in e.EntityValidationErrors)
                {
                    Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                        eve.Entry.Entity.GetType().Name, eve.Entry.State);
                    foreach (var ve in eve.ValidationErrors)
                    {
                        Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                            ve.PropertyName, ve.ErrorMessage);
                    }
                }
                throw;
            }
            catch (Exception ex)
            {
                log.Error("SyncController:UpdateSyncStatus:: Message: " + ex.Message + " StackTrace: " + ex.StackTrace + " InnerException: " + ex.InnerException + " Source: " + ex.Source);
                throw ex;
            }
        }
        public static bool IsSynceEnabled()
        {
            bool IsSyncedEnabled = false;
            try
            {

                using (DigiPhotoContext dbContext = new DigiPhotoContext())
                {
                    var configValue = dbContext.iMIXConfigurationValues.Where(c => c.IMIXConfigurationMasterId == (int)iMixConfigurationMasterId.IsSyncedEnabled).FirstOrDefault();
                    if (configValue != null)
                    {
                        IsSyncedEnabled = Convert.ToBoolean(configValue.ConfigurationValue);
                    }
                }
                return IsSyncedEnabled;
            }
            catch (Exception ex)
            {
                log.Error("SyncController:IsSyncedEnabled:: Message: " + ex.Message + " StackTrace: " + ex.StackTrace + " InnerException: " + ex.InnerException + " Source: " + ex.Source);

                throw ex;
            }

        }
        public static int SyncServiceInterval()
        {
            int SyncServiceInterval = 5;
            try
            {

                using (DigiPhotoContext dbContext = new DigiPhotoContext())
                {
                    var configValue = dbContext.iMIXConfigurationValues.Where(c => c.IMIXConfigurationMasterId == (int)iMixConfigurationMasterId.SyncServiceInterval).FirstOrDefault();
                    if (configValue != null)
                    {
                        SyncServiceInterval = Convert.ToInt32(configValue.ConfigurationValue);
                        if (SyncServiceInterval <= 0)
                            SyncServiceInterval = 5; //ByDefault 10 seconds
                    }
                }
                return SyncServiceInterval;
            }
            catch (Exception ex)
            {
                log.Error("SyncController:IsSyncedEnabled:: Message: " + ex.Message + " StackTrace: " + ex.StackTrace + " InnerException: " + ex.InnerException + " Source: " + ex.Source);

                throw ex;
            }

        }
        public static int SyncDataDelay()
        {
            int SyncDataDelay = 1;
            try
            {
                using (DigiPhotoContext dbContext = new DigiPhotoContext())
                {
                    var configValue = dbContext.iMIXConfigurationValues.Where(c => c.IMIXConfigurationMasterId == (int)iMixConfigurationMasterId.SyncDataDelay).FirstOrDefault();
                    if (configValue != null)
                    {
                        SyncDataDelay = Convert.ToInt32(configValue.ConfigurationValue);
                        if (SyncDataDelay <= 0)
                            SyncDataDelay = 1;

                    }
                }

                return SyncDataDelay;
            }
            catch (Exception ex)
            {
                log.Error("SyncController:IsSyncedEnabled:: Message: " + ex.Message + " StackTrace: " + ex.StackTrace + " InnerException: " + ex.InnerException + " Source: " + ex.Source);

                throw ex;
            }
        }
        //static List<long> PartialEditedImageList;
        //private static void PartialEditedImage()
        //{
        //    PartialEditedImageList = new List<long>();
        //    PartialEditedImageList.Add(Convert.ToInt64(iMixConfigurationMasterId.IsEnablePartialEditedImage));
        //    PartialEditedImageList.Add(Convert.ToInt64(iMixConfigurationMasterId.SyncImageType));
        //}
        public static List<iMIXconfigurationLocationValue> PartialEditedEnabled()
        {
            //bool partialEditedEnabled = false;
            try
            {
                List<long> PartialEditedImageList = new List<long>();
                PartialEditedImageList.Add(Convert.ToInt64(iMixConfigurationMasterId.IsEnablePartialEditedImage));
                PartialEditedImageList.Add(Convert.ToInt64(iMixConfigurationMasterId.SyncImageType));
                List<iMIXconfigurationLocationValue> configValueResult = new List<iMIXconfigurationLocationValue>();
                using (DigiPhotoContext dbContext = new DigiPhotoContext())
                {
                    var configDetails = dbContext.usp_getConfigurationLocationValue().Where(c => PartialEditedImageList.Contains(c.IMIXConfigurationMasterId)).ToList();

                    var config = configDetails.Where(c => c.IMIXConfigurationMasterId == (int)iMixConfigurationMasterId.IsEnablePartialEditedImage
                         && c.ConfigurationValue.ToUpper() == "TRUE").ToList();

                    var configValue = from S1 in configDetails
                                      join S2 in config
                                      on new { S1.LocationId, S1.SubstoreId } equals new { S2.LocationId, S2.SubstoreId }
                                      where S1.ConfigurationValue == "2"
                                      select new
                                      {
                                          S1.ConfigurationValue,
                                          S1.IMIXConfigurationMasterId,
                                          S1.LocationId,
                                          S1.SubstoreId
                                      };
                    //var configValue = dbContext.iMIXConfigurationValues.Where(c => c.IMIXConfigurationMasterId == (int)iMixConfigurationMasterId.IsEnablePartialEditedImage).FirstOrDefault();
                    //if (configValue != null)
                    //{
                    //    partialEditedEnabled = Convert.ToBoolean(configValue.ConfigurationValue);
                    //}
                    if (configValue != null)
                    {
                        foreach (var item in configValue)
                        {
                            iMIXconfigurationLocationValue configval = new iMIXconfigurationLocationValue();
                            configval.ConfigurationValue = item.ConfigurationValue;
                            configval.IMIXConfigurationMasterId = item.IMIXConfigurationMasterId;
                            configval.LocationId = item.LocationId;
                            configval.SubstoreId = item.SubstoreId;
                            configValueResult.Add(configval);
                        }
                    }
                }
                return configValueResult;
            }
            catch (Exception ex)
            {
                log.Error("SyncController:PartialEditedEnabled:: Message: " + ex.Message + " StackTrace: " + ex.StackTrace + " InnerException: " + ex.InnerException + " Source: " + ex.Source);

                throw ex;
            }
        }

        public static string SyncServiceURL()
        {
            try
            {
                string SyncServiceURL = string.Empty;
                using (DigiPhotoContext dbContext = new DigiPhotoContext())
                {
                    var configValue = dbContext.iMIXConfigurationValues.Where(c => c.IMIXConfigurationMasterId == (int)iMixConfigurationMasterId.DgServiceURL).FirstOrDefault();
                    if (configValue != null)
                    {
                        SyncServiceURL = configValue.ConfigurationValue;
                    }
                }
                return SyncServiceURL;
            }
            catch (Exception ex)
            {
                log.Error("SyncController:IsSyncedEnabled:: Message: " + ex.Message + " StackTrace: " + ex.StackTrace + " InnerException: " + ex.InnerException + " Source: " + ex.Source);

                throw ex;
            }

        }

        public static string HotFolderPath(int SubStoreId)
        {
            if (string.IsNullOrEmpty(hotfolderPath))
            {
                using (DigiPhotoContext dbContext = new DigiPhotoContext())
                {
                    var configValue = dbContext.DG_Configuration.Where(c => c.DG_Substore_Id == SubStoreId).FirstOrDefault();
                    if (configValue != null)
                    {
                        hotfolderPath = configValue.DG_Hot_Folder_Path;
                    }
                }
            }
            return hotfolderPath;
        }

        public static void InsertCloudinaryUploadDetails(CloudinaryDtl objCloudinaryDtl)
        {
            try
            {
                using (DigiPhotoContext dbContext = new DigiPhotoContext())
                {
                    dbContext.CloudinaryDtls.Add(objCloudinaryDtl);
                    dbContext.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                log.Error("SyncController:UpdateSyncStatus:: Message: " + ex.Message + " StackTrace: " + ex.StackTrace + " InnerException: " + ex.InnerException + " Source: " + ex.Source);

                throw ex;
            }
        }

        public static CloudinaryDtl GetCloudinaryDetails(UploadInfo upload)
        {
            try
            {
                using (DigiPhotoContext dbContext = new DigiPhotoContext())
                {
                    CloudinaryDtl GetCloudinaryDetails = dbContext.CloudinaryDtls.Where(c => c.PhotoID == upload.PhotoId
                        && c.OrderId == upload.OrderId && c.IdentificationCode == upload.IdentificationCode).FirstOrDefault();
                    return GetCloudinaryDetails;
                }
            }
            catch (Exception ex)
            {
                log.Error("SyncController:GetCloudinaryDetails:: Message: " + ex.Message + " StackTrace: " + ex.StackTrace + " InnerException: " + ex.InnerException + " Source: " + ex.Source);

                throw ex;
            }
        }

        public static void UpdateCloudinaryUploadDetails(CloudinaryDtl objCloudinaryDtl)
        {
            try
            {
                using (DigiPhotoContext dbContext = new DigiPhotoContext())
                {
                    CloudinaryDtl CloudinaryDtl = dbContext.CloudinaryDtls.Find(objCloudinaryDtl.CloudinaryInfoID);
                    CloudinaryDtl.CloudinaryStatusID = objCloudinaryDtl.CloudinaryStatusID;
                    CloudinaryDtl.ErrorMessage = objCloudinaryDtl.ErrorMessage;
                    CloudinaryDtl.CloudinaryPublicID = objCloudinaryDtl.CloudinaryPublicID;
                    CloudinaryDtl.RetryCount = objCloudinaryDtl.RetryCount;
                    CloudinaryDtl.ModifiedDateTime = objCloudinaryDtl.ModifiedDateTime;
                    CloudinaryDtl.Height = objCloudinaryDtl.Height;
                    CloudinaryDtl.Width = objCloudinaryDtl.Width;
                    CloudinaryDtl.ThumbnailDimension = objCloudinaryDtl.ThumbnailDimension;
                    dbContext.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                log.Error("SyncController:UpdateSyncStatus:: Message: " + ex.Message + " StackTrace: " + ex.StackTrace + " InnerException: " + ex.InnerException + " Source: " + ex.Source);

                throw ex;
            }
        }


        /// <summary>
        /// SetSyncStatusZereo methods set sync status 0 when syncstatus is 2
        /// </summary>
        public static void SetSyncStatusZero()
        {
            try
            {
                using (DigiPhotoContext dbContext = new DigiPhotoContext())
                {
                    List<ChangeTracking> list = (from c in dbContext.ChangeTrackings
                                                 where c.ApplicationObjectId == 14 && c.SyncStatus == 2
                                                 select c).ToList();
                    if (list != null)
                    {
                        foreach (var updateChangetracking in list)
                        {
                            updateChangetracking.SyncStatus = 0;
                        }
                        dbContext.SaveChanges();
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("SyncController:UpdateSyncStatus from Inprogress to Pending when service starts :: Message: " + ex.Message + " StackTrace: " + ex.StackTrace + " InnerException: " + ex.InnerException + " Source: " + ex.Source);

                throw ex;
            }
        }

        public static void AddChangeTrackingExceptionDtl(long changeTrackingId, Exception exdb)
        {
            try
            {
                ChangeTrackingExceptionDtl exceptionDtl = new ChangeTrackingExceptionDtl()
                {
                    ChangeTrackingId = changeTrackingId,
                    ExceptionMsg = exdb.Message.ToString(),
                    ExceptionStackTrace = exdb.StackTrace.ToString(),
                    ExceptionType = exdb.GetType().Name.ToString(),
                    ExceptionSource = exdb.Source,
                    InnerException = exdb.InnerException != null ? exdb.InnerException.Message : "",
                    ExceptionOnDate = DateTime.Now,
                    SyncCode = "Exception" + changeTrackingId.ToString()
                };
                using (DigiPhotoContext dbContext = new DigiPhotoContext())
                {
                    dbContext.ChangeTrackingExceptionDtls.Add(exceptionDtl);
                    dbContext.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                log.Error("SyncController:AddChangeTrackingExceptionDtl:: Message: " + ex.Message + " StackTrace: " + ex.StackTrace + " InnerException: " + ex.InnerException + " Source: " + ex.Source);
                throw ex;
            }
        }

        public static void updateSiteSyncCode(string sitecode, string siteSyncCode)
        {
            try
            {
                using (DigiPhotoContext dbContext = new DigiPhotoContext())
                {
                    int substoreID = dbContext.DG_SubStores.Where(c => c.DG_SubStore_Code == sitecode).FirstOrDefault().DG_SubStore_pkey;
                    var substore = dbContext.DG_SubStores.Where(c => c.DG_SubStore_Code == sitecode).FirstOrDefault();
                    substore.DG_SubStore_pkey = substoreID;
                    substore.SyncCode = siteSyncCode;
                    dbContext.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                log.Error("SyncController:updateSiteSyncCode from Inprogress to Pending when service starts :: Message: " + ex.Message + " StackTrace: " + ex.StackTrace + " InnerException: " + ex.InnerException + " Source: " + ex.Source);

                throw ex;
            }
        }

        public static void FailedOrderConfigValue()
        {
            List<long> filterValues = new List<long>();
            filterValues.Add((long)iMixConfigurationMasterId.IsResyncActive);
            filterValues.Add((long)iMixConfigurationMasterId.InitialResyncDateTime);
            filterValues.Add((long)iMixConfigurationMasterId.ResyncIntervalCount);
            filterValues.Add((long)iMixConfigurationMasterId.ResyncIntervalType);
            filterValues.Add((long)iMixConfigurationMasterId.ResyncMaxRetryCount);
            try
            {
                using (DigiPhotoContext dbContext = new DigiPhotoContext())
                {
                    DateTime dateTime = DateTime.MinValue;
                    var ConfigValuesList = dbContext.iMIXConfigurationValues.Where(o => filterValues.Contains(o.IMIXConfigurationMasterId)).ToList();
                    if (ConfigValuesList != null || ConfigValuesList.Count > 0)
                    {
                        for (int i = 0; i < ConfigValuesList.Count; i++)
                        {
                            switch (ConfigValuesList[i].IMIXConfigurationMasterId)
                            {
                                case (int)iMixConfigurationMasterId.IsResyncActive:
                                    IsResyncActive = Convert.ToBoolean(ConfigValuesList[i].ConfigurationValue);
                                    break;
                                case (int)iMixConfigurationMasterId.InitialResyncDateTime:
                                    DateTime.TryParse(ConfigValuesList[i].ConfigurationValue, out dateTime);
                                    if (dateTime != DateTime.MinValue)
                                        InitialResyncDateTime = dateTime;
                                    break;
                                case (int)iMixConfigurationMasterId.ResyncIntervalCount:
                                    ResyncIntervalCount = Convert.ToInt32(ConfigValuesList[i].ConfigurationValue);
                                    break;
                                case (int)iMixConfigurationMasterId.ResyncIntervalType:
                                    ResyncIntervalType = Convert.ToString(ConfigValuesList[i].ConfigurationValue);
                                    break;
                                case (int)iMixConfigurationMasterId.ResyncMaxRetryCount:
                                    ResyncMaxRetryCount = Convert.ToInt32(ConfigValuesList[i].ConfigurationValue);
                                    break;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("SyncController:FailedOrderConfigValue:: Message: " + ex.Message + " StackTrace: " + ex.StackTrace + " InnerException: " + ex.InnerException + " Source: " + ex.Source);
                throw ex;
            }
        }

        //public static bool IsFailedOrderResyncActive()
        //{
        //    // bool IsFailedOrderResyncActive = false;
        //    try
        //    {
        //        using (DigiPhotoContext dbContext = new DigiPhotoContext())
        //        {
        //            var configValue = dbContext.iMIXConfigurationValues.Where(c => c.IMIXConfigurationMasterId == (int)iMixConfigurationMasterId.IsFailedOrderResyncActive).FirstOrDefault();
        //            if (configValue != null)
        //            {
        //                IsFailedOrderResyncActive1 = Convert.ToBoolean(configValue.ConfigurationValue);
        //            }
        //        }
        //        return IsFailedOrderResyncActive1;
        //    }
        //    catch (Exception ex)
        //    {
        //        log.Error("SyncController:IsFailedOrderResyncActive:: Message: " + ex.Message + " StackTrace: " + ex.StackTrace + " InnerException: " + ex.InnerException + " Source: " + ex.Source);

        //        throw ex;
        //    }
        //}
        //public static DateTime FailedOrderResyncDateTime()
        //{
        //    DateTime IsFailedOrderResyncActive = DateTime.Now;
        //    try
        //    {
        //        using (DigiPhotoContext dbContext = new DigiPhotoContext())
        //        {
        //            var configValue = dbContext.iMIXConfigurationValues.Where(c => c.IMIXConfigurationMasterId == (int)iMixConfigurationMasterId.IsFailedOrderResyncActive).FirstOrDefault();
        //            if (configValue != null)
        //            {
        //                IsFailedOrderResyncActive = Convert.ToDateTime(configValue.ConfigurationValue);
        //            }
        //        }
        //        return IsFailedOrderResyncActive;
        //    }
        //    catch (Exception ex)
        //    {
        //        log.Error("SyncController:FailedOrderResyncDateTime:: Message: " + ex.Message + " StackTrace: " + ex.StackTrace + " InnerException: " + ex.InnerException + " Source: " + ex.Source);

        //        throw ex;
        //    }

        //}
        //public static Int32 FailedOrderIntervalCount()
        //{
        //    Int32 FailedOrderIntervalCount = 0;
        //    try
        //    {
        //        using (DigiPhotoContext dbContext = new DigiPhotoContext())
        //        {
        //            var configValue = dbContext.iMIXConfigurationValues.Where(c => c.IMIXConfigurationMasterId == (int)iMixConfigurationMasterId.FailedOrderIntervalCount).FirstOrDefault();
        //            if (configValue != null)
        //            {
        //                FailedOrderIntervalCount = Convert.ToInt32(configValue.ConfigurationValue);
        //            }
        //        }
        //        return FailedOrderIntervalCount;
        //    }
        //    catch (Exception ex)
        //    {
        //        log.Error("SyncController:FailedOrderIntervalCount:: Message: " + ex.Message + " StackTrace: " + ex.StackTrace + " InnerException: " + ex.InnerException + " Source: " + ex.Source);

        //        throw ex;
        //    }

        //}
        //public static string FailedOrderIntervalType()
        //{
        //    string FailedOrderIntervalType = string.Empty;
        //    try
        //    {
        //        using (DigiPhotoContext dbContext = new DigiPhotoContext())
        //        {
        //            var configValue = dbContext.iMIXConfigurationValues.Where(c => c.IMIXConfigurationMasterId == (int)iMixConfigurationMasterId.FailedOrderIntervalType).FirstOrDefault();
        //            if (configValue != null)
        //            {
        //                FailedOrderIntervalType = Convert.ToString(configValue.ConfigurationValue);
        //            }
        //        }
        //        return FailedOrderIntervalType;
        //    }
        //    catch (Exception ex)
        //    {
        //        log.Error("SyncController:FailedOrderIntervalType:: Message: " + ex.Message + " StackTrace: " + ex.StackTrace + " InnerException: " + ex.InnerException + " Source: " + ex.Source);
        //        throw ex;
        //    }

        //}
        //public static Int32 FailedOrderMaxRetryCount()
        //{
        //    Int32 FailedOrderMaxRetryCount = 0;
        //    try
        //    {
        //        using (DigiPhotoContext dbContext = new DigiPhotoContext())
        //        {
        //            var configValue = dbContext.iMIXConfigurationValues.Where(c => c.IMIXConfigurationMasterId == (int)iMixConfigurationMasterId.FailedOrderMaxRetryCount).FirstOrDefault();
        //            if (configValue != null)
        //            {
        //                FailedOrderMaxRetryCount = Convert.ToInt32(configValue.ConfigurationValue);
        //            }
        //        }
        //        return FailedOrderMaxRetryCount;
        //    }
        //    catch (Exception ex)
        //    {
        //        log.Error("SyncController:FailedOrderMaxRetryCount:: Message: " + ex.Message + " StackTrace: " + ex.StackTrace + " InnerException: " + ex.InnerException + " Source: " + ex.Source);
        //        throw ex;
        //    }

        //}

        public static bool IsNewArchitectureEnabled()
        {
            bool IsNewArchitectureEnabled = false;
            try
            {

                using (DigiPhotoContext dbContext = new DigiPhotoContext())
                {
                    var configValue = dbContext.iMIXConfigurationValues.Where(c => c.IMIXConfigurationMasterId == (int)iMixConfigurationMasterId.IsNewArchitectureEnable).FirstOrDefault();
                    if (configValue != null)
                    {
                        IsNewArchitectureEnabled = Convert.ToBoolean(configValue.ConfigurationValue);
                    }
                }
                return IsNewArchitectureEnabled;
            }
            catch (Exception ex)
            {
                log.Error("SyncController:IsSyncedEnabled:: Message: " + ex.Message + " StackTrace: " + ex.StackTrace + " InnerException: " + ex.InnerException + " Source: " + ex.Source);

                throw ex;
            }


        }

    }
}
