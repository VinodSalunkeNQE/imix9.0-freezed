﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ServiceModel;
using System.Configuration;
using log4net;
using DigiPhoto.DigiSync.ClientDataProcessor;
using System.Security.Cryptography.X509Certificates;
using System.IO;

namespace DigiPhoto.DigiSync.ClientDataProcessor.Controller
{
    public class DServiceProxy<T>
    {
        public static ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        public static void Use(Action<T> action)
        {
            //BasicHttpBinding binding = new BasicHttpBinding();
            BasicHttpsBinding binding = new BasicHttpsBinding();
            binding.Security.Mode = BasicHttpsSecurityMode.Transport;
            binding.Security.Transport.ClientCredentialType = HttpClientCredentialType.Certificate;
            string apiLocation = SyncController.SyncServiceURL();
            if (string.IsNullOrEmpty(apiLocation))
            {
                apiLocation = ConfigurationManager.AppSettings["API_URL"].ToString();
            }


            if (typeof(T).Name.StartsWith("I"))
            {
                apiLocation = apiLocation + "/" + typeof(T).Name.Substring(1) + ".svc";
            }
            else
            {
                apiLocation = apiLocation + "/" + typeof(T).Name + ".svc";
            }
            var certificate = new X509Certificate2(Path.Combine(@"C:\Program Files (x86)\iMix\SSL_Certificate", "__digiphotoglobal_cn.pfx"), "digi12345");
            EndpointAddress ep = new EndpointAddress(apiLocation);
            string dataSize = string.Empty;
            if (ConfigurationManager.AppSettings["ServiceDataSize"] == null)
            {
                dataSize = Convert.ToString(1024 * 65535);
            }
            else
            {
                dataSize = ConfigurationManager.AppSettings["ServiceDataSize"];
            }
            binding.MaxReceivedMessageSize = Convert.ToInt64(dataSize);
            binding.ReaderQuotas.MaxArrayLength = Convert.ToInt32(dataSize);
            binding.ReaderQuotas.MaxStringContentLength = Convert.ToInt32(dataSize);
            binding.MaxBufferSize = Convert.ToInt32(dataSize);

            binding.OpenTimeout = new TimeSpan(0, 0, 60*2);
            binding.CloseTimeout = new TimeSpan(0, 0, 60*2);
            binding.SendTimeout = new TimeSpan(0, 0, 60*2);   //most important
            binding.ReceiveTimeout = new TimeSpan(0, 0, 60*2);

            ChannelFactory<T> factory = new ChannelFactory<T>(binding, ep);
            T client = factory.CreateChannel();

            bool sucess = false;

            try
            {
                action(client);
                ((IClientChannel)client).Close();
                factory.Close();
                sucess = true;
            }
            catch (CommunicationException cex)
            {
                log.Error(cex);
                throw cex;
            }
            catch (TimeoutException tex)
            {

                log.Error(tex);
                throw tex;
            }
            catch (Exception ex)
            {
                log.Error(ex);
                throw ex;
            }
            finally
            {
                if (!sucess)
                {
                    //Abort the Channel if it didn't close sucessfully
                    ((IClientChannel)client).Abort();
                    factory.Abort();
                }
            }
        }
    }
}
