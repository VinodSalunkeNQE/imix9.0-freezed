﻿using DigiPhoto.DigiSync.Model;
using DigiPhoto.DigiSync.Utility;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DigiPhoto.DigiSync.ServiceLibrary.Interface;
using log4net;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows;
using System.Threading;

namespace DigiPhoto.DigiSync.ClientDataProcessor.Controller
{
    public class DownloadManager
    {
        private static ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        public static void Downloadfile(string filePath, long applicationObjectId, SiteInfo site)
        {
            RemoteFileInfo fileInfo = new RemoteFileInfo();
            Stream outputStream = null;
            try
            {
                DownloadRequest requestData = new DownloadRequest();
                requestData.FilePath = filePath;
                //FileController fileController = new FileController();
                //fileInfo = fileController.DownloadFile(requestData);
                DServiceProxy<IFileService>.Use(client =>
                {
                    fileInfo = client.DownloadFile(requestData);
                });

                //TBD
                string entityImagesPath = GetEntityImagesPath(applicationObjectId, site);
                string saveFilePath = Path.Combine(entityImagesPath, Path.GetFileName(filePath));
                Thread.Sleep(3000);
                outputStream = new FileInfo(saveFilePath).OpenWrite();
                byte[] buffer = new byte[6500];
                int bytesRead = 0;

                bytesRead = fileInfo.FileByteStream.Read(buffer, 0, buffer.Length);

                while (bytesRead > 0)
                {
                    outputStream.Write(buffer, 0, bytesRead);
                    bytesRead = fileInfo.FileByteStream.Read(buffer, 0, 6500);
                }
                if (fileInfo.FileByteStream != null)
                {
                    fileInfo.FileByteStream.Close();
                }
                if (outputStream != null)
                {
                    outputStream.Close();
                }
                //Create Thumbnails
                if (applicationObjectId != (int)ApplicationObjectEnum.Graphic)
                {
                    string thumbnailFileName = Path.GetFileNameWithoutExtension(filePath) + ".png";
                    using (FileStream fileStream = File.OpenRead(saveFilePath))
                    {
                        MemoryStream ms = new MemoryStream();
                        fileStream.CopyTo(ms);
                        ms.Seek(0, SeekOrigin.Begin);
                        fileStream.Close();
                        ResizeAndSaveHighQualityImage(System.Drawing.Image.FromStream(ms), entityImagesPath + "\\Thumbnails\\" + thumbnailFileName, 100, 150);
                    }
                }
                //create background for multiple product type
                if (applicationObjectId == (int)ApplicationObjectEnum.Background)
                {
                    if (Directory.Exists(System.IO.Path.Combine(entityImagesPath, "8x10")))
                    {
                        //DownloadManager.Instance.BackgroundImageOtherFormats(saveFilePath, entityImagesPath);
                        BackgroundImageOtherFormats(saveFilePath, entityImagesPath);
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("Downloadfile:: Message: " + ex.Message + " StackTrace: " + ex.StackTrace + " InnerException: " + ex.InnerException + " Source: " + ex.Source);
                throw new Exception(ex.ToString());
            }
            finally
            {
                if (fileInfo.FileByteStream != null)
                {
                    fileInfo.FileByteStream.Close();
                }
                if (outputStream != null)
                {
                    outputStream.Close();
                }
            }
        }

        private static string GetEntityImagesPath(long applicationObjectId, SiteInfo site)
        {
            string hotFolderPath = site.HotFolderPath;
            if (string.IsNullOrEmpty(site.HotFolderPath))
            {
                hotFolderPath = ConfigurationManager.AppSettings["DigiHotFolderPath"];
            }
            string entityImagePath = string.Empty;
            switch (applicationObjectId)
            {
                case (int)ApplicationObjectEnum.Border:
                    entityImagePath = Path.Combine(hotFolderPath, "Frames");
                    break;
                case (int)ApplicationObjectEnum.Background:
                    entityImagePath = Path.Combine(hotFolderPath, "BG");
                    break;
                case (int)ApplicationObjectEnum.Graphic:
                    entityImagePath = Path.Combine(hotFolderPath, "Graphics");
                    break;
            }
            return entityImagePath;
        }
        private static void ResizeAndSaveHighQualityImage(System.Drawing.Image image, string pathToSave, int quality, int height)
        {
            try
            {
                // the resized result bitmap
                decimal ratio = Convert.ToDecimal(image.Width) / Convert.ToDecimal(image.Height);
                int width = Convert.ToInt32(height * ratio);

                using (System.Drawing.Bitmap result = new System.Drawing.Bitmap(width, height))
                {
                    // get the graphics and draw the passed image to the result bitmap
                    using (System.Drawing.Graphics grphs = System.Drawing.Graphics.FromImage(result))
                    {
                        grphs.CompositingQuality = System.Drawing.Drawing2D.CompositingQuality.HighQuality;
                        grphs.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
                        grphs.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
                        grphs.DrawImage(image, 0, 0, result.Width, result.Height);
                    }
                    // check the quality passed in
                    if ((quality < 0) || (quality > 100))
                    {
                        string error = string.Format("quality must be 0, 100", quality);
                        throw new ArgumentOutOfRangeException(error);
                    }

                    System.Drawing.Imaging.EncoderParameter qualityParam = new System.Drawing.Imaging.EncoderParameter(System.Drawing.Imaging.Encoder.Quality, quality);
                    string lookupKey = "image/png";
                    var jpegCodec = System.Drawing.Imaging.ImageCodecInfo.GetImageEncoders().Where(i => i.MimeType.Equals(lookupKey)).FirstOrDefault();

                    //create a collection of EncoderParameters and set the quality parameter
                    var encoderParams = new System.Drawing.Imaging.EncoderParameters(1);
                    encoderParams.Param[0] = qualityParam;
                    //save the image using the codec and the encoder parameter
                    result.Save(pathToSave, jpegCodec, encoderParams);
                }
            }
            catch (Exception ex)
            {
                log.Error("ResizeAndSaveHighQualityImage:: Message: " + ex.Message + " StackTrace: " + ex.StackTrace + " InnerException: " + ex.InnerException + " Source: " + ex.Source);


            }
        }

        #region BackGround
        static int originalWidth;
        static int originalHeight;
        static int ProductTypeId = 0;
        static string sourcepath = string.Empty;
        static string savepath = string.Empty;
        static string selectedFileName = string.Empty;
        static string filenamebg = string.Empty;
        static string saveFolderPath = string.Empty;
        static string backgoundname = string.Empty;
        private static void BackgroundImageOtherFormats(string selectedBGFileName, string DownloadFolderPath)
        {
            BitmapImage bitmap = new BitmapImage();
            bitmap.BeginInit();
            bitmap.UriSource = new Uri(selectedBGFileName);
            bitmap.EndInit();
            originalWidth = bitmap.PixelWidth;
            originalHeight = bitmap.PixelHeight;
            sourcepath = selectedBGFileName;
            //savepath = LoginUser.DigiFolderPath;
            int index = savepath.IndexOf("DigiImages");
            if (index != -1)
            {
                savepath = savepath.Remove(index - 1);
            }
            Convert6by8(DownloadFolderPath);
            Convert4by6(DownloadFolderPath);
            Convert5by7(DownloadFolderPath);
            Convert3by3(DownloadFolderPath);
            Convert8by10(DownloadFolderPath);

        }

        // 6*8 = 2400*1800
        private static void Convert6by8(string DownloadFolderPath)
        {
            ProductTypeId = 1;
            // saveFolderPath = savepath + "\\6x8";
            saveFolderPath = System.IO.Path.Combine(DownloadFolderPath, "6x8");
            if (Directory.Exists(saveFolderPath))
            {
                //Directory.CreateDirectory(saveFolderPath);
                ResizeWPFImage(sourcepath, saveFolderPath);
                saveFolderPath = null;
            }
        }

        // 4*6 = 1800*1200
        private static void Convert4by6(string DownloadFolderPath)
        {
            ProductTypeId = 2;
            // saveFolderPath = savepath + "\\4x6";
            saveFolderPath = System.IO.Path.Combine(DownloadFolderPath, "4x6");
            if (Directory.Exists(saveFolderPath))
            {
                ResizeWPFImage(sourcepath, saveFolderPath);
                saveFolderPath = null;
            }
        }

        // 5*7 = 2100*1500
        private static void Convert5by7(string DownloadFolderPath)
        {
            ProductTypeId = 3;
            // saveFolderPath = savepath + "\\5x7";
            saveFolderPath = System.IO.Path.Combine(DownloadFolderPath, "5x7");
            if (Directory.Exists(saveFolderPath))
            {
                ResizeWPFImage(sourcepath, saveFolderPath);
                saveFolderPath = null;
            }
        }

        // 3*3 = 900*900
        private static void Convert3by3(string DownloadFolderPath)
        {
            ProductTypeId = 4;
            // saveFolderPath = savepath + "\\3x3";
            saveFolderPath = System.IO.Path.Combine(DownloadFolderPath, "3x3");
            if (Directory.Exists(saveFolderPath))
            {
                ResizeWPFImage(sourcepath, saveFolderPath);
                saveFolderPath = null;
            }
        }

        private static void Convert8by10(string DownloadFolderPath)
        {
            ProductTypeId = 5;
            saveFolderPath = System.IO.Path.Combine(DownloadFolderPath, "8x10");
            if (Directory.Exists(saveFolderPath))
            {
                ResizeWPFImage(sourcepath, saveFolderPath);
                saveFolderPath = null;
            }
        }

        private static void ResizeWPFImage(string sourceImage, string saveToPath)
        {
            BitmapImage bi = new BitmapImage();
            BitmapImage bitmapImage = new BitmapImage();
            double ratio = 1;
            try
            {
                using (FileStream fileStream = File.OpenRead(sourceImage.ToString()))
                {
                    MemoryStream ms = new MemoryStream();
                    fileStream.CopyTo(ms);
                    ms.Seek(0, SeekOrigin.Begin);
                    fileStream.Close();
                    bi.BeginInit();
                    bi.StreamSource = ms;
                    bi.EndInit();
                    bi.Freeze();

                    if (ProductTypeId == 1)
                    {
                        if (originalWidth >= originalHeight)
                        {
                            ms.Seek(0, SeekOrigin.Begin);
                            bitmapImage.BeginInit();
                            bitmapImage.StreamSource = ms;
                            bitmapImage.DecodePixelWidth = 2400;
                            bitmapImage.DecodePixelHeight = 1800;

                            bitmapImage.EndInit();
                            bitmapImage.Freeze();
                            ratio = (double)((double)bitmapImage.PixelWidth / (double)bitmapImage.PixelHeight);
                        }
                        else
                        {
                            ms.Seek(0, SeekOrigin.Begin);
                            bitmapImage.BeginInit();
                            bitmapImage.StreamSource = ms;
                            bitmapImage.DecodePixelWidth = 1800;
                            bitmapImage.DecodePixelHeight = 2400;
                            bitmapImage.EndInit();
                            bitmapImage.Freeze();
                            ratio = (double)((double)bitmapImage.PixelHeight / (double)bitmapImage.PixelWidth);
                        }
                    }
                    else if (ProductTypeId == 2)
                    {
                        if (originalWidth >= originalHeight)
                        {
                            ms.Seek(0, SeekOrigin.Begin);
                            bitmapImage.BeginInit();
                            bitmapImage.StreamSource = ms;
                            bitmapImage.DecodePixelWidth = 1800;
                            bitmapImage.DecodePixelHeight = 1200;

                            bitmapImage.EndInit();
                            bitmapImage.Freeze();
                            ratio = (double)((double)bitmapImage.PixelWidth / (double)bitmapImage.PixelHeight);
                        }
                        else
                        {
                            ms.Seek(0, SeekOrigin.Begin);
                            bitmapImage.BeginInit();
                            bitmapImage.StreamSource = ms;
                            bitmapImage.DecodePixelWidth = 1200;
                            bitmapImage.DecodePixelHeight = 1800;

                            bitmapImage.EndInit();
                            bitmapImage.Freeze();
                            ratio = (double)((double)bitmapImage.PixelHeight / (double)bitmapImage.PixelWidth);
                        }
                    }

                    else if (ProductTypeId == 3)
                    {
                        if (originalWidth >= originalHeight)
                        {
                            ms.Seek(0, SeekOrigin.Begin);
                            bitmapImage.BeginInit();
                            bitmapImage.StreamSource = ms;
                            bitmapImage.DecodePixelWidth = 2100;
                            bitmapImage.DecodePixelHeight = 1500;

                            bitmapImage.EndInit();
                            bitmapImage.Freeze();
                            ratio = (double)((double)bitmapImage.PixelWidth / (double)bitmapImage.PixelHeight);
                        }
                        else
                        {
                            ms.Seek(0, SeekOrigin.Begin);
                            bitmapImage.BeginInit();
                            bitmapImage.StreamSource = ms;
                            bitmapImage.DecodePixelWidth = 1500;
                            bitmapImage.DecodePixelHeight = 2100;

                            bitmapImage.EndInit();
                            bitmapImage.Freeze();
                            ratio = (double)((double)bitmapImage.PixelWidth / (double)bitmapImage.PixelHeight);
                        }
                    }
                    else if (ProductTypeId == 4)
                    {
                        ms.Seek(0, SeekOrigin.Begin);
                        bitmapImage.BeginInit();
                        bitmapImage.StreamSource = ms;
                        bitmapImage.DecodePixelWidth = 900;
                        bitmapImage.DecodePixelHeight = 900;

                        bitmapImage.EndInit();
                        bitmapImage.Freeze();
                        ratio = (double)((double)bitmapImage.PixelWidth / (double)bitmapImage.PixelHeight);
                    }
                    else if (ProductTypeId == 5)
                    {
                        if (originalWidth >= originalHeight)
                        {
                            ms.Seek(0, SeekOrigin.Begin);
                            bitmapImage.BeginInit();
                            bitmapImage.StreamSource = ms;
                            bitmapImage.DecodePixelWidth = 3000;
                            bitmapImage.DecodePixelHeight = 2400;

                            bitmapImage.EndInit();
                            bitmapImage.Freeze();
                            ratio = (double)((double)bitmapImage.PixelWidth / (double)bitmapImage.PixelHeight);
                        }
                        else
                        {
                            ms.Seek(0, SeekOrigin.Begin);
                            bitmapImage.BeginInit();
                            bitmapImage.StreamSource = ms;
                            bitmapImage.DecodePixelWidth = 2400;
                            bitmapImage.DecodePixelHeight = 3000;

                            bitmapImage.EndInit();
                            bitmapImage.Freeze();
                            ratio = (double)((double)bitmapImage.PixelHeight / (double)bitmapImage.PixelWidth);
                        }
                    }


                    bitmapImage.Freeze();
                    fileStream.Close();
                }
                string[] filepath = sourceImage.Split(new[] { "\\" }, StringSplitOptions.None);
                string filename = filepath[filepath.Length - 1];
                using (var fileStreamForSave = new FileStream(saveToPath + "\\" + filename, FileMode.Create, FileAccess.ReadWrite))
                {
                    //string savepath1 = savepath + "6x8";
                    //if (!Directory.Exists(savepath1))
                    //{
                    //    Directory.CreateDirectory(savepath1);
                    //}
                    //savepath1 = null;
                    JpegBitmapEncoder encoder = new JpegBitmapEncoder();
                    encoder.QualityLevel = 94;
                    encoder.Frames.Add(BitmapFrame.Create(bitmapImage));
                    encoder.Save(fileStreamForSave);
                    fileStreamForSave.Close();
                }
                bi = null;
                bitmapImage = null;
            }
            catch (Exception ex)
            {
                //_logger.LogException(ex, DigiPhoto.LogEnvelop.Data.LoggingLevel.Error);
                //  ErrorHandler.ErrorHandler.LogFileWrite(ex.Message);
            }
            finally
            {
                bi = null;
                bitmapImage = null;
            }
        }

        #endregion BacGround
    }
}
