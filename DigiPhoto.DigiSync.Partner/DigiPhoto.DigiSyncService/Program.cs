﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using log4net;

namespace DataSyncService
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        private static readonly ILog log = LogManager.GetLogger(typeof(Program));
        static void Main()
        {
            
            
            log4net.Config.XmlConfigurator.Configure();
            log.Info("Sync Service Started");
            //DigiSyncService.SyncOrder();
            ServiceBase[] ServicesToRun;
            ServicesToRun = new ServiceBase[]
            {
                new DigiSyncService()
            };
            ServiceBase.Run(ServicesToRun);
        }
    }
}
