﻿using CloudinaryDotNet;
using CloudinaryDotNet.Actions;
using DigiPhoto.DigiSync.Model;
using log4net;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Microsoft.Azure.Management.Fluent;//Install-Package Microsoft.Azure.Management.Fluent -Version 1.32.0
using Microsoft.Azure.Management.ResourceManager.Fluent;
using Microsoft.Azure.Management.ResourceManager.Fluent.Core;

using Microsoft.Azure.KeyVault;
using Microsoft.IdentityModel.Clients.ActiveDirectory;
using RestSharp;//Install-Package RestSharp -Version 106.10.1
using Newtonsoft.Json;
using Microsoft.WindowsAzure.Storage.Blob;
using System.Collections;
using DigiPhoto.IMIX.Business;
using Microsoft.WindowsAzure.Storage;
using DigiPhoto.DigiSync.Utility;

namespace DataSyncService
{
    public static class UploadToAzure
    {
        private static ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        static UploadToAzure()
        {

        }

        public static CloudinaryUploadInfo UploadFile(UploadFileInfo _uploadFileInfo, out string errorPhotoId)
        {
            CloudinaryUploadInfo objCloudinaryUploadInfo = new CloudinaryUploadInfo();
            try
            {
                //string CloudinaryOrderImagesPath = "OrderImages";
                errorPhotoId = string.Empty;
                //ImageUploadResult uploadResult;
                //VideoUploadResult uploadResultVideo;

                StoreSubStoreDataBusniess stoObj = new StoreSubStoreDataBusniess();
                Hashtable htStoreInfo = stoObj.GetStoreDetails();
                bool isSubscribedToEventGrid = false; //This has to be done only once ,save this info to DB
                bool isLifeCyclePolicySet = false; //This has to be done only once ,save this info to DB               
                string DB_Variable_storageaccountname = string.Empty;
                string DB_Variable_connectionsstring = string.Empty;
                string DB_Variable_containerName = string.Empty;
                string strLifeCyclePolicy = string.Empty;
                string strSubscribedEventGrid = string.Empty;
                string strCountry = string.Empty;

                DB_Variable_storageaccountname = htStoreInfo.ContainsKey("Country") ? "stagdei" + htStoreInfo["Country"].ToString().ToLower() : "stagdeinewdubai";
                DB_Variable_containerName = htStoreInfo.ContainsKey("Store") ? htStoreInfo["Store"].ToString().ToLower() : ""; //Store name is nothing but Venue name
                strCountry = Convert.ToString(htStoreInfo["Country"]).ToLower();

                List<string> lstConfg = stoObj.GetStoreAzureConfiguration(DB_Variable_storageaccountname);

                string path = Path.Combine(Environment.CurrentDirectory, "azureauth.properties");
                var credentials = SdkContext
                                   .AzureCredentialsFactory
                                   .FromFile(path);

                var azure = Azure
                            .Configure()
                            .WithLogLevel(HttpLoggingDelegatingHandler.Level.Basic)
                            .Authenticate(credentials)
                            .WithDefaultSubscription();
                var location = Microsoft.Azure.Management.ResourceManager.Fluent.Core.Region.EuropeWest;
                string rgName = System.Configuration.ConfigurationManager.AppSettings["resourceGroupName"];

                //Check if Azure configurations are saved in DB, if not create and save it
                if (lstConfg != null && lstConfg.Count() > 0)
                {
                    DB_Variable_storageaccountname = lstConfg[0];
                    DB_Variable_connectionsstring = lstConfg[3];//Azure connection string
                    if (lstConfg[2] != "")
                    {
                        isSubscribedToEventGrid = true;
                    }
                    else
                    {
                        Microsoft.Azure.Management.Storage.Fluent.CheckNameAvailabilityResult isAccountAvailableRes = azure.StorageAccounts.CheckNameAvailability(DB_Variable_storageaccountname);//Vins
                                                                                                                                                                                                  //Create Azure storage account if not created earlier
                        if ((bool)isAccountAvailableRes.IsAvailable)
                        {
                            var storageAccount = azure
                                                    .StorageAccounts.Define(DB_Variable_storageaccountname)
                                                    .WithRegion(location)
                                                    .WithNewResourceGroup(rgName)
                                                    .WithGeneralPurposeAccountKindV2()
                                                    .Create();

                            var storageAccountKeys = storageAccount.GetKeys();

                            //Console.WriteLine("Saving Connection Strings to KeyVault.");
                            KeyVaultClient kvClient = new KeyVaultClient(async (authority, resource, scope) =>
                            {
                                var adCredential = new ClientCredential(System.Configuration.ConfigurationManager.AppSettings["AZURE_CLIENT_ID"],
                                                                        System.Configuration.ConfigurationManager.AppSettings["AZURE_CLIENT_SECRET"]);
                                var authenticationContext = new AuthenticationContext(authority, null);
                                return (await authenticationContext.AcquireTokenAsync(resource, adCredential)).AccessToken;
                            });
                            var i = 1;

                            foreach (var batchGroup in storageAccountKeys)
                            {
                                //Save it into Keyvault
                                DB_Variable_connectionsstring = "DefaultEndpointsProtocol=https;AccountName=" + DB_Variable_storageaccountname +
                                    ";AccountKey=" + batchGroup.Value +
                                    ";EndpointSuffix=core.windows.net";
                                /*Note:- EndpointSuffix has to be replaced (Default is kept for now.)
                                    AzureChinaCloud   core.chinacloudapi.cn
                                    AzureCloud        core.windows.net
                                    AzureGermanCloud  core.cloudapi.de
                                    AzureUSGovernment core.usgovcloudapi.net
                                */
                                var secretName = DB_Variable_storageaccountname + "-connectionstring" + i; //Do not change this secretname template 
                                var kvURL = System.Configuration.ConfigurationManager.AppSettings["AZURE_KEY_VAULT_URL"];// Environment.GetEnvironmentVariable("AZURE_KEY_VAULT_URL");
                                i = i + 1;
                                var result = SetSecret(kvClient, kvURL, secretName, DB_Variable_connectionsstring);
                            }

                            // Console.WriteLine("Creating Life cycle Policy");
                            if (isLifeCyclePolicySet == false)
                            {
                                string token = GetAuthorizationHeader().Result;
                                var client = new RestSharp.RestClient("https://management.azure.com/subscriptions/" + System.Configuration.ConfigurationManager.AppSettings["AZURE_SUBSCRIPTION_ID"] +
                                    "/resourceGroups/" + System.Configuration.ConfigurationManager.AppSettings["resourceGroupName"] +
                                    "/providers/Microsoft.Storage/storageAccounts/" + DB_Variable_storageaccountname + "/managementPolicies/default?api-version=2019-06-01");
                                var request = new RestRequest(Method.PUT);
                                request.AddHeader("cache-control", "no-cache");
                                request.AddHeader("authorization", "Bearer " + token);
                                request.AddHeader("accept", "application/json; charset=utf-8");
                                string lifecyclepolicyname = DB_Variable_storageaccountname + "-policy1";
                                //replace with appropriate lifecyclepolicyname like policy1 or venue_name for each venue , since a storage account is for each country 
                                // and each container is a venue you can use the prefixMatch for each venue to demarcate how lifecycle should behave
                                // like for venue1 default delete is 30 (media folder) other venue is (60 days) like that. Place appropriately.
                                //Template prefixMatch :- container-name/media or container-name/transformations
                                //Console.Write("Enter Storage Account Name:- ");
                                //DB_Variable_storageaccountname = "stagedeivins";// Console.ReadLine();
                                //Console.Write("Enter Container Name:- ");
                                string container = DB_Variable_containerName;// Console.ReadLine();
                                                                             //Console.Write("Enter Folder name(media/transformations):- ");
                                string folder = "media";// Console.ReadLine();
                                string prefixMatch = container + "/" + folder;
                                //leave actions in body empty if don't want to apply. a template is given below , replace or remove from `actions` options that are not necessary like snapshort delete option.
                                var body = "{\"properties\":{\"policy\":{\"rules\": [{\"enabled\": true,\"name\": \"" + lifecyclepolicyname + "\",    \"type\": \"Lifecycle\",    \"definition\": { \"filters\": { \"blobTypes\": [   \"blockBlob\" ], \"prefixMatch\": [   \"" + prefixMatch + "\" ] }, \"actions\": { \"baseBlob\": {   \"tierToCool\": {\"daysAfterModificationGreaterThan\": 30   },   \"tierToArchive\": {\"daysAfterModificationGreaterThan\": 90   },   \"delete\": {\"daysAfterModificationGreaterThan\": 1000   } }, \"snapshot\": {   \"delete\": {\"daysAfterCreationGreaterThan\": 30   } } }    }  }]}   } }";

                                request.AddParameter("application/json", body, ParameterType.RequestBody);
                                IRestResponse response = client.Execute(request);

                                strLifeCyclePolicy = response.Content;
                                //Console.WriteLine(response.Content);
                                //Save response to DB_VINS

                                isLifeCyclePolicySet = true;
                            }

                            //-----------------------------------//
                            //Console.WriteLine("Subscribing Event Grid");
                            //Console.Write("Set eventgrid to storage account " + DB_Variable_storageaccountname + " ?: ");
                            //DB_Variable_storageaccountname = "stagedeivins";
                            if (isSubscribedToEventGrid == false)
                            {
                                string token = GetAuthorizationHeader().Result;
                                var client = new RestSharp.RestClient("https://management.azure.com/subscriptions/" + System.Configuration.ConfigurationManager.AppSettings["AZURE_SUBSCRIPTION_ID"] + "/" +
                                    "resourceGroups/" + System.Configuration.ConfigurationManager.AppSettings["resourceGroupName"] + "/providers/Microsoft.Storage/storageaccounts/" + DB_Variable_storageaccountname +
                                    "/providers/Microsoft.EventGrid/eventSubscriptions/" + DB_Variable_storageaccountname + "-events?api-version=2019-01-01");
                                var request = new RestRequest(Method.PUT);//Updates If not exists
                                request.AddHeader("cache-control", "no-cache");
                                request.AddHeader("authorization", "Bearer " + token);
                                request.AddHeader("accept", "application/json; charset=utf-8");
                                //endpointUrl would change for Production.
                                //https://{replace function endpoint url}/runtime/webhooks/EventGrid?functionName={replace function name}&code={replace function code}
                                // Code can be obtained from portal.Use Master key.
                                var body = "{\"properties\":{\"destination\": {\"endpointType\": \"WebHook\",\"properties\": {\"endpointUrl\": \"https://stagdeieventgrid.azurewebsites.net/runtime/webhooks/EventGrid?functionName=auto_thumbnail&code=lSgwnQcSSUfEnYd7ynxQIO7/WWoWiEthGLDVI2JcZ53V0xH2d8JFJA==\"}}}}";

                                request.AddParameter("application/json", body, ParameterType.RequestBody);
                                IRestResponse response = client.Execute(request);

                                strSubscribedEventGrid = response.Content;
                                //Console.WriteLine(response.Content);
                                //Console.ReadKey();
                                isSubscribedToEventGrid = true;
                            }
                            else
                            {
                                //Console.WriteLine("Skipping since already subscribed");
                            }
                            //Save to Database
                            stoObj.SaveStoreAzureDetails(DB_Variable_storageaccountname, strLifeCyclePolicy, strSubscribedEventGrid, DB_Variable_connectionsstring);
                        }//if ended for storage account creation
                    }
                }

                if (isSubscribedToEventGrid == true)
                {
                    //Console.WriteLine("Uploading Images");
                    if (DB_Variable_connectionsstring == "")
                    {
                        // Console.WriteLine("connectionsstring of storage account is empty. possible options [storage account not created | fetching connectionsstring for already created from your db failed] ");
                        //return;
                    }
                    CloudStorageAccount storageAccount_client = CloudStorageAccount.Parse(DB_Variable_connectionsstring);
                    CloudBlobClient blobClient = storageAccount_client.CreateCloudBlobClient();

                    //Console.Write("Enter Venue name:- ");
                    string containerName = DB_Variable_containerName;// Console.ReadLine();
                    CloudBlobContainer container = blobClient.GetContainerReference(containerName);
                    try
                    {
                        container.CreateIfNotExists();
                    }
                    catch (StorageException )
                    {
                        // Ensure that the DB_Variable_connectionsstring is correct or container name is correct.
                        //Console.WriteLine("Exception " + ex);
                        //Console.ReadLine();
                    }

                    //Generate an ad-hoc SAS URI for the container. The ad-hoc SAS has write , read and list and write permissions.
                    string adHocContainerSAS = GetContainerSasUri(container);
                    //Upload to Azure start
                    UploadPhotosAndSaveinPhotosIMS(Convert.ToString(container.Uri), adHocContainerSAS, containerName, _uploadFileInfo.ImagePath, _uploadFileInfo.TargetDirectory);

                    //Upload to Azure end
                }

                // CloudinaryUploadInfo objCloudinaryUploadInfo = new CloudinaryUploadInfo();
                if (!File.Exists(_uploadFileInfo.ImagePath))
                {
                    errorPhotoId = _uploadFileInfo.PhotoId.ToString();
                    objCloudinaryUploadInfo.CloudinaryStatusID = 4;
                    objCloudinaryUploadInfo.ErrorMessage = "Source image was missing";
                }
                else
                {
                    //Cloudinary objCloudinary = new Cloudinary("");
                    //objCloudinary.Api.ApiBaseAddress = cloundApiBaseAddress;
                    //if (ConfigurationManager.AppSettings.AllKeys.Contains("CloudinaryOrderImagesPath"))
                    //{
                    //    CloudinaryOrderImagesPath = ConfigurationManager.AppSettings["CloudinaryOrderImagesPath"].ToString();
                    //}

                    if (_uploadFileInfo.MediaType == 1)
                    {
                        //var uploadparameters = new ImageUploadParams
                        //{
                        //    File = new FileDescription(_uploadFileInfo.ImagePath),
                        //    PublicId = Path.Combine(CloudinaryOrderImagesPath, Path.Combine(_uploadFileInfo.TargetDirectory, Path.GetFileNameWithoutExtension(_uploadFileInfo.Title))).Replace(@"\", @"/"),
                        //    Invalidate = true
                        //};

                        string[] strStorageAccount = _uploadFileInfo.TargetDirectory.Split('\\');
                        //DB_Variable_storageaccountname
                        _uploadFileInfo.TargetDirectory = _uploadFileInfo.TargetDirectory.Replace(strStorageAccount[0], DB_Variable_storageaccountname);
                        _uploadFileInfo.TargetDirectory = _uploadFileInfo.TargetDirectory.Replace("stagdei", "");
                        //countryName = countryName.Replace("stagdei", "");

                        objCloudinaryUploadInfo.CloudinaryStatusID = 1;
                        objCloudinaryUploadInfo.ErrorMessage = "Uploaded successfully";
                        objCloudinaryUploadInfo.CloudinaryPublicID = Path.Combine(_uploadFileInfo.TargetDirectory, Path.GetFileNameWithoutExtension(_uploadFileInfo.Title)).Replace(@"\", @"/");

                        //if (CloudinaryImagesBufferSize == 0)
                        //    uploadResult = objCloudinary.Upload(uploadparameters);
                        //else
                        //    uploadResult = objCloudinary.UploadLarge(uploadparameters, CloudinaryImagesBufferSize);
                        //if (uploadResult.Error != null)
                        //{
                        //    errorPhotoId = _uploadFileInfo.PhotoId.ToString();
                        //    objCloudinaryUploadInfo.CloudinaryStatusID = 2;
                        //    objCloudinaryUploadInfo.ErrorMessage = uploadResult.Error.Message;

                        //}
                        //else
                        //{
                        //    objCloudinaryUploadInfo.CloudinaryStatusID = 1;
                        //    objCloudinaryUploadInfo.ErrorMessage = "Uploaded successfully";
                        //}
                        //objCloudinaryUploadInfo.CloudinaryPublicID = uploadResult.PublicId;
                    }
                    else
                    {
                        //var uploadparameters = new VideoUploadParams
                        //{
                        //    File = new FileDescription(_uploadFileInfo.ImagePath),
                        //    PublicId = Path.Combine(CloudinaryOrderImagesPath, Path.Combine(_uploadFileInfo.TargetDirectory, Path.GetFileNameWithoutExtension(_uploadFileInfo.Title))).Replace(@"\", @"/"),
                        //    Invalidate = true
                        //};

                        //if (CloudinaryVideosBufferSize == 0)
                        //    uploadResultVideo = objCloudinary.Upload(uploadparameters);
                        //else
                        //    uploadResultVideo = objCloudinary.UploadLarge(uploadparameters, CloudinaryVideosBufferSize);
                        //if (uploadResultVideo.Error != null)
                        //{
                        //    errorPhotoId = _uploadFileInfo.PhotoId.ToString();
                        //    objCloudinaryUploadInfo.CloudinaryStatusID = 2;
                        //    objCloudinaryUploadInfo.ErrorMessage = uploadResultVideo.Error.Message;
                        //}
                        //else
                        //{
                        //    objCloudinaryUploadInfo.CloudinaryStatusID = 1;
                        //    objCloudinaryUploadInfo.ErrorMessage = "Uploaded successfully";
                        //}
                        //objCloudinaryUploadInfo.CloudinaryPublicID = uploadResultVideo.PublicId;
                    }
                }
            }
            catch (Exception ex)
            {
                errorPhotoId = _uploadFileInfo.PhotoId.ToString();
                objCloudinaryUploadInfo.CloudinaryStatusID = 2;
                objCloudinaryUploadInfo.ErrorMessage = "Push:UnKnown Message : " + ex.Message + " StackTrace: " + ex.StackTrace + " InnerException: " + ex.InnerException + " Source: " + ex.Source;
                log.Error("Push:: Message: " + ex.Message + " StackTrace: " + ex.StackTrace + " InnerException: " + ex.InnerException + " Source: " + ex.Source);
            }

            ImageAttribute imageAttribute = ImageHelper.GetImageHeightWidth(_uploadFileInfo.ImagePath);
            System.Drawing.Size size = new System.Drawing.Size(_uploadFileInfo.ImageDefaultWidth, _uploadFileInfo.ImageDefaultHeight);
            imageAttribute.Dimension = ImageHelper.ScaleImage(imageAttribute.Width, imageAttribute.Height, size);
            objCloudinaryUploadInfo.Width = imageAttribute.Width;
            objCloudinaryUploadInfo.Height = imageAttribute.Height;
            objCloudinaryUploadInfo.ThumbnailDimension = imageAttribute.Dimension;

            return objCloudinaryUploadInfo;
        }


        public static UploadFile UploadGraphicsContent(UploadFile _uploadFile)
        {
            try
            {
                StoreSubStoreDataBusniess stoObj = new StoreSubStoreDataBusniess();
                Hashtable htStoreInfo = stoObj.GetStoreDetails();
                bool isSubscribedToEventGrid = false; //This has to be done only once ,save this info to DB
                bool isLifeCyclePolicySet = false; //This has to be done only once ,save this info to DB               
                string DB_Variable_storageaccountname = string.Empty;
                string DB_Variable_connectionsstring = string.Empty;
                string DB_Variable_containerName = string.Empty;
                string strLifeCyclePolicy = string.Empty;
                string strSubscribedEventGrid = string.Empty;
                string strCountry = string.Empty;

                DB_Variable_storageaccountname = htStoreInfo.ContainsKey("Country") ? "stagdei" + htStoreInfo["Country"].ToString().ToLower() : "stagdeinewdubai";
                DB_Variable_containerName = htStoreInfo.ContainsKey("Store") ? htStoreInfo["Store"].ToString().ToLower() : ""; //Store name is nothing but Venue name
                strCountry = Convert.ToString(htStoreInfo["Country"]).ToLower();

                List<string> lstConfg = stoObj.GetStoreAzureConfiguration(DB_Variable_storageaccountname);

                string path = Path.Combine(Environment.CurrentDirectory, "azureauth.properties");
                var credentials = SdkContext
                                   .AzureCredentialsFactory
                                   .FromFile(path);

                var azure = Azure
                            .Configure()
                            .WithLogLevel(HttpLoggingDelegatingHandler.Level.Basic)
                            .Authenticate(credentials)
                            .WithDefaultSubscription();
                var location = Microsoft.Azure.Management.ResourceManager.Fluent.Core.Region.EuropeWest;
                string rgName = System.Configuration.ConfigurationManager.AppSettings["resourceGroupName"];

                //Check if Azure configurations are saved in DB, if not create and save it
                if (lstConfg != null && lstConfg.Count() > 0)
                {
                    DB_Variable_storageaccountname = lstConfg[0];
                    DB_Variable_connectionsstring = lstConfg[3];//Azure connection string
                    if (lstConfg[2] != "")
                    {
                        isSubscribedToEventGrid = true;
                    }
                    else
                    {
                        Microsoft.Azure.Management.Storage.Fluent.CheckNameAvailabilityResult isAccountAvailableRes = azure.StorageAccounts.CheckNameAvailability(DB_Variable_storageaccountname);//Vins
                                                                                                                                                                                                  //Create Azure storage account if not created earlier
                        if ((bool)isAccountAvailableRes.IsAvailable)
                        {
                            var storageAccount = azure
                                                    .StorageAccounts.Define(DB_Variable_storageaccountname)
                                                    .WithRegion(location)
                                                    .WithNewResourceGroup(rgName)
                                                    .WithGeneralPurposeAccountKindV2()
                                                    .Create();

                            var storageAccountKeys = storageAccount.GetKeys();

                            //Console.WriteLine("Saving Connection Strings to KeyVault.");
                            KeyVaultClient kvClient = new KeyVaultClient(async (authority, resource, scope) =>
                            {
                                var adCredential = new ClientCredential(System.Configuration.ConfigurationManager.AppSettings["AZURE_CLIENT_ID"],
                                                                        System.Configuration.ConfigurationManager.AppSettings["AZURE_CLIENT_SECRET"]);
                                var authenticationContext = new AuthenticationContext(authority, null);
                                return (await authenticationContext.AcquireTokenAsync(resource, adCredential)).AccessToken;
                            });
                            var i = 1;

                            foreach (var batchGroup in storageAccountKeys)
                            {
                                //Save it into Keyvault
                                DB_Variable_connectionsstring = "DefaultEndpointsProtocol=https;AccountName=" + DB_Variable_storageaccountname +
                                    ";AccountKey=" + batchGroup.Value +
                                    ";EndpointSuffix=core.windows.net";
                                /*Note:- EndpointSuffix has to be replaced (Default is kept for now.)
                                    AzureChinaCloud   core.chinacloudapi.cn
                                    AzureCloud        core.windows.net
                                    AzureGermanCloud  core.cloudapi.de
                                    AzureUSGovernment core.usgovcloudapi.net
                                */
                                var secretName = DB_Variable_storageaccountname + "-connectionstring" + i; //Do not change this secretname template 
                                var kvURL = System.Configuration.ConfigurationManager.AppSettings["AZURE_KEY_VAULT_URL"];// Environment.GetEnvironmentVariable("AZURE_KEY_VAULT_URL");
                                i = i + 1;
                                var result = SetSecret(kvClient, kvURL, secretName, DB_Variable_connectionsstring);
                            }

                            // Console.WriteLine("Creating Life cycle Policy");
                            if (isLifeCyclePolicySet == false)
                            {
                                string token = GetAuthorizationHeader().Result;
                                var client = new RestSharp.RestClient("https://management.azure.com/subscriptions/" + System.Configuration.ConfigurationManager.AppSettings["AZURE_SUBSCRIPTION_ID"] +
                                    "/resourceGroups/" + System.Configuration.ConfigurationManager.AppSettings["resourceGroupName"] +
                                    "/providers/Microsoft.Storage/storageAccounts/" + DB_Variable_storageaccountname + "/managementPolicies/default?api-version=2019-06-01");
                                var request = new RestRequest(Method.PUT);
                                request.AddHeader("cache-control", "no-cache");
                                request.AddHeader("authorization", "Bearer " + token);
                                request.AddHeader("accept", "application/json; charset=utf-8");
                                string lifecyclepolicyname = DB_Variable_storageaccountname + "-policy1";
                                //replace with appropriate lifecyclepolicyname like policy1 or venue_name for each venue , since a storage account is for each country 
                                // and each container is a venue you can use the prefixMatch for each venue to demarcate how lifecycle should behave
                                // like for venue1 default delete is 30 (media folder) other venue is (60 days) like that. Place appropriately.
                                //Template prefixMatch :- container-name/media or container-name/transformations
                                //Console.Write("Enter Storage Account Name:- ");
                                //DB_Variable_storageaccountname = "stagedeivins";// Console.ReadLine();
                                //Console.Write("Enter Container Name:- ");
                                string container = DB_Variable_containerName;// Console.ReadLine();
                                                                             //Console.Write("Enter Folder name(media/transformations):- ");
                                string folder = "media";// Console.ReadLine();
                                string prefixMatch = container + "/" + folder;
                                //leave actions in body empty if don't want to apply. a template is given below , replace or remove from `actions` options that are not necessary like snapshort delete option.
                                var body = "{\"properties\":{\"policy\":{\"rules\": [{\"enabled\": true,\"name\": \"" + lifecyclepolicyname + "\",    \"type\": \"Lifecycle\",    \"definition\": { \"filters\": { \"blobTypes\": [   \"blockBlob\" ], \"prefixMatch\": [   \"" + prefixMatch + "\" ] }, \"actions\": { \"baseBlob\": {   \"tierToCool\": {\"daysAfterModificationGreaterThan\": 30   },   \"tierToArchive\": {\"daysAfterModificationGreaterThan\": 90   },   \"delete\": {\"daysAfterModificationGreaterThan\": 1000   } }, \"snapshot\": {   \"delete\": {\"daysAfterCreationGreaterThan\": 30   } } }    }  }]}   } }";

                                request.AddParameter("application/json", body, ParameterType.RequestBody);
                                IRestResponse response = client.Execute(request);

                                strLifeCyclePolicy = response.Content;
                                //Console.WriteLine(response.Content);
                                //Save response to DB_VINS

                                isLifeCyclePolicySet = true;
                            }

                            //-----------------------------------//
                            //Console.WriteLine("Subscribing Event Grid");
                            //Console.Write("Set eventgrid to storage account " + DB_Variable_storageaccountname + " ?: ");
                            //DB_Variable_storageaccountname = "stagedeivins";
                            if (isSubscribedToEventGrid == false)
                            {
                                string token = GetAuthorizationHeader().Result;
                                var client = new RestSharp.RestClient("https://management.azure.com/subscriptions/" + System.Configuration.ConfigurationManager.AppSettings["AZURE_SUBSCRIPTION_ID"] + "/" +
                                    "resourceGroups/" + System.Configuration.ConfigurationManager.AppSettings["resourceGroupName"] + "/providers/Microsoft.Storage/storageaccounts/" + DB_Variable_storageaccountname +
                                    "/providers/Microsoft.EventGrid/eventSubscriptions/" + DB_Variable_storageaccountname + "-events?api-version=2019-01-01");
                                var request = new RestRequest(Method.PUT);//Updates If not exists
                                request.AddHeader("cache-control", "no-cache");
                                request.AddHeader("authorization", "Bearer " + token);
                                request.AddHeader("accept", "application/json; charset=utf-8");
                                //endpointUrl would change for Production.
                                //https://{replace function endpoint url}/runtime/webhooks/EventGrid?functionName={replace function name}&code={replace function code}
                                // Code can be obtained from portal.Use Master key.
                                var body = "{\"properties\":{\"destination\": {\"endpointType\": \"WebHook\",\"properties\": {\"endpointUrl\": \"https://stagdeieventgrid.azurewebsites.net/runtime/webhooks/EventGrid?functionName=auto_thumbnail&code=lSgwnQcSSUfEnYd7ynxQIO7/WWoWiEthGLDVI2JcZ53V0xH2d8JFJA==\"}}}}";

                                request.AddParameter("application/json", body, ParameterType.RequestBody);
                                IRestResponse response = client.Execute(request);

                                strSubscribedEventGrid = response.Content;
                                //Console.WriteLine(response.Content);
                                //Console.ReadKey();
                                isSubscribedToEventGrid = true;
                            }
                            else
                            {
                                //Console.WriteLine("Skipping since already subscribed");
                            }
                            //Save to Database
                            stoObj.SaveStoreAzureDetails(DB_Variable_storageaccountname, strLifeCyclePolicy, strSubscribedEventGrid, DB_Variable_connectionsstring);
                        }//if ended for storage account creation
                    }
                }

                if (isSubscribedToEventGrid == true)
                {
                    //Console.WriteLine("Uploading Images");
                    if (DB_Variable_connectionsstring == "")
                    {
                        // Console.WriteLine("connectionsstring of storage account is empty. possible options [storage account not created | fetching connectionsstring for already created from your db failed] ");
                        //return;
                    }
                    CloudStorageAccount storageAccount_client = CloudStorageAccount.Parse(DB_Variable_connectionsstring);
                    CloudBlobClient blobClient = storageAccount_client.CreateCloudBlobClient();

                    //Console.Write("Enter Venue name:- ");
                    string containerName = DB_Variable_containerName;// Console.ReadLine();
                    CloudBlobContainer container = blobClient.GetContainerReference(containerName);
                    try
                    {
                        container.CreateIfNotExists();
                    }
                    catch (StorageException )
                    {
                        // Ensure that the DB_Variable_connectionsstring is correct or container name is correct.
                        //Console.WriteLine("Exception " + ex);
                        //Console.ReadLine();
                    }

                    //Generate an ad-hoc SAS URI for the container. The ad-hoc SAS has write , read and list and write permissions.
                    string adHocContainerSAS = GetContainerSasUri(container);
                    //Upload to Azure start
                    UploadGraphicsToAzure(Convert.ToString(container.Uri), adHocContainerSAS, containerName, _uploadFile);

                    //Upload to Azure end
                }
            }
            catch (Exception ex)
            {
                log.Error("Push:: Message: " + ex.Message + " StackTrace: " + ex.StackTrace + " InnerException: " + ex.InnerException + " Source: " + ex.Source);
            }

            return _uploadFile;
        }

        private static void UploadGraphicsToAzure(string containerURI, string adHocContainerSAS, string venueName, UploadFile _uploadFile)
        {
            string source_dir = string.Empty;
            //string siteName = TargetFileName.Substring(TargetFileName.ToLower().IndexOf(venueName) + venueName.Length + 1);//For ex. DUBAI\LocalVenue\LocalSite\20200617\DG-1937103164\test5432
            string TargetFileName = "Images/PartnerImages/" + _uploadFile.SaveFolderPath;
            string dest_dir = containerURI + "/media/" + "" + TargetFileName + "/" + adHocContainerSAS; // media is a must , rest can be configured

            string include_files = System.IO.Path.GetFileName(_uploadFile.UploadFilePath);// string.Join(";", sitePhotosDateWise.Select(x => x.DG_Photos_FileName).ToArray());
            string includeMinified_files = "m_" + include_files.Replace(";", ";m_");

            source_dir = _uploadFile.UploadFilePath.Replace(include_files, "");// sitePhotosDateWise.ElementAt(0).HotFolderPath + sitePhotosDateWise.ElementAt(0).DG_Photos_CreatedOn.ToString("yyyyMMdd");

            if (source_dir.EndsWith("\\"))
            {
                source_dir = source_dir.Substring(0, source_dir.Length - 1);
            }

            ProcessStartInfo startInfo = new ProcessStartInfo();
            startInfo.CreateNoWindow = false;
            startInfo.UseShellExecute = false;
            startInfo.RedirectStandardOutput = true;
            startInfo.FileName = Path.Combine(Environment.CurrentDirectory, "azcopy_windows_amd64_10.3.4", "azcopy.exe");
            startInfo.WindowStyle = ProcessWindowStyle.Hidden;
            startInfo.Arguments = "sync \"" + source_dir + "\" \"" + dest_dir + "\"" + " --include-pattern=\"" + include_files + "\"";
            try
            {
                // Start the process with the info we specified.
                // Call WaitForExit and then the using statement will close.
                using (Process exeProcess = Process.Start(startInfo))
                {
                    string output = exeProcess.StandardOutput.ReadToEnd();
                    exeProcess.WaitForExit();
                    log.Error("Azure: Log info about original :" + output);
                }
            }
            catch (Exception ex)
            {
                log.Error("Azure: Exceptionlog info about original :" + ex.InnerException.ToString());
            }

            //upload minified images now..                    
            //startInfo.Arguments = "sync \"" + source_dir + "\" \"" + dest_dir + "\"" + " --include-pattern=\"" + includeMinified_files + "\"";
            //try
            //{
            //    // Start the process with the info we specified.
            //    // Call WaitForExit and then the using statement will close.
            //    using (Process exeProcess = Process.Start(startInfo))
            //    {
            //        string output = exeProcess.StandardOutput.ReadToEnd();
            //        exeProcess.WaitForExit();
            //        log.Error("Azure: Log info about minified :" + output);
            //    }
            //}
            //catch (Exception ex)
            //{
            //    log.Error("Azure: Exceptionlog info about minified :" + ex.InnerException.ToString());
            //}
        }


        static async Task<bool> SetSecret(KeyVaultClient kvClient, string kvURL, string secretName, string secretValue)
        {
            // <setsecret>
            await kvClient.SetSecretAsync($"{kvURL}", secretName, secretValue);
            // </setsecret>

            return true;
        }

        private static async Task<string> GetAuthorizationHeader()
        {
            string applicationId = System.Configuration.ConfigurationManager.AppSettings["AZURE_CLIENT_ID"];
            string password = System.Configuration.ConfigurationManager.AppSettings["AZURE_CLIENT_SECRET"];
            string tenantId = System.Configuration.ConfigurationManager.AppSettings["AZURE_TENANT_ID"];
            ClientCredential cc = new ClientCredential(applicationId, password);
            var context = new AuthenticationContext("https://login.windows.net/" + tenantId);
            var result = await context.AcquireTokenAsync("https://management.azure.com/", cc);

            if (result == null)
            {
                throw new InvalidOperationException("Failed to obtain the JWT token");
            }

            string token = result.AccessToken;

            return token;
        }

        static string GetContainerSasUri(CloudBlobContainer container)
        {
            string sasContainerToken;
            SharedAccessBlobPolicy adHocPolicy = new SharedAccessBlobPolicy()
            {
                // Set start time to five minutes before now to avoid clock skew.
                SharedAccessStartTime = DateTime.UtcNow.AddMinutes(-5),
                SharedAccessExpiryTime = DateTime.UtcNow.AddHours(24),
                Permissions = SharedAccessBlobPermissions.Write | SharedAccessBlobPermissions.List | SharedAccessBlobPermissions.Read
            };
            sasContainerToken = container.GetSharedAccessSignature(adHocPolicy, null);

            //Return the URI string for the container, including the SAS token.
            return sasContainerToken;
        }

        private static void UploadPhotosAndSaveinPhotosIMS(string containerURI, string adHocContainerSAS, string venueName, string sourceFileName, string TargetFileName)
        {
            string source_dir = string.Empty;
            string siteName = TargetFileName.Substring(TargetFileName.ToLower().IndexOf(venueName) + venueName.Length + 1);//For ex. DUBAI\LocalVenue\LocalSite\20200617\DG-1937103164\test5432

            string dest_dir = containerURI + "/media/" + "" + siteName + "/" + adHocContainerSAS; // media is a must , rest can be configured

            string include_files = System.IO.Path.GetFileName(sourceFileName).Replace("m_", "");// string.Join(";", sitePhotosDateWise.Select(x => x.DG_Photos_FileName).ToArray());
            string includeMinified_files = System.IO.Path.GetFileName(sourceFileName); //"m_" + include_files.Replace(";", ";m_");

            source_dir = sourceFileName.Replace(includeMinified_files, "");// sitePhotosDateWise.ElementAt(0).HotFolderPath + sitePhotosDateWise.ElementAt(0).DG_Photos_CreatedOn.ToString("yyyyMMdd");

            if (source_dir.EndsWith("\\"))
            {
                source_dir = source_dir.Substring(0, source_dir.Length - 1);
            }

            ProcessStartInfo startInfo = new ProcessStartInfo();
            startInfo.CreateNoWindow = false;
            startInfo.UseShellExecute = false;
            startInfo.RedirectStandardOutput = true;
            startInfo.FileName = Path.Combine(Environment.CurrentDirectory, "azcopy_windows_amd64_10.3.4", "azcopy.exe");
            startInfo.WindowStyle = ProcessWindowStyle.Hidden;
            startInfo.Arguments = "sync \"" + source_dir + "\" \"" + dest_dir + "\"" + " --include-pattern=\"" + include_files + "\"";
            try
            {
                // Start the process with the info we specified.
                // Call WaitForExit and then the using statement will close.
                using (Process exeProcess = Process.Start(startInfo))
                {
                    string output = exeProcess.StandardOutput.ReadToEnd();
                    exeProcess.WaitForExit();
                    log.Error("Azure: Log info about original :" + output);
                }
            }
            catch (Exception ex)
            {
                log.Error("Azure: Exceptionlog info about original :" + ex.InnerException.ToString());
            }

            //upload minified images now..                    
            startInfo.Arguments = "sync \"" + source_dir + "\" \"" + dest_dir + "\"" + " --include-pattern=\"" + includeMinified_files + "\"";
            try
            {
                // Start the process with the info we specified.
                // Call WaitForExit and then the using statement will close.
                using (Process exeProcess = Process.Start(startInfo))
                {
                    string output = exeProcess.StandardOutput.ReadToEnd();
                    exeProcess.WaitForExit();
                    log.Error("Azure: Log info about minified :" + output);
                }
            }
            catch (Exception ex)
            {
                log.Error("Azure: Exceptionlog info about minified :" + ex.InnerException.ToString());
            }
        }

    }
}
