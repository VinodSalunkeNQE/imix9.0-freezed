﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Transactions;

namespace DigiPhoto.DigiSync.Utility
{
   public class TransactionManagement
    {
        public static TransactionScope CreateTransactionScope()
       {
           var transactionOptions = new TransactionOptions();
           transactionOptions.IsolationLevel = IsolationLevel.ReadCommitted;
           //This transaction will never abort and allocated maximum time to complete db task.
           transactionOptions.Timeout = TransactionManager.MaximumTimeout;
            return new TransactionScope(TransactionScopeOption.Required, transactionOptions);
       }
    }
}
