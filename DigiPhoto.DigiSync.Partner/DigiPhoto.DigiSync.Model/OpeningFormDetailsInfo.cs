﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DigiPhoto.DigiSync.Model
{
    public class OpeningFormDetailsInfo
    {
        public long OpeningFormDetailID { get; set; }
        public DateTime BussinessDate { get; set; }
        public long StartingNumber6X8 { get; set; }
        public long StartingNumber8X10 { get; set; }
        public long PosterStartingNumber { get; set; }
        public long AutoOpening6X8PrinterCount { get; set; }
        public long AutoOpening8x10PrinterCount { get; set; }
        public decimal CashFloatAmount { get; set; }
        public Int32 SubStoreID { get; set; }
        public DateTime OpeningDate { get; set; }
        public Int32 FilledBy { get; set; }
        public string FilledBySyncCode { get; set; }
        public string SyncCode { get; set; }
        public SubStoreInfo SubStore { get; set; }
    }
}
