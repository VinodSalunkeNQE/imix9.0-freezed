﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DigiPhoto.DigiSync.Model
{
    public class IncomingChangeInfo
    {
        public long IncomingChangeId { get; set; }
        public long ChangeTrackingId { get; set; }
        public long ApplicationObjectId { get; set; }
        public long ObjectValueId { get; set; }
        public int ChangeAction { get; set; }
        public DateTime ChangeDateOnIMIX { get; set; }
        public long ChangeByOnIMIX { get; set; }
        public DateTime ChangeRecievedOn { get; set; }
        public string DataXML { get; set; }       
        public string StoreName { get; set; }
        public int ProcessingStatus { get; set; }
        public string EntityCode { get; set; }
        public SiteInfo SubStore { get; set; }

        public string QrCodes { get; set; }

        public int RetryCount { get; set; }
    }
}
