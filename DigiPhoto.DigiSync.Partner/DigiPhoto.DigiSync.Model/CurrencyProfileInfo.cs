﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DigiPhoto.DigiSync.Model
{
   public class CurrencyProfileInfo
    {
        
            public long ProfileAuditID { get; set; }
            public string ProfileName { get; set; }
            public DateTime startDate { get; set; }
            public DateTime? Enddate { get; set; }
            public bool IsActive { get; set; }
            public long CreatedBy { get; set; }
            public DateTime CreatedOn { get; set; }
            public string Updatedby { get; set; }
            public DateTime updatedon { get; set; }
            public DateTime? PublishedOn { get; set; }
            public string SyncCode { get; set; }
            public List<CurrencyProfileRateInfo> lstCurrencyProfileRate { get; set; }
            public bool? IsDeleted { get; set; }
        
    }
}
