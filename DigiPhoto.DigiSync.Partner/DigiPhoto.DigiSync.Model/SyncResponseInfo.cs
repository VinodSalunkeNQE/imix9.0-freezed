﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DigiPhoto.DigiSync.Model
{
    public class SyncResponseInfo
    {
        public Guid SessionTokenId { get; set; }
    }
}
