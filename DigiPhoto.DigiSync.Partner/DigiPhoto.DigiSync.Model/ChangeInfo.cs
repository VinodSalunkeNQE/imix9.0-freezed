﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ServiceModel;
using System.IO;
using System.Security.Cryptography;

namespace DigiPhoto.DigiSync.Model
{

    public class ChangeInfo
    {
        public long ApplicationObjectId { get; set; }
        public long ObjectValueId { get; set; }
        public long ChangeTrackingId { get; set; }
        public DateTime ChangeDate { get; set; }
        public string dataXML { get; set; }
        public int ChangeAction { get; set; }
        public long ChangeBy { get; set; }
        public long sessionId { get; set; }
        public string EntityCode { get; set; }
        public SiteInfo SubStore { get; set; }
        public DownloadInfo DownloadDetail { get; set; }
        public List<UploadInfo> UploadList { get; set; }

        public UploadFile UploadFile { get; set; }
        public bool? IsDeleted { get; set; }

        //new arch start
        public string QrCodes { get; set; }
        //new arch end

        public string Authkey { get; set; } // Added by Arshad
    }

    public static class Authentication
    {
        public static bool CustomValidator(string Authkey)
        {
            bool Flag = false;
            string FkeyValue = AuthkeyDecryption(Authkey);//Decrypt(AuthKey);
            string ValidateKey = System.Configuration.ConfigurationSettings.AppSettings["Authkey"];
            if (FkeyValue == ValidateKey)
            {
                Flag = true;
            }
            return Flag;
        }
        public static string AuthkeyDecryption(string Decrypt)
        {
            try
            {
                string textToDecrypt = Decrypt;
                string ToReturn = "";
                string publickey = "deiimix1";
                string privatekey = "deiimix1";
                byte[] privatekeyByte = { };
                privatekeyByte = System.Text.Encoding.UTF8.GetBytes(privatekey);
                byte[] publickeybyte = { };
                publickeybyte = System.Text.Encoding.UTF8.GetBytes(publickey);
                MemoryStream ms = null;
                CryptoStream cs = null;
                byte[] inputbyteArray = new byte[textToDecrypt.Replace(" ", "+").Length];
                inputbyteArray = Convert.FromBase64String(textToDecrypt.Replace(" ", "+"));
                using (DESCryptoServiceProvider des = new DESCryptoServiceProvider())
                {
                    ms = new MemoryStream();
                    cs = new CryptoStream(ms, des.CreateDecryptor(publickeybyte, privatekeyByte), CryptoStreamMode.Write);
                    cs.Write(inputbyteArray, 0, inputbyteArray.Length);
                    cs.FlushFinalBlock();
                    Encoding encoding = Encoding.UTF8;
                    ToReturn = encoding.GetString(ms.ToArray());
                }
                return ToReturn;
            }
            catch (Exception ae)
            {
                throw new Exception(ae.Message, ae.InnerException);
            }
        }
    }
}
