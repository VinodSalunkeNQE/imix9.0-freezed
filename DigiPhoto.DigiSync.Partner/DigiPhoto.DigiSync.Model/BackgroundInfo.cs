﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DigiPhoto.DigiSync.Model
{
    public class BackgroundInfo
    {

        public long BackgoundId { get; set; }
        public string BackgroundImage { get; set; }
        public long? ProductId { get; set; }
        public string DisplayName { get; set; }
        public int ?BackgroundGroupId { get; set; }
        public string SyncCode { get; set; }

        public DateTime? ModifiedDate { get; set; }
        public string SiteCodeSyncCode { get; set; }
        public ProductInfo Product { get; set; }
        public bool? IsActive { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public long? ModifiedBy { get; set; }
    }
}
