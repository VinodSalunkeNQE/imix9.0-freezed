﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DigiPhoto.DigiSync.Model
{
 public   class CameraInfo
    {
        public long CameraId { get; set; }       
        public string Name { get; set; }     
        public string Make { get; set; }      
        public string Model { get; set; }
        public DateTime CreatedDateTime { get; set; }
        public DateTime? ModifiedDateTime { get; set; }
        public int CreatedBy { get; set; }
        public long? ModifiedBy { get; set; }
        public bool IsActive { get; set; }      
        public string SyncCode { get; set; }
        public string Start_Series { get; set; }
        public int AssignTo { get; set; }
        public string syncCodeForUser { get; set; }
    }
}
