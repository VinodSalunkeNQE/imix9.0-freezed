﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DigiPhoto.DigiSync.Model
{
    public class DownloadInfo
    {
        public string DownloadFilePath { get; set; }
    }
}
