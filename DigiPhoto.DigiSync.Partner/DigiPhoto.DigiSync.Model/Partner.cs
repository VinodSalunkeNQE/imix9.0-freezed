﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DigiPhoto.DigiSync.Model
{
    public class Partner
    {
        public int PartnerId { get; set; }

        public string PartnerName { get; set; }

        public bool IsActive { get; set; }
    }
}
