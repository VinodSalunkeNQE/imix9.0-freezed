﻿using DigiPhoto.DigiSync.DataAccess.DataModels;
using DigiPhoto.DigiSync.Model;
using DigiPhoto.DigiSync.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using log4net;
namespace DigiPhoto.DigiSync.Business.Processing
{
    public class CloudinaryProcessor : BaseProcessor
    {
        private static ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        protected override void ProcessInsert()
        {
            try
            {
                if (!string.IsNullOrEmpty(ChangeInfo.DataXML))
                {
                    CloudinaryInfo cloudInaryInfo = CommonUtility.DeserializeXML<CloudinaryInfo>(ChangeInfo.DataXML);
                    using (DigiPhotoWebContext dbContext = new DigiPhotoWebContext())
                    {
                        int PhotoID;
                        Photo Photos = null;
                        var subStoreId = dbContext.DigiMasterLocations.Where(s => s.SyncCode == cloudInaryInfo.SubStoreSyncCode && s.Level == 4).FirstOrDefault().DigiMasterLocationId;
                        Photos = dbContext.Photos.Where(p => p.IMIXPhotoId == cloudInaryInfo.PhotoID && p.FileName == cloudInaryInfo.SourceImageID && p.SubStoreId == subStoreId).FirstOrDefault();
                        if (Photos != null)
                            PhotoID = Photos.PhotoId;
                        else
                            PhotoID = dbContext.Photos.Where(p => p.IMIXPhotoId == cloudInaryInfo.PhotoID && p.FileName == cloudInaryInfo.SourceImageID).FirstOrDefault().PhotoId;
                        var WebPhoto = dbContext.WebPhotos.Where(w => w.PhotoId == PhotoID && w.OrderNumber == cloudInaryInfo.OrderNumber && w.IdentificationCode == cloudInaryInfo.IdentificationCode).FirstOrDefault();

                        var partnerId = (from DigiLocation in dbContext.DigiMasterLocations
                                         join DigiSubStore in dbContext.DigiMasterLocations
                                         on DigiLocation.DigiMasterLocationId equals DigiSubStore.ParentDigiMasterLocationId
                                         join PtrLocation in dbContext.PartnerLocations
                                         on DigiLocation.DigiMasterLocationId equals PtrLocation.StoreID
                                         where DigiSubStore.DigiMasterLocationId == subStoreId
                                         select PtrLocation.PartnerID).FirstOrDefault();
                        //if (WebPhoto.WebPhotoId > 0)
                        if (WebPhoto != null && WebPhoto.WebPhotoId > 0)
                        {
                            //Image successfully uploaded to Cloudinary
                            if (cloudInaryInfo.CloudinaryStatusID == 1)
                            {
                                CloudinaryDtl obj = new CloudinaryDtl();
                                obj.WebPhotoID = WebPhoto.WebPhotoId;
                                obj.PartnerId = partnerId;
                                obj.SourceImageID = cloudInaryInfo.SourceImageID;
                                obj.CloudinaryPublicID = cloudInaryInfo.CloudinaryPublicID;
                                obj.CloudinaryStatusID = Convert.ToInt16(cloudInaryInfo.CloudinaryStatusID);
                                obj.RetryCount = Convert.ToInt16(cloudInaryInfo.RetryCount);
                                obj.ErrorMessage = cloudInaryInfo.ErrorMessage;
                                obj.AddedBy = cloudInaryInfo.AddedBy;
                                obj.CreatedDateTime = cloudInaryInfo.CreatedDateTime;
                                obj.ModifiedBy = cloudInaryInfo.ModifiedBy;
                                obj.ModifiedDateTime = cloudInaryInfo.ModifiedDateTime;
                                obj.IsActive = cloudInaryInfo.IsActive;
                                dbContext.CloudinaryDtls.Add(obj);

                                WebPhoto.Height = cloudInaryInfo.Height;
                                WebPhoto.Width = cloudInaryInfo.Width;
                                WebPhoto.ThumbnailDimension = cloudInaryInfo.ThumbnailDimension;
                            }
                            else
                            {
                                //Image successfully not uploaded to Cloudinary
                                CloudinaryExceptionDtl obj = new CloudinaryExceptionDtl();
                                obj.WebPhotoID = WebPhoto.WebPhotoId;
                                obj.PartnerId = partnerId;
                                obj.SourceImageID = cloudInaryInfo.SourceImageID;
                                obj.CloudinaryStatusID = Convert.ToInt16(cloudInaryInfo.CloudinaryStatusID);
                                obj.AddedBy = cloudInaryInfo.AddedBy;
                                obj.CreatedDateTime = cloudInaryInfo.CreatedDateTime;
                                obj.ModifiedBy = cloudInaryInfo.ModifiedBy;
                                obj.ModifiedDateTime = cloudInaryInfo.ModifiedDateTime;
                                obj.IsActive = cloudInaryInfo.IsActive;
                                dbContext.CloudinaryExceptionDtls.Add(obj);
                            }
                            dbContext.SaveChanges();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("CloudinaryProcessor:ProcessInsert:: Message: " + ex.Message + " StackTrace: " + ex.StackTrace + " InnerException: " + ex.InnerException + " Source: " + ex.Source);
                Exception outerException = new ProcessingException("Error occured in Cloudinary Processor Insert.", ex);
                outerException.Source = "Business.Processing.CloudinaryProcessor";
                throw outerException;
            }
        }
        protected override void ProcessUpdate()
        {
            throw new Exception("Not emplemented");
        }

        protected override void ProcessDelete()
        {
            throw new Exception("Not emplemented");
        }
    }
}
