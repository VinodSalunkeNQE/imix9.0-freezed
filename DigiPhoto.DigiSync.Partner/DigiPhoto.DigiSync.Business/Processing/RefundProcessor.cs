﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DigiPhoto.DigiSync.DataAccess.DataModels;
using DigiPhoto.DigiSync.Model;
using DigiPhoto.DigiSync.Utility;
using log4net;

namespace DigiPhoto.DigiSync.Business.Processing
{
    class RefundProcessor : BaseProcessor
    {
        private static ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        protected override void ProcessInsert()
        {
            try
            {
                if (!string.IsNullOrEmpty(ChangeInfo.DataXML))
                {
                    RefundDetailInfo RefundInfo = CommonUtility.DeserializeXML<RefundDetailInfo>(ChangeInfo.DataXML);
                    using (DigiPhotoWebContext dbContext = new DigiPhotoWebContext())
                    {
                        var productrefund = new ProductRefund();
                        var productrefunddetails = new ProductRefundDetail();
                        var orders = dbContext.OrderMasters.Where(r => r.SyncCode == RefundInfo.OrderSyncCode).FirstOrDefault();                       
                        var users = dbContext.Users.Where(r => r.SyncCode == RefundInfo.UserSynccode).FirstOrDefault();
                        if (orders != null)
                        {
                            productrefund.OrderMasterId = orders.OrderMasterId;
                            productrefund.RefundAmount = RefundInfo.RefundAmount;
                            productrefund.RefundMode = RefundInfo.RefundMode;
                            productrefund.RefundDateTime = RefundInfo.RefundDate;
                            productrefund.RefundBy = users.UserId;
                            dbContext.ProductRefunds.Add(productrefund);
                            dbContext.SaveChanges();
                            long productrefundid = productrefund.ProductRefundId;

                            foreach (var item in RefundInfo.refundphotolist)
                            {
                                int photoId = 0;
                                if (!string.IsNullOrEmpty(item.RefundPhotoId))
                                {
                                    photoId = Convert.ToInt32(item.RefundPhotoId);
                                }
                                var photo = dbContext.Photos.Where(r => r.IMIXPhotoId == photoId && r.SubStoreId == orders.SubStoreId).FirstOrDefault();                            
                                if (photo != null)
                                {
                                    productrefunddetails.RefundDigiImageId = photo.PhotoId.ToString();
                                }
                                var orderdetails = dbContext.OrderDetails.Where(r => r.SyncCode == item.OrderDetailsSyncCode).FirstOrDefault();
                                if (orderdetails != null)
                                {
                                    productrefunddetails.OrderDetailId = orderdetails.OrderDetailId;
                                }
                                productrefunddetails.ProductRefundId = productrefundid;
                                productrefunddetails.Reason = RefundInfo.RefundReason;
                                productrefunddetails.Amount = RefundInfo.RefundedAmount;
                                dbContext.ProductRefundDetails.Add(productrefunddetails);
                                dbContext.SaveChanges();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("RefundProcessor:ProcessInsert:: Message: " + ex.Message + " StackTrace: " + ex.StackTrace + " InnerException: " + ex.InnerException + " Source: " + ex.Source);
                Exception outerException = new ProcessingException("Error occured in User Processor Insert.", ex);
                outerException.Source = "Business.Processing.UserProcessor";
                throw outerException;
            }
        }
    }
}
