﻿using DigiPhoto.DigiSync.DataAccess.DataModels;
using DigiPhoto.DigiSync.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DigiPhoto.DigiSync.Business.Processing
{
    public interface IProcesser
    {
        void Process(IncomingChangeInfo changeInfo);
    }
}
