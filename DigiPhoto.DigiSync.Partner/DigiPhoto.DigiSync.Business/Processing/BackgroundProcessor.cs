﻿using DigiPhoto.DigiSync.DataAccess.DataModels;
using DigiPhoto.DigiSync.Model;
using DigiPhoto.DigiSync.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using log4net;
namespace DigiPhoto.DigiSync.Business.Processing
{
    //Created By Kumar Gaurav.............
    public class BackgroundProcessor : BaseProcessor
    {
        private static ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        protected override void ProcessInsert()
        {
            try
            {
                if (!string.IsNullOrEmpty(ChangeInfo.DataXML))
                {
                    BackgroundInfo backGroundInfo = CommonUtility.DeserializeXML<BackgroundInfo>(ChangeInfo.DataXML);
                    using (DigiPhotoWebContext dbContext = new DigiPhotoWebContext())
                    {
                        var existingBackground = dbContext.Backgrounds.Count(r => r.SyncCode == backGroundInfo.SyncCode);
                        var storeId = (from DMStore in dbContext.DigiMasterLocations
                                         join DMSite in dbContext.DigiMasterLocations
                                         on DMStore.DigiMasterLocationId equals DMSite.ParentDigiMasterLocationId
                                         where DMSite.SyncCode == backGroundInfo.SiteCodeSyncCode
                                       select DMStore.DigiMasterLocationId).FirstOrDefault();
                        if (existingBackground == 0)
                        {
                            //var subStore = dbContext.DigiMasterLocations.Where(l => l.SyncCode == ChangeInfo.SubStore.SiteCode && l.Level == 4).FirstOrDefault();
                            var backGround = new Background();
                            backGround.DisplayName = backGroundInfo.DisplayName;
                            backGround.ProductId = backGroundInfo.ProductId;
                            backGround.BackgroundImage = backGroundInfo.BackgroundImage;
                            backGround.BackgroundGroupId = backGroundInfo.BackgroundGroupId;
                            backGround.CreatedBy = 1;
                            backGround.CreatedDateTime = backGroundInfo.ModifiedDate;
                            backGround.IsActive = backGroundInfo.IsActive;
                            backGround.SyncCode = backGroundInfo.SyncCode;
                            backGround.IsSynced = true;
                            dbContext.Backgrounds.Add(backGround);
                            dbContext.SaveChanges();
                            Int64 backGroundId = backGround.BackgoundId;

                            ObjectAuthorization obj = new ObjectAuthorization();
                            obj.ApplicationObjectId = 2;
                            obj.ObjectValueId = backGroundId;
                            obj.IsDeleted = 0;
                            obj.DigiMasterLocationId =Convert.ToInt64(storeId);
                            dbContext.ObjectAuthorizations.Add(obj);
                            dbContext.SaveChanges();
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                log.Error("BackgroundProcessor:ProcessInsert:: Message: " + ex.Message + " StackTrace: " + ex.StackTrace + " InnerException: " + ex.InnerException + " Source: " + ex.Source);
                Exception outerException = new ProcessingException("Error occured in Background Processor Insert.", ex);
                outerException.Source = "Business.Processing.BackgroundProcessor";
                throw outerException;
            }
        }
        protected override void ProcessUpdate()
        {
            try
            {
                if (!string.IsNullOrEmpty(ChangeInfo.DataXML))
                {
                    BackgroundInfo backGroundInfo = CommonUtility.DeserializeXML<BackgroundInfo>(ChangeInfo.DataXML);
                    using (DigiPhotoWebContext dbContext = new DigiPhotoWebContext())
                    {
                        var dbBackground = dbContext.Backgrounds.Where(r => r.SyncCode == backGroundInfo.SyncCode).FirstOrDefault();
                        if (dbBackground != null)
                        {
                            dbBackground.DisplayName = backGroundInfo.DisplayName;
                            dbBackground.ProductId = backGroundInfo.ProductId;
                            dbBackground.BackgroundImage = backGroundInfo.BackgroundImage;
                            dbBackground.BackgroundGroupId = backGroundInfo.BackgroundGroupId;
                            dbBackground.ModifiedBy = 1;
                            dbBackground.ModifiedDateTime = backGroundInfo.ModifiedDate;
                            dbBackground.IsActive = backGroundInfo.IsActive;
                            dbBackground.SyncCode = backGroundInfo.SyncCode;
                            dbBackground.IsSynced = true;
                            dbContext.SaveChanges();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("BackgroundProcessor:ProcessUpdate:: Message: " + ex.Message + " StackTrace: " + ex.StackTrace + " InnerException: " + ex.InnerException + " Source: " + ex.Source);
                Exception outerException = new ProcessingException("Error occured in Background Processor Update.", ex);
                outerException.Source = "Business.Processing.BackgroundProcessor";
                throw outerException;
            }
        }
        protected override void ProcessDelete()
        {
            try
            {

                using (DigiPhotoWebContext dbContext = new DigiPhotoWebContext())
                {
                    var dbBackGround = dbContext.Backgrounds.Where(r => r.SyncCode == ChangeInfo.EntityCode).FirstOrDefault();
                    if (dbBackGround != null)
                    {
                        dbContext.Backgrounds.Remove(dbBackGround);
                        dbContext.SaveChanges(); //If multiple remove operations are done, commit will be at last.
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("BackgroundProcessor:ProcessDelete:: Message: " + ex.Message + " StackTrace: " + ex.StackTrace + " InnerException: " + ex.InnerException + " Source: " + ex.Source);
                Exception outerException = new ProcessingException("Error occured in Background Processor  Delete.", ex);
                outerException.Source = "Business.Processing.BackgroundProcessor";
                throw outerException;
            }
        }
    }
}
