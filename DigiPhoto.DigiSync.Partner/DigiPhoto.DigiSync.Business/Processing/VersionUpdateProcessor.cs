﻿using DigiPhoto.DigiSync.DataAccess.DataModels;
using DigiPhoto.DigiSync.Model;
using DigiPhoto.DigiSync.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using log4net;

namespace DigiPhoto.DigiSync.Business.Processing
{
    public class VersionUpdateProcessor : BaseProcessor
    {
        private static ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        protected override void ProcessInsert()
        {
            try
            {
                if (!string.IsNullOrEmpty(ChangeInfo.DataXML))
                {
                    ApplicationClientVersionInfo clientVersionInfo = CommonUtility.DeserializeXML<ApplicationClientVersionInfo>(ChangeInfo.DataXML);
                    using (DigiPhotoWebContext dbContext = new DigiPhotoWebContext())
                    {
                        var IsAlreadySynced = dbContext.ApplicationClientVersions.Count(r => r.SyncCode == clientVersionInfo.SyncCode);
                        //var user = dbContext.Users.Where(r => r.SyncCode == clientVersionInfo.UserSyncCode).FirstOrDefault();
                        var digilocation = dbContext.DigiMasterLocations.Where(r => r.Name == ChangeInfo.StoreName && r.Level == 3).FirstOrDefault();
                        if (IsAlreadySynced == 0)
                        {
                            var clientVersion = new ApplicationClientVersion();

                            clientVersion.ApplicationVersionID = clientVersionInfo.ApplicationVersionID;
                            clientVersion.MACAddress = clientVersionInfo.MACAddress;
                            clientVersion.MachineName = clientVersionInfo.MachineName;
                            clientVersion.IPAddress = clientVersionInfo.IPAddress;
                            clientVersion.Country = clientVersionInfo.Country;
                            clientVersion.Store = clientVersionInfo.Store;
                            clientVersion.SubStore = clientVersionInfo.SubStore;
                            clientVersion.Status = clientVersionInfo.Status;
                            clientVersion.SyncCode = clientVersionInfo.SyncCode;
                            clientVersion.UpdatedDate = clientVersionInfo.UpdatedDate;
                            dbContext.ApplicationClientVersions.Add(clientVersion);
                            dbContext.SaveChanges();

                            int versionId = (int)clientVersion.ApplicationClientVersionID;
                            if (versionId > 0)
                            {

                                var objectauth = dbContext.ObjectAuthorizations.Where(r => r.ObjectValueId == versionId && r.DigiMasterLocationId == digilocation.DigiMasterLocationId && r.ApplicationObjectId == ChangeInfo.ApplicationObjectId).FirstOrDefault();

                                if (objectauth != null)
                                {
                                    dbContext.ObjectAuthorizations.Remove(objectauth);
                                    dbContext.SaveChanges();

                                }
                                ObjectAuthorization obj = new ObjectAuthorization();
                                obj.ApplicationObjectId = ChangeInfo.ApplicationObjectId;
                                obj.ObjectValueId = versionId;
                                obj.DigiMasterLocationId = digilocation.DigiMasterLocationId;
                                dbContext.ObjectAuthorizations.Add(obj);
                                dbContext.SaveChanges();

                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("CurrencyProcessor:ProcessInsert:: Message: " + ex.Message + " StackTrace: " + ex.StackTrace + " InnerException: " + ex.InnerException + " Source: " + ex.Source);
                Exception outerException = new ProcessingException("Error occured in Currencies Processor Insert.", ex);
                outerException.Source = "Business.Processing.CurrencyProcessor";
                throw outerException;
            }
        }
        protected override void ProcessUpdate()
        {
            throw new Exception("Not emplemented");
        }

        protected override void ProcessDelete()
        {
            throw new Exception("Not emplemented");
        }
    }
}
