﻿using DigiPhoto.DigiSync.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using log4net;

namespace DigiPhoto.DigiSync.Business.Processing
{
    public class ProcessorFactory
    {
        private static ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private static Dictionary<long, IProcesser> processors = new Dictionary<long, IProcesser>();
        public static IProcesser Create(long applicationObjectId)
        {
            try
            {
                object objLock = new object();
                lock (objLock)
                {
                    ApplicationObjectEnum applicationObject = (ApplicationObjectEnum)applicationObjectId;
                    string applicationObjectName = applicationObject.ToString();
                    string className = applicationObjectName + "Processor";
                    Type type = Type.GetType("DigiPhoto.DigiSync.Business.Processing." + className);
                    processors[applicationObjectId] = Activator.CreateInstance(type) as IProcesser;
                    return processors[applicationObjectId];
                }
            }
            catch (Exception ex)
            {
                log.Error("ProductProcessor:ProcessDelete:: Message: " + ex.Message + " StackTrace: " + ex.StackTrace + " InnerException: " + ex.InnerException + " Source: " + ex.Source);
                throw ex;
            }
        }
    }
}
