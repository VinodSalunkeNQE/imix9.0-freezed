﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DigiPhoto.DigiSync.Business.Serialization
{
    public abstract class Serializer
    {
        public abstract string Serialize(long ObjectValueId);

        public abstract object Deserialize(string input);
    }
}
