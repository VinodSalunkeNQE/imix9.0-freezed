﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DigiPhoto.DigiSync.DataAccess.DataModels;
using DigiPhoto.DigiSync.Model;
using DigiPhoto.DigiSync.Utility;
namespace DigiPhoto.DigiSync.Business.Serialization
{
    class CameraSerializer:Serializer
    {
        public override string Serialize(long ObjectValueId)
        {
            try
            {
                using (DigiPhotoWebContext dbContext = new DigiPhotoWebContext())
                {
                    string dataXML = string.Empty;
                    var camera = dbContext.Cameras.Where(r => r.CameraId == ObjectValueId).FirstOrDefault();
                    if (camera != null)
                    {
                        CameraInfo cameraInfo = new CameraInfo()
                        {
                            CameraId = camera.CameraId,
                            Name = camera.Name,
                            Make = camera.Make,
                            Model = camera.Model,
                            IsActive = camera.IsActive,
                            ModifiedBy = camera.ModifiedBy,
                            ModifiedDateTime = camera.ModifiedDateTime,
                            SyncCode = camera.SyncCode ,
                            
                        };

                        dataXML = CommonUtility.SerializeObject<CameraInfo>(cameraInfo);
                    }
                    return dataXML;
                }
            }
            catch (Exception e)
            {
                Exception outerException = new SerializationException("Error occured in Camera Serialization.", e);
                outerException.Source = "Business.Serialization.CameraSerializer";
                throw outerException;
            }
        }
        public override object Deserialize(string input)
        {
            throw new Exception("Not emplemented");
        }
    }
}
