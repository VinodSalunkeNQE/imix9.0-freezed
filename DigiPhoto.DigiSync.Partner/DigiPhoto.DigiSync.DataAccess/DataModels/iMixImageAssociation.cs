﻿
namespace DigiPhoto.DigiSync.DataAccess.DataModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("iMixImageAssociation")]
    public partial class iMixImageAssociation
    {
        [Key]
        public Int64 IMIXImageAssociationId { get; set; }
        public int IMIXCardTypeId { get; set; }
        public int PhotoId { get; set; }
        public string CardUniqueIdentifier { get; set; }
        public string MappedIdentifier { get; set; }
        public DateTime ModifiedDate { get; set; }
        public int IsOrdered { get; set; }
        public int RfidIdentifierId { get; set; }
        public int IsMoved { get; set; }
        public int Nationality { get; set; }
        public string Email { get; set; }
    }
}
