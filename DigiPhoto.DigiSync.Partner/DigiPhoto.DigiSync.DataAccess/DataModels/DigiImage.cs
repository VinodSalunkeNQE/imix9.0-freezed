namespace DigiPhoto.DigiSync.DataAccess.DataModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class DigiImage
    {
        //public DigiImage()
        //{
        //    OrderDetails = new HashSet<OrderDetail>();
        //}

       [Key]
        public long DigiImagesId { get; set; }

        [Required]
        [StringLength(150)]
        public string FileName { get; set; }

        [Required]
        [StringLength(150)]
        public string PhotoRFID { get; set; }

        [StringLength(150)]
        public string Frame { get; set; }

        [StringLength(150)]
        public string Background { get; set; }

        [Column(TypeName = "xml")]
        public string Layering { get; set; }

        [Column(TypeName = "xml")]
        public string Effects { get; set; }

        public bool IsCroped { get; set; }

        public bool IsGreen { get; set; }

        [Column(TypeName = "xml")]
        public string MetaData { get; set; }

        public string Size { get; set; }

        public bool? IsArchived { get; set; }

        public long LocationId { get; set; }

        public long SubStoreId { get; set; }

        public long CreatedBy { get; set; }

        public DateTime CreatedDateTime { get; set; }

     // public virtual DigiMasterLocation DigiMasterLocation { get; set; }

     //  public virtual DigiMasterLocation DigiMasterLocation1 { get; set; }

        public virtual User User { get; set; }

        //public virtual ICollection<OrderDetail> OrderDetails { get; set; }
    }
}
