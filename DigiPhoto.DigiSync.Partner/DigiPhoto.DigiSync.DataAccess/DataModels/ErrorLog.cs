namespace DigiPhoto.DigiSync.DataAccess.DataModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ErrorLog")]
    public partial class ErrorLog
    {
        [Key]
       // [Column(Order = 0)]
        public int ErrorLogID { get; set; }

        //[Key]
        //[Column(Order = 1)]
        public DateTime ErrorTime { get; set; }

        //[Key]
        //[Column(Order = 2)]
        public string UserName { get; set; }

        //[Key]
        ////[Column(Order = 3)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int ErrorNumber { get; set; }

        public int? ErrorSeverity { get; set; }

        public int? ErrorState { get; set; }

        [StringLength(126)]
        public string ErrorProcedure { get; set; }

        public int? ErrorLine { get; set; }

        //[Key]
        //[Column(Order = 4)]
        //[StringLength(4000)]
        public string ErrorMessage { get; set; }
    }
}
