namespace DigiPhoto.DigiSync.DataAccess.DataModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("SubStore")]
    public partial class SubStore
    {
        public SubStore()
        {
            CustomerQRCodeDetails = new HashSet<CustomerQRCodeDetail>();
            Photos = new HashSet<Photo>();
        }

        public long SubStoreId { get; set; }

        [Required]
        [StringLength(50)]
        public string SubStoreCode { get; set; }

        //[Column("SubStore")]
        //[Required]
        //[StringLength(50)]
        //public string SubStore1 { get; set; }

        [Required]
        [StringLength(50)]
        public string Store { get; set; }

        public DateTime CreatedDate { get; set; }

        public long CreatedBy { get; set; }

        public bool Active { get; set; }

        public virtual ICollection<CustomerQRCodeDetail> CustomerQRCodeDetails { get; set; }

        public virtual ICollection<Photo> Photos { get; set; }

        public string SyncCode { get; set; }
    }
}
