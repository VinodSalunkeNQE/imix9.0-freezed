namespace DigiPhoto.DigiSync.DataAccess.DataModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("iMixImageCardType")]
    public partial class iMixImageCardType
    {
        public int IMIXImageCardTypeId { get; set; }

        [Required]
        [StringLength(50)]
        public string Name { get; set; }

        [Required]
        [StringLength(50)]
        public string CardIdentificationDigit { get; set; }

        public int ImageIdentificationType { get; set; }

        public bool? IsActive { get; set; }

        public int? MaxImages { get; set; }

        [StringLength(500)]
        public string Description { get; set; }

        public DateTime CreatedDate { get; set; }
    }
}
