﻿namespace DigiPhoto.DigiSync.DataAccess.DataModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("TransDetails", Schema = "Sage")]
    public partial class TransDetails
    {
        [Key]
        public long TransDetailID { get; set; }

        [Column("SiteID")]
        public Int64 SubstoreID { get; set; }

        public DateTime TransDate { get; set; }

        public Int32 PackageID { get; set; }

        //public string PackageSyncCode { get; set; }

        public decimal UnitPrice { get; set; }

        public decimal Quantity { get; set; }

        public decimal Discount { get; set; }

        public decimal Total { get; set; }

        public long ClosingFormDetailID { get; set; }
    }
}
