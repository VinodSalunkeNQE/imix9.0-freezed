namespace DigiPhoto.DigiSync.DataAccess.DataModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("SyncRegistration")]
    public partial class SyncRegistration
    {
        [Key]
        public long SyncRegisterationId { get; set; }

        [StringLength(50)]
        public string ClientVersion { get; set; }

        public DateTime RequestDate { get; set; }

        [StringLength(50)]
        public string CountryName { get; set; }

        [StringLength(50)]
        public string StoreName { get; set; }

        [StringLength(50)]
        public string SubStoreName { get; set; }

        [StringLength(50)]
        public string CountryCode { get; set; }

        [StringLength(50)]
        public string StoreCode { get; set; }

        [StringLength(50)]
        public string SubStoreCode { get; set; }
    }
}
