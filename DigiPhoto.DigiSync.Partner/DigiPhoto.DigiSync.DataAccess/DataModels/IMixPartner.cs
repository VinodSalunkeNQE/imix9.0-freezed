﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;

namespace DigiPhoto.DigiSync.DataAccess.DataModels
{
    [Table("IMixPartner")]
    public partial class IMixPartner
    {
        [Key]
        public int PartnerId { get; set; }

        public string Name { get; set; }

        public bool IsActive { get; set; }
    }
}
