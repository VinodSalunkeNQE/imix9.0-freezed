namespace DigiPhoto.DigiSync.DataAccess.DataModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("MessageQueue")]
    public partial class MessageQueue
    {
        public MessageQueue()
        {
            MessageAttributeValues = new HashSet<MessageAttributeValue>();
        }

        [Key]
        public long MessageId { get; set; }

        public long ProfileId { get; set; }

        public bool IsSent { get; set; }

        public DateTime? CreatedDateTime { get; set; }

        public DateTime? SentDateTime { get; set; }

        public virtual ICollection<MessageAttributeValue> MessageAttributeValues { get; set; }

        public virtual MessageProfile MessageProfile { get; set; }
    }
}
