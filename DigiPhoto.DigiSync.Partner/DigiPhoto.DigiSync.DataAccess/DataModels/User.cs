namespace DigiPhoto.DigiSync.DataAccess.DataModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("User")]
    public partial class User
    {
        public User()
        {
            Borders = new HashSet<Border>();
            Borders1 = new HashSet<Border>();
            Currencies = new HashSet<Currency>();
            DigiConfigurations = new HashSet<DigiConfiguration>();
            DigiConfigurations1 = new HashSet<DigiConfiguration>();
            DigiImages = new HashSet<DigiImage>();
            Products = new HashSet<Product>();
            Products1 = new HashSet<Product>();
            ProductRefunds = new HashSet<ProductRefund>();
            UserActivityLogs = new HashSet<UserActivityLog>();
            DigiMasterLocations = new HashSet<DigiMasterLocation>();
            Roles = new HashSet<Role>();
        }

        public long UserId { get; set; }

        public long UserDetailId { get; set; }

        [Required]
        [StringLength(50)]
        public string UserName { get; set; }

        [Required]
        [StringLength(50)]
        public string PasswordHash { get; set; }

        public long UserTypeCodeId { get; set; }

        public bool ?IsActive { get; set; }
      
        public DateTime ?CreatedDateTime { get; set; }

        public DateTime? ModifiedDateTime { get; set; }

        public long? CreatedBy { get; set; }

        public long? ModifiedBy { get; set; }

        [StringLength(50)]
        public string SyncCode { get; set; }
        public bool IsSynced { get; set; }
        public bool IsImixUser { get; set; }
        public Nullable<long> StoreId { get; set; }
        public virtual ICollection<Border> Borders { get; set; }

        public virtual ICollection<Border> Borders1 { get; set; }

        public virtual Code Code { get; set; }

        public virtual ICollection<Currency> Currencies { get; set; }

        public virtual ICollection<DigiConfiguration> DigiConfigurations { get; set; }

        public virtual ICollection<DigiConfiguration> DigiConfigurations1 { get; set; }

        public virtual ICollection<DigiImage> DigiImages { get; set; }

        public virtual ICollection<Product> Products { get; set; }

        public virtual ICollection<Product> Products1 { get; set; }

        public virtual ICollection<ProductRefund> ProductRefunds { get; set; }

        public virtual ICollection<UserActivityLog> UserActivityLogs { get; set; }

        public virtual UserDetail UserDetail { get; set; }

        public virtual ICollection<DigiMasterLocation> DigiMasterLocations { get; set; }

        public virtual ICollection<Role> Roles { get; set; }
    }
}
