namespace DigiPhoto.DigiSync.DataAccess.DataModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("MessageAttribute")]
    public partial class MessageAttribute
    {
        public MessageAttribute()
        {
            MessageAttributeValues = new HashSet<MessageAttributeValue>();
        }

        [Key]
        public long AttributeId { get; set; }

        [StringLength(100)]
        public string Name { get; set; }

        [StringLength(200)]
        public string Description { get; set; }

        public bool IsActive { get; set; }

        public virtual ICollection<MessageAttributeValue> MessageAttributeValues { get; set; }
    }
}
