﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using DigiPhoto.DigiSync.Model;
using DigiPhoto.DigiSync.ClientDataProcessor;
using DigiPhoto.DigiSync.ServiceLibrary.Interface;
using log4net;
using DigiPhoto.DigiSync.Utility;
using System.IO;
using System.Drawing;
using DigiPhoto.DigiSync.ClientDataProcessor.Controller;
using DigiPhoto.DigiSync.ClientDataAccess.DataModels;
using System.Configuration;

namespace DigiSyncApp
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        bool IsSyncServerOnline = false;
        private static ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private static bool IsPartialEdited = false;
        private static List<iMIXconfigurationLocationValue> iMIXconfigurationLocationWiseValue;
        public MainWindow()
        {
            log4net.Config.XmlConfigurator.Configure();
            InitializeComponent();
        }
        //public static Guid currentSessionId;
        private void btnSyncRole_Click(object sender, RoutedEventArgs e)
        {

            SyncController.UpdateSyncStatus(0, (int)SyncStatus.Error);
            CheckServerIsOnline();
            bool IsSyncEnabled = SyncController.IsSynceEnabled();
            int SyncServiceInterval = SyncController.SyncServiceInterval();
            int SyncDataDelay = SyncController.SyncDataDelay();
            iMIXconfigurationLocationWiseValue = SyncController.PartialEditedEnabled();

            SyncController.FailedOrderConfigValue();
            SyncController.SetSyncStatusZero();


            //SyncController.FailedOrderConfigValue();
            //SyncController.SetSyncStatusZero();

            //iMIXconfigurationLocationWiseValue = SyncController.PartialEditedEnabled();


            //bool IsSyncEnabled = SyncController.IsSynceEnabled();
            //int SyncServiceInterval = SyncController.SyncServiceInterval();

            //int SyncDataDelay = SyncController.SyncDataDelay();

            ////CheckServerIsOnline();
            //SyncController.UpdateSyncStatus(0, (int)SyncStatus.Error);
            //CheckServerIsOnline();
            //IsSyncEnabled = SyncController.IsSynceEnabled();
            //SyncServiceInterval = SyncController.SyncServiceInterval();
            //SyncDataDelay = SyncController.SyncDataDelay();
            //iMIXconfigurationLocationWiseValue = SyncController.PartialEditedEnabled();


            if (!IsSyncEnabled)
            {
                log.Info("Sync is not Enabled !");
            }
            CheckServerIsOnline();
            if (IsSyncEnabled && IsSyncServerOnline)
            {
                //Pull();
                Push(SyncDataDelay);
                //GetOnlineOrdersStatus();
            }

            //if (IsSyncEnabled && IsSyncServerOnline)
            //{
            //    Push(SyncDataDelay);
            //   // Pull();
            //    MessageBox.Show("Data sync successfully !");
            //}
            else
            {
                MessageBox.Show("Sync not Enabled !");
            }
            #region Obsolete



            /*
                        //TBD
                        SyncRequestInfo requestInfo = new SyncRequestInfo();
                        requestInfo.Venue = new VenueInfo()
                                                            {
                                                                VenueCode = "Dubai Mall",
                                                                VenueName = "Dubai Mall",
                                                                Site = new SiteInfo()
                                                                {
                                                                    SiteCode = "Aquarium",
                                                                    SiteName = "Aquarium",
                                                                    Store = new StoreInfo()
                                                                    {
                                                                        StoreCode = "Aquarium",
                                                                        StoreName = "Aquarium"
                                                                    }
                                                                }
                                                            };

                        ChangeInfo lastDBChange = SyncController.GetLastChangeInfo();
                        if (lastDBChange.ChangeTrackingId == 0)
                        {
                            requestInfo.StartChangeTrackingId = 0;
                            requestInfo.LastSyncOnDate = null;    //first sync request.
                        }
                        else
                        {
                            requestInfo.StartChangeTrackingId = lastDBChange.ChangeTrackingId;
                            requestInfo.LastSyncOnDate = lastDBChange.ChangeDate;
                        }

                        SyncRequestHandler requestHandler = new SyncRequestHandler();
                        SyncResponseInfo syncResponse = requestHandler.SignIn(requestInfo);
                        if (syncResponse != null)
                        {
                            currentSessionId = syncResponse.SessionTokenId;
                            //Successfully recieved sync response, create sync history on Client machine.
                            SyncSessionInfo syncSession = new SyncSessionInfo
                            {
                                SessionTokenId = currentSessionId,
                                ClientStoreCode = requestInfo.Venue.Site.Store.StoreCode,
                                ClientSiteCode = requestInfo.Venue.Site.SiteCode,
                                ClientVenueCode = requestInfo.Venue.VenueCode,
                                SignInDate = DateTime.Now,
                                Status = 1,   //TBD,
                                StartChangeTrackingId = requestInfo.StartChangeTrackingId
                            };
                            long syncSessionId = SyncController.SaveSyncHistory(syncSession);
                            syncSession.SyncSessionId = syncSessionId;

                            ChangeInfo lastChangeInfo = null;
                            ChangeInfo changeInfo = null;
                            do
                            {
                                if (changeInfo != null)
                                {
                                    lastChangeInfo = changeInfo;
                                }
                                else
                                {
                                    //Query last change info from Database for first request;
                                    lastChangeInfo = lastDBChange;
                                }
                                //Get the change greater then current change in current session
                                changeInfo = requestHandler.GetNextChangeInfo(currentSessionId, lastChangeInfo);
                                if (changeInfo.ChangeTrackingId > lastChangeInfo.ChangeTrackingId)
                                {
                                    //This change will be applied/processed under current session.
                                    changeInfo.sessionId = syncSessionId;
                                    //Process
                                    IProcesser processor = ProcessorFactory.Create(changeInfo.ApplicationObjectId);
                                    processor.Process(changeInfo);
                                }
                            } while (changeInfo.ChangeTrackingId > lastChangeInfo.ChangeTrackingId);

                            //Close session on Server
                            requestHandler.SignOut(syncResponse.SessionTokenId, changeInfo.ChangeTrackingId);
                            //Update this sync session in Client DB
                            syncSession.SignOutDate = DateTime.Now;
                            syncSession.Status = 3;       //Closed //TBD
                            syncSession.EndChangeTrackingId = changeInfo.ChangeTrackingId;
                            SyncController.UpdateSyncStatus(syncSession);
                            MessageBox.Show("Sync completed successfully.");
                        }
                        else
                        {
                            throw new Exception("Unable to open sync session on Server");
                        }
                    }

                    */
            //private void btnSyncProduct_Click(object sender, RoutedEventArgs e)
            //{
            //    //TBD
            //    SyncRequestInfo requestInfo = new SyncRequestInfo();
            //    requestInfo.Venue = new VenueInfo()
            //                                        {
            //                                            VenueCode = "Dubai Mall",
            //                                            VenueName = "Dubai Mall",
            //                                            Site = new SiteInfo()
            //                                            {
            //                                                SiteCode = "Aquarium",
            //                                                SiteName = "Aquarium",
            //                                                Store = new StoreInfo()
            //                                                {
            //                                                    StoreCode = "Aquarium",
            //                                                    StoreName = "Aquarium"
            //                                                }
            //                                            }
            //                                        };

            //    ChangeInfo lastDBChange = SyncController.GetLastChangeInfo();
            //    if (lastDBChange.ChangeTrackingId == 0)
            //    {
            //        requestInfo.StartChangeTrackingId = 0;
            //        requestInfo.LastSyncOnDate = null;    //first sync request.
            //    }
            //    else
            //    {
            //        requestInfo.StartChangeTrackingId = lastDBChange.ChangeTrackingId;
            //        requestInfo.LastSyncOnDate = lastDBChange.ChangeDate; 
            //    }

            //    SyncRequestHandler requestHandler = new SyncRequestHandler();
            //    SyncResponseInfo syncResponse = requestHandler.SignIn(requestInfo);
            //    if (syncResponse != null)
            //    {
            //        currentSessionId = syncResponse.SessionTokenId;
            //        //Successfully recieved sync response, create sync history on Client machine.
            //        SyncSessionInfo syncSession = new SyncSessionInfo
            //        {
            //            SessionTokenId = currentSessionId,
            //            ClientStoreCode = requestInfo.Venue.Site.Store.StoreCode,
            //            ClientSiteCode = requestInfo.Venue.Site.SiteCode,
            //            ClientVenueCode = requestInfo.Venue.VenueCode,
            //            SignInDate = DateTime.Now,
            //            Status = 1,   //TBD,
            //            StartChangeTrackingId = requestInfo.StartChangeTrackingId
            //        };
            //        long syncSessionId = SyncController.SaveSyncHistory(syncSession);
            //        syncSession.SyncSessionId = syncSessionId;

            //        ChangeInfo lastChangeInfo = null;
            //        ChangeInfo changeInfo = null;
            //        do
            //        {
            //            if (changeInfo != null)
            //            {
            //                lastChangeInfo = changeInfo;
            //            }
            //            else
            //            {
            //                //Query last change info from Database for first request;
            //                lastChangeInfo = lastDBChange;
            //            }
            //            //Get the change greater then current change in current session
            //            changeInfo = requestHandler.GetNextChangeInfo(currentSessionId, lastChangeInfo);
            //            if (changeInfo.ChangeTrackingId > lastChangeInfo.ChangeTrackingId)
            //            {
            //                //This change will be applied/processed under current session.
            //                changeInfo.sessionId = syncSessionId;
            //                //Process
            //                IProcesser processor = ProcessorFactory.Create(changeInfo.ApplicationObjectId);
            //                processor.Process(changeInfo);
            //            }
            //        } while (changeInfo.ChangeTrackingId > lastChangeInfo.ChangeTrackingId);

            //        //Close session on Server
            //        requestHandler.SignOut(syncResponse.SessionTokenId, changeInfo.ChangeTrackingId);
            //        //Update this sync session in Client DB
            //        syncSession.SignOutDate = DateTime.Now;
            //        syncSession.Status = 3;       //Closed //TBD
            //        syncSession.EndChangeTrackingId = changeInfo.ChangeTrackingId;
            //        SyncController.UpdateSyncStatus(syncSession);
            //        MessageBox.Show("Sync completed successfully.");
            //    }
            //    else
            //    {
            //        throw new Exception("Unable to open sync session on Server");
            //    }
            //}
            //private void btnSyncDiscount_Click(object sender, RoutedEventArgs e)
            //{
            //    //TBD
            //    SyncRequestInfo requestInfo = new SyncRequestInfo();
            //    requestInfo.Venue = new VenueInfo()
            //    {
            //        VenueCode = "Dubai Mall",
            //        VenueName = "Dubai Mall",
            //        Site = new SiteInfo()
            //        {
            //            SiteCode = "Aquarium",
            //            SiteName = "Aquarium",
            //            Store = new StoreInfo()
            //            {
            //                StoreCode = "Aquarium",
            //                StoreName = "Aquarium"
            //            }
            //        }
            //    };

            //    ChangeInfo lastDBChange = SyncController.GetLastChangeInfo();
            //    if (lastDBChange.ChangeTrackingId == 0)
            //    {
            //        requestInfo.StartChangeTrackingId = 0;
            //        requestInfo.LastSyncOnDate = null;    //first sync request.
            //    }
            //    else
            //    {
            //        requestInfo.StartChangeTrackingId = lastDBChange.ChangeTrackingId;
            //        requestInfo.LastSyncOnDate = lastDBChange.ChangeDate;
            //    }

            //    SyncRequestHandler requestHandler = new SyncRequestHandler();
            //    SyncResponseInfo syncResponse = requestHandler.SignIn(requestInfo);
            //    if (syncResponse != null)
            //    {
            //        currentSessionId = syncResponse.SessionTokenId;
            //        //Successfully recieved sync response, create sync history on Client machine.
            //        SyncSessionInfo syncSession = new SyncSessionInfo
            //        {
            //            SessionTokenId = currentSessionId,
            //            ClientStoreCode = requestInfo.Venue.Site.Store.StoreCode,
            //            ClientSiteCode = requestInfo.Venue.Site.SiteCode,
            //            ClientVenueCode = requestInfo.Venue.VenueCode,
            //            SignInDate = DateTime.Now,
            //            Status = 1,   //TBD,
            //            StartChangeTrackingId = requestInfo.StartChangeTrackingId
            //        };
            //        long syncSessionId = SyncController.SaveSyncHistory(syncSession);
            //        syncSession.SyncSessionId = syncSessionId;

            //        ChangeInfo lastChangeInfo = null;
            //        ChangeInfo changeInfo = null;
            //        do
            //        {
            //            if (changeInfo != null)
            //            {
            //                lastChangeInfo = changeInfo;
            //            }
            //            else
            //            {
            //                //Query last change info from Database for first request;
            //                lastChangeInfo = lastDBChange;
            //            }
            //            //Get the change greater then current change in current session
            //            changeInfo = requestHandler.GetNextChangeInfo(currentSessionId, lastChangeInfo);
            //            if (changeInfo.ChangeTrackingId > lastChangeInfo.ChangeTrackingId)
            //            {
            //                //This change will be applied/processed under current session.
            //                changeInfo.sessionId = syncSessionId;
            //                //Process
            //                IProcesser processor = ProcessorFactory.Create(changeInfo.ApplicationObjectId);
            //                processor.Process(changeInfo);
            //            }
            //        } while (changeInfo.ChangeTrackingId > lastChangeInfo.ChangeTrackingId);

            //        //Close session on Server
            //        requestHandler.SignOut(syncResponse.SessionTokenId, changeInfo.ChangeTrackingId);
            //        //Update this sync session in Client DB
            //        syncSession.SignOutDate = DateTime.Now;
            //        syncSession.Status = 3;       //Closed //TBD
            //        syncSession.EndChangeTrackingId = changeInfo.ChangeTrackingId;
            //        SyncController.UpdateSyncStatus(syncSession);
            //        MessageBox.Show("Sync completed successfully.");
            //    }
            //    else
            //    {
            //        throw new Exception("Unable to open sync session on Server");
            //    }

            #endregion
        }
        void CheckServerIsOnline()
        {
            IsSyncServerOnline = false;
            string serverdate = string.Empty;
            try
            {
                DServiceProxy<IDataSyncService>.Use(client =>
                {
                    serverdate = client.GetServerDate();
                });
            }
            catch (Exception ex)
            {
                serverdate = string.Empty;
                log.Error("CheckServerIsOnline:: Message: " + ex.Message + " StackTrace: " + ex.StackTrace + " InnerException: " + ex.InnerException + " Source: " + ex.Source);

            }
            if (!String.IsNullOrEmpty(serverdate))
            {
                IsSyncServerOnline = true;
            }

        }

        private void GetOnlineOrdersStatus()
        {
            try
            {
                List<QrCodeStatus> QrCodeList = new List<QrCodeStatus>();

                QrCodeList = SyncController.GetPendingOnlineOrders(10);
                log.Info("QR code status started");
                foreach (QrCodeStatus item in QrCodeList)
                {
                    log.Info("Started for QR code:" + item.IdentificationCode);
                    OnlineOrderStatus qrCodeInfo = new OnlineOrderStatus();
                    DServiceProxy<IDataSyncService>.Use(client =>
                    {
                        qrCodeInfo = client.GetOnlineOrderStatus(item);
                    });

                    using (DigiPhotoContext dbContext = new DigiPhotoContext())
                    {
                        var changeTrack = (from DOD in dbContext.DG_Orders_Details
                                           join DO in dbContext.DG_Orders
                                           on DOD.DG_Orders_ID equals DO.DG_Orders_pkey
                                           join QCT in dbContext.ChangeTrackings
                                           on DO.DG_Orders_pkey equals QCT.ObjectValueId
                                           where
                                           QCT.ApplicationObjectId == 14
                                           && DOD.DG_Order_ImageUniqueIdentifier == item.IdentificationCode
                                           && (QCT.SyncStatus == 3 || QCT.SyncStatus == 11 || QCT.SyncStatus == 12 || QCT.SyncStatus == 13 || QCT.SyncStatus == 14)
                                           orderby QCT.ChangeTrackingId descending
                                           select QCT).FirstOrDefault();

                        changeTrack.SyncStatus = qrCodeInfo.OnlineOrderIMSStatus;

                        dbContext.SaveChanges();

                    }

                    log.Info("Completed for QR code:" + item.IdentificationCode + "=" + qrCodeInfo.OnlineOrderIMSStatus);
                }

            }
            catch (Exception ex)
            {

                log.Error("Error:GetOnlineOrdersStatus-" + ex.Message);
            }


        }


        private void Push(int SyncDataDelay)
        {
            string errorPhotoId = string.Empty;
            bool flag = true;
            bool SuccessFlag = true;
            if (log.IsInfoEnabled)
                log.Info("Push Start");
            List<ChangeInfo> changeList = null;
            try
            {
                List<SiteInfo> sites = SyncController.GetSites();
                foreach (var site in sites)
                {
                    if (log.IsInfoEnabled)
                        log.Info("SubStore Name/Code:" + site.SiteName + "/" + site.SiteCode);
                    //Get the ChangeInfo from database.
                    changeList = SyncController.GetNextChangeInfo(SyncDataDelay);



                    if (changeList == null)
                    {
                        if (log.IsInfoEnabled)
                            log.Info("Change Tracking Not Found");
                    }
                    if (changeList != null && changeList.Count > 0)
                    {
                        foreach (ChangeInfo change in changeList)
                        {
                            //check if order is cancel -- Clear Upload LIST (do not load photos to cloudinary)-- ASHIRWAD
                            SuccessFlag = true;//important
                            flag = true;
                            bool? IsOrderCancel = false;
                            IsOrderCancel = SyncController.GetOrderCancelStatus(change.ObjectValueId);
                            if (IsOrderCancel != null && IsOrderCancel == true)
                            {
                                change.UploadList = null;
                            }

                            try
                            {
                                if (log.IsInfoEnabled)
                                    log.Info("Change Tracking Id:" + change.ChangeTrackingId);
                                change.SubStore = new SiteInfo
                                {
                                    SiteCode = site.SiteCode,
                                    SiteName = site.SiteName,
                                    Store = new StoreInfo
                                    {
                                        StoreCode = site.Store.StoreCode,
                                        StoreName = site.Store.StoreName
                                    }
                                };

                                if (!string.IsNullOrEmpty(change.dataXML))
                                {
                                    if (log.IsInfoEnabled)
                                        log.Info("Sync Started for ChangeTrackingId : " + change.ChangeTrackingId);
                                    bool success = false;
                                    try
                                    {
                                        SyncController.UpdateSyncStatus(change.ChangeTrackingId, (int)SyncStatus.InProgressSavingIncomingChangeUploadedDataOn);
                                        DServiceProxy<IDataSyncService>.Use(client =>
                                        {
                                            success = client.SaveChangeInfo(change);
                                        });

                                        log.Info("Data saved in IncomingChange : " + success);

                                    }
                                    catch(Exception ex)
                                    {
                                        log.Error("Execption while saving Data in IncomingChange : ");
                                        SyncController.UpdateSyncStatus(change.ChangeTrackingId, (int)SyncStatus.ErrorSavingIncomingChange);
                                        SuccessFlag = false;
                                        continue;
                                    }
                                    if (success)
                                    {
                                        //Start Compressing photos of this Order.
                                        if (change.UploadList != null)
                                        {
                                            ImageCompress imgCompress = ImageCompress.GetImageCompressObject;
                                            foreach (UploadInfo upload in change.UploadList)
                                            {
                                                if (upload.MediaType == 1)
                                                {
                                                    try
                                                    {
                                                        IsPartialEdited = false;
                                                        if (!Directory.Exists(System.IO.Path.GetDirectoryName(upload.UploadCompressedFilePath)))
                                                        {
                                                            System.IO.Directory.CreateDirectory(System.IO.Path.GetDirectoryName(upload.UploadCompressedFilePath));
                                                        }
                                                        if (iMIXconfigurationLocationWiseValue != null && iMIXconfigurationLocationWiseValue.Count > 0)
                                                        {
                                                            var res = iMIXconfigurationLocationWiseValue.Where(x => x.SubstoreId == upload.SubstoreId && x.LocationId == upload.LocationId).FirstOrDefault();
                                                            if (res != null)
                                                                IsPartialEdited = true;
                                                        }
                                                        if (IsPartialEdited)
                                                        {
                                                            if (File.Exists(upload.PartialEditedUploadFilePath))
                                                            {
                                                                upload.UploadFilePath = upload.PartialEditedUploadFilePath;
                                                            }
                                                            else if (File.Exists(upload.EditedUploadFilePath))
                                                            {
                                                                upload.UploadFilePath = upload.EditedUploadFilePath;
                                                            }
                                                        }
                                                        else
                                                        {
                                                            if (File.Exists(upload.EditedUploadFilePath))
                                                            {
                                                                upload.UploadFilePath = upload.EditedUploadFilePath;
                                                            }

                                                        }

                                                        if (!File.Exists(upload.UploadFilePath))
                                                        {
                                                            flag = false;
                                                            log.Info("Image Not found : " + upload.UploadFilePath);
                                                            SyncController.UpdateSyncStatus(change.ChangeTrackingId, (int)SyncStatus.FailedImageNotFound);
                                                            SuccessFlag = false;
                                                            continue;
                                                        }
                                                        imgCompress.GetImage = new System.Drawing.Bitmap(upload.UploadFilePath);
                                                        imgCompress.Height = Convert.ToInt32(imgCompress.GetImage.Height * (Convert.ToDecimal(upload.OnlineImageCompression) / 100));
                                                        imgCompress.Width = Convert.ToInt32(imgCompress.GetImage.Width * (Convert.ToDecimal(upload.OnlineImageCompression) / 100));
                                                        imgCompress.Save(System.IO.Path.GetFileName(upload.UploadFilePath), System.IO.Path.GetDirectoryName(upload.UploadCompressedFilePath));

                                                        log.Info("Compressing Image : " + upload.UploadFilePath);
                                                        if (SuccessFlag)
                                                        {
                                                            log.Info("status updated InProgressCompressingImages");
                                                            SyncController.UpdateSyncStatus(change.ChangeTrackingId, (int)SyncStatus.InProgressCompressingImages);
                                                        }
                                                    }
                                                    catch (Exception ex)
                                                    {
                                                        flag = false;
                                                        log.Error("Push:: Error Compressing Image: " + ex.Message + " StackTrace: " + ex.StackTrace + " InnerException: " + ex.InnerException + " Source: " + ex.Source);
                                                        SyncController.UpdateSyncStatus(change.ChangeTrackingId, (int)SyncStatus.FailedCompressingImages);
                                                        SuccessFlag = false;
                                                        //continue;
                                                    }
                                                }


                                            }

                                            int uploadCount = 0;
                                            //Upload Photos.
                                            foreach (UploadInfo upload in change.UploadList)
                                            {
                                                try
                                                {
                                                    log.Info("Upload Main Photo");
                                                    string uploadFilePath = string.Empty;
                                                    //Upload Main Photo
                                                    if (upload.MediaType == 1)
                                                    {
                                                        uploadFilePath = upload.UploadCompressedFilePath;
                                                    }
                                                    else
                                                    {
                                                        uploadFilePath = upload.UploadFilePath;
                                                    }
                                                    if (File.Exists(uploadFilePath))
                                                    {
                                                        //System.IO.FileInfo fileInfo = new System.IO.FileInfo(upload.UploadCompressedFilePath);
                                                        //RemoteFileInfo uploadRequestInfo = new RemoteFileInfo();
                                                        //using (System.IO.FileStream stream = new System.IO.FileStream(upload.UploadCompressedFilePath, System.IO.FileMode.Open, System.IO.FileAccess.Read))
                                                        //{
                                                        //    uploadRequestInfo.FileName = System.IO.Path.GetFileName(upload.UploadCompressedFilePath);
                                                        //    uploadRequestInfo.SaveFolderPath = upload.SaveFolderPath;
                                                        //    uploadRequestInfo.Length = fileInfo.Length;
                                                        //    uploadRequestInfo.FileByteStream = stream;
                                                        //    DServiceProxy<IFileService>.Use(client =>
                                                        //    {
                                                        //        client.UploadFile(uploadRequestInfo);
                                                        //    });
                                                        //}



                                                        //Move below lines before loop.
                                                        //string cloundApiBaseAddress = ConfigurationManager.AppSettings["ApiBaseAddress"].ToString();

                                                        //Search Cloudinary Record for OrderID, PhotoID, QRCode
                                                        var cloudinaryDetails = SyncController.GetCloudinaryDetails(upload);
                                                        if (cloudinaryDetails != null && cloudinaryDetails.CloudinaryStatusID == 1)
                                                        {
                                                            //ALREADY SUCCESSFULLY UPLOADED
                                                            log.Info("ALREADY SUCCESSFULLY UPLOADED");
                                                            uploadCount++;
                                                            continue;
                                                        }
                                                        else
                                                        {

                                                            string filePath = ConfigurationManager.AppSettings["DestinationSiteDirectoryPath"];
                                                            int defaultWidth = ImageHelper.GetDefaultWidth;
                                                            int defaultHeight = ImageHelper.GetDefaultHeight;

                                                            UploadFileInfo obj = new UploadFileInfo
                                                            {
                                                                Title = System.IO.Path.GetFileName(uploadFilePath),
                                                                ImagePath = uploadFilePath,
                                                                TargetDirectory = filePath + upload.SaveFolderPath,
                                                                ImageDefaultHeight = defaultHeight,
                                                                ImageDefaultWidth = defaultWidth,
                                                                MediaType = upload.MediaType,
                                                                PhotoId = upload.PhotoId

                                                            };
                                                            CloudinaryUploadInfo objCloudinaryUploadInfo = UploadToCloud.UploadFile(obj, out errorPhotoId);

                                                            if (cloudinaryDetails != null)
                                                            {
                                                                //Update Cloudinary upload status in CloudinaryDtl table for same CloudinaryInfoID
                                                                cloudinaryDetails.CloudinaryStatusID = objCloudinaryUploadInfo.CloudinaryStatusID;
                                                                cloudinaryDetails.ErrorMessage = objCloudinaryUploadInfo.ErrorMessage;
                                                                cloudinaryDetails.CloudinaryPublicID = objCloudinaryUploadInfo.CloudinaryPublicID;
                                                                cloudinaryDetails.RetryCount += 1;
                                                                cloudinaryDetails.ModifiedDateTime = DateTime.Now;
                                                                cloudinaryDetails.Height = objCloudinaryUploadInfo.Height;
                                                                cloudinaryDetails.Width = objCloudinaryUploadInfo.Width;
                                                                cloudinaryDetails.ThumbnailDimension = objCloudinaryUploadInfo.ThumbnailDimension;
                                                                SyncController.UpdateCloudinaryUploadDetails(cloudinaryDetails);
                                                            }
                                                            else
                                                            {
                                                                //update cloudinary upload status in CloudinaryDtl table
                                                                CloudinaryDtl objCloudinaryDtl = new CloudinaryDtl();
                                                                objCloudinaryDtl.PhotoID = upload.PhotoId;
                                                                objCloudinaryDtl.SourceImageID = obj.Title;
                                                                objCloudinaryDtl.AddedBy = "sa";
                                                                objCloudinaryDtl.CreatedDateTime = DateTime.Now;
                                                                objCloudinaryDtl.ModifiedDateTime = DateTime.Now;
                                                                objCloudinaryDtl.ModifiedBy = "webusers";
                                                                objCloudinaryDtl.IsActive = true;
                                                                objCloudinaryDtl.SyncCode = CommonUtility.GetUniqueSynccode(Convert.ToInt32(ApplicationObjectEnum.Cloudinary).ToString().PadLeft(2, '0'), site.Store.Country.CountryCode, site.Store.StoreCode, "00");
                                                                objCloudinaryDtl.OrderId = upload.OrderId;
                                                                objCloudinaryDtl.IdentificationCode = upload.IdentificationCode;
                                                                objCloudinaryDtl.UploadCompressedFilePath = uploadFilePath;
                                                                objCloudinaryDtl.TargetDirectory = obj.TargetDirectory;

                                                                objCloudinaryDtl.CloudinaryStatusID = objCloudinaryUploadInfo.CloudinaryStatusID;
                                                                objCloudinaryDtl.ErrorMessage = objCloudinaryUploadInfo.ErrorMessage;
                                                                objCloudinaryDtl.CloudinaryPublicID = objCloudinaryUploadInfo.CloudinaryPublicID;
                                                                objCloudinaryDtl.Width = objCloudinaryUploadInfo.Width;
                                                                objCloudinaryDtl.Height = objCloudinaryUploadInfo.Height;
                                                                objCloudinaryDtl.ThumbnailDimension = objCloudinaryUploadInfo.ThumbnailDimension;
                                                                SyncController.InsertCloudinaryUploadDetails(objCloudinaryDtl);
                                                            }


                                                            //change regarding partial sync showing wrong status --12 march 19
                                                            if (objCloudinaryUploadInfo.CloudinaryStatusID == 1)
                                                            {
                                                                uploadCount++;
                                                            }
                                                            else
                                                            {
                                                                SuccessFlag = false;
                                                                flag = false;
                                                                log.Error("Push:: Error Uploading Image to Cloudinary: CloudinaryDtl.PhotoID :" + upload.PhotoId);
                                                                SyncController.UpdateSyncStatus(change.ChangeTrackingId, (int)SyncStatus.FailedUploadPhotosCloudinary);
                                                            }
                                                        }


                                                        log.Info("Uploading Image to Cloudinary : " + upload.UploadFilePath);

                                                        if (SuccessFlag)
                                                        {
                                                            SyncController.UpdateSyncStatus(change.ChangeTrackingId, (int)SyncStatus.InProgressUploadPhotosCloudinary);
                                                        }

                                                    }
                                                    else
                                                    {

                                                        flag = false;
                                                        SuccessFlag = false;
                                                        log.Info("Image Not found - Failed CloudinaryUploading: " + upload.UploadFilePath);
                                                        SyncController.UpdateSyncStatus(change.ChangeTrackingId, (int)SyncStatus.FailedImageNotFound);
                                                    }


                                                }
                                                catch (Exception ex)
                                                {
                                                    SuccessFlag = false;
                                                    flag = false;
                                                    log.Error("Push:: Error Uploading Image to Cloudinary: " + ex.Message + " StackTrace: " + ex.StackTrace + " InnerException: " + ex.InnerException + " Source: " + ex.Source);
                                                    SyncController.UpdateSyncStatus(change.ChangeTrackingId, (int)SyncStatus.FailedUploadPhotosCloudinary);
                                                    //continue;
                                                }
                                            }

                                            log.Info("After continue ALREADY SUCCESSFULLY UPLOADED");
                                            if (uploadCount > 0 && uploadCount < change.UploadList.Count)
                                            {
                                                log.Info("Partially Uploaded Image to Cloudinary : ");
                                                SyncController.UpdateSyncStatus(change.ChangeTrackingId, (int)SyncStatus.PartillyFailedUploading);
                                            }
                                            //Delete Order Number folder.
                                            if (change.UploadList.Count > 0)
                                            {
                                                if (Directory.Exists(System.IO.Path.GetDirectoryName(change.UploadList[0].UploadCompressedFilePath)))
                                                    Directory.Delete(System.IO.Path.GetDirectoryName(change.UploadList[0].UploadCompressedFilePath), true);
                                            }

                                        }
                                        try
                                        {
                                            // Upload Border , BackGround and Graphic
                                            log.Info("Upload Border , BackGround and Graphic");
                                            if (change.UploadFile != null)
                                            {
                                                string Folderpath = string.Empty;
                                                UploadFile upload = new UploadFile();
                                                upload = change.UploadFile;

                                                if (upload.FileType == 1)
                                                {
                                                    upload.SaveFolderPath = "Border";
                                                    upload.SaveThumbnailFolderPath = ("BorderThumbnails");
                                                }
                                                else if (upload.FileType == 2)
                                                {
                                                    upload.SaveFolderPath = "BackGround";
                                                    upload.SaveThumbnailFolderPath = "BackGroundThumbnails";
                                                }
                                                else if (upload.FileType == 3)
                                                {
                                                    upload.SaveFolderPath = "Graphics";
                                                    upload.SaveThumbnailFolderPath = "";
                                                }
                                                if (File.Exists(upload.UploadFilePath))
                                                {
                                                    System.IO.FileInfo fileInfo = new System.IO.FileInfo(upload.UploadFilePath);
                                                    RemoteFileInfo uploadRequestInfo = new RemoteFileInfo();
                                                    using (System.IO.FileStream stream = new System.IO.FileStream(upload.UploadFilePath, System.IO.FileMode.Open, System.IO.FileAccess.Read))
                                                    {
                                                        uploadRequestInfo.FileName = System.IO.Path.GetFileName(upload.UploadFilePath);
                                                        uploadRequestInfo.SaveFolderPath = upload.SaveFolderPath;
                                                        uploadRequestInfo.Length = fileInfo.Length;
                                                        uploadRequestInfo.FileByteStream = stream;
                                                        DServiceProxy<IFileService>.Use(client =>
                                                        {
                                                            client.UploadFile(uploadRequestInfo);
                                                        });
                                                    }
                                                }
                                                //Upload Thumbnails
                                                if (upload.FileType == 1 || upload.FileType == 2)
                                                {
                                                    if (File.Exists(upload.UploadThumbnailFilePath))
                                                    {
                                                        System.IO.FileInfo fileInfo = new System.IO.FileInfo(upload.UploadThumbnailFilePath);
                                                        RemoteFileInfo uploadRequestInfo = new RemoteFileInfo();
                                                        using (System.IO.FileStream stream = new System.IO.FileStream(upload.UploadThumbnailFilePath, System.IO.FileMode.Open, System.IO.FileAccess.Read))
                                                        {
                                                            uploadRequestInfo.FileName = System.IO.Path.GetFileName(upload.UploadThumbnailFilePath);
                                                            uploadRequestInfo.SaveFolderPath = upload.SaveThumbnailFolderPath;
                                                            uploadRequestInfo.Length = fileInfo.Length;
                                                            uploadRequestInfo.FileByteStream = stream;
                                                            DServiceProxy<IFileService>.Use(client =>
                                                            {
                                                                client.UploadFile(uploadRequestInfo);
                                                            });
                                                        }
                                                    }
                                                }
                                                flag = true;

                                                log.Info("Uploaded " + upload.SaveFolderPath + "to Cloudinary : " + upload.UploadFilePath);
                                                SyncController.UpdateSyncStatus(change.ChangeTrackingId, (int)SyncStatus.UploadedGraphicsToCloud);
                                            }

                                        }
                                        catch
                                        {
                                            log.Error("Error in Graphics Uploading: " + change.UploadFile);
                                            SyncController.UpdateSyncStatus(change.ChangeTrackingId, (int)SyncStatus.FailedUploadedGraphicsToCloud);
                                            continue;
                                        }
                                        //Delete Order Number folder.
                                        if (change.UploadList != null && change.UploadList.Count > 0)
                                        {
                                            if (Directory.Exists(System.IO.Path.GetDirectoryName(change.UploadList[0].UploadCompressedFilePath)))
                                                Directory.Delete(System.IO.Path.GetDirectoryName(change.UploadList[0].UploadCompressedFilePath), true);
                                        }

                                        log.Info("Towards final Status");
                                        if (SuccessFlag)
                                        {
                                            if (flag == true)
                                            {
                                                log.Info("Final Status=flag=" + flag);
                                                //photo uploaded to cloudinary 3
                                                if (change.ApplicationObjectId == 14)
                                                {
                                                    if (IsOrderCancel != null && IsOrderCancel == true)
                                                    {
                                                        log.Info("Final Status=5");
                                                        SyncController.UpdateSyncStatus(change.ChangeTrackingId, (int)SyncStatus.UploadedDataOnCentralServerandCancel);
                                                    }
                                                    else
                                                    {
                                                        log.Info("Final Status=3");
                                                        SyncController.UpdateSyncStatus(change.ChangeTrackingId, (int)SyncStatus.UploadedDataOnCentralServerandCloudinary);
                                                    }
                                                }
                                                else
                                                {
                                                    SyncController.UpdateSyncStatus(change.ChangeTrackingId, (int)SyncStatus.Synced);
                                                }
                                                if (log.IsInfoEnabled)
                                                    log.Info("Sync completed for ChangeTrackingId : " + change.ChangeTrackingId);
                                            }
                                            else
                                            {
                                                //photo not uploaded -3
                                                if (change.ApplicationObjectId == 14)
                                                {
                                                    SyncController.UpdateSyncStatus(change.ChangeTrackingId, (int)SyncStatus.UploadedDataOnCentralServerButFailedonCloudinary);
                                                }
                                                else
                                                {
                                                    SyncController.UpdateSyncStatus(change.ChangeTrackingId, (int)SyncStatus.Error);
                                                }
                                                //Added by Anand, this will generate record in ChangeTrackingExceptionDtl for this ChangeTrackingID.
                                                //Exception ex = new Exception("Error in uploadloading photos to cloudinary");
                                                //SyncController.AddChangeTrackingExceptionDtl(change.ChangeTrackingId, ex);
                                                if (log.IsInfoEnabled)
                                                    log.Error("Sync failed for ChangeTrackingId : " + change.ChangeTrackingId);
                                            }
                                        }
                                    }
                                    else
                                    {
                                        SyncController.UpdateSyncStatus(change.ChangeTrackingId, (int)SyncStatus.Invalid);
                                    }

                                }
                                else
                                {
                                    if (log.IsInfoEnabled)
                                        log.Info("Sync Started for ChangeTrackingId : " + change.ChangeTrackingId);
                                    bool success = false;
                                    DServiceProxy<IDataSyncService>.Use(client =>
                                    {
                                        success = client.SaveChangeInfo(change);
                                    });
                                    if (success)
                                    {
                                        SyncController.UpdateSyncStatus(change.ChangeTrackingId, (int)SyncStatus.Synced);
                                        if (log.IsInfoEnabled)
                                            log.Info("Sync completed for ChangeTrackingId : " + change.ChangeTrackingId);
                                    }
                                    else
                                    {
                                        SyncController.UpdateSyncStatus(change.ChangeTrackingId, (int)SyncStatus.Invalid);
                                    }
                                }
                            }
                            catch (Exception ex)
                            {
                                if (ex.Message.ToString().ToUpper().Contains("NO ENDPOINT") || ex.Message.ToString().ToUpper().Contains("LONGER TIMEOUT"))
                                {
                                    log.Error("Push:: Message: " + ex.Message + " StackTrace: " + ex.StackTrace + " InnerException: " + ex.InnerException + " Source: " + ex.Source);
                                    break;
                                }
                                else
                                {
                                    SyncController.UpdateSyncStatus(change.ChangeTrackingId, (int)SyncStatus.Error);
                                    //Added by Anand, this will generate record in ChangeTrackingExceptionDtl for this ChangeTrackingID.
                                    //SyncController.AddChangeTrackingExceptionDtl(change.ChangeTrackingId, ex);
                                    log.Error("Push:: Message: " + ex.Message + " StackTrace: " + ex.StackTrace + " InnerException: " + ex.InnerException + " Source: " + ex.Source);
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                //TBD
                //Mark this change as sync error
                //SyncController.UpdateSyncStatus(change.ChangeTrackingId, (int)SyncStatus.Error);
                log.Error("Push:: Message: " + ex.Message + " StackTrace: " + ex.StackTrace + " InnerException: " + ex.InnerException + " Source: " + ex.Source);
            }
            log.Info("Push End");
        }
        private void PushOld(int SyncDataDelay)
        {
            log.Info("Push Start");

            List<ChangeInfo> changeList = null;
            try
            {
                List<SiteInfo> sites = SyncController.GetSites();
                foreach (var site in sites)
                {
                    log.Info("SubStore Name/Code:" + site.SiteName + "/" + site.SiteCode);
                    //Get the ChangeInfo from database.
                    changeList = SyncController.GetNextChangeInfo(SyncDataDelay);

                    if (changeList == null)
                    {
                        log.Info("Change Tracking Not Found");
                    }
                    if (changeList != null)
                    {
                        foreach (ChangeInfo change in changeList)
                        {
                            try
                            {
                                log.Info("Change Tracking Id:" + change.ChangeTrackingId);
                                change.SubStore = new SiteInfo
                                {
                                    SiteCode = site.SiteCode,
                                    SiteName = site.SiteName,
                                    Store = new StoreInfo
                                    {
                                        StoreCode = site.Store.StoreCode,
                                        StoreName = site.Store.StoreName
                                    }
                                };

                                if (!string.IsNullOrEmpty(change.dataXML))
                                {
                                    if (log.IsInfoEnabled)
                                        log.Info("Sync Started for ChangeTrackingId : " + change.ChangeTrackingId);
                                    bool success = false;
                                    DServiceProxy<IDataSyncService>.Use(client =>
                                    {
                                        success = client.SaveChangeInfo(change);
                                    });
                                    if (success)
                                    {
                                        //Start Compressing photos of this Order.
                                        if (change.UploadList != null)
                                        {
                                            ImageCompress imgCompress = ImageCompress.GetImageCompressObject;
                                            foreach (UploadInfo upload in change.UploadList)
                                            {
                                                if (upload.MediaType == 1)
                                                {
                                                    if (!Directory.Exists(System.IO.Path.GetDirectoryName(upload.UploadCompressedFilePath)))
                                                    {
                                                        System.IO.Directory.CreateDirectory(System.IO.Path.GetDirectoryName(upload.UploadCompressedFilePath));
                                                    }
                                                    if (File.Exists(upload.EditedUploadFilePath))
                                                    {
                                                        upload.UploadFilePath = upload.EditedUploadFilePath;
                                                    }

                                                    imgCompress.GetImage = new System.Drawing.Bitmap(upload.UploadFilePath);
                                                    imgCompress.Height = Convert.ToInt32(imgCompress.GetImage.Height * (Convert.ToDecimal(upload.OnlineImageCompression) / 100));
                                                    imgCompress.Width = Convert.ToInt32(imgCompress.GetImage.Width * (Convert.ToDecimal(upload.OnlineImageCompression) / 100));
                                                    imgCompress.Save(System.IO.Path.GetFileName(upload.UploadFilePath), System.IO.Path.GetDirectoryName(upload.UploadCompressedFilePath));
                                                }
                                            }

                                            //Upload Photos.
                                            foreach (UploadInfo upload in change.UploadList)
                                            {
                                                //Upload Main Photo
                                                if (upload.MediaType == 1)
                                                {
                                                    if (File.Exists(upload.UploadCompressedFilePath))
                                                    {
                                                        System.IO.FileInfo fileInfo = new System.IO.FileInfo(upload.UploadCompressedFilePath);
                                                        RemoteFileInfo uploadRequestInfo = new RemoteFileInfo();
                                                        using (System.IO.FileStream stream = new System.IO.FileStream(upload.UploadCompressedFilePath, System.IO.FileMode.Open, System.IO.FileAccess.Read))
                                                        {
                                                            uploadRequestInfo.FileName = System.IO.Path.GetFileName(upload.UploadCompressedFilePath);
                                                            uploadRequestInfo.SaveFolderPath = upload.SaveFolderPath;
                                                            uploadRequestInfo.Length = fileInfo.Length;
                                                            uploadRequestInfo.FileByteStream = stream;
                                                            DServiceProxy<IFileService>.Use(client =>
                                                            {
                                                                client.UploadFile(uploadRequestInfo);
                                                            });
                                                        }
                                                    }
                                                }
                                                else
                                                {
                                                    //Upload Main Videos
                                                    if (File.Exists(upload.UploadFilePath))
                                                    {
                                                        System.IO.FileInfo fileInfo = new System.IO.FileInfo(upload.UploadFilePath);
                                                        RemoteFileInfo uploadRequestInfo = new RemoteFileInfo();
                                                        using (System.IO.FileStream stream = new System.IO.FileStream(upload.UploadFilePath, System.IO.FileMode.Open, System.IO.FileAccess.Read))
                                                        {
                                                            uploadRequestInfo.FileName = System.IO.Path.GetFileName(upload.UploadFilePath);
                                                            uploadRequestInfo.SaveFolderPath = upload.SaveFolderPath;
                                                            uploadRequestInfo.Length = fileInfo.Length;
                                                            uploadRequestInfo.FileByteStream = stream;
                                                            DServiceProxy<IFileService>.Use(client =>
                                                            {
                                                                client.UploadFile(uploadRequestInfo);
                                                            });
                                                        }
                                                    }
                                                }
                                                //Upload Thumbnails
                                                if (File.Exists(upload.UploadThumbnailFilePath))
                                                {
                                                    System.IO.FileInfo fileInfo = new System.IO.FileInfo(upload.UploadThumbnailFilePath);
                                                    RemoteFileInfo uploadRequestInfo = new RemoteFileInfo();
                                                    using (System.IO.FileStream stream = new System.IO.FileStream(upload.UploadThumbnailFilePath, System.IO.FileMode.Open, System.IO.FileAccess.Read))
                                                    {
                                                        uploadRequestInfo.FileName = System.IO.Path.GetFileName(upload.UploadThumbnailFilePath);
                                                        uploadRequestInfo.SaveFolderPath = upload.SaveThumbnailFolderPath;
                                                        uploadRequestInfo.Length = fileInfo.Length;
                                                        uploadRequestInfo.FileByteStream = stream;
                                                        DServiceProxy<IFileService>.Use(client =>
                                                        {
                                                            client.UploadFile(uploadRequestInfo);
                                                        });
                                                    }
                                                }
                                            }
                                            //Delete Order Number folder.
                                            if (change.UploadList.Count > 0)
                                            {
                                                if (Directory.Exists(System.IO.Path.GetDirectoryName(change.UploadList[0].UploadCompressedFilePath)))
                                                    Directory.Delete(System.IO.Path.GetDirectoryName(change.UploadList[0].UploadCompressedFilePath), true);
                                            }
                                        }

                                        SyncController.UpdateSyncStatus(change.ChangeTrackingId, (int)SyncStatus.Synced);
                                        if (log.IsInfoEnabled)
                                            log.Info("Sync completed for ChangeTrackingId : " + change.ChangeTrackingId);
                                    }
                                    else
                                    {
                                        SyncController.UpdateSyncStatus(change.ChangeTrackingId, (int)SyncStatus.Invalid);
                                    }

                                }
                                else
                                {
                                    if (log.IsInfoEnabled)
                                        log.Info("Sync Started for ChangeTrackingId : " + change.ChangeTrackingId);
                                    bool success = false;
                                    DServiceProxy<IDataSyncService>.Use(client =>
                                    {
                                        success = client.SaveChangeInfo(change);
                                    });
                                    if (success)
                                    {
                                        SyncController.UpdateSyncStatus(change.ChangeTrackingId, (int)SyncStatus.Synced);
                                        if (log.IsInfoEnabled)
                                            log.Info("Sync completed for ChangeTrackingId : " + change.ChangeTrackingId);
                                    }
                                    else
                                    {
                                        SyncController.UpdateSyncStatus(change.ChangeTrackingId, (int)SyncStatus.Invalid);
                                    }
                                }
                            }
                            catch (Exception ex)
                            {
                                SyncController.UpdateSyncStatus(change.ChangeTrackingId, (int)SyncStatus.Error);
                                log.Error("Push:: Message: " + ex.Message + " StackTrace: " + ex.StackTrace + " InnerException: " + ex.InnerException + " Source: " + ex.Source);
                            }
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                //TBD
                //Mark this change as sync error
                //SyncController.UpdateSyncStatus(change.ChangeTrackingId, (int)SyncStatus.Error);
                log.Error("Push:: Message: " + ex.Message + " StackTrace: " + ex.StackTrace + " InnerException: " + ex.InnerException + " Source: " + ex.Source);
            }
            log.Info("Push End");
        }
        void Pull()
        {
            try
            {


                List<SiteInfo> sites = SyncController.GetSites();

                foreach (var site in sites)
                {
                    if (string.IsNullOrEmpty(site.SiteCode))
                    {
                        continue;
                    }
                    //Get Last ChnageDetails By SubStore
                    ChangeInfo lastChangeInfo = SyncController.GetLastChangeDetail(site);
                    ChangeInfo changeInfo = null;
                    do
                    {
                        if (lastChangeInfo != null)
                        {
                            changeInfo = SyncController.GetLastChangeDetail(site);  //lastChangeInfo;
                        }
                        else
                        {
                            lastChangeInfo = new ChangeInfo
                            {
                                ChangeTrackingId = 0,
                                SubStore = site
                            };
                        }
                        if (changeInfo != null)
                        {
                            lastChangeInfo = changeInfo;
                        }

                        //requestHandler = new SyncRequestHandler();
                        //changeInfo = requestHandler.GetNextChangeInfo(lastChangeInfo);

                   


                        DServiceProxy<IDataSyncService>.Use(client =>
                        {
                            changeInfo = client.GetNextChangeInfo(lastChangeInfo);
                        });

                        if (changeInfo != null)
                        {
                            if (changeInfo.ChangeTrackingId > lastChangeInfo.ChangeTrackingId)
                            {
                                changeInfo.SubStore = new SiteInfo
                                {
                                    SiteCode = site.SiteCode,
                                    SiteName = site.SiteName,
                                    SubStoreId = site.SubStoreId,
                                    HotFolderPath = site.HotFolderPath,
                                };
                                IProcesser processor = ProcessorFactory.Create(changeInfo.ApplicationObjectId);
                                processor.Process(changeInfo);
                                if (changeInfo.ChangeAction != 3 && changeInfo.DownloadDetail != null)
                                {
                                    DownloadManager.Downloadfile(changeInfo.DownloadDetail.DownloadFilePath, changeInfo.ApplicationObjectId, site);
                                }
                            }
                        }
                    } while (changeInfo.ChangeTrackingId > lastChangeInfo.ChangeTrackingId);

                }
            }
            catch (Exception ex)
            {
                log.Error("Pull:: Message: " + ex.Message + " StackTrace: " + ex.StackTrace + " InnerException: " + ex.InnerException + " Source: " + ex.Source);

            }
        }


    }
}
