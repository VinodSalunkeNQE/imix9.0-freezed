﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing.Drawing2D;
using System.Globalization;
using System.Linq;
using System.Windows;
using System.Windows.Documents;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.IO;
using System.Threading;
using System.Drawing.Imaging;
using System.Drawing;
using System.Windows.Threading;
using System.Xml;
using DigiPhoto.DataLayer;
using DigiPhoto.DataLayer.Model;
using DigiPhoto;
//using DigiPhoto.Interop;
using DigiWatcher.Shader;
using System.Runtime.InteropServices;
using DigiPhoto.Common;
using ImageMagickObject;
using LevDan.Exif;
using ExifLib;
using System.Diagnostics;
using System.ServiceProcess;
using BarcodeReaderLib;
using System.Reflection;
using DigiPhoto.DataSync.Controller;
using DigiPhoto.DigiSync.Model;
using DigiPhoto.DigiSync.ServiceLibrary.Interface;
using DigiAuditLogger;
using FrameworkHelper;
using DigiPhoto.IMIX.Model;
using DigiPhoto.IMIX.Business;
using System.Windows.Controls;
using System.Threading.Tasks;
using System.Collections.Concurrent;
using DigiPhoto.Utility.Repository.ValueType;
using CryptorEngine = DigiPhoto.CryptorEngine;
using CurrencyInfo = DigiPhoto.IMIX.Model.CurrencyInfo;
using Image = System.Drawing.Image;
using StoreInfo = DigiPhoto.IMIX.Model.StoreInfo;

using ImageMagick;
using System.Security.Cryptography;
using FrameworkHelper.Common;
using System.Text;
using System.Configuration;

namespace DigiWatcher
{
    public partial class FileWatcher : Window
    {

        #region Declaration
        string[] mediaExtensions = new[] { ".jpg", ".avi", ".mp4", ".wmv", ".mov", ".3gp", ".3g2", ".m2v", ".m4v", ".flv", ".mpeg", ".ffmpeg" };
        public delegate void NextPrimeDelegate();
        static bool isrotated = false;
        int photoId;
        ReadImageMetaData _objmetdata;
        FileSystemWatcher _watchFolder = new FileSystemWatcher();
        public static Int32 counter;
        private DispatcherTimer timer;
        private List<SemiOrderSettings> lstDG_SemiOrder_Settings;
        private SemiOrderSettings objDG_SemiOrder_Settings;
        string Directoryname = string.Empty;
        string DirectoryNameWithDate = string.Empty;
        string _thumbnailFilePathDate = string.Empty;
        string _bigThumbnailFilePathDate = string.Empty;
        string _minifiedFilePathDate = string.Empty;
        private String _BorderFolder;
        private bool is_SemiOrder;
        //DigiPhotoDataServices objDigiPhotoDataServices;
        string defaultBrightness = string.Empty;
        string defaultContrast = string.Empty;
        string UniqueCode = string.Empty;
        string codeFormat = string.Empty;
        int substoreId = 0;
        int LocationID = 0;
        int mediaType = 1;
        bool IsBarcodeActive = false;
        bool IsMobileRfidEnabled = false;
        Int32 MappingType = 0;
        Int32 scanType = 0;
        Int32 GapTimeInSec = 0;
        Boolean IsAnonymousCodeActive = false;
        Boolean IsAutoPurchaseActive = false;
        string QRWebURLReplace = string.Empty;
        List<ImgInfoPostScanList> lstImgInfoPostScanList = null;
        List<ImgInfoPostScanList> lstImgInfoPreScanList = null;
        Int32 PhotoGrapherId = 0;
        List<PreScanWatcher> PreScanWatcherList = null;
        CardLimitInfo objCardLimit = new CardLimitInfo();
        bool IsOnline = false;
        int CardImageLimit = 0;
        int CardImageSold = 0;
        public bool IsQrCodeUsed = false;
        int rest = 0;
        DateTime lastOrderChecktime;
        DateTime lastmemoryUpdateTime = DateTime.Now;
        int? RfidScanType = null;
        bool isSpecChromaActive = false;
        bool isSpecCropActive = false;
        string ZoomInfo = string.Empty;
        string GraphicInfo = string.Empty;
        bool IsSpecBrightActive = false;
        bool IsSpecContrastActive = false;
        double SpecBright = 0;
        double SpecContrast = 1;
        public string DownloadFolderPath;
        public string ArchivedFolderPath;
        Boolean isResetDPIRequired = false;
        bool isAutoVideoProcessingActive = false;
        public static int videoCount = 0;
        bool _IsAdvancedVideoEditActive = false;
        string videoFramePositions = string.Empty;
        string cropVideoFrameRatio = string.Empty;
        bool IsGreenScreenWorkFlow = false;
        public bool IsAutoColorCorrectionActive = false;
        bool IsCorrectionAtDownloadActive = false;
        bool IsContrastCorrectionActive = false;
        bool IsGammaCorrectionActive = false;
        bool IsNoiseReductionActive = false;
        public List<String> FilesToDelete = new List<string>();
        bool isVideoProcessed = true;
        VideoSceneBusiness VBusiness = new VideoSceneBusiness();
        List<Watchersetting> watchersetting = new List<DigiPhoto.IMIX.Model.Watchersetting>();
        List<ConfigurationInfo> ConfigurationData = null;
        List<iMixConfigurationLocationInfoList> NewConfigList = null;
        List<SemiOrderSettingsList> SemiOrderSettingList = null;
        List<UsersInfo> UserInfoList = null;

        string OriginalFileName = string.Empty;
        int ParentImageID = -1;
        int imagenameCntr = 0;
        private List<iMixConfigurationLocationInfo> _lstLocationWiseConfigParams;
        private List<iMixConfigurationLocationInfo> _lstPreviewWallLocation;
        List<ImgCopyLocation> imgCopyLocationlst = new List<ImgCopyLocation>();
        private string _previewWallSubStore;
        private string _copyFolderName;
        private string _locationName;
        private string screen = string.Empty;
        string GumBallRideFolderPath = string.Empty;
        string _subStoreName = string.Empty;
        string GumBallRidetxtContent = string.Empty;
        List<long> GumRideList;
        List<iMixConfigurationLocationInfo> LstRideConfigValueLocationWise = new List<iMixConfigurationLocationInfo>();
        List<iMixConfigurationLocationInfo> LstGumBallRideConfigValueLocationWise = new List<iMixConfigurationLocationInfo>();
        List<iMixConfigurationLocationInfo> GumBallConfigurationValueList = new List<iMixConfigurationLocationInfo>();
        List<long> GumBallActiveLocationList = new List<long>();
        string isVisibleZeroScoreOnImage = "False";

        string gumballScoreSeperater = ",";
        string gumballScore = string.Empty;

        public string _QRCode = null;

        string picNameMinified = string.Empty;

        Int32 _QRCodeLenth = 8;

        public string ImageMetaData
        {
            get;
            set;
        }
        string ProductNamePreference = string.Empty;

        System.ComponentModel.BackgroundWorker MyWorker = new System.ComponentModel.BackgroundWorker();//Added by Vinod

        #endregion

        #region Constructor
        /// <summary>
        /// Initializes a new instance of the <see cref="FileWatcher"/> class.
        /// </summary>
        public FileWatcher()
        {
#if DEBUG
            var watch = FrameworkHelper.CommonUtility.Watch();
            ErrorHandler.ErrorHandler.LogFileWrite("File Watcher Step 3. FileWatcher Consturcter :" + DateTime.Now.ToString("MM / dd / yyyy HH: mm:ss"));
            watch.Start();
           
#endif
            try
            {
                ErrorHandler.ErrorHandler.LogFileWrite("File Watcher Step 4. FileWatcher Consturcter :" + DateTime.Now.ToString("MM / dd / yyyy HH: mm:ss"));
                ServicePosInfoBusiness svcPosinfoBusiness = new ServicePosInfoBusiness();
                string ret = svcPosinfoBusiness.ServiceStart(true);

                ErrorHandler.ErrorHandler.LogFileWrite("File Watcher Step 5. FileWatcher Consturcter :" + DateTime.Now.ToString("MM / dd / yyyy HH: mm:ss"));
                //ServicePosInfoBusiness svcPosinfoBusiness = new ServicePosInfoBusiness();
                // int ret = svcPosinfoBusiness.ServiceStart(false);

                if (!string.IsNullOrEmpty(ret))
                {
                    MessageBox.Show("This application is already running on " + ret);
                    Environment.Exit(0);
                    // return;
                }
                ErrorHandler.ErrorHandler.LogFileWrite("File Watcher Step 6. FileWatcher Consturcter :" + DateTime.Now.ToString("MM / dd / yyyy HH: mm:ss"));
                InitializeComponent();

                //MyWorker.DoWork += MyWorker_DoWork;
                //MyWorker.RunWorkerCompleted += MyWorker_RunWorkerCompleted;

                ErrorHandler.ErrorHandler.LogFileWrite("File Watcher Step 7. FileWatcher Consturcter :" + DateTime.Now.ToString("MM / dd / yyyy HH: mm:ss"));
                this.Left = 0;
                this.Top = 0;
                ConfigurationData = new List<ConfigurationInfo>();
                lstImgInfoPostScanList = new List<ImgInfoPostScanList>();
                lstImgInfoPreScanList = new List<ImgInfoPostScanList>();
                PreScanWatcherList = new List<PreScanWatcher>();
                NewConfigList = new List<iMixConfigurationLocationInfoList>();
                SemiOrderSettingList = new List<SemiOrderSettingsList>();
                UserInfoList = new List<UsersInfo>();
                RobotImageLoader.GetConfigData();
                substoreId = LoginUser.SubStoreId;
                ErrorHandler.ErrorHandler.LogFileWrite("File Watcher Step 8. FileWatcher Consturcter :" + DateTime.Now.ToString("MM / dd / yyyy HH: mm:ss"));
                SetNewConfigValues();
                GetStoreName();
                ErrorHandler.ErrorHandler.LogFileWrite("File Watcher Step 9. FileWatcher Consturcter :" + DateTime.Now.ToString("MM / dd / yyyy HH: mm:ss"));
                Directoryname = LoginUser.DigiFolderPath;
                _subStoreName = LoginUser.SubstoreName;
                SetGumRideMasterId();
                ErrorHandler.ErrorHandler.LogFileWrite("File Watcher Step 10. FileWatcher Consturcter :" + DateTime.Now.ToString("MM / dd / yyyy HH: mm:ss"));
                //Create Datewise folder to store image, thumbnail and bigthumbnail
                DirectoryNameWithDate = Path.Combine(Directoryname, DateTime.Now.ToString("yyyyMMdd"));
                _thumbnailFilePathDate = Path.Combine(Directoryname, "Thumbnails", DateTime.Now.ToString("yyyyMMdd"));
                _bigThumbnailFilePathDate = Path.Combine(Directoryname, "Thumbnails_Big", DateTime.Now.ToString("yyyyMMdd"));
                _minifiedFilePathDate = Path.Combine(Directoryname, "Minified_Images", DateTime.Now.ToString("yyyyMMdd"));
                if (!Directory.Exists(DirectoryNameWithDate))
                    Directory.CreateDirectory(DirectoryNameWithDate);
                if (!Directory.Exists(_thumbnailFilePathDate))
                    Directory.CreateDirectory(_thumbnailFilePathDate);
                if (!Directory.Exists(_bigThumbnailFilePathDate))
                    Directory.CreateDirectory(_bigThumbnailFilePathDate);
                if (!Directory.Exists(_minifiedFilePathDate))
                    Directory.CreateDirectory(_minifiedFilePathDate);
                ErrorHandler.ErrorHandler.LogFileWrite("File Watcher Step 11. FileWatcher Consturcter :" + DateTime.Now.ToString("MM / dd / yyyy HH: mm:ss"));
                //Function will fill all substore configuration
                GetConfigDataList();
                ErrorHandler.ErrorHandler.LogFileWrite("File Watcher Step 12. After GetConfigDataList start :" + DateTime.Now.ToString("MM / dd / yyyy HH: mm:ss"));
                
                StoreSubStoreDataBusniess storeObj = new StoreSubStoreDataBusniess();
                QRWebURLReplace = storeObj.GetQRCodeWebUrl();
                //objDigiPhotoDataServices = new DigiPhotoDataServices();
                //QRWebURLReplace = objDigiPhotoDataServices.GetQRCodeWebUrl();
                ErrorHandler.ErrorHandler.LogFileWrite("Semi Order Setting Completed");

                DigiPhoto.IMIX.Model.StoreInfo store = storeObj.GetStore();
                if (store.RunApplicationsSubStoreLevel)
                {
                    string SubStoreName = string.Empty;
                    //Set current SubStoreId
                    try
                    {
                        string pathtosave = Environment.CurrentDirectory;
                        //Bhavin edit
                        ErrorHandler.ErrorHandler.LogFileWrite("SS.Dat File Path : " + pathtosave + "\\ss.dat");
                        //pathtosave = @"D:\work\work\s3sync\DigiPhotoEnhancedCode\DigiPhoto\bin\Debug";
                        if (File.Exists(pathtosave + "\\ss.dat"))
                        {
                            string line;
                            using (StreamReader reader = new StreamReader(pathtosave + "\\ss.dat"))
                            {
                                line = reader.ReadLine();
                                string subID = DigiPhoto.CryptorEngine.Decrypt(line, true);
                                int subStoreId = (subID.Split(',')[0]).ToInt32();
                                SubStoreName = (new StoreSubStoreDataBusniess()).GetSubstoreNameById(subStoreId);
                                if (string.IsNullOrEmpty(SubStoreName))
                                {
                                    SubStoreName = "NQDEVENV";
                                    ErrorHandler.ErrorHandler.LogFileWrite("Site not found.Please update site details with respective site code.");
                                }
                                QRCodeLength(subStoreId);
                                _lstLocationWiseConfigParams = new List<iMixConfigurationLocationInfo>();
                                _lstLocationWiseConfigParams = (new ConfigBusiness()).GetLocationWiseConfigParams(subStoreId);

                                _lstPreviewWallLocation = _lstLocationWiseConfigParams.Where(s => s.IMIXConfigurationMasterId == Convert.ToDouble(ConfigParams.NoOfScreenPW)).ToList();
                                GumBallConfigurationValueList = _lstLocationWiseConfigParams.Where(y => GumRideList.Contains(y.IMIXConfigurationMasterId)).ToList();
                                GumBallActiveLocationList = GumBallConfigurationValueList.Where(n => n.IMIXConfigurationMasterId == (long)ConfigParams.IsGumRideActive
                        && n.ConfigurationValue.ToUpper() == "TRUE").Select(x => Convert.ToInt64(x.LocationId)).ToList();
                                if (_lstPreviewWallLocation != null && _lstPreviewWallLocation.Count > 0)
                                {
                                    ImgCopyLocation imgCopyLocation;
                                    foreach (iMixConfigurationLocationInfo PreviewWallLocation in _lstPreviewWallLocation)
                                    {
                                        imgCopyLocation = new ImgCopyLocation();
                                        imgCopyLocation.locID = PreviewWallLocation.LocationId;
                                        imgCopyLocation.counter = 1;
                                        imgCopyLocation.range = Convert.ToInt32(PreviewWallLocation.ConfigurationValue);
                                        imgCopyLocationlst.Add(imgCopyLocation);
                                    }
                                }
                                _previewWallSubStore = Directoryname + "\\PreviewWall\\" + SubStoreName;
                            }
                        }
                        else
                        {
                            MessageBox.Show("Please select substore from Configuration Section in Imix for this machine.");
                            Application.Current.Shutdown();
                        }
                    }
                    catch (Exception ex)
                    {
                        ErrorHandler.ErrorHandler.LogError(ex);
                    }
                    DownloadFolderPath = Directoryname + "\\Download\\" + SubStoreName;
                    ArchivedFolderPath = Directoryname + "\\Archived\\" + SubStoreName;
                    if (!Directory.Exists(ArchivedFolderPath))
                    {
                        Directory.CreateDirectory(ArchivedFolderPath);
                    }
                    
                }
                else
                {
                    DownloadFolderPath = Directoryname + "\\Download";
                    ArchivedFolderPath = Directoryname + "\\Archived";
                }

                ErrorHandler.ErrorHandler.LogFileWrite("File Watcher Step 13. FileWatcher Consturcter :" + DateTime.Now.ToString("MM / dd / yyyy HH: mm:ss"));
  
                Task t2 = new Task(DeleteTempFiles);
                t2.Start();
  
                ErrorHandler.ErrorHandler.LogFileWrite("File Watcher Step 14. FileWatcher Consturcter :" + DateTime.Now.ToString("MM / dd / yyyy HH: mm:ss"));
                txtProcessing.Text = string.Empty;

                lastOrderChecktime = DateTime.Now;
                timer = new DispatcherTimer();
                timer.Interval = TimeSpan.FromSeconds(1);
                timer.Tick += timer1_Tick;
                timer.Start();

                //If Watcher is running on server.
                //if (!store.RunApplicationsSubStoreLevel)
                //{
                try
                {
                    ServiceController controller = new ServiceController("DigiWifiImageProcessing");
                    controller.Start();
                    ErrorHandler.ErrorHandler.LogFileWrite("File Watcher Step 15. FileWatcher Consturcter :" + DateTime.Now.ToString("MM / dd / yyyy HH: mm:ss"));
                }
                catch (Exception ex)
                {
                    ErrorHandler.ErrorHandler.LogError(ex);
                }
                //}
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
            }
#if DEBUG
            if (watch != null)
                FrameworkHelper.CommonUtility.WatchStop("Watcher Start Up", watch);
#endif
        }
        #endregion

        private void DeleteTempFiles()
        {
            ErrorHandler.ErrorHandler.LogFileWrite("File Watcher Step 14- 1. DeleteTemp Files  :" + DateTime.Now.ToString("MM / dd / yyyy HH: mm:ss"));
            DirectoryInfo drinfo = new DirectoryInfo(ArchivedFolderPath);
            var tempThumbnailsPath = Path.Combine(Directoryname, "Thumbnails\\Temp");

            if (!Directory.Exists(tempThumbnailsPath))
                Directory.CreateDirectory(tempThumbnailsPath);

            DirectoryInfo dir = new DirectoryInfo(tempThumbnailsPath);//Directoryname + "\\Thumbnails\\Temp");
            FileInfo[] FInfoLst = dir.GetFiles("*jpg", SearchOption.AllDirectories);
            try
            {
                foreach (var item in drinfo.GetFiles())
                {
                    try
                    {
                        item.Delete();
                    }
                    catch { }
                }
                foreach (FileInfo FInfo in FInfoLst)
                {
                    try
                    {
                        FInfo.Delete();
                    }
                    catch { }
                }

                if (!Directory.Exists(Path.Combine(Directoryname, "Download", "CorruptImages")))
                    Directory.CreateDirectory(Path.Combine(Directoryname, "Download", "CorruptImages"));
                ErrorHandler.ErrorHandler.LogFileWrite("File Watcher Step 14- 2. DeleteTemp Files  :" + DateTime.Now.ToString("MM / dd / yyyy HH: mm:ss"));
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                ErrorHandler.ErrorHandler.LogFileWrite("File Watcher Step 14- Catch block. DeleteTemp Files  :" + DateTime.Now.ToString("MM / dd / yyyy HH: mm:ss"));
            }
            finally
            {
                GC.Collect();
            }
        }

        private void MyWorker_RunWorkerCompleted(object Sender, System.ComponentModel.RunWorkerCompletedEventArgs e)
        {
            //if (!e.Cancelled)
            //{
            //    //label2.Content = "Complete My Background Work";
            //    CompressToMinifiedImage(0.5, "", "");
            //}
            //else
            //{
            //    // label2.Content = "Sorry it's Fail";
            //}
        }


        private void MyWorker_DoWork(object Sender, System.ComponentModel.DoWorkEventArgs e)
        {

            //var task1 = Task.Run(() => CompressToMinifiedImage(0.5, Path.Combine(_bigThumbnailFilePathDate, picname), Path.Combine(_bigThumbnailFilePathDate, picname).Replace(picname, "m_" + picname)));//Added by Vinod for SET3 implementation
            //var task2 = Task.Run(() => CompressToMinifiedImage(0.5, Path.Combine(_thumbnailFilePathDate, picname), Path.Combine(_thumbnailFilePathDate, picname).Replace(picname, "m_" + picname)));//Added by Vinod for SET3 implementation
            //var task3 = Task.Run(() => CompressToMinifiedImage(0.5, Path.Combine(DirectoryNameWithDate, picname), Path.Combine(DirectoryNameWithDate, picname).Replace(picname, "m_" + picname)));//Added by Vinod for SET3 implementation

            //CompressToMinifiedImage(0.5, Path.Combine(DirectoryNameWithDate, picNameMinified), Path.Combine(DirectoryNameWithDate, picNameMinified).Replace(picNameMinified, "m_" + picNameMinified));
            //CompressToMinifiedImage(0.25, Path.Combine(DirectoryNameWithDate, picNameMinified), Path.Combine(_thumbnailFilePathDate, picNameMinified).Replace(picNameMinified, "m_" + picNameMinified));
            //CompressToMinifiedImage(0.5, Path.Combine(DirectoryNameWithDate, picNameMinified), Path.Combine(_bigThumbnailFilePathDate, picNameMinified).Replace(picNameMinified, "m_" + picNameMinified));

            //if (MyWorker.CancellationPending)
            //{
            //    e.Cancel = true;
            //    return;
            //}           
        }

        #region SemiOrder

        /// <summary>
        /// Gets the semi order settings.
        /// </summary>
        /// <returns></returns>
        private List<SemiOrderSettings> GetSemiOrderSettings(int substoreId, int LocationId)
        {
            try
            {
                List<SemiOrderSettings> objDG_SemiOrder_Settings = new List<SemiOrderSettings>();

                if (lstDG_SemiOrder_Settings != null && lstDG_SemiOrder_Settings.Count > 0)
                {
                    objDG_SemiOrder_Settings = lstDG_SemiOrder_Settings.Where(x => x.DG_LocationId == LocationId).ToList();
                }
                else
                {
                    lstDG_SemiOrder_Settings = (new SemiOrderBusiness()).GetSemiOrderSetting(null, 0);
                    objDG_SemiOrder_Settings = lstDG_SemiOrder_Settings.Where(x => x.DG_LocationId == LocationId).ToList();
                }
                return objDG_SemiOrder_Settings;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        private void SaveSpecPrintEffectsIntoDb(int photoId, int substoreId, int locationId, SemiOrderSettings objDG_SemiOrder_Settings, string dateFolderPath
            , string fileName, string GumBallRidetxtContent)
        {
            //objDigiPhotoDataServices = new DigiPhotoDataServices();
            PhotoBusiness phBusiness = new PhotoBusiness();
            if (objDG_SemiOrder_Settings.DG_SemiOrder_Settings_AutoBright == false && !string.IsNullOrEmpty(defaultBrightness))
            {
                objDG_SemiOrder_Settings.DG_SemiOrder_Settings_AutoBright_Value = Convert.ToDouble(defaultBrightness);
            }

            if (objDG_SemiOrder_Settings.DG_SemiOrder_Settings_AutoContrast == false && !string.IsNullOrEmpty(defaultBrightness))
            {
                objDG_SemiOrder_Settings.DG_SemiOrder_Settings_AutoContrast_Value = Convert.ToDouble(defaultContrast);
            }
            //int prdId = 0;
            //if (objDG_SemiOrder_Settings.DG_SemiOrder_ProductTypeId.Contains(','))
            //    prdId = 1;
            //else
            //    prdId = Convert.ToInt32(objDG_SemiOrder_Settings.DG_SemiOrder_ProductTypeId);

            //if ((bool)objDG_SemiOrder_Settings.DG_SemiOrder_Settings_IsImageBG)
            //{
            //    //phBusiness.SaveIsGreenPhotos(photoId, true);
            //    SaveXml(Convert.ToString(objDG_SemiOrder_Settings.DG_SemiOrder_Settings_AutoBright_Value),
            //        Convert.ToString(objDG_SemiOrder_Settings.DG_SemiOrder_Settings_AutoContrast_Value),
            //        Convert.ToString(objDG_SemiOrder_Settings.DG_SemiOrder_Settings_ImageFrame),
            //        objDG_SemiOrder_Settings.DG_SemiOrder_Settings_ImageFrame_Vertical,
            //        objDG_SemiOrder_Settings.ProductName,
            //        objDG_SemiOrder_Settings.DG_SemiOrder_BG, objDG_SemiOrder_Settings.DG_SemiOrder_Graphics_layer,
            //        photoId, objDG_SemiOrder_Settings.DG_SemiOrder_Image_ZoomInfo,
            //        (bool)objDG_SemiOrder_Settings.DG_SemiOrder_IsCropActive,
            //        prdId, dateFolderPath, true, fileName, GumBallRidetxtContent, objDG_SemiOrder_Settings.TextLogos);
            //}
            //else
            //{
            //    SaveXml(Convert.ToString(objDG_SemiOrder_Settings.DG_SemiOrder_Settings_AutoBright_Value),
            //        Convert.ToString(objDG_SemiOrder_Settings.DG_SemiOrder_Settings_AutoContrast_Value),
            //        Convert.ToString(objDG_SemiOrder_Settings.DG_SemiOrder_Settings_ImageFrame),
            //        objDG_SemiOrder_Settings.DG_SemiOrder_Settings_ImageFrame_Vertical,
            //        objDG_SemiOrder_Settings.ProductName, string.Empty, objDG_SemiOrder_Settings.DG_SemiOrder_Graphics_layer,
            //        photoId, objDG_SemiOrder_Settings.DG_SemiOrder_Image_ZoomInfo,
            //        (bool)objDG_SemiOrder_Settings.DG_SemiOrder_IsCropActive,
            //        prdId, dateFolderPath, false, fileName, GumBallRidetxtContent, objDG_SemiOrder_Settings.TextLogos);
            //}

            SaveXml(objDG_SemiOrder_Settings, photoId, dateFolderPath, fileName, GumBallRidetxtContent);

        }

        #endregion

        #region watcher
        private void CopyImgToDisplayFolder(string imgSourceFilePath, string RFID, Int32 photoId)
        {
            ImgCopyLocation targetFolderLocation = imgCopyLocationlst.Where(s => s.locID == LocationID).FirstOrDefault();
            if (targetFolderLocation.counter <= targetFolderLocation.range)
            {
                screen = "Display" + targetFolderLocation.counter;
                imgCopyLocationlst.Where(s => s.locID == LocationID).ToList().ForEach(s => s.counter++);
            }
            else
            {
                screen = "Display1";
                imgCopyLocationlst.Where(s => s.locID == LocationID).ToList().ForEach(s => s.counter = 2);
            }
            if (Directory.Exists(_previewWallSubStore + "\\" + _locationName + "\\" + screen))
            {
                _copyFolderName = Path.Combine(_previewWallSubStore, _locationName, screen, (RFID + "@" + photoId + ".jpg"));
                File.Copy(imgSourceFilePath, _copyFolderName, true);
            }
        }

        string ImageEffect;
        List<string> _objFileList = new List<string>();
        int needrotaion = 0;
        /// <summary>
        /// Gets the rotation value.
        /// </summary>
        /// <param name="orientation">The orientation.</param>
        /// <returns></returns>
        private static int GetRotationValue(string orientation)
        {
            switch (int.Parse(orientation))
            {
                case 1:
                    return 0;

                case 2:
                    return 0;

                case 3:
                    return 180;

                case 4:
                    return 180;

                case 5:
                    return 90;

                case 6:
                    return 90;

                case 7:
                    return 270;

                case 8:
                    return 270;

                default:
                    return 0;
            }
        }

        private void rotateImageLocationWise(string rotatePath)
        {
            try
            {
                ImageMagickObject.MagickImage jmagic = new ImageMagickObject.MagickImage();
                object[] o = new object[] { "-rotate", System.Configuration.ConfigurationManager.AppSettings["RotationAngle"], rotatePath };
                jmagic.Mogrify(o);
                o = null;
                isrotated = true;
                jmagic = null;
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
            }
        }

        //string FileNameDownload = string.Empty;
        bool IsFrame = false;
        private void eventRaised(FileInfo file)
        {
#if DEBUG
            ErrorHandler.ErrorHandler.LogFileWrite("File Watcher Step 19. eventRaised :" + DateTime.Now.ToString("MM / dd / yyyy HH: mm:ss"));
            var watch = FrameworkHelper.CommonUtility.Watch();
            watch.Start();
#endif
#if DEBUG   
            var watch1 = FrameworkHelper.CommonUtility.Watch();
            watch1.Start();
#endif
            IsFrame = false;
            GumBallRidetxtContent = string.Empty;
            ErrorHandler.ErrorHandler.LogFileWrite("File Watcher Step 20. eventRaised :" + DateTime.Now.ToString("MM / dd / yyyy HH: mm:ss"));
          
            string _mediaType = string.Empty;
            if (file.Name.Contains('#'))
            {
                _mediaType = file.Name;
                _mediaType = _mediaType.Split('#')[1].Split('@')[0];
                IsFrame = true;
            }
            DateTime? CaptureDate = new DateTime();
            ErrorHandler.ErrorHandler.LogFileWrite("File Watcher Step 21. eventRaised :" + DateTime.Now.ToString("MM / dd / yyyy HH: mm:ss"));
            bool isResetDPIRequired = false;
            PhotoGrapherId = Convert.ToInt32(file.Name.Split('@')[1].ToString().Split('.')[0]);
            //Get cahced data if available otherwise load from DB
            //ErrorHandler.ErrorHandler.LogFileWrite("FillUserData started ------------- " + PhotoGrapherId);
            FillUserData(PhotoGrapherId);
            ErrorHandler.ErrorHandler.LogFileWrite("File Watcher Step 22. eventRaised :" + DateTime.Now.ToString("MM / dd / yyyy HH: mm:ss"));
            //ErrorHandler.ErrorHandler.LogFileWrite("FillUserData End -------------- " + PhotoGrapherId);
            GumBallRideFolderPath = Path.Combine(Directoryname, "GumBallRide", _subStoreName, _locationName, DateTime.Now.ToString("yyyyMMdd"));
            
            if (!Directory.Exists(GumBallRideFolderPath))
                Directory.CreateDirectory(GumBallRideFolderPath);
            
            FileInfo txtFile = new FileInfo(Path.Combine(DownloadFolderPath, _locationName, file.Name.Split('.')[0] + ".txt"));
            PhotoBusiness phBusiness = new PhotoBusiness();
            string picname = string.Empty; //made global by Vinod_11May2020
            try
            {
                string CameraManufacture = "'" + "##" + "'";
                string CameraModel = "'" + "##" + "'";
                string orientation = "'" + "##" + "'";
                string HorizontalResolution = "'" + "##" + "'";
                string VerticalResolution = "'" + "##" + "'";
                string Datetaken = "'" + "##" + "'";
                string dimension = "'" + "##" + "'";
                string ISOSpeedRatings = "'" + "##" + "'";
                string ExposureMode = "'" + "##" + "'";
                string Sharpness = "'" + "##" + "'";
                string GPSLatitude = "'" + "##" + "'";
                string GPSLongitude = "'" + "##" + "'";
                string ExposureTime = "'" + "##" + "'";         //Shutter speed
                string ApertureValue = "'" + "##" + "'";
                ErrorHandler.ErrorHandler.LogFileWrite("File Watcher Step 23. eventRaised :" + DateTime.Now.ToString("MM / dd / yyyy HH: mm:ss"));
                ErrorHandler.ErrorHandler.LogFileWrite("Watcher Started for " + file.Name);
                if (file.Name != "Thumbs.db")
                {
                    ErrorHandler.ErrorHandler.LogFileWrite("637");
                    _objmetdata = new ReadImageMetaData();

                    //Get cached data
                    GetConfigData(substoreId);
                    //Get cahced data if available otherwise load from DB
                    SetNewConfigLocationValues(LocationID);
                    //Get cahced data if available otherwise load from DB
                    List<SemiOrderSettings> lstobjDG_SemiOrder_Settings = new List<SemiOrderSettings>();
                    if (is_SemiOrder)
                        lstobjDG_SemiOrder_Settings = GetSemiOrderSettings(substoreId, LocationID);
                    else
                        lstobjDG_SemiOrder_Settings = null;
                    ErrorHandler.ErrorHandler.LogFileWrite("File Watcher Step 24. eventRaised :" + DateTime.Now.ToString("MM / dd / yyyy HH: mm:ss"));
                    if (_IsAdvancedVideoEditActive)
                        isVideoProcessed = false;
                    else if (isAutoVideoProcessingActive)
                        isVideoProcessed = false;
                    else isVideoProcessed = true;

                    if (IsGreenScreenWorkFlow)
                        isVideoProcessed = true;
                    //here i will go to loop 
                    try
                    {
                        int photoCount = phBusiness.PhotoCountCurrentDate(RFID: file.Name.Split('@')[0].ToString(), photographerID: Convert.ToInt32(file.Name.Split('@')[1].ToString().Split('.')[0]), mediaType: 1);
                        ErrorHandler.ErrorHandler.LogFileWrite("File Watcher Step 25. eventRaised :" + DateTime.Now.ToString("MM / dd / yyyy HH: mm:ss"));
                        if ((lstobjDG_SemiOrder_Settings == null || lstobjDG_SemiOrder_Settings.Count() == 0) || (IsFrame & _mediaType == "3"))
                        {
                            ImageEffect = "<image brightness = '" + defaultBrightness + "' contrast = '" + defaultContrast + "' Crop='##' colourvalue = '##' rotate='##' ><effects sharpen='##' greyscale='0' digimagic='0' sepia='0' defog='0' underwater='0' emboss='0' invert = '0' granite='0' hue ='##' cartoon = '0'></effects></image>";
                            CustomBusineses custObj = new CustomBusineses();
                            DateTime curDate = custObj.ServerDateTime();
                            //string picname = string.Empty;
                            //if (photoCount > 0)
                            //    picname = curDate.Day.ToString() + curDate.Month.ToString() + file.Name.Split('@')[0].ToString() + "(" + photoCount + ")" + ".jpg";
                            //else
                            //    picname = curDate.Day.ToString() + curDate.Month.ToString() + file.Name.Split('@')[0].ToString() + ".jpg";

                            if (photoCount > 0)
                            {
                                //picname = curDate.Day.ToString() + curDate.Month.ToString() + file.Name.Split('@')[0].ToString() + "(" + photoCount + ")" + ".jpg";
                                //Added by Vins_19Feb2019  to resolve same photo series issue for different location
                                picname = curDate.Day.ToString() + curDate.Month.ToString() + Convert.ToString(PhotoGrapherId) + file.Name.Split('@')[0].ToString() + "(" + photoCount + ")" + ".jpg";
                                ErrorHandler.ErrorHandler.LogFileWrite("File Watcher Step 26. eventRaised IF True :" + DateTime.Now.ToString("MM / dd / yyyy HH: mm:ss"));
                            }
                            else
                            {
                                //picname = curDate.Day.ToString() + curDate.Month.ToString() + file.Name.Split('@')[0].ToString() + ".jpg"; //Commented by Vins
                                //Added by Vins_19Feb2019  to resolve same photo series issue for different location
                                picname = curDate.Day.ToString() + curDate.Month.ToString() + Convert.ToString(PhotoGrapherId) + file.Name.Split('@')[0].ToString() + ".jpg";
                                ErrorHandler.ErrorHandler.LogFileWrite("File Watcher Step 26. eventRaised IF False:" + DateTime.Now.ToString("MM / dd / yyyy HH: mm:ss"));
                            }

                            if (picname.Contains('#'))
                            {
                                picname = picname = picname.Split('#')[0] + "_" + _mediaType + ".jpg";
                                isVideoProcessed = true;
                                ErrorHandler.ErrorHandler.LogFileWrite("698");
                            }
                            string filename = file.Name.Split('@')[0].ToString();
                            string rfid = file.Name.Split('@')[0].ToString();
                            if (filename.Contains('_'))
                            {
                                filename = filename.Split('_')[0].ToString();
                                rfid = filename.Split('_')[0].ToString();
                                isVideoProcessed = true;
                            }
                            ErrorHandler.ErrorHandler.LogFileWrite("File Watcher Step 27. eventRaised :" + DateTime.Now.ToString("MM / dd / yyyy HH: mm:ss"));
                            string txtFileName = curDate.Day.ToString() + curDate.Month.ToString() + txtFile.Name.Split('@')[0].ToString() + ".txt";
                          
                            string renderedTag = string.Empty;
                            try
                            {
                                #region Image Meta Data
                                ExifTagCollection exif = new ExifTagCollection(file.FullName);

                                #region Auto-Rotate
                                //Start:Code for Auto-Rotate
                                ExifReader reader = null;
                                //string renderedTag = "";
                                try
                                {
                                    reader = new ExifReader(file.FullName);
                                    foreach (ushort tagID in Enum.GetValues(typeof(ExifTags)))
                                    {
                                        object val;
                                        if (reader.GetTagValue(tagID, out val))
                                        {
                                            renderedTag = val.ToString();
                                        }
                                    }
                                }
                                catch (Exception ex)
                                {
                                    ErrorHandler.ErrorHandler.LogError(ex);
                                }
                                finally
                                {
                                    if (reader != null)
                                        reader.Dispose();
                                }
                                //FileNameDownload = Directoryname + "\\Download\\" + file.Name;
                                if (!string.IsNullOrEmpty(renderedTag))
                                {
                                    needrotaion = GetRotationValue(renderedTag);
                                    isrotated = true;
                                }
                                else
                                {
                                    needrotaion = 0;
                                    isrotated = false;
                                }
                                //End:Code for Auto-Rotate
                                #endregion

                                if (exif.Where(o => o.Id == 36867).Count() > 0)
                                {
                                    try
                                    {
                                        System.Globalization.DateTimeFormatInfo objFormat = new System.Globalization.DateTimeFormatInfo();
                                        objFormat.ShortDatePattern = @"yyyy/MM/dd HH:mm:ss";
                                        string datePart = exif[36867].Value.Split(' ').First();
                                        datePart = datePart.Replace(':', '/');
                                        string timePart = exif[36867].Value.Split(' ').Last();
                                        CaptureDate = Convert.ToDateTime(datePart + " " + timePart, objFormat);
                                    }
                                    catch (Exception ex)
                                    {
                                        CaptureDate = null;
                                        ErrorHandler.ErrorHandler.LogError(ex);
                                    }
                                }
                                else if (file.CreationTime != null)
                                {
                                    try
                                    {
                                        System.Globalization.DateTimeFormatInfo objFormat = new System.Globalization.DateTimeFormatInfo();
                                        objFormat.ShortDatePattern = @"yyyy/MM/dd HH:mm:ss";
                                        string datePart = Convert.ToString(file.CreationTime.Date).Split(' ').First();
                                        datePart = datePart.Replace(':', '/');
                                        string timePart = Convert.ToString(file.CreationTime.TimeOfDay);
                                        CaptureDate = Convert.ToDateTime(datePart + " " + timePart, objFormat);
                                    }
                                    catch (Exception ex)
                                    {
                                        CaptureDate = null;
                                        string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                                        ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                                    }
                                }
                                else
                                    CaptureDate = null;

                                foreach (LevDan.Exif.ExifTag tag in exif)
                                {
                                    if (tag.Id == 271)
                                    {
                                        CameraManufacture = "'" + exif[271].Value + "'";
                                    }
                                    else if (tag.Id == 272)
                                    {
                                        CameraModel = "'" + exif[272].Value + "'";
                                    }
                                    else if (tag.Id == 274)
                                    {
                                        orientation = "'" + exif[274].Value + "'";
                                    }
                                    else if (tag.Id == 282)
                                    {
                                        HorizontalResolution = "'" + exif[282].Value + "'";
                                    }
                                    else if (tag.Id == 283)
                                    {
                                        VerticalResolution = "'" + exif[283].Value + "'";
                                    }
                                    else if (tag.Id == 36867)
                                    {
                                        Datetaken = "'" + exif[36867].Value + "'";
                                    }
                                    else if (tag.Id == 40962 || tag.Id == 40963)
                                    {
                                        dimension = "'" + exif[40962].Value + " x " + exif[40963].Value + "'";
                                    }
                                    else if (tag.Id == 34855)
                                    {
                                        ISOSpeedRatings = "'" + exif[34855].Value + "'";
                                    }
                                    else if (tag.Id == 41986)
                                    {
                                        ExposureMode = "'" + exif[41986].Value + "'";
                                    }
                                    else if (tag.Id == 41994)
                                    {
                                        Sharpness = "'" + exif[41994].Value + "'";
                                    }
                                    else if (tag.Id == 2)
                                    {
                                        GPSLatitude = "'" + ConvertDegreeAngleToDouble(tag.Value.Split(' ')[0].Replace('°', ' ').Trim(), tag.Value.Split(' ')[1].Replace('\'', ' ').Trim(), tag.Value.Split(' ')[2].Replace('\"', ' ').Trim()) + "'";
                                    }
                                    else if (tag.Id == 4)
                                    {
                                        GPSLongitude = "'" + ConvertDegreeAngleToDouble(tag.Value.Split(' ')[0].Replace('°', ' ').Trim(), tag.Value.Split(' ')[1].Replace('\'', ' ').Trim(), tag.Value.Split(' ')[2].Replace('\"', ' ').Trim()) + "'";
                                    }
                                    else if (tag.Id == 33434)
                                    {
                                        ExposureTime = "'" + exif[33434].Value + "'";
                                    }
                                    else if (tag.Id == 37381)
                                    {
                                        ApertureValue = "'" + exif[37381].Value + "'";
                                    }
                                }
                                ImageMetaData = "<image Dimensions=" + dimension + " CameraManufacture=" + CameraManufacture + " HorizontalResolution=" + HorizontalResolution + " VerticalResolution=" + VerticalResolution + " CameraModel=" + CameraModel + " ISO-SpeedRating=" + ISOSpeedRatings + " DateTaken=" + Datetaken + " ExposureMode=" + ExposureMode + " Sharpness=" + Sharpness + " Orientation=" + orientation + " GPSLatitude=" + GPSLatitude + " GPSLongitude=" + GPSLongitude + " ExposureTime=" + ExposureTime + " ApertureValue=" + ApertureValue + " ></image>";
                            }
                            catch (System.ArgumentException aex)
                            {
                                file.MoveTo(Path.Combine(Directoryname, "Download", "CorruptImages", file.Name));
                                if (txtFile.Exists)
                                    txtFile.MoveTo(Path.Combine(Directoryname, "Download", "CorruptImages", txtFile.Name));
                                return;
                            }
                            catch (Exception ex)
                            {
                                ErrorHandler.ErrorHandler.LogError(ex);
                                ImageMetaData = "<image Dimensions=" + "##" + " CameraManufacture=" + "##" + " HorizontalResolution=" + "##" + " VerticalResolution=" + "##" + " CameraModel=" + "##" + " ISO-SpeedRating=" + "##" + " DateTaken=" + "##" + " ExposureMode=" + "##" + " Sharpness=" + "##" + " Orientation=" + "##" + " GPSLatitude=##" + " GPSLongitude=##" + " ExposureTime=##" + " ApertureValue=##" + "></image>";
                            }
                            ErrorHandler.ErrorHandler.LogFileWrite("File Watcher Step 28. eventRaised :" + DateTime.Now.ToString("MM / dd / yyyy HH: mm:ss"));
                            #endregion

                            bool IsImageCorrupt = false;
                            HorizontalResolution = HorizontalResolution.Replace("'", "");
                            VerticalResolution = VerticalResolution.Replace("'", "");
                            Int32 HorizontalDPI = 0;
                            Int32 VerticalDPI = 0;
                            ErrorHandler.ErrorHandler.LogFileWrite("875");
                            if (HorizontalResolution.Contains("##") || VerticalResolution.Contains("##"))
                            {
                                BitmapImage testbmp = new BitmapImage();
                                using (FileStream fileStream = File.OpenRead(file.FullName))
                                {
                                    MemoryStream ms = new MemoryStream();
                                    fileStream.CopyTo(ms);
                                    ms.Seek(0, SeekOrigin.Begin);
                                    fileStream.Close();
                                    testbmp.BeginInit();
                                    testbmp.StreamSource = ms;
                                    testbmp.EndInit();
                                    testbmp.Freeze();
                                }
                                HorizontalDPI = Convert.ToInt32(testbmp.DpiX);
                                VerticalDPI = Convert.ToInt32(testbmp.DpiY);
                                ErrorHandler.ErrorHandler.LogFileWrite("892");
                            }
                            else
                            {
                                //Imagica India was returning HorizontalResolution - 0.72 and VerticalResolution - 0.72
                                if (!Int32.TryParse(HorizontalResolution, out HorizontalDPI))
                                    HorizontalDPI = 72;

                                if (!Int32.TryParse(VerticalResolution, out VerticalDPI))
                                    VerticalDPI = 72;
                                ErrorHandler.ErrorHandler.LogFileWrite("902");
                            }

                            ErrorHandler.ErrorHandler.LogFileWrite("File Watcher Step 29. eventRaised :" + DateTime.Now.ToString("MM / dd / yyyy HH: mm:ss"));
                            
                            if (ConfigurationManager.AppSettings.AllKeys.Contains("LocationIds"))
                            {
                                string[] LocationIds = System.Configuration.ConfigurationManager.AppSettings["LocationIds"].Split(',');
                                // ErrorHandler.ErrorHandler.LogFileWrite("LocationID1: " + LocationID.ToString());
                                if (LocationIds.Contains(LocationID.ToString()))
                                    rotateImageLocationWise(file.FullName);
                                else
                                    RotateImage(file.FullName);
                            }
                            else
                                RotateImage(file.FullName);

                            ErrorHandler.ErrorHandler.LogFileWrite("File Watcher Step 30. eventRaised :" + DateTime.Now.ToString("MM / dd / yyyy HH: mm:ss"));
                            isResetDPIRequired = CheckResetDPIRequired(HorizontalDPI, VerticalDPI, Convert.ToString(PhotoGrapherId));

                            if (isResetDPIRequired)
                                ResetLowDPI(file.FullName, Path.Combine(DirectoryNameWithDate, picname));
                            else if (IsAutoColorCorrectionActive && IsCorrectionAtDownloadActive)
                                AutoCorrectImageWIthCopy(file.FullName, Path.Combine(DirectoryNameWithDate, picname));
                            else
                            {
                                file.CopyTo(Path.Combine(DirectoryNameWithDate, picname), true);
                                ErrorHandler.ErrorHandler.LogFileWrite("930");
                               // file.CopyTo(Path.Combine(DirectoryNameWithDate, "m_" + picname), true); //minified image original one
                            }
                            ErrorHandler.ErrorHandler.LogFileWrite("File Watcher Step 31. eventRaised :" + DateTime.Now.ToString("MM / dd / yyyy HH: mm:ss"));
                            if (txtFile.Exists)
                            {
                                try
                                {
                                    gumballScoreSeperater = GetgumballScoreSeperater();
                                    //if (ConfigurationManager.AppSettings.AllKeys.Contains("GumballScoreSeperater"))
                                    //    gumballScoreSeperater = Convert.ToChar(ConfigurationManager.AppSettings["GumballScoreSeperater"].ToString());
                                    gumballScore = File.ReadAllText(txtFile.FullName);
                                    GumBallRidetxtContent = gumballScore.Replace(gumballScoreSeperater, ",");
                                    //GumBallRidetxtContent = File.ReadAllText(txtFile.FullName);
                                    if (!File.Exists(Path.Combine(GumBallRideFolderPath, txtFileName)))
                                        txtFile.MoveTo(Path.Combine(GumBallRideFolderPath, txtFileName));
                                    else
                                    {
                                        File.Delete(Path.Combine(GumBallRideFolderPath, txtFileName));
                                        txtFile.MoveTo(Path.Combine(GumBallRideFolderPath, txtFileName));
                                    }
                                }
                                catch (Exception ex)
                                {
                                    string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                                    ErrorHandler.ErrorHandler.LogFileWrite(errorMessage + txtFile.Name);
                                }
                            }
                            isResetDPIRequired = false;
                            ErrorHandler.ErrorHandler.LogFileWrite("File Watcher Step 32. eventRaised :" + DateTime.Now.ToString("MM / dd / yyyy HH: mm:ss"));
                            if (is_SemiOrder && objDG_SemiOrder_Settings != null)
                            {
                                ErrorHandler.ErrorHandler.LogFileWrite("962");
                                //RotateImageIfRequired(Path.Combine(DirectoryNameWithDate, picname));
                            }
                            else
                            {
                                ErrorHandler.ErrorHandler.LogFileWrite("967");
                                //IsImageCorrupt = ResizeWPFImage(Path.Combine(DirectoryNameWithDate, picname), 210, Path.Combine(_thumbnailFilePathDate, picname), Path.Combine(DirectoryNameWithDate, picname));
                                IsImageCorrupt = CompressToMinifiedImage(0.25, Path.Combine(DirectoryNameWithDate, picname), Path.Combine(_thumbnailFilePathDate, picname), string.Empty);
                                if (IsImageCorrupt)
                                {
                                    ErrorHandler.ErrorHandler.LogFileWrite(file.Name + " is corrupt image and is moved to CorruptImages folder");
                                    File.Delete(Path.Combine(DirectoryNameWithDate, picname));
                                    file.MoveTo(Path.Combine(Directoryname, "Download", "CorruptImages", file.Name));
                                    if (File.Exists(Path.Combine(GumBallRideFolderPath, txtFileName)))
                                        File.Delete(Path.Combine(GumBallRideFolderPath, txtFileName));
                                    return;
                                }
                                //ResizeWPFImage(Path.Combine(DirectoryNameWithDate, picname), 1200, Path.Combine(_bigThumbnailFilePathDate, picname));
                                CompressToMinifiedImage(0.50, Path.Combine(DirectoryNameWithDate, picname), Path.Combine(_bigThumbnailFilePathDate, picname), Path.Combine(_minifiedFilePathDate, "m_" + picname));
                            }
                            ErrorHandler.ErrorHandler.LogFileWrite("File Watcher Step 33. eventRaised :" + DateTime.Now.ToString("MM / dd / yyyy HH: mm:ss"));

                            LocationBusniess locBuiness = new LocationBusniess();

                            photoId = phBusiness.SetPhotoDetails(substoreId, filename, picname.ToString(), curDate, file.Name.Split('@')[1].ToString().Split('.')[0], ImageMetaData, LocationID, "test", ImageEffect, CaptureDate, RfidScanType, 1, (new CharacterBusiness()).GetCharacterId(file.Name.Split('@')[1].ToString().Split('.')[0]), 0, isVideoProcessed, 1, -1, picname.ToString(), 0);
                            string imgPath = Path.Combine(DirectoryNameWithDate, picname);
                            ErrorHandler.ErrorHandler.LogFileWrite("File Watcher Step 34. eventRaised :" + DateTime.Now.ToString("MM / dd / yyyy HH: mm:ss"));
                            if (IsMobileRfidEnabled)
                            {
                                MobileRfidAssociationCheck(photoId, Path.GetFileNameWithoutExtension(file.FullName));
                            }
                            ErrorHandler.ErrorHandler.LogFileWrite("File Watcher Step 35. eventRaised :" + DateTime.Now.ToString("MM / dd / yyyy HH: mm:ss"));
                            if (IsBarcodeActive)
                            {
                                BarcodeAssociationCheck(photoId, imgPath);
                            }
                            ErrorHandler.ErrorHandler.LogFileWrite("File Watcher Step 36. eventRaised :" + DateTime.Now.ToString("MM / dd / yyyy HH: mm:ss"));
                            bool isCodeType = IsCodeType(photoId);
                            if (is_SemiOrder && objDG_SemiOrder_Settings != null && !isCodeType)
                            {
                                if (_mediaType != "3")
                                    SaveSpecPrintEffectsIntoDb(photoId, substoreId, LocationID, objDG_SemiOrder_Settings, DirectoryNameWithDate, picname, GumBallRidetxtContent);
                            }
                            ErrorHandler.ErrorHandler.LogFileWrite("File Watcher Step 37. eventRaised :" + DateTime.Now.ToString("MM / dd / yyyy HH: mm:ss"));
                            var lstconfig = _lstLocationWiseConfigParams.Where(s => s.LocationId == LocationID && s.IMIXConfigurationMasterId == Convert.ToInt64(ConfigParams.IsPreviewEnabledPW)
                                               && s.ConfigurationValue.ToUpper() == "TRUE").ToList();
                            if (lstconfig != null && lstconfig.Count() > 0)
                            {
                                CopyImgToDisplayFolder(Path.Combine(DirectoryNameWithDate, picname), filename, photoId);
                            }
                            ErrorHandler.ErrorHandler.LogFileWrite("File Watcher Step 38. eventRaised :" + DateTime.Now.ToString("MM / dd / yyyy HH: mm:ss"));
                             
                            try
                            {
                                //DispatcherFrame frame = new DispatcherFrame();
                                //mainImage.Dispatcher.BeginInvoke(
                                //    System.Windows.Threading.DispatcherPriority.Input
                                //    , new System.Windows.Threading.DispatcherOperationCallback(delegate
                                //    {
                                //        FileInfo infofile = new FileInfo(Path.Combine(DirectoryNameWithDate, picname));
                                //        infofile.CopyTo(Directoryname + "\\Thumbnails\\Temp\\" + picname, true);
                                //        mainImage.Source =
                                //             new BitmapImage
                                //                 (new Uri(Directoryname + "\\Thumbnails\\Temp\\" + picname));
                                //        frame.Continue = false;
                                //        counter++;
                                //        UpdateTotalCounter();
                                //        return null;

                                //    }), null);
                                //Dispatcher.PushFrame(frame);
                                //if (this.Dispatcher.HasShutdownFinished)
                                //{

                                //}
                                UpdateTotalCounter();
                            }
                            catch (IOException IOex)
                            {
                                string errorMessage = "Got IO Exception after Dispatcher Frame for file " + file.FullName;
                                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                                for (int i = 0; i < FilesToDelete.Count; i++)
                                {
                                    if (File.Exists(FilesToDelete[i]))
                                    {
                                        File.Move(FilesToDelete[i], Path.Combine(ArchivedFolderPath, Path.GetFileName(FilesToDelete[i])));
                                        //File.Delete(FilesToDelete[i]);
                                        FilesToDelete.RemoveAt(i);
                                    }
                                }
                                FilesToDelete.Add(file.FullName.ToLower());
                            }
                            catch (Exception ex)
                            {
                                ErrorHandler.ErrorHandler.LogError(ex);
                            }

                            ErrorHandler.ErrorHandler.LogFileWrite("File Watcher Step 39. eventRaised :" + DateTime.Now.ToString("MM / dd / yyyy HH: mm:ss"));

                            if (is_SemiOrder)
                            {
                                if (objDG_SemiOrder_Settings != null && !isCodeType)
                                {
                                    if (is_SemiOrder == true && objDG_SemiOrder_Settings.DG_SemiOrder_Environment == true)
                                    {

                                    }
                                    else if (is_SemiOrder == true && objDG_SemiOrder_Settings.DG_SemiOrder_Environment == false && (bool)objDG_SemiOrder_Settings.DG_SemiOrder_IsPrintActive)
                                    {
                                        string[] prdIdList = objDG_SemiOrder_Settings.DG_SemiOrder_ProductTypeId.Split(',');
                                        foreach (string prodList in prdIdList)
                                        {
                                            string SyncCode = CommonUtility.GetUniqueSynccode(Convert.ToInt32(ApplicationObjectEnum.OrderDetails).ToString().PadLeft(2, '0'), LoginUser.countrycode, LoginUser.Storecode, substoreId.ToString());
                                            int OrderDetailId = (new OrderBusiness()).AddSemiOrderDetails(photoId, prodList, LocationID, SyncCode, false, substoreId);
                                        }
                                    }
                                }
                            }
                            ErrorHandler.ErrorHandler.LogFileWrite("File Watcher Step 40. eventRaised :" + DateTime.Now.ToString("MM / dd / yyyy HH: mm:ss"));
                        }
                        else
                        {

                            //semiorder
                            if (IsGreenScreenWorkFlow)
                                isVideoProcessed = true;
                            //string picname = string.Empty; //Commented by Vinod_11May2020
                            string txtFileName = string.Empty;
                            ErrorHandler.ErrorHandler.LogFileWrite("1083");
                            bool isRotatedFirstTime = false; //flag to check if already rotated
                            foreach (SemiOrderSettings objDG_SemiOrder_Settings in lstobjDG_SemiOrder_Settings)
                            {
                                imagenameCntr = imagenameCntr + 1;
                                ImageEffect = "<image brightness = '" + defaultBrightness + "' contrast = '" + defaultContrast + "' Crop='##' colourvalue = '##' rotate='##' ><effects sharpen='##' greyscale='0' digimagic='0' sepia='0' defog='0' underwater='0' emboss='0' invert = '0' granite='0' hue ='##' cartoon = '0'></effects></image>";
                                CustomBusineses custObj = new CustomBusineses();
                                DateTime curDate = custObj.ServerDateTime();
                                if (string.IsNullOrEmpty(picname))
                                    if (photoCount > 0)
                                        picname = curDate.Day.ToString() + curDate.Month.ToString() + file.Name.Split('@')[0].ToString() + "(" + photoCount + ")" + ".jpg";
                                    else
                                        picname = curDate.Day.ToString() + curDate.Month.ToString() + file.Name.Split('@')[0].ToString() + ".jpg";
                                if (picname.Contains('#'))
                                {
                                    picname = picname = picname.Split('#')[0] + "_" + _mediaType + ".jpg";
                                    IsFrame = true;
                                    isVideoProcessed = true;
                                }
                                string filename = file.Name.Split('@')[0].ToString();
                                string rfid = file.Name.Split('@')[0].ToString();
                                if (filename.Contains('_'))
                                {
                                    filename = filename.Split('_')[0].ToString();
                                    rfid = filename.Split('_')[0].ToString();
                                    isVideoProcessed = true;
                                }
                                if (string.IsNullOrEmpty(txtFileName))
                                    txtFileName = curDate.Day.ToString() + curDate.Month.ToString() + txtFile.Name.Split('@')[0].ToString() + ".txt";
                                ErrorHandler.ErrorHandler.LogFileWrite("Watcher Started for " + DateTime.Now.ToString());
                                ErrorHandler.ErrorHandler.LogFileWrite("1113");
                                string renderedTag = "";
                                try
                                {
                                    #region Image Meta Data
                                    ExifTagCollection exif = new ExifTagCollection(file.FullName);
                                    ErrorHandler.ErrorHandler.LogFileWrite("1119");
                                    #region Auto-Rotate
                                    //Start:Code for Auto-Rotate
                                    ExifReader reader = null;
                                    //string renderedTag = "";
                                    try
                                    {
                                        reader = new ExifReader(file.FullName);
                                        ErrorHandler.ErrorHandler.LogFileWrite("1127");
                                        foreach (ushort tagID in Enum.GetValues(typeof(ExifTags)))
                                        {
                                            object val;
                                            if (reader != null)
                                            {
                                                if (reader.GetTagValue(tagID, out val))
                                                {
                                                    renderedTag = val.ToString();
                                                    ErrorHandler.ErrorHandler.LogFileWrite("1136");
                                                }
                                            }
                                        }
                                    }
                                    catch (Exception ex)
                                    {
                                        ErrorHandler.ErrorHandler.LogError(ex);
                                    }
                                    finally
                                    {
                                        if (reader != null)
                                            reader.Dispose();
                                    }
                                    ErrorHandler.ErrorHandler.LogFileWrite("1150");
                                    //FileNameDownload = Directoryname + "\\Download\\" + file.Name;
                                    if (!string.IsNullOrEmpty(renderedTag))
                                    {
                                        needrotaion = GetRotationValue(renderedTag);
                                        //ErrorHandler.ErrorHandler.LogError("need rotation = " + Convert.ToString(needrotaion));
                                        //ErrorHandler.ErrorHandler.LogError("renderedTag = " + Convert.ToString(renderedTag));
                                        isrotated = true;
                                    }
                                    else
                                    {
                                        needrotaion = 0;
                                        isrotated = false;
                                    }
                                    //End:Code for Auto-Rotate
                                    #endregion
                                    ErrorHandler.ErrorHandler.LogFileWrite("1166");
                                    if (exif.Where(o => o.Id == 36867).Count() > 0)
                                    {
                                        try
                                        {
                                            System.Globalization.DateTimeFormatInfo objFormat = new System.Globalization.DateTimeFormatInfo();
                                            objFormat.ShortDatePattern = @"yyyy/MM/dd HH:mm:ss";
                                            string datePart = exif[36867].Value.Split(' ').First();
                                            datePart = datePart.Replace(':', '/');
                                            string timePart = exif[36867].Value.Split(' ').Last();
                                            CaptureDate = Convert.ToDateTime(datePart + " " + timePart, objFormat);
                                        }
                                        catch (Exception ex)
                                        {
                                            CaptureDate = null;
                                            ErrorHandler.ErrorHandler.LogError(ex);
                                        }
                                    }
                                    else if (file.CreationTime != null)
                                    {
                                        try
                                        {
                                            System.Globalization.DateTimeFormatInfo objFormat = new System.Globalization.DateTimeFormatInfo();
                                            objFormat.ShortDatePattern = @"yyyy/MM/dd HH:mm:ss";
                                            string datePart = Convert.ToString(file.CreationTime.Date).Split(' ').First();
                                            datePart = datePart.Replace(':', '/');
                                            string timePart = Convert.ToString(file.CreationTime.TimeOfDay);
                                            CaptureDate = Convert.ToDateTime(datePart + " " + timePart, objFormat);
                                        }
                                        catch (Exception ex)
                                        {
                                            CaptureDate = null;
                                            string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                                            ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                                        }
                                    }
                                    else
                                        CaptureDate = null;
                                    ErrorHandler.ErrorHandler.LogFileWrite("1204");
                                    foreach (LevDan.Exif.ExifTag tag in exif)
                                    {
                                        if (tag.Id == 271)
                                        {
                                            CameraManufacture = "'" + exif[271].Value + "'";
                                        }
                                        else if (tag.Id == 272)
                                        {
                                            CameraModel = "'" + exif[272].Value + "'";
                                        }
                                        else if (tag.Id == 274)
                                        {
                                            orientation = "'" + exif[274].Value + "'";
                                        }
                                        else if (tag.Id == 282)
                                        {
                                            HorizontalResolution = "'" + exif[282].Value + "'";
                                        }
                                        else if (tag.Id == 283)
                                        {
                                            VerticalResolution = "'" + exif[283].Value + "'";
                                            //ErrorHandler.ErrorHandler.LogError("VerticalResolution =" + VerticalResolution);
                                            //ErrorHandler.ErrorHandler.LogError("IsRotation =" + Convert.ToString(isrotated));
                                        }
                                        else if (tag.Id == 36867)
                                        {
                                            Datetaken = "'" + exif[36867].Value + "'";
                                        }
                                        else if (tag.Id == 40962 || tag.Id == 40963)
                                        {
                                            dimension = "'" + exif[40962].Value + " x " + exif[40963].Value + "'";
                                        }
                                        else if (tag.Id == 34855)
                                        {
                                            ISOSpeedRatings = "'" + exif[34855].Value + "'";
                                        }
                                        else if (tag.Id == 41986)
                                        {
                                            ExposureMode = "'" + exif[41986].Value + "'";
                                        }
                                        else if (tag.Id == 41994)
                                        {
                                            Sharpness = "'" + exif[41994].Value + "'";
                                        }
                                        else if (tag.Id == 2)
                                        {
                                            GPSLatitude = "'" + ConvertDegreeAngleToDouble(tag.Value.Split(' ')[0].Replace('°', ' ').Trim(), tag.Value.Split(' ')[1].Replace('\'', ' ').Trim(), tag.Value.Split(' ')[2].Replace('\"', ' ').Trim()) + "'";
                                        }
                                        else if (tag.Id == 4)
                                        {
                                            GPSLongitude = "'" + ConvertDegreeAngleToDouble(tag.Value.Split(' ')[0].Replace('°', ' ').Trim(), tag.Value.Split(' ')[1].Replace('\'', ' ').Trim(), tag.Value.Split(' ')[2].Replace('\"', ' ').Trim()) + "'";
                                        }
                                        else if (tag.Id == 33434)
                                        {
                                            ExposureTime = "'" + exif[33434].Value + "'";
                                        }
                                        else if (tag.Id == 37381)
                                        {
                                            ApertureValue = "'" + exif[37381].Value + "'";
                                        }
                                    }
                                    ErrorHandler.ErrorHandler.LogFileWrite("1266");
                                    if (!HorizontalResolution.Contains("'"))
                                        HorizontalResolution = "'" + HorizontalResolution + "'";
                                    if (!VerticalResolution.Contains("'"))
                                        VerticalResolution = "'" + VerticalResolution + "'";
                                    ImageMetaData = "<image Dimensions=" + dimension + " CameraManufacture=" + CameraManufacture + " HorizontalResolution=" + HorizontalResolution + " VerticalResolution=" + VerticalResolution + " CameraModel=" + CameraModel + " ISO-SpeedRating=" + ISOSpeedRatings + " DateTaken=" + Datetaken + " ExposureMode=" + ExposureMode + " Sharpness=" + Sharpness + " Orientation=" + orientation + " GPSLatitude=" + GPSLatitude + " GPSLongitude=" + GPSLongitude + " ExposureTime=" + ExposureTime + " ApertureValue=" + ApertureValue + " ></image>";
                                }
                                catch (System.ArgumentException aex)
                                {
                                    file.MoveTo(Path.Combine(Directoryname, "Download", "CorruptImages", file.Name));
                                    if (txtFile.Exists)
                                        txtFile.MoveTo(Path.Combine(Directoryname, "Download", "CorruptImages", txtFile.Name));
                                    return;
                                }
                                catch (Exception ex)
                                {
                                    ErrorHandler.ErrorHandler.LogError(ex);
                                    ImageMetaData = "<image Dimensions=" + "##" + " CameraManufacture=" + "##" + " HorizontalResolution=" + "##" + " VerticalResolution=" + "##" + " CameraModel=" + "##" + " ISO-SpeedRating=" + "##" + " DateTaken=" + "##" + " ExposureMode=" + "##" + " Sharpness=" + "##" + " Orientation=" + "##" + " GPSLatitude=##" + " GPSLongitude=##" + " ExposureTime=##" + " ApertureValue=##" + "></image>";
                                }
                                ErrorHandler.ErrorHandler.LogFileWrite("1285");
                                ErrorHandler.ErrorHandler.LogFileWrite("Watcher Started for " + DateTime.Now.ToString());
                                #endregion

                                bool IsImageCorrupt = false;
                                HorizontalResolution = HorizontalResolution.Replace("'", "");
                                VerticalResolution = VerticalResolution.Replace("'", "");
                                Int32 HorizontalDPI = 0;
                                Int32 VerticalDPI = 0;
                                ErrorHandler.ErrorHandler.LogFileWrite("1294");
                                if (HorizontalResolution.Contains("##") || VerticalResolution.Contains("##"))
                                {
                                    BitmapImage testbmp = new BitmapImage();
                                    using (FileStream fileStream = File.OpenRead(file.FullName))
                                    {
                                        MemoryStream ms = new MemoryStream();
                                        fileStream.CopyTo(ms);
                                        ms.Seek(0, SeekOrigin.Begin);
                                        fileStream.Close();
                                        testbmp.BeginInit();
                                        testbmp.StreamSource = ms;
                                        testbmp.EndInit();
                                        testbmp.Freeze();
                                    }
                                    HorizontalDPI = Convert.ToInt32(testbmp.DpiX);
                                    VerticalDPI = Convert.ToInt32(testbmp.DpiY);
                                }
                                else
                                {
                                    //Imagica India was returning HorizontalResolution - 0.72 and VerticalResolution - 0.72
                                    if (!Int32.TryParse(HorizontalResolution, out HorizontalDPI))
                                        HorizontalDPI = 72;

                                    if (!Int32.TryParse(VerticalResolution, out VerticalDPI))
                                        VerticalDPI = 72;
                                }


                                ErrorHandler.ErrorHandler.LogFileWrite("1323");
                                //RotateImage(file.FullName);
                                // ErrorHandler.ErrorHandler.LogFileWrite("LocationID: " + LocationID.ToString());
                                string[] LocationIds = System.Configuration.ConfigurationManager.AppSettings["LocationIds"].Split(',');
                                // ErrorHandler.ErrorHandler.LogFileWrite("LocationID1: " + LocationID.ToString());
                                ErrorHandler.ErrorHandler.LogFileWrite("1328");
                                if (LocationIds.Contains(LocationID.ToString()))
                                {
                                    //ErrorHandler.ErrorHandler.LogFileWrite("LocationID locationwise :" + LocationID.ToString());
                                    if (!isRotatedFirstTime)
                                    {
                                        rotateImageLocationWise(file.FullName);
                                        isRotatedFirstTime = true;
                                    }

                                }
                                else
                                {
                                    //ErrorHandler.ErrorHandler.LogFileWrite("LocationID  rotateImage:" + LocationID.ToString());
                                    if (!isRotatedFirstTime)
                                    {
                                        RotateImage(file.FullName);
                                        isRotatedFirstTime = true;
                                    }
                                }


                                isResetDPIRequired = CheckResetDPIRequired(HorizontalDPI, VerticalDPI, Convert.ToString(PhotoGrapherId));
                                ErrorHandler.ErrorHandler.LogFileWrite("1346");
                                //needs to change here also
                                if (isResetDPIRequired)
                                    ResetLowDPI(file.FullName, Path.Combine(DirectoryNameWithDate, picname));
                                else if (IsAutoColorCorrectionActive && IsCorrectionAtDownloadActive)
                                    AutoCorrectImageWIthCopy(file.FullName, Path.Combine(DirectoryNameWithDate, picname));
                                else
                                    file.CopyTo(Path.Combine(DirectoryNameWithDate, picname), true);
                                string FilePath = Path.Combine(GumBallRideFolderPath, txtFileName);
                                ErrorHandler.ErrorHandler.LogFileWrite("1355");
                                if (txtFile.Exists)
                                {
                                    try
                                    {
                                        gumballScoreSeperater = GetgumballScoreSeperater();
                                        //if (ConfigurationManager.AppSettings.AllKeys.Contains("GumballScoreSeperater"))
                                        //    gumballScoreSeperater = Convert.ToChar(ConfigurationManager.AppSettings["GumballScoreSeperater"].ToString());
                                        gumballScore = File.ReadAllText(txtFile.FullName);
                                        GumBallRidetxtContent = gumballScore.Replace(gumballScoreSeperater, ",");
                                        //GumBallRidetxtContent = File.ReadAllText(txtFile.FullName);
                                        if (!File.Exists(FilePath))
                                            txtFile.MoveTo(FilePath);
                                        else
                                        {
                                            File.Delete(FilePath);
                                            txtFile.MoveTo(FilePath);
                                        }
                                    }
                                    catch (Exception ex)
                                    {
                                        string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                                        ErrorHandler.ErrorHandler.LogFileWrite(errorMessage + txtFile.Name);
                                    }
                                }

                                isResetDPIRequired = false;
                                ErrorHandler.ErrorHandler.LogFileWrite("is_SemiOrder " + Convert.ToString(is_SemiOrder) + " for " + picname);
                                if (is_SemiOrder && lstobjDG_SemiOrder_Settings != null)
                                {
                                    //uncommented below code on 26 Feb by Ajay to resolve the rotation issue.
                                    if (!string.IsNullOrEmpty(renderedTag) && renderedTag != "8")
                                    {
                                        RotateImageIfRequired(Path.Combine(DirectoryNameWithDate, picname));
                                    }
                                    CompressToMinifiedImage(0.25, Path.Combine(DirectoryNameWithDate, picname), Path.Combine(_thumbnailFilePathDate, picname), string.Empty);
                                    CompressToMinifiedImage(0.50, Path.Combine(DirectoryNameWithDate, picname), Path.Combine(_bigThumbnailFilePathDate, picname), Path.Combine(_minifiedFilePathDate, "m_" + picname));
                                }
                                else
                                {
                                    //IsImageCorrupt = ResizeWPFImage(Path.Combine(DirectoryNameWithDate, picname), 210, Path.Combine(_thumbnailFilePathDate, picname), Path.Combine(DirectoryNameWithDate, picname));
                                    IsImageCorrupt = CompressToMinifiedImage(0.25, Path.Combine(DirectoryNameWithDate, picname), Path.Combine(_thumbnailFilePathDate, picname), string.Empty);
                                    if (IsImageCorrupt)
                                    {
                                        //ErrorHandler.ErrorHandler.LogFileWrite(file.Name + " is corrupt image and is moved to CorruptImages folder");
                                        File.Delete(Path.Combine(DirectoryNameWithDate, picname));
                                        file.MoveTo(Path.Combine(Directoryname, "Download", "CorruptImages", file.Name));
                                        if (File.Exists(Path.Combine(GumBallRideFolderPath, txtFileName)))
                                            File.Delete(Path.Combine(GumBallRideFolderPath, txtFileName));
                                        return;
                                    }
                                    //ResizeWPFImage(Path.Combine(DirectoryNameWithDate, picname), 1200, Path.Combine(_bigThumbnailFilePathDate, picname));
                                    CompressToMinifiedImage(0.50, Path.Combine(DirectoryNameWithDate, picname), Path.Combine(_bigThumbnailFilePathDate, picname), Path.Combine(_minifiedFilePathDate, "m_" + picname));
                                }
                                //ErrorHandler.ErrorHandler.LogFileWrite("Watcher Started for " + DateTime.Now.ToString());
                                LocationBusniess locBuiness = new LocationBusniess();

                                //try
                                //{
                                //    foreach (SemiOrderSettings objDG_SemiOrder_Settings in lstobjDG_SemiOrder_Settings)
                                //{
                                //        imagenameCntr = imagenameCntr + 1;
                                if (ParentImageID == -1)
                                {

                                    // string filenametobesaved = picname.Substring(0, picname.Length - 4) + "_" + imagenameCntr + ".jpg";
                                    string filenametobesaved = picname;

                                    OriginalFileName = picname;
                                    photoId = phBusiness.SetPhotoDetails(substoreId, filename, filenametobesaved, curDate, file.Name.Split('@')[1].ToString().Split('.')[0], ImageMetaData, LocationID, "test", ImageEffect, CaptureDate, RfidScanType, 1, (new CharacterBusiness()).GetCharacterId(file.Name.Split('@')[1].ToString().Split('.')[0]), 0, isVideoProcessed, 1, ParentImageID, OriginalFileName, objDG_SemiOrder_Settings.DG_SemiOrder_Settings_Pkey);
                                    ParentImageID = photoId;

                                }
                                else
                                {
                                    string filenametobesaved = picname;//.Substring(0, picname.Length - 4) + "_" + imagenameCntr + ".jpg";

                                    photoId = phBusiness.SetPhotoDetails(substoreId, filename, filenametobesaved, curDate, file.Name.Split('@')[1].ToString().Split('.')[0], ImageMetaData, LocationID, "test", ImageEffect, CaptureDate, RfidScanType, 1, (new CharacterBusiness()).GetCharacterId(file.Name.Split('@')[1].ToString().Split('.')[0]), 0, isVideoProcessed, 1, ParentImageID, OriginalFileName, objDG_SemiOrder_Settings.DG_SemiOrder_Settings_Pkey);
                                }
                                string imgPath = Path.Combine(DirectoryNameWithDate, picname);
                                ErrorHandler.ErrorHandler.LogFileWrite("1434");
                                //ErrorHandler.ErrorHandler.LogFileWrite("Watcher Started for " + DateTime.Now.ToString());
                                if (IsMobileRfidEnabled)
                                {
                                    MobileRfidAssociationCheck(photoId, Path.GetFileNameWithoutExtension(file.FullName));
                                }

                                if (IsBarcodeActive)
                                {
                                    BarcodeAssociationCheck(photoId, imgPath);
                                    ErrorHandler.ErrorHandler.LogFileWrite("1445");
                                }
                                bool isCodeType = IsCodeType(photoId);
                                if (is_SemiOrder && objDG_SemiOrder_Settings != null && !isCodeType)
                                {
                                    if (_mediaType != "3")
                                        SaveSpecPrintEffectsIntoDb(photoId, substoreId, LocationID, objDG_SemiOrder_Settings, DirectoryNameWithDate, picname, GumBallRidetxtContent);
                                }
                                var lstconfiglst = _lstLocationWiseConfigParams.Where(s => s.LocationId == LocationID && s.IMIXConfigurationMasterId == Convert.ToInt64(ConfigParams.IsPreviewEnabledPW)
                                               && s.ConfigurationValue.ToUpper() == "TRUE").ToList();
                                if (lstconfiglst != null && lstconfiglst.Count > 0)
                                {
                                    var lstconfig = _lstLocationWiseConfigParams.Where(s => s.LocationId == LocationID && s.IMIXConfigurationMasterId == Convert.ToInt64(ConfigParams.IsSpecImgPW) && s.ConfigurationValue.ToUpper() == "FALSE").ToList();
                                    if (lstconfig != null && lstconfig.Count() > 0)
                                        CopyImgToDisplayFolder(Path.Combine(DirectoryNameWithDate, picname), filename, photoId);
                                }
                                try
                                {
                                    ErrorHandler.ErrorHandler.LogFileWrite("1462");
                                    DispatcherFrame frame = new DispatcherFrame();
                                    mainImage.Dispatcher.BeginInvoke(
                                        System.Windows.Threading.DispatcherPriority.Input
                                        , new System.Windows.Threading.DispatcherOperationCallback(delegate
                                        {
                                            FileInfo infofile = new FileInfo(Path.Combine(DirectoryNameWithDate, picname));
                                            infofile.CopyTo(Directoryname + "\\Thumbnails\\Temp\\" + picname, true);
                                            mainImage.Source =
                                                 new BitmapImage
                                                     (new Uri(Directoryname + "\\Thumbnails\\Temp\\" + picname));
                                            frame.Continue = false;
                                            counter++;
                                            UpdateTotalCounter();
                                            return null;

                                        }), null);
                                    Dispatcher.PushFrame(frame);
                                    if (this.Dispatcher.HasShutdownFinished)
                                    {

                                    }
                                }
                                catch (IOException IOex)
                                {
                                    string errorMessage = "Got IO Exception after Dispatcher Frame for file " + file.FullName;
                                    ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                                    for (int i = 0; i < FilesToDelete.Count; i++)
                                    {
                                        if (File.Exists(FilesToDelete[i]))
                                        {
                                            File.Move(FilesToDelete[i], Path.Combine(ArchivedFolderPath, Path.GetFileName(FilesToDelete[i])));
                                            //File.Delete(FilesToDelete[i]);
                                            FilesToDelete.RemoveAt(i);
                                        }
                                    }
                                    FilesToDelete.Add(file.FullName.ToLower());
                                }
                                catch (Exception ex)
                                {
                                    ErrorHandler.ErrorHandler.LogError(ex);
                                }
                                if (is_SemiOrder)
                                {
                                    ErrorHandler.ErrorHandler.LogFileWrite("1507");
                                    if (objDG_SemiOrder_Settings != null && !isCodeType)
                                    {
                                        if (is_SemiOrder == true && objDG_SemiOrder_Settings.DG_SemiOrder_Environment == true)
                                        {

                                        }
                                        else if (is_SemiOrder == true && objDG_SemiOrder_Settings.DG_SemiOrder_Environment == false && (bool)objDG_SemiOrder_Settings.DG_SemiOrder_IsPrintActive)
                                        {
                                            string[] prdIdList = objDG_SemiOrder_Settings.DG_SemiOrder_ProductTypeId.Split(',');
                                            foreach (string prodList in prdIdList)
                                            {
                                                string SyncCode = CommonUtility.GetUniqueSynccode(Convert.ToInt32(ApplicationObjectEnum.OrderDetails).ToString().PadLeft(2, '0'), LoginUser.countrycode, LoginUser.Storecode, substoreId.ToString());
                                                int OrderDetailId = (new OrderBusiness()).AddSemiOrderDetails(photoId, prodList, LocationID, SyncCode, false, substoreId);
                                            }
                                        }
                                    }
                                }

                                picname = picname.Substring(0, picname.Length - 4) + "_" + imagenameCntr + ".jpg";
                            }
                        }
                        ErrorHandler.ErrorHandler.LogFileWrite("File Watcher Step 41. eventRaised :" + DateTime.Now.ToString("MM / dd / yyyy HH: mm:ss"));
                        try
                        {
                            //.Substring(0, picname.Length - 4) + "_" + imagenameCntr + ".jpg"
                            ErrorHandler.ErrorHandler.LogFileWrite("1532");
                            string img = System.IO.Path.Combine(ArchivedFolderPath, file.Name);
                            //string fileNameArchive = string.Empty;
                            //if (file.Name.Contains('#'))
                            //{
                            //    fileNameArchive = file.Name.Split('#')[0] + "_1"+".jpg";
                            //    img = System.IO.Path.Combine(ArchivedFolderPath, fileNameArchive);
                            //}
                            if (File.Exists(img))
                                File.Delete(img);
                            file.MoveTo(img);
                        }
                        catch (IOException IOex)
                        {
                            string errorMessage = "Got IO Exception after Archived movement for file " + file.FullName;
                            ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                            for (int i = 0; i < FilesToDelete.Count; i++)
                            {
                                if (File.Exists(FilesToDelete[i]))
                                {
                                    File.Move(FilesToDelete[i], Path.Combine(ArchivedFolderPath, Path.GetFileName(FilesToDelete[i])));
                                    //File.Delete(FilesToDelete[i]);
                                    FilesToDelete.RemoveAt(i);
                                }
                            }
                            FilesToDelete.Add(file.FullName.ToLower());
                        }
                        ErrorHandler.ErrorHandler.LogFileWrite("File Watcher Step 42. eventRaised :" + DateTime.Now.ToString("MM / dd / yyyy HH: mm:ss"));
                        ParentImageID = -1;
                        OriginalFileName = string.Empty;
                        imagenameCntr = 0;
                    }
                    catch (Exception ex)
                    {
                        ParentImageID = -1;
                        OriginalFileName = string.Empty;
                        imagenameCntr = 0;
                        ErrorHandler.ErrorHandler.LogError(ex);
                    }
                    ErrorHandler.ErrorHandler.LogFileWrite("File Watcher Step 43. eventRaised :" + DateTime.Now.ToString("MM / dd / yyyy HH: mm:ss"));
                }
            }
            catch (System.NotSupportedException sex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(sex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage + "File not supported " + file.FullName);
                file.MoveTo(Path.Combine(Directoryname, "Download", "CorruptImages", file.Name));
                if (txtFile.Exists)
                    txtFile.MoveTo(Path.Combine(Directoryname, "Download", "CorruptImages", txtFile.Name));
            }
            catch (IOException IOex)
            {
                string errorMessage = "Got IO Exception in outer catch for file " + file.FullName;
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                for (int i = 0; i < FilesToDelete.Count; i++)
                {
                    if (File.Exists(FilesToDelete[i]))
                    {
                        File.Move(FilesToDelete[i], Path.Combine(ArchivedFolderPath, Path.GetFileName(FilesToDelete[i])));
                        //File.Delete(FilesToDelete[i]);
                        FilesToDelete.RemoveAt(i);
                    }
                }
                FilesToDelete.Add(file.FullName.ToLower());
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
            }
            finally
            {
                if (DateTime.Now.Subtract(lastmemoryUpdateTime).Seconds > 5)
                {
                    MemoryManagement.FlushMemory();
                    lastmemoryUpdateTime = DateTime.Now;
                }
            }
            
#if DEBUG
            if (watch1 != null)
                FrameworkHelper.CommonUtility.WatchStop("Watcher for 1 image after flushing memory", watch1);
#endif
            ErrorHandler.ErrorHandler.LogFileWrite("File Watcher Step 44. eventRaised Ended:" + DateTime.Now.ToString("MM / dd / yyyy HH: mm:ss"));
        }

        private void ManualDownloadworker_RunWorkerCompleted(object sender, System.ComponentModel.RunWorkerCompletedEventArgs e)
        {

        }

        //        private void Minified_DoWork(object Sender, System.ComponentModel.DoWorkEventArgs e)
        //        {
        //#if DEBUG
        //            var watch = FrameworkHelper.CommonUtility.Watch();
        //            watch.Start();
        //#endif
        //            CompressToMinifiedImage(0.5, "", "");
        //#if DEBUG
        //            if (watch != null)
        //                FrameworkHelper.CommonUtility.WatchStop(this.GetType().FullName + "." + System.Reflection.MethodBase.GetCurrentMethod().Name + "|", watch);
        //#endif
        //        }


        /// <summary>
        /// Added by Vinod for SET3
        /// </summary>
        /// <param name="scaleFactor"></param>
        /// <param name="sourcePath"></param>
        /// <param name="targetPath"></param>
        private bool CompressToMinifiedImage(double scaleFactor, string sourcePath, string targetPath, string targetMinifiedPath)
        {
            try
            {
                using (FileStream fileStream = File.OpenRead(sourcePath))
                {
                    using (var image = System.Drawing.Image.FromStream(fileStream))
                    {
                        var newWidth = (int)(image.Width * scaleFactor);
                        var newHeight = (int)(image.Height * scaleFactor);
                        var thumbnailImg = new Bitmap(newWidth, newHeight);
                        var thumbGraph = Graphics.FromImage(thumbnailImg);
                        thumbGraph.CompositingQuality = CompositingQuality.HighQuality;
                        thumbGraph.SmoothingMode = SmoothingMode.HighQuality;
                        thumbGraph.InterpolationMode = InterpolationMode.HighQualityBicubic;
                        var imageRectangle = new System.Drawing.Rectangle(0, 0, newWidth, newHeight);
                        thumbGraph.DrawImage(image, imageRectangle);
                        thumbnailImg.Save(targetPath, image.RawFormat);
                        if (targetMinifiedPath != string.Empty)
                            thumbnailImg.Save(targetMinifiedPath, image.RawFormat);
                    }
                }

                return false;
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogFileWrite("CompressToMinifiedImage:: Message: " + ex.Message + " StackTrace: " + ex.StackTrace + " InnerException: " + ex.InnerException + " Source: " + ex.Source);
                return true;//for corrupted image
            }
            finally
            {

            }
        }

        private string GetgumballScoreSeperater()
        {
            if (GumBallActiveLocationList != null && GumBallActiveLocationList.Count > 0)
                LstRideConfigValueLocationWise = GumBallConfigurationValueList.Where(x => x.LocationId.Equals(LocationID) && GumBallActiveLocationList.Contains(x.LocationId)).ToList();
            if (LstRideConfigValueLocationWise != null && LstRideConfigValueLocationWise.Count > 0)
            {
                gumballScoreSeperater = LstRideConfigValueLocationWise.Where(x => x.IMIXConfigurationMasterId.Equals(Convert.ToInt32(ConfigParams.GumballScoreSeperater))).Select(x => x.ConfigurationValue).FirstOrDefault();
            }
            if (string.IsNullOrEmpty(gumballScoreSeperater))
                gumballScoreSeperater = ",";
            return gumballScoreSeperater;
        }
        private void MobileRfidAssociationCheck(int PhtoId, string imageName)
        {
            try
            {
                string tagId = string.Empty;
                if (imageName.Split('@').Length > 2)
                    tagId = imageName.Split('@')[2];
                if (!string.IsNullOrEmpty(tagId))
                {
                    tagId = "2222" + tagId;
                    new AssociateImageBusiness().AssociateMobileImage(tagId, PhtoId);
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
            }
        }
        private bool IsCodeType(int PhotoId)
        {
            //objDigiPhotoDataServices = new DigiPhotoDataServices();
            PhotoBusiness phBusiness = new PhotoBusiness();
            return phBusiness.CheckIsCodeType(PhotoId);
        }
        private void BarcodeAssociationCheck(int PhotoId, string ImagePath)
        {
            try
            {
                PhotoBusiness phBusiness = new PhotoBusiness();
                //objDigiPhotoDataServices = new DigiPhotoDataServices();
                if (scanType == Convert.ToInt32(ScanType.PreScan))
                {
                    bool isUniqueCodeFound = SetUniqueCodeInfoForPreScan(PhotoId, ImagePath);
                    if (isUniqueCodeFound)
                    {
                        var item = lstImgInfoPreScanList.Where(x => x.PhotoGrapherId == PhotoGrapherId).FirstOrDefault();
                        if (item != null && !String.IsNullOrEmpty(item.Barcode))
                        {
                            phBusiness.SetImageAssociationInfo(PhotoId, item.Format, item.Barcode, IsAnonymousCodeActive);
                        }
                    }
                }
                else if (scanType == Convert.ToInt32(ScanType.PostScan))
                {
                    bool isUniqueCodeFound = SetUniqueCodeInfoForPostScan(PhotoId, ImagePath);
                    if (isUniqueCodeFound)
                    {
                        var item = lstImgInfoPostScanList.Where(x => x.PhotoGrapherId == PhotoGrapherId).FirstOrDefault();
                        if (item != null && !String.IsNullOrEmpty(item.Barcode))
                        {
                            phBusiness.SetImageAssociationInfoForPostScan(item.ListImgPhotoId, item.Format, item.Barcode, IsAnonymousCodeActive);
                            AutoPurchasePostScan(item);
                            int ind = lstImgInfoPostScanList.FindIndex(x => x.PhotoGrapherId == PhotoGrapherId);
                            if (ind >= 0 && ind < lstImgInfoPostScanList.Count)
                                lstImgInfoPostScanList.RemoveAt(lstImgInfoPostScanList.FindIndex(x => x.PhotoGrapherId == PhotoGrapherId));
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
            }
        }
        private void AutoPurchasePostScan(ImgInfoPostScanList item)
        {
            try
            {
                CardBusiness cardBusiness = new CardBusiness();
                if (IsAutoPurchaseActive && !IsAnonymousCodeActive)
                {
                    if (cardBusiness.IsValidPrepaidCodeType(item.Barcode, 405))
                    {
                        if (ReadSystemOnlineStatus())
                        {
                            try
                            {
                                ServiceProxy<IDataSyncService>.Use(client =>
                                {
                                    objCardLimit = client.CheckCardLimit(item.Barcode);
                                });
                                IsOnline = true;
                            }
                            catch (Exception ex)
                            {
                                ErrorHandler.ErrorHandler.LogError(ex);
                                IsOnline = false;
                            }
                        }

                        if (IsOnline)
                        {
                            if (objCardLimit.ValidCard)
                            {
                                CardImageLimit = objCardLimit.Allowed;
                                CardImageSold = objCardLimit.Associated;
                            }
                        }
                        else
                        {
                            CardImageLimit = cardBusiness.GetCardImageLimit(item.Barcode);
                            CardImageSold = cardBusiness.GetCardImageSold(item.Barcode);
                        }
                        //Set flag to set the package price.
                        if (CardImageSold > 0)
                            IsQrCodeUsed = true;
                        else
                            IsQrCodeUsed = false;

                        if (CardImageSold < CardImageLimit)
                        {
                            rest = CardImageLimit - CardImageSold;
                            rest = rest < 0 ? 0 : rest;
                        }
                        else
                        {
                            rest = 0;
                        }
                        string images = string.Empty;
                        if (scanType == 501)
                            images = string.Join(",", item.ListImgPhotoId.Skip(1).Take(rest));
                        else if (scanType == 502)
                            images = string.Join(",", item.ListImgPhotoId.Take(rest));

                        if (!String.IsNullOrWhiteSpace(images))
                        {
                            iMixImageCardTypeInfo imgCardTypeInfo = cardBusiness.GetCardTypeList().Where(d => d.CardIdentificationDigit == item.Barcode.Substring(0, 4)).FirstOrDefault();
                            int pkgId = imgCardTypeInfo.PackageId == null ? 0 : (int)imgCardTypeInfo.PackageId;
                            decimal pkgPrice = 0;
                            ProductBusiness prodBusiness = new ProductBusiness();
                            if (CardImageSold == 0)
                            {

                                pkgPrice = (Decimal)prodBusiness.GetProductPricing(pkgId);
                            }

                            int PaymentMode = 0;
                            CurrencyBusiness curBusiness = new CurrencyBusiness();
                            OrderBusiness ordBusiness = new OrderBusiness();
                            DigiPhoto.IMIX.Model.CurrencyInfo currency = curBusiness.GetCurrencyList().Where(g => g.DG_Currency_Default == true).FirstOrDefault();
                            int prodId = prodBusiness.GetPackagDetails(pkgId).Where(k => k.DG_Product_Quantity > 0).FirstOrDefault().DG_Orders_ProductType_pkey;
                            string PaymentDetails = "<Payment Mode = 'prepaid' Amount = '" + pkgPrice.ToString() + "' OrignalAmount = '" + pkgPrice.ToString() + "' CurrencyID = '" + currency.DG_Currency_pkey.ToString() + "' CurrencySyncCode = '" + currency.SyncCode.ToString() + "'/>";
                            string OrderNumber = GenerateOrderNumber();
                            string SyncCode = CommonUtility.GetUniqueSynccode(Convert.ToInt32(ApplicationObjectEnum.Order).ToString().PadLeft(2, '0'), LoginUser.countrycode, LoginUser.Storecode, "14");

                            DigiPhoto.IMIX.Model.OrderInfo orderInfo = ordBusiness.GenerateOrder(OrderNumber, pkgPrice, pkgPrice, PaymentDetails, PaymentMode, 0, null, 3, curBusiness.GetDefaultCurrency(), "0", SyncCode, LoginUser.Storecode);

                            int OrderID = orderInfo.DG_Orders_pkey;
                            AuditLog.AddUserLog(3, (int)FrameworkHelper.ActionType.GenerateOrder, "Create Order of No :" + OrderNumber + " of total Amount " + currency.DG_Currency_Symbol + " " + pkgPrice.ToString("0.000"));
                            string OrderDetailsSyncCode = CommonUtility.GetUniqueSynccode(Convert.ToInt32(ApplicationObjectEnum.OrderDetails).ToString().PadLeft(2, '0'), LoginUser.countrycode, LoginUser.Storecode, substoreId.ToString());
                            int ParentID = ordBusiness.SaveOrderLineItems(pkgId, OrderID, "", 1, null, 0, pkgPrice, pkgPrice, pkgPrice, -1, substoreId, 0, null, OrderDetailsSyncCode, null, null);
                            ordBusiness.SaveOrderLineItems(prodId, OrderID, images, 1, null, 0, 0, 0, 0, ParentID, substoreId, imgCardTypeInfo.ImageIdentificationType, item.Barcode, OrderDetailsSyncCode, null, null);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
            }
        }
        private void RotateImageIfRequired(string sourceImage)
        {
            try
            {
                BitmapImage bitmapImage = new BitmapImage();
                using (FileStream fileStream = File.OpenRead(sourceImage.ToString()))
                {
                    MemoryStream ms = new MemoryStream();
                    fileStream.CopyTo(ms);
                    fileStream.Close();
                    ms.Seek(0, SeekOrigin.Begin);
                    bitmapImage.BeginInit();
                    bitmapImage.StreamSource = ms;
                    ImageMagickObject.MagickImage jmagic = new ImageMagickObject.MagickImage();

                    if (needrotaion > 0)
                    {
                        if (needrotaion == 0)
                        {
                            bitmapImage.Rotation = Rotation.Rotate0;
                            isrotated = false;
                        }
                        else if (needrotaion == 90)
                        {
                            bitmapImage.Rotation = Rotation.Rotate90;

                            object[] o = new object[] { "-rotate", " 90 ", sourceImage };
                            jmagic.Mogrify(o);
                            o = null;
                            isrotated = true;
                        }
                        else if (needrotaion == 180)
                        {
                            bitmapImage.Rotation = Rotation.Rotate180;

                            object[] o = new object[] { "-rotate", " 180 ", sourceImage };
                            jmagic.Mogrify(o);
                            o = null;
                            isrotated = false;
                        }
                        else if (needrotaion == 270)
                        {
                            bitmapImage.Rotation = Rotation.Rotate270;

                            object[] o = new object[] { "-rotate", " 270 ", sourceImage };
                            jmagic.Mogrify(o);
                            o = null;
                            isrotated = true;
                        }
                    }

                    jmagic = null;
                    bitmapImage.EndInit();
                    bitmapImage.Freeze();
                    fileStream.Close();
                }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }

        private void CompressToMinifiedImage(System.IO.Stream sourceImage, int maxHeight, string saveToPath)
        {
            try
            {
                using (var image = System.Drawing.Image.FromStream(sourceImage))
                {
                    decimal ratio = Convert.ToDecimal(image.Width) / Convert.ToDecimal(image.Height);
                    int newWidth = Convert.ToInt32(maxHeight * ratio);
                    int newHeight = maxHeight;

                    var thumbnailImg = new Bitmap(newWidth, newHeight);
                    var thumbGraph = Graphics.FromImage(thumbnailImg);
                    thumbGraph.CompositingQuality = CompositingQuality.HighQuality;
                    thumbGraph.SmoothingMode = SmoothingMode.HighQuality;
                    thumbGraph.InterpolationMode = InterpolationMode.HighQualityBicubic;
                    var imageRectangle = new System.Drawing.Rectangle(0, 0, newWidth, newHeight);
                    thumbGraph.DrawImage(image, imageRectangle);
                    thumbnailImg.Save(saveToPath, image.RawFormat);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
            finally
            {
                if (DateTime.Now.Subtract(lastmemoryUpdateTime).Seconds > 15)
                {
                    MemoryManagement.FlushMemory();
                    lastmemoryUpdateTime = DateTime.Now;
                }
            }
        }

        /// <summary>
        /// Resizes the WPF image.
        /// </summary>
        /// <param name="sourceImage">The source image.</param>
        /// <param name="maxHeight">The maximum height.</param>
        /// <param name="saveToPath">The save automatic path.</param>
        private void ResizeWPFImage(string sourceImage, int maxHeight, string saveToPath)
        {
            try
            {
                BitmapImage bi = new BitmapImage();
                BitmapImage bitmapImage = new BitmapImage();
                using (FileStream fileStream = File.OpenRead(sourceImage.ToString()))
                {
                    MemoryStream ms = new MemoryStream();
                    fileStream.CopyTo(ms);
                    ms.Seek(0, SeekOrigin.Begin);
                    fileStream.Close();
                    bi.BeginInit();
                    bi.StreamSource = ms;
                    bi.EndInit();
                    bi.Freeze();
                    decimal ratio = Convert.ToDecimal(bi.Width) / Convert.ToDecimal(bi.Height);

                    int newWidth = Convert.ToInt32(maxHeight * ratio);
                    int newHeight = maxHeight;

                    ms.Seek(0, SeekOrigin.Begin);
                    bitmapImage.BeginInit();
                    bitmapImage.StreamSource = ms;
                    bitmapImage.DecodePixelWidth = newWidth;
                    bitmapImage.DecodePixelHeight = newHeight;

                    bitmapImage.EndInit();
                    bitmapImage.Freeze();
                }

                using (var fileStreamForSave = new FileStream(saveToPath, FileMode.Create, FileAccess.ReadWrite))
                {
                    JpegBitmapEncoder encoder = new JpegBitmapEncoder();
                    encoder.QualityLevel = 94;
                    encoder.Frames.Add(BitmapFrame.Create(bitmapImage));
                    encoder.Save(fileStreamForSave);
                }
                bi = null;
                bitmapImage = null;
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
            }
            finally
            {
                if (DateTime.Now.Subtract(lastmemoryUpdateTime).Seconds > 15)
                {
                    MemoryManagement.FlushMemory();
                    lastmemoryUpdateTime = DateTime.Now;
                }
            }
        }
        /// <summary>
        /// Resizes the WPF image.
        /// </summary>
        /// <param name="sourceImage">The source image.</param>
        /// <param name="maxHeight">The maximum height.</param>
        /// <param name="saveToPath">The save automatic path.</param>
        /// <param name="rotatePath">The rotate path.</param>
        private bool ResizeWPFImage(string sourceImage, int maxHeight, string saveToPath, string rotatePath)
        {
            bool IsImageCorrupt = false;
            try
            {
                BitmapImage bi = new BitmapImage();
                BitmapImage bitmapImage = new BitmapImage();
                using (FileStream fileStream = File.OpenRead(sourceImage.ToString()))
                {
                    MemoryStream ms = new MemoryStream();
                    fileStream.CopyTo(ms);
                    ms.Seek(0, SeekOrigin.Begin);
                    fileStream.Close();
                    bi.BeginInit();
                    bi.StreamSource = ms;
                    bi.EndInit();
                    bi.Freeze();

                    decimal ratio = 0;
                    int newWidth = 0;
                    int newHeight = 0;

                    if (bi.Width >= bi.Height)
                    {
                        ratio = Convert.ToDecimal(bi.Width) / Convert.ToDecimal(bi.Height);
                        newWidth = maxHeight;
                        newHeight = Convert.ToInt32(maxHeight / ratio);
                    }
                    else
                    {
                        ratio = Convert.ToDecimal(bi.Height) / Convert.ToDecimal(bi.Width);
                        newHeight = maxHeight;
                        newWidth = Convert.ToInt32(maxHeight / ratio);
                    }

                    ms.Seek(0, SeekOrigin.Begin);
                    bitmapImage.BeginInit();
                    bitmapImage.StreamSource = ms;
                    bitmapImage.DecodePixelWidth = newWidth;
                    bitmapImage.DecodePixelHeight = newHeight;

                    bitmapImage.EndInit();
                    bitmapImage.Freeze();
                    fileStream.Close();
                }

                using (var fileStreamForSave = new FileStream(saveToPath, FileMode.Create, FileAccess.ReadWrite))
                {
                    JpegBitmapEncoder encoder = new JpegBitmapEncoder();
                    encoder.QualityLevel = 94;
                    encoder.Frames.Add(BitmapFrame.Create(bitmapImage));
                    encoder.Save(fileStreamForSave);
                    fileStreamForSave.Close();
                }
                bi = null;
                bitmapImage = null;
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
                IsImageCorrupt = true;
            }
            return IsImageCorrupt;
        }

        /// <summary>
        /// Resizes the and save high quality image.
        /// </summary>
        /// <param name="image">The image.</param>
        /// <param name="pathToSave">The path automatic save.</param>
        /// <param name="quality">The quality.</param>
        /// <param name="height">The height.</param>
        /// <exception cref="System.ArgumentOutOfRangeException"></exception>
        private void ResizeAndSaveHighQualityImage(System.Drawing.Image image, string pathToSave, int quality, int height)
        {
            try
            {

                // the resized result bitmap
                decimal ratio = Convert.ToDecimal(image.Width) / Convert.ToDecimal(image.Height);
                int width = Convert.ToInt32(height * ratio);

                using (Bitmap result = new Bitmap(width, height))
                {
                    // get the graphics and draw the passed image to the result bitmap
                    using (Graphics grphs = Graphics.FromImage(result))
                    {
                        grphs.CompositingQuality = System.Drawing.Drawing2D.CompositingQuality.HighQuality;
                        grphs.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
                        grphs.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
                        grphs.DrawImage(image, 0, 0, result.Width, result.Height);
                    }

                    // check the quality passed in
                    if ((quality < 0) || (quality > 100))
                    {
                        string error = string.Format("quality must be 0, 100", quality);
                        throw new ArgumentOutOfRangeException(error);
                    }

                    EncoderParameter qualityParam = new EncoderParameter(System.Drawing.Imaging.Encoder.Quality, quality);
                    string lookupKey = "image/jpeg";
                    var jpegCodec =
                        ImageCodecInfo.GetImageEncoders().Where(i => i.MimeType.Equals(lookupKey)).FirstOrDefault();

                    //create a collection of EncoderParameters and set the quality parameter
                    var encoderParams = new EncoderParameters(1);
                    encoderParams.Param[0] = qualityParam;
                    //save the image using the codec and the encoder parameter
                    result.Save(pathToSave, jpegCodec, encoderParams);
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
            }
        }
        #endregion

        #region Events
        /// <summary>
        /// Handles the Tick event of the timer1 control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void timer1_Tick(object sender, EventArgs e)
        {
            //Object lockobj = new Object();
            ErrorHandler.ErrorHandler.LogFileWrite("File Watcher Step 16. timer1_Tick :" + DateTime.Now.ToString("MM / dd / yyyy HH: mm:ss"));
            timer.Stop();
            try
            {
                if (string.IsNullOrEmpty(DownloadFolderPath))
                {
                    return;
                }
                DirectoryNameWithDate = Path.Combine(Directoryname, DateTime.Now.ToString("yyyyMMdd"));
                _thumbnailFilePathDate = Path.Combine(Directoryname, "Thumbnails", DateTime.Now.ToString("yyyyMMdd"));
                _bigThumbnailFilePathDate = Path.Combine(Directoryname, "Thumbnails_Big", DateTime.Now.ToString("yyyyMMdd"));
                _minifiedFilePathDate = Path.Combine(Directoryname, "Minified_Images", DateTime.Now.ToString("yyyyMMdd"));
                if (!Directory.Exists(DirectoryNameWithDate))
                    Directory.CreateDirectory(DirectoryNameWithDate);
                if (!Directory.Exists(_thumbnailFilePathDate))
                    Directory.CreateDirectory(_thumbnailFilePathDate);
                if (!Directory.Exists(_bigThumbnailFilePathDate))
                    Directory.CreateDirectory(_bigThumbnailFilePathDate);
                if (!Directory.Exists(_minifiedFilePathDate))
                    Directory.CreateDirectory(_minifiedFilePathDate);
                
                UpdateTotalCounter();
                ErrorHandler.ErrorHandler.LogFileWrite("File Watcher Step 17. timer1_Tick :" + DateTime.Now.ToString("MM / dd / yyyy HH: mm:ss"));
                DirectoryInfo _objnew = new DirectoryInfo(DownloadFolderPath);
                if (_objnew.Exists)
                {
                    FileInfo[] lstFileInfo;
                    //   lstFileInfo = _objnew.GetFiles("*.jpg");
                    lstFileInfo = _objnew.EnumerateFiles()
                                           .Where(f => mediaExtensions.Contains(f.Extension.ToLower()) && f.Name.Contains("@"))
                                           .ToArray();
                    List<FileInfo> filesList = lstFileInfo.ToList().Where(x => FilesToDelete.Contains(x.FullName.ToLower()) == false).ToList();
                    filesList = filesList.OrderBy(t => t.CreationTime).ToList();
                    //bool result = true;
                    //if (filesList.Count == 0)
                    //{
                    //    result = false;
                    //    ErrorHandler.ErrorHandler.LogFileWrite("File Watcher Parallel Processing No Count ");
                    //}
                    //if (result)
                    //{
                    //    Parallel.For(0, filesList.Count, i =>
                    //    {
                    //        //string total =
                    //        FileProcess(filesList[i]);
                    //       // Console.WriteLine("{0} - {1}", i, total);
                    //    });
                    //}
                    foreach (var item in filesList.OrderBy(t => t.CreationTime))
                    {
                        ErrorHandler.ErrorHandler.LogFileWrite("Line Number : 2228  Event Called 1");
                        if (item.Extension.ToLower().Equals(".jpg"))
                        {
                            if (!IsFileLocked(item))
                            {
                                var task1 = Task.Run(() => FileProcess(item));
                                //eventRaised(item);
                                ErrorHandler.ErrorHandler.LogFileWrite("Line Number : 2228  Event Called 2");
                            }
                        }
                        else
                        {
                            ErrorHandler.ErrorHandler.LogFileWrite("Line Number : 2188  Timer Function");
                            TimerFunction(item);
                        }
                        ErrorHandler.ErrorHandler.LogFileWrite("Line Number : 2228  Event Called 3");
                    }
                }
                ErrorHandler.ErrorHandler.LogFileWrite("File Watcher Step 18. timer1_Tick :" + DateTime.Now.ToString("MM / dd / yyyy HH: mm:ss"));
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
            }
            finally
            {
                timer.Start();
            }
        }

        /// <summary>
        /// Added By Suraj For Palleral Processing.
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        private void FileProcess (FileInfo item )
        {
            try
            {
                if (item.Extension.ToLower().Equals(".jpg"))
                {
                    if (!IsFileLocked(item))
                    {
                        eventRaised(item);
                    }
                }
                else
                {
                    ErrorHandler.ErrorHandler.LogFileWrite("Line Number : 2250 : FileProcess Function : File is not jpg format : File Name :"+item.Name);
                    TimerFunction(item);
                }
            }
            catch(Exception ex)
            {
                ErrorHandler.ErrorHandler.LogFileWrite("Line Number : 2256 : FileProcess Function  Exception Massage : "+ex.Message);
            }
            finally
            {  }
        }

        public void TimerFunction(FileInfo file)
        {
            try
            {
                string _videoFileName = Path.GetFileNameWithoutExtension(file.Name);
                //Process each video media file
                //1) Move Video File,  2) Create Video Thumbnail, 3) Add entry in DG_photos.
                string userId = file.Name.Split('@')[1].Split('.')[0]; //user id for processed video
                bool IsGreenScreenVideo = false;
                bool IsProcessedVideo = false;
                string fileName = file.Name.Split('@')[0];
                string downloadpath = file.DirectoryName + "\\";
                PhotoBusiness photoBiz = new PhotoBusiness();

                if (userId.Contains("_PR"))
                {
                    IsProcessedVideo = true;

                    userId = userId.Split('_')[0]; //user id for greenscreen

                }
                if (userId.Contains("_GS"))
                {
                    IsGreenScreenVideo = true;
                    userId = userId.Split('_')[0]; //user id for greenscreen
                }
                int vidCount = photoBiz.PhotoCountCurrentDate(RFID: fileName, photographerID: Convert.ToInt32(userId), mediaType: 2);
                UsersInfo userInfo = (new UserBusiness().GetUserDetailByUserId(userId.ToInt32(), string.Empty, 0).FirstOrDefault());
                LocationID = userInfo.DG_Location_pkey;
                int substoreId = userInfo.DG_Substore_ID;
                SetNewConfigLocationValues(LocationID);
                DateTime curDate = (new CustomBusineses()).ServerDateTime();
                string videoFileName = string.Empty;
                string rfid = file.Name.Split('@')[0];

                if (IsGreenScreenVideo)
                {
                    mediaType = 2;
                    if (vidCount > 0)
                        videoFileName = curDate.Day.ToString() + curDate.Month.ToString() + file.Name.Split('@')[0] + "_GS" + "(" + vidCount + ")" + file.Extension;
                    else
                        videoFileName = curDate.Day.ToString() + curDate.Month.ToString() + file.Name.Split('@')[0] + "_GS" + file.Extension;
                    string videoSavePath = Path.Combine(Directoryname, "Videos", DateTime.Now.ToString("yyyyMMdd"));
                    if (!Directory.Exists(videoSavePath))
                        Directory.CreateDirectory(videoSavePath);
                    file.MoveTo(Path.Combine(videoSavePath, videoFileName));

                    FileInfo fi = new FileInfo(Path.Combine(videoSavePath, videoFileName));
                    //Create Minified Video by Suraj ....
                    CreateMinifiedVideo(Path.Combine(videoSavePath, videoFileName), videoSavePath, videoFileName);
                    // End Changes ...
                    //fi.CopyTo(Path.Combine(videoSavePath, "m_" + videoFileName)); //Create minified video

                }
                else if (IsProcessedVideo)
                {
                    mediaType = 3;
                    int processedVidCount = photoBiz.PhotoCountCurrentDate(RFID: fileName, photographerID: Convert.ToInt32(userId), mediaType: mediaType);
                    if (processedVidCount > 0)
                        videoFileName = curDate.Day.ToString() + curDate.Month.ToString() + file.Name.Split('@')[0] + "(" + processedVidCount + ")" + file.Extension;
                    else
                        videoFileName = curDate.Day.ToString() + curDate.Month.ToString() + file.Name.Split('@')[0] + file.Extension;
                    string videoSavePath = Path.Combine(Directoryname, "ProcessedVideos", DateTime.Now.ToString("yyyyMMdd"));
                    if (!Directory.Exists(videoSavePath))
                        Directory.CreateDirectory(videoSavePath);
                    file.MoveTo(Path.Combine(videoSavePath, videoFileName));
                    FileInfo fi = new FileInfo(Path.Combine(videoSavePath, videoFileName));
                    //Create Minified Video by Suraj ....
                    CreateMinifiedVideo(Path.Combine(videoSavePath, videoFileName), videoSavePath, videoFileName);
                    // End Changes ...
                    //fi.CopyTo(Path.Combine(videoSavePath, "m_" + videoFileName)); //Create minified video
                }
                else
                {
                    mediaType = 2;
                    if (vidCount > 0)
                        videoFileName = curDate.Day.ToString() + curDate.Month.ToString() + file.Name.Split('@')[0] + "(" + vidCount + ")" + file.Extension;
                    else
                        videoFileName = curDate.Day.ToString() + curDate.Month.ToString() + file.Name.Split('@')[0] + file.Extension;
                    string videoSavePath = Path.Combine(Directoryname, "Videos", DateTime.Now.ToString("yyyyMMdd"));
                    if (!Directory.Exists(videoSavePath))
                        Directory.CreateDirectory(videoSavePath);
                    file.MoveTo(Path.Combine(videoSavePath, videoFileName));
                    FileInfo fi = new FileInfo(Path.Combine(videoSavePath, videoFileName));
                    // Create Minified Video By Suraj....
                    CreateMinifiedVideo(Path.Combine(videoSavePath, videoFileName), videoSavePath, videoFileName);
                    //End Changes ...
                    //fi.CopyTo(Path.Combine(videoSavePath, "m_" + videoFileName)); //Create minified video
                }

                //Save Video Thumbnail.
                long videoLength;
                string filSavePath = Path.Combine(_thumbnailFilePathDate, Path.GetFileNameWithoutExtension(file.FullName) + ".jpg");
                string minifidFilePath = Path.Combine(_thumbnailFilePathDate, "m_" + Path.GetFileNameWithoutExtension(file.FullName) + ".jpg");

                //ThumbnailExtractor.ExtractThumbnailFromVideo(file.FullName, 4, 4, filSavePath, _VisioForgeLicenseKey, _VisioForgeUserName, _VisioForgeEmailID);
                string FilePath = FrameworkHelper.Common.MLHelpers.ExtractThumbnail(file.FullName);
                if (!string.IsNullOrEmpty(FilePath))
                {
                    FrameworkHelper.Common.MLHelpers.ResizeImage(FilePath, 210, filSavePath);
                    File.Copy(filSavePath, minifidFilePath);
                }

                //Extract video frames by reading from XML and save it into Frame Folder
                if (IsFrameExtractionEnabled && !IsProcessedVideo)
                {
                    List<double> framesec = ReadXml();

                    if (framesec != null || framesec.Count > 0)
                    {
                        List<string> FrameFilePath = FrameworkHelper.Common.MLHelpers.ExtractFrame(file.FullName, framesec, userId, fileName, cropVideoFrameRatio);

                        //Move File into download folder
                        foreach (string derivedFrame in FrameFilePath)
                        {
                            string filename = Path.GetFileName(derivedFrame);
                            File.Move(derivedFrame, downloadpath + filename);
                        }
                    }
                }

                //Thread.Sleep(5000);
                DispatcherFrame frame = new DispatcherFrame();
                mainImage.Dispatcher.BeginInvoke(
                               DispatcherPriority.Input
                               , new DispatcherOperationCallback(delegate
                               {
                                   mainImage.Source = new BitmapImage
                                                                (new Uri(filSavePath));

                                   counter++;           //Increment processed counter.
                                   UpdateTotalCounter();
                                   frame.Continue = false;


                                   return null;
                               }), null);

                Dispatcher.PushFrame(frame);

                watchersetting = VBusiness.GetSettingWatcher(LocationID);

                int VideoSceneCount = watchersetting.Where(e => e.SceneName != "" && (e.IsMixerScene == null || e.IsMixerScene == false)).Count();
                int ProfileNameCount = watchersetting.Where(e => e.SceneName != "" && e.IsMixerScene == true).Count();
                videoLength = Convert.ToInt64(FrameworkHelper.Common.MLHelpers.VideoLength);
                isVideoProcessed = true;
                if (_IsAdvancedVideoEditActive)
                {
                    // if (watchersetting.Any(x => x.Isstream == true)) //look in camera detail table for livestream
                    // {
                    if (!IsProcessedVideo)     //Specify the GreenscreenVideo and guest video
                        if (VideoSceneCount > 0)  //look in the Videoscene table for scene name
                            isVideoProcessed = false;
                    // }
                }
                if (isAutoVideoProcessingActive && isVideoProcessed == true)
                    if (!IsProcessedVideo)
                        if (ProfileNameCount > 0)
                            isVideoProcessed = false;


                //if (_IsAdvancedVideoEditActive)
                //{
                //    // if (watchersetting.Any(x => x.Isstream == true)) //look in camera detail table for livestream
                //    // {
                //    if (!IsProcessedVideo && IsGreenScreenVideo)     //Specify the GreenscreenVideo and guest video
                //    {
                //        if (VideoSceneCount > 0)  //look in the Videoscene table for scene name
                //            isVideoProcessed = false;
                //    }

                //    // }
                //}
                //if (isAutoVideoProcessingActive && !IsProcessedVideo && !IsGreenScreenVideo)
                //{
                //    if (ProfileNameCount > 0)
                //        isVideoProcessed = false;
                //    else isVideoProcessed = true;
                //}
                //else
                //    isVideoProcessed = true;


                int? charId = (new CharacterBusiness()).GetCharacterId(userId);
                photoId = photoBiz.SetPhotoDetails(substoreId, rfid, videoFileName, curDate, userId, null, LocationID, "", "", null, RfidScanType, 1, charId, videoLength, isVideoProcessed, mediaType);
                videoCount++;
                UpdateTotalCounter();
                if (IsMobileRfidEnabled)
                {
                    MobileRfidAssociationCheck(photoId, _videoFileName);
                }

            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
            finally
            {
                //if (DateTime.Now.Subtract(LastMemoryClearTime).Seconds > 10)
                //{
                MemoryManagement.FlushMemory();
                //    LastMemoryClearTime = DateTime.Now;
                //}
            }
            //file.MoveTo(System.IO.Path.Combine(Directoryname, "Archived", file.Name));
        }
        // Create MinifiedVideos Function is created by Suraj.
        private void CreateMinifiedVideo(string sourceFilePath, string tragetFilePath, string minifiedFileName)
        {
            try
            {
                string file = sourceFilePath;

                ErrorHandler.ErrorHandler.LogFileWrite("Minified Video Crate Start");
                string command = "C:\\WinFF\\ffmpeg.exe -i " + @file + " -vf scale=480:-2,setsar=1:1 -c:v libx264 -c:a copy -crf 20 " + @tragetFilePath + "\\m_" + minifiedFileName;
                //string command = "C:\\Program Files (x86)\\iMix\\WinFF\\ffmpeg.exe -i " + @file + " -vf scale=480:-2,setsar=1:1 -c:v libx264 -c:a copy -crf 20 " + @tragetFilePath + "\\m_" + minifiedFileName;  // For deployment.
                ErrorHandler.ErrorHandler.LogFileWrite("Line Number : 2422  Command :"+command);
                ProcessStartInfo procStartInfo = new ProcessStartInfo("cmd", "/c " + command);

                procStartInfo.RedirectStandardOutput = false;
                procStartInfo.UseShellExecute = false;
                procStartInfo.CreateNoWindow = true;

                // wrap IDisposable into using (in order to release hProcess) 
                using (Process process = new Process())
                {
                    process.StartInfo = procStartInfo;
                    process.Start();
                    ErrorHandler.ErrorHandler.LogFileWrite("Line Number : 2434  Command :" + process.StandardOutput.ReadToEnd());
                    process.WaitForExit();
                    process.WaitForExit();

                    ///and only then read the result
                    //string result = process.StandardOutput.ReadToEnd();
                    //Console.WriteLine(result);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }
        // End Changes
        private List<double> ReadXml()
        {
            if (videoFramePositions != "Empty")
            {
                var xmlDocument = new XmlDocument();
                xmlDocument.LoadXml(videoFramePositions);
                XmlNodeList itemNodes = xmlDocument.SelectNodes("Frames/frame");
                List<double> frameSec = new List<double>();
                foreach (XmlNode loc in itemNodes)
                {
                    frameSec.Add(Convert.ToDouble(loc.InnerText));
                }
                return frameSec;
            }
            return null;
        }
        private void UpdateTotalCounter()
        {
            DirectoryInfo drinfo = new DirectoryInfo(DownloadFolderPath);
            int pending = 0;
            int archived = 0;
            if (drinfo.Exists)
            {
                pending = drinfo.EnumerateFiles()
                                             .Where(f => mediaExtensions.Contains(f.Extension.ToLower()) && f.Name.Contains("@"))
                                             .Count();
            }

            DirectoryInfo drinfoArchived = new DirectoryInfo(ArchivedFolderPath);
            if (drinfoArchived.Exists)
            {
                archived = drinfoArchived.EnumerateFiles()
                                             .Where(f => mediaExtensions.Contains(f.Extension.ToLower()) && f.Name.Contains("@"))
                                             .Count();
            }
            int totalFiles = pending + archived + videoCount;

            if (totalFiles > 0)
            {
                txbtotalfiles.Text = totalFiles.ToString();
                prgbar.Maximum = totalFiles;
                txbProcessed.Text = (archived + videoCount).ToString();// counter.ToString();
                prgbar.Value = (archived + videoCount);// counter;
            }
            if (IsAutoPurchaseActive)
            {
                if (lstImgInfoPreScanList.Count > 0 && (DateTime.Now - lastOrderChecktime).TotalSeconds > GapTimeInSec)
                {
                    lstImgInfoPreScanList.ForEach(a =>
                    {
                        if ((DateTime.Now - a.LastImgTime).TotalSeconds > GapTimeInSec)
                        {
                            AutoPurchasePostScan(a);
                            lstImgInfoPreScanList.Remove(a);
                        }
                    }
                    );
                    lastOrderChecktime = DateTime.Now;
                }
            }
        }
        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            Process[] processes = Process.GetProcesses();
            foreach (Process process in processes)
            {
                if (process.ProcessName == "DigiWifiImageProcessing")
                {
                    try
                    {
                        ServiceController controller = new ServiceController("DigiWifiImageProcessing");
                        controller.Stop();
                        controller.WaitForStatus(ServiceControllerStatus.Stopped);
                    }
                    catch (Exception)
                    {
                        //handle any exception here 
                    }
                }
            }
            try
            {
                for (int i = 0; i < FilesToDelete.Count; i++)
                {
                    if (File.Exists(FilesToDelete[i]))
                    {
                        File.Move(FilesToDelete[i], Path.Combine(ArchivedFolderPath, Path.GetFileName(FilesToDelete[i])));
                        FilesToDelete.RemoveAt(i);
                    }
                }
            }
            catch (Exception)
            { }
            foreach (Process process in processes)
            {
                if (process.ProcessName == "DigiWatcher")
                {
                    try
                    {
                        ServicePosInfoBusiness svcPosinfoBusiness = new ServicePosInfoBusiness();
                        svcPosinfoBusiness.StopService(true);
                        //MessageBox.Show("watcher stopped");
                        process.Kill();
                    }
                    catch (Exception)
                    {
                        //handle any exception here 
                    }
                }
            }
        }
        #endregion

        #region Common Methods

        private void AutoCorrectImageWIthCopy(string InputFilePath, string OutputFilePath)
        {
            try
            {
                using (FileStream fs = File.OpenRead(InputFilePath))
                {
                    using (MemoryStream ms = new MemoryStream())
                    {
                        fs.CopyTo(ms);
                        ms.Seek(0, SeekOrigin.Begin);

                        using (ImageMagick.MagickImage image = new ImageMagick.MagickImage(ms))
                        {
                            if (IsContrastCorrectionActive)
                                image.Contrast();
                            if (IsGammaCorrectionActive)
                                image.AutoGamma();// auto-gamma correction
                            if (IsNoiseReductionActive)
                                image.ReduceNoise();
                            //image.Normalize();
                            IsAutoColorCorrectionActive = false;
                            image.Write(OutputFilePath);
                            image.Dispose();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogFileWrite(ex.Message);
            }
        }

        /// <summary>
        /// Gets the configuration data.
        /// </summary>
        private void GetConfigData(int subStoreId)
        {
            try
            {
                ConfigurationInfo config = ConfigurationData.Where(x => x.DG_Substore_Id == subStoreId).FirstOrDefault();
                is_SemiOrder = Convert.ToBoolean(config.DG_SemiOrderMain);
                _BorderFolder = config.DG_Frame_Path.ToString();
                Directoryname = config.DG_Hot_Folder_Path;
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
            }
        }

        private void FillUserData(int photographerid)
        {
            try
            {
                ErrorHandler.ErrorHandler.LogFileWrite("FillUserData Inside function 1 ------------- " + photographerid);
                ErrorHandler.ErrorHandler.LogFileWrite("FillUserData Inside function line 2350 ------------- ");
                if (UserInfoList == null)
                {
                    ErrorHandler.ErrorHandler.LogFileWrite("UserInfoList is null");
                }

                UsersInfo userInfo = new UsersInfo();
                var result = UserInfoList.Where(x => x.DG_User_pkey == PhotoGrapherId);
                if (result != null && result.Count() > 0)
                {

                    userInfo = result.FirstOrDefault();
                    ErrorHandler.ErrorHandler.LogFileWrite("if (result != null && result.Count() > 0) ------------- ");
                }
                else
                {
                    if (userInfo != null)
                    {
                        userInfo = (new UserBusiness().GetUserDetailByUserId(PhotoGrapherId, string.Empty, 0).FirstOrDefault());
                        ErrorHandler.ErrorHandler.LogFileWrite("FillUserData Inside function 3 ------------- " + userInfo);
                        if (UserInfoList.Contains(userInfo))
                        {
                            ErrorHandler.ErrorHandler.LogFileWrite("FillUserData Inside function 3 ------------- " + userInfo.UserName);
                        }
                        else
                        {
                            UserInfoList.Add(userInfo);
                        }
                    }
                    else
                    {
                        ErrorHandler.ErrorHandler.LogFileWrite("userInfo is null ");
                    }
                }

                LocationID = userInfo.DG_Location_pkey;
                substoreId = userInfo.DG_Substore_ID;
                _locationName = userInfo.DG_Location_Name;
                ErrorHandler.ErrorHandler.LogFileWrite("FillUserData Inside function 4 ------------- " + LocationID + substoreId + _locationName);
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
                ErrorHandler.ErrorHandler.LogFileWrite("FillUserData Inside Exception  ------------- ");
            }
        }
        private void GetConfigDataList()
        {
            ConfigBusiness confObj = new ConfigBusiness();
            try
            {
                ErrorHandler.ErrorHandler.LogFileWrite("ConfigurationData Started");
                ConfigurationData = confObj.GetConfigurationData();
                if (ConfigurationData.Count > 0)
                {
                    ErrorHandler.ErrorHandler.LogFileWrite("ConfigurationData End" + ConfigurationData[0].DG_BG_Path);

                }
                ErrorHandler.ErrorHandler.LogFileWrite("ConfigurationData End 0 count");

            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
            }
            finally { confObj = null; }
        }

        private static RotateFlipType OrientationToFlipType(string orientation)
        {
            switch (int.Parse(orientation))
            {
                case 1:
                    return RotateFlipType.RotateNoneFlipNone;
                    break;
                case 2:
                    return RotateFlipType.RotateNoneFlipX;
                    break;
                case 3:
                    return RotateFlipType.Rotate180FlipNone;
                    break;
                case 4:
                    return RotateFlipType.Rotate180FlipX;
                    break;
                case 5:
                    return RotateFlipType.Rotate90FlipX;
                    break;
                case 6:
                    {
                        return RotateFlipType.Rotate90FlipNone;
                        isrotated = true;
                        break;
                    }
                case 7:
                    return RotateFlipType.Rotate270FlipX;
                    break;
                case 8:
                    {
                        return RotateFlipType.Rotate270FlipNone;
                        isrotated = true;
                    }
                    break;
                default:
                    return RotateFlipType.RotateNoneFlipNone;
            }
        }
        private bool SetUniqueCodeInfoForPreScan(int PhotoId, string BigThumbnailPath)
        {
            bool isCode = false;
            ImgInfoPostScanList item = null;
            //objDigiPhotoDataServices = new DigiPhotoDataServices();
            PhotoBusiness phBusiness = new PhotoBusiness();
            try
            {
                BarcodeReaderLib.barcode barcodeInfo = GetBarcodeInfo(BigThumbnailPath);

                if (barcodeInfo != null && ((int)barcodeInfo.BarcodeType == (int)BarcodeTypes.Code128 || (int)barcodeInfo.BarcodeType == (int)BarcodeTypes.Code39 || (int)barcodeInfo.BarcodeType == (int)BarcodeTypes.QRCode))
                {
                    UniqueCode = barcodeInfo.Text;
                    if (!String.IsNullOrWhiteSpace(QRWebURLReplace))
                        UniqueCode = UniqueCode.Replace(QRWebURLReplace, "");

                    codeFormat = barcodeInfo.BarcodeType.ToString();

                    if (lstImgInfoPreScanList != null)
                        item = lstImgInfoPreScanList.Where(x => x.PhotoGrapherId == PhotoGrapherId).FirstOrDefault();

                    if (item != null)
                    {
                        AutoPurchasePostScan(item);
                        int ind = lstImgInfoPreScanList.FindIndex(x => x.PhotoGrapherId == PhotoGrapherId);
                        if (ind >= 0 && ind < lstImgInfoPreScanList.Count)
                            lstImgInfoPreScanList.RemoveAt(lstImgInfoPreScanList.FindIndex(x => x.PhotoGrapherId == PhotoGrapherId));
                    }

                    phBusiness.DeletePhotoByPhotoId(PhotoId);
                    ImgInfoPostScanList imgInfoPreScanList = new ImgInfoPostScanList();
                    imgInfoPreScanList.Barcode = UniqueCode;
                    imgInfoPreScanList.Format = codeFormat;
                    imgInfoPreScanList.PhotoGrapherId = PhotoGrapherId;
                    imgInfoPreScanList.ListImgPhotoId.Add(PhotoId);
                    imgInfoPreScanList.LastImgTime = DateTime.Now;
                    lstImgInfoPreScanList.Add(imgInfoPreScanList);

                    isCode = false;
                }
                else
                {
                    if (lstImgInfoPreScanList != null)
                        item = lstImgInfoPreScanList.Where(x => x.PhotoGrapherId == PhotoGrapherId).FirstOrDefault();
                    if (item != null)
                    {
                        DateTime dt = DateTime.Now;
                        if ((dt - item.LastImgTime).TotalSeconds <= GapTimeInSec)
                        {
                            foreach (var obj in lstImgInfoPreScanList.Where(x => x.PhotoGrapherId == PhotoGrapherId))
                            {
                                obj.ListImgPhotoId.Add(PhotoId);
                                obj.LastImgTime = dt;
                            }
                        }
                        else
                        {
                            AutoPurchasePostScan(item);
                            int ind = lstImgInfoPreScanList.FindIndex(x => x.PhotoGrapherId == PhotoGrapherId);
                            if (ind >= 0 && ind < lstImgInfoPreScanList.Count)
                                lstImgInfoPreScanList.RemoveAt(lstImgInfoPreScanList.FindIndex(x => x.PhotoGrapherId == PhotoGrapherId));

                            ImgInfoPostScanList imgInfoPreScanList = new ImgInfoPostScanList();
                            imgInfoPreScanList.Barcode = "1111";
                            imgInfoPreScanList.Format = "Lost";
                            imgInfoPreScanList.PhotoGrapherId = PhotoGrapherId;
                            imgInfoPreScanList.ListImgPhotoId.Add(PhotoId);
                            imgInfoPreScanList.LastImgTime = DateTime.Now;
                            lstImgInfoPreScanList.Add(imgInfoPreScanList);
                        }
                    }
                    else
                    {
                        ImgInfoPostScanList imgInfoPreScanList = new ImgInfoPostScanList();
                        imgInfoPreScanList.Barcode = "1111";
                        imgInfoPreScanList.Format = "Lost";
                        imgInfoPreScanList.PhotoGrapherId = PhotoGrapherId;
                        imgInfoPreScanList.ListImgPhotoId.Add(PhotoId);
                        imgInfoPreScanList.LastImgTime = DateTime.Now;
                        lstImgInfoPreScanList.Add(imgInfoPreScanList);
                    }
                    isCode = true;
                }
            }

            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
            }
            return isCode;
        }
        private bool SetUniqueCodeInfoForPostScan(int PhotoId, string BigThumbnailPath)
        {
            bool isCode = true;
            // objDigiPhotoDataServices = new DigiPhotoDataServices();
            PhotoBusiness phBusines = new PhotoBusiness();
            try
            {
                BarcodeReaderLib.barcode barcodeInfo = GetBarcodeInfo(BigThumbnailPath);

                if (barcodeInfo != null && ((int)barcodeInfo.BarcodeType == (int)BarcodeTypes.Code128 || (int)barcodeInfo.BarcodeType == (int)BarcodeTypes.Code39 || (int)barcodeInfo.BarcodeType == (int)BarcodeTypes.QRCode))
                {
                    UniqueCode = barcodeInfo.Text;

                    codeFormat = barcodeInfo.BarcodeType.ToString();
                    var item = lstImgInfoPostScanList.Where(x => x.PhotoGrapherId == PhotoGrapherId).FirstOrDefault();
                    if (item != null)
                    {
                        foreach (var obj in lstImgInfoPostScanList.Where(x => x.PhotoGrapherId == PhotoGrapherId))
                        {
                            obj.Format = codeFormat;
                            if (!String.IsNullOrWhiteSpace(QRWebURLReplace))
                                obj.Barcode = UniqueCode.Replace(QRWebURLReplace, "");
                            else
                                obj.Barcode = UniqueCode;
                        }
                        phBusines.DeletePhotoByPhotoId(PhotoId);
                    }
                    isCode = true;
                }
                else
                {
                    if (lstImgInfoPostScanList.Count <= 0)
                    {
                        ImgInfoPostScanList frstList = new ImgInfoPostScanList();
                        frstList.ListImgPhotoId.Add(PhotoId);
                        frstList.PhotoGrapherId = PhotoGrapherId;
                        frstList.LastImgTime = DateTime.Now;
                        lstImgInfoPostScanList.Add(frstList);
                    }
                    else
                    {
                        var item = lstImgInfoPostScanList.Where(x => x.PhotoGrapherId == PhotoGrapherId).FirstOrDefault();
                        if (item == null)
                        {
                            ImgInfoPostScanList frstList = new ImgInfoPostScanList();
                            frstList.ListImgPhotoId.Add(PhotoId);
                            frstList.PhotoGrapherId = PhotoGrapherId;
                            frstList.LastImgTime = DateTime.Now;
                            lstImgInfoPostScanList.Add(frstList);
                            isCode = false;
                        }
                        else
                        {
                            DateTime dt = DateTime.Now;
                            if ((dt - item.LastImgTime).TotalSeconds <= GapTimeInSec)
                            {
                                foreach (var obj in lstImgInfoPostScanList.Where(x => x.PhotoGrapherId == PhotoGrapherId))
                                {
                                    obj.ListImgPhotoId.Add(PhotoId);
                                    obj.LastImgTime = dt;
                                }
                            }
                            else
                            {
                                //objDigiPhotoDataServices = new DigiPhotoDataServices();
                                phBusines.SetImageAssociationInfoForPostScan(item.ListImgPhotoId, "Lost", "1111", IsAnonymousCodeActive);
                                foreach (var obj in lstImgInfoPostScanList.Where(x => x.PhotoGrapherId == PhotoGrapherId))
                                {
                                    obj.ListImgPhotoId.Clear();
                                    obj.ListImgPhotoId.Add(PhotoId);
                                    obj.LastImgTime = dt;
                                }
                            }
                        }
                    }
                    isCode = false;
                }
            }
            catch { }

            return isCode;
        }
        private BarcodeReaderLib.barcode GetBarcodeInfo(string imgpath)
        {
            BarcodeReaderLib.BarcodeList BarCodeList = null;
            BarcodeReaderLib.BarcodeList QRCodeList = null;
            BarcodeReaderLib.barcode barcodeInfo = null;

            switch (MappingType)
            {
                case 401:
                    BarcodeDecoder barcodeDecoder = new BarcodeReaderLib.BarcodeDecoder();
                    barcodeDecoder.BarcodeTypes = (int)(BarcodeReaderLib.EBarcodeTypes.QRCode);// | BarcodeReaderLib.EBarcodeTypes.QRCodeUnrecognized);
                    barcodeDecoder.DecodeFile(imgpath);
                    QRCodeList = barcodeDecoder.Barcodes;
                    if (QRCodeList.length > 0)
                        barcodeInfo = QRCodeList.item(0);
                    break;

                case 402:
                    BarcodeDecoder barcodeDecoder1 = new BarcodeReaderLib.BarcodeDecoder();
                    barcodeDecoder1.LinearFindBarcodes = 1;
                    barcodeDecoder1.BarcodeTypes = (int)(BarcodeReaderLib.EBarcodeTypes.Code128) | (int)(BarcodeReaderLib.EBarcodeTypes.Code39);
                    barcodeDecoder1.LinearShowSymbologyID = false;
                    barcodeDecoder1.LinearShowCheckDigit = false;
                    barcodeDecoder1.DecodeFile(imgpath);
                    BarCodeList = barcodeDecoder1.Barcodes;
                    if (BarCodeList.length > 0)
                        barcodeInfo = BarCodeList.item(0);
                    break;

                case 405:
                    BarcodeDecoder barcodeDecoder2 = new BarcodeReaderLib.BarcodeDecoder();
                    barcodeDecoder2.BarcodeTypes = (int)(BarcodeReaderLib.EBarcodeTypes.QRCode);// | BarcodeReaderLib.EBarcodeTypes.QRCodeUnrecognized);
                    barcodeDecoder2.DecodeFile(imgpath);
                    QRCodeList = barcodeDecoder2.Barcodes;
                    BarcodeDecoder barcodeDecoder3 = new BarcodeReaderLib.BarcodeDecoder();
                    barcodeDecoder3.LinearFindBarcodes = 1;
                    barcodeDecoder3.BarcodeTypes = (int)(BarcodeReaderLib.EBarcodeTypes.Code128) | (int)(BarcodeReaderLib.EBarcodeTypes.Code39);//.LinearUnrecognized);
                    barcodeDecoder3.LinearShowCheckDigit = false;
                    barcodeDecoder3.LinearShowSymbologyID = false;
                    barcodeDecoder3.DecodeFile(imgpath);
                    BarCodeList = barcodeDecoder3.Barcodes;

                    if (QRCodeList.length == 1 && BarCodeList.length == 1)
                    {
                        barcodeInfo = QRCodeList.item(0);
                    }
                    else if (QRCodeList.length == 1 && BarCodeList.length < 1)
                    {
                        barcodeInfo = QRCodeList.item(0);
                    }
                    else if (QRCodeList.length < 1 && BarCodeList.length == 1)
                    {
                        barcodeInfo = BarCodeList.item(0);
                    }
                    break;

                default:
                    break;
            }
            return barcodeInfo;
        }
        private void SetNewConfigValues()
        {
            //objDigiPhotoDataServices = new DigiPhotoDataServices();
            ConfigBusiness configBusiness = new ConfigBusiness();
            bool IsRfidEnabled = false;
            List<iMIXConfigurationInfo> _objNewConfigList = configBusiness.GetNewConfigValues(substoreId);
            foreach (iMIXConfigurationInfo _objNewConfig in _objNewConfigList)
            {
                switch (_objNewConfig.IMIXConfigurationMasterId)
                {
                    case (int)ConfigParams.IsBarcodeActive:
                        IsBarcodeActive = string.IsNullOrWhiteSpace(_objNewConfig.ConfigurationValue) ? false : Convert.ToBoolean(_objNewConfig.ConfigurationValue);
                        break;
                    case (int)ConfigParams.DefaultMappingCode:
                        MappingType = Convert.ToInt32(_objNewConfig.ConfigurationValue);
                        break;
                    case (int)ConfigParams.ScanType:
                        scanType = Convert.ToInt32(_objNewConfig.ConfigurationValue);
                        break;
                    case (int)ConfigParams.LostImgTimeGap:
                        GapTimeInSec = Convert.ToInt32(_objNewConfig.ConfigurationValue);
                        break;
                    case (int)ConfigParams.IsAnonymousQrCodeEnabled:
                        IsAnonymousCodeActive = string.IsNullOrWhiteSpace(_objNewConfig.ConfigurationValue) ? false : Convert.ToBoolean(_objNewConfig.ConfigurationValue);
                        break;
                    case (int)ConfigParams.Contrast:
                        defaultContrast = _objNewConfig.ConfigurationValue != null ? _objNewConfig.ConfigurationValue : "1";
                        break;
                    case (int)ConfigParams.Brightness:
                        defaultBrightness = _objNewConfig.ConfigurationValue != null ? _objNewConfig.ConfigurationValue : "0";
                        break;
                    case (int)ConfigParams.IsPrepaidAutoPurchaseActive:
                        IsAutoPurchaseActive = string.IsNullOrWhiteSpace(_objNewConfig.ConfigurationValue) ? false : Convert.ToBoolean(_objNewConfig.ConfigurationValue);
                        break;
                    case (int)ConfigParams.IsEnabledRFID:
                        IsRfidEnabled = string.IsNullOrWhiteSpace(_objNewConfig.ConfigurationValue) ? false : Convert.ToBoolean(_objNewConfig.ConfigurationValue);
                        break;
                    case (int)ConfigParams.RFIDScanType:
                        RfidScanType = string.IsNullOrWhiteSpace(_objNewConfig.ConfigurationValue) ? 0 : Convert.ToInt32(_objNewConfig.ConfigurationValue);
                        break;
                    case (int)ConfigParams.IsGreenScreenFlow:
                        IsGreenScreenWorkFlow = string.IsNullOrWhiteSpace(_objNewConfig.ConfigurationValue) ? false : Convert.ToBoolean(_objNewConfig.ConfigurationValue);
                        break;
                    //  case (int)ConfigParams.MediaPlayerLicenseKey:
                    //     _VisioForgeLicenseKey = _objNewConfig.ConfigurationValue != null ? _objNewConfig.ConfigurationValue : string.Empty;
                    //    break;

                    default:
                        break;
                }
            }
            RfidScanType = IsRfidEnabled == true ? RfidScanType : null;
        }
        bool IsFrameExtractionEnabled = false;
        private void SetNewConfigLocationValues(int locationId)
        {

            bool IsRfidEnabled = false;
            List<iMixConfigurationLocationInfo> _objNewConfigList = new List<iMixConfigurationLocationInfo>();
            var result = NewConfigList.Where(x => x.SubstoreId == substoreId && x.LocationId == locationId).FirstOrDefault();
            if (result != null)
            {
                _objNewConfigList = result.iMixConfigurationLocationList;
            }
            else
            {
                ConfigBusiness configBusiness = new ConfigBusiness();
                _objNewConfigList = configBusiness.GetConfigLocation(locationId, substoreId);
                iMixConfigurationLocationInfoList configList = new iMixConfigurationLocationInfoList();
                configList.iMixConfigurationLocationList = _objNewConfigList;
                configList.SubstoreId = substoreId;
                configList.LocationId = locationId;
                NewConfigList.Add(configList);
            }

            foreach (iMixConfigurationLocationInfo _objNewConfig in _objNewConfigList)
            {
                switch (_objNewConfig.IMIXConfigurationMasterId)
                {
                    case (int)ConfigParams.IsBarcodeActive:
                        IsBarcodeActive = string.IsNullOrWhiteSpace(_objNewConfig.ConfigurationValue) ? false : Convert.ToBoolean(_objNewConfig.ConfigurationValue);
                        break;
                    case (int)ConfigParams.DefaultMappingCode:
                        MappingType = Convert.ToInt32(_objNewConfig.ConfigurationValue);
                        break;
                    case (int)ConfigParams.ScanType:
                        scanType = Convert.ToInt32(_objNewConfig.ConfigurationValue);
                        break;
                    case (int)ConfigParams.LostImgTimeGap:
                        GapTimeInSec = Convert.ToInt32(_objNewConfig.ConfigurationValue);
                        break;
                    case (int)ConfigParams.IsAnonymousQrCodeEnabled:
                        IsAnonymousCodeActive = string.IsNullOrWhiteSpace(_objNewConfig.ConfigurationValue) ? false : Convert.ToBoolean(_objNewConfig.ConfigurationValue);
                        break;
                    case (int)ConfigParams.Contrast:
                        defaultContrast = _objNewConfig.ConfigurationValue != null ? _objNewConfig.ConfigurationValue : "1";
                        break;
                    case (int)ConfigParams.Brightness:
                        defaultBrightness = _objNewConfig.ConfigurationValue != null ? _objNewConfig.ConfigurationValue : "0";
                        break;
                    case (int)ConfigParams.IsPrepaidAutoPurchaseActive:
                        IsAutoPurchaseActive = string.IsNullOrWhiteSpace(_objNewConfig.ConfigurationValue) ? false : Convert.ToBoolean(_objNewConfig.ConfigurationValue);
                        break;
                    case (int)ConfigParams.IsEnabledRFID:
                        IsRfidEnabled = string.IsNullOrWhiteSpace(_objNewConfig.ConfigurationValue) ? false : Convert.ToBoolean(_objNewConfig.ConfigurationValue);
                        break;
                    case (int)ConfigParams.IsMobileRfidEnabled:
                        IsMobileRfidEnabled = string.IsNullOrWhiteSpace(_objNewConfig.ConfigurationValue) ? false : Convert.ToBoolean(_objNewConfig.ConfigurationValue);
                        break;
                    case (int)ConfigParams.RFIDScanType:
                        //if (IsRfidEnabled == true && !string.IsNullOrWhiteSpace(_objNewConfig.ConfigurationValue))
                        if (!string.IsNullOrWhiteSpace(_objNewConfig.ConfigurationValue))
                            RfidScanType = Convert.ToInt32(_objNewConfig.ConfigurationValue);
                        else
                            RfidScanType = null;
                        break;
                    case (int)ConfigParams.IsEnableAutoVidProcessing:

                        isAutoVideoProcessingActive = string.IsNullOrWhiteSpace(_objNewConfig.ConfigurationValue) ? false : Convert.ToBoolean(_objNewConfig.ConfigurationValue);
                        break;
                    case (int)ConfigParams.IsAutoColorCorrection:
                        IsAutoColorCorrectionActive = string.IsNullOrWhiteSpace(_objNewConfig.ConfigurationValue) ? false : Convert.ToBoolean(_objNewConfig.ConfigurationValue);
                        break;
                    case (int)ConfigParams.IsCorrectAtDownloadActive:
                        IsCorrectionAtDownloadActive = string.IsNullOrWhiteSpace(_objNewConfig.ConfigurationValue) ? false : Convert.ToBoolean(_objNewConfig.ConfigurationValue);
                        break;
                    case (int)ConfigParams.IsContrastCorrection:
                        IsContrastCorrectionActive = string.IsNullOrWhiteSpace(_objNewConfig.ConfigurationValue) ? false : Convert.ToBoolean(_objNewConfig.ConfigurationValue);
                        break;
                    case (int)ConfigParams.IsGammaCorrection:
                        IsGammaCorrectionActive = string.IsNullOrWhiteSpace(_objNewConfig.ConfigurationValue) ? false : Convert.ToBoolean(_objNewConfig.ConfigurationValue);
                        break;
                    case (int)ConfigParams.IsNoiseReduction:
                        IsNoiseReductionActive = string.IsNullOrWhiteSpace(_objNewConfig.ConfigurationValue) ? false : Convert.ToBoolean(_objNewConfig.ConfigurationValue);
                        break;
                    case (int)ConfigParams.IsAdvancedVideoEditActive:
                        _IsAdvancedVideoEditActive = string.IsNullOrWhiteSpace(_objNewConfig.ConfigurationValue) ? false : Convert.ToBoolean(_objNewConfig.ConfigurationValue);
                        break;
                    case (int)ConfigParams.IsAutoVideoFrameExtEnabled:
                        IsFrameExtractionEnabled = string.IsNullOrWhiteSpace(_objNewConfig.ConfigurationValue) ? false : Convert.ToBoolean(_objNewConfig.ConfigurationValue);
                        break;
                    case (int)ConfigParams.VideoFramePositions:
                        videoFramePositions = _objNewConfig.ConfigurationValue != null ? _objNewConfig.ConfigurationValue : "Empty";
                        break;
                    case (int)ConfigParams.VideoFrameCropRatio:
                        cropVideoFrameRatio = _objNewConfig.ConfigurationValue != null ? _objNewConfig.ConfigurationValue : "None";
                        break;
                    default:
                        break;
                }
            }
            RfidScanType = IsRfidEnabled == true ? RfidScanType : null;
        }
        /// <summary>
        /// Gets the bitmap image from path.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <returns></returns>
        public BitmapImage GetBitmapImageFromPath(string value)
        {
            BitmapImage bi = new BitmapImage();
            try
            {
                if (value != null)
                {
                    using (FileStream fileStream = File.OpenRead(value.ToString()))
                    {
                        MemoryStream ms = new MemoryStream();
                        fileStream.CopyTo(ms);
                        ms.Seek(0, SeekOrigin.Begin);
                        fileStream.Close();
                        bi.BeginInit();
                        bi.StreamSource = ms;
                        bi.EndInit();
                    }
                }
                else
                {
                    bi = new BitmapImage();
                }
                return bi;
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
                return bi;
            }
        }
        /// <summary>
        /// Saves the XML.
        /// </summary>
        /// <param name="bvalue">The bvalue.</param>
        /// <param name="childnode">if set to <c>true</c> [childnode].</param>
        /// <param name="rfid">The rfid.</param>
        /// <param name="cvalue">The cvalue.</param>
        /// <param name="fvalue">The fvalue.</param>
        /// <param name="fvvalue">The fvvalue.</param>
        /// <param name="BG">The debug.</param>
        /// <param name="glayer">The glayer.</param>
        /// <param name="photoId">The photo unique identifier.</param>
        /// <param name="ZoomDetails">The zoom details.</param>
        /// 
        public void SaveXml(SemiOrderSettings semiOrderSettings, int photoId, string filedatepath, string fileName, string GumBallRidetxtContent)
        {
            _QRCode = null;
            string layeringdata = "";
            StringBuilder PlayerXml = new StringBuilder();
            string[] NoOfPlayers;
            NoOfPlayers = GumBallRidetxtContent.Split(',');
            int ProductId = 0;
            if (semiOrderSettings.DG_SemiOrder_ProductTypeId.Contains(','))
                ProductId = 1;
            else
                ProductId = Convert.ToInt32(semiOrderSettings.DG_SemiOrder_ProductTypeId);
            System.Xml.XmlDocument Xdocument = new System.Xml.XmlDocument();
            Xdocument.LoadXml(ImageEffect);

            System.Xml.XmlNodeList list = Xdocument.SelectNodes("//image");

            foreach (System.Xml.XmlElement XElement in list)
            {
                XElement.SetAttribute("brightness", Convert.ToString(semiOrderSettings.DG_SemiOrder_Settings_AutoBright_Value));
            }

            foreach (System.Xml.XmlElement XElement in list)
            {
                XElement.SetAttribute("contrast", Convert.ToString(semiOrderSettings.DG_SemiOrder_Settings_AutoContrast_Value));
            }

            if (semiOrderSettings.DG_SemiOrder_IsCropActive.ToBoolean())
            {
                foreach (System.Xml.XmlElement XElement in list)
                {
                    if (ProductId == 1)
                        XElement.SetAttribute("Crop", "6 * 8");
                    else if (ProductId == 2 || ProductId == 3)
                        XElement.SetAttribute("Crop", "8 * 10");
                    else if (ProductId == 30 || ProductId == 5 || ProductId == 104)
                        XElement.SetAttribute("Crop", "4 * 6");
                    else if (ProductId == 98)
                        XElement.SetAttribute("Crop", "3 * 3");
                }
            }

            if (semiOrderSettings.ProductName.Contains("6 * 8"))
                ProductNamePreference = "1";
            else if (semiOrderSettings.ProductName.Contains("8 * 10"))
                ProductNamePreference = "2";
            else if (semiOrderSettings.ProductName.Contains("4 * 6") || semiOrderSettings.ProductName.Contains("(4x6) & QR") || semiOrderSettings.ProductName.Contains("4 Small Wallets"))
                ProductNamePreference = "30";

            else if (semiOrderSettings.ProductName.Contains("6 * 20"))
                ProductNamePreference = "125";
            else if (semiOrderSettings.ProductName.Contains("6 * 14"))
                ProductNamePreference = "123";
            else if (semiOrderSettings.ProductName.Contains("8 * 18"))
                ProductNamePreference = "122";
            else if (semiOrderSettings.ProductName.Contains("8 * 26"))
                ProductNamePreference = "121";
            else if (semiOrderSettings.ProductName.Contains("8 * 24"))
                ProductNamePreference = "120";

            else
                ProductNamePreference = "2";


            #region Gumball Code
            bool IsGumballShow = false;
            if (GumBallActiveLocationList != null && GumBallActiveLocationList.Count > 0)
                LstGumBallRideConfigValueLocationWise = GumBallConfigurationValueList.Where(x => x.LocationId.Equals(LocationID) && GumBallActiveLocationList.Contains(x.LocationId)).ToList();

            if (LstGumBallRideConfigValueLocationWise != null && LstGumBallRideConfigValueLocationWise.Count > 0)
            {
                if (!string.IsNullOrWhiteSpace(NoOfPlayers[0]) && NoOfPlayers.Length != 1)
                {
                    string[] playerPosition = GetAllPlayerPositions(LstGumBallRideConfigValueLocationWise.Where(x => x.IMIXConfigurationMasterId.Equals(Convert.ToInt32(ConfigParams.GumRidePosition))).Select(x => x.ConfigurationValue).FirstOrDefault());
                    string[] playerMargin = GetAllPlayerMargins(LstGumBallRideConfigValueLocationWise.Where(x => x.IMIXConfigurationMasterId.Equals(Convert.ToInt32(ConfigParams.GumRideMargin))).Select(x => x.ConfigurationValue).FirstOrDefault());

                    //--------Start----Nilesh-----------Dynamically Add Img----------24th Feb 2018--------
                    string[] playerBGImgPath = GetAllPlayerBGImg(LstGumBallRideConfigValueLocationWise.Where(x => x.IMIXConfigurationMasterId.Equals(Convert.ToInt32(ConfigParams.GumBGImagePath))).Select(x => x.ConfigurationValue).FirstOrDefault());
                    string[] playerBGImgHeigthWidth = GetAllPlayerBGImgHeightWidth(LstGumBallRideConfigValueLocationWise.Where(x => x.IMIXConfigurationMasterId.Equals(Convert.ToInt32(ConfigParams.GumBGImgHeightWidth))).Select(x => x.ConfigurationValue).FirstOrDefault());
                    string[] playerBGImgScorePostition = GetAllPlayerScoreBGImgPosition(LstGumBallRideConfigValueLocationWise.Where(x => x.IMIXConfigurationMasterId.Equals(Convert.ToInt32(ConfigParams.GumBGImgPosition))).Select(x => x.ConfigurationValue).FirstOrDefault());

                    //--------End----Nilesh-----------Dynamically Add Img----------24th Feb 2018--------

                    isVisibleZeroScoreOnImage = LstGumBallRideConfigValueLocationWise.Where(x => x.IMIXConfigurationMasterId.Equals(Convert.ToInt32(ConfigParams.IsVisibleGumBallZeroScoreOnImage))).Select(x => x.ConfigurationValue).FirstOrDefault();
                    int playercount = NoOfPlayers.Length;
                    if (playercount > 6)
                        playercount = 6;
                    for (int i = 0; i < playercount; i++)
                    {
                        try
                        {
                            string playerPos = !String.IsNullOrEmpty(playerPosition[i]) ? playerPosition[i] : string.Empty;
                            string playerMrg = !String.IsNullOrEmpty(playerMargin[i]) ? playerMargin[i] : string.Empty;
                            //--------Start----Nilesh-----------Dynamically Add Img----------24th Feb 2018--------
                            string playerimgpath = !String.IsNullOrEmpty(playerBGImgPath[i]) ? playerBGImgPath[i] : string.Empty;
                            string playerimgHW = !String.IsNullOrEmpty(playerBGImgHeigthWidth[i]) ? playerBGImgHeigthWidth[i] : string.Empty;
                            string playerScorePs = !String.IsNullOrEmpty(playerBGImgScorePostition[i]) ? playerBGImgScorePostition[i] : string.Empty;

                            //--------End----Nilesh-----------Dynamically Add Img----------24th Feb 2018--------

                            string[] fontcolorArray = LstGumBallRideConfigValueLocationWise.Where(x => x.IMIXConfigurationMasterId.Equals(Convert.ToInt32(ConfigParams.GumRideFontColor))).Select(x => x.ConfigurationValue).FirstOrDefault().Split(',');
                            string[] BGcolorArray = LstGumBallRideConfigValueLocationWise.Where(x => x.IMIXConfigurationMasterId.Equals(Convert.ToInt32(ConfigParams.GumRideBackgrondColor))).Select(x => x.ConfigurationValue).FirstOrDefault().Split(',');
                            if (isVisibleZeroScoreOnImage.ToUpper() == "FALSE")
                                if (NoOfPlayers[i].Equals("0"))
                                    continue;
                            PlayerXml.Append("<Gumball player='" + (i + 1) + "' zindex='7' text='Player" + (i + 1) + "=" + NoOfPlayers[i] + "' foreground='" + fontcolorArray[i] + "' fontfamily='" + LstGumBallRideConfigValueLocationWise.Where(x => x.IMIXConfigurationMasterId.Equals(Convert.ToInt32(ConfigParams.GumRideFontFamily))).
                            Select(x => x.ConfigurationValue).FirstOrDefault() + "' fontsize='" + LstGumBallRideConfigValueLocationWise.Where(x => x.IMIXConfigurationMasterId.Equals(Convert.ToInt32(ConfigParams.GumRideFontSize))).
                            Select(x => x.ConfigurationValue).FirstOrDefault() + "' fontweight='" + LstGumBallRideConfigValueLocationWise.Where(x => x.IMIXConfigurationMasterId.Equals(Convert.ToInt32(ConfigParams.GumRideFontWeight))).
                            Select(x => x.ConfigurationValue).FirstOrDefault() + "' fontStyle='" + LstGumBallRideConfigValueLocationWise.Where(x => x.IMIXConfigurationMasterId.Equals(Convert.ToInt32(ConfigParams.GumRideFontStyle))).
                            Select(x => x.ConfigurationValue).FirstOrDefault() + "' background='" + BGcolorArray[i] + "' "
                           + " position='" + playerPos + "'" + " margin='" + playerMrg + "'"
                            + " BGImgPath='" + playerimgpath + "'"
                            + " BGImgHeightWidth='" + playerimgHW + "'"
                            + " BGImgScorePosition='" + playerScorePs + "'"
                            +
                            " />");
                        }
                        catch (Exception ex)
                        {
                        }
                    }
                }
                if (LstGumBallRideConfigValueLocationWise.Where(x => x.IMIXConfigurationMasterId.Equals(Convert.ToInt32(ConfigParams.IsGumPlayerScoreVisible))).Select(x => x.ConfigurationValue).FirstOrDefault().ToUpper() == "TRUE")
                {
                    IsGumballShow = true;
                }
            }
            #endregion

            PhotoBusiness phBusiness = new PhotoBusiness();
            string fname = string.Empty;
            string[] zoomdetails;
            string graphicLayer = string.Empty;
            string textLogo = string.Empty;
            string background = string.Empty;
            bool IsCropActive = false;
            bool IsChromaActive = false;
            BitmapImage _objPhoto = GetBitmapImageFromPath(Path.Combine(filedatepath, fileName));
            //commented by ajay sinha on 16 july 2019 to resolve applying border to the normal- horizontal  image to  vertical.
            //if (_objPhoto.Height > _objPhoto.Width)
            if ((_objPhoto.Height > _objPhoto.Width) || !String.IsNullOrWhiteSpace(semiOrderSettings.VerticalCropValues))
            {
                fname = Convert.ToString(semiOrderSettings.ImageFrame_Vertical);
                zoomdetails = semiOrderSettings.ZoomInfo_Vertical.Split(',');
                graphicLayer = semiOrderSettings.Graphics_layer_Vertical;
                textLogo = semiOrderSettings.TextLogo_Vertical;
                background = semiOrderSettings.Background_Vertical;
                if (semiOrderSettings.DG_SemiOrder_Settings_IsImageBG.Value && !String.IsNullOrWhiteSpace(background))
                    IsChromaActive = true;

                if (semiOrderSettings.DG_SemiOrder_IsCropActive.ToBoolean() == true)
                    IsCropActive = String.IsNullOrWhiteSpace(semiOrderSettings.VerticalCropValues) == true ? false : true;
            }
            else
            {
                fname = Convert.ToString(semiOrderSettings.ImageFrame_Horizontal);
                zoomdetails = semiOrderSettings.ZoomInfo_Horizontal.Split(',');
                graphicLayer = semiOrderSettings.Graphics_layer_Horizontal;
                textLogo = semiOrderSettings.TextLogo_Horizontal;
                background = semiOrderSettings.Background_Horizontal;
                if (semiOrderSettings.DG_SemiOrder_Settings_IsImageBG.Value && !String.IsNullOrWhiteSpace(background))
                    IsChromaActive = true;
                if (semiOrderSettings.DG_SemiOrder_IsCropActive.ToBoolean() == true)
                    IsCropActive = String.IsNullOrWhiteSpace(semiOrderSettings.HorizontalCropValues) == true ? false : true;
            }
            string zoomFactor = "1";
            string canvasleft = "0";
            string canvastop = "0";
            string scalecentrex = "-1";
            string scalecentrey = "-1";
            // Vertical Layering Effects
            if (zoomdetails.Count() > 1)
            {
                zoomFactor = zoomdetails[0];
                canvasleft = zoomdetails[1];
                canvastop = zoomdetails[2];
                scalecentrex = zoomdetails[3];
                scalecentrey = zoomdetails[4];
            }
            layeringdata = "<photo producttype='" + ProductNamePreference + "' zoomfactor='" + zoomFactor + "' border='" + fname + "' bg= '" + background + "' canvasleft='" + canvasleft + "' canvastop='" + canvastop + "' scalecentrex='" + scalecentrex + "' scalecentrey='" + scalecentrey + "'>" + graphicLayer + PlayerXml + textLogo + "</photo>";
            //if (ProductId == 104 || semiOrderSettings.ProductName.Contains("(4x6) & QR"))
            if (ProductId == 104 || semiOrderSettings.ProductName.Contains("(4x6) & QR") || ProductId == 124 || semiOrderSettings.ProductName.Contains("(4*6) & QR"))//Added by VINS
                _QRCode = GenerateQRCode.GetNextQRCode(_QRCodeLenth);

            ImageEffect = Xdocument.InnerXml.ToString();
            phBusiness.UpdateEffectsSpecPrint(photoId, layeringdata, ImageEffect, IsChromaActive, IsCropActive, IsGumballShow, GumBallRidetxtContent, _QRCode);
            GumBallRidetxtContent = string.Empty;
            PlayerXml.Length = 0;
        }

        private Int32 QRCodeLength(Int32 subStoreId)
        {
            List<long> filterValues = new List<long>();
            filterValues.Add((Int32)ConfigParams.QRCodeLengthSetting);
            ConfigBusiness conBiz = new ConfigBusiness();
            iMIXConfigurationInfo ConfigValuesList = conBiz.GetNewConfigValues(subStoreId).Where(o => o.IMIXConfigurationMasterId == (Int32)ConfigParams.QRCodeLengthSetting).FirstOrDefault();
            if (ConfigValuesList != null)
                _QRCodeLenth = ConfigValuesList.ConfigurationValue != null ? Convert.ToInt32(ConfigValuesList.ConfigurationValue) : 0;
            return _QRCodeLenth;
        }
        private string[] GetAllPlayerPositions(string allPos)
        {
            string[] singlePos = allPos.Split(',');
            if (singlePos != null && singlePos.Length > 0)
                return singlePos;
            else
                return null;
        }
        private string[] GetAllPlayerMargins(string allMargins)
        {
            string[] singleMargin = allMargins.Split(':');
            if (singleMargin != null && singleMargin.Length > 0)
                return singleMargin;
            else
                return null;
        }
        /// <summary>
        /// Determines whether [is file locked] [the specified file].
        /// </summary>
        /// <param name="file">The file.</param>
        /// <returns></returns>
        protected bool IsFileLocked(FileInfo file)
        {
            FileStream stream = null;

            try
            {
                stream = file.Open(FileMode.Open, FileAccess.ReadWrite, FileShare.None);
            }
            catch (IOException)
            {
                //the file is unavailable because it is: 
                //still being written to 
                //or being processed by another thread 
                //or does not exist (has already been processed) 
                return true;
            }
            finally
            {
                if (stream != null)
                {
                    stream.Close();
                    stream.Dispose();
                }
            }

            //file is not locked 
            return false;
        }
        private bool ReadSystemOnlineStatus()
        {
            bool status = false;
            try
            {
                string pathtosave = System.IO.Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
                if (File.Exists(pathtosave + "\\Config.dat"))
                {
                    string OnlineStatus;
                    using (StreamReader reader = new StreamReader(pathtosave + "\\Config.dat"))
                    {
                        OnlineStatus = reader.ReadLine();
                        status = Convert.ToBoolean(OnlineStatus.Split(',')[1]);
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
            }
            return status;
        }
        private string GenerateOrderNumber()
        {
            string orderNumber = LoginUser.OrderPrefix + "-";
            string uniqueNumber = string.Empty;
            try
            {
                using (RNGCryptoServiceProvider rng = new RNGCryptoServiceProvider())
                {
                    // Buffer storage.
                    byte[] data = new byte[4];

                    // Ten iterations.                     
                    {
                        // Fill buffer.
                        rng.GetBytes(data);

                        // Convert to int 32.
                        int value = BitConverter.ToInt32(data, 0);
                        if (value < 0)
                            value = -value;
                        if (value.ToString().Length > 10)
                        {
                            uniqueNumber = value.ToString().Substring(0, 10);
                        }
                        else
                        {
                            uniqueNumber = value.ToString();
                        }
                    }
                }
            }
            catch (Exception ex)
            { ErrorHandler.ErrorHandler.LogFileWrite(ex.Message); }
            orderNumber = orderNumber + uniqueNumber;
            return orderNumber;
        }
        private void GetStoreName()
        {
            StoreSubStoreDataBusniess storeObj = new StoreSubStoreDataBusniess();
            List<DigiPhoto.IMIX.Model.StoreInfo> objstore = storeObj.GetStoreName();
            LoginUser.countrycode = objstore.FirstOrDefault().CountryCode;
            LoginUser.Storecode = objstore.FirstOrDefault().StoreCode;
        }
        public string ConvertDegreeAngleToDouble(string degrees, string minutes, string seconds)
        {
            //Decimal degrees = 
            //   whole number of degrees, 
            //   plus minutes divided by 60, 
            //   plus seconds divided by 3600

            return (Convert.ToDouble(degrees) + (Convert.ToDouble(minutes) / 60) + (Convert.ToDouble(seconds) / 3600)).ToString();
        }
        private void ResetLowDPI(string InputFilePath, string OutputFilePath)
        {
            try
            {
                System.Drawing.Image newImage = System.Drawing.Image.FromFile(InputFilePath);
                System.Drawing.Image outImage = ResetDPI.ScaleByHeightAndResolution(newImage, 2136, 300);

                if (IsAutoColorCorrectionActive && IsCorrectionAtDownloadActive)
                {
                    using (MemoryStream ms = new MemoryStream())
                    {
                        outImage.Save(ms, System.Drawing.Imaging.ImageFormat.Jpeg);
                        ms.Seek(0, SeekOrigin.Begin);

                        using (ImageMagick.MagickImage image = new ImageMagick.MagickImage(ms))
                        {
                            if (IsContrastCorrectionActive)
                                image.Contrast();
                            if (IsGammaCorrectionActive)
                                image.AutoGamma();// auto-gamma correction
                            if (IsNoiseReductionActive)
                                image.ReduceNoise();
                            //image.Normalize();
                            IsAutoColorCorrectionActive = false;
                            image.Write(OutputFilePath);
                            image.Dispose();
                        }
                    }
                }
                else
                {
                    outImage.Save(OutputFilePath);
                }
                newImage.Dispose();
                outImage.Dispose();
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogFileWrite(ex.Message);
            }
        }
        private void RotateImage(string rotatePath)
        {
            try
            {
                ImageMagickObject.MagickImage jmagic = new ImageMagickObject.MagickImage();

                if (needrotaion > 0)
                {
                    if (needrotaion == 90)
                    {
                        object[] o = new object[] { "-rotate", " 90 ", rotatePath };
                        jmagic.Mogrify(o);
                        o = null;
                        isrotated = true;
                    }
                    else if (needrotaion == 180)
                    {
                        object[] o = new object[] { "-rotate", " 180 ", rotatePath };
                        jmagic.Mogrify(o);
                        o = null;
                        isrotated = false;
                    }
                    else if (needrotaion == 270)
                    {
                        object[] o = new object[] { "-rotate", " 270 ", rotatePath };
                        jmagic.Mogrify(o);
                        o = null;
                        isrotated = true;
                    }
                }
                jmagic = null;
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
            }
        }
        private bool CheckResetDPIRequired(int hr, int vr, string PhotographerId)
        {
            try
            {
                bool cameraSetting = (new CameraBusiness()).GetIsResetDPIRequired(Convert.ToInt32(PhotographerId));
                if ((hr != 300 || vr != 300) && cameraSetting)
                    return true;
                else
                    return false;
            }
            catch
            {
                return false;
            }
        }
        #endregion

        //void MainWindow_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        //{
        //    ServicePosInfoBusiness svcPosinfoBusiness = new ServicePosInfoBusiness();            
        //    svcPosinfoBusiness.StopService(true);
        //}

        //void MainWindow_Loaded(object sender, System.Windows.RoutedEventArgs e)
        //{           
        //    ServicePosInfoBusiness svcPosinfoBusiness = new ServicePosInfoBusiness();
        //    svcPosinfoBusiness.ServiceStart(true);
        //    MessageBox.Show("called");
        //}
        private void SetGumRideMasterId()
        {
            GumRideList = new List<long>();
            GumRideList.Add(Convert.ToInt64(ConfigParams.GumRideAvailableLocations));
            GumRideList.Add(Convert.ToInt64(ConfigParams.GumRideFontSize));
            GumRideList.Add(Convert.ToInt64(ConfigParams.GumRideFontColor));
            GumRideList.Add(Convert.ToInt64(ConfigParams.GumRideFontWeight));
            GumRideList.Add(Convert.ToInt64(ConfigParams.GumRideBackgrondColor));
            GumRideList.Add(Convert.ToInt64(ConfigParams.GumRidePosition));
            GumRideList.Add(Convert.ToInt64(ConfigParams.GumRideMargin));
            GumRideList.Add(Convert.ToInt64(ConfigParams.GumRideFilePath));
            GumRideList.Add(Convert.ToInt64(ConfigParams.IsGumRideActive));
            GumRideList.Add(Convert.ToInt64(ConfigParams.IsGumPlayerScoreVisible));
            GumRideList.Add(Convert.ToInt64(ConfigParams.GumRideFontStyle));
            GumRideList.Add(Convert.ToInt64(ConfigParams.GumRideFontFamily));
            GumRideList.Add(Convert.ToInt64(ConfigParams.IsVisibleGumBallZeroScore));
            GumRideList.Add(Convert.ToInt64(ConfigParams.IsVisibleGumBallZeroScoreOnImage));
            GumRideList.Add(Convert.ToInt64(ConfigParams.GumRideInputPhotoPath));
            GumRideList.Add(Convert.ToInt64(ConfigParams.GumballScoreSeperater));
            GumRideList.Add(Convert.ToInt64(ConfigParams.IsPrefixActiveFlow));
            GumRideList.Add(Convert.ToInt64(ConfigParams.PrefixPhotoName));
            GumRideList.Add(Convert.ToInt64(ConfigParams.PrefixScoreFileName));
            //------Start------Nilesh-----Dynamically add img BG for Gumball-------24th Feb2018-----
            GumRideList.Add(Convert.ToInt64(ConfigParams.GumBGImagePath));
            GumRideList.Add(Convert.ToInt64(ConfigParams.GumBGImgHeightWidth));
            GumRideList.Add(Convert.ToInt64(ConfigParams.GumBGImgPosition));
            //------End------Nilesh-----Dynamically add img BG for Gumball-------24th Feb2018-----
        }
        private string[] GetAllPlayerBGImg(string allBGImg)
        {
            string[] singleBGImg = allBGImg.Split(',');
            if (singleBGImg != null && singleBGImg.Length > 0)
                return singleBGImg;
            else
                return null;
        }

        private string[] GetAllPlayerBGImgHeightWidth(string allBGImgHeightWidth)
        {
            string[] singleBGImgHeightWidth = allBGImgHeightWidth.Split(',');
            if (singleBGImgHeightWidth != null && singleBGImgHeightWidth.Length > 0)
                return singleBGImgHeightWidth;
            else
                return null;
        }

        private string[] GetAllPlayerScoreBGImgPosition(string allScorePost)
        {
            string[] singleBGImgHeightWidth = allScorePost.Split(',');
            if (singleBGImgHeightWidth != null && singleBGImgHeightWidth.Length > 0)
                return singleBGImgHeightWidth;
            else
                return null;
        }
    }
    public class MemoryManagement
    {
        /// <summary>
        /// Sets the size of the process working set.
        /// </summary>
        /// <param name="process">The process.</param>
        /// <param name="minimumWorkingSetSize">Minimum size of the working set.</param>
        /// <param name="maximumWorkingSetSize">Maximum size of the working set.</param>
        /// <returns></returns>
        [DllImportAttribute("kernel32.dll", EntryPoint = "SetProcessWorkingSetSize", ExactSpelling = true, CharSet =
        CharSet.Ansi, SetLastError = true)]

        private static extern int SetProcessWorkingSetSize(IntPtr process, int minimumWorkingSetSize, int
        maximumWorkingSetSize);

        /// <summary>
        /// Flushes the memory.
        /// </summardy>
        public static void FlushMemory()
        {
            GC.Collect();
            GC.WaitForPendingFinalizers();
            if (Environment.OSVersion.Platform == PlatformID.Win32NT)
            {
                SetProcessWorkingSetSize(System.Diagnostics.Process.GetCurrentProcess().Handle, -1, -1);
            }
            //GC.Collect();
        }

    }
    public class ImgCopyLocation
    {
        public Int32 locID { get; set; }
        public string locName { get; set; }
        public Int32 counter { get; set; }
        public Int32 range { get; set; }

    }

}
