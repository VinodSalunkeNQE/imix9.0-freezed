﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceProcess;
using System.Text;

namespace DigiImageProcessor
{
    static class Program
    {
        static void Main()
        {
            //ImageProcessing imgpro = new ImageProcessing();
            //imgpro.TimerFunction();
            ServiceBase[] ServicesToRun;
            ServicesToRun = new ServiceBase[]
            {
                new ImageProcessing()
            };
            ServiceBase.Run(ServicesToRun);
        }
    }
}
