﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Net.Mail;
using Microsoft.ApplicationBlocks.Data;

namespace DigiImageProcessor
{
    class Common
    {
        static readonly String strConn = ConfigurationManager.ConnectionStrings["DigiConnectionString"].ConnectionString;
       public static DataTable GetCameraList(int subStoreId)
       {
           try
           {
               DataSet ds = new DataSet();
               SqlParameter[] spparam = { new SqlParameter("@SubStoreID", subStoreId) };
               ds = SqlHelper.ExecuteDataset(strConn, CommandType.StoredProcedure, "DG_GetCameraDetails", spparam);
               if (ds.Tables.Count > 0)
                   return ds.Tables[0];
               else
                   return null;
           }
           catch (Exception ex)
           {
               return null;
           }
       }
       public static string GetCameraSeries(int CameraId)
       {
           try
           {
               SqlParameter[] spparam = { new SqlParameter("@CameraId", CameraId) };
               object _objnumber=SqlHelper.ExecuteScalar(strConn, "DG_GetPhotoNumberCamerawise", spparam);
               return Convert.ToString(_objnumber);
           }
           catch (Exception ex)
           {
               return null;
           }
       }
       public static void SetCameraSeries(int CameraId, string SeriesNumber)
       {
           try
           {
               SqlParameter[] spparam={new SqlParameter("@CameraId",CameraId),new SqlParameter("@DG_Camera_Start_Series",SeriesNumber)};
               SqlHelper.ExecuteScalar(strConn, "DG_SetPhotoNumberCamerawise", spparam);
           }
           catch (Exception ex)
           {
           }
       }
       public static string GetConfigData(int SubStoreID)
       {
           try
           {
               DataSet ds = new DataSet();
               SqlParameter[] spparam = { new SqlParameter("@SubStoreID", SubStoreID)};
               ds = SqlHelper.ExecuteDataset(strConn, CommandType.StoredProcedure, "DG_GetConfigData", spparam);
               if (ds.Tables.Count > 0)
                   return ds.Tables[0].Rows[0][0].ToString();
               else
                   return null;
           }
           catch (Exception ex)
           {
               return null;
           }
       }
    }
}
