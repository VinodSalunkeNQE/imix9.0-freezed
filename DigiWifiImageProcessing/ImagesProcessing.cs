﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.IO;
using System.Runtime.InteropServices;
using ErrorHandler;
using System.Threading;
using System.Drawing;
using System.Windows.Media.Imaging;
using System.IO.IsolatedStorage;
using DigiPhoto.IMIX.Business;
using DigiPhoto.Utility.Repository.ValueType;
using DigiPhoto.IMIX.Model;
using DigiPhoto.DataLayer;
using System.Configuration;

namespace DigiImageProcessor
{
    public partial class ImageProcessing : ServiceBase
    {
        static string dghotfolderpath;
        ConfigBusiness configBusiness = null;
        List<iMixConfigurationLocationInfo> LstRideConfigValueLocationWise = new List<iMixConfigurationLocationInfo>();
        List<iMixConfigurationLocationInfo> lstLocationWiseConfigParams = new List<iMixConfigurationLocationInfo>();
        List<iMixConfigurationLocationInfo> GumBallConfigurationValueList = new List<iMixConfigurationLocationInfo>();
        List<long> GumBallActiveLocationList = new List<long>();
        List<long> GumRideList;
        static bool RunApplicationsSubStoreLevel;
        bool moblileRfidEnabled = false;
        static int SubStoreID;
        char valueSeparator = '-';
        string txtdirPath = string.Empty;
        //static DateTime LastMemoryClearTime = DateTime.Now;
        System.Timers.Timer DirectoryService = new System.Timers.Timer(100);
        string _prefixPhotoName, _prefixScoreFileName, _prefixActiveFlow = string.Empty;
        /// <summary>
        /// Initializes a new instance of the <see cref="ImageProcessing"/> class.
        /// </summary>
        public ImageProcessing()
        {
            InitializeComponent();
            SetGumRideMasterId();
        }
        //public void Execute()
        //{
        //    try
        //    {
        //        EventLog.WriteEntry("Service On Start Event ");
        //        //Get Substore from ss.dat file.
        //        ServicePosInfoBusiness svcPosinfoBusiness = new ServicePosInfoBusiness();
        //        string ret = svcPosinfoBusiness.ServiceStart(false);
        //        if (string.IsNullOrEmpty(ret))
        //        {
        //            try
        //            {
        //                //if (ConfigurationManager.AppSettings.AllKeys.Contains("PrefixActiveFlow"))
        //                //    _prefixActiveFlow = ConfigurationManager.AppSettings["PrefixActiveFlow"].ToString();
        //                //if (ConfigurationManager.AppSettings.AllKeys.Contains("PrefixPhotoName"))
        //                //    _prefixPhotoName = ConfigurationManager.AppSettings["PrefixPhotoName"].ToString();
        //                //if (ConfigurationManager.AppSettings.AllKeys.Contains("PrefixScoreFileName"))
        //                //    _prefixScoreFileName = ConfigurationManager.AppSettings["PrefixScoreFileName"].ToString();
        //                string pathtosave = Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location);
        //                if (File.Exists(pathtosave + "\\ss.dat"))
        //                {
        //                    string line;
        //                    using (StreamReader reader = new StreamReader(pathtosave + "\\ss.dat"))
        //                    {
        //                        line = reader.ReadLine();
        //                        string subID = DigiPhoto.CryptorEngine.Decrypt(line, true);
        //                        SubStoreID = (subID.Split(',')[0]).ToInt32();
        //                    }
        //                }
        //                else
        //                {
        //                    throw new Exception("Please select substore from Configuration Section in Imix for this machine.");
        //                }
        //            }
        //            catch (Exception ex)
        //            {
        //                ErrorHandler.ErrorHandler.LogError(ex);
        //            }
        //            dghotfolderpath = Common.GetConfigData(SubStoreID);
        //            StoreSubStoreDataBusniess storeObj = new StoreSubStoreDataBusniess();
        //            StoreInfo store = storeObj.GetStore();
        //            configBusiness = new ConfigBusiness();
        //            lstLocationWiseConfigParams = configBusiness.GetLocationWiseConfigParams(SubStoreID);
        //            GumBallConfigurationValueList = lstLocationWiseConfigParams.Where(y => GumRideList.Contains(y.IMIXConfigurationMasterId)).ToList();
        //            GumBallActiveLocationList = lstLocationWiseConfigParams.Where(n => n.IMIXConfigurationMasterId == (long)ConfigParams.IsGumRideActive
        //                && n.ConfigurationValue.ToUpper() == "TRUE").Select(x => Convert.ToInt64(x.LocationId)).ToList();
        //            if (lstLocationWiseConfigParams != null && lstLocationWiseConfigParams.Count > 0)
        //            {
        //                lstLocationWiseConfigParams = lstLocationWiseConfigParams.Where(x => x.IMIXConfigurationMasterId == (long)ConfigParams.GumRideFilePath || x.IMIXConfigurationMasterId == (long)ConfigParams.GumRideTextFileTimeOut
        //                    || x.IMIXConfigurationMasterId == (long)ConfigParams.GumRideInputPhotoPath && GumBallActiveLocationList.Contains(x.LocationId)).ToList();
        //            }
        //            RunApplicationsSubStoreLevel = store.RunApplicationsSubStoreLevel;
        //            EventLog.WriteEntry("Reading Hot Folder Path :  " + dghotfolderpath);

        //            DirectoryService.Elapsed += new System.Timers.ElapsedEventHandler(DirectoryService_Elapsed);
        //            DirectoryService.Start();
        //        }
        //        else
        //        {
        //            throw new Exception("Already Started");

        //        }
        //        // ServicePosInfoBusiness svcPosinfoBusiness = new ServicePosInfoBusiness();
        //        // svcPosinfoBusiness.ServiceStart(true);
        //    }
        //    catch (Exception ex)
        //    {
        //        EventLog.WriteEntry("Error is: " + ex.Message.ToString());
        //        ExitCode = 13816;
        //        this.Stop();
        //    }
        //}
        /// <summary>
        /// When implemented in a derived class, executes when a Start command is sent to the service by the Service Control Manager (SCM) or when the operating system starts (for a service that starts automatically). Specifies actions to take when the service starts.
        /// </summary>
        /// <param name="args">Data passed by the start command.</param>
        /// 
        protected override void OnStart(string[] args)
        {
            try
            {
                EventLog.WriteEntry("Service On Start Event ");
                //Get Substore from ss.dat file.
                ServicePosInfoBusiness svcPosinfoBusiness = new ServicePosInfoBusiness();
                string ret = svcPosinfoBusiness.ServiceStart(false);
                if (string.IsNullOrEmpty(ret))
                {
                    try
                    {
                        //if (ConfigurationManager.AppSettings.AllKeys.Contains("PrefixActiveFlow"))
                        //    _prefixActiveFlow = ConfigurationManager.AppSettings["PrefixActiveFlow"].ToString();
                        //if (ConfigurationManager.AppSettings.AllKeys.Contains("PrefixPhotoName"))
                        //    _prefixPhotoName = ConfigurationManager.AppSettings["PrefixPhotoName"].ToString();
                        //if (ConfigurationManager.AppSettings.AllKeys.Contains("PrefixScoreFileName"))
                        //    _prefixScoreFileName = ConfigurationManager.AppSettings["PrefixScoreFileName"].ToString();
                        string pathtosave = Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location);
                        if (File.Exists(pathtosave + "\\ss.dat"))
                        {
                            string line;
                            using (StreamReader reader = new StreamReader(pathtosave + "\\ss.dat"))
                            {
                                line = reader.ReadLine();
                                string subID = DigiPhoto.CryptorEngine.Decrypt(line, true);
                                SubStoreID = (subID.Split(',')[0]).ToInt32();
                            }
                        }
                        else
                        {
                            throw new Exception("Please select substore from Configuration Section in Imix for this machine.");
                        }
                    }
                    catch (Exception ex)
                    {
                        ErrorHandler.ErrorHandler.LogError(ex);
                    }
                    dghotfolderpath = Common.GetConfigData(SubStoreID);
                    StoreSubStoreDataBusniess storeObj = new StoreSubStoreDataBusniess();
                    StoreInfo store = storeObj.GetStore();
                    configBusiness = new ConfigBusiness();
                    lstLocationWiseConfigParams = configBusiness.GetLocationWiseConfigParams(SubStoreID);
                    GumBallConfigurationValueList = lstLocationWiseConfigParams.Where(y => GumRideList.Contains(y.IMIXConfigurationMasterId)).ToList();
                    GumBallActiveLocationList = lstLocationWiseConfigParams.Where(n => n.IMIXConfigurationMasterId == (long)ConfigParams.IsGumRideActive
                        && n.ConfigurationValue.ToUpper() == "TRUE").Select(x => Convert.ToInt64(x.LocationId)).ToList();

                   

                    if (lstLocationWiseConfigParams != null && lstLocationWiseConfigParams.Count > 0)
                    {
                        lstLocationWiseConfigParams = lstLocationWiseConfigParams.Where(x => x.IMIXConfigurationMasterId == (long)ConfigParams.GumRideFilePath || x.IMIXConfigurationMasterId == (long)ConfigParams.GumRideTextFileTimeOut
                            || x.IMIXConfigurationMasterId == (long)ConfigParams.GumRideInputPhotoPath && GumBallActiveLocationList.Contains(x.LocationId)
                            || x.IMIXConfigurationMasterId == (long)ConfigParams.IsMobileRfidEnabled).ToList();
                    }
                    RunApplicationsSubStoreLevel = store.RunApplicationsSubStoreLevel;
                    EventLog.WriteEntry("Reading Hot Folder Path :  " + dghotfolderpath);

                    DirectoryService.Elapsed += new System.Timers.ElapsedEventHandler(DirectoryService_Elapsed);
                    DirectoryService.Start();
                }
                else
                {
                    throw new Exception("Already Started");

                }
                // ServicePosInfoBusiness svcPosinfoBusiness = new ServicePosInfoBusiness();
                // svcPosinfoBusiness.ServiceStart(true);
            }
            catch (Exception ex)
            {
                EventLog.WriteEntry("Error is: " + ex.Message.ToString());
                ExitCode = 13816;
                this.Stop();
            }
        }


        /// <summary>
        /// Handles the Elapsed event of the DirectoryService control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Timers.ElapsedEventArgs"/> instance containing the event data.</param>
        void DirectoryService_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            try
            {
                DirectoryService.Stop();
                EventLog.WriteEntry("Timer Event Start ");
                TimerFunction();
            }
            catch (Exception ex)
            {

            }

            finally
            {
                DirectoryService.Start();
            }
        }

        /// <summary>
        /// When implemented in a derived class, executes when a Stop command is sent to the service by the Service Control Manager (SCM). Specifies actions to take when a service stops running.
        /// </summary>
        protected override void OnStop()
        {
            ServicePosInfoBusiness svcPosinfoBusiness = new ServicePosInfoBusiness();
            svcPosinfoBusiness.StopService(false);
        }

        /// <summary>
        /// Determines whether [is file locked] [the specified file].
        /// </summary>
        /// <param name="file">The file.</param>
        /// <returns></returns>
        protected bool IsFileLocked(FileInfo file)
        {
            FileStream stream = null;
            try
            {
                stream = file.Open(FileMode.Open, FileAccess.ReadWrite, FileShare.None);
            }
            catch (IOException)
            {
                return false;
            }
            finally
            {
                if (stream != null)
                    stream.Close();
            }
            return true;
        }

        /// <summary>
        /// Timers the function.
        /// </summary>
        public void TimerFunction()
        {
            try
            {
                Thread.Sleep(1000);
                var pendingPendingItemPath = Path.Combine(dghotfolderpath, "PendingItems");

                if (!Directory.Exists(pendingPendingItemPath))
                    Directory.CreateDirectory(pendingPendingItemPath);

                DirectoryInfo _objdir = new DirectoryInfo(pendingPendingItemPath);

                foreach (var items in _objdir.GetFiles("*.jpg"))
                {
                    FileInfo _objnewfile = new FileInfo(items.FullName);
                    _objnewfile.MoveTo(dghotfolderpath + "\\Download\\" + items.Name);
                }

                DataTable Cameradt = new DataTable();
                Cameradt = Common.GetCameraList(SubStoreID);
                EventLog.WriteEntry("Total Camera Found : " + Cameradt.Rows.Count.ToString());
                foreach (DataRow cameraitem in Cameradt.Rows)
                {
                    TimeSpan ts = new TimeSpan();
                    Int32 camLocation = Convert.ToInt32(cameraitem["DG_Location_pkey"].ToString());
                    string DownloadFolderPath;
                    string cameraHotFolderPath;
                    txtdirPath = string.Empty;

                   // var mobileRfidConfig = lstLocationWiseConfigParams.Where((x=>x.IMIXConfigurationMasterId==(long)ConfigParams.IsMobileRfidEnabled) &&  (LocationId == camLocation)).FirstOrDefault();
                    var mobileRfidConfig = lstLocationWiseConfigParams.Where(x => x.IMIXConfigurationMasterId == (long)ConfigParams.IsMobileRfidEnabled && x.LocationId == camLocation).FirstOrDefault();
                    if (mobileRfidConfig == null)
                    { 
                        moblileRfidEnabled=false;
                    }
                    else 
                    {
                        moblileRfidEnabled = Convert.ToBoolean(mobileRfidConfig.ConfigurationValue);
                    }
                    if (GumBallActiveLocationList != null && GumBallActiveLocationList.Count > 0)
                    {
                        iMixConfigurationLocationInfo _TextFileTimeOut = lstLocationWiseConfigParams.Where(x => x.IMIXConfigurationMasterId.Equals(Convert.ToInt32(ConfigParams.GumRideTextFileTimeOut)) && GumBallActiveLocationList.Contains(camLocation)).ToList().FirstOrDefault();
                      if (_TextFileTimeOut!=null)
                          ts = TimeSpan.FromSeconds(Convert.ToInt32(_TextFileTimeOut.ConfigurationValue));
                    }
                    cameraHotFolderPath = cameraitem["DG_Hot_Folder_Path"].ToString();
                    if (RunApplicationsSubStoreLevel)
                    {
                        DownloadFolderPath = cameraHotFolderPath + "\\Download\\" + cameraitem["DG_SubStore_Name"];
                        //Create Substore Directory
                        if (!Directory.Exists(cameraHotFolderPath + "\\Download\\" + cameraitem["DG_SubStore_Name"]))
                        {
                            Directory.CreateDirectory(cameraHotFolderPath + "\\Download\\" + cameraitem["DG_SubStore_Name"]);
                        }
                        if (!Directory.Exists(cameraHotFolderPath + "\\Download\\" + cameraitem["DG_SubStore_Name"] + "\\" + cameraitem["DG_Location_Name"]))
                        {
                            Directory.CreateDirectory(cameraHotFolderPath + "\\Download\\" + cameraitem["DG_SubStore_Name"] + "\\" + cameraitem["DG_Location_Name"]);
                        }
                    }
                    else
                    {
                        DownloadFolderPath = cameraHotFolderPath + "\\Download";
                    }

                    DirectoryInfo _objnew;
                    string CameraSeries = Convert.ToString(cameraitem["DG_Camera_Start_Series"]);
                    string dirPath = cameraHotFolderPath + "Camera\\C" + cameraitem["DG_Camera_ID"].ToString() + "\\";
                    if (lstLocationWiseConfigParams != null && lstLocationWiseConfigParams.Count > 0)
                    {
                        iMixConfigurationLocationInfo obj = lstLocationWiseConfigParams.Where(x => x.LocationId == camLocation && x.IMIXConfigurationMasterId.Equals(Convert.ToInt32(ConfigParams.GumRideFilePath))).ToList().FirstOrDefault();
                        if (obj != null && !String.IsNullOrEmpty(obj.ConfigurationValue))
                            txtdirPath = obj.ConfigurationValue;

                        iMixConfigurationLocationInfo objGB = lstLocationWiseConfigParams.Where(x => x.LocationId == camLocation && x.IMIXConfigurationMasterId.Equals(Convert.ToInt32(ConfigParams.GumRideInputPhotoPath))).ToList().FirstOrDefault();
                        if (objGB != null && !String.IsNullOrEmpty(objGB.ConfigurationValue))
                        {
                            if (GumBallActiveLocationList != null && GumBallActiveLocationList.Count > 0)
                            {
                                dirPath = objGB.ConfigurationValue;
                            }
                        }
                    }

                    _objnew = new DirectoryInfo(dirPath);
                    FileInfo[] lstFileInfo;
                    string[] extensions = new[]
                        {
                            ".jpg", ".avi", ".mp4", ".wmv", ".mov", ".3gp", ".3g2", ".m2v", ".m4v", ".flv", ".mpeg",
                            ".ffmpeg"
                        };
                    if (_objnew.Exists)
                    {
                        lstFileInfo = _objnew.EnumerateFiles()
                            .Where(f => extensions.Contains(f.Extension.ToLower()) && !f.Name.Contains("@") && !f.Name.Contains("_PR"))
                            .ToArray();
                        foreach (var item in lstFileInfo.ToList().OrderBy(t => t.CreationTime))
                        {
                            bool IsImageLocked = false;
                            if (IsFileLocked(item))
                            {
                                if (item.Extension.ToLower().CompareTo(".jpg") == 0)
                                {

                                    if (Convert.ToBoolean(cameraitem["IsTripCam"]) == true)
                                    {
                                        if (!string.IsNullOrEmpty(txtdirPath))
                                        {
                                            string txtFileName = item.Name;
                                            //item.CopyTo(cameraHotFolderPath + "\\Camera\\RideC" + Convert.ToInt32(cameraitem["DG_Camera_pkey"]) + "\\" + CameraSeries + ".jpg");
                                            //item.CopyTo(cameraHotFolderPath + "\\Camera\\RideCamera\\" + CameraSeries + ".jpg");

                                            //FileInfo _objnewfile = new FileInfo(item.FullName);
                                            //_objnewfile.MoveTo(DownloadFolderPath + "\\" + CameraSeries + "@" +
                                            //                   cameraitem["DG_AssignTo"] + ".jpg");
                                            GetgumballNameSetting(camLocation);
                                            string _GumBallFileName = txtFileName.Split('.')[0].ToString();
                                            if (_prefixActiveFlow.ToLower() == "true")
                                            {
                                                _GumBallFileName = _GumBallFileName.Replace(_prefixPhotoName, _prefixScoreFileName);
                                            }
                                            string txtSourceFile = System.IO.Path.Combine(txtdirPath, _GumBallFileName + ".txt");
                                            string txtFileAtLocation = string.Empty;
                                            if (File.Exists(txtSourceFile))
                                            {
                                                try
                                                {
                                                    txtFileAtLocation = Path.Combine(txtSourceFile, DownloadFolderPath + "\\" + cameraitem["DG_Location_Name"]
                                                            + "\\" + CameraSeries + "@" + cameraitem["DG_AssignTo"] + ".txt");
                                                    if (!File.Exists(txtFileAtLocation))
                                                        File.Move(txtSourceFile, DownloadFolderPath + "\\" + cameraitem["DG_Location_Name"]
                                                                + "\\" + CameraSeries + "@" + cameraitem["DG_AssignTo"] + ".txt");
                                                    else
                                                    {
                                                        File.Delete(txtFileAtLocation);
                                                        File.Move(txtSourceFile, DownloadFolderPath + "\\" + cameraitem["DG_Location_Name"]
                                                               + "\\" + CameraSeries + "@" + cameraitem["DG_AssignTo"] + ".txt");
                                                    }
                                                    item.CopyTo(cameraHotFolderPath + "\\Camera\\RideC" + Convert.ToInt32(cameraitem["DG_Camera_pkey"]) + "\\" + CameraSeries + ".jpg");
                                                    item.CopyTo(cameraHotFolderPath + "\\Camera\\RideCamera\\" + CameraSeries + ".jpg");

                                                    FileInfo _objnewfile = new FileInfo(item.FullName);
                                                    _objnewfile.MoveTo(DownloadFolderPath + "\\" + CameraSeries + "@" +
                                                                       cameraitem["DG_AssignTo"] + ".jpg");
                                                }
                                                catch (Exception ex)
                                                {
                                                    string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                                                    EventLog.WriteEntry(errorMessage + txtFileAtLocation);
                                                }
                                            }
                                            else
                                            {
                                                var lstNoOfNextFile = lstFileInfo.ToList().Where(t => t.CreationTime > item.CreationTime);
                                                if ((lstNoOfNextFile != null && lstNoOfNextFile.Count() > 0) || DateTime.Now > item.CreationTime.Add(ts))
                                                {
                                                    item.CopyTo(cameraHotFolderPath + "\\Camera\\RideC" + Convert.ToInt32(cameraitem["DG_Camera_pkey"]) + "\\" + CameraSeries + ".jpg");
                                                    item.CopyTo(cameraHotFolderPath + "\\Camera\\RideCamera\\" + CameraSeries + ".jpg");

                                                    FileInfo _objnewfile = new FileInfo(item.FullName);
                                                    _objnewfile.MoveTo(DownloadFolderPath + "\\" + CameraSeries + "@" +
                                                                       cameraitem["DG_AssignTo"] + ".jpg");
                                                }
                                                else
                                                {
                                                    continue;
                                                }
                                            }
                                        }
                                        else
                                        {
                                            item.CopyTo(cameraHotFolderPath + "\\Camera\\RideC" + Convert.ToInt32(cameraitem["DG_Camera_pkey"]) + "\\" + CameraSeries + ".jpg");
                                            item.CopyTo(cameraHotFolderPath + "\\Camera\\RideCamera\\" + CameraSeries + ".jpg");

                                            FileInfo _objnewfile = new FileInfo(item.FullName);
                                            _objnewfile.MoveTo(DownloadFolderPath + "\\" + CameraSeries + "@" +
                                                               cameraitem["DG_AssignTo"] + ".jpg");
                                        }
                                    }
                                    else
                                    {
                                        if (!string.IsNullOrEmpty(txtdirPath))
                                        {
                                            string txtFileName = item.Name;
                                            GetgumballNameSetting(camLocation);
                                            string _GumBallFileName = txtFileName.Split('.')[0].ToString();
                                            if (_prefixActiveFlow.ToLower() == "true")
                                            {
                                                _GumBallFileName = _GumBallFileName.Replace(_prefixPhotoName, _prefixScoreFileName);
                                            }
                                            string txtSourceFile = System.IO.Path.Combine(txtdirPath, _GumBallFileName + ".txt");
                                            //string txtSourceFile = System.IO.Path.Combine(txtdirPath, txtFileName.Split('.')[0].ToString() + ".txt");
                                            string txtFileAtLocation = string.Empty;
                                            if (File.Exists(txtSourceFile))
                                            {
                                                try
                                                {
                                                    txtFileAtLocation = Path.Combine(txtSourceFile, DownloadFolderPath + "\\" + cameraitem["DG_Location_Name"]
                                                            + "\\" + CameraSeries + "@" + cameraitem["DG_AssignTo"] + getTagId(txtFileName.Split('.')[0].ToString() + ".txt") + ".txt");
                                                    if (!File.Exists(txtFileAtLocation))
                                                        File.Move(txtSourceFile, DownloadFolderPath + "\\" + cameraitem["DG_Location_Name"]
                                                                + "\\" + CameraSeries + "@" + cameraitem["DG_AssignTo"] + getTagId(txtFileName.Split('.')[0].ToString() + ".txt") + ".txt");
                                                    else
                                                    {
                                                        File.Delete(txtFileAtLocation);
                                                        File.Move(txtSourceFile, DownloadFolderPath + "\\" + cameraitem["DG_Location_Name"]
                                                               + "\\" + CameraSeries + "@" + cameraitem["DG_AssignTo"] + getTagId(txtFileName.Split('.')[0].ToString() + ".txt") + ".txt");
                                                    }

                                                    item.MoveTo(DownloadFolderPath + "\\" + CameraSeries + "@" +
                                                                                                    cameraitem["DG_AssignTo"] + getTagId(item.Name) + ".jpg");
                                                }
                                                catch (Exception ex)
                                                {
                                                    string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                                                    EventLog.WriteEntry(errorMessage + txtFileAtLocation);
                                                }
                                            }
                                            else
                                            {
                                                var lstNoOfNextFile = lstFileInfo.ToList().Where(t => t.CreationTime > item.CreationTime);
                                                if ((lstNoOfNextFile != null && lstNoOfNextFile.Count() > 0) || DateTime.Now > item.CreationTime.Add(ts))
                                                {
                                                    item.MoveTo(DownloadFolderPath + "\\" + CameraSeries + "@" + cameraitem["DG_AssignTo"] + getTagId(item.Name) + ".jpg");
                                                }
                                                else
                                                {
                                                    continue;
                                                }
                                            }
                                        }
                                        else
                                        {
                                            item.MoveTo(DownloadFolderPath + "\\" + CameraSeries + "@" + cameraitem["DG_AssignTo"] + getTagId(item.Name) + ".jpg");
                                        }
                                    }
                                    //}
                                }
                                else
                                {
                                    string origExt = item.Extension;
                                    if (Convert.ToBoolean(cameraitem["IsLiveStream"]) == true)
                                    {
                                        string origName = item.Name.Replace("_GS", "_PR");
                                        if (item.Name.Contains("_GS") && File.Exists(Path.Combine(dirPath, origName)))
                                        {
                                            string GsName = item.Name;
                                            File.Move(Path.Combine(dirPath, origName), Path.Combine(DownloadFolderPath, CameraSeries + "@" + cameraitem["DG_AssignTo"] + "_PR" + getTagId(item.Name) + origExt));
                                            item.MoveTo(DownloadFolderPath + "\\" + CameraSeries + "@" + cameraitem["DG_AssignTo"] + "_GS" + getTagId(item.Name) + origExt);
                                        }
                                        else
                                        {
                                            var corruptVideoPath = Path.Combine(dghotfolderpath, "Download", "CorruptVideos");

                                            if (!Directory.Exists(corruptVideoPath))
                                                Directory.CreateDirectory(corruptVideoPath);

                                            if (File.Exists(corruptVideoPath + "\\" + item.Name))
                                            {
                                                File.Delete(corruptVideoPath + "\\" + item.Name);
                                                item.MoveTo(corruptVideoPath + "\\" + item.Name);
                                            }
                                            else
                                            {
                                                item.MoveTo(corruptVideoPath + "\\" + item.Name);
                                            }

                                        }
                                    }
                                    else
                                    {
                                        if (!string.IsNullOrEmpty(txtdirPath))
                                        {
                                            string txtFileName = item.Name;
                                            GetgumballNameSetting(camLocation);
                                            string _GumBallFileName = txtFileName.Split('.')[0].ToString();
                                            if (_prefixActiveFlow.ToLower() == "true")
                                            {
                                                _GumBallFileName = _GumBallFileName.Replace(_prefixPhotoName, _prefixScoreFileName);
                                            }
                                            string txtSourceFile = System.IO.Path.Combine(txtdirPath, _GumBallFileName + ".txt");
                                            //string txtSourceFile = System.IO.Path.Combine(dirPath, txtFileName.Split('.')[0].ToString() + ".txt");
                                            string txtFileAtLocation = string.Empty;
                                            if (File.Exists(txtSourceFile))
                                            {
                                                txtFileAtLocation = Path.Combine(txtSourceFile, DownloadFolderPath + "\\" + cameraitem["DG_Location_Name"]
                                                            + "\\" + CameraSeries + "@" + cameraitem["DG_AssignTo"] + getTagId(txtFileName.Split('.')[0].ToString() + ".txt") + ".txt");
                                                try
                                                {
                                                    if (!File.Exists(txtFileAtLocation))
                                                        File.Move(txtSourceFile, DownloadFolderPath + "\\" + cameraitem["DG_Location_Name"]
                                                                + "\\" + CameraSeries + "@" + cameraitem["DG_AssignTo"] + getTagId(txtFileName.Split('.')[0].ToString() + ".txt") + ".txt");
                                                    else
                                                    {
                                                        File.Delete(Path.Combine(txtSourceFile, DownloadFolderPath + "\\" + cameraitem["DG_Location_Name"]
                                                            + "\\" + CameraSeries + "@" + cameraitem["DG_AssignTo"] + getTagId(txtFileName.Split('.')[0].ToString() + ".txt") + ".txt"));
                                                        File.Move(txtSourceFile, DownloadFolderPath + "\\" + cameraitem["DG_Location_Name"]
                                                                + "\\" + CameraSeries + "@" + cameraitem["DG_AssignTo"] + getTagId(txtFileName.Split('.')[0].ToString() + ".txt") + ".txt");
                                                    }
                                                    item.MoveTo(DownloadFolderPath + "\\" + CameraSeries + "@" + cameraitem["DG_AssignTo"] + getTagId(item.Name) + origExt);
                                                }
                                                catch (Exception ex)
                                                {
                                                    string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                                                    EventLog.WriteEntry(errorMessage + txtFileAtLocation);
                                                }
                                            }
                                            else
                                            {
                                                var lstNoOfNextFile = lstFileInfo.ToList().Where(t => t.CreationTime > item.CreationTime);
                                                if (lstNoOfNextFile != null && lstNoOfNextFile.Count() > 0 || DateTime.Now > item.CreationTime.Add(ts))
                                                {
                                                    item.MoveTo(DownloadFolderPath + "\\" + CameraSeries + "@" + cameraitem["DG_AssignTo"] + getTagId(item.Name) + origExt);
                                                }
                                                else
                                                {
                                                    continue;
                                                }
                                            }
                                        }
                                        else
                                        {
                                            item.MoveTo(DownloadFolderPath + "\\" + CameraSeries + "@" + cameraitem["DG_AssignTo"] + getTagId(item.Name) + origExt);
                                        }

                                    }
                                }
                                Common.SetCameraSeries(Convert.ToInt32(cameraitem["DG_Camera_pkey"]),
                                        (Convert.ToInt64(CameraSeries) + 1).ToString());
                                CameraSeries = Common.GetCameraSeries(Convert.ToInt32(cameraitem["DG_Camera_pkey"]));
                            }
                            else
                            {
                                EventLog.WriteEntry(item.FullName + " is locked and we are retrying to get it..");
                                IsImageLocked = true;
                            }
                            if (IsImageLocked)
                                break;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                EventLog.WriteEntry(errorMessage);
            }
            finally
            {
                //if (DateTime.Now.Subtract(LastMemoryClearTime).Minutes >= 1)
                //{
                MemoryManagement.FlushMemory();
                //LastMemoryClearTime = DateTime.Now;
                //}
            }
        }
        private void SetGumRideMasterId()
        {
            GumRideList = new List<long>();
            //GumRideList.Add(Convert.ToInt64(ConfigParams.GumRideAvailableLocations));
            //GumRideList.Add(Convert.ToInt64(ConfigParams.GumRideFontSize));
            //GumRideList.Add(Convert.ToInt64(ConfigParams.GumRideFontColor));
            //GumRideList.Add(Convert.ToInt64(ConfigParams.GumRideFontWeight));
            //GumRideList.Add(Convert.ToInt64(ConfigParams.GumRideBackgrondColor));
            //GumRideList.Add(Convert.ToInt64(ConfigParams.GumRidePosition));
            //GumRideList.Add(Convert.ToInt64(ConfigParams.GumRideMargin));
            //GumRideList.Add(Convert.ToInt64(ConfigParams.GumRideFilePath));
            //GumRideList.Add(Convert.ToInt64(ConfigParams.IsGumRideActive));
            //GumRideList.Add(Convert.ToInt64(ConfigParams.IsGumPlayerScoreVisible));
            //GumRideList.Add(Convert.ToInt64(ConfigParams.GumRideFontStyle));
            //GumRideList.Add(Convert.ToInt64(ConfigParams.GumRideFontFamily));
            //GumRideList.Add(Convert.ToInt64(ConfigParams.IsVisibleGumBallZeroScore));
            //GumRideList.Add(Convert.ToInt64(ConfigParams.IsVisibleGumBallZeroScoreOnImage));
            //GumRideList.Add(Convert.ToInt64(ConfigParams.GumRideInputPhotoPath));
            GumRideList.Add(Convert.ToInt64(ConfigParams.GumballScoreSeperater));
            GumRideList.Add(Convert.ToInt64(ConfigParams.IsPrefixActiveFlow));
            GumRideList.Add(Convert.ToInt64(ConfigParams.PrefixPhotoName));
            GumRideList.Add(Convert.ToInt64(ConfigParams.PrefixScoreFileName));
        }
        private void GetgumballNameSetting(int camLocation)
        {
            if (GumBallActiveLocationList != null && GumBallActiveLocationList.Count > 0)
                LstRideConfigValueLocationWise = GumBallConfigurationValueList.Where(x => x.LocationId.Equals(camLocation) && GumBallActiveLocationList.Contains(x.LocationId)).ToList();
            if (LstRideConfigValueLocationWise != null && LstRideConfigValueLocationWise.Count > 0)
            {
                _prefixPhotoName = LstRideConfigValueLocationWise.Where(x => x.IMIXConfigurationMasterId.Equals(Convert.ToInt32(ConfigParams.PrefixPhotoName))).Select(x => x.ConfigurationValue).FirstOrDefault();
                _prefixScoreFileName = LstRideConfigValueLocationWise.Where(x => x.IMIXConfigurationMasterId.Equals(Convert.ToInt32(ConfigParams.PrefixScoreFileName))).Select(x => x.ConfigurationValue).FirstOrDefault();
                _prefixActiveFlow = LstRideConfigValueLocationWise.Where(x => x.IMIXConfigurationMasterId.Equals(Convert.ToInt32(ConfigParams.IsPrefixActiveFlow))).Select(x => x.ConfigurationValue).FirstOrDefault();
            }
        }

        string getTagId(string imageName)
        {
            bool MobileAssociationEnabled = moblileRfidEnabled;
            string[] extensions = new[]
                        {
                            ".avi", ".mp4", ".wmv", ".mov", ".3gp", ".3g2", ".m2v", ".m4v", ".flv", ".mpeg",
                            ".ffmpeg"
                        };

            try
            {
                if (imageName.ToLower().Split('.')[1] == "jpg")
                {
                    string tagId = string.Empty;
                    //img name contains tags id in first part separeted by -
                    tagId = imageName.Split(valueSeparator).FirstOrDefault();
                    //In some cases the img name starts with -
                    if (string.IsNullOrEmpty(tagId) && imageName.Split(valueSeparator).Length > 1)
                        tagId = imageName.Split(valueSeparator)[1];
                    if (!string.IsNullOrEmpty(tagId) && tagId.Length > 1 && imageName.Split(valueSeparator).Length > 1)
                        tagId = "@" + tagId;
                    else
                        tagId = string.Empty;

                    ErrorHandler.ErrorHandler.LogFileWrite("Line 676: GetDatgID() tagId:" + tagId);
                    return tagId;
                }
                else if (extensions.Any(imageName.ToLower().Contains) && !imageName.Contains("_GS") && !imageName.Contains("_PR") && MobileAssociationEnabled == true)
                {
                    try
                    {
                        string tagId = string.Empty;
                        //img name contains tags id in first part separeted by -
                        tagId = imageName.Split(valueSeparator).FirstOrDefault();
                        //In some cases the img name starts with -
                        if (string.IsNullOrEmpty(tagId) && imageName.Split(valueSeparator).Length > 1)
                            tagId = imageName.Split(valueSeparator)[1];
                        if (!string.IsNullOrEmpty(tagId) && tagId.Length > 1 && imageName.Split(valueSeparator).Length > 1)
                            tagId = "@" + tagId;
                        else
                            tagId = string.Empty;


                        ErrorHandler.ErrorHandler.LogFileWrite("Line 693: GetDatgID() tagId:" + tagId);
                        return tagId;
                    }
                    catch (Exception ex)
                    {
                        ErrorHandler.ErrorHandler.LogFileWrite("Line 695: exception in GetDatgID()");
                        ErrorHandler.ErrorHandler.LogError(ex);
                        return "@" + imageName.Split('.')[0];
                    }
                }
                else return "";
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogFileWrite("Line 713: exception in GetDatgID()");
                ErrorHandler.ErrorHandler.LogError(ex);
                return "";
            }
        }
    }

    public class MemoryManagement
    {
        [DllImportAttribute("kernel32.dll", EntryPoint = "SetProcessWorkingSetSize", ExactSpelling = true, CharSet =
        CharSet.Ansi, SetLastError = true)]

        private static extern int SetProcessWorkingSetSize(IntPtr process, int minimumWorkingSetSize, int
        maximumWorkingSetSize);

        /// <summary>
        /// Flushes the memory.
        /// </summary>
        public static void FlushMemory()
        {
            GC.Collect();
            GC.WaitForPendingFinalizers();
            if (Environment.OSVersion.Platform == PlatformID.Win32NT)
            {
                SetProcessWorkingSetSize(System.Diagnostics.Process.GetCurrentProcess().Handle, -1, -1);
            }
            GC.Collect();
        }

    }
}
