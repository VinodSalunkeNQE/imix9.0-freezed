﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using DigiPhoto.DataLayer.Model;
//using System.Data.EntityClient; Commented for 7.0 (EntityFramwork 6.0)
using DigiPhoto.DataLayer;
using DigiPhoto.Common;
using DigiPhoto.Utility.Repository.ValueType;
using DigiPhoto.IMIX.Business;
using DigiPhoto.IMIX.Model;
using FrameworkHelper;
using DigiPhoto.Utility.Repository.Data;

namespace DigiPhoto.Manage
{
    public partial class ManageProductPricing : Window
    {
        #region Declaration & Properties
        /// <summary>
        /// The _obj data layer
        /// </summary>
        //DigiPhotoDataServices _objDataLayer = new DigiPhotoDataServices();
        /// <summary>
        /// The _obj pricing list
        /// </summary>
        List<ProductListWithPricing> _objPricingList = new List<ProductListWithPricing>();

        #endregion

        #region Constructor
        /// <summary>
        /// Initializes a new instance of the <see cref="ManageProductPricing"/> class.
        /// </summary>
        public ManageProductPricing()
        {
            InitializeComponent();
            GetStoreDropDown();
        }
        #endregion

        #region Events
        /// <summary>
        /// Handles the SelectionChanged event of the cmbStoreName control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="SelectionChangedEventArgs"/> instance containing the event data.</param>
        private void cmbStoreName_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                if (cmbStoreName.SelectedValue.ToString() != "0")
                    GetPricingDataForStore(cmbStoreName.SelectedValue.ToInt32());
                else
                    dgPricing.ItemsSource = null;
            }
            catch (Exception ex)
            {

            }
        }
        /// <summary>
        /// Handles the Click event of the btnSubmit control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void btnSubmit_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                foreach (var item in _objPricingList)
                {
                    //_objDataLayer.SetProductPricingData(item.ProductTypeId, item.Price, item.SelectedCurrency.ToInt32(), LoginUser.UserId,cmbStoreName.SelectedValue.ToInt32(), item.IsAvailable);
                    (new ProductBusiness()).SetProductPricingData(item.ProductTypeId, item.Price, item.SelectedCurrency.ToInt32(), LoginUser.UserId, cmbStoreName.SelectedValue.ToInt32(), item.IsAvailable);
                }
            }
            catch (Exception ex)
            {

            }
        }

        #endregion

        #region Common Methods
        /// <summary>
        /// Gets the store drop down.
        /// </summary>
        void GetStoreDropDown()
        {
            //List<DG_Store> lstStore = new List<DG_Store>();
            //DG_Store _objfirst = new DG_Store();
            //_objfirst.DG_Store_Name = "--Select--";
            //_objfirst.DG_Store_pkey = 0;
            //lstStore.Add(_objfirst);
            //var lststoresname = _objDataLayer.GetStoreName();
            //foreach (var item in lststoresname)
            //{
            //    lstStore.Add(item);
            //}
            //cmbStoreName.ItemsSource = lstStore;
            //cmbStoreName.SelectedValue = LoginUser.StoreId.ToString();
            //cmbStoreName.IsEnabled = false;

            List<StoreInfo> lstStore = (new StoreSubStoreDataBusniess()).GetStoreName();
            CommonUtility.BindComboWithSelect<StoreInfo>(cmbStoreName, lstStore, "DG_Store_Name", "DG_Store_pkey", 0, ClientConstant.SelectString);
            cmbStoreName.SelectedValue = LoginUser.StoreId.ToString();
            cmbStoreName.IsEnabled = false;
        }
        /// <summary>
        /// Gets the pricing data for store.
        /// </summary>
        /// <param name="storeId">The store unique identifier.</param>
        void GetPricingDataForStore(int storeId)
        {
            _objPricingList = new List<ProductListWithPricing>();
            //var itemList = _objDataLayer.GetProductPricingStoreWise(storeId);
            var itemList = (new ProductBusiness()).GetProductPricingStoreWise(LoginUser.StoreId);
            if (itemList.Count == 0)
            {
                //var ProductList = _objDataLayer.GetProductType();
                var ProductList = (new ProductBusiness()).GetPackageNames(false);
                foreach (var item in ProductList)
                {
                    ProductListWithPricing _objPLP = new ProductListWithPricing();
                    _objPLP.IsAvailable = false;
                    //_objPLP.LstCurrency = _objDataLayer.GetCurrencyList();
                    _objPLP.LstCurrency = (new CurrencyBusiness()).GetCurrencyList();
                    _objPLP.Price = 0;
                    _objPLP.ProductType = item.DG_Orders_ProductType_Name;
                    _objPLP.ProductTypeId = item.DG_Orders_ProductType_pkey;
                    // _objPLP.SelectedCurrency = _objDataLayer.GetDefaultCurrency().ToString();
                    _objPLP.SelectedCurrency = (new CurrencyBusiness()).GetDefaultCurrency().ToString();
                    _objPricingList.Add(_objPLP);
                }
            }
            else
            {
                //var ProductList = _objDataLayer.GetProductType();
                var ProductList = (new ProductBusiness()).GetPackageNames(false);
                List<int?> _objPLst = itemList.Select(t => t.DG_Product_Pricing_ProductType).ToList();
                foreach (var item in ProductList)
                {
                    ProductListWithPricing _objPLP = new ProductListWithPricing();
                    if (_objPLst.Where(t => t.Value.ToString().Contains(item.DG_Orders_ProductType_pkey.ToString())).ToList().Count > 0)
                    {
                        _objPLP.IsAvailable = itemList.Where(t => t.DG_Product_Pricing_ProductType == item.DG_Orders_ProductType_pkey).FirstOrDefault().DG_Product_Pricing_IsAvaliable;
                        //_objPLP.LstCurrency = _objDataLayer.GetCurrencyList();
                        _objPLP.LstCurrency = (new CurrencyBusiness()).GetCurrencyList();
                        _objPLP.Price = itemList.Where(t => t.DG_Product_Pricing_ProductType == item.DG_Orders_ProductType_pkey).FirstOrDefault().DG_Product_Pricing_ProductPrice;
                        _objPLP.ProductType = item.DG_Orders_ProductType_Name;
                        _objPLP.ProductTypeId = item.DG_Orders_ProductType_pkey;
                        //_objPLP.SelectedCurrency = _objDataLayer.GetDefaultCurrency().ToString();
                        _objPLP.SelectedCurrency = (new CurrencyBusiness()).GetDefaultCurrency().ToString();
                    }
                    else
                    {
                        _objPLP.IsAvailable = false;
                        //_objPLP.LstCurrency = _objDataLayer.GetCurrencyList();
                        _objPLP.LstCurrency = (new CurrencyBusiness()).GetCurrencyList();
                        _objPLP.Price = 0;
                        _objPLP.ProductType = item.DG_Orders_ProductType_Name;
                        _objPLP.ProductTypeId = item.DG_Orders_ProductType_pkey;
                        //_objPLP.SelectedCurrency = _objDataLayer.GetDefaultCurrency().ToString();
                        _objPLP.SelectedCurrency = (new CurrencyBusiness()).GetDefaultCurrency().ToString();
                    }
                    _objPricingList.Add(_objPLP);
                }
            }
            dgPricing.ItemsSource = _objPricingList;
        }
        #endregion

       
    }
    #region Custom Class
    class ProductListWithPricing
    {
        public bool? IsAvailable { get; set; }
        public List<CurrencyInfo> LstCurrency { get; set; }
        public double? Price { get; set; }
        public string SelectedCurrency { get; set; }
        public string ProductType { get; set; }
        public int? ProductTypeId { get; set; }
    }
    #endregion
}
