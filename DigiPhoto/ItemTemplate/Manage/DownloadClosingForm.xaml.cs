﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using DigiPhoto.DataLayer.Model;
using DigiPhoto.DataLayer;
using DigiPhoto.Common;
using DigiPhoto.Manage;
using DigiAuditLogger;
using Microsoft.Win32;
using System.IO;
using Excel;
using System.Data;
using System.Collections;
using DigiPhoto.IMIX.Business;
using DigiPhoto.IMIX.Model;
using DigiPhoto.Utility.Repository.ValueType;
using System.Windows.Controls.Primitives;


namespace DigiPhoto
{
    /// <summary>
    /// Interaction logic for ManageProduct.xaml
    /// </summary>
    public partial class DownloadClosingForm : Window
    {
        #region Declaration (CardType)
        int cardId = 0;
        /// <summary>
        /// The _parent
        /// </summary>
        private UIElement _parent;
        List<SageClosingFormDownloadInfo> lstClosingForm = new List<SageClosingFormDownloadInfo>();
        #endregion

        /// <summary>
        /// Sets the parent.
        /// </summary>
        /// <param name="parent">The parent.</param>
        public void SetParent(UIElement parent)
        {
            _parent = (UIElement)parent;

        }
        #region Constructor
        /// <summary>
        /// Initializes a new instance of the <see cref="ManageProduct"/> class.
        /// </summary>
        public DownloadClosingForm()
        {
            try
            {
                InitializeComponent();
                FillSubStoreCombo();
                dtFrom.Value = DateTime.Now.Date.Add(new TimeSpan(6, 0, 0));
                dtTo.Value = DateTime.Now.Date.Add(new TimeSpan(23, 0, 0));
                //loadClosingFormGrid(dtFrom.Value, dtTo.Value, ((KeyValuePair<string, string>)(((Selector)(cmbsubstore)).SelectedItem)).Key.ToInt32());
                //GetCardTypeList();
                txbUserName.Text = LoginUser.UserName;
                txbStoreName.Text = LoginUser.StoreName;
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }
        #endregion


        #region Events (Card Types)

        /// <summary>
        /// Handles the Click event of the btnLogout control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void btnLogout_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                AuditLog.AddUserLog(Common.LoginUser.UserId, 39, "Logged out at ");
                Login _objLogin = new Login();
                _objLogin.Show();
                this.Close();
            }
            catch (Exception ex)
            {

                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
            finally
            {
                MemoryManagement.FlushMemory();
            }
        }

        /// <summary>
        /// Gets the product typeby product unique identifier.
        /// </summary>
        /// <returns></returns>

        private void btnBack_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                ManageHome _objHome = new ManageHome();
                _objHome.Show();
                this.Close();
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }
        #endregion

        #region Common Method (Manage Product)
        /// <summary>
        /// Isvalidates this instance.
        /// </summary>
        /// <returns></returns>


        public void loadClosingFormGrid(DateTime? dtFrom, DateTime? dtTo, int substoreId)
        {
            try
            {
                SageBusiness sage = new SageBusiness();
                lstClosingForm = sage.GetClosingFormDownloadInfo(dtFrom, dtTo, substoreId);
                if (lstClosingForm == null || lstClosingForm.Count == 0)
                {
                    grdClosingForm.ItemsSource = null;
                    MessageBox.Show("No record found.");
                    return;
                }
                grdClosingForm.ItemsSource = lstClosingForm;
            }
            catch (Exception ex)
            {

                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }

        #endregion
        #region Events(Manage Package)
        /// <summary>
        /// Handles the Click event of the btnBackToHome control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void btnBackToHome_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                ManageHome _objHome = new ManageHome();
                _objHome.Show();
                this.Close();
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }

        }
        #endregion
        #region Custom Class
        public class CardTypeList
        {
            public int IMIXImageCardTypeId { get; set; }
            public string Name { get; set; }
            public string CardIdentificationDigit { get; set; }
            public string ImageIdentificationType { get; set; }
            public string Status { get; set; }
            public int MaxImages { get; set; }
            public string Description { get; set; }
            public DateTime CreatedOn { get; set; }
        }
        #endregion
        Microsoft.Win32.SaveFileDialog dlg = new Microsoft.Win32.SaveFileDialog();
        public string filename { get; set; }
        private void btnDownload_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Button btn = (Button)sender;
                int closingFormId = Convert.ToInt32(btn.Tag);
                SageBusiness sageBuzz = new SageBusiness();

                SageOpenClose objSage = sageBuzz.GetClosingFormDownloadInfo(closingFormId);
                string dataXML = string.Empty;
                dataXML = serialize.SerializeObject<SageOpenClose>(objSage);
                string encryptedData = CryptorEngine.Encrypt(dataXML, true);


                dlg.FileName = filename;
                dlg.DefaultExt = ".xml";
                dlg.Filter = "Xml documents (.xml)|*.xml";

                if (IsEnabledExportToAnyDrive() == false)
                {
                    dlg.FileOk += dlg_FileOk;
                    DriveInfo drives = DriveInfo.GetDrives().Where(drive => drive.DriveType == DriveType.Removable && drive.IsReady == true).FirstOrDefault();
                    if (drives != null)
                    {
                        dlg.InitialDirectory = drives.Name;
                    }
                    else
                    {
                        MessageBox.Show("No removable device found.");
                        return;
                    }
                }

                Nullable<bool> result = dlg.ShowDialog();
                if (result == true)
                {
                    string filename2 = dlg.FileName;
                    File.WriteAllText(filename2, encryptedData);
                }

            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
            }
            finally
            {
                dlg.FileOk -= dlg_FileOk;
            }

        }
        private void dlg_FileOk(object sender, System.ComponentModel.CancelEventArgs e)
        {
            try
            {
                FileInfo f = new FileInfo(dlg.FileName);
                string s = f.Directory.Root.ToString();
                if (s.Length > 0 && s.Substring(0, 1) == @"\")
                {
                    MessageBox.Show("You can save to removable device only.");
                    e.Cancel = true;
                }
                else
                {
                    DriveInfo df = new DriveInfo(s);
                    if (df.DriveType != DriveType.Removable)
                    {
                        MessageBox.Show("You can save to removable device only.");
                        e.Cancel = true;
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
            }
        }

        bool IsEnabledExportToAnyDrive()
        {
            bool returnValue = false;
            try
            {
                var item = new ConfigBusiness().GetConfigurationData(LoginUser.SubStoreId);
                if (item != null)
                    returnValue = Convert.ToBoolean(item.IsExportReportToAnyDrive);
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
            }
            return returnValue;
        }
        private void FillSubStoreCombo()
        {
            try
            {
                Dictionary<string, string> SelectDict = new Dictionary<string, string>();
                SelectDict.Add("--Select--", "--Select--");
                StoreSubStoreDataBusniess stoBiz = new StoreSubStoreDataBusniess();
                cmbsubstore.ItemsSource = stoBiz.GetSubstoreDataDir(SelectDict);
                cmbsubstore.SelectedValue = "--Select--";
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }
        private void btnSearch_Click(object sender, RoutedEventArgs e)
        {
            loadClosingFormGrid(dtFrom.Value, dtTo.Value, ((KeyValuePair<string, string>)(((Selector)(cmbsubstore)).SelectedItem)).Key.ToInt32());
        }
    }
}
