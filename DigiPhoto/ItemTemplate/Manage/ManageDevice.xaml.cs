﻿using DigiPhoto.DataLayer;
using DigiPhoto.DataLayer.Model;
using DigiPhoto.IMIX.Business;
using DigiPhoto.IMIX.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using DigiPhoto.Utility.Repository.ValueType;
using FrameworkHelper;

namespace DigiPhoto.Manage
{
    /// <summary>
    /// Interaction logic for ManageDevice.xaml
    /// </summary>
    public partial class ManageDevice : Window
    {
        #region Declaration
        Dictionary<int, string> _deviceTypes = new Dictionary<int, string>();
        DigiPhoto.IMIX.Business.DeviceManager _objDeviceBusiness = null;
        List<DeviceInfo> _deviceList = null;
        #endregion
        public ManageDevice()
        {
            InitializeComponent();
            BindDeviceTypeCombo();
            btnSaveDevice.Tag = 0;
            GetDeviceData();
        }

        private void btnSaveDevice_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                _objDeviceBusiness = new DeviceManager();
                DeviceInfo device = new DeviceInfo();
                bool isActive = (bool)(chkIsActive.IsChecked == null ? false : chkIsActive.IsChecked);
                int DeviceId = (int)(btnSaveDevice.Tag == null ? 0 : btnSaveDevice.Tag);
                if (!IsValidEntry(DeviceId))
                    return;
                device.DeviceId = DeviceId;
                device.Name = txtDeviceName.Text.Trim();
                device.DeviceTypeId = (int)cmbDeviceType.SelectedValue;
                device.SerialNo = txtDeviceSerialNo.Text.Trim();
                device.BDA = txtDeviceBDA.Text.Trim();
                device.CreatedBy = Common.LoginUser.UserId;
                device.IsActive = isActive;
                device.CreatedDate = DateTime.Now;
                bool isSaved = _objDeviceBusiness.SaveDevice(device);
                if (isSaved)
                {
                    if (DeviceId!=0)
                        MessageBox.Show(UIConstant.DeviceUpdatedSuccessfully);
                    else
                        MessageBox.Show(UIConstant.DeviceSavedSuccessfully);
                    ResetControls();
                    GetDeviceData();
                }
                else
                    MessageBox.Show(UIConstant.ProblemInUpdatingDevice);
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }
        private bool IsValidEntry(int DeviceId)
        {
            bool trueFalse = false;
            _objDeviceBusiness = new DeviceManager();
            try
            {
                if (string.IsNullOrEmpty(txtDeviceName.Text.Trim()))
                {
                    MessageBox.Show(UIConstant.PleaseEnterDeviceName);
                    trueFalse = false;
                }
                else if (string.IsNullOrEmpty(txtDeviceSerialNo.Text.Trim()))
                {
                    MessageBox.Show(UIConstant.PleaseEnterDeviceSerialNumber);
                    trueFalse = false;
                }
                else if ((int)cmbDeviceType.SelectedValue <= 0)
                {
                    MessageBox.Show(UIConstant.PleaseSelectDeviceType);
                    trueFalse = false;
                }
                //else if (IsDeviceExists(DeviceId, txtDeviceName.Text.Trim()))
                //{
                //    MessageBox.Show("Device name already exists.");
                //    trueFalse = false;
                //}
                else if (IsDeviceExists(DeviceId, txtDeviceSerialNo.Text.Trim()))
                {
                    MessageBox.Show(UIConstant.DeviceSerialNumberAlreadyExists);
                    trueFalse = false;
                }
                else
                    trueFalse = true;
            }
            catch (Exception ex)
            {
                trueFalse = false;
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
            return trueFalse;
        }

        private bool IsDeviceExists(int DeviceId, string deviceNameOrSerialNo)
        {
            try
            {
                //if (deviceList.Exists(o => o.DeviceId != DeviceId && (string.Compare(o.Name, deviceNameOrSerialNo, true) == 0 || string.Compare(o.SerialNo, deviceNameOrSerialNo, true) == 0)))
                if (_deviceList.Exists(o => o.DeviceId != DeviceId && string.Compare(o.SerialNo, deviceNameOrSerialNo, true) == 0))
                    return true;
                else
                    return false;
            }
            catch
            {
                return false;
            }
        }

        private void BindDeviceTypeCombo()
        {
            try
            {
                //DigiPhotoDataServices _objdbLayer = new DigiPhotoDataServices();
                ValueTypeBusiness valBiz = new ValueTypeBusiness();
                //var list1 = _objdbLayer.GetReasonType((int)DigiPhoto.DataLayer.DigiPhotoDataServices.ValueTypeGroupEnum.ScanningDeviceType);
                var list = valBiz.GetReasonType((int)ValueTypeGroupEnum.ScanningDeviceType);
                //deviceTypes.Add(0, "--Select--");
                //foreach (DigiPhoto.IMIX.Model.ValueTypeInfo itm in list)
                //{
                //    deviceTypes.Add(itm.ValueTypeId, itm.Name);
                //}
                //cmbDeviceType.ItemsSource = deviceTypes;
                //cmbDeviceType.SelectedIndex = 0;           
                CommonUtility.BindComboWithSelect<ValueTypeInfo>(cmbDeviceType, list, "Name", "ValueTypeId", 0, DigiPhoto.Utility.Repository.Data.ClientConstant.SelectString);
                cmbDeviceType.SelectedValue = "0";
            
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }


        void GetDeviceData()
        {
            _objDeviceBusiness = new DeviceManager();
            _deviceList = new List<DeviceInfo>();
            _deviceList = _objDeviceBusiness.GetDeviceList();
            //var deviceTypes = objDeviceBusiness.GetDeviceTypes();
            //foreach (DeviceInfo device in _deviceList)
            //{
            //    device.DeviceTypeName = _deviceTypes[(int)device.DeviceTypeId].ToString();
            //}
            dgDevice.ItemsSource = _deviceList;

        }

        /// <summary>
        /// Handles the Click event of the btnEdit control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void btnEdit_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                DeviceInfo objDevice = (DeviceInfo)dgDevice.SelectedItem;
                txtDeviceName.Text = objDevice.Name;
                txtDeviceSerialNo.Text = objDevice.SerialNo;
                cmbDeviceType.SelectedValue = objDevice.DeviceTypeId;
                chkIsActive.IsChecked = objDevice.IsActive;
                btnSaveDevice.Tag = objDevice.DeviceId;
                txtDeviceBDA.Text = objDevice.BDA;

            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }
        /// <summary>
        /// Handles the Click event of the btnDelete control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void btnDelete_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                _objDeviceBusiness = new DeviceManager();
                if (MessageBox.Show("Do you want to delete this device?", "Confirm delete", MessageBoxButton.YesNo) == MessageBoxResult.Yes)
                {
                    bool IsSaved = _objDeviceBusiness.DeleteDevice(((Button)sender).Tag.ToInt32());
                    if (IsSaved)
                    {
                        MessageBox.Show(UIConstant.DeviceDeletedSuccessfully);
                        GetDeviceData();
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }
        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                ResetControls();
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }
        private void ResetControls()
        {
           txtDeviceBDA.Text= txtDeviceSerialNo.Text = txtDeviceName.Text = string.Empty;
            cmbDeviceType.SelectedIndex = 0;
            btnSaveDevice.Tag = 0;
            chkIsActive.IsChecked = true;
        }

        /// <summary>
        /// Handles the Click event of the btnBack control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void btnBack_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                ManageHome _objHome = new ManageHome();
                _objHome.Show();
                this.Close();
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }
    }
}
