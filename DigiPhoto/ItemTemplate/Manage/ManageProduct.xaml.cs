﻿using DigiAuditLogger;
using DigiPhoto.Common;
using DigiPhoto.DataLayer;
using DigiPhoto.IMIX.Business;
using DigiPhoto.IMIX.Model;
using DigiPhoto.Manage;
using DigiPhoto.Utility.Repository.ValueType;
using Excel;
using FrameworkHelper;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Media;
namespace DigiPhoto
{
    /// <summary>
    /// Interaction logic for ManageProduct.xaml
    /// </summary>
    public partial class ManageProduct : Window
    {
        #region Declaration (Manage Product)
        bool isEditGroupProduct = false;
        private int _productId = 0;
        private bool? IsPanorama = false;
        private bool? IsPersonalizedAR = false;// by latika for AR personalised
        string packagefilename = string.Empty;
        string _filedatasource = string.Empty;
        //DigiPhotoDataServices _objDataService = new DigiPhotoDataServices();
        string _syncCode = string.Empty;
        string _syncodeforPackage = string.Empty;
        int? _isInvisible = null;
        DataTable _dtExcelRecords;
        DataTable _dt;
        System.ComponentModel.BackgroundWorker BackupWorker = new System.ComponentModel.BackgroundWorker();
        BusyWindow bs = new BusyWindow();

        #endregion

        #region Constructor
        /// <summary>
        /// Initializes a new instance of the <see cref="ManageProduct"/> class.
        /// </summary>
        public ManageProduct()
        {
            try
            {
                InitializeComponent();
                GetProductTypeList();
                GetPackageDetails();
                txbUserName.Text = LoginUser.UserName;
                txbStoreName.Text = LoginUser.StoreName;
                btnSave.IsDefault = true;
                //BindGroupGrid();
                //BindGroupProductGrid();
                FillSubstore();
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }
        #endregion

        #region Events (Manage Product)

        /// <summary>
        /// Handles the Click event of the btnLogout control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void btnLogout_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                AuditLog.AddUserLog(Common.LoginUser.UserId, (int)FrameworkHelper.ActionType.Logout, "Logged out at ");
                Login _objLogin = new Login();
                _objLogin.Show();
                this.Close();
            }
            catch (Exception ex)
            {

                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
            finally
            {
                MemoryManagement.FlushMemory();
            }
        }
        /// <summary>
        /// Determines whether [is pricing updated] [the specified product unique identifier].
        /// </summary>
        /// <param name="ProductID">The product unique identifier.</param>
        /// <param name="newprice">The newprice.</param>
        /// <param name="OldPrice">The old price.</param>
        /// <returns></returns>
        private bool IsPricingUpdated(int ProductID, double newprice, out string OldPrice)
        {
            //DigiPhotoDataServices _objDataService = new DigiPhotoDataServices();
            ProductBusiness proBiz = new ProductBusiness();
            var product = proBiz.GetProductTypeListById(_productId);
            if (product.DG_Product_Pricing_ProductPrice == newprice)
            {
                OldPrice = product.DG_Product_Pricing_ProductPrice.ToString();
                return false;

            }
            else
            {
                OldPrice = product.DG_Product_Pricing_ProductPrice.ToString();
                return true;
            }
        }
        /// <summary>
        /// Handles the Click event of the btnSave control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            string SyncCode = string.Empty;
            try
            {
                if (Isvalidate())
                {
                    bool update = false;
                        IsPersonalizedAR = chkIsPersnldAR.IsChecked;
                    if (_productId > 0)
                    {
                        update = true;
                    }
                    int? isInvisible = null;
                    // SyncCode =CommonUtility.GetRandomString(8)+ Convert.ToInt32(ApplicationObjectEnum.Product).ToString().PadLeft(2, '0'); 
                    SyncCode = CommonUtility.GetUniqueSynccode(Convert.ToInt32(ApplicationObjectEnum.Product).ToString().PadLeft(2, '0'), LoginUser.countrycode, LoginUser.Storecode, "00");
                    if (update)
                    {
                        MessageBoxResult response = MessageBox.Show("Alert: Are you sure you want to update the product details?", "Temper Alert", MessageBoxButton.YesNo, MessageBoxImage.Warning);
                        if (response == MessageBoxResult.Yes)
                        {
                            ProductBusiness proBiz = new ProductBusiness();
                            var item = proBiz.GetProductByID(_productId);
                            _syncodeforPackage = item.SyncCode;
                            if (chkPackage.IsChecked == true)
                            {

                                String NewString = _syncodeforPackage.Remove(_syncodeforPackage.Length - 2, 2);
                                _syncodeforPackage = NewString + Convert.ToInt32(ApplicationObjectEnum.Package).ToString().PadLeft(2, '0');

                            }
                            if (chkInvisible.IsChecked == true)
                            {
                                isInvisible = 0;
                            }
                            else if (item.DG_Orders_ProductNumber == 0)
                            {
                                isInvisible = null;
                            }
                            else
                            {
                                isInvisible = item.DG_Orders_ProductNumber;
                            }
                            string oldprice = string.Empty;
                            bool IsPricingChanged = IsPricingUpdated(_productId, txtProductPrice.Text.ToDouble(), out oldprice);
                            //if (chkPackage.IsChecked != true)
                            //{
                            //    IsPanorama = true;
                            //}
                            IsPanorama = item.IsPanorama;

                            //if (_objDataService.SetProductTypeInfo(ProductId, txtProductName.Text, txtPRoductDescription.Text, chkDiscount.IsChecked, txtProductPrice.Text, LoginUser.StoreId, LoginUser.UserId, chkPackage.IsChecked,  .IsChecked, chkAccessory.IsChecked, txtProductCode.Text, SyncCode, syncodeforPackage, isInvisible))
                            if (new ProductBusiness().SetProductTypeInfo(_productId, txtProductName.Text, txtPRoductDescription.Text, chkDiscount.IsChecked, txtProductPrice.Text, LoginUser.StoreId, LoginUser.UserId, chkPackage.IsChecked, chkIsActive.IsChecked, chkAccessory.IsChecked, chkTaxIncluded.IsChecked, txtProductCode.Text, SyncCode, _syncodeforPackage, isInvisible, chkWaterMarkIncluded.IsChecked, Convert.ToInt32(cmbSite.SelectedValue),IsPanorama, IsPersonalizedAR))//changed by latika for AR Personalised added IsPersonalizedAR column
                            {
                                GetProductTypeList();
                                GetPackageDetails();

                                if (IsPricingChanged)
                                {
                                    AuditLog.AddUserLog(Common.LoginUser.UserId, (int)FrameworkHelper.ActionType.ChangePricing, "Product (" + txtProductName.Text + ") pricing has been changed from " + (oldprice.ToDouble()).ToString() + " to " + (txtProductPrice.Text.ToDouble()).ToString() + " on ");
                                }
                                else
                                {
                                    AuditLog.AddUserLog(Common.LoginUser.UserId, (int)FrameworkHelper.ActionType.AddProduct, "Product (" + txtProductName.Text + ") has been updated on ");
                                }


                                MessageBox.Show("[" + txtProductName.Text + "] Product has been updated successfully");

                            }
                        }
                    }
                    else
                    {
                        if (chkPackage.IsChecked == true)
                        {
                            SyncCode = CommonUtility.GetUniqueSynccode(Convert.ToInt32(ApplicationObjectEnum.Package).ToString().PadLeft(2, '0'), LoginUser.countrycode, LoginUser.Storecode, "00");
                        }
                        if (chkInvisible.IsChecked == true)
                            isInvisible = 0;
                        //if (_objDataService.SetProductTypeInfo(ProductId, txtProductName.Text, txtPRoductDescription.Text, chkDiscount.IsChecked, txtProductPrice.Text, LoginUser.StoreId, LoginUser.UserId, chkPackage.IsChecked, chkIsActive.IsChecked, chkAccessory.IsChecked, txtProductCode.Text, SyncCode, syncodeforPackage, isInvisible))
                        ProductBusiness proBiz = new ProductBusiness();

                        if (chkPackage.IsChecked != true)
                        {
                            IsPanorama = true;
                        }
                        if (proBiz.SetProductTypeInfo(_productId, txtProductName.Text, txtPRoductDescription.Text, chkDiscount.IsChecked, txtProductPrice.Text, LoginUser.StoreId, LoginUser.UserId, chkPackage.IsChecked, chkIsActive.IsChecked, chkAccessory.IsChecked, chkTaxIncluded.IsChecked, txtProductCode.Text, SyncCode, _syncodeforPackage, isInvisible, chkWaterMarkIncluded.IsChecked, Convert.ToInt32(cmbSite.SelectedValue),IsPanorama, IsPersonalizedAR))//changed by latika for AR Personalised
                        {
                            GetProductTypeList();
                            GetPackageDetails();
                            AuditLog.AddUserLog(Common.LoginUser.UserId, (int)FrameworkHelper.ActionType.EditProduct, "Product (" + txtProductName.Text + ") has been created on " + (new CustomBusineses()).ServerDateTime().ToShortDateString());
                            MessageBox.Show("[" + txtProductName.Text + "] Product details has been created successfully");
                        }
                    }

                    ClearControls();

                }
                else
                {
                    MessageBox.Show("Please enter all required values");
                }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }
        private void btnImport_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                OpenFileDialog ofd = new OpenFileDialog();
                ofd.Multiselect = false;
                ofd.Filter = "Excel documents (.xls)|*.xls";
                ofd.ShowDialog();
                GetProductsFromExcel(ofd.FileName);
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }

        }
        /// <summary>
        /// Handles the Click event of the btnCancel control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                ClearControls();
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }
        /// <summary>
        /// Handles the Click event of the btnBack control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void btnBack_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                ManageHome _objHome = new ManageHome();
                _objHome.Show();
                this.Close();
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }
        /// <summary>
        /// Handles the Click event of the btnEdit control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void btnEdit_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Button _objbtn = new Button();
                _objbtn = (Button)sender;
                _productId = _objbtn.CommandParameter.ToInt32();
                ProductTypeInfo _objProductType = GetProductTypebyProductId();
                if (_objProductType != null)
                {
                    txtProductName.Text = _objProductType.DG_Orders_ProductType_Name;
                    txtPRoductDescription.Text = _objProductType.DG_Orders_ProductType_Desc;
                    chkDiscount.IsChecked = _objProductType.DG_Orders_ProductType_DiscountApplied;
                    txtProductPrice.Text = _objProductType.DG_Product_Pricing_ProductPrice.ToString();
                    if (!_objProductType.DG_IsPackage)
                        chkPackage.IsEnabled = false;
                    else
                        chkPackage.IsEnabled = true;

                    chkPackage.IsChecked = _objProductType.DG_IsPackage;
                    chkIsActive.IsChecked = _objProductType.DG_IsActive;
                    chkAccessory.IsChecked = _objProductType.DG_IsAccessory;
                    chkTaxIncluded.IsChecked = _objProductType.DG_IsTaxEnabled;
                    txtProductCode.Text = _objProductType.DG_Orders_ProductCode;
                    if (_objProductType.DG_Orders_ProductNumber == 0)
                        chkInvisible.IsChecked = true;
                    else
                        chkInvisible.IsChecked = false;
                    chkWaterMarkIncluded.IsChecked = _objProductType.DG_IsWaterMarkIncluded;
                    cmbSite.SelectedValue = _objProductType.DG_SubStore_pkey;
                    chkIsPersnldAR.IsChecked = false;
                    if (_objProductType.IsPersonalizedAR == true)
                    {
                        chkIsPersnldAR.IsChecked = _objProductType.IsPersonalizedAR;
                    }
                    //if (_objProductType.DG_Orders_ProductNumber == 0)
                    //    chkInvisible.IsChecked = true;
                    //else
                    //    chkInvisible.IsChecked = false;

                }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }
        /// <summary>
        /// Handles the Click event of the btnDelete control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void btnDelete_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Button _objbtn = new Button();
                _objbtn = (Button)sender;
                _productId = _objbtn.CommandParameter.ToInt32();
                MessageBoxResult response = MessageBox.Show("Alert: Are you sure you want to delete the product details?", "Temper Alert", MessageBoxButton.YesNo, MessageBoxImage.Warning);
                if (response == MessageBoxResult.Yes)
                {
                    ProductBusiness proBiz = new ProductBusiness();
                    //  string productname = _objDataService.GetProductNameFromID(ProductId);
                    string productname = proBiz.GetProductTypeListById(_productId).DG_Orders_ProductType_Name;

                    //if (_objDataService.DeleteProductType(ProductId))
                    if (new ProductBusiness().DeleteProductType(_productId))
                    {

                        AuditLog.AddUserLog(Common.LoginUser.UserId, (int)FrameworkHelper.ActionType.DeleteProduct, "Product (" + productname + ") has been deleted on ");
                        MessageBox.Show("Record deleted successfully");
                        GetProductTypeList();
                        GetPackageDetails();
                        _productId = 0;
                    }
                    else
                    {
                        MessageBox.Show("Record is in use");
                    }
                }

            }
            catch (Exception ex)
            {

                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }
        #endregion

        #region Common Method (Manage Product)
        /// <summary>
        /// Isvalidates this instance.
        /// </summary>
        /// <returns></returns>
        private bool Isvalidate()
        {
            bool isvalid = true;
            if (string.IsNullOrEmpty(txtProductName.Text))
            {
                isvalid = false;
                txtProductName.Focus();
            }
            else if (!string.IsNullOrEmpty(txtProductName.Text))
            {
                ProductBusiness proBiz = new ProductBusiness();
                /// _productId

                if (proBiz.CheckduplicateProduct(txtProductName.Text, _productId))
                {
                    MessageBox.Show("Package Name already exists! kindly change the name.");
                    isvalid = false;
                    txtProductName.Focus();
                };


            }
            if (string.IsNullOrEmpty(txtPRoductDescription.Text))
            {
                isvalid = false;
                txtPRoductDescription.Focus();
            }
            if (string.IsNullOrEmpty(txtProductCode.Text))
            {
                isvalid = false;
                txtProductCode.Focus();
            }
            if (string.IsNullOrEmpty(txtProductPrice.Text))
            {
                isvalid = false;
                txtProductPrice.Focus();
            }
            if (cmbSite.SelectedIndex == 0)
            {
                isvalid = false;
                cmbSite.Focus();
            }
            return isvalid;
        }
        /// <summary>
        /// Clears the controls.
        /// </summary>
        private void ClearControls()
        {
            txtPRoductDescription.Text = "";
            txtProductName.Text = "";
            txtProductPrice.Text = "";
            txtProductCode.Text = "";
            _productId = 0;
            chkAccessory.IsChecked = false;
            chkDiscount.IsChecked = false;
            chkIsActive.IsChecked = true;
            chkPackage.IsChecked = true;
            chkInvisible.IsChecked = false;
            chkTaxIncluded.IsChecked = false;
            chkWaterMarkIncluded.IsChecked = false;
            cmbSite.SelectedIndex = 0;
            cmbSitePckg.SelectedIndex = 0;
            chkPackage.IsEnabled = true;
            chkIsPersnldAR.IsChecked = false;
        }
        /// <summary>
        /// Gets the product type list.
        /// </summary>
        private void GetProductTypeList()
        {
            ProductBusiness proBiz = new ProductBusiness();
            // grdProductType.ItemsSource = _objDataService.GetProductTypeList();
            grdProductType.ItemsSource = proBiz.GetProductTypeList(null);
        }
        /// <summary>
        /// Gets the product typeby product unique identifier.
        /// </summary>
        /// <returns></returns>
        private ProductTypeInfo GetProductTypebyProductId()
        {
            //DigiPhotoDataServices _objDataService = new DigiPhotoDataServices();
            //return _objDataService.GetProductTypeList().Where(t => t.DG_Orders_ProductType_pkey == ProductId).First();
            ProductBusiness proBiz = new ProductBusiness();
            return proBiz.GetProductTypeListById(_productId);
        }


        private void GetProductsFromExcel(string filepath)
        {

            FileStream stream = File.Open(filepath, FileMode.Open, FileAccess.Read);

            //1. Reading from a binary Excel file ('97-2003 format; *.xls)
            IExcelDataReader excelReader = ExcelReaderFactory.CreateBinaryReader(stream);
            //...
            //2. Reading from a OpenXml Excel file (2007 format; *.xlsx)
            // IExcelDataReader excelReader = ExcelReaderFactory.CreateOpenXmlReader(stream);
            //...
            //3. DataSet - The result of each spreadsheet will be created in the result.Tables
            //DataSet result = excelReader.AsDataSet();
            //...
            //4. DataSet - Create column names from first row
            excelReader.IsFirstRowAsColumnNames = true;
            DataSet result = excelReader.AsDataSet();

            //5. Data Reader methods
            int count = 0;
            foreach (DataTable table in result.Tables)
            {
                for (int i = 0; i <= table.Rows.Count; i++)
                {
                    try
                    {
                        ProductBusiness proBiz = new ProductBusiness();
                        if (chkPackage.IsChecked != true)
                        {
                            IsPanorama = true;
                        }

                          if (proBiz.SetProductTypeInfo(_productId, ProductTypeName: table.Rows[i][1].ToString(), ProductTypeDesc: table.Rows[i][2].ToString(), ProductPrice: table.Rows[i][3].ToString(), IsDiscount: (table.Rows[i][7]).ToBoolean(), stroreId: LoginUser.StoreId, UserId: LoginUser.UserId, ispackage: table.Rows[i][6].ToBoolean(), Isactive: table.Rows[i][5].ToBoolean(), IsAccessory: table.Rows[i][4].ToBoolean(), Productcode: table.Rows[i][8].ToString(), SyncCode: _syncCode, syncodeforPackage: _syncodeforPackage, IsInvisible: Convert.ToInt32(table.Rows[i][10]), IsTaxIncluded: (table.Rows[i][11]).ToBoolean(), IschkWaterMarked: table.Rows[i][12].ToBoolean(), SubStoreID: Convert.ToInt32(table.Rows[i][9]), IsPanorama: IsPanorama,IsIsPersonalizedAR: IsPersonalizedAR))//changed byt latika for AR Personalised 4Apr19
                        {
                            count++;
                        }
                    }
                    catch (Exception ex)
                    {
                        continue;
                    }
                }
            }
            if (count > 0)
            {
                MessageBox.Show("Products imported from excel successfully!");
                GetProductTypeList();
                GetPackageDetails();
            }

            //while (excelReader.Read())
            //{
            //    excelReader.GetString(0);
            //}

            //6. Free resources (IExcelDataReader is IDisposable)
            excelReader.Close();
        }
        #endregion

        #region Declaration (Manage Package)
        List<ProductTypeList> lstProductTypeList;
        int packageId = 0;
        #endregion

        #region Common Methods(Manage Package)
        /// <summary>
        /// Gets the product type listfor package.
        /// </summary>
        /// <param name="packageId">The package unique identifier.</param>
        void GetProductTypeListforPackage(int packageId)
        {
            ProductBusiness proBiz = new ProductBusiness();
            lstProductTypeList = new List<ProductTypeList>();
            if (packageId == 0)
            {

                foreach (var item in proBiz.GetProductTypeList(false))
                {
                    ProductTypeList _objnew = new ProductTypeList();
                    _objnew.IsPackage = false;
                    _objnew.ProductTypeId = item.DG_Orders_ProductType_pkey;
                    _objnew.ProductTypeName = item.DG_Orders_ProductType_Name;
                    _objnew.MaxQuantity = item.DG_MaxQuantity.ToInt32();
                    _objnew.IsPersonalizedAR = item.IsPersonalizedAR; //chaned by latika for AR Personalised 4Apr2019
                    _objnew.Quantity = 0;
                    if (_objnew.ProductTypeId == 95)
                    {
                        _objnew.visibleVideoLength = Visibility.Visible;
                        _objnew.Isactive = false;
                    }
                    else
                    {
                        _objnew.visibleVideoLength = Visibility.Collapsed;
                    }
                    lstProductTypeList.Add(_objnew);
                }
            }
            else
            {
                List<PackageDetailsViewInfo> _objlst = proBiz.GetPackagDetails(packageId);
                if (_objlst.Count > 0)
                {
                    foreach (var item in _objlst)
                    {
                        ProductTypeList _objnew = new ProductTypeList();
                        _objnew.ProductTypeId = item.DG_Orders_ProductType_pkey;
                        _objnew.ProductTypeName = item.DG_Orders_ProductType_Name;
                        _objnew.Quantity = item.DG_Product_Quantity;
                        _objnew.IsPersonalizedAR = item.IsPersonalizedAR;//chaned by latika for AR Personalised 4Apr2019
                        if (item.DG_Orders_ProductType_IsBundled == true)
                            _objnew.Isactive = false;
                        if (item.DG_IsAccessory == true)
                            _objnew.Isactive = false;
                        //if (item.DG_Orders_ProductType_pkey == 35 || item.DG_Orders_ProductType_pkey == 36 || item.DG_Orders_ProductType_pkey == 78)
                        //    _objnew.Isactive = false;
                        if (_objnew.ProductTypeId == 95)
                        {
                            _objnew.visibleVideoLength = Visibility.Visible;
                            _objnew.Isactive = false;
                        }
                        else
                        {
                            _objnew.visibleVideoLength = Visibility.Collapsed;
                        }
                        _objnew.MaxQuantity = item.DG_Product_MaxImage;
                        _objnew.VideoLength = item.DG_Video_Length;
                        lstProductTypeList.Add(_objnew);
                    }
                }
                else
                {
                    foreach (var item in proBiz.GetProductTypeList(false))
                    {
                        ProductTypeList _objnew = new ProductTypeList();
                        _objnew.IsPackage = false;
                        _objnew.ProductTypeId = item.DG_Orders_ProductType_pkey;
                        _objnew.ProductTypeName = item.DG_Orders_ProductType_Name;
                        _objnew.IsPersonalizedAR = item.IsPersonalizedAR;//chaned by latika for AR Personalised 4Apr2019
                        _objnew.MaxQuantity = 0;
                        _objnew.Quantity = 0;
                        if (item.DG_Orders_ProductType_IsBundled == true)
                            _objnew.Isactive = false;
                        if (item.DG_IsAccessory == true)
                            _objnew.Isactive = false;
                        //if (item.DG_Orders_ProductType_pkey == 35 || item.DG_Orders_ProductType_pkey == 36 || item.DG_Orders_ProductType_pkey == 78)
                        //    _objnew.Isactive = false;
                        if (_objnew.ProductTypeId == 95)
                        {
                            _objnew.visibleVideoLength = Visibility.Visible;
                            _objnew.Isactive = false;

                        }
                        else
                        {
                            _objnew.visibleVideoLength = Visibility.Collapsed;

                        }
                        lstProductTypeList.Add(_objnew);
                    }
                }
            }
            dgProducttype.ItemsSource = lstProductTypeList;
        }
        /// <summary>
        /// Gets the package details.
        /// </summary>
        void GetPackageDetails()
        {
            try
            {
                ProductBusiness pacBiz = new ProductBusiness();
                List<ProductTypeInfo> lst = pacBiz.GetPackageNames(true);
                if (cmbSitePckg.SelectedItem != null)
                {
                    SubStoresInfo subStoreInfo = (SubStoresInfo)cmbSitePckg.SelectedItem;
                    grdPackageType.ItemsSource = lst.Where(x => x.DG_SubStore_pkey.Equals(subStoreInfo.DG_SubStore_pkey));
                }
            }
            catch (Exception ex)
            {
            }


            //ProductBusiness pacBiz = new ProductBusiness();
            //grdPackageType.ItemsSource = pacBiz.GetPackageNames(true);
        }
        /// <summary>
        /// Packages the clear control.
        /// </summary>
        void PackageClearControl()
        {
            packageId = 0;
            txtPackageName.Text = "";
            dgProducttype.ItemsSource = null;
        }
        private void FillSubstore()
        {
            try
            {
                List<SubStoresInfo> lstStoreSubStore = (new StoreSubStoreDataBusniess()).GetLogicalSubStore();
                List<SubStoresInfo> lstStoreSubStorePckg = (new StoreSubStoreDataBusniess()).GetLogicalSubStore();
                //List<SubStoresInfo> lstStoreSubStore1 = new List<SubStoresInfo>();
                //lstStoreSubStore1 = lstStoreSubStore.Where(t => t.IsLogicalSubStore == true).ToList();
                CommonUtility.BindComboWithSelect<SubStoresInfo>(cmbSite, lstStoreSubStore, "DG_SubStore_Name", "DG_SubStore_pkey", 0, "--Select--");
                CommonUtility.BindComboWithSelect<SubStoresInfo>(cmbSitePckg, lstStoreSubStorePckg, "DG_SubStore_Name", "DG_SubStore_pkey", 0, "--Select--");

                //Here we are finding the logical substoreid and then setting the selected value

                SubStoresInfo _objSubstore = (new StoreSubStoreDataBusniess()).GetSubstoreData(LoginUser.SubStoreId);

                if (_objSubstore != null)
                {
                    cmbSite.SelectedValue = (_objSubstore.LogicalSubStoreID == 0) ? LoginUser.SubStoreId.ToString() : _objSubstore.LogicalSubStoreID.ToString();
                    cmbSitePckg.SelectedValue = (_objSubstore.LogicalSubStoreID == 0) ? LoginUser.SubStoreId.ToString() : _objSubstore.LogicalSubStoreID.ToString();
                }
                else
                {
                    cmbSite.SelectedValue = LoginUser.SubStoreId.ToString();
                    cmbSitePckg.SelectedValue = 0;
                }

            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
            }
        }

        #endregion

        #region Events(Manage Package)
        /// <summary>
        /// Handles the Click event of the btnBackToHome control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void btnBackToHome_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                ManageHome _objHome = new ManageHome();
                _objHome.Show();
                this.Close();
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }

        }
        /// <summary>
        /// Handles the Click event of the btnPackageClose control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void btnPackageClose_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                ///////Clear Flag
                GrdPackage.Visibility = Visibility.Collapsed;
                PackageClearControl();
                GetPackageDetails();

                IsBeingEdited = false;
                btnSave.IsDefault = true;
                btnpackageSave.IsDefault = false;
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }

        bool IsBeingEdited = false;
        /// <summary>
        /// Handles the Click event of the btnPackageEdit control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void btnPackageEdit_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Button _objbtn = new Button();
                _objbtn = (Button)sender;
                packageId = _objbtn.CommandParameter.ToInt32();
                GrdPackage.Visibility = Visibility.Visible;
                GetProductTypeListforPackage(_objbtn.CommandParameter.ToInt32());
                ProductBusiness proBiz = new ProductBusiness();
                var itemprice = proBiz.GetProductTypeListById(_objbtn.CommandParameter.ToInt32()).DG_Product_Pricing_ProductPrice;
              ////by latika for AR personalised
			    var IsPersonalise= proBiz.GetProductTypeListById(_objbtn.CommandParameter.ToInt32()).IsPersonalizedAR; //chaned by latika for AR Personalised 4Apr2019
                txtProductPricePackage.Text = itemprice.ToString();
                txtPackageName.Text = _objbtn.Tag.ToString();
                if (IsPersonalise == true)
                { IsPersonalizedAR = true; }
                /////Open Flag
                IsBeingEdited = true;
                btnSave.IsDefault = false;
                btnBackToHome.IsDefault = false;
                btnpackageSave.IsDefault = true;

            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }
        /// <summary>
        /// Handles the Click event of the btnPackageDelete control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void btnPackageDelete_Click(object sender, RoutedEventArgs e)
        {
            try
            {

            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }
        /// <summary>
        /// Handles the Click event of the btnpackageSave control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void btnpackageSave_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                PackageBusniess pacBiz = new PackageBusniess();/////by latika for AR personalised
                bool Online = true;
                //string SyncCode = CommonUtility.GetRandomString(8) + Convert.ToInt32(ApplicationObjectEnum.Package).ToString().PadLeft(2, '0'); 
                //string SyncCode = CommonUtility.GetUniqueSynccode(Convert.ToInt32(ApplicationObjectEnum.Package).ToString().PadLeft(2, '0'), LoginUser.countrycode, LoginUser.Storecode, "00");
                foreach (var item in lstProductTypeList)
                {
                    if ((item.ProductTypeId == 35 || item.ProductTypeId == 36 || item.ProductTypeId == 80 || item.ProductTypeId == 81 || item.ProductTypeId == 82 || item.ProductTypeId == 83 || item.ProductTypeId == 78) && (item.Quantity != 0 && item.Quantity != 1))
                    {
                        MessageBox.Show("Quantity should be 0 or 1 for product : " + item.ProductTypeName);
                        return;
                    }
                }

                var QrCode = (from c in lstProductTypeList
                              where c.ProductTypeId == 104
                              select c).FirstOrDefault();

                var Onlinepkg = (from c in lstProductTypeList
                                 where c.ProductTypeId == 84
                                 select c).FirstOrDefault();
                var video = (from c in lstProductTypeList
                             where c.ProductTypeName == "VideoProduct"
                             select c).FirstOrDefault();
                if (QrCode != null)
                {
                    if (QrCode.Quantity > 0)
                    {

                        if (Onlinepkg.Quantity < 1)
                        {
                            Online = false;
                        }
                        else { Online = true; }
                    }
                }
                if (!Online)
                {
                    MessageBox.Show("Please Include " + Onlinepkg.ProductTypeName + " product with " + QrCode.ProductTypeName + " product");
                    return;
                }

                // Code Addad by Anis for validation
                var UQrCode = (from c in lstProductTypeList
                               where c.ProductTypeId == 124
                               select c).FirstOrDefault();

                if (UQrCode != null)
                {
                    if (UQrCode.Quantity > 0)
                    {

                        if (Onlinepkg.Quantity < 1)
                        {
                            Online = false;
                        }
                    }
                }
					/////////by latika for AR Personalised
                if (pacBiz.GETIsPersonalizedAR(packageId) == true)
                {
                    //if (video.Quantity < 1 && (!Online))
                    //{
                    //    MessageBox.Show("Please Include " + video.ProductTypeName + " ," + Onlinepkg.ProductTypeName + " product with " + QrCode.ProductTypeName + " product");
                    //    return;
                    //}
                    //else if(video.Quantity < 1)  { 
                    //MessageBox.Show("Please Include " + video.ProductTypeName +" product");
                    //    return;
                    //}
                    //else 
                    if (!Online)
                    {
                        MessageBox.Show("Please Include " + Onlinepkg.ProductTypeName + " product with " + QrCode.ProductTypeName + " product");
                        return;
                    }
                    else if (Onlinepkg.MaxQuantity != 2)
                    {
                        MessageBox.Show("Max Quantity should be 2 for " + Onlinepkg.ProductTypeName + "  product");
                        return;
                    }

                }
               else  if (!Online)////end
                {
                    MessageBox.Show("Please Include " + Onlinepkg.ProductTypeName + " product with " + QrCode.ProductTypeName + " product");
                    return;
                }
                else if (!Online)
                {
                    MessageBox.Show("Please Include " + Onlinepkg.ProductTypeName + " product with " + UQrCode.ProductTypeName + " product");
                    return;
                }
                //End
                //if (item.ProductTypeId == 104 && item.Quantity == 1)
                //{
                //}
                //PackageBusniess pacBiz = new PackageBusniess();
                bool retvalue = pacBiz.SetPackageMasterDetails(packageId, txtPackageName.Text, txtProductPricePackage.Text);
                if (retvalue)
                {
                    foreach (var item in lstProductTypeList)
                    {
                        if (item.ProductTypeId == 4 || item.ProductTypeId == 101)        //Unique Four 4 * 5 or Unique Four 4 * 5 small Wallet
                        {
                            item.MaxQuantity = item.Quantity > 0 ? 4 : 1;
                        }
                        if (item.ProductTypeId == 105)        //Unique Four 4 * 5 or Unique Four 4 * 5 small Wallet
                        {
                            item.MaxQuantity = item.Quantity > 0 ? 2 : 1;
                        }
                        pacBiz.SetPackageDetails(packageId, item.ProductTypeId, item.Quantity, item.MaxQuantity == 0 ? 1 : item.MaxQuantity, item.VideoLength);
                    }
                }
                ////////Check Flag and entries on Log

                if (IsBeingEdited)
                    AuditLog.AddUserLog(Common.LoginUser.UserId, (int)FrameworkHelper.ActionType.EditaPackage, "Package (" + txtPackageName.Text + ") has been Edited on ");
                else
                    AuditLog.AddUserLog(Common.LoginUser.UserId, (int)FrameworkHelper.ActionType.CreatePackage, "Package (" + txtPackageName.Text + ") has been created on ");

                if (QrCode != null && QrCode.Quantity > 0)
                {
                    MessageBox.Show("Package[" + txtPackageName.Text + "] has been saved successfully.Please verify the setting for " + QrCode.ProductTypeName + " Product in digiconfig utility.");
                }
                else
                {
                    MessageBox.Show("Package[" + txtPackageName.Text + "] has been saved successfully");
                }

                IsBeingEdited = false;
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }

        }
        private void btnimportPackageSave_Click(object sender, RoutedEventArgs e)
        {

            BackupWorker.DoWork += new System.ComponentModel.DoWorkEventHandler(BackupWorker_DoWork);
            BackupWorker.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(BackupWorker_RunWorkerCompleted);
            System.Windows.Forms.OpenFileDialog fdlg = new System.Windows.Forms.OpenFileDialog();
            fdlg.Title = "Select file";
            fdlg.Filter = "Excel Sheet(*.xls)|*.xls|All Files(*.*)|*.*";
            fdlg.FilterIndex = 1;
            fdlg.RestoreDirectory = true;
            if (fdlg.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                _filedatasource = fdlg.FileName;
                string[] filename = fdlg.FileName.Split('\\');
                packagefilename = filename.Last();
                Import(_filedatasource);
                System.Windows.Forms.Application.DoEvents();


            }
        }
        private void btndownload_Click(object sender, RoutedEventArgs e)
        {
            string productTemplate = System.IO.Path.Combine(Environment.CurrentDirectory, "ICER Price List - Template.xls");
            //string path = LoginUser.DigiFolderPath;

            string result;
            result = System.IO.Path.GetFileName(productTemplate);
            System.Windows.Forms.SaveFileDialog sfd = new System.Windows.Forms.SaveFileDialog();
            sfd.FileName = result;
            sfd.Filter = "Excel Sheet(*.xls)|*.xls|All Files(*.*)|*.*";
            sfd.RestoreDirectory = true;

            //string path1 = sfd.FileName;
            if (sfd.ShowDialog() != System.Windows.Forms.DialogResult.Cancel)
            {
                string productFile = sfd.FileName;
                if (File.Exists(productTemplate))
                {
                    System.IO.File.Copy(productTemplate, productFile, true);
                    MessageBox.Show("File Downloaded Successfully");
                }
            }
        }
        private void Import(string filedatasource)
        {
            if (filedatasource != string.Empty)
            {
                try
                {

                    String constr = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + filedatasource + ";Extended Properties='Excel 12.0 XML;HDR=YES;';";
                    OleDbConnection excelConnection = new OleDbConnection(constr);
                    OleDbCommand command = new OleDbCommand();
                    command.CommandType = System.Data.CommandType.Text;
                    command.Connection = excelConnection;
                    OleDbDataAdapter dAdapter = new OleDbDataAdapter(command);
                    _dtExcelRecords = new DataTable("Package");
                    excelConnection.Open();

                    DataTable dtExcelSheets = excelConnection.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
                    int rowCount = 0;
                    for (rowCount = 0; rowCount < dtExcelSheets.Rows.Count; rowCount++)
                    {
                        if (dtExcelSheets.Rows[rowCount]["Table_Name"].ToString().EndsWith("$"))
                        {
                            break;
                        }
                    }

                    if (rowCount >= dtExcelSheets.Rows.Count)
                    {
                        throw new Exception("There is no valid Excel Sheet");
                    }

                    string getExcelSheetName = dtExcelSheets.Rows[rowCount]["Table_Name"].ToString();

                    command.CommandText = "SELECT * FROM [" + getExcelSheetName + "]";
                    dAdapter.SelectCommand = command;
                    dAdapter.Fill(_dtExcelRecords);
                    excelConnection.Close();
                    System.Data.DataColumn newColumn = new System.Data.DataColumn("SyncCode", typeof(System.String));
                    _dtExcelRecords.Columns.Add(newColumn);
                    foreach (DataRow dr in _dtExcelRecords.Rows)
                    {
                        if (!String.IsNullOrWhiteSpace(Convert.ToString(dr[0])))
                            dr["SyncCode"] = CommonUtility.GetUniqueSynccode(Convert.ToInt32(ApplicationObjectEnum.Product).ToString().PadLeft(2, '0'), LoginUser.countrycode, LoginUser.Storecode, "00");
                    }
                    _dt = _dtExcelRecords.Rows.Cast<DataRow>().Where(row => !row.ItemArray.All(field => field is System.DBNull || string.Compare((field as string), string.Empty) == 0)).CopyToDataTable();
                    bs.Show();
                    BackupWorker.RunWorkerAsync();
                    //bool result = _objDataService.UploadPackagefromExcel(dtExcelRecords);

                }

                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message.ToString());
                }
            }
        }


        private void BackupWorker_DoWork(object Sender, System.ComponentModel.DoWorkEventArgs e)
        {
            bool result = (new ProductBusiness()).BulkSaveProduct(_dt, LoginUser.UserId, LoginUser.StoreId);
            //bool result = _objDataService.UploadPackagefromExcel(dt, LoginUser.UserId, LoginUser.StoreId);
            if (result)
                MessageBox.Show("Packages Saved Successfully");
            else
                MessageBox.Show("There is some error while saving Products/Packages");
        }
        private void BackupWorker_RunWorkerCompleted(object Sender, System.ComponentModel.RunWorkerCompletedEventArgs e)
        {
            bs.Hide();
        }
        /// <summary>
        /// Handles the Click event of the btnpackageCancel control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void btnpackageCancel_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                txtPackageName.Text = "";
                txtProductPricePackage.Text = "0";
                GetProductTypeListforPackage(0);
                btnSave.IsDefault = true;
                btnpackageSave.IsDefault = false;
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }


        #endregion


        #region group
        int GroupPkey = 0;
        bool isEditGroup = false;

        int GroupProductPkey = 0;
        private static bool IsTextAllowed(string text)
        {
            Regex regex = new Regex("[^0-9.-]+"); //regex that matches disallowed text
            return !regex.IsMatch(text);
        }
        private void txtProductPrice_PreviewTextInput(object sender, System.Windows.Input.TextCompositionEventArgs e)
        {
            e.Handled = !IsTextAllowed(e.Text);
        }
        private void PastingHandler(object sender, DataObjectPastingEventArgs e)
        {
            if (e.DataObject.GetDataPresent(typeof(String)))
            {
                String text = (String)e.DataObject.GetData(typeof(String));
                if (!IsTextAllowed(text)) e.CancelCommand();
            }
            else e.CancelCommand();
        }

        //private void btnSaveGroup_Click(object sender, RoutedEventArgs e)
        //{
        //    try
        //    {
        //        string syncCode = CommonUtility.GetUniqueSynccode(Convert.ToInt32(ApplicationObjectEnum.Group).ToString().PadLeft(2, '0'), LoginUser.countrycode, LoginUser.Storecode, "00");
        //        GroupDetails groupInfo = null;
        //        if (!string.IsNullOrEmpty(txtGroupName.Text))
        //        {
        //            GroupBusiness groupBiz = new GroupBusiness();
        //            new System.Windows.Media.SolidColorBrush(Colors.Gray);
        //            //groupInfo = new GroupDetails
        //            //{
        //            //    DG_Group_pkey = 0,
        //            //    DG_Group_Name = txtGroupName.Text,
        //            //    SyncCode = syncCode,
        //            //    IsSynced = true,
        //            //    OperationType = 1
        //            //};
        //            if (isEditGroup)
        //            {
        //                groupInfo = new GroupDetails
        //              {
        //                  DG_Group_pkey = GroupPkey,
        //                  DG_Group_Name = txtGroupName.Text,
        //                  SyncCode = syncCode,
        //                  IsSynced = true,
        //                  OperationType = 2
        //              };
        //            }
        //            else
        //            {
        //groupInfo = new GroupDetails
        //{
        //    DG_Group_pkey = 0,
        //    DG_Group_Name = txtGroupName.Text,
        //    SyncCode = syncCode,
        //    IsSynced = true,
        //    OperationType = 1
        //};


        //            }



        //            var strmsg = groupBiz.SetGroupDetails(groupInfo);
        //            if (strmsg == true)
        //            {
        //                MessageBox.Show("Record saved succesfully");
        //                BindGroupGrid();
        //            }

        //        }
        //        else
        //        {
        //            MessageBox.Show("Please enter the Group");
        //            //new System.Windows.Media.SolidColorBrush(Colors.Red);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        ErrorHandler.ErrorHandler.LogError(ex);
        //    }
        //    txtGroupName.Text = string.Empty;
        //    isEditGroup = false;
        //    GroupPkey = 0;

        //}

        //private void btnRefreshGroup_Click(object sender, RoutedEventArgs e)
        //{
        //    isEditGroup = false;
        //    GroupPkey = 0;
        //    txtGroupName.Text = string.Empty;

        //}
        //private void btnGroupEdit_Click(object sender, RoutedEventArgs e)
        //{
        //    try
        //    {
        //        isEditGroup = true;

        //        Button _objbtn = (Button)(sender);
        //        var groupId = GroupPkey = _objbtn.CommandParameter.ToInt32();

        //        var groupBiz = new GroupBusiness();
        //        var lstGroupDetails = groupBiz.GetGroupDetails(groupId);
        //        txtGroupName.Text = lstGroupDetails.FirstOrDefault().DG_Group_Name;
        //    }
        //    catch (Exception ex)
        //    {
        //        ErrorHandler.ErrorHandler.LogError(ex);
        //    }
        //}
        //private void btnGroupDelete_Click(object sender, RoutedEventArgs e)
        //{
        //    try
        //    {
        //        if (MessageBox.Show("Do you want to delete this Group?", "Confirm delete", MessageBoxButton.YesNo) == MessageBoxResult.Yes)
        //        {
        //            Button _objbtn = (Button)(sender);
        //            var groupId = _objbtn.CommandParameter.ToInt32();
        //            var groupInfo = new GroupDetails
        //            {
        //                DG_Group_pkey = groupId,
        //                DG_Group_Name = string.Empty,
        //                IsSynced = true,
        //                SyncCode = string.Empty,
        //                OperationType = 3

        //            };
        //            var groupBiz = new GroupBusiness();
        //            var result = groupBiz.SetGroupDetails(groupInfo);
        //            if (result == true)
        //            {
        //                MessageBox.Show("Record Deleted Succesfully ");
        //                BindGroupGrid();

        //            }

        //        }

        //    }
        //    catch (Exception ex)
        //    {
        //        ErrorHandler.ErrorHandler.LogError(ex);
        //    }

        //}

        //private void BindGroupGrid()
        //{
        //    try
        //    {
        //        var groupBiz = new GroupBusiness();
        //        var result = groupBiz.GetGroupDetails(0);
        //        grdGroup.ItemsSource = result;
        //        var result1 = groupBiz.GetGroupDetails(0);
        //        CommonUtility.BindComboWithSelect<GroupDetails>(cmbGroupName, result1, "DG_Group_Name", "DG_Group_pkey", 0, DigiPhoto.Utility.Repository.Data.ClientConstant.SelectString);
        //        cmbGroupName.SelectedValue = "0";
        //    }
        //    catch (Exception ex)
        //    {
        //        ErrorHandler.ErrorHandler.LogError(ex);
        //    }
        //}

        //private void btnSavemapping_Click(object sender, RoutedEventArgs e)
        //{
        //    try
        //    {
        //        if (cmbGroupName.SelectedIndex <= 0)
        //{
        //            MessageBox.Show("Please Select Group Name");
        //            return;
        //        }
        //        if (string.IsNullOrEmpty(txtGroupProductCode.Text))
        //  {
        //            MessageBox.Show("Please Enter Product Code");
        //            return;
        //}
        //        GroupDetails groupInfo = null;
        //        if (!string.IsNullOrEmpty(txtGroupProductCode.Text))
        //{
        //            GroupBusiness groupBiz = new GroupBusiness();
        //            new System.Windows.Media.SolidColorBrush(Colors.Gray);
        //    groupInfo = new GroupDetails
        //    {
        //        DG_Group_pkey = Convert.ToInt16(cmbGroupName.SelectedValue),
        //        DG_ProductCode = txtGroupProductCode.Text,
        //        OperationType = 1
        //    };

        //            //if (isEditGroupProduct == true)
        //            //{
        //            //    groupInfo = new GroupDetails
        //            //  {
        //            //      DG_Group_pkey = GroupProductPkey,
        //            //      DG_ProductCode = txtGroupProductCode.Text,
        //            //      OperationType = 2
        //            //  };
        //            //}
        //            //else
        //            //{
        //            //    groupInfo = new GroupDetails
        //            //    {
        //            //        DG_Group_pkey = Convert.ToInt16(cmbGroupName.SelectedValue),
        //            //        DG_ProductCode = txtGroupProductCode.Text,
        //            //        OperationType = 1
        //            //    };
        //            //}



        //            var strmsg = groupBiz.SetGroupProductDetails(groupInfo);
        //            if (strmsg == true)
        //            {
        //                MessageBox.Show("Record saved succesfully");
        //                BindGroupProductGrid();
        //            }

        //        }
        //        else
        //        {
        //            MessageBox.Show("Please enter the Group");
        //            //new System.Windows.Media.SolidColorBrush(Colors.Red);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        ErrorHandler.ErrorHandler.LogError(ex);
        //    }
        //    cmbGroupName.SelectedValue = "0";
        //    txtGroupName.Text = string.Empty;
        //    isEditGroupProduct = false;
        //    GroupProductPkey = 0;

        //}

        //private void BindGroupProductGrid()
        //{

        //    try
        //    {
        //        var groupBiz = new GroupBusiness();
        //        var result = groupBiz.GetGroupproductDetails(0);
        //        grdGroupProductMap.ItemsSource = result;
        //    }
        //    catch (Exception ex)
        //    {
        //        ErrorHandler.ErrorHandler.LogError(ex);
        //    }

        //}
        //private void btnGroupProductEdit_Click(object sender, RoutedEventArgs e)
        //{
        //    try
        //    {
        //        isEditGroupProduct = true;
        //        Button _objbtn = (Button)(sender);
        //        var groupId = GroupPkey = _objbtn.CommandParameter.ToInt32();

        //        var groupBiz = new GroupBusiness();
        //        var lstGroupDetails = groupBiz.GetGroupproductDetails(groupId);
        //        txtGroupProductCode.Text = lstGroupDetails.FirstOrDefault().DG_ProductCode;
        //        cmbGroupName.Text = lstGroupDetails.FirstOrDefault().DG_Group_Name;
        //    }
        //    catch (Exception ex)
        //    {
        //        ErrorHandler.ErrorHandler.LogError(ex);
        //    }
        //}

        //private void btnGroupProductDelete_Click(object sender, RoutedEventArgs e)
        //{
        //    try
        //    {
        //        if (MessageBox.Show("Do you want to delete this Record?", "Confirm delete", MessageBoxButton.YesNo) == MessageBoxResult.Yes)
        //        {
        //            Button _objbtn = (Button)(sender);
        //            var groupId = _objbtn.CommandParameter.ToInt32();
        //            var groupInfo = new GroupDetails
        //            {
        //                DG_Group_pkey = groupId,
        //                OperationType = 3,
        //                DG_ProductCode=string.Empty 

        //            };
        //            var groupBiz = new GroupBusiness();
        //            var result = groupBiz.SetGroupProductDetails(groupInfo);
        //            if (result == true)
        //            {
        //                MessageBox.Show("Record Deleted Succesfully ");
        //                BindGroupProductGrid();
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        ErrorHandler.ErrorHandler.LogError(ex);
        //    }
        //}

        //private void cmbGroupName_SelectionChanged(object sender, SelectionChangedEventArgs e)
        //{
        //    try
        //    {
        //        //GetPermissionDataforGrid();
        //        // GetPermissionDataforGridIsChecked(false, false);
        //    }
        //    catch (Exception ex)
        //    {
        //        ErrorHandler.ErrorHandler.LogError(ex);
        //    }

        //  }

        #endregion

        private void cmbSitePckg_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                ProductBusiness pacBiz = new ProductBusiness();
                List<ProductTypeInfo> lst = pacBiz.GetPackageNames(true);
                //if(cmbSitePckg.SelectedValue)
                SubStoresInfo subStoreInfo = (SubStoresInfo)cmbSitePckg.SelectedItem;
                grdPackageType.ItemsSource = lst.Where(x => x.DG_SubStore_pkey.Equals(subStoreInfo.DG_SubStore_pkey));
            }
            catch (Exception ex)
            {

            }
        }
    }




    #region Custom Class
    public class ProductTypeList
    {
        public int PackageDetailsID { get; set; }
        public bool IsPackage { get; set; }
        public int ProductTypeId { get; set; }
        public string ProductTypeName { get; set; }
        public bool IsSeletedPack { get; set; }
        public int? Quantity { get; set; }
        public int? MaxQuantity { get; set; }
        public bool? Isactive { get; set; }
        public int? VideoLength { get; set; }

        public Visibility visibleVideoLength { get; set; }
        public Visibility visibleMaxImagesVideos { get; set; }
        public bool? IsPersonalizedAR { get; set; }//changed by latika for AR personalised 4Apr19
    }
    #endregion
}
