﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using CrystalDecisions.Shared.Json;
using DigiAuditLogger;
//using DigiPhoto.DataLayer;
//using DigiPhoto.DataLayer.Model;
using DigiPhoto.IMIX.Business;
using DigiPhoto.IMIX.Model;
using DigiPhoto.Utility.Repository.ValueType;
using DigiPhoto.Common;
using System.Data;

namespace DigiPhoto.Manage
{
    /// <summary>
    /// Interaction logic for SyncStatus.xaml
    /// </summary>
    public partial class SyncStatus : Window
    {
        #region Declaration

        //public enum SyncOrderStatus
        //{
        //    NotSynced = 0,
        //    Synced = 1,
        //    Error = -1,
        //    Invalid = -2
        //}
        List<SyncStatusInfo> _lstReSyncOnline;
        List<SyncStatusInfo> _lstReSyncNormal;
        List<SyncStatusInfo> _lstReSyncForm;
        List<MasterData> _lstReSyncEntity;
        List<WhatsAppSettingsTracking> _lstWhtsAppOrders;
        Statistics statictics = new Statistics();
        WhatsAppStatistics Whtsstatictics = new WhatsAppStatistics();
        int TotalOrderCount = 0;
        int SucessOrderCount = 0;
        int ImageUploadedCount = 0;
        int ImageFailedCount = 0;
        int SuccesfullImagesCount = 0;

        int WhtsTotalOrderCount = 0;
        int WhtsSucessOrderCount = 0;
        int WhtsImageUploadedCount = 0;
        int WhtsImageFailedCount = 0;
        

        #endregion
        public SyncStatus()
        {
            InitializeComponent();
            txbUserName.Text = LoginUser.UserName;
            txbStoreName.Text = LoginUser.StoreName;
            CtrlSyncStatus.SetParent(PageName);
            FillMasterData();
        }
        private void GetSyncData()
        {
            dtFrom.Value = DateTime.Now.Date.Add(new TimeSpan(6, 0, 0));
            dtTo.Value = DateTime.Now.Date.Add(new TimeSpan(23, 0, 0));
            _lstReSyncOnline = (new SyncStatusBusiness()).GetOrdersyncStatus(dtFrom.Value, dtTo.Value,txtQRCOde.Text);
            DGManageSyncStatus.ItemsSource = _lstReSyncOnline;
        }

        private void btndetail_Click(object sender, RoutedEventArgs e)
        {
            CtrlSyncStatus.ChangeTrackingID = ((Button)sender).Tag.ToInt64();
            CtrlSyncStatus.ShowHandlerDialog("Rate detail");
        }
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                //GetSyncData();
                //For Normal Orders
                dtFrom.Value = DateTime.Now.Date.Add(new TimeSpan(6, 0, 0));
                dtTo.Value = DateTime.Now.Date.Add(new TimeSpan(23, 0, 0));

                dtFromNormalOrd.Value = DateTime.Now.Date.Add(new TimeSpan(6, 0, 0));
                dtToNormalOrd.Value = DateTime.Now.Date.Add(new TimeSpan(23, 0, 0));

                dtFromOpenCloseForm.Value = DateTime.Now.Date.Add(new TimeSpan(6, 0, 0));
                dtToOpenCloseForm.Value = DateTime.Now.Date.Add(new TimeSpan(23, 0, 0));

                dtFromEntity.Value = DateTime.Now.Date.Add(new TimeSpan(6, 0, 0));
                dtToEntity.Value = DateTime.Now.Date.Add(new TimeSpan(23, 0, 0));

                dtWtsFrom.Value = DateTime.Now.Date.Add(new TimeSpan(6, 0, 0));
                dtWtsTo.Value = DateTime.Now.Date.Add(new TimeSpan(23, 0, 0));
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
            }
        }
        private void btnSearch_Click(object sender, RoutedEventArgs e)
        {
            GetSyncResult();
        }



        private void chkALLOnline_Checked(object sender, RoutedEventArgs e)
        {
            try
            {
                _lstReSyncOnline.ForEach(z => z.IsAvailable = true);
                DGManageSyncStatus.ItemsSource = _lstReSyncOnline;
                DGManageSyncStatus.Items.Refresh();
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
            }
        }
        private void chkALLOnline_Unchecked(object sender, RoutedEventArgs e)
        {
            try
            {
                _lstReSyncOnline.ForEach(z => z.IsAvailable = false);
                DGManageSyncStatus.ItemsSource = _lstReSyncOnline;
                DGManageSyncStatus.Items.Refresh();
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
            }
        }

        private void btnReSyncOnline_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                ReSync(_lstReSyncOnline);
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
            }
        }

        private void chkALLNormal_Checked(object sender, RoutedEventArgs e)
        {
            try
            {
                _lstReSyncNormal.ForEach(z => z.IsAvailable = true);
                DGManageSyncStatusNormalOrd.ItemsSource = _lstReSyncNormal;
                DGManageSyncStatusNormalOrd.Items.Refresh();
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
            }
        }
        private void chkALLNormal_Unchecked(object sender, RoutedEventArgs e)
        {
            try
            {
                _lstReSyncNormal.ForEach(z => z.IsAvailable = false);
                DGManageSyncStatusNormalOrd.ItemsSource = _lstReSyncNormal;
                DGManageSyncStatusNormalOrd.Items.Refresh();
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
            }
        }

        private void btnReSyncNormal_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                ReSync(_lstReSyncNormal);

            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
            }
        }

        private void btnBack_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                ManageHome _objHome = new ManageHome();
                _objHome.Show();
                this.Close();
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
            }
        }

        private void btnLogout_Click(object sender, RoutedEventArgs e)
        {
            try
            {

                AuditLog.AddUserLog(Common.LoginUser.UserId, 39, "Logged out at ");

                Login _objLogin = new Login();
                _objLogin.Show();
                this.Close();
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
            }
        }

        private void btnBackNormalOrd_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                ManageHome _objHome = new ManageHome();
                _objHome.Show();
                this.Close();
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
            }
        }

        private void btnSearchNormal_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                DateTime? fromDtNormalOrd = dtFromNormalOrd.Value;
                DateTime? toDtNormalOrd = dtToNormalOrd.Value;
                if (fromDtNormalOrd != null && toDtNormalOrd != null)
                    GetSyncStatusNormalOrders((DateTime)fromDtNormalOrd, (DateTime)toDtNormalOrd);
                else
                    MessageBox.Show("From date or To date cannot be empty!", "DigiPhoto", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
            }
        }
        private void GetSyncStatusNormalOrders(DateTime fromDate, DateTime toDate)
        {
            try
            {
                _lstReSyncNormal = (new SyncStatusBusiness()).GetSyncStatusList(fromDate, toDate);
                if (_lstReSyncNormal.Count <= 0)
                    MessageBox.Show("No records found!", "DigiPhoto", MessageBoxButton.OK, MessageBoxImage.Information);
                else
                    DGManageSyncStatusNormalOrd.ItemsSource = _lstReSyncNormal;
                chkALLNormal.IsChecked = false;
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
            }
        }

        private void GetSyncStatusopenCloseForm(DateTime fromDate, DateTime toDate)
        {
            try
            {
                _lstReSyncNormal = (new SyncStatusBusiness()).GetSyncStatusList(fromDate, toDate);
                if (_lstReSyncNormal.Count <= 0)
                    MessageBox.Show("No records found!", "DigiPhoto", MessageBoxButton.OK, MessageBoxImage.Information);
                else
                    DGManageSyncStatusNormalOrd.ItemsSource = _lstReSyncNormal;
                chkALLNormal.IsChecked = false;
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
            }
        }

        private void ReSync(List<SyncStatusInfo> lstReSync)
        {
            Boolean flag = true;
            StringBuilder SyncId = new StringBuilder();
            foreach (var item in lstReSync)
            {
                if (item.IsAvailable == true)
                    SyncId.Append(item.ChangeTrackingId + ",");
            }
            if (SyncId.Length > 0)
            {
                flag = (new SyncStatusBusiness()).ReSync(SyncId.ToString().Substring(0, SyncId.Length - 1));
                if (flag == true)
                    MessageBox.Show("Sync Re-initiated Successfully");
            }
        }

        private void ReSyncImages(List<SyncStatusInfo> lstReSync)
        {
            Boolean flag = true;
            StringBuilder OrderId = new StringBuilder();
            foreach (var item in lstReSync)
            {
                if (item.IsAvailable == true)
                    OrderId.Append(item.DGOrderspkey + ",");
            }
            if (OrderId.Length > 0)
            {
                flag = (new SyncStatusBusiness()).ReSyncImages(OrderId.ToString().Substring(0, OrderId.Length - 1));
                if (flag == true)
                    MessageBox.Show("Image ReSync started Successfully");
            }
        }


        private void ReSyncWhatsAppImages(List<WhatsAppSettingsTracking> lstReSync)
        {
            Boolean flag = true;
            StringBuilder Order_Numbers = new StringBuilder();
            foreach (var item in lstReSync)
            {
                if (item.IsAvailable == true)
                    Order_Numbers.Append("'"+item.Order_Number+ "'" + ",");
            }
            if (Order_Numbers.Length > 0)
            {
                flag = (new WhatsAppBusiness()).ResyncWtsAppOrder(Order_Numbers.ToString().Substring(0, Order_Numbers.Length - 1));
                if (flag == true)
                    MessageBox.Show("Images Re-Send Successfully");
            }
        }

        private void btnReSyncImages_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                ReSyncImages(_lstReSyncOnline);
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
            }
        }

        private void DGManageSyncStatus_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        private void statistics_Click(object sender, RoutedEventArgs e)
        {
            GetSyncResult();
            if (_lstReSyncOnline.Count > 0)
            {
                statictics.SetParent(this);
                Statistics.SucessOrderCounts = SucessOrderCount;
                Statistics.ImageFailedCounts = ImageFailedCount;
                Statistics.ImageUploadedCounts = ImageUploadedCount;
                Statistics.TotalOrderCounts = TotalOrderCount;
                //StatisticsControl.ShowPanHandlerDialog(SucessOrderCount, ImageFailedCount, ImageUploadedCount, TotalOrderCount);
                StatisticsControl.ShowPanHandlerDialog();
            }
        }
        public void GetSyncResult()
        {
            try
            {
                TotalOrderCount = 0;
                SucessOrderCount = 0;
                ImageUploadedCount = 0;
                ImageFailedCount = 0;
                SuccesfullImagesCount = 0;
                DateTime fromDt = DateTime.Now;
                DateTime toDt = DateTime.Now;
                if (dtFrom.Text != null)
                    fromDt = (DateTime)dtFrom.Value;
                if (dtTo.Text != null)
                    toDt = (DateTime)dtTo.Value;
                
                _lstReSyncOnline = (new SyncStatusBusiness()).GetOrdersyncStatus(fromDt, toDt,txtQRCOde.Text);
                if (_lstReSyncOnline.Count <= 0)
                    MessageBox.Show("No Record Found");
                else
                    //
                    foreach (var get in _lstReSyncOnline.Select(x => x.ImageSynced))
                    {
                        string tail = get.Substring(get.LastIndexOf('/') + 1);
                        string head = get.Split('/')[0];


                        ImageUploadedCount = ImageUploadedCount + tail.ToInt32();//4 total number of images
                        SuccesfullImagesCount = SuccesfullImagesCount + head.ToInt32();
                    }

                foreach (var getStatus in _lstReSyncOnline.Select(x => x.Syncstatus))
                {
                    if (getStatus == "Successful")//2
                    {
                        SucessOrderCount++;
                    }

                }

                foreach (var ImageSynced in _lstReSyncOnline.Where(x => x.Syncstatus == "Successful" || x.Syncstatus == "Failed").Select(x => x.ImageSynced))
                {
                    string tails = ImageSynced.Substring(ImageSynced.LastIndexOf('/') + 1);
                    string heads = ImageSynced.Split('/')[0];
                    ImageFailedCount = tails.ToInt32() - heads.ToInt32();

                }

                //on popcall 'TotalOrderCount','ImageUploadedCount','ImageFailedCount','SucessOrderCount'
                TotalOrderCount = _lstReSyncOnline.Count;


                DGManageSyncStatus.ItemsSource = _lstReSyncOnline;
                chkALLOnline.IsChecked = false;
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
            }
        }

        public void GetWhtsAppResult()
        {
            try
            {
                WhtsTotalOrderCount = 0;
                WhtsSucessOrderCount = 0;
                WhtsImageUploadedCount = 0;
                WhtsImageFailedCount = 0;
                
                DateTime fromDt = DateTime.Now;
                DateTime toDt = DateTime.Now;
                if (dtFrom.Text != null)
                    fromDt = (DateTime)dtWtsFrom.Value;
                if (dtTo.Text != null)
                    toDt = (DateTime)dtWtsTo.Value;

                _lstWhtsAppOrders = (new WhatsAppBusiness()).GetWhtsAppOrderStatus(fromDt, toDt, txtOrderNum.Text.Trim(), txtMobNum.Text.Trim());
                if (_lstWhtsAppOrders.Count <= 0)
                    MessageBox.Show("No Record Found");
                else
                    //
                    //    foreach (var get in _lstWhtsAppOrders.Select(x => x.ImageSynced))
                    //    {
                    //        string tail = get.Substring(get.LastIndexOf('/') + 1);
                    //        string head = get.Split('/')[0];


                    //        ImageUploadedCount = ImageUploadedCount + tail.ToInt32();//4 total number of images
                    //        SuccesfullImagesCount = SuccesfullImagesCount + head.ToInt32();
                    //    }

                    foreach (var getStatus in _lstWhtsAppOrders)
                    {
                        if (getStatus.Status == 3)//2
                        {
                            WhtsSucessOrderCount++;
                        }
                        if (getStatus.Status < 0)//2
                        {
                            WhtsImageFailedCount++;
                        }
                        if (getStatus.Status == 1)//2
                        {
                            WhtsImageUploadedCount++;
                        }

                    }

                WhtsImageUploadedCount = WhtsSucessOrderCount + WhtsImageUploadedCount;
                //foreach (var ImageSynced in _lstWhtsAppOrders.Where(x => x.Syncstatus == "Successful" || x.Syncstatus == "Failed").Select(x => x.ImageSynced))
                //{
                //    string tails = ImageSynced.Substring(ImageSynced.LastIndexOf('/') + 1);
                //    string heads = ImageSynced.Split('/')[0];
                //    ImageFailedCount = tails.ToInt32() - heads.ToInt32();

                //}

                //on popcall 'TotalOrderCount','ImageUploadedCount','ImageFailedCount','SucessOrderCount'
                WhtsTotalOrderCount = _lstWhtsAppOrders.Count;


                DGManageWtsStatus.ItemsSource = _lstWhtsAppOrders;
                chkALLWhtsApp.IsChecked = false;
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
            }
        }
        private void btnSearchOpenCloseForm_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                DateTime fromDtNormalOrd = (DateTime)dtFromOpenCloseForm.Value;
                DateTime toDtNormalOrd = (DateTime)dtToOpenCloseForm.Value;

                _lstReSyncForm = (new SyncStatusBusiness()).GetFormSyncStatusList(fromDtNormalOrd, toDtNormalOrd).OrderByDescending(x => x.SyncFormTransDate).ToList();
                if (_lstReSyncForm.Count <= 0)
                    MessageBox.Show("No records found!", "DigiPhoto", MessageBoxButton.OK, MessageBoxImage.Information);
                //else
                DGManageSyncStatusOpenCloseForm.ItemsSource = _lstReSyncForm;
                chkALLNormal.IsChecked = false;
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
            }
        }

        private void chkALLOpenCloseForm_Checked(object sender, RoutedEventArgs e)
        {
            try
            {
                _lstReSyncForm.ForEach(z => z.IsAvailable = true);
                DGManageSyncStatusOpenCloseForm.ItemsSource = _lstReSyncForm;
                DGManageSyncStatusOpenCloseForm.Items.Refresh();
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
            }
        }
        private void chkALLOpenCloseForm_Unchecked(object sender, RoutedEventArgs e)
        {
            try
            {
                _lstReSyncForm.ForEach(z => z.IsAvailable = false);
                DGManageSyncStatusOpenCloseForm.ItemsSource = _lstReSyncForm;
                DGManageSyncStatusOpenCloseForm.Items.Refresh();
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
            }
        }


        private void btnReSyncOpenCloseForm_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                StoreSubStoreDataBusniess storeObj = new StoreSubStoreDataBusniess();
                DigiPhoto.IMIX.Model.StoreInfo store = storeObj.GetStore();
                ReSync(_lstReSyncForm);
                _lstReSyncForm = (new SyncStatusBusiness()).GetFormSyncStatusList((DateTime)dtFromOpenCloseForm.Value, (DateTime)dtToOpenCloseForm.Value).OrderByDescending(x => x.SyncFormTransDate).ToList();
                DGManageSyncStatusOpenCloseForm.ItemsSource = _lstReSyncForm;
                DGManageSyncStatusOpenCloseForm.Items.Refresh();

            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
            }
        }

        private void btnChkConnectivity_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                StoreSubStoreDataBusniess storeObj = new StoreSubStoreDataBusniess();
                //bool value = storeObj.GetStore().IsOnline;
                #region Sync Online check - Ashirwad
                bool value = storeObj.GetSyncIsOnline(LoginUser.SubStoreId);
                #endregion
                if (value)
                    MessageBox.Show("You are online. Data is syncing on server", "DEI");
                else
                    MessageBox.Show("You are offline. Data is not syncing on server, please contact IT administrator", "DEI");


            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
            }
        }


        private void btnBackSyncEntity_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                ManageHome _objHome = new ManageHome();
                _objHome.Show();
                this.Close();
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
            }
        }
        public void FillMasterData()
        {
            Dictionary<int, string> dictionary = new Dictionary<int, string>();

            //List<string> my_list = new List<string>();
            dictionary.Add(0, "--Select--");
            dictionary.Add(12, "User");
            dictionary.Add(4, "Package");
            dictionary.Add(13, "Location");
            cmbEntity.ItemsSource = dictionary;
            cmbEntity.DisplayMemberPath = "Value";
            cmbEntity.SelectedValuePath = "Key";
            cmbEntity.SelectedIndex = 0;

        }
        private void btnSearchEntity_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (cmbEntity.SelectedIndex > 0)
                {
                    DateTime fromDtNormalOrd = (DateTime)dtFromEntity.Value;
                    DateTime toDtNormalOrd = (DateTime)dtToEntity.Value;
                    long ApplicationObjectID = Convert.ToInt64(cmbEntity.SelectedValue);

                    DataSet ds = (new SyncStatusBusiness()).GetMasterDataSyncStaus(fromDtNormalOrd, toDtNormalOrd, ApplicationObjectID);

                    var myEnumerable = ds.Tables[0].AsEnumerable();



                    if (ds.Tables.Count > 0)
                    {

                        if (cmbEntity.SelectedValue.ToString() == "4")
                        {
                            DGManageSyncStatusUser.Visibility = Visibility.Collapsed;
                            DGManageSyncStatusLocation.Visibility = Visibility.Collapsed;
                            DGManageSyncStatusPackage.Visibility = Visibility.Visible;

                            _lstReSyncEntity =
                        (from item in myEnumerable
                         select new MasterData
                         {
                             ProductName = item.Field<string>("DG_Orders_ProductType_Name"),
                             Desc = item.Field<string>("DG_Orders_ProductType_Desc"),
                             ProductCode = item.Field<string>("DG_Orders_ProductCode"),
                             SyncCode = item.Field<string>("SyncCode"),
                             Price = item.Field<string>("DG_Product_Pricing_ProductPrice"),
                             SyncStatus = item.Field<string>("SyncStatus"),
                             Action = item.Field<string>("ChangeAction"),
                             ChangeTrackingID = item.Field<Int64>("ChangeTrackingId")

                         }).ToList();
                            if (_lstReSyncEntity.Count() > 0)
                                DGManageSyncStatusPackage.ItemsSource = _lstReSyncEntity;
                            else
                                MessageBox.Show("No records found!", "DigiPhoto", MessageBoxButton.OK, MessageBoxImage.Information);
                        }
                        else if (cmbEntity.SelectedValue.ToString() == "12")
                        {
                            DGManageSyncStatusUser.Visibility = Visibility.Visible;
                            DGManageSyncStatusLocation.Visibility = Visibility.Collapsed;
                            DGManageSyncStatusPackage.Visibility = Visibility.Collapsed;


                            _lstReSyncEntity =
                        (from item in myEnumerable
                         select new MasterData
                         {

                             SyncCode = item.Field<string>("SyncCode"),
                             SyncStatus = item.Field<string>("SyncStatus"),
                             Action = item.Field<string>("ChangeAction"),
                             UserName = item.Field<string>("DG_User_Name"),
                             FirstName = item.Field<string>("DG_User_First_Name"),
                             LastName = item.Field<string>("DG_User_Last_Name"),
                             Email = item.Field<string>("DG_User_Email"),
                             ChangeTrackingID = item.Field<Int64>("ChangeTrackingId")


                         }).ToList();

                            if (_lstReSyncEntity.Count() > 0)
                                DGManageSyncStatusUser.ItemsSource = _lstReSyncEntity;
                            else
                                MessageBox.Show("No records found!", "DigiPhoto", MessageBoxButton.OK, MessageBoxImage.Information);


                        }
                        else if (cmbEntity.SelectedValue.ToString() == "13")
                        {
                            DGManageSyncStatusUser.Visibility = Visibility.Collapsed;
                            DGManageSyncStatusLocation.Visibility = Visibility.Visible;
                            DGManageSyncStatusPackage.Visibility = Visibility.Collapsed;


                            _lstReSyncEntity =
                       (from item in myEnumerable
                        select new MasterData
                        {

                            Desc = item.Field<string>("DG_SubStore_Description"),
                            SyncStatus = item.Field<string>("SyncStatus"),
                            Action = item.Field<string>("ChangeAction"),
                            SyncCode = item.Field<string>("SyncCode"),
                            SubStoreName = item.Field<string>("DG_SubStore_Name"),
                            SubStoreCode = item.Field<string>("DG_SubStore_Code"),
                            ChangeTrackingID = item.Field<Int64>("ChangeTrackingId")

                        }).ToList();

                            if (_lstReSyncEntity.Count() > 0)
                                DGManageSyncStatusLocation.ItemsSource = _lstReSyncEntity;
                            else
                                MessageBox.Show("No records found!", "DigiPhoto", MessageBoxButton.OK, MessageBoxImage.Information);
                        }

                    }
                    else
                    {
                        MessageBox.Show("No records found!", "DigiPhoto", MessageBoxButton.OK, MessageBoxImage.Information);
                    }
                }
                else
                {
                    MessageBox.Show("Please select data type", "DigiPhoto", MessageBoxButton.OK, MessageBoxImage.Information);
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
            }
        }




        private void chkALLEntity_Checked(object sender, RoutedEventArgs e)
        {
            try
            {
                _lstReSyncEntity.ForEach(z => z.IsAvailable = true);

                if (cmbEntity.SelectedValue.ToString() == "4")
                {
                    DGManageSyncStatusPackage.ItemsSource = _lstReSyncEntity;
                    DGManageSyncStatusPackage.Items.Refresh();
                }
                else if (cmbEntity.SelectedValue.ToString() == "12")
                {
                    DGManageSyncStatusUser.ItemsSource = _lstReSyncEntity;
                    DGManageSyncStatusUser.Items.Refresh();
                }
                else if (cmbEntity.SelectedValue.ToString() == "13")
                {
                    DGManageSyncStatusLocation.ItemsSource = _lstReSyncEntity;
                    DGManageSyncStatusLocation.Items.Refresh();
                }

            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
            }
        }

        private void chkALLEntity_Unchecked(object sender, RoutedEventArgs e)
        {
            try
            {
                _lstReSyncEntity.ForEach(z => z.IsAvailable = false);
                if (cmbEntity.SelectedValue.ToString() == "4")
                {
                    DGManageSyncStatusPackage.ItemsSource = _lstReSyncEntity;
                    DGManageSyncStatusPackage.Items.Refresh();
                }
                else if (cmbEntity.SelectedValue.ToString() == "12")
                {
                    DGManageSyncStatusUser.ItemsSource = _lstReSyncEntity;
                    DGManageSyncStatusUser.Items.Refresh();
                }
                else if (cmbEntity.SelectedValue.ToString() == "13")
                {
                    DGManageSyncStatusLocation.ItemsSource = _lstReSyncEntity;
                    DGManageSyncStatusLocation.Items.Refresh();
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
            }
        }

        private void btnReSyncEntity_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Boolean flag = true;
                StringBuilder SyncId = new StringBuilder();
                if (_lstReSyncEntity != null && _lstReSyncEntity.Count > 0)
                {
                    foreach (var item in _lstReSyncEntity)
                    {
                        if (item.IsAvailable == true)
                            SyncId.Append(item.ChangeTrackingID + ",");
                    }
                    if (SyncId.Length > 0)
                    {
                        flag = (new SyncStatusBusiness()).ReSync(SyncId.ToString().Substring(0, SyncId.Length - 1));
                        if (flag == true)
                            MessageBox.Show("Sync Re-initiated Successfully");
                    }
                }

            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
            }
        }

        private void btnWtsAppSearch_Click(object sender, RoutedEventArgs e)
        {
            GetWhtsAppResult();
        }

        private void chkALLWhtsApp_Checked(object sender, RoutedEventArgs e)
        {
            try
            {
                _lstWhtsAppOrders.ForEach(z => z.IsAvailable = true);
                DGManageWtsStatus.ItemsSource = _lstWhtsAppOrders;
                DGManageWtsStatus.Items.Refresh();
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
            }
        }

        private void chkALLWhtsApp_Unchecked(object sender, RoutedEventArgs e)
        {
            try
            {
                _lstWhtsAppOrders.ForEach(z => z.IsAvailable = false);
                DGManageWtsStatus.ItemsSource = _lstWhtsAppOrders;
                DGManageWtsStatus.Items.Refresh();
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
            }
        }

        private void btnReSyncWhtsImages_Click(object sender, RoutedEventArgs e)
        {
            //System.Windows.Forms.DialogResult result = System.Windows.Forms.MessageBox.Show("Do you want to re-send Images to same Whats App Number?", "Re-send Whats App Images",
            //System.Windows.Forms.MessageBoxButtons.YesNoCancel, System.Windows.Forms.MessageBoxIcon.Warning);
            //if (result == System.Windows.Forms.DialogResult.Yes)
            //{
            //    //code for Yes
            //}
            //else if (result == System.Windows.Forms.DialogResult.No)
            //{
            //    //code for No
            //}
            //else if (result == System.Windows.Forms.DialogResult.Cancel)
            //{
            //    //code for Cancel
            //}

            try
            {
               
                if (_lstWhtsAppOrders.Count <= 0)
                {
                    MessageBox.Show("No Record Found");
                }
                else
                {
                    System.Windows.Forms.DialogResult dialogResult = System.Windows.Forms.MessageBox.Show("Do you want to re-send Images to same Whats App Number?", "Re-send Whats App Images", System.Windows.Forms.MessageBoxButtons.YesNo);
                    if (dialogResult == System.Windows.Forms.DialogResult.Yes)
                    {
                        ReSyncWhatsAppImages(_lstWhtsAppOrders);
                        GetWhtsAppResult();
                    }
                    else if (dialogResult == System.Windows.Forms.DialogResult.No)
                    {
                        //do something else
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
            }
        }

        private void txtMobNum_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
              e.Handled = !IsTextAllowed(e.Text);
        }

        private static bool IsTextAllowed(string text)
        {
            System.Text.RegularExpressions.Regex regex = new System.Text.RegularExpressions.Regex("[^0-9.-]+"); //regex that matches disallowed text
            return !regex.IsMatch(text);
        }

        private void btnWtsAppstatistics_Click(object sender, RoutedEventArgs e)
        {
            GetWhtsAppResult();
            if (_lstWhtsAppOrders.Count > 0)
            {
                Whtsstatictics.SetParent(this);
                WhatsAppStatistics.SucessOrderCounts = WhtsSucessOrderCount;
                WhatsAppStatistics.ImageFailedCounts = WhtsImageFailedCount;
                WhatsAppStatistics.ImageUploadedCounts = WhtsImageUploadedCount;
                WhatsAppStatistics.TotalOrderCounts = WhtsTotalOrderCount;
                //StatisticsControl.ShowPanHandlerDialog(SucessOrderCount, ImageFailedCount, ImageUploadedCount, TotalOrderCount);
                WhatsAppStatistics.ShowPanHandlerDialog();
            }
        }
    }

    public class MasterData
    {


        public string ProductName { get; set; }
        public string Desc { get; set; }
        public string ProductCode { get; set; }
        public string SyncCode { get; set; }
        public string Price { get; set; }
        public string SyncStatus { get; set; }
        public string Action { get; set; }
        public string UserName { get; set; }
        public string FirstName { get; set; }

        public string LastName { get; set; }
        public string Email { get; set; }
        public string SubStoreName { get; set; }
        public string SubStoreCode { get; set; }
        public Int64 ChangeTrackingID { get; set; }
        public bool IsAvailable { get; set; }

    }
}

