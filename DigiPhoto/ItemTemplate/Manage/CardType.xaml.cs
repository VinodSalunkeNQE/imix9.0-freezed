﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using DigiPhoto.DataLayer.Model;
using DigiPhoto.DataLayer;
using DigiPhoto.Common;
using DigiPhoto.Manage;
using DigiAuditLogger;
using Microsoft.Win32;
using System.IO;
using Excel;
using System.Data;
using System.Collections;
using DigiPhoto.IMIX.Business;
using DigiPhoto.IMIX.Model;
using DigiPhoto.Utility.Repository.ValueType;


namespace DigiPhoto
{
    /// <summary>
    /// Interaction logic for ManageProduct.xaml
    /// </summary>
    public partial class CardType : Window
    {
        #region Declaration (CardType)
        int cardId = 0;
        /// <summary>
        /// The _parent
        /// </summary>
        private UIElement _parent;

        #endregion

        /// <summary>
        /// Sets the parent.
        /// </summary>
        /// <param name="parent">The parent.</param>
        public void SetParent(UIElement parent)
        {
            _parent = (UIElement)parent;

        }
        #region Constructor
        /// <summary>
        /// Initializes a new instance of the <see cref="ManageProduct"/> class.
        /// </summary>
        public CardType()
        {
            try
            {
                InitializeComponent();
                LoadComboValues();
                GetCardTypeList();

                txbUserName.Text = LoginUser.UserName;
                txbStoreName.Text = LoginUser.StoreName;
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }
        #endregion


        #region Events (Card Types)

        /// <summary>
        /// Handles the Click event of the btnLogout control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void btnLogout_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                AuditLog.AddUserLog(Common.LoginUser.UserId, 39, "Logged out at ");
                Login _objLogin = new Login();
                _objLogin.Show();
                this.Close();
            }
            catch (Exception ex)
            {

                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
            finally
            {
                MemoryManagement.FlushMemory();
            }
        }

        /// <summary>
        /// Gets the product typeby product unique identifier.
        /// </summary>
        /// <returns></returns>
        private iMixImageCardTypeInfo GetCardTypebyCardId()
        {
            CardBusiness _objCardBusiness = new CardBusiness();
            return _objCardBusiness.GetCardTypeList(cardId);
            //DigiPhotoDataServices _objDataService = new DigiPhotoDataServices();
            //return _objDataService.GetCardTypeList().Where(t => t.IMIXImageCardTypeId == cardId).First();
        }

        //private iMixImageCardType GetCardTypebyCardId()
        //{
        //    DigiPhotoDataServices _objDataService = new DigiPhotoDataServices();
        //    return _objDataService.GetCardTypeList().Where(t => t.IMIXImageCardTypeId == cardId).First();
        //}

        /// <summary>
        /// Handles the Click event of the btnCancel control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void btnAddNew_Click(object sender, RoutedEventArgs e)
        {
            ctrlEditAddCard.cardId = 0;

            ctrlEditAddCard.Visibility = System.Windows.Visibility.Visible;
            ctrlEditAddCard.SetParent(this);
        }
        /// <summary>
        /// Handles the Click event of the btnBack control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void btnBack_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                ManageHome _objHome = new ManageHome();
                _objHome.Show();
                this.Close();
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }
        /// <summary>
        /// Handles the Click event of the btnEdit control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void btnEdit_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Button btnSender = (Button)sender;
                cardId = btnSender.Tag.ToInt32();
                iMixImageCardTypeInfo _objcardType = GetCardTypebyCardId();
                //iMixImageCardType _objcardType = GetCardTypebyCardId();
                if (_objcardType != null)
                {

                    ctrlEditAddCard.txtName.Text = _objcardType.Name;
                    ctrlEditAddCard.txtCardDesc.Text = _objcardType.Description;
                    ctrlEditAddCard.txtCardIdentificationDigit.Text = _objcardType.CardIdentificationDigit;
                    ctrlEditAddCard.chkIsActive.IsChecked = _objcardType.IsActive.ToBoolean();
                    ctrlEditAddCard.cmbCardFormatType.SelectedValue = _objcardType.ImageIdentificationType;
                    ctrlEditAddCard.txtMaxImage.Text = _objcardType.MaxImages.ToString();
                    ctrlEditAddCard.chkIsPrePaid.IsChecked = _objcardType.IsPrepaid;
                    ctrlEditAddCard.cmbPackage.SelectedValue = _objcardType.PackageId;
                    ctrlEditAddCard.chkIsWaterMark.IsChecked = _objcardType.IsWaterMark.ToBoolean();
                    ctrlEditAddCard.cardId = cardId;
                    ctrlEditAddCard.Visibility = System.Windows.Visibility.Visible;
                    ctrlEditAddCard.SetParent(this);

                }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }
        /// <summary>
        /// Handles the Click event of the btnDelete control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void btnDelete_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Button btnSender = (Button)sender;
                cardId = btnSender.Tag.ToInt32();
                MessageBoxResult response = MessageBox.Show("Alert: Are you sure you want to change the status of Card?", "Temper Alert", MessageBoxButton.YesNo, MessageBoxImage.Warning);
                if (response == MessageBoxResult.Yes)
                {
                    CardBusiness carBiz = new CardBusiness();
                    string cardname = carBiz.GetCardTypeList(cardId).Name;
                    if (carBiz.ChangeCardStatus(cardId))
                    {
                        AuditLog.AddUserLog(Common.LoginUser.UserId, (int)FrameworkHelper.ActionType.DeleteProduct, "Card (" + cardname + ") status has been changed on ");
                        // MessageBox.Show("Status changed successfully");
                        GetCardTypeList();
                        cardId = 0;
                    }
                    else
                    {
                        MessageBox.Show("Record is in use");
                    }
                }

            }
            catch (Exception ex)
            {

                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }
        #endregion

        #region Common Method (Manage Product)
        /// <summary>
        /// Isvalidates this instance.
        /// </summary>
        /// <returns></returns>


        /// <summary>
        /// Gets the Card type list.
        /// </summary>
        private void LoadComboValues()
        {
            Dictionary<string, int> CardTypeList = new Dictionary<string, int>();
            CardTypeList.Add("--Select--", 0);
            CardTypeList.Add("QRCode", 401);
            CardTypeList.Add("Barcode", 402);
            CardTypeList.Add("QRCode+Barcode", 405);
        }
        public void GetCardTypeList()
        {
            CardBusiness _objCardBusiness = new CardBusiness();
            grdCardType.ItemsSource = _objCardBusiness.GetCardTypeListview().SkipWhile(x => x.IMIXImageCardTypeId == 3);
        }
        #endregion
        #region Events(Manage Package)
        /// <summary>
        /// Handles the Click event of the btnBackToHome control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void btnBackToHome_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                ManageHome _objHome = new ManageHome();
                _objHome.Show();
                this.Close();
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }

        }
        #endregion
        #region Custom Class
        public class CardTypeList
        {
            public int IMIXImageCardTypeId { get; set; }
            public string Name { get; set; }
            public string CardIdentificationDigit { get; set; }
            public string ImageIdentificationType { get; set; }
            public string Status { get; set; }
            public int MaxImages { get; set; }
            public string Description { get; set; }
            public DateTime CreatedOn { get; set; }
        }
        #endregion
    }
}
