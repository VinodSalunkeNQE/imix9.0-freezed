﻿using DigiPhoto.DataLayer;
using DigiPhoto.DataLayer.Model;
using DigiPhoto.IMIX.Business;
using DigiPhoto.IMIX.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using DigiPhoto.Utility.Repository.ValueType;
using FrameworkHelper;
using System.Data.OleDb;
using System.Data;
using DigiPhoto.Common;
using DigiAuditLogger;

namespace DigiPhoto.Manage
{
    /// <summary>
    /// Interaction logic for ManageCurrencyProfile.xaml
    /// </summary>
    public partial class ManageCurrencyProfile : Window
    {
        string _filedatasource = string.Empty;
        string packagefilename = string.Empty;
        DigiPhoto.IMIX.Business.CurrencyExchangeBussiness _objCurrencyBusiness = null;
        System.ComponentModel.BackgroundWorker BackupWorker; 
        BusyWindow bs = new BusyWindow();

        DataTable _dtExcel;
        DataTable _dt;
        public ManageCurrencyProfile()
        {
            
            InitializeComponent();
            GetProfileData();
            CtrlProfileRate.SetParent(modelparent);
            txbUserName.Text = LoginUser.UserName;
            txbStoreName.Text = LoginUser.StoreName;
        }

        
        private void btnBrowse_Click(object sender, RoutedEventArgs e)
        {
            //try
            //{
            //    Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog();

            //    // Set filter for file extension and default file extension 
            //    dlg.DefaultExt = ".xls";
            //    dlg.Filter = "Excel Files(.xls)|*.xls| Excel Files(.xlsx)|*.xlsx| Excel Files(*.xlsm)|*.xlsm";

            //    // Display OpenFileDialog by calling ShowDialog method 
            //    Nullable<bool> result = dlg.ShowDialog();

            //    // Get the selected file name and display in a TextBox 
            //    if (result == true)
            //    {
            //        // Open document 
            //        string filename = dlg.FileName;
            //        txtFileName.Text = filename;
            //    }

            //}
            //catch (Exception ex)
            //{
            //    string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
            //    ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            //}
        }
        void GetProfileData()
        {
            _objCurrencyBusiness = new DigiPhoto.IMIX.Business.CurrencyExchangeBussiness();
            List<CurrencyExchangeinfo> _profileList = new List<CurrencyExchangeinfo>();
            _profileList = _objCurrencyBusiness.GetCurrencyProfileList();
            dgProfile.ItemsSource = _profileList;

        }

        private void dgProfile_LoadingRow(object sender, DataGridRowEventArgs e)
        {
            DataGridRow row = e.Row;
            DataRowView rView = row.Item as DataRowView;

            if ((((DigiPhoto.IMIX.Model.CurrencyExchangeinfo)(row.Item)).IsCurrent)==true)
            {
                //e.Row.Style = null;
                e.Row.Background = Brushes.LightGreen;
            }
        }

        private void btnLogout_Click(object sender, RoutedEventArgs e)
        {
            AuditLog.AddUserLog(Common.LoginUser.UserId, (int)FrameworkHelper.ActionType.Logout, "Logged out at");
            Login _objLogin = new Login();
            _objLogin.Show();
            this.Close();
        }
        private void btnBack_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                ManageHome _objHome = new ManageHome();
                _objHome.Show();
                this.Close();
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }

        private void btndetail_Click(object sender, RoutedEventArgs e)
        {
            CtrlProfileRate.ProfileID = ((Button)sender).Tag.ToInt64();
            CtrlProfileRate.ShowHandlerDialog("Rate detail");
        }

        private void Import(string filedatasource)
        {
            if (filedatasource != string.Empty)
            {
                try
                {

                    String constr = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + filedatasource + ";Extended Properties='Excel 12.0 XML;HDR=YES;';";
                    OleDbConnection excelConnection = new OleDbConnection(constr);
                    OleDbCommand command = new OleDbCommand();
                    command.CommandType = System.Data.CommandType.Text;
                    command.Connection = excelConnection;
                    OleDbDataAdapter dAdapter = new OleDbDataAdapter(command);
                    _dtExcel = new DataTable("Package");
                    excelConnection.Open();

                    DataTable dtExcelSheets = excelConnection.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
                    int rowCount = 0;
                    for (rowCount = 0; rowCount < dtExcelSheets.Rows.Count; rowCount++)
                    {
                        if (dtExcelSheets.Rows[rowCount]["Table_Name"].ToString().EndsWith("$"))
                        {
                            break;
                        }
                    }

                    if (rowCount >= dtExcelSheets.Rows.Count)
                    {
                        throw new Exception("There is no valid Excel Sheet");
                    }

                    string getExcelSheetName = dtExcelSheets.Rows[rowCount]["Table_Name"].ToString();

                    command.CommandText = "SELECT * FROM [" + getExcelSheetName + "]";
                    dAdapter.SelectCommand = command;
                    dAdapter.Fill(_dtExcel);
                    excelConnection.Close();
                    //here i will chekc all the columns

                    if (IsAllColumnExist(_dtExcel))
                    {


                        System.Data.DataColumn newColumn = new System.Data.DataColumn("SyncCode", typeof(System.String));
                        _dtExcel.Columns.Add(newColumn);

                        string SyncCode = string.Empty;
                        string profilename = "";
                        string countryname = "";
                        string storename = "";
                        //1,11,13
                        foreach (DataRow dr in _dtExcel.Rows)
                        {
                            if (!String.IsNullOrWhiteSpace(Convert.ToString(dr[0])))
                            {
                                if (Convert.ToString(dr[0]) != profilename && Convert.ToString(dr[8]) != countryname && Convert.ToString(dr[7]) != storename)
                                {
                                    profilename = Convert.ToString(dr[0]);
                                    countryname = Convert.ToString(dr[8]);
                                    storename = Convert.ToString(dr[7]);

                                    //profilename countryname storename 1,11,13

                                    SyncCode = CommonUtility.GetUniqueSynccode(Convert.ToInt32(ApplicationObjectEnum.Product).ToString().PadLeft(2, '0'), LoginUser.countrycode, LoginUser.Storecode, "00");
                                }
                            }
                            dr["SyncCode"] = SyncCode;
                        }
                        _dt = _dtExcel.Rows.Cast<DataRow>().Where(row => !row.ItemArray.All(field => field is System.DBNull || string.Compare((field as string), string.Empty) == 0)).CopyToDataTable();

                        DataView view = new System.Data.DataView(_dt);
                        _dt = view.ToTable("Selected", false, "ProfileName", "StartDate", "EndDate", "CurrencyRate", "CurrencyName", "CurrencyCode",  "VenueName", "CountryName", "PublishedOn", "SyncCode");

                        bs.Show();
                        BackupWorker.RunWorkerAsync();
                        //GetProfileData();
                    }
                    else
                    {
                        throw new Exception("Please upload a valid Excel Sheet");
                    }
                }

                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message.ToString());
                }
            }
        }

        private void btnUpload_Click(object sender, RoutedEventArgs e)
        {
            BackupWorker = new System.ComponentModel.BackgroundWorker();
            BackupWorker.DoWork += new System.ComponentModel.DoWorkEventHandler(BackupWorker_DoWork);
            BackupWorker.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(BackupWorker_RunWorkerCompleted);
            System.Windows.Forms.OpenFileDialog fdlg = new System.Windows.Forms.OpenFileDialog();
            fdlg.Title = "Select file";
            fdlg.Filter = "Excel Sheet(*.xls)|*.xls|Excel Sheet(*.xlsx)|*.xlsx|All Files(*.*)|*.*";
            fdlg.FilterIndex = 1;
            fdlg.RestoreDirectory = true;
            if (fdlg.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                _filedatasource = fdlg.FileName;
                string[] filename = fdlg.FileName.Split('\\');
                packagefilename = filename.Last();
                string[] fileext = packagefilename.Split('.');
                if (fileext[1] == "xls" || fileext[1] == "xlsx")
                {
                    Import(_filedatasource);
                    System.Windows.Forms.Application.DoEvents();
                }
                else
                {
                    MessageBox.Show("Please select a valid file(.xls/.xlsx)");
                }

            }
        }

        private void BackupWorker_DoWork(object Sender, System.ComponentModel.DoWorkEventArgs e)
        {
            bool result = (new CurrencyExchangeBussiness()).UploadCurrencyData(LoginUser.UserId, DateTime.Now, _dt);
            if (result)
            {
                MessageBox.Show("Currency imported successfully.");                
            }
            else
                MessageBox.Show("There is some error while saving Currency");
        }
        private void BackupWorker_RunWorkerCompleted(object Sender, System.ComponentModel.RunWorkerCompletedEventArgs e)
        {
            bs.Hide();
            GetProfileData();
        }
        private bool IsAllColumnExist(DataTable tableNameToCheck)
        {
            List<string> columnsNames = new List<string>();

            //columnsNames.Add("CurrencyProfileID");
            columnsNames.Add("ProfileName");
            columnsNames.Add("StartDate");
            columnsNames.Add("EndDate");
            //columnsNames.Add("CurrencyProfileRateID");
            columnsNames.Add("CurrencyRate");
           // columnsNames.Add("CurrencyMasterID");
            columnsNames.Add("CurrencyName");
            columnsNames.Add("CurrencyCode");
            columnsNames.Add("PublishedOn");
            //columnsNames.Add("VenueCode");
            columnsNames.Add("VenueName");
           // columnsNames.Add("CountryCode");
            columnsNames.Add("CountryName");
            bool iscolumnExist = true;
            try
            {
                if (null != tableNameToCheck && tableNameToCheck.Columns != null)
                {
                    foreach (string columnName in columnsNames)
                    {
                        if (!tableNameToCheck.Columns.Contains(columnName))
                        {
                            iscolumnExist = false;
                            break;
                        }
                    }
                }
                else
                {
                    iscolumnExist = false;
                }
            }
            catch (Exception ex)
            {

            }
            return iscolumnExist;
        }

        //private void dgProfile_LoadingRow(object sender, DataGridRowEventArgs e)
        //{

        //}
    }
}
