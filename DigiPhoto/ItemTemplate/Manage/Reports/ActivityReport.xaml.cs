﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using DigiPhoto.DataLayer;
using DigiPhoto.Common;
using CrystalDecisions.Shared;
using CrystalDecisions.CrystalReports.Engine;
using System.Windows.Xps.Packaging;
using System.IO;
using DigiPhoto.DataLayer.Model;
using System.Data;
using System.Collections;
using System.Reflection;
using System.Windows.Controls.Primitives;
using DigiPhoto.IMIX.Business;
using DigiPhoto.IMIX.Model;
using DigiPhoto.Utility.Repository.ValueType;
using Microsoft.Win32;
using System.Data.SqlClient;
using System.ComponentModel;
using System.Diagnostics;
using ClosedXML.Excel;
using iTextSharp.text;
using iTextSharp.text.pdf;
using DigiPhoto.ExtensionMethods;

namespace DigiPhoto.Manage.Reports
{
    /// <summary>
    /// Interaction logic for ActivityReport.xaml
    /// </summary>
    public partial class ActivityReport : Window
    {
        ReportBusiness reportBusiness = null;
        Dictionary<string, string> lstPhotographerList;
        Dictionary<string, string> lstLocationList;
        Dictionary<string, string> lstReportTypeList;
        Dictionary<string, string> lstsubstoreList;
        Dictionary<string, string> lststoreList;
        bool sendMailFlag = false;
        static DataTable ReportSource { get; set; }
        List<ReportTypeDetails> ReportTypeDetail
        {
            get;
            set;
        }
        /// <summary>
        /// Initializes a new instance of the <see cref="ActivityReport" /> class.
        /// </summary>
        public ActivityReport()
        {
            InitializeComponent();
            FillUserCombo();
            FillLocation();
            FillSubStoreCombo();
            txbUserName.Text = LoginUser.UserName;
            txbStoreName.Text = LoginUser.StoreName;
            FillReportTypeCombo();
            ActivityReportviewer.ShowCopyButton = false;
            ActivityReportviewer.ShowLogo = false;
            ActivityReportviewer.ShowToggleSidePanelButton = false;
            ActivityReportviewer.ShowOpenFileButton = false;
            ActivityReportviewer.ShowExportButton = false;
            ActivityReportviewer.ShowPrintButton = false;
            ActivityReportviewer.ShowStatusbar = true;
            ActivityReportviewer.ToggleSidePanel = SAPBusinessObjects.WPF.Viewer.Constants.SidePanelKind.None;
            dtFrom.Value = DateTime.Now.Date.Add(new TimeSpan(6, 0, 0));
            dtTo.Value = DateTime.Now.Date.Add(new TimeSpan(23, 0, 0));
            dtFrom1.Value = DateTime.Now.Date.Add(new TimeSpan(6, 0, 0));
            dtTo1.Value = DateTime.Now.Date.Add(new TimeSpan(23, 0, 0));
            ActivityReportviewer.Owner = this;
            sendMailFlag = false;
            MailPopUp.SetParent(emailPopUp);
            FillProductCombo();
            //gridReport.AutoGeneratingColumn += DataGridAutoGeneratingColumnEventArgs;

        }


        /// <summary>
        /// Fills the report type combo.
        /// </summary>
        private void FillReportTypeCombo()
        {
            ReportTypeDetail = new ConfigBusiness().GetReport();
            cmbReportType.Items.Clear();
            //lstReportTypeList = new Dictionary<string, string>();
            //lstReportTypeList.Add("Activity Reports", "0");
            //lstReportTypeList.Add("Production Summary Report", "1");
            //lstReportTypeList.Add("Operators Performance Report", "2");
            //lstReportTypeList.Add("Taking Reports", "3");
            //lstReportTypeList.Add("Operation Audit Trail", "4");
            //lstReportTypeList.Add("Photographer Performance", "5");
            //lstReportTypeList.Add("Site Performance Report", "6");
            //lstReportTypeList.Add("Financial Audit Report", "7");
            //lstReportTypeList.Add("Order Detailed Report", "8");
            //lstReportTypeList.Add("Printing Report", "9");
            cmbReportType.ItemsSource = RobotImageLoader.GetReports();
            cmbReportType.SelectedValue = "0";
        }
        /// <summary>
        /// Fills the user combo.
        /// </summary>
        private void FillUserCombo()
        {
            try
            {
                UserBusiness useBiz = new UserBusiness();
                cmbPhotographer.ItemsSource = useBiz.GetUserDetailsByUserId(0);
                cmbPhotographer.SelectedValue = "0";
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }
        /// <summary>
        /// Fills the location.
        /// </summary>
        private void FillLocation()
        {
            lstLocationList = new Dictionary<string, string>();
            lstLocationList.Add("--Select--", "0");
            try
            {
                LocationBusniess locBiz = new LocationBusniess();
                lstLocationList = locBiz.GetLocationList(Common.LoginUser.StoreId).ToDictionary(o => o.DG_Location_pkey.ToString(), o => o.DG_Location_Name);
                cmbLocation.ItemsSource = lstLocationList;
                cmbLocation.SelectedValue = "0";
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }

        /// <summary>
        /// Handles the Click event of the btnback control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void btnback_Click(object sender, RoutedEventArgs e)
        {
            ManageHome _objhome = new ManageHome();
            _objhome.Show();
            this.Close();
        }

        /// <summary>
        /// Handles the Click event of the btnLogout control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void btnLogout_Click(object sender, RoutedEventArgs e)
        {

            DigiAuditLogger.AuditLog.AddUserLog(Common.LoginUser.UserId, (int)FrameworkHelper.ActionType.Logout, "Logged out at ");
            Login _objLogin = new Login();
            _objLogin.Show();
            this.Close();
        }

        /// <summary>
        /// Datas the table from attribute enumerable.
        /// </summary>
        /// <param name="ien">The ien.</param>
        /// <returns></returns>
        private DataTable DataTableFromIEnumerable(IEnumerable ien)
        {
            DataTable dt = new DataTable();
            foreach (object obj in ien)
            {
                Type t = obj.GetType();
                PropertyInfo[] pis = t.GetProperties();
                if (dt.Columns.Count == 0)
                {
                    foreach (PropertyInfo pi in pis)
                    {

                        Type pt = pi.PropertyType;
                        if (pt.IsGenericType && pt.GetGenericTypeDefinition() == typeof(Nullable<>))
                            pt = Nullable.GetUnderlyingType(pt);
                        dt.Columns.Add(pi.Name, pt);
                    }
                }
                DataRow dr = dt.NewRow();
                foreach (PropertyInfo pi in pis)
                {
                    object value = pi.GetValue(obj, null);
                    dr[pi.Name] = value;
                }
                dt.Rows.Add(dr.ItemArray);
            }
            return dt;
        }

        /// <summary>
        /// Fills the sub store combo.
        /// </summary>
        private void FillSubStoreCombo()
        {
            try
            {
                Dictionary<string, string> SelectDict = new Dictionary<string, string>();
                SelectDict.Add("--Select--", "--Select--");
                StoreSubStoreDataBusniess stoBiz = new StoreSubStoreDataBusniess();
                cmbsubstore.ItemsSource = stoBiz.GetSubstoreDataDir(SelectDict);
                cmbsubstore.SelectedValue = "--Select--";
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }

        ReportDocument report = new ReportDocument();
        string filename = string.Empty;
        bool isIPPrintTrackingRpt = false;

        /// <summary>
        /// Handles the Click event of the btnSearch control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs" /> instance containing the event data.</param>
        /// 
        private void btnSearch_Click(object sender, RoutedEventArgs e)
        {
            BusyWindow bs = new BusyWindow();
            try
            {
                if (VenueLavel.IsChecked == true)
                {
                    //cmbsubstore.Visibility = Visibility.Collapsed;
                    //txtsubstore.Visibility = Visibility.Collapsed;

                    ReportSource = null;
                    isIPPrintTrackingRpt = false;
                    bs.Show();
                    bool IsFromDate = false;
                    bool IsToDate = false;

                    DateTime FromDt1 = DateTime.Now;
                    DateTime ToDt1 = DateTime.Now;

                    if (dtFrom.Text != null)
                    {
                        IsFromDate = true;
                        FromDt = (DateTime)dtFrom.Value;
                    }
                    if (dtTo.Text != null)
                    {
                        IsToDate = true;
                        ToDt = (DateTime)dtTo.Value;
                    }
                    if (dtFrom1.Text != null)
                    {
                        IsFromDate = true;
                        FromDt1 = (DateTime)dtFrom1.Value;
                    }
                    if (dtTo1.Text != null)
                    {
                        IsToDate = true;
                        ToDt1 = (DateTime)dtTo1.Value;
                    }
                    string ApplicationPath = AppDomain.CurrentDomain.BaseDirectory;
                    ReportProductSummaryVenue(FromDt, ToDt, ApplicationPath);
                    //SearchButtonCode();
                    if (!isIPPrintTrackingRpt)
                    {
                        ActivityReportviewer.ViewerCore.ReportSource = report;
                        //Reports view. Report window is too small, need full screen option. Mahesh Patel Report Zoom
                        ActivityReportviewer.ViewerCore.Zoom(138);
                        ActivityReportviewer.Focusable = true;
                        ActivityReportviewer.Focus();
                    }
                    bs.Hide();
                    sendMailFlag = true;
                }
                else
                {
                    //VenueLavel.Visibility = Visibility.Visible;
                    //txtVenueTxt.Visibility = Visibility.Visible;
                    ReportSource = null;
                    isIPPrintTrackingRpt = false;
                    bs.Show();
                    SearchButtonCode();
                    if (!isIPPrintTrackingRpt)
                    {

                        ActivityReportviewer.ViewerCore.ReportSource = report;
                        ActivityReportviewer.Focusable = true;
                        ActivityReportviewer.Focus();
                    }

                    bs.Hide();
                    sendMailFlag = true;
                }
            }
            catch (Exception ex)
            {
                bs.Hide();
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }

        }
        DateTime FromDt = DateTime.Now;
        DateTime ToDt = DateTime.Now;
        private void SearchButtonCode()
        {
            bool IsFromDate = false;
            bool IsToDate = false;

            DateTime FromDt1 = DateTime.Now;
            DateTime ToDt1 = DateTime.Now;

            if (dtFrom.Text != null)
            {
                IsFromDate = true;
                FromDt = (DateTime)dtFrom.Value;
            }
            if (dtTo.Text != null)
            {
                IsToDate = true;
                ToDt = (DateTime)dtTo.Value;
            }
            if (dtFrom1.Text != null)
            {
                IsFromDate = true;
                FromDt1 = (DateTime)dtFrom1.Value;
            }
            if (dtTo1.Text != null)
            {
                IsToDate = true;
                ToDt1 = (DateTime)dtTo1.Value;
            }

            //DigiPhotoDataServices objDigiPhotoDataServices = new DigiPhotoDataServices();

            string ApplicationPath = AppDomain.CurrentDomain.BaseDirectory;

            //mahesh Start
            //if (VenueLavel.IsChecked == true)
            //{
            //    ReportProductSummaryVenue(FromDt, ToDt, ApplicationPath);
            //}
            //mahesh end
            if (cmbReportType.SelectedValue.ToString() == "0")
            {
                ReportActivity(IsFromDate, IsToDate, FromDt, ToDt, ApplicationPath);
            }
            else if (cmbReportType.SelectedValue.ToString() == "1")
            {
                ReportProductSummary(FromDt, ToDt, ApplicationPath);
            }
            else if (cmbReportType.SelectedValue.ToString() == "2")
            {
                ReportOperatorPerformance(FromDt, ToDt, FromDt1, ToDt1, ApplicationPath);
            }
            else if (cmbReportType.SelectedValue.ToString() == "3")
            {

                //ReportTaking(IsFromDate, IsToDate, FromDt, ToDt, objDigiPhotoDataServices, ApplicationPath);
                ReportTaking(IsFromDate, IsToDate, FromDt, ToDt, ApplicationPath);
            }
            else if (cmbReportType.SelectedValue.ToString() == "4")
            {
                ReportOperationAudit(FromDt, ToDt, ApplicationPath);
            }
            else if (cmbReportType.SelectedValue.ToString() == "6")
            {
                ReportLocationPerformance(FromDt, ToDt, FromDt1, ToDt1, ApplicationPath);
            }
            else if (cmbReportType.SelectedValue.ToString() == "5")
            {
                ReportPhotoGrapherPerformance(FromDt, ToDt, FromDt1, ToDt1, ApplicationPath);
            }
            else if (cmbReportType.SelectedValue.ToString() == "7")
            {
                ReportFinancialaudit(FromDt, ToDt, ApplicationPath);
            }
            else if (cmbReportType.SelectedValue.ToString() == "8")
            {
                ReportOrderDiscount(FromDt, ToDt, FromDt1, ToDt1, ApplicationPath);
            }
            else if (cmbReportType.SelectedValue.ToString() == "8")
            {
                ReportsOrderDiscount(FromDt, ToDt, FromDt1, ToDt1, ApplicationPath);
            }

            #region
            //        else if (cmbReportType.SelectedValue.ToString() == "9")
            //        {
            //            DateTime FromDate1 = FromDt;
            //            DateTime Todate1 = ToDt;

            //            report.Load(ApplicationPath + "\\Reports\\PrintLogReport.rpt");

            //            List<GetPrintedProduct_Result> result = objDigiPhotoDataServices.GetDataForPrintingLog(FromDate1,
            //                Todate1);
            //            //List<PrintSummary_Result> result2 = objDigiPhotoDataServices.GetDataForPrintingSummary(FromDate1,
            //            //    Todate1);
            //            if (result.Count != 0)
            //            {
            //                DataTable dt = DataTableFromIEnumerable(result);
            //               // DataTable dt2 = DataTableFromIEnumerable(result2);
            //                report.SetDataSource(dt);

            //                //   report.OpenSubreport(ApplicationPath + "\\Reports\\Summary.rpt").SetDataSource(dt2);

            //                //report.Subreports[0].SetDataSource(dt2);
            //                report.Subreports[0].SetDataSource(dt);
            //                filename = "Printing Report";
            //            }
            //            else
            //            {
            //                report.Load(ApplicationPath + "\\Reports\\Temp.rpt");
            //            }
            //        }

            //        ActivityReportviewer.ViewerCore.ReportSource = report;
            //        ActivityReportviewer.Focusable = true;
            //        ActivityReportviewer.Focus();
            //    }
            //    catch (Exception ex)
            //    {
            //        string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
            //        ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            //    }
            //    finally
            //    {

            //       // report.Close();
            //       // report.Dispose();
            //    }
            //}

            //Microsoft.Win32.SaveFileDialog dlg = new Microsoft.Win32.SaveFileDialog();

            #endregion
            else if (cmbReportType.SelectedValue.ToString() == "9")
            {
                ReportPrintLog(FromDt, ToDt, ApplicationPath);
            }
            else if (cmbReportType.SelectedValue.ToString() == "10")
            {
                ReportStorePaymentSummary(FromDt, ToDt, ApplicationPath);
            }
            else if (cmbReportType.SelectedValue.ToString() == "11")
            {
                ReportIPPrintTracking(FromDt, ToDt, ApplicationPath);
            }
            else if (cmbReportType.SelectedValue.ToString() == "12")
            {
                ReportIPContentTracking(FromDt, ToDt, ApplicationPath);
            }
        }
        DataTable dtDataToExport;
        private void ReportIPPrintTracking(DateTime FromDt, DateTime ToDt, string ApplicationPath)
        {
            try
            {
                SourceTable = new DataTable();
                DateTime FromDate1 = FromDt;
                DateTime Todate1 = ToDt;
                bool Comparasion = false;
                Comparasion = (bool)chkChecked.IsChecked;
                ReportBusiness repBiz = new ReportBusiness();
                DataSet result =
                 repBiz.GetDataForIPPrintTracking(FromDt, ToDt, ((KeyValuePair<string, string>)(((Selector)(cmbsubstore)).SelectedItem)).Key.ToInt32(), cmbPackages.SelectedValue.ToInt32());
                TaxBusiness taxBussiness = new TaxBusiness();
                //List<TaxDetailInfo> taxDetails = taxBussiness.GetReportTaxDetail(FromDt, ToDt, ((KeyValuePair<string, string>)(((Selector)(cmbsubstore)).SelectedItem)).Key.ToInt32());
                if (result.Tables.Count > 0 && result.Tables[0].Rows.Count > 0)
                {
                    string columnToLeave = "Order Date,Order No,Site,Package,Sell Price,Payment Mode,DG_Orders_LineItems_pkey";
                    int index = 0;
                    result.Tables[0].Columns.Remove("DG_Orders_LineItems_pkey");
                    DataRow dr = result.Tables[0].NewRow();
                    dr[5] = "Total";
                    foreach (DataColumn column in result.Tables[0].Columns)
                    {
                        int i = result.Tables[0].Columns.Count;
                        if (!columnToLeave.Contains(column.ColumnName))
                        {
                            dr[index] = result.Tables[0].Compute("sum([" + column.ColumnName + "])", "");
                            if (column.ColumnName.ToLower() != "total")
                                column.ColumnName = column.ColumnName + "(No of Images)";

                        }
                        index++;
                    }
                    result.Tables[0].Rows.Add(dr);
                    dtDataToExport = new DataTable();
                    dtDataToExport = result.Tables[0];
                    ReportSource = dtDataToExport;
                    gridReport.ItemsSource = dtDataToExport.DefaultView;

                    gridReport.Visibility = Visibility.Visible;
                }
                else
                {
                    gridReport.ItemsSource = null;
                    MessageBox.Show("No record found");
                    gridReport.Visibility = Visibility.Collapsed;
                    dtDataToExport = null;
                    //report.Load(ApplicationPath + "\\Reports\\Temp.rpt");
                }
                isIPPrintTrackingRpt = true;
                filename = "IP Print Tracking Report";
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }

        private void ReportIPContentTracking(DateTime FromDt, DateTime ToDt, string ApplicationPath)
        {
            try
            {
                SourceTable = new DataTable();
                DateTime FromDate1 = FromDt;
                DateTime Todate1 = ToDt;
                bool Comparasion = false;
                Comparasion = (bool)chkChecked.IsChecked;
                ReportBusiness repBiz = new ReportBusiness();
                DataSet result =
                 repBiz.GetDataForIPContentTracking(FromDt, ToDt, ((KeyValuePair<string, string>)(((Selector)(cmbsubstore)).SelectedItem)).Key.ToInt32(), cmbPackages.SelectedValue.ToInt32());
                TaxBusiness taxBussiness = new TaxBusiness();
                //List<TaxDetailInfo> taxDetails = taxBussiness.GetReportTaxDetail(FromDt, ToDt, ((KeyValuePair<string, string>)(((Selector)(cmbsubstore)).SelectedItem)).Key.ToInt32());
                if (result.Tables[0].Rows.Count > 0)
                {
                    report.Load(ApplicationPath + "\\Reports\\IPContentTrackingReport.rpt");
                    DataTable dt = result.Tables[0];
                    report.SetDataSource(dt);
                    SetDBLogonReports(report);
                    ReportSource = dt;
                    filename = "IP Content Tracking Report";
                    report.Refresh();

                }
                else
                {
                    report.Load(ApplicationPath + "\\Reports\\Temp.rpt");
                }
                filename = "IP Content Tracking Report";
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }
        //public static DataSet1 convert(DataSet dataSet)
        //{
        //    if (dataSet == null)
        //        return null;
        //    if (dataSet.Tables.Count == 0)
        //        return null;
        //    DataTable dataTable = dataSet.Tables[0];
        //    return convert(dataTable);
        //}
        /// <summary>
        /// Convert an ordinary DataTable to a strongly-typed
        /// data table.
        /// </summary>
        //public static DataSet1 convert(DataTable dataTable)
        //{
        //    if (dataTable == null)
        //        return null;
        //    DataSet1 stronglyTyped = new DataSet1();
        //    // add data from the regular DataTable to the
        //    // strongly typed DataTable.
        //    stronglyTyped.Merge(dataTable);
        //    return stronglyTyped;
        //}
        private void ReportStorePaymentSummary(DateTime FromDt, DateTime ToDt, string ApplicationPath)
        {
            try
            {
                reportBusiness = new ReportBusiness();
                DateTime FromDate = FromDt;
                DateTime ToDate = ToDt;
                List<PaymentSummaryInfo> result = reportBusiness.GetPaymentSummary(FromDate, ToDate, Common.LoginUser.StoreName.ToString(), ((KeyValuePair<string, string>)(((Selector)(cmbsubstore)).SelectedItem)).Key.ToInt32());
                if (result.Count > 0)
                {

                    //report.Load(ApplicationPath + "\\Reports\\Payment.rpt");
                    //TextObject txtObj = (TextObject)(report.ReportDefinition.ReportObjects["StoreCardSale1"]);

                    //txtObj.ObjectFormat.EnableSuppress = true;
                    //txtObj.Text = "Hello";
                    //DataTable dt = DataTableFromIEnumerable(result);                 
                    //SetDBLogonReports(report);
                    //ReportSource = new ConfigBusiness().ParseListIntoPaymentDataTable(result);
                    //filename = "Payment Summary Report";
                    //report.Refresh();
                    report.Load(ApplicationPath + "\\Reports\\Payment.rpt");
                    //TextObject txtObj = (TextObject)report.ReportDefinition.Sections["Section1"].ReportObjects["StoreCardSale1"];
                    //var cardSale = result.Where(x => x.StoreCardSale > 0).FirstOrDefault().StoreCardSale;
                    //var cardSale = result.AsEnumerable().Select(r => r.StoreCardSale).FirstOrDefault();
                    //txtObj.Text = Convert.ToString(cardSale);
                    //TextObject DateRange = (TextObject)report.ReportDefinition.Sections["Section2"].ReportObjects["txtTax"];
                    DataTable dt = DataTableFromIEnumerable(result);
                    report.SetDataSource(dt);
                    report.Subreports[0].SetDataSource(dt);
                    SetDBLogonReports(report);
                    ReportSource = new ConfigBusiness().ParseListIntoPaymentDataTable(result);
                    filename = "Payment Summary Report";
                    report.Refresh();

                }
                else
                {
                    report.Load(ApplicationPath + "\\Reports\\Temp.rpt");
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        private void ReportPrintLog(DateTime FromDt, DateTime ToDt, string ApplicationPath)
        {
            reportBusiness = new ReportBusiness();
            DateTime FromDate1 = FromDt;
            DateTime Todate1 = ToDt;


            //List<GetPrintedProduct_Result> result = objDigiPhotoDataServices.GetDataForPrintingLog(FromDate1,
            //     Todate1);
            //List<PrintSummary_Result> result2 = objDigiPhotoDataServices.GetDataForPrintingSummary(FromDate1,
            //    Todate1);
            //List<PrintSummaryDetail> printSummaryDetail = reportBusiness.GetPrintSummaryDetail(FromDate1, Todate1);

            DataSet result = reportBusiness.GetDataForPrintingLog(FromDate1,
                 Todate1, ((KeyValuePair<string, string>)(((Selector)(cmbsubstore)).SelectedItem)).Key.ToInt32());
            DataSet result2 = reportBusiness.GetDataForPrintingSummary(FromDate1,
                Todate1, ((KeyValuePair<string, string>)(((Selector)(cmbsubstore)).SelectedItem)).Key.ToInt32());
            DataSet printSummaryDetail = reportBusiness.GetPrintSummaryDetail(FromDate1, Todate1, ((KeyValuePair<string, string>)(((Selector)(cmbsubstore)).SelectedItem)).Key.ToInt32());

            if (result.Tables.Count > 0)// && result.Tables[0].Rows.Count > 0)
            {
                report.Load(ApplicationPath + "\\Reports\\PrintLogReport.rpt");
                //DataTable dt3 = DataTableFromIEnumerable(printSummaryDetail);
                //-------------Add Date Time In Report       ///////
                TextObject DateRange = (TextObject)report.ReportDefinition.Sections["Section2"].ReportObjects["txtDate"];
                DateRange.Text = "From " + FromDt.ToString("dd MMM yyyy hh:mm tt") + " to  " + ToDt.ToString("dd MMM yyyy hh:mm tt");



                //--------------- End Section---------//

                report.SetDataSource(result.Tables[0]);

                //   report.OpenSubreport(ApplicationPath + "\\Reports\\Summary.rpt").SetDataSource(dt2);

                report.Subreports[1].SetDataSource(result2.Tables[0]);
                report.Subreports[0].SetDataSource(printSummaryDetail.Tables[0]);
                filename = "Printing Report";
            }
            else
            {
                report.Load(ApplicationPath + "\\Reports\\Temp.rpt");
            }
        }

        private void ReportsOrderDiscount(DateTime FromDt, DateTime ToDt, DateTime FromDt1, DateTime ToDt1, string ApplicationPath)
        {
            DateTime FromDate1 = FromDt;
            DateTime Todate1 = ToDt;
            DateTime FromDate2 = FromDt1;
            DateTime Todate2 = ToDt1;
            bool Comparasion = false;
            Comparasion = (bool)chkChecked.IsChecked;
            //report.Load(ApplicationPath + "\\Reports\\OrderDiscountReport.rpt");
            //List<OrderDetailedDiscount_Get_Result> result = objDigiPhotoDataServices.GetOrderDetailReport(
            //    FromDt, ToDt, Common.LoginUser.UserName.ToString(), Common.LoginUser.StoreName.ToString());
            ReportBusiness repBiz = new ReportBusiness();
            DataSet result = repBiz.GetOrderDetailReport(
                FromDt, ToDt, Common.LoginUser.UserName.ToString(), Common.LoginUser.StoreName.ToString());
            if (result.Tables.Count > 0 && result.Tables[0].Rows.Count > 0)
            {
                report.Load(ApplicationPath + "\\Reports\\OrderDiscountReport.rpt");
                //DataTable dt = DataTableFromIEnumerable(result);
                report.SetDataSource(result.Tables[0]);
                filename = "Order Detailed Report";
            }
            else
            {
                report.Load(ApplicationPath + "\\Reports\\Temp.rpt");
            }
        }

        private void ReportOrderDiscount(DateTime FromDt, DateTime ToDt, DateTime FromDt1, DateTime ToDt1, string ApplicationPath)
        {
            DateTime FromDate1 = FromDt;
            DateTime Todate1 = ToDt;
            DateTime FromDate2 = FromDt1;
            DateTime Todate2 = ToDt1;
            bool Comparasion = false;
            Comparasion = (bool)chkChecked.IsChecked;
            report.Load(ApplicationPath + "\\Reports\\OrderDiscountReport.rpt");

            //List<DigiPhoto.DataLayer.Model.OrderDetailedDiscount_Get_Result> result = objDigiPhotoDataServices.GetOrderDetailReport(
            //   FromDt, ToDt, Common.LoginUser.UserName.ToString(), Common.LoginUser.StoreName.ToString());
            ReportBusiness repBiz = new ReportBusiness();
            DataSet result = repBiz.GetOrderDetailReport(
                       FromDt, ToDt, Common.LoginUser.UserName.ToString(), Common.LoginUser.StoreName.ToString());

            if (result.Tables.Count > 0 && result.Tables[0].Rows.Count > 0)
            {

                //DataTable dt = DataTableFromIEnumerable(result);
                report.SetDataSource(result.Tables[0]);
                //Set Database Logon for Subreports.
                SetDBLogonForSubreports(report);
                filename = "Order Detailed Report";
            }
            else
            {
                report.Load(ApplicationPath + "\\Reports\\Temp.rpt");
            }
        }

        private void ReportFinancialaudit(DateTime FromDt, DateTime ToDt, string ApplicationPath)
        {
            DateTime FromDate1 = FromDt;
            DateTime Todate1 = ToDt;
            bool Comparasion = false;
            Comparasion = (bool)chkChecked.IsChecked;
            ReportBusiness repBiz = new ReportBusiness();
            DataSet result =
             repBiz.GetFinancialAuditData(Common.LoginUser.UserName.ToString(),
                  Common.LoginUser.StoreName.ToString(), FromDt, ToDt, ((KeyValuePair<string, string>)(((Selector)(cmbsubstore)).SelectedItem)).Key.ToInt32());
            TaxBusiness taxBussiness = new TaxBusiness();
            List<TaxDetailInfo> taxDetails = taxBussiness.GetReportTaxDetail(FromDt, ToDt, ((KeyValuePair<string, string>)(((Selector)(cmbsubstore)).SelectedItem)).Key.ToInt32());
            if (result.Tables.Count > 0 && result.Tables[0].Rows.Count > 0)
            {
                report.Load(ApplicationPath + "\\Reports\\Financialauditreport.rpt");

                TextObject DateRange = (TextObject)report.ReportDefinition.Sections["Section1"].ReportObjects["txtDate"];
                DateRange.Text = "From " + FromDt.ToString("dd MMM yyyy hh:mm tt") + " to  " + ToDt.ToString("dd MMM yyyy hh:mm tt");
                report.SetDataSource(result.Tables[0]);
                SetDBLogonForSubreports(report);
                filename = "Financial Audit Report";
            }
            else
            {
                report.Load(ApplicationPath + "\\Reports\\Temp.rpt");
            }
        }
        private void ReportPhotoGrapherPerformance(DateTime FromDt, DateTime ToDt, DateTime FromDt1, DateTime ToDt1, string ApplicationPath)
        {
            DateTime FromDate1 = FromDt;
            DateTime Todate1 = ToDt;
            DateTime FromDate2 = FromDt1;
            DateTime Todate2 = ToDt1;
            bool Comparasion = false;
            Comparasion = (bool)chkChecked.IsChecked;
            report.Load(ApplicationPath + "\\Reports\\PhotoGrapherPerformanceReport.rpt");
            //List<GetPhotgrapherPerformance_Result> result =
            //    objDigiPhotoDataServices.GetPhotographerPerformanceReports(FromDt, Todate1, FromDate2, Todate2,
            //        Common.LoginUser.StoreName.ToString(), Common.LoginUser.UserName.ToString(), Comparasion);
            //sanchit
            ReportBusiness repBiz = new ReportBusiness();
            DataSet result =
              repBiz.GetPhotographerPerformanceReports(FromDt, Todate1, FromDate2, Todate2,
                  Common.LoginUser.StoreName.ToString(), Common.LoginUser.UserName.ToString(), Comparasion);
            if (result.Tables.Count > 0 && result.Tables[0].Rows.Count > 0)
            {
                report.SetDataSource(result.Tables[0]);
                filename = "Photographer Performance Report";
            }
            else
            {
                report.Load(ApplicationPath + "\\Reports\\Temp.rpt");
            }
        }

        private void ReportLocationPerformance(DateTime FromDt, DateTime ToDt, DateTime FromDt1, DateTime ToDt1, string ApplicationPath)
        {
            DateTime FromDate1 = FromDt;
            DateTime Todate1 = ToDt;
            DateTime FromDate2 = FromDt1;
            DateTime Todate2 = ToDt1;
            bool Comparasion = false;
            #region Added by Ajay on 20 Sep to solve the Vat Amount as per the Production summary report
            decimal vatValue;
            decimal totalCost = 0;
            decimal totalRevenue = 0;
            decimal totalDiscount = 0;
            decimal taxPercentage = 0;
            //By VINS 15Nov 2018
            string perSign = " % ";
            decimal vatPer;
            string taxNameVat;
            //added by nilesh
            decimal calTotalCost = 0;

            #endregion
            Comparasion = (bool)chkChecked.IsChecked;

            //List<DigiPhoto.DataLayer.Model.GetLocationPerformance_Result> result =
            //   objDigiPhotoDataServices.GetLocationPerformanceReports(FromDt, Todate1, FromDate2, Todate2,
            //       Common.LoginUser.StoreName.ToString(), Common.LoginUser.UserName.ToString(), Comparasion);
            ReportBusiness repBiz = new ReportBusiness();
            DataSet result =
            repBiz.GetLocationPerformanceReports(FromDt, Todate1, FromDate2, Todate2,
                Common.LoginUser.StoreName.ToString(), Common.LoginUser.UserName.ToString(), Comparasion, cmbsubstore.SelectedIndex > 0 ? cmbsubstore.SelectedValue.ToString() : string.Empty, ((KeyValuePair<string, string>)(((Selector)(cmbsubstore)).SelectedItem)).Key.ToInt32());

            TaxBusiness taxBussiness = new TaxBusiness();
            List<TaxDetailInfo> taxDetails = taxBussiness.GetReportTaxDetail(FromDt, ToDt, ((KeyValuePair<string, string>)(((Selector)(cmbsubstore)).SelectedItem)).Key.ToInt32());

            /**/
            #region Added by Ajay on 20 Sep to solve the Vat Amount as per the Production summary report
            taxPercentage = taxDetails.Select(x => x.TaxPercentage).ToList().FirstOrDefault();
            ProductBusiness proBiz = new ProductBusiness();
            DataSet resultGetProductSummary = proBiz.GetProductSummary(FromDt, ToDt,
                    Common.LoginUser.StoreName.ToString(), Common.LoginUser.UserName.ToString(), ((KeyValuePair<string, string>)(((Selector)(cmbsubstore)).SelectedItem)).Key.ToInt32());
            DataRow[] data = null;
            if (((KeyValuePair<string, string>)(((Selector)(cmbsubstore)).SelectedItem)).Value == "--Select--")
            {
                data = resultGetProductSummary.Tables[0].Select();
            }
            else
            {
                data = resultGetProductSummary.Tables[0].Select("DG_SubStore_Name = " + "'" + ((KeyValuePair<string, string>)(((Selector)(cmbsubstore)).SelectedItem)).Value + "'", "", DataViewRowState.CurrentRows);
            }
            foreach (DataRow dr in data)
            {
                // By KCB ON 28 MAR 2018 For rectifying var amount
                string cancel = "(C)";
                if (!Convert.ToString(dr["ProductName"].ToString()).Contains(cancel))
                {
                    totalCost += Convert.ToDecimal(dr["NetPrice"]);//TotalCost
                    totalDiscount += Convert.ToDecimal(dr["Discount"]);
                    //added by nilesh
                    calTotalCost += Convert.ToDecimal(dr["TotalCost"]);
                }
                else
                {
                    totalCost -= Convert.ToDecimal(dr["NetPrice"]);
                    totalDiscount -= Convert.ToDecimal(dr["Discount"]);
                }
            }
            //added by nilesh for the tax calculation for tax include
            StoreInfo store = new StoreInfo();
            TaxBusiness taxBusiness = new TaxBusiness();
            store = taxBusiness.getTaxConfigData();


            if (store.IsTaxIncluded)
            {
                // vatValue = (totalCost - totalDiscount) / (1 + (taxPercentage / 100)) * taxPercentage / 100;
                vatValue = totalCost / (1 + (taxPercentage / 100)) * taxPercentage / 100;
            }
            else
            {
                // vatValue = (totalCost - totalDiscount) * (taxPercentage / 100);
                vatValue = (calTotalCost - totalDiscount) * (taxPercentage / 100);
            }


            //  vatValue = (totalCost - totalDiscount) / (1 + (taxPercentage / 100)) * taxPercentage / 100;
            if (vatValue > 0)
            {
                taxDetails[0].TaxAmount = Convert.ToDecimal(vatValue);
            }
            #endregion
            /**/



            if (result.Tables.Count > 0 && result.Tables[0].Rows.Count > 0)
            {
                DataTable resultTable = result.Tables[0];
                report.Load(ApplicationPath + "\\Reports\\LocationPerformance.rpt");

                if (cmbsubstore.SelectedIndex > 0)
                {
                    IEnumerable<DataRow> substoreQuery =
                                from sd in resultTable.AsEnumerable()
                                where sd.Field<string>("DG_SubStore_Name") == cmbsubstore.SelectedValue.ToString()
                                select sd;
                    if (substoreQuery.Count<DataRow>() > 0)
                    {
                        DataTable subStorTable = substoreQuery.CopyToDataTable<DataRow>();
                        ReportSource = new ConfigBusiness().CreateReportData(subStorTable, ReportTypes.SitePerformanceReport);
                        report.SetDataSource(subStorTable);
                    }
                    else
                    {
                        report.Load(ApplicationPath + "\\Reports\\Temp.rpt");
                    }
                }
                else
                {
                    report.SetDataSource(result.Tables[0]);
                    ReportSource = new ConfigBusiness().CreateReportData(result.Tables[0], ReportTypes.SitePerformanceReport);
                }

                ///report.Subreports[0].SetDataSource(taxDetails); //Handled in code by VINS 15Nov2018
                //SetDBLogonForSubreports(report);

                //* Below code added by VINS 15Nov2018 *****Start
                TextObject txtTotalRevenue = (TextObject)report.ReportDefinition.Sections["Section4"].ReportObjects["txtTotalRevenue"];
                //totalRevenue = totalCost - totalDiscount;
                totalRevenue = totalCost;
                txtTotalRevenue.Text = totalRevenue.ToString("0.00");

                vatPer = taxPercentage;
                taxNameVat = taxDetails.Select(x => x.TaxName).ToList().FirstOrDefault();

                TextObject txttax = (TextObject)report.ReportDefinition.Sections["Section4"].ReportObjects["txtTax"];

                txttax.Text = vatValue.ToString("0.00");
                TextObject actualRevenue = (TextObject)report.ReportDefinition.Sections["Section4"].ReportObjects["txtactualRevenue"];
                // By KCB ON 28 MAR 2018 For rectifying var amount

                actualRevenue.Text = (totalRevenue - vatValue).ToString("0.00");
                /*******************************End********************************************/


                TextObject txttaxpercentage = (TextObject)report.ReportDefinition.Sections["Section4"].ReportObjects["txtTaxPercentageText"];
                txttaxpercentage.Text = vatPer.ToString("0.00") + perSign;
                TextObject txttaxname = (TextObject)report.ReportDefinition.Sections["Section4"].ReportObjects["txtTaxNameText"];
                txttaxname.Text = taxNameVat ?? string.Empty;
                //* Below code added by VINS 15Nov2018 *****End


                filename = "Site Performance Report";
            }
            else
            {
                report.Load(ApplicationPath + "\\Reports\\Temp.rpt");
            }
        }

        private void ReportOperationAudit(DateTime FromDt, DateTime ToDt, string ApplicationPath)
        {

            //List<DigiPhoto.DataLayer.Model.OperationalAudit_Result> result = objDigiPhotoDataServices.GetAuditTrail(FromDt, ToDt);
            ReportBusiness repBiz = new ReportBusiness();
            DataSet result = repBiz.GetAuditTrail(FromDt, ToDt, ((KeyValuePair<string, string>)(((Selector)(cmbsubstore)).SelectedItem)).Key.ToInt32());
            if (result.Tables.Count > 0 && result.Tables[0].Rows.Count > 0)
            {
                report.Load(ApplicationPath + "\\Reports\\OperationAuditTrail.rpt");
                //-------------Add Date Time In Report       ///////
                TextObject DateRange = (TextObject)report.ReportDefinition.Sections["Section1"].ReportObjects["txtDate"];
                DateRange.Text = "From " + FromDt.ToString("dd MMM yyyy hh:mm tt") + " to  " + ToDt.ToString("dd MMM yyyy hh:mm tt");
                //--------------- End Section---------//
                report.SetDataSource(result.Tables[0]);
                filename = "Operation Audit Trail Report";
            }
            else
            {
                report.Load(ApplicationPath + "\\Reports\\Temp.rpt");
            }
        }

        //private void ReportTaking(bool IsFromDate, bool IsToDate, DateTime FromDt, DateTime ToDt, DigiPhotoDataServices objDigiPhotoDataServices, string ApplicationPath)
        //private void ReportTaking(bool IsFromDate, bool IsToDate, DateTime FromDt, DateTime ToDt, string ApplicationPath)
        //{


        //    //List<DigiPhoto.DataLayer.Model.vw_TakingReport> result = objDigiPhotoDataServices.GetTakingReport(IsFromDate, IsToDate, FromDt,
        //    //   ToDt, cmbPhotographer.SelectedValue.ToInt32(), cmbsubstore.SelectedIndex > 0 ? cmbsubstore.SelectedValue.ToString() : string.Empty);
        //    ReportBusiness repBiz = new ReportBusiness();
        //    DataSet result = repBiz.GetTakingReport(IsFromDate, IsToDate, FromDt,
        //                                                        ToDt, Convert.ToInt32(cmbPhotographer.SelectedValue), ((KeyValuePair<string, string>)(((Selector)(cmbsubstore)).SelectedItem)).Key.ToInt32());

        //    if (result.Tables.Count > 0 && result.Tables[0].Rows.Count > 0)
        //    {
        //        report.Load(ApplicationPath + "\\Reports\\TakingReports.rpt");
        //        // DataTable dt = DataTableFromIEnumerable(result);
        //        //-------------Add Date Time In Report       ///////
        //        TextObject DateRange = (TextObject)report.ReportDefinition.Sections["Section1"].ReportObjects["txtDate"];
        //        DateRange.Text = "From " + FromDt.ToString("dd MMM yyyy hh:mm tt") + " to  " + ToDt.ToString("dd MMM yyyy hh:mm tt");
        //        //--------------- End Section---------//
        //        report.SetDataSource(result.Tables[0]);
        //        SetDBLogonForSubreports(report);
        //        filename = "Taking Report ";
        //    }
        //    else
        //    {
        //        report.Load(ApplicationPath + "\\Reports\\Temp.rpt");
        //    }

        //}
        private void ReportTaking(bool IsFromDate, bool IsToDate, DateTime FromDt, DateTime ToDt, string ApplicationPath)
        {


            //List<DigiPhoto.DataLayer.Model.vw_TakingReport> result = objDigiPhotoDataServices.GetTakingReport(IsFromDate, IsToDate, FromDt,
            //   ToDt, cmbPhotographer.SelectedValue.ToInt32(), cmbsubstore.SelectedIndex > 0 ? cmbsubstore.SelectedValue.ToString() : string.Empty);
            ReportBusiness repBiz = new ReportBusiness();
            DataSet result = repBiz.GetTakingReport(IsFromDate, IsToDate, FromDt,
                                                                ToDt, Convert.ToInt32(cmbPhotographer.SelectedValue), ((KeyValuePair<string, string>)(((Selector)(cmbsubstore)).SelectedItem)).Key.ToInt32());

            if (result != null && result.Tables.Count > 0 && result.Tables[0].Rows.Count > 0)
            {
                DataTable takingReportData = result.Tables[0];
                takingReportData.Columns.Add(new DataColumn("StoreName"));
                foreach (DataRow dr in takingReportData.Rows)
                {
                    dr["StoreName"] = Common.LoginUser.StoreName;
                }
                report.Load(ApplicationPath + "\\Reports\\TakingReports.rpt");
                // DataTable dt = DataTableFromIEnumerable(result);
                //-------------Add Date Time In Report       ///////
                TextObject DateRange = (TextObject)report.ReportDefinition.Sections["Section1"].ReportObjects["txtDate"];
                DateRange.Text = "From " + FromDt.ToString("dd MMM yyyy hh:mm tt") + " to  " + ToDt.ToString("dd MMM yyyy hh:mm tt");
                //--------------- End Section---------//
                report.SetDataSource(takingReportData);
                ReportSource = new ConfigBusiness().CreateReportData(takingReportData, ReportTypes.TakingReport);
                SetDBLogonForSubreports(report);
                filename = "Taking Report ";
            }
            else
            {
                report.Load(ApplicationPath + "\\Reports\\Temp.rpt");
            }

        }


        private void ReportOperatorPerformance(DateTime FromDt, DateTime ToDt, DateTime FromDt1, DateTime ToDt1, string ApplicationPath)
        {
            DateTime FromDate1 = FromDt;
            DateTime Todate1 = ToDt;
            DateTime FromDate2 = FromDt1;
            DateTime Todate2 = ToDt1;

            bool Comparasion = false;
            Comparasion = (bool)chkChecked.IsChecked;

            //List<DigiPhoto.DataLayer.Model.OperatorPerformanceReport_Result> result = objDigiPhotoDataServices.GetOperatorReports(3,
            //    FromDt, Todate1, FromDate2, Todate2, Common.LoginUser.StoreName.ToString(),
            //    Common.LoginUser.UserName.ToString(), Comparasion);

            ReportBusiness repBiz = new ReportBusiness();
            DataSet result = repBiz.GetOperatorReports(3,
               FromDt, Todate1, FromDate2, Todate2, Common.LoginUser.StoreName.ToString(),
               Common.LoginUser.UserName.ToString(), Comparasion);


            if (result.Tables.Count > 0 && result.Tables[0].Rows.Count > 0)
            {
                report.Load(ApplicationPath + "\\Reports\\OperatorPerformanceReport.rpt");
                //DataTable dt = DataTableFromIEnumerable(result);
                report.SetDataSource(result.Tables[0]);
                filename = "Operator Performance Report";
            }
            else
            {
                report.Load(ApplicationPath + "\\Reports\\Temp.rpt");
            }
        }

        /// <summary>
        /// production Summary report
        /// </summary>
        /// <param name="FromDt"></param>
        /// <param name="ToDt"></param>
        /// <param name="ApplicationPath"></param>
        private void ReportProductSummary(DateTime FromDt, DateTime ToDt, string ApplicationPath)
        {
            decimal taxPercentage;
            string perSign = " % ";
            decimal vatPer;
            decimal vatValue;
            string taxNameVat;
            decimal totalCost = 0;
            decimal totalRevenue = 0;
            decimal totalDiscount = 0;
            decimal calTotalCost = 0;
            try
            {
                bool NoDataFound = false;
                report.Load(ApplicationPath + "\\Reports\\ProductSummary.rpt");
                ProductBusiness proBiz = new ProductBusiness();
                DataSet result = proBiz.GetProductSummary(FromDt, ToDt,
                    Common.LoginUser.StoreName.ToString(), Common.LoginUser.UserName.ToString(), ((KeyValuePair<string, string>)(((Selector)(cmbsubstore)).SelectedItem)).Key.ToInt32());
                //DataRow[] data = result.Tables[0].Select("DG_SubStore_Name=" + ((KeyValuePair<string, string>)(((Selector)(cmbsubstore)).SelectedItem)).Value);
                /**********************************************Manoj Started **************/
                DataRow[] data = null;
                if (((KeyValuePair<string, string>)(((Selector)(cmbsubstore)).SelectedItem)).Value == "--Select--")
                {
                    data = result.Tables[0].Select();
                }
                else
                {
                    data = result.Tables[0].Select("DG_SubStore_Name = " + "'" + ((KeyValuePair<string, string>)(((Selector)(cmbsubstore)).SelectedItem)).Value + "'", "", DataViewRowState.CurrentRows);
                }

                TaxBusiness taxBussiness = new TaxBusiness();
                List<TaxDetailInfo> taxDetails = taxBussiness.GetReportTaxDetail(FromDt, ToDt, ((KeyValuePair<string, string>)(((Selector)(cmbsubstore)).SelectedItem)).Key.ToInt32());

                taxPercentage = taxDetails.Select(x => x.TaxPercentage).ToList().FirstOrDefault();

                StoreInfo store = new StoreInfo();
                TaxBusiness taxBusiness = new TaxBusiness();
                store = taxBusiness.getTaxConfigData();

                if (store.IsTaxEnabled)
                {
                    foreach (DataRow dr in data)
                    {
                        // By KCB ON 28 MAR 2018 For rectifying var amount
                        string cancel = "(C)";
                        if (!Convert.ToString(dr["ProductName"].ToString()).Contains(cancel))
                        {
                            totalCost += Convert.ToDecimal(dr["NetPrice"]);//TotalCost
                            //totalCost += Convert.ToDecimal(dr["TotalCost"]);//TotalCost..Manju
                            totalDiscount += Convert.ToDecimal(dr["Discount"]);
                            calTotalCost += Convert.ToDecimal(dr["TotalCost"]);
                        }
                        else
                        {
                            if (Convert.ToString(dr["NetPrice"].ToString()).Contains("-"))//TotalCost
                            {
                                totalCost += Convert.ToDecimal(dr["NetPrice"]);//TotalCost
                                //totalCost += Convert.ToDecimal(dr["TotalCost"]);//TotalCost manju
                                calTotalCost += Convert.ToDecimal(dr["TotalCost"]);
                            }
                            else
                            {
                                totalCost -= Convert.ToDecimal(dr["NetPrice"]);//TotalCost
                                //totalCost -= Convert.ToDecimal(dr["TotalCost"]);//TotalCost Manju
                                calTotalCost -= Convert.ToDecimal(dr["TotalCost"]);
                            }

                            if (Convert.ToString(dr["Discount"].ToString()).Contains("-"))
                            {
                                totalDiscount += Convert.ToDecimal(dr["Discount"]);
                            }
                            else
                            {
                                totalDiscount -= Convert.ToDecimal(dr["Discount"]);
                            }
                        }

                        //txtActRev
                        //TextObject txtActRev = (TextObject)report.ReportDefinition.Sections["Section3"].ReportObjects["txtActRev"];
                        //txtActRev.Text = "32";


                    }

                    // By KCB ON 28 MAR 2018 For rectifying var amount
                    if (store.IsTaxIncluded)
                    {
                        // vatValue = (totalCost - totalDiscount) / (1 + (taxPercentage / 100)) * taxPercentage / 100;
                        vatValue = totalCost / (1 + (taxPercentage / 100)) * taxPercentage / 100;
                    }
                    else
                    {
                        // vatValue = (totalCost - totalDiscount) * (taxPercentage / 100);
                        vatValue = (calTotalCost - totalDiscount) * (taxPercentage / 100);
                    }
                    TextObject txtTotalRevenue = (TextObject)report.ReportDefinition.Sections["Section4"].ReportObjects["txtTotalRevenue"];
                    //totalRevenue = totalCost - totalDiscount;
                    totalRevenue = totalCost;
                    txtTotalRevenue.Text = totalRevenue.ToString("0.00");
                    vatPer = taxPercentage;
                    taxNameVat = taxDetails.Select(x => x.TaxName).ToList().FirstOrDefault();

                    TextObject txttax = (TextObject)report.ReportDefinition.Sections["Section4"].ReportObjects["txtTax"];

                    txttax.Text = vatValue.ToString("0.00");
                    TextObject actualRevenue = (TextObject)report.ReportDefinition.Sections["Section4"].ReportObjects["txtactualRevenue"];
                    // By KCB ON 28 MAR 2018 For rectifying var amount

                    actualRevenue.Text = (totalRevenue - vatValue).ToString("0.00");
                    /*******************************End********************************************/

                }
                else
                {
                    foreach (DataRow dr in data)
                    {
                        // By KCB ON 28 MAR 2018 For rectifying var amount
                        string cancel = "(C)";
                        if (!Convert.ToString(dr["ProductName"].ToString()).Contains(cancel))
                        {
                            totalCost += Convert.ToDecimal(dr["NetPrice"]);//TotalCost                            
                            totalDiscount += Convert.ToDecimal(dr["Discount"]);
                            calTotalCost += Convert.ToDecimal(dr["TotalCost"]);
                        }
                        else
                        {
                            if (Convert.ToString(dr["NetPrice"].ToString()).Contains("-"))//TotalCost
                            {
                                totalCost += Convert.ToDecimal(dr["NetPrice"]);//TotalCost  
                                calTotalCost += Convert.ToDecimal(dr["TotalCost"]);
                            }
                            else
                            {
                                totalCost -= Convert.ToDecimal(dr["NetPrice"]);//TotalCost 

                            }

                            if (Convert.ToString(dr["Discount"].ToString()).Contains("-"))
                            {
                                totalDiscount += Convert.ToDecimal(dr["Discount"]);
                            }
                            else
                            {
                                totalDiscount -= Convert.ToDecimal(dr["Discount"]);
                            }
                        }
                    }


                    // By KCB ON 28 MAR 2018 For rectifying var amount
                    if (store.IsTaxIncluded)
                    {
                        //  vatValue = (totalCost - totalDiscount) / (1 + (taxPercentage / 100)) * taxPercentage / 100;
                        vatValue = totalCost / (1 + (taxPercentage / 100)) * taxPercentage / 100;
                    }
                    else
                    {
                        //vatValue = (totalCost - totalDiscount) * (taxPercentage / 100);
                        vatValue = (calTotalCost - totalDiscount) * (taxPercentage / 100);
                    }
                    TextObject txtTotalRevenue = (TextObject)report.ReportDefinition.Sections["Section4"].ReportObjects["txtTotalRevenue"];
                  //  totalRevenue = totalCost - totalDiscount;
                    totalRevenue = totalCost;
                    txtTotalRevenue.Text = totalRevenue.ToString("0.00");
                    vatPer = taxPercentage;
                    taxNameVat = taxDetails.Select(x => x.TaxName).ToList().FirstOrDefault();

                    TextObject txttax = (TextObject)report.ReportDefinition.Sections["Section4"].ReportObjects["txtTax"];

                    txttax.Text = vatValue.ToString("0.00");
                    TextObject actualRevenue = (TextObject)report.ReportDefinition.Sections["Section4"].ReportObjects["txtactualRevenue"];
                    // By KCB ON 28 MAR 2018 For rectifying var amount

                    actualRevenue.Text = (totalRevenue - vatValue).ToString("0.00");
                    /*******************************End********************************************/

                }


                /*
                // By KCB ON 28 MAR 2018 For rectifying var amount

                vatValue = (totalCost - totalDiscount) / (1 + (taxPercentage / 100)) * taxPercentage / 100;

                TextObject txtTotalRevenue = (TextObject)report.ReportDefinition.Sections["Section4"].ReportObjects["txtTotalRevenue"];
                //totalRevenue = totalCost - totalDiscount;
                totalRevenue = totalCost;
                txtTotalRevenue.Text = totalRevenue.ToString("0.00");
                vatPer = taxPercentage;
                taxNameVat = taxDetails.Select(x => x.TaxName).ToList().FirstOrDefault();

                TextObject txttax = (TextObject)report.ReportDefinition.Sections["Section4"].ReportObjects["txtTax"];

                txttax.Text = vatValue.ToString("0.00");
                TextObject actualRevenue = (TextObject)report.ReportDefinition.Sections["Section4"].ReportObjects["txtactualRevenue"];
                // By KCB ON 28 MAR 2018 For rectifying var amount

                actualRevenue.Text = (totalRevenue - vatValue).ToString("0.00");
                /*******************************End*******************************************
                
                */



                TextObject txttaxpercentage = (TextObject)report.ReportDefinition.Sections["Section4"].ReportObjects["txtTaxPercentageText"];
                txttaxpercentage.Text = vatPer.ToString("0.00") + perSign;
                TextObject txttaxname = (TextObject)report.ReportDefinition.Sections["Section4"].ReportObjects["txtTaxNameText"];
                txttaxname.Text = taxNameVat ?? string.Empty;

                if (result.Tables.Count > 0 && result.Tables[0].Rows.Count > 0)
                {
                    var test = new ReportBusiness().FetchReportFormatDetails(1);
                    if (cmbsubstore.SelectedIndex > 0)
                    {
                        DataTable resultTable = result.Tables[0];
                        IEnumerable<DataRow> productQuery =
                                    from product in resultTable.AsEnumerable()
                                    where product.Field<string>("DG_SubStore_Name") == cmbsubstore.SelectedValue.ToString()
                                    select product;
                        if (productQuery.Count<DataRow>() > 0)
                        {
                            DataTable productTable = productQuery.CopyToDataTable<DataRow>();
                            ReportSource = new ConfigBusiness().CreateReportData(productTable, ReportTypes.ProductionSummaryReport);
                            report.SetDataSource(productTable);
                            SetDBLogonReports(report);
                        }
                        else
                        {
                            NoDataFound = true;
                            report.Load(ApplicationPath + "\\Reports\\Temp.rpt");
                        }
                    }
                    else
                    {
                        report.SetDataSource(result.Tables[0]);
                        ReportSource = new ConfigBusiness().CreateReportData(result.Tables[0], ReportTypes.ProductionSummaryReport);
                        SetDBLogonReports(report);
                    }
                    // DataTable dt = DataTableFromIEnumerable(result);
                    if (!NoDataFound)
                    {
                        if (report.Subreports.Count > 0)
                        {
                            report.Subreports[0].SetDataSource(taxDetails);
                        }
                    }
                    //SetDBLogonForSubreports(report);
                    filename = "Product Summary Report";
                }
                else
                {
                    report.Load(ApplicationPath + "\\Reports\\Temp.rpt");
                }

            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogFileWrite("ReportProductSummary : catch " + ex.StackTrace);
            }
        }
        /// <summary>
        /// Production Summary report venue level
        /// </summary>
        /// <param name="FromDt"></param>
        /// <param name="ToDt"></param>
        /// <param name="ApplicationPath"></param>
        private void ReportProductSummaryVenue(DateTime FromDt, DateTime ToDt, string ApplicationPath)
        {
            decimal taxPercentage;
            string perSign = " % ";
            decimal vatPer;
            decimal vatValue;
            string taxNameVat;
            decimal totalCost = 0;
            decimal totalRevenue = 0;
            decimal totalDiscount = 0;// By KCB ON 28 MAR 2018 For rectifying var amount
            decimal calTotalCost = 0;
            try
            {
                bool NoDataFound = false;
                report.Load(ApplicationPath + "\\Reports\\ProductSummaryVenue.rpt");
                ProductBusiness proBiz = new ProductBusiness();
                DataSet result = proBiz.GetProductSummary(FromDt, ToDt,
                    Common.LoginUser.StoreName.ToString(), Common.LoginUser.UserName.ToString(), ((KeyValuePair<string, string>)(((Selector)(cmbsubstore)).SelectedItem)).Key.ToInt32());

                TaxBusiness taxBussiness = new TaxBusiness();
                List<TaxDetailInfo> taxDetails = taxBussiness.GetReportTaxDetail(FromDt, ToDt, ((KeyValuePair<string, string>)(((Selector)(cmbsubstore)).SelectedItem)).Key.ToInt32());
                DataRow[] data = null;
                if (((KeyValuePair<string, string>)(((Selector)(cmbsubstore)).SelectedItem)).Value == "--Select--")
                {
                    data = result.Tables[0].Select();
                }
                else
                {
                    data = result.Tables[0].Select("DG_SubStore_Name = " + "'" + ((KeyValuePair<string, string>)(((Selector)(cmbsubstore)).SelectedItem)).Value + "'", "", DataViewRowState.CurrentRows);
                }
                //foreach (DataRow dr in result.Tables[0].Rows)
                foreach (DataRow dr in data)
                {
                    // By KCB ON 28 MAR 2018 For rectifying var amount                   

                    string cancel = "(C)";
                    if (!Convert.ToString(dr["ProductName"].ToString()).Contains(cancel))
                    {
                        totalCost += Convert.ToDecimal(dr["NetPrice"]);
                        totalDiscount += Convert.ToDecimal(dr["Discount"]);
                        calTotalCost += Convert.ToDecimal(dr["TotalCost"]);
                    }
                    else
                    {
                        // totalCost -= Convert.ToDecimal(dr["TotalCost"]);
                        // totalDiscount -= Convert.ToDecimal(dr["Discount"]);
                        if (Convert.ToString(dr["NetPrice"].ToString()).Contains("-"))
                        {
                            totalCost += Convert.ToDecimal(dr["NetPrice"]);
                            calTotalCost += Convert.ToDecimal(dr["TotalCost"]);
                        }
                        else
                        {
                            totalCost -= Convert.ToDecimal(dr["NetPrice"]);
                        }

                        if (Convert.ToString(dr["Discount"].ToString()).Contains("-"))
                        {
                            totalDiscount += Convert.ToDecimal(dr["Discount"]);
                        }
                        else
                        {
                            totalDiscount -= Convert.ToDecimal(dr["Discount"]);
                        }
                    }
                    //END
                }

                StoreInfo store = new StoreInfo();
                TaxBusiness taxBusiness = new TaxBusiness();
                store = taxBusiness.getTaxConfigData();

                taxPercentage = taxDetails.Select(x => x.TaxPercentage).ToList().FirstOrDefault();
                // By KCB ON 28 MAR 2018 For rectifying var amount
                //actualValue = totalCost / ((100 + taxPercentage) / 100) * taxPercentage / 100;
                if (store.IsTaxIncluded)
                {
                    // vatValue = (totalCost - totalDiscount) / (1 + (taxPercentage / 100)) * 5 / 100;
                    vatValue = totalCost / (1 + (taxPercentage / 100)) * taxPercentage / 100;
                }
                else
                {
                    // vatValue = totalCost * (taxPercentage / 100);
                    vatValue = (calTotalCost - totalDiscount) * (taxPercentage / 100);

                }

                TextObject txttax = (TextObject)report.ReportDefinition.Sections["Section4"].ReportObjects["txtTax"];
                txttax.Text = vatValue.ToString("0.00");
                //end
                /**********************************************Manoj Started **************/

                //Total revenue
                TextObject txtTotalRevenue = (TextObject)report.ReportDefinition.Sections["Section4"].ReportObjects["txtTotalRevenue"];
                //totalRevenue = totalCost - totalDiscount;
                totalRevenue = totalCost;
                txtTotalRevenue.Text = totalRevenue.ToString("0.00");

                //end
                vatPer = taxPercentage;
                taxNameVat = taxDetails.Select(x => x.TaxName).ToList().FirstOrDefault();


                //Actual revenue
                TextObject actualRevenue = (TextObject)report.ReportDefinition.Sections["Section4"].ReportObjects["txtactualRevenue"];
                // By KCB ON 28 MAR 2018 For rectifying var amount               
                actualRevenue.Text = (totalRevenue - vatValue).ToString("0.00");


                TextObject txttaxpercentage = (TextObject)report.ReportDefinition.Sections["Section4"].ReportObjects["txtTaxPercentageText"];
                txttaxpercentage.Text = vatPer.ToString("0.00") + perSign;
                TextObject txttaxname = (TextObject)report.ReportDefinition.Sections["Section4"].ReportObjects["txtTaxNameText"];
                txttaxname.Text = taxNameVat ?? string.Empty;

                if (result.Tables.Count > 0 && result.Tables[0].Rows.Count > 0)
                {
                    var test = new ReportBusiness().FetchReportFormatDetails(1);
                    if (cmbsubstore.SelectedIndex > 0)
                    {
                        DataTable resultTable = result.Tables[0];
                        IEnumerable<DataRow> productQuery =
                                    from product in resultTable.AsEnumerable()
                                    where product.Field<string>("DG_SubStore_Name") == cmbsubstore.SelectedValue.ToString()
                                    select product;
                        if (productQuery.Count<DataRow>() > 0)
                        {
                            DataTable productTable = productQuery.CopyToDataTable<DataRow>();
                            ReportSource = new ConfigBusiness().CreateReportData(productTable, ReportTypes.ProductionSummaryReport);
                            report.SetDataSource(productTable);
                            SetDBLogonReports(report);
                        }
                        else
                        {
                            NoDataFound = true;
                            report.Load(ApplicationPath + "\\Reports\\Temp.rpt");
                        }
                    }
                    else
                    {
                        report.SetDataSource(result.Tables[0]);
                        ReportSource = new ConfigBusiness().CreateReportData(result.Tables[0], ReportTypes.ProductionSummaryReport);
                        SetDBLogonReports(report);
                    }
                    // DataTable dt = DataTableFromIEnumerable(result);
                    if (!NoDataFound)
                        filename = "Product Summary Report Venue Level";
                }
                else
                {
                    report.Load(ApplicationPath + "\\Reports\\Temp.rpt");
                }

            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogFileWrite("ReportProductSummaryVenue : catch " + ex.StackTrace);
            }
        }

        private void ReportActivity(bool IsFromDate, bool IsToDate, DateTime FromDt, DateTime ToDt, string ApplicationPath)
        {
            DigiAuditLogger.AuditLog Al = new DigiAuditLogger.AuditLog();
            ActivityBusiness actBiz = new ActivityBusiness();
            //actBiz.ge
            DataSet result = new DataSet();
            result = actBiz.GetActivityReport(FromDt, ToDt, cmbPhotographer.SelectedValue.ToInt32());
            // result = result.OrderByDescending(t => t.ActivityDate).ToList();
            if (result.Tables.Count > 0 && result.Tables[0].Rows.Count > 0)
            {
                report.Load(ApplicationPath + "\\Reports\\ActivityReports.rpt");
                //  DataTable dt = DataTableFromIEnumerable(result);

                ////-------------Add Date Time In Report       ///////
                //TextObject DateRange = (TextObject)report.ReportDefinition.Sections["Section1"].ReportObjects["txtDate"];
                //DateRange.Text = "From " + FromDt.ToString("dd MMM yyyy hh:mm tt") + " to  " + ToDt.ToString("dd MMM yyyy hh:mm tt");



                //--------------- End Section---------//
                report.SetDataSource(result.Tables[0]);
                report.ReportOptions.EnableSaveDataWithReport = true;


                filename = "Activity Report";
            }
            else
            {
                report.Load(ApplicationPath + "\\Reports\\Temp.rpt");
            }
        }
        Microsoft.Win32.SaveFileDialog dlg = new Microsoft.Win32.SaveFileDialog();


        /// <summary>
        /// Handles the Click event of the btnExport control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void btnExport_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                dlg.FileName = filename;
                dlg.DefaultExt = ".xls";
                dlg.Filter = "Excel documents (.xls)|*.xls";
                if (IsEnabledExportToAnyDrive() == false)
                {
                    dlg.FileOk += dlg_FileOk;
                    DriveInfo drives = DriveInfo.GetDrives().Where(drive => drive.DriveType == DriveType.Removable && drive.IsReady == true).FirstOrDefault();
                    if (drives != null)
                    {
                        dlg.InitialDirectory = drives.Name;

                    }
                    else
                    {
                        MessageBox.Show("No removable device found.");
                        return;
                    }
                }
                Nullable<bool> result = dlg.ShowDialog();
                if (result == true)
                {
                    string filename2 = dlg.FileName;
                    if (cmbReportType.SelectedValue.ToString() == "11")
                    {
                        if (dtDataToExport != null)
                            ExportToExcel(dtDataToExport, filename2 + "x");
                    }
                    else

                        report.ExportToDisk(ExportFormatType.Excel, filename2);
                }

            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
            finally
            {
                // if (dlg.FileOk.IsNull()==false)
                dlg.FileOk -= dlg_FileOk;
            }
        }

        private void ExportToCSV(string filePath)
        {
            if (gridReport.HasItems)
            {
                gridReport.SelectAllCells();
                gridReport.ClipboardCopyMode = DataGridClipboardCopyMode.IncludeHeader;
                ApplicationCommands.Copy.Execute(null, gridReport);
                gridReport.UnselectAllCells();
                String Clipboardresult = (string)Clipboard.GetData(DataFormats.CommaSeparatedValue);
                StreamWriter swObj = new StreamWriter(filePath);
                swObj.WriteLine(Clipboardresult);
                swObj.Close();
            }
            //Process.Start(filePath);
        }
        public static void ExportToExcel(DataTable dt, string filePath)
        {

            int RowValue = 2;
            var wb = new XLWorkbook(); //create workbook
            var ws = wb.Worksheets.Add("Sheet 1"); //add worksheet to workbook
            int i = 1;
            foreach (DataColumn dc in dt.Columns)
            {
                ws.Cell(1, i).Value = dc.ToString();
                var columnRange = ws.Range(ws.Cell(1, i).Address, ws.Cell(1, dt.Columns.Count).Address);
                columnRange.Style.Font.Bold = true;
                columnRange.Style.Font.FontSize = 10;
                i++;
            }
            foreach (DataRow dr in dt.Rows)
            {
                int columnvalue = 1;
                foreach (DataColumn dc in dt.Columns)
                {
                    string value = dr[dc].ToString();
                    ws.Cell(RowValue, columnvalue).SetValue(value);
                    columnvalue++;
                }
                RowValue++;
            }
            if (File.Exists(filePath))
            {
                MessageBoxResult result = MessageBox.Show("'" + filePath + "'" + " already exists.\nDo you want to replace it?", "Save As", MessageBoxButton.YesNo, MessageBoxImage.Error);
                if (result == MessageBoxResult.Yes)
                {
                    wb.SaveAs(filePath);
                }
            }
            else
                wb.SaveAs(filePath);
        }
        private void ExportToPDF(DataTable dt, string filePath)
        {
            Font headerFont = FontFactory.GetFont("Arial", 9, 1, iTextSharp.text.Color.BLACK);
            headerFont.IsBold();
            Font rowfont = FontFactory.GetFont("Arial", 8, iTextSharp.text.Color.BLACK);
            Document document = new Document(PageSize.A4);
            PdfWriter writer = PdfWriter.GetInstance(document,
                       new FileStream(filePath, FileMode.Create, FileAccess.Write));

            document.Open();
            iTextSharp.text.Image image = null;
            string imgFilePath = System.IO.Path.Combine(System.IO.Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "DEILogo.jpg");
            if (File.Exists(imgFilePath))
            {
                image = iTextSharp.text.Image.GetInstance(imgFilePath);
                image.SpacingAfter = 20;
            }
            iTextSharp.text.Paragraph para = new iTextSharp.text.Paragraph("For the Period " + FromDt + " to " + ToDt, FontFactory.GetFont("Arial", 9, iTextSharp.text.Color.BLACK));
            para.SpacingAfter = 20;
            iTextSharp.text.Paragraph para1 = new iTextSharp.text.Paragraph(LoginUser.SubstoreName + " - IP PRINT TRACKING REPORT", FontFactory.GetFont("Arial", 12, 1, iTextSharp.text.Color.BLACK));
            //if (image != null)
            //    document.Add(image);
            //document.Add(para1);
            //document.Add(para);

            int pageColumnSize = 6;
            int columnIndex = 0;
            int iterateIndex = pageColumnSize;
            int k = 0;
            for (int j = 1; j <= dt.Columns.Count; j = j + pageColumnSize)
            {
                if (iterateIndex > dt.Columns.Count)
                    iterateIndex = dt.Columns.Count;
                PdfPTable table = new PdfPTable(iterateIndex - columnIndex);
                document.NewPage();
                if (image != null)
                    document.Add(image);
                document.Add(para1);
                document.Add(para);

                table.WidthPercentage = 100;
                for (int column = columnIndex; column < iterateIndex; column++)
                {
                    PdfPCell cell = new PdfPCell(new Phrase(dt.Columns[column].ToString(), headerFont));
                    cell.BackgroundColor = iTextSharp.text.Color.LIGHT_GRAY;
                    table.AddCell(cell);
                }
                foreach (DataRow dr in dt.Rows)
                {
                    for (int column = columnIndex; column < iterateIndex; column++)
                    {
                        string value = dr[column].ToString();
                        PdfPCell cellrow = new PdfPCell(new Phrase(value, rowfont));
                        table.AddCell(cellrow);
                    }
                }
                k = k + pageColumnSize;
                if (k > dt.Columns.Count)
                    k = iterateIndex;
                if (k == iterateIndex)
                {
                    document.Add(table);

                    columnIndex = k;
                    iterateIndex = iterateIndex + pageColumnSize;
                }
            }

            document.Close();
        }

        /// <summary>
        /// Handles the Click event of the btnExporttopdf control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void btnExporttopdf_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                dlg.FileName = filename;
                dlg.DefaultExt = ".pdf";
                dlg.Filter = "Pdf documents (.pdf)|*.pdf";
                if (IsEnabledExportToAnyDrive() == false)
                {
                    dlg.FileOk += dlg_FileOk;
                    DriveInfo drives = DriveInfo.GetDrives().Where(drive => drive.DriveType == DriveType.Removable && drive.IsReady == true).FirstOrDefault();
                    if (drives != null)
                    {
                        dlg.InitialDirectory = drives.Name;
                    }
                    else
                    {
                        MessageBox.Show("No removable device found.");
                        return;
                    }
                }
                Nullable<bool> result = dlg.ShowDialog();
                if (result == true)
                {
                    string filename2 = dlg.FileName;
                    if (cmbReportType.SelectedValue.ToString() == "11")
                    {
                        if (dtDataToExport != null)
                            ExportToPDF(dtDataToExport, filename2);
                    }
                    else
                    {
                        report.ExportToDisk(ExportFormatType.PortableDocFormat, filename2);
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
            finally
            {
                // if (dlg.FileOk.IsNull()==false )
                dlg.FileOk -= dlg_FileOk;
            }
        }

        bool IsEnabledExportToAnyDrive()
        {
            bool returnValue = false;
            var item = new ConfigBusiness().GetConfigurationData(LoginUser.SubStoreId);
            if (item != null)
                returnValue = Convert.ToBoolean(item.IsExportReportToAnyDrive);
            return returnValue;
        }

        private void dlg_FileOk(object sender, System.ComponentModel.CancelEventArgs e)
        {
            FileInfo f = new FileInfo(dlg.FileName);
            string s = f.Directory.Root.ToString();
            if (s.Length > 0 && s.Substring(0, 1) == @"\")
            {
                MessageBox.Show("You can save to removable device only.");
                e.Cancel = true;
            }
            else
            {
                DriveInfo df = new DriveInfo(s);
                if (df.DriveType != DriveType.Removable)
                {
                    MessageBox.Show("You can save to removable device only.");
                    e.Cancel = true;
                }
            }
        }

        /// <summary>
        /// Handles the SelectionChanged event of the cmbReportType control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="SelectionChangedEventArgs"/> instance containing the event data.</param>
        private void cmbReportType_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                if (cmbReportType.SelectedIndex > -1)
                {
                    try
                    {
                        var selectedItem = (System.Collections.Generic.KeyValuePair<string, string>)cmbReportType.SelectedItem;
                        var reportType = ReportTypeDetail.SingleOrDefault(rpt => rpt.ReportLabel.Trim().ToLower().Equals(selectedItem.Key.ToString().Trim().ToLower()) && rpt.IsActive);
                        if (reportType != null)
                            btnExportToCSV.Visibility = rdbtnCSV.Visibility = Visibility.Visible;
                        else
                            btnExportToCSV.Visibility = rdbtnCSV.Visibility = Visibility.Collapsed;
                    }
                    catch (Exception ex)
                    {
                        string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                        ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                    }
                }
                if (cmbReportType.SelectedValue != null)
                {
                    if (cmbReportType.SelectedValue.ToString() == "11")
                    {
                        ActivityReportviewer.Visibility = Visibility.Collapsed;
                        if (gridReport.Items.Count > 0)
                            gridReport.Visibility = Visibility.Visible;
                        txtPackages.Visibility = Visibility.Visible;
                        cmbPackages.Visibility = Visibility.Visible;
                    }
                    else
                    {
                        ActivityReportviewer.Visibility = Visibility.Visible;
                        gridReport.Visibility = Visibility.Collapsed;
                        txtPackages.Visibility = Visibility.Collapsed;
                        cmbPackages.Visibility = Visibility.Collapsed;

                    }
                    if (cmbReportType.SelectedValue.ToString() == "0") //Activity Reports
                    {
                        cmbPhotographer.Visibility = System.Windows.Visibility.Visible;
                        txtuser.Visibility = System.Windows.Visibility.Visible;
                        cmbLocation.Visibility = System.Windows.Visibility.Collapsed;
                        txtlocation.Visibility = System.Windows.Visibility.Collapsed;
                        chkChecked.Visibility = Visibility.Collapsed;
                        dtFrom1.Visibility = Visibility.Collapsed;
                        dtTo1.Visibility = Visibility.Collapsed;
                        from1.Visibility = Visibility.Collapsed;
                        to1.Visibility = Visibility.Collapsed;
                        txtlocation.Visibility = Visibility.Collapsed;
                        cmbsubstore.Visibility = Visibility.Collapsed;
                        txtsubstore.Visibility = Visibility.Collapsed;

                        VenueLavel.Visibility = Visibility.Collapsed;
                        txtVenueTxt.Visibility = Visibility.Collapsed;
                    }
                    else if (cmbReportType.SelectedValue.ToString() == "2")
                    {
                        cmbPhotographer.Visibility = System.Windows.Visibility.Visible;
                        txtuser.Visibility = System.Windows.Visibility.Collapsed;
                        cmbPhotographer.Visibility = System.Windows.Visibility.Collapsed;
                        cmbLocation.Visibility = System.Windows.Visibility.Collapsed;
                        chkChecked.Visibility = Visibility.Visible;
                        dtFrom1.Visibility = Visibility.Visible;
                        dtTo1.Visibility = Visibility.Visible;
                        from1.Visibility = Visibility.Visible;
                        to1.Visibility = Visibility.Visible;
                        cmbsubstore.Visibility = Visibility.Collapsed;
                        txtsubstore.Visibility = Visibility.Collapsed;

                        VenueLavel.Visibility = Visibility.Collapsed;
                        txtVenueTxt.Visibility = Visibility.Collapsed;
                    }
                    else if (cmbReportType.SelectedValue.ToString() == "7")
                    {
                        cmbPhotographer.Visibility = System.Windows.Visibility.Collapsed;
                        txtuser.Visibility = System.Windows.Visibility.Collapsed;
                        cmbLocation.Visibility = System.Windows.Visibility.Collapsed;
                        txtlocation.Visibility = System.Windows.Visibility.Collapsed;
                        chkChecked.Visibility = Visibility.Collapsed;
                        dtFrom1.Visibility = Visibility.Collapsed;
                        dtTo1.Visibility = Visibility.Collapsed;
                        from1.Visibility = Visibility.Collapsed;
                        to1.Visibility = Visibility.Collapsed;
                        txtlocation.Visibility = Visibility.Collapsed;
                        cmbsubstore.Visibility = Visibility.Visible;
                        txtsubstore.Visibility = Visibility.Visible;

                        VenueLavel.Visibility = Visibility.Collapsed;
                        txtVenueTxt.Visibility = Visibility.Collapsed;
                    }
                    else if (cmbReportType.SelectedValue.ToString() == "5")
                    {
                        cmbPhotographer.Visibility = System.Windows.Visibility.Visible;
                        txtuser.Visibility = System.Windows.Visibility.Collapsed;
                        cmbPhotographer.Visibility = System.Windows.Visibility.Collapsed;
                        cmbLocation.Visibility = System.Windows.Visibility.Collapsed;
                        dtFrom1.Visibility = Visibility.Visible;
                        chkChecked.Visibility = Visibility.Visible;
                        dtTo1.Visibility = Visibility.Visible;
                        from1.Visibility = Visibility.Visible;
                        to1.Visibility = Visibility.Visible;
                        cmbsubstore.Visibility = Visibility.Collapsed;
                        txtsubstore.Visibility = Visibility.Collapsed;

                        VenueLavel.Visibility = Visibility.Collapsed;
                        txtVenueTxt.Visibility = Visibility.Collapsed;
                    }
                    else if (cmbReportType.SelectedValue.ToString() == "6")
                    {
                        cmbPhotographer.Visibility = System.Windows.Visibility.Visible;
                        txtuser.Visibility = System.Windows.Visibility.Collapsed;
                        chkChecked.Visibility = Visibility.Visible;
                        cmbPhotographer.Visibility = System.Windows.Visibility.Collapsed;
                        cmbLocation.Visibility = System.Windows.Visibility.Collapsed;
                        dtFrom1.Visibility = Visibility.Visible;
                        dtTo1.Visibility = Visibility.Visible;
                        from1.Visibility = Visibility.Visible;
                        to1.Visibility = Visibility.Visible;
                        cmbsubstore.Visibility = Visibility.Visible;
                        txtsubstore.Visibility = Visibility.Visible;

                        VenueLavel.Visibility = Visibility.Collapsed;
                        txtVenueTxt.Visibility = Visibility.Collapsed;
                    }

                    else if (cmbReportType.SelectedValue.ToString() == "3")
                    {
                        cmbPhotographer.Visibility = System.Windows.Visibility.Visible;
                        txtuser.Visibility = System.Windows.Visibility.Collapsed;
                        cmbPhotographer.Visibility = System.Windows.Visibility.Collapsed;
                        cmbLocation.Visibility = System.Windows.Visibility.Collapsed;
                        dtFrom1.Visibility = Visibility.Visible;
                        dtTo1.Visibility = Visibility.Visible;
                        dtFrom1.Visibility = Visibility.Collapsed;
                        dtTo1.Visibility = Visibility.Collapsed;
                        from1.Visibility = Visibility.Collapsed;
                        to1.Visibility = Visibility.Collapsed;
                        chkChecked.Visibility = Visibility.Collapsed;
                        cmbsubstore.Visibility = Visibility.Visible;
                        txtsubstore.Visibility = Visibility.Visible;

                        VenueLavel.Visibility = Visibility.Collapsed;
                        txtVenueTxt.Visibility = Visibility.Collapsed;
                    }
                    else if (cmbReportType.SelectedValue.ToString() == "1")
                    {
                        cmbPhotographer.Visibility = System.Windows.Visibility.Collapsed;
                        txtuser.Visibility = System.Windows.Visibility.Collapsed;
                        cmbLocation.Visibility = System.Windows.Visibility.Collapsed;
                        txtlocation.Visibility = System.Windows.Visibility.Collapsed;
                        chkChecked.Visibility = Visibility.Collapsed;
                        dtFrom1.Visibility = Visibility.Collapsed;
                        dtTo1.Visibility = Visibility.Collapsed;
                        from1.Visibility = Visibility.Collapsed;
                        to1.Visibility = Visibility.Collapsed;
                        txtlocation.Visibility = Visibility.Collapsed;
                        cmbsubstore.Visibility = Visibility.Visible;
                        txtsubstore.Visibility = Visibility.Visible;

                        VenueLavel.Visibility = Visibility.Visible;
                        txtVenueTxt.Visibility = Visibility.Visible;
                    }
                    else if (cmbReportType.SelectedValue.ToString() == "4")
                    {
                        cmbPhotographer.Visibility = System.Windows.Visibility.Collapsed;
                        txtuser.Visibility = System.Windows.Visibility.Collapsed;
                        cmbLocation.Visibility = System.Windows.Visibility.Collapsed;
                        txtlocation.Visibility = System.Windows.Visibility.Collapsed;
                        chkChecked.Visibility = Visibility.Collapsed;
                        dtFrom1.Visibility = Visibility.Collapsed;
                        dtTo1.Visibility = Visibility.Collapsed;
                        from1.Visibility = Visibility.Collapsed;
                        to1.Visibility = Visibility.Collapsed;
                        txtlocation.Visibility = Visibility.Collapsed;
                        cmbsubstore.Visibility = Visibility.Visible;
                        txtsubstore.Visibility = Visibility.Visible;

                        VenueLavel.Visibility = Visibility.Collapsed;
                        txtVenueTxt.Visibility = Visibility.Collapsed;

                    }
                    else if (cmbReportType.SelectedValue.ToString() == "7")
                    {
                        cmbPhotographer.Visibility = System.Windows.Visibility.Collapsed;
                        txtuser.Visibility = System.Windows.Visibility.Collapsed;
                        cmbLocation.Visibility = System.Windows.Visibility.Collapsed;
                        txtlocation.Visibility = System.Windows.Visibility.Collapsed;
                        chkChecked.Visibility = Visibility.Collapsed;
                        dtFrom1.Visibility = Visibility.Collapsed;
                        dtTo1.Visibility = Visibility.Collapsed;
                        from1.Visibility = Visibility.Collapsed;
                        to1.Visibility = Visibility.Collapsed;
                        txtlocation.Visibility = Visibility.Collapsed;
                        cmbsubstore.Visibility = Visibility.Visible;
                        txtsubstore.Visibility = Visibility.Visible;

                        VenueLavel.Visibility = Visibility.Collapsed;
                        txtVenueTxt.Visibility = Visibility.Collapsed;
                    }
                    else if (cmbReportType.SelectedValue.ToString() == "8")
                    {

                        cmbPhotographer.Visibility = System.Windows.Visibility.Collapsed;
                        cmbLocation.Visibility = System.Windows.Visibility.Collapsed;
                        dtFrom1.Visibility = Visibility.Collapsed;
                        dtTo1.Visibility = Visibility.Collapsed;
                        from1.Visibility = Visibility.Collapsed;
                        to1.Visibility = Visibility.Visible;
                        txtuser.Visibility = Visibility.Collapsed;
                        from1.Visibility = Visibility.Collapsed;
                        to1.Visibility = Visibility.Collapsed;
                        cmbsubstore.Visibility = Visibility.Collapsed;
                        txtsubstore.Visibility = Visibility.Collapsed;

                        VenueLavel.Visibility = Visibility.Collapsed;
                        txtVenueTxt.Visibility = Visibility.Collapsed;

                    }
                    else if (cmbReportType.SelectedValue.ToString() == "9")
                    {
                        cmbPhotographer.Visibility = System.Windows.Visibility.Collapsed;
                        txtuser.Visibility = System.Windows.Visibility.Collapsed;
                        cmbLocation.Visibility = System.Windows.Visibility.Collapsed;
                        txtlocation.Visibility = System.Windows.Visibility.Collapsed;
                        chkChecked.Visibility = Visibility.Collapsed;
                        dtFrom1.Visibility = Visibility.Collapsed;
                        dtTo1.Visibility = Visibility.Collapsed;
                        from1.Visibility = Visibility.Collapsed;
                        to1.Visibility = Visibility.Collapsed;
                        txtlocation.Visibility = Visibility.Collapsed;
                        cmbsubstore.Visibility = Visibility.Visible;
                        txtsubstore.Visibility = Visibility.Visible;

                        VenueLavel.Visibility = Visibility.Collapsed;
                        txtVenueTxt.Visibility = Visibility.Collapsed;
                    }
                    else if (cmbReportType.SelectedValue.ToString() == "10")
                    {
                        cmbPhotographer.Visibility = System.Windows.Visibility.Collapsed;
                        txtuser.Visibility = System.Windows.Visibility.Collapsed;
                        cmbLocation.Visibility = System.Windows.Visibility.Collapsed;
                        txtlocation.Visibility = System.Windows.Visibility.Collapsed;
                        chkChecked.Visibility = Visibility.Collapsed;
                        dtFrom1.Visibility = Visibility.Collapsed;
                        dtTo1.Visibility = Visibility.Collapsed;
                        from1.Visibility = Visibility.Collapsed;
                        to1.Visibility = Visibility.Collapsed;
                        txtlocation.Visibility = Visibility.Collapsed;
                        cmbsubstore.Visibility = Visibility.Visible;
                        txtsubstore.Visibility = Visibility.Visible;

                        VenueLavel.Visibility = Visibility.Collapsed;
                        txtVenueTxt.Visibility = Visibility.Collapsed;
                    }
                    else if (cmbReportType.SelectedValue.ToString() == "11")
                    {

                        cmbPhotographer.Visibility = System.Windows.Visibility.Collapsed;
                        txtuser.Visibility = System.Windows.Visibility.Collapsed;
                        cmbLocation.Visibility = System.Windows.Visibility.Collapsed;
                        txtlocation.Visibility = System.Windows.Visibility.Collapsed;
                        chkChecked.Visibility = Visibility.Collapsed;
                        dtFrom1.Visibility = Visibility.Collapsed;
                        dtTo1.Visibility = Visibility.Collapsed;
                        from1.Visibility = Visibility.Collapsed;
                        to1.Visibility = Visibility.Collapsed;
                        txtlocation.Visibility = Visibility.Collapsed;
                        cmbsubstore.Visibility = Visibility.Visible;
                        txtsubstore.Visibility = Visibility.Visible;

                        VenueLavel.Visibility = Visibility.Collapsed;
                        txtVenueTxt.Visibility = Visibility.Collapsed;
                    }
                    else if (cmbReportType.SelectedValue.ToString() == "12")
                    {
                        cmbPhotographer.Visibility = System.Windows.Visibility.Collapsed;
                        txtuser.Visibility = System.Windows.Visibility.Collapsed;
                        cmbLocation.Visibility = System.Windows.Visibility.Collapsed;
                        txtlocation.Visibility = System.Windows.Visibility.Collapsed;
                        chkChecked.Visibility = Visibility.Collapsed;
                        dtFrom1.Visibility = Visibility.Collapsed;
                        dtTo1.Visibility = Visibility.Collapsed;
                        from1.Visibility = Visibility.Collapsed;
                        to1.Visibility = Visibility.Collapsed;
                        txtlocation.Visibility = Visibility.Collapsed;
                        cmbsubstore.Visibility = Visibility.Visible;
                        txtsubstore.Visibility = Visibility.Visible;

                        VenueLavel.Visibility = Visibility.Collapsed;
                        txtVenueTxt.Visibility = Visibility.Collapsed;
                    }
                    else
                    {
                        cmbPhotographer.Visibility = System.Windows.Visibility.Collapsed;
                        txtuser.Visibility = System.Windows.Visibility.Collapsed;
                        cmbLocation.Visibility = System.Windows.Visibility.Visible;
                        chkChecked.Visibility = Visibility.Visible;
                        dtFrom1.Visibility = Visibility.Visible;
                        dtTo1.Visibility = Visibility.Visible;
                        from1.Visibility = Visibility.Visible;
                        to1.Visibility = Visibility.Visible;
                        cmbsubstore.Visibility = Visibility.Collapsed;
                        txtsubstore.Visibility = Visibility.Collapsed;

                        VenueLavel.Visibility = Visibility.Collapsed;
                        txtVenueTxt.Visibility = Visibility.Collapsed;

                    }
                }

                VenueLavel.IsChecked = false;
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }

        private void FillProductCombo()
        {
            try
            {
                var productTypes = (new ProductBusiness()).GetPackageType();
                //var primaryProduct = productTypes.Where(t => t.DG_IsPrimary == true).ToList();
                FrameworkHelper.CommonUtility.BindComboWithSelect<ProductTypeInfo>(cmbPackages, productTypes, "DG_Orders_ProductType_Name", "DG_Orders_ProductType_pkey", 0, DigiPhoto.Utility.Repository.Data.ClientConstant.SelectString);
                cmbPackages.SelectedIndex = 0;
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }

        /// <summary>
        /// Handles the Checked event of the chkChecked control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void chkChecked_Checked(object sender, RoutedEventArgs e)
        {

        }

        /// <summary>
        /// Handles the Unloaded event of the Window control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void Window_Unloaded(object sender, RoutedEventArgs e)
        {
            report.Close();
            report.Dispose();
        }

        private void SetDBLogonForSubreports(ReportDocument report)
        {
            string connectString = System.Configuration.ConfigurationManager.ConnectionStrings["DigiConnectionString"].ToString();
            System.Data.SqlClient.SqlConnectionStringBuilder builder = new System.Data.SqlClient.SqlConnectionStringBuilder(connectString);

            ConnectionInfo crConnectionInfo = new ConnectionInfo();
            crConnectionInfo.ServerName = builder.DataSource;
            crConnectionInfo.DatabaseName = builder.InitialCatalog;
            crConnectionInfo.UserID = builder.UserID;
            crConnectionInfo.Password = builder.Password;

            TableLogOnInfo crTableLogoninfo = new TableLogOnInfo();
            foreach (ReportDocument subreport in report.Subreports)
            {
                foreach (CrystalDecisions.CrystalReports.Engine.Table CrTable in subreport.Database.Tables)
                {
                    crTableLogoninfo = CrTable.LogOnInfo;
                    crTableLogoninfo.ConnectionInfo = crConnectionInfo;
                    CrTable.ApplyLogOnInfo(crTableLogoninfo);
                }
            }
        }
        private void SetDBLogonReports(ReportDocument report)
        {
            string connectString = System.Configuration.ConfigurationManager.ConnectionStrings["DigiConnectionString"].ToString();
            System.Data.SqlClient.SqlConnectionStringBuilder builder = new System.Data.SqlClient.SqlConnectionStringBuilder(connectString);

            ConnectionInfo crConnectionInfo = new ConnectionInfo();
            crConnectionInfo.ServerName = builder.DataSource;
            crConnectionInfo.DatabaseName = builder.InitialCatalog;
            crConnectionInfo.UserID = builder.UserID;
            crConnectionInfo.Password = builder.Password;
            report.SetDatabaseLogon(crConnectionInfo.UserID, crConnectionInfo.Password, crConnectionInfo.DatabaseName, crConnectionInfo.ServerName);
        }
        private void btnMail_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                string otherMessage = string.Empty;
                if (!sendMailFlag)
                {
                    MessageBox.Show("Generate Report Then Click On E-mail ");
                    return;
                }

                //copy files 
                string docName = "abc.xls";
                if (cmbReportType.SelectedIndex > -1)
                {
                    if (rdbtnExcel.IsChecked == true)
                    {
                        docName = cmbReportType.SelectedValue.ToString() + ".xls";
                        otherMessage = cmbReportType.SelectedItem.ToString().ToString().Split(',').First().Replace("[", string.Empty).ToString();
                    }
                    else if (rdbtnCSV.IsChecked == true)
                    {
                        docName = cmbReportType.SelectedValue.ToString() + ".csv";
                        otherMessage = cmbReportType.SelectedItem.ToString().ToString().Split(',').First().Replace("[", string.Empty).ToString();
                    }
                    else
                    {
                        docName = cmbReportType.SelectedValue.ToString() + ".pdf";
                        otherMessage = cmbReportType.SelectedItem.ToString().ToString().Split(',').First().Replace("[", string.Empty).ToString();
                    }
                    //docName = cmbReportType.SelectedValue.ToString() + ".xls";
                    //otherMessage = cmbReportType.SelectedItem.ToString().ToString().Split(',').First().Replace("[", string.Empty).ToString();
                }
                DateTime serverDate = (new CustomBusineses()).ServerDateTime();
                var fileName = LoginUser.UserId.ToString() + LoginUser.StoreId.ToString() + LoginUser.SubStoreId.ToString() + serverDate.Hour.ToString() + serverDate.Minute.ToString() + serverDate.Second.ToString();

                string folderName = serverDate.Date.ToString("yyyyMMdd");
                fileName = otherMessage + "_" + fileName + docName;
                string folderPath = System.IO.Path.Combine(LoginUser.DigiReportPath, folderName);
                if (!Directory.Exists(folderPath))
                    System.IO.Directory.CreateDirectory(folderPath);
                if (cmbReportType.SelectedValue.ToString() == "11")
                {
                    if (dtDataToExport == null)
                    {
                        MessageBox.Show("Generate Report Then Click On E-mail ");
                        return;
                    }

                    if (rdbtnExcel.IsChecked == true)
                    {
                        ExportToExcel(dtDataToExport, System.IO.Path.Combine(folderPath, fileName + "x"));
                        //ExportToExcel1(dtDataToExport, "E:\\Vinod\\");
                        fileName = fileName + "x";
                    }
                    else if (rdbtnCSV.IsChecked == true && ReportSource != null && ReportSource.Rows.Count > 0)
                    {
                        ReportSource.ExportToCSV("vw_GetActivityReports.DG_", System.IO.Path.Combine(folderPath, fileName));
                    }
                    else
                        ExportToPDF(dtDataToExport, System.IO.Path.Combine(folderPath, fileName));

                }
                else
                {

                    if (rdbtnExcel.IsChecked == true)
                    {
                        report.ExportToDisk(ExportFormatType.Excel, System.IO.Path.Combine(folderPath, fileName));
                    }
                    else if (rdbtnCSV.IsChecked == true && ReportSource != null && ReportSource.Rows.Count > 0)
                    {
                        ReportSource.ExportToCSV("vw_GetActivityReports.DG_", System.IO.Path.Combine(folderPath, fileName));
                    }
                    else if (rdbtnPDF.IsChecked == true)
                    {
                        report.ExportToDisk(ExportFormatType.PortableDocFormat, System.IO.Path.Combine(folderPath, fileName));
                    }
                }

                var objEmailInfo = new EMailInfo
                {
                    StoreId = LoginUser.StoreId.ToString(),
                    UserId = LoginUser.UserId.ToString(),
                    OrderId = System.IO.Path.Combine(folderPath, fileName).ToString(),
                    Sendername = LoginUser.UserName,
                    SubstoreId = LoginUser.SubStoreId.ToString(),
                    OtherMessage = otherMessage
                };
                MailPopUp.ShowHandlerDialog(objEmailInfo);
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
            finally
            {

            }
        }

        DataTable _sourceTable;
        public DataTable SourceTable
        {
            get
            {

                return _sourceTable;
            }
            set
            {
                _sourceTable = value;
                NotifyPropertyChanged("SourceTable");

            }
        }
        public event PropertyChangedEventHandler PropertyChangedSource;

        private void NotifyPropertyChanged(String info)
        {
            if (PropertyChangedSource != null)
            {
                PropertyChangedSource(this, new PropertyChangedEventArgs(info));
            }
        }
        //public DataTable SourceTable
        //{
        //    get
        //    {

        //        return _sourceTable;
        //    }
        //    set
        //    {
        //        _sourceTable = value;
        //        NotifyPropertyChanged("SourceTable");

        //    }
        //}
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(PropertyChangedEventArgs e)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, e);
            }
        }

        //public delegate void PropertyChangedEventHandler(object sender, PropertyChangedEventArgs e);
        public class TryCatch
        {
            public static void BeginTryCatch(Action<CommandArgs> function, CommandArgs obj)
            {
                try
                {
                    function.Invoke(obj);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
            public static void BeginTryCatch(Action<object> function, object obj)
            {
                try
                {
                    function.Invoke(obj);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
            public static void BeginTryCatch(Action function)
            {
                try
                {
                    function.Invoke();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                    //You can log error here for e.g you can use log4net
                }
            }
        }

        private void btnExportToCSV_Click(object sender, RoutedEventArgs e)
        {
            #region Old code
            //try
            //{
            //    dlg.FileName = filename;
            //    dlg.DefaultExt = ".csv";
            //    dlg.Filter = "CSV documents (.csv)|*.csv";
            //    if (IsEnabledExportToAnyDrive() == false)
            //    {
            //        dlg.FileOk += dlg_FileOk;
            //        DriveInfo drives = DriveInfo.GetDrives().Where(drive => drive.DriveType == DriveType.Removable && drive.IsReady == true).FirstOrDefault();
            //        if (drives != null)
            //        {
            //            dlg.InitialDirectory = drives.Name;

            //        }
            //        else
            //        {
            //            MessageBox.Show("No removable device found.");
            //            return;
            //        }
            //    }
            //    Nullable<bool> result = dlg.ShowDialog();
            //    if (result == true)
            //    {
            //        string filename2 = dlg.FileName;
            //        if (cmbReportType.SelectedValue.ToString() == "11")
            //        {
            //            ExportToCSV(filename2);
            //        }
            //        else
            //        {
            //            MessageBox.Show("CSV Foramt availble for only IP Print Tracking Report.");
            //        }

            //    }

            //}
            //catch (Exception ex)
            //{
            //    string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
            //    ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            //}
            //finally
            //{
            //    // if (dlg.FileOk.IsNull()==false)
            //    dlg.FileOk -= dlg_FileOk;
            //}
            #endregion
            try
            {
                //if (ReportSource == null || cmbReportType.SelectedValue.ToString() != "11")
                //    return;
                dlg.FileName = filename;
                dlg.DefaultExt = ".csv";
                dlg.Filter = "CSV files (.csv)|*.csv";
                if (IsEnabledExportToAnyDrive() == false)
                {
                    dlg.FileOk += dlg_FileOk;
                    DriveInfo drives = DriveInfo.GetDrives().Where(drive => drive.DriveType == DriveType.Removable && drive.IsReady == true).FirstOrDefault();
                    if (drives != null)
                    {
                        dlg.InitialDirectory = drives.Name;
                    }
                    else
                    {
                        MessageBox.Show("No removable device found.");
                        return;
                    }
                }
                Nullable<bool> result = dlg.ShowDialog();
                if (result == true)
                {
                    string filename2 = dlg.FileName;
                    if (ReportSource != null)
                        // By KCB ON 28 MAR 2018 For rectifying var amount
                        //ReportSource.ExportToCSV("vw_GetActivityReports.DG_", filename2);
                        report.ExportToDisk(ExportFormatType.ExcelRecord, filename2);
                    //END
                    //else if(cmbReportType.SelectedValue.ToString() == "11")
                    //    ExportToCSV(filename2);
                }

            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
            finally
            {
                // if (dlg.FileOk.IsNull()==false)
                dlg.FileOk -= dlg_FileOk;
            }
        }
        public class CommandArgs
        {
            public object CommandParameters { get; set; }
            public object OriginalArgs { get; set; }
            public object OriginalSource { get; set; }
        }

        private void ActivityReportviewer_ViewZoom(object source, SAPBusinessObjects.WPF.Viewer.ZoomEventArgs e)
        {

        }

        /// <summary>
        /// venue report visible on basis of venue level check or uncheck
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void VenueLavel_Click(object sender, RoutedEventArgs e)
        {
            if (VenueLavel.IsChecked == true)
            {
                FillSubStoreCombo();
                Console.WriteLine("checked");
                Console.Write("checked");
                VenueLavel.Visibility = Visibility.Visible;
                txtVenueTxt.Visibility = Visibility.Visible;
                cmbsubstore.Visibility = Visibility.Collapsed;
                txtsubstore.Visibility = Visibility.Collapsed;
            }
            else
            {

                Console.WriteLine("unchecked");
                Console.Write("unchecked");
                VenueLavel.Visibility = Visibility.Visible;
                txtVenueTxt.Visibility = Visibility.Visible;

                cmbsubstore.Visibility = Visibility.Visible;
                txtsubstore.Visibility = Visibility.Visible;
            }
        }
    }
}

