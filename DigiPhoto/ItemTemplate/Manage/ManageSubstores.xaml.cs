﻿using DigiAuditLogger;
using DigiPhoto.Common;
using DigiPhoto.DataLayer;
using DigiPhoto.IMIX.Business;
using DigiPhoto.IMIX.Model;
using DigiPhoto.Utility.Repository.ValueType;
using FrameworkHelper;
using System;
using System.Windows;
using System.Windows.Controls;
using System.Collections.Generic;
using DigiPhoto.Utility.Repository.Data;
using System.Linq;

namespace DigiPhoto.Manage
{
    public partial class ManageSubstores : Window
    {
        #region Declaration
        //public DigiPhotoDataServices _objDbLayer;
        public int _subStoreId = 0;
        public List<SubStoresInfo> lstStoreSubStore;
        #endregion

        #region Constructor
        /// <summary>
        /// Initializes a new instance of the <see cref="ManageSubstores"/> class.
        /// </summary>
        public ManageSubstores()
        {
            try
            {
                InitializeComponent();
                FillSubstore();
                ddlLogicalSiteName.SelectedIndex = 0;
                //_objDbLayer = new DigiPhotoDataServices();
                GetSubstoreData();
                GetSiteCode();
                txbUserName.Text = LoginUser.UserName;
                txbStoreName.Text = LoginUser.StoreName;
                btnSave.IsDefault = true;
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
            }

        }
        #endregion

        #region Common Method

        /// <summary>
        /// Determines whether this instance is validate.
        /// </summary>
        /// <returns></returns>
        public bool IsValidate()
        {
            bool retValue = true;
            if (string.IsNullOrEmpty(txtSubStoreName.Text.Trim()))
            {
                MessageBox.Show("Please enter Substore name");
                retValue = false;
            }
            return retValue;
        }

        private void FillSubstore()
        {
            try
            {
                lstStoreSubStore = (new StoreSubStoreDataBusniess()).GetAllLogicalSubstoreName();
                if (lstStoreSubStore.Count <= 0)
                    ErrorHandler.ErrorHandler.LogFileWrite("Site not found.Please update site details with respective site code.");
                List<SubStoresInfo> lstStoreSubStore1 = new List<SubStoresInfo>();
                lstStoreSubStore1 = lstStoreSubStore.Where(t => t.IsLogicalSubStore == true).ToList();
                CommonUtility.BindComboWithSelect<SubStoresInfo>(ddlLogicalSiteName, lstStoreSubStore1, "DG_SubStore_Name", "DG_SubStore_pkey", 0, ClientConstant.SelectString);
                ddlLogicalSiteName.SelectedValue = LoginUser.SubStoreId.ToString();

            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
            }
        }

        /// <summary>
        /// Gets the substore data.
        /// </summary>
        public void GetSubstoreData()
        {
            //DGManageSubStore.ItemsSource=_objDbLayer.GetSubstoreData();
            DGManageSubStore.ItemsSource = lstStoreSubStore;// (new StoreSubStoreDataBusniess()).GetSubstoreDataFillGrid();
        }
        public void GetSiteCode()
        {
            List<SiteCodeDetail> siteCodeDetail = new List<SiteCodeDetail>();
            siteCodeDetail = (new StoreSubStoreDataBusniess()).GetSiteCodeBusiness();
            CommonUtility.BindComboWithSelect<SiteCodeDetail>(cmbSiteCode, siteCodeDetail, "SiteCode", "SiteId", 0, ClientConstant.SelectString);
            cmbSiteCode.ItemsSource = siteCodeDetail;
            cmbSiteCode.SelectedIndex = 0;
            cmbSiteCode.DisplayMemberPath = "SiteCode";
            cmbSiteCode.SelectedValuePath = "SiteId";
        }

        #endregion

        #region Events
        /// <summary>
        /// Handles the Click event of the btnSave control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                //if (_subStoreId != 0)
                //{
                if (cmbSiteCode.SelectedIndex > 0)
                {
                    var Useritem = lstStoreSubStore.Where(t => t.SiteID == Convert.ToInt32(cmbSiteCode.SelectedValue) && t.DG_SubStore_pkey != _subStoreId && (t.LogicalSubStoreID == 0 ? null : t.LogicalSubStoreID) != _subStoreId).ToList();

                    if (chkLogicalSite.IsChecked == true)
                    {
                        var IsLogical = Useritem.Where(s => s.SiteID == Convert.ToInt32(cmbSiteCode.SelectedValue) && s.IsLogicalSubStore == true).FirstOrDefault();
                        if (IsLogical != null)
                        {
                            MessageBox.Show("This site code is already mapped.");
                            return;
                        }
                    }
                    else if (ddlLogicalSiteName.SelectedIndex == 0)
                    {
                        var Exist = Useritem.Where(s => s.SiteID == Convert.ToInt32(cmbSiteCode.SelectedValue)).FirstOrDefault();
                        //var def = Useritem.Where(s => s.SiteID == Convert.ToInt32(cmbSiteCode.SelectedValue) && s.IsLogicalSubStore == false).FirstOrDefault();
                        if (Exist != null)
                        {
                            MessageBox.Show("This site code is already mapped.");
                            return;
                        }
                    }
                }
                //}

                if (IsValidate())
                {
                    string SyncCode = CommonUtility.GetUniqueSynccode(Convert.ToInt32(ApplicationObjectEnum.Location).ToString().PadLeft(2, '0'), LoginUser.countrycode, LoginUser.Storecode, "00");
                    // string SyncCode = DigiConfigUtility.Utility.CommonUtility.GetRandomString(8) + Convert.ToInt32(ApplicationObjectEnum.Location).ToString().PadLeft(2, '0');
                    //bool retvalue = _objDbLayer.SetSubStoreDetails(txtSubStoreName.Text, txtDescription.Text, _subStoreId, SyncCode);
                    bool haslogical = chkLogicalSite.IsChecked == true ? true : false;
                    int logicalSubStoreID = (ddlLogicalSiteName.SelectedValue != null) ? Int32.Parse(ddlLogicalSiteName.SelectedValue.ToString()) : 0;
                    bool retvalue = false;
                    //if (chkLogicalSite.IsChecked == true)
                    //{
                    //    if (ddlLogicalSiteName.SelectedIndex == 0)
                    //    {
                    //        MessageBox.Show("Please select logical site name", "DEI");
                    //        ddlLogicalSiteName.Focus();
                    //        return;
                    //    }
                    //    else
                    //    {
                    //         retvalue = (new StoreSubStoreDataBusniess()).SetSubStoreDetails(txtSubStoreName.Text, txtDescription.Text, _subStoreId, SyncCode, haslogical, null);
                    //    }
                    //}
                    //else
                    //{
                    //     retvalue = (new StoreSubStoreDataBusniess()).SetSubStoreDetails(txtSubStoreName.Text, txtDescription.Text, _subStoreId, SyncCode, haslogical, Int32.Parse(ddlLogicalSiteName.SelectedValue.ToString()));
                    //}
                    if (logicalSubStoreID > 0)
                    {
                        if (cmbSiteCode.SelectedIndex != 0)
                            retvalue = (new StoreSubStoreDataBusniess()).SetSubStoreDetails(txtSubStoreName.Text, txtDescription.Text, _subStoreId, SyncCode, haslogical, logicalSubStoreID, cmbSiteCode.Text.Split('-').Last());
                        else
                        {
                            MessageBox.Show("Please Select Site Code");
                            cmbSiteCode.Focus();
                            return;
                        }
                    }
                    else
                    {
                        if (cmbSiteCode.SelectedIndex != 0)
                            retvalue = (new StoreSubStoreDataBusniess()).SetSubStoreDetails(txtSubStoreName.Text, txtDescription.Text, _subStoreId, SyncCode, haslogical, null, cmbSiteCode.Text.Split('-').Last());
                        else
                        {
                            MessageBox.Show("Please Select Site Code");
                            cmbSiteCode.Focus();
                            return;
                        }
                    }
                    if (retvalue)
                    {


                        if (_subStoreId != 0)
                            AuditLog.AddUserLog(Common.LoginUser.UserId, (int)FrameworkHelper.ActionType.EditSubStore, "Edit a Substore at");
                        else
                            AuditLog.AddUserLog(Common.LoginUser.UserId, (int)FrameworkHelper.ActionType.AddSubStore, "Add a Substore at");
                        if (_subStoreId == 0)
                            MessageBox.Show("Record saved successfully");
                        else
                            MessageBox.Show("Record updated successfully");
                        txtDescription.Text = string.Empty;
                        txtSubStoreName.Text = string.Empty;
                        _subStoreId = 0;
                        chkLogicalSite.IsChecked = false;
                        ddlLogicalSiteName.SelectedIndex = 0;
                        cmbSiteCode.SelectedIndex = 0;

                        cmbSiteCode.IsEnabled = true;
                        chkLogicalSite.IsEnabled = true;

                        // chkLogicalSite.Visibility = Visibility.Collapsed;
                        //  ddlLogicalSiteName.Visibility = Visibility.Collapsed;

                    }
                    else
                    {
                        MessageBox.Show("Problem while saving record.");
                    }
                    FillSubstore();
                    GetSubstoreData();
                    ddlLogicalSiteName.SelectedIndex = 0;
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
            }

        }
        /// <summary>
        /// Handles the Click event of the btnClear control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void btnClear_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                txtDescription.Text = string.Empty;
                txtSubStoreName.Text = string.Empty;
                _subStoreId = 0;
                chkLogicalSite.IsChecked = false;
                ddlLogicalSiteName.SelectedIndex = 0;
                cmbSiteCode.SelectedIndex = 0;

                cmbSiteCode.IsEnabled = true;
                chkLogicalSite.IsEnabled = true;

                // chkLogicalSite.Visibility = Visibility.Collapsed;
                // ddlLogicalSiteName.Visibility = Visibility.Collapsed;
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
            }
        }
        /// <summary>
        /// Handles the Click event of the btnLogout control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void btnLogout_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                AuditLog.AddUserLog(Common.LoginUser.UserId, (int)FrameworkHelper.ActionType.Logout, "Logged out at ");
                Login _objLogin = new Login();
                _objLogin.Show();
                this.Close();
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }
        /// <summary>
        /// Handles the Click event of the btnBack control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void btnBack_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                ManageHome _objManage = new ManageHome();
                _objManage.Show();
                this.Close();
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
            }
        }
        /// <summary>
        /// Handles the Click event of the btnEdit control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void btnEdit_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Button btn = (Button)(sender);
                //_objDbLayer = new DigiPhotoDataServices();
                //DG_SubStores _objSubstore=_objDbLayer.GetSubstoreData(Convert.ToInt32(btn.CommandParameter));
                SubStoresInfo _objSubstore = (new StoreSubStoreDataBusniess()).GetSubstoreData(btn.CommandParameter.ToInt32());
                if (_objSubstore != null)
                {
                    txtSubStoreName.Text = _objSubstore.DG_SubStore_Name;
                    txtDescription.Text = _objSubstore.DG_SubStore_Description;
                    _subStoreId = _objSubstore.DG_SubStore_pkey;
                    cmbSiteCode.SelectedValue = _objSubstore.SiteID;


                    chkLogicalSite.IsEnabled = false;

                    if (_objSubstore.IsLogicalSubStore == true)
                    {
                        chkLogicalSite.IsChecked = true;
                        lblLogicalsiteNmae.Visibility = Visibility.Visible;
                        ddlLogicalSiteName.Visibility = Visibility.Visible;
                        ddlLogicalSiteName.SelectedIndex = 0;
                        // chkLogicalSite.IsEnabled = false;
                        ddlLogicalSiteName.IsEnabled = false;
                        chkLogicalSite_Checked(sender, e);
                        cmbSiteCode.IsEnabled = true;

                    }
                    else
                    {
                        // chkLogicalSite.IsEnabled = true;
                        ddlLogicalSiteName.IsEnabled = true;
                        chkLogicalSite.IsChecked = false;
                        ddlLogicalSiteName.SelectedValue = _objSubstore.LogicalSubStoreID.ToString();
                        chkLogicalSite_Unchecked(sender, e);
                        if (ddlLogicalSiteName.SelectedIndex > 0)
                            cmbSiteCode.IsEnabled = false;
                        //  lblLogicalsiteNmae.Visibility = Visibility.Collapsed;
                        //ddlLogicalSiteName.Visibility = Visibility.Collapsed;
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
            }
        }
        #endregion

        /// <summary>
        /// Handles the Click event of the btnLocations control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void btnLocations_Click(object sender, RoutedEventArgs e)
        {
            AddLocationstoSubstore objNew = new AddLocationstoSubstore();
            objNew.Show();
            this.Close();
        }

        /// <summary>
        /// Handles the Click event of the btnDelete control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void btnDelete_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                string ret = string.Empty;
                Button btn = (Button)(sender);
                int subStoreId = btn.CommandParameter.ToInt32();
                if (subStoreId != 0)
                {
                    LocationBusniess locBiz = new LocationBusniess();
                    if (locBiz.IsSiteAssociatedToLocation(subStoreId))
                    {
                        string msg = UIConstant.SiteAssociation;
                        MessageBox.Show(msg);
                        return;
                    }
                    if (MessageBox.Show("Do you want to delete this Site?", "Confirm delete", MessageBoxButton.YesNo) == MessageBoxResult.Yes)
                    {
                        //if (_objDbLayer.DeleteSubstore(SubStoreId))
                        ret = new StoreSubStoreDataBusniess().DeleteSubstore(subStoreId);

                        if (string.IsNullOrEmpty(ret))
                        {

                            MessageBox.Show("Site Successfully deleted");
                            AuditLog.AddUserLog(Common.LoginUser.UserId, (int)FrameworkHelper.ActionType.DeleteSubStore, "Delete substore at:- ");
                        }
                        else
                        {
                            GetSubstoreData();
                            MessageBox.Show("You can not delete this site because this site has some dependencies, first remove those dependencies.");
                        }
                    }

                }
                chkLogicalSite.IsChecked = false;
                FillSubstore();
                GetSubstoreData();
                ddlLogicalSiteName.SelectedIndex = 0;

            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
            }
        }

        private void chkLogicalSite_Checked(object sender, RoutedEventArgs e)
        {
            //if (chkLogicalSite.IsChecked == true)
            //{
            //    lblLogicalsiteNmae.Visibility = Visibility.Visible;
            //    ddlLogicalSiteName.Visibility = Visibility.Visible;
            //}
            //else
            //{
            //    lblLogicalsiteNmae.Visibility = Visibility.Collapsed;
            //    ddlLogicalSiteName.Visibility = Visibility.Collapsed;
            //}
            ddlLogicalSiteName.IsEnabled = false;
            cmbSiteCode.IsEnabled = true;

        }

        private void chkLogicalSite_Unchecked(object sender, RoutedEventArgs e)
        {
            ddlLogicalSiteName.IsEnabled = true;
            //if (chkLogicalSite.IsChecked == false)
            //{
            //    lblLogicalsiteNmae.Visibility = Visibility.Collapsed;
            //    ddlLogicalSiteName.Visibility = Visibility.Collapsed;
            //}
            //else
            //{
            //    lblLogicalsiteNmae.Visibility = Visibility.Visible;
            //    ddlLogicalSiteName.Visibility = Visibility.Visible;
            //}
        }

        private void ddlLogicalSiteName_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (ddlLogicalSiteName.SelectedIndex > 0)
            {
                var Useritem = lstStoreSubStore.Where(t => t.DG_SubStore_pkey == Convert.ToInt32(ddlLogicalSiteName.SelectedValue)).FirstOrDefault();
                if (Useritem != null)
                {
                    cmbSiteCode.SelectedValue = Useritem.SiteID;
                    cmbSiteCode.IsEnabled = false;
                }
            }
            else
            {
                cmbSiteCode.IsEnabled = true;
                ddlLogicalSiteName.SelectedIndex = 0;
            }
        }
    }
}
