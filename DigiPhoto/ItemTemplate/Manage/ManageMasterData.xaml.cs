﻿using DigiAuditLogger;
using DigiPhoto.Common;
using DigiPhoto.IMIX.Business;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Xml.Serialization;

namespace DigiPhoto.Manage
{
    /// <summary>
    /// Interaction logic for ManageMasterData.xaml
    /// </summary>
    public partial class ManageMasterData : Window
    {
        string _filedatasource = string.Empty;
        string packagefilename = string.Empty;
        DigiPhoto.IMIX.Business.CurrencyExchangeBussiness _objCurrencyBusiness = null;
        System.ComponentModel.BackgroundWorker BackupWorker;
        BusyWindow bs = new BusyWindow();

        DataTable _dtExcel;
        DataTable _dt;
        DataTable dtSite;
        DataTable dtItem;
        DataTable dtpkg;
        public ManageMasterData()
        {
            InitializeComponent();
        }
        private void btnBrowse_Click(object sender, RoutedEventArgs e)
        {
            //try
            //{
            //    Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog();

            //    // Set filter for file extension and default file extension 
            //    dlg.DefaultExt = ".xls";
            //    dlg.Filter = "Excel Files(.xls)|*.xls| Excel Files(.xlsx)|*.xlsx| Excel Files(*.xlsm)|*.xlsm";

            //    // Display OpenFileDialog by calling ShowDialog method 
            //    Nullable<bool> result = dlg.ShowDialog();

            //    // Get the selected file name and display in a TextBox 
            //    if (result == true)
            //    {
            //        // Open document 
            //        string filename = dlg.FileName;
            //        txtFileName.Text = filename;
            //    }

            //}
            //catch (Exception ex)
            //{
            //    string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
            //    ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            //}
        }
       
        private void btnLogout_Click(object sender, RoutedEventArgs e)
        {
            AuditLog.AddUserLog(Common.LoginUser.UserId, (int)FrameworkHelper.ActionType.Logout, "Logged out at");
            Login _objLogin = new Login();
            _objLogin.Show();
            this.Close();
        }
        private void btnBack_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                ManageHome _objHome = new ManageHome();
                _objHome.Show();
                this.Close();
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }

        private void Import(string filedatasource)
        {


            if (filedatasource != string.Empty)
            {
                try
                {
                    XmlSerializer serializer = new XmlSerializer(typeof(OfflineMasterDataFile));
                    OfflineMasterDataFile result;
                    using (FileStream fileStream = new FileStream(filedatasource, FileMode.Open))
                    {
                        result = (OfflineMasterDataFile)serializer.Deserialize(fileStream);
                    }


                    if (LoginUser.countrycode.ToLower() == result.Venue.CountryCode.ToLower() && LoginUser.StoreName.ToLower() == result.Venue.VenueName.ToLower() && LoginUser.Storecode.ToLower() == result.Venue.VenueCode.ToLower())
                    {
                        var CodeLong = (from c in result.Venue.SiteDetails
                                        where c.SiteCode.Length > 4
                                        select c).FirstOrDefault();
                        if (CodeLong == null)
                        {
                            List<Site> SiteDetails = new List<Site>();
                            List<Item> ItemDetails = new List<Item>();
                            List<PackageDetail> PackageDetails = new List<PackageDetail>();

                            foreach (Site site in result.Venue.SiteDetails)
                            {
                                SiteDetails.Add(site);
                                foreach (Item item in site.ItemDetails)
                                {
                                    item.ItemSiteSyncCode = site.SiteSyncCode;
                                    ItemDetails.Add(item);
                                    foreach (PackageDetail packagedetail in item.PackageKittingDetails)
                                    {
                                        packagedetail.PkgSiteSyncCode = site.SiteSyncCode;
                                        packagedetail.pkgitemcode = item.ItemCode;
                                        PackageDetails.Add(packagedetail);
                                    }
                                }
                            }
                            dtpkg = ConvertListToDataTable(PackageDetails);
                            dtSite = ToDataTable<Site>(SiteDetails);
                            dtItem = ToDataTable<Item>(ItemDetails);
                            // PackageDetails.tota
                            //here needs to create site detail package nd package details table and move to ahead


                            DataView view = new System.Data.DataView(dtSite);
                            dtSite = view.ToTable("Selected", false, "SiteName", "SiteCode", "SiteSyncCode", "IsLogical", "LogicalSyncCode");

                            view = new System.Data.DataView(dtItem);
                            dtItem = view.ToTable("Selected", false, "ItemName", "ItemCode", "IsPackage", "IsAccessory", "IsActive", "ItemSyncCode", "Price", "ItemSiteSyncCode", "ItemDescription");

                            view = new System.Data.DataView(dtpkg);
                            dtpkg = view.ToTable("Selected", false, "ItemName", "ItemCode", "IsPackage", "IsAccessory", "IsActive", "ItemSyncCode", "Price", "MaxImages", "PkgSiteSyncCode", "ItemDescription", "pkgitemcode");


                            bs.Show();
                            BackupWorker.RunWorkerAsync();
                        }
                        else
                        {
                            MessageBox.Show("The file contain wrong site code.");
                        }
                    }
                    else
                    {
                        MessageBox.Show("The file does not contain the valid Country/Venue.");
                    }
                    //GetProfileData();
                }
                catch (InvalidOperationException IOE)
                {
                    MessageBox.Show("Invalid xml file.");
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message.ToString());
                }
            }
        }
        public DataTable ToDataTable<T>(List<T> items)
        {
            DataTable dataTable = new DataTable(typeof(T).Name);
            //Get all the properties  
            PropertyInfo[] Props = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);
            // Loop through all the properties  
            foreach (PropertyInfo prop in Props)
            {
                //Setting column names as Property names  
                dataTable.Columns.Add(prop.Name);
            }
            foreach (T item in items)
            {
                var values = new object[Props.Length];
                for (int i = 0; i < Props.Length; i++)
                {
                    //inserting property values to datatable rows  
                    values[i] = Props[i].GetValue(item, null);
                }
                // Finally add value to datatable  
                dataTable.Rows.Add(values);
            }
            //put a breakpoint here and check datatable of return values  
            return dataTable;
        }
        static DataTable ConvertListToDataTable(List<PackageDetail> list)
        {
            DataTable dataTable = new DataTable(typeof(PackageDetail).Name);
            //Get all the properties  
            PropertyInfo[] Props = typeof(PackageDetail).GetProperties(BindingFlags.Public | BindingFlags.Instance);
            PropertyInfo[] Props1 = typeof(Item).GetProperties(BindingFlags.Public | BindingFlags.Instance);
            // Loop through all the properties  
            foreach (PropertyInfo prop in Props)
            {
                //Setting column names as Property names  
                if (prop.Name == "BaseProduct")
                {
                    
                    foreach (PropertyInfo prop1 in Props1)
                    {
                        dataTable.Columns.Add(prop1.Name);
                    }
                }
                else
                {
                    dataTable.Columns.Add(prop.Name);
                }

            }
            foreach (PackageDetail item in list)
            {
                int cnt = 0;
                var values = new object[Props.Length+Props1.Length-1];
                for (int i = 0; i < Props.Length; i++)
                {
                    if (i == 1)
                    {
                        for (int j = 0; j < Props1.Length; j++)
                        {
                            values[i + j] = Props1[j].GetValue(item.BaseProduct, null);
                        }
                        cnt = Props.Length + Props1.Length - 3;
                    }
                    else
                    {
                        //inserting property values to datatable rows
                        
                            values[cnt] = Props[i].GetValue(item, null);

                            if(i != 0)
                              cnt = cnt + 1;
                    }
                   
                }
                // Finally add value to datatable  
                dataTable.Rows.Add(values);
            }
            //put a breakpoint here and check datatable of return values  
            return dataTable;
        }
    

        private void btnUpload_Click(object sender, RoutedEventArgs e)
        {
            BackupWorker = new System.ComponentModel.BackgroundWorker();
            BackupWorker.DoWork += new System.ComponentModel.DoWorkEventHandler(BackupWorker_DoWork);
            BackupWorker.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(BackupWorker_RunWorkerCompleted);
            System.Windows.Forms.OpenFileDialog fdlg = new System.Windows.Forms.OpenFileDialog();
            fdlg.Title = "Select file";
            fdlg.Filter = "XML File(*.xml)|*.xml|All Files(*.*)|*.*";
            fdlg.FilterIndex = 1;
            fdlg.RestoreDirectory = true;
            if (fdlg.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                _filedatasource = fdlg.FileName;
                string[] filename = fdlg.FileName.Split('\\');
                packagefilename = filename.Last();
                string[] fileext = packagefilename.Split('.');
                if (fileext[1] == "xml")
                {
                    Import(_filedatasource);
                    System.Windows.Forms.Application.DoEvents();
                }
                else
                {
                    MessageBox.Show("Please select a valid file(.xml)");
                }

            }
        }

        private void BackupWorker_DoWork(object Sender, System.ComponentModel.DoWorkEventArgs e)
        {
            bool result = (new CommonBusiness()).ImportMasterData(dtSite, dtItem, dtpkg);
            if (result)
            {
                MessageBox.Show("Master Data imported successfully.");
            }
            else
                MessageBox.Show("There is some error while importing master data");
        }
        private void BackupWorker_RunWorkerCompleted(object Sender, System.ComponentModel.RunWorkerCompletedEventArgs e)
        {
            bs.Hide();
            //GetProfileData();
        }
        private bool IsAllColumnExist(DataTable tableNameToCheck)
        {
            List<string> columnsNames = new List<string>();

            //columnsNames.Add("CurrencyProfileID");
            columnsNames.Add("ProfileName");
            columnsNames.Add("StartDate");
            columnsNames.Add("EndDate");
            //columnsNames.Add("CurrencyProfileRateID");
            columnsNames.Add("CurrencyRate");
            // columnsNames.Add("CurrencyMasterID");
            columnsNames.Add("CurrencyName");
            columnsNames.Add("CurrencyCode");
            columnsNames.Add("PublishedOn");
            //columnsNames.Add("VenueCode");
            columnsNames.Add("VenueName");
            // columnsNames.Add("CountryCode");
            columnsNames.Add("CountryName");
            bool iscolumnExist = true;
            try
            {
                if (null != tableNameToCheck && tableNameToCheck.Columns != null)
                {
                    foreach (string columnName in columnsNames)
                    {
                        if (!tableNameToCheck.Columns.Contains(columnName))
                        {
                            iscolumnExist = false;
                            break;
                        }
                    }
                }
                else
                {
                    iscolumnExist = false;
                }
            }
            catch (Exception ex)
            {

            }
            return iscolumnExist;
        }
    }
    public class OfflineMasterDataFile
    {
        public Venue Venue { get; set; }
    }

    public class Venue
    {
        public string CountryName { get; set; }
        public string CountryCode { get; set; }
        public string VenueName { get; set; }
        public string VenueCode { get; set; }
        public List<Site> SiteDetails { get; set; }
    }

    public class Site
    {
        public string SiteName { get; set; }
        public string SiteCode { get; set; }
        public string SiteSyncCode { get; set; }
        public bool IsLogical { get; set; }
        public string LogicalSyncCode { get; set; }
        public List<Item> ItemDetails { get; set; }
    }
    public class Item
    {
        public string ItemName { get; set; }
        public string ItemCode { get; set; }
        public string ItemDescription { get; set; }
        public bool IsPackage { get; set; }
        public bool IsAccessory { get; set; }
        public bool IsActive { get; set; }
        public string ItemSyncCode { get; set; }
        public decimal Price { get; set; }
        public string ItemSiteSyncCode { get; set; } 
        public List<PackageDetail> PackageKittingDetails { get; set; }
    }

    public class PackageDetail
    {
        public int MaxImages { get; set; }
        public Item BaseProduct { get; set; }
        public string PkgSiteSyncCode { get; set; }
        public string pkgitemcode { get; set; }
    }

}
