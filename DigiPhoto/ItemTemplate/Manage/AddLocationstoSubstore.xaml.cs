﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
//using DigiPhoto.DataLayer;
//using DigiPhoto.DataLayer.Model;
using DigiPhoto.Common;
using DigiAuditLogger;
using DigiPhoto.IMIX.Business;
using DigiPhoto.IMIX.Model;
using System.Collections.ObjectModel;
using DigiPhoto.Utility.Repository.ValueType;
using FrameworkHelper;

namespace DigiPhoto.Manage
{
    /// <summary>
    /// Interaction logic for AddLocationstoSubstore.xaml
    /// </summary>
    public partial class AddLocationstoSubstore : Window
    {
        #region Declartion
        Dictionary<string, string> lstSubstores;        
        List<LocationInfo> availableList;
        List<LocationInfo> associatedList;
        #endregion

        #region Constructor
        /// <summary>
        /// Initializes a new instance of the <see cref="AddLocationstoSubstore"/> class.
        /// </summary>
        public AddLocationstoSubstore()
        {
            InitializeComponent();          
            GetSubstoreDropDown();
            txbUserName.Text = LoginUser.UserName;
            txbStoreName.Text = LoginUser.StoreName;
            btnSave.IsDefault = true;
        }
        #endregion

        #region Common Methods
        /// <summary>
        /// Gets the substore drop down.
        /// </summary>
        public void GetSubstoreDropDown()
        {
            try
            {
                StoreSubStoreDataBusniess stoBiz = new StoreSubStoreDataBusniess();
                lstSubstores = new Dictionary<string, string>();
                lstSubstores.Add("0", "--Select--");
                cmbSubStoreName.ItemsSource = stoBiz.GetSubstoreDataDir(lstSubstores);
                //{
                //    lstSubstores.Add(item.DG_SubStore_Name, item.DG_SubStore_pkey.ToString());
                //}

                // stoBiz.GetSubstoreDataDir();
                //cmbSubStoreName.ItemsSource = lstSubstores;
                cmbSubStoreName.SelectedValue = "0";
            }
            catch (Exception ex)
            {
                /////////////////error logging
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }
        /// <summary>
        /// Gets the list available locations.
        /// </summary>
        /// <param name="SubstoreID">The substore unique identifier.</param>
        public void GetListAvailableLocations(int SubstoreID)
        {
            StoreSubStoreDataBusniess stoBiz = new StoreSubStoreDataBusniess();
            try
            {
                availableList = stoBiz.GetAvailableLocationsSubstore(SubstoreID);
                lstAvailableLocations.ItemsSource = availableList;
                //if (itemlist != null)
                //{
                //    foreach (var item in itemlist)
                //    {
                //        lstAvailableLocations.Items.Add(item);
                //    }

                //}
                //else
                //{
                //    //MessageBox.Show("No location associated to this Substore");
                //    lstAvailableLocations.Items.Clear();
                //}
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }
        //public void GetListAvailableLocations()
        //{
        //    var itemlist=_objDbLayer.GetLocations(LoginUser.StoreId);
        //    if (itemlist.Count > 0)
        //    {
        //        lstAvailableLocations.ItemsSource = itemlist;

        //    }
        //}
        /// <summary>
        /// Gets the list selected locations.
        /// </summary>
        /// <param name="SubStoreID">The sub store unique identifier.</param>
        public void GetListSelectedLocations(int SubStoreID)
        {
            try
            {
                //StoreSubStoreDataBusniess stoBiz = new StoreSubStoreDataBusniess();
                associatedList = (new StoreSubStoreDataBusniess()).GetSelectedLocationsSubstore(SubStoreID);


                lstSelectedLocations.ItemsSource = associatedList;
                //if (itemlist != null)
                //{
                //    foreach (var item in itemlist)
                //    {
                //        lstSelectedLocations.Items.Add(item);
                //    }

                //}
                //else
                //{
                //    //MessageBox.Show("No location associated to this Substore");
                //    lstSelectedLocations.Items.Clear();
                //}
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }

        #endregion

        #region Events
        /// <summary>
        /// Handles the Click event of the btnLogout control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void btnLogout_Click(object sender, RoutedEventArgs e)
        {
            Login _objnew = new Login();
            _objnew.Show();
            this.Close();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {

        }

        /// <summary>
        /// Doubles the click handler.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="MouseEventArgs"/> instance containing the event data.</param>
        public void DoubleClickHandler(object sender, MouseEventArgs e)
        {
            var item = sender as ListBoxItem;
            if (item.Content.ToString() == "DigiPhoto.DataLayer.Model.vw_GetSubStoreLocations")
            {
                if (cmbSubStoreName.SelectedValue != "0" && lstSelectedLocations.SelectedItem != null)
                {
                    var items = lstSelectedLocations.SelectedItem;
                    LocationInfo _objItem = new LocationInfo();
                    _objItem.DG_Location_Name = ((LocationInfo)(items)).DG_Location_Name;
                    _objItem.DG_Location_pkey = ((LocationInfo)(items)).DG_Location_pkey;
                    lstAvailableLocations.Items.Add(_objItem);
                    lstSelectedLocations.Items.Remove(items);
                    lstSelectedLocations.Items.Refresh();
                    lstAvailableLocations.Items.Refresh();
                }
                else
                {
                    MessageBox.Show(UIConstant.SelectSubstoreAndLocation);
                }
                ApplyNoSelection();
            }
            else if (item.Content.ToString() == "DigiPhoto.DataLayer.Model.vw_GetListAvailableLocations")
            {
                if (cmbSubStoreName.SelectedValue != "0" && lstAvailableLocations.SelectedItem != null)
                {

                    var items = lstAvailableLocations.SelectedItem;
                    LocationInfo _objItem = new LocationInfo();
                    _objItem.DG_Location_Name = ((LocationInfo)(items)).DG_Location_Name;
                    _objItem.DG_Location_pkey = ((LocationInfo)(items)).DG_Location_pkey;
                    lstSelectedLocations.Items.Add(_objItem);
                    lstAvailableLocations.Items.Remove(items);
                    lstSelectedLocations.Items.Refresh();
                    lstAvailableLocations.Items.Refresh();
                }
                else
                {
                    MessageBox.Show(UIConstant.SelectSubstoreAndLocation);
                }
                ApplyNoSelection();
            }
        }
        /// <summary>
        /// Handles the SelectionChanged event of the cmbSubStoreName control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="SelectionChangedEventArgs"/> instance containing the event data.</param>
        private void cmbSubStoreName_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                lstSelectedLocations.ItemsSource = null;
                lstAvailableLocations.ItemsSource = null;
                
                //lstSelectedLocations.Items.Clear();
                //lstAvailableLocations.Items.Clear();
                GetListSelectedLocations(cmbSubStoreName.SelectedValue.ToInt32());
                GetListAvailableLocations(((KeyValuePair<string, string>)(((System.Windows.Controls.Primitives.Selector)(cmbSubStoreName)).SelectedItem)).Key.ToInt32());

                ApplyNoSelection();
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }


        }
        /// <summary>
        /// Applies the no selection.
        /// </summary>
        private void ApplyNoSelection()
        {
            if (lstAvailableLocations.Items.Count == 0)
            {
                lstAvailableLocations.Visibility = Visibility.Collapsed;
                noitemlstAvailableLocations.Visibility = Visibility.Visible;
            }
            else
            {
                noitemlstAvailableLocations.Visibility = Visibility.Collapsed;
                lstAvailableLocations.Visibility = Visibility.Visible;
            }
            if (lstSelectedLocations.Items.Count == 0)
            {
                lstSelectedLocations.Visibility = Visibility.Collapsed;
                noitemlstSelectedLocations.Visibility = Visibility.Visible;
            }
            else
            {
                noitemlstSelectedLocations.Visibility = Visibility.Collapsed;
                lstSelectedLocations.Visibility = Visibility.Visible;
            }
        }
        /// <summary>
        /// Handles the Click event of the btnSave control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            StoreSubStoreDataBusniess stoBiz = new StoreSubStoreDataBusniess();
            try
            {
                if (cmbSubStoreName.SelectedValue != "0")
                {

                    stoBiz.DeleteSubStoreLocations(cmbSubStoreName.SelectedValue.ToInt32());
                    string strlocs = "";

                    foreach (var item in lstSelectedLocations.Items)
                    {
                        stoBiz.SetSubStoreLocationsDetails(cmbSubStoreName.SelectedValue.ToInt32(), ((LocationInfo)(item)).DG_Location_pkey);
                        strlocs = strlocs + ", " + ((LocationInfo )(item)).DG_Location_Name;
                    }
                    CustomBusineses cusBiz = new CustomBusineses();
                    AuditLog.AddUserLog(Common.LoginUser.UserId, (int)FrameworkHelper.ActionType.ChangesinLocationAllocationtoSubstore, "Assign " + strlocs + "to " + cmbSubStoreName.Text + " at :-");
                    MessageBox.Show(UIConstant.SubstoreSavedSuccessfully);
                }
                else
                {
                    MessageBox.Show(UIConstant.SelectSubstore);
                }

            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }

        /// <summary>
        /// Handles the Click event of the btnClear control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void btnClear_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                cmbSubStoreName.SelectedValue = "0";

                lstAvailableLocations.ItemsSource = null;
                lstSelectedLocations.ItemsSource = null;
                //lstAvailableLocations.Items.Clear();
                //lstSelectedLocations.Items.Clear();
                GetListAvailableLocations(cmbSubStoreName.SelectedValue.ToInt32());
                ApplyNoSelection();

            }
            catch (Exception ex)
            {
            }
        }

        /// <summary>
        /// Handles the Click event of the btnBack control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void btnBack_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                ManageSubstores _objsubstore = new ManageSubstores();
                _objsubstore.Show();
                this.Close();
            }
            catch (Exception ex)
            {
            }
        }
        /// <summary>
        /// Handles the Click event of the AddBtn control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void AddBtn_Click(object sender, RoutedEventArgs e)
        {
            if (cmbSubStoreName.SelectedValue != "0" && lstAvailableLocations.SelectedItem != null)
            {
                LocationInfo items = (LocationInfo)lstAvailableLocations.SelectedItem;
                LocationInfo _objItem = new LocationInfo();
                _objItem.DG_Location_Name = ((LocationInfo)(items)).DG_Location_Name;
                _objItem.DG_Location_pkey = ((LocationInfo)(items)).DG_Location_pkey;

                //lstSelectedLocations.Items.Add(_objItem);
                associatedList.Add(_objItem);
                lstSelectedLocations.ItemsSource = associatedList;

                //lstAvailableLocations.Items.Remove(items);
                availableList.Remove(items);
                lstAvailableLocations.ItemsSource = availableList;
                lstSelectedLocations.Items.Refresh();
                lstAvailableLocations.Items.Refresh();
            }
            else
            {
                MessageBox.Show(UIConstant.SelectSubstoreAndLocation);
            }
            ApplyNoSelection();
        }

        /// <summary>
        /// Handles the Click event of the Removebtn control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void Removebtn_Click(object sender, RoutedEventArgs e)
        {
            if (cmbSubStoreName.SelectedValue != "0" && lstSelectedLocations.SelectedItem != null)
            {
                var items = (LocationInfo)lstSelectedLocations.SelectedItem;
                LocationInfo _objItem = new LocationInfo();
                _objItem.DG_Location_Name = ((LocationInfo)(items)).DG_Location_Name;
                _objItem.DG_Location_pkey = ((LocationInfo)(items)).DG_Location_pkey;
                availableList.Add(_objItem);
                associatedList.Remove(items);
                lstAvailableLocations.ItemsSource = availableList;
                lstSelectedLocations.ItemsSource = associatedList;
                //lstAvailableLocations.Items.Add(_objItem);
                //lstSelectedLocations.Items.Remove(items);
                lstSelectedLocations.Items.Refresh();
                lstAvailableLocations.Items.Refresh();
            }
            else
            {
                MessageBox.Show(UIConstant.SelectSubstoreAndLocation);
            }
            ApplyNoSelection();
        }
        #endregion


    }

}
