﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using DigiPhoto.DataLayer;
using DigiPhoto.DataLayer.Model;
using DigiPhoto.Common;
using DigiAuditLogger;
using DigiPhoto.Utility.Repository.ValueType;
using DigiPhoto.IMIX.Business;
using DigiPhoto.IMIX.Model;
using FrameworkHelper;
namespace DigiPhoto.Manage
{
    /// <summary>
    /// Interaction logic for PrintLog.xaml
    /// </summary>
    public partial class PrintLog : Window
    {

        #region
        //Dictionary<string, string> lstPhotographerList;
        #endregion
        public PrintLog()
        {
            try
            {
                InitializeComponent();
                //DigiPhotoDataServices _objdbLayer = new DigiPhotoDataServices();
                //DGPrintLog.ItemsSource = _objdbLayer.GetPrintLogDetails();\
                DGPrintLog.ItemsSource = (new PrinterBusniess()).GetPrintLogDetails();
                FillPhotographerCombo();
                txbUserName.Text = LoginUser.UserName;
                txbStoreName.Text = LoginUser.StoreName;
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
            }
        }

        private void btnback_Click(object sender, RoutedEventArgs e)
        {

            ManageReport _objreport = new ManageReport();
            _objreport.Show();
            this.Close();
        }
        private void FillPhotographerCombo()
        {
            //lstPhotographerList = new Dictionary<string, string>();
            //lstPhotographerList.Add("--Select--", "0");
            //try
            //{
            //    DigiPhotoDataServices _objdbLayer = new DigiPhotoDataServices();
            //    foreach (var item in _objdbLayer.GetPhotoGrapher())
            //    {
            //        lstPhotographerList.Add(item.Photograper, item.DG_User_pkey.ToString());
            //    }
            //    cmbPhotographer.ItemsSource = lstPhotographerList;
            //    cmbPhotographer.SelectedValue = "0";
            //}
            //catch (Exception ex)
            //{

            //}

            List<PhotoGraphersInfo> objPhoto = (new PhotoBusiness()).GetPhotoGrapher();
            CommonUtility.BindComboWithSelect<PhotoGraphersInfo>(cmbPhotographer, objPhoto, "Photograper", "DG_User_pkey", 0, DigiPhoto.Utility.Repository.Data.ClientConstant.SelectString);
            cmbPhotographer.SelectedValue = "0";
        }

        private void btnLogout_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                AuditLog.AddUserLog(Common.LoginUser.UserId, (int)FrameworkHelper.ActionType.Logout, "Logged out at ");
                Login _objLogin = new Login();
                _objLogin.Show();
                this.Close();
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }

        private void btnSearch_Click(object sender, RoutedEventArgs e)
        {
            string Photographer = cmbPhotographer.SelectedValue.ToString();
        }
    }
}
