﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Script.Serialization;

namespace DigiPhoto
{
    public class APIWrapper
    {

        public APIResponse GetImagesImage(string MasterImagePath)
        {

            try
            {
                //MasterImagePath = "E:\\MasterImage\\20190610\\MI636957824343032277.jpg";
                string request = this.CreateRequest(MasterImagePath);
                request = request.Replace(@"\", @"\\");

                //string apiUrl = "http://10.91.1.143:5001/POC/get_images";
                string apiUrl = ConfigurationManager.AppSettings["FaceAPIUrl"].ToString();
                WebClient client = new WebClient();
                client.Headers["Content-type"] = "application/json";
                client.Encoding = Encoding.UTF8;

                string json = client.UploadString(apiUrl, request);
                APIResponse response = (new JavaScriptSerializer()).Deserialize<APIResponse>(json);
                return response;
            }
            catch(Exception ex)
            {
                return new APIResponse();
                //resourceFile.HasError = true;
                //resourceFile.IsProcessed = false;
                //resourceFile.Error = ex;
            }
        }
        private string CreateRequest(string MasterImagePath)
        {
            //MasterImagePath = "D:\\MasterImage\\img20190606123801.jpg";
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.Append("{");
            stringBuilder.Append("\"");
            stringBuilder.Append("path");
            stringBuilder.Append("\":\"");
            stringBuilder.Append(MasterImagePath);
            stringBuilder.Append("\"");
            stringBuilder.Append("}");
            return stringBuilder.ToString();
            //return "{path\":\"D:\MasterImage\img20190606123801.jpg\"";
        }
    }
    public class APIResponse
    {
        public string[] response { get; set; }
    }
}
