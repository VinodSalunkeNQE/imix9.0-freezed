﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace DigiPhoto
{
    /// <summary>
    /// Interaction logic for SplashScreen.xaml
    /// </summary>
    public partial class SplashScreen : Window
    {


        /// <summary>
        /// Initializes a new instance of the <see cref="SplashScreen"/> class.
        /// </summary>
        public SplashScreen()
        {
            InitializeComponent();
            ClientView window = null;



            foreach (Window wnd in System.Windows.Application.Current.Windows)
            {
                if (wnd.Title == "ClientView")
                {
                    window = (ClientView)wnd;
                }

            }
            if (window != null)
            {
                window.WindowStartupLocation = System.Windows.WindowStartupLocation.Manual;
                window.DefaultView = true;
                window.ChildImage = new BitmapImage(new Uri(@"/images/all_frames_new.png", UriKind.Relative));


            }
            else
            {

                window = new ClientView();
                window.WindowStartupLocation = System.Windows.WindowStartupLocation.Manual;
                window.DefaultView = true;
                window.ChildImage = new BitmapImage(new Uri(@"/images/all_frames_new.png", UriKind.Relative));

            }





            System.Windows.Forms.Screen[] screens = System.Windows.Forms.Screen.AllScreens;
            if (screens.Length > 1)
            {
                if (screens[0].Primary)
                {
                    System.Windows.Forms.Screen s2 = System.Windows.Forms.Screen.AllScreens[1];
                    System.Drawing.Rectangle r2 = s2.WorkingArea;

                    window.Top = r2.Top;
                    window.Left = r2.Left;
                    //
                    window.Show();
                }
                else
                {
                    System.Drawing.Rectangle r2 = screens[0].WorkingArea;
                    window.Top = r2.Top;
                    window.Left = r2.Left;
                    window.Show();
                }

            }
            else
            {
                window.Show();
            }

            // Comment this line out and the problem goes away
           // m_mainWindow.Show();

        }
    }
}
