﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using DigiPhoto.Common;
using DigiPhoto.DataLayer;
using DigiAuditLogger;
using System.Collections.ObjectModel;
using System.Management;
using System.Printing;
using DigiPhoto.IMIX.Model;
using DigiPhoto.IMIX.Business;
using DigiPhoto.Utility.Repository.Data;
using DigiPhoto.Utility.Repository.ValueType;
using FrameworkHelper;
using System.Collections;
using Dynamsoft.UVC;
using Dynamsoft.Core;
using System.Windows.Media;
using Dynamsoft.Common;
using System.Drawing;
using System.Windows.Media.Imaging;
using DigiPhoto.FR;

namespace DigiPhoto
{

    public partial class SearchByPhoto : Window
    {


        #region Declaration

        private TextBox _txtKeyboard = new TextBox();
        //private Dictionary<string, Int32> _locationList;
        //private Dictionary<Int32, string> _photoGrapherList;
        //private Dictionary<string, string> _subStoreList;
        private string _mktImgPath = string.Empty;
        private int _mktImgTime = 0;
        private string _cardCode = string.Empty;

        //public Dictionary<string, string> SubStoreList
        //{
        //    get { return _subStoreList; }
        //    set { _subStoreList = value; }
        //}
        //DigiPhotoDataServices _objservice;
        private Visibility _isPanelVisible;
        //public int serachType = 0;
        #endregion
        #region camperstuffs By Kailash Chandra Behera on 31 AUG 2019 to capture image from cam
        private int m_iRotate = 0;
        // ADDED BY KAILASH ON 25 SEP 2019 TO GET API CONFIGURATION DETAILS.
        private string m_StrProductKey = "t0078oQAAALS176sNWXF5Io4vXp2khlj+SCbN1ElJvDJhZo7HC0N3UdopMK+K7l9TSzE8mE9arzyR+jVrx7+VblMIP6jGm7nGJsamBgAfnCAB";
        private ImageCore m_ImageCore = null;
        private CameraManager m_CameraManager = null;
        private Camera m_Camera = null;
        // End
        public static Dictionary<String, ImageBrush> icons = new Dictionary<string, ImageBrush>();
        public static readonly string imageDirectory;
        public static readonly string strCurrentDirectory;
        private Window m_ControlWindow;
        private delegate void RefreshDelegate(System.Windows.Media.Imaging.BitmapImage temp);
        private RefreshDelegate m_Refresh; // ADDED BY KAILASH ON 25 SEP 2019 TO GET API CONFIGURATION DETAILS.
        private FRAPIDetails fRAPI;// ADDED BY KAILASH ON 25 SEP 2019 TO GET API CONFIGURATION DETAILS.
        #endregion
        #region Properties
        //public Dictionary<string, Int32> LocationList
        //{
        //    get { return _locationList; }
        //    set { _locationList = value; }
        //}
        //public Dictionary<Int32, string> PhotoGrapherList
        //{
        //    get { return _photoGrapherList; }
        //    set { _photoGrapherList = value; }
        //}
        public Visibility IsPanelVisible
        {
            get { return _isPanelVisible; }
            set { _isPanelVisible = value; }
        }
        #endregion

        #region Constructor
        public SearchByPhoto()
        {
            try
            {
                InitializeComponent();
                txbUserName.Text = LoginUser.UserName;
                txbStoreName.Text = LoginUser.StoreName;
                txtRFID.Focus();
                rdbtnPhotoId.IsChecked = true;
                searchbyPhotoId.Visibility = Visibility.Visible;
                searchbyScan.Visibility = Visibility.Collapsed;
                DateTime serverDateTime = (new CustomBusineses()).ServerDateTime();
                dtselect.SelectedDate = serverDateTime;
                dtselectToDate.SelectedDate = serverDateTime;
                GetAllDropdown();
                txtRFID.Text = "";

                //Search BY QR Code
                BindCodeType();

                //set to prev State of Radio button
                //int index = App.ViewOrderSearchIndex;

                rdbtnPhotoId.IsChecked = App.ViewOrderSearchIndex == 0 ? true : false;
                rdbtnTime.IsChecked = App.ViewOrderSearchIndex == 1 ? true : false;
                rdbtnGroup.IsChecked = App.ViewOrderSearchIndex == 2 ? true : false;
                // rdbtnQRCode.IsChecked = App.ViewOrderSearchIndex == 3 ? true : false;
                SetQrCodeSearchState();


                if (string.IsNullOrEmpty(App.QRCodeWebUrl.Trim()))
                {
                    //_objservice = new DigiPhotoDataServices();
                    //LocationBusniess locBiz = new LocationBusniess();
                    string webUrl = (new LocationBusniess()).GetQRCodeWebUrl();
                    App.QRCodeWebUrl = string.IsNullOrEmpty(webUrl) ? " " : webUrl;
                }
                //rdbtnPhotoId.is
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
            }
        }
        #endregion

        #region Events
        private void btn_Click_keyboard(object sender, RoutedEventArgs e)
        {
            try
            {
                //Button _objbtn = new Button();
                Button objbtn = (Button)sender;
                switch (objbtn.Content.ToString())
                {
                    case "ENTER":
                        {
                            KeyBorder1.Visibility = Visibility.Collapsed;
                            break;
                        }
                    case "SPACE":
                        {

                            //txtgroupname.Text = txtgroupname.Text + " ";
                            _txtKeyboard.Text = _txtKeyboard.Text + " ";

                            break;
                        }
                    case "CLOSE":
                        {
                            KeyBorder1.Visibility = Visibility.Collapsed;
                            break;
                        }
                    case "Back":
                        {

                            //txtgroupname.Text = txtgroupname.Text.Remove(txtgroupname.Text.Length - 1, 1);
                            if (_txtKeyboard.Text.Length > 0)
                                _txtKeyboard.Text = _txtKeyboard.Text.Remove(_txtKeyboard.Text.Length - 1, 1);
                            break;
                        }
                    default:
                        {

                            //txtgroupname.Text = txtgroupname.Text + _objbtn.Content;
                            _txtKeyboard.Text = _txtKeyboard.Text + objbtn.Content;
                        }
                        break;
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
            }
        }
        private void btnHome_Click(object sender, RoutedEventArgs e)
        {
            ClientView window = null;

            foreach (Window wnd in Application.Current.Windows)
            {
                if (wnd.Title == "ClientView")
                {
                    window = (ClientView)wnd;
                }
            }

            if (window == null)
            {

                window = new ClientView();
                window.WindowStartupLocation = System.Windows.WindowStartupLocation.Manual;

            }


            window.GroupView = false;
            window.DefaultView = false;

            GetMktImgInfo();
            if (!(_mktImgPath == "" || _mktImgTime == 0))
            {
                window.instructionVideo.Visibility = System.Windows.Visibility.Visible;
                window.instructionVideo.Play();
                // Added for Blur Iamges
                window.instructionVideoBlur.Visibility = System.Windows.Visibility.Visible;
                window.instructionVideoBlur.Play();
                // End Changes.
            }
            else
            {
                window.imgDefault.Visibility = System.Windows.Visibility.Visible;
                window.imgDefaultBlur.Visibility = System.Windows.Visibility.Visible; // Added for Blur Iamges
            }
            window.testR.Fill = null;
            window.DefaultView = true;
            if (window.instructionVideo.Visibility == System.Windows.Visibility.Visible)
                window.instructionVideo.Play();
            else
                window.instructionVideo.Pause();

            if (window.instructionVideoBlur.Visibility == System.Windows.Visibility.Visible)
                window.instructionVideoBlur.Play();
            else
                window.instructionVideoBlur.Pause();

            Home objhome = new Home();
            objhome.Show();
            if (cmbQRCodeType.SelectedValue != null)
            {
                if (cmbQRCodeType.SelectedValue.ToString() != "404")
                    txtQRCode.Text = string.Empty;
            }
            this.Hide();

            //By KCB ON 05 AUG 2019 for manage camera
            if (m_Camera != null)
            {
                m_Camera.Close();
                m_Camera.Dispose();
                m_Camera = null;
            }
            m_CameraManager = null;

            //  this.Close();
        }
        private void btnRefresh_Click(object sender, RoutedEventArgs e)
        {
            //////Gettign Offline Printer status on click of Refresh Button
            GetPrintersStatus();
        }
        private void DatePicker_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            //Thread.Sleep(500);
            if (e.Key == Key.Return)
                btnSearchDetailWithQrcode_Click(btnsearch, new RoutedEventArgs());

        }

        // commented by santosh as default button does the same thing
        //private void btnSavegroup_Enter(object sender, KeyEventArgs e)
        //{
        //    if (e.Key == Key.Enter)
        //        btn_Click(btnsearchgroup, new RoutedEventArgs());

        //}


        private void bttnLogin_Enter(object sender, KeyEventArgs e)
        {
            // commented by santosh as default button does the same thing
            //if (e.Key == Key.Enter)
            //{
            //    if (string.IsNullOrEmpty(txtRFID.Text.Trim())) //Fix for multiple window opening
            //        btn_Click(Ok, new RoutedEventArgs());
            //}
        }

        private void btnSearchDetailWithQrcode_Click(object sender, RoutedEventArgs e)
        {
            setQRCode();///changed by latika ATP QR Code workflow 03 Apr 2019
            if (txtRFID.Text.Trim() == "")
                txtRFID.Text = "0";
            try
            {
                if (cmbFromTime.SelectedItem != null && cmbToTime.SelectedItem != null)
                {
                    //SearchDetailInfo Searchdetails = new SearchDetailInfo();
                    //SearchCriteriaInfo SearchBiz = new SearchCriteriaInfo();

                    ////////assigning search criteria to Common Fields for search logic
                    RobotImageLoader.RFID = txtRFID.Text;
                    RobotImageLoader.PhotoId = txtRFID.Text;
                    RobotImageLoader.SearchCriteria = "TimeWithQrcode";
                    DateTime fromTime;
                    DateTime ToTime;
                    if ((dtselect.SelectedDate != null) && (dtselectToDate.SelectedDate != null))
                    {
                        fromTime = dtselect.SelectedDate.Value;
                        ToTime = dtselectToDate.SelectedDate.Value;
                    }
                    else if ((dtselect.SelectedDate != null) && (dtselectToDate.SelectedDate == null))
                    {
                        fromTime = dtselect.SelectedDate.Value;
                        ToTime = dtselect.SelectedDate.Value;
                    }
                    else if ((dtselectToDate.SelectedDate != null) && (dtselect.SelectedDate == null))
                    {
                        fromTime = dtselectToDate.SelectedDate.Value;
                        ToTime = dtselectToDate.SelectedDate.Value;
                    }
                    else
                    {
                        fromTime = (new CustomBusineses()).ServerDateTime().Date;
                        ToTime = (new CustomBusineses()).ServerDateTime().Date;
                    }
                    fromTime = fromTime.AddHours(Convert.ToInt32(cmbFromTime.SelectionBoxItem.ToString().Substring(0, 2)));
                    fromTime = fromTime.AddMinutes(Convert.ToInt32(cmbFromTime.SelectionBoxItem.ToString().Substring(3, 2)));

                    ToTime = ToTime.AddHours(Convert.ToInt32(cmbToTime.SelectionBoxItem.ToString().Substring(0, 2)));
                    ToTime = ToTime.AddMinutes(Convert.ToInt32(cmbToTime.SelectionBoxItem.ToString().Substring(3, 2)));



                    ////////assigning search criteria to Common Fields for search logic
                    RobotImageLoader.FromTime = fromTime;
                    RobotImageLoader.ToTime = ToTime;
                    RobotImageLoader.LocationId = Convert.ToInt32(((LocationInfo)((cmbLocation.SelectedItem))).DG_Location_pkey);
                    RobotImageLoader.UserId = Convert.ToInt32(((PhotoGraphersInfo)((cmbPhotographer.SelectedItem))).DG_User_pkey);
                    //added by Latika for table workflow at 
                    RobotImageLoader.TableName = txtTable.Text;
                    RobotImageLoader.GuestName = DigiPhoto.CryptorEngine.Encrypt(txtGuestName.Text, true);
                    RobotImageLoader.EmailID = DigiPhoto.CryptorEngine.Encrypt(txtEmail.Text, true);
                    RobotImageLoader.ContactNo = DigiPhoto.CryptorEngine.Encrypt(txtContact.Text, true);
                    ///end
                    ///
                    if (cmbSubstore.SelectedValue != "0")
                    {
                        RobotImageLoader.SearchedStoreId = cmbSubstore.SelectedValue.ToString();

                    }
                    else
                    {
                        RobotImageLoader.SearchedStoreId = "";
                    }

                    if (cmbCharacter.SelectedValue != "0")
                    {
                        RobotImageLoader.CharacterId = cmbCharacter.SelectedValue.ToInt32();
                    }
                    else
                    {
                        RobotImageLoader.CharacterId = null;
                    }





                    ErrorHandler.ErrorHandler.LogFileWrite(RobotImageLoader.SearchedStoreId);

                    txtQRCode.Text = txtQRCode.Text.Replace(App.QRCodeWebUrl, string.Empty);

                    if (!String.IsNullOrEmpty(txtQRCode.Text))
                    {
                        if (cmbQRCodeType.SelectedIndex <= 0)
                        {
                            MessageBox.Show("Please select Code Type.");
                            return;
                        }
                    }
                    if (cmbQRCodeType.SelectedIndex > 0)
                    {
                        if (String.IsNullOrEmpty(txtQRCode.Text))
                        {
                            int codeTypeId = (int)cmbQRCodeType.SelectedValue;
                            MessageBox.Show("Enter " + cmbQRCodeType.Text + (codeTypeId != 405 ? " Code." : "."));
                            return;
                        }
                    }
                    if ((Convert.ToInt32(cmbQRCodeType.SelectedValue) > 0))
                    {
                        RobotImageLoader.CodeType = (int)cmbQRCodeType.SelectedValue;
                    }
                    RobotImageLoader.IsAnonymousQrCodeEnabled = App.IsAnonymousQrCodeEnabled == true && RobotImageLoader.CodeType == 405 ? true : false;

                    if (!string.IsNullOrEmpty(txtQRCode.Text.Trim()))
                    {
                        SearchResult window = null;
                        foreach (Window wn in Application.Current.Windows)
                        {
                            if (string.Compare(wn.Title, "View/Order Station", true) == 0)
                            {
                                window = (SearchResult)wn;
                                break;

                            }
                        }
                        CardBusiness cardBiz = new CardBusiness();
                        SearchResult objSearchResult = window != null ? window : new SearchResult();

                        RobotImageLoader.Code = txtQRCode.Text;
                        if (RobotImageLoader.CodeType == 403)
                            RobotImageLoader.Code = "0000" + RobotImageLoader.Code;
                        if (RobotImageLoader.CodeType == 406)
                            RobotImageLoader.Code = "2222" + RobotImageLoader.Code;
                        if (App.IsAnonymousQrCodeEnabled == false && !cardBiz.IsValidCodeType(RobotImageLoader.Code, RobotImageLoader.CodeType))
                        {
                            MessageBox.Show("Invalid code.");
                            if (RobotImageLoader.CodeType != 404)
                                txtQRCode.Text = string.Empty;
                            return;
                        }
                        if (txtQRCode != null)
                        {
                            if (IsImageExists(RobotImageLoader.Code))
                            {
                                objSearchResult.pagename = "";
                                objSearchResult.Show();
                                objSearchResult.serachType = 1;
                                ////////Calling logic of searching 
                                objSearchResult.LoadWindow();
                                ////////Closing current window
                                if (cmbQRCodeType.SelectedValue.ToString() != "404")
                                    txtQRCode.Text = string.Empty;
                                this.Hide();
                            }
                            else
                            {
                                if (RobotImageLoader.CodeType != 404)
                                {///changed by latika ATP QR Code workflow 03 Apr 2019
                                    MessageBox.Show("No image is associated with this code having ." + txtQRCode.Text);
                                    txtQRCode.Text = string.Empty;
                                }
                            }
                        }


                    }
                    else
                    {
                        RobotImageLoader.Code = string.Empty;
                        SearchResult objSearchResult = null;
                        foreach (Window wnd in Application.Current.Windows)
                        {
                            if (wnd.Title == "View/Order Station")
                            {
                                objSearchResult = (SearchResult)wnd;
                            }
                        }
                        if (objSearchResult == null)
                            objSearchResult = new SearchResult();
                        //SearchResult _objSearchResult = new SearchResult();
                        objSearchResult.pagename = "";
                        if (RobotImageLoader.NotPrintedImages == null)
                            RobotImageLoader.NotPrintedImages = new List<LstMyItems>();

                        RobotImageLoader._objnewincrement = new List<LstMyItems>();
                        objSearchResult.grdSelectAll.Visibility = Visibility.Visible; // Change this
                        RobotImageLoader.ViewGroupedImagesCount = new List<string>();
                        objSearchResult.Show();
                        objSearchResult.serachType = 1;
                        ////////Calling logic of searching 
                        objSearchResult.LoadWindow();
                        ////////Closing current window
                        if (cmbQRCodeType.SelectedValue.ToString() != "404")
                            txtQRCode.Text = string.Empty;
                        this.Hide();

                    }
                    //else
                    //    MessageBox.Show("Please enter " + cmbQRCodeType.Text + (RobotImageLoader.CodeType != 405 ? " Code." : "."));
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
            }
        }
        private void btn_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                // _objservice = new DigiPhotoDataServices();

                //Button _objbtn = new Button();
                RobotImageLoader.MediaTypes = 0;
                Button objbtn = (Button)sender;
                switch (objbtn.Name)
                {
                    case "btn1":
                        {
                            txtRFID.Text = txtRFID.Text + "1";
                            break;
                        }
                    case "btn2":
                        {
                            txtRFID.Text = txtRFID.Text + "2";
                            break;
                        }
                    case "btn3":
                        {
                            txtRFID.Text = txtRFID.Text + "3";
                            break;
                        }
                    case "btn4":
                        {
                            txtRFID.Text = txtRFID.Text + "4";
                            break;
                        }
                    case "btn5":
                        {
                            txtRFID.Text = txtRFID.Text + "5";
                            break;
                        }
                    case "btn6":
                        {
                            txtRFID.Text = txtRFID.Text + "6";
                            break;
                        }
                    case "btn7":
                        {
                            txtRFID.Text = txtRFID.Text + "7";
                            break;
                        }
                    case "btn8":
                        {
                            txtRFID.Text = txtRFID.Text + "8";
                            break;
                        }
                    case "btn9":
                        {
                            txtRFID.Text = txtRFID.Text + "9";
                            break;
                        }
                    case "btn0":
                        {
                            txtRFID.Text = txtRFID.Text + "0";
                            break;
                        }
                    case "Ok":
                        {
                            if (txtRFID.Text.Trim() == "")
                            {
                                txtRFID.Text = "0";
                                RobotImageLoader.IsZeroSearchNeeded = true;
                            }

                            if (!string.IsNullOrEmpty(txtRFID.Text.Trim()))
                            {
                                RobotImageLoader.RFID = txtRFID.Text;
                                RobotImageLoader.SearchCriteria = "PhotoId";
                                if (RobotImageLoader.GroupImages == null)
                                {
                                    RobotImageLoader.GroupImages = new List<LstMyItems>();
                                }
                                else
                                {
                                    if (RobotImageLoader.GroupImages.Count == 0)
                                    {
                                        RobotImageLoader.GroupImages = new List<LstMyItems>();
                                    }
                                }
                                if (txtRFID.Text != "0")
                                {
                                    RobotImageLoader._rfidSearch = 0;
                                    RobotImageLoader.StartIndexRFID = 0;
                                    RobotImageLoader.PhotoId = txtRFID.Text;
                                }
                                else
                                {
                                    RobotImageLoader.IsZeroSearchNeeded = true;
                                    RobotImageLoader.currentCount = 0;
                                }
                                //////creating Instance of Search result window//////
                                SearchResult objSearchResult = null;

                                //Commented for fixing issue of refreshing screen

                                foreach (Window wnd in Application.Current.Windows)
                                {
                                    if (wnd.Title == "View/Order Station")
                                    {
                                        objSearchResult = (SearchResult)wnd;
                                    }
                                }
                                if (objSearchResult == null)
                                    objSearchResult = new SearchResult();
                                if (RobotImageLoader.NotPrintedImages == null)
                                    RobotImageLoader.NotPrintedImages = new List<LstMyItems>();

                                RobotImageLoader._objnewincrement = new List<LstMyItems>();
                                objSearchResult.grdSelectAll.Visibility = Visibility.Visible; // Change this
                                RobotImageLoader.ViewGroupedImagesCount = new List<string>();
                                objSearchResult.pagename = "";
                                objSearchResult.Show();
                                objSearchResult.returntoHome = false;
                                ////////Calling logic of searching 
                                objSearchResult.LoadWindow();
                                ////////Closing current window                               
                                if (cmbQRCodeType.SelectedValue.ToString() != "404")
                                    txtQRCode.Text = string.Empty;
                                this.Hide();
                            }
                            break;
                        }
                    case "btnsearchgroup":
                        {
                            if (txtRFID.Text.Trim() == "")
                                txtRFID.Text = "0";
                            if (!string.IsNullOrEmpty(txtRFID.Text.Trim()))
                            {
                                if (string.IsNullOrEmpty(txtgroupname.Text.Trim()))
                                {
                                    MessageBox.Show("Please enter group name.");
                                    return;
                                }

                                //if (_objservice == null)
                                //    _objservice = new DigiPhotoDataServices();
                                PhotoBusiness photoBiz = new PhotoBusiness();
                                if (photoBiz.GetSavedGroupImages(txtgroupname.Text.Trim()).Count() <= 0)
                                {
                                    MessageBox.Show("No image is grouped with this name.");
                                    return;
                                }
                                RobotImageLoader.GroupId = txtgroupname.Text;
                                RobotImageLoader.SearchCriteria = "Group";
                                SearchResult objSearchResult = null;
                                foreach (Window wnd in Application.Current.Windows)
                                {
                                    if (wnd.Title == "View/Order Station")
                                    {
                                        objSearchResult = (SearchResult)wnd;
                                    }
                                }
                                if (objSearchResult == null)
                                    objSearchResult = new SearchResult();
                                //SearchResult _objSearchResult = new SearchResult();
                                objSearchResult.pagename = "MainGroup";
                                objSearchResult.txtgpnumber.Text = txtgroupname.Text;
                                objSearchResult.Show();
                                ////////Calling logic of searching 
                                objSearchResult.LoadWindow();
                                ////////Closing current window                               
                                if (cmbQRCodeType.SelectedValue.ToString() != "404")
                                    txtQRCode.Text = string.Empty;
                                txtgroupname.Text = "";
                                this.Hide();
                            }
                            break;
                        }
                    case "btnSearchQRCode":
                        {
                            txtQRCode.Text = txtQRCode.Text.Replace(App.QRCodeWebUrl, string.Empty);

                            if (cmbQRCodeType.SelectedIndex <= 0)
                            {
                                MessageBox.Show("Please select card type.");
                                return;
                            }
                            RobotImageLoader.CodeType = (int)cmbQRCodeType.SelectedValue;
                            RobotImageLoader.IsAnonymousQrCodeEnabled = App.IsAnonymousQrCodeEnabled == true && RobotImageLoader.CodeType == 405 ? true : false;
                            if (!string.IsNullOrEmpty(txtQRCode.Text.Trim()))
                            {
                                SearchResult window = null;
                                foreach (Window wn in Application.Current.Windows)
                                {
                                    if (string.Compare(wn.Title, "View/Order Station", true) == 0)
                                    {
                                        window = (SearchResult)wn;
                                        break;

                                    }
                                }
                                CardBusiness cardBiz = new CardBusiness();
                                RobotImageLoader.SearchCriteria = "QRCODE";
                                SearchResult _objSearchResult = window != null ? window : new SearchResult();
                                RobotImageLoader.Code = txtQRCode.Text;
                                if (RobotImageLoader.CodeType == 403)
                                    RobotImageLoader.Code = "0000" + RobotImageLoader.Code;
                                if (RobotImageLoader.CodeType == 406)
                                    RobotImageLoader.Code = "2222" + RobotImageLoader.Code;
                                if (App.IsAnonymousQrCodeEnabled == false && !cardBiz.IsValidCodeType(RobotImageLoader.Code, RobotImageLoader.CodeType))
                                {
                                    MessageBox.Show("Invalid code.");
                                    if (RobotImageLoader.CodeType != 404)
                                        txtQRCode.Text = string.Empty;
                                    return;
                                }

                                if (IsImageExists(RobotImageLoader.Code))
                                {
                                    _objSearchResult.pagename = "";
                                    _objSearchResult.Show();
                                    ////////Calling logic of searching 
                                    _objSearchResult.LoadWindow();
                                    if (cmbQRCodeType.SelectedValue.ToString() != "404")
                                        txtQRCode.Text = string.Empty;
                                    ////////Closing current window                                   
                                    this.Hide();
                                }
                                else
                                {
                                    if (RobotImageLoader.CodeType != 404)
                                        txtQRCode.Text = string.Empty;
                                    MessageBox.Show("No image is associated with this code.");
                                }
                            }
                            else
                                MessageBox.Show("Please enter " + cmbQRCodeType.Text + (RobotImageLoader.CodeType != 405 ? " Code." : "."));

                            break;
                        }

                    case "Cancel":
                        {
                            if (string.IsNullOrEmpty(RobotImageLoader.RFID))
                            {
                                RobotImageLoader.RFID = "1";
                                txtRFID.Text = "0";
                            }
                            else
                            {
                                SearchResult objSearchResult = null;
                                foreach (Window wnd in Application.Current.Windows)
                                {
                                    if (wnd.Title == "View/Order Station")
                                    {
                                        objSearchResult = (SearchResult)wnd;
                                    }
                                }
                                if (objSearchResult == null)
                                    objSearchResult = new SearchResult();

                                if (RobotImageLoader.NotPrintedImages == null)
                                    RobotImageLoader.NotPrintedImages = new List<LstMyItems>();

                                RobotImageLoader._objnewincrement = new List<LstMyItems>();
                                objSearchResult.grdSelectAll.Visibility = Visibility.Visible; // Change this
                                RobotImageLoader.ViewGroupedImagesCount = new List<string>();
                                //SearchResult _objSearchResult = new SearchResult();
                                objSearchResult.Show();
                                ////////Calling logic of searching 
                                objSearchResult.LoadWindow();
                                if (cmbQRCodeType.SelectedValue.ToString() != "404")
                                    txtQRCode.Text = string.Empty;
                                txtgroupname.Text = "";
                                ////////Closing current window                               
                                this.Hide();
                            }
                            break;
                        }
                    case "btnback":
                        {
                            if (txtRFID.Text.Length > 0)
                            {
                                txtRFID.Text = txtRFID.Text.Remove(txtRFID.Text.Length - 1, 1);
                            }
                            break;
                        }
                    case "btnsearch":
                        {
                            if (txtRFID.Text.Trim() == "")
                                txtRFID.Text = "0";
                            try
                            {
                                /////////////Search Logic///////
                                if (cmbFromTime.SelectedItem != null && cmbToTime.SelectedItem != null)
                                {

                                    ////////assigning search criteria to Common Fields for search logic
                                    RobotImageLoader.RFID = txtRFID.Text;
                                    RobotImageLoader.PhotoId = txtRFID.Text;
                                    DateTime fromTime;
                                    DateTime ToTime;
                                    RobotImageLoader.SearchCriteria = "TimeWithQrcode";// "Time";
                                    if (dtselect.SelectedDate != null)
                                    {
                                        ToTime = dtselect.SelectedDate.Value;
                                        fromTime = dtselect.SelectedDate.Value;
                                    }
                                    else
                                    {
                                        ToTime = (new CustomBusineses()).ServerDateTime().Date;
                                        fromTime = (new CustomBusineses()).ServerDateTime().Date;
                                    }
                                    fromTime = fromTime.AddHours(Convert.ToInt32(cmbFromTime.SelectionBoxItem.ToString().Substring(0, 2)));
                                    ToTime = ToTime.AddHours(Convert.ToInt32(cmbToTime.SelectionBoxItem.ToString().Substring(0, 2)));
                                    ////////assigning search criteria to Common Fields for search logic
                                    RobotImageLoader.FromTime = fromTime;
                                    RobotImageLoader.ToTime = ToTime;
                                    RobotImageLoader.LocationId = Convert.ToInt32(((LocationInfo)((cmbLocation.SelectedItem))).DG_Location_pkey);
                                    RobotImageLoader.UserId = Convert.ToInt32(((PhotoGraphersInfo)((cmbPhotographer.SelectedItem))).DG_User_pkey);
                                    if (cmbSubstore.SelectedValue != "0")
                                    {
                                        RobotImageLoader.SearchedStoreId = cmbSubstore.SelectedValue.ToString();
                                    }
                                    else
                                    {
                                        RobotImageLoader.SearchedStoreId = LoginUser.DefaultSubstores;
                                    }
                                    ErrorHandler.ErrorHandler.LogFileWrite(RobotImageLoader.SearchedStoreId);
                                    /////creating instance for Search result window
                                    SearchResult _objSearchResult = null;
                                    foreach (Window wnd in Application.Current.Windows)
                                    {
                                        if (wnd.Title == "View/Order Station")
                                        {
                                            _objSearchResult = (SearchResult)wnd;
                                        }
                                    }
                                    if (_objSearchResult == null)
                                        _objSearchResult = new SearchResult();

                                    //SearchResult _objSearchResult = new SearchResult();
                                    _objSearchResult.pagename = "";
                                    _objSearchResult.Show();

                                    ///////////Calling Search Logic////
                                    _objSearchResult.LoadWindow();
                                    if (cmbQRCodeType.SelectedValue.ToString() != "404")
                                        txtQRCode.Text = string.Empty;
                                    txtgroupname.Text = "";
                                    this.Hide();
                                }
                            }
                            catch (Exception ex)
                            {
                                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                            }

                            break;
                        }
                }
            }
            catch (Exception ex)
            {
                ///////ENTRY IN ERROR LOG FOR APPLICATION ERROR 
                ErrorHandler.ErrorHandler.LogError(ex);
            }

        }
        private void btnLogout_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                ///////ENTRY IN Audit LOG FOR Logout
                AuditLog.AddUserLog(Common.LoginUser.UserId, (int)FrameworkHelper.ActionType.Logout, "Logged out at ");
                Login objLogin = new Login();
                objLogin.Show();
                this.Close();
            }
            catch (Exception ex)
            {
                ///////ENTRY IN ERROR LOG FOR APPLICATION ERROR 
                ErrorHandler.ErrorHandler.LogError(ex);
            }

        }
        private void txtRFID_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                /////////avoid character values in input box and also handling enter key
                if (e.Key == Key.D0 || e.Key == Key.D1 || e.Key == Key.D2 || e.Key == Key.D3 || e.Key == Key.D4 || e.Key == Key.D5 || e.Key == Key.D6 || e.Key == Key.D7 || e.Key == Key.D8 || e.Key == Key.D9
                    || e.Key == Key.NumPad0 || e.Key == Key.NumPad1 || e.Key == Key.NumPad2 || e.Key == Key.NumPad3 || e.Key == Key.NumPad4 || e.Key == Key.NumPad5 || e.Key == Key.NumPad6 || e.Key == Key.NumPad7 || e.Key == Key.NumPad8 || e.Key == Key.NumPad9)
                {

                }
                else if (e.Key == Key.Enter)
                {
                    if (!string.IsNullOrEmpty(txtRFID.Text.Trim()))
                    {
                        // commented by santosh as default button does the same thing
                        //btn_Click(Ok, new RoutedEventArgs());


                        // RobotImageLoader.RFID = txtRFID.Text;
                        // if (txtRFID.Text != "0")
                        // {
                        //     RobotImageLoader.PhotoId = txtRFID.Text;
                        // }
                        // else
                        // {
                        //     RobotImageLoader.PhotoId = "0";
                        // }
                        // /////creating instance for Search result window and calling search Logic
                        // SearchResult _objSearchResult = null;
                        // foreach (Window wnd in Application.Current.Windows)
                        // {
                        //     if (wnd.Title == "View/Order Station")
                        //     {
                        //         _objSearchResult = (SearchResult)wnd;
                        //     }
                        // }
                        // if (_objSearchResult == null)
                        //     _objSearchResult = new SearchResult();

                        // //SearchResult _objSearchResult = new SearchResult();
                        // if (txtRFID.Text.StartsWith("00"))
                        // {
                        //     _objSearchResult.pagename = "MainGroup";
                        //     _objSearchResult.txtgpnumber.Text = txtRFID.Text.Replace("00", "GP-");
                        // }
                        // _objSearchResult.returntoHome = false;
                        //_objSearchResult.Show();
                        //_objSearchResult.LoadWindow();
                        //this.Hide();
                    }

                }
                else
                {
                    e.Handled = true;
                }


            }
            catch (Exception ex)
            {
                ///////ENTRY IN ERROR LOG FOR APPLICATION ERROR 
                ErrorHandler.ErrorHandler.LogError(ex);
            }
        }
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                //txtRFID.Text = "";
                ////ConfigBusiness configBiz = new ConfigBusiness();
                //if (Convert.ToBoolean((new ConfigBusiness()).GetConfigEnableGroup(LoginUser.SubStoreId)))
                //{
                //    rdbtnGroup.Visibility = Visibility.Visible;
                //}
                //else
                //{
                //    rdbtnGroup.Visibility = Visibility.Collapsed;
                //}
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
            }
        }
        private void rdbtnPhotoId_Click(object sender, RoutedEventArgs e)
        {
            //By KCB ON 05 AUG 2019 for manage camera
            if (m_Camera != null)
            {
                m_Camera.Close();
                m_Camera.Dispose();
                m_Camera = null;
            }
            m_CameraManager = null;
            //End
            PhotoIdSearch();
        }
        private void PhotoIdSearch()
        {
            searchbyCode.Visibility = Visibility.Collapsed;
            searchbyPhotoId.Visibility = Visibility.Visible;
            searchbyTimeRange.Visibility = Visibility.Hidden;
            searchbyScan.Visibility = Visibility.Hidden;  //By KCB ON 05 AUG 2019 for manage camera
            txtRFID.Text = "";
            txtRFID.Focus();
            _txtKeyboard = txtRFID;
            dtselect.SelectedDate = null;
            searchbyGroup.Visibility = Visibility.Collapsed;
            txtgroupname.Text = "";
            KeyBorder1.Visibility = Visibility.Collapsed;
            Ok.IsDefault = true;
            //Set the state of selection
            App.ViewOrderSearchIndex = rdbtnPhotoId.TabIndex;
        }
        private void rdbtnTime_Click(object sender, RoutedEventArgs e)
        {
            //By KCB ON 05 AUG 2019 for manage camera
            if (m_Camera != null)
            {
                m_Camera.Close();
                m_Camera.Dispose();
                m_Camera = null;
            }
            m_CameraManager = null;
            //End
            ChangeKey();
            searchbyCode.Visibility = Visibility.Collapsed;
            searchbyPhotoId.Visibility = Visibility.Hidden;
            searchbyTimeRange.Visibility = Visibility.Visible;
            searchbyScan.Visibility = Visibility.Hidden;  //By KCB ON 05 AUG 2019 for manage camera
            txtRFID.Text = "0";
            //dtselect.Focus();
            dtselect.SelectedDate = (new CustomBusineses()).ServerDateTime().Date;
            searchbyGroup.Visibility = Visibility.Collapsed;
            txtgroupname.Text = "";
            KeyBorder1.Visibility = Visibility.Collapsed;

            //Set the state of selection
            App.ViewOrderSearchIndex = rdbtnTime.TabIndex;
            btnsearch.IsDefault = true;
        }
        private void dtselectEnter_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

            dtselect.Focus();
            Keyboard.Focus(dtselect);
            btnsearch.IsDefault = true;
        }
        private void rdbtnGroup_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (m_Camera != null)
                {
                    m_Camera.Close();
                    m_Camera.Dispose();
                    m_Camera = null;
                }
                m_CameraManager = null;
                //lblCapturedFlag.Visibility = Visibility.Collapsed;

                ChangeKey();
                searchbyCode.Visibility = Visibility.Collapsed;
                searchbyPhotoId.Visibility = Visibility.Collapsed;
                searchbyTimeRange.Visibility = Visibility.Collapsed;
                searchbyGroup.Visibility = Visibility.Visible;
                searchbyScan.Visibility = Visibility.Hidden;  //By KCB ON 05 AUG 2019 for manage camera
                txtRFID.Text = "0";
                txtgroupname.Focus();
                FocusManager.SetFocusedElement(this, txtgroupname);
                Keyboard.Focus(txtgroupname);
                _txtKeyboard = txtgroupname;
                dtselect.SelectedDate = null;
                btnsearchgroup.IsDefault = true;
                //Set the state of selection
                App.ViewOrderSearchIndex = rdbtnGroup.TabIndex;

            }
            catch (Exception ex)
            {
            }
        }
        private void txtgroupname_GotFocus(object sender, RoutedEventArgs e)
        {
            KeyBorder1.Visibility = Visibility.Visible;
        }
        private void rdbtnQRCode_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (m_Camera != null)
                {
                    m_Camera.Close();
                    m_Camera.Dispose();
                    m_Camera = null;
                }
                m_CameraManager = null;

                ChangeKey();
                searchbyPhotoId.Visibility = Visibility.Collapsed;
                searchbyTimeRange.Visibility = Visibility.Collapsed;
                searchbyGroup.Visibility = Visibility.Collapsed;
                searchbyCode.Visibility = Visibility.Visible;


                txtRFID.Text = "0";
                txtQRCode.Focus();
                _txtKeyboard = txtQRCode;
                dtselect.SelectedDate = null;
                SetQrCodeSearchState();
                btnSearchQRCode.IsDefault = true;
                //Set the state of selection
                // App.ViewOrderSearchIndex = rdbtnQRCode.TabIndex;

            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
            }
        }
        private void txtQRCode_GotFocus(object sender, RoutedEventArgs e)
        {
            _txtKeyboard = txtQRCode;
            KeyBorder1.Visibility = Visibility.Visible;
        }
        private void cmbQRCodeType_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (cmbQRCodeType.SelectedValue != null)
            {
                App.SelectedCodeType = (int)cmbQRCodeType.SelectedValue;
                if (string.Compare(cmbQRCodeType.SelectedValue.ToString(), "404", true) == 0)
                {
                    txtQRCode.Text = _cardCode;
                    txtQRCode.IsEnabled = false;
                    KeyBorder1.Visibility = Visibility.Collapsed;
                }
                else
                {
                    KeyBorder1.Visibility = Visibility.Visible;
                    txtQRCode.Text = string.Empty;
                    txtQRCode.IsEnabled = true;
                    txtQRCode.Focus();
                }
            }
        }
        private void Window_Closed(object sender, EventArgs e)
        {
            ClearLocalResouces();
        }
        #endregion

        #region Private Methods
        private void ClearLocalResouces()
        {
            cmbQRCodeType.SelectionChanged -= new SelectionChangedEventHandler(cmbQRCodeType_SelectionChanged);
            txtQRCode.GotFocus -= new RoutedEventHandler(txtQRCode_GotFocus);
            //rdbtnQRCode.Click -= new RoutedEventHandler(rdbtnQRCode_Click);
            txtgroupname.GotFocus -= new RoutedEventHandler(txtgroupname_GotFocus);
            rdbtnGroup.Click -= new RoutedEventHandler(rdbtnGroup_Click);
            rdbtnPhotoId.Click -= new RoutedEventHandler(rdbtnPhotoId_Click);
            this.Loaded -= new RoutedEventHandler(Window_Loaded);
            txtRFID.KeyDown -= new KeyEventHandler(txtRFID_KeyDown);
            btnLogout.Click -= new RoutedEventHandler(btnLogout_Click);
            dtselect.PreviewKeyDown -= new KeyEventHandler(DatePicker_PreviewKeyDown);
            dtselect.SelectedDateChanged -= new EventHandler<SelectionChangedEventArgs>(dtselectEnter_SelectionChanged);
            btnLogout.Click -= new RoutedEventHandler(btnLogout_Click);
            btnback.Click -= new RoutedEventHandler(btn_Click);
            btnRefresh.Click -= new RoutedEventHandler(btnRefresh_Click);
            btnHome.Click -= new RoutedEventHandler(btnHome_Click);
            btnA.Click -= new RoutedEventHandler(btn_Click_keyboard);
            btnB.Click -= new RoutedEventHandler(btn_Click_keyboard);
            btnC.Click -= new RoutedEventHandler(btn_Click_keyboard);
            btnD.Click -= new RoutedEventHandler(btn_Click_keyboard);
            btnE.Click -= new RoutedEventHandler(btn_Click_keyboard);
            btnF.Click -= new RoutedEventHandler(btn_Click_keyboard);
            btnG.Click -= new RoutedEventHandler(btn_Click_keyboard);
            btnH.Click -= new RoutedEventHandler(btn_Click_keyboard);
            btnI.Click -= new RoutedEventHandler(btn_Click_keyboard);
            btnJ.Click -= new RoutedEventHandler(btn_Click_keyboard);
            btnK.Click -= new RoutedEventHandler(btn_Click_keyboard);
            btnL.Click -= new RoutedEventHandler(btn_Click_keyboard);
            btnM.Click -= new RoutedEventHandler(btn_Click_keyboard);
            btnN.Click -= new RoutedEventHandler(btn_Click_keyboard);
            btnO.Click -= new RoutedEventHandler(btn_Click_keyboard);
            btnP.Click -= new RoutedEventHandler(btn_Click_keyboard);
            btnQ.Click -= new RoutedEventHandler(btn_Click_keyboard);
            btnR.Click -= new RoutedEventHandler(btn_Click_keyboard);
            btnS.Click -= new RoutedEventHandler(btn_Click_keyboard);
            btnT.Click -= new RoutedEventHandler(btn_Click_keyboard);
            btnU.Click -= new RoutedEventHandler(btn_Click_keyboard);
            btnV.Click -= new RoutedEventHandler(btn_Click_keyboard);
            btnW.Click -= new RoutedEventHandler(btn_Click_keyboard);
            btnX.Click -= new RoutedEventHandler(btn_Click_keyboard);
            btnY.Click -= new RoutedEventHandler(btn_Click_keyboard);
            btnZ.Click -= new RoutedEventHandler(btn_Click_keyboard);
            btn1.Click -= new RoutedEventHandler(btn_Click_keyboard);
            btn0.Click -= new RoutedEventHandler(btn_Click);
            btn2.Click -= new RoutedEventHandler(btn_Click);
            btn3.Click -= new RoutedEventHandler(btn_Click);
            btn4.Click -= new RoutedEventHandler(btn_Click);
            btn5.Click -= new RoutedEventHandler(btn_Click);
            btn6.Click -= new RoutedEventHandler(btn_Click);
            btn7.Click -= new RoutedEventHandler(btn_Click);
            btn8.Click -= new RoutedEventHandler(btn_Click);
            btn9.Click -= new RoutedEventHandler(btn_Click);
            this.Closed -= new EventHandler(Window_Closed);
            //txtgroupname.KeyDown -= new KeyEventHandler(btnSavegroup_Enter);
            txtRFID.KeyDown -= new KeyEventHandler(bttnLogin_Enter);
            _txtKeyboard = null;
            MemoryManagement.FlushMemory();
        }
        public void GetAllDropdown()
        {
            try
            {
                LoadPhotoGrapher();
                LoadLocation();
                LoadSubStore();
                FillCharacter();
            }
            catch (Exception ex)
            {
                ///////ENTRY IN ERROR LOG FOR APPLICATION ERROR 
                ErrorHandler.ErrorHandler.LogError(ex);
            }

        }

        public void LoadSubStore()
        {
            List<string> str = LoginUser.DefaultSubstores.Split(',').ToList();
            var lstSubStore = (new StoreSubStoreDataBusniess()).GetSubstoreData().Where((l => str.Contains(l.DG_SubStore_pkey.ToString()))).ToList();

            CommonUtility.BindComboWithSelect<SubStoresInfo>(cmbSubstore, lstSubStore, "DG_SubStore_Name", "DG_SubStore_pkey", 0, ClientConstant.SelectString);
            cmbSubstore.SelectedValue = "0";
        }

        public void LoadLocation(List<LocationInfo> objLocation = null)
        {
            List<LocationInfo> lstLocations = null;
            if (objLocation == null)
                lstLocations = (new LocationBusniess()).GetLocationName(LoginUser.StoreId);
            else
                lstLocations = objLocation;
            CommonUtility.BindComboWithSelect<LocationInfo>(cmbLocation, lstLocations, "DG_Location_Name", "DG_Location_pkey", 0, ClientConstant.SelectString);
            cmbLocation.SelectedValue = "0";
        }

        public void LoadPhotoGrapher()
        {
            List<PhotoGraphersInfo> lstUsers = (new UserBusiness()).GetPhotoGraphersList(LoginUser.StoreId);
            CommonUtility.BindComboWithSelect<PhotoGraphersInfo>(cmbPhotographer, lstUsers, "DG_User_Name", "DG_User_pkey", 0, ClientConstant.SelectString);
            cmbPhotographer.SelectedValue = "0";
        }

        private void FillCharacter()
        {
            List<CharacterInfo> characters = new List<CharacterInfo>();
            characters = (new CharacterBusiness()).GetCharacter().ToList();
            CommonUtility.BindComboWithSelect<CharacterInfo>(cmbCharacter, characters, "DG_Character_Name", "DG_Character_Pkey", 0, ClientConstant.SelectString);
            cmbCharacter.SelectedValue = "0";

        }
        private void GetPrintersStatus()
        {
            var myprinterdetails = new ObservableCollection<DigiPhoto.Manage.SystemPrinterQueuee.PrinterDetails>();
            var server = new PrintServer();
            var queues = server.GetPrintQueues(new[] { EnumeratedPrintQueueTypes.Local, EnumeratedPrintQueueTypes.Connections });

            var objlist = (new PrinterBusniess()).GetAssociatedPrintersName(LoginUser.SubStoreId);
            if (objlist == null)
                return;
            IEnumerable<string> objprname = objlist.Select(t => t.DG_AssociatedPrinters_Name).Distinct();

            IEnumerator printerEnumerator = objprname.GetEnumerator();
            while (printerEnumerator.MoveNext())
            {
                var name = printerEnumerator.Current;
                var item = queues.FirstOrDefault(t => t.FullName == name.ToString());
                if (item == null) continue;

                var getStatus = getPrinterStatus(item.FullName);
                if (getStatus.Equals("Offline"))
                {
                    myprinterdetails.Add(new DigiPhoto.Manage.SystemPrinterQueuee.PrinterDetails
                        (item.FullName, null, getStatus));
                }
            }

            //for (int i = 0; i < objprname.Count(); i++)
            //{
            //    var item = queues.Where(t => t.FullName == (objprname.ToArray())[i].ToString()).FirstOrDefault();
            //    if (item != null)
            //    {
            //        if (getPrinterStatus(item.FullName) == "Offline")
            //        {
            //            myprinterdetails.Add(new DigiPhoto.Manage.SystemPrinterQueuee.PrinterDetails(item.FullName, null, getPrinterStatus(item.FullName)));
            //        }
            //    }
            //}

            lstprinters.ItemsSource = myprinterdetails;


        }
        private void SetQrCodeSearchState()
        {
            // cmbQRCodeType.SelectedValue = App.SelectedCodeType;

            //if (rdbtnQRCode.IsChecked == true)
            if (rdbtnTime.IsChecked != true) return;

            if (cmbQRCodeType.SelectedValue != null && Convert.ToInt32(cmbQRCodeType.SelectedValue) == 404)
            {
                txtQRCode.Text = _cardCode;
                txtQRCode.IsEnabled = false;
            }
            else
            {
                txtQRCode.Text = string.Empty;
                txtQRCode.IsEnabled = true;
                txtQRCode.Focus();
                FocusManager.SetFocusedElement(this, txtQRCode);
                Keyboard.Focus(txtQRCode);
            }
        }

        private string getPrinterStatus(string printerName)
        {
            // Set management scope
            string retvalue = "";
            var scope = new ManagementScope("\\root\\cimv2");
            scope.Connect();

            // Select Printers from WMI Object Collections
            var searcher = new ManagementObjectSearcher("SELECT * FROM Win32_Printer");

            //var prtName = string.Empty;

            foreach (ManagementObject printer in searcher.Get())
            {
                var prtName = printer["Name"].ToString().ToLower();
                if (!prtName.Equals(printerName.ToLower())) continue;

                Console.WriteLine("Printer = " + printer["Name"]);
                retvalue = printer["WorkOffline"].ToString().ToLower().Equals("true") ? "Offline" : "Online";
            }

            return retvalue;
        }
        private void GetMktImgInfo()
        {
            try
            {
                //_objservice = new DigiPhotoDataServices();
                //ConfigBusiness configbBiz = new ConfigBusiness();
                var objList = new List<long> { (long)ConfigParams.MktImgPath, (long)ConfigParams.MktImgTimeInSec };
                //objList.Add((long)ConfigParams.MktImgPath);
                //objList.Add((long)ConfigParams.MktImgTimeInSec);

                var configValuesList = (new ConfigBusiness()).GetNewConfigValues(LoginUser.SubStoreId)
                                        .Where(o => objList.Contains(o.IMIXConfigurationMasterId)).ToList();

                if (configValuesList != null && configValuesList.Count > 0)
                {
                    foreach (iMIXConfigurationInfo t in configValuesList)
                    {
                        switch (t.IMIXConfigurationMasterId)
                        {
                            case (int)ConfigParams.MktImgPath:
                                _mktImgPath = t.ConfigurationValue;
                                break;
                            case (int)ConfigParams.MktImgTimeInSec:
                                _mktImgTime = (t.ConfigurationValue != null ? Convert.ToInt32(t.ConfigurationValue) : 10) * 1000;
                                break;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
            }
        }
        private void BindCodeType()
        {
            try
            {
                var cardBiz = new CardBusiness();
                var codeType = cardBiz.GetCardCodeTypes();

                var dicItems = new Dictionary<string, int>();
                dicItems.Add("--Select--", 0);

                foreach (var itm in codeType)
                {
                    //406 is rfid, visible only if it is enabled 
                    //if (itm.Value != 406 || (itm.Value == 406 && App.IsRFIDEnabled))
                    dicItems.Add(itm.Key, itm.Value);
                }

                //p.IsRFIDEnabled
                cmbQRCodeType.ItemsSource = dicItems;
                cmbQRCodeType.SelectedValue = "0";

                _cardCode = cardBiz.GetCardCode(404);
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
            }
        }
        private bool IsImageExists(string code)
        {
            try
            {
                //_objservice = new DigiPhotoDataServices();
                //PhotoBusiness photoBiz = new PhotoBusiness();
                return (new PhotoBusiness()).GetImagesBYQRCode(code, RobotImageLoader.IsAnonymousQrCodeEnabled).Count > 0;
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
                return false;
            }
        }
        private void ChangeKey()
        {
            //if (rdbtnQRCode.IsChecked == true)
            if (rdbtnTime.IsChecked == true)
            {
                btnA.Content = "A";
                btnB.Content = "B";
                btnC.Content = "C";
                btnD.Content = "D";
                btnE.Content = "E";
                btnF.Content = "F";
                btnG.Content = "G";
                btnH.Content = "H";
                btnI.Content = "I";
                btnJ.Content = "J";
                btnK.Content = "K";
                btnL.Content = "L";
                btnM.Content = "M";
                btnN.Content = "N";
                btnO.Content = "O";
                btnP.Content = "P";
                btnQ.Content = "Q";
                btnR.Content = "R";
                btnS.Content = "S";
                btnT.Content = "T";
                btnU.Content = "U";
                btnV.Content = "V";
                btnW.Content = "W";
                btnX.Content = "X";
                btnY.Content = "Y";
                btnZ.Content = "Z";

            }
            else
            {
                btnA.Content = "a";
                btnB.Content = "b";
                btnC.Content = "c";
                btnD.Content = "d";
                btnE.Content = "e";
                btnF.Content = "f";
                btnG.Content = "g";
                btnH.Content = "h";
                btnI.Content = "i";
                btnJ.Content = "j";
                btnK.Content = "k";
                btnL.Content = "l";
                btnM.Content = "m";
                btnN.Content = "n";
                btnO.Content = "o";
                btnP.Content = "p";
                btnQ.Content = "q";
                btnR.Content = "r";
                btnS.Content = "s";
                btnT.Content = "t";
                btnU.Content = "u";
                btnV.Content = "v";
                btnW.Content = "w";
                btnX.Content = "x";
                btnY.Content = "y";
                btnZ.Content = "z";
            }
        }

        #endregion




        #region Public Methods
        public void LoadData()
        {
            try
            {
                ///////////////setting control visibility for photoid and time
                switch (RobotImageLoader.SearchCriteria)
                {
                    case "PhotoId":
                        rdbtnPhotoId.IsChecked = true;
                        searchbyPhotoId.Visibility = Visibility.Visible;
                        searchbyTimeRange.Visibility = Visibility.Hidden;
                        txtRFID.Text = "";
                        txtRFID.Focus();
                        break;
                    case "Time":
                        rdbtnTime.IsChecked = true;
                        searchbyPhotoId.Visibility = Visibility.Hidden;
                        searchbyTimeRange.Visibility = Visibility.Visible;
                        txtRFID.Text = "";
                        txtRFID.Focus();
                        break;
                    default:
                        txtRFID.Text = "";
                        txtRFID.Focus();
                        break;
                }
                GetPrintersStatus();
                txbUserName.Text = LoginUser.UserName;
                txbStoreName.Text = LoginUser.StoreName;
                if (Convert.ToBoolean((new ConfigBusiness()).GetConfigEnableGroup(LoginUser.SubStoreId)))
                {
                    rdbtnGroup.Visibility = Visibility.Visible;
                }
                else
                {
                    rdbtnGroup.Visibility = Visibility.Collapsed;
                    PhotoIdSearch();
                }
            }
            catch (Exception ex)
            {
                ///////ENTRY IN ERROR LOG FOR APPLICATION ERROR 
                ErrorHandler.ErrorHandler.LogError(ex);
            }
        }
        #endregion



        ///////changed by latika ATP QR Code workflow 03 Apr 2019 for url split
        private void txtQRCode_KeyDown(object sender, KeyEventArgs e)
        {

            if (e.Key == Key.Enter)
            {
                txtQRCode.Text = txtQRCode.Text.Replace(App.QRCodeWebUrl, string.Empty);
                setQRCode();
                if (txtQRCode.Text.Trim().Length > 20)
                {
                    txtQRCode.Text = txtQRCode.Text.Trim().Remove(0, 20);
                    txtQRCode.SelectionStart = txtQRCode.Text.Length;
                }

            }
        }

        /// <summary>
        /// ////////created by latika remove link for QR Code 12-Mar-19
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtQRCode_MouseLeave(object sender, MouseEventArgs e)
        {
            setQRCode();
        }
        public void setQRCode()
        {
            string[] strTag = txtQRCode.Text.Split('=');
            string tagid = strTag[strTag.Length - 1];
            txtQRCode.Text = tagid;
            txtQRCode.Text = txtQRCode.Text.Trim();
        }

        private void txtQRCode_LostKeyboardFocus(object sender, KeyboardFocusChangedEventArgs e)
        {
            setQRCode();
        }

        private void txtQRCode_TextChanged(object sender, TextChangedEventArgs e)
        {
            setQRCode();
        }

        /// <summary>
        /// By KCB ON 31 AUG 2019 for enable face scan
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void rdbtnScan_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                //ChangeKey();
                searchbyCode.Visibility = Visibility.Collapsed;
                searchbyPhotoId.Visibility = Visibility.Hidden;
                searchbyTimeRange.Visibility = Visibility.Hidden;
                searchbyScan.Visibility = Visibility.Visible;
                txtRFID.Text = "0";
                dtselect.Focus();
                //dtselect.SelectedDate = (new CustomBusineses()).ServerDateTime().Date;
                //dtscselect.Value = DateTime.Now;
                //dtscselectToDate.Value = DateTime.Now;

                dtscselect.Value = DateTime.Now.Date.Add(new TimeSpan(6, 0, 0));
                dtscselectToDate.Value = DateTime.Now.Date.Add(new TimeSpan(23, 59, 59));

                //image1.Source = null;
                //WebCamHelper.MasterImagePath = string.Empty;

                //CalendarDateRange calendarDateRange = new CalendarDateRange();
                //calendarDateRange.Start = DateTime.Now.AddDays(1);
                //dtscselect.BlackoutDates.Add(calendarDateRange);
                //dtscselectToDate.BlackoutDates.Add(calendarDateRange);

                searchbyGroup.Visibility = Visibility.Collapsed;
                txtgroupname.Text = "";
                KeyBorder1.Visibility = Visibility.Collapsed;

                //Set the state of selection
                App.ViewOrderSearchIndex = rdbtnTime.TabIndex;
                btnsearch.IsDefault = true;
                //By Kailash Chandra Behera on 06 MAY 2019 to capture image from webcam
                // TODO: Add event handler implementation here.
                //if (this.webcam == null)
                //{
                //    this.webcam = new WebCam();
                //    this.webcam.InitializeWebCam(ref this.imgVideo);
                //    this.webcam.Start();
                //}

                try
                {
                    InitializeComponent();
                    //m_ImageCore = new ImageCore();
                    //dSViewer1.Bind(m_ImageCore);
                    m_CameraManager = new CameraManager(m_StrProductKey);
                    //cbxSources.SelectedIndex = 0;

                    cbxSources.Items.Clear();//Clear all cameras
                    if (m_CameraManager.GetCameraNames() != null)
                    {
                        foreach (string temp in m_CameraManager.GetCameraNames())
                        {
                            this.cbxSources.Items.Add(temp);
                        }

                        if (cbxSources.Items.Count > 0)
                        {
                            if (cbxSources.Items.Count > Properties.Settings.Default.SelectedCamera)
                                cbxSources.SelectedIndex = Properties.Settings.Default.SelectedCamera;
                            else
                                cbxSources.SelectedIndex = 0;
                        }
                    }
                    m_Refresh += new RefreshDelegate(RefreshImage);
                    m_ControlWindow = Window.GetWindow(image1);

                    StoreSubStoreDataBusniess storeSubStoreDataBusniess = new StoreSubStoreDataBusniess();
                    List<SubStoresInfo> subStores = storeSubStoreDataBusniess.GetSubstoreData();
                    subStores.Insert(0, new SubStoresInfo() { DG_SubStore_Name = "ALL", DG_SubStore_pkey = 0 });
                    this.cbxStore.ItemsSource = subStores;
                    this.cbxStore.SelectedIndex = 0;

                }
                catch (Exception exp)
                {
                    MessageBox.Show(exp.Message);
                }
                //End
            }
            catch (Exception ex)
            {

            }
        }
        /// <summary>
        /// By KCB on 05 JUN 2019By KCB ON 31 AUG 2019 for enable face scan        
        /// </summary>
        /// <param name="temp"></param>
        private void RefreshImage(System.Windows.Media.Imaging.BitmapImage temp)
        {
            try
            {
                DSViewer.Source = temp;
            }
            catch
            {
            }

        }
        /// <summary>
        /// By KCB on 05 JUN 2019
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnCapture_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                //System.Threading.Thread.Sleep(100);
                Bitmap temp = m_Camera.GrabImage();
                image1.Source = DSViewer.Source;

                //if (m_Camera != null)
                //    m_Camera.Open();//Need to check exception
                //System.Threading.Thread.Sleep(1000);
                WebCamHelper.SaveImageCapture((BitmapSource)this.image1.Source);
            }
            catch (Exception ex)
            {

            }
        }
        /// <summary>
        /// By KCB on 05 JUN 2019By KCB ON 31 AUG 2019 for enable face scan        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Reset_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                this.image1.Source = null;
                System.IO.File.Delete(WebCamHelper.MasterImagePath);
                WebCamHelper.MasterImagePath = string.Empty;
            }
            catch (Exception ex)
            {

            }
        }
        /// <summary>
        /// By KCB ON 31 AUG 2019 for enable face scan
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnsearchwithscan_Click(object sender, RoutedEventArgs e)
        {
            this.Cursor = Cursors.Wait;
            //ErrorHandler.ErrorHandler.LogFileWrite("KCB1");

            //ErrorHandler.ErrorHandler.LogFileWrite("KCB1");
            // string s = configBusiness.GetSubStoreNameBySuubStoreId(configurationInfos[0].DG_Substore_Id.Value);

            if (string.IsNullOrEmpty(WebCamHelper.MasterImagePath))
            {
                MessageBox.Show("Image not captured", "Requeired Guest Photo", MessageBoxButton.OK, MessageBoxImage.Information);
                ErrorHandler.ErrorHandler.LogFileWrite("No image captured");
                this.Cursor = Cursors.Arrow;
                return;
            }
            rdbtnPhotoId.IsChecked = true;
            //By KCB ON 05 AUG 2019 for manage camera
            if (m_Camera != null)
            {
                m_Camera.Close();
                m_Camera.Dispose();
                m_Camera = null;
            }
            //image1.Source = null;
            //ErrorHandler.ErrorHandler.LogFileWrite("KCB2");

            try
            {
                //ConfigBusiness configBusiness = new ConfigBusiness();
                //List<ConfigurationInfo> configurationInfos = configBusiness.GetAllSubstoreConfigdata();

                #region will be uncomment soon
                APIWrapper aPIWrapper = new APIWrapper();

                //string _threashold = "LOW";
                if (thbthreshold1.IsChecked == true)
                    APIWrapper.SelectedThreshold = "HIGH";
                else if (thbthreshold2.IsChecked == true)
                    APIWrapper.SelectedThreshold = "MEDIUM";
                else if (thbthreshold4.IsChecked == true)
                    APIWrapper.SelectedThreshold = "LOWER";

                DateTime dateTime = dtscselectToDate.Value.Value;
                APIWrapper.SelectedSite = Convert.ToInt32(cbxStore.SelectedValue);
                APIResponse response = aPIWrapper.GetImages(WebCamHelper.MasterImagePath, dtscselect.Value.Value.ToString("dd-MM-yyyy HH:mm:ss"), dateTime.ToString("dd-MM-yyyy HH:mm:ss"), APIWrapper.SelectedThreshold, APIWrapper.SelectedSite);
                #endregion
                //APIResponse response = new APIResponse();
                //    response.SiteIDs= "45,44,43,42,41,40,39,38,37,36,35,34,33,32,31,30,29,28,27,26".Split(',').ToList();
                RobotImageLoader.IsZeroSearchNeeded = false;

                RobotImageLoader.RFID = String.Join(",", response.SiteIDs);
                RobotImageLoader.SearchCriteria = "FaceScan";
                if (RobotImageLoader.GroupImages == null)
                {
                    RobotImageLoader.GroupImages = new List<LstMyItems>();
                }
                else
                {
                    if (RobotImageLoader.GroupImages.Count == 0)
                    {
                        RobotImageLoader.GroupImages = new List<LstMyItems>();
                    }
                }
                RobotImageLoader._rfidSearch = 0;
                RobotImageLoader.StartIndexRFID = 0;
                ErrorHandler.ErrorHandler.LogFileWrite("1630");
                if (response != null)
                {
                    if (response.SiteIDs != null && response.SiteIDs.Count() > 0)
                    {
                        ErrorHandler.ErrorHandler.LogFileWrite("No of photo : " + response.SiteIDs.Count());
                        RobotImageLoader.PhotoIds = response.SiteIDs;
                    }
                    else
                    {
                        RobotImageLoader.PhotoIds = new List<string>();
                        RobotImageLoader.PhotoIds.Add("0");
                    }
                    ErrorHandler.ErrorHandler.LogFileWrite("1638 Response not null");
                }
                else
                {
                    RobotImageLoader.PhotoIds = new List<string>();
                    RobotImageLoader.PhotoIds.Add("0");
                }

                //RobotImageLoader.PhotoIds = ids.ToList();

                //////creating Instance of Search result window//////
                SearchResult objSearchResult = null;

                //Commented for fixing issue of refreshing screen
                ErrorHandler.ErrorHandler.LogFileWrite("1646");
                foreach (Window wnd in Application.Current.Windows)
                {
                    if (wnd.Title == "View/Order Station")
                    {
                        objSearchResult = (SearchResult)wnd;
                    }
                }
                if (objSearchResult == null)
                    objSearchResult = new SearchResult();
                if (RobotImageLoader.NotPrintedImages == null)
                    RobotImageLoader.NotPrintedImages = new List<LstMyItems>();

                RobotImageLoader._objnewincrement = new List<LstMyItems>();
                objSearchResult.grdSelectAll.Visibility = Visibility.Visible; // Change this
                RobotImageLoader.ViewGroupedImagesCount = new List<string>();
                objSearchResult.pagename = "";
                objSearchResult.UpdateThreshold();
                objSearchResult.Show();
                objSearchResult.returntoHome = false;
                ////////Calling logic of searching 

                if ((RobotImageLoader.PhotoIds.Count() == 1 && RobotImageLoader.PhotoIds[0] != "0") || (RobotImageLoader.PhotoIds.Count() > 1))
                {
                    ErrorHandler.ErrorHandler.LogFileWrite("Entering SearchImages");
                    //objSearchResult.SearchImages(response.SiteIDs.Take(10).ToList());
                    objSearchResult.SearchImages(RobotImageLoader.PhotoIds);
                    ErrorHandler.ErrorHandler.LogFileWrite("Came out from SearchImages");
                }
                else
                {
                    List<string> lst = new List<string>();
                    lst.Add("0");
                    RobotImageLoader.PhotoIds = lst;
                    ErrorHandler.ErrorHandler.LogFileWrite("1676");
                    objSearchResult.SearchImages(lst);
                    ErrorHandler.ErrorHandler.LogFileWrite("1678");
                }
                //objSearchResult.SearchImages(ids.ToList());
                ////////Closing current window                               
                if (cmbQRCodeType.SelectedValue.ToString() != "404")
                    txtQRCode.Text = string.Empty;
                this.Hide();

                //ErrorHandler.ErrorHandler.LogFileWrite("Closed Camera");
                //m_CameraManager = null;
                App.ViewOrderSearchIndex = 0;
            }
            catch (Exception ex)
            {
                this.Hide();
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
            finally
            {
                this.Cursor = Cursors.Arrow;
            }
        }
        /// <summary>
        ///By KCB ON 31 AUG 2019 for enable face scan
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void searchbyScan_Loaded(object sender, RoutedEventArgs e)
        {
            
        }
        /// <summary>
        /// By KCB ON 31 AUG 2019 for Listout camera resolutions
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cbxSources_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (m_CameraManager.GetCameraNames() != null)
            {
                if (((ComboBox)sender).SelectedIndex >= 0 && ((ComboBox)sender).SelectedIndex < m_CameraManager.GetCameraNames().Count)
                {
                    m_Camera = m_CameraManager.SelectCamera((short)cbxSources.SelectedIndex);
                    try
                    {
                        m_Camera.Open();
                    }
                    catch (Exception ex)
                    {
                        m_Camera.Close();
                        m_Camera.Open();
                    }
                    //m_Camera.Open();
                    m_Camera.OnFrameCaptrue += m_Camera_OnFrameCaptrue;
                    ResizePictureBox();
                    Properties.Settings.Default.SelectedCamera = cbxSources.SelectedIndex;
                    Properties.Settings.Default.Save();
                }
                if (m_Camera != null)
                {
                    cbxResolution.Items.Clear();
                    foreach (CamResolution camR in m_Camera.SupportedResolutions)
                    {
                        cbxResolution.Items.Add(camR.ToString());
                    }
                    if (cbxResolution.Items.Count > 0 && cbxResolution.Items.Count > Properties.Settings.Default.Resolution)
                    {

                        ErrorHandler.ErrorHandler.LogFileWrite("Properties.Settings.Default.Resolution :" + Properties.Settings.Default.Resolution);
                        cbxResolution.SelectedIndex = Properties.Settings.Default.Resolution;
                    }
                    else
                        cbxResolution.SelectedIndex = 0;
                    ResizePictureBox();

                    this.cbxResolution.IsEnabled = true;
                    this.btnCapture.IsEnabled = true;
                    this.btnReset.IsEnabled = true;
                    this.btnsearchwithscan.IsEnabled = true;

                }
                else
                {
                    this.cbxResolution.IsEnabled = false;
                    this.btnCapture.IsEnabled = false;
                    this.btnRefresh.IsEnabled = false;
                    this.btnsearchwithscan.IsEnabled = false;
                    //Properties.Settings.Default.SelectedCamera = 0;
                    //WebCamHelper.MasterImagePath = string.Empty;
                }

            }

        }
        /// <summary>
        /// By KCB ON 31 AUG 2019 for Listout camera resolutions
        /// </summary>
        /// <param name="bitmap"></param>
        private void m_Camera_OnFrameCaptrue(Bitmap bitmap)
        {
            System.Windows.Media.Imaging.BitmapImage m_Frame = new System.Windows.Media.Imaging.BitmapImage();
            using (var stream = new System.IO.MemoryStream())
            {
                bitmap.Save(stream, System.Drawing.Imaging.ImageFormat.Bmp);
                stream.Seek(0, System.IO.SeekOrigin.Begin);
                m_Frame.BeginInit();
                m_Frame.CacheOption = System.Windows.Media.Imaging.BitmapCacheOption.OnLoad;
                m_Frame.StreamSource = stream;
                m_Frame.EndInit();
                stream.SetLength(0);
                stream.Capacity = 0;
                stream.Dispose();

            }
            m_Frame.Freeze();
            if (m_ControlWindow != null)
                m_ControlWindow.Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.SystemIdle, m_Refresh, m_Frame);
        }
        /// <summary>
        //By KCB ON 31 AUG 2019 for resizeing picture box
        /// </summary>
        private void ResizePictureBox()
        {
            if (m_Camera != null)
            {
                CamResolution camResolution = m_Camera.CurrentResolution;
                if (camResolution != null && camResolution.Width > 0 && camResolution.Height > 0)
                {
                    {
                        double iVideoWidth = gvcapture.Width;
                        double iVideoHeight = gvcapture.Width * camResolution.Height / camResolution.Width;
                        if (iVideoHeight < gvcapture.Height)
                        {
                            image1.Margin = new Thickness(0, (gvcapture.Height - iVideoHeight) / 2, 0, 0);
                            image1.Width = iVideoWidth;
                            image1.Height = iVideoHeight;
                        }
                        else
                        {
                            image1.Margin = new Thickness(0, 0, 0, 0);
                            image1.Width = iVideoWidth;
                            image1.Height = iVideoHeight;
                        }
                    }
                }
            }
        }
        /// <summary>
        /// KCB On 25 31 AUG 2019 for resizeing picture box
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cbxResolution_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (cbxResolution.SelectedValue != null)
            {
                string[] strWXH = cbxResolution.SelectedValue.ToString().Split(new char[] { ' ' });
                if (strWXH.Length == 3)
                {
                    try
                    {
                        m_Camera.CurrentResolution = new CamResolution(int.Parse(strWXH[0]), int.Parse(strWXH[2]));
                    }
                    catch { }
                }
                m_Camera.RotateVideo(Dynamsoft.UVC.Enums.EnumVideoRotateType.Rotate_0);
                ResizePictureBox();
            }
        }
        /////end
        private void CbxStore_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            APIWrapper.SelectedSite = Convert.ToInt32(cbxStore.SelectedValue);
        }
    }

}
