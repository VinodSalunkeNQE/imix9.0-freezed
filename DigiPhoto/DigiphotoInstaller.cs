﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration.Install;
using System.Linq;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;
using System.Windows.Forms;
using System.IO;
using System.Reflection;
using FrameworkHelper.Common;
using System.Diagnostics;
using System.ServiceProcess;
using System.Net;
using System.Management;
using DigiPhoto.IMIX.Model;
using DigiPhoto.IMIX.Business;
using DigiPhoto.DataLayer;
using DigiPhoto.Common;
using FrameworkHelper;



namespace DigiPhoto
{
    [RunInstaller(true)]
    public partial class DigiphotoInstaller : System.Configuration.Install.Installer
    {
        private String conn = string.Empty;
        public DigiphotoInstaller()
        {

            InitializeComponent();
        }

        public override void Install(System.Collections.IDictionary stateSaver)
        {
            try
            {
                base.Install(stateSaver);
                //System.Diagnostics.Debugger.Launch();
                //System.Diagnostics.Debugger.Break();
                //MessageBox.Show("Install section");
                string targetDirectory = Context.Parameters["targetdir"];
                string servername = Context.Parameters["Servername"];
                string username = Context.Parameters["Username"];
                string password = Context.Parameters["Password"];
                string hotfolder = Context.Parameters["HotFolder"];
                string database = "Digiphoto";

                ServiceController ctl = ServiceController.GetServices().FirstOrDefault(j => j.ServiceName == "DigiBackupService");

                if (string.IsNullOrEmpty(targetDirectory) || string.IsNullOrEmpty(servername) || string.IsNullOrEmpty(username) || string.IsNullOrEmpty(password) || string.IsNullOrEmpty(hotfolder))
                {
                    if (ctl != null)
                    {
                        return;
                    }
                }

                string exePath = string.Format("{0}DigiPhoto.exe", targetDirectory);
                Configuration config = ConfigurationManager.OpenExeConfiguration(exePath);
                string connectionsection = config.ConnectionStrings.ConnectionStrings
                ["DigiConnectionString"].ConnectionString;
                
                //Removing Existing Connection string if available Adding new one

                string connectionsection2 = config.ConnectionStrings.ConnectionStrings
                ["DigiphotoEntities"].ConnectionString;
                ConnectionStringSettings connectionstring = null;
                if (connectionsection != null)
                {
                    config.ConnectionStrings.ConnectionStrings.Remove("DigiConnectionString");
                }
                if (connectionsection2 != null)
                {
                    config.ConnectionStrings.ConnectionStrings.Remove("DigiphotoEntities");
                }

                conn = "Data Source=" + servername + ";Initial Catalog=" + database + ";User ID=" + username + ";Password=" + password + ";MultipleActiveResultSets=True";
                string conn2 = "metadata=res://*/Model.DigiDataModel.csdl|res://*/Model.DigiDataModel.ssdl|res://*/Model.DigiDataModel.msl;provider=System.Data.SqlClient;provider connection string ='Data Source= " + servername + ";Initial Catalog=" + database + ";User ID=" + username + ";Password=" + password + ";MultipleActiveResultSets=True'";

                connectionstring = new ConnectionStringSettings("DigiConnectionString", conn);
                config.ConnectionStrings.ConnectionStrings.Add(connectionstring);
                ConfigurationSection section = config.GetSection("connectionStrings");
                //Ensures that the section is not already protected
                if (!section.SectionInformation.IsProtected)
                {
                    //Uses the Windows Data Protection API (DPAPI) to encrypt the configuration section
                    //using a machine-specific secret key
                    section.SectionInformation.ProtectSection("DataProtectionConfigurationProvider");
                }
                config.Save();
                ConfigurationManager.RefreshSection("connectionStrings");

                connectionstring = new ConnectionStringSettings("DigiphotoEntities", conn2);
                config.ConnectionStrings.ConnectionStrings.Add(connectionstring);
                config.Save();
                //config.SaveAs(@"/BackupUtility/BackupUtility/bin/Debug/BackupUtility.exe.config", ConfigurationSaveMode.Modified);
                ConfigurationManager.RefreshSection("connectionStrings");


                #region Code to Comment for POS
                //For Printing Console
                exePath = string.Format("{0}DigiPrintingConsole.exe", targetDirectory);
                config = ConfigurationManager.OpenExeConfiguration(exePath);
                connectionsection = config.ConnectionStrings.ConnectionStrings["DigiConnectionString"].ConnectionString;
                connectionsection2 = config.ConnectionStrings.ConnectionStrings["DigiphotoEntities"].ConnectionString;
                connectionstring = null;
                if (connectionsection != null)
                {
                    config.ConnectionStrings.ConnectionStrings.Remove("DigiConnectionString");
                }
                if (connectionsection2 != null)
                {
                    config.ConnectionStrings.ConnectionStrings.Remove("DigiphotoEntities");
                }
                connectionstring = new ConnectionStringSettings("DigiConnectionString", conn);
                config.ConnectionStrings.ConnectionStrings.Add(connectionstring);
                ConfigurationSection section2 = config.GetSection("connectionStrings");
                //Ensures that the section is not already protected
                if (!section2.SectionInformation.IsProtected)
                {
                    //Uses the Windows Data Protection API (DPAPI) to encrypt the configuration section
                    //using a machine-specific secret key
                    section2.SectionInformation.ProtectSection("DataProtectionConfigurationProvider");
                }
                config.Save();
                ConfigurationManager.RefreshSection("connectionStrings");

                connectionstring = new ConnectionStringSettings("DigiphotoEntities", conn2);
                config.ConnectionStrings.ConnectionStrings.Add(connectionstring);
                config.Save();
                ConfigurationManager.RefreshSection("connectionStrings");

                //Section for Image Processing Engine
                exePath = string.Format("{0}ImageProcessingEngine.exe", targetDirectory);
                config = ConfigurationManager.OpenExeConfiguration(exePath);
                connectionsection = config.ConnectionStrings.ConnectionStrings["DigiConnectionString"].ConnectionString;
                connectionsection2 = config.ConnectionStrings.ConnectionStrings["DigiphotoEntities"].ConnectionString;
                connectionstring = null;
                if (connectionsection != null)
                {
                    config.ConnectionStrings.ConnectionStrings.Remove("DigiConnectionString");
                }
                if (connectionsection2 != null)
                {
                    config.ConnectionStrings.ConnectionStrings.Remove("DigiphotoEntities");
                }
                connectionstring = new ConnectionStringSettings("DigiConnectionString", conn);
                config.ConnectionStrings.ConnectionStrings.Add(connectionstring);

                ConfigurationSection imageProcessingsection = config.GetSection("connectionStrings");
                //Ensures that the section is not already protected
                if (!imageProcessingsection.SectionInformation.IsProtected)
                {
                    //Uses the Windows Data Protection API (DPAPI) to encrypt the configuration section
                    //using a machine-specific secret key
                    imageProcessingsection.SectionInformation.ProtectSection("DataProtectionConfigurationProvider");
                }
                config.Save();
                ConfigurationManager.RefreshSection("connectionStrings");

                connectionstring = new ConnectionStringSettings("DigiphotoEntities", conn2);
                config.ConnectionStrings.ConnectionStrings.Add(connectionstring);
                config.Save();
                ConfigurationManager.RefreshSection("connectionStrings");
                
                //Section for Video Processing Engine
                exePath = string.Format("{0}VideoProcessingEngine.exe", targetDirectory);
                config = ConfigurationManager.OpenExeConfiguration(exePath);
                connectionsection = config.ConnectionStrings.ConnectionStrings["DigiConnectionString"].ConnectionString;
                connectionsection2 = config.ConnectionStrings.ConnectionStrings["DigiphotoEntities"].ConnectionString;
                connectionstring = null;
                if (connectionsection != null)
                {
                    config.ConnectionStrings.ConnectionStrings.Remove("DigiConnectionString");
                }
                if (connectionsection2 != null)
                {
                    config.ConnectionStrings.ConnectionStrings.Remove("DigiphotoEntities");
                }
                connectionstring = new ConnectionStringSettings("DigiConnectionString", conn);
                config.ConnectionStrings.ConnectionStrings.Add(connectionstring);
                ConfigurationSection VideoProcessingsection = config.GetSection("connectionStrings");
                //Ensures that the section is not already protected
                if (!VideoProcessingsection.SectionInformation.IsProtected)
                {
                    //Uses the Windows Data Protection API (DPAPI) to encrypt the configuration section
                    //using a machine-specific secret key
                    VideoProcessingsection.SectionInformation.ProtectSection("DataProtectionConfigurationProvider");
                }
                //config.Save(ConfigurationSaveMode.Modified, true);
                config.Save();
                ConfigurationManager.RefreshSection("connectionStrings");
                connectionstring = new ConnectionStringSettings("DigiphotoEntities", conn2);

                config.ConnectionStrings.ConnectionStrings.Add(connectionstring);
                //config.Save(ConfigurationSaveMode.Modified, true);
                config.Save();
                ConfigurationManager.RefreshSection("connectionStrings");

                //Section for Live Video Editings
                exePath = string.Format("{0}LiveVideoEditing.exe", targetDirectory);
                config = ConfigurationManager.OpenExeConfiguration(exePath);
                connectionsection = config.ConnectionStrings.ConnectionStrings["DigiConnectionString"].ConnectionString;
                connectionsection2 = config.ConnectionStrings.ConnectionStrings["DigiphotoEntities"].ConnectionString;
                connectionstring = null;
                if (connectionsection != null)
                {
                    config.ConnectionStrings.ConnectionStrings.Remove("DigiConnectionString");
                }
                if (connectionsection2 != null)
                {
                    config.ConnectionStrings.ConnectionStrings.Remove("DigiphotoEntities");
                }
                connectionstring = new ConnectionStringSettings("DigiConnectionString", conn);
                config.ConnectionStrings.ConnectionStrings.Add(connectionstring);
                ConfigurationSection LiveVideoEditingsection = config.GetSection("connectionStrings");
                //Ensures that the section is not already protected
                if (!LiveVideoEditingsection.SectionInformation.IsProtected)
                {
                    //Uses the Windows Data Protection API (DPAPI) to encrypt the configuration section
                    //using a machine-specific secret key
                    LiveVideoEditingsection.SectionInformation.ProtectSection("DataProtectionConfigurationProvider");
                }

                //config.Save(ConfigurationSaveMode.Modified, true);
                config.Save();
                ConfigurationManager.RefreshSection("connectionStrings");

                connectionstring = new ConnectionStringSettings("DigiphotoEntities", conn2);
                config.ConnectionStrings.ConnectionStrings.Add(connectionstring);
                //config.Save(ConfigurationSaveMode.Modified, true);
                config.Save();
                ConfigurationManager.RefreshSection("connectionStrings");

                //For PresoldService
                exePath = string.Format("{0}PreSoldService.exe", targetDirectory);
                config = ConfigurationManager.OpenExeConfiguration(exePath);
                connectionsection = config.ConnectionStrings.ConnectionStrings["DigiConnectionString"].ConnectionString;

                connectionstring = null;
                if (connectionsection != null)
                {
                    config.ConnectionStrings.ConnectionStrings.Remove("DigiConnectionString");
                }
                connectionstring = new ConnectionStringSettings("DigiConnectionString", conn);
                config.ConnectionStrings.ConnectionStrings.Add(connectionstring);
                ConfigurationSection presoldsection = config.GetSection("connectionStrings");
                //Ensures that the section is not already protected
                if (!presoldsection.SectionInformation.IsProtected)
                {
                    //Uses the Windows Data Protection API (DPAPI) to encrypt the configuration section
                    //using a machine-specific secret key
                    presoldsection.SectionInformation.ProtectSection("DataProtectionConfigurationProvider");
                }
                config.Save();
                ConfigurationManager.RefreshSection("connectionStrings");

                //For DigiWatermark Service
                exePath = string.Format("{0}DigiWatermarkService.exe", targetDirectory);
                config = ConfigurationManager.OpenExeConfiguration(exePath);
                connectionsection = config.ConnectionStrings.ConnectionStrings["DigiConnectionString"].ConnectionString;
                connectionstring = null;
                if (connectionsection != null)
                {
                    config.ConnectionStrings.ConnectionStrings.Remove("DigiConnectionString");
                }
                connectionstring = new ConnectionStringSettings("DigiConnectionString", conn);
                config.ConnectionStrings.ConnectionStrings.Add(connectionstring);
                ConfigurationSection digiwatermarksection = config.GetSection("connectionStrings");
                //Ensures that the section is not already protected
                if (!digiwatermarksection.SectionInformation.IsProtected)
                {
                    //Uses the Windows Data Protection API (DPAPI) to encrypt the configuration section
                    //using a machine-specific secret key
                    digiwatermarksection.SectionInformation.ProtectSection("DataProtectionConfigurationProvider");
                }
                config.Save();
                ConfigurationManager.RefreshSection("connectionStrings");


                //         //For DigiSyncService
                //         exePath = string.Format("{0}DigiSyncService.exe", targetDirectory);
                //         config = ConfigurationManager.OpenExeConfiguration(exePath);

                //         connectionsection = config.ConnectionStrings.ConnectionStrings
                //["DigiConnectionString"].ConnectionString;


                //         connectionstring = null;
                //         if (connectionsection != null)
                //         {
                //             config.ConnectionStrings.ConnectionStrings.Remove("DigiConnectionString");
                //         }

                //         connectionstring = new ConnectionStringSettings("DigiConnectionString", conn);
                //         config.ConnectionStrings.ConnectionStrings.Add(connectionstring);
                //         ConfigurationSection syncsection = config.GetSection("connectionStrings");
                //         //Ensures that the section is not already protected
                //         if (!syncsection.SectionInformation.IsProtected)
                //         {
                //             //Uses the Windows Data Protection API (DPAPI) to encrypt the configuration section
                //             //using a machine-specific secret key
                //             syncsection.SectionInformation.ProtectSection("DataProtectionConfigurationProvider");
                //         }
                //         config.Save();
                //         ConfigurationManager.RefreshSection("connectionStrings");


                //For EmailService
                exePath = string.Format("{0}EmailService.exe", targetDirectory);
                config = ConfigurationManager.OpenExeConfiguration(exePath);
                connectionsection = config.ConnectionStrings.ConnectionStrings
                ["DigiConnectionString"].ConnectionString;
                connectionstring = null;
                if (connectionsection != null)
                {
                    config.ConnectionStrings.ConnectionStrings.Remove("DigiConnectionString");
                }
                connectionstring = new ConnectionStringSettings("DigiConnectionString", conn);
                config.ConnectionStrings.ConnectionStrings.Add(connectionstring);
                ConfigurationSection emailsection = config.GetSection("connectionStrings");
                //Ensures that the section is not already protected
                if (!emailsection.SectionInformation.IsProtected)
                {
                    //Uses the Windows Data Protection API (DPAPI) to encrypt the configuration section
                    //using a machine-specific secret key
                    emailsection.SectionInformation.ProtectSection("DataProtectionConfigurationProvider");
                }
                config.Save();
                ConfigurationManager.RefreshSection("connectionStrings");

                //For DigiWatcher
                exePath = string.Format("{0}DigiWatcher.exe", targetDirectory);
                config = ConfigurationManager.OpenExeConfiguration(exePath);
                connectionsection = config.ConnectionStrings.ConnectionStrings
                ["DigiConnectionString"].ConnectionString;

                connectionsection2 = config.ConnectionStrings.ConnectionStrings
                ["DigiphotoEntities"].ConnectionString;
                connectionstring = null;
                if (connectionsection != null)
                {
                    config.ConnectionStrings.ConnectionStrings.Remove("DigiConnectionString");
                }
                if (connectionsection2 != null)
                {
                    config.ConnectionStrings.ConnectionStrings.Remove("DigiphotoEntities");
                }
                connectionstring = new ConnectionStringSettings("DigiConnectionString", conn);
                config.ConnectionStrings.ConnectionStrings.Add(connectionstring);
                ConfigurationSection section3 = config.GetSection("connectionStrings");
                //Ensures that the section is not already protected
                if (!section3.SectionInformation.IsProtected)
                {
                    //Uses the Windows Data Protection API (DPAPI) to encrypt the configuration section
                    //using a machine-specific secret key
                    section3.SectionInformation.ProtectSection("DataProtectionConfigurationProvider");
                }
                config.Save();
                ConfigurationManager.RefreshSection("connectionStrings");

                connectionstring = new ConnectionStringSettings("DigiphotoEntities", conn2);
                config.ConnectionStrings.ConnectionStrings.Add(connectionstring);
                config.Save();
                ConfigurationManager.RefreshSection("connectionStrings");


                //For DigiWindowService
                exePath = string.Format("{0}DigiWifiImageProcessing.exe", targetDirectory);
                config = ConfigurationManager.OpenExeConfiguration(exePath);
                connectionsection = config.ConnectionStrings.ConnectionStrings["DigiConnectionString"].ConnectionString;
                connectionstring = null;
                if (connectionsection != null)
                {
                    config.ConnectionStrings.ConnectionStrings.Remove("DigiConnectionString");
                }
                connectionstring = new ConnectionStringSettings("DigiConnectionString", conn);
                config.ConnectionStrings.ConnectionStrings.Add(connectionstring);
                ConfigurationSection wifisection = config.GetSection("connectionStrings");
                //Ensures that the section is not already protected
                if (!wifisection.SectionInformation.IsProtected)
                {
                    //Uses the Windows Data Protection API (DPAPI) to encrypt the configuration section
                    //using a machine-specific secret key
                    wifisection.SectionInformation.ProtectSection("DataProtectionConfigurationProvider");
                }
                config.Save();
                ConfigurationManager.RefreshSection("connectionStrings");

                //    //For PresoldService
                //    exePath = string.Format("{0}PreSoldService.exe", targetDirectory);
                //    config = ConfigurationManager.OpenExeConfiguration(exePath);
                //    connectionsection = config.ConnectionStrings.ConnectionStrings["DigiConnectionString"].ConnectionString;

                //    connectionstring = null;
                //    if (connectionsection != null)
                //    {
                //        config.ConnectionStrings.ConnectionStrings.Remove("DigiConnectionString");
                //    }
                //    connectionstring = new ConnectionStringSettings("DigiConnectionString", conn);
                //    config.ConnectionStrings.ConnectionStrings.Add(connectionstring);
                //    ConfigurationSection presoldsection = config.GetSection("connectionStrings");
                //    //Ensures that the section is not already protected
                //    if (!presoldsection.SectionInformation.IsProtected)
                //    {
                //        //Uses the Windows Data Protection API (DPAPI) to encrypt the configuration section
                //        //using a machine-specific secret key
                //        presoldsection.SectionInformation.ProtectSection("DataProtectionConfigurationProvider");
                //    }
                //     config.Save();
                //    ConfigurationManager.RefreshSection("connectionStrings");


                //config = ConfigurationManager.OpenExeConfiguration(exePath);
                //config.AppSettings.Settings["DigiConnString"].Value = conn;
                //ConfigurationSection sectionapp = config.GetSection("appSettings");

                ////Ensures that the section is not already protected
                //if (!sectionapp.SectionInformation.IsProtected)
                //{
                //    //Uses the Windows Data Protection API (DPAPI) to encrypt the configuration section
                //    //using a machine-specific secret key
                //    sectionapp.SectionInformation.ProtectSection("DataProtectionConfigurationProvider");
                //}
                //config.Save();

                //For DigiConfigUtility      
                exePath = string.Format("{0}DigiConfigUtility.exe", targetDirectory);
                config = ConfigurationManager.OpenExeConfiguration(exePath);
                connectionsection = config.ConnectionStrings.ConnectionStrings["DigiConnectionString"].ConnectionString;
                connectionsection2 = config.ConnectionStrings.ConnectionStrings["DigiphotoEntities"].ConnectionString;
                connectionstring = null;
                if (connectionsection != null)
                {
                    config.ConnectionStrings.ConnectionStrings.Remove("DigiConnectionString");
                }
                if (connectionsection2 != null)
                {
                    config.ConnectionStrings.ConnectionStrings.Remove("DigiphotoEntities");
                }
                connectionstring = new ConnectionStringSettings("DigiConnectionString", conn);
                config.ConnectionStrings.ConnectionStrings.Add(connectionstring);
                ConfigurationSection configUtilitySection1 = config.GetSection("connectionStrings");
                //Ensures that the section is not already protected
                if (!configUtilitySection1.SectionInformation.IsProtected)
                {
                    //Uses the Windows Data Protection API (DPAPI) to encrypt the configuration section
                    //using a machine-specific secret key
                    configUtilitySection1.SectionInformation.ProtectSection("DataProtectionConfigurationProvider");
                }
                config.Save();
                ConfigurationManager.RefreshSection("connectionStrings");

                connectionstring = new ConnectionStringSettings("DigiphotoEntities", conn2);
                config.ConnectionStrings.ConnectionStrings.Add(connectionstring);
                config.Save();
                ConfigurationManager.RefreshSection("connectionStrings");

                ////----Start---------Encrypt connection string of Cleanup Service----------------
                //exePath = string.Format("{0}DigiDBCleanupService.exe", targetDirectory);
                //config = ConfigurationManager.OpenExeConfiguration(exePath);
                //connectionsection = config.ConnectionStrings.ConnectionStrings
                //["DigiConnectionString"].ConnectionString;
                //connectionstring = null;
                //if (connectionsection != null)
                //{
                //    config.ConnectionStrings.ConnectionStrings.Remove("DigiConnectionString");
                //}
                //connectionstring = new ConnectionStringSettings("DigiConnectionString", conn);
                //config.ConnectionStrings.ConnectionStrings.Add(connectionstring);
                //ConfigurationSection cleanUpServiceSection = config.GetSection("connectionStrings");
                ////Ensures that the section is not already protected
                //if (!cleanUpServiceSection.SectionInformation.IsProtected)
                //{
                //    //Uses the Windows Data Protection API (DPAPI) to encrypt the configuration section
                //    //using a machine-specific secret key
                //    cleanUpServiceSection.SectionInformation.ProtectSection("DataProtectionConfigurationProvider");
                //}
                //config.Save();
                //ConfigurationManager.RefreshSection("connectionStrings");
                ////----End-----------Encrypt connection string of cleanup service ---------------
                //For Creating Hotfolder
                if (!Directory.Exists(hotfolder + "\\DigiImages\\"))
                {
                    Directory.CreateDirectory(hotfolder + "\\DigiImages\\");
                    File.Copy(targetDirectory + "\\Locked.png", hotfolder + "\\DigiImages\\Locked.png");

                    hotfolder = hotfolder + "\\DigiImages\\";
                    Directory.CreateDirectory(hotfolder + "ManualDownload");
                    Directory.CreateDirectory(hotfolder + "ManualDownloadBackup");
                    Directory.CreateDirectory(hotfolder + "BG");
                    Directory.CreateDirectory(hotfolder + "BG\\" + "Thumbnails");
                    Directory.CreateDirectory(hotfolder + "BG\\" + "8x10");
                    Directory.CreateDirectory(hotfolder + "BG\\" + "6x8");
                    Directory.CreateDirectory(hotfolder + "BG\\" + "5x7");
                    Directory.CreateDirectory(hotfolder + "BG\\" + "4x6");
                    Directory.CreateDirectory(hotfolder + "BG\\" + "3x3");

                    Directory.CreateDirectory(hotfolder + "Camera");

                    Directory.CreateDirectory(hotfolder + "Croped");

                    Directory.CreateDirectory(hotfolder + "Croped\\" + "sixbyeight");

                    Directory.CreateDirectory(hotfolder + "Croped\\" + "fourbysix");

                    Directory.CreateDirectory(hotfolder + "Croped\\" + "eightbyten");


                    Directory.CreateDirectory(hotfolder + "Archived");
                    Directory.CreateDirectory(hotfolder + "EditedImages");
                    Directory.CreateDirectory(hotfolder + "Download");
                    Directory.CreateDirectory(hotfolder + "Frames");
                    Directory.CreateDirectory(hotfolder + "StockShot");
                    Directory.CreateDirectory(hotfolder + "StockShot\\" + "Thumbnails");
                    Directory.CreateDirectory(hotfolder + "Frames\\" + "Thumbnails");

                    Directory.CreateDirectory(hotfolder + "Graphics");
                    //Directory.CreateDirectory(hotfolder + "GreenImage");
                    Directory.CreateDirectory(hotfolder + "PrintImages");
                    Directory.CreateDirectory(hotfolder + "Thumbnails");
                    File.Copy(targetDirectory + "\\Locked.png", hotfolder + "Thumbnails\\Locked.png");
                    Directory.CreateDirectory(hotfolder + "Thumbnails\\" + "Temp");

                    Directory.CreateDirectory(hotfolder + "Thumbnails_Big");
                    Directory.CreateDirectory(hotfolder + "PendingItems");
                    //Folders required for Videos.
                    Directory.CreateDirectory(hotfolder + "Audio");
                    Directory.CreateDirectory(hotfolder + "VideoBackGround");
                    Directory.CreateDirectory(hotfolder + "VideoTemplate");
                    Directory.CreateDirectory(hotfolder + "Videos");
                    Directory.CreateDirectory(hotfolder + "ProcessedVideos");

                    Directory.CreateDirectory(hotfolder + "ItemTemplate");
                    Directory.CreateDirectory(hotfolder + "ItemTemplate\\Calendar");
                    Directory.CreateDirectory(hotfolder + "ItemTemplate\\Calendar\\Blue");
                    Directory.CreateDirectory(hotfolder + "ItemTemplate\\Calendar\\Brown");
                    Directory.CreateDirectory(hotfolder + "ItemTemplate\\Calendar\\Green");
                    Directory.CreateDirectory(hotfolder + "ItemTemplate\\Calendar\\Red");
                    Directory.CreateDirectory(hotfolder + "MobileTags");
                    Copy(Path.Combine(targetDirectory, "ItemTemplate", "Calendar", "Blue"), hotfolder + "ItemTemplate\\Calendar\\Blue");
                    Copy(Path.Combine(targetDirectory, "ItemTemplate", "Calendar", "Brown"), hotfolder + "ItemTemplate\\Calendar\\Brown");
                    Copy(Path.Combine(targetDirectory, "ItemTemplate", "Calendar", "Green"), hotfolder + "ItemTemplate\\Calendar\\Green");
                    Copy(Path.Combine(targetDirectory, "ItemTemplate", "Calendar", "Red"), hotfolder + "ItemTemplate\\Calendar\\Red");

                    Directory.CreateDirectory(hotfolder + "Profiles");
                    Directory.CreateDirectory(hotfolder + "Profiles\\DefaultProfiles");
                    Copy(Path.Combine(targetDirectory, "DefaultProfiles"), hotfolder + "Profiles\\DefaultProfiles");

                    Directory.CreateDirectory(hotfolder + "CGSettings");
                    Directory.CreateDirectory(hotfolder + "Download\\CorruptVideos");
                    Directory.CreateDirectory(hotfolder + "ReceiptLogo");
                }
                else if (!Directory.Exists(hotfolder + "\\DigiImages\\Archived\\"))
                {
                    Directory.CreateDirectory(hotfolder + "\\DigiImages\\Archived");
                }

                if (Directory.Exists(hotfolder + "\\DigiImages\\") && !Directory.Exists(hotfolder + "\\DigiImages\\EditedImages\\"))
                {
                    Directory.CreateDirectory(hotfolder + "\\DigiImages\\EditedImages");
                }
                if (Directory.Exists(hotfolder + "\\DigiImages\\") && !Directory.Exists(hotfolder + "\\DigiImages\\OrderReceipt\\"))
                {
                    Directory.CreateDirectory(hotfolder + "\\DigiImages\\OrderReceipt");
                }
                if (Directory.Exists(hotfolder + "\\DigiImages\\") && !Directory.Exists(hotfolder + "\\DigiImages\\StockShot\\"))
                {
                    Directory.CreateDirectory(hotfolder + "\\DigiImages\\StockShot");
                }
                if (Directory.Exists(hotfolder + "\\DigiImages\\") && !Directory.Exists(hotfolder + "\\DigiImages\\StockShot\\Thumbnails\\"))
                {
                    Directory.CreateDirectory(hotfolder + "\\DigiImages\\StockShot\\Thumbnails");
                }
                if (Directory.Exists(hotfolder + "\\DigiImages\\") && !Directory.Exists(hotfolder + "\\DigiImages\\ReceiptLogo\\"))
                {
                    Directory.CreateDirectory(hotfolder + "\\DigiImages\\ReceiptLogo");
                }
                if (Directory.Exists(hotfolder + "\\DigiImages\\") && !Directory.Exists(hotfolder + "ItemTemplate"))
                {
                    hotfolder = hotfolder + "\\DigiImages\\";
                    Directory.CreateDirectory(hotfolder + "ItemTemplate");
                    Directory.CreateDirectory(hotfolder + "ItemTemplate\\Calendar");
                    Directory.CreateDirectory(hotfolder + "ItemTemplate\\Calendar\\Blue");
                    Directory.CreateDirectory(hotfolder + "ItemTemplate\\Calendar\\Brown");
                    Directory.CreateDirectory(hotfolder + "ItemTemplate\\Calendar\\Green");
                    Directory.CreateDirectory(hotfolder + "ItemTemplate\\Calendar\\Red");
                    Copy(Path.Combine(targetDirectory, "ItemTemplate", "Calendar", "Blue"), hotfolder + "ItemTemplate\\Calendar\\Blue");
                    Copy(Path.Combine(targetDirectory, "ItemTemplate", "Calendar", "Brown"), hotfolder + "ItemTemplate\\Calendar\\Brown");
                    Copy(Path.Combine(targetDirectory, "ItemTemplate", "Calendar", "Green"), hotfolder + "ItemTemplate\\Calendar\\Green");
                    Copy(Path.Combine(targetDirectory, "ItemTemplate", "Calendar", "Red"), hotfolder + "ItemTemplate\\Calendar\\Red");
                }


                if (Directory.Exists(hotfolder + "\\DigiImages\\") && !Directory.Exists(hotfolder + "\\DigiImages\\MobileTags\\"))
                {
                    Directory.CreateDirectory(hotfolder + "\\DigiImages\\MobileTags");
                }
                //if (Directory.Exists(hotfolder) && !Directory.Exists(hotfolder + "Profiles"))
                //{
                //    Directory.CreateDirectory(hotfolder + "Profiles");
                //}
                //if (Directory.Exists(hotfolder) && !Directory.Exists(hotfolder + "Profiles\\DefaultProfiles"))
                //{
                //    //hotfolder = hotfolder + "\\DigiImages\\";
                //    Directory.CreateDirectory(hotfolder + "Profiles\\DefaultProfiles");
                //    Copy(Path.Combine(targetDirectory, "DefaultProfiles"), hotfolder + "Profiles\\DefaultProfiles");
                //}
                //if (Directory.Exists(hotfolder) && !Directory.Exists(hotfolder + "CGSettings"))
                //{
                //    Directory.CreateDirectory(hotfolder + "CGSettings");
                //}
                #endregion

                #region VersionSettings

                var CurrentVersion = Assembly.GetExecutingAssembly().GetName().Version;
                string SystemName = System.Environment.MachineName;
                using (SqlConnection cn = new SqlConnection(conn))
                {
                    SqlCommand cmd = new SqlCommand("InsertVersion", cn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@version", CurrentVersion.ToString());
                    cmd.Parameters.AddWithValue("@versiondate", DateTime.Now);
                    cmd.Parameters.AddWithValue("@machinename", SystemName);
                    cn.Open();
                    cmd.ExecuteNonQuery();
                    cn.Close();
                }

                #endregion
                RegisterSystem();
                bool s = UpdateRegistry("InstallVersion", CurrentVersion.ToString());                
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + "\n" + ex.StackTrace);
            }
        }

        public override void Commit(IDictionary savedState)
        {
            base.Commit(savedState);
            //System.Diagnostics.Process.Start(System.IO.Path.GetDirectoryName(Context.Parameters["targetdir"] + "\\DigiConfigUtility.exe"));
        }

        private bool UpdateRegistry(string regKey, string regVal)
        {
            ModifyRegistry mr = new ModifyRegistry();
            bool success = mr.Write(regKey, regVal);
            return success;
        }

       private void Copy(string sourceDir, string targetDir)
        {
            try
            {
                if (!Directory.Exists(targetDir))
                    Directory.CreateDirectory(targetDir);

                foreach (var file in Directory.GetFiles(sourceDir))
                    File.Copy(file, Path.Combine(targetDir, Path.GetFileName(file)));

                foreach (var directory in Directory.GetDirectories(sourceDir))
                    Copy(directory, Path.Combine(targetDir, Path.GetFileName(directory)));
            }
            catch
            { }
        }
        private void RegisterSystem()
        {

            /// string connnectionName = ConfigurationManager.ConnectionStrings["DigiConnectionString"].ConnectionString;

            int Count = 0;
            object SystemName = "";
            string hostName = Dns.GetHostName(); // Retrive the Name of HOST
            string myIP = Dns.GetHostByName(hostName).AddressList[0].ToString();//ip              
            ManagementObjectSearcher searcher =
                new ManagementObjectSearcher("root\\CIMV2",
                "SELECT Name FROM Win32_ComputerSystem");

            foreach (ManagementObject queryObj in searcher.Get())
            {
                SystemName = queryObj["Name"];
            }
            ManagementObjectSearcher search = new ManagementObjectSearcher("SELECT * FROM Win32_NetworkAdapterConfiguration where IPEnabled=true");
            IEnumerable<ManagementObject> objects = search.Get().Cast<ManagementObject>();
            string mac = (from o in objects orderby o["IPConnectionMetric"] select o["MACAddress"].ToString()).FirstOrDefault();
            string SyncCode = CommonUtility.GetUniqueSynccode(Convert.ToInt32(ApplicationObjectEnum.PosDetail).ToString().PadLeft(2, '0'), LoginUser.countrycode, LoginUser.Storecode, "00");
            ImixPOSDetail imixposdetail = new ImixPOSDetail();
            imixposdetail.SystemName = SystemName.ToString();
            imixposdetail.IPAddress = myIP;
            imixposdetail.MacAddress = mac;
            imixposdetail.SubStoreID = 0;
            imixposdetail.IsActive = true;
            imixposdetail.CreatedBy = "webusers";
            imixposdetail.ImixPOSDetailID = 0;
            imixposdetail.IsStart = false;
            imixposdetail.SyncCode = SyncCode;
            InsertImixPosDetail(imixposdetail);
            //ServicePosInfoBusiness svcPosInfoBusiness = new ServicePosInfoBusiness();
            //Count = svcPosInfoBusiness.InsertImixPosBusiness(imixposdetail,conn);

        }

        private void InsertImixPosDetail(ImixPOSDetail imixposdetail)
        {
            using (SqlConnection cn = new SqlConnection(conn))
            {
                SqlCommand cmd = new SqlCommand("USP_INSERTUPDATEIMIXPOSDETAIL", cn);

                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@iMixPOSDetailID", imixposdetail.ImixPOSDetailID);
                cmd.Parameters.AddWithValue("@SystemName", imixposdetail.SystemName);
                cmd.Parameters.AddWithValue("@IPAddress", imixposdetail.IPAddress);
                cmd.Parameters.AddWithValue("@MacAddress", imixposdetail.MacAddress);
                cmd.Parameters.AddWithValue("@SubStoreID", imixposdetail.SubStoreID);
                cmd.Parameters.AddWithValue("@IsActive", imixposdetail.IsActive);
                cmd.Parameters.AddWithValue("@CreatedBy", imixposdetail.CreatedBy);
                cmd.Parameters.AddWithValue("@IsStart", imixposdetail.IsStart);
                cmd.Parameters.AddWithValue("@SyncCode", imixposdetail.SyncCode);
                cn.Open();
                cmd.ExecuteNonQuery();
                cn.Close();
            }
        }

    }
}
