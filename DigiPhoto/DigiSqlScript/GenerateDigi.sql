USE [master]
GO
IF NOT EXISTS (SELECT name FROM sys.databases WHERE name = N'Digiphoto')
BEGIN
CREATE DATABASE [Digiphoto] 
end
Go
ALTER DATABASE [Digiphoto] SET COMPATIBILITY_LEVEL = 100
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [Digiphoto].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [Digiphoto] SET ANSI_NULL_DEFAULT OFF
GO
ALTER DATABASE [Digiphoto] SET ANSI_NULLS OFF
GO
ALTER DATABASE [Digiphoto] SET ANSI_PADDING OFF
GO
ALTER DATABASE [Digiphoto] SET ANSI_WARNINGS OFF
GO
ALTER DATABASE [Digiphoto] SET ARITHABORT OFF
GO
ALTER DATABASE [Digiphoto] SET AUTO_CLOSE OFF
GO
ALTER DATABASE [Digiphoto] SET AUTO_CREATE_STATISTICS ON
GO
ALTER DATABASE [Digiphoto] SET AUTO_SHRINK OFF
GO
ALTER DATABASE [Digiphoto] SET AUTO_UPDATE_STATISTICS ON
GO
ALTER DATABASE [Digiphoto] SET CURSOR_CLOSE_ON_COMMIT OFF
GO
ALTER DATABASE [Digiphoto] SET CURSOR_DEFAULT  GLOBAL
GO
ALTER DATABASE [Digiphoto] SET CONCAT_NULL_YIELDS_NULL OFF
GO
ALTER DATABASE [Digiphoto] SET NUMERIC_ROUNDABORT OFF
GO
ALTER DATABASE [Digiphoto] SET QUOTED_IDENTIFIER OFF
GO
ALTER DATABASE [Digiphoto] SET RECURSIVE_TRIGGERS OFF
GO
ALTER DATABASE [Digiphoto] SET  DISABLE_BROKER
GO
ALTER DATABASE [Digiphoto] SET AUTO_UPDATE_STATISTICS_ASYNC OFF
GO
ALTER DATABASE [Digiphoto] SET DATE_CORRELATION_OPTIMIZATION OFF
GO
ALTER DATABASE [Digiphoto] SET TRUSTWORTHY OFF
GO
ALTER DATABASE [Digiphoto] SET ALLOW_SNAPSHOT_ISOLATION OFF
GO
ALTER DATABASE [Digiphoto] SET PARAMETERIZATION SIMPLE
GO
ALTER DATABASE [Digiphoto] SET READ_COMMITTED_SNAPSHOT OFF
GO
ALTER DATABASE [Digiphoto] SET HONOR_BROKER_PRIORITY OFF
GO
ALTER DATABASE [Digiphoto] SET  READ_WRITE
GO
ALTER DATABASE [Digiphoto] SET RECOVERY SIMPLE
GO
ALTER DATABASE [Digiphoto] SET  MULTI_USER
GO
ALTER DATABASE [Digiphoto] SET PAGE_VERIFY CHECKSUM
GO
ALTER DATABASE [Digiphoto] SET DB_CHAINING OFF
GO
EXEC sys.sp_db_vardecimal_storage_format N'Digiphoto', N'ON'
GO
USE [Digiphoto]
GO
/****** Object:  User [webusers]    Script Date: 01/05/2013 14:44:39 ******/
CREATE USER [webusers] WITHOUT LOGIN WITH DEFAULT_SCHEMA=[dbo]
GO

/****** Object:  Table [dbo].[DG_Currency]    Script Date: 01/05/2013 14:44:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DG_Currency](
	[DG_Currency_pkey] [int] IDENTITY(1,1) NOT NULL,
	[DG_Currency_Name] [varchar](50) NOT NULL,
	[DG_Currency_Rate] [float] NOT NULL,
	[DG_Currency_Symbol] [varchar](50) NULL,
	[DG_Currency_UpdatedDate] [datetime] NULL,
	[DG_Currency_ModifiedBy] [int] NULL,
	[DG_Currency_Default] [bit] NULL,
	[DG_Currency_Icon] [varchar](max) NULL,
 CONSTRAINT [PK_DG_Currency] PRIMARY KEY CLUSTERED 
(
	[DG_Currency_pkey] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[DG_Configuration]    Script Date: 01/05/2013 14:44:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DG_Configuration](
	[DG_Config_pkey] [int] IDENTITY(1,1) NOT NULL,
	[DG_Hot_Folder_Path] [varchar](500) NOT NULL,
	[DG_Frame_Path] [varchar](500) NOT NULL,
	[DG_BG_Path] [varchar](500) NOT NULL,
	[DG_Mod_Password] [nvarchar](20) NOT NULL,
	[DG_NoOfPhotos] [int] NOT NULL,
	[DG_Graphics_Path] [varchar](500) NULL,
	[DG_Watermark] [bit] NULL,
	[DG_SemiOrder] [bit] NULL,
	[DG_HighResolution] [bit] NULL,
	[DG_AllowDiscount] [bit] NULL,
	[DG_EnableDiscountOnTotal] [bit] NULL,
	[WiFiStartingNumber] [numeric](18, 0) NULL,
	[FolderStartingNumber] [numeric](18, 0) NULL,
	[IsAutoLock] [bit] NULL,
	[DG_SemiOrderMain] [bit] NULL,
	[PosOnOff] [bit] NULL,
	[DG_ReceiptPrinter] [varchar](200) NULL,
	[DG_IsAutoRotate] [bit] NULL,
 CONSTRAINT [PK_DG_Configuration] PRIMARY KEY CLUSTERED 
(
	[DG_Config_pkey] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[DG_ColorCodes]    Script Date: 01/05/2013 14:44:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DG_ColorCodes](
	[DG_ID] [int] IDENTITY(1,1) NOT NULL,
	[DG_ColorCode] [varchar](50) NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[DG_CameraDetails]    Script Date: 01/05/2013 14:44:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DG_CameraDetails](
	[DG_Camera_pkey] [int] IDENTITY(1,1) NOT NULL,
	[DG_Camera_Name] [varchar](50) NOT NULL,
	[DG_Camera_Make] [varchar](50) NOT NULL,
	[DG_Camera_Model] [varchar](50) NOT NULL,
	[DG_AssignTo] [int] NULL,
	[DG_Camera_Start_Series] [varchar](50) NOT NULL,
	[DG_Updatedby] [int] NOT NULL,
	[DG_UpdatedDate] [datetime] NOT NULL,
	[DG_Camera_IsDeleted] [bit] NULL,
	[DG_Camera_ID] [int] NULL,
 CONSTRAINT [PK_DG_CameraDetails] PRIMARY KEY CLUSTERED 
(
	[DG_Camera_pkey] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[DG_Borders]    Script Date: 01/05/2013 14:44:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DG_Borders](
	[DG_Borders_pkey] [int] IDENTITY(1,1) NOT NULL,
	[DG_Border] [varchar](100) NOT NULL,
	[DG_ProductTypeID] [int] NOT NULL,
	[DG_IsActive] [bit] NOT NULL,
 CONSTRAINT [PK_DG_Borders] PRIMARY KEY CLUSTERED 
(
	[DG_Borders_pkey] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[DG_Bill_Format]    Script Date: 01/05/2013 14:44:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DG_Bill_Format](
	[DG_Bill_Format_pkey] [int] IDENTITY(1,1) NOT NULL,
	[DG_Bill_Type] [int] NULL,
	[DG_Refund_Slogan] [varchar](150) NULL,
 CONSTRAINT [PK_DG_Bill_Format] PRIMARY KEY CLUSTERED 
(
	[DG_Bill_Format_pkey] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'0 for Cash' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DG_Bill_Format', @level2type=N'COLUMN',@level2name=N'DG_Bill_Type'
GO
/****** Object:  Table [dbo].[DG_BackGround]    Script Date: 01/05/2013 14:44:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DG_BackGround](
	[DG_Background_pkey] [int] IDENTITY(1,1) NOT NULL,
	[DG_Product_Id] [int] NOT NULL,
	[DG_BackGround_Image_Name] [nvarchar](max) NULL,
	[DG_BackGround_Image_Display_Name] [nvarchar](max) NULL,
	[DG_BackGround_Group_Id] [int] NULL,
 CONSTRAINT [PK_DG_BackGround] PRIMARY KEY CLUSTERED 
(
	[DG_Background_pkey] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[DG_AssociatedPrinters]    Script Date: 01/05/2013 14:44:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DG_AssociatedPrinters](
	[DG_AssociatedPrinters_Pkey] [int] IDENTITY(1,1) NOT NULL,
	[DG_AssociatedPrinters_Name] [nvarchar](max) NOT NULL,
	[DG_AssociatedPrinters_ProductType_ID] [int] NOT NULL,
	[DG_AssociatedPrinters_IsActive] [bit] NOT NULL,
	[DG_AssociatedPrinters_PaperSize] [varchar](100) NULL,
 CONSTRAINT [PK_DG_AssociatedPrinters] PRIMARY KEY CLUSTERED 
(
	[DG_AssociatedPrinters_Pkey] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[DG_ApplicationSettings]    Script Date: 01/05/2013 14:44:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DG_ApplicationSettings](
	[DG_ApplicationID] [uniqueidentifier] NOT NULL,
	[DG_LastSync] [datetime] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[DG_Albums_Photos]    Script Date: 01/05/2013 14:44:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DG_Albums_Photos](
	[DG_Albums_Photos_pkey] [int] NOT NULL,
	[DG_Albums_pkey] [int] NULL,
	[DG_Photos_pkey] [int] NULL,
	[DG_Albums_Photos_CreatedOn] [datetime] NULL,
	[DG_Albums_Photos_CreatedBy] [int] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[DG_Albums]    Script Date: 01/05/2013 14:44:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DG_Albums](
	[DG_Albums_pkey] [int] NOT NULL,
	[DG_Albums_Name] [int] NULL,
	[DG_Albums_CreatedOn] [datetime] NULL,
	[DG_Albums_CreatedBy] [int] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[DG_Activity]    Script Date: 01/05/2013 14:44:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DG_Activity](
	[DG_Acitivity_Action_Pkey] [int] IDENTITY(1,1) NOT NULL,
	[DG_Acitivity_ActionType] [int] NULL,
	[DG_Acitivity_Date] [datetime] NULL,
	[DG_Acitivity_By] [int] NOT NULL,
	[DG_Acitivity_Descrption] [varchar](max) NULL,
	[DG_Reference_ID] [int] NULL,
 CONSTRAINT [PK_DG_Activity] PRIMARY KEY CLUSTERED 
(
	[DG_Acitivity_Action_Pkey] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[DG_Actions_Type]    Script Date: 01/05/2013 14:44:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DG_Actions_Type](
	[DG_Actions_pkey] [int] IDENTITY(1,1) NOT NULL,
	[DG_Actions_Name] [varchar](150) NULL,
 CONSTRAINT [PK_DG_Actions_Type] PRIMARY KEY CLUSTERED 
(
	[DG_Actions_pkey] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  UserDefinedFunction [dbo].[fnSplit]    Script Date: 01/05/2013 14:44:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[fnSplit](
    @sInputList VARCHAR(8000) -- List of delimited items
  , @sDelimiter VARCHAR(8000) = ',' -- delimiter that separates items
) RETURNS @List TABLE (item VARCHAR(8000))

BEGIN
DECLARE @sItem VARCHAR(8000)
WHILE CHARINDEX(@sDelimiter,@sInputList,0) <> 0
 BEGIN
 SELECT
  @sItem=RTRIM(LTRIM(SUBSTRING(@sInputList,1,CHARINDEX(@sDelimiter,@sInputList,0)-1))),
  @sInputList=RTRIM(LTRIM(SUBSTRING(@sInputList,CHARINDEX(@sDelimiter,@sInputList,0)+LEN(@sDelimiter),LEN(@sInputList))))
 
 IF LEN(@sItem) > 0
  INSERT INTO @List SELECT @sItem
 END

IF LEN(@sInputList) > 0
 INSERT INTO @List SELECT @sInputList -- Put the last item in
RETURN
END
GO
/****** Object:  Table [dbo].[DG_Users]    Script Date: 01/05/2013 14:44:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DG_Users](
	[DG_User_pkey] [int] IDENTITY(1,1) NOT NULL,
	[DG_User_Name] [varchar](50) NOT NULL,
	[DG_User_First_Name] [varchar](50) NOT NULL,
	[DG_User_Last_Name] [varchar](50) NULL,
	[DG_User_Password] [nvarchar](50) NOT NULL,
	[DG_User_Roles_Id] [int] NOT NULL,
	[DG_Location_ID] [int] NOT NULL,
	[DG_User_Status] [bit] NULL,
	[DG_User_PhoneNo] [varchar](20) NULL,
	[DG_User_Email] [varchar](50) NULL,
 CONSTRAINT [PK_DG_Users] PRIMARY KEY CLUSTERED 
(
	[DG_User_pkey] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[DG_User_Roles]    Script Date: 01/05/2013 14:44:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DG_User_Roles](
	[DG_User_Roles_pkey] [int] IDENTITY(1,1) NOT NULL,
	[DG_User_Role] [varchar](50) NOT NULL,
 CONSTRAINT [PK_DG_User_Roles] PRIMARY KEY CLUSTERED 
(
	[DG_User_Roles_pkey] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[DG_SubProducts]    Script Date: 01/05/2013 14:44:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DG_SubProducts](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[DG_Orders_ProductType_pkey] [int] NOT NULL,
	[Child_DG_Orders_ProductType_pkey] [int] NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[DG_Store]    Script Date: 01/05/2013 14:44:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DG_Store](
	[DG_Store_pkey] [int] IDENTITY(1,1) NOT NULL,
	[DG_Store_Name] [varchar](150) NOT NULL,
 CONSTRAINT [PK_DG_Store] PRIMARY KEY CLUSTERED 
(
	[DG_Store_pkey] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[DG_Services]    Script Date: 01/05/2013 14:44:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DG_Services](
	[DG_Service_Id] [int] IDENTITY(1,1) NOT NULL,
	[DG_Sevice_Name] [varchar](50) NOT NULL,
	[DG_Service_Display_Name] [varchar](100) NOT NULL,
	[DG_Service_Path] [nvarchar](500) NULL,
	[IsInterface] [bit] NULL,
 CONSTRAINT [PK_DG_Services] PRIMARY KEY CLUSTERED 
(
	[DG_Service_Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[DG_SemiOrder_Settings]    Script Date: 01/05/2013 14:44:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DG_SemiOrder_Settings](
	[DG_SemiOrder_Settings_Pkey] [int] IDENTITY(1,1) NOT NULL,
	[DG_SemiOrder_Settings_AutoBright] [bit] NULL,
	[DG_SemiOrder_Settings_AutoBright_Value] [float] NULL,
	[DG_SemiOrder_Settings_AutoContrast] [bit] NULL,
	[DG_SemiOrder_Settings_AutoContrast_Value] [float] NULL,
	[DG_SemiOrder_Settings_ImageFrame] [varchar](350) NULL,
	[DG_SemiOrder_Settings_IsImageFrame] [bit] NULL,
	[DG_SemiOrder_ProductTypeId] [int] NULL,
	[DG_SemiOrder_Settings_ImageFrame_Vertical] [varchar](350) NULL,
	[DG_SemiOrder_Environment] [bit] NULL,
	[DG_SemiOrder_BG] [varchar](350) NULL,
	[DG_SemiOrder_Settings_IsImageBG] [bit] NULL,
 CONSTRAINT [PK_DG_SemiOrder_Settings] PRIMARY KEY CLUSTERED 
(
	[DG_SemiOrder_Settings_Pkey] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'0 for GreenScreen 1 for Without Green Screen' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DG_SemiOrder_Settings', @level2type=N'COLUMN',@level2name=N'DG_SemiOrder_Environment'
GO
/****** Object:  Table [dbo].[DG_RefundDetails]    Script Date: 01/05/2013 14:44:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DG_RefundDetails](
	[DG_RefundDetail_ID] [int] IDENTITY(1,1) NOT NULL,
	[DG_LineItemId] [int] NULL,
	[RefundPhotoId] [varchar](50) NULL,
	[DG_RefundMaster_ID] [int] NULL,
	[Refunded_Amount] [decimal](18, 0) NULL,
 CONSTRAINT [PK_DG_RefundDetails] PRIMARY KEY CLUSTERED 
(
	[DG_RefundDetail_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[DG_Refund]    Script Date: 01/05/2013 14:44:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DG_Refund](
	[DG_RefundId] [int] IDENTITY(1,1) NOT NULL,
	[DG_OrderId] [int] NULL,
	[RefundAmount] [money] NULL,
	[RefundDate] [datetime] NULL,
	[UserId] [int] NULL,
	[Refund_Mode] [int] NULL,
 CONSTRAINT [PK_DG_Refund] PRIMARY KEY CLUSTERED 
(
	[DG_RefundId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'0 for refund and 1 for cancel' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DG_Refund', @level2type=N'COLUMN',@level2name=N'Refund_Mode'
GO
/****** Object:  Table [dbo].[DG_Product_Pricing]    Script Date: 01/05/2013 14:44:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DG_Product_Pricing](
	[DG_Product_Pricing_Pkey] [int] IDENTITY(1,1) NOT NULL,
	[DG_Product_Pricing_ProductType] [int] NULL,
	[DG_Product_Pricing_ProductPrice] [float] NULL,
	[DG_Product_Pricing_Currency_ID] [int] NULL,
	[DG_Product_Pricing_UpdateDate] [datetime] NULL,
	[DG_Product_Pricing_CreatedBy] [int] NULL,
	[DG_Product_Pricing_StoreId] [int] NULL,
	[DG_Product_Pricing_IsAvaliable] [bit] NULL,
 CONSTRAINT [PK_DG_Product_Pricing] PRIMARY KEY CLUSTERED 
(
	[DG_Product_Pricing_Pkey] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[DG_PrintLog]    Script Date: 01/05/2013 14:44:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DG_PrintLog](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[PhotoId] [int] NULL,
	[PrintTime] [datetime] NULL,
	[ProductTypeId] [int] NULL,
	[UserID] [int] NULL,
 CONSTRAINT [PK_DG_PrintLog] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[DG_Printers]    Script Date: 01/05/2013 14:44:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DG_Printers](
	[DG_Printers_Pkey] [int] IDENTITY(1,1) NOT NULL,
	[DG_Printers_NickName] [varchar](250) NULL,
	[DG_Printers_DefaultName] [varchar](50) NULL,
	[DG_Printers_Address] [varchar](250) NULL,
 CONSTRAINT [PK_DG_Printers] PRIMARY KEY CLUSTERED 
(
	[DG_Printers_Pkey] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[DG_PrinterQueue]    Script Date: 01/05/2013 14:44:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DG_PrinterQueue](
	[DG_PrinterQueue_Pkey] [int] IDENTITY(1,1) NOT NULL,
	[DG_PrinterQueue_ProductID] [int] NULL,
	[DG_PrinterQueue_Image_Pkey] [varchar](max) NULL,
	[DG_Associated_PrinterId] [int] NULL,
	[DG_Order_Details_Pkey] [int] NULL,
	[DG_SentToPrinter] [bit] NULL,
	[is_Active] [bit] NULL,
	[QueueIndex] [int] NULL,
	[DG_IsSpecPrint] [bit] NULL,
 CONSTRAINT [PK_DG_PrinterQueue] PRIMARY KEY CLUSTERED 
(
	[DG_PrinterQueue_Pkey] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[DG_Preview_Counter]    Script Date: 01/05/2013 14:44:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DG_Preview_Counter](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[PhotoId] [int] NOT NULL,
	[PreviewDate] [datetime] NULL,
 CONSTRAINT [PK_DG_Preview_Counter] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[DG_Photos_Changes]    Script Date: 01/05/2013 14:44:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DG_Photos_Changes](
	[DG_Photos_Changes_pkey] [int] IDENTITY(1,1) NOT NULL,
	[DG_Photos_ID] [int] NULL,
	[DG_Photos_Changes_TypeID] [int] NULL,
	[DG_Photos_Changes_Value] [varchar](50) NULL,
	[DG_Photos_Changes_CreatedON] [datetime] NULL,
 CONSTRAINT [PK_DG_Photos_Changes] PRIMARY KEY CLUSTERED 
(
	[DG_Photos_Changes_pkey] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[DG_Photos]    Script Date: 01/05/2013 14:44:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DG_Photos](
	[DG_Photos_pkey] [int] IDENTITY(1,1) NOT NULL,
	[DG_Photos_FileName] [varchar](150) NOT NULL,
	[DG_Photos_CreatedOn] [datetime] NOT NULL,
	[DG_Photos_RFID] [varchar](150) NULL,
	[DG_Photos_UserID] [int] NULL,
	[DG_Photos_Background] [varchar](150) NULL,
	[DG_Photos_Frame] [varchar](150) NULL,
	[DG_Photos_DateTime] [datetime] NULL,
	[DG_Photos_Layering] [xml] NULL,
	[DG_Photos_Effects] [xml] NULL,
	[DG_Photos_IsCroped] [bit] NULL,
	[DG_Photos_IsRedEye] [bit] NULL,
	[DG_Photos_IsGreen] [bit] NULL,
	[DG_Photos_MetaData] [xml] NULL,
	[DG_Photos_Sizes] [varchar](max) NULL,
	[DG_Photos_Archive] [bit] NULL,
	[DG_Location_Id] [int] NULL,
 CONSTRAINT [PK_DG_Photos] PRIMARY KEY CLUSTERED 
(
	[DG_Photos_pkey] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'the value is 0#0#0 first zero is for 4 by 6 then 6 by 8 then 8 by 10' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DG_Photos', @level2type=N'COLUMN',@level2name=N'DG_Photos_Sizes'
GO
/****** Object:  Table [dbo].[DG_PhotoNumberConfiguration]    Script Date: 01/05/2013 14:44:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DG_PhotoNumberConfiguration](
	[DG_PhotoSequence_pkey] [int] IDENTITY(1,1) NOT NULL,
	[DG_WifiName] [varchar](50) NULL,
	[DG_StartingNumber] [numeric](18, 0) NULL,
 CONSTRAINT [PK_DG_PhotoNumberConfiguration] PRIMARY KEY CLUSTERED 
(
	[DG_PhotoSequence_pkey] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[DG_Photo_Effects]    Script Date: 01/05/2013 14:44:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DG_Photo_Effects](
	[DG_Photo_Effects_pkey] [int] IDENTITY(1,1) NOT NULL,
	[DG_Photo_Effects_Name] [varchar](50) NULL,
 CONSTRAINT [PK_DG_Photo_Effects] PRIMARY KEY CLUSTERED 
(
	[DG_Photo_Effects_pkey] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[DG_Permissions]    Script Date: 01/05/2013 14:44:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DG_Permissions](
	[DG_Permission_pkey] [int] IDENTITY(1,1) NOT NULL,
	[DG_Permission_Name] [varchar](50) NOT NULL,
 CONSTRAINT [PK_DG_Permissions] PRIMARY KEY CLUSTERED 
(
	[DG_Permission_pkey] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[DG_Permission_Role]    Script Date: 01/05/2013 14:44:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DG_Permission_Role](
	[DG_Permission_Role_pkey] [int] IDENTITY(1,1) NOT NULL,
	[DG_User_Roles_Id] [int] NOT NULL,
	[DG_Permission_Id] [int] NOT NULL,
 CONSTRAINT [PK_DG_Permission_Role] PRIMARY KEY CLUSTERED 
(
	[DG_Permission_Role_pkey] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[DG_PackageDetails]    Script Date: 01/05/2013 14:44:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DG_PackageDetails](
	[DG_Package_Details_Pkey] [int] IDENTITY(1,1) NOT NULL,
	[DG_ProductTypeId] [int] NOT NULL,
	[DG_PackageId] [int] NOT NULL,
	[DG_Product_Quantity] [int] NULL,
 CONSTRAINT [PK_DG_PackageDetails] PRIMARY KEY CLUSTERED 
(
	[DG_Package_Details_Pkey] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[DG_Orders_ProductType]    Script Date: 01/05/2013 14:44:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DG_Orders_ProductType](
	[DG_Orders_ProductType_pkey] [int] IDENTITY(1,1) NOT NULL,
	[DG_Orders_ProductType_Name] [varchar](150) NULL,
	[DG_Orders_ProductType_Desc] [varchar](500) NULL,
	[DG_Orders_ProductType_IsBundled] [bit] NULL,
	[DG_Orders_ProductType_DiscountApplied] [bit] NULL,
	[DG_Orders_ProductType_Image] [varchar](500) NULL,
	[DG_IsPackage] [bit] NOT NULL,
	[DG_MaxQuantity] [int] NOT NULL,
	[DG_Orders_ProductType_Active] [bit] NULL,
	[DG_IsActive] [bit] NULL,
	[DG_IsAccessory] [bit] NULL,
	[DG_IsPrimary] [bit] NULL,
 CONSTRAINT [PK_DG_Orders_ProductType] PRIMARY KEY CLUSTERED 
(
	[DG_Orders_ProductType_pkey] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[DG_Orders_DiscountType]    Script Date: 01/05/2013 14:44:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DG_Orders_DiscountType](
	[DG_Orders_DiscountType_Pkey] [int] IDENTITY(1,1) NOT NULL,
	[DG_Orders_DiscountType_Name] [varchar](250) NULL,
	[DG_Orders_DiscountType_Desc] [varchar](500) NULL,
	[DG_Orders_DiscountType_Active] [bit] NULL,
	[DG_Orders_DiscountType_Secure] [bit] NULL,
	[DG_Orders_DiscountType_ItemLevel] [bit] NULL,
	[DG_Orders_DiscountType_AsPercentage] [bit] NULL,
 CONSTRAINT [PK_DG_Orders_DiscountType] PRIMARY KEY CLUSTERED 
(
	[DG_Orders_DiscountType_Pkey] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[DG_Orders_Details]    Script Date: 01/05/2013 14:44:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DG_Orders_Details](
	[DG_Orders_LineItems_pkey] [int] IDENTITY(1,1) NOT NULL,
	[DG_Orders_ID] [int] NULL,
	[DG_Photos_ID] [varchar](max) NULL,
	[DG_Orders_LineItems_Created] [datetime] NULL,
	[DG_Orders_LineItems_DiscountType] [varchar](max) NULL,
	[DG_Orders_LineItems_DiscountAmount] [money] NULL,
	[DG_Orders_LineItems_Quantity] [int] NULL,
	[DG_Orders_Details_Items_UniPrice] [money] NULL,
	[DG_Orders_Details_Items_TotalCost] [money] NULL,
	[DG_Orders_Details_Items_NetPrice] [money] NULL,
	[DG_Orders_Details_ProductType_pkey] [int] NULL,
	[DG_Orders_Details_LineItem_ParentID] [int] NULL,
	[DG_Orders_Details_LineItem_PrinterReferenceID] [int] NULL,
	[DG_Photos_Burned] [bit] NULL,
 CONSTRAINT [PK_DG_Orders_Details_1] PRIMARY KEY CLUSTERED 
(
	[DG_Orders_LineItems_pkey] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'-1 if no disocunt Applied' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DG_Orders_Details', @level2type=N'COLUMN',@level2name=N'DG_Orders_LineItems_DiscountType'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Payable = No of Photos * Qty * Unit Price' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DG_Orders_Details', @level2type=N'COLUMN',@level2name=N'DG_Orders_Details_Items_TotalCost'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Net Cost = Total Cost - Discount' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DG_Orders_Details', @level2type=N'COLUMN',@level2name=N'DG_Orders_Details_Items_NetPrice'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Type of Product' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DG_Orders_Details', @level2type=N'COLUMN',@level2name=N'DG_Orders_Details_ProductType_pkey'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'LineItem Primary Key if current Lineitem is a part of any package' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DG_Orders_Details', @level2type=N'COLUMN',@level2name=N'DG_Orders_Details_LineItem_ParentID'
GO
/****** Object:  Table [dbo].[DG_Orders]    Script Date: 01/05/2013 14:44:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DG_Orders](
	[DG_Orders_pkey] [int] IDENTITY(1,1) NOT NULL,
	[DG_Orders_Number] [varchar](250) NULL,
	[DG_Orders_Date] [datetime] NULL,
	[DG_Albums_ID] [int] NULL,
	[DG_Order_Mode] [char](1) NULL,
	[DG_Orders_UserID] [int] NULL,
	[DG_Orders_Cost] [money] NULL,
	[DG_Orders_NetCost] [money] NULL,
	[DG_Orders_Currency_ID] [int] NULL,
	[DG_Orders_Currency_Conversion_Rate] [nvarchar](max) NULL,
	[DG_Orders_Total_Discount] [float] NULL,
	[DG_Orders_Total_Discount_Details] [nvarchar](max) NULL,
	[DG_Orders_PaymentMode] [int] NULL,
	[DG_Orders_PaymentDetails] [nvarchar](max) NULL,
	[DG_Orders_Canceled] [bit] NULL,
 CONSTRAINT [PK_DG_Orders] PRIMARY KEY CLUSTERED 
(
	[DG_Orders_pkey] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Actual Cost Of Order (No Discount)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DG_Orders', @level2type=N'COLUMN',@level2name=N'DG_Orders_Cost'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Net Payable Cost' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DG_Orders', @level2type=N'COLUMN',@level2name=N'DG_Orders_NetCost'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'0 for CashCard 1 for Room Charge 2 for Gift Voucher' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DG_Orders', @level2type=N'COLUMN',@level2name=N'DG_Orders_PaymentMode'
GO
/****** Object:  Table [dbo].[DG_Moderate_Photos]    Script Date: 01/05/2013 14:44:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DG_Moderate_Photos](
	[DG_Mod_Photo_pkey] [int] IDENTITY(1,1) NOT NULL,
	[DG_Mod_Photo_ID] [int] NOT NULL,
	[DG_Mod_Date] [datetime] NOT NULL,
	[DG_Mod_User_ID] [int] NOT NULL,
 CONSTRAINT [PK_DG_Moderate_Photos] PRIMARY KEY CLUSTERED 
(
	[DG_Mod_Photo_pkey] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[DG_Location]    Script Date: 01/05/2013 14:44:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DG_Location](
	[DG_Location_pkey] [int] IDENTITY(1,1) NOT NULL,
	[DG_Location_Name] [varchar](50) NOT NULL,
	[DG_Store_ID] [int] NOT NULL,
	[DG_Location_PhoneNumber] [varchar](150) NULL,
 CONSTRAINT [PK_DG_Location] PRIMARY KEY CLUSTERED 
(
	[DG_Location_pkey] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[DG_Graphics]    Script Date: 01/05/2013 14:44:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DG_Graphics](
	[DG_Graphics_pkey] [int] IDENTITY(1,1) NOT NULL,
	[DG_Graphics_Name] [nvarchar](100) NULL,
	[DG_Graphics_Displayname] [nvarchar](100) NULL,
	[DG_Graphics_IsActive] [bit] NULL,
 CONSTRAINT [PK_DG_Graphics] PRIMARY KEY CLUSTERED 
(
	[DG_Graphics_pkey] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblprinters]    Script Date: 01/05/2013 14:44:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblprinters](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Printer1] [varchar](50) NULL,
	[Printer2] [varchar](50) NULL,
 CONSTRAINT [PK_tblprinters] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tblPhotoDetails]    Script Date: 01/05/2013 14:44:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblPhotoDetails](
	[UserName] [bit] NULL,
	[tblId] [int] IDENTITY(1,1) NOT NULL,
 CONSTRAINT [PK_tblPhotoDetails] PRIMARY KEY CLUSTERED 
(
	[tblId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  UserDefinedFunction [dbo].[GetPhotoOrientation]    Script Date: 01/05/2013 14:44:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create Function [dbo].[GetPhotoOrientation]
(
@PhotoXml xml
)
returns int
as
begin
Declare @Orientation int

	if(@PhotoXml is null)
	begin
		set @Orientation = 0;
	end
		set @Orientation = @PhotoXml.value('(/photo/@rotatetransform)[1]', 'varchar(10)') 


return @Orientation
end
GO
/****** Object:  StoredProcedure [dbo].[GenerateBill]    Script Date: 01/05/2013 14:44:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create Procedure [dbo].[GenerateBill]
(
@LineItems xml
)
as
begin

--Set @strSql = '<LineItems><Item Name = ''8*10'' Qty = ''3'' Price =''10''/><Item Name = ''6*8'' Qty = ''3'' Price =''24''/></LineItems>'

SELECT
Tab.Col.value('@Name','varchar(20)') AS Name,
Tab.Col.value('@Qty','varchar(20)') AS Qty,
Tab.Col.value('@Price','varchar(20)') AS Price
FROM   @LineItems.nodes('/LineItems/Item') Tab(Col)

end
GO
/****** Object:  UserDefinedFunction [dbo].[TestFun]    Script Date: 01/05/2013 14:44:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
-- select * from dbo.TestFun(46,'1/Dec/2012',getdate())
CREATE FUNCTION [dbo].[TestFun]
(
	@UserID int,
@DateFrom Datetime,
@DateTo Datetime
)
RETURNS 
@List TABLE 
(
	Photostaken int,
	Preview int,
	NoOfSales int,
	UserName varchar(5000)
	
)
AS
BEGIN
	
	
declare @printtaken int 
DECLARE @listStr VARCHAR(MAX)

select @listStr = ltrim(substring(
(select ',' + DG_Photos_ID from DG_Orders_Details where DG_Photos_ID !=''
for xml path('')),2,10000))
SELECT    @printtaken= count(fnSplit_1.item)
FROM         dbo.fnSplit(@listStr, ',') AS fnSplit_1 INNER JOIN
                      DG_Photos ON fnSplit_1.item = DG_Photos.DG_Photos_pkey
WHERE     (DG_Photos.DG_Photos_UserID = @UserID) and DG_Photos.DG_Photos_CreatedOn between @DateFrom and @DateTo
insert into @List
Select 
(SELECT COUNT(*)  from DG_Photos where DG_Photos_UserID=@UserID and DG_Photos_CreatedOn between @DateFrom and @DateTo)as 'Photos taken'
,
(SELECT   COUNT(distinct PhotoId)  
FROM         DG_Preview_Counter INNER JOIN
                      DG_Photos ON DG_Preview_Counter.PhotoId = DG_Photos.DG_Photos_pkey
WHERE     (DG_Photos.DG_Photos_UserID = @UserID) and DG_Photos.DG_Photos_CreatedOn between @DateFrom and @DateTo
)as 'Preview',@printtaken as 'No of Sales',(Select (dg_user_first_Name + ' ' +dg_user_last_Name)  from dg_users where dg_user_pkey=@UserID)  as 'PhotoGrapher Name'

	
	RETURN 
END
GO
/****** Object:  View [dbo].[vw_GetPrinterDetails]    Script Date: 01/05/2013 14:44:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vw_GetPrinterDetails]
AS
SELECT     dbo.DG_AssociatedPrinters.DG_AssociatedPrinters_Pkey, dbo.DG_AssociatedPrinters.DG_AssociatedPrinters_Name, 
                      dbo.DG_AssociatedPrinters.DG_AssociatedPrinters_ProductType_ID, dbo.DG_AssociatedPrinters.DG_AssociatedPrinters_IsActive, 
                      dbo.DG_Orders_ProductType.DG_Orders_ProductType_Name, dbo.DG_AssociatedPrinters.DG_AssociatedPrinters_PaperSize
FROM         dbo.DG_AssociatedPrinters INNER JOIN
                      dbo.DG_Orders_ProductType ON dbo.DG_AssociatedPrinters.DG_AssociatedPrinters_ProductType_ID = dbo.DG_Orders_ProductType.DG_Orders_ProductType_pkey
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "DG_AssociatedPrinters"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 125
               Right = 320
            End
            DisplayFlags = 280
            TopColumn = 1
         End
         Begin Table = "DG_Orders_ProductType"
            Begin Extent = 
               Top = 6
               Left = 358
               Bottom = 125
               Right = 649
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vw_GetPrinterDetails'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vw_GetPrinterDetails'
GO
/****** Object:  View [dbo].[vw_GetPrintDetails]    Script Date: 01/05/2013 14:44:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vw_GetPrintDetails]
AS
SELECT     dbo.DG_Orders_ProductType.DG_Orders_ProductType_Name, dbo.DG_Photos.DG_Photos_FileName, dbo.DG_Photos.DG_Photos_CreatedOn, 
                      dbo.DG_PrintLog.PrintTime, dbo.DG_Users.DG_User_First_Name + ' ' + dbo.DG_Users.DG_User_Last_Name AS PrintedBy, 
                      DG_Users_1.DG_User_First_Name + ' ' + DG_Users_1.DG_User_Last_Name AS TakenBy, dbo.DG_PrintLog.ID
FROM         dbo.DG_Users INNER JOIN
                      dbo.DG_Photos INNER JOIN
                      dbo.DG_PrintLog INNER JOIN
                      dbo.DG_Orders_ProductType ON dbo.DG_PrintLog.ProductTypeId = dbo.DG_Orders_ProductType.DG_Orders_ProductType_pkey ON 
                      dbo.DG_Photos.DG_Photos_pkey = dbo.DG_PrintLog.PhotoId ON dbo.DG_Users.DG_User_pkey = dbo.DG_PrintLog.UserID INNER JOIN
                      dbo.DG_Users AS DG_Users_1 ON dbo.DG_Photos.DG_Photos_UserID = DG_Users_1.DG_User_pkey
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[29] 4[7] 2[23] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1[40] 2[34] 3) )"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 2
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = -278
      End
      Begin Tables = 
         Begin Table = "DG_Users"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 125
               Right = 229
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "DG_Photos"
            Begin Extent = 
               Top = 6
               Left = 267
               Bottom = 125
               Right = 471
            End
            DisplayFlags = 280
            TopColumn = 6
         End
         Begin Table = "DG_PrintLog"
            Begin Extent = 
               Top = 6
               Left = 509
               Bottom = 125
               Right = 669
            End
            DisplayFlags = 280
            TopColumn = 1
         End
         Begin Table = "DG_Orders_ProductType"
            Begin Extent = 
               Top = 6
               Left = 707
               Bottom = 125
               Right = 998
            End
            DisplayFlags = 280
            TopColumn = 3
         End
         Begin Table = "DG_Users_1"
            Begin Extent = 
               Top = 6
               Left = 1036
               Bottom = 125
               Right = 1227
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 10
         Width = 284
         Width = 2085
         Width = 1500
         Width = 2625
         Width = 3465
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      PaneHidden = 
      Begin Column' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vw_GetPrintDetails'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane2', @value=N'Widths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vw_GetPrintDetails'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=2 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vw_GetPrintDetails'
GO
/****** Object:  View [dbo].[vw_GetPhotoList]    Script Date: 01/05/2013 14:44:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vw_GetPhotoList]
AS
SELECT     dbo.DG_Photos.DG_Photos_UserID, dbo.DG_Users.DG_User_Name, dbo.DG_Photos.DG_Photos_FileName, dbo.DG_Photos.DG_Photos_CreatedOn, 
                      dbo.DG_Photos.DG_Photos_RFID, dbo.DG_Photos.DG_Photos_Background, dbo.DG_Photos.DG_Photos_Frame, dbo.DG_Photos.DG_Photos_DateTime, 
                      dbo.DG_Photos.DG_Photos_pkey, dbo.DG_Photos.DG_Photos_Layering, dbo.DG_Photos.DG_Photos_Effects, dbo.DG_Photos.DG_Photos_IsCroped, 
                      dbo.DG_Photos.DG_Photos_IsRedEye, dbo.DG_Photos.DG_Photos_Archive, 
                      CASE dbo.DG_Photos.DG_Location_Id WHEN - 1 THEN dbo.DG_Users.DG_Location_ID ELSE dbo.DG_Photos.DG_Location_Id END AS DG_Location_ID
FROM         dbo.DG_Users INNER JOIN
                      dbo.DG_Photos ON dbo.DG_Users.DG_User_pkey = dbo.DG_Photos.DG_Photos_UserID
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[20] 4[68] 2[4] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "DG_Users"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 125
               Right = 229
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "DG_Photos"
            Begin Extent = 
               Top = 0
               Left = 658
               Bottom = 210
               Right = 862
            End
            DisplayFlags = 280
            TopColumn = 7
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 17
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 12030
         Alias = 9375
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vw_GetPhotoList'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vw_GetPhotoList'
GO
/****** Object:  View [dbo].[vw_GetPhotographer]    Script Date: 01/05/2013 14:44:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vw_GetPhotographer]
AS
SELECT     DG_User_pkey, DG_User_Roles_Id, DG_User_First_Name + ' ' + DG_User_Last_Name AS Photograper
FROM         dbo.DG_Users
WHERE     (DG_User_Roles_Id = 8)
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[41] 4[20] 2[13] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "DG_Users"
            Begin Extent = 
               Top = 6
               Left = 268
               Bottom = 157
               Right = 471
            End
            DisplayFlags = 280
            TopColumn = 4
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1500
         Width = 2205
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vw_GetPhotographer'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vw_GetPhotographer'
GO
/****** Object:  View [dbo].[vw_GetPhotoEffects]    Script Date: 01/05/2013 14:44:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vw_GetPhotoEffects]
AS
SELECT     dbo.DG_Photo_Effects.DG_Photo_Effects_Name, dbo.DG_Photos_Changes.DG_Photos_Changes_Value, dbo.DG_Photos_Changes.DG_Photos_Changes_CreatedON, 
                      dbo.DG_Photos_Changes.DG_Photos_Changes_pkey, dbo.DG_Photos_Changes.DG_Photos_ID, dbo.DG_Photos_Changes.DG_Photos_Changes_TypeID
FROM         dbo.DG_Photo_Effects INNER JOIN
                      dbo.DG_Photos_Changes ON dbo.DG_Photo_Effects.DG_Photo_Effects_pkey = dbo.DG_Photos_Changes.DG_Photos_Changes_TypeID
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "DG_Photo_Effects"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 95
               Right = 248
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "DG_Photos_Changes"
            Begin Extent = 
               Top = 96
               Left = 38
               Bottom = 215
               Right = 288
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vw_GetPhotoEffects'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vw_GetPhotoEffects'
GO
/****** Object:  View [dbo].[vw_GetPackageDetails]    Script Date: 01/05/2013 14:44:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE View [dbo].[vw_GetPackageDetails]   
AS  
SELECT     DG_PackageDetails.DG_Package_Details_Pkey, DG_PackageDetails.DG_ProductTypeId, DG_PackageDetails.DG_PackageId, 
                      DG_PackageDetails.DG_Product_Quantity, DG_Orders_ProductType.DG_Orders_ProductType_pkey, DG_Orders_ProductType.DG_Orders_ProductType_Name
FROM         DG_PackageDetails RIGHT OUTER JOIN
                      DG_Orders_ProductType ON DG_PackageDetails.DG_ProductTypeId = DG_Orders_ProductType.DG_Orders_ProductType_pkey
GO
/****** Object:  UserDefinedFunction [dbo].[GetNumberofPhotosSales]    Script Date: 01/05/2013 14:44:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
-- select * from dbo.TestFun(46,'1/Dec/2012',getdate())
CREATE FUNCTION [dbo].[GetNumberofPhotosSales]
(
--@UserID int,
@DateFrom Datetime,
@DateTo Datetime
)
RETURNS 
@List TABLE 
(
	UserID int,
	NumberofSales int,
	ImageSold int,
	Revenue int
	
)
AS
BEGIN

declare @printtaken int 
DECLARE @listStr VARCHAR(MAX)

select @listStr = ltrim(substring(
(select ',' + DG_Photos_ID + '#' +  cast(DG_Orders_LineItems_pkey as varchar(max))  from DG_Orders_Details where DG_Photos_ID !=''
for xml path('')),2,10000))


Insert @List
Select DG_Photos_UserID UserID ,count(a.photoID) NumberofSales,sum(ActualQuantity) ImageSold, sum(case when DG_Orders_ProductType_IsBundled = 0 then DG_Orders_Details_Items_NetPrice/ActualQuantity else DG_Orders_Details_Items_NetPrice end) Revenue
from (
SELECT DG_Photos_UserID,SubString(item,0,CHARINDEX('#',item,0)) photoID,DG_Orders_ProductType_IsBundled,DG_Orders_Details_Items_NetPrice,DG_Orders_Details.DG_Orders_LineItems_pkey,
case when DG_Orders_ProductType_IsBundled = 0 then DG_Orders_Details.DG_Orders_LineItems_Quantity else 1 end ActualQuantity
FROM dbo.fnSplit(@listStr, ',') inner join DG_Orders_Details on DG_Orders_Details.DG_Orders_LineItems_pkey = SubString(item,CHARINDEX('#',item,0) + 1 ,Len(item) + 1 ) 
inner join DG_Orders_ProductType ptype on ptype.DG_Orders_ProductType_pkey = DG_Orders_Details.DG_Orders_Details_ProductType_pkey 
inner join DG_Photos on SubString(item,0,CHARINDEX('#',item,0)) = DG_Photos.DG_Photos_pkey
where DG_Orders_ID is not Null and (SubString(item,0,CHARINDEX('#',item,0)) is not null and SubString(item,0,CHARINDEX('#',item,0)) <> ' ')  and (DG_Photos.DG_Photos_CreatedOn between @DateFrom and @DateTo)) a  group by a.DG_Photos_UserID
   
                
RETURN 
END
GO
/****** Object:  UserDefinedFunction [dbo].[GetPackageChilds]    Script Date: 01/05/2013 14:44:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create function [dbo].[GetPackageChilds]
(
@PackageId int
)

returns varchar(max)
as
begin
declare @retvalue varchar(max)
Select @retvalue=substring( 
(SELECT    ' + ' + DG_Orders_ProductType.DG_Orders_ProductType_Name + '(' + cast(DG_PackageDetails.DG_Product_Quantity as varchar)+ ')'
FROM         DG_PackageDetails INNER JOIN
                      DG_Orders_ProductType ON DG_PackageDetails.DG_ProductTypeId = DG_Orders_ProductType.DG_Orders_ProductType_pkey
WHERE     (DG_PackageDetails.DG_PackageId = @PackageId) and DG_Product_Quantity>0
for xml path(''))
,3,5000)
return @retvalue

end
GO
/****** Object:  View [dbo].[GetOrderQuantity]    Script Date: 01/05/2013 14:44:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[GetOrderQuantity]
as
select DG_Orders_pkey,
sum(
case 
when DG_Orders_Details_ProductType_pkey = 1 then QNty*DG_Orders_LineItems_Quantity 
when DG_Orders_Details_ProductType_pkey = 2 then QNty*DG_Orders_LineItems_Quantity 
else 1*DG_Orders_LineItems_Quantity end
)
Quantity from (
SELECT     DG_Orders.DG_Orders_pkey, DG_Orders.DG_Orders_Number, DG_Orders_Details.DG_Photos_ID, DG_Orders_Details.DG_Orders_LineItems_Quantity, 
                      DG_Orders_Details.DG_Orders_Details_ProductType_pkey,LEN(','+DG_Photos_ID) - LEN(REPLACE(','+DG_Photos_ID, ',', ''))Qnty
FROM         DG_Orders INNER JOIN
                      DG_Orders_Details ON DG_Orders.DG_Orders_pkey = DG_Orders_Details.DG_Orders_ID
                      --where dg_orders_pkey = 216
                      )A group by DG_Orders_pkey
GO
/****** Object:  UserDefinedFunction [dbo].[GetOrderItemsRefunded]    Script Date: 01/05/2013 14:44:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Function [dbo].[GetOrderItemsRefunded]
(
	@OrderID int
)
returns varchar(max)
as
begin
DECLARE @listStr VARCHAR(MAX)

Select @listStr = COALESCE(@listStr+ ' + ' , '') + DG_Orders_ProductType_Name 
from (
SELECT DRD.DG_LineItemId, DRD.RefundPhotoId, pt.DG_Orders_ProductType_Name, DR.DG_OrderId
FROM   dbo.DG_RefundDetails AS DRD INNER JOIN
dbo.DG_Refund AS DR ON DR.DG_RefundId = DRD.DG_RefundMaster_ID INNER JOIN
dbo.DG_Orders_Details ON DRD.DG_LineItemId = dbo.DG_Orders_Details.DG_Orders_LineItems_pkey INNER JOIN
dbo.DG_Orders_ProductType AS pt ON dbo.DG_Orders_Details.DG_Orders_Details_ProductType_pkey = pt.DG_Orders_ProductType_pkey
where DG_Orders_ID = @OrderID
) refund
return @listStr
end
GO
/****** Object:  StoredProcedure [dbo].[OperatorPerformanceReport]    Script Date: 01/05/2013 14:44:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Procedure [dbo].[OperatorPerformanceReport]-- '12/13/2012 02:00:00 AM','12/30/2012 12:00:00 AM','11/1/2012 12:00:00 AM','11/30/2012 12:00:00 AM',3,null,null,1
(
@FromDate datetime =null,
@ToDate datetime=null,
@SecondFromDate datetime = null,
@SecondToDate datetime = null,
@CurrencyId int,
@StoreName varchar(max),
@UserName varchar(max),
@Comparasion bit
)
as
Set @CurrencyId = 3;
Declare @CurrencyRate decimal
Declare @CurrencySymbol varchar(100)
Select @CurrencyRate = DG_Currency_pkey, @CurrencySymbol= DG_Currency_Symbol  from DG_Currency where DG_Currency_pkey = @CurrencyId

if(@Comparasion = 1)
begin

	SELECT @CurrencySymbol CurrencySymbol,@StoreName StoreName,@UserName UserName,CONVERT(varchar(10), @FromDate, 101) FromDate ,CONVERT(varchar(10), @ToDate, 101) ToDate,0 Data1,
	(((select isnull(sum(DG_Orders_NetCost),0) from DG_Orders where DG_Orders_UserID = DO.DG_User_pkey and DG_Orders_Date between @FromDate and @ToDate))) Revenue,(select COUNT(DG_Orders_pkey) from DG_Orders where DG_Orders_UserID = DO.DG_User_pkey and DG_Orders_Date between @FromDate and @ToDate ) as TotalSale,
	(Select isnull(sum(case DG_Orders_ProductType_IsBundled when 0 then case CHARINDEX(',', DG_Photos_ID, 0) when 0 then DG_Orders_LineItems_Quantity else ((LEN( DG_Photos_ID) - LEN(REPLACE(DG_Photos_ID, ',', ''))) + 1) * DG_Orders_LineItems_Quantity end
	else DG_Orders_LineItems_Quantity end),0) NoofImageSale
	from DG_Orders_Details inner join DG_Orders_ProductType 
	on DG_Orders_ProductType.DG_Orders_ProductType_pkey = DG_Orders_Details.DG_Orders_Details_ProductType_pkey inner join DG_Orders on DG_Orders.DG_Orders_pkey = Dg_Orders_Id
	where Dg_Orders_Id is not null And DG_Orders_Userid = DO.DG_User_pkey and DG_Orders_Details_ProductType_pkey not in (35,36) and DG_Orders_Date between @FromDate and @ToDate) 'Images_Sold',(select count(*) from DG_Photos where DG_Photos_UserID = DO.DG_User_pkey and DG_Photos.DG_Photos_DateTime between @FromDate and @ToDate ) Capture,
	(Select count(*) from DG_Activity Where DG_Acitivity_ActionType in (1,2) and DG_Acitivity_By = DO.DG_User_pkey and  (DG_Acitivity_Date between @FromDate and @ToDate)) 'Shots_Previewed',
	(Select isnull(sum(DG_Orders_LineItems_Quantity),0) from DG_Orders_Details inner join DG_Orders on DG_Orders.DG_Orders_pkey = Dg_Orders_Id where Dg_Orders_Id is not null And DG_Orders_Userid = DO.DG_User_pkey and DG_Orders_Details_ProductType_pkey in (35,36) and DG_Orders_Date between @FromDate and @ToDate) TotalBurned,
	(select DG_Users.DG_User_Name from DG_Users where DG_User_pkey = DO.DG_User_pkey) OperatorName
	FROM  dbo.DG_Users DO
	GROUP BY DG_User_pkey 

	union

	SELECT @CurrencySymbol CurrencySymbol,@StoreName StoreName,@UserName UserName,CONVERT(varchar(10), @SecondFromDate, 101) FromDate,CONVERT(varchar(10), @SecondToDate, 101) ToDate,1 Data1,
	(((select isnull(sum(DG_Orders_NetCost),0) from DG_Orders where DG_Orders_UserID = DO.DG_User_pkey and DG_Orders_Date between @SecondFromDate and @SecondToDate))) Revenue,(select COUNT(DG_Orders_pkey) from DG_Orders where DG_Orders_UserID = DO.DG_User_pkey and DG_Orders_Date between @SecondFromDate and @SecondToDate ) as TotalSale,
	(Select isnull(sum(case DG_Orders_ProductType_IsBundled when 0 then case CHARINDEX(',', DG_Photos_ID, 0) when 0 then DG_Orders_LineItems_Quantity else ((LEN( DG_Photos_ID) - LEN(REPLACE(DG_Photos_ID, ',', ''))) + 1) * DG_Orders_LineItems_Quantity end
	else DG_Orders_LineItems_Quantity end),0) NoofImageSale
	from DG_Orders_Details inner join DG_Orders_ProductType 
	on DG_Orders_ProductType.DG_Orders_ProductType_pkey = DG_Orders_Details.DG_Orders_Details_ProductType_pkey inner join DG_Orders on DG_Orders.DG_Orders_pkey = Dg_Orders_Id
	where Dg_Orders_Id is not null And DG_Orders_Userid = DO.DG_User_pkey and DG_Orders_Details_ProductType_pkey not in (35,36) and DG_Orders_Date between @SecondFromDate and @SecondToDate) 'Images_Sold',(select count(*) from DG_Photos where DG_Photos_UserID = DO.DG_User_pkey and DG_Photos.DG_Photos_DateTime between @SecondFromDate and @SecondToDate ) Capture,(Select count(*) from DG_Activity Where DG_Acitivity_ActionType in (1,2) and DG_Acitivity_By = DO.DG_User_pkey and  (DG_Acitivity_Date between @SecondFromDate and @SecondToDate)) 'Shots_Previewed',
	(Select isnull(sum(DG_Orders_LineItems_Quantity),0) from DG_Orders_Details inner join DG_Orders on DG_Orders.DG_Orders_pkey = Dg_Orders_Id where Dg_Orders_Id is not null And DG_Orders_Userid = DO.DG_User_pkey and DG_Orders_Details_ProductType_pkey in (35,36) and DG_Orders_Date between @SecondFromDate and @SecondToDate) TotalBurned,
	(select DG_Users.DG_User_Name from DG_Users where DG_User_pkey = DO.DG_User_pkey) OperatorName
	FROM  dbo.DG_Users DO
	GROUP BY DG_User_pkey 

end
else
begin
SELECT @CurrencySymbol CurrencySymbol,@StoreName StoreName,@UserName UserName, CONVERT(varchar(10), @FromDate, 101)FromDate,CONVERT(varchar(10), @ToDate, 101) ToDate,'0' Data1,
	(((select isnull(sum(DG_Orders_NetCost),0) from DG_Orders where DG_Orders_UserID = DO.DG_User_pkey and DG_Orders_Date between @FromDate and @ToDate))) Revenue,(select COUNT(DG_Orders_pkey) from DG_Orders where DG_Orders_UserID = DO.DG_User_pkey and DG_Orders_Date between @FromDate and @ToDate ) as TotalSale,
	(Select isnull(sum(case DG_Orders_ProductType_IsBundled when 0 then case CHARINDEX(',', DG_Photos_ID, 0) when 0 then DG_Orders_LineItems_Quantity else ((LEN( DG_Photos_ID) - LEN(REPLACE(DG_Photos_ID, ',', ''))) + 1) * DG_Orders_LineItems_Quantity end
	else DG_Orders_LineItems_Quantity end),0) NoofImageSale
	from DG_Orders_Details inner join DG_Orders_ProductType 
	on DG_Orders_ProductType.DG_Orders_ProductType_pkey = DG_Orders_Details.DG_Orders_Details_ProductType_pkey inner join DG_Orders on DG_Orders.DG_Orders_pkey = Dg_Orders_Id
	where Dg_Orders_Id is not null And DG_Orders_Userid = DO.DG_User_pkey and DG_Orders_Date between @FromDate and @ToDate) 'Images_Sold',(select count(*) from DG_Photos where DG_Photos_UserID = DO.DG_User_pkey and DG_Photos.DG_Photos_DateTime between @FromDate and @ToDate ) Capture,
	(Select count(*) from DG_Activity Where DG_Acitivity_ActionType in (1,2) and DG_Acitivity_By = DO.DG_User_pkey and  (DG_Acitivity_Date between @FromDate and @ToDate)) 'Shots_Previewed',
	(Select isnull(sum(DG_Orders_LineItems_Quantity),0) from DG_Orders_Details inner join DG_Orders on DG_Orders.DG_Orders_pkey = Dg_Orders_Id where Dg_Orders_Id is not null And DG_Orders_Userid = DO.DG_User_pkey and DG_Orders_Details_ProductType_pkey in (35,36) and DG_Orders_Date between @FromDate and @ToDate) TotalBurned,
	(select DG_Users.DG_User_Name from DG_Users where DG_User_pkey = DO.DG_User_pkey) OperatorName
	FROM  dbo.DG_Users DO
	GROUP BY DG_User_pkey 
end

--Select * from DG_Orders
GO
/****** Object:  StoredProcedure [dbo].[OperationalAudit]    Script Date: 01/05/2013 14:44:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--
CREATE Procedure [dbo].[OperationalAudit] --'01/12/12' ,'12/31/12'
(
@FromDate datetime,
@ToDate datetime
)
as
Create Table #temp1(DG_Orders_pkey int ,DG_Orders_Number varchar(max),Location varchar(max),PhotoID varchar(max),Qty int,PhotoGrapher varchar(max),ProductType varchar(max))

Create Table #temp(OrderId int,PhotoID varchar(max),Qty int,ProductType varchar(max),isBundle bit,DG_Orders_Number varchar(max),ProductTypeID int,isAcessory bit)

Declare @Orderid int
Declare @PhotoID varchar(max)
Declare @PhotoGrapher varchar(max)
Declare @DG_Orders_Number varchar(max)
Declare @Qty int
Declare @ProductType varchar(max)
Declare @isBundle bit
Declare @DG_IsAccessory bit
Declare @PID int
Declare @ImageID int
Declare @ImageNumber varchar(max)
Declare @TempImageNumber varchar(max)
Declare @Location varchar(max)
Declare @ParentID int
Declare @TempProductID int
Declare @ParentName varchar(max)
--Select * from DG_Orders_Details

Declare AuditTrailCursor cursor for
SELECT     DO.DG_Orders_pkey, DOD.DG_Photos_ID, DOD.DG_Orders_LineItems_Quantity, Ptype.DG_Orders_ProductType_Name,DO.DG_Orders_Number,
                      Ptype.DG_Orders_ProductType_IsBundled,DG_Orders_ProductType_pkey,Ptype.DG_IsAccessory,DOD.DG_Orders_Details_LineItem_ParentID
FROM         dbo.DG_Orders AS DO INNER JOIN
                      dbo.DG_Orders_Details AS DOD ON DOD.DG_Orders_ID = DO.DG_Orders_pkey INNER JOIN
                      dbo.DG_Orders_ProductType AS Ptype ON DOD.DG_Orders_Details_ProductType_pkey = Ptype.DG_Orders_ProductType_pkey
where  (DG_Orders_Date between  @FromDate and @ToDate) and (DOD.DG_Orders_Details_LineItem_ParentID > -1 or DOD.DG_Photos_ID <> ' ')

open AuditTrailCursor
Fetch next from AuditTrailCursor
into @Orderid,@PhotoID,@Qty,@ProductType,@DG_Orders_Number,@isBundle,@PID,@DG_IsAccessory,@ParentID

while (@@Fetch_Status = 0)
begin

if(CHARINDEX(',', @PhotoID) > 0)
begin 
	Declare PhotoCursor cursor for
	Select item from dbo.fnSplit(@PhotoID,',')
	
	open PhotoCursor
	
	Fetch Next from PhotoCursor into @ImageID
	
	
	set @ImageNumber = '' 
	while (@@Fetch_Status = 0)
	begin
		
		Select @TempImageNumber = DG_Photos_RFID,@Location =  DG_Location_Name, @PhotoGrapher = DG_User_First_Name + ' '  + DG_User_Last_Name + ' (' + DG_User_Name + ')' from DG_Photos inner join DG_Location on DG_Photos.DG_Location_Id = DG_Location_pkey inner join DG_Users 
		on Dg_Users.DG_User_pkey = DG_Photos.DG_Photos_UserID where DG_Photos_pkey = @ImageID
	
		if(@PID = 4 or @PID = 35 or @PID = 36) -- unique 4 * 5
		begin
			if(@ImageNumber = '' )
			begin
				set @ImageNumber = @TempImageNumber
			end
			else
			begin
					set @ImageNumber = @ImageNumber + ',' + @TempImageNumber
			end	
		end
		else
		begin
			
		if((@ParentID <> -1) and (@ParentID <> ' '))
		begin
		Select @TempProductID = DG_Orders_Details_ProductType_pkey from DG_Orders_Details Where DG_Orders_LineItems_pkey = @ParentID
		Select @ParentName = DG_Orders_ProductType_Name from DG_Orders_ProductType where DG_Orders_ProductType.DG_Orders_ProductType_pkey = @TempProductID
		insert into #temp1 
			Select @Orderid,@DG_Orders_Number,@Location,@TempImageNumber,@Qty,@PhotoGrapher,@ProductType + ' ('+ @ParentName + ')'
		end
		else
		begin
			insert into #temp1 
			Select @Orderid,@DG_Orders_Number,@Location,@TempImageNumber,@Qty,@PhotoGrapher,@ProductType
		end
			
		
		end
	Fetch Next from PhotoCursor into @ImageID
	end
	
	Close PhotoCursor
	deallocate PhotoCursor

	if(@PID = 4 or @PID = 35 or @PID = 36) -- unique 4 * 5
		begin
		if(@ParentID <> -1)
		begin
				Select @TempProductID = DG_Orders_Details_ProductType_pkey from DG_Orders_Details Where DG_Orders_LineItems_pkey = @ParentID
				Select @ParentName = DG_Orders_ProductType_Name from DG_Orders_ProductType where DG_Orders_ProductType.DG_Orders_ProductType_pkey = @TempProductID
			insert into #temp1 
			Select @Orderid,@DG_Orders_Number,@Location,@ImageNumber,@Qty,@PhotoGrapher,@ProductType + ' ('+ @ParentName + ')'
		
		end
		else
		begin
			insert into #temp1 
			Select @Orderid,@DG_Orders_Number,@Location,@ImageNumber,@Qty,@PhotoGrapher,@ProductType
		end
			
		end
end
else
begin
if(@PhotoID <> ' ')
begin
	Select @ImageNumber = DG_Photos_RFID,@Location =  DG_Location_Name ,@PhotoGrapher = DG_User_First_Name + ' '  + DG_User_Last_Name + ' (' + DG_User_Name + ')' from DG_Photos inner join DG_Location on DG_Photos.DG_Location_Id = DG_Location_pkey inner join DG_Users 
	on Dg_Users.DG_User_pkey = DG_Photos.DG_Photos_UserID
	where DG_Photos_pkey = @PhotoID
	
	if(@ParentID <> -1)
	begin
		
		Select @TempProductID = DG_Orders_Details_ProductType_pkey from DG_Orders_Details Where DG_Orders_LineItems_pkey = @ParentID
		Select @ParentName = DG_Orders_ProductType_Name from DG_Orders_ProductType where DG_Orders_ProductType.DG_Orders_ProductType_pkey = @TempProductID
		insert into #temp1 
		Select @Orderid,@DG_Orders_Number,@Location,@ImageNumber,@Qty,@PhotoGrapher,@ProductType + ' ('+ @ParentName + ')'
	
	end
	else
	begin
		insert into #temp1 
		Select @Orderid,@DG_Orders_Number,@Location,@ImageNumber,@Qty,@PhotoGrapher,@ProductType
	end
	
	
end
else
begin
    	
    	if(@ParentID <> -1)
		begin
			
			Select @TempProductID = DG_Orders_Details_ProductType_pkey from DG_Orders_Details Where DG_Orders_LineItems_pkey = @ParentID
			Select @ParentName = DG_Orders_ProductType_Name from DG_Orders_ProductType where DG_Orders_ProductType.DG_Orders_ProductType_pkey = @TempProductID
		
			insert into #temp1 
			Select @Orderid,@DG_Orders_Number,'N/A','N/A',@Qty,'N/A',@ProductType + ' ('+ @ParentName + ')'
			
		end
		else
		begin
			insert into #temp1 
			Select @Orderid,@DG_Orders_Number,'N/A','N/A',@Qty,'N/A',@ProductType
		end
    	
	
end	
end


Fetch next from AuditTrailCursor
into @Orderid,@PhotoID,@Qty,@ProductType,@DG_Orders_Number,@isBundle,@PID,@DG_IsAccessory,@ParentID

end

Close AuditTrailCursor
deallocate AuditTrailCursor

Select * from #temp1

drop table #temp1
drop table #temp




--Select 0 DG_Orders_pkey  ,'fdfdf1' DG_Orders_Number,'1' Location,'dff' PhotoID,3 Qty,'dedsdsd' PhotoGrapher,'ssd ' ProductType











--drop table #temp
GO
/****** Object:  UserDefinedFunction [dbo].[GetRFID]    Script Date: 01/05/2013 14:44:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE function [dbo].[GetRFID]
(
@InputValue varchar(max)
)
 RETURNS varchar(max)
 begin
	declare @ReturnValue varchar(max)
	
	Select  @ReturnValue = COALESCE(@ReturnValue +',','') +   CAST(DP.DG_Photos_RFID  AS varchar(250)) from dbo.fnSplit(@InputValue,',') t1 inner join DG_Photos DP on DP.DG_Photos_pkey = t1.item
	return @ReturnValue
 end
GO
/****** Object:  StoredProcedure [dbo].[GetRefundedItems]    Script Date: 01/05/2013 14:44:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Procedure [dbo].[GetRefundedItems] --46  
(  
@DG_OrderID int  
)  
as  
begin  
  
declare @PhotoID varchar(max)  
declare @Lineitemid int  
Create Table #tempRefund (LineitemID int ,PhotoID int)  
  
declare  RefundItem cursor for  
Select DG_LineItemID,RefundPhotoID from DG_RefundDetails  DRD inner join DG_Refund DR on DR.Dg_RefundId = DRD.DG_RefundMaster_ID  
where DG_OrderId = @DG_OrderID
  
open RefundItem  
  
fetch next from RefundItem into @Lineitemid,@PhotoID  
while(@@fetch_status <> -1)  
begin  
insert into #tempRefund   
select @Lineitemid,item from dbo.fnSplit(@PhotoID,',')  
  
fetch next from RefundItem into @Lineitemid,@PhotoID  
end  
close RefundItem  
deallocate RefundItem  
  
Select * from #tempRefund  
  
drop table #tempRefund  
  
  
 
end
GO
/****** Object:  UserDefinedFunction [dbo].[GetPreviewCountLocationwise]    Script Date: 01/05/2013 14:44:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Function [dbo].[GetPreviewCountLocationwise]
(
@LocationId int,
@Fromdate datetime,
@ToDate datetime
)
returns int
As
begin
declare @TotalQuantity int
Select @TotalQuantity=count(*) from DG_Activity INNER JOIN
                      DG_Users ON DG_Activity.DG_Acitivity_By = DG_Users.DG_User_pkey inner JOIN
                      DG_Location ON DG_Users.DG_Location_ID = DG_Location.DG_Location_pkey
Where DG_Acitivity_ActionType in (1,2) and  
(DG_Acitivity_Date between @Fromdate and @ToDate) and DG_Location.DG_Location_pkey=@LocationId
return @TotalQuantity
end
GO
/****** Object:  StoredProcedure [dbo].[FinancialAudit]    Script Date: 01/05/2013 14:44:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--
Create Procedure [dbo].[FinancialAudit] --'01/12/12' ,'12/31/12'
(
@FromDate datetime,
@ToDate datetime
)
as

-- (DG_Orders_Date between  @FromDate and @ToDate) and 
Create Table #temp1(DG_Orders_pkey int ,DG_Orders_Number varchar(max),Location varchar(max),PhotoID varchar(max),Qty int,PhotoGrapher varchar(max),ProductType varchar(max))

Create Table #temp(OrderId int,PhotoID varchar(max),Qty int,ProductType varchar(max),isBundle bit,DG_Orders_Number varchar(max),ProductTypeID int,isAcessory bit)

Declare @Orderid int
Declare @PhotoID varchar(max)
Declare @PhotoGrapher varchar(max)
Declare @DG_Orders_Number varchar(max)
Declare @Qty int
Declare @ProductType varchar(max)
Declare @isBundle bit
Declare @DG_IsAccessory bit
Declare @PID int
Declare @ImageID int
Declare @ImageNumber varchar(max)
Declare @TempImageNumber varchar(max)
Declare @Location varchar(max)
Declare @ParentID int
Declare @TempProductID int
Declare @ParentName varchar(max)
--Select * from DG_Orders_Details

Declare AuditTrailCursor cursor for
SELECT     DO.DG_Orders_pkey, DOD.DG_Photos_ID, DOD.DG_Orders_LineItems_Quantity, Ptype.DG_Orders_ProductType_Name,DO.DG_Orders_Number,
                      Ptype.DG_Orders_ProductType_IsBundled,DG_Orders_ProductType_pkey,Ptype.DG_IsAccessory,DOD.DG_Orders_Details_LineItem_ParentID
FROM         dbo.DG_Orders AS DO INNER JOIN
                      dbo.DG_Orders_Details AS DOD ON DOD.DG_Orders_ID = DO.DG_Orders_pkey INNER JOIN
                      dbo.DG_Orders_ProductType AS Ptype ON DOD.DG_Orders_Details_ProductType_pkey = Ptype.DG_Orders_ProductType_pkey
where (DOD.DG_Orders_Details_LineItem_ParentID > -1 or DOD.DG_Photos_ID <> ' ')

open AuditTrailCursor
Fetch next from AuditTrailCursor
into @Orderid,@PhotoID,@Qty,@ProductType,@DG_Orders_Number,@isBundle,@PID,@DG_IsAccessory,@ParentID

while (@@Fetch_Status = 0)
begin

if(CHARINDEX(',', @PhotoID) > 0)
begin 
	Declare PhotoCursor cursor for
	Select item from dbo.fnSplit(@PhotoID,',')
	
	open PhotoCursor
	
	Fetch Next from PhotoCursor into @ImageID
	
	
	set @ImageNumber = '' 
	while (@@Fetch_Status = 0)
	begin
		
		Select @TempImageNumber = DG_Photos_RFID,@Location =  DG_Location_Name, @PhotoGrapher = DG_User_First_Name + ' '  + DG_User_Last_Name + ' (' + DG_User_Name + ')' from DG_Photos inner join DG_Location on DG_Photos.DG_Location_Id = DG_Location_pkey inner join DG_Users 
		on Dg_Users.DG_User_pkey = DG_Photos.DG_Photos_UserID where DG_Photos_pkey = @ImageID
	
		if(@PID = 4 or @PID = 35 or @PID = 36) -- unique 4 * 5
		begin
			if(@ImageNumber = '' )
			begin
				set @ImageNumber = @TempImageNumber
			end
			else
			begin
					set @ImageNumber = @ImageNumber + ',' + @TempImageNumber
			end	
		end
		else
		begin
			
		if((@ParentID <> -1) and (@ParentID <> ' '))
		begin
		Select @TempProductID = DG_Orders_Details_ProductType_pkey from DG_Orders_Details Where DG_Orders_LineItems_pkey = @ParentID
		Select @ParentName = DG_Orders_ProductType_Name from DG_Orders_ProductType where DG_Orders_ProductType.DG_Orders_ProductType_pkey = @TempProductID
		insert into #temp1 
			Select @Orderid,@DG_Orders_Number,@Location,@TempImageNumber,@Qty,@PhotoGrapher,@ProductType + ' ('+ @ParentName + ')'
		end
		else
		begin
			insert into #temp1 
			Select @Orderid,@DG_Orders_Number,@Location,@TempImageNumber,@Qty,@PhotoGrapher,@ProductType
		end
			
		
		end
	Fetch Next from PhotoCursor into @ImageID
	end
	
	Close PhotoCursor
	deallocate PhotoCursor

	if(@PID = 4 or @PID = 35 or @PID = 36) -- unique 4 * 5
		begin
		if(@ParentID <> -1)
		begin
				Select @TempProductID = DG_Orders_Details_ProductType_pkey from DG_Orders_Details Where DG_Orders_LineItems_pkey = @ParentID
				Select @ParentName = DG_Orders_ProductType_Name from DG_Orders_ProductType where DG_Orders_ProductType.DG_Orders_ProductType_pkey = @TempProductID
			insert into #temp1 
			Select @Orderid,@DG_Orders_Number,@Location,@ImageNumber,@Qty,@PhotoGrapher,@ProductType + ' ('+ @ParentName + ')'
		
		end
		else
		begin
			insert into #temp1 
			Select @Orderid,@DG_Orders_Number,@Location,@ImageNumber,@Qty,@PhotoGrapher,@ProductType
		end
			
		end
end
else
begin
if(@PhotoID <> ' ')
begin
	Select @ImageNumber = DG_Photos_RFID,@Location =  DG_Location_Name ,@PhotoGrapher = DG_User_First_Name + ' '  + DG_User_Last_Name + ' (' + DG_User_Name + ')' from DG_Photos inner join DG_Location on DG_Photos.DG_Location_Id = DG_Location_pkey inner join DG_Users 
	on Dg_Users.DG_User_pkey = DG_Photos.DG_Photos_UserID
	where DG_Photos_pkey = @PhotoID
	
	if(@ParentID <> -1)
	begin
		
		Select @TempProductID = DG_Orders_Details_ProductType_pkey from DG_Orders_Details Where DG_Orders_LineItems_pkey = @ParentID
		Select @ParentName = DG_Orders_ProductType_Name from DG_Orders_ProductType where DG_Orders_ProductType.DG_Orders_ProductType_pkey = @TempProductID
		insert into #temp1 
		Select @Orderid,@DG_Orders_Number,@Location,@ImageNumber,@Qty,@PhotoGrapher,@ProductType + ' ('+ @ParentName + ')'
	
	end
	else
	begin
		insert into #temp1 
		Select @Orderid,@DG_Orders_Number,@Location,@ImageNumber,@Qty,@PhotoGrapher,@ProductType
	end
	
	
end
else
begin
    	
    	if(@ParentID <> -1)
		begin
			
			Select @TempProductID = DG_Orders_Details_ProductType_pkey from DG_Orders_Details Where DG_Orders_LineItems_pkey = @ParentID
			Select @ParentName = DG_Orders_ProductType_Name from DG_Orders_ProductType where DG_Orders_ProductType.DG_Orders_ProductType_pkey = @TempProductID
		
			insert into #temp1 
			Select @Orderid,@DG_Orders_Number,'N/A','N/A',@Qty,'N/A',@ProductType + ' ('+ @ParentName + ')'
			
		end
		else
		begin
			insert into #temp1 
			Select @Orderid,@DG_Orders_Number,'N/A','N/A',@Qty,'N/A',@ProductType
		end
    	
	
end	
end


Fetch next from AuditTrailCursor
into @Orderid,@PhotoID,@Qty,@ProductType,@DG_Orders_Number,@isBundle,@PID,@DG_IsAccessory,@ParentID

end

Close AuditTrailCursor
deallocate AuditTrailCursor

Select * from #temp1

drop table #temp1
drop table #temp




--Select 0 DG_Orders_pkey  ,'fdfdf1' DG_Orders_Number,'1' Location,'dff' PhotoID,3 Qty,'dedsdsd' PhotoGrapher,'ssd ' ProductType











--drop table #temp
GO
/****** Object:  View [dbo].[digi]    Script Date: 01/05/2013 14:44:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[digi]
AS
SELECT     dbo.DG_Orders_ProductType.DG_Orders_ProductType_Name, SUM(dbo.DG_Orders_Details.DG_Orders_LineItems_Quantity) AS total_quantity
FROM         dbo.DG_Orders_ProductType INNER JOIN
                      dbo.DG_Orders_Details ON dbo.DG_Orders_ProductType.DG_Orders_ProductType_pkey = dbo.DG_Orders_Details.DG_Orders_Details_ProductType_pkey
GROUP BY dbo.DG_Orders_ProductType.DG_Orders_ProductType_Name
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[60] 4[16] 2[14] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1[50] 2[18] 3) )"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 2
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "DG_Orders_ProductType"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 251
               Right = 331
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "DG_Orders_Details"
            Begin Extent = 
               Top = 2
               Left = 424
               Bottom = 258
               Right = 818
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      PaneHidden = 
      Begin ColumnWidths = 12
         Column = 3090
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'digi'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'digi'
GO
/****** Object:  View [dbo].[vw_GetOrderDetails]    Script Date: 01/05/2013 14:44:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE View [dbo].[vw_GetOrderDetails]  
as  
SELECT     DG_Orders_Details.DG_Orders_LineItems_pkey, DG_Orders_Details.DG_Orders_ID, DG_Orders_Details.DG_Photos_ID, 
                      DG_Orders_Details.DG_Orders_LineItems_Created, DG_Orders_Details.DG_Orders_LineItems_DiscountType, 
                      DG_Orders_Details.DG_Orders_LineItems_DiscountAmount, DG_Orders_Details.DG_Orders_LineItems_Quantity, 
                      DG_Orders_Details.DG_Orders_Details_Items_UniPrice, DG_Orders_Details.DG_Orders_Details_Items_TotalCost, 
                      DG_Orders_Details.DG_Orders_Details_Items_NetPrice, DG_Orders_Details.DG_Orders_Details_ProductType_pkey, 
                      DG_Orders_Details.DG_Orders_Details_LineItem_ParentID, DG_Orders_Details.DG_Orders_Details_LineItem_PrinterReferenceID, 
                      DG_Orders.DG_Orders_Number, DG_Orders.DG_Orders_Date, DG_Orders.DG_Orders_Cost, DG_Orders.DG_Orders_NetCost, 
                      DG_Orders.DG_Orders_Currency_ID, DG_Orders.DG_Orders_Currency_Conversion_Rate, DG_Orders.DG_Orders_Total_Discount, 
                      DG_Orders.DG_Orders_Total_Discount_Details, DG_Orders.DG_Orders_PaymentDetails, DG_Orders.DG_Orders_PaymentMode, 
                      DG_Orders_ProductType.DG_Orders_ProductType_Name
FROM         DG_Orders_Details INNER JOIN
                      DG_Orders_ProductType ON 
                      DG_Orders_Details.DG_Orders_Details_ProductType_pkey = DG_Orders_ProductType.DG_Orders_ProductType_pkey LEFT OUTER JOIN
                      DG_Orders ON DG_Orders_Details.DG_Orders_ID = DG_Orders.DG_Orders_pkey
GO
/****** Object:  View [dbo].[vw_GetConfigdata]    Script Date: 01/05/2013 14:44:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vw_GetConfigdata]
AS
SELECT     DG_Config_pkey, DG_Hot_Folder_Path, DG_Frame_Path, DG_BG_Path, DG_Mod_Password, DG_NoOfPhotos, DG_Graphics_Path, DG_Watermark, DG_SemiOrder, 
                      DG_HighResolution, DG_AllowDiscount, DG_EnableDiscountOnTotal, WiFiStartingNumber, FolderStartingNumber, IsAutoLock, PosOnOff,
                          (SELECT     DG_Currency_pkey
                            FROM          dbo.DG_Currency
                            WHERE      (DG_Currency_Default = 1)) AS DefaultCurrency, DG_SemiOrderMain, DG_ReceiptPrinter, DG_IsAutoRotate
FROM         dbo.DG_Configuration
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "DG_Configuration"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 125
               Right = 258
            End
            DisplayFlags = 280
            TopColumn = 15
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vw_GetConfigdata'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vw_GetConfigdata'
GO
/****** Object:  View [dbo].[vw_GetCameraDetails]    Script Date: 01/05/2013 14:44:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vw_GetCameraDetails]
AS
SELECT     TOP (100) PERCENT dbo.DG_Users.DG_User_First_Name + ' ' + dbo.DG_Users.DG_User_Last_Name AS PhotographerName, 
                      dbo.DG_CameraDetails.DG_Camera_Name, dbo.DG_CameraDetails.DG_Camera_Make, dbo.DG_CameraDetails.DG_Camera_Model, 
                      dbo.DG_CameraDetails.DG_AssignTo, dbo.DG_CameraDetails.DG_Camera_Start_Series, dbo.DG_CameraDetails.DG_Updatedby, 
                      dbo.DG_CameraDetails.DG_UpdatedDate, dbo.DG_CameraDetails.DG_Camera_pkey, 'C' + CAST(dbo.DG_CameraDetails.DG_Camera_ID AS varchar) 
                      AS DG_CameraFolder, dbo.DG_CameraDetails.DG_Camera_IsDeleted, dbo.DG_CameraDetails.DG_Camera_ID
FROM         dbo.DG_CameraDetails INNER JOIN
                      dbo.DG_Users ON dbo.DG_CameraDetails.DG_AssignTo = dbo.DG_Users.DG_User_pkey
WHERE     (dbo.DG_CameraDetails.DG_Camera_IsDeleted <> 1)
ORDER BY dbo.DG_CameraDetails.DG_Camera_ID
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[31] 4[15] 2[32] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "DG_CameraDetails"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 123
               Right = 249
            End
            DisplayFlags = 280
            TopColumn = 6
         End
         Begin Table = "DG_Users"
            Begin Extent = 
               Top = 0
               Left = 457
               Bottom = 117
               Right = 648
            End
            DisplayFlags = 280
            TopColumn = 6
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 12
         Width = 284
         Width = 2505
         Width = 2520
         Width = 2250
         Width = 1500
         Width = 1500
         Width = 1905
         Width = 1785
         Width = 2715
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 7095
         Alias = 1635
         Table = 1785
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vw_GetCameraDetails'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vw_GetCameraDetails'
GO
/****** Object:  View [dbo].[vw_GetBorders]    Script Date: 01/05/2013 14:44:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vw_GetBorders]
AS
SELECT     dbo.DG_Borders.DG_Border
FROM         dbo.DG_Borders CROSS JOIN
                      dbo.DG_Orders_ProductType
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "DG_Borders"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 125
               Right = 219
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "DG_Orders_ProductType"
            Begin Extent = 
               Top = 6
               Left = 257
               Bottom = 125
               Right = 548
            End
            DisplayFlags = 280
            TopColumn = 1
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vw_GetBorders'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vw_GetBorders'
GO
/****** Object:  View [dbo].[vw_GetBorderDetails]    Script Date: 01/05/2013 14:44:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vw_GetBorderDetails]
AS
SELECT     dbo.DG_Orders_ProductType.DG_Orders_ProductType_Name, dbo.DG_Borders.DG_Borders_pkey, dbo.DG_Borders.DG_Border, dbo.DG_Borders.DG_ProductTypeID, 
                      dbo.DG_Borders.DG_IsActive
FROM         dbo.DG_Borders INNER JOIN
                      dbo.DG_Orders_ProductType ON dbo.DG_Borders.DG_ProductTypeID = dbo.DG_Orders_ProductType.DG_Orders_ProductType_pkey
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[32] 4[29] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "DG_Borders"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 125
               Right = 219
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "DG_Orders_ProductType"
            Begin Extent = 
               Top = 6
               Left = 257
               Bottom = 125
               Right = 548
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vw_GetBorderDetails'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vw_GetBorderDetails'
GO
/****** Object:  View [dbo].[vw_GetActivityReports]    Script Date: 01/05/2013 14:44:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vw_GetActivityReports]
AS
SELECT dbo.DG_Actions_Type.DG_Actions_Name, dbo.DG_Activity.DG_Acitivity_Date, dbo.DG_Users.DG_User_Name, dbo.DG_Users.DG_User_First_Name, 
               dbo.DG_Users.DG_User_Last_Name, dbo.DG_Users.DG_User_First_Name + '   ' + dbo.DG_Users.DG_User_Last_Name AS Name, 
               dbo.DG_Activity.DG_Acitivity_Descrption, dbo.DG_Actions_Type.DG_Actions_pkey, dbo.DG_Users.DG_User_pkey, dbo.DG_Activity.DG_Acitivity_Action_Pkey, 
               ISNULL(CAST(dbo.DG_Activity.DG_Acitivity_Date AS varchar(250)), '') AS ActivityDate
FROM  dbo.DG_Actions_Type INNER JOIN
               dbo.DG_Activity ON dbo.DG_Actions_Type.DG_Actions_pkey = dbo.DG_Activity.DG_Acitivity_ActionType INNER JOIN
               dbo.DG_Users ON dbo.DG_Activity.DG_Acitivity_By = dbo.DG_Users.DG_User_pkey
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[24] 4[37] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "DG_Actions_Type"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 211
               Right = 215
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "DG_Activity"
            Begin Extent = 
               Top = 6
               Left = 253
               Bottom = 216
               Right = 465
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "DG_Users"
            Begin Extent = 
               Top = 6
               Left = 503
               Bottom = 216
               Right = 694
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 12
         Width = 284
         Width = 3936
         Width = 3912
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 5688
         Width = 1500
         Width = 1500
         Width = 1200
         Width = 1200
         Width = 1200
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 6000
         Alias = 2844
         Table = 1176
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1356
         SortOrder = 1416
         GroupBy = 1350
         Filter = 1356
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vw_GetActivityReports'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vw_GetActivityReports'
GO
/****** Object:  StoredProcedure [dbo].[SP_InsertDefaultData]    Script Date: 01/05/2013 14:44:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Procedure [dbo].[SP_InsertDefaultData] --'test' ,'test1'
(
	@StoreName varchar(max),
	@LocationName varchar(max)
)
as
begin

declare @StoreID int 
declare @LocationID int 

if not exists (Select * from [Digiphoto].[dbo].DG_Store)
begin
SET IDENTITY_INSERT [Digiphoto].[dbo].DG_Store ON
INSERT INTO [Digiphoto].[dbo].[DG_Store]
           (DG_Store_pkey,DG_Store_Name)
     VALUES
           (1,@StoreName)
SET IDENTITY_INSERT [Digiphoto].[dbo].DG_Store OFF
end

select @StoreID = max(DG_Store_pkey) from [Digiphoto].[dbo].DG_Store


if not exists (Select * from [Digiphoto].[dbo].DG_Location)
begin

INSERT INTO [Digiphoto].[dbo].[DG_Location]
           (DG_Location_Name,DG_Store_ID,DG_Location_PhoneNumber)
			VALUES(@LocationName,@StoreID,'')
set @LocationID =  @@identity
end
print @LocationID

if not exists (Select * from [Digiphoto].[dbo].DG_User_Roles where DG_User_Roles_Pkey = 8)
begin
SET IDENTITY_INSERT [Digiphoto].[dbo].DG_User_Roles ON
INSERT INTO [Digiphoto].[dbo].[DG_User_Roles]
           (DG_User_Roles_Pkey,DG_User_Role)
     VALUES
           (8,'PhotoGrapher')

SET IDENTITY_INSERT [Digiphoto].[dbo].DG_User_Roles OFF
end


if not exists (Select * from [Digiphoto].[dbo].DG_User_Roles where DG_User_Roles_Pkey = 7)
begin
SET IDENTITY_INSERT [Digiphoto].[dbo].DG_User_Roles ON
INSERT INTO [Digiphoto].[dbo].[DG_User_Roles]
           (DG_User_Roles_Pkey,DG_User_Role)
     VALUES
           (7,'Administrator')

SET IDENTITY_INSERT [Digiphoto].[dbo].DG_User_Roles OFF
end

if not exists (Select * from [Digiphoto].[dbo].DG_Users where DG_User_Name = 'admin')
begin

INSERT INTO [Digiphoto].[dbo].[DG_Users]
           ([DG_User_Name]
           ,[DG_User_First_Name]
           ,[DG_User_Last_Name]
           ,[DG_User_Password]
           ,[DG_User_Roles_Id]
           ,[DG_Location_ID]
           ,[DG_User_Status]
           ,[DG_User_PhoneNo]
           ,[DG_User_Email])
     VALUES
           ('admin'
           ,'Super'
           ,'User',
           'admin',7,
           @LocationID
           ,1
           ,'971551076171'
           ,'faraz.hasan@digiphotogulf.com'
           
)

truncate table [dbo].[DG_Orders_ProductType]
SET IDENTITY_INSERT [dbo].[DG_Orders_ProductType] ON
INSERT [dbo].[DG_Orders_ProductType] ([DG_Orders_ProductType_pkey], [DG_Orders_ProductType_Name], [DG_Orders_ProductType_Desc], [DG_Orders_ProductType_IsBundled], [DG_Orders_ProductType_DiscountApplied], [DG_Orders_ProductType_Image], [DG_IsPackage], [DG_MaxQuantity], [DG_Orders_ProductType_Active], [DG_IsActive], [DG_IsAccessory], [DG_IsPrimary]) VALUES (1, N'6 * 8', N'6 * 8', 0, 1, N'/images/sixbyeightbyten.png', 0, 1, 1, 1, 0, 1)
INSERT [dbo].[DG_Orders_ProductType] ([DG_Orders_ProductType_pkey], [DG_Orders_ProductType_Name], [DG_Orders_ProductType_Desc], [DG_Orders_ProductType_IsBundled], [DG_Orders_ProductType_DiscountApplied], [DG_Orders_ProductType_Image], [DG_IsPackage], [DG_MaxQuantity], [DG_Orders_ProductType_Active], [DG_IsActive], [DG_IsAccessory], [DG_IsPrimary]) VALUES (2, N'8 * 10', N'8 * 10', 0, 1, N'/images/sixbyeightbyten.png', 0, 1, 1, 1, 0, 1)
INSERT [dbo].[DG_Orders_ProductType] ([DG_Orders_ProductType_pkey], [DG_Orders_ProductType_Name], [DG_Orders_ProductType_Desc], [DG_Orders_ProductType_IsBundled], [DG_Orders_ProductType_DiscountApplied], [DG_Orders_ProductType_Image], [DG_IsPackage], [DG_MaxQuantity], [DG_Orders_ProductType_Active], [DG_IsActive], [DG_IsAccessory], [DG_IsPrimary]) VALUES (3, N'4 Large Wallets', N'4 Large Wallets', 1, 1, N'/images/wallet.png', 0, 1, 1, 1, 0, 0)
INSERT [dbo].[DG_Orders_ProductType] ([DG_Orders_ProductType_pkey], [DG_Orders_ProductType_Name], [DG_Orders_ProductType_Desc], [DG_Orders_ProductType_IsBundled], [DG_Orders_ProductType_DiscountApplied], [DG_Orders_ProductType_Image], [DG_IsPackage], [DG_MaxQuantity], [DG_Orders_ProductType_Active], [DG_IsActive], [DG_IsAccessory], [DG_IsPrimary]) VALUES (4, N'Unique Four 4 * 5', N'Unique Four 4 * 5', 1, 1, N'/images/unique4x5.png', 0, 4, 1, 1, 0, 0)
INSERT [dbo].[DG_Orders_ProductType] ([DG_Orders_ProductType_pkey], [DG_Orders_ProductType_Name], [DG_Orders_ProductType_Desc], [DG_Orders_ProductType_IsBundled], [DG_Orders_ProductType_DiscountApplied], [DG_Orders_ProductType_Image], [DG_IsPackage], [DG_MaxQuantity], [DG_Orders_ProductType_Active], [DG_IsActive], [DG_IsAccessory], [DG_IsPrimary]) VALUES (5, N'4 Small Wallets', N'4 Small Wallets', 1, 1, N'/images/unique4x5.png', 0, 1, 1, 1, 0, 0)
INSERT [dbo].[DG_Orders_ProductType] ([DG_Orders_ProductType_pkey], [DG_Orders_ProductType_Name], [DG_Orders_ProductType_Desc], [DG_Orders_ProductType_IsBundled], [DG_Orders_ProductType_DiscountApplied], [DG_Orders_ProductType_Image], [DG_IsPackage], [DG_MaxQuantity], [DG_Orders_ProductType_Active], [DG_IsActive], [DG_IsAccessory], [DG_IsPrimary]) VALUES (19, N'Key Chain', N'Key Chain', 1, 1, N'/images/jpgfloppy.png', 0, 0, 1, 1, 0, 0)
INSERT [dbo].[DG_Orders_ProductType] ([DG_Orders_ProductType_pkey], [DG_Orders_ProductType_Name], [DG_Orders_ProductType_Desc], [DG_Orders_ProductType_IsBundled], [DG_Orders_ProductType_DiscountApplied], [DG_Orders_ProductType_Image], [DG_IsPackage], [DG_MaxQuantity], [DG_Orders_ProductType_Active], [DG_IsActive], [DG_IsAccessory], [DG_IsPrimary]) VALUES (30, N'4 * 6', N'Product having height 6 and width 4', 0, 0, N'/images/jpgfloppy.png', 0, 0, 1, 1, 0, 1)
INSERT [dbo].[DG_Orders_ProductType] ([DG_Orders_ProductType_pkey], [DG_Orders_ProductType_Name], [DG_Orders_ProductType_Desc], [DG_Orders_ProductType_IsBundled], [DG_Orders_ProductType_DiscountApplied], [DG_Orders_ProductType_Image], [DG_IsPackage], [DG_MaxQuantity], [DG_Orders_ProductType_Active], [DG_IsActive], [DG_IsAccessory], [DG_IsPrimary]) VALUES (31, N'Mug', N'Mug with Image', 0, 0, N'/images/jpgfloppy.png', 0, 0, 1, 1, 1, 0)
INSERT [dbo].[DG_Orders_ProductType] ([DG_Orders_ProductType_pkey], [DG_Orders_ProductType_Name], [DG_Orders_ProductType_Desc], [DG_Orders_ProductType_IsBundled], [DG_Orders_ProductType_DiscountApplied], [DG_Orders_ProductType_Image], [DG_IsPackage], [DG_MaxQuantity], [DG_Orders_ProductType_Active], [DG_IsActive], [DG_IsAccessory], [DG_IsPrimary]) VALUES (32, N'Mug * by 10', N'Package contains MUG and 8 by 10 image', 0, 0, N'/images/jpgfloppy.png', 1, 0, 1, 1, 0, 0)
INSERT [dbo].[DG_Orders_ProductType] ([DG_Orders_ProductType_pkey], [DG_Orders_ProductType_Name], [DG_Orders_ProductType_Desc], [DG_Orders_ProductType_IsBundled], [DG_Orders_ProductType_DiscountApplied], [DG_Orders_ProductType_Image], [DG_IsPackage], [DG_MaxQuantity], [DG_Orders_ProductType_Active], [DG_IsActive], [DG_IsAccessory], [DG_IsPrimary]) VALUES (33, N'Snow Globe', N'Snow Globe', 1, 0, N'/images/jpgfloppy.png', 0, 0, 1, 1, 1, 0)
INSERT [dbo].[DG_Orders_ProductType] ([DG_Orders_ProductType_pkey], [DG_Orders_ProductType_Name], [DG_Orders_ProductType_Desc], [DG_Orders_ProductType_IsBundled], [DG_Orders_ProductType_DiscountApplied], [DG_Orders_ProductType_Image], [DG_IsPackage], [DG_MaxQuantity], [DG_Orders_ProductType_Active], [DG_IsActive], [DG_IsAccessory], [DG_IsPrimary]) VALUES (35, N'Acessory CD', N'CD', 0, 0, N'/images/jpgfloppy.png', 0, 0, 1, 1, 1, 0)
INSERT [dbo].[DG_Orders_ProductType] ([DG_Orders_ProductType_pkey], [DG_Orders_ProductType_Name], [DG_Orders_ProductType_Desc], [DG_Orders_ProductType_IsBundled], [DG_Orders_ProductType_DiscountApplied], [DG_Orders_ProductType_Image], [DG_IsPackage], [DG_MaxQuantity], [DG_Orders_ProductType_Active], [DG_IsActive], [DG_IsAccessory], [DG_IsPrimary]) VALUES (36, N'Acessory USB', N'USB', 0, 0, N'/images/jpgfloppy.png', 0, 0, 1, 1, 1, 0)
INSERT [dbo].[DG_Orders_ProductType] ([DG_Orders_ProductType_pkey], [DG_Orders_ProductType_Name], [DG_Orders_ProductType_Desc], [DG_Orders_ProductType_IsBundled], [DG_Orders_ProductType_DiscountApplied], [DG_Orders_ProductType_Image], [DG_IsPackage], [DG_MaxQuantity], [DG_Orders_ProductType_Active], [DG_IsActive], [DG_IsAccessory], [DG_IsPrimary]) VALUES (37, N'test', N'Usb With 10 photos and 01 8 * 10 photo', 0, 0, N'/images/jpgfloppy.png', 1, 0, NULL, 1, 0, 0)
INSERT [dbo].[DG_Orders_ProductType] ([DG_Orders_ProductType_pkey], [DG_Orders_ProductType_Name], [DG_Orders_ProductType_Desc], [DG_Orders_ProductType_IsBundled], [DG_Orders_ProductType_DiscountApplied], [DG_Orders_ProductType_Image], [DG_IsPackage], [DG_MaxQuantity], [DG_Orders_ProductType_Active], [DG_IsActive], [DG_IsAccessory], [DG_IsPrimary]) VALUES (38, N'USB Package Large', N'One 8 * 10 image and 15 Photo in usb', 0, 0, N'/images/jpgfloppy.png', 1, 0, NULL, 1, 0, 0)
INSERT [dbo].[DG_Orders_ProductType] ([DG_Orders_ProductType_pkey], [DG_Orders_ProductType_Name], [DG_Orders_ProductType_Desc], [DG_Orders_ProductType_IsBundled], [DG_Orders_ProductType_DiscountApplied], [DG_Orders_ProductType_Image], [DG_IsPackage], [DG_MaxQuantity], [DG_Orders_ProductType_Active], [DG_IsActive], [DG_IsAccessory], [DG_IsPrimary]) VALUES (39, N'Dec Prom', N'Dec promotion', 0, 0, N'/images/jpgfloppy.png', 1, 0, NULL, 1, 0, 0)
SET IDENTITY_INSERT [dbo].[DG_Orders_ProductType] OFF

SET IDENTITY_INSERT [dbo].[DG_Product_Pricing] ON
INSERT [dbo].[DG_Product_Pricing] ([DG_Product_Pricing_Pkey], [DG_Product_Pricing_ProductType], [DG_Product_Pricing_ProductPrice], [DG_Product_Pricing_Currency_ID], [DG_Product_Pricing_UpdateDate], [DG_Product_Pricing_CreatedBy], [DG_Product_Pricing_StoreId], [DG_Product_Pricing_IsAvaliable]) VALUES (40, 30, 27, 1, CAST(0x0000A10F011DFAB1 AS DateTime), 6, @StoreID, 1)
INSERT [dbo].[DG_Product_Pricing] ([DG_Product_Pricing_Pkey], [DG_Product_Pricing_ProductType], [DG_Product_Pricing_ProductPrice], [DG_Product_Pricing_Currency_ID], [DG_Product_Pricing_UpdateDate], [DG_Product_Pricing_CreatedBy], [DG_Product_Pricing_StoreId], [DG_Product_Pricing_IsAvaliable]) VALUES (41, 31, 25, 1, CAST(0x0000A11300C32265 AS DateTime), 6, @StoreID, 1)
INSERT [dbo].[DG_Product_Pricing] ([DG_Product_Pricing_Pkey], [DG_Product_Pricing_ProductType], [DG_Product_Pricing_ProductPrice], [DG_Product_Pricing_Currency_ID], [DG_Product_Pricing_UpdateDate], [DG_Product_Pricing_CreatedBy], [DG_Product_Pricing_StoreId], [DG_Product_Pricing_IsAvaliable]) VALUES (42, 32, 125, 1, CAST(0x0000A11300C35B49 AS DateTime), 6, @StoreID, 1)
INSERT [dbo].[DG_Product_Pricing] ([DG_Product_Pricing_Pkey], [DG_Product_Pricing_ProductType], [DG_Product_Pricing_ProductPrice], [DG_Product_Pricing_Currency_ID], [DG_Product_Pricing_UpdateDate], [DG_Product_Pricing_CreatedBy], [DG_Product_Pricing_StoreId], [DG_Product_Pricing_IsAvaliable]) VALUES (43, 33, 55, 1, CAST(0x0000A11300C81F89 AS DateTime), 6, @StoreID, 1)
INSERT [dbo].[DG_Product_Pricing] ([DG_Product_Pricing_Pkey], [DG_Product_Pricing_ProductType], [DG_Product_Pricing_ProductPrice], [DG_Product_Pricing_Currency_ID], [DG_Product_Pricing_UpdateDate], [DG_Product_Pricing_CreatedBy], [DG_Product_Pricing_StoreId], [DG_Product_Pricing_IsAvaliable]) VALUES (44, 34, 110, 1, CAST(0x0000A11300C8650B AS DateTime), 6,@StoreID, 1)
INSERT [dbo].[DG_Product_Pricing] ([DG_Product_Pricing_Pkey], [DG_Product_Pricing_ProductType], [DG_Product_Pricing_ProductPrice], [DG_Product_Pricing_Currency_ID], [DG_Product_Pricing_UpdateDate], [DG_Product_Pricing_CreatedBy], [DG_Product_Pricing_StoreId], [DG_Product_Pricing_IsAvaliable]) VALUES (45, 35, 100, 1, CAST(0x0000A11800ED4AD6 AS DateTime), 6, @StoreID, 1)
INSERT [dbo].[DG_Product_Pricing] ([DG_Product_Pricing_Pkey], [DG_Product_Pricing_ProductType], [DG_Product_Pricing_ProductPrice], [DG_Product_Pricing_Currency_ID], [DG_Product_Pricing_UpdateDate], [DG_Product_Pricing_CreatedBy], [DG_Product_Pricing_StoreId], [DG_Product_Pricing_IsAvaliable]) VALUES (46, 36, 100, 1, CAST(0x0000A11800ED78DC AS DateTime), 6, @StoreID, 1)
INSERT [dbo].[DG_Product_Pricing] ([DG_Product_Pricing_Pkey], [DG_Product_Pricing_ProductType], [DG_Product_Pricing_ProductPrice], [DG_Product_Pricing_Currency_ID], [DG_Product_Pricing_UpdateDate], [DG_Product_Pricing_CreatedBy], [DG_Product_Pricing_StoreId], [DG_Product_Pricing_IsAvaliable]) VALUES (48, 38, 225, 1, CAST(0x0000A11C0121606A AS DateTime), 6, @StoreID, 1)
INSERT [dbo].[DG_Product_Pricing] ([DG_Product_Pricing_Pkey], [DG_Product_Pricing_ProductType], [DG_Product_Pricing_ProductPrice], [DG_Product_Pricing_Currency_ID], [DG_Product_Pricing_UpdateDate], [DG_Product_Pricing_CreatedBy], [DG_Product_Pricing_StoreId], [DG_Product_Pricing_IsAvaliable]) VALUES (49, 39, 200, 1, CAST(0x0000A11E00B6FAEC AS DateTime), 6, @StoreID, 1)
INSERT [dbo].[DG_Product_Pricing] ([DG_Product_Pricing_Pkey], [DG_Product_Pricing_ProductType], [DG_Product_Pricing_ProductPrice], [DG_Product_Pricing_Currency_ID], [DG_Product_Pricing_UpdateDate], [DG_Product_Pricing_CreatedBy], [DG_Product_Pricing_StoreId], [DG_Product_Pricing_IsAvaliable]) VALUES (50, 1, 75, 1, CAST(0x0000A11C0121606A AS DateTime), 6, @StoreID, 1)
INSERT [dbo].[DG_Product_Pricing] ([DG_Product_Pricing_Pkey], [DG_Product_Pricing_ProductType], [DG_Product_Pricing_ProductPrice], [DG_Product_Pricing_Currency_ID], [DG_Product_Pricing_UpdateDate], [DG_Product_Pricing_CreatedBy], [DG_Product_Pricing_StoreId], [DG_Product_Pricing_IsAvaliable]) VALUES (51, 2, 100, 1, CAST(0x0000A11E00B6FAEC AS DateTime), 6, @StoreID, 1)
INSERT [dbo].[DG_Product_Pricing] ([DG_Product_Pricing_Pkey], [DG_Product_Pricing_ProductType], [DG_Product_Pricing_ProductPrice], [DG_Product_Pricing_Currency_ID], [DG_Product_Pricing_UpdateDate], [DG_Product_Pricing_CreatedBy], [DG_Product_Pricing_StoreId], [DG_Product_Pricing_IsAvaliable]) VALUES (52, 37, 100, 1, CAST(0x0000A11E00B6FAEC AS DateTime), 6, @StoreID, 1)
INSERT [dbo].[DG_Product_Pricing] ([DG_Product_Pricing_Pkey], [DG_Product_Pricing_ProductType], [DG_Product_Pricing_ProductPrice], [DG_Product_Pricing_Currency_ID], [DG_Product_Pricing_UpdateDate], [DG_Product_Pricing_CreatedBy], [DG_Product_Pricing_StoreId], [DG_Product_Pricing_IsAvaliable]) VALUES (53, 19, 45, 1, CAST(0x0000A11E00B6FAEC AS DateTime), 6, @StoreID, 1)
INSERT [dbo].[DG_Product_Pricing] ([DG_Product_Pricing_Pkey], [DG_Product_Pricing_ProductType], [DG_Product_Pricing_ProductPrice], [DG_Product_Pricing_Currency_ID], [DG_Product_Pricing_UpdateDate], [DG_Product_Pricing_CreatedBy], [DG_Product_Pricing_StoreId], [DG_Product_Pricing_IsAvaliable]) VALUES (54, 5, 30, 1, CAST(0x0000A11E00B6FAEC AS DateTime), 6, @StoreID, 1)
INSERT [dbo].[DG_Product_Pricing] ([DG_Product_Pricing_Pkey], [DG_Product_Pricing_ProductType], [DG_Product_Pricing_ProductPrice], [DG_Product_Pricing_Currency_ID], [DG_Product_Pricing_UpdateDate], [DG_Product_Pricing_CreatedBy], [DG_Product_Pricing_StoreId], [DG_Product_Pricing_IsAvaliable]) VALUES (55, 4, 50, 1, CAST(0x0000A11E00B6FAEC AS DateTime), 6, @StoreID, 1)
INSERT [dbo].[DG_Product_Pricing] ([DG_Product_Pricing_Pkey], [DG_Product_Pricing_ProductType], [DG_Product_Pricing_ProductPrice], [DG_Product_Pricing_Currency_ID], [DG_Product_Pricing_UpdateDate], [DG_Product_Pricing_CreatedBy], [DG_Product_Pricing_StoreId], [DG_Product_Pricing_IsAvaliable]) VALUES (56, 3, 60, 1, CAST(0x0000A11E00B6FAEC AS DateTime), 6, @StoreID, 1)
SET IDENTITY_INSERT [dbo].[DG_Product_Pricing] OFF
/****** Object:  Table [dbo].[DG_Currency]    Script Date: 12/19/2012 15:06:33 ******/
SET IDENTITY_INSERT [dbo].[DG_Currency] ON
INSERT [dbo].[DG_Currency] ([DG_Currency_pkey], [DG_Currency_Name], [DG_Currency_Rate], [DG_Currency_Symbol], [DG_Currency_UpdatedDate], [DG_Currency_ModifiedBy], [DG_Currency_Default], [DG_Currency_Icon]) VALUES (2, N'EUR-Euro', 45, N'EUR', CAST(0x0000A0AF00DA70E1 AS DateTime), 4, 0, N'\images\euro.png')
INSERT [dbo].[DG_Currency] ([DG_Currency_pkey], [DG_Currency_Name], [DG_Currency_Rate], [DG_Currency_Symbol], [DG_Currency_UpdatedDate], [DG_Currency_ModifiedBy], [DG_Currency_Default], [DG_Currency_Icon]) VALUES (3, N'Dhiram', 1, N'AED', CAST(0x0000A0AF00DA70E1 AS DateTime), 4, 1, N'\images\euro.png')
INSERT [dbo].[DG_Currency] ([DG_Currency_pkey], [DG_Currency_Name], [DG_Currency_Rate], [DG_Currency_Symbol], [DG_Currency_UpdatedDate], [DG_Currency_ModifiedBy], [DG_Currency_Default], [DG_Currency_Icon]) VALUES (27, N'Indian Rupee', 15, N'Rs', CAST(0x0000A0AF00DA70E1 AS DateTime), 4, 0, N'\images\euro.png')
SET IDENTITY_INSERT [dbo].[DG_Currency] OFF


/****** Object:  Table [dbo].[DG_Permissions]    Script Date: 12/31/2012 17:55:43 ******/
SET IDENTITY_INSERT [dbo].[DG_Permissions] ON
INSERT [dbo].[DG_Permissions] ([DG_Permission_pkey], [DG_Permission_Name]) VALUES (1, N'Manage Product Pricing')
INSERT [dbo].[DG_Permissions] ([DG_Permission_pkey], [DG_Permission_Name]) VALUES (2, N'Manage Semi Order')
INSERT [dbo].[DG_Permissions] ([DG_Permission_pkey], [DG_Permission_Name]) VALUES (4, N'Add/Edit User')
INSERT [dbo].[DG_Permissions] ([DG_Permission_pkey], [DG_Permission_Name]) VALUES (5, N'Manage Folders ')
INSERT [dbo].[DG_Permissions] ([DG_Permission_pkey], [DG_Permission_Name]) VALUES (6, N'Change Printer Setting')
INSERT [dbo].[DG_Permissions] ([DG_Permission_pkey], [DG_Permission_Name]) VALUES (7, N'Manage Border Setting')
INSERT [dbo].[DG_Permissions] ([DG_Permission_pkey], [DG_Permission_Name]) VALUES (8, N'Manage Background Setting')
INSERT [dbo].[DG_Permissions] ([DG_Permission_pkey], [DG_Permission_Name]) VALUES (9, N'Manage Currency')
INSERT [dbo].[DG_Permissions] ([DG_Permission_pkey], [DG_Permission_Name]) VALUES (10, N'View Reports')
INSERT [dbo].[DG_Permissions] ([DG_Permission_pkey], [DG_Permission_Name]) VALUES (11, N'Unlock System')
INSERT [dbo].[DG_Permissions] ([DG_Permission_pkey], [DG_Permission_Name]) VALUES (12, N'Manage Camera')
INSERT [dbo].[DG_Permissions] ([DG_Permission_pkey], [DG_Permission_Name]) VALUES (13, N'Manage Graphics')
INSERT [dbo].[DG_Permissions] ([DG_Permission_pkey], [DG_Permission_Name]) VALUES (14, N'Manage Discount')
INSERT [dbo].[DG_Permissions] ([DG_Permission_pkey], [DG_Permission_Name]) VALUES (15, N'Reprinting Queue')
INSERT [dbo].[DG_Permissions] ([DG_Permission_pkey], [DG_Permission_Name]) VALUES (16, N'Refund Order')
INSERT [dbo].[DG_Permissions] ([DG_Permission_pkey], [DG_Permission_Name]) VALUES (17, N'Archive Photos')
INSERT [dbo].[DG_Permissions] ([DG_Permission_pkey], [DG_Permission_Name]) VALUES (18, N'Manage Locations')

SET IDENTITY_INSERT [dbo].[DG_Permissions] OFF

SET IDENTITY_INSERT [dbo].[DG_Permission_Role] ON
INSERT [dbo].[DG_Permission_Role] ([DG_Permission_Role_pkey], [DG_User_Roles_Id], [DG_Permission_Id]) VALUES (108, 7, 1)
INSERT [dbo].[DG_Permission_Role] ([DG_Permission_Role_pkey], [DG_User_Roles_Id], [DG_Permission_Id]) VALUES (109, 7, 2)
INSERT [dbo].[DG_Permission_Role] ([DG_Permission_Role_pkey], [DG_User_Roles_Id], [DG_Permission_Id]) VALUES (111, 7, 4)
INSERT [dbo].[DG_Permission_Role] ([DG_Permission_Role_pkey], [DG_User_Roles_Id], [DG_Permission_Id]) VALUES (113, 7, 6)
INSERT [dbo].[DG_Permission_Role] ([DG_Permission_Role_pkey], [DG_User_Roles_Id], [DG_Permission_Id]) VALUES (114, 7, 9)
INSERT [dbo].[DG_Permission_Role] ([DG_Permission_Role_pkey], [DG_User_Roles_Id], [DG_Permission_Id]) VALUES (115, 7, 12)
INSERT [dbo].[DG_Permission_Role] ([DG_Permission_Role_pkey], [DG_User_Roles_Id], [DG_Permission_Id]) VALUES (116, 7, 8)
INSERT [dbo].[DG_Permission_Role] ([DG_Permission_Role_pkey], [DG_User_Roles_Id], [DG_Permission_Id]) VALUES (117, 7, 10)
INSERT [dbo].[DG_Permission_Role] ([DG_Permission_Role_pkey], [DG_User_Roles_Id], [DG_Permission_Id]) VALUES (118, 7, 7)
INSERT [dbo].[DG_Permission_Role] ([DG_Permission_Role_pkey], [DG_User_Roles_Id], [DG_Permission_Id]) VALUES (119, 7, 5)
INSERT [dbo].[DG_Permission_Role] ([DG_Permission_Role_pkey], [DG_User_Roles_Id], [DG_Permission_Id]) VALUES (121, 7, 13)
INSERT [dbo].[DG_Permission_Role] ([DG_Permission_Role_pkey], [DG_User_Roles_Id], [DG_Permission_Id]) VALUES (123, 7, 14)
INSERT [dbo].[DG_Permission_Role] ([DG_Permission_Role_pkey], [DG_User_Roles_Id], [DG_Permission_Id]) VALUES (127, 7, 11)
INSERT [dbo].[DG_Permission_Role] ([DG_Permission_Role_pkey], [DG_User_Roles_Id], [DG_Permission_Id]) VALUES (129, 7, 16)
INSERT [dbo].[DG_Permission_Role] ([DG_Permission_Role_pkey], [DG_User_Roles_Id], [DG_Permission_Id]) VALUES (130, 7, 15)
INSERT [dbo].[DG_Permission_Role] ([DG_Permission_Role_pkey], [DG_User_Roles_Id], [DG_Permission_Id]) VALUES (131, 7, 17)

SET IDENTITY_INSERT [dbo].[DG_Permission_Role] OFF
/****** Object:  Table [dbo].[DG_Actions_Type]    Script Date: 12/31/2012 17:55:43 ******/
SET IDENTITY_INSERT [dbo].[DG_Actions_Type] ON
INSERT [dbo].[DG_Actions_Type] ([DG_Actions_pkey], [DG_Actions_Name]) VALUES (1, N'Preview Photo')
INSERT [dbo].[DG_Actions_Type] ([DG_Actions_pkey], [DG_Actions_Name]) VALUES (2, N'Preview Photo(High Resolution)')
INSERT [dbo].[DG_Actions_Type] ([DG_Actions_pkey], [DG_Actions_Name]) VALUES (3, N'Moderate Photo')
INSERT [dbo].[DG_Actions_Type] ([DG_Actions_pkey], [DG_Actions_Name]) VALUES (4, N'Edit Photo')
INSERT [dbo].[DG_Actions_Type] ([DG_Actions_pkey], [DG_Actions_Name]) VALUES (5, N'Unlock Photo')
INSERT [dbo].[DG_Actions_Type] ([DG_Actions_pkey], [DG_Actions_Name]) VALUES (6, N'AddCurrency')
INSERT [dbo].[DG_Actions_Type] ([DG_Actions_pkey], [DG_Actions_Name]) VALUES (7, N'DeleteCurrency')
INSERT [dbo].[DG_Actions_Type] ([DG_Actions_pkey], [DG_Actions_Name]) VALUES (8, N'EditCurrency')
INSERT [dbo].[DG_Actions_Type] ([DG_Actions_pkey], [DG_Actions_Name]) VALUES (9, N'ChangeCurrencyRate')
INSERT [dbo].[DG_Actions_Type] ([DG_Actions_pkey], [DG_Actions_Name]) VALUES (10, N'ChangeDefaultCurrency')
INSERT [dbo].[DG_Actions_Type] ([DG_Actions_pkey], [DG_Actions_Name]) VALUES (11, N'AddBorder')
INSERT [dbo].[DG_Actions_Type] ([DG_Actions_pkey], [DG_Actions_Name]) VALUES (12, N'RemoveBorder')
INSERT [dbo].[DG_Actions_Type] ([DG_Actions_pkey], [DG_Actions_Name]) VALUES (13, N'AddBackGround')
INSERT [dbo].[DG_Actions_Type] ([DG_Actions_pkey], [DG_Actions_Name]) VALUES (14, N'RemoveBackGround')
INSERT [dbo].[DG_Actions_Type] ([DG_Actions_pkey], [DG_Actions_Name]) VALUES (15, N'AddPrinter')
INSERT [dbo].[DG_Actions_Type] ([DG_Actions_pkey], [DG_Actions_Name]) VALUES (16, N'ChangePrinter')
INSERT [dbo].[DG_Actions_Type] ([DG_Actions_pkey], [DG_Actions_Name]) VALUES (17, N'DeletePrinter')
INSERT [dbo].[DG_Actions_Type] ([DG_Actions_pkey], [DG_Actions_Name]) VALUES (18, N'AddUser')
INSERT [dbo].[DG_Actions_Type] ([DG_Actions_pkey], [DG_Actions_Name]) VALUES (19, N'EditUser')
INSERT [dbo].[DG_Actions_Type] ([DG_Actions_pkey], [DG_Actions_Name]) VALUES (20, N'DeleteUser')
INSERT [dbo].[DG_Actions_Type] ([DG_Actions_pkey], [DG_Actions_Name]) VALUES (21, N'ChangeUserRole')
INSERT [dbo].[DG_Actions_Type] ([DG_Actions_pkey], [DG_Actions_Name]) VALUES (22, N'AddRole')
INSERT [dbo].[DG_Actions_Type] ([DG_Actions_pkey], [DG_Actions_Name]) VALUES (23, N'DeleteRole')
INSERT [dbo].[DG_Actions_Type] ([DG_Actions_pkey], [DG_Actions_Name]) VALUES (24, N'AddPermission')
INSERT [dbo].[DG_Actions_Type] ([DG_Actions_pkey], [DG_Actions_Name]) VALUES (25, N'DeletePermission')
INSERT [dbo].[DG_Actions_Type] ([DG_Actions_pkey], [DG_Actions_Name]) VALUES (26, N'AddProduct')
INSERT [dbo].[DG_Actions_Type] ([DG_Actions_pkey], [DG_Actions_Name]) VALUES (27, N'DeleteProduct')
INSERT [dbo].[DG_Actions_Type] ([DG_Actions_pkey], [DG_Actions_Name]) VALUES (28, N'EditProduct')
INSERT [dbo].[DG_Actions_Type] ([DG_Actions_pkey], [DG_Actions_Name]) VALUES (29, N'CreatePackage')
INSERT [dbo].[DG_Actions_Type] ([DG_Actions_pkey], [DG_Actions_Name]) VALUES (30, N'ChangePackage')
INSERT [dbo].[DG_Actions_Type] ([DG_Actions_pkey], [DG_Actions_Name]) VALUES (31, N'DeletePackage')
INSERT [dbo].[DG_Actions_Type] ([DG_Actions_pkey], [DG_Actions_Name]) VALUES (32, N'ChangePricing')
INSERT [dbo].[DG_Actions_Type] ([DG_Actions_pkey], [DG_Actions_Name]) VALUES (33, N'ChangeModeratePassword')
INSERT [dbo].[DG_Actions_Type] ([DG_Actions_pkey], [DG_Actions_Name]) VALUES (34, N'AllowPreview300')
INSERT [dbo].[DG_Actions_Type] ([DG_Actions_pkey], [DG_Actions_Name]) VALUES (35, N'DisablePreview300')
INSERT [dbo].[DG_Actions_Type] ([DG_Actions_pkey], [DG_Actions_Name]) VALUES (36, N'AddPrinter')
INSERT [dbo].[DG_Actions_Type] ([DG_Actions_pkey], [DG_Actions_Name]) VALUES (37, N'DeletePrinter')
INSERT [dbo].[DG_Actions_Type] ([DG_Actions_pkey], [DG_Actions_Name]) VALUES (38, N'Login')
INSERT [dbo].[DG_Actions_Type] ([DG_Actions_pkey], [DG_Actions_Name]) VALUES (39, N'Logout')
INSERT [dbo].[DG_Actions_Type] ([DG_Actions_pkey], [DG_Actions_Name]) VALUES (40, N'RefundOrder')
INSERT [dbo].[DG_Actions_Type] ([DG_Actions_pkey], [DG_Actions_Name]) VALUES (41, N'ShutDown')
INSERT [dbo].[DG_Actions_Type] ([DG_Actions_pkey], [DG_Actions_Name]) VALUES (42, N'Generate Order')
SET IDENTITY_INSERT [dbo].[DG_Actions_Type] OFF
INSERT INTO [dbo].[DG_Bill_Format]([DG_Bill_Type],[DG_Refund_Slogan]) VALUES(0,'No Refunds!')
end
end
GO
/****** Object:  View [dbo].[vw_productsales]    Script Date: 01/05/2013 14:44:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vw_productsales] AS
SELECT     dbo.DG_Orders_ProductType.DG_Orders_ProductType_Name, SUM(dbo.DG_Orders_Details.DG_Orders_LineItems_Quantity) AS total_quantity
FROM         dbo.DG_Orders_ProductType INNER JOIN
                      dbo.DG_Orders_Details ON dbo.DG_Orders_ProductType.DG_Orders_ProductType_pkey = dbo.DG_Orders_Details.DG_Orders_Details_ProductType_pkey
GROUP BY dbo.DG_Orders_ProductType.DG_Orders_ProductType_Name
GO
/****** Object:  View [dbo].[vw_ProductPrinters]    Script Date: 01/05/2013 14:44:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vw_ProductPrinters]
AS
SELECT dbo.DG_Printers.DG_Printers_Pkey, dbo.DG_Printers.DG_Printers_NickName, dbo.DG_Printers.DG_Printers_Address, 
               dbo.DG_Printers.DG_Printers_DefaultName, dbo.DG_AssociatedPrinters.DG_AssociatedPrinters_ProductType_ID
FROM  dbo.DG_Printers INNER JOIN
               dbo.DG_AssociatedPrinters ON dbo.DG_Printers.DG_Printers_Pkey = dbo.DG_AssociatedPrinters.DG_AssociatedPrinters_Pkey
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[49] 4[4] 2[22] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "DG_Printers"
            Begin Extent = 
               Top = 9
               Left = 670
               Bottom = 251
               Right = 912
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "DG_AssociatedPrinters"
            Begin Extent = 
               Top = 32
               Left = 99
               Bottom = 363
               Right = 419
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1200
         Width = 1200
         Width = 1200
         Width = 1200
         Width = 3888
         Width = 1200
         Width = 1200
         Width = 1200
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vw_ProductPrinters'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vw_ProductPrinters'
GO
/****** Object:  View [dbo].[vw_OrderBurnedItems]    Script Date: 01/05/2013 14:44:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vw_OrderBurnedItems]
AS
SELECT     dbo.DG_Orders_ProductType.DG_Orders_ProductType_Name, dbo.DG_Orders_ProductType.DG_Orders_ProductType_pkey, 
                      dbo.DG_Orders_ProductType.DG_IsAccessory, dbo.DG_Orders_Details.DG_Photos_ID, dbo.DG_Orders_Details.DG_Orders_LineItems_pkey, 
                      dbo.DG_Orders_Details.DG_Photos_Burned, dbo.DG_Orders_Details.DG_Orders_ID
FROM         dbo.DG_Orders_ProductType INNER JOIN
                      dbo.DG_Orders_Details ON dbo.DG_Orders_ProductType.DG_Orders_ProductType_pkey = dbo.DG_Orders_Details.DG_Orders_Details_ProductType_pkey
WHERE     (dbo.DG_Orders_ProductType.DG_Orders_ProductType_pkey = 35 OR
                      dbo.DG_Orders_ProductType.DG_Orders_ProductType_pkey = 36) AND (dbo.DG_Orders_Details.DG_Photos_Burned = 1)
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[18] 4[13] 2[41] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1[50] 2[25] 3) )"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2[66] 3) )"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = -50
      End
      Begin Tables = 
         Begin Table = "DG_Orders_ProductType"
            Begin Extent = 
               Top = 6
               Left = 612
               Bottom = 324
               Right = 903
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "DG_Orders_Details"
            Begin Extent = 
               Top = 6
               Left = 88
               Bottom = 342
               Right = 414
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 14
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 6870
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 4845
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vw_OrderBurnedItems'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vw_OrderBurnedItems'
GO
/****** Object:  View [dbo].[vw_GetUserDetails]    Script Date: 01/05/2013 14:44:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vw_GetUserDetails]    
AS    
SELECT     DG_Users.DG_User_pkey, DG_User_Roles.DG_User_Role, DG_Users.DG_User_First_Name, DG_Users.DG_User_Last_Name, DG_Users.DG_User_Password, 
                      DG_Users.DG_User_Name, DG_Users.DG_User_Roles_Id, DG_Location.DG_Location_Name, DG_Location.DG_Store_ID, DG_Location.DG_Location_pkey, 
                      DG_Users.DG_User_Status, DG_Users.DG_User_PhoneNo, DG_Users.DG_User_First_Name + RTRIM(' ' + DG_Users.DG_User_Last_Name) AS 'UserName', 
                      CASE dbo.DG_Users.DG_User_Status WHEN 1 THEN 'Active' ELSE 'InActive' END AS 'StatusName', DG_Users.DG_User_Email
FROM         DG_Users INNER JOIN
                      DG_User_Roles ON DG_Users.DG_User_Roles_Id = DG_User_Roles.DG_User_Roles_pkey INNER JOIN
                      DG_Location ON DG_Users.DG_Location_ID = DG_Location.DG_Location_pkey INNER JOIN
                      DG_Store ON DG_Location.DG_Store_ID = DG_Store.DG_Store_pkey
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[43] 4[5] 2[22] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "DG_Users"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 216
               Right = 229
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "DG_User_Roles"
            Begin Extent = 
               Top = 114
               Left = 565
               Bottom = 203
               Right = 757
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "DG_Location"
            Begin Extent = 
               Top = 0
               Left = 262
               Bottom = 104
               Right = 444
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "DG_Store"
            Begin Extent = 
               Top = 11
               Left = 771
               Bottom = 100
               Right = 939
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 11
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1710
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vw_GetUserDetails'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane2', @value=N'350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vw_GetUserDetails'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=2 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vw_GetUserDetails'
GO
/****** Object:  View [dbo].[vw_GetRecordsForArchive]    Script Date: 01/05/2013 14:44:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vw_GetRecordsForArchive]
AS
SELECT     DG_Photos_pkey, DG_Photos_FileName, DG_Photos_CreatedOn, DG_Photos_Archive
FROM         dbo.DG_Photos
WHERE     (DG_Photos_CreatedOn < DATEADD(day, - 7, GETDATE())) AND (DG_Photos_Archive IS NULL)
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[33] 4[20] 2[14] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "DG_Photos"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 289
               Right = 263
            End
            DisplayFlags = 280
            TopColumn = 1
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1500
         Width = 1500
         Width = 2385
         Width = 2325
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vw_GetRecordsForArchive'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vw_GetRecordsForArchive'
GO
/****** Object:  View [dbo].[vw_GetProductTypeData]    Script Date: 01/05/2013 14:44:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vw_GetProductTypeData]  
AS  
SELECT     DG_Orders_ProductType.DG_Orders_ProductType_Name, DG_Orders_ProductType.DG_Orders_ProductType_Desc, 
                      DG_Orders_ProductType.DG_Orders_ProductType_DiscountApplied, DG_Orders_ProductType.DG_Orders_ProductType_IsBundled, 
                      DG_Orders_ProductType.DG_IsPackage, DG_Orders_ProductType.DG_MaxQuantity, DG_Product_Pricing.DG_Product_Pricing_ProductPrice, 
                      DG_Product_Pricing.DG_Product_Pricing_Currency_ID, DG_Orders_ProductType.DG_Orders_ProductType_pkey, DG_Orders_ProductType.DG_IsActive, 
                      DG_Orders_ProductType.DG_IsAccessory
FROM         DG_Orders_ProductType INNER JOIN
                      DG_Product_Pricing ON DG_Orders_ProductType.DG_Orders_ProductType_pkey = DG_Product_Pricing.DG_Product_Pricing_ProductType
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "DG_Orders_ProductType"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 125
               Right = 329
            End
            DisplayFlags = 280
            TopColumn = 6
         End
         Begin Table = "DG_Product_Pricing"
            Begin Extent = 
               Top = 126
               Left = 38
               Bottom = 245
               Right = 288
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 11
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vw_GetProductTypeData'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vw_GetProductTypeData'
GO
/****** Object:  StoredProcedure [dbo].[DG_GetPhotoNumberCamerawise]    Script Date: 01/05/2013 14:44:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Proc [dbo].[DG_GetPhotoNumberCamerawise]
@CameraId int
AS
SELECT    DG_Camera_Start_Series
FROM         DG_CameraDetails where DG_Camera_pkey=@CameraId
GO
/****** Object:  StoredProcedure [dbo].[DG_GetConfigData]    Script Date: 01/05/2013 14:44:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[DG_GetConfigData]
AS
SELECT     DG_Hot_Folder_Path
FROM         DG_Configuration
GO
/****** Object:  StoredProcedure [dbo].[DG_GetCameraDetails]    Script Date: 01/05/2013 14:44:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Proc [dbo].[DG_GetCameraDetails]  
AS  
SELECT     DG_Camera_pkey, DG_Camera_Name, DG_Camera_Make, DG_Camera_Model, DG_AssignTo, DG_Camera_Start_Series, DG_Updatedby, DG_UpdatedDate  
FROM         DG_CameraDetails
where DG_Camera_IsDeleted = 0
GO
/****** Object:  StoredProcedure [dbo].[DG_SetPhotoNumberCamerawise]    Script Date: 01/05/2013 14:44:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Proc [dbo].[DG_SetPhotoNumberCamerawise]
@CameraId int,
@DG_Camera_Start_Series varchar(50)
AS
update DG_CameraDetails set DG_Camera_Start_Series=@DG_Camera_Start_Series where DG_Camera_pkey=@CameraId
GO
/****** Object:  StoredProcedure [dbo].[GetLastGeneratedNumber]    Script Date: 01/05/2013 14:44:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Procedure [dbo].[GetLastGeneratedNumber] 
(
	@UserID int
 )
 as
 begin
 Select isnull(Max(DG_Photos_RFID),'None') as NextNumber   from DG_Photos
where DG_Photos_UserID = @UserID and  CONVERT(VARCHAR(8),DG_Photos_CreatedOn, 10) =  CONVERT(VARCHAR(8), GETDATE(), 10)


 
--Select isnull(Max(DG_Photos_RFID),'None') as NextNumber from DG_Photos where DG_Photos_UserID = @UserID and DG_Photos_CreatedOn = getdate()
end
GO
/****** Object:  UserDefinedFunction [dbo].[GetItemsCountInPackage]    Script Date: 01/05/2013 14:44:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE function [dbo].[GetItemsCountInPackage]
(
@InputValue int
)
 RETURNS int
 Begin
 declare @ReturnValue INT
	Select @ReturnValue=COUNT(*) from DG_PackageDetails WHERE DG_PackageId=@InputValue and DG_Product_Quantity>0
	RETURN @ReturnValue
 end
GO
/****** Object:  UserDefinedFunction [dbo].[GetImageCountFromOrder]    Script Date: 01/05/2013 14:44:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Function [dbo].[GetImageCountFromOrder]
(
@OrderId int
)
returns int
As
begin

Declare @Quantity int
Declare @PhotoId varchar(max)
Declare @ProductType int 
Declare @IsBundled int 
Declare @LineItemID int 
Declare @TotalQuantity  int 

Set @TotalQuantity = 0 
Declare LineItem cursor for
Select DG_Orders_LineItems_Pkey	from DG_Orders_Details Where DG_Orders_Id = @OrderId and DG_Orders_Details_LineItem_ParentID = -1

open LineItem

fetch next from LineItem into @LineItemID
while (@@FETCH_STATUS = 0)
begin
	set @Quantity = 0
	Select @PhotoId = DG_Photos_ID ,@ProductType = DG_Orders_Details_ProductType_pkey,@Quantity = DG_Orders_LineItems_Quantity, @IsBundled = DG_Orders_ProductType_IsBundled
	from DG_Orders_Details inner join DG_Orders_ProductType Dtype on Dtype.DG_Orders_ProductType_pkey = DG_Orders_Details.DG_Orders_Details_ProductType_pkey  where DG_Orders_LineItems_Pkey =@LineItemId

	if(@IsBundled = 0)
	begin
		if(CHARINDEX(',', @PhotoId) > 0 ) 
			begin
					set @Quantity = (((LEN(@PhotoId) - LEN(REPLACE(@PhotoId, ',', '')))/LEN(',')) + 1 ) * @Quantity 
			end
	end

	fetch next from LineItem into @LineItemID

	Set @TotalQuantity = @TotalQuantity + @Quantity

end

return @TotalQuantity
end
GO
/****** Object:  UserDefinedFunction [dbo].[GetImageCountFromLineItem]    Script Date: 01/05/2013 14:44:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Function [dbo].[GetImageCountFromLineItem]
(
@LineItemId int
)
returns int
As
begin


Declare @Quantity int
Declare @PhotoId varchar(max)
Declare @ProductType int 
Declare @IsBundled int 

Select @PhotoId = DG_Photos_ID ,@ProductType = DG_Orders_Details_ProductType_pkey,@Quantity = DG_Orders_LineItems_Quantity, @IsBundled = DG_Orders_ProductType_IsBundled
from DG_Orders_Details inner join DG_Orders_ProductType Dtype on Dtype.DG_Orders_ProductType_pkey = DG_Orders_Details.DG_Orders_Details_ProductType_pkey  where DG_Orders_LineItems_Pkey =@LineItemId

--print @IsBundled
if(@IsBundled = 0)
begin
	if(CHARINDEX(',', @PhotoId) > 0 ) 
		begin
				set @Quantity = (((LEN(@PhotoId) - LEN(REPLACE(@PhotoId, ',', '')))/LEN(',')) + 1 ) * @Quantity 
		end
end

 
return @Quantity

end


--Select dbo.GetImageCountFromLineItem(3)
--Select  DG_Photos_ID ,DG_Orders_Details_ProductType_pkey, DG_Orders_LineItems_Quantity,  DG_Orders_ProductType_IsBundled
--from DG_Orders_Details inner join DG_Orders_ProductType Dtype on Dtype.DG_Orders_ProductType_pkey = DG_Orders_Details.DG_Orders_Details_ProductType_pkey  where DG_Orders_LineItems_Pkey =3
GO
/****** Object:  UserDefinedFunction [dbo].[GetCaptureCountUserwise]    Script Date: 01/05/2013 14:44:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create Function [dbo].[GetCaptureCountUserwise]
(
@UserId int,
@Fromdate datetime,
@ToDate datetime
)
returns int
As
begin
declare @TotalQuantity int
SELECT   @TotalQuantity =COUNT(*)
FROM         DG_Photos
WHERE     (DG_Photos_UserID = @UserId) and DG_Photos_CreatedOn between @Fromdate and @ToDate
return @TotalQuantity
end
GO
/****** Object:  StoredProcedure [dbo].[GetActivityReports]    Script Date: 01/05/2013 14:44:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create Procedure [dbo].[GetActivityReports]
(
@FromDate datetime,
@ToDate datetime,
@UserId int,
@ISFromDate bit,
@IsToDate bit
)
as
begin
   if (@UserId != 0 and @FromDate = 0 and @IsToDate = 0)
    begin
         select DG_Actions_Name,vw_GetActivityReports.DG_User_Name,vw_GetActivityReports.ActivityDate from vw_GetActivityReports where  vw_GetActivityReports.DG_User_pkey = @UserId 
                  
    end
                    --else if (UserID != 0 && @FromDate && !@IsToDate)
                    --begin
                    --    objresult = (from item in _objPackageDetailsdata
                    --                 where item.DG_User_pkey == UserID && item.DG_Acitivity_Date >= FromDate
                    --                 select item).Distinct().ToList();
                    --end
                    --else if (UserID != 0 && !@FromDate && @IsToDate)
                    --begin
                    --    objresult = (from item in _objPackageDetailsdata
                    --                 where item.DG_User_pkey == UserID && item.DG_Acitivity_Date <= ToDate
                    --                 select item).Distinct().ToList();
                    --end
                    --else if (UserID != 0 && @FromDate && @IsToDate)
                    --begin
                    --    objresult = (from item in _objPackageDetailsdata
                    --                 where item.DG_User_pkey == UserID && item.DG_Acitivity_Date >= FromDate && item.DG_Acitivity_Date <= ToDate
                    --                 select item).Distinct().ToList();
                    --end
                    --else if (UserID == 0 && @FromDate && !@IsToDate)
                    --begin
                    --    objresult = (from item in _objPackageDetailsdata
                    --                 where item.DG_Acitivity_Date >= FromDate
                    --                 select item).Distinct().ToList();
                    --end
                    --else if (UserID == 0 && !@FromDate && @IsToDate)
                    --begin
                    --    objresult = (from item in _objPackageDetailsdata
                    --                 where item.DG_Acitivity_Date <= ToDate
                    --                 select item).Distinct().ToList();
                    --end
                    --else if (UserID == 0 && @FromDate && @IsToDate)
                    --begin
                    --    objresult = (from item in _objPackageDetailsdata
                    --                 where item.DG_Acitivity_Date >= FromDate && item.DG_Acitivity_Date <= ToDate
                    --                 select item).Distinct().ToList();
                    --end
end
GO
/****** Object:  StoredProcedure [dbo].[FinancialAuditTrail]    Script Date: 01/05/2013 14:44:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Procedure [dbo].[FinancialAuditTrail]-- 'test', 'user', '01/01/2013' ,'11/01/2013'
(
 @UserName Varchar(max),
 @StoreName Varchar(max),
 @StartDate datetime,
 @EndDate datetime
)
as
begin


Create Table #tempOrders (UserName varchar(max), StoreName varchar(max) ,StartDate Datetime ,EndDate Datetime ,OrderNumber varchar(max),OrderDate datetime,ProductType Varchar(max),SellPrice varchar(max),Quantity int,TotalPrice varchar(max),Discount varchar(max),revenue varchar(max))

Declare @PhotoId varchar(400)
Declare @SinglePhotoId int
Declare @OrderNumber varchar(400)
Declare @OrderDate datetime
Declare @ProductType varchar(400)
Declare @Quantity varchar(400)
Declare @ItemPrice decimal
Declare @LineItemNetPrice decimal
Declare @LintItemUnitPrice decimal
Declare @TotalOrderPrice decimal
Declare @TotalOrderNetPrice decimal
Declare @Discount decimal
Declare @OrderDiscount decimal
Declare @LineItemDiscount decimal
Declare @ParentID int
Declare @OrderID int
Declare @IsBundled bit
Declare @TotalPhotoinLineItem int
Declare @TotalPhotoinOrder int


Declare Order_Cursor cursor for
SELECT     DG_Orders.DG_Orders_pkey, dbo.DG_Orders.DG_Orders_Number, dbo.DG_Orders.DG_Orders_Date, dbo.DG_Orders_Details.DG_Orders_Details_Items_UniPrice,DG_Orders_Details_Items_TotalCost,DG_Orders_NetCost
                     , dbo.DG_Orders.DG_Orders_Total_Discount, dbo.DG_Orders_Details.DG_Orders_LineItems_DiscountAmount, 
                      dbo.DG_Orders_Details.DG_Orders_Details_LineItem_ParentID, dbo.DG_Orders_Details.DG_Photos_ID,dbo.DG_Orders.DG_Orders_Cost, 
                      dbo.DG_Orders_ProductType.DG_Orders_ProductType_Name, dbo.DG_Orders_ProductType.DG_Orders_ProductType_IsBundled,dbo.DG_Orders_Details.DG_Orders_LineItems_Quantity
FROM         dbo.DG_Orders INNER JOIN
                      dbo.DG_Orders_Details ON dbo.DG_Orders.DG_Orders_pkey = dbo.DG_Orders_Details.DG_Orders_ID INNER JOIN
                      dbo.DG_Orders_ProductType ON 
                      dbo.DG_Orders_Details.DG_Orders_Details_ProductType_pkey = dbo.DG_Orders_ProductType.DG_Orders_ProductType_pkey
WHERE     ((dbo.DG_Orders_Details.DG_Orders_Details_LineItem_ParentID = - 1) and (dbo.DG_Orders.DG_Orders_Date between @StartDate and @EndDate))

open Order_Cursor

Fetch next from Order_Cursor into 
@OrderID,@OrderNumber,@OrderDate,@ItemPrice,@LineItemNetPrice,@TotalOrderNetPrice,@OrderDiscount,@Discount,@ParentID,@PhotoId,@TotalOrderPrice,@ProductType,@IsBundled,@Quantity



while (@@Fetch_Status = 0)
begin
	
	if(CHARINDEX(',', @PhotoID) > 0)
	begin 
		if(@IsBundled = 0)
		begin
			
				Select @TotalPhotoinLineItem = count(item) from dbo.fnSplit(@PhotoId,',')
				set @Quantity = @TotalPhotoinLineItem * @Quantity
				if(@OrderDiscount > 0)
				begin
					
					Select @LineItemDiscount = SUM(DG_Orders_LineItems_DiscountAmount) from DG_Orders_Details where DG_Orders_ID = @OrderID -- Get All discount give in item level
					set @OrderDiscount = @OrderDiscount - @LineItemDiscount -- Then Find Discount given at total Level
					
					Select @TotalPhotoinOrder = dbo.GetImageCountFromOrder(@OrderID)
					if(@OrderDiscount >0)
					begin
					set @Discount = @Discount + ((@LineItemNetPrice/@TotalOrderPrice) * @OrderDiscount)
					end
												
					insert into #tempOrders values (  @UserName,@StoreName ,@StartDate,@EndDate,@OrderNumber,@OrderDate,@ProductType,@ItemPrice,@Quantity,(@Quantity * @ItemPrice),@Discount,(@Quantity * @ItemPrice) - @Discount)
					
				end
				else
				begin
					insert into #tempOrders values (  @UserName,@StoreName ,@StartDate,@EndDate,@OrderNumber,@OrderDate,@ProductType,@ItemPrice,@Quantity,(@Quantity * @ItemPrice),@Discount,(@Quantity * @ItemPrice) - @Discount)
				end
		
		end
		else
		begin
				insert into #tempOrders values (  @UserName,@StoreName ,@StartDate,@EndDate,@OrderNumber,@OrderDate,@ProductType,@ItemPrice,@Quantity,(@Quantity * @ItemPrice),@Discount,(@Quantity * @ItemPrice) - @Discount)
		end	
		
	end
	else
	begin			 
				if(@OrderDiscount > 0)
				begin
				
										
					Select @LineItemDiscount = SUM(DG_Orders_LineItems_DiscountAmount) from DG_Orders_Details where DG_Orders_ID = @OrderID -- Get All discount give in item level
					set @OrderDiscount = @OrderDiscount - @LineItemDiscount -- Then Find Discount given at total Level
					Select @TotalPhotoinOrder = dbo.GetImageCountFromOrder(@OrderID)
					if(@OrderDiscount >0)
					begin
						set @Discount = @Discount + ((@LineItemNetPrice/@TotalOrderPrice) * @OrderDiscount)
					end
					insert into #tempOrders values (  @UserName,@StoreName ,@StartDate,@EndDate,@OrderNumber,@OrderDate,@ProductType,@ItemPrice,@Quantity,(@Quantity * @ItemPrice),@Discount,(@Quantity * @ItemPrice) - @Discount)
					
				end
				else
				begin
					insert into #tempOrders values (  @UserName,@StoreName ,@StartDate,@EndDate,@OrderNumber,@OrderDate,@ProductType,@ItemPrice,@Quantity,(@Quantity * @ItemPrice),@Discount,(@Quantity * @ItemPrice) - @Discount)
				end
	end
Fetch next from Order_Cursor into 
@OrderID,@OrderNumber,@OrderDate,@ItemPrice,@LineItemNetPrice,@TotalOrderNetPrice,@OrderDiscount,@Discount,@ParentID,@PhotoId,@TotalOrderPrice,@ProductType,@IsBundled,@Quantity
end 
Close Order_Cursor
deallocate Order_Cursor

insert into #tempOrders
SELECT     @UserName,@StoreName ,@StartDate,@EndDate,dbo.DG_Orders.DG_Orders_Number + ' R',dbo.DG_Refund.RefundDate,dbo.DG_Orders_ProductType.DG_Orders_ProductType_Name,
                       dbo.DG_Orders_Details.DG_Orders_Details_Items_UniPrice,COUNT(*) *  -1,(-1) * dbo.DG_Orders_Details.DG_Orders_Details_Items_UniPrice, 
                       (-1) * (dbo.DG_RefundDetails.Refunded_Amount - dbo.DG_Orders_Details.DG_Orders_Details_Items_UniPrice), (-1) * sum(dbo.DG_RefundDetails.Refunded_Amount)
FROM         dbo.DG_Orders_Details INNER JOIN
                      dbo.DG_RefundDetails INNER JOIN
                      dbo.DG_Refund ON dbo.DG_RefundDetails.DG_RefundMaster_ID = dbo.DG_Refund.DG_RefundId ON 
                      dbo.DG_Orders_Details.DG_Orders_LineItems_pkey = dbo.DG_RefundDetails.DG_LineItemId INNER JOIN
                      dbo.DG_Orders ON dbo.DG_Refund.DG_OrderId = dbo.DG_Orders.DG_Orders_pkey LEFT OUTER JOIN
                      dbo.DG_Orders_ProductType ON dbo.DG_Orders_Details.DG_Orders_Details_ProductType_pkey = dbo.DG_Orders_ProductType.DG_Orders_ProductType_pkey
			where (dbo.DG_Refund.RefundDate between @StartDate and @EndDate)                     
GROUP BY dbo.DG_RefundDetails.DG_LineItemId, dbo.DG_Orders.DG_Orders_Number,dbo.DG_Orders_ProductType.DG_Orders_ProductType_Name, 
                      dbo.DG_Refund.RefundDate, dbo.DG_Orders_Details.DG_Orders_Details_Items_UniPrice, 
                      dbo.DG_RefundDetails.Refunded_Amount 
                      

Select * from #tempOrders 

Drop table #tempOrders

--Select '' UserName, '' StoreName ,getdate() StartDate  ,getdate() EndDate ,'' OrderNumber ,getdate() OrderDate , '' ProductType ,'0.00' SellPrice ,0 Quantity ,'0.00' TotalPrice ,'0.00' Discount ,'0.00' revenue 
end
GO
/****** Object:  View [dbo].[vw_GetProductNameforOrder]    Script Date: 01/05/2013 14:44:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE View [dbo].[vw_GetProductNameforOrder]  
as  
SELECT     DG_Orders_ProductType_pkey, DG_Orders_ProductType_Name, DG_Orders_ProductType_Desc, DG_Orders_ProductType_IsBundled,   
                      DG_Orders_ProductType_DiscountApplied, DG_Orders_ProductType_Image, DG_IsPackage, DG_MaxQuantity, DG_Orders_ProductType_Active, DG_IsActive,   
                      DG_IsAccessory,dbo.GetItemsCountInPackage(DG_Orders_ProductType_pkey) AS Itemcount  
FROM         DG_Orders_ProductType
GO
/****** Object:  View [dbo].[vw_GetPrinterQueue]    Script Date: 01/05/2013 14:44:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vw_GetPrinterQueue]
AS
SELECT     dbo.DG_PrinterQueue.DG_PrinterQueue_Pkey, dbo.DG_PrinterQueue.DG_Order_Details_Pkey, dbo.DG_PrinterQueue.DG_SentToPrinter, 
                      dbo.DG_AssociatedPrinters.DG_AssociatedPrinters_Name, dbo.DG_Orders_ProductType.DG_Orders_ProductType_Name, 
                      dbo.GetRFID(dbo.DG_PrinterQueue.DG_PrinterQueue_Image_Pkey) AS DG_Photos_RFID, dbo.DG_PrinterQueue.DG_Associated_PrinterId, 
                      dbo.DG_PrinterQueue.DG_PrinterQueue_ProductID, dbo.DG_PrinterQueue.is_Active, dbo.DG_PrinterQueue.QueueIndex, 
                      dbo.DG_Orders_ProductType.DG_Orders_ProductType_pkey, dbo.DG_Orders.DG_Orders_Number, dbo.DG_Orders.DG_Orders_pkey
FROM         dbo.DG_Orders RIGHT OUTER JOIN
                      dbo.DG_Orders_Details ON dbo.DG_Orders.DG_Orders_pkey = dbo.DG_Orders_Details.DG_Orders_ID RIGHT OUTER JOIN
                      dbo.DG_PrinterQueue INNER JOIN
                      dbo.DG_Orders_ProductType ON dbo.DG_PrinterQueue.DG_PrinterQueue_ProductID = dbo.DG_Orders_ProductType.DG_Orders_ProductType_pkey INNER JOIN
                      dbo.DG_AssociatedPrinters ON dbo.DG_PrinterQueue.DG_Associated_PrinterId = dbo.DG_AssociatedPrinters.DG_AssociatedPrinters_Pkey ON 
                      dbo.DG_Orders_Details.DG_Orders_LineItems_pkey = dbo.DG_PrinterQueue.DG_Order_Details_Pkey
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[53] 4[4] 2[23] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1[34] 4[40] 3) )"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 1
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = -42
      End
      Begin Tables = 
         Begin Table = "DG_Orders_Details"
            Begin Extent = 
               Top = 162
               Left = 378
               Bottom = 377
               Right = 704
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "DG_PrinterQueue"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 315
               Right = 276
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "DG_Orders_ProductType"
            Begin Extent = 
               Top = 0
               Left = 493
               Bottom = 233
               Right = 784
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "DG_AssociatedPrinters"
            Begin Extent = 
               Top = 19
               Left = 1035
               Bottom = 196
               Right = 1317
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "DG_Orders"
            Begin Extent = 
               Top = 157
               Left = 937
               Bottom = 385
               Right = 1218
            End
            DisplayFlags = 280
            TopColumn = 3
         End
      End
   End
   Begin SQLPane = 
      PaneHidden = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 15
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 2940
         Width = 1500
         Width = 5040
         Width = 2745
         Width = 1500
         Width = 1500
         Width = 1500
         Wi' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vw_GetPrinterQueue'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane2', @value=N'dth = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 10425
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vw_GetPrinterQueue'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=2 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vw_GetPrinterQueue'
GO
/****** Object:  StoredProcedure [dbo].[ProductSummary]    Script Date: 01/05/2013 14:44:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Procedure [dbo].[ProductSummary]-- '','','2323','23232'

(
	@ToDate datetime,
	@FromDate datetime,
	@UserName varchar(max),
	@StoreName varchar(max)
)
as
Declare @TotalRevnue decimal

Select @TotalRevnue = Sum(dbo.DG_Orders_Details.DG_Orders_Details_Items_NetPrice) from dbo.DG_Orders_Details inner join
 dbo.DG_Orders ON dbo.DG_Orders_Details.DG_Orders_ID = dbo.DG_Orders.DG_Orders_pkey            
 WHERE (dbo.DG_Orders.DG_Orders_Date BETWEEN @FromDate AND @ToDate) AND 
                      (dbo.DG_Orders_Details.DG_Orders_Details_LineItem_ParentID = - 1) 
;
with orderProductDetail(ProductName,TotalQuantity,UnitPrice,TotalCost,Discount,NetPrice)
as
(
SELECT     dbo.DG_Orders_ProductType.DG_Orders_ProductType_Name, sum(dbo.GetImageCountFromLineItem(dbo.DG_Orders_Details.DG_Orders_LineItems_Pkey)), 
                      dbo.DG_Orders_Details.DG_Orders_Details_Items_UniPrice, SUM(dbo.DG_Orders_Details.DG_Orders_Details_Items_TotalCost) AS TotalCost, 
                      SUM(dbo.DG_Orders_Details.DG_Orders_LineItems_DiscountAmount) AS Discount, SUM(dbo.DG_Orders_Details.DG_Orders_Details_Items_NetPrice) AS NetPrice
FROM         dbo.DG_Orders_Details INNER JOIN
                      dbo.DG_Orders_ProductType ON 
                      dbo.DG_Orders_Details.DG_Orders_Details_ProductType_pkey = dbo.DG_Orders_ProductType.DG_Orders_ProductType_pkey INNER JOIN
                      dbo.DG_Orders ON dbo.DG_Orders_Details.DG_Orders_ID = dbo.DG_Orders.DG_Orders_pkey
WHERE     (dbo.DG_Orders.DG_Orders_Date BETWEEN @FromDate AND @ToDate) AND 
                      (dbo.DG_Orders_Details.DG_Orders_Details_LineItem_ParentID = - 1)
GROUP BY dbo.DG_Orders_ProductType.DG_Orders_ProductType_Name,
                      dbo.DG_Orders_Details.DG_Orders_Details_Items_UniPrice
                      )
select ProductName,TotalQuantity,UnitPrice,TotalCost,Discount,NetPrice,@TotalRevnue TotalRevenue,cast(convert(decimal(10,2),convert(decimal(10,4),(convert(decimal(10,4),NetPrice)/convert(decimal(10,4),@TotalRevnue))) * 100) as varchar(150)) as 'Revpercentage', @FromDate FromDate,@ToDate Todate ,@UserName UserName ,@StoreName StoreName
from orderProductDetail
GO
/****** Object:  StoredProcedure [dbo].[GetPhotgrapherPerformance]    Script Date: 01/05/2013 14:44:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Procedure  [dbo].[GetPhotgrapherPerformance]--= '01-jan-2013','31-jan-2013','1-jan-2013','31-jan-2013','teststore','deepak',0
(
@FromDate datetime=null,
@ToDate datetime=null,
@SecondFromDate datetime = null,
@SecondToDate datetime = null,
@StoreName varchar(max),
@UserName varchar(max),
@Comparasion bit
)
as

if(@Comparasion=1)
begin
SELECT distinct  dbo.DG_Users.DG_User_Name + ' (' + dbo.DG_Users.DG_User_First_Name + ' ' +  dbo.DG_Users.DG_User_Last_Name + ')' AS UserName , dbo.GetCaptureCountUserwise(DG_Users.DG_User_pkey,@FromDate,@ToDate) AS NumberofCapture,
isnull(NumberofSales,0) NumberofSales ,Isnull(ImageSold,0) ImageSold,Isnull(Revenue,0) Revenue,'1' Dataflag,@StoreName StoreName,@UserName  'Printedby',(Select count(*) from DG_Activity Where DG_Acitivity_ActionType in (1,2) and DG_Acitivity_By = dbo.DG_Users.DG_User_pkey and  (DG_Acitivity_Date between @FromDate and @ToDate)) 'Shots_Previewed',@FromDate as FromDate1,@ToDate as ToDate1,@SecondFromDate as FromDate2,@SecondToDate as ToDate2

FROM         dbo.DG_Photos INNER JOIN
             dbo.DG_Users ON dbo.DG_Photos.DG_Photos_UserID = dbo.DG_Users.DG_User_pkey left outer join dbo.GetNumberofPhotosSales(@FromDate,@ToDate) PhotoSales
             on PhotoSales.UserID = dbo.DG_Users.DG_User_pkey
GROUP BY dbo.DG_Users.DG_User_pkey,dbo.DG_Users.DG_User_Name, dbo.DG_Users.DG_User_First_Name, dbo.DG_Users.DG_User_Last_Name, dbo.DG_Users.DG_User_Roles_Id,NumberofSales,ImageSold,Revenue,DG_Photos.DG_Photos_CreatedOn
HAVING      (dbo.DG_Users.DG_User_Roles_Id = 8) 
union 
SELECT distinct  dbo.DG_Users.DG_User_Name + ' (' + dbo.DG_Users.DG_User_First_Name + ' ' +  dbo.DG_Users.DG_User_Last_Name + ')' AS UserName , dbo.GetCaptureCountUserwise(DG_Users.DG_User_pkey,@SecondFromDate,@SecondToDate) AS NumberofCapture,
isnull(NumberofSales,0) NumberofSales ,Isnull(ImageSold,0) ImageSold,Isnull(Revenue,0) Revenue,'2' Dataflag,@StoreName StoreName,@UserName  'Printedby',(Select count(*) from DG_Activity Where DG_Acitivity_ActionType in (1,2) and DG_Acitivity_By = dbo.DG_Users.DG_User_pkey and  (DG_Acitivity_Date between @SecondFromDate and @SecondToDate)) 'Shots_Previewed',@FromDate as FromDate1,@ToDate as ToDate1,@SecondFromDate as FromDate2,@SecondToDate as ToDate2

FROM         dbo.DG_Photos INNER JOIN
             dbo.DG_Users ON dbo.DG_Photos.DG_Photos_UserID = dbo.DG_Users.DG_User_pkey left outer join dbo.GetNumberofPhotosSales(@SecondFromDate,@SecondToDate) PhotoSales
             on PhotoSales.UserID = dbo.DG_Users.DG_User_pkey
GROUP BY dbo.DG_Users.DG_User_pkey,dbo.DG_Users.DG_User_Name, dbo.DG_Users.DG_User_First_Name, dbo.DG_Users.DG_User_Last_Name, dbo.DG_Users.DG_User_Roles_Id,NumberofSales,ImageSold,Revenue,DG_Photos.DG_Photos_CreatedOn
HAVING      (dbo.DG_Users.DG_User_Roles_Id = 8) 
end
else
begin
SELECT  distinct dbo.DG_Users.DG_User_Name + ' (' + dbo.DG_Users.DG_User_First_Name + ' ' +  dbo.DG_Users.DG_User_Last_Name + ')' AS UserName , dbo.GetCaptureCountUserwise(DG_Users.DG_User_pkey,@FromDate,@ToDate) AS NumberofCapture,
isnull(NumberofSales,0) NumberofSales ,Isnull(ImageSold,0) ImageSold,Isnull(Revenue,0) Revenue,'1' Dataflag,@StoreName StoreName,@UserName  'Printedby',(Select count(*) from DG_Activity Where DG_Acitivity_ActionType in (1,2) and DG_Acitivity_By = dbo.DG_Users.DG_User_pkey and  (DG_Acitivity_Date between @FromDate and @ToDate)) 'Shots_Previewed',@FromDate as FromDate1,@ToDate as ToDate1,@SecondFromDate as FromDate2,@SecondToDate as ToDate2

FROM         dbo.DG_Photos INNER JOIN
             dbo.DG_Users ON dbo.DG_Photos.DG_Photos_UserID = dbo.DG_Users.DG_User_pkey left outer join dbo.GetNumberofPhotosSales(@FromDate,@ToDate) PhotoSales
             on PhotoSales.UserID = dbo.DG_Users.DG_User_pkey
GROUP BY dbo.DG_Users.DG_User_pkey,dbo.DG_Users.DG_User_Name, dbo.DG_Users.DG_User_First_Name, dbo.DG_Users.DG_User_Last_Name, dbo.DG_Users.DG_User_Roles_Id,NumberofSales,ImageSold,Revenue,DG_Photos.DG_Photos_CreatedOn
HAVING      (dbo.DG_Users.DG_User_Roles_Id = 8) 

end
GO
/****** Object:  UserDefinedFunction [dbo].[GetOrderItemsPrinted]    Script Date: 01/05/2013 14:44:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Function [dbo].[GetOrderItemsPrinted] 
(
@OrderID int
)
returns varchar(max)
as
begin
DECLARE @listStr VARCHAR(MAX)

select @listStr = ltrim(substring(
(
SELECT     distinct ' + ' + case when DG_Orders_ProductType.DG_IsPackage=1 then DG_Orders_ProductType.DG_Orders_ProductType_Name + '[' + [dbo].[GetPackageChilds](DG_Orders_ProductType.DG_Orders_ProductType_pkey) + ']' else DG_Orders_ProductType.DG_Orders_ProductType_Name end + 
+ ' (' + CAST(DG_Orders_Details.DG_Orders_LineItems_Quantity * (case when DG_Orders_ProductType.DG_Orders_ProductType_IsBundled=0 then (len(DG_Photos_ID) - len(replace(DG_Photos_ID, ',', '')) + 1) else 1 end)  AS varchar) + ')' 
FROM         DG_Orders INNER JOIN
                      DG_Orders_Details ON DG_Orders.DG_Orders_pkey = DG_Orders_Details.DG_Orders_ID INNER JOIN
                      DG_Orders_ProductType ON DG_Orders_Details.DG_Orders_Details_ProductType_pkey = DG_Orders_ProductType.DG_Orders_ProductType_pkey
WHERE     (DG_Orders.DG_Orders_pkey = @OrderID) and DG_Orders_Details.DG_Orders_Details_LineItem_ParentID = -1
for xml path('')
)
,3,5000))

return @listStr
end
GO
/****** Object:  StoredProcedure [dbo].[GetLocationPerformance]    Script Date: 01/05/2013 14:44:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--------------------Location Wise Performance-----------------
CREATE Procedure  [dbo].[GetLocationPerformance] --'01-dec-2012','31-dec-2012','1-dec-2011','31-dec-2012','teststore','deepak',1
(
@FromDate datetime=null,
@ToDate datetime=null,
@SecondFromDate datetime = null,
@SecondToDate datetime = null,
@StoreName varchar(max),
@UserName varchar(max),
@Comparasion bit
)
as

if(@Comparasion=1)
begin
SELECT  COUNT(dbo.DG_Photos.DG_Photos_pkey) AS NumberofCapture,
isnull(NumberofSales,0) NumberofSales ,Isnull(ImageSold,0) ImageSold,Isnull(Revenue,0) Revenue,DG_Location.DG_Location_Name,
'Store' StoreName,'Deepak' Printedby,dbo.DG_Location.DG_Location_pkey
into #temp1
FROM         dbo.DG_Photos INNER JOIN
             dbo.DG_Users ON dbo.DG_Photos.DG_Photos_UserID = dbo.DG_Users.DG_User_pkey 
             left outer join dbo.GetNumberofPhotosSales(@FromDate,@ToDate) PhotoSales
             on PhotoSales.UserID = dbo.DG_Users.DG_User_pkey
             inner join dbo.DG_Location on DG_Users.DG_Location_ID=DG_Location.DG_Location_pkey
             where DG_Photos.DG_Photos_CreatedOn between @FromDate and @ToDate
GROUP BY  NumberofSales,ImageSold,Revenue,DG_Location.DG_Location_pkey,DG_Location.DG_Location_Name
SELECT  COUNT(dbo.DG_Photos.DG_Photos_pkey) AS NumberofCapture,
isnull(NumberofSales,0) NumberofSales ,Isnull(ImageSold,0) ImageSold,Isnull(Revenue,0) Revenue,DG_Location.DG_Location_Name,
'Store' StoreName,'Deepak' Printedby,dbo.DG_Location.DG_Location_pkey
into #temp2
FROM         dbo.DG_Photos INNER JOIN
             dbo.DG_Users ON dbo.DG_Photos.DG_Photos_UserID = dbo.DG_Users.DG_User_pkey 
             left outer join dbo.GetNumberofPhotosSales(@SecondFromDate,@SecondToDate) PhotoSales
             on PhotoSales.UserID = dbo.DG_Users.DG_User_pkey
             inner join dbo.DG_Location on DG_Users.DG_Location_ID=DG_Location.DG_Location_pkey
             where DG_Photos.DG_Photos_CreatedOn between @SecondFromDate and @SecondToDate
GROUP BY  NumberofSales,ImageSold,Revenue,DG_Location.DG_Location_pkey,DG_Location.DG_Location_Name



select isnull(sum(numberofcapture),0) as 'Number_of_Capture',isnull(sum(numberofsales),0) 'Number_of_Sales',
isnull(SUM(Imagesold),0) 'ImageSold',isnull(SUM(revenue),0) 'Revenue',DG_Location.DG_Location_Name,DG_Location.DG_Location_pkey,
dbo.GetPreviewCountLocationwise(DG_Location.DG_Location_pkey,@FromDate,@ToDate) as Shots_Previewed ,'1' DataFlag,@UserName PrintedBy,@StoreName StoreName,@FromDate as FromDate,@ToDate as ToDate,@SecondFromDate as SecondFromDate,@SecondToDate as SecondToDate,
case when SUM(Imagesold) > 0 then isnull(SUM(revenue),0)/isnull(SUM(Imagesold),1) else 0 end as AveragePrice,case when sum(numberofcapture) <= 0 then 0 else isnull(SUM(Imagesold),0)/ isnull(sum(numberofcapture),1) end as SellThru

from #temp1 right outer join dbo.DG_Location on #temp1.DG_Location_pkey=DG_Location.DG_Location_pkey
group by DG_Location.DG_Location_pkey, DG_Location.DG_Location_Name
union 
select isnull(sum(numberofcapture),0) as 'Number_of_Capture',isnull(sum(numberofsales),0) 'Number_of_Sales',
isnull(SUM(Imagesold),0) 'ImageSold',isnull(SUM(revenue),0) 'Revenue',DG_Location.DG_Location_Name,DG_Location.DG_Location_pkey,
dbo.GetPreviewCountLocationwise(DG_Location.DG_Location_pkey,@FromDate,@ToDate) as Shots_Previewed ,'2' DataFlag,@UserName PrintedBy,@StoreName StoreName,@FromDate as FromDate,@ToDate as ToDate,@SecondFromDate as SecondFromDate,@SecondToDate as SecondToDate,
case when SUM(Imagesold) > 0 then isnull(SUM(revenue),0)/isnull(SUM(Imagesold),1) else 0 end as AveragePrice,case when sum(numberofcapture) <= 0 then 0 else isnull(SUM(Imagesold),0)/ isnull(sum(numberofcapture),1) end as SellThru

from #temp2 right outer join dbo.DG_Location on #temp2.DG_Location_pkey=DG_Location.DG_Location_pkey
group by DG_Location.DG_Location_pkey, DG_Location.DG_Location_Name
end
else
begin
SELECT  COUNT(dbo.DG_Photos.DG_Photos_pkey) AS NumberofCapture,
isnull(NumberofSales,0) NumberofSales ,Isnull(ImageSold,0) ImageSold,Isnull(Revenue,0) Revenue,DG_Location.DG_Location_Name,
'Store' StoreName,'Deepak' Printedby,dbo.DG_Location.DG_Location_pkey
into #temp
FROM         dbo.DG_Photos INNER JOIN
             dbo.DG_Users ON dbo.DG_Photos.DG_Photos_UserID = dbo.DG_Users.DG_User_pkey 
             left outer join dbo.GetNumberofPhotosSales(@FromDate,@ToDate) PhotoSales
             on PhotoSales.UserID = dbo.DG_Users.DG_User_pkey
             inner join dbo.DG_Location on DG_Users.DG_Location_ID=DG_Location.DG_Location_pkey
             where DG_Photos.DG_Photos_CreatedOn between @FromDate and @ToDate
GROUP BY  NumberofSales,ImageSold,Revenue,DG_Location.DG_Location_pkey,DG_Location.DG_Location_Name

select isnull(sum(numberofcapture),0) as 'Number_of_Capture',isnull(sum(numberofsales),0) 'Number_of_Sales',
isnull(SUM(Imagesold),0) 'ImageSold',isnull(SUM(revenue),0) 'Revenue',DG_Location.DG_Location_Name,DG_Location.DG_Location_pkey,
dbo.GetPreviewCountLocationwise(DG_Location.DG_Location_pkey,@FromDate,@ToDate) as Shots_Previewed ,'1' DataFlag,@UserName PrintedBy,@StoreName StoreName,@FromDate as FromDate,@ToDate as ToDate,@SecondFromDate as SecondFromDate,@SecondToDate as SecondToDate,
case when SUM(Imagesold) > 0 then isnull(SUM(revenue),0)/isnull(SUM(Imagesold),1) else 0 end as AveragePrice,case when sum(numberofcapture) <= 0 then 0 else isnull(SUM(Imagesold),0)/ isnull(sum(numberofcapture),1) end as SellThru
from #temp right outer join dbo.DG_Location on #temp.DG_Location_pkey=DG_Location.DG_Location_pkey
group by DG_Location.DG_Location_pkey, DG_Location.DG_Location_Name

end



--select 0 'Number_of_Capture',0 'Number_of_Sales',
--0 'ImageSold',0 'Revenue','' DG_Location_Name,0 DG_Location_pkey,
--0 as Shots_Previewed ,'1' DataFlag,'' PrintedBy,'' StoreName,getdate() as FromDate,getdate() as ToDate,getdate() as SecondFromDate,getdate() as SecondToDate,0 as AveragePrice,0 SellThru
GO
/****** Object:  View [dbo].[vw_GetOrderDetailsforRefund]    Script Date: 01/05/2013 14:44:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vw_GetOrderDetailsforRefund]
AS
SELECT     dbo.DG_Orders_Details.DG_Orders_LineItems_pkey, dbo.DG_Orders_Details.DG_Orders_ID, dbo.DG_Orders_Details.DG_Photos_ID, 
                      dbo.DG_Orders_Details.DG_Orders_LineItems_Quantity, dbo.GetImageCountFromLineItem(dbo.DG_Orders_Details.DG_Orders_LineItems_pkey) AS TotalQuantity, 
                      dbo.DG_Orders_Details.DG_Orders_LineItems_Created, dbo.DG_Orders_Details.DG_Orders_LineItems_DiscountType, 
                      dbo.DG_Orders_Details.DG_Orders_LineItems_DiscountAmount, dbo.DG_Orders_Details.DG_Orders_Details_Items_UniPrice, 
                      dbo.DG_Orders_Details.DG_Orders_Details_Items_TotalCost, dbo.DG_Orders_Details.DG_Orders_Details_Items_NetPrice, 
                      dbo.DG_Orders_Details.DG_Orders_Details_ProductType_pkey, dbo.DG_Orders_Details.DG_Orders_Details_LineItem_ParentID, 
                      dbo.DG_Orders_Details.DG_Orders_Details_LineItem_PrinterReferenceID, dbo.DG_Orders.DG_Orders_Number, dbo.DG_Orders.DG_Orders_Date, 
                      dbo.DG_Orders.DG_Orders_Cost, dbo.DG_Orders.DG_Orders_NetCost, dbo.DG_Orders.DG_Orders_Currency_ID, 
                      dbo.DG_Orders.DG_Orders_Currency_Conversion_Rate, dbo.DG_Orders.DG_Orders_Total_Discount, dbo.DG_Orders.DG_Orders_Total_Discount_Details, 
                      dbo.DG_Orders.DG_Orders_PaymentDetails, dbo.DG_Orders.DG_Orders_PaymentMode, dbo.DG_Orders_ProductType.DG_Orders_ProductType_Name, 
                      dbo.DG_Orders_ProductType.DG_Orders_ProductType_IsBundled, dbo.DG_Orders_ProductType.DG_IsPackage
FROM         dbo.DG_Orders_Details INNER JOIN
                      dbo.DG_Orders_ProductType ON 
                      dbo.DG_Orders_Details.DG_Orders_Details_ProductType_pkey = dbo.DG_Orders_ProductType.DG_Orders_ProductType_pkey INNER JOIN
                      dbo.DG_Orders ON dbo.DG_Orders_Details.DG_Orders_ID = dbo.DG_Orders.DG_Orders_pkey
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[23] 4[4] 2[4] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1[63] 4[4] 3) )"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 1
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "DG_Orders_Details"
            Begin Extent = 
               Top = 39
               Left = 638
               Bottom = 361
               Right = 964
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "DG_Orders_ProductType"
            Begin Extent = 
               Top = 29
               Left = 1060
               Bottom = 330
               Right = 1351
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "DG_Orders"
            Begin Extent = 
               Top = 38
               Left = 321
               Bottom = 389
               Right = 602
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
      PaneHidden = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 29
         Width = 284
         Width = 1500
         Width = 1500
         Width = 4140
         Width = 5205
         Width = 6240
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 5265
         Alias = 6465
         Table = 1170
 ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vw_GetOrderDetailsforRefund'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane2', @value=N'        Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vw_GetOrderDetailsforRefund'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=2 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vw_GetOrderDetailsforRefund'
GO
/****** Object:  View [dbo].[vw_TakingReport]    Script Date: 01/05/2013 14:44:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE view [dbo].[vw_TakingReport]
as
SELECT     DG_Orders.DG_Orders_Number, DG_Orders.DG_Orders_Date, 
                      CASE DG_Orders_PaymentMode WHEN 0 THEN 'Credit/Cash' WHEN 1 THEN 'Room Charges' WHEN 2 THEN 'Gift Voucher' END AS DG_Orders_PaymentMode, 
                      DG_Currency.DG_Currency_Symbol AS DG_Orders_Currency_ID, DG_Orders.DG_Orders_NetCost AS NetCost, dbo.GetOrderItemsPrinted(DG_Orders.DG_Orders_pkey) 
                      AS ItemDetail, 0 AS State, DG_Orders.DG_Orders_pkey
FROM         DG_Orders INNER JOIN
                      DG_Currency ON DG_Currency.DG_Currency_pkey = DG_Orders.DG_Orders_Currency_ID
WHERE     (DG_Orders.DG_Orders_Canceled IS NULL) OR
                      (DG_Orders.DG_Orders_Canceled = 0)
UNION
SELECT     DG_Orders.DG_Orders_Number + '(C)' AS DG_Orders_Number, DG_Orders.DG_Orders_Date, 
                      CASE DG_Orders_PaymentMode WHEN 0 THEN 'Credit/Cash' WHEN 1 THEN 'Room Charges' WHEN 2 THEN 'Gift Voucher' END AS DG_Orders_PaymentMode, 
                      DG_Currency.DG_Currency_Symbol AS DG_Orders_Currency_ID, DG_Orders.DG_Orders_NetCost AS NetCost, 'None' AS ItemDetail, 1 AS State, 
                      DG_Orders.DG_Orders_pkey
FROM         DG_Orders INNER JOIN
                      DG_Currency ON DG_Currency.DG_Currency_pkey = DG_Orders.DG_Orders_Currency_ID
WHERE     (DG_Orders.DG_Orders_Canceled = 1)
UNION
SELECT     DO.DG_Orders_Number + ' (R)' AS DG_Orders_Number, DR.RefundDate AS DG_Orders_Date, 'N/A' AS DG_Orders_PaymentMode, 
                      DG_Currency.DG_Currency_Symbol AS DG_Orders_Currency_ID, DR.RefundAmount AS NetCost, dbo.GetOrderItemsRefunded(DR.DG_OrderId) AS ItemDetail, 
                      2 AS State, DO.DG_Orders_pkey
FROM         DG_Refund AS DR INNER JOIN
                      DG_Orders AS DO ON DR.DG_OrderId = DO.DG_Orders_pkey INNER JOIN
                      DG_Currency ON DG_Currency.DG_Currency_pkey = DO.DG_Orders_Currency_ID
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[16] 4[4] 2[49] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "DG_Orders"
            Begin Extent = 
               Top = 42
               Left = 83
               Bottom = 321
               Right = 401
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 11
         Width = 284
         Width = 1200
         Width = 2595
         Width = 1650
         Width = 2400
         Width = 2025
         Width = 1200
         Width = 1200
         Width = 1200
         Width = 1200
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vw_TakingReport'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vw_TakingReport'
GO
/****** Object:  Default [DF_DG_CameraDetails_DG_Camera_IsDeleted]    Script Date: 01/05/2013 14:44:39 ******/
ALTER TABLE [dbo].[DG_CameraDetails] ADD  CONSTRAINT [DF_DG_CameraDetails_DG_Camera_IsDeleted]  DEFAULT ((0)) FOR [DG_Camera_IsDeleted]
GO
/****** Object:  Default [DF_DG_Activity_DG_Acitivity_Date]    Script Date: 01/05/2013 14:44:39 ******/
ALTER TABLE [dbo].[DG_Activity] ADD  CONSTRAINT [DF_DG_Activity_DG_Acitivity_Date]  DEFAULT (getdate()) FOR [DG_Acitivity_Date]
GO
/****** Object:  Default [DF_DG_Refund_Refund_Mode]    Script Date: 01/05/2013 14:44:39 ******/
ALTER TABLE [dbo].[DG_Refund] ADD  CONSTRAINT [DF_DG_Refund_Refund_Mode]  DEFAULT ((0)) FOR [Refund_Mode]
GO
/****** Object:  Default [DF_DG_PrinterQueue_is_Active]    Script Date: 01/05/2013 14:44:39 ******/
ALTER TABLE [dbo].[DG_PrinterQueue] ADD  CONSTRAINT [DF_DG_PrinterQueue_is_Active]  DEFAULT ((1)) FOR [is_Active]
GO
/****** Object:  Default [DF_DG_PrinterQueue_DG_IsSpecPrint]    Script Date: 01/05/2013 14:44:39 ******/
ALTER TABLE [dbo].[DG_PrinterQueue] ADD  CONSTRAINT [DF_DG_PrinterQueue_DG_IsSpecPrint]  DEFAULT ((0)) FOR [DG_IsSpecPrint]
GO
/****** Object:  Default [DF_DG_Orders_ProductType_DG_Orders_ProductType_IsBundled]    Script Date: 01/05/2013 14:44:39 ******/
ALTER TABLE [dbo].[DG_Orders_ProductType] ADD  CONSTRAINT [DF_DG_Orders_ProductType_DG_Orders_ProductType_IsBundled]  DEFAULT ((0)) FOR [DG_Orders_ProductType_IsBundled]
GO
/****** Object:  Default [DF_DG_Orders_ProductType_DG_Orders_ProductType_DiscountApplied]    Script Date: 01/05/2013 14:44:39 ******/
ALTER TABLE [dbo].[DG_Orders_ProductType] ADD  CONSTRAINT [DF_DG_Orders_ProductType_DG_Orders_ProductType_DiscountApplied]  DEFAULT ((0)) FOR [DG_Orders_ProductType_DiscountApplied]
GO
/****** Object:  Default [DF_DG_Orders_ProductType_DG_IsPackage]    Script Date: 01/05/2013 14:44:39 ******/
ALTER TABLE [dbo].[DG_Orders_ProductType] ADD  CONSTRAINT [DF_DG_Orders_ProductType_DG_IsPackage]  DEFAULT ((0)) FOR [DG_IsPackage]
GO
/****** Object:  Default [DF_DG_Orders_ProductType_DG_MaxQuantity]    Script Date: 01/05/2013 14:44:39 ******/
ALTER TABLE [dbo].[DG_Orders_ProductType] ADD  CONSTRAINT [DF_DG_Orders_ProductType_DG_MaxQuantity]  DEFAULT ((1)) FOR [DG_MaxQuantity]
GO
/****** Object:  Default [DF_DG_Orders_ProductType_DG_Orders_ProductType_Active]    Script Date: 01/05/2013 14:44:39 ******/
ALTER TABLE [dbo].[DG_Orders_ProductType] ADD  CONSTRAINT [DF_DG_Orders_ProductType_DG_Orders_ProductType_Active]  DEFAULT ((1)) FOR [DG_Orders_ProductType_Active]
GO
/****** Object:  Default [DF_DG_Orders_Details_DG_Orders_ID_1]    Script Date: 01/05/2013 14:44:39 ******/
ALTER TABLE [dbo].[DG_Orders_Details] ADD  CONSTRAINT [DF_DG_Orders_Details_DG_Orders_ID_1]  DEFAULT ((-1)) FOR [DG_Orders_ID]
GO
/****** Object:  Default [DF_DG_Orders_Details_DG_Orders_LineItems_Created]    Script Date: 01/05/2013 14:44:39 ******/
ALTER TABLE [dbo].[DG_Orders_Details] ADD  CONSTRAINT [DF_DG_Orders_Details_DG_Orders_LineItems_Created]  DEFAULT (getdate()) FOR [DG_Orders_LineItems_Created]
GO
/****** Object:  Default [DF_DG_Orders_Details_DG_Orders_LineItems_DiscountType]    Script Date: 01/05/2013 14:44:39 ******/
ALTER TABLE [dbo].[DG_Orders_Details] ADD  CONSTRAINT [DF_DG_Orders_Details_DG_Orders_LineItems_DiscountType]  DEFAULT ((-1)) FOR [DG_Orders_LineItems_DiscountType]
GO
/****** Object:  Default [DF_DG_Orders_Details_DG_Orders_LineItems_DiscountAmount]    Script Date: 01/05/2013 14:44:39 ******/
ALTER TABLE [dbo].[DG_Orders_Details] ADD  CONSTRAINT [DF_DG_Orders_Details_DG_Orders_LineItems_DiscountAmount]  DEFAULT ((0)) FOR [DG_Orders_LineItems_DiscountAmount]
GO
/****** Object:  Default [DF_DG_Orders_Details_DG_Orders_LineItems_Quantity]    Script Date: 01/05/2013 14:44:39 ******/
ALTER TABLE [dbo].[DG_Orders_Details] ADD  CONSTRAINT [DF_DG_Orders_Details_DG_Orders_LineItems_Quantity]  DEFAULT ((0)) FOR [DG_Orders_LineItems_Quantity]
GO
/****** Object:  Default [DF_DG_Orders_Details_DG_Orders_Details_Items_UniPrice_1]    Script Date: 01/05/2013 14:44:39 ******/
ALTER TABLE [dbo].[DG_Orders_Details] ADD  CONSTRAINT [DF_DG_Orders_Details_DG_Orders_Details_Items_UniPrice_1]  DEFAULT ((0)) FOR [DG_Orders_Details_Items_UniPrice]
GO
/****** Object:  Default [DF_DG_Orders_Details_DG_Orders_Details_ProductType_pkey_1]    Script Date: 01/05/2013 14:44:39 ******/
ALTER TABLE [dbo].[DG_Orders_Details] ADD  CONSTRAINT [DF_DG_Orders_Details_DG_Orders_Details_ProductType_pkey_1]  DEFAULT ((-1)) FOR [DG_Orders_Details_ProductType_pkey]
GO
/****** Object:  Default [DF_DG_Orders_Details_DG_Orders_Details_LineItem_ParentID_1]    Script Date: 01/05/2013 14:44:39 ******/
ALTER TABLE [dbo].[DG_Orders_Details] ADD  CONSTRAINT [DF_DG_Orders_Details_DG_Orders_Details_LineItem_ParentID_1]  DEFAULT ((-1)) FOR [DG_Orders_Details_LineItem_ParentID]
GO
/****** Object:  Default [DF_DG_Orders_Details_DG_Orders_Details_LineItem_PrinterReferenceID]    Script Date: 01/05/2013 14:44:39 ******/
ALTER TABLE [dbo].[DG_Orders_Details] ADD  CONSTRAINT [DF_DG_Orders_Details_DG_Orders_Details_LineItem_PrinterReferenceID]  DEFAULT ((-1)) FOR [DG_Orders_Details_LineItem_PrinterReferenceID]
GO
/****** Object:  Default [DF_DG_Orders_DG_Orders_Date]    Script Date: 01/05/2013 14:44:39 ******/
ALTER TABLE [dbo].[DG_Orders] ADD  CONSTRAINT [DF_DG_Orders_DG_Orders_Date]  DEFAULT (getdate()) FOR [DG_Orders_Date]
GO
/****** Object:  Default [DF_DG_Orders_DG_Orders_PaymentMode]    Script Date: 01/05/2013 14:44:39 ******/
ALTER TABLE [dbo].[DG_Orders] ADD  CONSTRAINT [DF_DG_Orders_DG_Orders_PaymentMode]  DEFAULT ((0)) FOR [DG_Orders_PaymentMode]
GO
/****** Object:  Default [DF_DG_Orders_DG_Orders_Canceled]    Script Date: 01/05/2013 14:44:39 ******/
ALTER TABLE [dbo].[DG_Orders] ADD  CONSTRAINT [DF_DG_Orders_DG_Orders_Canceled]  DEFAULT ((0)) FOR [DG_Orders_Canceled]
GO
