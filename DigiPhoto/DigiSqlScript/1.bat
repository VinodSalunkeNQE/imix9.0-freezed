@echo off
 cls
 
set /p SName=Server Name :
set /p UName=User Name :
set /p Pwd=Password :
set /p DbName=Database Name :
set /p Location=Location Name :
set /p Store=Store Name : 

set /p choice=ARE YOU SURE TO EXECUTE SCRIPTS in %DbName% (y/n) ?
 
if '%choice%'=='y' goto begin
 goto end
 
:begin
 if exist _Deploy.txt del _Deploy.txt
 
@echo on
 
sqlcmd -S %SName% -U %UName% -P %Pwd% -d %DbName% -I -i GenerateDigi.sql >> _Deploy.txt 2>&1


sqlcmd -Q "exec SP_InsertDefaultData @LocationName='%Location%', @StoreName='%Store%'" -S %SName% -d DigiPhoto 
 


@notepad _Deploy.txt
 

:end
