﻿using System;
using System.IO;
using System.Linq;
using System.Text;
using System.Collections.Generic;
using System.Windows.Media.Imaging;
using DigiPhoto.IMIX.Model;
using DigiPhoto.IMIX.Business;
using System.Configuration;

namespace DigiPhoto
{
    /// <summary>
    /// Developed by Kailash Chandra Behera
    /// On 06 MAY 2019
    /// For capture images from webcam to face recognization
    /// </summary>
    class WebCamHelper
    {

        //Block Memory Leak
        [System.Runtime.InteropServices.DllImport("gdi32.dll")]
        public static extern bool DeleteObject(IntPtr handle);
        public static BitmapSource bs;
        public static IntPtr ip;
        public static string MasterImagePath;
        public static BitmapSource LoadBitmap(System.Drawing.Bitmap source)
        {

            ip = source.GetHbitmap();

            bs = System.Windows.Interop.Imaging.CreateBitmapSourceFromHBitmap(ip, IntPtr.Zero, System.Windows.Int32Rect.Empty,

                System.Windows.Media.Imaging.BitmapSizeOptions.FromEmptyOptions());

            DeleteObject(ip);

            return bs;

        }
        public static void SaveImageCapture(BitmapSource bitmap)
        {
            JpegBitmapEncoder encoder = new JpegBitmapEncoder();
            encoder.Frames.Add(BitmapFrame.Create(bitmap));
            encoder.QualityLevel = 100;

            ConfigBusiness conBiz = new ConfigBusiness();
            ConfigurationInfo Item1 = conBiz.GetDeafultPathData();

            // Process save file dialog box results
            if (Item1 != null)
            {
                if (!string.IsNullOrEmpty(Item1.DG_Hot_Folder_Path))
                {
                    if (Directory.Exists(Item1.DG_Hot_Folder_Path) == true)
                    {
                        string masterimagefolder = Path.Combine(Item1.DG_Hot_Folder_Path, "MasterImages");
                        //string masterimagefolder = ConfigurationManager.AppSettings["MasterImagePath"];//  @"\\DEI_DT_02\MasterImage";

                        if (Directory.Exists(masterimagefolder) == false)
                            Directory.CreateDirectory(masterimagefolder);

                        string datefolder = DateTime.Now.Year.ToString() + new string('0', (2 - DateTime.Now.Month.ToString().Length)) + DateTime.Now.Month.ToString() + new string('0', (2 - DateTime.Now.Day.ToString().Length)) + DateTime.Now.Day.ToString();

                        masterimagefolder = Path.Combine(masterimagefolder, datefolder);
                        if (Directory.Exists(masterimagefolder) == false)
                            Directory.CreateDirectory(masterimagefolder);
                        //Path.Combine(Item1.DG_Hot_Folder_Path);

                        // Save Image
                        string filename = "MI" + DateTime.Now.Ticks.ToString() + ".jpg";
                        //filename = Path.Combine(masterimagefolder, filename);
                        FileStream fstream = new FileStream(Path.Combine(masterimagefolder, filename), FileMode.Create);
                        //FileStream fstream = new FileStream(filename, FileMode.Create);
                        encoder.Save(fstream);
                        fstream.Close();
                        //string localpath = System.IO.Path.Combine(ConfigurationManager.AppSettings["MasterImagePath"], datefolder);
                        //filename ="D:\\MasterImage\\"+datefolder+"\\"+ filename;
                        MasterImagePath = System.IO.Path.Combine(masterimagefolder, filename);
                    }
                }
            }
        }
    }
}
