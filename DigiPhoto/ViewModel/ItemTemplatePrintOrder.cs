﻿using System;
using System.ComponentModel;
using System.Data.Entity.Core.Objects.DataClasses; // Code Added for 7.0
namespace DigiPhoto.Calender
{

    public partial class ItemTemplatePrintOrder : EntityObject, INotifyPropertyChanged, INotifyPropertyChanging
    {

        public static long AddPrintOrder(ItemTemplatePrintOrder order)
        {
            long id = 0l;
            return id;
            //using (DigiphotoEntities _db = new DigiphotoEntities())
            //{
            //    _db.ItemTemplatePrintOrders.AddObject(order);
            //    _db.SaveChanges();
            //}
            //return order.Id;
        }

        public static void GetCalenderPages(long printerQueueId, string digiFolderThumbnailPath, string templatePath)
        {
            //using (DigiphotoEntities _db = new DigiphotoEntities())
            //{
            //    var data = from pq in _db.DG_PrinterQueue
            //               join itpo in _db.ItemTemplatePrintOrders on pq.DG_PrinterQueue_Pkey equals itpo.PrinterQueueId
            //               join itd in _db.ItemTemplateDetails on itpo.DetailTemplateId equals itd.Id
            //               join ph in _db.DG_Photos on itpo.PhotoId equals ph.DG_Photos_pkey
            //               where pq.DG_PrinterQueue_Pkey == printerQueueId
            //               select new
            //               {
            //                   pq.DG_PrinterQueue_Pkey,
            //                   pq.DG_Order_Details_Pkey,
            //                   ItemTemplateId = itd.Id,
            //                   itpo.PhotoId,
            //                   PhotoFilePath = digiFolderThumbnailPath + "\\" + ph.DG_Photos_FileName,
            //                   ph.DG_Photos_CreatedOn, 
            //                   //PhotoFilePath = digiFolderThumbnailPath + ph.DG_Photos_CreatedOn.Year.ToString() + ph.DG_Photos_CreatedOn.Month.ToString() + ph.DG_Photos_CreatedOn.Day.ToString() + "\\" + ph.DG_Photos_FileName,
            //                   TemplateFilePath = templatePath + itd.FilePath
            //               };

            //    var result = data.ToList();
            //    foreach (var item in result)
            //    {
                    
            //    }
            //}
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public event PropertyChangingEventHandler PropertyChanging;

        protected void NotifyPropertyChanged(String info)
        {
            info = info.Replace("get_", "").Replace("set_", "");
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(info));
            }
        }

        protected void NotifyPropertyChanging(String info)
        {
            info = info.Replace("get_", "").Replace("set_", "");
            if (PropertyChanging != null)
            {
                PropertyChanging(this, new PropertyChangingEventArgs(info));
            }
        }
    }


}
