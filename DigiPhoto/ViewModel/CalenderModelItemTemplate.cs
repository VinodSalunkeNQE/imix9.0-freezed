﻿namespace DigiPhoto.Calender
{
    using System;
    using System.Collections.Generic;

    public partial class ItemTemplateMaster
    {
        public ItemTemplateMaster()
        {
            this.ItemTemplateDetails = new HashSet<ItemTemplateDetail>();
            this.ItemTemplatePrintOrders = new HashSet<ItemTemplatePrintOrder>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public Int32 SubtemplateCount { get; set; }
        public Int32 PathType { get; set; }
        public Int32 TemplateType { get; set; }
        public string ThumbnailImagePath { get; set; }
        public string ThumbnailImageName { get; set; }
        public Int32 Status { get; set; }
        //public string CreatedBy { get; set; }
        //public string ModifiedBy { get; set; }
        //public Nullable<System.DateTime> CreatedDate { get; set; }
        //public Nullable<System.DateTime> ModifiedDate { get; set; }

        public   ICollection<ItemTemplateDetail> ItemTemplateDetails { get; set; }
        public virtual ICollection<ItemTemplatePrintOrder> ItemTemplatePrintOrders { get; set; }
    }
}