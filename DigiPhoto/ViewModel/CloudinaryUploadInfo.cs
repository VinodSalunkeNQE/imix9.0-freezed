﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DigiPhoto.ViewModel
{
    public class CloudinaryUploadInfo
    {
        public Int64 PhotoID { get; set; }
        public string CloudinaryPublicID { get; set; }
        public Int16 CloudinaryStatusID { get; set; }
        public int RetryCount { get; set; }
        public string ErrorMessage { get; set; }
        public Int32 Height { get; set; }
        public Int32 Width { get; set; }
        public string ThumbnailDimension { get; set; }

    }
}
