﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Linq.Expressions;
using System.Runtime.Remoting.Messaging;
using System.Collections.ObjectModel;
using System.IO;
//using  DigiphotoEntities=DigiPhoto.DataLayer.Model.DigiphotoEntities1;
using DigiPhoto.IMIX.Model;
using DigiPhoto.IMIX.Business;

namespace DigiPhoto.Calender
{
    public partial class ItemTemplateMaster : INotifyPropertyChanged, INotifyPropertyChanging
    {
        public string _BasePath;
        public string BasePath
        {
            get { return _BasePath; }
            set
            {
                if (_BasePath != value)
                {
                    NotifyPropertyChanging(MethodBase.GetCurrentMethod().Name);
                    NotifyPropertyChanging("ThumbnailImageFullPath");
                    _BasePath = value;
                    NotifyPropertyChanged("ThumbnailImageFullPath");
                    NotifyPropertyChanged(MethodBase.GetCurrentMethod().Name);
                }
            }
        }

        public ObservableCollection<ItemTemplateDetail> _ItemTemplateDetailList;
        public ObservableCollection<ItemTemplateDetail> ItemTemplateDetailList
        {
            get { return _ItemTemplateDetailList; }
            set
            {
                NotifyPropertyChanging(MethodBase.GetCurrentMethod().Name);
                _ItemTemplateDetailList = value;
                NotifyPropertyChanged(MethodBase.GetCurrentMethod().Name);
            }
        }

        public string ThumbnailImageFullPath
        {
            get
            {
                string path = this.BasePath + this.ThumbnailImagePath;
                return File.Exists(path) ? path : null;
            }
        }

        public void LoadDetailsAsync()
        {
            LoadDetailsHandler _LoadDetailsHandler = new LoadDetailsHandler(LoadDetails);
            IAsyncResult result = _LoadDetailsHandler.BeginInvoke(new AsyncCallback(LoadDetailsCallback), null);
        }

        public List<ItemTemplateDetail> LoadDetails()
        {
            List<ItemTemplateDetail> result = new List<ItemTemplateDetail>();
            List<ItemTemplateDetailModel> objImixModel = new List<ItemTemplateDetailModel>();
            objImixModel = (new CalenderBusiness()).GetItemTemplateDetail();

            foreach (ItemTemplateDetailModel obj in objImixModel)
            {
                ItemTemplateDetail objEnt = new ItemTemplateDetail()
                {
                    AssociationPhotoFileName = obj.AssociationPhotoFileName,
                    AssociationPhotoFilePath = obj.AssociationPhotoFilePath,
                    AssociationPhotoId = obj.AssociationPhotoId,
                    AssociationPhotoName = obj.AssociationPhotoName,
                    FileName = obj.FileName,
                    FileOrder = obj.FileOrder,
                    FilePath = obj.FilePath,
                    I1_X1 = obj.I1_X1,
                    I1_X2 = obj.I1_X2,
                    I1_Y1 = obj.I1_Y1,
                    I1_Y2 = obj.I1_Y2,
                    Id = obj.Id,
                    MasterTemplateId = obj.MasterTemplateId,
                    Status = obj.Status,
                    TemplateType = obj.TemplateType
                };
                result.Add(objEnt);
            }

            //using (DigiphotoEntities _db = new DigiphotoEntities())
            //{
            //    result = _db.ItemTemplateDetails.Where(i => i.MasterTemplateId == this.Id).ToList();
            //}
            // return result;
            return result.Where(x => x.MasterTemplateId == this.Id).ToList();
        }

        public void LoadDetailsCallback(IAsyncResult result)
        {
            AsyncResult _result = result as AsyncResult;
            LoadDetailsHandler _LoadDetailsHandler = _result.AsyncDelegate as LoadDetailsHandler;
            object resultData = _LoadDetailsHandler.EndInvoke(result);
            this.ItemTemplateDetailList = new ObservableCollection<ItemTemplateDetail>(resultData as List<ItemTemplateDetail>);
            if (OperationDoneEvent != null)
                OperationDoneEvent(this, CalenderViewOperationType.LoadCalenderDetailData);
        }

        /// <summary>
        /// used to construct the full image paths
        /// </summary>
        /// <param name="basePath"></param>
        public void SetBaseFilePath(string basePath)
        {
            this.BasePath = basePath;
        }

        /// <summary>
        /// retrives all the data item template master data 
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        public static List<ItemTemplateMaster> GetItemTemplateMaster(Expression<Func<ItemTemplateMaster, bool>> filter = null)
        {
            List<ItemTemplateMaster> result = null;
            //using (DigiphotoEntities _db = new DigiphotoEntities())
            //{
            //    if (filter != null)
            //        result = _db.ItemTemplateMasters.Where(filter).ToList();
            //}
            CalenderBusiness objBizz = new CalenderBusiness();
            List<ItemTemplateMasterModel> objImixModel = objBizz.GetItemTemplateMaster();
            List<ItemTemplateDetailModel> objIMIXItemDetail = objBizz.GetItemTemplateDetail();

            foreach (ItemTemplateMasterModel obj in objImixModel)
            {
                ItemTemplateMaster objEnt = new ItemTemplateMaster()
                {
                    Id = obj.Id,
                    Name = obj.Name,
                    Description = obj.Description,
                    SubtemplateCount = obj.SubtemplateCount,
                    PathType = obj.PathType,
                    TemplateType = obj.TemplateType,
                    ThumbnailImagePath = obj.ThumbnailImagePath,
                    ThumbnailImageName = obj.ThumbnailImageName,
                    Status = obj.Status,
                    ItemTemplateDetails = CopyItemTemplateDetailsFromImix(objIMIXItemDetail, obj.Id)
                };
                result.Add(objEnt);
            }


            return result;
        }

        /// <summary>
        /// retrives all the data item calender template master data (where TemplateType==1)
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        public static List<ItemTemplateMaster> GetItemCalenderMaster(Expression<Func<ItemTemplateMaster, bool>> filter = null)
        {
            // List<ItemTemplateMaster> result = null;
            //using (DigiphotoEntities _db = new DigiphotoEntities())
            //{
            //    if (filter != null)
            //    {
            //        var query = _db.ItemTemplateMasters.Where(filter);
            //        result = query.Where(i => i.TemplateType == 1).ToList();
            //    }
            //    else
            //    {
            //        result = _db.ItemTemplateMasters.Where(i => i.TemplateType == 1).ToList();
            //    }
            //}
            List<ItemTemplateMaster> result = new List<ItemTemplateMaster>();
            //using (DigiphotoEntities _db = new DigiphotoEntities())
            //{
            //    if (filter != null)
            //        result = _db.ItemTemplateMasters.Where(filter).ToList();
            //}
            CalenderBusiness objBizz = new CalenderBusiness();
            List<ItemTemplateMasterModel> objImixModel = objBizz.GetItemTemplateMaster();
            List<ItemTemplateDetailModel> objIMIXItemDetail = objBizz.GetItemTemplateDetail();

            foreach (ItemTemplateMasterModel obj in objImixModel)
            {
                ItemTemplateMaster objEnt = new ItemTemplateMaster()
                {
                    Id = obj.Id,
                    Name = obj.Name,
                    Description = obj.Description,
                    SubtemplateCount = obj.SubtemplateCount,
                    PathType = obj.PathType,
                    TemplateType = obj.TemplateType,
                    ThumbnailImagePath = obj.ThumbnailImagePath,
                    ThumbnailImageName = obj.ThumbnailImageName,
                    Status = obj.Status,
                    ItemTemplateDetails = CopyItemTemplateDetailsFromImix(objIMIXItemDetail, obj.Id)
                };
                result.Add(objEnt);
            }

            if (filter != null)
            {
                //var query = result.Where(filter);
                result = result.Where(i => i.TemplateType == 1).ToList();
            }
            else
            {
                result = result.Where(i => i.TemplateType == 1).ToList();
            }


            return result;
            //return result;
        }


        public event PropertyChangedEventHandler PropertyChanged;
        public event PropertyChangingEventHandler PropertyChanging;

        protected void NotifyPropertyChanged(String info)
        {
            info = info.Replace("get_", "").Replace("set_", "");
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(info));
            }
        }

        protected void NotifyPropertyChanging(String info)
        {
            info = info.Replace("get_", "").Replace("set_", "");
            if (PropertyChanging != null)
            {
                PropertyChanging(this, new PropertyChangingEventArgs(info));
            }
        }

        public delegate List<ItemTemplateDetail> LoadDetailsHandler();
        public delegate void OperationDoneHandler(ItemTemplateMaster sender, CalenderViewOperationType operation);
        public event OperationDoneHandler OperationDoneEvent;

        private static List<ItemTemplateDetail> CopyItemTemplateDetailsFromImix(List<ItemTemplateDetailModel> lstItemDetail, Int32 idImix)
        {
            List<ItemTemplateDetail> lstImixModel = new List<ItemTemplateDetail>();
            List<ItemTemplateDetailModel> ItemTemplateDetaillst = lstItemDetail.Where(x => x.MasterTemplateId == idImix).ToList();
            foreach (ItemTemplateDetailModel obj in ItemTemplateDetaillst)
            {
                ItemTemplateDetail objEnt = new ItemTemplateDetail()
                {
                    AssociationPhotoFileName = obj.AssociationPhotoFileName,
                    AssociationPhotoFilePath = obj.AssociationPhotoFilePath,
                    AssociationPhotoId = obj.AssociationPhotoId,
                    AssociationPhotoName = obj.AssociationPhotoName,
                    FileName = obj.FileName,
                    FileNameWithotExtension = obj.FileName.Replace(".jpg", string.Empty).Replace(".JPG", string.Empty),
                    FileOrder = obj.FileOrder,
                    FilePath = obj.FilePath,
                    I1_X1 = obj.I1_X1,
                    I1_X2 = obj.I1_X2,
                    I1_Y1 = obj.I1_Y1,
                    I1_Y2 = obj.I1_Y2,
                    Id = obj.Id,
                    MasterTemplateId = obj.MasterTemplateId,
                    Status = obj.Status,
                    TemplateType = obj.TemplateType
                };
                lstImixModel.Add(objEnt);
            }
            return lstImixModel;

        }
    }


    public enum CalenderViewOperationType
    {
        LoadCalenderMasterData,
        LoadCalenderDetailData
    }


}
