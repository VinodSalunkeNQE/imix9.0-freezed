﻿ namespace DigiPhoto.Calender
{
    using System;
    using System.Collections.Generic;
    
    public partial class ItemTemplateDetail
    {
        public ItemTemplateDetail()
        {
            this.ItemTemplatePrintOrders = new HashSet<ItemTemplatePrintOrder>();
        }
   // AssociationPhotoFilePath
        public string AssociationPhotoFilePath { get; set; }
         public string AssociationPhotoFileName { get; set; }
         public Int32  AssociationPhotoId { get; set; }
        //AssociationPhotoName
         public string  AssociationPhotoName { get; set; }
        public Int32 Id { get; set; }
        public  Int32 MasterTemplateId { get; set; }
        public Int32 TemplateType { get; set; }
        public string FileName { get; set; }
        public string FileNameWithotExtension { get; set; }
        public string FilePath { get; set; }
        public Int32 Status { get; set; }
        //public string CreatedBy { get; set; }
        //public string ModifiedBy { get; set; }
        //public Nullable<System.DateTime> CreatedDate { get; set; }
        //public Nullable<System.DateTime> ModifiedDate { get; set; }
        public  Int32 I1_X1 { get; set; }
        public Int32 I1_Y1 { get; set; }
        public Int32 I1_X2 { get; set; }
        public Int32 I1_Y2 { get; set; }
        public Int32 FileOrder { get; set; }
    
        public virtual ItemTemplateMaster ItemTemplateMaster { get; set; }
        public virtual ICollection<ItemTemplatePrintOrder> ItemTemplatePrintOrders { get; set; }
    }
}