﻿using DigiPhoto.IMIX.Business;
using DigiPhoto.IMIX.Model;
using RelayCommandClass;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media.Effects;

namespace DigiPhoto.ViewModel
{
    public class WaterMarkViewModel : WatermarkModel
    {
       
        private ICommand checkCommand;
        BlurEffect effect = new BlurEffect();
        

        public ICommand CheckCommand
        {
            get
            {
                if (checkCommand == null)
                    checkCommand = new RelayCommand(i => Checkprocess(i), null);
                return checkCommand;
            }
            set
            {
                checkCommand = value;
                OnPropertyChanged("CheckCommand");
            }
        }
        public void Checkprocess(object sender)
        {
            //this DOES react when the checkbox is checked or unchecked
            effect.Radius =(IsChecked==true ? 5 : 0);
        }
        public WaterMarkViewModel()
        {
            IsChecked = false;
            effect.Radius = 0;
            Blureffect = effect;
        }
        private void GetClientWatermarkConfig(int SubStoreId)
        {
            ConfigBusiness configBiz = new ConfigBusiness();
            WatermarkModel objWatermarkModel = new WatermarkModel();
            ConfigBusiness conBiz = new ConfigBusiness();
            objWatermarkModel = configBiz.GetClientWatermarkConfig(SubStoreId);
            //watermark.Text = objWatermarkModel.WatermarkText;
           
            //watermark.FontSize = objWatermarkModel.TextFontSize;
            //watermark.FontWeight = (FontWeight)new FontWeightConverter().ConvertFromString(objWatermarkModel.TextFontWeights);
            //watermark.FontFamily = (FontFamily)new FontFamilyConverter().ConvertFromString(objWatermarkModel.TextFontfamilyNames);
            //var converter = new System.Windows.Media.BrushConverter();
            //watermark.Foreground = (Brush)converter.ConvertFromString(objWatermarkModel.TextFontColor.ToString());
            //watermark.Opacity =Convert.ToDouble(objWatermarkModel.Opacity);

        }
      
    }
}
