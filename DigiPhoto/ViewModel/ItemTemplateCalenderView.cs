﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Reflection;
using DigiPhoto.IMIX.Model;
//using DigiPhoto.DigiSync.Model.ItemTemplate;

namespace DigiPhoto.Calender
{
    /// <summary>
    /// this class is used to handle the view model for the Item - Calender Template
    /// </summary>
    public class ItemTemplateCalenderView : ViewModelBase, IDisposable
    {

        private List<ItemTemplateMaster> _ItemTemplateCalender;
        /// <summary>
        /// holds the list of calender template items
        /// </summary>
        public List<ItemTemplateMaster> ItemTemplateCalender
        {
            get { return _ItemTemplateCalender; }
            set
            {
                NotifyPropertyChanging(MethodBase.GetCurrentMethod().Name);
                _ItemTemplateCalender = value;
                NotifyPropertyChanged(MethodBase.GetCurrentMethod().Name);
            }
        }

        private List<LstMyItems> _PrintImages;
        /// <summary>
        /// holds the list of the items selected for printing -- from  RobotImageLoader.PrintImages
        /// </summary>
        public List<LstMyItems> PrintImages
        {
            get { return _PrintImages; }
            set
            {
                NotifyPropertyChanging(MethodBase.GetCurrentMethod().Name);
                _PrintImages = value;
                NotifyPropertyChanged(MethodBase.GetCurrentMethod().Name);
            }
        }

        private ObservableCollection<PrintOrderPage> _PrintOrderPageList;
        /// <summary>
        /// this property is used to hold the images that has been selected for printing
        /// </summary>
        public ObservableCollection<PrintOrderPage> PrintOrderPageList
        {
            get { return _PrintOrderPageList; }
            set
            {
                NotifyPropertyChanging(MethodBase.GetCurrentMethod().Name);
                _PrintOrderPageList = value;
                NotifyPropertyChanged(MethodBase.GetCurrentMethod().Name);
            }
        }

        ItemTemplateMaster _ItemTemplateCalenderLastSelected;
        ItemTemplateMaster _ItemTemplateCalenderSelected;
        /// <summary>
        /// this holds the selected calender template
        /// </summary>
        public ItemTemplateMaster ItemTemplateCalenderSelected
        {
            get { return _ItemTemplateCalenderSelected; }
            set
            {
                NotifyPropertyChanging(MethodBase.GetCurrentMethod().Name);
                _ItemTemplateCalenderLastSelected = _ItemTemplateCalenderSelected;
                _ItemTemplateCalenderSelected = value;
                BuildDetailItems();
                NotifyPropertyChanged(MethodBase.GetCurrentMethod().Name);
            }
        }


        private ObservableCollection<LstMyItems> _LstMyItems;
        /// <summary>
        /// this property is used to hold the images that has been selected for printing
        /// </summary>
        public ObservableCollection<LstMyItems> LstMyItems
        {
            get { return _LstMyItems; }
            set
            {
                NotifyPropertyChanging(MethodBase.GetCurrentMethod().Name);
                _LstMyItems = value;
                NotifyPropertyChanged(MethodBase.GetCurrentMethod().Name);
            }
        }

        /// <summary>
        /// initlize the View 
        /// </summary>
        public ItemTemplateCalenderView()
        {
            this.PrintImages = RobotImageLoader.PrintImages; // getting selected images from the global reference
            this.ItemTemplateCalender = new List<ItemTemplateMaster>();
            this.PrintOrderPageList = new ObservableCollection<PrintOrderPage>();
        }


        private void BuildDetailItems()
        {
            if (_ItemTemplateCalenderSelected != null)
            {   
                _ItemTemplateCalenderSelected.LoadDetailsAsync();
            }         
        }

      

        /// <summary>
        /// used to copy the values from the source Image
        /// </summary>
        /// <param name="lstMyItem"></param>
        /// <param name="destination"></param>
        public void CopyMyListItem(LstMyItems source, ItemTemplateDetail destination, bool canCopyListPosition = false)
        {
            if (destination != null)
            {
                destination.AssociationPhotoId = source.PhotoId;
                destination.AssociationPhotoName = source.Name;
                destination.AssociationPhotoFileName = source.FileName;
                destination.AssociationPhotoFilePath = source.FilePath.Replace("Thumbnails", "Thumbnails_Big");
                if (canCopyListPosition)
                    destination.FileOrder = source.ListPosition;
            }
        }

        /// <summary>
        /// used to copy the values from the previous grid items
        /// </summary>
        /// <param name="lstMyItem"></param>
        /// <param name="destination"></param>
        public void CopyItemTemplateDetail(ItemTemplateDetail source, ItemTemplateDetail destination, bool canCopyListPosition = false)
        {
            if (destination != null)
            {
                destination.AssociationPhotoId = source.AssociationPhotoId;
                destination.AssociationPhotoName = source.AssociationPhotoName;
                destination.AssociationPhotoFileName = source.AssociationPhotoFileName;
                destination.AssociationPhotoFilePath = source.AssociationPhotoFilePath;
                if (canCopyListPosition)
                    destination.FileOrder = source.FileOrder;
            }
        }

      

        /// <summary>
        /// loads the calender template data
        /// </summary>
        public void LoadCalenderTemplate(string basePath = null)
        {
            this.ItemTemplateCalender = ItemTemplateMaster.GetItemCalenderMaster();
            if (string.IsNullOrWhiteSpace(basePath) == false)
            {
                foreach (var item in this.ItemTemplateCalender)
                {
                    item.BasePath = basePath;
                    item.OperationDoneEvent += _ItemTemplateCalender_OperationDoneEvent;
                }
            }
            // OperationDoneHandler _OperationDoneHandler = new OperationDoneHandler(this, CalenderViewOperationType.LoadCalenderMasterData); 
        }

        void _ItemTemplateCalender_OperationDoneEvent(ItemTemplateMaster sender, CalenderViewOperationType operation)
        {
            if (operation== CalenderViewOperationType.LoadCalenderDetailData && _ItemTemplateCalenderLastSelected != null)
            {
                foreach(var previousDetail in  _ItemTemplateCalenderLastSelected.ItemTemplateDetailList)
                {
                    if (sender.ItemTemplateDetailList != null)
                    {
                        var currentDetailFileOrder = sender.ItemTemplateDetailList.Where(i => i.FileOrder == previousDetail.FileOrder).SingleOrDefault();
                        CopyItemTemplateDetail(previousDetail,currentDetailFileOrder);
                    }
                }
            }
            if (OperationDoneEvent != null)
                OperationDoneEvent(this, CalenderViewOperationType.LoadCalenderDetailData);
        }



      

        public void Dispose()
        {
            PrintImages = null; // no need to clear this because it has a global reference to  RobotImageLoader.PrintImages
            if (ItemTemplateCalender != null)
                ItemTemplateCalender.Clear();
            ItemTemplateCalender = null;
            //PropertyChanged = null;
            //PropertyChanging = null;
        }

        public delegate void OperationDoneHandler(ItemTemplateCalenderView sender, CalenderViewOperationType operation);
        public event OperationDoneHandler OperationDoneEvent;


      
    }
}
