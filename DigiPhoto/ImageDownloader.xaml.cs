﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.IO;
using DigiPhoto.DataLayer.Model;
using DigiPhoto.DataLayer;
using System.Collections.ObjectModel;
using DigiPhoto.Common;
using System.Diagnostics;
using FrameworkHelper;
using DigiPhoto.IMIX.Business;
using DigiPhoto.IMIX.Model;
using System.Threading.Tasks;
using System.Windows.Threading;
using System.Windows.Controls.Primitives;
using WMPLib;
using System.Threading;
using DigiPhoto.Interop;
using System.Collections;
using DigiPhoto.DigiSync.Model;
using FrameworkHelper.Common;
using System.Xml.Linq;
using System.Reflection;
using System.Xml;
using ExifLib;
using LevDan.Exif;
using System.Configuration;

namespace DigiPhoto
{
    public partial class ImageDownloader : Window
    {
        #region Variables
        #region Private
        private int _selectStartIndex = -1;
        int _selectEndIndex = -1;
        //media extension for png file
        string[] mediaExtensions = new[] { ".jpg", ".avi", ".mp4", ".wmv", ".mov", ".3gp", ".3g2", ".m2v", ".m4v", ".flv", ".mpeg", ".ffmpeg", ".mkv", ".mts" , ".png" };
        //const string fileExtension = ".jpg";
        static string jpgExtension = ConfigurationManager.AppSettings["jpgFileExtension"];
        static string pngExtension = ConfigurationManager.AppSettings["pngFileExtension"];
        string fileExtension = jpgExtension;
        
        /// <summary>
        /// Process videos or not
        /// </summary>
        ///bool processVideos;
        /// <summary>
        /// The filepath
        /// </summary>
        ///string filepath;
        /// <summary>
        /// The image name
        /// </summary>
        ///private List<string> ImageName;
        /// <summary>
        /// The count
        /// </summary>
        ///static int count = 0;
        /// <summary>
        /// The processed count
        /// </summary>
        ///static int ProcessedCount = 0;

        private string _filePath;
        private string _path = string.Empty;
        private string _thumbnailsPath = string.Empty;
        private Stopwatch _stopwatch = new Stopwatch();
        private bool _isBarcodeActive = false;
        private bool _isUsbDelete = false;
        private bool _show = false;

        private Dictionary<string, DownloadFileInfo> _img = new Dictionary<string, DownloadFileInfo>();
        private System.ComponentModel.BackgroundWorker _manualDownloadWorker = new System.ComponentModel.BackgroundWorker();
        private BusyWindow _busyWindows = new BusyWindow();
        readonly DispatcherTimer plyTimer = new DispatcherTimer();
        //private bool userIsDraggingSlider = false;
        //private bool mediaPlayerIsPlaying = false;
        //bool CheckBoxState = true;
        string selectedImage = string.Empty;
        System.ComponentModel.BackgroundWorker ManualDownloadworker = new System.ComponentModel.BackgroundWorker();
        BusyWindow bs = new BusyWindow();
       // private FileStream memoryFileStream;
        static string vsMediaFileName = "";
        //bool isMuted = false;
        Hashtable htFileSize = new Hashtable();
        bool isVideoEditingEnabled = false;
        MLMediaPlayer mplayer;

        //private bool imgType = false;
        //private BitmapImage _bitMapImage;
        string ImageEffect = string.Empty;
        string ProductNamePreference = string.Empty;
        private List<SemiOrderSettings> lstDG_SemiOrder_Settings = new List<SemiOrderSettings>();
        private string hotFolderPath = string.Empty;
        private string dateFolder = string.Empty;
        private string cropFolderPath = string.Empty;
        private bool IsSpecActiveForSubstore = false;
        private List<ConfigurationInfo> lstConfig = null;
        private string originalDirectoryPath = String.Empty;
        string DateFolder = string.Empty;
        private Nullable<bool> _IsAutoRotate = false;
        private int _needRotation = 0;
        private ConfigurationInfo _configInfo = null;
        string binpath = AppDomain.CurrentDomain.BaseDirectory;
        string LayeringHorizontal = string.Empty;
        string LayeringVertical = string.Empty;
        #endregion
        #region Public
        public bool isImageAcquired = false;
        public Hashtable htVidLength = new Hashtable();
        public ObservableCollection<MyImageClass> MyImages { get; set; }
        public ObservableCollection<MyImageClass> UpdatedSelectedList { get; set; }

        public bool IsEnabledSessionSelect = false;
        public bool IsEnabledSelectEndPoint = false;
        public bool _isEnabledImageEditingMDownload = false;
        public string defaultContrast = "1";
        public string defaultBrightness = "0";
        #endregion
        #endregion

        #region Constructor
        public ImageDownloader()
        {
            try
            {
                InitializeComponent();
                GetConfigurationInfo();
                lstImages.Items.Clear();
                lstConfig = (new ConfigBusiness()).GetConfigurationData();
                ConfigurationInfo config = lstConfig.Where(x => x.DG_Substore_Id == LoginUser.SubStoreId).FirstOrDefault();
                //Start of Changes by Bhavin Udani for Bug : Path pointing to incorrect directory Date: 26112020
                _filePath = config.DG_Hot_Folder_Path;
                _path = _filePath + "\\";//AppDomain.CurrentDomain.BaseDirectory + "\\"; \\ Bhavin : Giving hotfolder instead of Basedirectory
                _path = _filePath + "Download\\"; //System.IO.Path.Combine(_path, "Download\\"); 
                _thumbnailsPath = System.IO.Path.Combine(_path, "Temp\\");
                //End of Changes by Bhavin Udani for Bug : Path pointing to incorrect directory Date: 26112020
                if (Directory.Exists(_path))
                    DeletePath(_path);
                _manualDownloadWorker.DoWork += new System.ComponentModel.DoWorkEventHandler(ManualDownloadworker_DoWork);
                _manualDownloadWorker.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(ManualDownloadworker_RunWorkerCompleted);

                if (CheckDriveForImages())
                {
                    _show = true;
                }
                else
                {
                    Home obj = new Home();
                    obj.Show();

                }
                btnacquire.IsDefault = true;
                hotFolderPath = AppDomain.CurrentDomain.BaseDirectory; //_objphoto.HotFolderPath;
                dateFolder = DateTime.Now.ToString("yyyyMMdd");
                cropFolderPath = System.IO.Path.Combine(hotFolderPath, dateFolder, "Croped");
                if (!Directory.Exists(cropFolderPath))
                    Directory.CreateDirectory(cropFolderPath);
                mplayer = new MLMediaPlayer(vsMediaFileName, "ImageDownloader");
                ManualCtrl.SetParent(this);
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
            
        }
        #endregion

        #region Private Functions
        private void ShowImages()
        {
            try
            {
                lstImages.Items.Clear();

                MyImages = new ObservableCollection<MyImageClass>();
                MyImages.Clear();

                string DateFolder = DateTime.Now.ToString("yyyyMMdd");
                if (_isEnabledImageEditingMDownload)
                {
                    try
                    {
                        if (Directory.Exists(binpath + DateFolder + "\\Thumbnails"))
                            Directory.Delete(binpath + DateFolder + "\\Thumbnails" , true);
                        if (Directory.Exists(binpath + DateFolder + "\\Temp"))
                            Directory.Delete(binpath + DateFolder + "\\Temp", true);
                        if (Directory.Exists(binpath + DateFolder + "\\Croped"))
                            Directory.Delete(binpath + DateFolder + "\\Croped", true);
                        if (Directory.Exists(binpath + DateFolder))
                            Directory.Delete(binpath + DateFolder, true);
                        if (!Directory.Exists(binpath + DateFolder))
                            Directory.CreateDirectory(binpath + DateFolder);
                        if (!Directory.Exists(binpath + DateFolder + "\\Thumbnails"))
                            Directory.CreateDirectory(binpath + DateFolder + "\\Thumbnails");
                        if (!Directory.Exists(binpath + DateFolder + "\\Croped"))
                            Directory.CreateDirectory(binpath + DateFolder + "\\Croped");
                        if (!Directory.Exists(binpath + DateFolder + "\\Temp"))
                            Directory.CreateDirectory(binpath + DateFolder + "\\Temp");
                        btnOpenEditControl.Visibility = Visibility.Visible;
                    }
                    catch(Exception c)
                    {
                        string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(c);
                        ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                    }
                }
                else
                    btnOpenEditControl.Visibility = Visibility.Collapsed;

                int count = 0;
                foreach (var item in _img)
                {
                    count++;
                    var objFileInfo = (DownloadFileInfo)item.Value;
                    string drivePath = string.Empty;
                    string filenamewithextension = System.IO.Path.GetFileName(objFileInfo.fileName);
                    string fileName = System.IO.Path.GetFileNameWithoutExtension(objFileInfo.fileName);
                    string displayFileName = fileName;
                    if (!objFileInfo.isVideo)
                    {
                        drivePath = objFileInfo.filePath;
                    }
                    else
                    {
                        drivePath = objFileInfo.videoPath;
                        displayFileName = System.IO.Path.GetFileNameWithoutExtension(displayFileName);
                    }
                    if (_isEnabledImageEditingMDownload && !objFileInfo.isVideo)
                    {
                        //i can put the logic to rotate image here
                        File.Copy(objFileInfo.filePath + "\\" + objFileInfo.fileName, binpath + DateFolder + "\\" + objFileInfo.fileName, true);
                        // rotateImage(binpath + DateFolder + "\\" + objFileInfo.fileName);

                        ResizeWPFImageThumbnails(objFileInfo.filePath + "\\" + objFileInfo.fileName, 1200, binpath + DateFolder + "\\Thumbnails\\" + objFileInfo.fileName);
                        drivePath = binpath + DateFolder;

                        string filenamewithext = System.IO.Path.GetFileName(objFileInfo.fileName);
                        MyImages.Add(new MyImageClass(displayFileName, GetImageFromResourceString(fileName, drivePath,filenamewithext), false, objFileInfo.CreatedDate, drivePath + "\\Thumbnails\\" + objFileInfo.fileName, objFileInfo.fileExtension, null, null, SettingStatus.None, count, false));
                    }
                    else
                    {
                        string filenamewithext = System.IO.Path.GetFileName(objFileInfo.fileName);
                        MyImages.Add(new MyImageClass(displayFileName, GetImageFromResourceString(fileName, objFileInfo.filePath, filenamewithextension), false, objFileInfo.CreatedDate, drivePath, objFileInfo.fileExtension));
                    }
                }
                lstImages.ItemsSource = MyImages;
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
            }
        }
        private bool CheckResetDPIRequired(int hr, int vr, string PhotographerId)
        {
            try
            {
                bool cameraSetting = (new CameraBusiness()).GetIsResetDPIRequired(Convert.ToInt32(PhotographerId));
                if ((hr != 300 || vr != 300) && cameraSetting)
                    return true;
                else
                    return false;
            }
            catch
            {
                return false;
            }
        }
        private void ResetImageDPI(string fileName)
        {
            try
            {
                string HorizontalResolution = "'" + "##" + "'";
                string VerticalResolution = "'" + "##" + "'";
                FileInfo file = new FileInfo(fileName);
                ExifTagCollection exif = new ExifTagCollection(file.FullName);
                bool isHorizontalresolutionset = false;
                bool isVerticalresolutionset = false;
                foreach (LevDan.Exif.ExifTag tag in exif)
                {
                    if (tag.Id == 282)
                    {
                        HorizontalResolution = "'" + exif[282].Value + "'";
                        isHorizontalresolutionset = true;
                        if (isVerticalresolutionset)
                            break;
                    }
                    else if (tag.Id == 283)
                    {
                        VerticalResolution = "'" + exif[283].Value + "'";
                        isVerticalresolutionset = true;
                        if (isHorizontalresolutionset)
                            break;
                    }
                }
                HorizontalResolution = HorizontalResolution.Replace("'", "");
                VerticalResolution = VerticalResolution.Replace("'", "");
                Int32 HorizontalDPI = 0;
                Int32 VerticalDPI = 0;
                if (HorizontalResolution.Contains("##") || VerticalResolution.Contains("##"))
                {
                    BitmapImage testbmp = new BitmapImage();
                    using (FileStream fileStream = File.OpenRead(file.FullName))
                    {
                        MemoryStream ms = new MemoryStream();
                        fileStream.CopyTo(ms);
                        ms.Seek(0, SeekOrigin.Begin);
                        fileStream.Close();
                        testbmp.BeginInit();
                        testbmp.StreamSource = ms;
                        testbmp.EndInit();
                        testbmp.Freeze();
                    }
                    HorizontalDPI = Convert.ToInt32(testbmp.DpiX);
                    VerticalDPI = Convert.ToInt32(testbmp.DpiY);
                }
                else
                {
                    //Imagica India was returning HorizontalResolution - 0.72 and VerticalResolution - 0.72
                    if (!Int32.TryParse(HorizontalResolution, out HorizontalDPI))
                        HorizontalDPI = 72;

                    if (!Int32.TryParse(VerticalResolution, out VerticalDPI))
                        VerticalDPI = 72;
                }
                if (HorizontalDPI != 300 || VerticalDPI != 300)
                    ResetLowDPI(file.FullName);
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogFileWrite(ex.Message);
            }
        }
        private void ResetLowDPI(string FilePath)
        {
            try
            {
                string file = System.IO.Path.GetFileName(FilePath);
                string Dirname = System.IO.Path.GetDirectoryName(FilePath);
                //string baseDirname = System.IO.Path.GetDirectoryName(Dirname);
                string InputFilePath = System.IO.Path.Combine(Dirname, "Temp");
                if (!Directory.Exists(InputFilePath))
                    Directory.CreateDirectory(InputFilePath);
                File.Move(System.IO.Path.Combine(Dirname, file), System.IO.Path.Combine(InputFilePath, file));
                //Dirname = System.IO.Path.GetDirectoryName(Dirname);
                //File.Move(FilePath, binpath + DateFolder + "\\Temp" + objFileInfo.fileName, true);
                System.Drawing.Image newImage = System.Drawing.Image.FromFile(System.IO.Path.Combine(InputFilePath, file));
                System.Drawing.Image outImage = ResetDPI.ScaleByHeightAndResolution(newImage, 2136, 300);
                outImage.Save(System.IO.Path.Combine(Dirname, file));
                newImage.Dispose();
                outImage.Dispose();
                File.Delete(System.IO.Path.Combine(InputFilePath, file));
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogFileWrite(ex.Message);
            }
        }
        private bool rotateImage(string rotatePath, int changed = 0)
        {
            bool isAutoRotated = false;
            try
            {
                _configInfo = (new ConfigBusiness()).GetConfigurationData(LoginUser.SubStoreId);
                _IsAutoRotate = _configInfo.DG_IsAutoRotate;
                if ((bool)_IsAutoRotate)
                {
                    //ExifReader reader = null;
                    string renderedTag = "";
                    try
                    {
                        using (var reader = new ExifReader(rotatePath))
                        {
                            foreach (ushort tagID in Enum.GetValues(typeof(ExifTags)))
                            {
                                object val;
                                if (reader.GetTagValue(tagID, out val))
                                {
                                    renderedTag = val.ToString();
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        ErrorHandler.ErrorHandler.LogFileWrite(ex.Message);
                    }

                    if (!string.IsNullOrEmpty(renderedTag))
                    {
                        _needRotation = GetRotationValue(renderedTag);
                    }
                    else
                    {
                        _needRotation = 0;
                    }
                }

                ImageMagickObject.MagickImage jmagic = new ImageMagickObject.MagickImage();

                if (_needRotation > 0)
                {

                    if (_needRotation == 90)
                    {
                        object[] o = new object[] { "-rotate", " 90 ", rotatePath };
                        jmagic.Mogrify(o);
                        o = null;
                    }
                    else if (_needRotation == 180)
                    {
                        object[] o = new object[] { "-rotate", " 180 ", rotatePath };
                        jmagic.Mogrify(o);
                        o = null;
                    }
                    else if (_needRotation == 270)
                    {
                        object[] o = new object[] { "-rotate", " 270 ", rotatePath };
                        jmagic.Mogrify(o);
                        o = null;
                    }
                    isAutoRotated = true;
                }
                jmagic = null;

            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogFileWrite(ex.Message);
            }
            return isAutoRotated;
        }
        private int GetRotationValue(string orientation)
        {
            switch (int.Parse(orientation))
            {
                case 1:
                    return 0;

                case 2:
                    return 0;

                case 3:
                    return 180;

                case 4:
                    return 180;

                case 5:
                    return 90;

                case 6:
                    return 90;

                case 7:
                    return 270;

                case 8:
                    return 270;
                default:
                    return 0;
            }
        }
        /// <summary>
        /// For resizing png image thumb nail for read and write 
        /// </summary>
        /// <param name="sourceImage"></param>
        /// <param name="maxHeight"></param>
        /// <param name="saveToPath"></param>
        private void ResizeWPFImageThumbnails(string sourceImage, int maxHeight, string saveToPath)
        {
            // added for png 
            bool flgpng = (sourceImage.Substring(sourceImage.Length - 4 ,4).ToLower()== pngExtension) ? true : false;
            try
            {
                BitmapImage bi = new BitmapImage();
                BitmapImage bitmapImage = new BitmapImage();
                using (FileStream fileStream = File.OpenRead(sourceImage.ToString()))
                {
                    MemoryStream ms = new MemoryStream();
                    fileStream.CopyTo(ms);
                    ms.Seek(0, SeekOrigin.Begin);
                    fileStream.Close();
                    bi.BeginInit();
                    bi.StreamSource = ms;
                    bi.EndInit();
                    bi.Freeze();
                    decimal ratio = Convert.ToDecimal(bi.Width) / Convert.ToDecimal(bi.Height);
                    int newWidth = Convert.ToInt32(maxHeight * ratio);
                    int newHeight = maxHeight;
                    ms.Seek(0, SeekOrigin.Begin);
                    bitmapImage.BeginInit();
                    bitmapImage.StreamSource = ms;
                    bitmapImage.DecodePixelWidth = newWidth;
                    bitmapImage.DecodePixelHeight = newHeight;
                    bitmapImage.EndInit();
                    bitmapImage.Freeze();
                }
                //for png
                if(!flgpng)
                {
                    using (var fileStreamForSave = new FileStream(saveToPath, FileMode.Create, FileAccess.ReadWrite, FileShare.ReadWrite))
                    {
                        JpegBitmapEncoder encoder = new JpegBitmapEncoder();
                        encoder.QualityLevel = 94;
                        encoder.Frames.Add(BitmapFrame.Create(bitmapImage));
                        encoder.Save(fileStreamForSave);
                    }
                }
                else
                {
                    var encoder = new PngBitmapEncoder();
                    encoder.Frames.Add(BitmapFrame.Create((bitmapImage)));
                    using (var fileStreamForSave = new FileStream(saveToPath, FileMode.Create, FileAccess.ReadWrite, FileShare.ReadWrite))
                    {
                        encoder.Save(fileStreamForSave);
                        fileStreamForSave.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
            finally
            {
                MemoryManagement.FlushMemory();
            }
        }
        /// <summary>
        /// For source image contains png 
        /// </summary>
        /// <param name="sourceImage"></param>
        /// <param name="maxHeight"></param>
        /// <param name="saveToPath"></param>
        private void ResizeWPFImage(string sourceImage, int maxHeight, string saveToPath)
        {
#if DEBUG
            var watch = FrameworkHelper.CommonUtility.Watch();
            //for png  
            //bool isPng = (sourceImage.Substring(sourceImage.Length - 4, 4).ToLower() == pngExtension) ? true : false;
            watch.Start();
#endif
            try
            {
                bool isPng = (sourceImage.Substring(sourceImage.Length - 4, 4).ToLower() == pngExtension) ? true : false;
                BitmapImage bi = new BitmapImage();
                BitmapImage bitmapImage = new BitmapImage();
                using (FileStream fileStream = File.OpenRead(sourceImage.ToString()))
                {
                    using (MemoryStream ms = new MemoryStream())
                    {
                        fileStream.CopyTo(ms);
                        ms.Seek(0, SeekOrigin.Begin);
                        fileStream.Close();
                        bi.BeginInit();
                        bi.StreamSource = ms;
                        bi.EndInit();
                        bi.Freeze();

                        decimal ratio = 0;
                        int newWidth = 0;
                        int newHeight = 0;

                        if (bi.Width >= bi.Height)
                        {
                            ratio = Convert.ToDecimal(bi.Width) / Convert.ToDecimal(bi.Height);
                            newWidth = maxHeight;
                            newHeight = Convert.ToInt32(maxHeight / ratio);
                        }
                        else
                        {
                            ratio = Convert.ToDecimal(bi.Height) / Convert.ToDecimal(bi.Width);
                            newHeight = maxHeight;
                            newWidth = Convert.ToInt32(maxHeight / ratio);
                        }

                        ms.Seek(0, SeekOrigin.Begin);
                        bitmapImage.BeginInit();
                        bitmapImage.StreamSource = ms;
                        bitmapImage.DecodePixelWidth = newWidth;
                        bitmapImage.DecodePixelHeight = newHeight;
                        bitmapImage.EndInit();
                        bitmapImage.Freeze();
                        fileStream.Close();
                    }
                }
                //added for png changes  for file access readwrite
                 if (!isPng)
                {
                    using (var fileStreamForSave = new FileStream(saveToPath, FileMode.Create, FileAccess.ReadWrite))
                    {
                        JpegBitmapEncoder encoder = new JpegBitmapEncoder();
                        encoder.QualityLevel = 94;
                        encoder.Frames.Add(BitmapFrame.Create(bitmapImage));
                        encoder.Save(fileStreamForSave);
                        fileStreamForSave.Close();
                    }
                }
                else
                {
                    var encoder = new PngBitmapEncoder();
                    encoder.Frames.Add(BitmapFrame.Create((bitmapImage)));
                    using (var fileStreamForSave = new FileStream(saveToPath, FileMode.Create, FileAccess.ReadWrite, FileShare.ReadWrite))
                    {
                        encoder.Save(fileStreamForSave);
                        fileStreamForSave.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
            }
#if DEBUG
            if (watch != null)
                FrameworkHelper.CommonUtility.WatchStop(this.GetType().FullName + "." + System.Reflection.MethodBase.GetCurrentMethod().Name + "|", watch);
#endif
        }
        private void DeletePath(string path)
        {
#if DEBUG
            var watch = FrameworkHelper.CommonUtility.Watch();
            watch.Start();
#endif
            try
            {
                string[] filePaths = Directory.GetFiles(path);
                foreach (var item in filePaths)
                {
                    try
                    {
                        if (File.Exists(item))
                        {
                            File.Delete(item);
                        }
                    }
                    catch (Exception ex)
                    {
                        ErrorHandler.ErrorHandler.LogError(ex);
                    }
                }

                if (Directory.Exists(path))
                {
                    Directory.Delete(path, true);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
            finally
            {
                if (Directory.Exists(path))
                {
                    try
                    {
                        Directory.Delete(path, true);
                    }
                    catch (Exception ex)
                    {
                        ErrorHandler.ErrorHandler.LogError(ex);
                    }
                }
            }
#if DEBUG
            if (watch != null)
                FrameworkHelper.CommonUtility.WatchStop(this.GetType().FullName + "." + System.Reflection.MethodBase.GetCurrentMethod().Name + "|", watch);
#endif
        }
        private bool CheckDriveForImages()
        {
            bool imgFound = false;
            bool vidFound = false;
            var drives = from drive in DriveInfo.GetDrives()
                         where drive.DriveType == DriveType.Removable && drive.IsReady == true
                         select drive;
            if (drives.Count() > 0)
            {
                _show = true;

                foreach (var drivesitem in drives)
                {
                    var items = Directory.EnumerateFiles(drivesitem.Name, "*.*", SearchOption.AllDirectories).Where(s => s.ToLower().EndsWith(jpgExtension)).ToList();
                    //searches png files from directories
                    var pngitems = Directory.EnumerateFiles(drivesitem.Name, "*.*", SearchOption.AllDirectories).Where(s => s.ToLower().EndsWith(pngExtension)).ToList();
                    var videoitems = Directory.EnumerateFiles(drivesitem.Name, "*.*", SearchOption.AllDirectories).Where(s => s.ToLower().EndsWith(".wmv") || s.ToLower().EndsWith(".mp4") || s.ToLower().EndsWith(".avi") || s.ToLower().EndsWith(".mov") || s.ToLower().EndsWith(".3gp") || s.ToLower().EndsWith(".3g2") || s.ToLower().EndsWith(".m2v") || s.ToLower().EndsWith(".m4v") || s.ToLower().EndsWith(".flv") || s.ToLower().EndsWith(".mpeg") || s.ToLower().EndsWith(".mkv") || s.ToLower().EndsWith(".mts")).ToList();
                    if (isVideoEditingEnabled)
                    {
                        if ((items.Count + videoitems.Count + pngitems.Count) > 0)
                        {
                            string msg = "Found\n\rImages: " + (items.Count + pngitems.Count) + " \n\rVideos: " + videoitems.Count + "\n\rProcessing starting....";
                            MessageBox.Show(msg, "iMIX", MessageBoxButton.OK, MessageBoxImage.Information);
                            imgFound = true;
                        }
                        else
                        {
                            MessageBox.Show("No file(s) found.");
                            _show = false;
                        }
                    }
                    else
                    {
                        if (items.Count > 0 ||pngitems.Count > 0)
                        {

                            string msg = "Found images: " + (items.Count + pngitems.Count)+ "\n\rProcessing starting....";
                            MessageBox.Show(msg, "iMIX", MessageBoxButton.OK, MessageBoxImage.Information);
                            imgFound = true;
                        }
                        else
                        {
                            MessageBox.Show("No file(s) found.");
                            _show = false;
                        }
                    }
                }
            }
            else
            {
                MessageBox.Show("No drive(s) find.");
                _show = false;
            }
            return imgFound;
        }
        void SelectSessionImages()
        {
#if DEBUG
            var watch = FrameworkHelper.CommonUtility.Watch();
            watch.Start();
#endif
            if (_selectStartIndex > -1 && _selectEndIndex > -1 && _selectStartIndex <= _selectEndIndex)
            {
                for (int index = _selectStartIndex; index <= _selectEndIndex; index++)
                {
                    if (((MyImageClass)lstImages.Items[index]).IsChecked == false)
                    {
                        ((MyImageClass)lstImages.Items[index]).IsChecked = true;
                        txbSelectedImages.Text = "Selected :" + (int.Parse(txbSelectedImages.Text.Split(':').Last().Trim()) + 1).ToString();
                    }
                }

                lstImages.Items.Refresh();
                if (int.Parse(txbSelectedImages.Text.Split(':').Last().Trim()) == lstImages.Items.Count)
                    chkSelectAll.IsChecked = true;
                else
                    chkSelectAll.IsChecked = false;
                if (!IsEnabledSelectEndPoint)
                    _selectStartIndex = _selectEndIndex = -1;
            }
#if DEBUG
            if (watch != null)
                FrameworkHelper.CommonUtility.WatchStop(this.GetType().FullName + "." + System.Reflection.MethodBase.GetCurrentMethod().Name + "|", watch);
#endif
        }
        /// <summary>
        /// checks png in resourcestring 
        /// </summary>
        /// <param name="imageName"></param>
        /// <param name="imageDrivePath"></param>
        /// <param name="fullfilename"></param>
        /// <returns></returns>
        private BitmapImage GetImageFromResourceString(string imageName, string imageDrivePath, string fullfilename  )
        {
            bool flgpng  = (fullfilename.Substring(fullfilename.Length - 4,4 ).ToLower() == pngExtension);
            var image = new BitmapImage();
            image.BeginInit();
            image.DecodePixelWidth = 150;
            image.CacheOption = BitmapCacheOption.OnLoad;
            image.UriSource = new Uri(imageDrivePath + "\\" + imageName + (flgpng? pngExtension : jpgExtension));            
            image.EndInit();
            return image;
        }
        private void CreateImages()
        {
#if DEBUG
            var watch = FrameworkHelper.CommonUtility.Watch();
            watch.Start();
#endif
            GC.AddMemoryPressure(20000);
            var drives = from drive in DriveInfo.GetDrives()
                         where drive.DriveType == DriveType.Removable && drive.IsReady == true
                         select drive;
            if (drives.Count() > 0)
            {
                foreach (var drivesitem in drives)
                {
                    try
                    {
                        DirectoryInfo dirInfo = new DirectoryInfo(drivesitem.Name);
                        FileInfo[] fileinfo = dirInfo.EnumerateFiles("*.*", SearchOption.AllDirectories).Where(f => mediaExtensions.Contains(f.Extension.ToLower())).ToArray();
                        try
                        {
                            #region Clear the file directory if it exists
                           if (Directory.Exists(_path))
                            {
                                if (Directory.Exists(_thumbnailsPath))
                                {
                                    string[] filePaths1 = Directory.GetFiles(_thumbnailsPath);
                                    foreach (var item in filePaths1)
                                    {
                                        try
                                        {
                                            if (File.Exists(item))
                                            {
                                                File.Delete(item);
                                            }
                                        }
                                        catch (Exception ex)
                                        {
                                            ErrorHandler.ErrorHandler.LogError(ex);
                                        }
                                    }
                                    Directory.Delete(_thumbnailsPath, true);
                                }
                                string[] filePaths = Directory.GetFiles(_path);
                                foreach (var item in filePaths)
                                {
                                    try
                                    {
                                        if (File.Exists(item))
                                        {
                                            File.Delete(item);
                                        }
                                    }
                                    catch (Exception ex)
                                    {
                                        ErrorHandler.ErrorHandler.LogError(ex);
                                    }
                                }
                                Directory.Delete(_path, true);
                            }
                            #endregion
                        }
                        catch(Exception n)
                        {
                            string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(n);
                            ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                        }

                        if (Directory.Exists(_path))
                            DeletePath(_path);

                        #region Create the file directory if it does not exists
                        if (!Directory.Exists(_path))
                            System.IO.Directory.CreateDirectory(_path);
                        if (fileinfo != null && fileinfo.Count() > 0 && _isBarcodeActive)
                        {

                            if (!Directory.Exists(_thumbnailsPath))
                                System.IO.Directory.CreateDirectory(_thumbnailsPath);
                        }

                        #endregion

                        foreach (var fileitem in fileinfo)
                        {
                            FileAttributes fileAttributes = File.GetAttributes(fileitem.FullName);
                            if ((fileAttributes & FileAttributes.ReadOnly) == FileAttributes.ReadOnly)
                                File.SetAttributes(fileitem.FullName, FileAttributes.Normal);
                            if (!fileitem.Name.Contains("Thumbs.db"))
                            {
                                //check the fileextension whether it is jpg or png  
                                if (fileitem.Extension.ToLower().Equals(jpgExtension) || fileitem.Extension.ToLower().Equals(pngExtension))
                                {
                                    originalDirectoryPath = fileitem.DirectoryName;
                                    if (_isBarcodeActive)
                                        ResizeWPFImage(System.IO.Path.Combine(fileitem.DirectoryName, fileitem.Name), 900, _thumbnailsPath + fileitem.Name);

                                    if (!_img.ContainsKey(fileitem.Name))
                                    {
                                        _img.Add(fileitem.Name, new DownloadFileInfo
                                        {
                                            CreatedDate = fileitem.CreationTime,
                                            isVideo = false,
                                            fileName = fileitem.Name,
                                            filePath = fileitem.DirectoryName,
                                            fileExtension = fileExtension
                                        });
                                    }
                                    
                                }
                                else
                                {
                                    if (isVideoEditingEnabled)
                                    {
                                        string FilePath = string.Empty;
                                        FilePath = MLHelpers.ExtractThumbnail(fileitem.FullName);
                                        if (!string.IsNullOrEmpty(FilePath))
                                            MLHelpers.ResizeImage(FilePath, 210, _path + fileitem.Name + fileExtension);

                                        long vLen = Convert.ToInt64(MLHelpers.VideoLength);
                                        if (!htVidLength.ContainsKey(fileitem.Name))
                                        {
                                            htVidLength.Add(fileitem.Name, vLen);
                                        }
                                        if (!_img.ContainsKey(fileitem.Name))
                                        {
                                            _img.Add(fileitem.Name, new DownloadFileInfo
                                            {
                                                CreatedDate = fileitem.CreationTime,
                                                isVideo = true,
                                                fileName = fileitem.Name + fileExtension,
                                                filePath = _path,
                                                videoPath = fileitem.DirectoryName,
                                                fileExtension = fileitem.Extension
                                            });

                                        }
                                    }

                                }

                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        ErrorHandler.ErrorHandler.LogError(ex);
                    }
                }

                GC.Collect();
                GC.RemoveMemoryPressure(20000);
            }


#if DEBUG
            if (watch != null)
                FrameworkHelper.CommonUtility.WatchStop(this.GetType().FullName + "." + System.Reflection.MethodBase.GetCurrentMethod().Name + "|", watch);
#endif
        }
        private void GetConfigurationInfo()
        {
            try
            {
                List<long> objList = new List<long>();
                objList.Add((long)ConfigParams.MktImgPath);
                objList.Add((long)ConfigParams.MktImgTimeInSec);
                objList.Add((long)ConfigParams.IsEnabledVideoEdit);
                objList.Add((long)ConfigParams.IsBarcodeActive);
                objList.Add((long)ConfigParams.IsDeleteFromUSB);
                objList.Add((long)ConfigParams.IsEnableImageEditingMDownload);
                objList.Add((long)ConfigParams.Brightness);
                objList.Add((long)ConfigParams.Contrast);

                List<iMIXConfigurationInfo> ConfigValuesList = (new ConfigBusiness()).GetNewConfigValues(LoginUser.SubStoreId).Where(o => objList.Contains(o.IMIXConfigurationMasterId)).ToList();
                if (ConfigValuesList != null || ConfigValuesList.Count > 0)
                {
                    for (int i = 0; i < ConfigValuesList.Count; i++)
                    {
                        switch (ConfigValuesList[i].IMIXConfigurationMasterId)
                        {
                            case (int)ConfigParams.IsEnabledVideoEdit:
                                isVideoEditingEnabled = ConfigValuesList[i].ConfigurationValue != null ? Convert.ToBoolean(ConfigValuesList[i].ConfigurationValue) : false;
                                break;
                            case (int)ConfigParams.IsBarcodeActive:
                                _isBarcodeActive = ConfigValuesList[i].ConfigurationValue != null ? Convert.ToBoolean(ConfigValuesList[i].ConfigurationValue) : false;
                                break;
                            case (int)ConfigParams.IsDeleteFromUSB:
                                _isUsbDelete = ConfigValuesList[i].ConfigurationValue != null ? Convert.ToBoolean(ConfigValuesList[i].ConfigurationValue) : false;
                                break;
                            case (int)ConfigParams.IsEnableImageEditingMDownload:
                                _isEnabledImageEditingMDownload = ConfigValuesList[i].ConfigurationValue != null ? Convert.ToBoolean(ConfigValuesList[i].ConfigurationValue) : false;
                                break;
                            case (int)ConfigParams.Contrast:
                                defaultContrast = ConfigValuesList[i].ConfigurationValue != null ? ConfigValuesList[i].ConfigurationValue : "1";
                                break;
                            case (int)ConfigParams.Brightness:
                                defaultBrightness = ConfigValuesList[i].ConfigurationValue != null ? ConfigValuesList[i].ConfigurationValue : "0";
                                break;
                            default:
                                break;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }
        private bool ApplyCrop(string fileName)
        {
            try
            {
                if (lstDG_SemiOrder_Settings.FirstOrDefault().DG_SemiOrder_IsCropActive == true)
                {
                    MemoryStream memoryStream = new MemoryStream();
                    byte[] fileBytes = File.ReadAllBytes(System.IO.Path.Combine(hotFolderPath, dateFolder, fileName));
                    memoryStream.Write(fileBytes, 0, fileBytes.Length);
                    memoryStream.Position = 0;
                    BitmapSource bs = BitmapFrame.Create(memoryStream);
                    double ratioValue = bs.PixelWidth / bs.Width;
                    CroppedBitmap cb;
                    string[] rectValues;
                    double x;
                    double y;
                    double width;
                    double height;
                    double m;
                    double n;
                    if(bs.Width>bs.Height)
                        rectValues = lstDG_SemiOrder_Settings.FirstOrDefault().HorizontalCropValues.Split(',');
                    else
                        rectValues = lstDG_SemiOrder_Settings.FirstOrDefault().VerticalCropValues.Split(',');

                    if (rectValues.Length == 4)
                    {
                        if (bs.Width > bs.Height)
                        {

                            if (Convert.ToDouble(rectValues[2]) > Convert.ToDouble(rectValues[3]))
                                m = Convert.ToDouble(rectValues[3]) / Convert.ToDouble(rectValues[2]);
                            else
                                m = Convert.ToDouble(rectValues[2]) / Convert.ToDouble(rectValues[3]);
                            x = ratioValue * Convert.ToDouble(rectValues[0]);
                            y = ratioValue * Convert.ToDouble(rectValues[1]);
                            width = ratioValue * Convert.ToDouble(rectValues[2]);
                            height = ratioValue * Convert.ToDouble(rectValues[3]);
                            m = GetActualCropRatio(m);
                            if (bs.PixelHeight < (y + height))
                            {
                                height = Convert.ToInt32(bs.PixelHeight - y);
                                if (height > width)
                                    width = m * height;
                                else
                                    width = height / m;
                            }
                        }
                        else
                        {
                            if (Convert.ToDouble(rectValues[2]) > Convert.ToDouble(rectValues[3]))
                                n = Convert.ToDouble(rectValues[3]) / Convert.ToDouble(rectValues[2]);
                            else
                                n = Convert.ToDouble(rectValues[2]) / Convert.ToDouble(rectValues[3]);
                            x = ratioValue * Convert.ToDouble(rectValues[0]);
                            y = ratioValue * Convert.ToDouble(rectValues[1]);
                            width = ratioValue * Convert.ToDouble(rectValues[2]);
                            height = ratioValue * Convert.ToDouble(rectValues[3]);
                            n = GetActualCropRatio(n);
                            if (bs.PixelWidth < (x + width))
                            {
                                if (height > width)
                                    width = n * height;
                                else
                                    width = height / n;
                                width = bs.PixelWidth - x;
                            }
                        }
                        width = Math.Round(width, 0);
                        height = Math.Round(height, 0);

                        // If Width & height is more than PixelWidth &  PixelHeight of image than adjust it.
                        if (x + width > bs.PixelWidth)
                            width = bs.PixelWidth - x;

                        if (y + height > bs.PixelHeight)
                            height = bs.PixelHeight - y;

                        try
                        {
                            cb = new CroppedBitmap(bs, new Int32Rect(Convert.ToInt32(x), Convert.ToInt32(y), Convert.ToInt32(width), Convert.ToInt32(height)));       //select region rect
                        }
                        catch (ArgumentException ex)
                        {
                            //System.ArgumentException: Value does not fall within the expected range When Int32Rect is large
                            // A CroppedBitmap with a SourceRect set to Int32Rect.Empty will render the entire image.
                            //A rectangle that is not fully within the bounds of the bitmap will not render.

                            cb = new CroppedBitmap(bs, Int32Rect.Empty);
                        }

                        MemoryStream mStream = new MemoryStream();
                        //checks whether it is png
                        if (fileName.Substring(fileName.Length - 4, 4).ToLower() == pngExtension)
                        {
                            var pencoder = new PngBitmapEncoder();
                            pencoder.Frames.Add(BitmapFrame.Create((cb)));
                            pencoder.Save(mStream);
                        }
                        else
                        {
                            JpegBitmapEncoder jEncoder = new JpegBitmapEncoder();
                            jEncoder.Frames.Add(BitmapFrame.Create(cb));  //the croppedBitmap is a CroppedBitmap object 
                            jEncoder.QualityLevel = 50;
                            jEncoder.Save(mStream);
                        }            
                        

                        System.Drawing.Image imgSave = System.Drawing.Image.FromStream(mStream);
                        System.Drawing.Bitmap bmSave = new System.Drawing.Bitmap(imgSave);
                        bmSave.SetResolution(300, 300);
                        bmSave.Save(System.IO.Path.Combine(cropFolderPath, fileName));
                        mStream.Dispose();
                        bs.Freeze();
                        bs = null;
                        memoryStream.Dispose();
                        return true;
                    }
                    else
                    {
                        string errorMessage = "Invalid HorizontalCropValues in Semiorder settings";
                        ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                        return false;
                    }
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                return false;
            }
        }
        private double GetActualCropRatio(double m)
        {
            if (m < 0.68 && m > 0.64)
                m = 0.6667;
            else if (m < 0.76 && m > 0.74)
                m = 0.75;
            else if (m < 0.81 && m > 0.79)
                m = 0.8;
            return m;
        }
        private void CreateXMLwithSpecPrintEffects(SemiOrderSettings objDG_SemiOrder_Settings)
        {
            //objDigiPhotoDataServices = new DigiPhotoDataServices();
            PhotoBusiness phBusiness = new PhotoBusiness();
            if (objDG_SemiOrder_Settings.DG_SemiOrder_Settings_AutoBright == false && !string.IsNullOrEmpty(defaultBrightness))
            {
                objDG_SemiOrder_Settings.DG_SemiOrder_Settings_AutoBright_Value = Convert.ToDouble(defaultBrightness);
            }

            if (objDG_SemiOrder_Settings.DG_SemiOrder_Settings_AutoContrast == false && !string.IsNullOrEmpty(defaultBrightness))
            {
                objDG_SemiOrder_Settings.DG_SemiOrder_Settings_AutoContrast_Value = Convert.ToDouble(defaultContrast);
            }
            int prdId = 0;
            if (objDG_SemiOrder_Settings.DG_SemiOrder_ProductTypeId.Contains(','))
                prdId = 1;
            else
                prdId = Convert.ToInt32(objDG_SemiOrder_Settings.DG_SemiOrder_ProductTypeId);
            string Layering = string.Empty;
            string BGHor = string.Empty;
            string BGVer = string.Empty;
            bool isGrn = false;
            if ((bool)objDG_SemiOrder_Settings.DG_SemiOrder_Settings_IsImageBG)
            {
                BGHor = objDG_SemiOrder_Settings.Background_Horizontal;
                BGVer = objDG_SemiOrder_Settings.Background_Vertical;
                isGrn = true;
            }

            SaveXml(Convert.ToString(objDG_SemiOrder_Settings.DG_SemiOrder_Settings_AutoBright_Value),
                    Convert.ToString(objDG_SemiOrder_Settings.DG_SemiOrder_Settings_AutoContrast_Value),
                    Convert.ToString(objDG_SemiOrder_Settings.ImageFrame_Horizontal), Convert.ToString(objDG_SemiOrder_Settings.ImageFrame_Vertical),
                    objDG_SemiOrder_Settings.ProductName, BGHor, BGVer,
                    objDG_SemiOrder_Settings.Graphics_layer_Horizontal, objDG_SemiOrder_Settings.Graphics_layer_Vertical,
                    objDG_SemiOrder_Settings.ZoomInfo_Horizontal, objDG_SemiOrder_Settings.ZoomInfo_Vertical,
                    (bool)objDG_SemiOrder_Settings.DG_SemiOrder_IsCropActive, prdId,
                    objDG_SemiOrder_Settings.TextLogo_Horizontal, objDG_SemiOrder_Settings.TextLogo_Vertical);
        }
        private void SaveXml(string brightValue, string contrValue, string borderHorizontal, string borderVertical, string ProductName, string BGHorizontal, string BGVertical, string graphicHorizontal, string graphicVertical, string ZoomDetailsHorizontal, string ZoomDetailsVertical,
           bool isCropActive, int ProductId, string textLogoHorizontal, string textLogoVertical)
        {
            StringBuilder PlayerXml = new StringBuilder();
            System.Xml.XmlDocument Xdocument = new System.Xml.XmlDocument();
            Xdocument.LoadXml(ImageEffect);

            System.Xml.XmlNodeList list = Xdocument.SelectNodes("//image");

            foreach (System.Xml.XmlElement XElement in list)
            {
                XElement.SetAttribute("brightness", brightValue);
            }

            foreach (System.Xml.XmlElement XElement in list)
            {
                XElement.SetAttribute("contrast", contrValue);
            }

            if (isCropActive)
            {
                foreach (System.Xml.XmlElement XElement in list)
                {
                    if (ProductId == 1)
                        XElement.SetAttribute("Crop", "6 * 8");
                    else if (ProductId == 2 || ProductId == 3)
                        XElement.SetAttribute("Crop", "8 * 10");
                    else if (ProductId == 30 || ProductId == 5)
                        XElement.SetAttribute("Crop", "4 * 6");
                    else if (ProductId == 98)
                        XElement.SetAttribute("Crop", "3 * 3");
                }
            }
            if (ProductName.Contains("6 * 8"))
                ProductNamePreference = "1";
            else if (ProductName.Contains("8 * 10"))
                ProductNamePreference = "2";
            else if (ProductName.Contains("4 * 6") || ProductName.Contains("(4x6) & QR") || ProductName.Contains("4 Small Wallets"))
                ProductNamePreference = "30";
            else
                ProductNamePreference = "2";

            string fname = string.Empty;
            
            string[] zoomdetails;
            string zoomFactor = "1";
            string canvasleft = "0";
            string canvastop = "0";
            string scalecentrex = "-1";
            string scalecentrey = "-1";
            // Vertical Layering Effects
            if (!String.IsNullOrWhiteSpace(ZoomDetailsVertical) && ZoomDetailsVertical.Contains(','))
            {
                zoomdetails = ZoomDetailsVertical.Split(',');
                zoomFactor = zoomdetails[0];
                canvasleft = zoomdetails[1];
                canvastop = zoomdetails[2];
                scalecentrex = zoomdetails[3];
                scalecentrey = zoomdetails[4];
            }
            
            fname = borderVertical;
            LayeringVertical = "<photo producttype='" + ProductNamePreference + "' zoomfactor='" + zoomFactor + "' border='" + fname + "' bg= '" + BGVertical + "' canvasleft='" + canvasleft + "' canvastop='" + canvastop + "' scalecentrex='" + scalecentrex + "' scalecentrey='" + scalecentrey + "'>" + graphicVertical + PlayerXml + textLogoVertical + "</photo>";
            
            //Horizontal Layering Effects
            if (!String.IsNullOrWhiteSpace(ZoomDetailsHorizontal) && ZoomDetailsHorizontal.Contains(','))
            {
                zoomdetails = ZoomDetailsHorizontal.Split(',');
                zoomFactor = zoomdetails[0];
                canvasleft = zoomdetails[1];
                canvastop = zoomdetails[2];
                scalecentrex = zoomdetails[3];
                scalecentrey = zoomdetails[4];
            }
            else
            {
                zoomFactor = "1";
                canvasleft = "0";
                canvastop = "0";
                scalecentrex = "-1";
                scalecentrey = "-1";
            }
            fname = borderHorizontal;
            LayeringHorizontal = "<photo producttype='" + ProductNamePreference + "' zoomfactor='" + zoomFactor + "' border='" + fname + "' bg= '" + BGHorizontal + "' canvasleft='" + canvasleft + "' canvastop='" + canvastop + "' scalecentrex='" + scalecentrex + "' scalecentrey='" + scalecentrey + "'>" + graphicHorizontal + PlayerXml + textLogoHorizontal + "</photo>";

            ImageEffect = Xdocument.InnerXml.ToString();
        }
        private BitmapImage GetBitmapImageFromPath(string value)
        {
            BitmapImage bi = new BitmapImage();
            try
            {
                if (value != null)
                {
                    using (FileStream fileStream = File.OpenRead(value.ToString()))
                    {
                        MemoryStream ms = new MemoryStream();
                        fileStream.CopyTo(ms);
                        ms.Seek(0, SeekOrigin.Begin);
                        fileStream.Close();
                        bi.BeginInit();
                        bi.StreamSource = ms;
                        bi.EndInit();
                    }
                }
                else
                {
                    bi = new BitmapImage();
                }
                return bi;
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
                return bi;
            }
        }
        private static DependencyObject RecursiveVisualChildFinder<T>(DependencyObject rootObject)
        {
            var child = VisualTreeHelper.GetChild(rootObject, 0);
            if (child == null) return null;

            return child.GetType() == typeof(T) ? child : RecursiveVisualChildFinder<T>(child);
        }
        #endregion

        #region Events Raised
        public void btnBack_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                MediaStop();
                isImageAcquired = false;
                MyImages.Clear();

                if (!Directory.Exists(_path)) return;

                if (Directory.Exists(_thumbnailsPath))
                {
                    string[] filePaths1 = Directory.GetFiles(_thumbnailsPath);
                    foreach (var item in filePaths1)
                    {
                        if (File.Exists(item))
                        {
                            File.Delete(item);
                        }
                    }
                    Directory.Delete(_thumbnailsPath, true);
                }
                string[] filePaths = Directory.GetFiles(_path);
                foreach (var item in filePaths)
                {
                    if (File.Exists(item))
                    {
                        File.Delete(item);
                    }
                }
                Directory.Delete(_path, true);
                Home objhome = new Home();
                objhome.Show();
                this.Close();
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
            }
        }
        private void btnacquire_Click(object sender, RoutedEventArgs e)
        {
#if DEBUG
            var watch = FrameworkHelper.CommonUtility.Watch();
            watch.Start();
#endif
            try
            {
                btnacquire.IsEnabled = false;
                btnBack.IsEnabled = false;
                chkSelectAll.IsEnabled = false;
                isImageAcquired = true;
                var allimages = (from item in MyImages
                                 select item).ToList();
                int selectedImageCount = allimages.Count(x => x.IsChecked == true);
                if (selectedImageCount > 0)
                {
                    MediaStop();
                    ManualCtrl.Visibility = Visibility.Collapsed;

                    ManualCtrl.Imagelist = allimages.FindAll(x => x.IsChecked == true);
                    btnacquire.IsDefault = false;
                    ManualCtrl.btnDownload.IsDefault = true;
                    ManualCtrl.htVidL = this.htVidLength;
                    var res = ManualCtrl.ShowHandlerDialog();
                    btnacquire.IsDefault = true;
                    ManualCtrl.btnDownload.IsDefault = false;
                    if (res != null)
                    {
                        try
                        {
                        if (Directory.Exists(_path))
                        {
                            if (Directory.Exists(_thumbnailsPath))
                            {
                                string[] filePaths1 = Directory.GetFiles(_thumbnailsPath);
                                foreach (var item in filePaths1)
                                {
                                    try
                                    {
                                        if (File.Exists(item))
                                        {
                                            File.Delete(item);
                                        }
                                    }
                                    catch (Exception ex)
                                    {
                                        ErrorHandler.ErrorHandler.LogError(ex);
                                    }
                                }
                                Directory.Delete(_thumbnailsPath, true);
                            }
                            string[] filePaths = Directory.GetFiles(_path);
                            foreach (var item in filePaths)
                            {
                                try
                                {
                                    if (File.Exists(item))
                                    {
                                        File.Delete(item);
                                    }
                                }
                                catch (Exception ex)
                                {
                                    ErrorHandler.ErrorHandler.LogError(ex);
                                }
                            }
                            Directory.Delete(_path, true);
                        }
                            var objhome = new Home();
                            objhome.Show();
                            this.Close();
                    }
                        catch(Exception m)
                        {
                            string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(m);
                            ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                        }
                    }
                }
                else
                {
                    btnacquire.IsEnabled = true;
                    btnBack.IsEnabled = true;
                    chkSelectAll.IsEnabled = true;
                    MessageBox.Show("Please select at least one image/video.");
                }

            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }

#if DEBUG
            if (watch != null)
                FrameworkHelper.CommonUtility.WatchStop(this.GetType().FullName + "." + System.Reflection.MethodBase.GetCurrentMethod().Name + "|", watch);
#endif
        }

        private void chkSelectAll_Click(object sender, RoutedEventArgs e)
        {
            foreach (FrameworkHelper.MyImageClass myImageClass in lstImages.Items)
            {
                myImageClass.IsChecked = (bool)((CheckBox)sender).IsChecked;
            }
            txbSelectedImages.Text = "Selected:" + (((CheckBox)sender).IsChecked == true ? lstImages.Items.Count.ToString() : "0");
            lstImages.Items.Refresh();
        }
        private void Selectedimages_Click(object sender, RoutedEventArgs e)
        {
            if (IMGFrame.Visibility == Visibility.Visible)
            {
                if (((CheckBox)sender).IsChecked == true)
                {
                    txbSelectedImages.Text = "Selected :" + (int.Parse(txbSelectedImages.Text.Split(':').Last().Trim()) + 1).ToString();
                }
                if (((CheckBox)sender).IsChecked == false)
                {
                    txbSelectedImages.Text = "Selected :" + (int.Parse(txbSelectedImages.Text.Split(':').Last().Trim()) - 1).ToString();
                }
                if (int.Parse(txbSelectedImages.Text.Split(':').Last().Trim()) == lstImages.Items.Count)
                    chkSelectAll.IsChecked = true;
                else
                    chkSelectAll.IsChecked = false;

                string vidFileName = ((System.Windows.Media.Imaging.BitmapImage)((((Image)((System.Windows.Controls.Grid)(((CheckBox)e.Source).Parent)).FindName("thumbImage"))).Source)).UriSource.OriginalString.ToLower();
                if (vidFileName.EndsWith("avi.jpg") || vidFileName.EndsWith("mp4.jpg") || vidFileName.EndsWith("wmv.jpg") || vidFileName.EndsWith("mov.jpg") || vidFileName.EndsWith("3gp.jpg") || vidFileName.EndsWith("3g2.jpg") || vidFileName.EndsWith("m2v.jpg") || vidFileName.EndsWith("m4v.jpg") || vidFileName.EndsWith("flv.jpg") || vidFileName.EndsWith("mpg.jpg") || vidFileName.EndsWith("ffmpeg.jpg"))
                {

                }
                else
                {
                    MediaStop();
                    imgmain.Visibility = Visibility.Visible;
                    vidoriginal.Visibility = Visibility.Collapsed;
                    imgmain.Visibility = Visibility.Visible;
                    vidoriginal.Visibility = Visibility.Collapsed;
                    imgmain.Source = CommonUtility.GetImageFromPath(vidFileName);
                }
            }
            else
            {
                selectedImage = ((System.Windows.Media.Imaging.BitmapImage)((((Image)((System.Windows.Controls.Grid)(((CheckBox)e.Source).Parent)).FindName("thumbImage"))).Source)).UriSource.OriginalString; ;
                if (((CheckBox)sender).IsChecked == true)
                {
                    txbSelectedImages.Text = "Selected :" + (int.Parse(txbSelectedImages.Text.Split(':').Last().Trim()) + 1).ToString();
                }
                if (((CheckBox)sender).IsChecked == false)
                {
                    txbSelectedImages.Text = "Selected :" + (int.Parse(txbSelectedImages.Text.Split(':').Last().Trim()) - 1).ToString();
                }
                if (int.Parse(txbSelectedImages.Text.Split(':').Last().Trim()) == lstImages.Items.Count)
                    chkSelectAll.IsChecked = true;
                else
                    chkSelectAll.IsChecked = false;
            }
            if (IsEnabledSessionSelect && ((CheckBox)sender).IsChecked == true)
            {
                lstImages.SelectedItem = ((MyImageClass)((CheckBox)sender).DataContext);
                if (IsEnabledSelectEndPoint)
                {
                    if (_selectStartIndex > -1)
                        _selectEndIndex = lstImages.SelectedIndex;
                }
                else
                    _selectEndIndex = lstImages.Items.Count - 1;
                if (int.Parse(txbSelectedImages.Text.Split(':').Last().Trim()) == 1)
                {
                    _selectStartIndex = lstImages.SelectedIndex;
                }
                SelectSessionImages();
            }
        }
        private void btnWithPreviewActive_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (lstImages.Items.Count > 0)
                {
                    chkSelectAll.IsEnabled = true;//saurabh
                    thumbPreview.HorizontalAlignment = HorizontalAlignment.Right;
                    lstImages.Width = 250;
                    IMGFrame.Visibility = Visibility.Visible;

                    imgwithPreview.Source = new BitmapImage(new Uri(@"/images/thumbnailview1_active.png", UriKind.Relative));
                    imgwithoutPreview.Source = new BitmapImage(new Uri(@"images/thumbnailview2.png", UriKind.Relative));
                    string vidFileName = selectedImage;
                    if (string.IsNullOrEmpty(vidFileName))
                    {
                        lstImages.SelectedItem = lstImages.Items[0];
                        MediaStop();
                        vidFileName = ((System.Windows.Media.Imaging.BitmapImage)(((FrameworkHelper.MyImageClass)(lstImages.SelectedItem)).Image)).UriSource.OriginalString.ToLower();
                    }
                    if (vidFileName.EndsWith("avi.jpg") || vidFileName.EndsWith("mp4.jpg") || vidFileName.EndsWith("wmv.jpg") || vidFileName.EndsWith("mov.jpg") || vidFileName.EndsWith("3gp.jpg") || vidFileName.EndsWith("3g2.jpg") || vidFileName.EndsWith("m2v.jpg") || vidFileName.EndsWith("m4v.jpg") || vidFileName.EndsWith("flv.jpg") || vidFileName.EndsWith("mpg.jpg") || vidFileName.EndsWith("ffmpeg.jpg") || vidFileName.EndsWith("mkv.jpg") || vidFileName.EndsWith("mts.jpg"))
                    {
                        vidFileName = vidFileName.Replace(jpgExtension, string.Empty);
                        var drives = from drive in DriveInfo.GetDrives()
                                     where drive.DriveType == DriveType.Removable && drive.IsReady == true
                                     select drive;
                        if (drives.Count() > 0)
                        {
                            foreach (var drivesitem in drives)
                            {
                                var videoitems = Directory.EnumerateFiles(drivesitem.Name, "*.*", SearchOption.AllDirectories).Where(s => s.ToLower().EndsWith(".wmv") || s.ToLower().EndsWith(".mp4") || s.ToLower().EndsWith(".avi") || s.ToLower().EndsWith(".mov") || s.ToLower().EndsWith(".3gp") || s.ToLower().EndsWith(".3g2") || s.ToLower().EndsWith(".m2v") || s.ToLower().EndsWith(".m4v") || s.ToLower().EndsWith(".flv") || s.ToLower().EndsWith(".mpg") || s.ToLower().EndsWith(".ffmpeg") || s.ToLower().EndsWith(".mkv") || s.ToLower().EndsWith(".mts")).ToList();
                                var tempvid = videoitems.Where(v => v.ToLower().Contains(System.IO.Path.GetFileName(vidFileName))).FirstOrDefault();
                                if (tempvid != null)
                                {
                                    imgmain.Visibility = Visibility.Collapsed;
                                    vidoriginal.Visibility = Visibility.Visible;
                                    MediaStop();
                                    vsMediaFileName = tempvid.ToString();
                                    MediaPlay();
                                }
                            }
                        }
                    }
                    else
                    {
                        imgmain.Visibility = Visibility.Visible;
                        vidoriginal.Visibility = Visibility.Collapsed;
                        imgmain.Source = CommonUtility.GetImageFromPath(vidFileName);
                    }

                    lstImages.SelectedItem = lstImages.Items[0];
                    imgmain.Source = CommonUtility.GetImageFromPath(((System.Windows.Media.Imaging.BitmapImage)(((FrameworkHelper.MyImageClass)(lstImages.SelectedItem)).Image)).UriSource.OriginalString);
                }
                else
                    IMGFrame.Visibility = Visibility.Collapsed;
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
            }
            finally
            {
                MemoryManagement.FlushMemory();
            }
        }
        private void btnWithoutPreview_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                MediaStop();
                if (lstImages.Items.Count > 0)
                {
                    selectedImage = string.Empty;
                    chkSelectAll.IsEnabled = true;
                    thumbPreview.HorizontalAlignment = HorizontalAlignment.Stretch;
                    lstImages.Width = double.NaN;
                    IMGFrame.Visibility = Visibility.Collapsed;
                    imgwithPreview.Source = new BitmapImage(new Uri(@"images/thumbnailview1.png", UriKind.Relative));
                    imgwithoutPreview.Source = new BitmapImage(new Uri(@"/images/thumbnailview2_active.png", UriKind.Relative));
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
            }
            finally
            {
                MemoryManagement.FlushMemory();
            }
        }
        private void vidPlay_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            e.Handled = true;
        }
        private void vidPlay_Click(object sender, RoutedEventArgs e)
        {
            string vidFileName = ((System.Windows.Media.Imaging.BitmapImage)((((Image)((System.Windows.Controls.Grid)(((Button)e.Source).Parent)).FindName("thumbImage"))).Source)).UriSource.OriginalString.ToLower();
            selectedImage = vidFileName;
            btnWithPreviewActive_Click(sender, e);
        }

        private void btnOpenEditControl_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                _busyWindows.Show();
                _busyWindows.BringIntoView();
                
                bool IsCrop = false;
                bool IsGreen = false;
                var allimages = (from item in MyImages
                                 where item.FileExtension == jpgExtension || item.FileExtension == pngExtension
                                 select item).ToList();
                int selectedImageCount = allimages.Count(x => x.IsChecked == true);
                var selectedList = allimages.Where(x => x.IsChecked == true).ToList();
                string Layering = string.Empty;
                selectedList = selectedList.OrderBy(x => x.CreatedDate).ToList();
                if (selectedList.Count > 0)
                {
                    int photoId = Convert.ToInt32(selectedList.First().PhotoNumber);
                    ucEditing.PhotoName = selectedList.First().Title;
                    ucEditing.PhotoId = photoId;
                    ucEditing.dbBrit = ucEditing._brighteff.Brightness = Convert.ToDouble(defaultBrightness);
                    ucEditing.dbContr = ucEditing._brighteff.Contrast = Convert.ToDouble(defaultContrast);
                    ucEditing.SetEffect();

                    PhotoBusiness photoBiz = new PhotoBusiness();
                    ucEditing.HotFolderPath = hotFolderPath;
                    ucEditing.DateFolder = dateFolder;
                    ucEditing.CropFolderPath = cropFolderPath;
                    ucEditing.tempfilename = selectedList.First().Title + jpgExtension;
                    if (String.IsNullOrWhiteSpace(defaultBrightness))
                        defaultBrightness = "0";
                    if (String.IsNullOrWhiteSpace(defaultContrast))
                        defaultContrast = "1";
                    ImageEffect = "<image brightness = '" + defaultBrightness + "' contrast = '" + defaultContrast + "' Crop='##' colourvalue = '##' rotate='##' ><effects sharpen='##' greyscale='0' digimagic='0' sepia='0' defog='0' underwater='0' emboss='0' invert = '0' granite='0' hue ='##' cartoon = '0'></effects></image>";
                    ucEditing.ImageEffect = ImageEffect;
                    ucEditing.SpecImageEffect = ImageEffect;
                    foreach (var item in selectedList)
                    {
                        if (item.NewEffectsXML == null && item.NewLayeringXML == null)
                        {
                            if (item.IsAutorotated == false)
                                item.IsAutorotated = rotateImage(item.ImagePath.Replace("\\Thumbnails", ""));
                            ResetImageDPI(item.ImagePath.Replace("\\Thumbnails", ""));
                        }
                    }
                    if (lstDG_SemiOrder_Settings != null && lstDG_SemiOrder_Settings.Count > 0)
                    {
                        ucEditing.semiOrderProfileId = lstDG_SemiOrder_Settings.FirstOrDefault().DG_SemiOrder_Settings_Pkey;
                        ucEditing.semiOrderSettings = lstDG_SemiOrder_Settings.FirstOrDefault();
                        CreateXMLwithSpecPrintEffects(lstDG_SemiOrder_Settings.FirstOrDefault());
                        ucEditing.SpecLayeringHorizontal = LayeringHorizontal;
                        ucEditing.SpecLayeringVertical = LayeringVertical;
                        
                        BitmapImage _objPhoto = GetBitmapImageFromPath(System.IO.Path.Combine(System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory, DateTime.Now.ToString("yyyyMMdd")), ucEditing.tempfilename));
                        if (_objPhoto.Height > _objPhoto.Width)
                            Layering = LayeringVertical;
                        else
                            Layering = LayeringHorizontal;
                        ucEditing.SpecLayeringUpdated = Layering;
                        if (lstDG_SemiOrder_Settings.FirstOrDefault().DG_SemiOrder_IsCropActive == true)
                        {
                            foreach (var item in selectedList)
                            {
                                if (item.SettingStatus == SettingStatus.Spec)
                                {
                                    if (!File.Exists(System.IO.Path.Combine(cropFolderPath, (item.Title + jpgExtension))))
                                    {
                                        item.IsCropped = ApplyCrop((item.Title + jpgExtension));
                                    }
                                    else
                                        item.IsCropped = true;
                                }
                            }
                        }
                        if (selectedList.First().SettingStatus == SettingStatus.Spec)
                            IsCrop = (bool)selectedList.First().IsCropped;
                        IsGreen = (bool)lstDG_SemiOrder_Settings.FirstOrDefault().DG_SemiOrder_Settings_IsImageBG;
                    }
                    else
                    {
                        ucEditing.semiOrderProfileId = 0;
                        ucEditing.semiOrderSettings = new SemiOrderSettings();
                        LayeringHorizontal = String.Empty;
                        LayeringVertical = String.Empty;
                        Layering = string.Empty;
                        ucEditing.SpecLayeringHorizontal = String.Empty;
                        ucEditing.SpecLayeringVertical = String.Empty;
                        ucEditing.SpecLayeringUpdated = String.Empty;
                        IsCrop = false;
                        IsGreen = false;
                    }
                    if (selectedList.First().SettingStatus == SettingStatus.SpecUpdated)
                    {
                        IsCrop = selectedList.First().IsCropped;
                        Layering = selectedList.First().NewLayeringXML;
                    }
                    if (!string.IsNullOrWhiteSpace(selectedList.First().NewEffectsXML))
                        ucEditing.ImageEffect = selectedList.First().NewEffectsXML;
                    if (selectedList.First().SettingStatus == SettingStatus.None)
                    {
                        IsCrop = false;
                        Layering = null;
                    }
                    ucEditing.selectedIndex = 0;
                    ucEditing.lstMyImageClass = selectedList;
                    ucEditing.SetParent(this);
                    _busyWindows.Hide();
                    var res = ucEditing.ShowHandlerDialog(IsCrop, IsGreen, Layering);
                    foreach (MyImageClass miClass in UpdatedSelectedList)
                    {
                        int index = 0;
                        if ((index = allimages.FindIndex(x =>
                                  x.IsChecked == true &&
                                  x.PhotoNumber == miClass.PhotoNumber)) > -1)
                        {
                            allimages[index] = miClass;
                        }
                    }
                    ucEditing.lstStrip.ItemsSource = null;
                    ucEditing.lstStrip.Items.Refresh();
                    ucEditing.mainImageundo.Source = ucEditing.mainImage.Source = null;
                    ucEditing.canbackground.Background = null;
                    ucEditing.widthimg.Source = null;
                    ucEditing.imgoriginal.Source = null;
                    ucEditing.imgRotateCrop.Source = null;
                }
                else
                {
                    _busyWindows.Hide();
                    MessageBox.Show("Please select at least one photo", "Digiphoto");
                }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
#if DEBUG
            var watch = FrameworkHelper.CommonUtility.Watch();
            watch.Start();
#endif
            if (_show == true)
            {
                btnacquire.IsEnabled = true;
                _busyWindows.Show();
                _busyWindows.BringIntoView();
                _manualDownloadWorker.RunWorkerAsync();
            }
#if DEBUG
            if (watch != null)
                FrameworkHelper.CommonUtility.WatchStop(this.GetType().FullName + "." + System.Reflection.MethodBase.GetCurrentMethod().Name + "|", watch);
#endif
        }
        private void Window_Unloaded(object sender, RoutedEventArgs e)
        {
#if DEBUG
            var watch = FrameworkHelper.CommonUtility.Watch();
            watch.Start();
#endif
            try
            {
            if (_isUsbDelete)
            {
                if (isImageAcquired)
                {
                    foreach (var item in MyImages.Where(item => item.IsChecked))
                    {
                        try { File.Delete(System.IO.Path.Combine(originalDirectoryPath, item.Title + item.FileExtension)); }
                            catch(Exception n) {
                                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(n);
                                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                            }
                    }
                }
            }
            isImageAcquired = false;
            MyImages.Clear();
            Directory.Delete(System.IO.Path.Combine(binpath, dateFolder), true);
            btnWithoutPreview.Click -= new RoutedEventHandler(btnWithoutPreview_Click);
            btnWithPreviewActive.Click -= new RoutedEventHandler(btnWithPreviewActive_Click);
            chkSelectAll.Click -= new RoutedEventHandler(chkSelectAll_Click);
            this.Loaded -= new RoutedEventHandler(Window_Loaded);
            btnBack.Click -= new RoutedEventHandler(btnBack_Click);
            btnacquire.Click -= new RoutedEventHandler(btnacquire_Click);
            }
            catch(Exception en)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(en);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage + " DirectoryPath: " + binpath + ", DateFolder: " + dateFolder);
            }
#if DEBUG
            if (watch != null)
                FrameworkHelper.CommonUtility.WatchStop(this.GetType().FullName + "." + System.Reflection.MethodBase.GetCurrentMethod().Name + "|", watch);
#endif
        }
        #endregion

        #region Public Method
        public bool IsShoworHide()
        {
            return _show;
        }
        #endregion

        #region Manual Download Background Worder
        private void ManualDownloadworker_DoWork(object sender, System.ComponentModel.DoWorkEventArgs e)
        {
#if DEBUG
            var watch = FrameworkHelper.CommonUtility.Watch();
            watch.Start();
#endif
            CreateImages();
#if DEBUG
            if (watch != null)
                FrameworkHelper.CommonUtility.WatchStop(this.GetType().FullName + "." + System.Reflection.MethodBase.GetCurrentMethod().Name + "|", watch);
#endif
        }
        private void ManualDownloadworker_RunWorkerCompleted(object sender, System.ComponentModel.RunWorkerCompletedEventArgs e)
        {
#if DEBUG
            var watch = FrameworkHelper.CommonUtility.Watch();
            watch.Start();
#endif
            txbCount.Text = "/" + _img.Count.ToString();
            txbSelectedImages.Text = "Selected :" + 0.ToString();// img.Count.ToString();
            ShowImages();
            _busyWindows.Hide();
            var res = ManualCtrl.ShowHandlerDialog();
            int p = ManualDownload._photogrpherIdS;
            Int64 num = ManualDownload._startingNoS;
            int loc = ManualDownload._locationIdS;
            int subs = ManualDownload._substoreIdS;
            if (subs > 0)
            {
                IsSpecActiveForSubstore = Convert.ToBoolean(lstConfig.Where(x => x.DG_Substore_Id == subs).FirstOrDefault().DG_SemiOrderMain);
            }
            if (IsSpecActiveForSubstore)
                lstDG_SemiOrder_Settings = (new SemiOrderBusiness()).GetSemiOrderSetting(null, loc);
            if (lstDG_SemiOrder_Settings != null && lstDG_SemiOrder_Settings.Count > 0)
            {
                foreach (MyImageClass obj in MyImages)
                {
                    obj.SettingStatus = SettingStatus.Spec;
                }
            }
#if DEBUG
            if (watch != null)
                FrameworkHelper.CommonUtility.WatchStop(this.GetType().FullName + "." + System.Reflection.MethodBase.GetCurrentMethod().Name + "|", watch);
#endif
        }
        #endregion

        #region MediaPlayer
        void MediaStop()
        {
            if (mplayer != null)
            {
                mplayer.MediaStop();
                mplayer.Dispose();
                mplayer = null;
            }
            gdMediaPlayer.Children.Clear();
            gdMediaPlayer.Children.Remove(mplayer);
        }
        void MediaPlay()
        {
            MediaStop();
            if (mplayer != null)
            {
                mplayer.Dispose();
            }
            gdMediaPlayer.Dispatcher.BeginInvoke(new Action(
                () =>
                {
                    mplayer = new MLMediaPlayer(vsMediaFileName, "ImageDownloader");
                    gdMediaPlayer.BeginInit();
                    gdMediaPlayer.Children.Clear();
                    gdMediaPlayer.Children.Add(mplayer);
                    gdMediaPlayer.EndInit();
                }));
        }

        #endregion

        #region Unused functions
        private BitmapImage GetImageFromPath(string path)
        {
#if DEBUG
            var watch = FrameworkHelper.CommonUtility.Watch();
            watch.Start();
#endif
            try
            {
                BitmapImage bi = new BitmapImage();
                using (FileStream fileStream = File.OpenRead(path))
                {
                    using (MemoryStream ms = new MemoryStream())
                    {
                        fileStream.CopyTo(ms);
                        ms.Seek(0, SeekOrigin.Begin);
                        bi.CacheOption = BitmapCacheOption.OnLoad; // Memory 
                        bi.BeginInit();
                        bi.StreamSource = ms;
                        bi.EndInit();
                        bi.Freeze(); // Memory 
                        fileStream.Close();
                        ms.Close();
#if DEBUG
                        if (watch != null)
                            FrameworkHelper.CommonUtility.WatchStop(this.GetType().FullName + "." + System.Reflection.MethodBase.GetCurrentMethod().Name + "|", watch);
#endif
                        return bi;
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
                return null;
            }
        }

        private string EstimatedAcquisitionTime()
        {
            string time = string.Empty;
            double tempSeconds = 0.0; double filecopylag = 0.0;
            foreach (var item1 in lstImages.Items)
            {
                var tempItem1 = (MyImageClass)item1;
                if (tempItem1.IsChecked)
                {
                    if (htFileSize.ContainsKey(tempItem1.Title))
                    {
                        //file of 1mb takes 0.25 sec to acquire
                        filecopylag = Convert.ToDouble(htFileSize[tempItem1.Title].ToString()) * 0.25;
                        if (!tempItem1.Title.ToLower().EndsWith(jpgExtension) && !tempItem1.Title.ToLower().EndsWith(pngExtension))
                        {
                            tempSeconds += filecopylag + 2;//2 for video thumbnail
                        }
                        else
                        {
                            tempSeconds += filecopylag; //for images
                        }
                    }
                }
            }
            TimeSpan t = TimeSpan.FromSeconds(tempSeconds);
            time = "[Minimum acquisition time: " + string.Format("{0:D2}h:{1:D2}m:{2:D2}s", t.Hours, t.Minutes, t.Seconds) + "]";
            return time;
        }
        private BitmapImage GetImageFromResourceString(string imageName)
        {

            var image = new BitmapImage();
            image.BeginInit();
            image.DecodePixelWidth = 150;
            image.CacheOption = BitmapCacheOption.OnLoad;
            if (File.Exists(imageName))
            {
                image.UriSource = new Uri(imageName);
                image.EndInit();
            }
            return image;
        }
        private double ConvertBytesToMegabytes(long bytes)
        {
            return (bytes / 1024f) / 1024f;
        }
        private void btnWithPreviewActive_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            e.Handled = true;
        }
        #endregion
    }
}
