﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Collections.ObjectModel;
using DigiPhoto.DataLayer.Model;
using DigiPhoto.DataLayer;
using DigiPhoto.Common;
using System.IO;
using XPBurn;
using FrameworkHelper;
using DigiPhoto.IMIX.Business;
using DigiPhoto.IMIX.Model;
using DigiPhoto.Utility.Repository.ValueType;
using System.Security.Cryptography;
using DigiPhoto.Calender;

namespace DigiPhoto.Orders
{
    /// <summary>
    /// Interaction logic for PlaceOrder.xaml
    /// </summary>
    public partial class PlaceOrder : Window
    {
        private List<Product> _productType;
        private bool _issemiorder = false;
        //private ObservableCollection<LineItem> _objLineItem;//Ols code
        public ObservableCollection<LineItem> _objLineItem; //Updated by VINS_17Mar2020
        private List<ProductPriceInfo> _lstPricing;
        private string _defaultEffect = @"<image brightness=""0"" contrast=""1"" Crop=""##"" colourvalue=""##"" rotate=""##""><effects sharpen=""##"" greyscale=""0"" digimagic=""0"" sepia=""0"" defog=""0"" underwater=""0"" emboss=""0"" invert=""0"" granite=""0"" hue=""##"" cartoon=""0"" /></image>";
        private string _defaultSpecPrintLayer = @"<photo zoomfactor=""1"" border="""" bg="""" canvasleft=""-1"" canvastop=""-1"" scalecentrex=""-1"" scalecentrey=""-1"" />";
        /// <summary>
        /// Initializes a new instance of the <see cref="PlaceOrder"/> class.
        /// </summary>
        LineItem _current = new LineItem();
        public string DefaultCurrency
        {
            get;
            set;
        }
        public bool isChkSpecOnlinePackage = false;
        public PlaceOrder()
        {
            try
            {
                InitializeComponent();
                _objLineItem = new ObservableCollection<LineItem>();
                dgOrder.ItemsSource = _objLineItem;
                _lstPricing = new List<ProductPriceInfo>();
                _productType = new List<Product>();
                AddPricing();
                FillProductType();
                DefaultCurrency = GetDefaultCurrency();
                ShowTotalPrice();
                btnNextsimg.IsDefault = true;
                CtrlSelectedCalenderList.IsVisibleChanged += CtrlSelectedCalenderList_IsVisibleChanged;
                AddPrintedImagesOnOff();
                Keyboard.Focus(CtrlProductType);
                CtrlProductType.IsActive = true;



            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
            }
        }

        void CtrlSelectedCalenderList_IsVisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            if (CtrlSelectedCalenderList.Visibility == Visibility.Collapsed)
            {
                LineItem current = _current;
                btnNextsimg.IsDefault = true;
                CtrlSelectedCalenderList.btnSubmit.IsDefault = false;
                current.PrintOrderPageList = CtrlSelectedCalenderList.PrintOrderPageList;
                current.GroupItems = CtrlSelectedCalenderList.SelectedImage;
                BindMyListItems(current, current.GroupItems);
            }
        }



        /// <summary>
        /// Finds the name of the by.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <param name="root">The root.</param>
        /// <returns></returns>
        private FrameworkElement FindByName(string name, FrameworkElement root)
        {
#if DEBUG
            var watch = FrameworkHelper.CommonUtility.Watch();
            watch.Start();
#endif
            Stack<FrameworkElement> tree = new Stack<FrameworkElement>();
            tree.Push(root);
            while (tree.Count > 0)
            {
                FrameworkElement current = tree.Pop(); // root is null
                if (current.Name == name)
                    return current;

                int count = VisualTreeHelper.GetChildrenCount(current);
                for (int SupplierCounter = 0; SupplierCounter < count; ++SupplierCounter)
                {
                    DependencyObject child = VisualTreeHelper.GetChild(current, SupplierCounter);
                    if (child is FrameworkElement)
                        tree.Push((FrameworkElement)child);
                }
            }
#if DEBUG
            if (watch != null)
                FrameworkHelper.CommonUtility.WatchStop(this.GetType().FullName + "." + System.Reflection.MethodBase.GetCurrentMethod().Name + "|", watch);
#endif
            return null;
        }

        /// <summary>
        /// Updates the pricing.
        /// </summary>
        /// <param name="currentRecord">The current record.</param>
        protected void UpdatePricing(LineItem currentRecord)
        {
            try
            {
#if DEBUG
                var watch = FrameworkHelper.CommonUtility.Watch();
                watch.Start();
#endif

                //Call method to save album print order incase of productType=79
                if (currentRecord.SelectedProductType_ID == 79 && currentRecord.GroupItems.Count() > 0)
                {
                    List<PhotoPrintPositionDic> objList = new List<PhotoPrintPositionDic>();
                    foreach (LstMyItems LstItm in currentRecord.GroupItems)
                    {
                        if (objList.Where(p => p.PhotoId == LstItm.PhotoId).FirstOrDefault() == null)
                        {
                            foreach (PhotoPrintPosition PhotoPosition in LstItm.PhotoPrintPositionList)
                                objList.Add(new PhotoPrintPositionDic(LstItm.PhotoId, PhotoPosition));
                        }
                    }
                    currentRecord.PrintPhotoOrderPosition = objList;
                    //Added by Anand, Clear source of GroupImages(RobotImageLoader.PrintImages), so that same old PhotoPrintPositionList will not come for new Album.
                    foreach (LstMyItems item in RobotImageLoader.PrintImages)
                    {
                        item.PhotoPrintPositionList.Clear();
                    }
                }

                if (currentRecord.SelectedProductType_ID == 100 && currentRecord.GroupItems.Count() > 0)
                {
                    List<PhotoPrintPositionDic> objList = new List<PhotoPrintPositionDic>();
                    foreach (LstMyItems LstItm in currentRecord.GroupItems)
                    {
                        if (objList.Where(p => p.PhotoId == LstItm.PhotoId).FirstOrDefault() == null)
                        {
                            foreach (PhotoPrintPosition PhotoPosition in LstItm.PhotoPrintPositionList)
                                objList.Add(new PhotoPrintPositionDic(LstItm.PhotoId, PhotoPosition));
                        }
                    }
                    currentRecord.PrintPhotoOrderPosition = objList;
                    foreach (LstMyItems item in RobotImageLoader.PrintImages)
                    {
                        item.PhotoPrintPositionList.Clear();
                    }
                }

                if (currentRecord.ParentID == currentRecord.ItemNumber)
                {
                    int Quantity = 0;
                    var item = (from result in _lstPricing
                                where result.DG_Product_Pricing_ProductType == currentRecord.SelectedProductType_ID
                                select result.DG_Product_Pricing_ProductPrice).FirstOrDefault();
                    double UnitPrice = Convert.ToDouble(item);
                    currentRecord.UnitPrice = UnitPrice;
                    if (currentRecord.IsBundled)
                    {
                        Quantity = currentRecord.Quantity;
                        currentRecord.TotalPrice = (UnitPrice * Quantity).ToDouble();
                    }
                    else
                    {
                        if (!currentRecord.IsPackage)
                        {
                            if (currentRecord.SelectedProductType_ID == 35 || currentRecord.SelectedProductType_ID == 36 || currentRecord.SelectedProductType_ID == 78 || currentRecord.SelectedProductType_ID == 100)
                            {
                                currentRecord.TotalPrice = UnitPrice;
                            }
                            else
                            {
                                if (currentRecord.SelectedImages != null)
                                {
                                    if (currentRecord.SelectedImages.Count > 0)
                                    {
                                        Quantity = currentRecord.SelectedImages.Count * currentRecord.Quantity;
                                    }
                                    else
                                    {
                                        Quantity = currentRecord.Quantity;
                                    }
                                }
                                else
                                {
                                    Quantity = currentRecord.Quantity;
                                }
                                if (currentRecord.SelectedProductType_ID == 1091)
                                {
                                    currentRecord.TotalPrice = 0;
                                }
                                else
                                {
                                    currentRecord.TotalPrice = (UnitPrice * Quantity).ToDouble();
                                }
                            }
                        }
                        else
                        {
                            Quantity = currentRecord.Quantity;
                            currentRecord.TotalPrice = (UnitPrice * Quantity).ToDouble();
                        }
                    }
                    //Set package price=0 having qrcode product and the qrcode is used earlier and still have some limit
                    if (_objLineItem.Where(o => o.SelectedProductType_ID == 84 && o.ParentID == currentRecord.ItemNumber).Count() > 0 && CtrlSelectedQrCode.IsQrCodeUsed == true)
                    {
                        Quantity = currentRecord.Quantity;
                        //If package limit exceeds take charge of full package
                        Quantity = Quantity > 0 ? Quantity - 1 : Quantity;
                        currentRecord.TotalPrice = (UnitPrice * Quantity).ToDouble();
                    }
                    currentRecord.NetPrice = currentRecord.TotalPrice;
                }
                ShowTotalPrice();
#if DEBUG
                if (watch != null)
                    FrameworkHelper.CommonUtility.WatchStop(this.GetType().FullName + "." + System.Reflection.MethodBase.GetCurrentMethod().Name + "|", watch);
#endif
            }

            catch (Exception ex)
            {

            }
        }
        private void ShowTotalPrice()
        {
            Double total = 0;
            total = _objLineItem.Select(i => i.TotalPrice).Sum();
            TxtAmounttobePaid.Text = DefaultCurrency.ToString() + "  " + total.ToString("N2");
        }


        protected void AddPricing()
        {
            _lstPricing = (new ProductBusiness()).GetProductPricingStoreWise(LoginUser.StoreId);
        }
        /// <summary>
        /// Adds the new row.
        /// </summary>
        /// <param name="productId">The productid.</param>
        /// <param name="parentId">The parentid.</param>
        /// <param name="visible">if set to <c>true</c> [visible].</param>
        /// <param name="index">The index.</param>
        /// <returns></returns>
        protected string AddNewRow(Product productId, string parentId, bool visible, int index)
        {
            string output = string.Empty;
            if (productId == null)
            {
                LineItem objItem = new LineItem();
                string itemNumber = System.Guid.NewGuid().ToString();
                objItem.ItemNumber = itemNumber;
                objItem.ParentID = itemNumber;
                objItem.IssemiOrder = false;
                objItem.GroupItems = new List<LstMyItems>();
                foreach (LstMyItems items in GetGroupImages(itemNumber, null))
                {
                    objItem.GroupItems.Add(items);
                }
                objItem.CommandVisible = true;
                objItem.ItemIndex = (_objLineItem.Count - 1) + 1;
                _objLineItem.Add(objItem);
                output = itemNumber.ToString();
            }
            else
            {
                LineItem objItem = new LineItem();
                string itemNumber = System.Guid.NewGuid().ToString();
                objItem.ItemNumber = itemNumber;
                objItem.GroupItems = new List<LstMyItems>();
                foreach (LstMyItems items in GetGroupImages(itemNumber, productId.IsPrintType))
                {
                    objItem.GroupItems.Add(items);
                }
                objItem.ParentID = parentId;
                objItem.SelectedProductType_ID = productId.ProductID;
                objItem.SelectedProductType_Text = productId.ProductName;
                objItem.SelectedProductCode = productId.ProductCode; // // Added by Anisur Rahman for Dubai frame (Product code + Barcode) receipt requirement
                objItem.SelectedProductType_Image = productId.ProductIcon;
                objItem.IsAccessory = productId.IsAccessory;
                objItem.IsWaterMarked = productId.IsWaterMarked;
                objItem.IsBundled = productId.IsBundled;
                objItem.IsPersonalizedAR = productId.IsPersonalizedAR;///added by latika to check the personalised functionality.
                objItem.CommandVisible = visible;
                var pid = from i in _objLineItem
                          where i.ItemNumber == parentId
                          select i.SelectedProductType_ID;

                int MaxQuantity = (new PackageBusniess()).GetMaxQuantityofIteminaPackage(pid.FirstOrDefault().ToInt32(), productId.ProductID);

                objItem.TotalMaxSelectedPhotos = MaxQuantity;
                objItem.Quantity = (new PackageBusniess()).GetChildProductTypeQuantity(productId.ProductID, pid.FirstOrDefault().ToInt32()).ToInt32();
                objItem.ItemIndex = (_objLineItem.Count - 1) + 1;
                _objLineItem.Add(objItem);

                if (output == string.Empty)
                {
                    output = itemNumber.ToString();
                }
                else
                {
                    output = output + "," + itemNumber.ToString();
                }
            }
            return output;
        }
        /// <summary>
        /// Fills the type of the product.
        /// </summary>
        private void FillProductType()
        {
            try
            {
                SubStoresInfo _objSubstore = (new StoreSubStoreDataBusniess()).GetSubstoreData(LoginUser.SubStoreId);

                //Here we are finding the logical substoreid
                int SubStoreId = 0;
                if (_objSubstore != null)
                    SubStoreId = (_objSubstore.LogicalSubStoreID == 0) ? LoginUser.SubStoreId : Convert.ToInt32(_objSubstore.LogicalSubStoreID);

                List<ProductTypeInfo> productObj = (new ProductBusiness()).GetProductTypeforOrder(SubStoreId);
                var product = productObj.FindAll(t => t.DG_Orders_ProductNumber != 0).OrderBy(t => t.DG_Orders_ProductNumber);

                foreach (ProductTypeInfo pType in product)
                {
                    Product item = new Product();
                    item.ProductID = pType.DG_Orders_ProductType_pkey;
                    item.ProductCode = pType.DG_Orders_ProductType_ProductCode; // Added by Anisur Rahman for Dubai frame (Product code + Barcode) receipt requirement
                    item.ProductName = pType.DG_Orders_ProductType_Name.ToString();
                    //Updated by Anand on 24-March-2015
                    bool isBundled = false;
                    if (pType.DG_Orders_ProductType_IsBundled.HasValue)
                    {
                        isBundled = pType.DG_Orders_ProductType_IsBundled.Value;
                    }
                    item.IsBundled = isBundled;
                    item.ProductIcon = pType.DG_Orders_ProductType_Image.ToString();
                    item.DiscountOption = (bool)pType.DG_Orders_ProductType_DiscountApplied;
                    item.IsPackage = (bool)pType.DG_IsPackage;
                    item.IsAccessory = (bool)pType.DG_IsAccessory;
                    item.IsWaterMarked = (bool)pType.DG_IsWaterMarkIncluded;
                    item.MaxQuantity = pType.DG_MaxQuantity;
                    item.IsPrintType = pType.IsPrintType;
                    _productType.Add(item);
                }
            }
            catch { }
        }

        /// <summary>
        /// Handles the CellEditEnding event of the dgOrder control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="DataGridCellEditEndingEventArgs"/> instance containing the event data.</param>
        private void dgOrder_CellEditEnding(object sender, DataGridCellEditEndingEventArgs e)
        {
            e.Cancel = true;
        }

        private void btnAddMore_Click(object sender, RoutedEventArgs e)
        {
            if (dgOrder.Items.Count > 0)
            {
                if (!SelectImageRequired())
                {
                    AddNewRow(null, "", true, -1);
                    if (dgOrder.Items.Count - 1 > 0)
                    {
                        dgOrder.SelectedItem = dgOrder.Items[dgOrder.Items.Count - 1];
                    }
                    else
                    {
                        dgOrder.SelectedItem = dgOrder.Items[0];
                    }
                    if (!CheckOnlineUploadQRCode())
                    {
                        MessageBox.Show("Please Enter QR/Bar code.", "Digiphoto");
                        return;
                    }
                }
                else
                {
                    MessageBox.Show("Please select atleast one image per line item to continue..", "DigiPhoto");
                }
            }
            else
            {
                AddNewRow(null, "", true, -1);
                if (dgOrder.Items.Count - 1 > 0)
                {
                    dgOrder.SelectedItem = dgOrder.Items[dgOrder.Items.Count - 1];
                }
                else
                {
                    dgOrder.SelectedItem = dgOrder.Items[0];
                }
                if (!CheckOnlineUploadQRCode())
                {
                    MessageBox.Show("Please Enter QR/Bar code.", "Digiphoto");
                    return;
                }
            }
            LoadProductTypeControl(sender);
            CtrlSelectedQrCode.QRCode = string.Empty;////changed by latika
        }

        /// <summary>
        /// Gets the group images.
        /// </summary>
        /// <param name="itemNumber">The item number.</param>
        /// <returns></returns>
        public List<LstMyItems> GetGroupImages(String itemNumber, int? IsPrintType)
        {
            if (IsPrintType == 0)
            {
                List<LstMyItems> items = RobotImageLoader.GroupImages;
                items.Select(c => { c.FrameBrdr = itemNumber; return c; }).ToList();
                return items;
            }
            else
            {
                List<LstMyItems> items = RobotImageLoader.PrintImages;
                items.Select(c => { c.FrameBrdr = itemNumber; return c; }).ToList();
                return items;
            }
        }

        /// <summary>
        /// Handles the Click event of the btndelete control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void btndelete_Click(object sender, RoutedEventArgs e)
        {
            LineItem ToBeDeleteditem = (LineItem)dgOrder.CurrentItem;
            Button source = (Button)sender;
            if (source.Tag != null)
            {
                if (source.Tag.ToString() != "-1")
                {
                    var Childitem = (from lineitem in _objLineItem
                                     where lineitem.ParentID == source.Tag.ToString()
                                     select lineitem).ToList();

                    if (Childitem != null)
                    {
                        foreach (var children in Childitem)
                        {
                            _objLineItem.Remove(children);
                        }
                    }
                }

                var item = (from lineitem in _objLineItem
                            where lineitem.ItemNumber == ToBeDeleteditem.ItemNumber.ToString()
                            select lineitem).FirstOrDefault();
                if (item != null)
                {
                    _objLineItem.Remove(item);
                }

            }
            ShowTotalPrice();
            CtrlSelectedQrCode.QRCode = string.Empty;
        }

        private bool SelectImageRequired()
        {
            bool result = false;
            int counter = 0;
            foreach (var lineitem in _objLineItem)
            {
                DataGridRow row = (DataGridRow)dgOrder.ItemContainerGenerator.ContainerFromIndex(counter);
                Button btnSelectImage = FindByName("btnSelectImage", row) as Button;
                Button btnSpecSelectImage = FindByName("btnAddSemiPrinted", row) as Button;
                if (btnSelectImage.Visibility != System.Windows.Visibility.Collapsed || btnSpecSelectImage.Visibility != System.Windows.Visibility.Collapsed)
                {
                    if (!lineitem.IsAccessory)
                    {
                        if (lineitem.SelectedImages == null)
                        {
                            if (row != null)
                            {
                                row.Background = Brushes.LightYellow;
                            }
                            result = true;
                        }
                        else if (lineitem.SelectedImages.Count == 0)
                        {
                            if (row != null)
                            {
                                row.Background = Brushes.LightYellow;
                            }
                            result = true;
                        }
                    }
                }
                counter++;
            }
            if (result == true)
            {
                int productID, packageID = 0;
                if (_objLineItem != null && _objLineItem.Count > 1)
                {
                    packageID = _objLineItem[0].SelectedProductType_ID;
                    productID = _objLineItem[1].SelectedProductType_ID;
                    if (productID == 84 && new OrderBusiness().chKIsWaterMarkedOrNot(packageID))
                    {
                        if (RobotImageLoader.GroupImages.Count() > 0)
                        {
                            result = false;
                        }
                    }
                }
            }
            return result;
        }

        private void SaveOrderDetails()
        {
            try
            {
                ErrorHandler.ErrorHandler.LogError("Line Number: SaveOrderDetails : 551");
                List<LstMyItems> GroupImage = RobotImageLoader.GroupImages;
                StringBuilder itemDetail = new StringBuilder();
                String outPutImages = string.Empty;
                List<BurnImagesForCD> burnedImages = new List<BurnImagesForCD>();
                string images = string.Empty;
                int maxGap = 0;
                List<LinetItemsDetails> billDetails = new List<LinetItemsDetails>();
                int parentId = -1;
                ErrorHandler.ErrorHandler.LogError("Line Number: SaveOrderDetails : 560");
                var lineItem = from result in _objLineItem
                               where result.ParentID == result.ItemNumber
                               select result;
                ErrorHandler.ErrorHandler.LogError("Line Number: SaveOrderDetails : 564");
                foreach (LineItem item in lineItem)
                {
                    images = string.Empty;
                    if (!item.IsPackage)  // check item is not a package
                    {
                        ErrorHandler.ErrorHandler.LogError("Line Number: SaveOrderDetails : 570");
                        if (!item.IsBundled) // check item is not a Bundled like four by four image
                        {
                            int copy = item.Quantity;
                            int iteration = 1;
                            if (!item.IsAccessory)
                            {
                                foreach (var img in item.SelectedImages)
                                {
                                    iteration = 1;
                                    while (iteration <= copy)
                                    {
                                        String Space = string.Empty;
                                        int counter = maxGap - item.SelectedProductType_Text.Length;
                                        counter = counter + 35;

                                        for (int i = 1; i <= counter; i++)
                                        {
                                            Space += " ";
                                        }

                                        iteration++;
                                        ErrorHandler.ErrorHandler.LogError("Line Number: SaveOrderDetails : 592");
                                        itemDetail.AppendLine(item.SelectedProductType_Text + Space + item.UnitPrice.ToString(".00"));
                                    }
                                    if (images == string.Empty)
                                    {
                                        images = img;
                                    }
                                    else
                                    {
                                        images += "," + img;
                                    }
                                }
                            }
                            LinetItemsDetails _obj = new LinetItemsDetails();
                            _obj.Productname = item.SelectedProductType_Text;
                            _obj.Productprice = item.UnitPrice.ToString(".00");
                            if (!item.IsAccessory)
                            {
                                ErrorHandler.ErrorHandler.LogError("Line Number: SaveOrderDetails : 610");
                                _obj.Productquantity = (item.Quantity * item.SelectedImages.Count).ToString();
                            }
                            else
                            {
                                _obj.Productquantity = item.Quantity.ToString();
                            }
                            billDetails.Add(_obj);
                        }
                        else
                        {
                            String space = string.Empty;
                            int counter = maxGap - item.SelectedProductType_Text.Length;
                            counter = counter + 35;
                            for (int i = 1; i <= counter; i++)
                            {
                                space += " ";
                            }
                            int copy = item.Quantity;
                            int iteration = 1;

                            while (iteration <= copy)
                            {
                                itemDetail.AppendLine(item.SelectedProductType_Text + space + item.UnitPrice.ToString(".00"));
                                iteration++;
                            }
                            foreach (var img in item.SelectedImages)
                            {
                                if (images == string.Empty)
                                {
                                    images = img;
                                }
                                else
                                {
                                    images += "," + img;
                                }
                            }

                            LinetItemsDetails obj = new LinetItemsDetails();
                            obj.Productname = item.SelectedProductType_Text;
                            obj.Productprice = item.UnitPrice.ToString(".00");
                            obj.Productquantity = (item.Quantity).ToString();
                            billDetails.Add(obj);
                        }
                        ErrorHandler.ErrorHandler.LogError("Line Number: SaveOrderDetails : 654");
                        string OrderDetailsSyncCode = CommonUtility.GetUniqueSynccode(Convert.ToInt32(ApplicationObjectEnum.OrderDetails).ToString().PadLeft(2, '0'), LoginUser.countrycode, LoginUser.Storecode, LoginUser.SubStoreId.ToString());
                        int id = (new OrderBusiness()).SaveOrderLineItems(item.SelectedProductType_ID, null, images, item.Quantity, item.TotalDiscount, (decimal)item.TotalDiscountAmount, (decimal)item.UnitPrice, (decimal)item.TotalPrice, (decimal)item.NetPrice, parentId, LoginUser.SubStoreId, item.CodeType, item.UniqueCode, OrderDetailsSyncCode, item.EvoucherCode, null);
                        ErrorHandler.ErrorHandler.LogError("Line Number: SaveOrderDetails : 657");
                        //Call method to save album print order incase of productType=79
                        if (item.SelectedProductType_ID == 79 && id > 0 && !string.IsNullOrEmpty(images) && item.GroupItems.Count() > 0)
                        {
                            (new PrinterBusniess()).SaveAlbumPrintPosition(id, item.PrintPhotoOrderPosition);
                        }
                        ErrorHandler.ErrorHandler.LogError("Line Number: SaveOrderDetails : 663");
                        if (item.SelectedProductType_ID == 35 || item.SelectedProductType_ID == 36 || item.SelectedProductType_ID == 78) // Check for CD burning and USB option
                        {
                            foreach (var img in item.SelectedImages)
                            {
                                BurnImagesForCD objnew = new BurnImagesForCD();
                                objnew.ImageID = img.ToInt32();
                                objnew.Producttype = item.SelectedProductType_ID;
                                burnedImages.Add(objnew);
                            }
                        }
                        else
                        {
                            //if (!item.IsAccessory && item.SelectedProductType_ID != 84) //item acessory does not Insert into Printqueue 
                            if (item.SelectedProductType_ID != 127 && item.SelectedProductType_ID != 128 && item.SelectedProductType_ID != 95 && item.SelectedProductType_ID != 84) //item whatsapp, wechat & online ,video does not Insert into Printqueue
                            {
                                ErrorHandler.ErrorHandler.LogError("Line Number: SaveOrderDetails : 679");
                                if (!_issemiorder)
                                {
                                    AddToPrintQueue(item, id, item.PrintPhotoOrderPosition);

                                }
                            }
                        }
                    }
                    else
                    {
                        // get All the Chiuldren of Package
                        var Children = from result in _objLineItem
                                       where result.ParentID == item.ItemNumber && result.IsPackage == false
                                       select result;
                        string OrderDetailsSyncCode = CommonUtility.GetUniqueSynccode(Convert.ToInt32(ApplicationObjectEnum.OrderDetails).ToString().PadLeft(2, '0'), LoginUser.countrycode, LoginUser.Storecode, LoginUser.SubStoreId.ToString());
                        parentId = (new OrderBusiness()).SaveOrderLineItems(item.SelectedProductType_ID, null, images, item.Quantity, item.TotalDiscount,
                            (decimal)item.TotalDiscountAmount, (decimal)item.UnitPrice, (decimal)item.TotalPrice, (decimal)item.NetPrice, -1,
                            LoginUser.SubStoreId, item.CodeType, item.UniqueCode, OrderDetailsSyncCode, item.EvoucherCode, null);
                        //Call method to save album print order incase of productType=79
                        if (item.SelectedProductType_ID == 79 && parentId > 0 && !string.IsNullOrEmpty(images) && item.GroupItems.Count() > 0)
                        {
                            (new PrinterBusniess()).SaveAlbumPrintPosition(parentId, item.PrintPhotoOrderPosition);
                        }

                        String space = string.Empty;
                        int counter = maxGap - item.SelectedProductType_Text.Length;
                        counter = counter + 35;

                        for (int i = 1; i <= counter; i++)
                        {
                            space += " ";
                        }

                        itemDetail.AppendLine(item.SelectedProductType_Text + space + item.UnitPrice.ToString(".00"));

                        LinetItemsDetails _obj = new LinetItemsDetails();
                        _obj.Productname = item.SelectedProductType_Text;
                        _obj.Productprice = item.UnitPrice.ToString(".00");
                        _obj.Productquantity = (item.Quantity).ToString();
                        billDetails.Add(_obj);

                        foreach (LineItem Citem in Children)
                        {
                            images = string.Empty;
                            if (!Citem.IsAccessory)
                            {
                                foreach (var img in Citem.SelectedImages)
                                {
                                    if (images == string.Empty)
                                    {
                                        images = img;
                                    }
                                    else
                                    {
                                        images += "," + img;
                                    }
                                }
                            }
                            int Id = 0;
                            if (!item.IssemiOrder)
                            {
                                (new OrderBusiness()).setSemiOrderImageOrderDetails(null, images.ToString(), parentId, LoginUser.SubStoreId, item.TotalDiscount, 0, 0, 0);
                            }
                            else
                            {
                                string OrderDetailsSyncCode1 = CommonUtility.GetUniqueSynccode(Convert.ToInt32(ApplicationObjectEnum.OrderDetails).ToString().PadLeft(2, '0'), LoginUser.countrycode, LoginUser.Storecode, LoginUser.SubStoreId.ToString());
                                Id = (new OrderBusiness()).SaveOrderLineItems(Citem.SelectedProductType_ID, null, images, Citem.Quantity, item.TotalDiscount, 0, 0, 0, 0, parentId, LoginUser.SubStoreId, item.CodeType, item.UniqueCode, OrderDetailsSyncCode1, item.EvoucherCode, null);

                                //Call method to save album print order incase of productType=79
                                if (Citem.SelectedProductType_ID == 79 && Id > 0 && !string.IsNullOrEmpty(images) && Citem.GroupItems.Count() > 0)
                                {
                                    (new PrinterBusniess()).SaveAlbumPrintPosition(Id, Citem.PrintPhotoOrderPosition);
                                }
                            }

                            if (Citem.SelectedProductType_ID == 35 || Citem.SelectedProductType_ID == 36 || Citem.SelectedProductType_ID == 78) // Check for CD burning and USB option
                            {
                                foreach (var img in Citem.SelectedImages)
                                {
                                    BurnImagesForCD _objnew = new BurnImagesForCD();
                                    _objnew.ImageID = img.ToInt32();
                                    _objnew.Producttype = Citem.SelectedProductType_ID;
                                    burnedImages.Add(_objnew);
                                }
                            }
                            else
                            {
                                //if (!Citem.IsAccessory && item.SelectedProductType_ID != 84) //item acessory does not Insert into Printqueue
                                if (Citem.SelectedProductType_ID != 127 && Citem.SelectedProductType_ID != 128 && Citem.SelectedProductType_ID != 95 && Citem.SelectedProductType_ID != 84) //item whatsapp, wechat & online ,video does not Insert into Printqueue
                                {
                                    AddToPrintQueue(Citem, Id, Citem.PrintPhotoOrderPosition);
                                }
                            }

                            images = string.Empty;
                        }
                    }
                }

                System.Threading.Thread.Sleep(500);
                Burningimage mw = new Burningimage();
                if (burnedImages.Count > 0)
                {
                    string ApplicationPath = Environment.CurrentDirectory + "\\DigiOrderdImages\\";

                    if (System.IO.Directory.Exists(ApplicationPath))
                    {
                        System.IO.Directory.Delete(ApplicationPath, true);
                        System.IO.Directory.CreateDirectory(ApplicationPath);
                    }
                    mw.Show();
                    foreach (var itemid in burnedImages)
                    {
                        StartEngine(itemid.ImageID.ToInt32(), mw);

                        if (outPutImages == string.Empty)
                            outPutImages = itemid.ImageID.ToString();
                        else
                            outPutImages += "," + itemid.ImageID;
                    }

                    bool drivefound = false;

                    if (System.IO.Directory.Exists(ApplicationPath))
                    {
                        var TobeTransFertoUsb = (from result in burnedImages
                                                 where result.Producttype == 36
                                                 select result.ImageID).ToList();

                        var TobeBurnedintoCD = (from result in burnedImages
                                                where result.Producttype == 35
                                                select result.ImageID).ToList();

                        if (TobeTransFertoUsb.Count > 0)
                        {
                            DriveInfo[] ListDrives = DriveInfo.GetDrives();
                            string TargetPath = string.Empty;
                            foreach (DriveInfo Drive in ListDrives)
                            {
                                if (Drive.DriveType == DriveType.Removable)
                                {
                                    TargetPath = Drive.RootDirectory.ToString();
                                    drivefound = true;
                                }
                            }

                            if (!drivefound)
                            {
                                MessageBox.Show("No USB Drive Found ,please reinsert usb or cd again and press OK then");
                                ListDrives = DriveInfo.GetDrives();

                                foreach (DriveInfo Drive in ListDrives)
                                {
                                    if (Drive.DriveType == DriveType.Removable)
                                    {
                                        TargetPath = Drive.RootDirectory.ToString();
                                        drivefound = true;
                                    }
                                }
                            }
                            if (!drivefound)
                            {
                                MessageBox.Show("Sorry your order could not be processed.", "DigiPhoto");
                            }
                            else
                            {
                                foreach (int fileid in TobeTransFertoUsb)
                                {
                                    if (File.Exists(ApplicationPath + fileid.ToString() + ".jpg"))
                                    {
                                        File.Copy(ApplicationPath + fileid.ToString() + ".jpg", TargetPath + "\\" + fileid.ToString() + ".jpg", true);
                                    }
                                }
                            }
                        }
                        if (TobeBurnedintoCD.Count > 0)
                        {
                            if (!Directory.Exists(ApplicationPath + "\\DigiCD"))
                            {
                                Directory.CreateDirectory(ApplicationPath + "\\DigiCD");
                            }
                            else
                            {
                                // write code for existing files remove
                            }

                            foreach (int fileid in TobeBurnedintoCD)
                            {
                                if (File.Exists(ApplicationPath + fileid.ToString() + ".jpg"))
                                {
                                    File.Copy(ApplicationPath + fileid.ToString() + ".jpg", ApplicationPath + "\\DigiCD\\" + fileid.ToString() + ".jpg", true);
                                }
                            }

                            XPBurnCD cd = new XPBurnCD();
                            DirectoryInfo dir = new DirectoryInfo(ApplicationPath + "\\DigiCD\\");
                            foreach (FileInfo file in dir.GetFiles())
                            {
                                cd.AddFile(file.FullName, file.Name);
                            }
                            try
                            {
                                cd.RecordDisc(false, false);
                            }
                            catch (Exception ex)
                            {
                                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                            }
                        }

                        if (mw != null)
                        {
                            mw.Close();
                        }
                    }
                }
            }
            catch(Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(" SaveOrderDetails : Error Logs : " + ex.Message + "  Inner Exception :" + ex.InnerException.ToString());
            }
        }

        private void PrepareImage(PhotoInfo objphoto, string destinationPath, Burningimage mw)
        {
            if (objphoto != null)
            {
                if (!string.IsNullOrEmpty(objphoto.DG_Photos_Effects))
                {
                    mw.ImageEffect = objphoto.DG_Photos_Effects;
                }
                else
                {
                    mw.ImageEffect = "<image brightness = '0' contrast = '1' Crop='##' colourvalue = '##' rotate='##' ><effects sharpen='##' greyscale='0' digimagic='0' sepia='0' defog='0' underwater='0' emboss='0' invert = '0' granite='0' hue ='##' cartoon = '0'></effects></image>";
                }
            }
            mw.WindowStyle = System.Windows.WindowStyle.None;
            mw.ResetForm(objphoto);
            mw.UpdateLayout();
            RenderTargetBitmap _objeditedImage = mw.jCaptureScreen();

            using (var fileStream = new FileStream(destinationPath, FileMode.Create, FileAccess.ReadWrite))
            {
                JpegBitmapEncoder encoder = new JpegBitmapEncoder();
                encoder.QualityLevel = 94;
                encoder.Frames.Add(BitmapFrame.Create(_objeditedImage));
                encoder.Save(fileStream);
            }
        }
        private bool IsEffectApplied(PhotoInfo _objphoto)
        {
            if (string.IsNullOrEmpty(_objphoto.DG_Photos_Effects) || string.Compare(_objphoto.DG_Photos_Effects, _defaultEffect.TrimStart().TrimEnd(), true) == 0)
            {
                //No effect applied. Check Layering.
                if (string.IsNullOrEmpty(_objphoto.DG_Photos_Layering) || _objphoto.DG_Photos_Layering == "test" || string.Compare(_objphoto.DG_Photos_Layering, _defaultSpecPrintLayer.TrimStart().TrimEnd(), true) == 0)
                    return false;
            }
            return true;
        }



        /// <summary>
        /// Starts the engine.
        /// </summary>
        /// <param name="imgID">The img unique identifier.</param>
        /// <param name="MW">The mw.</param>
        private void StartEngine(int imgID, Burningimage MW)
        {
            try
            {
                PhotoInfo objphoto = (new PhotoBusiness()).GetPhotoDetailsbyPhotoId(imgID);
                MW.PhotoName = objphoto.DG_Photos_RFID;
                MW.PhotoId = imgID;
                string editedImagePath = System.IO.Path.Combine(objphoto.HotFolderPath, "EditedImages", objphoto.DG_Photos_FileName);
                string ApplicationPath = Environment.CurrentDirectory + "\\DigiOrderdImages\\";

                if (!System.IO.Directory.Exists(ApplicationPath))
                    System.IO.Directory.CreateDirectory(ApplicationPath);

                if (File.Exists(editedImagePath))
                {
                    File.Copy(editedImagePath, System.IO.Path.Combine(ApplicationPath, ApplicationPath + imgID + ".jpg"), true);
                }
                else if (!IsEffectApplied(objphoto))
                {
                    File.Copy(System.IO.Path.Combine(objphoto.HotFolderPath, objphoto.DG_Photos_CreatedOn.ToString("yyyyMMdd"), objphoto.DG_Photos_FileName), System.IO.Path.Combine(ApplicationPath, ApplicationPath + imgID + ".jpg"), true);
                }
                else
                    PrepareImage(objphoto, ApplicationPath + imgID + ".jpg", MW);
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
            finally
            {
                MemoryManagement.FlushMemory();
            }
        }
        /// <summary>
        /// Handles the Unchecked event of the ChkSelected control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void ChkSelected_Unchecked(object sender, RoutedEventArgs e)
        {
            CheckBox checkbox = e.OriginalSource as CheckBox;
            DependencyObject dep = (DependencyObject)checkbox;
            while ((dep != null) && !(dep is ComboBoxItem))
            {
                dep = VisualTreeHelper.GetParent(dep);
            }

            ComboBoxItem objmyitem = (ComboBoxItem)dep;
            LstMyItems ItemNumber = (LstMyItems)objmyitem.Content;

            if (checkbox != null)
            {
                LineItem CurrentRecord = (LineItem)dgOrder.CurrentItem;
                var image = string.Empty;
                if (CurrentRecord.SelectedImages != null)
                {
                    image = (from lineitem in CurrentRecord.SelectedImages
                             where lineitem == checkbox.Uid.ToString()
                             select lineitem).FirstOrDefault();
                    if (image != null)
                    {
                        if (image.Count() > 0)
                        {
                            CurrentRecord.SelectedImages.Remove(checkbox.Uid.ToString());
                            DataGridRow row = (DataGridRow)dgOrder.ItemContainerGenerator.ContainerFromIndex(dgOrder.SelectedIndex);
                            System.Windows.Controls.Primitives.DataGridCellsPresenter presenter = GetVisualChild<System.Windows.Controls.Primitives.DataGridCellsPresenter>(row);

                            DataGridCell cell = (DataGridCell)presenter.ItemContainerGenerator.ContainerFromIndex(3);
                            cell.Content = CurrentRecord.SelectedImages.Count;
                        }
                    }
                }
                else
                {
                    CurrentRecord.SelectedImages = new List<string>();
                    CurrentRecord.SelectedImages.Remove(checkbox.Uid.ToString());
                    DataGridRow row = (DataGridRow)dgOrder.ItemContainerGenerator.ContainerFromIndex(dgOrder.SelectedIndex);
                    System.Windows.Controls.Primitives.DataGridCellsPresenter presenter = GetVisualChild<System.Windows.Controls.Primitives.DataGridCellsPresenter>(row);

                    DataGridCell cell = (DataGridCell)presenter.ItemContainerGenerator.ContainerFromIndex(3);
                    cell.Content = CurrentRecord.SelectedImages.Count;

                    UpdatePricing(CurrentRecord);
                }
            }
        }
        /// <summary>
        /// Handles the Checked event of the ChkSelected control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void ChkSelected_Checked(object sender, RoutedEventArgs e)
        {
            CheckBox checkbox = e.OriginalSource as CheckBox;
            DependencyObject dep = (DependencyObject)checkbox;
            while ((dep != null) && !(dep is ComboBoxItem))
            {
                dep = VisualTreeHelper.GetParent(dep);
            }

            ComboBoxItem objmyitem = (ComboBoxItem)dep;
            LstMyItems ItemNumber = (LstMyItems)objmyitem.Content;

            if (checkbox != null)
            {
                LineItem CurrentRecord = (LineItem)dgOrder.CurrentItem;

                var Image = "";
                if (CurrentRecord.SelectedImages != null)
                {
                    Image = (from lineitem in CurrentRecord.SelectedImages
                             where lineitem == checkbox.Uid.ToString()
                             select lineitem).FirstOrDefault();

                    if (Image == null)
                    {
                        CurrentRecord.SelectedImages.Add(checkbox.Uid.ToString());
                        DataGridRow row = (DataGridRow)dgOrder.ItemContainerGenerator.ContainerFromIndex(dgOrder.SelectedIndex);
                        System.Windows.Controls.Primitives.DataGridCellsPresenter presenter = GetVisualChild<System.Windows.Controls.Primitives.DataGridCellsPresenter>(row);

                        DataGridCell cell = (DataGridCell)presenter.ItemContainerGenerator.ContainerFromIndex(3);
                        cell.Content = CurrentRecord.SelectedImages.Count;
                    }
                }
                else
                {
                    CurrentRecord.SelectedImages = new List<string>();
                    CurrentRecord.SelectedImages.Add(checkbox.Uid.ToString());
                    DataGridRow row = (DataGridRow)dgOrder.ItemContainerGenerator.ContainerFromIndex(dgOrder.SelectedIndex);
                    System.Windows.Controls.Primitives.DataGridCellsPresenter presenter = GetVisualChild<System.Windows.Controls.Primitives.DataGridCellsPresenter>(row);

                    DataGridCell cell = (DataGridCell)presenter.ItemContainerGenerator.ContainerFromIndex(3);
                    cell.Content = CurrentRecord.SelectedImages.Count;
                }
                if (CurrentRecord.SelectedImages != null)
                {
                    UpdatePricing(CurrentRecord);
                }
            }
        }
        /// <summary>
        /// Gets the visual child.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="parent">The parent.</param>
        /// <returns></returns>
        static T GetVisualChild<T>(Visual parent) where T : Visual
        {
            T child = default(T);
            int numVisuals = VisualTreeHelper.GetChildrenCount(parent);
            for (int i = 0; i < numVisuals; i++)
            {
                Visual v = (Visual)VisualTreeHelper.GetChild(parent, i);
                child = v as T;
                if (child == null)
                {
                    child = GetVisualChild<T>(v);
                }
                if (child != null)
                {
                    break;
                }
            }
            return child;
        }
        /// <summary>
        /// Handles the SelectionChanged event of the ProductType control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="SelectionChangedEventArgs"/> instance containing the event data.</param>
        private void ProductType_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ComboBox cmb = (ComboBox)sender;
            LineItem CurrentRecord = (LineItem)dgOrder.CurrentItem;

            CurrentRecord.SelectedProductType_ID = cmb.SelectedValue.ToInt32();
            Product product = (Product)cmb.SelectedItem;
            CurrentRecord.AllowDiscount = product.DiscountOption;

            CurrentRecord.SelectedProductType_Text = product.ProductName.ToString();
            CurrentRecord.SelectedProductType_Image = product.ProductIcon.ToString();

            List<PrinterDetailsInfo> objAssociatePrinter = (new PrinterBusniess()).GetAssociatedPrinters(cmb.SelectedValue.ToInt32());
            CurrentRecord.AssociatedPrinters = objAssociatePrinter;

            UpdatePricing(CurrentRecord);
        }

        private void dgOrder_Loaded(object sender, RoutedEventArgs e)
        {

        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            AddNewRow(null, "", true, -1);
        }
        /// <summary>
        /// Gets the default currency.
        /// </summary>
        /// <returns></returns>
        private string GetDefaultCurrency()
        {
            var currency = (from objcurrency in (new CurrencyBusiness()).GetCurrencyList()
                            where objcurrency.DG_Currency_Default == true
                            select objcurrency.DG_Currency_Symbol.ToString()).FirstOrDefault();
            return currency;
        }
        /// <summary>
        /// Handles the Click event of the btnNextsimg control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>


        private void btnNextsimg_Click(object sender, RoutedEventArgs e)
        {
            if (_objLineItem.Count > 0)
            {
                var isVoucher = _objLineItem.Count(itm => itm.EvoucherCode != null);////changed by latika for evoucher 25 March2020
                if (!SelectImageRequired())
                {
                    if (!CheckOnlineUploadQRCode())
                    {
                        MessageBox.Show("Please Enter QR/Bar code.", "Digiphoto");
                        return;
                    }
                    if (!CheckISARPersonalisedValid())
                    {
                        MessageBox.Show("Please Select the One Video and One Image for Personalised Product.", "Digiphoto");
                        return;
                    }
                    var result = (new ConfigBusiness()).GetConfigurationData(LoginUser.SubStoreId);
                    ErrorHandler.ErrorHandler.LogFileWrite("btnNextsimg_Click SaveOrderDetails before : 1172 ---------------");
                    if (result.PosOnOff == true)
                    {
                         ErrorHandler.ErrorHandler.LogFileWrite("btnNextsimg_Click SaveOrderDetails before : 1175 ---------------");
                        SaveOrderDetails();
                          ErrorHandler.ErrorHandler.LogFileWrite("btnNextsimg_Click SaveOrderDetails after : 1177 ---------------");
                        SearchResult window = null;
                        foreach (Window wnd in Application.Current.Windows)
                        {
                            if (wnd.Title == "View/Order Station")
                            {
                                window = (SearchResult)wnd;
                            }
                        }
                        if (window == null)
                        {
                            window = new SearchResult();
                        }
                        RobotImageLoader.SearchCriteria = "PhotoId";
                        window.pagename = "";
                        window.Savebackpid = "";
                        window.Show();
                        ////////Calling logic of searching 
                        window.LoadWindow();
                        this.Close();
                    }
                    else if ((bool)Common.LoginUser.IsDiscountAllowed && isVoucher < 1)////changed by latika for evoucher 25 march 20202
                    {/////End Code///////

                        ///else if ((bool)Common.LoginUser.IsDiscountAllowed)

                        SelectPrinter objSelectPrinter = new SelectPrinter();
                        objSelectPrinter.ImagesToPrint = _objLineItem;
                        objSelectPrinter.SourceParent = this;

                        foreach (LineItem item in _objLineItem)
                        {
                            if (item.IsPackage)
                            {
                                item.TotalQty = item.Quantity;
                            }
                            else if (item.IsBundled)
                            {
                                item.TotalQty = item.Quantity;
                            }
                            else
                            {
                                if (item.IsAccessory)
                                {
                                    item.TotalQty = item.Quantity;
                                }
                                else
                                {
                                    if (item.SelectedImages != null)
                                    {
                                        item.TotalQty = item.Quantity * item.SelectedImages.Count;
                                    }
                                    else
                                    {
                                        item.TotalQty = item.Quantity;
                                    }
                                }
                            }
                        }
                        objSelectPrinter._issemiorder = _issemiorder;
                        objSelectPrinter.Show();
                    }
                    else
                    {
                        Double total = 0;
                        Double Evouchertotal = 0;
                        EvoucherManager objevchr = new EvoucherManager();
                        foreach (LineItem item in _objLineItem)
                        {
                            if (item.IsPackage)
                            {
                                item.TotalQty = item.Quantity;

                                ///////changed by latika for Evoucher
                                if (item.IsEvoucherDisc == true && item.EvoucherCode.Length > 4 && item.EvoucherPer > 1)
                                {
                                    Evouchertotal += (((item.TotalPrice * item.EvoucherPer) / 100) * 100) / 100;

                                    //  item.TotalDiscountAmount= (((item.TotalPrice * item.EvoucherPer) / 100) * 100) / 100;

                                    //objevchr.SaveEvoucherItemDtls(item.EvoucherCode, item.ItemTemplateDetailId,Convert.ToDecimal(Evouchertotal));
                                }
                                //////End
                            }
                            else if (item.IsBundled)
                            {
                                item.TotalQty = item.Quantity;
                            }
                            else
                            {
                                if (item.SelectedImages != null)
                                {
                                    item.TotalQty = item.Quantity * item.SelectedImages.Count;
                                }
                                else
                                {
                                    item.TotalQty = item.Quantity;
                                }
                            }
                        }

                        foreach (LineItem item in _objLineItem)
                        {
                            total += (item.TotalPrice);
                        }

                        GetDefaultCurrency();
                        Payment obj = new Payment();
                        obj.TotalBill = total;
                        obj.SourceParent = this;
                        obj.PaybleAmount = total.ToDouble();
                        obj.EvoucherAmnt = Evouchertotal.ToDouble();//////changed by latika for evoucher 25 mar 2020

                        obj.ImagesToPrint = _objLineItem;
                        obj.DefaultCurrency = GetDefaultCurrency();
                        obj.Show();
                        this.Visibility = Visibility.Hidden;
                        obj.ShowPopup();
                    }

                    this.Visibility = System.Windows.Visibility.Hidden;
                    #region Modified below code on 18Feb20  by ajay sinha to solve the minimize issue and group delete issue.
                    ///below code commented by Ajay as issue of minimize / Print icon not working full screen preview  and written new code
                    //////////////////created by latika ////for clear the grouping images ////////////////////////////////
                    //ConfigBusiness configBiz = new ConfigBusiness();
                    //if (Convert.ToBoolean(configBiz.GETGroupDeleteID(LoginUser.SubStoreId)) == true)
                    //{
                    //    try
                    //    {
                    //        SearchResult frmResult = new SearchResult();
                    //        frmResult.ClearGroup(null, null);
                    //        // RobotImageLoader.GroupImages.Clear();
                    //        frmResult = null;
                    //    }
                    //    catch { }


                    //}
                    ConfigBusiness configBiz = new ConfigBusiness();
                    if (Convert.ToBoolean(configBiz.GETGroupDeleteID(LoginUser.SubStoreId)) == true)
                    {
                        try
                        {
                            RobotImageLoader.currentCount = 0;
                            RobotImageLoader.IsZeroSearchNeeded = true;
                            if (RobotImageLoader.ViewGroupedImagesCount != null)
                                RobotImageLoader.ViewGroupedImagesCount.Clear();
                            ////For showing the paginate buttons and hiding the scrollbars
                            if (RobotImageLoader.GroupImages != null)
                            {
                                /////////////////////////Logic to save group photos for future use of Groups//////////////
                                if (RobotImageLoader.GroupImages.Count > 0)
                                {
                                    RobotImageLoader.GroupImages.Clear();
                                    if (RobotImageLoader.PrintImages == null)
                                        RobotImageLoader.PrintImages = new List<LstMyItems>();
                                    RobotImageLoader.PrintImages.Clear();
                                    RobotImageLoader.RFID = "0";
                                    RobotImageLoader.SearchCriteria = "";
                                    RobotImageLoader.LoadImages();
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                            ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                        }
                    }
                    #endregion
                }
                else
                {
                    MessageBox.Show("Please select atleast one image per line item to continue..", "DigiPhoto");
                }
            }
            else
            {
                MessageBox.Show("Please select atleast one line item to continue..", "DigiPhoto");
            }
        }

        /// <summary>
        /// Handles the Click event of the btnExit control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void btnExit_Click(object sender, RoutedEventArgs e)
        {
            SearchResult objSearchResult = null;

            foreach (Window wnd in Application.Current.Windows)
            {
                if (wnd.Title == "View/Order Station")
                {
                    objSearchResult = (SearchResult)wnd;
                }
            }

            if (objSearchResult == null)
                objSearchResult = new SearchResult();

            if (RobotImageLoader.GroupImages.Count > 0)
            {
                var SBItems = RobotImageLoader.GroupImages.Where(t => t.StoryBookId > 0 && t.IsVosDisplay == false).ToList();
                if (SBItems != null)
                {
                    foreach (var itm in SBItems)
                    {
                        RobotImageLoader.GroupImages.Remove(itm);
                    }
                }
                objSearchResult.pagename = "MainGroup";
            }
            else if (RobotImageLoader.PrintImages.Count > 0)
            {
                objSearchResult.pagename = "Placeback";
                objSearchResult.Savebackpid = RobotImageLoader.PrintImages[0].PhotoId.ToString(); //RobotImageLoader.PrintImages[0].Name.ToString();
            }

            var printInGroup = RobotImageLoader.GroupImages.Where(pb => pb.PrintGroup.UriSource.ToString() == @"/images/print-accept.png").ToList();
            for (int pg = 0; pg < printInGroup.Count; pg++)
            {
                printInGroup[pg].PrintGroup = new BitmapImage(new Uri(@"/images/print-group.png", UriKind.Relative));
            }

            objSearchResult.Show();
            objSearchResult.flgViewScrl = true;
            objSearchResult.LoadWindow();
            this.Close();
        }

        /// <summary>
        /// Handles the Click event of the btnProductType control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        /// 
        public void LoadProductType(object sender)
        {
            LoadProductTypeControl(sender);
        }
        private void btnProductType_Click(object sender, RoutedEventArgs e)
        {

        }

        private void LoadProductTypeControl(object sender)
        {
            Button btn = (Button)sender;
            bool isPrefilledList = false;
            bool isUSBOrCD = false;
            OnlineUploadrow = null;
            if (btn != null)
            {
                LineItem current;
                if (btn.Name == "btnProductType")
                {
                    current = (LineItem)dgOrder.CurrentItem;
                }
                else
                {
                    if (dgOrder.Items.Count - 1 > 0)
                        current = (LineItem)dgOrder.Items[dgOrder.Items.Count - 1];
                    else
                        current = (LineItem)dgOrder.Items[0];
                }

                if (current.ParentID == current.ItemNumber)
                {
                    CtrlProductType._ProductType = _productType;
                    CtrlProductType.ItemNumber = current.ItemNumber.ToString();
                    CtrlProductType._SelectedProductID = current.SelectedProductType_ID;

                    CtrlProductType.SetParent(this);
                    btnNextsimg.IsDefault = false;
                    CtrlProductType.btnSubmit.IsDefault = true;
                    CtrlProductType.IsActive = true;

                    var res = CtrlProductType.ShowHandlerDialog("ProductType");
                    SelectedImagesQrCode.PackageCode = res;
                    _productType = CtrlProductType._ProductType;
                    CtrlProductType.btnSubmit.IsDefault = false;
                    CtrlProductType.IsActive = true;

                    btnNextsimg.IsDefault = true;
                    bool IsBaseVideo = false;

                    if (res != 0)
                    {
                        if (res != -2)
                        {
                            if (btn.Name == "btnProductType")
                                btn.IsEnabled = false;
                        }
                    }
                    var productType = (from ptype in _productType
                                       where ptype.ProductID == res
                                       select ptype).FirstOrDefault();
                    int index = dgOrder.Items.Count - 1;
                    if (index < 0)
                    {
                        index = 0;
                    }
                    else
                    {
                        index = dgOrder.Items.IndexOf(current);
                    }
                    DataGridRow Parentrow = (DataGridRow)dgOrder.ItemContainerGenerator.ContainerFromIndex(index);

                    if (productType != null)
                    {
                        List<AssociatedPrintersInfo> lstprinters = (new PrinterBusniess()).GetAssociatedPrintersforPrint(productType.ProductID, LoginUser.SubStoreId);
                        if (lstprinters.Count == 0 && productType.IsPackage == false)
                        {
                            MessageBox.Show("No Printer Association exists for that Product.");
                            btn.IsEnabled = true;
                        }
                        else
                        {
                            current.SelectedProductType_ID = productType.ProductID;
                            current.SelectedProductType_Image = productType.ProductIcon.ToString();
                            current.SelectedProductType_Text = productType.ProductName.ToString();
                            current.IsBundled = productType.IsBundled;
                            current.AllowDiscount = productType.DiscountOption;
                            current.SelectedProductCode = productType.ProductCode; // Added by Anisur Rahman for Dubai frame (Product code + Barcode) receipt requirement
                            if (current.SelectedProductType_ID == 35 || current.SelectedProductType_ID == 36 || current.SelectedProductType_ID == 78 || current.SelectedProductType_ID == 96 || current.SelectedProductType_ID == 97)
                            {
                                current.TotalMaxSelectedPhotos = 1;   //Added By Anand on 20-Mar-2014, Accessory CD, Accessory USB, Email will be max quantity as 1
                            }
                            else
                            {
                                current.TotalMaxSelectedPhotos = productType.MaxQuantity;
                            }
                            current.IsPackage = productType.IsPackage;
                            current.IsAccessory = productType.IsAccessory;
                            current.IsWaterMarked = productType.IsWaterMarked;
                            ////////////Latika ruke changed by for Evoucher
                            current.EvoucherCode = productType.EvoucherCode;
                            current.EvoucherPer = productType.EvoucherPer;
                            current.IsEvoucherDisc = productType.IsEvoucherDisc;
                            /////////////end by latika 
                            current.Quantity = 1;
                            
                            if (Parentrow != null)
                            {
                                TextBlock tx = FindByName("txtProductType", Parentrow) as TextBlock;
                                tx.Text = current.SelectedProductType_Text;

                                Image img = FindByName("ProductImage", Parentrow) as Image;

                                img.Source = new BitmapImage(new Uri(current.SelectedProductType_Image.ToString(), UriKind.Relative));
                                current.SelectedImages = new List<string>();

                                ListBox lstimage = FindByName("lstPrintImage", Parentrow) as ListBox;
                                lstimage.ItemsSource = null;

                                TextBlock txParentrow = FindByName("TotalImageSelected", Parentrow) as TextBlock;
                                txParentrow.Text = "Total " + current.SelectedImages.Count + " Items Selected";


                                Button btndelete = FindByName("btndelete", Parentrow) as Button;
                                btndelete.Tag = "-1";
                            }
                            UpdatePricing(current);
                            if (productType.IsPackage)
                            {
                                try
                                {
                                    Button btnQtyUp = FindByName("btnQtyUp", Parentrow) as Button;
                                    btnQtyUp.IsEnabled = true;

                                    Button btnQtyDown = FindByName("btnQtyDown", Parentrow) as Button;
                                    btnQtyDown.IsEnabled = true;

                                    Button btnSelectImage = FindByName("btnSelectImage", Parentrow) as Button;
                                    btnSelectImage.Visibility = System.Windows.Visibility.Collapsed;

                                    Button btndelete = FindByName("btndelete", Parentrow) as Button;
                                    btndelete.Tag = current.ItemNumber.ToString();
                                    btndelete.Visibility = System.Windows.Visibility.Visible;

                                    TextBlock txtQuantity = FindByName("TotalQuantity", Parentrow) as TextBlock;
                                    txtQuantity.Text = "1";

                                    string child = (new PackageBusniess()).GetChildProductType(productType.ProductID);

                                    List<Product> _ProductTypeforPackage = new List<Product>();

                                    List<ProductTypeInfo> productObj = (new ProductBusiness()).GetProductTypeforOrder(0);
                                    foreach (ProductTypeInfo ptype in productObj)
                                    {
                                        Product item = new Product();
                                        item.ProductID = ptype.DG_Orders_ProductType_pkey;
                                        item.ProductName = ptype.DG_Orders_ProductType_Name.ToString();
                                        item.ProductCode = ptype.DG_Orders_ProductType_ProductCode; // Added by Anisur Rahman for Dubai frame (Product code + Barcode) receipt requirement
                                        item.IsBundled = (bool)ptype.DG_Orders_ProductType_IsBundled;
                                        item.ProductIcon = ptype.DG_Orders_ProductType_Image.ToString();
                                        item.DiscountOption = (bool)ptype.DG_Orders_ProductType_DiscountApplied;
                                        item.IsPackage = (bool)ptype.DG_IsPackage;
                                        item.IsAccessory = (bool)ptype.DG_IsAccessory;
                                        item.IsWaterMarked = (bool)ptype.DG_IsWaterMarkIncluded;
                                        item.MaxQuantity = ptype.DG_MaxQuantity;
                                        item.IsPrintType = ptype.IsPrintType;
                                        item.IsPersonalizedAR = ptype.IsPersonalizedAR;
                                        ///////changed by latika for evoucher 25 mar 20202
                                        if (item.ProductCode == current.SelectedProductCode)
                                        {
                                            item.IsEvoucherDisc = current.IsEvoucherDisc;
                                            item.EvoucherPer = current.EvoucherPer;
                                            item.EvoucherCode = current.EvoucherCode;
                                        }
                                        /////end by latika
                                        _ProductTypeforPackage.Add(item);
                                    }
                                    IEnumerable<int> ids = child.Split(',').Select(str => int.Parse(str));
                                    var records = _ProductTypeforPackage.Where(rec => ids.Contains(rec.ProductID));//.OrderByDescending(x => x.ProductID);
                                    string itemnumber = string.Empty;
                                    int accessoryCount = 0;
                                    foreach (var item in records)
                                    {
                                        DataGridRow insertAtRow = (DataGridRow)dgOrder.ItemContainerGenerator.ContainerFromIndex(index + 1);

                                        if (insertAtRow != null)
                                        {
                                            itemnumber = AddNewRow(item, current.ItemNumber.ToString(), false, index + 1);
                                        }
                                        else
                                        {
                                            itemnumber = AddNewRow(item, current.ItemNumber.ToString(), false, index + 1);
                                        }
                                        DataGridRow row = (DataGridRow)dgOrder.ItemContainerGenerator.ContainerFromIndex(_objLineItem.Count - 1);

                                        if (row == null)
                                        {
                                            // May be virtualized, bring into view and try again.
                                            dgOrder.UpdateLayout();
                                            row = (DataGridRow)dgOrder.ItemContainerGenerator.ContainerFromIndex(_objLineItem.Count - 1);
                                        }
                                        if (row != null)
                                        {
                                            TextBlock tx = FindByName("txtProductType", row) as TextBlock;
                                            tx.Text = item.ProductName.ToString();

                                            Image img = FindByName("ProductImage", row) as Image;

                                            img.Source = new BitmapImage(new Uri(item.ProductIcon.ToString(), UriKind.Relative));

                                            //if (item.ProductID == 35 || item.ProductID == 36 || item.ProductID == 96 || item.ProductID == 97 || item.ProductID == 84 || item.ProductID == 78)
                                            if (item.IsPrintType == 0)
                                            {
                                                tx = FindByName("TotalQuantity", row) as TextBlock;
                                                tx.Text = "1";
                                                if (item.ProductID != 78)
                                                    isUSBOrCD = true;
                                            }
                                            else if (item.ProductID == 100)
                                            {
                                                tx = FindByName("TotalQuantity", row) as TextBlock;
                                                tx.Text = "1";
                                            }
                                            else
                                            {
                                                tx = FindByName("TotalQuantity", row) as TextBlock;
                                                tx.Text = _objLineItem[_objLineItem.Count - 1].Quantity.ToString();
                                            }

                                            if (item.IsAccessory)
                                            {
                                                Button btnSelectImagetoprint = FindByName("btnSelectImage", row) as Button;
                                                btnSelectImagetoprint.Visibility = System.Windows.Visibility.Collapsed;
                                                accessoryCount++;
                                            }
                                            if (item.ProductID == 95)
                                            {
                                                Button btnSelectImagetoprint = FindByName("btnSelectImage", row) as Button;
                                                btnSelectImagetoprint.Visibility = System.Windows.Visibility.Collapsed;
                                                IsBaseVideo = true;
                                            }

                                            img = FindByName("ProductParent", row) as Image;
                                            img.Visibility = System.Windows.Visibility.Visible;

                                            Button btnchildelete = FindByName("btndelete", row) as Button;
                                            btnchildelete.Visibility = System.Windows.Visibility.Collapsed;
                                            if (item.ProductID == 1091)
                                            {
                                                Button btnSelectImagetoprint = FindByName("btnSelectImage", row) as Button;
                                                Button btnSemiOrder = FindByName("btnAddSemiPrinted", row) as Button;
                                                btnSelectImagetoprint.Visibility = Visibility.Collapsed;
                                                btnSemiOrder.Visibility = Visibility.Visible;
                                            }
                                            if (item.ProductID == 84)
                                            {
                                                isChkSpecOnlinePackage = (new ProductBusiness()).IsChkSpecOnlinePackage(productType.ProductID);
                                                if (isChkSpecOnlinePackage)
                                                {
                                                    Button btnSelectImagetoprint1 = FindByName("btnSelectImage", row) as Button;
                                                    Button btnSemiOrder1 = FindByName("btnAddSemiPrinted", row) as Button;
                                                    btnSelectImagetoprint1.Visibility = Visibility.Collapsed;
                                                    btnSemiOrder1.Visibility = Visibility.Collapsed;
                                                    OnlineUploadrow = row;
                                                }
                                            }

                                        }
                                        index++;
                                    }
                                    //To pre fill the image list
                                    if (ids.Count() == 1 || ids.Count() == accessoryCount + 1 || (ids.Count() == accessoryCount + 2 && IsBaseVideo))
                                    {
                                        int indx = 1;
                                        var items = _objLineItem.Where(o => o.ParentID == _objLineItem.LastOrDefault().ParentID && o.SelectedProductType_ID != 95 && o.IsAccessory == false).ToList();
                                        LineItem firstChild = items[indx];
                                        int MaxQuantity = items[indx].TotalMaxSelectedPhotos;
                                        if (items[indx].SelectedProductType_ID == 4 || items[indx].SelectedProductType_ID == 101)
                                        {
                                            if (RobotImageLoader.PrintImages.Count == MaxQuantity)
                                                isPrefilledList = true;
                                        }
                                        else if (items[indx].SelectedProductType_ID == 105)
                                        {
                                            if (RobotImageLoader.PrintImages.Count == 2)
                                                isPrefilledList = true;
                                        }
                                        else if (RobotImageLoader.PrintImages.Count <= MaxQuantity)
                                        {
                                            isPrefilledList = true;
                                        }

                                        //Spec print product 1091
                                        if (items[indx].SelectedProductType_ID == 1091)
                                        {
                                            //--Start-----Added by Nilesh-----Added condition to check groupImages > MaxQuantity--1st-April-2019
                                            isPrefilledList = true;
                                            if (RobotImageLoader.PrintImages.Count > MaxQuantity)
                                            {
                                                isPrefilledList = false;
                                            }
                                            //--End-----Added by Nilesh-----Added condition to check groupImages > MaxQuantity--1st-April-2019
                                        }
                                        if (items[indx].SelectedProductType_ID == 120 || items[indx].SelectedProductType_ID == 121)//120 productTypeID for 8*18
                                        {
                                            isPrefilledList = true;
                                        }

                                        if (isUSBOrCD == true)
                                        {
                                            if (RobotImageLoader.GroupImages.Count > MaxQuantity)
                                                isPrefilledList = false;
                                        }
                                    }
                                }
                                catch (Exception ex)
                                {
                                }
                                finally
                                {
                                }
                            }
                            else
                            {
                                if (productType.IsAccessory)
                                {
                                    Button btnSelectImagetoprint = FindByName("btnSelectImage", Parentrow) as Button;
                                    btnSelectImagetoprint.Visibility = System.Windows.Visibility.Collapsed;
                                }
                            }
                        }
                    }
                    var packageItem = (new ProductBusiness()).GetProductTypeListById(current.SelectedProductType_ID);
                    CtrlSelectedQrCode.packageInfo.MaxImgQuantity = current.TotalQty;
                    CtrlSelectedQrCode.packageInfo.PackageCode = packageItem == null ? string.Empty : packageItem.DG_Orders_ProductCode;
                    CtrlSelectedQrCode.packageInfo.PackageName = current.SelectedProductType_Text;
                    CtrlSelectedQrCode.packageInfo.PackagePrice = current.UnitPrice;
                }
                if (isPrefilledList)
                {
                    //If Package contains only one product AND RobotImageLoader.PrintImages <= product max images count
                    PreFillImageList(isUSBOrCD);
                }
                else
                {
                    foreach (var image in RobotImageLoader.PrintImages)
                    {
                        image.IsItemSelected = false;
                    }
                }
            }
        }

        DataGridRow OnlineUploadrow;

        private void PreFillImageList(bool isUSBOrCD)
        {
            int indx = 1;
            var items1 = _objLineItem.Where(o => o.ParentID == _objLineItem.LastOrDefault().ParentID && o.IsAccessory == false).ToList();
            var items = items1.Where(o => o.SelectedProductType_ID != 95 && o.IsAccessory == false).ToList();
            LineItem firstChild = items[indx];
            List<String> SelectedImageNumber = new List<string>();
            List<LstMyItems> selectList = new List<LstMyItems>();
            LineItem current = new LineItem();
            PhotoBusiness photoBusiness = new PhotoBusiness();
            if (!isUSBOrCD)
            {
                selectList = RobotImageLoader.PrintImages;
            }
            else
            {
                //For USB and CD pack Group images are used
                selectList = RobotImageLoader.GroupImages;
            }

            /* for Storybook Photos Added by Ajk*/
            if (firstChild.SelectedProductType_ID == 131)
            {
                selectList = RobotImageLoader.GroupImages.Where(s => s.MediaType == 1).ToList();
                foreach (var image in selectList)
                {
                    PhotoInfo _objphoto = photoBusiness.GetPhotoStoryBookDetailsbyPhotoId(image.PhotoId);
                    if (_objphoto.StoryBookID != 0)
                    {

                        RobotImageLoader.GroupImages.Clear();
                        var StoryBookPhotos = (new PhotoBusiness()).GetAllStoryBookPhotoBYSTID(_objphoto.StoryBookID);
                        foreach (var storyitem in StoryBookPhotos)
                        {
                            LstMyItems _mySBitem = new LstMyItems();
                            _mySBitem.MediaType = storyitem.DG_MediaType;
                            _mySBitem.VideoLength = storyitem.DG_VideoLength;
                            _mySBitem.Name = storyitem.DG_Photos_RFID;
                            _mySBitem.StoryBookId = storyitem.StoryBookID;
                            _mySBitem.IsPassKeyVisible = Visibility.Collapsed;
                            _mySBitem.PhotoId = storyitem.DG_Photos_pkey;
                            _mySBitem.FileName = storyitem.DG_Photos_RFID;
                            _mySBitem.IsVosDisplay = storyitem.IsVosDisplay;
                            if (_mySBitem.IsVosDisplay)
                            {
                                _mySBitem.BmpImageUplaoad = new BitmapImage(new Uri(@"/images/flipbook.png", UriKind.Relative));
                            }
                            _mySBitem.CreatedOn = storyitem.DG_Photos_CreatedOn;
                            var date = _mySBitem.CreatedOn.ToString("yyyyMMdd");
                            string FilepathforStoryBook = storyitem.HotFolderPath + "Thumbnails" + "\\" + date + "\\" + storyitem.DG_Photos_FileName;
                            _mySBitem.OnlineQRCode = "";
                            _mySBitem.IsLocked = Visibility.Visible;
                            _mySBitem.BmpImageGroup = new BitmapImage(new Uri(@"/images/view-accept.png", UriKind.Relative));
                            _mySBitem.FilePath = FilepathforStoryBook;
                            _mySBitem.PrintGroup = image.PrintGroup;
                            RobotImageLoader.GroupImages.Add(_mySBitem);
                            image.PhotoId = _mySBitem.PhotoId;

                           
                            SelectedImageNumber.Add(Convert.ToString(image.PhotoId));

                            image.IsItemSelected = true;
                        }
                    }
                    //else if(selectList.Count>0)
                    //{
                    //    MessageBox.Show("Plase select storybook images");
                    //    return;
                    //}

                }
                selectList = RobotImageLoader.GroupImages.Where(s => s.MediaType == 1).ToList();
            }
            /* pre-selected print*/
            else if (firstChild.SelectedProductType_ID == 84)//online upload
            {

                foreach (var image in RobotImageLoader.PrintImages)
                {
                    SelectedImageNumber.Add(image.PhotoId.ToString());
                    if (firstChild.IsWaterMarked)//------------
                    {
                        image.IsItemSelected = true;
                    }
                }
                /*Pre-selected print images*/
                if (!firstChild.IsWaterMarked)
                {
                    foreach (var image in selectList)
                    {
                        if (!SelectedImageNumber.Contains(image.PhotoId.ToString()))
                        {
                            SelectedImageNumber.Add(image.PhotoId.ToString());
                        }
                        image.IsItemSelected = true;
                    }
                }
            }
            else if (firstChild.SelectedProductType_ID == 96 || firstChild.SelectedProductType_ID == 97)
            {
                int packageId = items[0].SelectedProductType_ID;
                selectList = RobotImageLoader.GroupImages.Where(o => o.MediaType != 1).ToList();
                selectList = LoadProcessedVideos(packageId, selectList);

                foreach (var image in selectList)
                {
                    if (image.MediaType != 1)
                    {
                        SelectedImageNumber.Add(image.PhotoId.ToString());
                        image.IsItemSelected = true;
                    }
                }
            }
            else
            {
                selectList = selectList.Where(x => x.MediaType == 1).ToList();
                foreach (var image in selectList.Where(x => x.MediaType == 1))
                {
                    SelectedImageNumber.Add(image.PhotoId.ToString());
                    image.IsItemSelected = true;
                }
            }
            firstChild.SelectedImages = SelectedImageNumber;
            int index = firstChild.ItemIndex;
            DataGridRow row = (DataGridRow)dgOrder.ItemContainerGenerator.ContainerFromIndex(index);
            if (row != null)
            {
                TextBlock tx = FindByName("TotalImageSelected", row) as TextBlock;
                tx.Text = "Total " + firstChild.SelectedImages.Count.ToString() + " Items Selected";
                tx = FindByName("TotalQuantity", row) as TextBlock;
                int Qty = tx.Text.ToInt32();
                ListBox lst = FindByName("lstPrintImage", row) as ListBox;
                lst.ItemsSource = null;
                lst.ItemsSource = selectList;
                UpdatePricing(firstChild);
            }
        }
        private List<LstMyItems> LoadProcessedVideos(int PackageId, List<LstMyItems> lst)
        {
            List<LstMyItems> listMyItems = new List<LstMyItems>();
            ProcessedVideoBusiness objBusiness = new ProcessedVideoBusiness();
            List<ProcessedVideoInfo> objProcessedList = objBusiness.GetProcessedVideosByPackageId(PackageId);
            //Show only Processed Videos
            foreach (var item in lst)
            {
                if (objProcessedList.Where(o => o.VideoId == item.PhotoId).ToList().Count > 0)
                {
                    listMyItems.Add(item);
                }
            }
            return listMyItems;
        }

        #region Quantity
        private void btnQtyUp_Click(object sender, RoutedEventArgs e)
        {
            int index = dgOrder.SelectedIndex;
            DataGridRow row = (DataGridRow)dgOrder.ItemContainerGenerator.ContainerFromIndex(index);
            if (row != null)
            {
                TextBlock tx = FindByName("TotalQuantity", row) as TextBlock;
                int Qty = tx.Text.ToInt32() + 1;
                tx.Text = Qty.ToString();

                LineItem CurrentRecord = (LineItem)dgOrder.CurrentItem;
                CurrentRecord.Quantity = Qty;

                ListBox tx1 = FindByName("lstPrintImage", row) as ListBox;
                int Qty1 = tx1.Items.Count.ToInt32();

                UpdatePricing(CurrentRecord);
            }
        }
        private void btnQtyDown_Click(object sender, RoutedEventArgs e)
        {
            int index = dgOrder.SelectedIndex;
            DataGridRow row = (DataGridRow)dgOrder.ItemContainerGenerator.ContainerFromIndex(index);
            if (row != null)
            {
                TextBlock tx = FindByName("TotalQuantity", row) as TextBlock;
                if (tx.Text.ToInt32() >= 1)
                {
                    int Qty = tx.Text.ToInt32() - 1;
                    if (Qty != 0)
                    {
                        tx.Text = Qty.ToString();
                        LineItem CurrentRecord = (LineItem)dgOrder.CurrentItem;
                        CurrentRecord.Quantity = Qty;
                        ListBox tx1 = FindByName("lstPrintImage", row) as ListBox;
                        int Qty1 = tx1.Items.Count.ToInt32();
                        UpdatePricing(CurrentRecord);
                    }
                    else
                    {
                        tx.Text = "1";
                    }
                }
            }
        }
        #endregion

        /// <summary>
        /// Handles the Click event of the btnSelectImage control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void btnSelectImage_Click(object sender, RoutedEventArgs e)
        {
            Button btn = (Button)sender;

            if (btn != null)
            {
                LineItem current = (LineItem)dgOrder.CurrentItem;

                if (current.SelectedProductType_ID > 0 && current.SelectedProductType_ID != 1091)
                {
                    List<LstMyItems> objLstMyItems = new List<LstMyItems>();
                    LineItem parentLineItem = _objLineItem.Where(l => l.ItemNumber == current.ParentID && l.IsPackage == true).FirstOrDefault();
                    if (current.SelectedProductType_ID == 96 || current.SelectedProductType_ID == 97)
                    {
                        CtrlSelectedProcessedVideoslist.SetParent(this);
                        CtrlSelectedProcessedVideoslist.PreviousImage = current.SelectedImages;
                        CtrlSelectedProcessedVideoslist.ProductTypeID = current.SelectedProductType_ID;
                        CtrlSelectedProcessedVideoslist.PackageId = parentLineItem.SelectedProductType_ID;
                        CtrlSelectedProcessedVideoslist.maxSelectedPhotos = current.TotalMaxSelectedPhotos;

                        CtrlSelectedProcessedVideoslist.IsBundled = current.IsBundled;
                        if (current.ParentID == current.ItemNumber)
                        {
                            CtrlSelectedProcessedVideoslist.IsPackage = false;
                        }
                        else
                        {
                            CtrlSelectedProcessedVideoslist.IsPackage = true;
                        }
                        objLstMyItems = CtrlSelectedProcessedVideoslist.ShowHandlerDialog("Select Processed Video");
                        btnNextsimg.IsDefault = true;
                        CtrlSelectedProcessedVideoslist.btnSubmit.IsDefault = false;
                    }
                    else if (current.SelectedProductType_ID == 79)
                    {
                        CtrlSelectedAlbumlist.SetParentAlbum(this);
                        CtrlSelectedAlbumlist.PrintOrderPageList = current.PrintOrderPageList;
                        CtrlSelectedAlbumlist.ProductTypeID = current.SelectedProductType_ID;
                        CtrlSelectedAlbumlist.maxSelectedPhotos = current.TotalMaxSelectedPhotos;

                        CtrlSelectedAlbumlist.IsBundled = current.IsBundled;
                        if (current.ParentID == current.ItemNumber)
                        {
                            CtrlSelectedAlbumlist.IsPackage = false;
                        }
                        else
                        {
                            CtrlSelectedAlbumlist.IsPackage = true;
                        }
                        btnNextsimg.IsDefault = false;
                        CtrlSelectedAlbumlist.btnSubmit.IsDefault = true;
                        objLstMyItems = CtrlSelectedAlbumlist.ShowAlbumHandlerDialog("SelectImage");
                        btnNextsimg.IsDefault = true;
                        CtrlSelectedAlbumlist.btnSubmit.IsDefault = false;
                        current.PrintOrderPageList = CtrlSelectedAlbumlist.PrintOrderPageList;
                        current.GroupItems = objLstMyItems;
                    }
                    else if (current.SelectedProductType_ID == 84 || current.SelectedProductType_ID == 85)
                    {

                        CtrlSelectedQrCode.SetParentQrCode(this);

                        /*Pre-selected print images*/
                        if (current.SelectedProductType_ID == 84)
                        {
                            if (current.SelectedImages == null)
                            {
                                current.SelectedImages = new List<string>();
                                foreach (var item in RobotImageLoader.PrintImages)
                                {
                                    current.SelectedImages.Add(item.PhotoId.ToString());
                                }
                            }
                        }

                        CtrlSelectedQrCode.PreviousImage = current.SelectedImages;
                        CtrlSelectedQrCode.ProductTypeID = current.SelectedProductType_ID;
                        CtrlSelectedQrCode.maxSelectedPhotos = current.TotalMaxSelectedPhotos;
                        CtrlSelectedQrCode.QRCode = current.UniqueCode;
                        CtrlSelectedQrCode.IsBundled = current.IsBundled;
                        CtrlSelectedQrCode.lstQrCode.Clear();
                        foreach (string qrcode in _objLineItem.Select(o => o.UniqueCode).ToList())
                            CtrlSelectedQrCode.lstQrCode.Add(qrcode);
                        if (current.ParentID == current.ItemNumber)
                        {
                            CtrlSelectedQrCode.IsPackage = false;
                        }
                        else
                        {
                            CtrlSelectedQrCode.IsPackage = true;
                        }

                        CtrlSelectedQrCode._lstSelectedImage = new List<LstMyItems>();
                        int indexReq = current.GroupItems.IndexOf(SpecImageGroupForOnline);
                        if (indexReq < 0 && SpecImageGroupForOnline != null)
                            current.GroupItems.Add(SpecImageGroupForOnline);
                        foreach (LstMyItems items in current.GroupItems)
                        {
                            LstMyItems newitem = new LstMyItems();
                            newitem = items;
                            CtrlSelectedQrCode._lstSelectedImage.Add(newitem);
                        }

                        // *** Code Added by Anis for Magic Shot Kitted on 26th Nov 18
                        if (isChkSpecOnlinePackage)
                        {
                            foreach (LstMyItems item in RobotImageLoader.GroupImages)
                            {
                                LstMyItems newitem = new LstMyItems();
                                newitem = item;
                                CtrlSelectedQrCode._lstSelectedImage.Add(newitem);
                            }
                        }
                        // *** End Here ***//

                        objLstMyItems = CtrlSelectedQrCode.ShowQrCodeHandlerDialog("SelectImage");
                        //Update unique list of qr code.
                        if (CtrlSelectedQrCode.lstQrCode.Where(o => string.Compare(o, current.UniqueCode, true) == 0).Count() > 0)
                            CtrlSelectedQrCode.lstQrCode.RemoveAll(o => string.Compare(o, current.UniqueCode, true) == 0);
                        CtrlSelectedQrCode.lstQrCode.Add(CtrlSelectedQrCode.QRCode);
                        current.UniqueCode = CtrlSelectedQrCode.QRCode;
                        current.CodeType = current.SelectedProductType_ID == 84 ? 401 : (current.SelectedProductType_ID == 85 ? 402 : 0);
                    }
                    //For StoryBook Added By AJK
                    else if (current.SelectedProductType_ID == 131)
                    {

                        CtrlSelectedQrCode.SetParentQrCode(this);

                        /*Pre-selected print images*/
                        if (current.SelectedProductType_ID == 131)
                        {
                            if (current.SelectedImages == null)
                            {
                                current.SelectedImages = new List<string>();
                                foreach (var item in RobotImageLoader.PrintImages)
                                {
                                    current.SelectedImages.Add(item.PhotoId.ToString());
                                }
                            }
                        }

                        CtrlSelectedQrCode.PreviousImage = current.SelectedImages;
                        CtrlSelectedQrCode.ProductTypeID = current.SelectedProductType_ID;
                        CtrlSelectedQrCode.maxSelectedPhotos = current.TotalMaxSelectedPhotos;
                        CtrlSelectedQrCode.QRCode = current.UniqueCode;
                        CtrlSelectedQrCode.IsBundled = current.IsBundled;
                        CtrlSelectedQrCode.lstQrCode.Clear();
                        foreach (string qrcode in _objLineItem.Select(o => o.UniqueCode).ToList())
                            CtrlSelectedQrCode.lstQrCode.Add(qrcode);
                        if (current.ParentID == current.ItemNumber)
                        {
                            CtrlSelectedQrCode.IsPackage = false;
                        }
                        else
                        {
                            CtrlSelectedQrCode.IsPackage = true;
                        }

                        CtrlSelectedQrCode._lstSelectedImage = new List<LstMyItems>();
                        int indexReq = current.GroupItems.IndexOf(SpecImageGroupForOnline);
                        if (indexReq < 0 && SpecImageGroupForOnline != null)
                            current.GroupItems.Add(SpecImageGroupForOnline);
                        foreach (LstMyItems items in RobotImageLoader.GroupImages)
                        {
                            LstMyItems newitem = new LstMyItems();
                            newitem = items;
                            CtrlSelectedQrCode._lstSelectedImage.Add(newitem);
                        }

                        // *** Code Added by Anis for Magic Shot Kitted on 26th Nov 18
                        if (isChkSpecOnlinePackage)
                        {
                            foreach (LstMyItems item in RobotImageLoader.GroupImages)
                            {
                                LstMyItems newitem = new LstMyItems();
                                newitem = item;
                                CtrlSelectedQrCode._lstSelectedImage.Add(newitem);
                            }
                        }
                        // *** End Here ***//

                        objLstMyItems = CtrlSelectedQrCode.ShowQrCodeHandlerDialog("SelectImage");
                        //Update unique list of qr code.
                        if (CtrlSelectedQrCode.lstQrCode.Where(o => string.Compare(o, current.UniqueCode, true) == 0).Count() > 0)
                            CtrlSelectedQrCode.lstQrCode.RemoveAll(o => string.Compare(o, current.UniqueCode, true) == 0);
                        CtrlSelectedQrCode.lstQrCode.Add(CtrlSelectedQrCode.QRCode);
                        current.UniqueCode = CtrlSelectedQrCode.QRCode;
                        current.CodeType = current.SelectedProductType_ID == 131 ? 401 : (current.SelectedProductType_ID == 131 ? 402 : 0);
                    }

                    else if (current.SelectedProductType_ID == 100)
                    {
                        // put check here ajeet
                        if (RobotImageLoader.PrintImages != null && RobotImageLoader.PrintImages.Count <= 0)
                        {
                            MessageBox.Show("Please Select Images while Placing order");
                            return;
                        }

                        CtrlSelectedCalenderList.SetParentAlbum(this);
                        CtrlSelectedCalenderList.PrintOrderPageList = current.PrintOrderPageList;
                        CtrlSelectedCalenderList.ProductTypeID = current.SelectedProductType_ID;
                        CtrlSelectedCalenderList.maxSelectedPhotos = current.TotalMaxSelectedPhotos;

                        CtrlSelectedCalenderList.IsBundled = current.IsBundled;
                        if (current.ParentID == current.ItemNumber)
                        {
                            CtrlSelectedCalenderList.IsPackage = false;
                        }
                        else
                        {
                            CtrlSelectedCalenderList.IsPackage = true;
                        }
                        btnNextsimg.IsDefault = false;
                        CtrlSelectedCalenderList.btnSubmit.IsDefault = true;
                        objLstMyItems = CtrlSelectedCalenderList.ShowAlbumHandlerDialog("SelectImage");

                    }
                    else
                    {
                        CtrlSelectedImageslist.SetParent(this);
                        CtrlSelectedImageslist.PreviousImage = current.SelectedImages;
                        CtrlSelectedImageslist.ProductTypeID = current.SelectedProductType_ID;
                        CtrlSelectedImageslist.maxSelectedPhotos = current.TotalMaxSelectedPhotos;
                        CtrlSelectedImageslist._lstSelectedImage = new List<LstMyItems>();

                        foreach (LstMyItems items in current.GroupItems)
                        {
                            LstMyItems newitem = new LstMyItems();
                            newitem = items;
                            CtrlSelectedImageslist._lstSelectedImage.Add(newitem);
                        }
                        CtrlSelectedImageslist.IsBundled = current.IsBundled;
                        if (current.ParentID == current.ItemNumber)
                        {
                            CtrlSelectedImageslist.IsPackage = false;
                        }
                        else
                        {
                            CtrlSelectedImageslist.IsPackage = true;
                        }
                        btnNextsimg.IsDefault = false;
                        CtrlSelectedImageslist.btnSubmit.IsDefault = true;
                        objLstMyItems = CtrlSelectedImageslist.ShowHandlerDialog("SelectImage");
                        btnNextsimg.IsDefault = true;
                        CtrlSelectedImageslist.btnSubmit.IsDefault = false;
                    }
                    var res = objLstMyItems;

                    if (res != null)
                    {
                        List<String> SelectedImageNumber = new List<string>();

                        foreach (var image in res)
                        {
                            SelectedImageNumber.Add(image.PhotoId.ToString());
                        }

                        current.SelectedImages = SelectedImageNumber;

                        int index = dgOrder.SelectedIndex;
                        DataGridRow row = (DataGridRow)dgOrder.ItemContainerGenerator.ContainerFromIndex(index);
                        if (row != null)
                        {
                            TextBlock tx = FindByName("TotalImageSelected", row) as TextBlock;
                            tx.Text = "Total " + res.Count.ToString() + " Items Selected";

                            tx = FindByName("TotalQuantity", row) as TextBlock;
                            int Qty = tx.Text.ToInt32();

                            ListBox lst = FindByName("lstPrintImage", row) as ListBox;
                            lst.ItemsSource = res;
                            UpdatePricing(current);
                        }
                        //Code Added by Anand - 4/6/2014                       
                        if (current.ItemNumber != current.ParentID)
                        {
                            LineItem parentLineItem1 = _objLineItem.Where(l => l.ItemNumber == current.ParentID).FirstOrDefault();

                            var Children = from result in _objLineItem
                                           where result.ParentID == parentLineItem1.ItemNumber && result.IsPackage == false && result.SelectedImages != null
                                           select result;

                            var productsWithExceedPhotos = Children.Where(p => p.SelectedImages.Count > p.TotalMaxSelectedPhotos).ToList();
                            if (productsWithExceedPhotos.Count > 0)
                            {
                                int parentLineItemQuantity = 0;
                                foreach (LineItem productWithExceedPhoto in productsWithExceedPhotos)
                                {
                                    int CurrentParentLineItem = (productWithExceedPhoto.SelectedImages.Count / productWithExceedPhoto.TotalMaxSelectedPhotos) + (productWithExceedPhoto.SelectedImages.Count % productWithExceedPhoto.TotalMaxSelectedPhotos > 0 ? 1 : 0);
                                    if (CurrentParentLineItem > parentLineItemQuantity)
                                        parentLineItemQuantity = CurrentParentLineItem;
                                }
                                int parentIndex = dgOrder.Items.IndexOf(parentLineItem1);
                                DataGridRow Parentrow = (DataGridRow)dgOrder.ItemContainerGenerator.ContainerFromIndex(parentIndex);
                                if (Parentrow != null)
                                {
                                    Button btnQtyUp = FindByName("btnQtyUp", Parentrow) as Button;
                                    btnQtyUp.IsEnabled = false;

                                    Button btnQtyDown = FindByName("btnQtyDown", Parentrow) as Button;
                                    btnQtyDown.IsEnabled = false;

                                    TextBlock txtQuantity = FindByName("TotalQuantity", Parentrow) as TextBlock;
                                    txtQuantity.Text = parentLineItemQuantity.ToString();
                                    parentLineItem1.Quantity = parentLineItemQuantity;
                                }
                            }
                            else
                            {
                                int parentIndex = dgOrder.Items.IndexOf(parentLineItem1);
                                DataGridRow Parentrow = (DataGridRow)dgOrder.ItemContainerGenerator.ContainerFromIndex(parentIndex);
                                if (Parentrow != null)
                                {
                                    Button btnQtyUp = FindByName("btnQtyUp", Parentrow) as Button;
                                    btnQtyUp.IsEnabled = true;

                                    Button btnQtyDown = FindByName("btnQtyDown", Parentrow) as Button;
                                    btnQtyDown.IsEnabled = true;

                                    //KCB on 30 JUL 2018 to persist order number
                                    //TextBlock txtQuantity = FindByName("TotalQuantity", Parentrow) as TextBlock;
                                    //txtQuantity.Text = "1";
                                    //parentLineItem1.Quantity = 1;
                                    ////End

                                }
                            }
                            UpdatePricing(parentLineItem1);
                        }
                    }

                }
                else if (current.SelectedProductType_ID == 1091)
                {
                    bool isSpecPrintOrderAcrossSites = false;
                    StoreInfo store = new StoreInfo();
                    TaxBusiness taxBusiness = new TaxBusiness();
                    store = taxBusiness.getTaxConfigData();
                    isSpecPrintOrderAcrossSites = store.PlaceSpecOrderAcrossSites == null ? false : Convert.ToBoolean(store.PlaceSpecOrderAcrossSites);
                    if ((Common.LoginUser.IsSemiOrder == true || isSpecPrintOrderAcrossSites) && Common.LoginUser.ListSemiOrderSettingsSubStoreWise.Count > 0)
                    {
                        string printedorder = "";
                        string alreadyplacedstr = "";
                        bool alreadyplaced = false;
                        bool wrongphotoId = true;
                        string _tempOrderQRCode = string.Empty;
                        if (btn != null)
                        {
                            CtrlImportSpecProductImages.SetParent(this);
                            CtrlImportSpecProductImages.txtRFID.Focus();
                            btnNextsimg.IsDefault = false;
                            CtrlImportSpecProductImages.btnSearch.IsDefault = true;
                            CtrlImportSpecProductImages.IsSpecPrintOrderAcrossSites = isSpecPrintOrderAcrossSites;
                            var res = CtrlImportSpecProductImages.ShowSpecImagesDialog("ImportSpecImages");
                            btnNextsimg.IsDefault = true;
                            CtrlImportSpecProductImages.btnSearch.IsDefault = false;

                            if (res != null && res.Count > 0)
                            {
                                if (_objLineItem.Count > 0)
                                {
                                    var item = _objLineItem[0].SelectedProductType_ID.ToInt32();
                                    if (item == 0)
                                    {
                                        _objLineItem.RemoveAt(0);
                                    }
                                }
                                _issemiorder = true;
                                SemiOrderSettings ds = Common.LoginUser.ListSemiOrderSettingsSubStoreWise.Where(t => t.DG_LocationId == res[0].DG_Location_Id).FirstOrDefault();

                                if (ds != null)
                                {
                                    List<string> _objstr = new List<string>();
                                    foreach (IMIX.Model.PhotoInfo photoLst in res)
                                    {
                                        _objstr.Add(photoLst.DG_Photos_pkey.ToString());
                                    }
                                    // Added code by Anis for Magic Shot Kitted
                                    if (current.TotalMaxSelectedPhotos == res.Count)
                                    {
                                        if (current.TotalMaxSelectedPhotos == res.Count)
                                        {
                                            List<LstMyItems> SelectImages = new List<LstMyItems>();
                                            List<LstMyItems> SelectImagesmp4 = new List<LstMyItems>();
                                            foreach (var semiimg in res)
                                            {
                                                ds = Common.LoginUser.ListSemiOrderSettingsSubStoreWise.Where(t => t.DG_LocationId == semiimg.DG_Location_Id && t.DG_SemiOrder_Settings_Pkey == semiimg.SemiOrderProfileId).FirstOrDefault();
                                                int prdId = 0;
                                                if (ds != null)
                                                {
                                                    if (ds.DG_SemiOrder_ProductTypeId.Contains(','))
                                                        prdId = 1;
                                                    else
                                                        prdId = Convert.ToInt32(ds.DG_SemiOrder_ProductTypeId);
                                                }
                                                var productType = (new ProductBusiness()).GetProductType().Where(f => f.DG_Orders_ProductType_pkey == prdId).FirstOrDefault();
                                                OrderDetailInfo ordDetails = (new OrderBusiness()).GetSemiOrderImage(semiimg.DG_Photos_pkey.ToString());

                                                if (semiimg.DG_Photos_pkey == 0)
                                                {
                                                    wrongphotoId = false;
                                                }

                                                if ((new OrderBusiness()).GetSemiOrderImageforValidation(semiimg.DG_Photos_pkey.ToString()))
                                                {
                                                    alreadyplaced = true;
                                                }
                                                if (alreadyplaced)
                                                {
                                                    alreadyplacedstr = semiimg.DG_Photos_RFID + ",";
                                                }
                                                LstMyItems ImageDetail = new LstMyItems();
                                                VideoProcessingClass vpc = new VideoProcessingClass();
                                                if (ordDetails != null)
                                                {

                                                    int photoId = semiimg.DG_Photos_pkey;
                                                    ImageDetail = (from result in RobotImageLoader.robotImages
                                                                   where result.PhotoId == photoId
                                                                   select result).FirstOrDefault();

                                                    if (ImageDetail == null)
                                                    {
                                                        PhotoInfo photoDetails = new PhotoBusiness().GetPhotoDetailsbyPhotoId(photoId);
                                                        ImageDetail = new LstMyItems();
                                                        ImageDetail.OnlineQRCode = photoDetails.OnlineQRCode;
                                                        ImageDetail.Name = photoDetails.DG_Photos_RFID;
                                                        ImageDetail.PhotoId = photoDetails.DG_Photos_pkey;
                                                        ImageDetail.FileName = photoDetails.DG_Photos_RFID;
                                                        ImageDetail.HotFolderPath = photoDetails.HotFolderPath;
                                                        ImageDetail.MediaType = photoDetails.DG_MediaType;
                                                        ImageDetail.VideoLength = photoDetails.DG_VideoLength;
                                                        ImageDetail.CreatedOn = photoDetails.DG_Photos_CreatedOn;
                                                        ImageDetail.OnlineQRCode = photoDetails.OnlineQRCode;
                                                        ImageDetail.IsPassKeyVisible = Visibility.Collapsed;
                                                        string fileName = photoDetails.DG_Photos_FileName.Replace(vpc.SupportedVideoFormats["avi"].ToString(), ".jpg").Replace(vpc.SupportedVideoFormats["mp4"].ToString(), ".jpg").Replace(vpc.SupportedVideoFormats["wmv"].ToString(), ".jpg").Replace(vpc.SupportedVideoFormats["mov"].ToString(), ".jpg").Replace(vpc.SupportedVideoFormats["3gp"].ToString(), ".jpg").Replace(vpc.SupportedVideoFormats["3g2"].ToString(), ".jpg").Replace(vpc.SupportedVideoFormats["m2v"].ToString(), ".jpg").Replace(vpc.SupportedVideoFormats["m4v"].ToString(), ".jpg").Replace(vpc.SupportedVideoFormats["flv"].ToString(), ".jpg").Replace(vpc.SupportedVideoFormats["mpeg"].ToString(), ".jpg").Replace(vpc.SupportedVideoFormats["mkv"].ToString(), ".jpg").Replace(vpc.SupportedVideoFormats["mts"].ToString(), ".jpg"); ;
                                                        ImageDetail.BigThumbnailPath = System.IO.Path.Combine(ImageDetail.BigDBThumnailPath, photoDetails.DG_Photos_CreatedOn.ToString("yyyyMMdd"), fileName);
                                                        ImageDetail.FilePath = System.IO.Path.Combine(ImageDetail.ThumnailPath, photoDetails.DG_Photos_CreatedOn.ToString("yyyyMMdd"), fileName);
                                                        ImageDetail.BmpImageGroup = new BitmapImage(new Uri(@"/images/view-accept.png", UriKind.Relative));
                                                        ImageDetail.PrintGroup = new BitmapImage(new Uri(@"/images/print-group.png", UriKind.Relative));

                                                        RobotImageLoader.GroupImages.Add(ImageDetail);
                                                    }
                                                    if (ImageDetail != null)
                                                    {
                                                        // *** Code Added by Anis for Magic Shot Kitted on 26th Nov 18 *** //
                                                        if (isChkSpecOnlinePackage)
                                                        {
                                                            RobotImageLoader.GroupImages.Add(ImageDetail);
                                                        }
                                                        // *** End Here ***//
                                                        if (!String.IsNullOrEmpty(ImageDetail.OnlineQRCode) && String.IsNullOrEmpty(_tempOrderQRCode))
                                                        {
                                                            _tempOrderQRCode = ImageDetail.OnlineQRCode;
                                                        }
                                                    }
                                                    SelectImages.Add(ImageDetail);
                                                    LstMyItems _objnew = new LstMyItems();
                                                    current.SelectedProductType_ID = ordDetails.DG_Orders_Details_ProductType_pkey.ToInt32();
                                                    if (productType != null)
                                                    {
                                                        int index1 = dgOrder.SelectedIndex;
                                                        DataGridRow Parentrow = (DataGridRow)dgOrder.ItemContainerGenerator.ContainerFromIndex(index1);
                                                        if (Parentrow != null)
                                                        {
                                                            TextBlock tx = FindByName("txtProductType", Parentrow) as TextBlock;
                                                            tx.Text = current.SelectedProductType_Text;
                                                            Image img = FindByName("ProductImage", Parentrow) as Image;
                                                            img.Source = new BitmapImage(new Uri(current.SelectedProductType_Image.ToString(), UriKind.Relative));
                                                            ListBox lstimage = FindByName("lstPrintImage", Parentrow) as ListBox;
                                                            current.SelectedImages = _objstr;
                                                            current.IssemiOrder = true;
                                                            lstimage.ItemsSource = null;
                                                            lstimage.ItemsSource = SelectImages;
                                                            TextBlock txParentrow = FindByName("TotalImageSelected", Parentrow) as TextBlock;
                                                            if (tx.Text == "Spec Print") // Added by Anis
                                                            {
                                                                txParentrow.Text = "Total " + SelectImages.Count + " Images Selected";
                                                            }
                                                            else
                                                            {
                                                                txParentrow.Text = "Total " + current.SelectedImages.Count + " Images Selected";
                                                            }
                                                            Button btndelete = FindByName("btndelete", Parentrow) as Button;
                                                            btndelete.Tag = "-1";
                                                            Button btnSelectImage = FindByName("btnSelectImage", Parentrow) as Button;
                                                            btnSelectImage.Visibility = System.Windows.Visibility.Collapsed;
                                                            Button btnSelectImagesemi = FindByName("btnAddSemiPrinted", Parentrow) as Button;
                                                            btnSelectImagesemi.Visibility = System.Windows.Visibility.Collapsed;
                                                            if (OnlineUploadrow != null)
                                                            {
                                                                Button btnOnlineSelectImage = FindByName("btnSelectImage", OnlineUploadrow) as Button;
                                                                btnOnlineSelectImage.Visibility = System.Windows.Visibility.Visible;
                                                            }
                                                            // *** Added by Anis for Magic Shot kitted requirement *** //
                                                            List<String> SelectedImageNumber = new List<string>();

                                                            foreach (var image in SelectImages)
                                                            {
                                                                SelectedImageNumber.Add(image.PhotoId.ToString());
                                                            }
                                                            current.SelectedImages = SelectedImageNumber;

                                                            tx = FindByName("TotalQuantity", Parentrow) as TextBlock;
                                                            int Qty = tx.Text.ToInt32();

                                                            UpdatePricing(current);
                                                        }
                                                        if (current.ItemNumber != current.ParentID)
                                                        {
                                                            LineItem parentLineItem1 = _objLineItem.Where(l => l.ItemNumber == current.ParentID).FirstOrDefault();

                                                            var Children = from result in _objLineItem
                                                                           where result.ParentID == parentLineItem1.ItemNumber && result.IsPackage == false && result.SelectedImages != null
                                                                           select result;

                                                            var productsWithExceedPhotos = Children.Where(p => p.SelectedImages.Count > p.TotalMaxSelectedPhotos).ToList();
                                                            if (productsWithExceedPhotos.Count > 0)
                                                            {
                                                                int parentLineItemQuantity = 0;
                                                                foreach (LineItem productWithExceedPhoto in productsWithExceedPhotos)
                                                                {
                                                                    int CurrentParentLineItem = (productWithExceedPhoto.SelectedImages.Count / productWithExceedPhoto.TotalMaxSelectedPhotos) + (productWithExceedPhoto.SelectedImages.Count % productWithExceedPhoto.TotalMaxSelectedPhotos > 0 ? 1 : 0);
                                                                    if (CurrentParentLineItem > parentLineItemQuantity)
                                                                        parentLineItemQuantity = CurrentParentLineItem;
                                                                }
                                                                int parentIndex = dgOrder.Items.IndexOf(parentLineItem1);
                                                                DataGridRow Parentrow1 = (DataGridRow)dgOrder.ItemContainerGenerator.ContainerFromIndex(parentIndex);
                                                                if (Parentrow1 != null)
                                                                {
                                                                    Button btnQtyUp = FindByName("btnQtyUp", Parentrow1) as Button;
                                                                    btnQtyUp.IsEnabled = false;

                                                                    Button btnQtyDown = FindByName("btnQtyDown", Parentrow1) as Button;
                                                                    btnQtyDown.IsEnabled = false;

                                                                    TextBlock txtQuantity = FindByName("TotalQuantity", Parentrow1) as TextBlock;
                                                                    txtQuantity.Text = parentLineItemQuantity.ToString();
                                                                    parentLineItem1.Quantity = parentLineItemQuantity;
                                                                }
                                                            }
                                                            else
                                                            {
                                                                int parentIndex = dgOrder.Items.IndexOf(parentLineItem1);
                                                                DataGridRow Parentrow2 = (DataGridRow)dgOrder.ItemContainerGenerator.ContainerFromIndex(parentIndex);
                                                                if (Parentrow2 != null)
                                                                {
                                                                    Button btnQtyUp = FindByName("btnQtyUp", Parentrow2) as Button;
                                                                    btnQtyUp.IsEnabled = true;

                                                                    Button btnQtyDown = FindByName("btnQtyDown", Parentrow2) as Button;
                                                                    btnQtyDown.IsEnabled = true;

                                                                    TextBlock txtQuantity = FindByName("TotalQuantity", Parentrow2) as TextBlock;
                                                                    txtQuantity.Text = "1";
                                                                    parentLineItem1.Quantity = 1;
                                                                }
                                                            }
                                                            UpdatePricing(parentLineItem1);
                                                            // *** End Here *** //


                                                        }
                                                        //UpdatePricing(current);

                                                    }
                                                    if (isChkSpecOnlinePackage)
                                                    {
                                                        if (!String.IsNullOrEmpty(_tempOrderQRCode) && string.IsNullOrEmpty(ImageDetail.OnlineQRCode))
                                                        {
                                                            ImageDetail.OnlineQRCode = _tempOrderQRCode;
                                                        }
                                                        if (ImageDetail != null && !string.IsNullOrEmpty(ImageDetail.OnlineQRCode))
                                                        {
                                                            SpecImageGroupForOnline = null;
                                                            PreFillOnlieSpec(SelectImages, ImageDetail.OnlineQRCode);
                                                            CtrlSelectedQrCode.txtQRCode.IsReadOnly = true;
                                                            SpecImageGroupForOnline = ImageDetail;
                                                            CtrlSelectedQrCode.QRCode = _tempOrderQRCode;//////changed by latika for spec print QRCode is not coming
                                                        }
                                                        else
                                                        {
                                                            CtrlSelectedQrCode.txtQRCode.IsReadOnly = false;
                                                        }
                                                    }
                                                }
                                                // *** Code Added by Anis for Magic Shot Kitted on 26th Nov 18 *** //
                                                else if (semiimg.DG_Photos_FileName.EndsWith(".mp4") && isChkSpecOnlinePackage)
                                                {
                                                    LstMyItems itemVideo = new LstMyItems();
                                                    itemVideo.MediaType = semiimg.DG_MediaType;
                                                    itemVideo.FileName = semiimg.DG_Photos_FileName;
                                                    itemVideo.PhotoId = semiimg.DG_Photos_pkey;
                                                    itemVideo.PhotoLocation = semiimg.DG_Location_Id;
                                                    itemVideo.SemiOrderProfileId = semiimg.SemiOrderProfileId;
                                                    itemVideo.CreatedOn = semiimg.DG_Photos_CreatedOn;
                                                    itemVideo.FilePath = semiimg.DG_Photos_FilePath;
                                                    SelectImagesmp4.Add(itemVideo);
                                                    PreFillOnlieAndSpecForMagicShot(SelectImagesmp4, ImageDetail.OnlineQRCode);

                                                    // Adding items into group
                                                    LstMyItems newItems = new LstMyItems();
                                                    newItems.MediaType = semiimg.DG_MediaType;
                                                    newItems.FileName = semiimg.DG_Photos_FileName;
                                                    newItems.FilePath = semiimg.DG_Photos_FilePath;
                                                    newItems.Name = System.IO.Path.GetFileNameWithoutExtension(newItems.FilePath);
                                                    newItems.PhotoId = semiimg.DG_Photos_pkey;
                                                    newItems.BmpImageGroup = new BitmapImage(new Uri(@"/images/view-accept.png", UriKind.Relative));
                                                    newItems.PrintGroup = new BitmapImage(new Uri(@"/images/print-group.png", UriKind.Relative));
                                                    RobotImageLoader.GroupImages.Add(newItems);
                                                }
                                                // *** End Here *** //
                                                else
                                                {
                                                    printedorder = semiimg.DG_Photos_RFID + ",";
                                                }
                                                if (!String.IsNullOrEmpty(_tempOrderQRCode))
                                                {
                                                    if (ordDetails != null)
                                                    {
                                                        ordDetails.DG_Order_ImageUniqueIdentifier = _tempOrderQRCode;
                                                        foreach (LstMyItems ordit in SelectImages)
                                                        {
                                                            ordit.OnlineQRCode = _tempOrderQRCode;
                                                        }
                                                    }
                                                    foreach (LstMyItems item in RobotImageLoader.GroupImages)
                                                    {
                                                        item.OnlineQRCode = _tempOrderQRCode;
                                                    }
                                                    foreach (var item in _objLineItem)
                                                    {
                                                        if (item.SelectedProductType_ID == 84)
                                                            item.UniqueCode = _tempOrderQRCode;
                                                    }
                                                }
                                            }
                                        }
                                        else
                                        {
                                            if (System.Windows.MessageBox.Show("You have selected more than " + current.TotalMaxSelectedPhotos + " images, do you want to continue ?", "Digiphoto", MessageBoxButton.YesNo, MessageBoxImage.Warning) == MessageBoxResult.Yes)
                                            {
                                                List<LstMyItems> SelectImages = new List<LstMyItems>();
                                                List<LstMyItems> SelectImagesmp4 = new List<LstMyItems>();
                                                foreach (var semiimg in res)
                                                {
                                                    ds = Common.LoginUser.ListSemiOrderSettingsSubStoreWise.Where(t => t.DG_LocationId == semiimg.DG_Location_Id && t.DG_SemiOrder_Settings_Pkey == semiimg.SemiOrderProfileId).FirstOrDefault();
                                                    int prdId = 0;
                                                    if (ds.DG_SemiOrder_ProductTypeId.Contains(','))
                                                        prdId = 1;
                                                    else
                                                        prdId = Convert.ToInt32(ds.DG_SemiOrder_ProductTypeId);

                                                    //var productType = objGetSemiOrderSetting.GetProductType().Where(f => f.DG_Orders_ProductType_pkey == ds.DG_SemiOrder_ProductTypeId).FirstOrDefault();
                                                    var productType = (new ProductBusiness()).GetProductType().Where(f => f.DG_Orders_ProductType_pkey == prdId).FirstOrDefault();
                                                    //DG_Orders_Details ORD_Details = objGetSemiOrderSetting.GetSemiOrderImage(semiimg.DG_Photos_pkey.ToString());
                                                    OrderDetailInfo ordDetails = (new OrderBusiness()).GetSemiOrderImage(semiimg.DG_Photos_pkey.ToString());

                                                    if (semiimg.DG_Photos_pkey == 0)
                                                    {
                                                        wrongphotoId = false;
                                                    }

                                                    //if (objGetSemiOrderSetting.GetSemiOrderImageforValidation(semiimg.DG_Photos_pkey.ToString()))
                                                    if ((new OrderBusiness()).GetSemiOrderImageforValidation(semiimg.DG_Photos_pkey.ToString()))
                                                    {
                                                        alreadyplaced = true;
                                                    }
                                                    if (alreadyplaced)
                                                    {
                                                        alreadyplacedstr = semiimg.DG_Photos_RFID + ",";
                                                    }
                                                    LstMyItems ImageDetail = new LstMyItems();
                                                    VideoProcessingClass vpc = new VideoProcessingClass();
                                                    if (ordDetails != null)
                                                    {

                                                        int photoId = semiimg.DG_Photos_pkey;// objGetSemiOrderSetting.GetPhotoIDByRFFID(semiimg);
                                                        ImageDetail = (from result in RobotImageLoader.robotImages
                                                                       where result.PhotoId == photoId
                                                                       select result).FirstOrDefault();



                                                        if (ImageDetail == null)
                                                        {
                                                            PhotoInfo photoDetails = new PhotoBusiness().GetPhotoDetailsbyPhotoId(photoId);
                                                            ImageDetail = new LstMyItems();
                                                            ImageDetail.OnlineQRCode = photoDetails.OnlineQRCode;
                                                            ImageDetail.Name = photoDetails.DG_Photos_RFID;
                                                            ImageDetail.PhotoId = photoDetails.DG_Photos_pkey;
                                                            ImageDetail.FileName = photoDetails.DG_Photos_RFID;
                                                            ImageDetail.HotFolderPath = photoDetails.HotFolderPath;
                                                            ImageDetail.MediaType = photoDetails.DG_MediaType;
                                                            ImageDetail.VideoLength = photoDetails.DG_VideoLength;
                                                            ImageDetail.CreatedOn = photoDetails.DG_Photos_CreatedOn;
                                                            ImageDetail.OnlineQRCode = photoDetails.OnlineQRCode;
                                                            ImageDetail.IsPassKeyVisible = Visibility.Collapsed;
                                                            // RobotImageLoader.GroupImages.Add()
                                                            string fileName = photoDetails.DG_Photos_FileName.Replace(vpc.SupportedVideoFormats["avi"].ToString(), ".jpg").Replace(vpc.SupportedVideoFormats["mp4"].ToString(), ".jpg").Replace(vpc.SupportedVideoFormats["wmv"].ToString(), ".jpg").Replace(vpc.SupportedVideoFormats["mov"].ToString(), ".jpg").Replace(vpc.SupportedVideoFormats["3gp"].ToString(), ".jpg").Replace(vpc.SupportedVideoFormats["3g2"].ToString(), ".jpg").Replace(vpc.SupportedVideoFormats["m2v"].ToString(), ".jpg").Replace(vpc.SupportedVideoFormats["m4v"].ToString(), ".jpg").Replace(vpc.SupportedVideoFormats["flv"].ToString(), ".jpg").Replace(vpc.SupportedVideoFormats["mpeg"].ToString(), ".jpg").Replace(vpc.SupportedVideoFormats["mkv"].ToString(), ".jpg").Replace(vpc.SupportedVideoFormats["mts"].ToString(), ".jpg"); ;
                                                            ImageDetail.BigThumbnailPath = System.IO.Path.Combine(ImageDetail.BigDBThumnailPath, photoDetails.DG_Photos_CreatedOn.ToString("yyyyMMdd"), fileName);
                                                            ImageDetail.FilePath = System.IO.Path.Combine(ImageDetail.ThumnailPath, photoDetails.DG_Photos_CreatedOn.ToString("yyyyMMdd"), fileName);
                                                            ImageDetail.BmpImageGroup = new BitmapImage(new Uri(@"/images/view-accept.png", UriKind.Relative));
                                                            ImageDetail.PrintGroup = new BitmapImage(new Uri(@"/images/print-group.png", UriKind.Relative));

                                                            RobotImageLoader.GroupImages.Add(ImageDetail);
                                                        }
                                                        if (ImageDetail != null)
                                                        {
                                                            // *** Code Added by Anis for Magic Shot Kitted on 26th Nov 18 *** //
                                                            if (isChkSpecOnlinePackage)
                                                            {
                                                                RobotImageLoader.GroupImages.Add(ImageDetail);
                                                            }
                                                            // *** End Here ***//
                                                            if (!String.IsNullOrEmpty(ImageDetail.OnlineQRCode) && String.IsNullOrEmpty(_tempOrderQRCode))
                                                            {
                                                                _tempOrderQRCode = ImageDetail.OnlineQRCode;
                                                            }
                                                        }
                                                        SelectImages.Add(ImageDetail);

                                                        LstMyItems _objnew = new LstMyItems();
                                                        var selectedImg = 0;
                                                        current.SelectedProductType_ID = ordDetails.DG_Orders_Details_ProductType_pkey.ToInt32();
                                                        if (productType != null)
                                                        {
                                                            int index1 = dgOrder.SelectedIndex;
                                                            DataGridRow Parentrow = (DataGridRow)dgOrder.ItemContainerGenerator.ContainerFromIndex(index1);
                                                            if (Parentrow != null)
                                                            {
                                                                TextBlock tx = FindByName("txtProductType", Parentrow) as TextBlock;
                                                                tx.Text = current.SelectedProductType_Text;
                                                                Image img = FindByName("ProductImage", Parentrow) as Image;
                                                                img.Source = new BitmapImage(new Uri(current.SelectedProductType_Image.ToString(), UriKind.Relative));
                                                                ListBox lstimage = FindByName("lstPrintImage", Parentrow) as ListBox;
                                                                current.SelectedImages = _objstr;
                                                                current.IssemiOrder = true;
                                                                //current.UniqueCode = ImageDetail.OnlineQRCode;
                                                                //current.CodeType = 401;
                                                                lstimage.ItemsSource = null;
                                                                lstimage.ItemsSource = SelectImages;
                                                                TextBlock txParentrow = FindByName("TotalImageSelected", Parentrow) as TextBlock;

                                                                if (tx.Text == "Spec Print") // Added by Anis
                                                                {
                                                                    txParentrow.Text = "Total " + SelectImages.Count + " Images Selected";
                                                                    selectedImg = SelectImages.Count;
                                                                }
                                                                else
                                                                {
                                                                    txParentrow.Text = "Total " + current.SelectedImages.Count + " Images Selected";
                                                                }
                                                                Button btndelete = FindByName("btndelete", Parentrow) as Button;
                                                                btndelete.Tag = "-1";
                                                                Button btnSelectImage = FindByName("btnSelectImage", Parentrow) as Button;
                                                                btnSelectImage.Visibility = System.Windows.Visibility.Collapsed;
                                                                Button btnSelectImagesemi = FindByName("btnAddSemiPrinted", Parentrow) as Button;
                                                                btnSelectImagesemi.Visibility = System.Windows.Visibility.Collapsed;

                                                                // *** Added by Anis for Magic Shot kitted requirement *** //
                                                                List<String> SelectedImageNumber = new List<string>();

                                                                foreach (var image in SelectImages)
                                                                {
                                                                    SelectedImageNumber.Add(image.PhotoId.ToString());
                                                                }
                                                                current.SelectedImages = SelectedImageNumber;

                                                                tx = FindByName("TotalQuantity", Parentrow) as TextBlock;
                                                                int Qty = tx.Text.ToInt32();

                                                                UpdatePricing(current);
                                                            }

                                                            if (current.ItemNumber != current.ParentID)
                                                            {
                                                                LineItem parentLineItem1 = _objLineItem.Where(l => l.ItemNumber == current.ParentID).FirstOrDefault();

                                                                var Children = from result in _objLineItem
                                                                               where result.ParentID == parentLineItem1.ItemNumber && result.IsPackage == false && result.SelectedImages != null
                                                                               select result;

                                                                var productsWithExceedPhotos = Children.Where(p => p.SelectedImages.Count > p.TotalMaxSelectedPhotos).ToList();
                                                                if (productsWithExceedPhotos.Count > 0)
                                                                {
                                                                    int parentLineItemQuantity = 0;
                                                                    foreach (LineItem productWithExceedPhoto in productsWithExceedPhotos)
                                                                    {
                                                                        int CurrentParentLineItem = (productWithExceedPhoto.SelectedImages.Count / productWithExceedPhoto.TotalMaxSelectedPhotos) + (productWithExceedPhoto.SelectedImages.Count % productWithExceedPhoto.TotalMaxSelectedPhotos > 0 ? 1 : 0);
                                                                        if (CurrentParentLineItem > parentLineItemQuantity)
                                                                            parentLineItemQuantity = CurrentParentLineItem;
                                                                    }
                                                                    int parentIndex = dgOrder.Items.IndexOf(parentLineItem1);
                                                                    DataGridRow Parentrow1 = (DataGridRow)dgOrder.ItemContainerGenerator.ContainerFromIndex(parentIndex);
                                                                    if (Parentrow1 != null)
                                                                    {
                                                                        Button btnQtyUp = FindByName("btnQtyUp", Parentrow1) as Button;
                                                                        btnQtyUp.IsEnabled = false;

                                                                        Button btnQtyDown = FindByName("btnQtyDown", Parentrow1) as Button;
                                                                        btnQtyDown.IsEnabled = false;

                                                                        TextBlock txtQuantity = FindByName("TotalQuantity", Parentrow1) as TextBlock;
                                                                        txtQuantity.Text = parentLineItemQuantity.ToString();
                                                                        parentLineItem1.Quantity = parentLineItemQuantity;
                                                                    }
                                                                }
                                                                else
                                                                {
                                                                    int parentIndex = dgOrder.Items.IndexOf(parentLineItem1);
                                                                    DataGridRow Parentrow2 = (DataGridRow)dgOrder.ItemContainerGenerator.ContainerFromIndex(parentIndex);
                                                                    if (Parentrow2 != null)
                                                                    {
                                                                        Button btnQtyUp = FindByName("btnQtyUp", Parentrow2) as Button;
                                                                        btnQtyUp.IsEnabled = true;

                                                                        Button btnQtyDown = FindByName("btnQtyDown", Parentrow2) as Button;
                                                                        btnQtyDown.IsEnabled = true;

                                                                        TextBlock txtQuantity = FindByName("TotalQuantity", Parentrow2) as TextBlock;
                                                                        txtQuantity.Text = "1";
                                                                        parentLineItem1.Quantity = 1;
                                                                    }
                                                                }
                                                                UpdatePricing(parentLineItem1);
                                                                // *** End Here *** //
                                                            }
                                                            //UpdatePricing(current); // Commented by Anis
                                                        }
                                                        if (isChkSpecOnlinePackage)
                                                        {
                                                            if (!String.IsNullOrEmpty(_tempOrderQRCode) && string.IsNullOrEmpty(ImageDetail.OnlineQRCode))
                                                            {
                                                                ImageDetail.OnlineQRCode = _tempOrderQRCode;
                                                            }
                                                            if (ImageDetail != null && !string.IsNullOrEmpty(ImageDetail.OnlineQRCode))
                                                                PreFillOnlieSpec(SelectImages, ImageDetail.OnlineQRCode);
                                                        }
                                                    }
                                                    // *** Code Added by Anis for Magic Shot Kitted on 26th Nov 18 *** //
                                                    else if (semiimg.DG_Photos_FileName.EndsWith(".mp4") && isChkSpecOnlinePackage)
                                                    {
                                                        LstMyItems itemVideo = new LstMyItems();
                                                        itemVideo.MediaType = semiimg.DG_MediaType;
                                                        itemVideo.FileName = semiimg.DG_Photos_FileName;
                                                        itemVideo.PhotoId = semiimg.DG_Photos_pkey;
                                                        itemVideo.PhotoLocation = semiimg.DG_Location_Id;
                                                        itemVideo.SemiOrderProfileId = semiimg.SemiOrderProfileId;
                                                        itemVideo.CreatedOn = semiimg.DG_Photos_CreatedOn;
                                                        itemVideo.FilePath = semiimg.DG_Photos_FilePath;
                                                        SelectImagesmp4.Add(itemVideo);
                                                        PreFillOnlieAndSpecForMagicShot(SelectImagesmp4, ImageDetail.OnlineQRCode);

                                                        // Adding items into group
                                                        LstMyItems newItems = new LstMyItems();
                                                        newItems.MediaType = semiimg.DG_MediaType;
                                                        newItems.FileName = semiimg.DG_Photos_FileName;
                                                        newItems.FilePath = semiimg.DG_Photos_FilePath;
                                                        newItems.Name = System.IO.Path.GetFileNameWithoutExtension(newItems.FilePath);
                                                        newItems.PhotoId = semiimg.DG_Photos_pkey;
                                                        newItems.BmpImageGroup = new BitmapImage(new Uri(@"/images/view-accept.png", UriKind.Relative));
                                                        newItems.PrintGroup = new BitmapImage(new Uri(@"/images/print-group.png", UriKind.Relative));
                                                        RobotImageLoader.GroupImages.Add(newItems);
                                                    }
                                                    // *** End Here *** //
                                                    else
                                                    {
                                                        printedorder = semiimg.DG_Photos_RFID + ",";
                                                    }
                                                    if (!String.IsNullOrEmpty(_tempOrderQRCode))
                                                    {
                                                        if (ordDetails != null)
                                                        {
                                                            ordDetails.DG_Order_ImageUniqueIdentifier = _tempOrderQRCode;
                                                            foreach (LstMyItems ordit in SelectImages)
                                                            {
                                                                ordit.OnlineQRCode = _tempOrderQRCode;
                                                            }
                                                        }
                                                        foreach (LstMyItems item in RobotImageLoader.GroupImages)
                                                        {
                                                            item.OnlineQRCode = _tempOrderQRCode;
                                                        }
                                                        //_objLineItem
                                                        foreach (var item in _objLineItem)
                                                        {
                                                            if (item.SelectedProductType_ID == 84)
                                                                item.UniqueCode = _tempOrderQRCode;
                                                        }

                                                    }

                                                }
                                            }
                                        }
                                    }
                                    else
                                    {
                                        MessageBox.Show("Please confirm the quantity from Package");
                                    }
                                }
                            }
                            else
                            {
                                MessageBox.Show("Sorry No Images found.", "DigiPhoto");
                            }

                            if (!wrongphotoId)
                            {
                                MessageBox.Show("Image not found.Please check the image number", "DigiPhoto");
                                return;
                            }
                            if (!string.IsNullOrEmpty(alreadyplacedstr))
                            {
                                MessageBox.Show("You have already placed an order for the images (" + printedorder.Trim(',') + ")");
                                return;
                            }
                            if (!string.IsNullOrEmpty(printedorder))
                            {
                                MessageBox.Show("Image is not edited/printed yet!");
                            }
                        }
                    }
                    else
                    {
                        MessageBox.Show("Spec print is not active on this substore", "DigiPhoto");
                    }
                }
                else
                {
                    MessageBox.Show("Please Select Product Type first.", "DigiPhoto");
                }
            }
        }

        LstMyItems SpecImageGroupForOnline;

        private void PreFillOnlieSpec(List<LstMyItems> selectList, string OnlineQRCode)
        {
            int indx = 1;
            for (int i = 0; i < dgOrder.Items.Count; i++)
            {
                if (((LineItem)dgOrder.Items[i]).SelectedProductType_ID == 84)
                {
                    indx = i;
                    break;
                }
            }

            var items1 = _objLineItem.Where(o => o.ParentID == _objLineItem.LastOrDefault().ParentID && o.IsAccessory == false).ToList();
            _objLineItem.Where(o => o.SelectedProductType_ID == 84).FirstOrDefault().UniqueCode = OnlineQRCode;
            _objLineItem.Where(o => o.SelectedProductType_ID == 84).FirstOrDefault().CodeType = 401;
            var items = items1.Where(o => o.SelectedProductType_ID == 84).ToList();
            LineItem firstChild = items1[indx];
            List<String> SelectedImageNumber = new List<string>();
            if (firstChild.SelectedProductType_ID == 84)
            {
                foreach (var image in selectList)
                {
                    SelectedImageNumber.Add(image.PhotoId.ToString());
                    image.IsItemSelected = true;
                }
            }
            firstChild.SelectedImages = SelectedImageNumber;
            int index = firstChild.ItemIndex;
            DataGridRow row = (DataGridRow)dgOrder.ItemContainerGenerator.ContainerFromIndex(index);
            if (row != null)
            {
                TextBlock tx = FindByName("TotalImageSelected", row) as TextBlock;
                tx.Text = "Total " + firstChild.SelectedImages.Count.ToString() + " Items Selected";
                tx = FindByName("TotalQuantity", row) as TextBlock;
                int Qty = tx.Text.ToInt32();
                ListBox lst = FindByName("lstPrintImage", row) as ListBox;
                lst.ItemsSource = null;
                lst.ItemsSource = selectList;
                UpdatePricing(firstChild);
            }
        }
        private void BindMyListItems(LineItem current, List<LstMyItems> res)
        {
            if (res != null)
            {
                List<String> SelectedImageNumber = new List<string>();

                foreach (var image in res)
                {
                    SelectedImageNumber.Add(image.PhotoId.ToString());
                }

                current.SelectedImages = SelectedImageNumber;

                int index = dgOrder.SelectedIndex;
                DataGridRow row = (DataGridRow)dgOrder.ItemContainerGenerator.ContainerFromIndex(index);
                if (row != null)
                {
                    TextBlock tx = FindByName("TotalImageSelected", row) as TextBlock;
                    tx.Text = "Total " + res.Count.ToString() + " Images Selected";

                    tx = FindByName("TotalQuantity", row) as TextBlock;
                    int Qty = tx.Text.ToInt32();

                    ListBox lst = FindByName("lstPrintImage", row) as ListBox;
                    lst.ItemsSource = res;

                    var calItem = _objLineItem.Where(o => o.IsAccessory == false && o.IsPackage == false && o.ItemIndex == index).FirstOrDefault();
                    calItem.SelectedImages = SelectedImageNumber;
                    UpdatePricing(current);
                }
                //Code Added by Anand - 4/6/2014                       
                if (current.ItemNumber != current.ParentID)
                {
                    LineItem parentLineItem = _objLineItem.Where(l => l.ItemNumber == current.ParentID).FirstOrDefault();

                    var Children = from result in _objLineItem
                                   where result.ParentID == parentLineItem.ItemNumber && result.IsPackage == false && result.SelectedImages != null
                                   select result;

                    var productsWithExceedPhotos = Children.Where(p => p.SelectedImages.Count > p.TotalMaxSelectedPhotos).ToList();
                    if (productsWithExceedPhotos.Count > 0)
                    {
                        int parentLineItemQuantity = 0;
                        foreach (LineItem productWithExceedPhoto in productsWithExceedPhotos)
                        {
                            int CurrentParentLineItem = (productWithExceedPhoto.SelectedImages.Count / productWithExceedPhoto.TotalMaxSelectedPhotos) + (productWithExceedPhoto.SelectedImages.Count % productWithExceedPhoto.TotalMaxSelectedPhotos > 0 ? 1 : 0);
                            if (CurrentParentLineItem > parentLineItemQuantity)
                                parentLineItemQuantity = CurrentParentLineItem;
                        }
                        int parentIndex = dgOrder.Items.IndexOf(parentLineItem);
                        DataGridRow Parentrow = (DataGridRow)dgOrder.ItemContainerGenerator.ContainerFromIndex(parentIndex);
                        if (Parentrow != null)
                        {
                            Button btnQtyUp = FindByName("btnQtyUp", Parentrow) as Button;
                            btnQtyUp.IsEnabled = false;

                            Button btnQtyDown = FindByName("btnQtyDown", Parentrow) as Button;
                            btnQtyDown.IsEnabled = false;

                            TextBlock txtQuantity = FindByName("TotalQuantity", Parentrow) as TextBlock;
                            txtQuantity.Text = parentLineItemQuantity.ToString();
                            parentLineItem.Quantity = parentLineItemQuantity;
                        }
                    }
                    else
                    {
                        int parentIndex = dgOrder.Items.IndexOf(parentLineItem);
                        DataGridRow Parentrow = (DataGridRow)dgOrder.ItemContainerGenerator.ContainerFromIndex(parentIndex);
                        if (Parentrow != null)
                        {
                            Button btnQtyUp = FindByName("btnQtyUp", Parentrow) as Button;
                            btnQtyUp.IsEnabled = true;

                            Button btnQtyDown = FindByName("btnQtyDown", Parentrow) as Button;
                            btnQtyDown.IsEnabled = true;

                            TextBlock txtQuantity = FindByName("TotalQuantity", Parentrow) as TextBlock;
                            txtQuantity.Text = "1";
                            parentLineItem.Quantity = 1;
                        }
                    }
                    UpdatePricing(parentLineItem);
                }
            }
        }

        // *** Code Added by Anis for Magic Shot Kitted on 23rd Nov 18
        private void PreFillOnlieAndSpecForMagicShot(List<LstMyItems> selectList, string OnlineQRCode)
        {
            int indx = 0;
            try
            {
                var items1 = _objLineItem.Where(o => o.ParentID == _objLineItem.LastOrDefault().ParentID && o.IsAccessory == false).ToList();
                //_objLineItem.Where(o => o.SelectedProductType_ID == 84 && o.UniqueCode == null).FirstOrDefault().UniqueCode = OnlineQRCode;
                _objLineItem.Where(o => o.SelectedProductType_ID == 84).FirstOrDefault().UniqueCode = OnlineQRCode;
                //_objLineItem.Where(o => o.SelectedProductType_ID == 84 && o.CodeType == 0).FirstOrDefault().CodeType = 401;
                _objLineItem.Where(o => o.SelectedProductType_ID == 84).FirstOrDefault().CodeType = 401;
                var items = items1.Where(o => o.SelectedProductType_ID == 84).ToList();
                if (items1 != null && items1.Count > 1)
                {
                    indx = 1;
                }
                LineItem firstChild = items1[indx];
                List<String> SelectedImageNumber = new List<string>();
                if (firstChild.SelectedProductType_ID == 84)//online upload
                {
                    foreach (var image in selectList)//RobotImageLoader.PrintImages)
                    {
                        SelectedImageNumber.Add(image.PhotoId.ToString());
                        image.IsItemSelected = true;
                    }
                }
                firstChild.SelectedImages = SelectedImageNumber;
                int index = firstChild.ItemIndex;
                DataGridRow row = (DataGridRow)dgOrder.ItemContainerGenerator.ContainerFromIndex(index);
                if (row != null)
                {
                    TextBlock tx = FindByName("TotalImageSelected", row) as TextBlock;
                    tx.Text = "Total " + firstChild.SelectedImages.Count.ToString() + " Items Selected";
                    tx = FindByName("TotalQuantity", row) as TextBlock;
                    int Qty = tx.Text.ToInt32();
                    ListBox lst = FindByName("lstPrintImage", row) as ListBox;
                    lst.ItemsSource = null;
                    lst.ItemsSource = selectList;//RobotImageLoader.PrintImages;
                    UpdatePricing(firstChild);

                    if (isChkSpecOnlinePackage)
                    {
                        Button btnSelectImagetoprint1 = FindByName("btnSelectImage", row) as Button;
                        Button btnSemiOrder1 = FindByName("btnAddSemiPrinted", row) as Button;
                        btnSelectImagetoprint1.Visibility = Visibility.Visible;
                        //btnSemiOrder1.Visibility = Visibility.Collapsed;
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
            }
        }
        // *** End Here **//

        /// <summary>
        /// Handles the Click event of the btnAddSemiPrinted control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void btnAddSemiPrinted_Click(object sender, RoutedEventArgs e)
        {
            Button btn = (Button)sender;
            string printedOrder = string.Empty;

            if (btn != null)
            {
                if (Common.LoginUser.IsSemiOrder == true && Common.LoginUser.ListSemiOrderSettingsSubStoreWise.Count > 0)
                {
                    CtrlImportSpecProductImages.SetParent(this);
                    CtrlImportSpecProductImages.txtRFID.Focus();
                    var res = CtrlImportSpecProductImages.ShowSpecImagesDialog("ImportSpecImages");

                    if (res != null && res.Count > 0)
                    {
                        if (_objLineItem.Count > 0)
                        {
                            var item = _objLineItem[0].SelectedProductType_ID.ToInt32();
                            if (item == 0)
                            {
                                _objLineItem.RemoveAt(0);
                            }
                        }
                        _issemiorder = true;
                        SemiOrderSettings ds = Common.LoginUser.ListSemiOrderSettingsSubStoreWise.Where(t => t.DG_LocationId == res[0].DG_Location_Id).FirstOrDefault();

                        if (ds != null)
                        {
                            foreach (var semiimg in res)
                            {
                                ds = Common.LoginUser.ListSemiOrderSettingsSubStoreWise.Where(t => t.DG_LocationId == semiimg.DG_Location_Id && t.DG_SemiOrder_Settings_Pkey == semiimg.SemiOrderProfileId).FirstOrDefault();
                                int prdId = 0;
                                if (ds.DG_SemiOrder_ProductTypeId.Contains(','))
                                    prdId = 1;
                                else
                                    prdId = Convert.ToInt32(ds.DG_SemiOrder_ProductTypeId);
                                var productType = (new ProductBusiness()).GetProductType().Where(f => f.DG_Orders_ProductType_pkey == prdId).FirstOrDefault();
                                OrderDetailInfo ordDetails = (new OrderBusiness()).GetSemiOrderImage(semiimg.DG_Photos_pkey.ToString());

                                if (ordDetails != null)
                                {
                                    int PhotoID = semiimg.DG_Photos_pkey;
                                    LstMyItems ImageDetail = (from result in RobotImageLoader.robotImages
                                                              where result.PhotoId == PhotoID
                                                              select result).FirstOrDefault();

                                    List<LstMyItems> SelectImages = new List<LstMyItems>();
                                    SelectImages.Add(ImageDetail);

                                    LineItem current = new LineItem();
                                    string itemNumber = System.Guid.NewGuid().ToString();

                                    current.ItemNumber = itemNumber;
                                    current.ParentID = itemNumber;
                                    LstMyItems _objnew = new LstMyItems();
                                    _objnew.FilePath = semiimg.HotFolderPath + "\\" + semiimg.DG_Photos_FileName;
                                    _objnew.Name = System.IO.Path.GetFileNameWithoutExtension(_objnew.FilePath);
                                    _objnew.PhotoId = PhotoID;
                                    current.GroupItems = new List<LstMyItems>();
                                    current.GroupItems.Add(_objnew);
                                    current.OrderDetailsID = ordDetails.DG_Orders_LineItems_pkey;
                                    current.SelectedProductType_ID = productType.DG_Orders_ProductType_pkey;
                                    current.SelectedProductType_Image = productType.DG_Orders_ProductType_Image.ToString();
                                    current.SelectedProductType_Text = productType.DG_Orders_ProductType_Name.ToString();
                                    current.SelectedProductCode = productType.DG_Orders_ProductType_ProductCode.ToString(); // Added by Anisur Rahman for Dubai frame (Product code + Barcode) receipt requirement
                                    current.IsBundled = productType.DG_Orders_ProductType_IsBundled != null ? productType.DG_Orders_ProductType_IsBundled.ToBoolean() : false;
                                    current.AllowDiscount = productType.DG_Orders_ProductType_DiscountApplied != null ? productType.DG_Orders_ProductType_DiscountApplied.ToBoolean() : false;
                                    current.TotalMaxSelectedPhotos = productType.DG_MaxQuantity;
                                    current.Quantity = 1;
                                    current.SelectedImages = new List<String>() { PhotoID.ToString() };
                                    current.TotalMaxSelectedPhotos = 1;
                                    current.IssemiOrder = true;
                                    current.ItemIndex = (_objLineItem.Count - 1) + 1;
                                    _objLineItem.Add(current);
                                    dgOrder.UpdateLayout();

                                    if (productType != null)
                                    {
                                        DataGridRow Parentrow = (DataGridRow)dgOrder.ItemContainerGenerator.ContainerFromIndex(_objLineItem.Count - 1);

                                        if (Parentrow != null)
                                        {
                                            TextBlock tx = FindByName("txtProductType", Parentrow) as TextBlock;
                                            tx.Text = current.SelectedProductType_Text;

                                            Image img = FindByName("ProductImage", Parentrow) as Image;
                                            img.Source = new BitmapImage(new Uri(current.SelectedProductType_Image.ToString(), UriKind.Relative));

                                            ListBox lstimage = FindByName("lstPrintImage", Parentrow) as ListBox;
                                            lstimage.ItemsSource = SelectImages;

                                            TextBlock txParentrow = FindByName("TotalImageSelected", Parentrow) as TextBlock;
                                            txParentrow.Text = "Total " + current.SelectedImages.Count + " Images Selected";

                                            Button btndelete = FindByName("btndelete", Parentrow) as Button;
                                            btndelete.Tag = "-1";

                                            Button btnSelectImage = FindByName("btnSelectImage", Parentrow) as Button;
                                            btnSelectImage.Visibility = System.Windows.Visibility.Collapsed;
                                        }
                                        UpdatePricing(current);
                                    }
                                }
                                else
                                {
                                    printedOrder = semiimg.DG_Photos_RFID + ",";
                                }
                            }
                        }
                    }
                    else
                    {
                        MessageBox.Show("Sorry No Images found.", "DigiPhoto");
                    }

                    if (!string.IsNullOrEmpty(printedOrder))
                    {
                        MessageBox.Show("You have already placed an order for the images (" + printedOrder.Trim(',') + ")");
                    }
                }
            }
        }


        #region Order
        private bool CheckOnlineUploadQRCode()
        {
            var result = true;
            if (_objLineItem != null && _objLineItem.Count > 0)
            {
                var productType = _objLineItem.Where(o => o.SelectedProductType_ID.ToString() == "84").FirstOrDefault();
                if (productType != null && string.IsNullOrEmpty(CtrlSelectedQrCode.QRCode))
                {
                    /// result = false;
                    /// ///changed by latika 
                    if (productType != null && string.IsNullOrEmpty(productType.UniqueCode))
                    {
                        result = false;
                    }
                    ///end
                }
                // Commented by Anis // Added for Magic shot kitted + Online
                //if (isChkSpecOnlinePackage)
                //{
                //    result = true;
                //}
                //if (isChkSpecOnlinePackage && string.IsNullOrEmpty(CtrlSelectedQrCode.QRCode))
                //{
                //    result = false;
                //}
                /// ///changed by latika for Qrcode issue 2019-09-28
                if (isChkSpecOnlinePackage && string.IsNullOrEmpty(CtrlSelectedQrCode.QRCode) && _objLineItem[0].Quantity > 0)
                {
                    result = false;
                }

                //Added by VINS_13Mar2020 for Unique(4*6)+QR product as per Pete, no need to input QR code manually for this product
                //var pid = from i in _objLineItem
                //          where i.SelectedProductType_ID == 124
                //          select i.SelectedProductType_ID;
                //if (pid != null)
                //{
                //    result = true;
                //}

            }
            return result;
        }
        private bool CheckISARPersonalisedValid()
        {
            bool result = true;

            if (_objLineItem != null && _objLineItem.Count > 0)
            {
                var productTypelist = _objLineItem.Where(o => Convert.ToBoolean(o.IsPackage) == true);
                foreach (var itemsinGrplst in productTypelist)
                {
                    var productType = _objLineItem.Where(o => o.SelectedProductType_ID.ToString() == Convert.ToString(itemsinGrplst.SelectedProductType_ID)).FirstOrDefault(); ;
                    PackageBusniess packObj = new PackageBusniess();
                    bool isARPersonalised = packObj.GETIsPersonalizedAR(productType.SelectedProductType_ID);
                    int images = 0;
                    int Video = 0;
                    // (RobotImageLoader.GroupImages.Count > 0) { }
                    if (isARPersonalised)
                    {
                        result = false;
                        //if (productType.GroupItems.Count == 0)
                        //{
                        foreach (var itemsinGrp in RobotImageLoader.GroupImages)
                        {
                            if (itemsinGrp.IsItemSelected == true)
                            {
                                string fileName = itemsinGrp.FileName;
                                if (fileName.ToLower().Contains(".mp")) { Video = 1; }
                                if (fileName.ToLower().Contains(".jpg")) { images = 1; }
                                else if (fileName.ToLower().Contains(".jpeg")) { images = 1; }
                                else if (fileName.ToLower().Contains(".png")) { images = 1; }
                            }


                        }
                        //}
                        //else
                        //{
                        foreach (var itemsinGrp in productType.GroupItems)
                        {
                            if (itemsinGrp.IsItemSelected == true)
                            {
                                string fileName = itemsinGrp.FileName;
                                if (fileName.ToLower().Contains(".mp")) { Video += 1; }
                                if (fileName.ToLower().Contains(".jpg")) { images += 1; }
                                else if (fileName.ToLower().Contains(".png")) { images += 1; }
                            }


                        }
                        // }
                        if (images > 0 && Video > 0) { result = true; }
                        else { result = false; }
                    }


                }
            }
            return result;
        }
        private int GetDefaultCurrencyID()
        {
            var currency = (from objcurrency in (new CurrencyBusiness()).GetCurrencyList()
                            where objcurrency.DG_Currency_Default == true
                            select objcurrency.DG_Currency_pkey).FirstOrDefault();
            return currency;
        }


        private string GenerateOrderNumber()
        {
            string orderNumber = Common.LoginUser.OrderPrefix + "-";
            string uniqueNumber = string.Empty;
            try
            {
                using (RNGCryptoServiceProvider rng = new RNGCryptoServiceProvider())
                {
                    // Buffer storage.
                    byte[] data = new byte[4];

                    // Ten iterations.
                    {
                        // Fill buffer.
                        rng.GetBytes(data);

                        int value = BitConverter.ToInt32(data, 0);
                        if (value < 0)
                            value = -value;
                        if (value.ToString().Length > 10)
                        {
                            uniqueNumber = value.ToString().Substring(0, 10);
                        }
                        else
                        {
                            uniqueNumber = value.ToString();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
            orderNumber = orderNumber + uniqueNumber;
            return orderNumber;
        }
        protected void SaveOrder()
        {
            try
            {
                double _TotalAmount = 0;
                foreach (LineItem item in _objLineItem)
                {
                    _TotalAmount += ((item.Quantity * item.SelectedImages.Count) * item.UnitPrice);
                }

                string OrderNumber = GenerateOrderNumber();
                string SyncCode = CommonUtility.GetUniqueSynccode(Convert.ToInt32(ApplicationObjectEnum.Camera).ToString().PadLeft(2, '0'), LoginUser.countrycode, LoginUser.Storecode, "14");
                OrderInfo orderinfo = (new OrderBusiness()).GenerateOrder(OrderNumber, (decimal)_TotalAmount, (decimal)_TotalAmount, String.Empty, 0, 0, string.Empty, Common.LoginUser.UserId, GetDefaultCurrencyID(), "3", SyncCode, LoginUser.Storecode, "NA");//BY KCB ON 20 JUN 2020 to store user' employee id
                //OrderInfo orderinfo = (new OrderBusiness()).GenerateOrder(OrderNumber, (decimal)_TotalAmount, (decimal)_TotalAmount, String.Empty, 0, 0, string.Empty, Common.LoginUser.UserId, GetDefaultCurrencyID(), "3", SyncCode, LoginUser.Storecode,"NA"); // 3 for pos off
                OrderNumber = orderinfo.DG_Orders_Number;
                int OrderID = orderinfo.DG_Orders_pkey;
                TaxBusiness buss = new TaxBusiness();
                buss.SaveOrderTaxDetails(LoginUser.StoreId, OrderID, LoginUser.SubStoreId);
                if (OrderID > 0)
                {
                    StringBuilder ItemDetail = new StringBuilder();
                    string images = string.Empty;
                    int MaxGap = 0;

                    foreach (LineItem item in _objLineItem)
                    {
                        if (MaxGap < item.SelectedProductType_Text.Length)
                        {
                            MaxGap = item.SelectedProductType_Text.Length;
                        }
                    }

                    foreach (LineItem item in _objLineItem)
                    {
                        string OrderDetailsSyncCode = CommonUtility.GetUniqueSynccode(Convert.ToInt32(ApplicationObjectEnum.OrderDetails).ToString().PadLeft(2, '0'), LoginUser.countrycode, LoginUser.Storecode, LoginUser.SubStoreId.ToString());

                        foreach (var img in item.SelectedImages)
                        {
                            String Space = string.Empty;
                            int counter = MaxGap - item.SelectedProductType_Text.Length;
                            counter = counter + 45;

                            for (int i = 1; i <= counter; i++)
                            {
                                Space += " ";
                            }

                            ItemDetail.AppendLine(item.SelectedProductType_Text + Space + item.UnitPrice.ToString(".00"));
                            if (images == string.Empty)
                            {
                                images = img;
                            }
                            else
                            {
                                images += "," + img;
                            }
                        }

                        int id = (new OrderBusiness()).SaveOrderLineItems(item.SelectedProductType_ID, OrderID, images, item.Quantity, item.TotalDiscount, (decimal)item.TotalDiscountAmount, (decimal)item.UnitPrice, (decimal)item.TotalPrice, (decimal)item.NetPrice, -1, LoginUser.SubStoreId, item.CodeType, item.UniqueCode, OrderDetailsSyncCode, item.EvoucherCode, null);

                        //Call method to save album print order incase of productType=79
                        if (item.SelectedProductType_ID == 79 && id > 0 && !string.IsNullOrEmpty(images) && item.GroupItems.Count() > 0)
                        {
                            (new PrinterBusniess()).SaveAlbumPrintPosition(id, item.PrintPhotoOrderPosition);
                        }

                        images = string.Empty;

                        //if (!item.IsAccessory && item.SelectedProductType_ID != 84) //item acessory and Package does not Insert into Printqueue

                        {
                            if (!_issemiorder)
                            {
                                AddToPrintQueue(item, id, item.PrintPhotoOrderPosition);
                            }
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
            finally
            {
                this.Close();
            }
        }

        protected void PrintSlip(string ordernumber, string ItemDetails, double TotalPercentage, double Discount, double NetPrice)
        {
            TextBlock tb = new TextBlock();
            StringBuilder sb = new StringBuilder();
            sb.Append("                 DIGIPHOTO STUDIO        " + System.Environment.NewLine);
            sb.Append("              Welcome to " + Common.LoginUser.StoreName.ToString() + "  " + System.Environment.NewLine);
            sb.Append(System.Environment.NewLine);
            sb.Append("------------------------------------------------" + System.Environment.NewLine);
            sb.Append("Operator: " + Common.LoginUser.UserName + System.Environment.NewLine);
            sb.Append("Order No: " + ordernumber + System.Environment.NewLine);
            sb.Append("------------------------------------------------" + System.Environment.NewLine);
            sb.Append(System.Environment.NewLine);
            sb.Append(ItemDetails);

            if (Discount != 0)
            {
                int Occupied = (TotalPercentage.ToString(".00") + " %  Dis ").Length;
                String Space = string.Empty;
                int MaxGap = 0;

                foreach (LineItem item in _objLineItem)
                {
                    if (MaxGap < item.SelectedProductType_Text.Length)
                    {
                        MaxGap = item.SelectedProductType_Text.Length;
                    }
                }

                int counter = MaxGap - Occupied;
                counter += 50;
                for (int i = 0; i < counter; i++)
                {
                    Space += " ";
                }
                tb.Inlines.Add(sb.ToString());
                sb.Clear();
                tb.Inlines.Add(new Run() { Text = TotalPercentage.ToString(".00") + " %  Dis " + Space + Discount.ToString(), FontWeight = FontWeights.Bold });
                sb.Append(System.Environment.NewLine);
            }

            sb.Append(System.Environment.NewLine);
            sb.Append("SUBTOTAL                                    " + NetPrice.ToString(".00") + System.Environment.NewLine);
            sb.Append("TAX                                               0.00" + System.Environment.NewLine);
            sb.Append("AMOUNT DUE                              " + "0.00" + System.Environment.NewLine);
            sb.Append(System.Environment.NewLine);
            sb.Append(System.Environment.NewLine);
            sb.Append("CASH                                            0.00" + System.Environment.NewLine);
            sb.AppendLine("CHANGE                                      0.00 " + System.Environment.NewLine);

            tb.Inlines.Add(sb.ToString());
            sb.Clear();
            tb.Inlines.Add(new Run() { Text = "             THANK  you.         ", FontWeight = FontWeights.Bold });

            sb.Append(System.Environment.NewLine);

            sb.Append("        Your Cashier Was: 1310" + System.Environment.NewLine);
            sb.Append("        Your Terminal Was: 1310" + System.Environment.NewLine + System.Environment.NewLine);
            tb.Inlines.Add(sb.ToString());
            sb.Clear();

            tb.Inlines.Add(new Run() { Text = "           NO REFUNDS", FontWeight = FontWeights.Bold });
            sb.Append(System.Environment.NewLine);
            sb.Append("           GUEST COPY" + System.Environment.NewLine);
            sb.Append("------------------------------------------------" + System.Environment.NewLine);
            sb.Append("  " + (new CustomBusineses()).ServerDateTime().ToLongDateString() + " " + (new CustomBusineses()).ServerDateTime().ToLongTimeString() + "  " + System.Environment.NewLine);

            sb.Append("------------------------------------------------" + System.Environment.NewLine);
            tb.Inlines.Add(sb.ToString());
            PrintDialog diag = new PrintDialog();

            tb.Margin = new Thickness(50);
            tb.TextWrapping = TextWrapping.Wrap;
            tb.LayoutTransform = new ScaleTransform(1, 1);
            Size pageSize = new Size(diag.PrintableAreaWidth, diag.PrintableAreaHeight);
            tb.Measure(pageSize);
            tb.Arrange(new Rect(0, 0, pageSize.Width, pageSize.Height));
            diag.PrintVisual(tb, "DigiPhoto");
        }
        protected void AddToPrintQueue(LineItem liItem, int OrderDetailID, List<PhotoPrintPositionDic> PhotoPrintPositionDicList)
        {
            try
            {
                (new PrinterBusniess()).AddImageToPrinterQueue(liItem.SelectedProductType_ID, liItem.SelectedImages, OrderDetailID, liItem.IsBundled, false, PhotoPrintPositionDicList);
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }

        private void AddPrintedImagesOnOff()
        {
            int enablePOS = ((new ConfigBusiness()).GetConfigurationData()).Where(x => x.PosOnOff == true).Count();
            if (enablePOS > 0)
                btnAddSemiPrinted.Visibility = Visibility.Visible;
            else
                btnAddSemiPrinted.Visibility = Visibility.Collapsed;
        }
        #endregion
        public int id { get; set; }
    }

    public class LineItem
    {
        public int CodeType { get; set; }
        public string UniqueCode { get; set; }
        public int ItemSeqNumber { get; set; }
        public int OrderDetailsID { get; set; }
        public string ItemNumber { get; set; }
        public int Quantity { get; set; }
        public int TotalQty { get; set; }
        public int TotalMaxSelectedPhotos { get; set; }
        public int SelectedProductType_ID { get; set; }
        public String SelectedProductType_Text { get; set; }
        public String SelectedProductType_Image { get; set; }
        public int SelectedPrinterID { get; set; }
        public String ParentID { get; set; }
        public String SelectedPrinterText { get; set; }
        public bool AllowDiscount { get; set; }
        public bool IsBundled { get; set; }
        public bool CommandVisible { get; set; }
        public bool IsPackage { get; set; }
        public bool IsAccessory { get; set; }
        public bool IsWaterMarked { get; set; }
        public int? IsPrintType { get; set; }
        public double UnitPrice { get; set; }
        public double TotalPrice { get; set; }
        public string TotalDiscount { get; set; }
        public double TotalDiscountAmount { get; set; }
        public double NetPrice { get; set; }
        public bool IssemiOrder { get; set; }

        public Int32 ItemTemplateHeaderId { get; set; }
        public Int32 ItemTemplateDetailId { get; set; }

        /// <summary>
        /// //added by latika for Evoucher 2019Dec21
        /// </summary>
        public string EvoucherCode { get; set; }
        public int EvoucherPer { get; set; }

        public bool? IsEvoucherDisc { get; set; }////added by latika for Evoucher { get; set; }
                                                 /////ended by latika


        public int ItemIndex { get; set; }
        public List<String> SelectedImages
        {
            get;
            set;
        }
        public List<PrinterDetailsInfo> AssociatedPrinters
        {
            get;
            set;
        }
        public List<LstMyItems> GroupItems
        {
            get;
            set;
        }

        public Visibility SemiVisible { get; set; }

        public List<PhotoPrintPositionDic> PrintPhotoOrderPosition = new List<PhotoPrintPositionDic>();

        public ObservableCollection<PrintOrderPage> PrintOrderPageList = new ObservableCollection<PrintOrderPage>();
        public string HotFolderPath { get; set; }
        public double? TaxPercent { get; set; }
        public double TaxAmount { get; set; }
        public bool? IsTaxIncluded { get; set; }
        public bool? IsPersonalizedAR { get; set; }///changed by latika  for AR Personalised.
        public String SelectedProductCode { get; set; } // Added by Anisur Rahman for Dubai frame (Product code + Barcode) receipt requirement
    }

    public class BoolToVisibilityConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value.GetType() != typeof(bool)) return Visibility.Hidden;
            return (bool)value ? Visibility.Visible : Visibility.Hidden;
        }
        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return null; // not needed
        }
    }

    public class Printer
    {
        public string PrinterName { get; set; }
        public int PrinterID { get; set; }
    }
    class BurnImagesForCD
    {
        private int _ImageID;
        public int ImageID
        {
            get { return _ImageID; }
            set { _ImageID = value; }
        }
        private int producttype;
        public int Producttype
        {
            get { return producttype; }
            set { producttype = value; }
        }
    }
}
