﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
//using DigiPhoto.DataLayer;
//using DigiPhoto.DataLayer.Model;
using DigiPhoto.Common;
using DigiPhoto.Manage;
using DigiAuditLogger;
using CrystalDecisions.CrystalReports.Engine;
using DigiPhoto.IMIX.Model;
using DigiPhoto.IMIX.Business;
using System.IO;
using CrystalDecisions.Shared;
using FrameworkHelper;
using DigiPhoto.Utility.Repository.ValueType;
using DigiPhoto.DataLayer;
using System.Configuration;
using System.Resources;
using DigiPhoto.Culture;

namespace DigiPhoto.Orders
{
    /// <summary>
    /// Interaction logic for OrderRefund.xaml
    /// </summary>
    public partial class OrderRefund : Window
    {
        ReportDocument report = new ReportDocument();
        #region Declaration
        private List<PhotosDetails> _objPhotoList;
        private List<OrderDetails> _objOrderList;
        private List<CheckedItems> _objSelectedItems;
        private decimal? discountOnTotal = 0;

        private int _lineitemId;
        private int _orderId;
        #endregion
        TextBox controlon;

        bool isEnableSlipPrint = true;
        public string DefaultCurrency
        {
            get;
            set;
        }
        public double _NetAmount
        {
            get;
            set;
        }
        public double _TotalAmount
        {
            get;
            set;
        }
        public double _TotalDiscount
        {
            get;
            set;
        }
        public double _RefundAmount
        {
            get;
            set;
        }



        /// <summary>
        /// The already refunded
        /// </summary>
        private List<AlreadyrefundInfo> _alreadyRefunded;

        #region Constructor
        public OrderRefund()
        {
            InitializeComponent();
            GetDefaultCurrency();
            RobotImageLoader.GetConfigData();
            clear();
            txbUserName.Text = LoginUser.UserName;
            txbStoreName.Text = LoginUser.StoreName;
            dgOrderList.Visibility = Visibility.Collapsed;

            List<iMIXConfigurationInfo> _objdata = (new ConfigBusiness()).GetNewConfigValues(LoginUser.SubStoreId);
            foreach (iMIXConfigurationInfo imixConfigValue in _objdata)
            {
                switch (imixConfigValue.IMIXConfigurationMasterId)
                {
                    case (int)ConfigParams.IsEnableSlipPrint:
                        isEnableSlipPrint = string.IsNullOrWhiteSpace(imixConfigValue.ConfigurationValue) ? true : Convert.ToBoolean(imixConfigValue.ConfigurationValue);
                        break;
                }
            }

        }

        private void GetDefaultCurrency()
        {

            //DigiPhotoDataServices _objDataLayer = new DigiPhotoDataServices();
            var currency = (from objcurrency in (new CurrencyBusiness()).GetCurrencyList()
                            where objcurrency.DG_Currency_Default == true
                            select objcurrency.DG_Currency_Symbol.ToString()).FirstOrDefault();
            DefaultCurrency = currency;

        }
        #endregion Constructor

        #region Events
        /// <summary>
        /// Handles the Click event of the btnRefundOrder control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        /// 
        private void btnSubmitRefund(object sender, RoutedEventArgs e)
        {
            GrdPrint.Visibility = Visibility.Hidden;
            string reason = string.Empty;
            ValueTypeInfo reasonType = (ValueTypeInfo)CmbReasonType.SelectedItem;
            if (reasonType.Name == "Others")
            {
                if (string.IsNullOrWhiteSpace(txtreason.Text.Trim()))
                {
                    MessageBox.Show("Please enter the comment.");
                    return;
                }
                // reason = txtreason.Text;
                reason = reasonType.Name + "-" + txtreason.Text.Trim();
            }
            else
            {
                if (string.IsNullOrWhiteSpace(txtreason.Text.Trim()))
                {
                    reason = reasonType.Name;
                }
                else
                    reason = reasonType.Name + "-" + txtreason.Text.Trim();
                // reason = CmbReasonType.SelectedItem.ToString();
                if (string.IsNullOrWhiteSpace(reason))
                {
                    MessageBox.Show("Please select reason for refund.");
                    return;
                }

                if (reasonType.Name == "--Select--")
                {
                    MessageBox.Show("Please select reason for refund.");
                    return;
                }
            }
            try
            {
                if (dgOrderList.Items.Count > 0)
                {


                    //foreach(var orderitem in _objOrderList.Where(t=>t.DG_LineItemId==lineitemId))
                    //{
                    //    txtTotalRefund.Text = (Convert.ToDecimal(_RefundAmount) - orderitem.DG_Refund_Amount).ToString();
                    //    _RefundAmount = Convert.ToDouble(decimal.Round(Convert.ToDecimal(_RefundAmount), 2, MidpointRounding.AwayFromZero));
                    //    txtTotalRefund.Text = (decimal.Round(Convert.ToDecimal(_RefundAmount), 2, MidpointRounding.AwayFromZero)).ToString();
                    //    orderitem.DG_Refund_Amount = 0;
                    //    orderitem.DG_Refund_Quantity = 0;
                    //}
                    GrdPrinting.Visibility = Visibility.Collapsed;
                    RefundBusiness refBiz = new RefundBusiness();
                    //DigiPhotoDataServices _objServices = new DigiPhotoDataServices();
                    int refundMasterId = refBiz.SetRefundMasterData(_orderId, _RefundAmount.ToDecimal(), (new CustomBusineses()).ServerDateTime(), LoginUser.UserId);
                    foreach (var item in _objSelectedItems)
                    {
                        refBiz.SetRefundDetailsData(item.LineItemId, refundMasterId, item.SelectetdItems.ToString(), item.RefundPrice, reason);
                    }
                    // AuditLog.AddUserLog(Common.LoginUser.UserId, 67, "Reason For Refund -" + reason + " and Created on " + (new DigiPhotoDataServices()).ServerDateTime().ToString());
                    AuditLog.AddUserLog(Common.LoginUser.UserId, (int)FrameworkHelper.ActionType.RefundOrder, "Reason For Refund :" + reason + "Refund Order " + txtSearchOrderNO.Text + " and Total Refunded Amount is " + (new CurrencyBusiness()).GetDefaultCurrencyName() + " " + _RefundAmount.ToDecimal().ToString());
                    //AuditLog.AddUserLog(Common.LoginUser.UserId, 40, "Reason For Refund :" + reason + " and Created on " + (new DigiPhotoDataServices()).ServerDateTime().ToString() + "" + "Refund Order " + txtSearchOrderNO.Text + " and Total Refunded Amount is " + _objServices.GetDefaultCurrencyName() + " " + Convert.ToDecimal(_RefundAmount).ToString());
                    MessageBox.Show("Refund successfully.");
                    dgOrderList.ItemsSource = null;
                    dgOrderList.Visibility = Visibility.Collapsed;
                    txtreason.Clear();
                    lstItems.ItemsSource = null;
                    clear();
                    _objSelectedItems = new List<CheckedItems>();
                    //kapil
                    if (isEnableSlipPrint)
                    {
                        refundReceipt(Common.LoginUser.SubstoreName.ToString(), Common.LoginUser.UserName.ToString(), reason, txtSearchOrderNO.Text);
                    }

                }
                else
                {
                    MessageBox.Show("No item for refund.");
                }


            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }

        public void refundReceipt(string storeName, string operatorName, string refundReassonReason, string searchOrderNo)
        {
            try
            {
                StoreInfo store = new StoreInfo();
                TaxBusiness taxBusiness = new TaxBusiness();
                store = taxBusiness.getTaxConfigData();
                InitializeComponent();
                string ApplicationPath = AppDomain.CurrentDomain.BaseDirectory;
                report.Load(ApplicationPath + "\\Reports\\RefundOrder.rpt");

                //This is for ALL reports
                TextObject storename = (TextObject)report.ReportDefinition.Sections["Section1"].ReportObjects["Txtstorename"];
                storename.Text = storeName;

                TextObject op = (TextObject)report.ReportDefinition.Sections["Section3"].ReportObjects["txtOperator"];
                op.Text = operatorName;

                TextObject reason = (TextObject)report.ReportDefinition.Sections["DetailSection1"].ReportObjects["txtRefundReason"];
                reason.Text = refundReassonReason;

                TextObject OrderNos = (TextObject)report.ReportDefinition.Sections["DetailSection1"].ReportObjects["txtOrdersNo"];
                OrderNos.Text = searchOrderNo;

                TextObject txtAddress = (TextObject)report.ReportDefinition.Sections["Section1"].ReportObjects["txtAddress"];
                txtAddress.Text = "" + store.Address.Replace("\n", " ").Replace("\r", "");

                TextObject txtTtile = (TextObject)report.ReportDefinition.Sections["Section1"].ReportObjects["txtReceiptTitle"];
                txtTtile.Text = store.BillReceiptTitle;

                TextObject txtPhone = (TextObject)report.ReportDefinition.Sections["Section1"].ReportObjects["txtPhone"];
                txtPhone.Text = "Phone: " + store.PhoneNo;

                TextObject txtEmail = (TextObject)report.ReportDefinition.Sections["Section1"].ReportObjects["txtEmail"];
                txtEmail.Text = "Email: " + store.EmailID;

                TextObject txtUrl = (TextObject)report.ReportDefinition.Sections["Section1"].ReportObjects["txtUrl"];
                txtUrl.Text = store.WebsiteURL;

                System.Data.DataTable dt_123 = new System.Data.DataTable();
                dt_123.Columns.Add("StoreName", typeof(string));
                dt_123.Columns.Add("Operator", typeof(string));
                dt_123.Columns.Add("Reason", typeof(string));
                dt_123.Columns.Add("OrderNos", typeof(string));
                dt_123.Rows.Add(storename, operatorName, refundReassonReason);
                report.SetDataSource(dt_123);
                report.Refresh();
                report.PrintOptions.PrinterName = Common.LoginUser.ReceiptPrinterPath;
                //report.PrintOptions.PrinterName = "HP LaserJet 1020";
                //report.PrintOptions.PaperSize = PaperSize.Paper11x17;
                //report.PrintToPrinter(1, true, 1, 1);
                //Save receipt into File.
                string filepath = Common.LoginUser.DigiFolderPath;
                string filepathdate = System.IO.Path.Combine(filepath, "OrderReceipt\\" + DateTime.Now.ToString("yyyyMMdd"));
                if (!Directory.Exists(filepathdate))
                    Directory.CreateDirectory(filepathdate);
                string filename = searchOrderNo + "_Refund" + ".pdf";
                report.ExportToDisk(ExportFormatType.PortableDocFormat, filename);
                string sourceFile = Environment.CurrentDirectory + "\\" + filename;
                if (File.Exists(sourceFile))
                {
                    File.Move(sourceFile, filepathdate + "\\" + filename);
                }
                //End Save receipt into File.
                report.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, "refundslippp.pdf");
                report.PrintToPrinter(1, true, 1, 1);
                //BillReportviewer.ViewerCore.ReportSource = report;
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
            }
        }

        private void CmbReasonType_SelectedIndexChanged(object sender, EventArgs e)
        {
            //ComboBox cb = (ComboBox)sender;
            //if (cb.SelectedItem == "Others")
            //{
            //    txtreason.IsEnabled = true;
            //}
            //else
            //{
            //    txtreason.IsEnabled = true;
            //}

            txtreason.IsEnabled = true;
            // Here is your Code for selection change
        }


        private void CmbReasonTyped_SelectedIndexChanged(object sender, EventArgs e)
        {
            //ComboBox cb = (ComboBox)sender;
            //if (cb.SelectedItem == "Others".ToString())
            //{
            //    txtboxreason.IsEnabled = true;
            //}

            //else
            //{
            //    txtboxreason.IsEnabled = true;

            //}

            txtboxreason.IsEnabled = true;
            // Here is your Code for selection change
        }

        private void btn_Click2(object sender, RoutedEventArgs e)
        {
            Button _objbtn = new Button();
            _objbtn = (Button)sender;
            switch (_objbtn.Content.ToString())
            {
                case "ENTER":
                    {
                        controlon.Text = controlon.Text + Environment.NewLine;
                        controlon.Focus();
                        break;
                    }
                case "SPACE":
                    {
                        controlon.Text = controlon.Text + " ";
                        controlon.Focus();
                        break;
                    }
                case "CLOSE":
                    {
                        KeyBorderOrder.Visibility = Visibility.Collapsed;
                        break;
                    }
                case "Back":
                    {
                        TextBox objtxt = (TextBox)(controlon);
                        if (controlon.Text.Length > 0 && controlon.SelectionStart > 0)
                        {
                            int index = controlon.SelectionStart;
                            controlon.Text = controlon.Text.Remove(controlon.SelectionStart - 1, 1);
                            controlon.Select(index - 1, 0);
                        }
                        controlon.Focus();
                        break;
                    }
                default:
                    {
                        controlon.Text = controlon.Text + _objbtn.Content;
                        controlon.SelectionStart = controlon.Text.Length;
                        controlon.Focus();
                    }
                    break;
            }
        }

        private void btn_Click(object sender, RoutedEventArgs e)
        {
            //Button _objbtn = new Button();
            //_objbtn = (Button)sender;
            Button _objbtn = (Button)sender;

            switch (_objbtn.Content.ToString())
            {
                case "ENTER":
                    {
                        controlon.Text = controlon.Text + Environment.NewLine;
                        controlon.Focus();
                        break;
                    }
                case "SPACE":
                    {
                        controlon.Text = controlon.Text + " ";
                        controlon.Focus();
                        break;
                    }
                case "CLOSE":
                    {
                        KeyBorder.Visibility = Visibility.Collapsed;
                        break;
                    }
                case "Back":
                    {
                        TextBox objtxt = (TextBox)(controlon);
                        if (controlon.Text.Length > 0 && controlon.SelectionStart > 0)
                        {
                            int index = controlon.SelectionStart;
                            controlon.Text = controlon.Text.Remove(controlon.SelectionStart - 1, 1);
                            controlon.Select(index - 1, 0);
                        }
                        controlon.Focus();
                        break;
                    }
                default:
                    {
                        controlon.Text = controlon.Text + _objbtn.Content;
                        controlon.SelectionStart = controlon.Text.Length;
                        controlon.Focus();
                    }
                    break;
            }
        }

        private void txtreason_GotFocus(object sender, RoutedEventArgs e)
        {
            controlon = (TextBox)sender;
            KeyBorder.Visibility = Visibility.Visible;
        }

        private void txtboxreason_GotFocus(object sender, RoutedEventArgs e)
        {
            controlon = (TextBox)sender;
            KeyboxBorder1.Visibility = Visibility.Visible;
        }

        private void btn_Click1(object sender, RoutedEventArgs e)
        {

            //Button _objbtn = new Button();
            //_objbtn = (Button)sender;
            Button _objbtn = (Button)sender;
            switch (_objbtn.Content.ToString())
            {
                case "ENTER":
                    {
                        controlon.Text = controlon.Text + Environment.NewLine;
                        controlon.Focus();
                        break;
                    }
                case "SPACE":
                    {
                        controlon.Text = controlon.Text + " ";
                        controlon.Focus();
                        break;
                    }
                case "CLOSE":
                    {
                        KeyBorder.Visibility = Visibility.Collapsed;
                        break;
                    }
                case "Back":
                    {
                        TextBox objtxt = (TextBox)(controlon);
                        if (controlon.Text.Length > 0 && controlon.SelectionStart > 0)
                        {
                            int index = controlon.SelectionStart;
                            controlon.Text = controlon.Text.Remove(controlon.SelectionStart - 1, 1);
                            controlon.Select(index - 1, 0);
                        }
                        controlon.Focus();
                        break;
                    }
                default:
                    {
                        controlon.Text = controlon.Text + _objbtn.Content;
                        controlon.SelectionStart = controlon.Text.Length;
                        controlon.Focus();
                    }
                    break;
            }
        }

        private void btnRefundOrder_Click(object sender, RoutedEventArgs e)
        {
            if (dgOrderList.Items.Count <= 0)
            {
                MessageBox.Show("No item for refund.");
            }
            else
            {
                GrdPrinting.Visibility = Visibility.Visible;
            }

        }

        private void btnClosing_Click(object sender, RoutedEventArgs e)
        {
            GrdPrinting.Visibility = Visibility.Hidden;
        }

        private void btnSubmitCancel_Click(object sender, RoutedEventArgs e)
        {
            GrdPrinting.Visibility = Visibility.Hidden;

        }
        /// <summary>
        /// Handles the Click event of the btnLogout control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void btnLogout_Click(object sender, RoutedEventArgs e)
        {
            Login _obj = new Login();
            this.Close();
            _obj.Show();
        }
        /// <summary>
        /// Handles the Click event of the btnSearch control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void btnSearch_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                KeyBorderOrder.Visibility = Visibility.Collapsed;
                _alreadyRefunded = new List<AlreadyrefundInfo>();
                _objSelectedItems = new List<CheckedItems>();
                lstItems.ItemsSource = null;
                dgOrderList.Visibility = Visibility.Collapsed;
                btnRefundOrder.IsEnabled = true;
                discountOnTotal = 0;
                // btnCancel.IsEnabled = true;
                if (GetMasterData())
                {
                    //  GetMasterData();
                    GetSearchedDetailData();
                    //   _objSelectedItems = new List<CheckedItems>();
                    if (dgOrderList.Items.Count <= 0)
                    {
                        MessageBox.Show("Please enter a valid order number.");
                        dgOrderList.Visibility = Visibility.Collapsed;
                        clear();
                    }
                    else
                    {
                        dgOrderList.Visibility = Visibility.Visible;

                    }
                }
                else
                {
                    btnRefundOrder.IsEnabled = false;
                    //btnCancel.IsEnabled = false;
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
            }
        }

        /// <summary>
        /// Handles the Click event of the btnRefund control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void btnRefund_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Button btn = new Button();
                btn = (Button)sender;
                lstItems.ItemsSource = null;
                _objPhotoList = new List<PhotosDetails>();
                _lineitemId = btn.CommandParameter.ToInt32();
                //foreach(var orderitem in _objOrderList.Where(t=>t.DG_LineItemId==lineitemId))
                //{
                //    txtTotalRefund.Text = (Convert.ToDecimal(_RefundAmount) - orderitem.DG_Refund_Amount).ToString();
                //    _RefundAmount = Convert.ToDouble(decimal.Round(Convert.ToDecimal(_RefundAmount), 2, MidpointRounding.AwayFromZero));
                //    txtTotalRefund.Text = (decimal.Round(Convert.ToDecimal(_RefundAmount), 2, MidpointRounding.AwayFromZero)).ToString();
                //    orderitem.DG_Refund_Amount = 0;
                //    orderitem.DG_Refund_Quantity = 0;
                //}
                dgOrderList.ItemsSource = null;
                dgOrderList.ItemsSource = _objOrderList;
                //GetMasterData();


               //DigiPhotoDataServices _objDbLayer = new DigiPhotoDataServices();
                OrderDetails _objitem = _objOrderList.Where(t => t.DG_LineItemId == _lineitemId).FirstOrDefault();
                if (_objitem.IsBundled)
                {
                    int counter = 1;
                    int totalloop = _objitem.loopquantity;
                    while (counter <= totalloop)
                    {
                        foreach (var item in _objitem.PhotoIds.Split(','))
                        {
                            PhotosDetails _objphotoitem = new PhotosDetails();
                            //_objphotoitem.PhotoName = _objDbLayer.GetPhotoRFIDByPhotoID(item.ToInt32());
                            PhotoInfo objPhoto = (new PhotoBusiness()).GetPhotoRFIDByPhotoID(item.ToInt32());
                            _objphotoitem.PhotoName = objPhoto.DG_Photos_RFID;
                            _objphotoitem.PhotoId = Convert.ToString(item);
                            _objphotoitem.ImageVisibility = Visibility.Collapsed;
                            _objphotoitem.OtherVisibility = Visibility.Visible;
                            _objphotoitem.AmountRefunded = _objitem.DG_LineItem_RefundPrice;
                            _objphotoitem.FilePath = System.IO.Path.Combine(objPhoto.HotFolderPath, "Thumbnails", objPhoto.DG_Photos_CreatedOn.ToString("yyyyMMdd"), _objphotoitem.PhotoName + ".jpg");

                            var item1 = from result in _alreadyRefunded
                                        where result.Photo == (item == "" ? "0" : item).ToInt32() && result.LineItemId == _objitem.DG_LineItemId
                                        select result;

                            if (item != null)
                            {
                                if (item.Count() > 0)
                                {
                                    if (counter <= item1.Count())
                                    {
                                        _objphotoitem.ischekd = true;
                                    }
                                }
                            }
                            _objPhotoList.Add(_objphotoitem);
                            break;
                        }
                        counter++;
                    }
                }
                else
                {
                    var copy = _objitem.DG_Orders_LineItems_Quantity / _objitem.PhotoIds.Split(',').Count();
                    var photoId = _objitem.PhotoIds.Split(',');
                    int currentCopy = 1;
                    while (currentCopy <= copy)
                    {
                        foreach (string phid in photoId)
                        {
                            PhotosDetails _objphotoitem = new PhotosDetails();
                            _objphotoitem.PhotoName = _objitem.DG_Orders_ProductType_Name + "-" + (currentCopy).ToString();
                            _objphotoitem.PhotoId = phid;
                            _objphotoitem.AmountRefunded = _objitem.DG_LineItem_RefundPrice;
                            //var _Plist = _objDbLayer.GetProductType();
                            var _Plist = new ProductBusiness().GetProductType();
                            var pitem = _Plist.Where(t => t.DG_Orders_ProductType_pkey == _objitem.DG_ProductTypeId).FirstOrDefault();
                            if (pitem != null)
                            {
                                PhotoInfo objPhoto = (new PhotoBusiness()).GetPhotoNameByPhotoID(phid.ToInt32());
                                if (objPhoto != null)
                                {
                                    //_objphotoitem.FilePath = Common.LoginUser.DigiFolderThumbnailPath + "\\\\" + _objDbLayer.GetPhotoNameByPhotoID(Convert.ToInt32(phid));
                                    _objphotoitem.FilePath = System.IO.Path.Combine(objPhoto.HotFolderPath, "Thumbnails", objPhoto.DG_Photos_CreatedOn.ToString("yyyyMMdd"), objPhoto.DG_Photos_FileName);
                                }
                            }

                            var item = from result in _alreadyRefunded
                                       where result.Photo == (phid == "" ? "0" : phid).ToInt32() && result.LineItemId == _objitem.DG_LineItemId
                                       select result;

                            if (item != null)
                            {
                                if (item.Count() > 0)
                                {
                                    if (currentCopy <= item.Count())
                                    {
                                        _objphotoitem.ischekd = true;
                                    }
                                }
                            }

                            _objphotoitem.ImageVisibility = Visibility.Visible;
                            _objphotoitem.OtherVisibility = Visibility.Visible;
                            _objPhotoList.Add(_objphotoitem);
                        }
                        currentCopy++;
                    }

                    //for (int i = 0; i < _objitem.DG_Orders_LineItems_Quantity;i++)
                    //{
                    //    PhotosDetails _objphotoitem = new PhotosDetails();
                    //    _objphotoitem.PhotoName = _objitem.DG_Orders_ProductType_Name + "-" + (i + 1).ToString();
                    //    _objphotoitem.PhotoId = _objitem.PhotoIds;
                    //    _objphotoitem.AmountRefunded = _objitem.DG_LineItem_RefundPrice;
                    //    var _Plist = _objDbLayer.GetProductType();
                    //    var pitem = _Plist.Where(t => t.DG_Orders_ProductType_pkey == _objitem.DG_ProductTypeId).FirstOrDefault();
                    //    if (pitem != null)
                    //    {
                    //        _objphotoitem.FilePath = pitem.DG_Orders_ProductType_Image;
                    //    }

                    //    _objphotoitem.ImageVisibility = Visibility.Collapsed;
                    //    _objphotoitem.OtherVisibility = Visibility.Visible;
                    //    _objPhotoList.Add(_objphotoitem);
                    //}
                }
                lstItems.ItemsSource = _objPhotoList;
                GrdPrint.Visibility = Visibility.Visible;
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
            }
        }

        private void btnClose_Click(object sender, RoutedEventArgs e)
        {
            GrdPrint.Visibility = Visibility.Collapsed;

        }

        private void btnsubmit_Click(object sender, RoutedEventArgs e)
        {
            GrdPrint.Visibility = Visibility.Collapsed;
        }

        /// <summary>
        /// Handles the Checked event of the chkImageName control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void chkImageName_Checked(object sender, RoutedEventArgs e)
        {
            try
            {
                CheckBox _objchk = (CheckBox)sender;
                var item = _objOrderList.Where(t => t.DG_LineItemId == _lineitemId).FirstOrDefault();
                item.DG_Refund_Quantity = item.DG_Refund_Quantity + 1;
                item.DG_Refund_Amount = item.DG_Refund_Amount + _objchk.Tag.ToDecimal();

                dgOrderList.ItemsSource = null;
                dgOrderList.ItemsSource = _objOrderList;
                _RefundAmount = _RefundAmount + _objchk.Tag.ToDouble();
                txtTotalRefund.Text = (_RefundAmount).ToString();
                txtTotalRefund.Text = (decimal.Round(txtTotalRefund.Text.ToDecimal(), 2, MidpointRounding.ToEven)).ToString();

                // _RefundAmount = _RefundAmount + Convert.ToDouble(txtTotalRefund.Text);
                CheckedItems _objselItem = new CheckedItems();
                _objselItem.LineItemId = _lineitemId;
                _objselItem.RefundPrice = _objchk.Tag.ToDecimal();
                _objselItem.SelectetdItems = Convert.ToString(_objchk.CommandParameter);
                _objSelectedItems.Add(_objselItem);

                AlreadyrefundInfo AR = new AlreadyrefundInfo();
                AR.LineItemId = _lineitemId;
                //if (Convert.ToString(_objchk.CommandParameter) == "")
                if (string.IsNullOrEmpty(_objchk.CommandParameter.ToString()))
                {
                    AR.Photo = 0;
                }
                else
                {
                    AR.Photo = _objchk.CommandParameter.ToInt32();
                }
                _alreadyRefunded.Add(AR);
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
            }
        }

        /// <summary>
        /// Handles the Unchecked event of the chkImageName control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void chkImageName_Unchecked(object sender, RoutedEventArgs e)
        {
            try
            {
                CheckBox objchk = (CheckBox)sender;
                var item = _objOrderList.Where(t => t.DG_LineItemId == _lineitemId).FirstOrDefault();
                if (item.DG_Refund_Quantity > 0)
                {
                    item.DG_Refund_Quantity = item.DG_Refund_Quantity - 1;
                }
                if (item.DG_Refund_Amount > 0)
                {
                    item.DG_Refund_Amount = item.DG_Refund_Amount - objchk.Tag.ToDecimal();
                }
                dgOrderList.ItemsSource = null;
                dgOrderList.ItemsSource = _objOrderList;
                if (_RefundAmount.ToDecimal() > 0)
                {
                    _RefundAmount = (double)(_RefundAmount.ToDecimal() - objchk.Tag.ToDecimal());
                    txtTotalRefund.Text = DefaultCurrency + " " + _RefundAmount;
                }
                txtTotalRefund.Text = DefaultCurrency + " " + (decimal.Round(_RefundAmount.ToDecimal(), 2, MidpointRounding.AwayFromZero)).ToString();

                var item1 = _objSelectedItems.Where(t => t.LineItemId == _lineitemId && t.SelectetdItems == objchk.CommandParameter.ToString()).FirstOrDefault();
                if (item1 != null)
                {
                    _objSelectedItems.Remove(item1);
                }
                var refundedItem = (from result in _alreadyRefunded
                                    where result.LineItemId == _lineitemId && result.Photo == (Convert.ToString(objchk.CommandParameter) == "" ? "0" : Convert.ToString(objchk.CommandParameter)).ToInt32()
                                    select result).FirstOrDefault();

                if (refundedItem != null)
                {
                    _alreadyRefunded.Remove(refundedItem);
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
            }

        }

        #endregion

        #region common Methods
        public void clear()
        {
            txt_TotalDiscount.Text = DefaultCurrency + " " + "0.00";
            txt_NetAmount.Text = DefaultCurrency + " " + "0.00";
            txt_TotalAmount.Text = DefaultCurrency + " " + "0.00";
            txtTotalRefund.Text = DefaultCurrency + " " + "0.00";

        }
        /// <summary>
        /// Gets the refunded items.
        /// </summary>
        /// <param name="orderId">The order unique identifier.</param>
        /// <returns></returns>
        //public List<GetRefundedItems_Result> GetRefundedItems(int OrderId)
        //{
        //    DigiPhotoDataServices _objDbLayer = new DigiPhotoDataServices();
        //    return _objDbLayer.GetRefundedItems(OrderId);

        //}
        public List<RefundInfo> GetRefundedItems(int orderId)
        {
            return (new RefundBusiness()).GetRefundedItems(orderId);
        }
        /// <summary>
        /// Gets the searched detail data.
        /// </summary>
        void GetSearchedDetailData()
        {
            try
            {
                _alreadyRefunded = new List<AlreadyrefundInfo>();
                //DigiPhotoDataServices _objDbLayer = new DigiPhotoDataServices();
                _objOrderList = new List<OrderDetails>();
                string OrderNumber = txtSearchOrderNO.Text.ToUpperInvariant();
                if (OrderNumber.IndexOf("DG-", StringComparison.OrdinalIgnoreCase) < 0)
                {
                    OrderNumber = "DG-" + OrderNumber;
                }

                List<OrderDetailInfo> _objlst = (new OrderBusiness()).GetOrderDetailsforRefund(OrderNumber);

                if (_objlst != null && _objlst.Count > 0)
                {
                    var refundeditem = GetRefundedItems((int)_objlst[0].DG_Orders_ID);
                    _objSelectedItems = new List<CheckedItems>();
                    foreach (var refunditem in refundeditem)
                    {
                        AlreadyrefundInfo AR = new AlreadyrefundInfo();
                        AR.LineItemId = refunditem.DG_LineItemId;
                        AR.Photo = refunditem.RefundPhotoId;
                        _alreadyRefunded.Add(AR);

                        CheckedItems _objselItem = new CheckedItems();
                        _objselItem.LineItemId = _lineitemId;
                        _objselItem.SelectetdItems = Convert.ToString(refunditem.RefundPhotoId);
                        _objSelectedItems.Add(_objselItem);
                    }
                    decimal? totalLineItemDiscount = 0;
                    foreach (var item in _objlst)
                    {
                        totalLineItemDiscount = totalLineItemDiscount + item.DG_Orders_LineItems_DiscountAmount;
                        totalLineItemDiscount = decimal.Round(totalLineItemDiscount.ToDecimal(), 4, MidpointRounding.AwayFromZero);
                    }
                    if (_TotalDiscount.ToDecimal() > 0)
                    {
                        discountOnTotal = _TotalDiscount.ToDecimal() - totalLineItemDiscount;
                        discountOnTotal = decimal.Round(discountOnTotal.ToDecimal(), 4, MidpointRounding.AwayFromZero);
                    }
                    foreach (var item in _objlst)
                    {
                        decimal? peritemrefundinper = _TotalAmount > 0 ? (item.DG_Orders_Details_Items_UniPrice * 100) / _TotalAmount.ToDecimal() : 0;
                        decimal? peritemrefund;
                        // decimal? peritemrefund = peritemrefundinper * DiscountOnTotal / 100;
                        if (discountOnTotal == 0)
                        {
                            peritemrefund = 0;
                        }
                        else
                        {
                            peritemrefund = peritemrefundinper * (discountOnTotal == 0 ? 1 : discountOnTotal) / 100;
                        }
                        OrderDetails _objnew = new OrderDetails();
                        _objnew.DG_LineItemId = item.DG_Orders_LineItems_pkey;
                        _objnew.DG_Orders_Details_Items_TotalCost = item.DG_Orders_Details_Items_UniPrice * item.TotalQuantity;
                        _objnew.DG_Orders_LineItems_DiscountAmount = item.LineItemshare;
                        _objnew.DG_ProductTypeId = item.DG_Orders_Details_ProductType_pkey;
                        if (!(bool)item.DG_Orders_ProductType_IsBundled)
                        {
                            if (peritemrefund != 100)
                            {
                                if (item.DG_IsPackage)
                                {
                                    //  if (item.DG_Orders_LineItems_DiscountAmount != 0)
                                    _objnew.DG_LineItem_RefundPrice = item.DG_Orders_Details_Items_UniPrice - (item.LineItemshare / item.DG_Orders_LineItems_Quantity);
                                    //else
                                    //    _objnew.DG_LineItem_RefundPrice = item.DG_Orders_Details_Items_UniPrice - (item.DG_Orders_LineItems_DiscountAmount / item.TotalQuantity);
                                }
                                else
                                {
                                    //if (item.DG_Orders_LineItems_DiscountAmount == 0)
                                    //{
                                    //    _objnew.DG_LineItem_RefundPrice = (item.DG_Orders_Details_Items_UniPrice) - item.LineItemshare;// * item.TotalQuantity;
                                    //}
                                    //else
                                    //{
                                    _objnew.DG_LineItem_RefundPrice = (item.DG_Orders_Details_Items_UniPrice - ((item.DG_Orders_LineItems_DiscountAmount.ToDecimal() / item.TotalQuantity) + peritemrefund));// * item.TotalQuantity;
                                    //}
                                }
                            }
                            else
                            {
                                if (item.DG_IsPackage)
                                {
                                    if (item.DG_Orders_LineItems_DiscountAmount == 0)
                                        _objnew.DG_LineItem_RefundPrice = item.DG_Orders_Details_Items_UniPrice - (discountOnTotal / item.TotalQuantity);
                                    else
                                        _objnew.DG_LineItem_RefundPrice = item.DG_Orders_Details_Items_UniPrice - (item.DG_Orders_LineItems_DiscountAmount / item.TotalQuantity) - (discountOnTotal / item.TotalQuantity);
                                }
                                else
                                {
                                    _objnew.DG_LineItem_RefundPrice = item.DG_Orders_Details_Items_UniPrice - (item.DG_Orders_LineItems_DiscountAmount / item.TotalQuantity);
                                }
                            }
                            _objnew.DG_Orders_LineItems_Quantity = item.TotalQuantity;
                        }
                        else
                        {
                            _objnew.DG_Orders_LineItems_Quantity = item.TotalQuantity;
                            if (peritemrefund != 100)
                                _objnew.DG_LineItem_RefundPrice = item.DG_Orders_Details_Items_UniPrice - ((item.DG_Orders_LineItems_DiscountAmount / item.TotalQuantity) + peritemrefund);
                            else
                                _objnew.DG_LineItem_RefundPrice = item.DG_Orders_Details_Items_UniPrice - (item.DG_Orders_LineItems_DiscountAmount / item.TotalQuantity);
                        }
                        _objnew.DG_LineItem_RefundPrice = decimal.Round(_objnew.DG_LineItem_RefundPrice.ToDecimal(), 4, MidpointRounding.AwayFromZero);
                        _objnew.DG_LineItemUnitPrice = item.DG_Orders_Details_Items_UniPrice;

                        _objnew.DG_Orders_ProductType_Name = item.DG_Orders_ProductType_Name;

                        var RefundItem = from Ritem in _alreadyRefunded
                                         where Ritem.LineItemId == item.DG_Orders_LineItems_pkey
                                         select Ritem;

                        if (RefundItem != null)
                        {
                            if (RefundItem.Count() > 0)
                            {
                                //if (item.DG_Photos_ID.IndexOf(',') >= 0)
                                //{
                                _objnew.DG_Refund_Amount = _objnew.DG_LineItem_RefundPrice * (RefundItem.Count() == 0 ? 1 : RefundItem.Count());
                                //}
                                //else
                                //{
                                //    _objnew.DG_Refund_Amount = _objnew.DG_LineItem_RefundPrice;
                                //}
                                _objnew.DG_Refund_Quantity = RefundItem.Count();
                            }
                            else
                            {
                                _objnew.DG_Refund_Amount = 0;
                                _objnew.DG_Refund_Quantity = 0;
                            }
                        }
                        else
                        {
                            _objnew.DG_Refund_Amount = 0;
                            _objnew.DG_Refund_Quantity = 0;
                        }
                        _objnew.PhotoIds = item.DG_Photos_ID;
                        _objnew.IsBundled = (bool)item.DG_Orders_ProductType_IsBundled;
                        _objnew.IsPackage = (bool)item.DG_IsPackage;
                        _objnew.loopquantity = (int)item.DG_Orders_LineItems_Quantity;
                        //if ( item.DG_Orders_Details_Items_UniPrice >= 0)
                        if ((item.DG_Orders_Details_Items_TotalCost == 0 && _objnew.DG_Refund_Amount == 0) ||
                            (item.DG_Orders_Details_Items_TotalCost > 0 && item.DG_Orders_Details_Items_TotalCost >= _objnew.DG_Refund_Amount))
                        {
                            _objOrderList.Add(_objnew);
                        }
                    }
                    dgOrderList.ItemsSource = _objOrderList;
                }
                else
                {
                    dgOrderList.ItemsSource = null;
                    // MessageBox.Show("Please enter a valid order number.");
                }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }

        /// <summary>
        /// Gets the master data.
        /// </summary>
        /// <returns></returns>
        public bool GetMasterData()
        {
            //DigiPhotoDataServices objDbLayer = new DigiPhotoDataServices();
            string orderNumber = txtSearchOrderNO.Text.ToUpperInvariant();
            if (orderNumber.IndexOf("DG-", StringComparison.OrdinalIgnoreCase) < 0)
            {
                orderNumber = "DG-" + orderNumber;
            }

            //DG_Orders _objOrder = _objDbLayer.GetOrder(OrderNumber);

            OrderInfo objOrder = (new OrderBusiness()).GetOrder(orderNumber);

            //RefundBusiness refBiz = new RefundBusiness();
            double refundedAmount = (new RefundBusiness()).GetOrderRefundedAmount(objOrder.DG_Orders_pkey);
            if (objOrder.DG_Orders_Number != null)
            {
                txt_NetAmount.Text = DefaultCurrency + " " + (decimal.Round(objOrder.DG_Orders_NetCost.ToDecimal(), 2, MidpointRounding.ToEven)).ToString();
                _NetAmount = objOrder.DG_Orders_NetCost.ToString().ToDouble();

                txt_TotalAmount.Text = DefaultCurrency + " " + objOrder.DG_Orders_Cost.ToDouble().ToString(".00");
                _TotalAmount = objOrder.DG_Orders_Cost.ToString().ToDouble();


                _TotalDiscount = objOrder.DG_Orders_Total_Discount.ToString().ToDouble();

                txt_TotalDiscount.Text = DefaultCurrency + " " + decimal.Round((objOrder.DG_Orders_Total_Discount.ToDecimal()), 2, MidpointRounding.ToEven).ToString();

                txtTotalRefund.Text = DefaultCurrency + " " + refundedAmount.ToString("00.00");
                _RefundAmount = refundedAmount;
                _orderId = objOrder.DG_Orders_pkey;
                if (_RefundAmount > 0 || (_TotalAmount == 0 && _RefundAmount == 0 && GetRefundedItems(objOrder.DG_Orders_pkey).Count() > 0))
                {
                    txt_NetAmount.Text = DefaultCurrency + " " + decimal.Round((objOrder.DG_Orders_NetCost.ToDecimal() - _RefundAmount.ToDecimal()), 2, MidpointRounding.ToEven).ToString() + " ( " + objOrder.DG_Orders_NetCost.ToDouble().ToString(".00") + "-" + _RefundAmount.ToString(".00") + " (Refunded)) ";
                    MessageBox.Show("This Order is already refunded , you need to cancel the order.");
                    btnRefundOrder.IsEnabled = false;
                    //return false;
                }
                else
                {
                    btnRefundOrder.IsEnabled = true;
                    txt_NetAmount.Text = DefaultCurrency + " " + (objOrder.DG_Orders_NetCost.ToDouble().ToString("0.00"));
                }
            }
            if (objOrder.DG_Orders_Canceled != null)
            {
                if ((bool)objOrder.DG_Orders_Canceled)
                {
                    MessageBox.Show("Sorry you can't cancel/refund as it is already cancelled.");
                    return false;
                }
            }
            return true;
        }


        #endregion

        /// <summary>
        /// Handles the Click event of the btnBack control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void btnBack_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                ManageHome _objHome = new ManageHome();
                _objHome.Show();
                this.Close();
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
            }
        }

        /// <summary>
        /// Handles the Click event of the btnRefundCancel control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        /// 

        private void btnSubmitCanceling_Click(object sender, RoutedEventArgs e)
        {
            GrdPrintcancel.Visibility = Visibility.Collapsed;

        }
        private void btnClosed_Click(object sender, RoutedEventArgs e)
        {
            GrdPrintcancel.Visibility = Visibility.Collapsed;

        }
        private void btnRefundCancel_Click(object sender, RoutedEventArgs e)
        {

            if (dgOrderList.Items.Count <= 0)
            {
                MessageBox.Show("No item for cancel.");
            }
            else
            {
                // GrdPrinting.Visibility = Visibility.Visible;
                GrdPrintcancel.Visibility = Visibility.Visible;
                // btnSubmit.Visibility = Visibility.Hidden;

            }

        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                //fillCombo();
                fillCombos();
                txtSearchOrderNO.Focus();
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        //private void fillCombo()
        //{

        //    //Dictionary<string, string> lstProductList;
        //    List<string> lstProductList;
        //    lstProductList = new List<string>();
        //    lstProductList.Add("--Select--");
        //    try
        //    {
        //        //  DigiPhotoDataServices _objdbLayer = new DigiPhotoDataServices();
        //        //ValueTypeBusiness valBiz = new ValueTypeBusiness();
        //        var list = (new ValueTypeBusiness()).GetReasonType((int)ValueTypeGroupEnum.RefundReason);

        //        foreach (var item in list.ToList())
        //        {
        //            lstProductList.Add(item.Name);
        //        }
        //        lstProductList.Add("Others");
        //        CmbReasonType.ItemsSource = lstProductList;

        //        CmbReasonType.SelectedIndex = 0;

        //    }
        //    catch (Exception ex)
        //    {
        //        ErrorHandler.ErrorHandler.LogError(ex);
        //    }
        //}


        private void fillCombos()
        {

            //Dictionary<string, string> lstProductList;
            List<string> lstProductList;
            lstProductList = new List<string>();
            lstProductList.Add("--Select--");

            try
            {
                //DigiPhotoDataServices _objdbLayer = new DigiPhotoDataServices();
                //ValueTypeBusiness valBiz = new ValueTypeBusiness();
                var list = (new ValueTypeBusiness()).GetReasonType((int)ValueTypeGroupEnum.CancelReason);//.Where(t => t.IsActive = true && t.ValueTypeGroupId==1);
                list.Add(new ValueTypeInfo { Name = "Others", ValueTypeId = -1 });
                CommonUtility.BindComboWithSelect<ValueTypeInfo>(CmbReasonTyped, list, "Name", "ValueTypeId", 0, DigiPhoto.Utility.Repository.Data.ClientConstant.SelectString);
                CommonUtility.BindCombo<ValueTypeInfo>(CmbReasonType, list, "Name", "ValueTypeId");

                // BindComboWithSelect

                //foreach (var item in list.ToList())
                //{
                //    lstProductList.Add(item.Name);
                //}
                //lstProductList.Add("Others");

                //CmbReasonTyped.ItemsSource = lstProductList;

                CmbReasonTyped.SelectedIndex = 0;
                CmbReasonType.SelectedIndex = 0;
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
            }
        }
        private void btnSubmitCanceled_Click(object sender, RoutedEventArgs e)
        {
            string cancelReason = string.Empty;
            ValueTypeInfo reasonType = (ValueTypeInfo)CmbReasonTyped.SelectedItem;
            if (reasonType.Name == "Others")
            {
                if (string.IsNullOrWhiteSpace(txtboxreason.Text.TrimStart().TrimEnd()))
                {
                    MessageBox.Show("Please enter the comment.");
                    return;
                }
                // CancelReason = txtboxreason.Text;
                cancelReason = reasonType.Name + "-" + txtboxreason.Text.Trim();
            }
            else
            {
                if (string.IsNullOrWhiteSpace(txtboxreason.Text.TrimStart().TrimEnd()))
                {
                    cancelReason = reasonType.Name;
                }
                else
                {
                    cancelReason = reasonType.Name + "-" + txtboxreason.Text.Trim();
                }
                //CancelReason = CmbReasonTyped.SelectedItem.ToString();
                if (string.IsNullOrWhiteSpace(cancelReason.TrimStart().TrimEnd()))
                {
                    MessageBox.Show("Please select reason for cancel.");
                    return;
                }

                if (reasonType.Name == "--Select--")
                {
                    MessageBox.Show("Please select reason for cancel.");
                    return;
                }
            }
            try
            {
                if (dgOrderList.Items.Count > 0)
                {
                    //DigiPhotoDataServices objDbLayer = new DigiPhotoDataServices();
                    string OrderNumber = txtSearchOrderNO.Text.ToUpperInvariant();
                    if (OrderNumber.IndexOf("DG-", StringComparison.OrdinalIgnoreCase) < 0)
                    {
                        OrderNumber = "DG-" + OrderNumber;
                    }
                    GrdPrintcancel.Visibility = Visibility.Collapsed;
                    //DG_Orders _objOrder = _objDbLayer.GetOrder(OrderNumber);
                    //double refundedAmount = _objDbLayer.GetOrderRefundedAmount(_objOrder.DG_Orders_pkey);
                    //if (_objOrder != null)
                    //{
                    //}
                    MessageBoxResult mr = MessageBox.Show("Are you sure you want to cancel this order.", "DigiPhoto", MessageBoxButton.YesNo);
                    if (mr == MessageBoxResult.Yes)
                    {
                        //OrderBusiness ordBiz = new OrderBusiness();
                        (new OrderBusiness()).SetCancelOrder(OrderNumber, cancelReason);
                        // AuditLog.AddUserLog(Common.LoginUser.UserId, 66, "Reason For cancel order -" + CancelReason + " and Created on " + (new DigiPhotoDataServices()).ServerDateTime().ToString());
                        AuditLog.AddUserLog(Common.LoginUser.UserId, (int)FrameworkHelper.ActionType.CancelOrder, "Reason For cancel order :" + cancelReason + "Cancel Order " + txtSearchOrderNO.Text); //+ " on " + _objDbLayer.ServerDateTime().ToShortDateString());
                        MessageBox.Show("Your Order has been cancelled.");
                        dgOrderList.ItemsSource = null;
                        dgOrderList.Visibility = Visibility.Collapsed;
                        txtboxreason.Clear();
                        lstItems.ItemsSource = null;
                        clear();
                        _objSelectedItems = new List<CheckedItems>();
                        //kapil
                        if (isEnableSlipPrint)
                        {
                            CancelReceipt(Common.LoginUser.SubstoreName.ToString(), Common.LoginUser.UserName.ToString(), cancelReason, OrderNumber);
                        }
                    }

                }
                else
                {
                    MessageBox.Show("No item for cancel.");
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
            }
        }

        public void CancelReceipt(string storeName, string operatorName, string cancelReason, string OrderNumber)
        {
            try
            {
                InitializeComponent();
                StoreInfo store = new StoreInfo();
                TaxBusiness taxBusiness = new TaxBusiness();
                store = taxBusiness.getTaxConfigData();
                string ApplicationPath = AppDomain.CurrentDomain.BaseDirectory;

                //Added rptArebic to CancelReceiptTranslator
                string rptArabic = ConfigurationManager.AppSettings["IsArabic"];

                if (rptArabic == "1")
                {
                    report.Load(ApplicationPath + "\\Reports\\CancelReceiptTranslator.rpt");
                }
                else
                {
                    report.Load(ApplicationPath + "\\Reports\\CancelReceipt.rpt");
                }

                ResourceManager resourceManager = ResourceCulture.GetCulture();

                //This is for ALL reports
                TextObject storename = (TextObject)report.ReportDefinition.Sections["Section1"].ReportObjects["Txtstorename"];
                storename.Text = storeName;

                TextObject op = (TextObject)report.ReportDefinition.Sections["Section3"].ReportObjects["txtOperator"];
                op.Text = operatorName;

                TextObject reason = (TextObject)report.ReportDefinition.Sections["DetailSection1"].ReportObjects["txtCancelReason"];
                reason.Text = cancelReason;

                TextObject orderNumber = (TextObject)report.ReportDefinition.Sections["DetailSection1"].ReportObjects["txtOrderNo"];
                orderNumber.Text = OrderNumber;

                //KCB on 07 FEB 2018 for Saving report file name with order number and invoice number 

                TextObject txtVoidedSeqNo = (TextObject)report.ReportDefinition.Sections["Section1"].ReportObjects["txtVoidedSeqNo"];
                txtVoidedSeqNo.Text = (store.IsSequenceNoRequired == true ? (new OrderBusiness()).GetOrderInvoiceNumber(_orderId) : string.Empty);

                TextObject txtSeqNo = (TextObject)report.ReportDefinition.Sections["Section1"].ReportObjects["txtSeqNo"];
                txtSeqNo.Text = (store.IsSequenceNoRequired == true ? (new OrderBusiness()).GetCancelOrderInvoiceNumber(txtVoidedSeqNo.Text) : string.Empty);
                //end 

                TextObject txtAddress = (TextObject)report.ReportDefinition.Sections["Section1"].ReportObjects["txtAddress"];
                txtAddress.Text = "" + store.Address.Replace("\n", " ").Replace("\r", "");

                TextObject txtTtile = (TextObject)report.ReportDefinition.Sections["Section1"].ReportObjects["txtReceiptTitle"];
                txtTtile.Text = store.BillReceiptTitle;

                TextObject txtPhone = (TextObject)report.ReportDefinition.Sections["Section1"].ReportObjects["txtPhone"];
                txtPhone.Text = "Phone: " + store.PhoneNo;

                TextObject txtEmail = (TextObject)report.ReportDefinition.Sections["Section1"].ReportObjects["txtEmail"];
                txtEmail.Text = "Email: " + store.EmailID;

                TextObject txtUrl = (TextObject)report.ReportDefinition.Sections["Section1"].ReportObjects["txtUrl"];
                txtUrl.Text = store.WebsiteURL;

                #region
                ////Date:15-04-2019
                ////Change made by Saroj for lable text translation for both Arebic and English 
                ////----------10 May 2019---------Change by Nilesh for Muscat for Araic and English--------start----------- 
                TextObject ThankYouHeader;
                if (rptArabic == "1")
                {

                    TextObject txtTtileEng = (TextObject)report.ReportDefinition.Sections["Section1"].ReportObjects["txtTitleEng"];
                    //txtTtileEng.Text = store.BillReceiptTitle;

                    if (resourceManager.GetString(store.BillReceiptTitle) != null)
                    {
                        txtTtileEng.Text = resourceManager.GetString(store.BillReceiptTitle);
                    }
                    else
                    {
                        txtTtileEng.Text = "";
                    }

                    
                    TextObject txtStoreNameEng = (TextObject)report.ReportDefinition.Sections["Section1"].ReportObjects["txtStoreNameEng"];
                    //txtStoreNameEng.Text = storeName;
                    if (resourceManager.GetString(storeName) != null)
                    {
                        txtStoreNameEng.Text = resourceManager.GetString(storeName);
                    }
                    else
                    {
                        txtStoreNameEng.Text = "";
                    }


                    TextObject txtOrderNoArb = (TextObject)report.ReportDefinition.Sections["Section1"].ReportObjects["txtOrderNoArb"];
                    txtOrderNoArb.Text = resourceManager.GetString("Order No");

                    TextObject txtOperatorArb = (TextObject)report.ReportDefinition.Sections["Section4"].ReportObjects["txtOperatorArb"];
                    txtOperatorArb.Text = resourceManager.GetString("Operator");

                    ThankYouHeader = (TextObject)report.ReportDefinition.Sections["Section4"].ReportObjects["Text8"];
                    if (resourceManager.GetString("Thank You") != null)
                    {
                        ThankYouHeader.Text = resourceManager.GetString("Thank You");
                    }
                    else
                    {
                        ThankYouHeader.Text = "Thank You";
                    }
                    ThankYouHeader = (TextObject)report.ReportDefinition.Sections["Section4"].ReportObjects["txtThanksArb"];

                    TextObject txtEmailArb = (TextObject)report.ReportDefinition.Sections["Section1"].ReportObjects["txtEmailArb"];
                    txtEmailArb.Text = resourceManager.GetString("Email");

                    string arabicAddress = "";
                    if (store.Address.ToLower().Contains("the dubai mall"))
                    {
                        if (resourceManager.GetString("the dubai mall") != null)
                        {
                            arabicAddress = resourceManager.GetString("the dubai mall");
                        }
                        store.Address = store.Address.Replace("The Dubai Mall", "");
                        store.Address = arabicAddress + "    " + store.Address;
                        store.Address = store.Address.Replace("THE DUBAI MALL", "");
                        txtAddress.Text = store.Address;
                    }
                    else
                    {
                        txtAddress.Text = "";
                    }

                    if (store.Address.ToLower().Contains("muscat"))
                    {
                        if (resourceManager.GetString("PO Box : 197, Bousher - 134") != null)
                        {
                            arabicAddress = resourceManager.GetString("PO Box : 197, Bousher - 134");
                        }

                        if (resourceManager.GetString("Muscat, Sultanate of Oman.") != null)
                        {
                            arabicAddress = arabicAddress + System.Environment.NewLine + resourceManager.GetString("Muscat, Sultanate of Oman.");
                        }

                        txtAddress.Text = arabicAddress;
                    }

                }
                ////----------10 May 2019---------Change by Nilesh for Muscat for Araic and English--------End----------- 
                ////--------------------------------------------------------------
                #endregion

                System.Data.DataTable dts = new System.Data.DataTable();
                dts.Columns.Add("StoreName", typeof(string));
                dts.Columns.Add("Operator", typeof(string));
                dts.Columns.Add("Reason", typeof(string));
                dts.Columns.Add("OrderNumber", typeof(string));
                dts.Rows.Add(storename, operatorName, cancelReason);
                report.SetDataSource(dts);
                report.Refresh();
                report.PrintOptions.PrinterName = Common.LoginUser.ReceiptPrinterPath;
                //report.PrintOptions.PrinterName = "HP LaserJet 1020";
                //report.PrintOptions.PaperSize = PaperSize.Paper11x17;
                //report.PrintToPrinter(1, true, 1, 1);
                //Save receipt into File.
                string filepath = Common.LoginUser.DigiFolderPath;
                string filepathdate = System.IO.Path.Combine(filepath, "OrderReceipt\\" + DateTime.Now.ToString("yyyyMMdd"));
                if (!Directory.Exists(filepathdate))
                    Directory.CreateDirectory(filepathdate);
                string filename = OrderNumber + "_Cancel" + ".pdf";
                report.ExportToDisk(ExportFormatType.PortableDocFormat, filename);
                string sourceFile = Environment.CurrentDirectory + "\\" + filename;
                if (File.Exists(sourceFile))
                {
                    File.Move(sourceFile, filepathdate + "\\" + filename);
                }
                //End Save receipt into File.
                report.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, "cancelslip.pdf");
                report.PrintToPrinter(1, true, 1, 1);
                //BillReportviewer.ViewerCore.ReportSource = report;
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
            }
        }

        private void txtSearchOrderNO_LostFocus(object sender, RoutedEventArgs e)
        {
            btnSearch.IsDefault = false;
        }

        private void txtSearchOrderNO_GotFocus(object sender, RoutedEventArgs e)
        {
            controlon = (TextBox)sender;
            btnSearch.IsDefault = true;
            KeyBorderOrder.Visibility = Visibility.Visible;
        }

        private void btnReprintOrder_Click(object sender, RoutedEventArgs e)
        {
            // ctrlReceiptReprint.Visibility = Visibility.Visible;           
        }
    }
    #region custom class
    class PhotosDetails
    {
        private string _photoName;

        public string PhotoName
        {
            get { return _photoName; }
            set { _photoName = value; }
        }
        private decimal? _amountRefunded;

        public decimal? AmountRefunded
        {
            get { return _amountRefunded; }
            set { _amountRefunded = value; }
        }
        private string _photoId;

        public string PhotoId
        {
            get { return _photoId; }
            set { _photoId = value; }
        }

        public bool ischekd { get; set; }

        public string FilePath { get; set; }
        public Visibility OtherVisibility { get; set; }

        public Visibility ImageVisibility { get; set; }
    }
    //class OrderDetails
    //{
    //    public string DG_Orders_ProductType_Name { get; set; }
    //    public decimal? DG_Orders_Details_Items_TotalCost { get; set; }
    //    public decimal? DG_Orders_LineItems_DiscountAmount { get; set; }
    //    public Int64? DG_Orders_LineItems_Quantity { get; set; }
    //    public int DG_Refund_Quantity { get; set; }
    //    public decimal? DG_Refund_Amount { get; set; }
    //    public int DG_LineItemId { get; set; }
    //    public decimal? DG_LineItem_RefundPrice { get; set; }
    //    public decimal? DG_LineItemUnitPrice { get; set; }
    //    public string PhotoIds { get; set; }
    //    public int? DG_ProductTypeId { get; set; }
    //    public bool IsBundled { get; set; }
    //    public bool IsPackage { get; set; }
    //    public int loopquantity { get; set; }
    //}
    //class CheckedItems
    //{
    //    public string SelectetdItems { get; set; }
    //    public int LineItemId { get; set; }
    //    public decimal? RefundPrice { get; set; }
    //    public string _Reason
    //    {
    //        get;
    //        set;
    //    }
    //}
    //class Alreadyrefund
    //{
    //    public int Photo { get; set; }
    //    public int LineItemId { get; set; }

    //}

    class Alreadyrefund
    {
        public int Photo { get; set; }
        public int LineItemId { get; set; }

    }
    #endregion

}


