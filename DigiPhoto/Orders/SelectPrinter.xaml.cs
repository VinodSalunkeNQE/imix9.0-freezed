﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Collections.ObjectModel;
//using DigiPhoto.DataLayer.Model;
//using DigiPhoto.DataLayer;
using System.Xml.Linq;
using DigiPhoto.Utility.Repository.ValueType;
using DigiPhoto.IMIX.Business;
using static DigiPhoto.IMIX.Model.Product;

namespace DigiPhoto.Orders
{
    /// <summary>
    /// Interaction logic for SelectPrinter.xaml
    /// </summary>
    public partial class SelectPrinter : Window
    {
        private ObservableCollection<LineItem> _objImagesToPrint;
        public bool _issemiorder;
        /// <summary>
        /// Initializes a new instance of the <see cref="SelectPrinter"/> class.
        /// </summary>
        public SelectPrinter()
        {
            InitializeComponent();
            CtrlDiscount.SetParent(grdParent);
            btnNextsimg.IsDefault = true;

        }

        /// <summary>
        /// Discounts the avaliable.
        /// </summary>
        private void DiscountAvaliable()
        {
            if ((bool)Common.LoginUser.IsDiscountAllowed)
            {
                dgOrder.Columns[7].Visibility = System.Windows.Visibility.Visible;
                dgOrder.Columns[8].Visibility = System.Windows.Visibility.Visible;
                if ((bool)Common.LoginUser.IsDiscountAllowedonTotal)
                {
                    btnTotalDiscount.Visibility = System.Windows.Visibility.Visible;
                }
                else
                {
                    btnTotalDiscount.Visibility = System.Windows.Visibility.Collapsed;
                }
            }
            else
            {
                btnTotalDiscount.Visibility = System.Windows.Visibility.Collapsed;
                dgOrder.Columns[7].Visibility = System.Windows.Visibility.Collapsed;
                dgOrder.Columns[8].Visibility = System.Windows.Visibility.Collapsed;
                txt_TotalDiscount.Visibility = System.Windows.Visibility.Collapsed;
                lbl_TotalDiscount.Visibility = System.Windows.Visibility.Collapsed;

            }
        }

        public ObservableCollection<LineItem> ImagesToPrint
        {
            get
            {
                return _objImagesToPrint;
            }
            set
            {
                GetGroupImages(value);
            }
        }

        public PlaceOrder SourceParent
        {
            get;
            set;

        }
        private Double _NetAmount = 0;
        private Double _TotalAmount = 0;
        private Double _TotalDiscount = 0;
        private string DefaultCurrency;

        /// <summary>
        /// Gets the default currency.
        /// </summary>
        private void GetDefaultCurrency()
        {
            //DigiPhotoDataServices _objDataLayer = new DigiPhotoDataServices();
            //var currency = (from objcurrency in _objDataLayer.GetCurrencyList()
            //                where objcurrency.DG_Currency_Default == true
            //                select objcurrency.DG_Currency_Symbol.ToString()).FirstOrDefault();
            //DefaultCurrency = currency;

            DefaultCurrency = (from objcurrency in (new CurrencyBusiness()).GetCurrencyList()
                               where objcurrency.DG_Currency_Default == true
                               select objcurrency.DG_Currency_Symbol.ToString()).FirstOrDefault();

        }

        /// <summary>
        /// Handles the Loaded event of the Window control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            DiscountAvaliable();
            GetDefaultCurrency();

            var toBePrinteditem = (from result in ImagesToPrint
                                   where result.ParentID == result.ItemNumber
                                   select result).ToList();

            int counter = 1;
            foreach (var lineitem in toBePrinteditem)
            {
                lineitem.ItemSeqNumber = counter;
                counter += 1;
            }
            dgOrder.ItemsSource = toBePrinteditem;
            dgOrder.UpdateLayout();
            counter = 0;
            foreach (var lineitem in toBePrinteditem)
            {
                if (!lineitem.AllowDiscount)
                {
                    DataGridRow row = (DataGridRow)dgOrder.ItemContainerGenerator.ContainerFromIndex(counter);
                    Button btnDiscount = FindByName("btnDiscount", row) as Button;
                    btnDiscount.Visibility = System.Windows.Visibility.Collapsed;
                }

                // for package images
                //if (lineitem.IsPackage)
                //{
                //    var Children = from result in ImagesToPrint
                //                   where result.ParentID == lineitem.ItemNumber && result.IsPackage == false
                //                   select result;
                //    List<String> lstItems= new List<string>();
                //    foreach (var childlineitem in ToBePrinteditem)
                //    {
                //        lstItems.AddRange(childlineitem.SelectedImages);

                //    }

                //    lineitem.SelectedImages = lstItems;
                //}

                counter++;
            }





            TotalAmount(true);
            TotalItem.Text = " Total " + toBePrinteditem.Count.ToString() + " Items";
        }
        /// <summary>
        /// Finds the name of the by.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <param name="root">The root.</param>
        /// <returns></returns>
        private FrameworkElement FindByName(string name, FrameworkElement root)
        {
            Stack<FrameworkElement> tree = new Stack<FrameworkElement>();
            tree.Push(root);
            while (tree.Count > 0)
            {
                FrameworkElement current = tree.Pop(); // root is null
                if (current.Name == name)
                    return current;

                int count = VisualTreeHelper.GetChildrenCount(current);
                for (int SupplierCounter = 0; SupplierCounter < count; ++SupplierCounter)
                {
                    DependencyObject child = VisualTreeHelper.GetChild(current, SupplierCounter);
                    if (child is FrameworkElement)
                        tree.Push((FrameworkElement)child);
                }
            }
            return null;
        }




        /// <summary>
        /// Gets the group images.
        /// </summary>
        /// <param name="orderedImages">The ordered images.</param>
        public void GetGroupImages(ObservableCollection<LineItem> orderedImages)
        {
            List<LstMyItems> items = new List<LstMyItems>();
            int counter = 1;
            foreach (LineItem image in orderedImages)
            {
                if (image.SelectedImages != null)
                {
                    if (image.SelectedProductType_ID == 127 || image.SelectedProductType_ID == 128 || image.SelectedProductType_Text.ToLower().Contains("instamobile"))
                    {
                        items = RobotImageLoader.GroupImages.Where(OrderImage => image.SelectedImages.Contains(OrderImage.PhotoId.ToString())).ToList();
                        image.GroupItems = items;
                        image.ItemSeqNumber = counter;
                    }
                    else
                    {
                        items = RobotImageLoader.PrintImages.Where(OrderImage => image.SelectedImages.Contains(OrderImage.PhotoId.ToString())).ToList();
                        image.GroupItems = items;
                        image.ItemSeqNumber = counter;
                    }
                    counter++;
                }
            }
            _objImagesToPrint = orderedImages;
        }

        /// <summary>
        /// Handles the Click event of the btnDiscount control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs" /> instance containing the event data.</param>
        private void btnDiscount_Click(object sender, RoutedEventArgs e)
        {
            Button btn = (Button)sender;

            if (btn != null)
            {
                LineItem current = (LineItem)dgOrder.CurrentItem;
                CtrlDiscount.TotalCost = current.TotalPrice;
                CtrlDiscount.IsItemLevel = true;
                CtrlDiscount.DiscountResult = current.TotalDiscount;
                btnNextsimg.IsDefault = false;
                CtrlDiscount.btnSubmit.IsDefault = true;
                var res = CtrlDiscount.ShowHandlerDialog("Discount");
                btnNextsimg.IsDefault = true;
                CtrlDiscount.btnSubmit.IsDefault = false;
                if (res != null)
                {
                    current.TotalDiscountAmount = res.TotalDiscountAmount;
                    current.NetPrice = current.TotalPrice - res.TotalDiscountAmount;
                    if (res.DiscountDetail != null)
                    {
                        current.TotalDiscount = res.DiscountDetail.ToString();
                    }



                    DataGridRow row = (DataGridRow)dgOrder.ItemContainerGenerator.ContainerFromIndex(dgOrder.SelectedIndex);
                    if (row != null)
                    {
                        TextBox tx = FindByName("txtDiscount", row) as TextBox;
                        tx.Text = Math.Round(res.TotalDiscountAmount, 2).ToString("N2");

                        tx = FindByName("txtNetPrice", row) as TextBox;
                        tx.Text = Math.Round(current.NetPrice, 2).ToString("N2");


                        //tx = FindByName("txtNetPriceblock", row) as TextBox;
                        //tx.Text = Math.Round(current.NetPrice, 2).ToString("N2");

                    }
                    TotalAmount(false);
                }
            }

        }


        /// <summary>
        /// Handles the Click event of the btnPreviousimg control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void btnPreviousimg_Click(object sender, RoutedEventArgs e)
        {
            PlaceOrder obj = (PlaceOrder)this.SourceParent;
            if (obj == null)
            {
                this.Close();
            }
            else
            {
                obj.CtrlSelectedQrCode.QRCode = string.Empty;
                obj.Visibility = System.Windows.Visibility.Visible;
                obj.Focus();
                this.Close();

            }
        }

        /// <summary>
        /// Handles the Click event of the btnNextsimg control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void btnNextsimg_Click(object sender, RoutedEventArgs e)
        {
            if (_NetAmount.ToDouble() >= 0)
            {
                GetDefaultCurrency();
                Payment obj = new Payment();
                obj._issemiorder = _issemiorder;
                obj.SourceParent = this;
                obj.PaybleAmount = _NetAmount.ToDouble();
                obj.ImagesToPrint = this.ImagesToPrint;
                obj.DefaultCurrency = this.DefaultCurrency;
                obj.DiscountDetails = ItemDiscountDetails.Text;
                obj.TotalDiscount = _TotalDiscount.ToDouble();
                obj.TotalBill = _TotalAmount.ToDouble(); 
                obj.Show();
              
                this.Visibility = Visibility.Hidden;
                obj.ShowPopup();
            }


        }
        /// <summary>
        /// Totals the amount after line item discount.
        /// </summary>
        /// <param name="totalLineItemDiscount">The total line item discount.</param>
        /// <returns></returns>
        private double TotalAmountAfterLineItemDiscount(ref double totalLineItemDiscount)
        {
            double TotalItemDiscount = 0;
            double TotalAmount = 0;
            for (int i = 0; i < ImagesToPrint.Count; i++)
            {
                DataGridRow row = (DataGridRow)dgOrder.ItemContainerGenerator.ContainerFromIndex(i);
                if (row != null)
                {
                    double Idiscount = 0;
                    TextBox tx = FindByName("txtDiscount", row) as TextBox;
                    if (tx != null)
                    {
                        if (Double.TryParse(tx.Text, out Idiscount))
                        {
                            TotalItemDiscount += Idiscount;
                        }
                    }
                    tx = FindByName("txtNetPrice", row) as TextBox;
                    if (tx != null)
                    {
                        if (Double.TryParse(tx.Text, out Idiscount))
                        {
                            TotalAmount += Idiscount;
                        }
                    }


                }

            }
            totalLineItemDiscount = TotalItemDiscount;
            return TotalAmount;
        }


        /// <summary>
        /// Adjusts the total amount.
        /// </summary>
        private void AdjustTotalAmount()
        {
            double totalItemDiscount = 0;
            double totalAmount = 0;

            if (ItemDiscountDetails.Text != string.Empty)
            {
                totalAmount = TotalAmountAfterLineItemDiscount(ref totalItemDiscount);
                string xml = ItemDiscountDetails.Text;
                if (xml != null)
                {
                    if (xml != string.Empty)
                    {
                        XDocument XDoc = XDocument.Parse(xml);
                        //int counter = 0;
                        foreach (var item in XDoc.Element("Discount").Elements("Option"))
                        {
                            Double discount = item.Attribute("discount").Value.ToString().ToDouble();   
                        }
                    }
                }
            }
        }
        /// <summary>
        /// Totals the amount.
        /// </summary>
        /// <param name="initial">if set to <c>true</c> [initial].</param>
        private void TotalAmount(bool initial)
        {
            double total = 0;
            double netPrice = 0;
            double discountitem = 0;
            txt_TotalDiscount.Text = DefaultCurrency + " 0.00";
            foreach (LineItem item in ImagesToPrint)
            {
                discountitem += item.TotalDiscountAmount;
                netPrice += (item.NetPrice);
                total += (item.TotalPrice); 
            }
            if (ItemDiscountDetails.Text != String.Empty)
            {
                XDocument Xdoc = XDocument.Parse(ItemDiscountDetails.Text);
                if (Xdoc != null)
                {
                    string xml = ItemDiscountDetails.Text;
                    if (xml != null)
                    {
                        XDocument XDoc = XDocument.Parse(xml);
                        foreach (var item in XDoc.Element("Discount").Elements("Option"))
                        {
                            Double discount = item.Attribute("discount").Value.ToString().ToDouble();
                            bool IsPercentage = item.Attribute("InPercentmode").Value.ToString().ToBoolean();
                            if (IsPercentage)
                            {
                                discountitem = discountitem + (netPrice * (discount / 100));
                            }
                            else
                            {
                                discountitem = discountitem + discount;
                            } 
                        }
                    }
                }
            }
            _TotalAmount = total;
            _TotalDiscount = discountitem;
            txt_TotalAmount.Text = DefaultCurrency + " " + Math.Round(total, 2).ToString("N2");
            txt_TotalDiscount.Text = DefaultCurrency + " " + Math.Round(discountitem, 2).ToString("N2");
            _NetAmount = Math.Round((total - discountitem), 2);
            txt_NetAmount.Text = DefaultCurrency + " " + _NetAmount.ToString("N2");
        }
        /// <summary>
        /// Handles the Click event of the btnTotalDiscount control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void btnTotalDiscount_Click(object sender, RoutedEventArgs e)
        {
            double TotalItemDiscount = 0;
            double TotalAmount = 0;
            //foreach (LineItem item in ImagesToPrint)
            //{
            //    TotalItemDiscount += item.TotalDiscountAmount;
            //    TotalAmount += (item.TotalPrice);
            //}
            TotalItemDiscount = ImagesToPrint.Select(i => i.TotalDiscountAmount).Sum();
            TotalAmount = ImagesToPrint.Select(i => i.TotalPrice).Sum();


            CtrlDiscount.TotalCost = (TotalAmount - TotalItemDiscount);
            CtrlDiscount.DiscountResult = ItemDiscountDetails.Text;
            CtrlDiscount.IsItemLevel = false;
            btnNextsimg.IsDefault = false;
            CtrlDiscount.btnSubmit.IsDefault = true;
            var res = CtrlDiscount.ShowHandlerDialog("Discount");
            btnNextsimg.IsDefault = true;
            CtrlDiscount.btnSubmit.IsDefault = false;
            if (res != null)
            {
                _TotalDiscount = TotalItemDiscount + res.TotalDiscountAmount;
                _NetAmount = _TotalAmount - _TotalDiscount;

                txt_TotalDiscount.Text = DefaultCurrency + " " + _TotalDiscount.ToString("N2");
                txt_NetAmount.Text = DefaultCurrency + " " +  _NetAmount.ToString("N2");


                if (res.DiscountDetail != null)
                {
                    ItemDiscountDetails.Text = res.DiscountDetail.ToString();
                }
            }
            else
            {
                //ItemDiscountDetails.Text = string.Empty;
                //_TotalDiscount = TotalItemDiscount;
                //_NetAmount = _TotalAmount - _TotalDiscount;
                //txt_TotalDiscount.Text = DefaultCurrency + " " +  _TotalDiscount.ToString("N2");
                //txt_NetAmount.Text = DefaultCurrency + " " +  _NetAmount.ToString("N2");
            }
        }

        /// <summary>
        /// Handles the Closed event of the Window control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void Window_Closed(object sender, EventArgs e)
        {
            btnNextsimg.Click -= new RoutedEventHandler(btnNextsimg_Click);
            btnPreviousimg.Click -= new RoutedEventHandler(btnPreviousimg_Click);
            _objImagesToPrint.Clear();
            ImagesToPrint.Clear();
        }


    }

    public class discount
    {
        public string DiscountDetail { get; set; }
        public double TotalDiscountAmount { get; set; }

    }
}
