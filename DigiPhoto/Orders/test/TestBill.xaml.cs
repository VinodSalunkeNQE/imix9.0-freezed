﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using CrystalDecisions.Shared;
using CrystalDecisions.CrystalReports.Engine;
using DigiPhoto.Common;
using System.Data;
using System.Collections;
using System.Reflection;
//using DigiPhoto.DataLayer.Model;
//using DigiPhoto.DataLayer;
using System.Printing;
using System.Drawing;
using System.IO;
using DigiPhoto.IMIX.Model;
using DigiPhoto.IMIX.Business;
using DigiPhoto.Utility.Repository.ValueType;
using DigiPhoto.DataLayer;
using System.Configuration;
using System.Net;
using DigiPhoto.Culture;
using System.Web;
using System.Resources;
using System.Diagnostics;



namespace DigiPhoto.Orders
{
    /// <summary>
    /// Interaction logic for TestBill.xaml
    /// </summary>
    public partial class TestBill : Window
    {
        ReportDocument _report = new ReportDocument();
        //DigiPhotoDataServices _objDigiPhotoDataServices = new DigiPhotoDataServices();
        private string _imagedetails = string.Empty;
        //private List<GenerateBill_Result> _result;
        [System.Runtime.InteropServices.DllImport("gdi32.dll")]
        public static extern bool DeleteObject(IntPtr hObject);
        /// <summary>
        /// Initializes a new instance of the <see cref="TestBill"/> class.
        /// </summary>
        /// <param name="strName">The strname.</param>
        /// <param name="opName">The opname.</param>
        /// <param name="orderNo">The orderno.</param>
        /// <param name="photos">The photos.</param>
        /// <param name="refund">The refund.</param>
        /// <param name="amntDue">The amntdue.</param>
        /// <param name="txtCash">The txtcash.</param>
        /// <param name="txtChange">The txtchange.</param>
        /// <param name="itemDetails">The item details.</param>
        /// <param name="paymentType">Type of the payment.</param>
        /// <param name="cardNumber">The cardnumber.</param>
        /// <param name="cardHolderName">The card holdername.</param>
        /// <param name="customerName">The customername.</param>
        /// <param name="hotelName">The hotelname.</param>
        /// <param name="roomNo">The roomno.</param>
        /// <param name="voucherNo">The voucherno.</param>
        public bool IsGSTEnabled = false;
        bool iSSGSTCGST = Convert.ToBoolean(ConfigurationManager.AppSettings["ISSGSTCGST"]);

        public TestBill(string strName, string opName, string orderNo, string photos, string refund, string amntDue, string txtCash, string txtChange, List<LinetItemsDetails> itemDetails, string paymentType, string cardNumber, string cardHolderName, string customerName, string hotelName, string roomNo, string voucherNo, string currency, int orderId, Boolean IsReprint, double totalDiscount = 0.00, string qrCodePrint = "")
        {
            InitializeComponent();
            double discountOnTotal = totalDiscount - itemDetails.Sum(x => x.Discount);
            int tax_result = 0;
            double totalAmount = Convert.ToDouble(discountOnTotal + Double.Parse(amntDue));
            string barcodeId = orderNo.Replace("DG-", "");
            string applicationPath = AppDomain.CurrentDomain.BaseDirectory;
            BarCodeGenerator.Code39 baracode = new BarCodeGenerator.Code39(barcodeId);
            Bitmap img = new Bitmap(baracode.Paint());
            StoreInfo store = new StoreInfo();
            TaxBusiness taxBusiness = new TaxBusiness();
            store = taxBusiness.getTaxConfigData();
            GetStoreConfigData();
            TaxDetailInfo taxInfo = new TaxDetailInfo();
            ///Priyanka code to chech ISGSTActive from database on 10 july 2019
            GetAllTaxDetails();

            /////changed by latika for evoucher 4March2020
            EvoucherDtlsRpt EvlstDtls = new EvoucherDtlsRpt();
            EvlstDtls = taxBusiness.GetEvocuherDtls(orderId);
/////end by latika
            if (IsGSTEnabled == true && iSSGSTCGST == true) // Code added by Anis for IsGSTEnabled
            {
                tax_result = taxBusiness.Get_TaxPercent();
                tax_result = tax_result / 2;
            }
            List<TaxDetailInfo> taxList = taxBusiness.GetTaxDetail(orderId);
            string rptArabic = ConfigurationManager.AppSettings["IsArabic"];
            ///End Priyanka
            // Added by Anisur Rahman for Dubai frame (Product code + Barcode) receipt requirement
            string productExtentionCode = ConfigurationManager.AppSettings["ProductExtentionCode"];
            string isProductCodeWithBarCode = ConfigurationManager.AppSettings["IsProductCodeWithBarCode"];
            string imgPath = string.Empty;
            Bitmap imgpCode = null;
            string applicationPathPcode = string.Empty;
            //End Here
            //Manoj need to add code for Delivery Note            
            bool isDeliveryNote = new ConfigBusiness().GetDeliveryNoteStatus(LoginUser.SubStoreId).ToBoolean();

            try
            {
                try
                {
                    img.Save(Environment.CurrentDirectory + "\\Reports\\myimg.jpg");
                }
                catch (Exception ex)
                {
                    img.Dispose();
                    GC.Collect();

                    img = new Bitmap(baracode.Paint());
                }

                if (paymentType == "CARD")
                {
                    if (isDeliveryNote)
                    {
                        //_report.Load(applicationPath + "\\Reports\\CardDeliveryNote.rpt");
                        _report.Load(applicationPath + "\\Reports\\CrystalReport1.rpt");
                    }
                    else
                    {
                        if (store.IsTaxEnabled)
                        {
                            if (store.IsTaxIncluded)
                            {
                                if (rptArabic == "1")
                                {
                                    //MessageBox.Show("-------Nilesh---Inside 1--------------");
                                    _report.Load(applicationPath + "\\Reports\\CardBillTranslator.rpt");
                                }
                                else
                                {
                                    if (isProductCodeWithBarCode.ToLower() == "true")
                                    {
                                        _report.Load(applicationPath + "\\Reports\\CardBillProductBarcode.rpt");
                                    }
                                    else
                                    {
                                        _report.Load(applicationPath + "\\Reports\\CardBill.rpt");
                                    }
                                }
                            }
                            else
                            {
                                _report.Load(applicationPath + "\\Reports\\CardBillTaxExtra.rpt");
                            }
                        }
                        else
                        {
                            if (rptArabic == "1")
                            {
                                //MessageBox.Show("-------Nilesh---Inside 1--------------");
                                _report.Load(applicationPath + "\\Reports\\CardBillTranslator.rpt");
                            }
                            else
                            {
                                if (isProductCodeWithBarCode.ToLower() == "true")
                                {
                                    _report.Load(applicationPath + "\\Reports\\CardBillProductBarcode.rpt");
                                }
                                else
                                {
                                    _report.Load(applicationPath + "\\Reports\\CardBill.rpt");
                                }
                            }
                        }
                    }
                }
                else if (paymentType == "CASH")
                {
                    if (isDeliveryNote)
                    {
                        _report.Load(applicationPath + "\\Reports\\CashBillDeliveryNote.rpt");
                    }
                    else
                    {
                        img.Save(paymentType); // Code added by Latika for receipt

                        if (store.IsTaxEnabled)
                        {
                            if (store.IsTaxIncluded)
                            {
                                if (rptArabic == "1")
                                {
                                    // ErrorHandler.ErrorHandler.LogFileWrite("-----------Nilesh-------------Cash Inside 1");
                                    //_report.Load(applicationPath + "\\Reports\\CashBill.rpt");
                                    _report.Load(System.IO.Path.Combine(applicationPath, "Reports", "CashBillTranslator.rpt"));
                                }
                                else
                                {
                                    //ErrorHandler.ErrorHandler.LogFileWrite("-----------Nilesh-------------Cash Else 1");
                                    if (isProductCodeWithBarCode.ToLower() == "true")
                                    {
                                        //_report.Load(applicationPath + "\\Reports\\CardBillProductBarcode.rpt");
                                        _report.Load(System.IO.Path.Combine(applicationPath, "Reports", "CashBillProductBarcode.rpt"));
                                    }
                                    else
                                    {
                                        _report.Load(System.IO.Path.Combine(applicationPath, "Reports", "CashBill.rpt"));
                                    }
                                }

                            }
                            else
                            {
                                //_report.Load(applicationPath + "\\Reports\\CashBillTaxExtra.rpt");
                                //System.Threading.Thread.Sleep(5000);
                                _report.Load(System.IO.Path.Combine(applicationPath, "Reports", "CashBillTaxExtra.rpt"));
                            }
                        }
                        else
                        {
                            if (rptArabic == "1")
                            {
                                //_report.Load(applicationPath + "\\Reports\\CashBill.rpt");
                                // ErrorHandler.ErrorHandler.LogFileWrite("-----------Nilesh----- 2Else--------Cash Inside 1");
                                _report.Load(System.IO.Path.Combine(applicationPath, "Reports", "CashBillTranslator.rpt"));
                            }
                            else
                            {
                                //  ErrorHandler.ErrorHandler.LogFileWrite("-----------Nilesh------2Else-------Cash Else 1");
                                if (isProductCodeWithBarCode.ToLower() == "true")
                                {
                                    //_report.Load(applicationPath + "\\Reports\\CardBillProductBarcode.rpt");
                                    _report.Load(System.IO.Path.Combine(applicationPath, "Reports", "CashBillProductBarcode.rpt"));
                                }
                                else
                                {
                                    _report.Load(System.IO.Path.Combine(applicationPath, "Reports", "CashBill.rpt"));
                                    //_report.Load(applicationPath + "\\Reports\\CashBill.rpt");
                                }
                            }

                        }
                    }
                }
                else if (paymentType == "ROOM")
                {
                    if (store.IsTaxEnabled)
                    {
                        if (store.IsTaxIncluded)
                            _report.Load(applicationPath + "\\Reports\\RoomBill.rpt");
                        else
                            _report.Load(applicationPath + "\\Reports\\RoomBillTaxExtra.rpt");
                    }
                    else
                        _report.Load(applicationPath + "\\Reports\\RoomBill.rpt");

                }
                else if (paymentType == "VOUCHER")
                {
                    if (store.IsTaxEnabled)
                    {
                        if (store.IsTaxIncluded)
                            _report.Load(applicationPath + "\\Reports\\VoucherBill.rpt");
                        else
                            _report.Load(applicationPath + "\\Reports\\VoucherBillTaxExtra.rpt");
                    }
                    else
                        _report.Load(applicationPath + "\\Reports\\VoucherBill.rpt");

                }
                else if (paymentType == "KVL")
                {
                    if (store.IsTaxEnabled)
                    {
                        if (store.IsTaxIncluded)
                            _report.Load(applicationPath + "\\Reports\\KVLBill.rpt");
                        else
                            _report.Load(applicationPath + "\\Reports\\KVLBillTaxExtra.rpt");
                    }
                    else
                        _report.Load(applicationPath + "\\Reports\\KVLBill.rpt");

                }

                ResourceManager resourceManager = ResourceCulture.GetCulture();

                //This is for ALL reports
                TextObject storename = (TextObject)_report.ReportDefinition.Sections["Section1"].ReportObjects["Txtstorename"];
                storename.Text = strName;

                TextObject txtTtile = (TextObject)_report.ReportDefinition.Sections["Section1"].ReportObjects["txtReceiptTitle"];
                txtTtile.Text = store.BillReceiptTitle;
                /// want to add Tax in single variable
                TextObject txtSeqNo = (TextObject)_report.ReportDefinition.Sections["Section1"].ReportObjects["txtSeqNo"];
                //Supress Invoice number if configuration is available for Delivery note from DigiConfigUtility Anis_16Jan19
                if (isDeliveryNote)
                {
                    txtSeqNo.Text = (store.IsSequenceNoRequired == true ? "Delivery Number: " + (new OrderBusiness()).GetOrderInvoiceNumber(orderId) : string.Empty);
                }
                else
                {
                    txtSeqNo.Text = (store.IsSequenceNoRequired == true ? "Invoice Number: " + (new OrderBusiness()).GetOrderInvoiceNumber(orderId) : string.Empty);
                }

                TextObject txtAddress = (TextObject)_report.ReportDefinition.Sections["Section1"].ReportObjects["txtAddress"];
                txtAddress.Text = "" + store.Address.Replace("\n", " ").Replace("\r", "");

                TextObject txtPhone = (TextObject)_report.ReportDefinition.Sections["Section1"].ReportObjects["txtPhone"];
                txtPhone.Text = "Phone: " + store.PhoneNo;

                TextObject txtEmail = (TextObject)_report.ReportDefinition.Sections["Section1"].ReportObjects["txtEmail"];
                txtEmail.Text = "Email: " + store.EmailID;

                TextObject txtUrl = (TextObject)_report.ReportDefinition.Sections["Section1"].ReportObjects["txtUrl"];
                txtUrl.Text = store.WebsiteURL;

                TextObject txtRegistrationNo = (TextObject)_report.ReportDefinition.Sections["Section1"].ReportObjects["txtRegistrationNo"];
                txtRegistrationNo.Text = store.TaxRegistrationText + (!string.IsNullOrEmpty(store.TaxRegistrationText) ? ": " : string.Empty) + store.TaxRegistrationNumber;

                TextObject op = (TextObject)_report.ReportDefinition.Sections["Section2"].ReportObjects["txtoperator"];
                op.Text = opName;

                TextObject order = (TextObject)_report.ReportDefinition.Sections["Section2"].ReportObjects["txtorderno"];
                order.Text = orderNo;

                TextObject photono = (TextObject)_report.ReportDefinition.Sections["Section2"].ReportObjects["textphotono"];
                photono.Text = "Photo No: " + photos;
                photono.Height = 192 * (photono.Text.Length / 45);

                TextObject refundtxt = (TextObject)_report.ReportDefinition.Sections["Section4"].ReportObjects["txtrefund"];
                refundtxt.Text = refund;

                TextObject amountdue = (TextObject)_report.ReportDefinition.Sections["Section4"].ReportObjects["txtamountdue"];
                amountdue.Text = amntDue;

                TextObject txtCurrency = (TextObject)_report.ReportDefinition.Sections["Section2"].ReportObjects["txtCurrency"];
                txtCurrency.Text = " (" + currency + ")";
                //////changed by latika for Evoucher No
                try
                {
                    //TextObject txtEvoucher = (TextObject)_report.ReportDefinition.Sections["RhEvoucher"].ReportObjects["txtEvoucherNo"];
                    //TextObject txtPhotoCount = (TextObject)_report.ReportDefinition.Sections["RhEvoucher"].ReportObjects["txtPhotoCount"];

                    if (!string.IsNullOrEmpty(EvlstDtls.EvoucherNo))
                    {
                        if (!string.IsNullOrEmpty(EvlstDtls.EvoucherNo))
                        {
                            TextObject txtEvoucher = (TextObject)_report.ReportDefinition.Sections["RhEvoucher"].ReportObjects["txtEvoucherNo"];
                            txtEvoucher.Text = EvlstDtls.EvoucherNo;
                            TextObject txtPhotoCount = (TextObject)_report.ReportDefinition.Sections["RhEvoucher"].ReportObjects["txtPhotoCount"];
                            txtPhotoCount.Text = EvlstDtls.PhotoCount.ToString();
                        }
                        else
                        {
                            _report.ReportDefinition.Sections["RhEvoucher"].SectionFormat.EnableSuppress = true;
                        }
                    }
                }
                catch (Exception e) { }
                ////end by latika for Evoucher

                if (IsGSTEnabled == true && isDeliveryNote != true)
                {
                    //Add logic of calculation GST amount
                    amntDue = Convert.ToString(Math.Round(Convert.ToDouble(amntDue) / 1.18, 2));

                    if (iSSGSTCGST == true)
                    {
                        //Add CGST text value
                        TextObject amountCGST = (TextObject)_report.ReportDefinition.Sections["Section4"].ReportObjects["txtCGST"];
                        amountCGST.ObjectFormat.EnableSuppress = false;
                        amountCGST.Text = Convert.ToString(Math.Round(Convert.ToDouble(amntDue) * 0.09, 2));


                        TextObject amountSGST = (TextObject)_report.ReportDefinition.Sections["Section4"].ReportObjects["txtSGST"];
                        amountSGST.ObjectFormat.EnableSuppress = false;
                        amountSGST.Text = Convert.ToString(Math.Round(Convert.ToDouble(amntDue) * 0.09, 2));

                        TextObject txtCGST = (TextObject)_report.ReportDefinition.Sections["Section4"].ReportObjects["lblCGST"];
                        txtCGST.ObjectFormat.EnableSuppress = false;

                        TextObject txtSGST = (TextObject)_report.ReportDefinition.Sections["Section4"].ReportObjects["lblSGST"];
                        txtSGST.ObjectFormat.EnableSuppress = false;
                    }
                    else
                    {
                        if (paymentType == "CARD")
                        {
                            this._report.ReportDefinition.Sections["ReportFooterSection2"].SectionFormat.EnableSuppress = true;
                            this._report.ReportDefinition.Sections["ReportFooterSection4"].SectionFormat.EnableSuppress = true;
                        }
                        else if (paymentType == "CASH")
                        {
                            this._report.ReportDefinition.Sections["ReportFooterSection1"].SectionFormat.EnableSuppress = true;
                        }
                    }

                }

                //This is only for CASH
                if (paymentType == "CASH")
                {
                    TextObject subtotal = (TextObject)_report.ReportDefinition.Sections["Section4"].ReportObjects["txtsubtotal"];
                    //subtotal.Text = (Double.Parse(amntDue) + discountOnTotal).ToString("#0.00"); //Previous code
                    subtotal.Text = Convert.ToString(Double.Parse(amntDue));

                    if (IsGSTEnabled == true && isDeliveryNote != true)
                    {
                        TextObject discount = (TextObject)_report.ReportDefinition.Sections["Section4"].ReportObjects["txtDiscount"];
                        //discount.Text = discountOnTotal.ToString("#0.00"); // old code
                        //Double discountPer = Convert.ToDouble((discountOnTotal / totalAmount) * 100);
                        //Double totdeductedTax = totalAmount / 1.18;
                        //discount.Text = Convert.ToString(Math.Round((totdeductedTax * discountPer) / 100, 2));

                        //code for discount changes -- ashirwad

                        discount.Text = Convert.ToString(Math.Round(totalDiscount, 2));

                        if (!isDeliveryNote)
                        {
                            for (int ind = 0; ind < this._report.ReportDefinition.Sections.Count; ind++)
                            {
                                if (this._report.ReportDefinition.Sections[ind].Name == "ReportFooterSection1")
                                {
                                    this._report.ReportDefinition.Sections["ReportFooterSection1"].SectionFormat.EnableSuppress = false;
                                    break;
                                }
                            }
                        }
                    }
                    else
                    {
                        TextObject discount = (TextObject)_report.ReportDefinition.Sections["Section4"].ReportObjects["txtDiscount"];
                        discount.Text = discountOnTotal.ToString("#0.00");
                        if (!isDeliveryNote)
                        {
                            for (int ind = 0; ind < this._report.ReportDefinition.Sections.Count; ind++)
                            {
                                if (this._report.ReportDefinition.Sections[ind].Name == "ReportFooterSection1")
                                {
                                    this._report.ReportDefinition.Sections["ReportFooterSection1"].SectionFormat.EnableSuppress = true;
                                    break;
                                }
                            }
                        }
                    }
                    TextObject cash = (TextObject)_report.ReportDefinition.Sections["Section4"].ReportObjects["txtcash"];
                    cash.Text = txtCash;
                    TextObject change = (TextObject)_report.ReportDefinition.Sections["Section4"].ReportObjects["txtchange"];
                    change.Text = txtChange;
                }
                //This is for CARD,ROOM & VOUCHER
                if (paymentType == "CARD" || paymentType == "ROOM" || paymentType == "VOUCHER" || paymentType == "KVL")
                {
                    TextObject subtotal = (TextObject)_report.ReportDefinition.Sections["Section4"].ReportObjects["txtsubtotal"];
                    subtotal.Text = (Double.Parse(amntDue) + discountOnTotal).ToString("#0.00");


                    if (IsGSTEnabled == true)
                    {
                        TextObject discount = (TextObject)_report.ReportDefinition.Sections["Section4"].ReportObjects["txtDiscount"];
                        //discount.Text = discountOnTotal.ToString("#0.00"); // old codeS
                        //Double discountPer = Convert.ToDouble((discountOnTotal / totalAmount) * 100);
                        //Double totdeductedTax = totalAmount / 1.18;
                        //discount.Text = Convert.ToString(Math.Round((totdeductedTax * discountPer) / 100, 2));

                        //code for discount changes -- ashirwad

                        discount.Text = Convert.ToString(Math.Round(totalDiscount, 2));

                        if (!isDeliveryNote)
                        {
                            for (int ind = 0; ind < this._report.ReportDefinition.Sections.Count; ind++)
                            {
                                if (this._report.ReportDefinition.Sections[ind].Name == "ReportFooterSection2")
                                {
                                    this._report.ReportDefinition.Sections["ReportFooterSection2"].SectionFormat.EnableSuppress = false;
                                    break;
                                }

                            }
                            for (int ind = 0; ind < this._report.ReportDefinition.Sections.Count; ind++)
                            {
                                if (this._report.ReportDefinition.Sections[ind].Name == "ReportFooterSection4")
                                {
                                    this._report.ReportDefinition.Sections["ReportFooterSection4"].SectionFormat.EnableSuppress = false;
                                    break;
                                }

                            }
                        }
                    }
                    else
                    {
                        TextObject discount = (TextObject)_report.ReportDefinition.Sections["Section4"].ReportObjects["txtDiscount"];
                        discount.Text = discountOnTotal.ToString("#0.00");
                        if (!isDeliveryNote)
                        {
                            for (int ind = 0; ind < this._report.ReportDefinition.Sections.Count; ind++)
                            {
                                if (this._report.ReportDefinition.Sections[ind].Name == "ReportFooterSection2")
                                {
                                    this._report.ReportDefinition.Sections["ReportFooterSection2"].SectionFormat.EnableSuppress = true;
                                    break;
                                }
                            }

                            for (int ind = 0; ind < this._report.ReportDefinition.Sections.Count; ind++)
                            {
                                if (this._report.ReportDefinition.Sections[ind].Name == "ReportFooterSection4")
                                {
                                    this._report.ReportDefinition.Sections["ReportFooterSection4"].SectionFormat.EnableSuppress = true;
                                    break;
                                }
                            }
                        }
                    }
                }

                //This is only for CARD
                if (paymentType == "CARD")
                {
                    TextObject cardno = (TextObject)_report.ReportDefinition.Sections["Section4"].ReportObjects["txtCardno"];
                    cardno.Text = "XXXX XXXX XXXX " + cardNumber;

                    TextObject cardholdername = (TextObject)_report.ReportDefinition.Sections["Section4"].ReportObjects["txtcustomername"];
                    cardholdername.Text = cardHolderName;
                }

                //This is only for ROOM
                if (paymentType == "ROOM")
                {
                    TextObject name = (TextObject)_report.ReportDefinition.Sections["Section4"].ReportObjects["txtname"];
                    name.Text = customerName;

                    TextObject hotelname = (TextObject)_report.ReportDefinition.Sections["Section4"].ReportObjects["txthotel"];
                    hotelname.Text = hotelName;

                    TextObject roomno = (TextObject)_report.ReportDefinition.Sections["Section4"].ReportObjects["txtroomno"];
                    roomno.Text = roomNo;
                }

                //This is only for VOUCHER
                if (paymentType == "VOUCHER")
                {
                    TextObject name = (TextObject)_report.ReportDefinition.Sections["Section4"].ReportObjects["txtname"];
                    name.Text = customerName;

                    TextObject voucherno = (TextObject)_report.ReportDefinition.Sections["Section4"].ReportObjects["txtvoucherno"];
                    voucherno.Text = voucherNo;
                }

                /////made changes by latika for translation Discount header

                string productNameHeader = "Product Name";
                string strQTY = "Qty";
                string strPrice = "Price";
                string strDisc = "Disc";
                string strTotal = "Total";//--------- MONIKA
                decimal TotalValue = 0;//--------- MONIKA
                TextObject RefundHeader;//-----Saroj---------
                TextObject ThankYouHeader;//-----Saroj---------
                if (rptArabic == "1")
                {
                    TextObject SubtotalHeader;
                    TextObject ChangeHeader;
                    TextObject CashHeader;
                    TextObject DiscountHeader;

                    #region
                    ////Date:10-04-2019
                    ////Change made by Saroj for lable text translation for both Arebic and English 



                    //TextObject txtTtileEng = (TextObject)_report.ReportDefinition.Sections["Section1"].ReportObjects["txtTitleEng"];
                    //txtTtileEng.Text = store.BillReceiptTitle;

                    //TextObject txtStoreNameEng = (TextObject)_report.ReportDefinition.Sections["Section1"].ReportObjects["txtStoreNameEng"];
                    // txtStoreNameEng.Text = strName;

                    //TextObject txtProductNameEng = new TextObject(); //= (TextObject)_report.ReportDefinition.Sections["DetailSection2"].ReportObjects["txtProductNameEng"];
                    //txtProductNameEng.Text = productNameHeader;

                    //TextObject txtQTYEng = (TextObject)_report.ReportDefinition.Sections["Section2"].ReportObjects["txtQtyEng"];
                    //txtQTYEng.Text = strQTY;

                    //TextObject txtPriceEng = (TextObject)_report.ReportDefinition.Sections["Section2"].ReportObjects["txtPriceEng"];
                    //txtPriceEng.Text = strPrice;

                    //TextObject txtDiscEng = (TextObject)_report.ReportDefinition.Sections["Section2"].ReportObjects["txtDiscEng"];
                    //txtDiscEng.Text = strDisc;

                    //TextObject txtDiscHeaderEng = new TextObject(); //= (TextObject)_report.ReportDefinition.Sections["Section4"].ReportObjects["txtDiscHeaderEng"];
                    //txtDiscHeaderEng.Text = "Discount";

                    //TextObject txtSubTotalEng = new TextObject(); //(TextObject)_report.ReportDefinition.Sections["Section4"].ReportObjects["txtSubtotalEng"];
                    //txtSubTotalEng.Text = "Sub Total";

                    //TextObject txtAmountDueEng = (TextObject)_report.ReportDefinition.Sections["Section4"].ReportObjects["txtAmountDueEng"];
                    //txtAmountDueEng.Text = "Amount Due";

                    //TextObject txtIncludeEng = (TextObject)_report.ReportDefinition.Sections["Section4"].ReportObjects["txtIncludeEng"];
                    //txtIncludeEng.Text = "Included";

                    //TextObject txtTaxPerEng = (TextObject)_report.ReportDefinition.Sections["DetailSection2"].ReportObjects["txtTaxPercEng"];
                    //if (taxList.Count > 0)
                    //{
                    //    txtTaxPerEng.Text = string.Format("{0:F2}", decimal.Parse(taxList[0].TaxPercentage.ToString())) + "%";
                    //}

                    //TextObject txtVATEng = (TextObject)_report.ReportDefinition.Sections["Section4"].ReportObjects["txtVatEng"];
                    //txtVATEng.Text = "VAT(AED)";

                    //TextObject txtTaxValueEng;
                    //txtTaxValueEng = (TextObject)_report.ReportDefinition.Sections["Section4"].ReportObjects["txtTaxValueEng"];
                    //if (taxList.Count > 0)
                    //{
                    //    txtTaxValueEng.Text = taxList[0].TaxAmount.ToString();
                    //}

                    TextObject txtOrderNoArb = (TextObject)_report.ReportDefinition.Sections["Section1"].ReportObjects["txtOrderNoArb"];
                    txtOrderNoArb.Text = resourceManager.GetString("Order No");

                    TextObject txtPhotoNoArb = (TextObject)_report.ReportDefinition.Sections["Section4"].ReportObjects["txtPhotoNoArb"];
                    txtPhotoNoArb.Text = resourceManager.GetString("Photo No");

                    TextObject txtOperatorArb = (TextObject)_report.ReportDefinition.Sections["Section4"].ReportObjects["txtOperatorArb"];
                    txtOperatorArb.Text = resourceManager.GetString("Operator");

                    ThankYouHeader = (TextObject)_report.ReportDefinition.Sections["Section4"].ReportObjects["txtThanksArb"];

                    RefundHeader = (TextObject)_report.ReportDefinition.Sections["Section4"].ReportObjects["txtRefundTextArb"];

                    TextObject txtEmailArb = (TextObject)_report.ReportDefinition.Sections["Section1"].ReportObjects["txtEmailArb"];
                    txtEmailArb.Text = resourceManager.GetString("Email");

                    TextObject txtInvoiceArb = (TextObject)_report.ReportDefinition.Sections["Section1"].ReportObjects["txtSeqNoArb"];
                    if (isDeliveryNote)
                    {
                        txtInvoiceArb.Text = (store.IsSequenceNoRequired == true ? "Delivery Number: " + (new OrderBusiness()).GetOrderInvoiceNumber(orderId) : string.Empty);
                    }
                    else
                    {
                        txtInvoiceArb.Text = (resourceManager.GetString("Invoice Number") != null ? resourceManager.GetString("Invoice Number") : "Invoice Number");
                    }


                    ////--------------------------------------------------------------
                    #endregion
                    if (paymentType == "CARD")
                    {

                        DiscountHeader = (TextObject)_report.ReportDefinition.Sections["Section4"].ReportObjects["txtDischeader"];
                        SubtotalHeader = (TextObject)_report.ReportDefinition.Sections["Section4"].ReportObjects["Text9"]; ///card
                        ThankYouHeader = (TextObject)_report.ReportDefinition.Sections["Section4"].ReportObjects["Text8"]; ///card

                        TextObject CustomerNameHeader = (TextObject)_report.ReportDefinition.Sections["Section4"].ReportObjects["lblCustomerName"];
                        CustomerNameHeader.Text = resourceManager.GetString("CUSTOMER NAME");

                        TextObject SignatureOfCustHeader = (TextObject)_report.ReportDefinition.Sections["Section4"].ReportObjects["lblSignatureOfCust"];
                        SignatureOfCustHeader.Text = resourceManager.GetString("Signature of the customer");

                        TextObject CardNoHeader = (TextObject)_report.ReportDefinition.Sections["Section4"].ReportObjects["txtCardNoArb"];
                        CardNoHeader.Text = resourceManager.GetString("CARD NO");
                    }
                    else
                    {
                        ///cash
                        ///
                        DiscountHeader = (TextObject)_report.ReportDefinition.Sections["Section4"].ReportObjects["txtDischeader"];

                        CashHeader = (TextObject)_report.ReportDefinition.Sections["Section4"].ReportObjects["Text9"];
                        ChangeHeader = (TextObject)_report.ReportDefinition.Sections["Section4"].ReportObjects["Text14"];
                        ThankYouHeader = (TextObject)_report.ReportDefinition.Sections["Section4"].ReportObjects["Text8"];
                        // TextObject TaxHeader = (TextObject)_report.ReportDefinition.Sections["Section4"].ReportObjects["Subreport1"];
                        SubtotalHeader = (TextObject)_report.ReportDefinition.Sections["Section4"].ReportObjects["Text6"];///cash
                        ChangeHeader.Text = resourceManager.GetString("Change");
                        CashHeader.Text = resourceManager.GetString("Cash");

                        #region
                        //Date: 12-04-2019
                        ///Added by saroj for english translation for cash mode
                        TextObject txtCashEng = (TextObject)_report.ReportDefinition.Sections["Section4"].ReportObjects["txtCashEng"];
                        txtCashEng.Text = "Cash";

                        TextObject txtChangeEng = (TextObject)_report.ReportDefinition.Sections["Section4"].ReportObjects["txtChangeEng"];
                        txtChangeEng.Text = "Change";

                        #endregion  
                    }

                    TextObject AmountHeader = (TextObject)_report.ReportDefinition.Sections["Section4"].ReportObjects["Text3"];
                    productNameHeader = resourceManager.GetString("Product Name");
                    strQTY = resourceManager.GetString(strQTY);
                    strPrice = resourceManager.GetString(strPrice);
                    strDisc = resourceManager.GetString(strDisc);
                    strTotal = resourceManager.GetString(strTotal);
                    DiscountHeader.Text = resourceManager.GetString("Discount");
                    SubtotalHeader.Text = resourceManager.GetString("Sub Total");
                    AmountHeader.Text = resourceManager.GetString("Amount Due");

                    if (resourceManager.GetString(strName) != null)
                    {
                        storename.Text = resourceManager.GetString(strName);
                    }
                    else
                    {
                        storename.Text = "";
                    }

                    if (resourceManager.GetString(store.BillReceiptTitle) != null)
                    {
                        txtTtile.Text = resourceManager.GetString(store.BillReceiptTitle);
                    }
                    else
                    {
                        txtTtile.Text = "";// store.BillReceiptTitle;
                    }

                    if (resourceManager.GetString("No Refunds") != null)
                    {
                        RefundHeader.Text = resourceManager.GetString("No Refunds");
                    }
                    else
                    {
                        RefundHeader.Text = "No Refunds!";
                    }
                    string arabicAddress = "";
                    if (store.Address.ToLower().Contains("the dubai mall"))
                    {
                        if (resourceManager.GetString("the dubai mall") != null)
                        {
                            arabicAddress = resourceManager.GetString("the dubai mall");
                        }
                        store.Address = store.Address.Replace("The Dubai Mall", "");
                        store.Address = arabicAddress + "    " + store.Address;
                        store.Address = store.Address.Replace("THE DUBAI MALL", "");
                        txtAddress.Text = store.Address;
                    }
                    else
                    {
                        txtAddress.Text = "";
                    }

                    if (resourceManager.GetString("Thank You") != null)
                    {
                        ThankYouHeader.Text = resourceManager.GetString("Thank You");
                    }
                    else
                    {
                        ThankYouHeader.Text = "Thank You";
                    }
                    TextObject txtIncludedText;
                    txtIncludedText = (TextObject)_report.ReportDefinition.Sections["Section4"].ReportObjects["txtIncluded"];
                    if (resourceManager.GetString("Included") != null)
                    {
                        txtIncludedText.Text = resourceManager.GetString("Included");
                    }
                    else
                    {
                        txtIncludedText.Text = "Included";
                    }
                    TextObject txtTaxValPercText;
                    txtTaxValPercText = (TextObject)_report.ReportDefinition.Sections["Section4"].ReportObjects["txtTaxPerc"];
                    if (taxList.Count > 0)
                    {
                        if (!string.IsNullOrEmpty(taxList[0].TaxPercentage.ToString()))
                        {
                            txtTaxValPercText.Text = string.Format("{0:F2}", decimal.Parse(taxList[0].TaxPercentage.ToString())) + "%";
                        }
                    }

                    TextObject txtAEDText;
                    txtAEDText = (TextObject)_report.ReportDefinition.Sections["Section4"].ReportObjects["txtAED"];
                    if (resourceManager.GetString("VATAED") != null)
                    {
                        txtAEDText.Text = resourceManager.GetString("VATAED");
                    }
                    else
                    {
                        txtAEDText.Text = "VAT (AED)";
                    }

                    TextObject txtTaxValueObj;
                    txtTaxValueObj = (TextObject)_report.ReportDefinition.Sections["Section4"].ReportObjects["txtTaxValue"];
                    if (taxList.Count > 0)
                    {
                        txtTaxValueObj.Text = taxList[0].TaxAmount.ToString();
                    }

                }
                ////end by latika


                if (IsGSTEnabled == true && isDeliveryNote != true)
                {
                    int itemCount = 0;
                    _imagedetails += "<LineItems>";
                    foreach (var items in itemDetails)
                    {
                        // Added by Anisur Rahman for Dubai frame (Product code + Barcode) receipt requirement
                        string pCode = string.Empty;
                        if (!string.IsNullOrEmpty(isProductCodeWithBarCode))
                        {
                            if (isProductCodeWithBarCode.ToLower() == "true")
                            {
                                applicationPathPcode = @"C:\Program Files (x86)\iMix\";
                                string filepathdate = System.IO.Path.Combine(applicationPathPcode, "Barcode");
                                if (!Directory.Exists(filepathdate))
                                    Directory.CreateDirectory(filepathdate);
                                string file = "myimg.jpg";
                                pCode = items.Productcode;
                                string pBarcodeId = pBarcodeId = pCode.ToUpper().Replace(productExtentionCode.ToUpper(), "");
                                string productBarCode = pBarcodeId.Replace("-", "");
                                BarCodeGenerator.Code39 pbaracode = new BarCodeGenerator.Code39(productBarCode);
                                imgpCode = new Bitmap(pbaracode.Paint());
                                imgPath = System.IO.Path.Combine(applicationPathPcode, "Barcode\\" + pCode + System.IO.Path.GetExtension(file));
                                try
                                {
                                    imgpCode.Save(imgPath);
                                }
                                catch (Exception ex)
                                {
                                    imgpCode.Dispose();
                                    GC.Collect();
                                    imgpCode = new Bitmap(baracode.Paint());
                                }
                            }
                        }
                        // End Here 

                        TotalValue += Convert.ToDecimal(items.Productprice);//-------MONIKA
                        //Double itemDisc = 0;
                        string discount = "0";
                        if (items.Discount != 0.0)
                        {
                            //itemDisc = Convert.ToDouble(items.Productprice) / Convert.ToDouble(items.Discount);
                            //discount = Convert.ToString(Math.Round((Math.Round(Convert.ToDouble(items.Productprice) / 1.18, 2) * (itemDisc)) / 100, 2));

                            discount = Convert.ToString(Convert.ToDouble(Math.Round(items.Discount, 2)));
                        }

                        if (Convert.ToDouble(discount) <= 0)
                        {
                            discount = "0.00";
                        }
                        //_imagedetails += "<Item><Name>" + items.Productname + "</Name><Qty>" + items.Productquantity + "</Qty><Price>" + Convert.ToString(Math.Round(Convert.ToDouble(items.Productprice) / 1.18, 2)) + "</Price><Disc>" + discount + "</Disc></Item>";

                        _imagedetails += "<Item><Name>" + items.Productname + "</Name><Qty>" + items.Productquantity + "</Qty><Price>" + Convert.ToString(Math.Round(Convert.ToDouble(items.Productprice), 2)) + "</Price><Disc>" + discount + "</Disc><ProductCode>" + pCode + "</ProductCode></Item>";

                        //-----Start----------- QR Code should be display in Old Reciept----------Anis  29 Aug 2019--------------
                        if (items.QRCode != null)
                        {
                            itemCount++;
                            if (rptArabic == "0") //Removed && !isDeliveryNote from condition, display QR code in delivery note receipt added by Manoj at 19-Feb-20
                            {
                                TextObject ClaimCodelbl = (TextObject)_report.ReportDefinition.Sections["Section2"].ReportObjects["ClaimCodeText"];
                                ClaimCodelbl.Text = "Claim Code";

                                TextObject qrCodeObejct = (TextObject)_report.ReportDefinition.Sections["Section2"].ReportObjects["txtClaimCodeQR"];
                                if (itemCount <= 1)
                                {
                                    //modified by nilesh.
                                    if (string.IsNullOrEmpty(qrCodeObejct.Text) || qrCodeObejct.Text.ToUpper() != items.QRCode.ToUpper())
                                    {
                                        qrCodeObejct.Text = items.QRCode.ToUpper();
                                    }
                                }
                                else
                                {
                                    if (string.IsNullOrEmpty(qrCodeObejct.Text) || qrCodeObejct.Text.ToUpper() != items.QRCode.ToUpper())
                                    {
                                        qrCodeObejct.Text = qrCodeObejct.Text + "," + items.QRCode.ToUpper();
                                    }
                                }
                            }
                            //-----End-----------QR Code should be display in Old Reciept----------Anis  29 Aug 2019--------------
                        }
                    }
                    _imagedetails += "<TotalVal> " + TotalValue + " </TotalVal></LineItems>";//-------MONIKA
                }
                else
                {
                    //Culture for arabic convert using resource file mahesh patel
                    //ResourceManager resourceManager = ResourceCulture.GetCulture();
                    ////int qrcodeCount = 0; //online QRcode Count
                    int itemCount = 0;
                    _imagedetails += "<LineItems>";
                    foreach (var items in itemDetails)
                    {
                        //Added by Anisur Rahman for Dubai frame (Product code + Barcode) receipt requirement
                        string pCode = string.Empty;
                        if (!string.IsNullOrEmpty(isProductCodeWithBarCode))
                        {
                            if (isProductCodeWithBarCode.ToLower() == "true")
                            {
                                applicationPathPcode = @"C:\Program Files (x86)\iMix\";
                                string filepathdate = System.IO.Path.Combine(applicationPathPcode, "Barcode");
                                if (!Directory.Exists(filepathdate))
                                    Directory.CreateDirectory(filepathdate);
                                string file = "myimg.jpg";
                                pCode = items.Productcode;
                                string pBarcodeId = pBarcodeId = pCode.ToUpper().Replace(productExtentionCode.ToUpper(), "");
                                string productBarCode = pBarcodeId.Replace("-", "");
                                BarCodeGenerator.Code39 pbaracode = new BarCodeGenerator.Code39(productBarCode);
                                imgpCode = new Bitmap(pbaracode.Paint());
                                imgPath = System.IO.Path.Combine(applicationPathPcode, "Barcode\\" + pCode + System.IO.Path.GetExtension(file));
                                try
                                {
                                    imgpCode.Save(imgPath);
                                }
                                catch (Exception ex)
                                {
                                    imgpCode.Dispose();
                                    GC.Collect();
                                    imgpCode = new Bitmap(baracode.Paint());
                                }
                            }
                        }
                        //End Here
                        string priceValue = items.Productprice;
                        TotalValue += Convert.ToDecimal(items.Productprice);//-------MONIKA
                        string dicPrecision = "#0.00";
                        if (store.Address.ToLower().Contains("muscat"))
                        {
                            dicPrecision = "#0.000";
                            if (items.Productprice != null)
                            {
                                priceValue = Convert.ToDouble(items.Productprice).ToString("F3");
                            }
                        }
                        if (resourceManager != null)
                        {
                            items.ArabicName = resourceManager.GetString(items.Productname);
                        }
                        //if (qrCodePrint != null || qrCodePrint == ":")

                        if (items.QRCode != null)
                        {
                            //commented By Nilesh ---------change the logic of getting QR code----20 Aug 2018---------

                            itemCount++;
                            _imagedetails += "<Item><ProductNamelbl>" + productNameHeader + "</ProductNamelbl><Name>" + items.Productname + "</Name><ArabicName>" + items.ArabicName + "</ArabicName><ClaimCode>Claim Code</ClaimCode><QRCode>" + items.QRCode + "</QRCode><Qtylbl>" + strQTY + "</Qtylbl><Qty>" + items.Productquantity + "</Qty><Pricelbl>" + strPrice + "</Pricelbl><Price>" + priceValue + "</Price><Currency>" + currency + "</Currency><Disclbl>" + strDisc + "</Disclbl><Disc>" + items.Discount.ToString(dicPrecision) + "</Disc><ProductCode>" + pCode + "</ProductCode></Item>";//old using parameter qrcode                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             

                            //-----Start-----------For Old Reciept for Online i have set the Claim code----------Nilesh  5th Sep 2018--------------
                            if (rptArabic == "0")////Removed && !isDeliveryNote from condition, display QR code in delivery note receipt added by Manoj at 19-Feb-20
                            {
                                TextObject ClaimCodelbl = (TextObject)_report.ReportDefinition.Sections["Section2"].ReportObjects["ClaimCodeText"];
                                ClaimCodelbl.Text = "Claim Code";

                                TextObject qrCodeObejct = (TextObject)_report.ReportDefinition.Sections["Section2"].ReportObjects["txtClaimCodeQR"];
                                if (itemCount <= 1)
                                {
                                    //modified by nilesh.
                                    if (string.IsNullOrEmpty(qrCodeObejct.Text) || qrCodeObejct.Text.ToUpper() != items.QRCode.ToUpper())
                                    {
                                        qrCodeObejct.Text = items.QRCode.ToUpper();
                                    }
                                }
                                else
                                {
                                    if (string.IsNullOrEmpty(qrCodeObejct.Text) || qrCodeObejct.Text.ToUpper() != items.QRCode.ToUpper())
                                    {
                                        qrCodeObejct.Text = qrCodeObejct.Text + "," + items.QRCode.ToUpper();
                                    }
                                }
                            }
                            //-----End-----------For Old Reciept for Online i have set the Claim code----------Nilesh  5th Sep 2018--------------

                        }
                        else
                        {
                            if (_imagedetails == string.Empty)
                            {

                                _imagedetails = "<Item><ProductNamelblEnglish> +Product Name +</ProductNamelblEnglish><ProductNamelbl>" + productNameHeader + "</ProductNamelbl><Name>" + items.Productname + " </Name><ArabicName>" + items.ArabicName + "</ArabicName><ClaimCode>Claim Code</ClaimCode><QRCode>" + items.QRCode + "</QRCode><Qtylbl>" + strQTY + "</Qtylbl><Qty>" + items.Productquantity + "</Qty><Pricelbl>" + strPrice + "</Pricelbl><Price>" + priceValue + "</Price><Currency>" + currency + "</Currency><Disclbl>" + strDisc + "</Disclbl><Disc>" + items.Discount.ToString(dicPrecision) + "</Disc><ProductCode>" + pCode + "</ProductCode></Item>";//old using parameter qrcode                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             
                            }
                            else
                            {
                                _imagedetails += "<Item><ProductNamelbl>" + productNameHeader + "</ProductNamelbl><Name>" + items.Productname + "</Name><ArabicName>" + items.ArabicName + "</ArabicName><ClaimCode>Claim Code</ClaimCode><QRCode>" + items.QRCode + "</QRCode><Qtylbl>" + strQTY + "</Qtylbl><Qty>" + items.Productquantity + "</Qty><Pricelbl>" + strPrice + "</Pricelbl><Price>" + priceValue + "</Price><Currency>" + currency + "</Currency><Disclbl>" + strDisc + "</Disclbl><Disc>" + items.Discount.ToString(dicPrecision) + "</Disc><ProductCode>" + pCode + "</ProductCode></Item>"; //old using parameter qrcode                                                
                            }
                            if (items.QRCode == null)
                            {
                                _imagedetails = _imagedetails.Replace(@"<ClaimCode>Claim Code</ClaimCode><QRCode></QRCode>", "");
                            }
                        }
                        if (items.QRCode == "")
                        {
                            _imagedetails = _imagedetails.Replace(@"<ClaimCode>Claim Code</ClaimCode><QRCode></QRCode>", "");
                        }
                    }
                    _imagedetails += "<TotalVal> " + TotalValue + " </TotalVal></LineItems>";//-------MONIKA
                }
                /// ------START---- added by MONIKA // Condition Modified by Anis 20-Aug-19
                
                if (isDeliveryNote != true && (paymentType == "CARD" || paymentType == "CASH"))
                {

                    TextObject totalvalue = (TextObject)_report.ReportDefinition.Sections["Section4"].ReportObjects["txtTotaln"];
                    totalvalue.Text = TotalValue.ToString();

                    TextObject total_PerItemDisc = (TextObject)_report.ReportDefinition.Sections["Section4"].ReportObjects["txtDiscTotal"];
                    total_PerItemDisc.Text = (totalDiscount - discountOnTotal).ToString();

                    TextObject total_Disc = (TextObject)_report.ReportDefinition.Sections["Section4"].ReportObjects["txtDiscount"];
                    total_Disc.Text = totalDiscount.ToString();
                    if (iSSGSTCGST == true)
                    {
                        TextObject CGST_tax = (TextObject)_report.ReportDefinition.Sections["Section4"].ReportObjects["lblCGST"];
                        CGST_tax.Text = "CGST @" + tax_result.ToString() + "%";

                        TextObject SGST_tax = (TextObject)_report.ReportDefinition.Sections["Section4"].ReportObjects["lblSGST"];
                        SGST_tax.Text = "SGST @" + tax_result.ToString() + "%";
                    }
                    else
                    {
                        if (paymentType == "CARD")
                        {
                            this._report.ReportDefinition.Sections["ReportFooterSection2"].SectionFormat.EnableSuppress = true;
                            this._report.ReportDefinition.Sections["ReportFooterSection4"].SectionFormat.EnableSuppress = true;
                        }
                        else if (paymentType == "CASH")
                        {
                            this._report.ReportDefinition.Sections["ReportFooterSection1"].SectionFormat.EnableSuppress = true;
                        }
                    }

                }
                //////------- ENDtax_result

                DataTable dt = GetDataTablefromStream(_imagedetails);

                //result = objDigiPhotoDataServices.GetCashBillSummary(Imagedetails);
                if (!string.IsNullOrEmpty(logoImgName) && System.IO.File.Exists(logoImgName))
                    _report.DataDefinition.FormulaFields[1].Text = "'" + logoImgName + "'";
                _report.DataDefinition.FormulaFields[0].Text = "'" + Environment.CurrentDirectory + "\\Reports\\myimg.jpg" + "'";
                _report.SetDataSource(dt);
                //if (store.IsTaxEnabled)
                if (rptArabic == "0")
                {
                    _report.Subreports[0].SetDataSource(taxList);
                }
                _report.Refresh();
                _report.PrintOptions.PrinterName = Common.LoginUser.ReceiptPrinterPath;

                // For Single Single Print
                int noOfRecipt = GetNoOfBillReceipt();


                for (int copy = 0; copy < noOfRecipt; copy++)
                {
                    TextObject BillType = (TextObject)_report.ReportDefinition.Sections["Section1"].ReportObjects["txtBillType"];

                    if (copy == 0)
                    {
                        BillType.Text = "Original";
                    }
                    else
                    {
                        BillType.Text = "Duplicate";
                    }
                    CrystalDecisions.Shared.PageMargins margin = new CrystalDecisions.Shared.PageMargins(); ///changed by latika 20191019 for duplicate print mergine issue
                    if (IsReprint == true)
                    {  ///changed by latika 20191019 for duplicate print mergine issue
                        margin.leftMargin = 0;
                        margin.rightMargin = 0;
                        margin.topMargin = 0;
                        _report.PrintOptions.ApplyPageMargins(margin);

                        BillType.Text = "Duplicate";
                        _report.PrintToPrinter(1, true, 1, 0);
                        return;
                    }
                    // CrystalDecisions.Shared.PageMargins margin = new CrystalDecisions.Shared.PageMargins();
                    margin.leftMargin = 0;
                    margin.rightMargin = 0;
                    margin.topMargin = 0;
                    //Added condition for arabic translation
                    if (rptArabic != "0")
                    {
                        margin.bottomMargin = 0;
                    }
                    else
                    {
                        margin.bottomMargin = 292;
                    }
                    //margin.bottomMargin = 0;
                    _report.PrintOptions.ApplyPageMargins(margin);
                    _report.PrintToPrinter(1, true, 1, 0);
                }
                if (!IsReprint)
                {
                    SaveOrderNo(orderNo);
                    //Save receipt into File.
                    string filepath = Common.LoginUser.DigiFolderPath;
                    string filepathdate = System.IO.Path.Combine(filepath, "OrderReceipt\\" + DateTime.Now.ToString("yyyyMMdd"));
                    if (!Directory.Exists(filepathdate))
                        Directory.CreateDirectory(filepathdate);
                    //KCB on 07 FEB 2018 for Saving report file name with order number and invoice number  
                    //string filename = orderNo +".pdf";
                    string filename = string.Empty;
                    if (!string.IsNullOrEmpty(txtSeqNo.Text))
                    {
                        string[] InvoiceList = txtSeqNo.Text.Split(':');
                        filename = orderNo + "_" + InvoiceList[1] + ".pdf";
                    }
                    else
                        filename = orderNo + ".pdf";
                    //END
                    _report.ExportToDisk(ExportFormatType.PortableDocFormat, filename);
                    string sourceFile = Environment.CurrentDirectory + "\\" + filename;
                    if (File.Exists(sourceFile))
                    {
                        File.Move(sourceFile, filepathdate + "\\" + filename);
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
            }
            finally
            {
                if (img != null)
                {
                    IntPtr deleteObject = img.GetHbitmap();
                    DeleteObject(deleteObject);
                    img.Dispose();
                }
                // Added by Anisur Rahman for Dubai frame (Product code + Barcode) receipt requirement    
                if (isProductCodeWithBarCode.ToLower() == "true")
                {

                    if (imgpCode != null)
                    {
                        IntPtr deleteObject = imgpCode.GetHbitmap();
                        DeleteObject(deleteObject);
                        imgpCode.Dispose();
                    }
                    string[] files = Directory.GetFiles(applicationPathPcode + "Barcode\\");
                    {
                        if (files != null)
                        {
                            foreach (string file in files)
                            {
                                File.Delete(file);
                            }
                        }
                    }
                }
                // End Here
                baracode = null;
                BillReportviewer = null;
                _report.Dispose();
                GC.Collect(); // Code added by Latika to release the memory
            }


        }
        /// <summary>
        /// Priyanka code to chech ISGSTActive from database on 10 july 2019
        /// </summary>
        private void GetAllTaxDetails()
        {
            try
            {
                List<TaxDetailInfo> lstTaxDetails = new List<TaxDetailInfo>();
                TaxBusiness TaxData = new TaxBusiness();
                lstTaxDetails = TaxData.GetAllTaxDetails();
                var result = lstTaxDetails.Where(x => x.TaxName == "GST" && x.IsActive == true).ToList();
                foreach (var item in result)
                {
                    IsGSTEnabled = item.IsActive;
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
            }
        }
        /// <summary>
        /// End Priyanka
        /// </summary>
        /// <param name="strName"></param>
        /// <param name="opName"></param>
        /// <param name="orderNo"></param>
        /// <param name="photos"></param>
        /// <param name="refund"></param>
        /// <param name="amntDue"></param>
        /// <param name="txtCash"></param>
        /// <param name="txtChange"></param>
        /// <param name="itemDetails"></param>
        /// <param name="paymentType"></param>
        /// <param name="cardNumber"></param>
        /// <param name="cardHolderName"></param>
        /// <param name="customerName"></param>
        /// <param name="hotelName"></param>
        /// <param name="roomNo"></param>
        /// <param name="voucherNo"></param>
        /// <param name="currency"></param>
        /// <param name="orderId"></param>
        /// <param name="IsReprint"></param>
        /// <param name="totalDiscount"></param>
        /// <param name="qrCodePrint"></param>
        /// <param name="IsWeChat"></param>

        public TestBill(string strName, string opName, string orderNo, string photos, string refund, string amntDue, string txtCash, string txtChange, List<LinetItemsDetails> itemDetails, string paymentType, string cardNumber, string cardHolderName, string customerName, string hotelName, string roomNo, string voucherNo, string currency, int orderId, Boolean IsReprint, double totalDiscount = 0.00, string qrCodePrint = "", Boolean IsWeChat = true)
        {
            InitializeComponent();
            double discountOnTotal = totalDiscount - itemDetails.Sum(x => x.Discount);
            double totalAmount = Convert.ToDouble(discountOnTotal + Double.Parse(amntDue));
            string barcodeId = orderNo.Replace("DG-", "");
            string applicationPath = AppDomain.CurrentDomain.BaseDirectory;
            BarCodeGenerator.Code39 baracode = new BarCodeGenerator.Code39(barcodeId);
            Bitmap img = new Bitmap(baracode.Paint());
            StoreInfo store = new StoreInfo();
            TaxBusiness taxBusiness = new TaxBusiness();
            store = taxBusiness.getTaxConfigData();
            GetStoreConfigData();
            TaxDetailInfo taxInfo = new TaxDetailInfo();
            int tax_result = 0;
            if (IsGSTEnabled == true) // Code added by Anis for IsGSTEnabled
            {
                tax_result = taxBusiness.Get_TaxPercent();
                tax_result = tax_result / 2;
            }

            ErrorHandler.ErrorHandler.LogFileWrite("1");

            //TaxBusiness TaxBusiness = new TaxBusiness();
            List<TaxDetailInfo> taxList = taxBusiness.GetTaxDetail(orderId);
            ErrorHandler.ErrorHandler.LogFileWrite("2");
            string rptArabic = ConfigurationManager.AppSettings["IsArabic"];
            // Added by Anisur Rahman for Dubai frame (Product code + Barcode) receipt requirement
            string productExtentionCode = ConfigurationManager.AppSettings["ProductExtentionCode"];
            string isProductCodeWithBarCode = ConfigurationManager.AppSettings["IsProductCodeWithBarCode"];
            string imgPath = string.Empty;
            Bitmap imgpCode = null;
            string applicationPathPcode = string.Empty;
            //End Here
            //Manoj need to add code for Delivery Note            
            bool isDeliveryNote = new ConfigBusiness().GetDeliveryNoteStatus(LoginUser.SubStoreId).ToBoolean();

            ErrorHandler.ErrorHandler.LogFileWrite("3");

            try
            {
                try
                {
                    img.Save(Environment.CurrentDirectory + "\\Reports\\myimg.jpg");
                }
                catch (Exception ex)
                {
                    img.Dispose();
                    GC.Collect();

                    img = new Bitmap(baracode.Paint());
                }

                if (paymentType == "CARD")
                {
                    if (isDeliveryNote)
                    {
                        _report.Load(applicationPath + "\\Reports\\CardDeliveryNote.rpt");
                    }
                    else
                    {
                        if (store.IsTaxEnabled)
                        {
                            if (store.IsTaxIncluded)
                            {
                                if (rptArabic == "1")
                                {
                                    //MessageBox.Show("-------Nilesh---Inside 1--------------");
                                    _report.Load(applicationPath + "\\Reports\\CardBillTranslator.rpt");
                                }
                                else
                                {
                                    //_report.Load(applicationPath + "\\Reports\\CardBill.rpt");

                                    if (!IsWeChat)
                                    {
                                        _report.Load(applicationPath + "\\Reports\\CardBill.rpt");
                                    }
                                    else
                                    {
                                        _report.Load(System.IO.Path.Combine(applicationPath, "Reports", "CardBillWeChat.rpt"));
                                    }
                                }
                            }
                            else
                            {
                                _report.Load(applicationPath + "\\Reports\\CardBillTaxExtra.rpt");
                            }
                        }
                        else
                        {
                            if (rptArabic == "1")
                            {
                                //MessageBox.Show("-------Nilesh---Inside 1--------------");
                                _report.Load(applicationPath + "\\Reports\\CardBillTranslator.rpt");
                            }
                            else
                            {
                                //_report.Load(applicationPath + "\\Reports\\CardBill.rpt");
                                if (!IsWeChat)
                                {
                                    _report.Load(applicationPath + "\\Reports\\CardBill.rpt");
                                }
                                else
                                {
                                    _report.Load(System.IO.Path.Combine(applicationPath, "Reports", "CardBillWeChat.rpt"));
                                }
                            }
                        }
                    }
                }
                else if (paymentType == "CASH")
                {
                    if (isDeliveryNote)
                    {
                        _report.Load(applicationPath + "\\Reports\\CashBillDeliveryNote.rpt");
                    }
                    else
                    {
                        img.Save(paymentType); // Code added by Latika for receipt

                        if (store.IsTaxEnabled)
                        {
                            if (store.IsTaxIncluded)
                            {
                                if (rptArabic == "1")
                                {
                                    // ErrorHandler.ErrorHandler.LogFileWrite("-----------Nilesh-------------Cash Inside 1");
                                    //_report.Load(applicationPath + "\\Reports\\CashBill.rpt");
                                    _report.Load(System.IO.Path.Combine(applicationPath, "Reports", "CashBillTranslator.rpt"));
                                }
                                else
                                {
                                    //ErrorHandler.ErrorHandler.LogFileWrite("-----------Nilesh-------------Cash Else 1");
                                    if (!IsWeChat)
                                    {
                                        _report.Load(System.IO.Path.Combine(applicationPath, "Reports", "CashBill.rpt"));
                                    }
                                    else
                                    {
                                        _report.Load(System.IO.Path.Combine(applicationPath, "Reports", "CashBillWeChat.rpt"));
                                    }
                                }

                            }
                            else
                            {
                                //_report.Load(applicationPath + "\\Reports\\CashBillTaxExtra.rpt");
                                //System.Threading.Thread.Sleep(5000);
                                _report.Load(System.IO.Path.Combine(applicationPath, "Reports", "CashBillTaxExtra.rpt"));
                            }
                        }
                        else
                        {
                            if (rptArabic == "1")
                            {
                                //_report.Load(applicationPath + "\\Reports\\CashBill.rpt");
                                // ErrorHandler.ErrorHandler.LogFileWrite("-----------Nilesh----- 2Else--------Cash Inside 1");
                                _report.Load(System.IO.Path.Combine(applicationPath, "Reports", "CashBillTranslator.rpt"));
                            }
                            else
                            {
                                //  ErrorHandler.ErrorHandler.LogFileWrite("-----------Nilesh------2Else-------Cash Else 1");
                                // _report.Load(System.IO.Path.Combine(applicationPath, "Reports", "CashBillWeChat.rpt"));

                                if (!IsWeChat)
                                {
                                    _report.Load(System.IO.Path.Combine(applicationPath, "Reports", "CashBill.rpt"));
                                }
                                else
                                {
                                    _report.Load(System.IO.Path.Combine(applicationPath, "Reports", "CashBillWeChat.rpt"));
                                }

                            }
                            //_report.Load(applicationPath + "\\Reports\\CashBill.rpt");
                        }
                    }
                }
                else if (paymentType == "ROOM")
                {
                    if (store.IsTaxEnabled)
                    {
                        if (store.IsTaxIncluded)
                            _report.Load(applicationPath + "\\Reports\\RoomBill.rpt");
                        else
                            _report.Load(applicationPath + "\\Reports\\RoomBillTaxExtra.rpt");
                    }
                    else
                        _report.Load(applicationPath + "\\Reports\\RoomBill.rpt");

                }
                else if (paymentType == "VOUCHER")
                {
                    if (store.IsTaxEnabled)
                    {
                        if (store.IsTaxIncluded)
                            _report.Load(applicationPath + "\\Reports\\VoucherBill.rpt");
                        else
                            _report.Load(applicationPath + "\\Reports\\VoucherBillTaxExtra.rpt");
                    }
                    else
                        _report.Load(applicationPath + "\\Reports\\VoucherBill.rpt");

                }
                else if (paymentType == "KVL")
                {
                    if (store.IsTaxEnabled)
                    {
                        if (store.IsTaxIncluded)
                            _report.Load(applicationPath + "\\Reports\\KVLBill.rpt");
                        else
                            _report.Load(applicationPath + "\\Reports\\KVLBillTaxExtra.rpt");
                    }
                    else
                        _report.Load(applicationPath + "\\Reports\\KVLBill.rpt");

                }

                ResourceManager resourceManager = ResourceCulture.GetCulture();

                //This is for ALL reports
                TextObject storename = (TextObject)_report.ReportDefinition.Sections["Section1"].ReportObjects["Txtstorename"];
                storename.Text = strName;

                TextObject txtTtile = (TextObject)_report.ReportDefinition.Sections["Section1"].ReportObjects["txtReceiptTitle"];
                txtTtile.Text = store.BillReceiptTitle;

                TextObject txtSeqNo = (TextObject)_report.ReportDefinition.Sections["Section1"].ReportObjects["txtSeqNo"];
                //Supress Invoice number if configuration is available for Delivery note from DigiConfigUtility Anis_16Jan19
                if (isDeliveryNote)
                {
                    txtSeqNo.Text = (store.IsSequenceNoRequired == true ? "Delivery Number: " + (new OrderBusiness()).GetOrderInvoiceNumber(orderId) : string.Empty);
                }
                else
                {
                    txtSeqNo.Text = (store.IsSequenceNoRequired == true ? "Invoice Number: " + (new OrderBusiness()).GetOrderInvoiceNumber(orderId) : string.Empty);
                }

                TextObject txtAddress = (TextObject)_report.ReportDefinition.Sections["Section1"].ReportObjects["txtAddress"];
                txtAddress.Text = "" + store.Address.Replace("\n", " ").Replace("\r", "");

                TextObject txtPhone = (TextObject)_report.ReportDefinition.Sections["Section1"].ReportObjects["txtPhone"];
                txtPhone.Text = "Phone: " + store.PhoneNo;

                TextObject txtEmail = (TextObject)_report.ReportDefinition.Sections["Section1"].ReportObjects["txtEmail"];
                txtEmail.Text = "Email: " + store.EmailID;

                TextObject txtUrl = (TextObject)_report.ReportDefinition.Sections["Section1"].ReportObjects["txtUrl"];
                txtUrl.Text = store.WebsiteURL;

                TextObject txtRegistrationNo = (TextObject)_report.ReportDefinition.Sections["Section1"].ReportObjects["txtRegistrationNo"];
                txtRegistrationNo.Text = store.TaxRegistrationText + (!string.IsNullOrEmpty(store.TaxRegistrationText) ? ": " : string.Empty) + store.TaxRegistrationNumber;

                TextObject op = (TextObject)_report.ReportDefinition.Sections["Section2"].ReportObjects["txtoperator"];
                op.Text = opName;

                TextObject order = (TextObject)_report.ReportDefinition.Sections["Section2"].ReportObjects["txtorderno"];
                order.Text = orderNo;

                TextObject photono = (TextObject)_report.ReportDefinition.Sections["Section2"].ReportObjects["textphotono"];
                photono.Text = "Photo No: " + photos;
                photono.Height = 192 * (photono.Text.Length / 45);

                TextObject refundtxt = (TextObject)_report.ReportDefinition.Sections["Section4"].ReportObjects["txtrefund"];
                refundtxt.Text = refund;

                TextObject amountdue = (TextObject)_report.ReportDefinition.Sections["Section4"].ReportObjects["txtamountdue"];
                amountdue.Text = amntDue;

                TextObject txtCurrency = (TextObject)_report.ReportDefinition.Sections["Section2"].ReportObjects["txtCurrency"];
                txtCurrency.Text = " (" + currency + ")";

                if (IsGSTEnabled == true)
                {
                    //Add logic of calculation GST amount
                    amntDue = Convert.ToString(Math.Round(Convert.ToDouble(amntDue) / 1.18, 2));

                    //Add CGST text value
                    TextObject amountCGST = (TextObject)_report.ReportDefinition.Sections["Section4"].ReportObjects["txtCGST"];
                    amountCGST.ObjectFormat.EnableSuppress = false;
                    amountCGST.Text = Convert.ToString(Math.Round(Convert.ToDouble(amntDue) * 0.09, 2));


                    TextObject amountSGST = (TextObject)_report.ReportDefinition.Sections["Section4"].ReportObjects["txtSGST"];
                    amountSGST.ObjectFormat.EnableSuppress = false;
                    amountSGST.Text = Convert.ToString(Math.Round(Convert.ToDouble(amntDue) * 0.09, 2));

                    TextObject txtCGST = (TextObject)_report.ReportDefinition.Sections["Section4"].ReportObjects["lblCGST"];
                    txtCGST.ObjectFormat.EnableSuppress = false;

                    TextObject txtSGST = (TextObject)_report.ReportDefinition.Sections["Section4"].ReportObjects["lblSGST"];
                    txtSGST.ObjectFormat.EnableSuppress = false;

                }

                //This is only for CASH
                if (paymentType == "CASH")
                {
                    TextObject subtotal = (TextObject)_report.ReportDefinition.Sections["Section4"].ReportObjects["txtsubtotal"];
                    //subtotal.Text = (Double.Parse(amntDue) + discountOnTotal).ToString("#0.00"); //Previous code
                    subtotal.Text = Convert.ToString(Double.Parse(amntDue));

                    if (IsGSTEnabled == true)
                    {
                        TextObject discount = (TextObject)_report.ReportDefinition.Sections["Section4"].ReportObjects["txtDiscount"];
                        //discount.Text = discountOnTotal.ToString("#0.00"); // old code
                        //Double discountPer = Convert.ToDouble((discountOnTotal / totalAmount) * 100);
                        //Double totdeductedTax = totalAmount / 1.18;
                        //discount.Text = Convert.ToString(Math.Round((totdeductedTax * discountPer) / 100, 2));

                        //code for discount changes -- ashirwad

                        discount.Text = Convert.ToString(Math.Round(totalDiscount, 2));

                        if (!isDeliveryNote)
                        {
                            for (int ind = 0; ind < this._report.ReportDefinition.Sections.Count; ind++)
                            {
                                if (this._report.ReportDefinition.Sections[ind].Name == "ReportFooterSection1")
                                {
                                    this._report.ReportDefinition.Sections["ReportFooterSection1"].SectionFormat.EnableSuppress = false;
                                    break;
                                }
                            }
                        }
                    }
                    else
                    {
                        TextObject discount = (TextObject)_report.ReportDefinition.Sections["Section4"].ReportObjects["txtDiscount"];
                        discount.Text = discountOnTotal.ToString("#0.00");
                        if (!isDeliveryNote)
                        {
                            for (int ind = 0; ind < this._report.ReportDefinition.Sections.Count; ind++)
                            {
                                if (this._report.ReportDefinition.Sections[ind].Name == "ReportFooterSection1")
                                {
                                    this._report.ReportDefinition.Sections["ReportFooterSection1"].SectionFormat.EnableSuppress = true;
                                    break;
                                }
                            }
                        }
                    }

                    TextObject cash = (TextObject)_report.ReportDefinition.Sections["Section4"].ReportObjects["txtcash"];
                    cash.Text = txtCash;
                    TextObject change = (TextObject)_report.ReportDefinition.Sections["Section4"].ReportObjects["txtchange"];
                    change.Text = txtChange;
                }
                //This is for CARD,ROOM & VOUCHER
                if (paymentType == "CARD" || paymentType == "ROOM" || paymentType == "VOUCHER" || paymentType == "KVL")
                {
                    TextObject subtotal = (TextObject)_report.ReportDefinition.Sections["Section4"].ReportObjects["txtsubtotal"];
                    subtotal.Text = (Double.Parse(amntDue) + discountOnTotal).ToString("#0.00");


                    if (IsGSTEnabled == true)
                    {
                        TextObject discount = (TextObject)_report.ReportDefinition.Sections["Section4"].ReportObjects["txtDiscount"];
                        //discount.Text = discountOnTotal.ToString("#0.00"); // old codeS
                        //Double discountPer = Convert.ToDouble((discountOnTotal / totalAmount) * 100);
                        //Double totdeductedTax = totalAmount / 1.18;
                        //discount.Text = Convert.ToString(Math.Round((totdeductedTax * discountPer) / 100, 2));

                        //code for discount changes -- ashirwad

                        discount.Text = Convert.ToString(Math.Round(totalDiscount, 2));

                        if (!isDeliveryNote)
                        {
                            for (int ind = 0; ind < this._report.ReportDefinition.Sections.Count; ind++)
                            {
                                if (this._report.ReportDefinition.Sections[ind].Name == "ReportFooterSection2")
                                {
                                    this._report.ReportDefinition.Sections["ReportFooterSection2"].SectionFormat.EnableSuppress = false;
                                    break;
                                }

                            }
                            for (int ind = 0; ind < this._report.ReportDefinition.Sections.Count; ind++)
                            {
                                if (this._report.ReportDefinition.Sections[ind].Name == "ReportFooterSection4")
                                {
                                    this._report.ReportDefinition.Sections["ReportFooterSection4"].SectionFormat.EnableSuppress = false;
                                    break;
                                }

                            }
                        }
                    }
                    else
                    {
                        TextObject discount = (TextObject)_report.ReportDefinition.Sections["Section4"].ReportObjects["txtDiscount"];
                        discount.Text = discountOnTotal.ToString("#0.00");
                        if (!isDeliveryNote)
                        {
                            for (int ind = 0; ind < this._report.ReportDefinition.Sections.Count; ind++)
                            {
                                if (this._report.ReportDefinition.Sections[ind].Name == "ReportFooterSection2")
                                {
                                    this._report.ReportDefinition.Sections["ReportFooterSection2"].SectionFormat.EnableSuppress = true;
                                    break;
                                }
                            }

                            for (int ind = 0; ind < this._report.ReportDefinition.Sections.Count; ind++)
                            {
                                if (this._report.ReportDefinition.Sections[ind].Name == "ReportFooterSection4")
                                {
                                    this._report.ReportDefinition.Sections["ReportFooterSection4"].SectionFormat.EnableSuppress = true;
                                    break;
                                }
                            }
                        }
                    }
                }

                //This is only for CARD
                if (paymentType == "CARD")
                {
                    TextObject cardno = (TextObject)_report.ReportDefinition.Sections["Section4"].ReportObjects["txtCardno"];
                    cardno.Text = "XXXX XXXX XXXX " + cardNumber;

                    TextObject cardholdername = (TextObject)_report.ReportDefinition.Sections["Section4"].ReportObjects["txtcustomername"];
                    cardholdername.Text = cardHolderName;
                }

                //This is only for ROOM
                if (paymentType == "ROOM")
                {
                    TextObject name = (TextObject)_report.ReportDefinition.Sections["Section4"].ReportObjects["txtname"];
                    name.Text = customerName;

                    TextObject hotelname = (TextObject)_report.ReportDefinition.Sections["Section4"].ReportObjects["txthotel"];
                    hotelname.Text = hotelName;

                    TextObject roomno = (TextObject)_report.ReportDefinition.Sections["Section4"].ReportObjects["txtroomno"];
                    roomno.Text = roomNo;
                }

                //This is only for VOUCHER
                if (paymentType == "VOUCHER")
                {
                    TextObject name = (TextObject)_report.ReportDefinition.Sections["Section4"].ReportObjects["txtname"];
                    name.Text = customerName;

                    TextObject voucherno = (TextObject)_report.ReportDefinition.Sections["Section4"].ReportObjects["txtvoucherno"];
                    voucherno.Text = voucherNo;
                }

                /////made changes by latika for translation Discount header

                string productNameHeader = "Product Name";
                string strQTY = "Qty";
                string strPrice = "Price";
                string strDisc = "Disc";
                string strTotal = "Total";//--------- MONIKA
                decimal TotalValue = 0;//--------- MONIKA
                TextObject RefundHeader;//-----Saroj---------
                TextObject ThankYouHeader;//-----Saroj---------
                if (rptArabic == "1")
                {
                    TextObject SubtotalHeader;
                    TextObject ChangeHeader;
                    TextObject CashHeader;
                    TextObject DiscountHeader;

                    #region
                    ////Date:10-04-2019
                    ////Change made by Saroj for lable text translation for both Arebic and English 



                    TextObject txtTtileEng = (TextObject)_report.ReportDefinition.Sections["Section1"].ReportObjects["txtTitleEng"];
                    txtTtileEng.Text = store.BillReceiptTitle;

                    TextObject txtStoreNameEng = (TextObject)_report.ReportDefinition.Sections["Section1"].ReportObjects["txtStoreNameEng"];
                    txtStoreNameEng.Text = strName;

                    TextObject txtProductNameEng = (TextObject)_report.ReportDefinition.Sections["DetailSection2"].ReportObjects["txtProductNameEng"];
                    txtProductNameEng.Text = productNameHeader;

                    TextObject txtQTYEng = (TextObject)_report.ReportDefinition.Sections["Section2"].ReportObjects["txtQtyEng"];
                    txtQTYEng.Text = strQTY;

                    TextObject txtPriceEng = (TextObject)_report.ReportDefinition.Sections["Section2"].ReportObjects["txtPriceEng"];
                    txtPriceEng.Text = strPrice;

                    TextObject txtDiscEng = (TextObject)_report.ReportDefinition.Sections["Section2"].ReportObjects["txtDiscEng"];
                    txtDiscEng.Text = strDisc;

                    TextObject txtDiscHeaderEng = (TextObject)_report.ReportDefinition.Sections["Section4"].ReportObjects["txtDiscHeaderEng"];
                    txtDiscHeaderEng.Text = "Discount";

                    TextObject txtSubTotalEng = (TextObject)_report.ReportDefinition.Sections["Section4"].ReportObjects["txtSubtotalEng"];
                    txtSubTotalEng.Text = "Sub Total";

                    TextObject txtAmountDueEng = (TextObject)_report.ReportDefinition.Sections["Section4"].ReportObjects["txtAmountDueEng"];
                    txtAmountDueEng.Text = "Amount Due";

                    TextObject txtIncludeEng = (TextObject)_report.ReportDefinition.Sections["Section4"].ReportObjects["txtIncludeEng"];
                    txtIncludeEng.Text = "Included";

                    TextObject txtTaxPerEng = (TextObject)_report.ReportDefinition.Sections["DetailSection2"].ReportObjects["txtTaxPercEng"];
                    if (taxList.Count > 0)
                    {
                        txtTaxPerEng.Text = string.Format("{0:F2}", decimal.Parse(taxList[0].TaxPercentage.ToString())) + "%";
                    }

                    TextObject txtVATEng = (TextObject)_report.ReportDefinition.Sections["Section4"].ReportObjects["txtVatEng"];
                    txtVATEng.Text = "VAT(AED)";

                    TextObject txtTaxValueEng;
                    txtTaxValueEng = (TextObject)_report.ReportDefinition.Sections["Section4"].ReportObjects["txtTaxValueEng"];
                    if (taxList.Count > 0)
                    {
                        txtTaxValueEng.Text = taxList[0].TaxAmount.ToString();
                    }

                    TextObject txtOrderNoArb = (TextObject)_report.ReportDefinition.Sections["Section1"].ReportObjects["txtOrderNoArb"];
                    txtOrderNoArb.Text = resourceManager.GetString("Order No");

                    TextObject txtPhotoNoArb = (TextObject)_report.ReportDefinition.Sections["Section4"].ReportObjects["txtPhotoNoArb"];
                    txtPhotoNoArb.Text = resourceManager.GetString("Photo No");

                    TextObject txtOperatorArb = (TextObject)_report.ReportDefinition.Sections["Section4"].ReportObjects["txtOperatorArb"];
                    txtOperatorArb.Text = resourceManager.GetString("Operator");

                    ThankYouHeader = (TextObject)_report.ReportDefinition.Sections["Section4"].ReportObjects["txtThanksArb"];

                    RefundHeader = (TextObject)_report.ReportDefinition.Sections["Section4"].ReportObjects["txtRefundTextArb"];

                    TextObject txtEmailArb = (TextObject)_report.ReportDefinition.Sections["Section1"].ReportObjects["txtEmailArb"];
                    txtEmailArb.Text = resourceManager.GetString("Email");

                    TextObject txtInvoiceArb = (TextObject)_report.ReportDefinition.Sections["Section1"].ReportObjects["txtSeqNoArb"];
                    if (isDeliveryNote)
                    {
                        txtInvoiceArb.Text = (store.IsSequenceNoRequired == true ? "Delivery Number: " + (new OrderBusiness()).GetOrderInvoiceNumber(orderId) : string.Empty);
                    }
                    else
                    {
                        txtInvoiceArb.Text = (resourceManager.GetString("Invoice Number") != null ? resourceManager.GetString("Invoice Number") : "Invoice Number");
                    }


                    ////--------------------------------------------------------------
                    #endregion
                    if (paymentType == "CARD")
                    {

                        DiscountHeader = (TextObject)_report.ReportDefinition.Sections["Section4"].ReportObjects["txtDischeader"];
                        SubtotalHeader = (TextObject)_report.ReportDefinition.Sections["Section4"].ReportObjects["Text9"]; ///card
                        ThankYouHeader = (TextObject)_report.ReportDefinition.Sections["Section4"].ReportObjects["Text8"]; ///card

                        TextObject CustomerNameHeader = (TextObject)_report.ReportDefinition.Sections["Section4"].ReportObjects["lblCustomerName"];
                        CustomerNameHeader.Text = resourceManager.GetString("CUSTOMER NAME");

                        TextObject SignatureOfCustHeader = (TextObject)_report.ReportDefinition.Sections["Section4"].ReportObjects["lblSignatureOfCust"];
                        SignatureOfCustHeader.Text = resourceManager.GetString("Signature of the customer");

                        TextObject CardNoHeader = (TextObject)_report.ReportDefinition.Sections["Section4"].ReportObjects["txtCardNoArb"];
                        CardNoHeader.Text = resourceManager.GetString("CARD NO");
                    }
                    else
                    {
                        ///cash
                        ///
                        DiscountHeader = (TextObject)_report.ReportDefinition.Sections["Section4"].ReportObjects["txtDischeader"];

                        CashHeader = (TextObject)_report.ReportDefinition.Sections["Section4"].ReportObjects["Text9"];
                        ChangeHeader = (TextObject)_report.ReportDefinition.Sections["Section4"].ReportObjects["Text14"];
                        ThankYouHeader = (TextObject)_report.ReportDefinition.Sections["Section4"].ReportObjects["Text8"];
                        // TextObject TaxHeader = (TextObject)_report.ReportDefinition.Sections["Section4"].ReportObjects["Subreport1"];
                        SubtotalHeader = (TextObject)_report.ReportDefinition.Sections["Section4"].ReportObjects["Text6"];///cash
                        ChangeHeader.Text = resourceManager.GetString("Change");
                        CashHeader.Text = resourceManager.GetString("Cash");

                        #region
                        //Date: 12-04-2019
                        ///Added by saroj for english translation for cash mode
                        TextObject txtCashEng = (TextObject)_report.ReportDefinition.Sections["Section4"].ReportObjects["txtCashEng"];
                        txtCashEng.Text = "Cash";

                        TextObject txtChangeEng = (TextObject)_report.ReportDefinition.Sections["Section4"].ReportObjects["txtChangeEng"];
                        txtChangeEng.Text = "Change";

                        #endregion  
                    }

                    TextObject AmountHeader = (TextObject)_report.ReportDefinition.Sections["Section4"].ReportObjects["Text3"];
                    productNameHeader = resourceManager.GetString("Product Name");
                    strQTY = resourceManager.GetString(strQTY);
                    strPrice = resourceManager.GetString(strPrice);
                    strDisc = resourceManager.GetString(strDisc);
                    strTotal = resourceManager.GetString(strTotal);
                    DiscountHeader.Text = resourceManager.GetString("Discount");
                    SubtotalHeader.Text = resourceManager.GetString("Sub Total");
                    AmountHeader.Text = resourceManager.GetString("Amount Due");

                    if (resourceManager.GetString(strName) != null)
                    {
                        storename.Text = resourceManager.GetString(strName);
                    }
                    else
                    {
                        storename.Text = "";
                    }

                    if (resourceManager.GetString(store.BillReceiptTitle) != null)
                    {
                        txtTtile.Text = resourceManager.GetString(store.BillReceiptTitle);
                    }
                    else
                    {
                        txtTtile.Text = "";// store.BillReceiptTitle;
                    }

                    if (resourceManager.GetString("No Refunds") != null)
                    {
                        RefundHeader.Text = resourceManager.GetString("No Refunds");
                    }
                    else
                    {
                        RefundHeader.Text = "No Refunds!";
                    }
                    string arabicAddress = "";
                    if (store.Address.ToLower().Contains("the dubai mall"))
                    {
                        if (resourceManager.GetString("the dubai mall") != null)
                        {
                            arabicAddress = resourceManager.GetString("the dubai mall");
                        }
                        store.Address = store.Address.Replace("The Dubai Mall", "");
                        store.Address = arabicAddress + "    " + store.Address;
                        store.Address = store.Address.Replace("THE DUBAI MALL", "");
                        txtAddress.Text = store.Address;
                    }
                    else
                    {
                        txtAddress.Text = "";
                    }

                    if (resourceManager.GetString("Thank You") != null)
                    {
                        ThankYouHeader.Text = resourceManager.GetString("Thank You");
                    }
                    else
                    {
                        ThankYouHeader.Text = "Thank You";
                    }
                    TextObject txtIncludedText;
                    txtIncludedText = (TextObject)_report.ReportDefinition.Sections["Section4"].ReportObjects["txtIncluded"];
                    if (resourceManager.GetString("Included") != null)
                    {
                        txtIncludedText.Text = resourceManager.GetString("Included");
                    }
                    else
                    {
                        txtIncludedText.Text = "Included";
                    }
                    TextObject txtTaxValPercText;
                    txtTaxValPercText = (TextObject)_report.ReportDefinition.Sections["Section4"].ReportObjects["txtTaxPerc"];
                    if (taxList.Count > 0)
                    {
                        if (!string.IsNullOrEmpty(taxList[0].TaxPercentage.ToString()))
                        {
                            txtTaxValPercText.Text = string.Format("{0:F2}", decimal.Parse(taxList[0].TaxPercentage.ToString())) + "%";
                        }
                    }

                    TextObject txtAEDText;
                    txtAEDText = (TextObject)_report.ReportDefinition.Sections["Section4"].ReportObjects["txtAED"];
                    if (resourceManager.GetString("VATAED") != null)
                    {
                        txtAEDText.Text = resourceManager.GetString("VATAED");
                    }
                    else
                    {
                        txtAEDText.Text = "VAT (AED)";
                    }

                    TextObject txtTaxValueObj;
                    txtTaxValueObj = (TextObject)_report.ReportDefinition.Sections["Section4"].ReportObjects["txtTaxValue"];
                    if (taxList.Count > 0)
                    {
                        txtTaxValueObj.Text = taxList[0].TaxAmount.ToString();
                    }

                }
                ////end by latika

                if (IsGSTEnabled == true)
                {
                    foreach (var items in itemDetails)
                    {
                        //Double itemDisc = 0;
                        string discount = "0";
                        if (items.Discount != 0.0)
                        {
                            //itemDisc = Convert.ToDouble(items.Productprice) / Convert.ToDouble(items.Discount);
                            //discount = Convert.ToString(Math.Round((Math.Round(Convert.ToDouble(items.Productprice) / 1.18, 2) * (itemDisc)) / 100, 2));

                            discount = Convert.ToString(Convert.ToDouble(Math.Round(items.Discount, 2)));
                        }

                        if (Convert.ToDouble(discount) <= 0)
                        {
                            discount = "0.00";
                        }

                        if (_imagedetails == string.Empty)
                        {
                            _imagedetails = "<LineItems><Item><Name>" + items.Productname + "</Name><Qty>" + items.Productquantity + "</Qty><Price>" + Convert.ToString(Math.Round(Convert.ToDouble(items.Productprice), 2)) + "</Price><Disc>" + discount + "</Disc></Item></LineItems>";

                        }
                        else
                        {
                            _imagedetails += "<Item><Name>" + items.Productname + "</Name><Qty>" + items.Productquantity + "</Qty><Price>" + Convert.ToString(Math.Round(Convert.ToDouble(items.Productprice), 2)) + "</Price><Disc>" + discount + "</Disc></Item></LineItems>";
                        }
                    }
                }
                else
                {
                    //Culture for arabic convert using resource file mahesh patel
                    //ResourceManager resourceManager = ResourceCulture.GetCulture();
                    ////int qrcodeCount = 0; //online QRcode Count
                    int itemCount = 0;
                    foreach (var items in itemDetails)
                    {

                        // Added by Anisur Rahman for Dubai frame (Product code + Barcode) receipt requirement
                        string pCode = string.Empty;
                        if (!string.IsNullOrEmpty(isProductCodeWithBarCode))
                        {
                            if (isProductCodeWithBarCode.ToLower() == "true")
                            {
                                applicationPathPcode = @"C:\Program Files (x86)\iMix\";
                                string filepathdate = System.IO.Path.Combine(applicationPathPcode, "Barcode");
                                if (!Directory.Exists(filepathdate))
                                    Directory.CreateDirectory(filepathdate);
                                string file = "myimg.jpg";
                                pCode = items.Productcode;
                                string pBarcodeId = pBarcodeId = pCode.ToUpper().Replace(productExtentionCode.ToUpper(), "");
                                string productBarCode = pBarcodeId.Replace("-", "");
                                BarCodeGenerator.Code39 pbaracode = new BarCodeGenerator.Code39(productBarCode);
                                imgpCode = new Bitmap(pbaracode.Paint());
                                imgPath = System.IO.Path.Combine(applicationPathPcode, "Barcode\\" + pCode + System.IO.Path.GetExtension(file));
                                try
                                {
                                    imgpCode.Save(imgPath);
                                }
                                catch (Exception ex)
                                {
                                    imgpCode.Dispose();
                                    GC.Collect();
                                    imgpCode = new Bitmap(baracode.Paint());
                                }
                            }
                        }
                        // End Here 

                        TotalValue += Convert.ToDecimal(items.Productprice);//-------MONIKA

                        if (resourceManager != null)
                        {
                            items.ArabicName = resourceManager.GetString(items.Productname);
                        }
                        //if (qrCodePrint != null || qrCodePrint == ":")

                        if (items.QRCode != null)
                        {
                            //commented By Nilesh ---------change the logic of getting QR code----20 Aug 2018---------

                            itemCount++;
                            if (_imagedetails == string.Empty)
                            {
                                /////made changes by latika for translation 
                                _imagedetails = "<LineItems><Item><ProductNamelbl>" + productNameHeader + "</ProductNamelbl><Name>" + items.Productname + "</Name><ArabicName>" + items.ArabicName + "</ArabicName><ClaimCode>Claim Code</ClaimCode><QRCode>" + items.QRCode + "</QRCode><Qtylbl>" + strQTY + "</Qtylbl><Qty>" + items.Productquantity + "</Qty><Pricelbl>" + strPrice + "</Pricelbl><Price>" + items.Productprice + "</Price><Currency>" + currency + "</Currency><Disclbl>" + strDisc + "</Disclbl><Disc>" + items.Discount.ToString("#0.00") + "</Disc><ProductCode>" + pCode + "</ProductCode></Item>";//old using parameter qrcode                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             
                                //_imagedetails = "<LineItems><Item><ProductNamelbl>Product Name</ProductNamelbl><Name>" + items.Productname + "</Name><ArabicName>" + items.ArabicName + "</ArabicName><ClaimCode>Claim Code</ClaimCode><QRCode>" + items.QRCode + "</QRCode><Qtylbl>Qty</Qtylbl><Qty>" + items.Productquantity + "</Qty><Pricelbl>Price</Pricelbl><Price>" + items.Productprice + "</Price><Currency>" + currency + "</Currency><Disclbl>Disc</Disclbl><Disc>" + items.Discount.ToString("#0.00") + "</Disc></Item>";//old using parameter qrcode                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             
                            }
                            else
                            {
                                /////made changes by latika for translation 
                                _imagedetails += "<Item><ProductNamelbl>" + productNameHeader + "</ProductNamelbl><Name>" + items.Productname + "</Name><ArabicName>" + items.ArabicName + "</ArabicName><ClaimCode>Claim Code</ClaimCode><QRCode>" + items.QRCode + "</QRCode><Qtylbl>" + strQTY + "</Qtylbl><Qty>" + items.Productquantity + "</Qty><Pricelbl>" + strPrice + "</Pricelbl><Price>" + items.Productprice + "</Price><Currency>" + currency + "</Currency><Disclbl>" + strDisc + "</Disclbl><Disc>" + items.Discount.ToString("#0.00") + "</Disc><ProductCode>" + pCode + "</ProductCode></Item>"; //old using parameter qrcode                                                
                                //_imagedetails += "<Item><ProductNamelbl>Product Name</ProductNamelbl><Name>" + items.Productname + "</Name><ArabicName>" + items.ArabicName + "</ArabicName><ClaimCode>Claim Code</ClaimCode><QRCode>" + items.QRCode + "</QRCode><Qtylbl>Qty</Qtylbl><Qty>" + items.Productquantity + "</Qty><Pricelbl>Price</Pricelbl><Price>" + items.Productprice + "</Price><Currency>" + currency + "</Currency><Disclbl>Disc</Disclbl><Disc>" + items.Discount.ToString("#0.00") + "</Disc></Item>"; //old using parameter qrcode                                                
                            }

                            //-----Start-----------For Old Reciept for Online i have set the Claim code----------Nilesh  5th Sep 2018--------------
                            if (rptArabic == "0") ////Removed && !isDeliveryNote from condition, display QR code in delivery note receipt added by Manoj at 19-Feb-20
                            {
                                TextObject ClaimCodelbl = (TextObject)_report.ReportDefinition.Sections["Section2"].ReportObjects["ClaimCodeText"];
                                ClaimCodelbl.Text = "Claim Code";

                                TextObject qrCodeObejct = (TextObject)_report.ReportDefinition.Sections["Section2"].ReportObjects["txtClaimCodeQR"];
                                if (itemCount <= 1)
                                {
                                    //modified by nilesh.
                                    if (string.IsNullOrEmpty(qrCodeObejct.Text) || qrCodeObejct.Text.ToUpper() != items.QRCode.ToUpper())
                                    {
                                        qrCodeObejct.Text = items.QRCode.ToUpper();
                                    }
                                }
                                else
                                {
                                    if (string.IsNullOrEmpty(qrCodeObejct.Text) || qrCodeObejct.Text.ToUpper() != items.QRCode.ToUpper())
                                    {
                                        qrCodeObejct.Text = qrCodeObejct.Text + "," + items.QRCode.ToUpper();
                                    }
                                }
                            }
                            //-----End-----------For Old Reciept for Online i have set the Claim code----------Nilesh  5th Sep 2018--------------

                        }
                        else
                        {
                            if (_imagedetails == string.Empty)
                            {/////made changes by latika for translation 
                             //   _imagedetails = "<LineItems><Item><ProductNamelbl>" + productNameHeader + "</ProductNamelbl><Name>" + items.Productname + "</Name><ArabicName>" + items.ArabicName + "</ArabicName><ClaimCode>Claim Code</ClaimCode><QRCode>" + items.QRCode + "</QRCode><Qtylbl>" + strQTY + "</Qtylbl><Qty>" + items.Productquantity + "</Qty><Pricelbl>" + strPrice + "</Pricelbl><Price>" + items.Productprice + "</Price><Currency>" + currency + "</Currency><Disclbl>" + strDisc + "</Disclbl><Disc>" + items.Discount.ToString("#0.00") + "</Disc></Item>";//old using parameter qrcode                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             
                                _imagedetails = "<LineItems><Item><ProductNamelblEnglish> +Product Name +</ProductNamelblEnglish><ProductNamelbl>" + productNameHeader + "</ProductNamelbl><Name>" + items.Productname + " </Name><ArabicName>" + items.ArabicName + "</ArabicName><ClaimCode>Claim Code</ClaimCode><QRCode>" + items.QRCode + "</QRCode><Qtylbl>" + strQTY + "</Qtylbl><Qty>" + items.Productquantity + "</Qty><Pricelbl>" + strPrice + "</Pricelbl><Price>" + items.Productprice + "</Price><Currency>" + currency + "</Currency><Disclbl>" + strDisc + "</Disclbl><Disc>" + items.Discount.ToString("#0.00") + "</Disc><ProductCode>" + pCode + "</ProductCode></Item>";//old using parameter qrcode                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             

                            }
                            else
                            {/////made changes by latika for translation 
                                _imagedetails += "<Item><ProductNamelbl>" + productNameHeader + "</ProductNamelbl><Name>" + items.Productname + "</Name><ArabicName>" + items.ArabicName + "</ArabicName><ClaimCode>Claim Code</ClaimCode><QRCode>" + items.QRCode + "</QRCode><Qtylbl>" + strQTY + "</Qtylbl><Qty>" + items.Productquantity + "</Qty><Pricelbl>" + strPrice + "</Pricelbl><Price>" + items.Productprice + "</Price><Currency>" + currency + "</Currency><Disclbl>" + strDisc + "</Disclbl><Disc>" + items.Discount.ToString("#0.00") + "</Disc><ProductCode>" + pCode + "</ProductCode></Item>"; //old using parameter qrcode                                                
                            }
                            if (items.QRCode == null)
                            {
                                _imagedetails = _imagedetails.Replace(@"<ClaimCode>Claim Code</ClaimCode><QRCode></QRCode>", "");
                            }
                        }
                        if (items.QRCode == "")
                        {
                            _imagedetails = _imagedetails.Replace(@"<ClaimCode>Claim Code</ClaimCode><QRCode></QRCode>", "");
                        }
                    }
                    _imagedetails += "<TotalVal> " + TotalValue + " </TotalVal></LineItems>";//--------MONIKA
                }
                /// ------START---- added by MONIKA
                TextObject totalvalue = (TextObject)_report.ReportDefinition.Sections["Section4"].ReportObjects["txtTotaln"];
                totalvalue.Text = TotalValue.ToString();

                TextObject total_PerItemDisc = (TextObject)_report.ReportDefinition.Sections["Section4"].ReportObjects["txtDiscTotal"];
                total_PerItemDisc.Text = (totalDiscount - discountOnTotal).ToString();

                TextObject total_Disc = (TextObject)_report.ReportDefinition.Sections["Section4"].ReportObjects["txtDiscount"];
                total_Disc.Text = totalDiscount.ToString();

                TextObject CGST_tax = (TextObject)_report.ReportDefinition.Sections["Section4"].ReportObjects["lblCGST"];
                CGST_tax.Text = "CGST @" + tax_result.ToString() + "%";

                TextObject SGST_tax = (TextObject)_report.ReportDefinition.Sections["Section4"].ReportObjects["lblSGST"];
                SGST_tax.Text = "SGST @" + tax_result.ToString() + "%";

                //////------- END
                ///

                DataTable dt = GetDataTablefromStream(_imagedetails);
                //result = objDigiPhotoDataServices.GetCashBillSummary(Imagedetails);
                if (!string.IsNullOrEmpty(logoImgName) && System.IO.File.Exists(logoImgName))
                    _report.DataDefinition.FormulaFields[1].Text = "'" + logoImgName + "'";

                //if (IsWeChat)
                //{
                //    //Assign Qr code image
                //    string filepath = Common.LoginUser.DigiFolderPath.Replace("\\\\", "\\");
                //    string QRCodeImgPath = System.IO.Path.Combine("\\" + filepath, "OrderReceipt\\" + orderNo + ".jpg");
                //    QRCodeImgPath = System.IO.Path.Combine(Common.LoginUser.DigiFolderPath, "OrderReceipt\\QRCode.jpg");
                //    //QRCodeImgPath = @"\\Imixdatabase11\DigiImages\OrderReceipt\QRCode.jpg";
                //    // _report.DataDefinition.FormulaFields[0].Text = "'" + @"\\Imixdatabase11\DigiImages\OrderReceipt\DG-1440444636.jpg" + "'"; //"'" + QRCodeImgPath + "'";                    
                //    _report.DataDefinition.FormulaFields["testpath"].Text = "'" + QRCodeImgPath + "'"; //"'" + QRCodeImgPath + "'";                    
                //    //_report.DataDefinition.FormulaFields[0].Text = "'" + "\\" + QRCodeImgPath + "'";                    
                //}
                if (IsWeChat)
                {
                    //Assign Qr code image
                    string filepath = Common.LoginUser.DigiFolderPath.Replace("\\\\", "\\");
                    string QRCodeImgPath = System.IO.Path.Combine("\\" + filepath, "OrderReceipt\\" + orderNo + ".jpg");
                    if (Common.LoginUser.DigiFolderPath.Contains("\\\\\\\\"))
                    {
                        EventLog.WriteEntry("TestBill-HotFolder", Common.LoginUser.DigiFolderPath);
                        QRCodeImgPath = System.IO.Path.Combine(Common.LoginUser.DigiFolderPath.Replace("\\\\", "\\"), "OrderReceipt\\QRCode.jpg");
                    }
                    else
                    {
                        QRCodeImgPath = System.IO.Path.Combine(Common.LoginUser.DigiFolderPath, "OrderReceipt\\QRCode.jpg");
                    }

                    //QRCodeImgPath = @"\\Imixdatabase11\DigiImages\OrderReceipt\QRCode.jpg";
                    // _report.DataDefinition.FormulaFields[0].Text = "'" + @"\\Imixdatabase11\DigiImages\OrderReceipt\DG-1440444636.jpg" + "'"; //"'" + QRCodeImgPath + "'";                    
                    _report.DataDefinition.FormulaFields["testpath"].Text = "'" + QRCodeImgPath + "'"; //"'" + QRCodeImgPath + "'";                    
                    //_report.DataDefinition.FormulaFields[0].Text = "'" + "\\" + QRCodeImgPath + "'";                    
                }
                else
                {
                    _report.DataDefinition.FormulaFields[0].Text = "'" + Environment.CurrentDirectory + "\\Reports\\myimg.jpg" + "'";
                }
                _report.SetDataSource(dt);
                //if (store.IsTaxEnabled)
                if (rptArabic == "0")
                {
                    _report.Subreports[0].SetDataSource(taxList);
                }
                _report.Refresh();
                _report.PrintOptions.PrinterName = Common.LoginUser.ReceiptPrinterPath;

                // For Single Single Print
                int noOfRecipt = GetNoOfBillReceipt();


                for (int copy = 0; copy < noOfRecipt; copy++)
                {
                    TextObject BillType = (TextObject)_report.ReportDefinition.Sections["Section1"].ReportObjects["txtBillType"];

                    if (copy == 0)
                    {
                        BillType.Text = "Original";
                    }
                    else
                    {
                        BillType.Text = "Duplicate";
                    }
                    CrystalDecisions.Shared.PageMargins margin = new CrystalDecisions.Shared.PageMargins(); ///changed by latika 20191019 for duplicate print mergine issue
                    if (IsReprint == true)
                    {  ///changed by latika 20191019 for duplicate print mergine issue
                        margin.leftMargin = 0;
                        margin.rightMargin = 0;
                        margin.topMargin = 0;
                        _report.PrintOptions.ApplyPageMargins(margin);
                        ////end
                        BillType.Text = "Duplicate";
                        _report.PrintToPrinter(1, true, 1, 0);
                        return;
                    }

                    margin.leftMargin = 0;
                    margin.rightMargin = 0;
                    margin.topMargin = 0;
                    //Added condition for arabic translation
                    if (rptArabic != "0")
                    {
                        margin.bottomMargin = 0;
                    }
                    else
                    {
                        margin.bottomMargin = 292;
                    }
                    //margin.bottomMargin = 0;
                    _report.PrintOptions.ApplyPageMargins(margin);
                    _report.PrintToPrinter(1, true, 1, 0);
                }
                if (!IsReprint)
                {
                    SaveOrderNo(orderNo);
                    //Save receipt into File.
                    string filepath = Common.LoginUser.DigiFolderPath;
                    string filepathdate = System.IO.Path.Combine(filepath, "OrderReceipt\\" + DateTime.Now.ToString("yyyyMMdd"));
                    if (!Directory.Exists(filepathdate))
                        Directory.CreateDirectory(filepathdate);
                    //KCB on 07 FEB 2018 for Saving report file name with order number and invoice number  
                    //string filename = orderNo +".pdf";
                    string filename = string.Empty;
                    if (!string.IsNullOrEmpty(txtSeqNo.Text))
                    {
                        string[] InvoiceList = txtSeqNo.Text.Split(':');
                        filename = orderNo + "_" + InvoiceList[1] + ".pdf";
                    }
                    else
                        filename = orderNo + ".pdf";
                    //END
                    _report.ExportToDisk(ExportFormatType.PortableDocFormat, filename);
                    string sourceFile = Environment.CurrentDirectory + "\\" + filename;
                    if (File.Exists(sourceFile))
                    {
                        File.Move(sourceFile, filepathdate + "\\" + filename);
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
            }
            finally
            {
                if (img != null)
                {
                    IntPtr deleteObject = img.GetHbitmap();
                    DeleteObject(deleteObject);
                    img.Dispose();
                }
                // Added by Anisur Rahman for Dubai frame (Product code + Barcode) receipt requirement        
                if (imgpCode != null)
                {
                    IntPtr deleteObject = imgpCode.GetHbitmap();
                    DeleteObject(deleteObject);
                    imgpCode.Dispose();
                }
                string[] files = Directory.GetFiles(applicationPathPcode + "Barcode\\");
                {
                    if (files != null)
                    {
                        foreach (string file in files)
                        {
                            File.Delete(file);
                        }
                    }
                }
                // End Here
                baracode = null;
                BillReportviewer = null;
                _report.Dispose();
                GC.Collect(); // Code added by Latika to release the memory
            }


        }


        //public TestBill(string strName, string opName, string orderNo, string photos, string refund, string amntDue, string txtCash, string txtChange, List<LinetItemsDetails> itemDetails, string paymentType, string cardNumber, string cardHolderName, string customerName, string hotelName, string roomNo, string voucherNo, string currency, int orderId, Boolean IsReprint, double totalDiscount = 0.00, string qrCodePrint = "")
        //{
        //    InitializeComponent();
        //    double discountOnTotal = totalDiscount - itemDetails.Sum(x => x.Discount);
        //    double totalAmount = Convert.ToDouble(discountOnTotal + Double.Parse(amntDue));
        //    string barcodeId = orderNo.Replace("DG-", "");
        //    string applicationPath = AppDomain.CurrentDomain.BaseDirectory;
        //    BarCodeGenerator.Code39 baracode = new BarCodeGenerator.Code39(barcodeId);
        //    Bitmap img = new Bitmap(baracode.Paint());
        //    StoreInfo store = new StoreInfo();
        //    TaxBusiness taxBusiness = new TaxBusiness();
        //    store = taxBusiness.getTaxConfigData();
        //    GetStoreConfigData();
        //    TaxDetailInfo taxInfo = new TaxDetailInfo();
        //    //TaxBusiness TaxBusiness = new TaxBusiness();
        //    List<TaxDetailInfo> taxList = taxBusiness.GetTaxDetail(orderId);
        //    //BindTaxReport(taxList, ApplicationPath);
        //    try
        //    {
        //        img.Save(Environment.CurrentDirectory + "\\Reports\\myimg.jpg");

        //        if (paymentType == "CARD")
        //        {
        //            if (store.IsTaxEnabled)
        //            {
        //                if (store.IsTaxIncluded)
        //                    _report.Load(applicationPath + "\\Reports\\CardBill.rpt");
        //                else
        //                    _report.Load(applicationPath + "\\Reports\\CardBillTaxExtra.rpt");
        //            }
        //            else
        //                _report.Load(applicationPath + "\\Reports\\CardBill.rpt");
        //        }
        //        else if (paymentType == "CASH")
        //        {
        //            if (store.IsTaxEnabled)
        //            {
        //                if (store.IsTaxIncluded)
        //                    _report.Load(applicationPath + "\\Reports\\CashBill.rpt");
        //                else
        //                    _report.Load(applicationPath + "\\Reports\\CashBillTaxExtra.rpt");
        //            }
        //            else
        //                _report.Load(applicationPath + "\\Reports\\CashBill.rpt");

        //        }
        //        else if (paymentType == "ROOM")
        //        {
        //            if (store.IsTaxEnabled)
        //            {
        //                if (store.IsTaxIncluded)
        //                    _report.Load(applicationPath + "\\Reports\\RoomBill.rpt");
        //                else
        //                    _report.Load(applicationPath + "\\Reports\\RoomBillTaxExtra.rpt");
        //            }
        //            else
        //                _report.Load(applicationPath + "\\Reports\\RoomBill.rpt");

        //        }
        //        else if (paymentType == "VOUCHER")
        //        {
        //            if (store.IsTaxEnabled)
        //            {
        //                if (store.IsTaxIncluded)
        //                    _report.Load(applicationPath + "\\Reports\\VoucherBill.rpt");
        //                else
        //                    _report.Load(applicationPath + "\\Reports\\VoucherBillTaxExtra.rpt");
        //            }
        //            else
        //                _report.Load(applicationPath + "\\Reports\\VoucherBill.rpt");

        //        }
        //        else if (paymentType == "KVL")
        //        {
        //            if (store.IsTaxEnabled)
        //            {
        //                if (store.IsTaxIncluded)
        //                    _report.Load(applicationPath + "\\Reports\\KVLBill.rpt");
        //                else
        //                    _report.Load(applicationPath + "\\Reports\\KVLBillTaxExtra.rpt");
        //            }
        //            else
        //                _report.Load(applicationPath + "\\Reports\\KVLBill.rpt");

        //        }



        //        //This is for ALL reports
        //        TextObject storename = (TextObject)_report.ReportDefinition.Sections["Section1"].ReportObjects["Txtstorename"];
        //        storename.Text = strName;

        //        TextObject txtTtile = (TextObject)_report.ReportDefinition.Sections["Section1"].ReportObjects["txtReceiptTitle"];
        //        txtTtile.Text = store.BillReceiptTitle;

        //        TextObject txtSeqNo = (TextObject)_report.ReportDefinition.Sections["Section1"].ReportObjects["txtSeqNo"];
        //        txtSeqNo.Text = (store.IsSequenceNoRequired == true ? "Invoice Number: " + (new OrderBusiness()).GetOrderInvoiceNumber(orderId) : string.Empty);

        //        TextObject txtAddress = (TextObject)_report.ReportDefinition.Sections["Section1"].ReportObjects["txtAddress"];
        //        txtAddress.Text = "" + store.Address.Replace("\n", " ").Replace("\r", "");

        //        TextObject txtPhone = (TextObject)_report.ReportDefinition.Sections["Section1"].ReportObjects["txtPhone"];
        //        txtPhone.Text = "Phone: " + store.PhoneNo;

        //        TextObject txtEmail = (TextObject)_report.ReportDefinition.Sections["Section1"].ReportObjects["txtEmail"];
        //        txtEmail.Text = "Email: " + store.EmailID;

        //        TextObject txtUrl = (TextObject)_report.ReportDefinition.Sections["Section1"].ReportObjects["txtUrl"];
        //        txtUrl.Text = store.WebsiteURL;

        //        TextObject txtRegistrationNo = (TextObject)_report.ReportDefinition.Sections["Section1"].ReportObjects["txtRegistrationNo"];
        //        txtRegistrationNo.Text = store.TaxRegistrationText + (!string.IsNullOrEmpty(store.TaxRegistrationText) ? ": " : string.Empty) + store.TaxRegistrationNumber;

        //        TextObject op = (TextObject)_report.ReportDefinition.Sections["Section2"].ReportObjects["txtoperator"];
        //        op.Text = opName;

        //        TextObject order = (TextObject)_report.ReportDefinition.Sections["Section2"].ReportObjects["txtorderno"];
        //        order.Text = orderNo;

        //        TextObject photono = (TextObject)_report.ReportDefinition.Sections["Section2"].ReportObjects["textphotono"];
        //        photono.Text = "Photo No: " + photos;
        //        photono.Height = 192 * (photono.Text.Length / 45);

        //        TextObject refundtxt = (TextObject)_report.ReportDefinition.Sections["Section4"].ReportObjects["txtrefund"];
        //        refundtxt.Text = refund;

        //        TextObject amountdue = (TextObject)_report.ReportDefinition.Sections["Section4"].ReportObjects["txtamountdue"];
        //        amountdue.Text = amntDue;

        //        TextObject txtCurrency = (TextObject)_report.ReportDefinition.Sections["Section2"].ReportObjects["txtCurrency"];
        //        txtCurrency.Text = " (" + currency + ")";

        //        if (ConfigurationManager.AppSettings["IsGSTEnabled"].ToLower() == "true")
        //        {
        //            //Add logic of calculation GST amount
        //            amntDue = Convert.ToString(Math.Round(Convert.ToDouble(amntDue) / 1.18, 2));

        //            //Add CGST text value
        //            TextObject amountCGST = (TextObject)_report.ReportDefinition.Sections["Section4"].ReportObjects["txtCGST"];
        //            amountCGST.ObjectFormat.EnableSuppress = false;
        //            amountCGST.Text = Convert.ToString(Math.Round(Convert.ToDouble(amntDue) * 0.09, 2));


        //            TextObject amountSGST = (TextObject)_report.ReportDefinition.Sections["Section4"].ReportObjects["txtSGST"];
        //            amountSGST.ObjectFormat.EnableSuppress = false;
        //            amountSGST.Text = Convert.ToString(Math.Round(Convert.ToDouble(amntDue) * 0.09, 2));

        //            TextObject txtCGST = (TextObject)_report.ReportDefinition.Sections["Section4"].ReportObjects["lblCGST"];
        //            txtCGST.ObjectFormat.EnableSuppress = false;

        //            TextObject txtSGST = (TextObject)_report.ReportDefinition.Sections["Section4"].ReportObjects["lblSGST"];
        //            txtSGST.ObjectFormat.EnableSuppress = false;

        //        }
        //        else
        //        {
        //            //TextObject amountCGST = (TextObject)_report.ReportDefinition.Sections["Section4"].ReportObjects["txtCGST"];
        //            //amountCGST.ObjectFormat.EnableSuppress = true;
        //            //amountCGST.Text = string.Empty;

        //            //TextObject amountSGST = (TextObject)_report.ReportDefinition.Sections["Section4"].ReportObjects["txtSGST"];
        //            //amountSGST.ObjectFormat.EnableSuppress = true;
        //            //amountSGST.Text = string.Empty;

        //            //TextObject txtCGST = (TextObject)_report.ReportDefinition.Sections["Section4"].ReportObjects["lblCGST"];
        //            //txtCGST.ObjectFormat.EnableSuppress = true;

        //            //TextObject txtSGST = (TextObject)_report.ReportDefinition.Sections["Section4"].ReportObjects["lblSGST"];
        //            //txtSGST.ObjectFormat.EnableSuppress = true;
        //        }




        //        //This is only for CASH
        //        if (paymentType == "CASH")
        //        {
        //            TextObject subtotal = (TextObject)_report.ReportDefinition.Sections["Section4"].ReportObjects["txtsubtotal"];
        //            //subtotal.Text = (Double.Parse(amntDue) + discountOnTotal).ToString("#0.00"); //Previous code
        //            subtotal.Text = Convert.ToString(Double.Parse(amntDue));

        //            if (ConfigurationManager.AppSettings["IsGSTEnabled"].ToLower() == "true")
        //            {
        //                TextObject discount = (TextObject)_report.ReportDefinition.Sections["Section4"].ReportObjects["txtDiscount"];
        //                //discount.Text = discountOnTotal.ToString("#0.00"); // old code
        //                Double discountPer = Convert.ToDouble((discountOnTotal / totalAmount) * 100);
        //                Double totdeductedTax = totalAmount / 1.18;
        //                discount.Text = Convert.ToString(Math.Round((totdeductedTax * discountPer) / 100, 2));
        //            }
        //            else
        //            {
        //                TextObject discount = (TextObject)_report.ReportDefinition.Sections["Section4"].ReportObjects["txtDiscount"];
        //                discount.Text = discountOnTotal.ToString("#0.00");
        //            }

        //            TextObject cash = (TextObject)_report.ReportDefinition.Sections["Section4"].ReportObjects["txtcash"];
        //            cash.Text = txtCash;
        //            TextObject change = (TextObject)_report.ReportDefinition.Sections["Section4"].ReportObjects["txtchange"];
        //            change.Text = txtChange;
        //        }
        //        //This is for CARD,ROOM & VOUCHER
        //        if (paymentType == "CARD" || paymentType == "ROOM" || paymentType == "VOUCHER" || paymentType == "KVL")
        //        {
        //            TextObject subtotal = (TextObject)_report.ReportDefinition.Sections["Section4"].ReportObjects["txtsubtotal"];
        //            subtotal.Text = (Double.Parse(amntDue) + discountOnTotal).ToString("#0.00");


        //            if (ConfigurationManager.AppSettings["IsGSTEnabled"].ToLower() == "true")
        //            {
        //                TextObject discount = (TextObject)_report.ReportDefinition.Sections["Section4"].ReportObjects["txtDiscount"];
        //                //discount.Text = discountOnTotal.ToString("#0.00"); // old code
        //                Double discountPer = Convert.ToDouble((discountOnTotal / totalAmount) * 100);
        //                Double totdeductedTax = totalAmount / 1.18;
        //                discount.Text = Convert.ToString(Math.Round((totdeductedTax * discountPer) / 100, 2));
        //            }
        //            else
        //            {
        //                TextObject discount = (TextObject)_report.ReportDefinition.Sections["Section4"].ReportObjects["txtDiscount"];
        //                discount.Text = discountOnTotal.ToString("#0.00");
        //            }
        //        }

        //        //This is only for CARD
        //        if (paymentType == "CARD")
        //        {
        //            TextObject cardno = (TextObject)_report.ReportDefinition.Sections["Section4"].ReportObjects["txtCardno"];
        //            cardno.Text = "XXXX XXXX XXXX " + cardNumber;

        //            TextObject cardholdername = (TextObject)_report.ReportDefinition.Sections["Section4"].ReportObjects["txtcustomername"];
        //            cardholdername.Text = cardHolderName;
        //        }

        //        //This is only for ROOM
        //        if (paymentType == "ROOM")
        //        {
        //            TextObject name = (TextObject)_report.ReportDefinition.Sections["Section4"].ReportObjects["txtname"];
        //            name.Text = customerName;

        //            TextObject hotelname = (TextObject)_report.ReportDefinition.Sections["Section4"].ReportObjects["txthotel"];
        //            hotelname.Text = hotelName;

        //            TextObject roomno = (TextObject)_report.ReportDefinition.Sections["Section4"].ReportObjects["txtroomno"];
        //            roomno.Text = roomNo;
        //        }

        //        //This is only for VOUCHER
        //        if (paymentType == "VOUCHER")
        //        {
        //            TextObject name = (TextObject)_report.ReportDefinition.Sections["Section4"].ReportObjects["txtname"];
        //            name.Text = customerName;

        //            TextObject voucherno = (TextObject)_report.ReportDefinition.Sections["Section4"].ReportObjects["txtvoucherno"];
        //            voucherno.Text = voucherNo;
        //        }

        //        if (ConfigurationManager.AppSettings["IsGSTEnabled"].ToLower() == "true")
        //        {
        //            foreach (var items in itemDetails)
        //            {
        //                Double itemDisc = 0;
        //                string discount = "0";
        //                if (items.Discount != 0.0)
        //                {
        //                    itemDisc = Convert.ToDouble(items.Productprice) / Convert.ToDouble(items.Discount);
        //                    discount = Convert.ToString(Math.Round((Math.Round(Convert.ToDouble(items.Productprice) / 1.18, 2) * (itemDisc)) / 100, 2));
        //                }

        //                if (Convert.ToDouble(discount) <= 0)
        //                {
        //                    discount = "0.00";
        //                }

        //                if (_imagedetails == string.Empty)
        //                {
        //                    _imagedetails = "<LineItems><Item><Name>" + items.Productname + "</Name><Qty>" + items.Productquantity + "</Qty><Price>" + Convert.ToString(Math.Round(Convert.ToDouble(items.Productprice) / 1.18, 2)) + "</Price><Disc>" + discount + "</Disc></Item>";

        //                }
        //                else
        //                {
        //                    _imagedetails += "<Item><Name>" + items.Productname + "</Name><Qty>" + items.Productquantity + "</Qty><Price>" + Convert.ToString(Math.Round(Convert.ToDouble(items.Productprice) / 1.18, 2)) + "</Price><Disc>" + discount + "</Disc></Item>";
        //                }
        //            }
        //        }
        //        else
        //        {
        //            //Culture for arabic convert using resource file mahesh patel
        //            ResourceManager resourceManager = ResourceCulture.GetCulture();
        //            int qrcodeCount = 0; //online QRcode Count
        //            foreach (var items in itemDetails)
        //            {
        //                if (resourceManager != null)
        //                {
        //                    items.ArabicName = resourceManager.GetString(items.Productname);
        //                }
        //                if (qrCodePrint != null || qrCodePrint == ":")
        //                {
        //                    if (qrCodePrint.Contains(":"))
        //                    {
        //                        string[] OnlineQrcodeSplit = qrCodePrint.Split(':');
        //                        items.QRCode = OnlineQrcodeSplit[qrcodeCount];
        //                        qrcodeCount++;
        //                    }
        //                    else
        //                    {
        //                        items.QRCode = qrCodePrint;
        //                    }
        //                    if (_imagedetails == string.Empty)
        //                    {
        //                        _imagedetails = "<LineItems><Item><ProductNamelbl>Product Name</ProductNamelbl><Name>" + items.Productname + "</Name><ArabicName>" + items.ArabicName + "</ArabicName><ClaimCode>Claim Code</ClaimCode><QRCode>" + items.QRCode + "</QRCode><Qtylbl>Qty</Qtylbl><Qty>" + items.Productquantity + "</Qty><Pricelbl>Price</Pricelbl><Price>" + items.Productprice + "</Price><Currency>" + currency + "</Currency><Disclbl>Disc</Disclbl><Disc>" + items.Discount.ToString("#0.00") + "</Disc></Item>";//old using parameter qrcode                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             
        //                    }
        //                    else
        //                    {
        //                        _imagedetails += "<Item><ProductNamelbl>Product Name</ProductNamelbl><Name>" + items.Productname + "</Name><ArabicName>" + items.ArabicName + "</ArabicName><ClaimCode>Claim Code</ClaimCode><QRCode>" + items.QRCode + "</QRCode><Qtylbl>Qty</Qtylbl><Qty>" + items.Productquantity + "</Qty><Pricelbl>Price</Pricelbl><Price>" + items.Productprice + "</Price><Currency>" + currency + "</Currency><Disclbl>Disc</Disclbl><Disc>" + items.Discount.ToString("#0.00") + "</Disc></Item>"; //old using parameter qrcode                                                
        //                    }
        //                }
        //                else
        //                {
        //                    if (_imagedetails == string.Empty)
        //                    {
        //                        _imagedetails = "<LineItems><Item><ProductNamelbl>Product Name</ProductNamelbl><Name>" + items.Productname + "</Name><ArabicName>" + items.ArabicName + "</ArabicName><ClaimCode>Claim Code</ClaimCode><QRCode>" + items.QRCode + "</QRCode><Qtylbl>Qty</Qtylbl><Qty>" + items.Productquantity + "</Qty><Pricelbl>Price</Pricelbl><Price>" + items.Productprice + "</Price><Currency>" + currency + "</Currency><Disclbl>Disc</Disclbl><Disc>" + items.Discount.ToString("#0.00") + "</Disc></Item>";//old using parameter qrcode                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             
        //                    }
        //                    else
        //                    {
        //                        _imagedetails += "<Item><ProductNamelbl>Product Name</ProductNamelbl><Name>" + items.Productname + "</Name><ArabicName>" + items.ArabicName + "</ArabicName><ClaimCode>Claim Code</ClaimCode><QRCode>" + items.QRCode + "</QRCode><Qtylbl>Qty</Qtylbl><Qty>" + items.Productquantity + "</Qty><Pricelbl>Price</Pricelbl><Price>" + items.Productprice + "</Price><Currency>" + currency + "</Currency><Disclbl>Disc</Disclbl><Disc>" + items.Discount.ToString("#0.00") + "</Disc></Item>"; //old using parameter qrcode                                                
        //                    }
        //                    if (items.QRCode == null)
        //                    {
        //                        _imagedetails = _imagedetails.Replace(@"<ClaimCode>Claim Code</ClaimCode><QRCode></QRCode>", "");
        //                    }
        //                }
        //                if (items.QRCode == "")
        //                {
        //                    _imagedetails = _imagedetails.Replace(@"<ClaimCode>Claim Code</ClaimCode><QRCode></QRCode>", "");
        //                }
        //            }
        //            _imagedetails += "</LineItems>";
        //        }
        //        DataTable dt = GetDataTablefromStream(_imagedetails);
        //        //result = objDigiPhotoDataServices.GetCashBillSummary(Imagedetails);
        //        if (!string.IsNullOrEmpty(logoImgName) && System.IO.File.Exists(logoImgName))
        //            _report.DataDefinition.FormulaFields[1].Text = "'" + logoImgName + "'";
        //        _report.DataDefinition.FormulaFields[0].Text = "'" + Environment.CurrentDirectory + "\\Reports\\myimg.jpg" + "'";
        //        _report.SetDataSource(dt);
        //        //if (store.IsTaxEnabled)
        //        _report.Subreports[0].SetDataSource(taxList);
        //        _report.Refresh();
        //        _report.PrintOptions.PrinterName = Common.LoginUser.ReceiptPrinterPath;

        //        // For Single Single Print
        //        int noOfRecipt = GetNoOfBillReceipt();


        //        for (int copy = 0; copy < noOfRecipt; copy++)
        //        {
        //            TextObject BillType = (TextObject)_report.ReportDefinition.Sections["Section1"].ReportObjects["txtBillType"];

        //            if (copy == 0)
        //            {
        //                BillType.Text = "Original";
        //            }
        //            else
        //            {
        //                BillType.Text = "Duplicate";
        //            }
        //            if (IsReprint == true)
        //            {
        //                BillType.Text = "Duplicate";
        //                _report.PrintToPrinter(1, true, 1, 0);
        //                return;
        //            }
        //            CrystalDecisions.Shared.PageMargins margin = new CrystalDecisions.Shared.PageMargins();
        //            margin.leftMargin = 0;
        //            margin.rightMargin = 0;
        //            margin.topMargin = 0;
        //            margin.bottomMargin = 292;
        //            //margin.bottomMargin = 0;
        //            _report.PrintOptions.ApplyPageMargins(margin);
        //            _report.PrintToPrinter(1, true, 1, 0);
        //        }
        //        if (!IsReprint)
        //        {
        //            SaveOrderNo(orderNo);
        //            //Save receipt into File.
        //            string filepath = Common.LoginUser.DigiFolderPath;
        //            string filepathdate = System.IO.Path.Combine(filepath, "OrderReceipt\\" + DateTime.Now.ToString("yyyyMMdd"));
        //            if (!Directory.Exists(filepathdate))
        //                Directory.CreateDirectory(filepathdate);
        //            //KCB on 07 FEB 2018 for Saving report file name with order number and invoice number  
        //            //string filename = orderNo +".pdf";
        //            string filename = string.Empty;
        //            if (!string.IsNullOrEmpty(txtSeqNo.Text))
        //            {
        //                string[] InvoiceList = txtSeqNo.Text.Split(':');
        //                filename = orderNo + "_" + InvoiceList[1] + ".pdf";
        //            }
        //            else
        //                filename = orderNo + ".pdf";
        //            //END
        //            _report.ExportToDisk(ExportFormatType.PortableDocFormat, filename);
        //            string sourceFile = Environment.CurrentDirectory + "\\" + filename;
        //            if (File.Exists(sourceFile))
        //            {
        //                File.Move(sourceFile, filepathdate + "\\" + filename);
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        ErrorHandler.ErrorHandler.LogError(ex);
        //    }
        //    finally
        //    {
        //        if (img != null)
        //        {
        //            IntPtr deleteObject = img.GetHbitmap();
        //            DeleteObject(deleteObject);
        //            img.Dispose();
        //        }
        //        baracode = null;
        //        BillReportviewer = null;
        //        _report.Dispose();
        //    }


        //}

        //string GetSequenceNo(int orderId)
        //{
        //    return (new OrderBusiness()).GetOrderInvoiceNumber(orderId);
        //}

        public DataTable GetDataTablefromStream(string xmlData)
        {
            xmlData = xmlData.Replace("&", "&amp;");
            DataSet dataSet = new DataSet();
            DataTable dataTable = new DataTable("Item");
            dataTable.Columns.Add("Name", typeof(string));
            dataTable.Columns.Add("ArabicName", typeof(string));
            dataTable.Columns.Add("ClaimCode", typeof(string));
            dataTable.Columns.Add("QRCode", typeof(string));
            dataTable.Columns.Add("Qty", typeof(string));
            dataTable.Columns.Add("Price", typeof(string));
            dataTable.Columns.Add("Disc", typeof(string));
            dataTable.Columns.Add("ProductNamelbl", typeof(string));
            dataTable.Columns.Add("Qtylbl", typeof(string));
            dataTable.Columns.Add("Pricelbl", typeof(string));
            dataTable.Columns.Add("Disclbl", typeof(string));
            dataTable.Columns.Add("Currency", typeof(string));
            dataTable.Columns.Add("TotalVal", typeof(string));
            dataTable.Columns.Add("ProductCode", typeof(string)); // Added by Anisur Rahman for Dubai frame (Product code + Barcode) receipt requirement
            dataSet.Tables.Add(dataTable);

            System.IO.StringReader xmlSR = new System.IO.StringReader(xmlData);

            dataSet.ReadXml(xmlSR, XmlReadMode.IgnoreSchema);
            return dataSet.Tables[0];
        }

        // For Open Cash Box
        public TestBill(string storeName, string operatorName, string casboxOpenReason)
        {
            try
            {
                InitializeComponent();
                string ApplicationPath = AppDomain.CurrentDomain.BaseDirectory;
                TaxBusiness taxBusiness = new TaxBusiness();
                StoreInfo store = taxBusiness.getTaxConfigData();
                _report.Load(ApplicationPath + "\\Reports\\CashBoxOpenRecipt.rpt");

                //This is for ALL reports

                TextObject txtTtile = (TextObject)_report.ReportDefinition.Sections["Section1"].ReportObjects["txtReceiptTitle"];
                txtTtile.Text = store.BillReceiptTitle;

                TextObject txtAddress = (TextObject)_report.ReportDefinition.Sections["Section1"].ReportObjects["txtAddress"];
                txtAddress.Text = "" + store.Address.Replace("\n", " ").Replace("\r", "");

                TextObject txtPhone = (TextObject)_report.ReportDefinition.Sections["Section1"].ReportObjects["txtPhone"];
                txtPhone.Text = "Phone: " + store.PhoneNo;

                TextObject txtEmail = (TextObject)_report.ReportDefinition.Sections["Section1"].ReportObjects["txtEmail"];
                // txtEmail.Text = "Email: " + store.EmailID;
                txtEmail.Text = "Email: " + ConfigurationManager.AppSettings["ServiceEmail"].ToString();

                TextObject txtUrl = (TextObject)_report.ReportDefinition.Sections["Section1"].ReportObjects["txtUrl"];
                txtUrl.Text = store.WebsiteURL;

                TextObject txtRegistrationNo = (TextObject)_report.ReportDefinition.Sections["Section1"].ReportObjects["txtRegistrationNo"];
                txtRegistrationNo.Text = store.TaxRegistrationText + ": " + store.TaxRegistrationNumber;

                TextObject storename = (TextObject)_report.ReportDefinition.Sections["Section1"].ReportObjects["Txtstorename"];
                storename.Text = storeName;

                TextObject op = (TextObject)_report.ReportDefinition.Sections["Section3"].ReportObjects["txtOperator"];
                op.Text = operatorName;

                TextObject reason = (TextObject)_report.ReportDefinition.Sections["DetailSection1"].ReportObjects["txtCashboxOpenReason"];
                reason.Text = casboxOpenReason;

                DataTable dt = new DataTable();
                dt.Columns.Add("StoreName", typeof(string));
                dt.Columns.Add("Operator", typeof(string));
                dt.Columns.Add("Reason", typeof(string));
                dt.Rows.Add(storename, operatorName, reason.Text);
                _report.SetDataSource(dt);
                _report.Refresh();
                _report.PrintOptions.PrinterName = Common.LoginUser.ReceiptPrinterPath;
                //report.PrintOptions.PrinterName = "HP LaserJet 1020";
                //report.PrintOptions.PaperSize = PaperSize.Paper11x17;
                //report.PrintToPrinter(1, true, 1, 1);
                _report.PrintToPrinter(1, true, 1, 1);
                //BillReportviewer.ViewerCore.ReportSource = report;
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
            }
        }

        /// <summary>
        /// Datas the table from attribute enumerable.
        /// </summary>
        /// <param name="ien">The ien.</param>
        /// <returns></returns>
        //private DataTable DataTableFromIEnumerable(IEnumerable ien)
        //{
        //    DataTable dt = new DataTable();
        //    foreach (object obj in ien)
        //    {
        //        Type t = obj.GetType();
        //        PropertyInfo[] pis = t.GetProperties();
        //        if (dt.Columns.Count == 0)
        //        {
        //            foreach (PropertyInfo pi in pis)
        //            {
        //                Type pt = pi.PropertyType;
        //                if (pt.IsGenericType && pt.GetGenericTypeDefinition() == typeof(Nullable<>))
        //                    pt = Nullable.GetUnderlyingType(pt);
        //                dt.Columns.Add(pi.Name, pt);
        //            }
        //        }
        //        DataRow dr = dt.NewRow();
        //        foreach (PropertyInfo pi in pis)
        //        {
        //            object value = pi.GetValue(obj, null);
        //            dr[pi.Name] = value;
        //        }
        //        dt.Rows.Add(dr);
        //    }
        //    return dt;
        //}

        /// <summary>
        /// Get the no of bill receipt configured 
        /// </summary>
        /// <returns></returns>
        private int GetNoOfBillReceipt()
        {
            //Default is set as 1 
            int NoOfBillReceipt = 1;
            try
            {
                //NoOfBillReceipt = Convert.ToInt32(_objDigiPhotoDataServices.GetConfigurationData(LoginUser.SubStoreId).DG_NoOfBillReceipt);
                NoOfBillReceipt = new ConfigBusiness().GetConfigurationData(LoginUser.SubStoreId).DG_NoOfBillReceipt.ToInt32();
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
            }
            return NoOfBillReceipt;
        }

        void SaveOrderNo(string OrderNo)
        {
            try
            {
                string orders = string.Empty;
                string pathtosave = Environment.CurrentDirectory;
                //if exists get all orders
                if (File.Exists(pathtosave + "\\on.dat"))
                {
                    using (StreamReader reader = new StreamReader(Environment.CurrentDirectory + "\\on.dat"))
                    {
                        string line = reader.ReadLine();
                        orders = CryptorEngine.Decrypt(line, true);
                    }
                    File.Delete(pathtosave + "\\on.dat");
                }
                while (orders.Split(',').Count() > 2)
                    orders = orders.Remove(0, orders.IndexOf(',') + 1);

                orders += string.IsNullOrEmpty(orders) ? OrderNo : "," + OrderNo;
                using (StreamWriter b = new StreamWriter(File.Open(pathtosave + "\\on.dat", FileMode.Create)))
                {
                    b.Write(CryptorEngine.Encrypt(orders, true));
                    b.Close();
                }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }
        string logoImgName = string.Empty;
        private void GetStoreConfigData()
        {
            ConfigBusiness conBiz = new ConfigBusiness();
            List<iMIXStoreConfigurationInfo> ConfigValuesList = conBiz.GetStoreConfigData();

            for (int i = 0; i < ConfigValuesList.Count; i++)
            {
                switch (ConfigValuesList[i].IMIXConfigurationMasterId)
                {
                    case (int)ConfigParams.ReceiptLogoPath:
                        logoImgName = ConfigValuesList[i].ConfigurationValue != null ? System.IO.Path.Combine(ConfigManager.DigiFolderPath, "ReceiptLogo", ConfigValuesList[i].ConfigurationValue) : string.Empty;
                        break;

                }
            }

        }
    }
}
