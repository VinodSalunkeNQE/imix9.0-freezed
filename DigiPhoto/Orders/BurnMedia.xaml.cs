﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Collections.ObjectModel;
//using DigiPhoto.DataLayer.Model;
//using DigiPhoto.DataLayer;
using System.Text.RegularExpressions;
using DigiPhoto.Orders;
using System.IO;
using System.Printing;
using XPBurn;
using CrystalDecisions.Shared;
using CrystalDecisions.CrystalReports.Engine;
using DigiPhoto.Common;
using DigiAuditLogger;
using BurnMedia;
using System.Runtime.InteropServices;
using System.ComponentModel;
using System.Runtime.InteropServices.ComTypes;
using DigiPhoto.IMIX.Business;
using DigiPhoto.IMIX.Model;
using DigiPhoto.Utility.Repository.ValueType;

namespace DigiPhoto.Orders
{
    public partial class BurnMediaOrderList : Window
    {
        #region BurnImage Variables

        //private bool _flgIsProductCD = false;
        private List<MsftDiscRecorder2> _lstmain;
        private MsftDiscMaster2 _discMaster;
        private Int64 _totalDiscSize;
        private const string _clientName = "BurnMedia1";
        BackgroundWorker _backgroundBurnWorker = new BackgroundWorker();
        private bool _isBurning;
        private bool _isFormatting;
        private IMAPI_BURN_VERIFICATION_LEVEL _verificationLevel = IMAPI_BURN_VERIFICATION_LEVEL.IMAPI_BURN_VERIFICATION_NONE;
        private bool _closeMedia;
        private bool _ejectMedia;
        private BurnData _burnData = new BurnData();
        private string _msg = string.Empty;
        private string _cdOrderSuccess = String.Empty;
        #endregion BurnImage Variables

        public bool _isAutoStart = false;
        private bool _isActiveStockShot = false;
        BackgroundWorker _bwBurn = new BackgroundWorker();
        BackgroundWorker _bwResize = new BackgroundWorker();
        private bool _flgRecordLoaded = false;
        Burningimage _mw = new Burningimage();
        private delegate void UpdateDelegate(object[] x);
        private List<DirectoryItem> _lstDir = new List<DirectoryItem>();
        private List<BurnOrderElements> _objBurnOrderDetails = new List<BurnOrderElements>();
        private string _defaultEffect = @"<image brightness=""0"" contrast=""1"" Crop=""##"" colourvalue=""##"" rotate=""##""><effects sharpen=""##"" greyscale=""0"" digimagic=""0"" sepia=""0"" defog=""0"" underwater=""0"" emboss=""0"" invert=""0"" granite=""0"" hue=""##"" cartoon=""0"" /></image>";
        private string _defaultSpecPrintLayer = @"<photo zoomfactor=""1"" border="""" bg="""" canvasleft=""-1"" canvastop=""-1"" scalecentrex=""-1"" scalecentrey=""-1"" />";
        enum StatusCode { Pending = 0, Success = 1, Failed = 2 };
        enum ProductCode { CD = 35, USB = 36, VideoCD = 96, VideoUSB = 97 };
        int productId = 0;
        public BurnMediaOrderList()
        {
            InitializeComponent();
            _bwBurn.DoWork += bwBurn_DoWork;
            _bwBurn.RunWorkerCompleted += bwBurn_RunWorkerCompleted;
            _backgroundBurnWorker.DoWork += backgroundBurnWorker_DoWork;
            _backgroundBurnWorker.RunWorkerCompleted += backgroundBurnWorker_RunWorkerCompleted;
            _bwResize.DoWork += bwResize_DoWork;
            _bwResize.RunWorkerCompleted += bwResize_RunWorkerCompleted;
            burnLoadimage();
            _isActiveStockShot = ((new TaxBusiness()).getTaxConfigData()).IsActiveStockShot;
        }
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                LoadBurnOrders();
                //GetPendingBurnOrderDetails(false);
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
            }
        }
        private void GetPendingBurnOrderDetails(bool all)
         {
            _objBurnOrderDetails = new List<BurnOrderElements>();
            //DigiPhotoDataServices _objdatalayer = new DigiPhotoDataServices();
            try
            {
                List<IMIX.Model.BurnOrderInfo> burnOrderInfo = (new OrderBusiness()).GetPendingBurnOrders(all);
                // List<DigiPhoto.DataLayer.BurnOrderInfo> burnOrderInfo = _objdatalayer.GetPendingBurnOrders(all);
                foreach (var item in burnOrderInfo)
                {
                    if (isLocalOrder(item.OrderNumber, item.ProductType))
                    {
                        if (item.Status == 0 || item.Status == 2)
                        {
                            BurnOrderElements _objnew = new BurnOrderElements();
                            _objnew.BurnOrderNumber1 = item.OrderNumber;
                            _objnew.Status1 = Convert.ToString((StatusCode)item.Status);
                            _objnew.BurnKey1 = (long)item.OrderDetailId;
                            _objnew.MediaType1 = Convert.ToString((ProductCode)item.ProductType);
                            if (item.Status == 1)
                            {
                                _objnew.IsBurnEnable1 = false;
                                _objnew.IsBurnVisible1 = System.Windows.Visibility.Collapsed;
                                _objnew.IsDisableBurnVisible1 = System.Windows.Visibility.Visible;
                            }
                            else
                            {
                                _objnew.IsBurnEnable1 = true;
                                _objnew.IsBurnVisible1 = System.Windows.Visibility.Visible;
                                _objnew.IsDisableBurnVisible1 = System.Windows.Visibility.Collapsed;
                            }
                            _objBurnOrderDetails.Add(_objnew);
                        }
                    }
                }
                DGBurnOrders.ItemsSource = _objBurnOrderDetails;
                _flgRecordLoaded = true;
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
            }
        }
        private void ProcessUSBOrderDetails(int boId, int productId)
        {
            String ordNumber = string.Empty;
            string ordFiles = string.Empty;
            string[] ordList;
            int status = 0;
            BurnOrderElements.isExecuting = true;
            PhotoBusiness photoBiz = new PhotoBusiness();
            try
            {
                var ordDetail = (new OrderBusiness()).GetBODetails(boId);
                if (ordDetail != null)
                {
                    ordNumber = ordDetail.First().OrderNumber;
                    ordFiles = ordDetail.First().PhotosId;
                    ordList = ordFiles.Split(',');// SplitOrders(ordFiles);
                    List<PhotoDetail> photoList = photoBiz.GetPhotoDetailsByPhotoIds(ordDetail.First().PhotosId);
                    if (ordList.Length > 0)
                    {
                        //Execute USB Order
                        status = ExecuteUSBOrder(ordNumber, ordList, photoList);
                        if (status != 0 && status != 3)
                        {
                            (new OrderBusiness()).UpdateBurnOrderStatus(boId, status, LoginUser.UserId, (new CustomBusineses()).ServerDateTime());
                        }
                        if (status == 1)
                        {
                            _msg = "USB: Order " + ordNumber + " processed successfully!";
                            DeleteOrder(ordNumber, productId); //36 for USB
                        }
                        else if (status == 2)
                        {
                            _msg = "USB: Order " + ordNumber + " failed!";
                        }
                        else if (status == 3)
                        {
                            _msg = "No USB drive found!";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
            }
            finally
            {
                //_objdatalayer = null;
                photoBiz = null;
            }
        }

        #region Initiate CD Burn Order
        void bwResize_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (e.Result != null)
            {
                var varBurnData = e.Result;
                _backgroundBurnWorker.RunWorkerAsync(varBurnData);
            }
            else
            {
                BurnEnd();
            }
        }

        void bwResize_DoWork(object sender, DoWorkEventArgs e)
        {
            string[] arg = e.Argument.ToString().Split('#');
            int ord = Convert.ToInt32(arg[0]);
            productId = Convert.ToInt32(arg[1]);
            string proCode = listOfItems.Where(o => o.OrderDetailId == ord).FirstOrDefault().OrderNumber;
            var listOfItems11 = listOfItems.Where(o => o.OrderNumber == proCode && (o.ProductType == 96 || o.ProductType == 35)).ToList();
            foreach (var IndividualItem in listOfItems11)
            {
                e.Result = ProcessCDOrderDetails(IndividualItem.OrderDetailId, IndividualItem.ProductType);
            }
        }
        #endregion Initiate CD Burn Order
        private Object ProcessCDOrderDetails(int boId, int productId)
        {
            object varBdata = null;
            BurnOrderElements.isExecuting = true;
            String ordNumber = string.Empty;
            string ordFiles = string.Empty; string[] OrdList;
            string[] ordList;
            try
            {
                var ordDetail = (new OrderBusiness()).GetBODetails(boId);
                if (ordDetail != null)
                {
                    ordNumber = ordDetail.First().OrderNumber;
                    ordFiles = ordDetail.First().PhotosId;
                    ordList = ordFiles.Split(',');// SplitOrders(ordFiles);
                    if (ordList.Length > 0)
                    {
                        if (CheckCDAvailable())
                        {
                            PhotoBusiness photoBiz = new PhotoBusiness();
                            List<PhotoDetail> photoList = photoBiz.GetPhotoDetailsByPhotoIds(ordDetail.First().PhotosId);
                            //Execute CD Order
                            varBdata = ExecuteCDOrder(boId, ordNumber, ordList, photoList, productId);
                            photoBiz = null;
                        }
                        else
                        {
                            _msg = "No CD drive found!";
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
                _msg = "Error processing the order!";
            }
            finally
            {
                //_objdatalayer = null;
            }
            return varBdata;
        }
        private string getProdType(int boId)
        {
            string ProdType = string.Empty;
            try
            {
                var ordDetail = (new OrderBusiness()).GetBODetails(boId);
                if (ordDetail != null)
                {
                    ProdType = Convert.ToString((ProductCode)ordDetail.First().ProductType);
                }
                return ProdType;
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
                return ProdType;
            }
        }
        private int ExecuteUSBOrder(string ordNumber, string[] ordList, List<PhotoDetail> photoList)
        {
            bool drivefound = false;
            int stat = 0;
            try
            {
                DriveInfo[] ListDrives = DriveInfo.GetDrives();
                string TargetPath = string.Empty;
                foreach (DriveInfo Drive in ListDrives)
                {
                    /*if (Drive.DriveType == DriveType.Removable)* */
                    if (Drive.IsReady == true && Drive.DriveType == DriveType.Removable && Drive.DriveFormat == "FAT32")
                    {
                        TargetPath = Drive.RootDirectory.ToString();
                        drivefound = true;
                    }
                }
                if (!drivefound)
                {
                    stat = 3; //Burn failed
                }
                else
                {
                    foreach (PhotoDetail photoDetail in photoList)
                    {
                        //Apply Borders and other database effects before copying images in USB.
                        object[] x = new object[] { photoDetail, TargetPath };
                        UpdateDelegate update = new UpdateDelegate(StartEngine);
                        this.Dispatcher.Invoke(update, new object[] { x });
                    }
                    stat = 1; //Burn Succeeded
                }
            }
            catch (Exception ex)
            {
                stat = 2;
                ErrorHandler.ErrorHandler.LogError(ex);
            }
            return stat;
        }
        private void PrepareImage(PhotoInfo _objphoto, string destinationPath)
        {
            if (_objphoto != null)
            {
                if (!string.IsNullOrEmpty(_objphoto.DG_Photos_Effects))
                {
                    _mw.ImageEffect = _objphoto.DG_Photos_Effects;
                }
                else
                {
                    _mw.ImageEffect = "<image brightness = '0' contrast = '1' Crop='##' colourvalue = '##' rotate='##' ><effects sharpen='##' greyscale='0' digimagic='0' sepia='0' defog='0' underwater='0' emboss='0' invert = '0' granite='0' hue ='##' cartoon = '0'></effects></image>";
                }
            }
            _mw.WindowStyle = System.Windows.WindowStyle.None;
            _mw.ResetForm(_objphoto);
            _mw.UpdateLayout();
            RenderTargetBitmap _objeditedImage = _mw.jCaptureScreen();

            using (var fileStream = new FileStream(destinationPath, FileMode.Create, FileAccess.ReadWrite))
            {
                JpegBitmapEncoder encoder = new JpegBitmapEncoder();
                encoder.QualityLevel = 94;
                encoder.Frames.Add(BitmapFrame.Create(_objeditedImage));
                encoder.Save(fileStream);
            }
        }
        private bool IsEffectApplied(PhotoInfo _objphoto)
        {
            if (string.IsNullOrEmpty(_objphoto.DG_Photos_Effects) || string.Compare(_objphoto.DG_Photos_Effects, _defaultEffect.TrimStart().TrimEnd(), true) == 0)
            {
                //No effect applied. Check Layering.
                if (string.IsNullOrEmpty(_objphoto.DG_Photos_Layering) || _objphoto.DG_Photos_Layering == "test" || string.Compare(_objphoto.DG_Photos_Layering, _defaultSpecPrintLayer.TrimStart().TrimEnd(), true) == 0)
                    return false;
            }
            return true;
        }
        private bool IsEffectApplied(PhotoDetail photoDetail)
        {
            if (string.IsNullOrEmpty(photoDetail.Effects) || string.Compare(photoDetail.Effects, _defaultEffect.TrimStart().TrimEnd(), true) == 0)
            {
                //No effect applied. Check Layering.
                if (string.IsNullOrEmpty(photoDetail.Layering) || photoDetail.Layering == "test" || string.Compare(photoDetail.Layering, _defaultSpecPrintLayer.TrimStart().TrimEnd(), true) == 0)
                    return false;
            }
            return true;
        }
        private void StartEngine(object[] x)
        {
            try
            {
                PhotoDetail photoDetail = null;
                string saveFolderPath = x[1].ToString();
                if (!Directory.Exists(saveFolderPath))
                    Directory.CreateDirectory(saveFolderPath);
                if (x[0] != null)
                    photoDetail = (PhotoDetail)x[0];

                _mw.PhotoName = photoDetail.PhotoRFID;
                _mw.PhotoId = photoDetail.PhotoId;

                string editedImagePath = System.IO.Path.Combine(photoDetail.HotFolderPath, "EditedImages", photoDetail.FileName);
                if (!string.IsNullOrEmpty(saveFolderPath))
                {
                    if (photoDetail.MediaType == 1)
                    {
                        if (!File.Exists(editedImagePath))
                        {
                            File.Copy(System.IO.Path.Combine(photoDetail.HotFolderPath, photoDetail.DG_Photos_CreatedOn.ToString("yyyyMMdd"), photoDetail.FileName), System.IO.Path.Combine(saveFolderPath, photoDetail.PhotoId + ".jpg"));
                        }
                        else if (!File.Exists(editedImagePath) && IsEffectApplied(photoDetail))
                        {
                            //DigiPhotoDataServices _objDbLayer = new DigiPhotoDataServices();
                            PhotoInfo _objphoto = (new PhotoBusiness()).GetPhotoDetailsbyPhotoId(photoDetail.PhotoId);
                            PrepareImage(_objphoto, saveFolderPath + photoDetail.PhotoId + ".jpg");
                            //_objDbLayer = null;
                        }
                        else if (File.Exists(editedImagePath))
                        File.Copy(editedImagePath, System.IO.Path.Combine(saveFolderPath, photoDetail.PhotoId + ".jpg"), true);
                        else
                            File.Copy(System.IO.Path.Combine(photoDetail.HotFolderPath, photoDetail.DG_Photos_CreatedOn.ToString("yyyyMMdd"), photoDetail.FileName), System.IO.Path.Combine(saveFolderPath, photoDetail.PhotoId + ".jpg"), true);
                    }
                    else if (photoDetail.MediaType == 2)
                    {
                        string fileName = photoDetail.FileName;
                        if (!System.IO.File.Exists(System.IO.Path.Combine(saveFolderPath, photoDetail.PhotoId + System.IO.Path.GetExtension(photoDetail.FileName))))
                            File.Copy(System.IO.Path.Combine(photoDetail.HotFolderPath, "Videos", photoDetail.DG_Photos_CreatedOn.ToString("yyyyMMdd"), photoDetail.FileName), System.IO.Path.Combine(saveFolderPath, photoDetail.PhotoId + System.IO.Path.GetExtension(photoDetail.FileName)), true);
                    }
                    else if (photoDetail.MediaType == 3)
                    {
                        if (!System.IO.File.Exists(System.IO.Path.Combine(saveFolderPath, photoDetail.PhotoId + System.IO.Path.GetExtension(photoDetail.FileName))))
                            File.Copy(System.IO.Path.Combine(photoDetail.HotFolderPath, "ProcessedVideos", photoDetail.DG_Photos_CreatedOn.ToString("yyyyMMdd"), photoDetail.FileName), System.IO.Path.Combine(saveFolderPath, photoDetail.PhotoId + System.IO.Path.GetExtension(photoDetail.FileName)), true);
                    }
                }
                WriteStockShotImages(saveFolderPath);
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
            //finally
            //{
            //    //MemoryManagement.FlushMemory();
            //}
        }
        //private void StartEngine(object[] x)
        //{
        //    try
        //    {
        //        PhotoDetail photoDetail = null;
        //        string saveFolderPath = x[1].ToString();
        //        if (x[0] != null)
        //            photoDetail = (PhotoDetail)x[0];

        //        MW.PhotoName = photoDetail.PhotoRFID;
        //        MW.PhotoId = photoDetail.PhotoId;

        //        string editedImagePath = System.IO.Path.Combine(LoginUser.DigiFolderPath, "EditedImages", photoDetail.FileName);
        //        if (!string.IsNullOrEmpty(saveFolderPath))
        //        {
        //            if (!File.Exists(editedImagePath) && IsEffectApplied(photoDetail))
        //            {
        //                DigiPhotoDataServices _objDbLayer = new DigiPhotoDataServices();
        //                DG_Photos _objphoto = _objDbLayer.GetPhotoDetailsbyPhotoId(photoDetail.PhotoId);
        //                PrepareImage(_objphoto, saveFolderPath + photoDetail.PhotoId + ".jpg");
        //            }

        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
        //        ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
        //    }
        //    finally
        //    {
        //        //MemoryManagement.FlushMemory();
        //    }
        //}
        private Object ExecuteCDOrder(int ordId, string ordNumber, string[] OrdList, List<PhotoDetail> photoList, int productId)
        {
            try
            {
                string OrdPath = Environment.CurrentDirectory + "\\DigiOrderdImages\\CDOrders\\" + ordNumber + "\\";
                string newOrdPath = string.Empty;
                if (productId == 35)
                {
                    newOrdPath = OrdPath + "CD\\";
                }
                else
                {
                    newOrdPath = OrdPath + "VideoCD\\";
                }

                foreach (PhotoDetail photo in photoList)
                {
                    //Apply Borders and other database effects before copying images in CD.
                    object[] x = new object[] { photo, newOrdPath };
                    UpdateDelegate update = new UpdateDelegate(StartEngine);
                    this.Dispatcher.Invoke(update, new object[] { x });
                }

                _burnData = new BurnData();
                _lstDir = new List<DirectoryItem>();
                var directoryItem = new DirectoryItem(OrdPath);

                _lstDir.Add(directoryItem);
                UpdateCapacity();
                _isBurning = true;

                int selectedDriveIndex = _lstmain.Count() - 1;

                var discRecorder = (IDiscRecorder2)_lstmain[selectedDriveIndex];
                _burnData.uniqueRecorderId = discRecorder.ActiveDiscRecorder;
                _burnData.BOID = ordId;
                _burnData.ORDERNUMBER = ordNumber;
                var varBurnData = _burnData;
                return varBurnData;

            }
            catch (Exception ex)
            {
                string errorMessage = "lstmain.Count()" + _lstmain.Count() + ", " + ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                return null;
            }
        }

        private void WriteStockShotImages(string OrdPath)
        {
            try
            {
                if (_isActiveStockShot)
                {
                    foreach (StockShot item in (new ConfigBusiness()).GetStockShotImagesList(0).Where(x => x.IsActive == true))
                    {
                        string filePath = System.IO.Path.Combine(LoginUser.DigiFolderPath, "StockShot", item.ImageName);
                        if (File.Exists(filePath))
                        {
                            try
                            {
                            File.Copy(filePath, System.IO.Path.Combine(OrdPath, item.ImageName), true);
                        }
                            catch { }
                        }

                    }

                }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }
        void backgroundBurnWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (!string.IsNullOrEmpty(_cdOrderSuccess))
            {
                DeleteOrder(_cdOrderSuccess, productId); //35 for CD
                _cdOrderSuccess = String.Empty;
            }
            LoadBurnOrders();
            BurnEnd();
        }
        private void btnClose_Click(object sender, RoutedEventArgs e)
        {
            this.Hide();
        }
        private void btnAuto_Click(object sender, RoutedEventArgs e)
        {
            _isAutoStart = true;
            AutoExecuteOrders();
            //LoadBurnOrders();
        }
        public void LoadBurnOrders()
        {
            UpdateVOS();
            GetPendingBurnOrderDetails(true);
            //if (chkOnlyPOrders.IsChecked == true)
            //    GetPendingBurnOrderDetails(false);
            //else
            //    GetPendingBurnOrderDetails(true);
        }
        private void btnBurnMedia_Click(object sender, RoutedEventArgs e)
        {
            BurnStart(string.Empty);
            Button btnSender = (Button)sender;
            int ord = btnSender.Tag.ToInt32();

            if (getProdType(ord) == ProductCode.USB.ToString() || getProdType(ord) == ProductCode.VideoUSB.ToString())
            {
                if (getProdType(ord) == ProductCode.USB.ToString())
                {
                    _bwBurn.RunWorkerAsync(ord + "# " + 36);
                }
                else
                {
                    _bwBurn.RunWorkerAsync(ord + "#" + 97);
                }
            }
            else
            {
                burnLoadimage();
                if (getProdType(ord) == ProductCode.CD.ToString())
                {
                    _bwResize.RunWorkerAsync(ord + "#" + 35);
                }
                else
                {
                    _bwResize.RunWorkerAsync(ord + "#" + 96);
                }
            }
            //ProcessCDOrderDetails(ord);
        }
        public void AutoExecuteOrders()
        {
            BurnOrderElements.isExecuting = true;
            bool result = ExecuteNextOrder();
            if (!result)
            {
                msgBurn.Text = "No pending orders found!";
                MessageBox.Show("No pending orders found!", "iMIX", MessageBoxButton.OK, MessageBoxImage.Information);
                _isAutoStart = false;
                BurnOrderElements.isExecuting = false;
            }

        }
        List<IMIX.Model.BurnOrderInfo> listOfItems;
        private bool ExecuteNextOrder()
        {
            bool orderFound = false;
            int ordIdToExecute = 0;
            //DigiPhotoDataServices _objdatalayer = new DigiPhotoDataServices();
            listOfItems = (new OrderBusiness()).GetPendingBurnOrders(true);
            foreach (var IndividualItem in listOfItems)
            {
                if (IndividualItem.Status != 1)
                {
                    if (isLocalOrder(IndividualItem.OrderNumber, IndividualItem.ProductType))
                    {
                        orderFound = true;
                        ordIdToExecute = IndividualItem.OrderDetailId;
                        BurnStart(IndividualItem.OrderNumber);
                        if (IndividualItem.ProductType == 36 || IndividualItem.ProductType == 97)//USB Order
                            _bwBurn.RunWorkerAsync(ordIdToExecute + "#" + IndividualItem.ProductType);
                        else if (IndividualItem.ProductType == 35 || IndividualItem.ProductType == 96)//CD Order
                        {
                            //ProcessCDOrderDetails(OrdIdToExecute);
                            burnLoadimage();
                            _bwResize.RunWorkerAsync(ordIdToExecute + "#" + IndividualItem.ProductType);
                        }
                        break;
                    }
                }
            }
            //_objdatalayer = null;
            return orderFound;
        }
        public string FetchNextOrderDetails()
        {
            string nextOrdDetail = "\r\nNo active orders found!";
            string ordNum = string.Empty;
            string prodType = string.Empty;
            int OrdIdToExecute = 0;
            var listOfItems = (new OrderBusiness()).GetPendingBurnOrders(true);
            foreach (var IndividualItem in listOfItems)
            {
                if (IndividualItem.Status != 1)
                {
                    if (isLocalOrder(IndividualItem.OrderNumber, IndividualItem.ProductType))
                    {
                        ordNum = IndividualItem.OrderNumber;
                        prodType = Convert.ToString((ProductCode)IndividualItem.ProductType);
                        if (prodType == ProductCode.USB.ToString() || prodType == ProductCode.VideoUSB.ToString())
                        {
                            nextOrdDetail = "\r\nWould you like to continue processing orders?\r\nNext order in the queue is Order Number " + ordNum + " of type USB.\r\nPlease ensure that a USB drive is plugged in before pressing YES";
                        }
                        else
                        {
                            nextOrdDetail = "\r\nWould you like to continue processing orders?\r\nNext order in the queue is Order Number " + ordNum + " of type CD.\r\nPlease ensure that a fresh CD is inserted before pressing YES";
                        }
                        break;
                    }
                }
            }
            //_objdatalayer = null;
            return nextOrdDetail;
        }
        private bool DeleteOrder(string orderNumber, int prodType)
        {
            var checkForMultipleOrders = _objBurnOrderDetails.Where(Ord => Ord.BurnOrderNumber1 == orderNumber && Ord.Status1 == "Pending" && Ord.MediaType1 == Convert.ToString((ProductCode)prodType)).ToList();
            bool deleted = false;
            try
            {
                if (checkForMultipleOrders.Count == 1 || checkForMultipleOrders.Count == 0)
                {
                    string ordPath = string.Empty;
                    if (prodType == 35)
                    {
                        ordPath = Environment.CurrentDirectory + "\\DigiOrderdImages\\CDOrders\\" + orderNumber + "\\CD\\";
                    }
                    else if (prodType == 36)
                    {
                        ordPath = Environment.CurrentDirectory + "\\DigiOrderdImages\\USBOrders\\" + orderNumber + "\\USB\\";
                    }
                    else if (prodType == 96)
                    {
                        ordPath = Environment.CurrentDirectory + "\\DigiOrderdImages\\CDOrders\\" + orderNumber + "\\VideoCD\\";
                    }
                    else if (prodType == 97)
                    {
                        ordPath = Environment.CurrentDirectory + "\\DigiOrderdImages\\USBOrders\\" + orderNumber + "\\VideoUSB\\";
                    }
                    if (Directory.Exists(ordPath))
                    {

                      //  Directory.Delete(ordPath, true);
                        deleted = true;
                    }
                    // string orderFolder=string.Empty;
                    //if(ProdType == 36||ProdType == 97)
                    //    orderFolder=Environment.CurrentDirectory + "\\DigiOrderdImages\\USBOrders\\" + OrderNumber;
                    //else orderFolder = Environment.CurrentDirectory + "\\DigiOrderdImages\\CDOrders\\" + OrderNumber;
                    //if (Directory.GetFiles(orderFolder).Count()==0)
                    //{
                    //    Directory.Delete(orderFolder, true);
                    //}
                }
            }
            catch (Exception ex)
            {
                deleted = false;
                ErrorHandler.ErrorHandler.LogError(ex);
            }
            return deleted;
        }
        void bwBurn_DoWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                string[] arg = e.Argument.ToString().Split('#');
                int ord = Convert.ToInt32(arg[0]);
                int productId = Convert.ToInt32(arg[1]);
                //ProcessUSBOrderDetails(ord, productId);
                string proCode = listOfItems.Where(o => o.OrderDetailId == ord).FirstOrDefault().OrderNumber;
                var listOfItems11 = listOfItems.Where(o => o.OrderNumber == proCode && (o.ProductType == 97 || o.ProductType == 36)).ToList();
                foreach (var IndividualItem in listOfItems11)
                {
                   ProcessUSBOrderDetails(IndividualItem.OrderDetailId, IndividualItem.ProductType);
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
            }
        }
        void bwBurn_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            LoadBurnOrders();
            BurnEnd();
        }

        //private string[] SplitOrders(string ordFiles)
        //{
        //    string[] ord;
        //    ord = ordFiles.Split(',');
        //    return ord;
        //}
        private bool CheckCDAvailable()
        {
            var drives = DriveInfo.GetDrives();
            var cddrive = drives.Where(t => t.DriveType == DriveType.CDRom).FirstOrDefault();
            if (cddrive != null)
            {
                return true;
            }
            else
            {
                return false;
            }
            //return string.IsNullOrEmpty(cddrive.ToString());
        }
        private bool isLocalOrder(string orderNumber, int prodType)
        {
            string OrdPath = "";
            if (prodType == 35)
            {
                OrdPath = Environment.CurrentDirectory + "\\DigiOrderdImages\\CDOrders\\" + orderNumber + "\\CD\\";
            }
            else if (prodType == 36)
            {
                OrdPath = Environment.CurrentDirectory + "\\DigiOrderdImages\\USBOrders\\" + orderNumber + "\\USB\\";
            }
            else if (prodType == 96)
            {
                OrdPath = Environment.CurrentDirectory + "\\DigiOrderdImages\\CDOrders\\" + orderNumber + "\\VideoCD\\";
            }
            else if (prodType == 97)
            {
                OrdPath = Environment.CurrentDirectory + "\\DigiOrderdImages\\USBOrders\\" + orderNumber + "\\VideoUSB\\";
            }
            if (Directory.Exists(OrdPath))
                return true;
            else
                return false;
        }

        #region Burn Progress Disable/Enable Controls
        private void BurnStart(string orderNumber)
        {
            DGBurnOrders.Columns[3].Visibility = System.Windows.Visibility.Collapsed;
            //chkOnlyPOrders.Visibility = System.Windows.Visibility.Collapsed;
            btnLoadOrders.Visibility = System.Windows.Visibility.Collapsed;
            btnAuto.Visibility = System.Windows.Visibility.Collapsed;
            msgBurn.Text = "Processing Order " + orderNumber + "...";
            msgBurn.Visibility = System.Windows.Visibility.Visible;
            BurnOrderElements.isExecuting = true;
        }
        private void BurnEnd()
        {
            DGBurnOrders.Columns[3].Visibility = System.Windows.Visibility.Visible;
            //chkOnlyPOrders.Visibility = System.Windows.Visibility.Visible;
            btnLoadOrders.Visibility = System.Windows.Visibility.Visible;
            btnAuto.Visibility = System.Windows.Visibility.Visible;
            msgBurn.Text = _msg;
            if (_isAutoStart)
            {
                string FurtherMessage = FetchNextOrderDetails();
                if (FurtherMessage == "\r\nNo active orders found!")
                {
                    msgBurn.Text = FurtherMessage.Replace("\r\n", "");
                    MessageBox.Show(_msg + FurtherMessage, "iMIX", MessageBoxButton.OK, MessageBoxImage.Information);
                }
                else
                {
                    MessageBoxResult messageBoxResult = System.Windows.MessageBox.Show(_msg + FurtherMessage, "iMIX", System.Windows.MessageBoxButton.YesNo, MessageBoxImage.Question);
                    if (messageBoxResult == MessageBoxResult.Yes)
                        AutoExecuteOrders();
                }
            }
            else
            {
                MessageBox.Show(_msg, "iMIX", MessageBoxButton.OK, MessageBoxImage.Information);
            }
            BurnOrderElements.isExecuting = false;
        }
        #endregion Burn Progress Disable/Enable Controls

        #region Resize Image
        /// <summary>
        /// Resizes the WPF image.
        /// </summary>
        /// <param name="sourceImage">The source image.</param>
        /// <param name="maxHeight">The maximum height.(Pass the config compression level parameter)</param>
        /// <param name="saveToPath">The save automatic path.</param>
        private void ResizeWPFImage(string sourceImage, int maxHeight, string saveToPath)
        {
            try
            {
                BitmapImage bi = new BitmapImage();
                BitmapImage bitmapImage = new BitmapImage();
                using (FileStream fileStream = File.OpenRead(sourceImage.ToString()))
                {
                    MemoryStream ms = new MemoryStream();
                    fileStream.CopyTo(ms);
                    ms.Seek(0, SeekOrigin.Begin);
                    fileStream.Close();
                    bi.BeginInit();
                    bi.StreamSource = ms;
                    bi.EndInit();
                    bi.Freeze();

                    ms.Seek(0, SeekOrigin.Begin);
                    bitmapImage.BeginInit();
                    bitmapImage.StreamSource = ms;

                    bitmapImage.DecodePixelWidth = (bi.PixelWidth * maxHeight / 100).ToInt32();
                    bitmapImage.DecodePixelHeight = (bi.PixelHeight * maxHeight / 100).ToInt32();
                    bitmapImage.EndInit();
                    bitmapImage.Freeze();
                    fileStream.Close();

                    //decimal ratio = 0;
                    //int newWidth = 0;
                    //int newHeight = 0;

                    //int resHight = Convert.ToInt32(bi.PixelHeight * maxHeight / 100);

                    //if (bi.PixelWidth >= bi.PixelHeight)
                    //{
                    //    ratio = Convert.ToDecimal(bi.PixelWidth) / Convert.ToDecimal(bi.PixelHeight);
                    //    newWidth = resHight;//maxHeight;
                    //    newHeight = Convert.ToInt32(newWidth / ratio);
                    //}
                    //else
                    //{
                    //    ratio = Convert.ToDecimal(bi.PixelHeight) / Convert.ToDecimal(bi.PixelWidth);
                    //    newHeight = resHight;//maxHeight;
                    //    newWidth = Convert.ToInt32(newHeight / ratio);
                    //}

                    /*
                    int resHight = Convert.ToInt32(bi.Height * maxHeight / 100);

                    if (bi.Width >= bi.Height)
                    {
                        ratio = Convert.ToDecimal(bi.Width) / Convert.ToDecimal(bi.Height);
                        newWidth = resHight;//maxHeight;
                        newHeight = Convert.ToInt32(newWidth / ratio);
                    }
                    else
                    {
                        ratio = Convert.ToDecimal(bi.Height) / Convert.ToDecimal(bi.Width);
                        newHeight = resHight;//maxHeight;
                        newWidth = Convert.ToInt32(newHeight / ratio);
                    }
                    */


                }

                using (var fileStreamForSave = new FileStream(saveToPath, FileMode.Create, FileAccess.ReadWrite))
                {
                    JpegBitmapEncoder encoder = new JpegBitmapEncoder();
                    encoder.QualityLevel = 98; //It returns nearest quality of the image
                    encoder.Frames.Add(BitmapFrame.Create(bitmapImage));
                    encoder.Save(fileStreamForSave);
                    fileStreamForSave.Close();
                }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }
        #endregion Resize Image

        #region BurnImage Code

        public void burnLoadimage()
        {
            AddRecordingDevices();
            MsftDiscMaster2 discMaster = null;
            _lstmain = new List<MsftDiscRecorder2>();
            try
            {
                discMaster = new MsftDiscMaster2();
                if (!discMaster.IsSupportedEnvironment)
                    return;
                foreach (string uniqueRecorderId in discMaster)
                {
                    var discRecorder2 = new MsftDiscRecorder2();
                    discRecorder2.InitializeDiscRecorder(uniqueRecorderId);
                    _lstmain.Add(discRecorder2);
                }
                if (devicesComboBox.Items.Count > 0)
                {
                    devicesComboBox.SelectedIndex = 0;
                }
            }
            catch (COMException ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
            }
            finally
            {
                if (discMaster != null)
                {
                    Marshal.ReleaseComObject(discMaster);
                }
            }
            DateTime now = (new CustomBusineses()).ServerDateTime();
            UpdateCapacity();
        }
        private void AddRecordingDevices()
        {
            Dictionary<string, string> discRecorders = new Dictionary<string, string>();
            _discMaster = new MsftDiscMaster2();
            try
            {
                if (!_discMaster.IsSupportedEnvironment)
                    return;
                foreach (string uniqueRecorderId in _discMaster)
                {
                    int i = 0;
                    var discRecorder2 = new MsftDiscRecorder2();
                    discRecorder2.InitializeDiscRecorder(uniqueRecorderId);
                    var devicePaths = string.Empty;
                    if (discRecorder2 != null)
                    {
                        var volumePath = (string)discRecorder2.VolumePathNames.GetValue(0);
                        foreach (string volPath in discRecorder2.VolumePathNames)
                        {
                            if (!string.IsNullOrEmpty(devicePaths))
                            {
                                devicePaths += ",";
                            }
                            devicePaths += volumePath;
                        }
                        //string photoIDs = string.Join(", ", discRecorder2.Select(i => i.VolumePathNames.ToString()).ToArray());
                    }
                    if (discRecorder2 != null)
                        discRecorders.Add(string.Format("{0} [{1}]", devicePaths, discRecorder2.ProductId), i.ToString());
                }

                devicesComboBox.ItemsSource = discRecorders;
                devicesComboBox.SelectedValue = "0";
            }
            catch (COMException ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
            }
            //finally
            //{

            //}
        }
        private void UpdateCapacity()
        {
            //
            // Get the text for the Max Size
            //
            if (_totalDiscSize == 0)
            {
                return;
            }
            ////
            //// Calculate the size of the files
            ////
            Int64 totalMediaSize = 0;
            ////foreach (IMediaItem mediaItem in listBoxFiles.Items)
            var Ite1 = _lstDir.ToList();
            //foreach (IMediaItem mediaItem in Ite1)
            //{
            //    totalMediaSize += mediaItem.SizeOnDisc;
            //}

            totalMediaSize = Ite1.Select(i => i.SizeOnDisc).Sum();
        }
        private void backgroundBurnWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            MsftDiscRecorder2 discRecorder = null;
            MsftDiscFormat2Data discFormatData = null;
            //DigiPhotoDataServices _objdatalayer = new DigiPhotoDataServices();
            var burnData = (BurnData)e.Argument;
            try
            {
                //
                // Create and initialize the IDiscRecorder2 object
                //
                discRecorder = new MsftDiscRecorder2();

                discRecorder.InitializeDiscRecorder(burnData.uniqueRecorderId);

                //
                // Create and initialize the IDiscFormat2Data
                //
                discFormatData = new MsftDiscFormat2Data
                {
                    Recorder = discRecorder,
                    ClientName = _clientName,
                    ForceMediaToBeClosed = _closeMedia
                };
                //
                // Set the verification level
                //
                var burnVerification = (IBurnVerification)discFormatData;
                burnVerification.BurnVerificationLevel = _verificationLevel;

                //
                // Check if media is blank, (for RW media)
                //
                object[] multisessionInterfaces = null;
                if (!discFormatData.MediaHeuristicallyBlank)
                {
                    multisessionInterfaces = discFormatData.MultisessionInterfaces;
                }

                //
                // Create the file system
                //
                IStream fileSystem;
                if (!CreateMediaFileSystem(discRecorder, multisessionInterfaces, out fileSystem))
                {
                    string proCode = listOfItems.Where(o => o.OrderDetailId == burnData.BOID).FirstOrDefault().OrderNumber;
                    var listOfItems11 = listOfItems.Where(o => o.OrderNumber == proCode && (o.ProductType == 96 || o.ProductType == 35)).ToList();
                    foreach (var IndividualItem in listOfItems11)
                    {
                        new OrderBusiness().UpdateBurnOrderStatus(burnData.BOID, 2, LoginUser.UserId, new CustomBusineses().ServerDateTime());
                    }
                    
                    //new OrderBusiness().UpdateBurnOrderStatus(burnData.BOID, 2, LoginUser.UserId, new CustomBusineses().ServerDateTime());
                    _msg = "CD: Order " + burnData.ORDERNUMBER + " failed!";
                    e.Result = -1;
                    return;
                }

                //
                // add the Update event handler
                //
                discFormatData.Update += discFormatData_Update;

                //
                // Write the data here
                //
                try
                {
                    discFormatData.Write(fileSystem);
                    e.Result = 1;
                    _msg = "CD: Order " + burnData.ORDERNUMBER + " processed successfully!";
                    string proCode = listOfItems.Where(o => o.OrderDetailId == burnData.BOID).FirstOrDefault().OrderNumber;
                    var listOfItems11 = listOfItems.Where(o => o.OrderNumber == proCode && (o.ProductType == 96 || o.ProductType == 35)).ToList();
                    foreach (var IndividualItem in listOfItems11)
                    {
                        (new OrderBusiness()).UpdateBurnOrderStatus(IndividualItem.OrderDetailId, 1, LoginUser.UserId, (new CustomBusineses()).ServerDateTime());
                    }
                    
                    _cdOrderSuccess = burnData.ORDERNUMBER;
                    //DeleteOrder(burnData.ORDERNUMBER, 35); //35 for CD
                }
                catch (COMException ex)
                {
                    string errorMessage = "There may be some issue in CD format for order no " + burnData.ORDERNUMBER + ". Error may be " + ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                    ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                    e.Result = 1;
                    _msg = "CD: Order " + burnData.ORDERNUMBER + " processed successfully!";
                  //  (new OrderBusiness()).UpdateBurnOrderStatus(burnData.BOID, 1, LoginUser.UserId, (new CustomBusineses()).ServerDateTime());
                    string proCode = listOfItems.Where(o => o.OrderDetailId == burnData.BOID).FirstOrDefault().OrderNumber;
                    var listOfItems11 = listOfItems.Where(o => o.OrderNumber == proCode && (o.ProductType == 96 || o.ProductType == 35)).ToList();
                    foreach (var IndividualItem in listOfItems11)
                    {
                        (new OrderBusiness()).UpdateBurnOrderStatus(IndividualItem.OrderDetailId, 1, LoginUser.UserId, (new CustomBusineses()).ServerDateTime());
                    }
                    _cdOrderSuccess = burnData.ORDERNUMBER;
                }
                finally
                {
                    if (fileSystem != null)
                    {
                        Marshal.FinalReleaseComObject(fileSystem);
                    }
                }

                //
                // remove the Update event handler
                //
                discFormatData.Update -= discFormatData_Update;
                discRecorder.EjectMedia();
            }
            catch (COMException exception)
            {
                //
                // If anything happens during the format, show the message
                //
                string proCode = listOfItems.Where(o => o.OrderDetailId == burnData.BOID).FirstOrDefault().OrderNumber;
                var listOfItems11 = listOfItems.Where(o => o.OrderNumber == proCode && (o.ProductType == 96 || o.ProductType == 35)).ToList();
                foreach (var IndividualItem in listOfItems11)
                {
                    new OrderBusiness().UpdateBurnOrderStatus(burnData.BOID, 2, LoginUser.UserId, (new CustomBusineses()).ServerDateTime());
                }
                    
               // new OrderBusiness().UpdateBurnOrderStatus(burnData.BOID, 2, LoginUser.UserId, (new CustomBusineses()).ServerDateTime());
                _msg = "CD: Order " + burnData.ORDERNUMBER + " failed!";
                //System.Windows.Forms.MessageBox.Show(exception.Message);
                e.Result = -1; //exception.ErrorCode;
            }
            finally
            {
                if (discRecorder != null)
                {
                    Marshal.ReleaseComObject(discRecorder);
                }

                if (discFormatData != null)
                {
                    Marshal.ReleaseComObject(discFormatData);
                }
            }
        }

        void discFormatData_Update([In, MarshalAs(UnmanagedType.IDispatch)] object sender, [In, MarshalAs(UnmanagedType.IDispatch)] object progress)
        {
            //
            // Check if we've cancelled
            //
            if (_backgroundBurnWorker.CancellationPending)
            {
                var format2Data = (IDiscFormat2Data)sender;
                format2Data.CancelWrite();
                return;
            }

            var eventArgs = (IDiscFormat2DataEventArgs)progress;

            _burnData.task = BURN_MEDIA_TASK.BURN_MEDIA_TASK_WRITING;

            // IDiscFormat2DataEventArgs Interface
            _burnData.elapsedTime = eventArgs.ElapsedTime;
            _burnData.remainingTime = eventArgs.RemainingTime;
            _burnData.totalTime = eventArgs.TotalTime;

            // IWriteEngine2EventArgs Interface
            _burnData.currentAction = eventArgs.CurrentAction;
            _burnData.startLba = eventArgs.StartLba;
            _burnData.sectorCount = eventArgs.SectorCount;
            _burnData.lastReadLba = eventArgs.LastReadLba;
            _burnData.lastWrittenLba = eventArgs.LastWrittenLba;
            _burnData.totalSystemBuffer = eventArgs.TotalSystemBuffer;
            _burnData.usedSystemBuffer = eventArgs.UsedSystemBuffer;
            _burnData.freeSystemBuffer = eventArgs.FreeSystemBuffer;

            //
            // Report back to the UI
            //
            _backgroundBurnWorker.ReportProgress(0, _burnData);
        }
        #endregion BurnImage Code

        #region File System Process
        private bool CreateMediaFileSystem(IDiscRecorder2 discRecorder, object[] multisessionInterfaces, out IStream dataStream)
        {
            MsftFileSystemImage fileSystemImage = null;
            try
            {
                MsftDiscFormat2Data discFormatData = new MsftDiscFormat2Data();
                fileSystemImage = new MsftFileSystemImage();
                fileSystemImage.ChooseImageDefaults(discRecorder);
                fileSystemImage.FileSystemsToCreate =
                    FsiFileSystems.FsiFileSystemJoliet | FsiFileSystems.FsiFileSystemISO9660;
                if (discFormatData.IsCurrentMediaSupported(discRecorder))
                {
                    discFormatData.Recorder = discRecorder;
                    IMAPI_MEDIA_PHYSICAL_TYPE mediaType = discFormatData.CurrentPhysicalMediaType;
                    fileSystemImage.ChooseImageDefaultsForMediaType(mediaType);
                }
                fileSystemImage.Update += fileSystemImage_Update;

                //
                // If multisessions, then import previous sessions
                //
                /*
                if (multisessionInterfaces != null)
                {
                    fileSystemImage.MultisessionInterfaces = discFormatData.MultisessionInterfaces;// multisessionInterfaces;
                    fileSystemImage.ImportFileSystem();
                }
                */
                if (!discFormatData.MediaHeuristicallyBlank)
                {
                    fileSystemImage.MultisessionInterfaces = discFormatData.MultisessionInterfaces;
                    fileSystemImage.ImportFileSystem();
                }
                else
                {
                    fileSystemImage.ChooseImageDefaults(discRecorder);
                    fileSystemImage.FileSystemsToCreate = FsiFileSystems.FsiFileSystemUDF;
                }

                //
                // Get the image root
                //
                IFsiDirectoryItem rootItem = fileSystemImage.Root;

                //
                // Add Files and Directories to File System Image
                //
                var Ite1 = _lstDir.ToList();
                //foreach (IMediaItem mediaItem in listBoxFiles.Items)
                foreach (IMediaItem mediaItem in Ite1)
                {
                    //
                    // Check if we've cancelled
                    //
                    if (_backgroundBurnWorker.CancellationPending)
                    {
                        break;
                    }

                    //
                    // Add to File System
                    //
                    mediaItem.AddToFileSystem(rootItem);
                }

                fileSystemImage.Update -= fileSystemImage_Update;

                //
                // did we cancel?
                //
                if (_backgroundBurnWorker.CancellationPending)
                {
                    dataStream = null;
                    return false;
                }

                dataStream = fileSystemImage.CreateResultImage().ImageStream;
            }
            catch (COMException exception)
            {

                dataStream = null;
                return false;
            }
            finally
            {
                if (fileSystemImage != null)
                {
                    Marshal.ReleaseComObject(fileSystemImage);
                }
            }

            return true;
        }

        /// <summary>
        /// Event Handler for File System Progress Updates
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="currentFile"></param>
        /// <param name="copiedSectors"></param>
        /// <param name="totalSectors"></param>
        void fileSystemImage_Update([In, MarshalAs(UnmanagedType.IDispatch)] object sender,
            [In, MarshalAs(UnmanagedType.BStr)]string currentFile, [In] int copiedSectors, [In] int totalSectors)
        {
            var percentProgress = 0;
            if (copiedSectors > 0 && totalSectors > 0)
            {
                percentProgress = (copiedSectors * 100) / totalSectors;
            }

            if (!string.IsNullOrEmpty(currentFile))
            {
                var fileInfo = new FileInfo(currentFile);
                _burnData.statusMessage = "Adding \"" + fileInfo.Name + "\" to image...";

                //
                // report back to the ui
                //
                _burnData.task = BURN_MEDIA_TASK.BURN_MEDIA_TASK_FILE_SYSTEM;
                _backgroundBurnWorker.ReportProgress(percentProgress, _burnData);
            }

        }
        #endregion

        #region Format/Erase the Disc - Unused

        /// <summary>
        /// Worker thread that Formats the Disc
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void backgroundFormatWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            MsftDiscRecorder2 discRecorder = null;
            MsftDiscFormat2Erase discFormatErase = null;
            try
            {
                //
                // Create and initialize the IDiscRecorder2
                //
                discRecorder = new MsftDiscRecorder2();
                var activeDiscRecorder = (string)e.Argument;
                discRecorder.InitializeDiscRecorder(activeDiscRecorder);

                //
                // Create the IDiscFormat2Erase and set properties
                //
                discFormatErase = new MsftDiscFormat2Erase
                {
                    Recorder = discRecorder,
                    ClientName = _clientName,

                };

                //
                // Setup the Update progress event handler
                //
                discFormatErase.Update += discFormatErase_Update;

                //
                // Erase the media here
                //
                try
                {
                    discFormatErase.EraseMedia();
                    e.Result = 0;
                }
                catch (COMException ex)
                {

                }

                //
                // Remove the Update progress event handler
                //
                discFormatErase.Update -= discFormatErase_Update;
            }
            catch (COMException exception)
            {
                //
                // If anything happens during the format, show the message
                //
                ErrorHandler.ErrorHandler.LogError(exception);
            }
            finally
            {
                if (discRecorder != null)
                {
                    Marshal.ReleaseComObject(discRecorder);
                }

                if (discFormatErase != null)
                {
                    Marshal.ReleaseComObject(discFormatErase);
                }
            }
        }

        /// <summary>
        /// Event Handler for the Erase Progress Updates
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="elapsedSeconds"></param>
        /// <param name="estimatedTotalSeconds"></param>
        void discFormatErase_Update([In, MarshalAs(UnmanagedType.IDispatch)] object sender, int elapsedSeconds, int estimatedTotalSeconds)
        {
            var percent = elapsedSeconds * 100 / estimatedTotalSeconds;
            //
            // Report back to the UI
            //
            //backgroundFormatWorker.ReportProgress(percent);
        }

        private void backgroundFormatWorker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {

        }

        private void backgroundFormatWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {


            _isFormatting = false;

        }
        #endregion
        private void UpdateVOS()
        {
            //var SR = Application.Current.Windows
            //        .Cast<Window>()
            //        .FirstOrDefault(window => window is SearchResult) as SearchResult;
            //if (SR != null)
            //    SR.lblPendingOrders.Text = GetPendingBurnOrders();
            string tempValue = GetPendingBurnOrders();
            foreach (Window wnd in Application.Current.Windows)
            {
                if (wnd.Title == "View/Order Station")
                {
                    var window = wnd as SearchResult;
                    window.lblPendingOrders.Text = tempValue;
                }
            }


        }
        private string GetPendingBurnOrders()
        {
            string nextOrdDetail = "";
            string OrdNum = ""; int ProdType;
            int USBcount = 0; int CDcount = 0;
            //DigiPhotoDataServices _objdatalayer = new DigiPhotoDataServices();
            //var ListOfItems = _objdatalayer.GetPendingBurnOrders(true);
            var ListOfItems = (new OrderBusiness()).GetPendingBurnOrders(true);
            foreach (var IndividualItem in ListOfItems)
            {
                if (IndividualItem.Status != 1)
                {
                    if (isLocalOrder(IndividualItem.OrderNumber, IndividualItem.ProductType))
                    {
                        OrdNum = IndividualItem.OrderNumber;
                        ProdType = IndividualItem.ProductType;
                        if (ProdType == 36)
                        {
                            USBcount++;
                        }
                        else
                        {
                            CDcount++;
                        }
                    }
                }
            }
            nextOrdDetail += "USB(" + USBcount.ToString() + ") CD(" + CDcount.ToString() + ")";
            return nextOrdDetail;
        }
        private void btnLoadOrders_Click(object sender, RoutedEventArgs e)
        {
            if (!BurnOrderElements.isExecuting)
                LoadBurnOrders();
        }
        private void btnCancelBurn_Click(object sender, RoutedEventArgs e)
        {
            Button btn = sender as Button;
            int ord = btn.Tag.ToInt32();
            //DigiPhotoDataServices _objdatalayer = new DigiPhotoDataServices();
            (new OrderBusiness()).UpdateBurnOrderStatus(ord, Convert.ToInt32(StatusCode.Success), LoginUser.UserId, (new CustomBusineses()).ServerDateTime());
            GetPendingBurnOrderDetails(true);

        }


    }
}
