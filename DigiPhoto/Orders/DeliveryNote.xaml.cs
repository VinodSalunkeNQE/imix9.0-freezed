﻿using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using DigiPhoto.Common;
using DigiPhoto.Culture;
using DigiPhoto.DataLayer;
using DigiPhoto.IMIX.Business;
using DigiPhoto.IMIX.Model;
using DigiPhoto.Utility.Repository.ValueType;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Resources;
using System.Windows;

namespace DigiPhoto.Orders
{
    /// <summary>
    /// Interaction logic for DeliveryNote.xaml
    /// </summary>
    public partial class DeliveryNote : Window
    {
        ReportDocument _report = new ReportDocument();
        private string _imagedetails = string.Empty;
        public bool IsGSTEnabled = false;
        bool iSSGSTCGST = Convert.ToBoolean(ConfigurationManager.AppSettings["ISSGSTCGST"]);

        public DeliveryNote(string strName, string opName, string orderNo, string photos, string refund, string amntDue, string txtCash, string txtChange, List<LinetItemsDetails> itemDetails, string paymentType, string cardNumber, string cardHolderName, string customerName, string hotelName, string roomNo, string voucherNo, string currency, int orderId, Boolean IsReprint, double totalDiscount = 0.00, string qrCodePrint = "")
        {
            string[] orderlines;
            string stroyBookId = "";
            string storyBookOrderNo = "";
            if (orderNo.Contains("\r\n"))
            {
                string[] stringSeparators = new string[] { "\r\n" };
                orderlines = orderNo.Split(stringSeparators, StringSplitOptions.None);
                orderNo = orderlines[0];
                stroyBookId = orderlines[1];
                storyBookOrderNo = orderlines[0];
            }      

            InitializeComponent();            
            double discountOnTotal = totalDiscount - itemDetails.Sum(x => x.Discount);
            int tax_result = 0;
            double totalAmount = Convert.ToDouble(discountOnTotal + Double.Parse(amntDue));
            string barcodeId = orderNo.Replace("DG-", "");
            string applicationPath = AppDomain.CurrentDomain.BaseDirectory;
            BarCodeGenerator.Code39 baracode = new BarCodeGenerator.Code39(barcodeId);
            Bitmap img = new Bitmap(baracode.Paint());
            StoreInfo store = new StoreInfo();
            TaxBusiness taxBusiness = new TaxBusiness();
            store = taxBusiness.getTaxConfigData();
            GetStoreConfigData();
            TaxDetailInfo taxInfo = new TaxDetailInfo();
            ///Priyanka code to chech ISGSTActive from database on 10 july 2019
            GetAllTaxDetails();

            /////changed by latika for evoucher 4March2020
            EvoucherDtlsRpt EvlstDtls = new EvoucherDtlsRpt();
            EvlstDtls = taxBusiness.GetEvocuherDtls(orderId);
            /////end by latika
            if (IsGSTEnabled == true && iSSGSTCGST == true) // Code added by Anis for IsGSTEnabled
            {
                tax_result = taxBusiness.Get_TaxPercent();
                tax_result = tax_result / 2;
            }
            List<TaxDetailInfo> taxList = taxBusiness.GetTaxDetail(orderId);
            string rptArabic = ConfigurationManager.AppSettings["IsArabic"];
            string rptChina = ConfigurationManager.AppSettings["IsChina"];
            ///End Priyanka
            // Added by Anisur Rahman for Dubai frame (Product code + Barcode) receipt requirement
            string productExtentionCode = ConfigurationManager.AppSettings["ProductExtentionCode"];
            string isProductCodeWithBarCode = ConfigurationManager.AppSettings["IsProductCodeWithBarCode"];
            string imgPath = string.Empty;
            Bitmap imgpCode = null;
            string applicationPathPcode = string.Empty;
            //End Here
            //Manoj need to add code for Delivery Note            
            //bool isDeliveryNote = new ConfigBusiness().GetDeliveryNoteStatus(LoginUser.SubStoreId).ToBoolean();

            try
            {
                try
                {
                    img.Save(Environment.CurrentDirectory + "\\Reports\\myimg.jpg");
                }
                catch (Exception ex)
                {
                    img.Dispose();
                    GC.Collect();
                    img = new Bitmap(baracode.Paint());
                }

                _report.Load(System.IO.Path.Combine(applicationPath, "Reports", "DeliveryOrder.rpt"));
                //if (isDeliveryNote)
                //{
                //    if (rptChina == "1")
                //    {
                //        _report.Load(System.IO.Path.Combine(applicationPath, "Reports", "DeliveryOrder.rpt"));
                //    }
                //}
                ErrorHandler.ErrorHandler.LogFileWrite("105");
                OrderBusiness OBuzz = new OrderBusiness();
                List<ProductNameInfo> productInfo = new List<ProductNameInfo>();
                DataTable dt = new DataTable();
                dt.Clear();
                dt.Columns.Add("ProductName");
                dt.Columns.Add("Quantity");
                dt.Columns.Add("UnitPrice");
                dt.Columns.Add("TotalPrice");
                dt.Columns.Add("ProductNameCh");
                dt.Columns.Add("Unit");
                dt.Columns.Add("ProductNumber");
                //This is for ALL reports

                if (!string.IsNullOrEmpty(stroyBookId))
                {
                    TextObject Ordernumber = (TextObject)_report.ReportDefinition.Sections["Section1"].ReportObjects["txtorderno"];
                    orderNo = orderNo + "\r\n" + stroyBookId;
                    Ordernumber.Text = Convert.ToString(orderNo);
                }
                else
                {
                    TextObject Ordernumber = (TextObject)_report.ReportDefinition.Sections["Section1"].ReportObjects["txtorderno"];
                    Ordernumber.Text = Convert.ToString(orderNo);
                }               
                TextObject Oprator = (TextObject)_report.ReportDefinition.Sections["Section1"].ReportObjects["txtoperator"];
                Oprator.Text = Convert.ToString(opName);
                TextObject SiteName = (TextObject)_report.ReportDefinition.Sections["Section1"].ReportObjects["Txtstorename"];
                SiteName.Text = Convert.ToString(strName);
                string SiteNameCN = OBuzz.GetSiteNameDetails(strName);
                TextObject SiteNamech = (TextObject)_report.ReportDefinition.Sections["Section1"].ReportObjects["Txtstorech"];
                SiteNamech.Text = SiteNameCN;
                Double TotAmount = 0;
                foreach (var items in itemDetails)
                {
                    string ProductNameCN = string.Empty;
                    string ProductNumber = string.Empty;
                    productInfo = OBuzz.GetProductNameDetails(items.Productname);
                    foreach(ProductNameInfo product in productInfo)
                    {
                        ProductNameCN = product.ProductNameCN;
                        ProductNumber = product.ProductNumber;
                    }
                    DataRow _row = dt.NewRow();

                    _row["ProductName"] = items.Productname;

                    _row["ProductNameCh"] = ProductNameCN;

                    _row["Quantity"] = Convert.ToString(items.Productquantity);

                    _row["UnitPrice"] = Convert.ToString(items.Productprice);

                    Double TotalAmt = Convert.ToDouble(items.Productquantity) * Convert.ToDouble(items.Productprice);

                    _row["TotalPrice"] = Convert.ToDouble(TotalAmt).ToString("#.00");

                    _row["Unit"] = "¥";

                    _row["ProductNumber"] = ProductNumber;

                    TotAmount = TotAmount + TotalAmt;
                    dt.Rows.Add(_row);
                }

                ErrorHandler.ErrorHandler.LogFileWrite("170");

                TextObject TotalAmountcal = (TextObject)_report.ReportDefinition.Sections["Section4"].ReportObjects["txtTotal"];
                TotalAmountcal.Text = Convert.ToDouble(TotAmount).ToString("#.00");

                _report.SetDataSource(dt);

                _report.Refresh();
                _report.PrintOptions.PrinterName = Common.LoginUser.ReceiptPrinterPath;

                // For Single Single Print
                int noOfRecipt = GetNoOfBillReceipt();

                for (int copy = 0; copy < noOfRecipt; copy++)
                {
                    CrystalDecisions.Shared.PageMargins margin = new CrystalDecisions.Shared.PageMargins(); ///changed by latika 20191019 for duplicate print mergine issue
                    if (IsReprint == true)
                    {  ///changed by latika 20191019 for duplicate print mergine issue
                        margin.leftMargin = 0;
                        margin.rightMargin = 0;
                        margin.topMargin = 0;
                        margin.bottomMargin = 0;
                        _report.PrintOptions.ApplyPageMargins(margin);
                        ////end
                        //BillType.Text = "Duplicate";
                        _report.PrintToPrinter(1, true, 1, 0);
                        return;
                    }

                    margin.leftMargin = 0;
                    margin.rightMargin = 0;
                    margin.topMargin = 0;
                    //Added condition for arabic translation
                    if (rptChina != "0")
                    {
                        margin.bottomMargin = 0;
                    }
                    else
                    {
                        margin.bottomMargin = 0;
                    }
                    //margin.bottomMargin = 0;
                    _report.PrintOptions.ApplyPageMargins(margin);
                    _report.PrintToPrinter(1, true, 1, 0);
                }
                if (!IsReprint)
                {
                    if (!string.IsNullOrEmpty(stroyBookId))
                    {
                        SaveOrderNo(storyBookOrderNo);
                        string filepathSB = Common.LoginUser.DigiFolderPath;
                        string filepathdateSB = System.IO.Path.Combine(filepathSB, "OrderReceipt\\" + DateTime.Now.ToString("yyyyMMdd"));
                        if (!Directory.Exists(filepathdateSB))
                            Directory.CreateDirectory(filepathdateSB);

                        string filenameSB = string.Empty;
                        filenameSB = storyBookOrderNo + ".pdf";
                        //END
                        _report.ExportToDisk(ExportFormatType.PortableDocFormat, filenameSB);
                        string SBsourceFile = Environment.CurrentDirectory + "\\" + filenameSB;
                        if (File.Exists(SBsourceFile))
                        {
                            File.Move(SBsourceFile, filepathdateSB + "\\" + filenameSB);
                        }
                    }
                    else
                    {
                        SaveOrderNo(orderNo);
                        //Save receipt into File.
                        string filepath = Common.LoginUser.DigiFolderPath;
                        string filepathdate = System.IO.Path.Combine(filepath, "OrderReceipt\\" + DateTime.Now.ToString("yyyyMMdd"));
                        if (!Directory.Exists(filepathdate))
                            Directory.CreateDirectory(filepathdate);

                        string filename = string.Empty;
                        filename = orderNo + ".pdf";
                        //END
                        _report.ExportToDisk(ExportFormatType.PortableDocFormat, filename);
                        string sourceFile = Environment.CurrentDirectory + "\\" + filename;
                        if (File.Exists(sourceFile))
                        {
                            File.Move(sourceFile, filepathdate + "\\" + filename);
                        }
                    }                
                   
                   
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
            }
            finally
            {
                if (img != null)
                {
                    IntPtr deleteObject = img.GetHbitmap();
                    //DeleteObject(deleteObject);
                    img.Dispose();
                }
                // Added by Anisur Rahman for Dubai frame (Product code + Barcode) receipt requirement    
                if (isProductCodeWithBarCode.ToLower() == "true")
                {

                    if (imgpCode != null)
                    {
                        IntPtr deleteObject = imgpCode.GetHbitmap();
                        //DeleteObject(deleteObject);
                        imgpCode.Dispose();
                    }
                    string[] files = Directory.GetFiles(applicationPathPcode + "Barcode\\");
                    {
                        if (files != null)
                        {
                            foreach (string file in files)
                            {
                                File.Delete(file);
                            }
                        }
                    }
                }
                // End Here
                baracode = null;
                cryDeliveryNote = null;
                _report.Dispose();
                GC.Collect(); // Code added by Latika to release the memory
            }
        }

        private void GetAllTaxDetails()
        {
            try
            {
                List<TaxDetailInfo> lstTaxDetails = new List<TaxDetailInfo>();
                TaxBusiness TaxData = new TaxBusiness();
                lstTaxDetails = TaxData.GetAllTaxDetails();
                var result = lstTaxDetails.Where(x => x.TaxName == "GST" && x.IsActive == true).ToList();
                foreach (var item in result)
                {
                    IsGSTEnabled = item.IsActive;
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
            }
        }
        void SaveOrderNo(string OrderNo)
        {
            try
            {
                string orders = string.Empty;
                string pathtosave = Environment.CurrentDirectory;
                //if exists get all orders
                if (File.Exists(pathtosave + "\\on.dat"))
                {
                    using (StreamReader reader = new StreamReader(Environment.CurrentDirectory + "\\on.dat"))
                    {
                        string line = reader.ReadLine();
                        orders = CryptorEngine.Decrypt(line, true);
                    }
                    File.Delete(pathtosave + "\\on.dat");
                }
                while (orders.Split(',').Count() > 2)
                    orders = orders.Remove(0, orders.IndexOf(',') + 1);

                orders += string.IsNullOrEmpty(orders) ? OrderNo : "," + OrderNo;
                using (StreamWriter b = new StreamWriter(File.Open(pathtosave + "\\on.dat", FileMode.Create)))
                {
                    b.Write(CryptorEngine.Encrypt(orders, true));
                    b.Close();
                }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }
        string logoImgName = string.Empty;
        private void GetStoreConfigData()
        {
            ConfigBusiness conBiz = new ConfigBusiness();
            List<iMIXStoreConfigurationInfo> ConfigValuesList = conBiz.GetStoreConfigData();

            for (int i = 0; i < ConfigValuesList.Count; i++)
            {
                switch (ConfigValuesList[i].IMIXConfigurationMasterId)
                {
                    case (int)ConfigParams.ReceiptLogoPath:
                        logoImgName = ConfigValuesList[i].ConfigurationValue != null ? System.IO.Path.Combine(ConfigManager.DigiFolderPath, "ReceiptLogo", ConfigValuesList[i].ConfigurationValue) : string.Empty;
                        break;

                }
            }

        }
        private int GetNoOfBillReceipt()
        {
            //Default is set as 1 
            int NoOfBillReceipt = 1;
            try
            {
                //NoOfBillReceipt = Convert.ToInt32(_objDigiPhotoDataServices.GetConfigurationData(LoginUser.SubStoreId).DG_NoOfBillReceipt);
                NoOfBillReceipt = new ConfigBusiness().GetConfigurationData(LoginUser.SubStoreId).DG_NoOfBillReceipt.ToInt32();
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
            }
            return NoOfBillReceipt;
        }
        public DataTable GetDataTablefromStream(string xmlData)
        {
            xmlData = xmlData.Replace("&", "&amp;");
            DataSet dataSet = new DataSet();
            DataTable dataTable = new DataTable("Item");
            dataTable.Columns.Add("Name", typeof(string));
            dataTable.Columns.Add("ArabicName", typeof(string));
            dataTable.Columns.Add("ClaimCode", typeof(string));
            dataTable.Columns.Add("QRCode", typeof(string));
            dataTable.Columns.Add("Qty", typeof(string));
            dataTable.Columns.Add("Price", typeof(string));
            dataTable.Columns.Add("Disc", typeof(string));
            dataTable.Columns.Add("ProductNamelbl", typeof(string));
            dataTable.Columns.Add("Qtylbl", typeof(string));
            dataTable.Columns.Add("Pricelbl", typeof(string));
            dataTable.Columns.Add("Disclbl", typeof(string));
            dataTable.Columns.Add("Currency", typeof(string));
            dataTable.Columns.Add("TotalVal", typeof(string));
            dataTable.Columns.Add("ProductCode", typeof(string)); // Added by Anisur Rahman for Dubai frame (Product code + Barcode) receipt requirement
            dataSet.Tables.Add(dataTable);

            System.IO.StringReader xmlSR = new System.IO.StringReader(xmlData);

            dataSet.ReadXml(xmlSR, XmlReadMode.IgnoreSchema);
            return dataSet.Tables[0];
        }
    }    
}
