﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Collections.ObjectModel;
using DigiPhoto.DataLayer.Model;
using DigiPhoto.DataLayer;
using System.Text.RegularExpressions;
using DigiPhoto.Orders;
using System.IO;
using System.Printing;
using XPBurn;
using CrystalDecisions.Shared;
using CrystalDecisions.CrystalReports.Engine;
using DigiPhoto.Common;
using DigiAuditLogger;
using BurnMedia;
using System.Runtime.InteropServices;
using System.ComponentModel;
using System.Runtime.InteropServices.ComTypes;
using System.Drawing.Imaging;
using DigiPhoto.IMIX.Business;
using FrameworkHelper;
using DigiPhoto.IMIX.Model;
using DigiPhoto.Utility.Repository.ValueType;
using System.Security.Cryptography;
using System.Threading;
using System.Diagnostics;
using System.Collections;
using static DigiPhoto.IMIX.Model.Product;
using System.Windows.Forms.Integration;
using ZXing;
using System.Drawing;
using DigiPhoto.ViewModel;
using System.Configuration;
using System.Threading.Tasks;

namespace DigiPhoto.Orders
{
    public partial class Payment : Window
    {
        #region Variables
        public bool _issemiorder;
        private ObservableCollection<PaymentItem> _objPayment;
        private ObservableCollection<PaymentItem> _objCardPayment;
        private ObservableCollection<PaymentItem> _objRoomPayment;
        private ObservableCollection<PaymentItem> _objVoucherPayment;
        private ObservableCollection<PaymentItem> _objKVLPayment;
        private List<string> _lstPaymentMethod;
        TextBox controlon;
        //private List<DG_Currency> _lstCurrency;
        private List<CurrencyInfo> _lstCurrency;
        private double _totalBillAmount;
        private double _totalCardAmount = 0;
        private double _totalCashAmount = 0;
        private double _totalGrandAmount;
        private double _totalBalanceAmount;
        private double _totalAmount;
        private double _totalBillDiscount;
        private string _billDiscountDetails;
        private bool _card = false;
        private bool _cash = false;
        private bool _isEnableSlipPrint = true;
        //private DigiPhotoDataServices _objDataLayer = null;
        private int _defaultCurrencyId;
        private bool _isEventEmailPackage = false;
        private double _EvoucherAmnt;/// added by latika for Evoucher Amount
        private string _StroyBookId;
        #endregion

        #region Properties
        public double PaybleAmount
        {
            set
            {
                _totalBillAmount = value;
            }
        }
        public double TotalBill
        {
            set
            {
                _totalAmount = value;
            }
        }
        public double TotalDiscount
        {
            set
            {
                _totalBillDiscount = value;
            }
        }
        public String DiscountDetails
        {
            get
            {
                return _billDiscountDetails;
            }
            set
            {
                _billDiscountDetails = value;
            }
        }
        public Window SourceParent
        {
            get;
            set;
        }
        public string DefaultCurrency
        {
            get;
            set;
        }
        public ObservableCollection<LineItem> ImagesToPrint
        {
            get;
            set;
        }
        public double EvoucherAmnt
        {
            get
            {
                return _EvoucherAmnt;
            }
            set
            {
                _EvoucherAmnt = value;
            }
        }

        #endregion

        #region Constructor
        public Payment()
        {
            InitializeComponent();
            _lstCurrency = new List<CurrencyInfo>();
            _objPayment = new ObservableCollection<PaymentItem>();
            _objCardPayment = new ObservableCollection<PaymentItem>();
            _objRoomPayment = new ObservableCollection<PaymentItem>();
            _objVoucherPayment = new ObservableCollection<PaymentItem>();
            _objKVLPayment = new ObservableCollection<PaymentItem>();
            dgPayment.ItemsSource = _objPayment;
            dgCardPayment.ItemsSource = _objCardPayment;
            dgRoomPayment.ItemsSource = _objRoomPayment;
            dgVoucherPayment.ItemsSource = _objVoucherPayment;
            dgKVLPayment.ItemsSource = _objKVLPayment;

            CtrlCashPayment.SetParent(modelparent);
            CtrlCardPayment.SetParent(modelparent);
            MailPopUp.SetParent(PaymentPopup);
            bool showPaymentScreen = false;
            //if (_objDataLayer == null)
            //    _objDataLayer = new DigiPhotoDataServices();
            //Added for Jira task DIGIPHOTO-4
            //List<iMIXConfigurationValue> _objdata = _objDataLayer.GetNewConfigValues(LoginUser.SubStoreId);
            List<iMIXConfigurationInfo> objdata = (new ConfigBusiness()).GetNewConfigValues(LoginUser.SubStoreId);
            foreach (iMIXConfigurationInfo imixConfigValue in objdata)
            {
                switch (imixConfigValue.IMIXConfigurationMasterId)
                {
                    case (int)ConfigParams.EnablePaymentScreenOnClientView:
                        showPaymentScreen = imixConfigValue.ConfigurationValue.ToBoolean();
                        break;
                    case (int)ConfigParams.Card:
                        _card = imixConfigValue.ConfigurationValue.ToBoolean();
                        break;
                    case (int)ConfigParams.Cash:
                        _cash = imixConfigValue.ConfigurationValue.ToBoolean();
                        break;
                    case (int)ConfigParams.IsEnableSlipPrint:
                        _isEnableSlipPrint = string.IsNullOrWhiteSpace(imixConfigValue.ConfigurationValue) ? true : Convert.ToBoolean(imixConfigValue.ConfigurationValue);
                        break;
                    case (int)ConfigParams.IsEventEmailPackage:
                        _isEventEmailPackage = string.IsNullOrWhiteSpace(imixConfigValue.ConfigurationValue) ? true : Convert.ToBoolean(imixConfigValue.ConfigurationValue);
                        break;
                }
            }

            if (showPaymentScreen == true)
                ShowPaymentScreenToClient();

            btnNextsimg.IsDefault = false;
        }
        #endregion

        #region Private Methods
        private void InitializeValue()
        { /////////changed by latika for Evoucher 2019
            StringBuilder sb = new StringBuilder();
            if (EvoucherAmnt > 0)
            {
                BdrEvocuhereDisc.Visibility = Visibility.Visible;
                txtEvoucherAmount.Text = EvoucherAmnt.ToString("N2");

                DiscountBusiness sa = new DiscountBusiness();

                var Result = from discount in new DiscountBusiness().GetDiscountType()
                             where discount.IsEvoucher == true
                             select discount;
                sb.Append("<Discount TotalPrice = '" + _totalBillAmount + "'>");
                foreach (var item in Result)
                {

                    sb.Append("<Option Name='" + item.DG_Orders_DiscountType_Name + "' discount='" + EvoucherAmnt + "'  InPercentmode='" + item.DG_Orders_DiscountType_AsPercentage + "' discountid = '" + item.DG_Orders_DiscountType_Pkey + "' discountSyncCode = '" + item.SyncCode + "'/>");
                }
                sb.Append("</Discount>");
                DiscountDetails = sb.ToString();
                _totalBillAmount = _totalBillAmount - EvoucherAmnt;
            }////changed by latika for Evoucher
            double taxAmt = GetTotalTax();
            _totalBillAmount = _totalBillAmount + taxAmt;
            txtBalance.Text = DefaultCurrency.ToString() + " " + _totalBillAmount.ToString("N2");
            txtPayable.Text = txtBalance.Text;
            txtPaid.Text = "0.00 " + DefaultCurrency.ToString();
            txtTotalAmountCreditCard.Text = "0.00";
            TxtAmounttobePaid.Text = DefaultCurrency.ToString() + "  " + _totalBillAmount.ToString("N2");
            tbRoomCurrency.Text = DefaultCurrency.ToString();
            tbAmount.Text = _totalBillAmount.ToString("N2");
            tbName1.Text = _totalBillAmount.ToString("N2");
            tbGiftCurrency.Text = DefaultCurrency.ToString();
            tbKVL.Text = _totalBillAmount.ToString("N2");
            tbKVLCurrency.Text = DefaultCurrency.ToString();
            _totalBillDiscount = _totalBillDiscount + EvoucherAmnt;
            //if (_objDataLayer == null)
            //    _objDataLayer = new DigiPhotoDataServices();
            //_defaultCurrencyId = _objDataLayer.GetDefaultCurrency();

            _defaultCurrencyId = (new CurrencyBusiness()).GetDefaultCurrency();

        }
        private string GenerateOrderNumber()
        {
            string orderNumber = Common.LoginUser.OrderPrefix + "-";
            string uniqueNumber = string.Empty;
            try
            {
                //    Random rand = new Random((int)DateTime.UtcNow.Ticks);
                //    IEnumerable<int> lst = Enumerable.Range(0, 256).OrderBy(_ => rand.NextDouble());
                //    string orderNumber = Common.LoginUser.OrderPrefix + "-" + (new CustomBusineses()).ServerDateTime().Day.ToString();
                //    int counter = 0;
                //    foreach (int num in lst)
                //    {
                //        if (counter < 2)
                //            orderNumber += num;
                //        else
                //            return orderNumber;
                //        counter++;
                //    }
                //    return orderNumber;
                using (RNGCryptoServiceProvider rng = new RNGCryptoServiceProvider())
                {
                    // Buffer storage.
                    byte[] data = new byte[4];

                    // Ten iterations.                    
                    {
                        // Fill buffer.
                        rng.GetBytes(data);

                        // Convert to int 32.
                        int value = BitConverter.ToInt32(data, 0);
                        if (value < 0)
                            value = -value;
                        if (value.ToString().Length > 10)
                        {
                            uniqueNumber = value.ToString().Substring(0, 10);
                        }
                        else
                        {
                            uniqueNumber = value.ToString();
                        }
                    }
                }
            }
            catch (Exception ex)
            { }
            orderNumber = orderNumber + uniqueNumber;
            return orderNumber;
        }

        public IntPtr MainWindowHandle { get; set; }


        [DllImport("user32.dll", SetLastError = true)]
        private static extern long SetParent(IntPtr hWndChild, IntPtr hWndNewParent);

        private bool SaveOrder(out string orderNumber, ref string orderStatus)
        {
            try
            {
                ///int imgCount = 0;//VINS
                #region variables
                List<BurnImagesInfo> burnedImages = new List<BurnImagesInfo>();
                String paymentDetails = string.Empty;
                List<LinetItemsDetails> billDetails = new List<LinetItemsDetails>();
                int paymentMode = -1;
                String outPutImages = string.Empty;
                TabItem ti = (TabItem)tabPayment.SelectedItem;
                string paymentType = string.Empty;
                string cardHolderName = string.Empty;
                string cardNumber = string.Empty;
                string customerName = string.Empty;
                string hotelName = string.Empty;
                string roomNo = string.Empty;
                string voucherNo = string.Empty;
                string destinationPathMobile = string.Empty;
                string destinationPath = Environment.GetFolderPath(Environment.SpecialFolder.MyPictures);
                string source = string.Empty;
                string editedImagePathMobile = string.Empty;


                #endregion

                if (ti != null)
                {
                    #region Condition for Card/Cash flow
                    paymentDetails = "<Payments>";
                    //Check condition for Card/Cash flow
                    //if (ti.Header.ToString() == "Card/Cash")
                    //{
                    //Validate whether user has selected one tab or not
                    if (_objPayment.Count == 0 && _objCardPayment.Count == 0 && _objRoomPayment.Count == 0 && _objVoucherPayment.Count == 0 && _objKVLPayment.Count == 0)
                    {
                        MessageBox.Show("Please enter details first or select the Mode/Tab in which you filled the details");
                        orderNumber = string.Empty;
                        orderStatus = "CARDFAILED";
                        return false;
                    }
                    else
                    {
                        //Validate whether payment has been done or not
                        if (_totalBalanceAmount > 0)
                        {
                            MessageBox.Show("Please pay remaining " + this.DefaultCurrency.ToString() + " " + _totalBalanceAmount.ToString("N2") + " before continue.");
                            orderNumber = string.Empty;
                            orderStatus = "PAYREMAINING";
                            return false;
                        }

                        int counter = 0;
                        //Create transaction xml for Cash Amount
                        _lstPaymentMethod = new List<string>();
                        if (dgPayment.Items.Count != 0)
                        {
                            _lstPaymentMethod.Add("CASH");
                            paymentMode = (Int32)DigiPhoto.DataLayer.PaymentMode.Cash;
                            paymentType = "CASH";
                            foreach (PaymentItem item in _objPayment)
                            {
                                DataGridRow row = (DataGridRow)dgPayment.ItemContainerGenerator.ContainerFromIndex(counter);
                                if (row != null)
                                {
                                    double amount;
                                    TextBlock tx = FindByName("txtAmountEntered", row) as TextBlock;
                                    TextBlock tx1 = FindByName("txtNetPrice", row) as TextBlock;
                                    TextBlock tx2 = FindByName("txtCurrencyID", row) as TextBlock;

                                    //if (Double.TryParse(tx1.Text, out amount))
                                    //{
                                    //    paymentDetails += "<Payment Mode = 'cash' Amount = '" + item.PaidAmount.ToString() + "' OrignalAmount = '" + item.PaidAmount.ToString() + "' CurrencyID = '" + tx2.Text + "' CurrencySyncCode = '" + item.CurrencySyncCode + "' BaseCurrencyID = '" + _defaultCurrencyId + "' BaseCurrencyAmount = '" + tx1.Text + "'/>";
                                    //}

                                    if (_defaultCurrencyId != tx2.Text.ToInt32())
                                    {
                                        if (Double.TryParse(tx1.Text, out amount))
                                            paymentDetails += "<Payment Mode = 'FC' Amount = '" + Convert.ToDecimal(tx1.Text).ToString() + "' OrignalAmount = '" + Convert.ToDecimal(tx1.Text).ToString() + "' CurrencyID = '" + _defaultCurrencyId + "' CurrencyCode = '" + item.CurrencyCode + "' CurrencySyncCode = '" + item.CurrencySyncCode + "' FCID = '" + tx2.Text + "' FCAmount = '" + Convert.ToDecimal(item.PaidAmount).ToString() + "'/>";
                                    }
                                    else
                                    {
                                        if (Double.TryParse(tx1.Text, out amount))
                                            paymentDetails += "<Payment Mode = 'cash' Amount = '" + Convert.ToDecimal(item.PaidAmount).ToString() + "' OrignalAmount = '" + Convert.ToDecimal(item.PaidAmount).ToString() + "' CurrencyID = '" + tx2.Text + "' CurrencyCode = '" + item.CurrencyCode + "' CurrencySyncCode = '" + item.CurrencySyncCode + "'/>";
                                    }
                                }
                                counter++;
                            }
                        }

                        //Create transaction xml for Card Amount
                        if (dgCardPayment.Items.Count != 0)
                        {
                            _lstPaymentMethod.Add("CARD");
                            paymentMode = (Int32)DigiPhoto.DataLayer.PaymentMode.Card;
                            paymentType = "CARD";
                            counter = 0;
                            foreach (PaymentItem item in _objCardPayment)
                            {
                                DataGridRow row = (DataGridRow)dgCardPayment.ItemContainerGenerator.ContainerFromIndex(counter);
                                if (row != null)
                                {
                                    paymentDetails += "<Payment Mode = 'card' Amount = '" + Convert.ToDecimal(item.PaidAmount).ToString() + "' CurrencyID = '" + item.Currency.ToString() + "' CurrencyCode = '" + DefaultCurrency + "' CurrencySyncCode = '" + item.CurrencySyncCode.ToString() + "' CardMonth = '" + item.CardMonth.ToString() + "' CardYear = '" + item.CardYear.ToString() + "' CardNumber = '" + item.CardNumber.ToString() + "' CardType = '" + item.CardType.ToString() + "' />";
                                    //cardHolderName = item.CardHolderName.ToString();
                                    cardNumber = item.CardNumber.ToString();
                                }
                                counter++;
                            }
                        }

                        #endregion
                        #region Condition for Room Charges
                        if (dgRoomPayment.Items.Count != 0)
                        {
                            _lstPaymentMethod.Add("ROOM");
                            paymentType = "ROOM";
                            paymentMode = (Int32)DigiPhoto.DataLayer.PaymentMode.RoomCharges;
                            // paymentDetails += "<Payment Mode = 'Room' Amount = '" + tbAmount.Text.ToString() + "' HotelName = '" + tbHotelName.Text + "' RoomNumber = '" + tbRoomNumber.Text.ToString() + "' Name = '" + tbName.Text.ToString() + "' />";
                            //customerName = tbName.Text.ToString();
                            //hotelName = tbHotelName.Text.ToString();
                            //roomNo = tbRoomNumber.Text.ToString();

                            //paymentMode = (Int32)DigiPhoto.DataLayer.PaymentMode.Card;
                            //paymentType = "ROOM";
                            counter = 0;
                            foreach (PaymentItem item in _objRoomPayment)
                            {
                                DataGridRow row = (DataGridRow)dgRoomPayment.ItemContainerGenerator.ContainerFromIndex(counter);
                                if (row != null)
                                {
                                    paymentDetails += "<Payment Mode = 'Room' Amount = '" + Convert.ToDecimal(item.PaidAmount).ToString() + "' CurrencyID = '" + item.Currency.ToString() + "' CurrencyCode = '" + DefaultCurrency + "' CurrencySyncCode = '" + item.CurrencySyncCode.ToString() + "' HotelName = '" + item.HotelName + "' RoomNumber = '" + item.RoomNumber.ToString() + "' Name = '" + item.CandidateName.ToString() + "' />";
                                    customerName = item.CandidateName.ToString();
                                    hotelName = item.HotelName.ToString();
                                    roomNo = item.RoomNumber.ToString();
                                }
                                counter++;
                            }
                        }
                        #endregion
                        #region Condition for Gift Voucher
                        if (dgVoucherPayment.Items.Count != 0)
                        {
                            _lstPaymentMethod.Add("VOUCHER");
                            paymentMode = (Int32)DigiPhoto.DataLayer.PaymentMode.GiftVoucher;
                            paymentType = "VOUCHER";
                            counter = 0;
                            foreach (PaymentItem item in _objVoucherPayment)
                            {
                                DataGridRow row = (DataGridRow)dgVoucherPayment.ItemContainerGenerator.ContainerFromIndex(counter);
                                if (row != null)
                                {
                                    paymentDetails += "<Payment Mode = 'Voucher' Amount = '" + Convert.ToDecimal(item.PaidAmount).ToString() + "' CurrencyID = '" + item.Currency.ToString() + "' CurrencyCode = '" + DefaultCurrency + "' CurrencySyncCode = '" + item.CurrencySyncCode.ToString() + "' VoucherNumber = '" + item.VoucherNumber.ToString() + "' CustomerName = '" + item.CandidateName + "' />";
                                    voucherNo = item.VoucherNumber;
                                    customerName = item.CandidateName;
                                }
                                counter++;
                            }
                        }
                        #endregion
                        #region Condition for KVL
                        if (dgKVLPayment.Items.Count != 0)
                        {
                            _lstPaymentMethod.Add("KVL");
                            //paymentMode = (Int32)DigiPhoto.DataLayer.PaymentMode.GiftVoucher;
                            paymentType = "KVL";
                            counter = 0;
                            foreach (PaymentItem item in _objKVLPayment)
                            {
                                DataGridRow row = (DataGridRow)dgKVLPayment.ItemContainerGenerator.ContainerFromIndex(counter);
                                if (row != null)
                                {
                                    paymentDetails += "<Payment Mode = 'KVL' Amount = '" + Convert.ToDecimal(item.PaidAmount).ToString() + "' CurrencyID = '" + item.Currency.ToString() + "' CurrencyCode = '" + DefaultCurrency + "' CurrencySyncCode = '" + item.CurrencySyncCode.ToString() + "'  />";
                                    //voucherNo = item.VoucherNumber;
                                    //customerName = item.CandidateName;
                                }
                                counter++;
                            }
                        }
                        #endregion

                        #region Condition for cash refund
                        //string[] words = txtBalance.Text.ToString().Split(' ');
                        double ChangeAmt = Convert.ToDouble(txtBalance.Text.ToString().Split(' ')[1]);
                        // decimal cashpaid = 0;
                        if (ChangeAmt < 0)
                        {
                            //    paymentMode = (Int32)DigiPhoto.DataLayer.PaymentMode.GiftVoucher;
                            //    paymentType = "Cash Refund";
                            //    counter = 0;
                            //foreach (PaymentItem item in _objVoucherPayment)
                            //{
                            //    DataGridRow row = (DataGridRow)dgVoucherPayment.ItemContainerGenerator.ContainerFromIndex(counter);
                            //    if (row != null)
                            //    {
                            //double cashrefund = (ChangeAmt > cashpaid) ? cashpaid : cashpaid - ChangeAmt;
                            paymentDetails += "<Payment Mode = 'Cash Refund' Amount = '" + Convert.ToDecimal(ChangeAmt).ToString().Replace(",", "") + "'  />";
                            //    voucherNo = item.VoucherNumber;
                            //    customerName = item.CandidateName;
                            //}
                            //counter++;
                            // }
                        }
                        #endregion
                    }
                    paymentDetails += "</Payments>";
                    orderNumber = GenerateOrderNumber();
                    //OrderInfo orderInfo = null;
                    string empid = (string.IsNullOrEmpty(txtEmpID.Text.Trim()) == false ? txtEmpID.Text.Trim() : "NA");
                    //Save Order with details
                    string syncCode = CommonUtility.GetUniqueSynccode(Convert.ToInt32(ApplicationObjectEnum.Order).ToString().PadLeft(2, '0'), LoginUser.countrycode, LoginUser.Storecode, "14");
                    string newOrderNo = string.Empty;
                    OrderInfo orderInfo = (new OrderBusiness()).GenerateOrder(orderNumber, (decimal)_totalAmount, (decimal)_totalBillAmount, paymentDetails, paymentMode, _totalBillDiscount, DiscountDetails, Common.LoginUser.UserId, this._defaultCurrencyId, "0", syncCode, LoginUser.Storecode, empid);
                    orderNumber = orderInfo.DG_Orders_Number;
                    TaxBusiness buss = new TaxBusiness();
                    int orderId = orderInfo.DG_Orders_pkey;
                    buss.SaveOrderTaxDetails(LoginUser.StoreId, orderInfo.DG_Orders_pkey, LoginUser.SubStoreId);

                    int parentId = -1;
                    if (orderId > 0)
                    {
                        StringBuilder ItemDetail = new StringBuilder();
                        string images = string.Empty;
                        int MaxGap = 0;
                        //if (MaxGap < item.SelectedProductType_Text.Length) Old Condition
                        MaxGap = (int)ImagesToPrint.Where(x => x.SelectedProductType_Text.Length > MaxGap).FirstOrDefault().SelectedProductType_Text.Length;

                        var LineItem = from result in ImagesToPrint
                                       where result.ParentID == result.ItemNumber
                                       select result;

                        String BillLineItems = string.Empty;

                        //int codeType;//Commented by VINS as it's not used 
                        //int selectProductType_Id;//Commented by VINS as it's not used 
                        String qrcode = string.Empty;
                        string errorPhotoId = string.Empty;
                        string qrCodeprint = "";
                        bool isWeChat = false;

                        foreach (LineItem item in LineItem)
                        {
                            #region WhatsApp integration ashirwad
                            if (item.SelectedProductType_ID == 127 || item.SelectedProductType_ID == 128)//Check if WeChat or WhatsApp Product
                            {
                                var childrenW = from result in ImagesToPrint
                                                where result.ParentID == item.ItemNumber && result.IsPackage == false
                                                && (result.SelectedProductType_ID == 127 || result.SelectedProductType_ID == 128)
                                                select result;

                                var lineItemW = childrenW?.ToList().FirstOrDefault();
                                if (lineItemW != null)
                                {
                                    if (lineItemW.SelectedProductType_ID == 127)
                                    {

                                        try
                                        {
                                            foreach (var gitem in lineItemW.GroupItems)
                                            {
                                                if (gitem.IsItemSelected == true)
                                                {
                                                    editedImagePathMobile = gitem.HotFolderPath + "\\EditedImages" + "\\" + gitem.FileName;
                                                    if (File.Exists(editedImagePathMobile))
                                                    {
                                                        source = editedImagePathMobile;
                                                    }
                                                    else
                                                    {
                                                        source = gitem.HotFolderPath + "\\" + gitem.CreatedOn.ToString("yyyyMMdd") + "\\" + gitem.FileName;
                                                    }
                                                    //destinationPathMobile = gitem.HotFolderPath + "\\MobileProduct\\" + gitem.CreatedOn.ToString("yyyyMMdd")  ;
                                                    //original path
                                                    destinationPathMobile = gitem.HotFolderPath + "\\WhatsAppProduct\\" + DateTime.Now.ToString("yyyyMMdd");
                                                    //staging path
                                                    //if (lineItemW.SelectedProductType_ID == 127)
                                                    //{
                                                    //    destinationPathMobile = @"\\172.17.10.8\DigiPhotoApplications\Aquaria KLCC\WhatsApp\" + DateTime.Now.ToString("yyyyMMdd");
                                                    //}
                                                    //if (lineItemW.SelectedProductType_ID == 128)
                                                    //{
                                                    //    destinationPathMobile = @"\\172.17.10.8\DigiPhotoApplications\Aquaria KLCC\We-Chat\" + DateTime.Now.ToString("yyyyMMdd");
                                                    //}
                                                    CopyWhtsAppFiles(source, orderNumber, destinationPathMobile, gitem.FileName);

                                                    int isWhatsAppCloudUpload = 0;
                                                    try
                                                    {
                                                        isWhatsAppCloudUpload = Convert.ToInt32(ConfigurationManager.AppSettings["isWhatsAppCloudUpload"].ToString());
                                                    }
                                                    catch (Exception)
                                                    {

                                                        isWhatsAppCloudUpload = 0;
                                                    }


                                                    if (isWhatsAppCloudUpload == 1)
                                                    {
                                                        //Upload to cloud
                                                        UploadFileInfo obj = new UploadFileInfo
                                                        {
                                                            Title = gitem.FileName,//file.Name, // System.IO.Path.GetFileName(uploadFilePath),
                                                            ImagePath = gitem.BigThumbnailPath, //file.FullName,//uploadFilePath,
                                                            TargetDirectory = "",// filePath + upload.SaveFolderPath,
                                                            ImageDefaultHeight = 500,
                                                            ImageDefaultWidth = 500,
                                                            MediaType = 1,
                                                            PhotoId = gitem.PhotoId

                                                        };
                                                        //CloudinaryUploadInfo objCloudinaryUploadInfo = UploadToCloud.UploadFile(obj, orderNumber, out errorPhotoId);
                                                        UploadFileToCloud(obj, orderNumber);
                                                    }
                                                }
                                            }
                                        }
                                        catch (Exception ex)
                                        {
                                            string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                                            ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                                            MessageBox.Show("Error while placing WhatsAPP order. Image not found in HOT folder.");
                                            orderStatus = string.Empty;
                                            return false;
                                        }
                                        CtrlMobileNumber.SetParent(this);
                                        btnNextsimg.IsDefault = false;
                                        CtrlMobileNumber.btnSubmit.IsDefault = true;
                                        var res = CtrlMobileNumber.ShowHandlerDialog("Cash", lineItemW.GroupItems, orderNumber);
                                        btnNextsimg.IsDefault = true;
                                        CtrlMobileNumber.btnSubmit.IsDefault = false;
                                    }

                                    string generatedorder = string.Empty;
                                    string commaSepImages = string.Empty;
                                    string hotFolderPath = string.Empty;

                                    //WeChat product
                                    //Added by Vinod Salunke                                
                                    if (lineItemW.SelectedProductType_ID == 128)
                                    {
                                        foreach (var gitem in lineItemW.GroupItems)
                                        {
                                            if (string.IsNullOrEmpty(commaSepImages))
                                            {
                                                commaSepImages = gitem.FileName;
                                            }
                                            else
                                            {
                                                commaSepImages = commaSepImages + "," + gitem.FileName;
                                            }
                                            editedImagePathMobile = gitem.HotFolderPath + "\\EditedImages" + "\\" + gitem.FileName;
                                            if (File.Exists(editedImagePathMobile))
                                            {
                                                source = editedImagePathMobile;
                                            }
                                            else
                                            {
                                                source = gitem.HotFolderPath + "\\" + gitem.CreatedOn.ToString("yyyyMMdd") + "\\" + gitem.FileName;
                                            }
                                            destinationPathMobile = gitem.HotFolderPath + "\\WhatsAppProduct\\" + DateTime.Now.ToString("yyyyMMdd");
                                            hotFolderPath = gitem.HotFolderPath;
                                            generatedorder = gitem.HotFolderPath + "\\WhatsAppProduct\\" + DateTime.Now.ToString("yyyyMMdd");
                                            //if (lineItemW.SelectedProductType_ID == 128)
                                            //{
                                            //    destinationPathMobile = @"\\172.17.10.8\DigiPhotoApplications\Aquaria KLCC\WeChat\" + DateTime.Now.ToString("yyyyMMdd");
                                            //}

                                            CopyDirectoryFiles(source, orderNumber, destinationPathMobile, gitem.FileName);

                                            //Upload to cloud
                                            UploadFileInfo obj = new UploadFileInfo
                                            {
                                                Title = gitem.FileName,//file.Name, // System.IO.Path.GetFileName(uploadFilePath),
                                                ImagePath = gitem.BigThumbnailPath, //file.FullName,//uploadFilePath,
                                                TargetDirectory = "",// filePath + upload.SaveFolderPath,
                                                ImageDefaultHeight = 500,
                                                ImageDefaultWidth = 500,
                                                MediaType = 1,
                                                PhotoId = gitem.PhotoId

                                            };
                                            //CloudinaryUploadInfo objCloudinaryUploadInfo = UploadToCloud.UploadFile(obj, orderNumber, out errorPhotoId);//invokded in async method_VinS
                                            UploadFileToCloud(obj, orderNumber);
                                        }

                                        //Generate QR Code for placed order--Added by Vinod Salunke 28May2019
                                        GenerateMyQCCode(orderNumber, hotFolderPath, commaSepImages);

                                        //string QRCard = hotFolderPath + "OrderReceipt\\" + orderNumber + ".jpg";
                                        //ImageBrush imgbrush = new ImageBrush();
                                        ////imgbrush.ImageSource = new BitmapImage(new Uri(@"\\imixdatabase11\DigiImages\OrderReceipt\DG-1144148434.jpg", UriKind.Relative));
                                        //imgbrush.ImageSource = new BitmapImage(new Uri(QRCard, UriKind.Relative));
                                        ////@"\\imixdatabase11\DigiImages\OrderReceipt\DG-1144148434.jpg"

                                        //ShowToClientView(imgbrush);

                                        CtrlWeChat.SetParent(this);
                                        btnNextsimg.IsDefault = false;

                                        //generatedorder = generatedorder + "\\" + orderNumber;
                                        //string QRCard = hotFolderPath + "OrderReceipt\\QRCode.jpg";
                                        if (hotFolderPath.Contains("\\\\\\\\"))
                                        {
                                            hotFolderPath = hotFolderPath.Replace("\\\\", "\\");
                                        }
                                        string QRCard = hotFolderPath + "OrderReceipt\\" + orderNumber + ".jpg";
                                        //var res = CtrlWeChat.ShowHandlerDialog("WeChatShare", lineItemW.GroupItems, orderNumber, generatedorder);

                                        //Display QR Code on client side then on operator side_24thJun2019
                                        ShowToClientView(QRCard, true);

                                        var res = CtrlWeChat.ShowHandlerDialog("WeChatShare", QRCard);

                                        ShowToClientView(QRCard, false);

                                        btnNextsimg.IsDefault = true;
                                        isWeChat = true;

                                    }
                                }
                            }
                            #endregion


                            images = string.Empty;
                            //selectProductType_Id = item.SelectedProductType_ID;
                            //codeType = item.CodeType;
                            qrcode = item.UniqueCode;
                            if (string.IsNullOrEmpty(qrcode))
                            {
                                ///imgCount = 0;
                                foreach (LineItem qrCodelst in ImagesToPrint)
                                {
                                    if ((!string.IsNullOrEmpty(qrCodelst.UniqueCode)) && qrCodelst.ParentID == item.ParentID)
                                    {
                                        qrcode = qrCodelst.UniqueCode;
                                    }
                                    //if (qrCodelst.GroupItems.Count != 0)
                                    //{
                                    //source = qrCodelst.GroupItems[imgCount].HotFolderPath + "EditedImages\\";
                                    //source = System.IO.Path.Combine(qrCodelst.GroupItems[imgCount].HotFolderPath, "EditedImages");
                                    //if (File.Exists(source + "/" + qrCodelst.GroupItems[imgCount].FileName))
                                    //{
                                    //    source = source + "\\" + qrCodelst.GroupItems[imgCount].FileName;
                                    //}                                        
                                    //else
                                    //{
                                    //    source = qrCodelst.GroupItems[imgCount].HotFolderPath + "\\" + qrCodelst.GroupItems[imgCount].CreatedOn.ToString("yyyyMMdd") + "\\" + qrCodelst.GroupItems[imgCount].FileName;
                                    //}
                                    //CopyDirectoryFiles(source, orderNumber, destinationPath, qrCodelst.GroupItems[imgCount].FileName);
                                    //imgCount = imgCount + 1;
                                    //}
                                }
                            }
                            #region Condition If order is not an package
                            if (!item.IsPackage)  // check item is not a package
                            {
                                parentId = -1;
                                if (!item.IsBundled) // check item is not a Bundled like four by four image
                                {
                                    int copy = item.Quantity - 1;
                                    int iteration = 1;

                                    if (!item.IsAccessory)
                                    {
                                        foreach (var img in item.SelectedImages)
                                        {
                                            iteration = 1;
                                            while (iteration <= copy)
                                            {
                                                String Space = string.Empty;
                                                int counter = MaxGap - item.SelectedProductType_Text.Length;
                                                counter = counter + 35;

                                                for (int i = 1; i <= counter; i++)
                                                {
                                                    Space += " ";
                                                }

                                                iteration++;
                                                ItemDetail.AppendLine(item.SelectedProductType_Text + Space + item.UnitPrice.ToString(".00"));
                                            }

                                            if (images == string.Empty)
                                                images = img;
                                            else
                                                images += "," + img;
                                        }
                                    }
                                    LinetItemsDetails _obj = new LinetItemsDetails();

                                    _obj.Productname = item.SelectedProductType_Text;
                                    _obj.Productprice = item.UnitPrice.ToString("#.00");
                                    _obj.Discount = item.TotalDiscountAmount; ////modified by latika for evoucher
                                    if (!item.IsAccessory)
                                    {
                                        _obj.Productquantity = (item.Quantity * item.SelectedImages.Count).ToString();
                                    }
                                    else
                                    {
                                        _obj.Productquantity = item.Quantity.ToString();
                                    }
                                    billDetails.Add(_obj);
                                }
                                else
                                {
                                    String Space = string.Empty;
                                    int counter = MaxGap - item.SelectedProductType_Text.Length;
                                    counter = counter + 40;

                                    for (int i = 1; i <= counter; i++)
                                    {
                                        Space += " ";
                                    }

                                    int copy = item.Quantity;
                                    int iteration = 1;
                                    while (iteration <= copy)
                                    {
                                        ItemDetail.AppendLine(item.SelectedProductType_Text + Space + item.UnitPrice.ToString(".00"));
                                        iteration++;
                                    }

                                    foreach (var img in item.SelectedImages)
                                    {
                                        if (images == string.Empty)
                                            images = img;
                                        else
                                            images += "," + img;
                                    }

                                    LinetItemsDetails _obj = new LinetItemsDetails();
                                    _obj.Productname = item.SelectedProductType_Text;
                                    _obj.Productprice = item.UnitPrice.ToString("#.00");
                                    _obj.Productquantity = item.Quantity.ToString();
                                    _obj.Discount = item.TotalDiscountAmount + EvoucherAmnt;////modified by latika for evoucher
                                    _obj.Productcode = item.SelectedProductCode;  // Added by Anisur Rahman for Dubai frame (Product code + Barcode) receipt requirement
                                    billDetails.Add(_obj);
                                }

                                if (BillLineItems != string.Empty)
                                    BillLineItems += "," + images;
                                else
                                    BillLineItems = images;

                                int id = 0;
                                string OrderDetailsSyncCode = CommonUtility.GetUniqueSynccode(Convert.ToInt32(ApplicationObjectEnum.OrderDetails).ToString().PadLeft(2, '0'), LoginUser.countrycode, LoginUser.Storecode, LoginUser.SubStoreId.ToString());
                                if (!item.IssemiOrder)
                                {
                                    //id = _objDataLayer.SaveOrderLineItems(item.SelectedProductType_ID, orderId, images, item.Quantity, item.TotalDiscount, (decimal)item.TotalDiscountAmount, (decimal)item.UnitPrice, (decimal)item.TotalPrice, (decimal)item.NetPrice, parentId, LoginUser.SubStoreId, item.CodeType, item.UniqueCode, OrderDetailsSyncCode);
                                    id = (new OrderBusiness()).SaveOrderLineItems(item.SelectedProductType_ID, orderId, images, item.Quantity, item.TotalDiscount, (decimal)item.TotalDiscountAmount, (decimal)item.UnitPrice, (decimal)item.TotalPrice, (decimal)item.NetPrice, parentId, LoginUser.SubStoreId, item.CodeType, item.UniqueCode, OrderDetailsSyncCode, item.EvoucherCode, null);
                                    //selectProductType_Id = item.SelectedProductType_ID;
                                    qrcode = item.UniqueCode;
                                    ///////////changed by latika
                                    if (string.IsNullOrEmpty(qrcode))
                                    {
                                        ///imgCount = 0;
                                        foreach (LineItem qrCodelst in ImagesToPrint)
                                        {
                                            if ((!string.IsNullOrEmpty(qrCodelst.UniqueCode)) && qrCodelst.ParentID == item.ParentID)
                                            {
                                                qrcode = qrCodelst.UniqueCode;
                                            }
                                            //if (qrCodelst.GroupItems.Count != 0)
                                            //{
                                            //    //source = qrCodelst.GroupItems[imgCount].HotFolderPath + "\\" + qrCodelst.GroupItems[imgCount].CreatedOn.ToString("yyyyMMdd") + "\\" + qrCodelst.GroupItems[imgCount].FileName;
                                            //    source = System.IO.Path.Combine(qrCodelst.GroupItems[imgCount].HotFolderPath, "EditedImages");
                                            //    if (File.Exists(source + "/" + qrCodelst.GroupItems[imgCount].FileName))
                                            //    {
                                            //        source = source + "\\" + qrCodelst.GroupItems[imgCount].FileName;
                                            //    }
                                            //    else
                                            //    {
                                            //        source = qrCodelst.GroupItems[imgCount].HotFolderPath + "\\" + qrCodelst.GroupItems[imgCount].CreatedOn.ToString("yyyyMMdd") + "\\" + qrCodelst.GroupItems[imgCount].FileName;
                                            //    }
                                            //    CopyDirectoryFiles(source, orderNumber, destinationPath, qrCodelst.GroupItems[imgCount].FileName);
                                            //    imgCount = imgCount + 1;
                                            //}
                                        }
                                    }
                                    //codeType = item.CodeType;
                                    //Call method to save album print order incase of productType=79
                                    if (item.SelectedProductType_ID == 79 && id > 0 && !string.IsNullOrEmpty(images) && item.GroupItems.Count() > 0)
                                        //_objDataLayer.SaveAlbumPrintPosition(id, item.PrintPhotoOrderPosition);
                                        (new PrinterBusniess()).SaveAlbumPrintPosition(id, item.PrintPhotoOrderPosition);
                                }
                                else
                                {
                                    (new OrderBusiness()).setSemiOrderImageOrderDetails(orderId, images.ToString(), parentId, LoginUser.SubStoreId, item.TotalDiscount, (decimal)item.TotalDiscountAmount, (decimal)item.TotalPrice, (decimal)item.NetPrice);
                                }

                                if (item.SelectedProductType_ID == 41 || item.SelectedProductType_ID == 80 || item.SelectedProductType_ID == 81 || item.SelectedProductType_ID == 82 || item.SelectedProductType_ID == 83 || item.SelectedProductType_ID == 48 || item.SelectedProductType_ID == 78 || item.SelectedProductType_ID == 36 || item.SelectedProductType_ID == 35 || item.SelectedProductType_ID == 96 || item.SelectedProductType_ID == 97) // Check for CD burning and USB option
                                {

                                    foreach (var img in item.SelectedImages)
                                    {
                                        //Code added to remove duplicate entry (Raised in SKI Dubai)
                                        BurnImagesInfo isExists = burnedImages.Where(x => x.Producttype == item.SelectedProductType_ID && x.ImageID == Convert.ToInt32(img)).FirstOrDefault();
                                        if (isExists == null)
                                        {
                                            BurnImagesInfo _objnew = new BurnImagesInfo();
                                            _objnew.ImageID = img.ToInt32();
                                            _objnew.Producttype = item.SelectedProductType_ID;
                                            burnedImages.Add(_objnew);
                                        }
                                    }

                                }
                                else
                                {
                                    //if (!item.IsAccessory && item.SelectedProductType_ID != 84) //item acessory does not Insert into Printqueue
                                    if (item.SelectedProductType_ID != 127 && item.SelectedProductType_ID != 128 && item.SelectedProductType_ID != 95 && item.SelectedProductType_ID != 84) //item whatsapp, wechat & online ,video does not Insert into Printqueue
                                    {
                                        if (!item.IssemiOrder)
                                            AddToPrintQueue(item, id, item.PrintPhotoOrderPosition);
                                    }
                                }
                            }
                            #endregion

                            #region Condition If Order is a Package
                            else
                            {
                                // get All the Children of Package
                                var children = from result in ImagesToPrint
                                               where result.ParentID == item.ItemNumber && result.IsPackage == false
                                               select result;
                                //code added by manoj start at 11-Jan for wi-fi product
                                var lineItem = children?.ToList().FirstOrDefault();
                                if (lineItem != null)
                                {
                                    foreach (var gitem in lineItem.GroupItems)
                                    {
                                        //source = gitem.HotFolderPath + "\\" + gitem.CreatedOn.ToString("yyyyMMdd") + "\\" + gitem.FileName;
                                        source = System.IO.Path.Combine(gitem.HotFolderPath, "EditedImages");
                                        if (File.Exists(source + "/" + gitem.FileName))
                                        {
                                            source = source + "\\" + gitem.FileName;
                                        }
                                        else
                                        {
                                            source = gitem.HotFolderPath + "\\" + gitem.CreatedOn.ToString("yyyyMMdd") + "\\" + gitem.FileName;
                                        }
                                        CopyDirectoryFiles(source, orderNumber, destinationPath, gitem.FileName);
                                    }
                                }
                                //code added by manoj end at 11-Jan for wifi product

                                //Code added by Anand - 4-June-2014
                                var productsWithExceedPhotos = children.Where(p => p.IsAccessory == false && p.SelectedProductType_ID != 95 && p.SelectedImages.Count > p.TotalMaxSelectedPhotos).ToList();
                                if (productsWithExceedPhotos.Count > 0)
                                {
                                    int totalParentLineItem = 0;
                                    foreach (LineItem productWithExceedPhoto in productsWithExceedPhotos)
                                    {
                                        int CurrentParentLineItem = (productWithExceedPhoto.SelectedImages.Count / productWithExceedPhoto.TotalMaxSelectedPhotos) + (productWithExceedPhoto.SelectedImages.Count % productWithExceedPhoto.TotalMaxSelectedPhotos > 0 ? 1 : 0);
                                        if (CurrentParentLineItem > totalParentLineItem)
                                            totalParentLineItem = CurrentParentLineItem;
                                    }

                                    item.Quantity = 1;
                                    item.TotalPrice = (item.UnitPrice * item.Quantity).ToDouble();
                                    double totalDiscountAmount = item.TotalDiscountAmount;
                                    item.TotalDiscountAmount = totalDiscountAmount / totalParentLineItem;
                                    item.NetPrice = item.TotalPrice - item.TotalDiscountAmount;

                                    for (int count = 0; count < totalParentLineItem; count++)
                                    {
                                        # region Tax Calculation
                                        LineItem objLineItem = item;
                                        SetTaxDetails(ref objLineItem);
                                        item.TaxAmount = objLineItem.TaxAmount;
                                        item.TaxPercent = objLineItem.TaxPercent;
                                        item.NetPrice = objLineItem.NetPrice;
                                        item.IsTaxIncluded = objLineItem.IsTaxIncluded;

                                        #endregion
                                        //Add Parent Package Details
                                        string orderDetailsSyncCode = CommonUtility.GetUniqueSynccode(Convert.ToInt32(ApplicationObjectEnum.OrderDetails).ToString().PadLeft(2, '0'), LoginUser.countrycode, LoginUser.Storecode, LoginUser.SubStoreId.ToString());
                                        parentId = (new OrderBusiness()).SaveOrderLineItems(item.SelectedProductType_ID, orderId, images, item.Quantity, item.TotalDiscount, (decimal)item.TotalDiscountAmount, (decimal)item.UnitPrice, (decimal)item.TotalPrice, (decimal)item.NetPrice + (decimal)objLineItem.TaxAmount, -1, LoginUser.SubStoreId, item.CodeType, item.UniqueCode, orderDetailsSyncCode, item.EvoucherCode, null, item.TaxPercent, Math.Round((decimal)item.TaxAmount, 3, MidpointRounding.ToEven), item.IsTaxIncluded);
                                        //selectProductType_Id = item.SelectedProductType_ID;
                                        qrcode = item.UniqueCode;
                                        /////changed by latika 
                                        if (string.IsNullOrEmpty(qrcode))
                                        {
                                            ///imgCount = 0;
                                            foreach (LineItem qrCodelst in ImagesToPrint)
                                            {
                                                if ((!string.IsNullOrEmpty(qrCodelst.UniqueCode)) && qrCodelst.ParentID == item.ParentID)
                                                {
                                                    qrcode = qrCodelst.UniqueCode;
                                                }
                                                //if (qrCodelst.GroupItems.Count != 0)
                                                //{
                                                //    //source = qrCodelst.GroupItems[imgCount].HotFolderPath + "\\" + qrCodelst.GroupItems[imgCount].CreatedOn.ToString("yyyyMMdd") + "\\" + qrCodelst.GroupItems[count].FileName;
                                                //    source = System.IO.Path.Combine(qrCodelst.GroupItems[imgCount].HotFolderPath, "EditedImages");
                                                //    if (File.Exists(source + "/" + qrCodelst.GroupItems[imgCount].FileName))
                                                //    {
                                                //        source = source + "\\" + qrCodelst.GroupItems[imgCount].FileName;
                                                //    }
                                                //    else
                                                //    {
                                                //        source = qrCodelst.GroupItems[imgCount].HotFolderPath + "\\" + qrCodelst.GroupItems[imgCount].CreatedOn.ToString("yyyyMMdd") + "\\" + qrCodelst.GroupItems[imgCount].FileName;
                                                //    }
                                                //    CopyDirectoryFiles(source, orderNumber, destinationPath, qrCodelst.GroupItems[imgCount].FileName);
                                                //    imgCount = imgCount + 1;
                                                //}
                                            }
                                        }

                                        //codeType = item.CodeType;
                                        String Space = string.Empty;
                                        int counter = MaxGap - item.SelectedProductType_Text.Length;
                                        counter = counter + 35;

                                        for (int i = 1; i <= counter; i++)
                                        {
                                            Space += " ";
                                        }

                                        ItemDetail.AppendLine(item.SelectedProductType_Text + Space + item.UnitPrice.ToString(".00"));

                                        LinetItemsDetails obj = new LinetItemsDetails();
                                        obj.Productname = item.SelectedProductType_Text;
                                        obj.Productprice = item.UnitPrice.ToString("#.00");
                                        obj.Productquantity = (item.Quantity).ToString();
                                        obj.Discount = item.TotalDiscountAmount;
                                        obj.QRCode = qrcode;////changed by latika fro qr code is not printing in receipt
                                                            // _obj.QRCode = children.FirstOrDefault().UniqueCode;
                                        obj.Productcode = item.SelectedProductCode;  // Added by Anisur Rahman for Dubai frame (Product code + Barcode) receipt requirement
                                        billDetails.Add(obj);

                                        foreach (LineItem Citem in children)
                                        {
                                            int startIndex = Citem.TotalMaxSelectedPhotos * count;
                                            int photoCount = 0;
                                            if (!Citem.IsAccessory && Citem.SelectedProductType_ID != 95)
                                            {
                                                if ((Citem.SelectedImages.Count - startIndex) >= Citem.TotalMaxSelectedPhotos)
                                                    photoCount = Citem.TotalMaxSelectedPhotos;
                                                else
                                                    photoCount = Citem.SelectedImages.Count - startIndex;
                                            }

                                            if (Citem.SelectedProductType_ID == 95 || Citem.IsAccessory || (startIndex < Citem.SelectedImages.Count && photoCount > 0))
                                            {
                                                List<string> currentImages = null;

                                                images = string.Empty;
                                                if (!Citem.IsAccessory && Citem.SelectedProductType_ID != 95)
                                                {
                                                    currentImages = Citem.SelectedImages.GetRange(startIndex, photoCount);

                                                    foreach (var img in currentImages)
                                                    {
                                                        if (images == string.Empty)
                                                            images = img;
                                                        else
                                                            images += "," + img;
                                                    }
                                                }
                                                int id = 0;
                                                string OrderDetailsSyncCode1 = CommonUtility.GetUniqueSynccode(Convert.ToInt32(ApplicationObjectEnum.OrderDetails).ToString().PadLeft(2, '0'), LoginUser.countrycode, LoginUser.Storecode, LoginUser.SubStoreId.ToString());
                                                if (Citem.IssemiOrder)
                                                    (new OrderBusiness()).setSemiOrderImageOrderDetails(orderId, images, parentId, LoginUser.SubStoreId, item.TotalDiscount, 0, 0, 0);
                                                else
                                                    id = (new OrderBusiness()).SaveOrderLineItems(Citem.SelectedProductType_ID, orderId, images, Citem.Quantity, item.TotalDiscount, 0, 0, 0, 0, parentId, LoginUser.SubStoreId, Citem.CodeType, Citem.UniqueCode, OrderDetailsSyncCode1, item.EvoucherCode, null);
                                                //selectProductType_Id = Citem.SelectedProductType_ID;
                                                qrcode = Citem.UniqueCode;
                                                //codeType = item.CodeType;
                                                if (BillLineItems != string.Empty)
                                                    BillLineItems += "," + images;
                                                else
                                                    BillLineItems = images;

                                                if (Citem.SelectedProductType_ID == 80 || Citem.SelectedProductType_ID == 81 || Citem.SelectedProductType_ID == 82 || Citem.SelectedProductType_ID == 83 || Citem.SelectedProductType_ID == 41 || Citem.SelectedProductType_ID == 48 || Citem.SelectedProductType_ID == 78 || Citem.SelectedProductType_ID == 36 || Citem.SelectedProductType_ID == 35 || Citem.SelectedProductType_ID == 96 || Citem.SelectedProductType_ID == 97) // Check for CD burning and USB option
                                                {

                                                    foreach (var img in Citem.SelectedImages)
                                                    {
                                                        //Code added to remove duplicate entry (Raised in SKI Dubai)
                                                        BurnImagesInfo isExists = burnedImages.Where(x => x.Producttype == Citem.SelectedProductType_ID && x.ImageID == Convert.ToInt32(img)).FirstOrDefault();
                                                        if (isExists == null)
                                                        {
                                                            BurnImagesInfo _objnew = new BurnImagesInfo();
                                                            _objnew.ImageID = img.ToInt32();
                                                            _objnew.Producttype = Citem.SelectedProductType_ID;
                                                            burnedImages.Add(_objnew);
                                                        }
                                                    }
                                                }
                                                else
                                                {
                                                    //if (!Citem.IsAccessory && Citem.SelectedProductType_ID != 84 && Citem.SelectedProductType_ID != 95) //item acessory does not Insert into Printqueue
                                                    if (Citem.SelectedProductType_ID != 127 && Citem.SelectedProductType_ID != 128 && Citem.SelectedProductType_ID != 95 && Citem.SelectedProductType_ID != 84) //item whatsapp, wechat & online ,video does not Insert into Printqueue
                                                    {
                                                        AddToPrintQueue(Citem, id, currentImages, Citem.PrintPhotoOrderPosition);
                                                    }
                                                }

                                                if (outPutImages == string.Empty)
                                                    outPutImages = images;
                                                else
                                                    outPutImages += "," + images;

                                                images = string.Empty;
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    # region Tax Calculation
                                    string GroupImageslist = null;
                                    LineItem objLineItem = item;
                                    SetTaxDetails(ref objLineItem);
                                    item.TaxAmount = objLineItem.TaxAmount;
                                    item.TaxPercent = objLineItem.TaxPercent;
                                    item.NetPrice = objLineItem.NetPrice;
                                    item.IsTaxIncluded = objLineItem.IsTaxIncluded;

                                    #endregion
                                    string orderDetailsSyncCode = CommonUtility.GetUniqueSynccode(Convert.ToInt32(ApplicationObjectEnum.OrderDetails).ToString().PadLeft(2, '0'), LoginUser.countrycode, LoginUser.Storecode, LoginUser.SubStoreId.ToString());
                                    parentId = (new OrderBusiness()).SaveOrderLineItems(item.SelectedProductType_ID, orderId, images, item.Quantity, item.TotalDiscount, (decimal)item.TotalDiscountAmount, (decimal)item.UnitPrice, (decimal)item.TotalPrice, (decimal)item.NetPrice + (decimal)objLineItem.TaxAmount, -1, LoginUser.SubStoreId, item.CodeType, item.UniqueCode, orderDetailsSyncCode, item.EvoucherCode, null, item.TaxPercent, Math.Round((decimal)item.TaxAmount, 3, MidpointRounding.ToEven), item.IsTaxIncluded);
                                    Int32 PackageId = item.SelectedProductType_ID;
                                    //selectProductType_Id = item.SelectedProductType_ID;
                                    qrcode = item.UniqueCode;
                                    //////////////////created by latika for QR Code is not coming in Print Receipt
                                    if (string.IsNullOrEmpty(qrcode))
                                    {
                                        ///imgCount = 0;
                                        foreach (LineItem qrCodelst in ImagesToPrint)
                                        {
                                            if ((!string.IsNullOrEmpty(qrCodelst.UniqueCode)) && qrCodelst.ParentID == item.ParentID)
                                            {
                                                qrcode = qrCodelst.UniqueCode;
                                            }
                                            //if (qrCodelst.GroupItems.Count != 0)
                                            //{
                                            //    //source = qrCodelst.GroupItems[imgCount].HotFolderPath + "\\" + qrCodelst.GroupItems[imgCount].CreatedOn.ToString("yyyyMMdd") + "\\" + qrCodelst.GroupItems[imgCount].FileName;
                                            //    source = System.IO.Path.Combine(qrCodelst.GroupItems[imgCount].HotFolderPath, "EditedImages");
                                            //    if (File.Exists(source + "/" + qrCodelst.GroupItems[imgCount].FileName))
                                            //    {
                                            //        source = source + "\\" + qrCodelst.GroupItems[imgCount].FileName;
                                            //    }
                                            //    else
                                            //    {
                                            //        source = qrCodelst.GroupItems[imgCount].HotFolderPath + "\\" + qrCodelst.GroupItems[imgCount].CreatedOn.ToString("yyyyMMdd") + "\\" + qrCodelst.GroupItems[imgCount].FileName;
                                            //    }
                                            //    CopyDirectoryFiles(source, orderNumber, destinationPath, qrCodelst.GroupItems[imgCount].FileName);
                                            //    imgCount = imgCount + 1;
                                            //}
                                        }
                                    }
                                    ////////end/////
                                    //codeType = item.CodeType;
                                    //Call method to save album print order incase of productType=79
                                    if (item.SelectedProductType_ID == 79 && parentId > 0 && !string.IsNullOrEmpty(images) && item.GroupItems.Count() > 0)
                                    {
                                        //_objDataLayer.SaveAlbumPrintPosition(parentId, item.PrintPhotoOrderPosition);
                                        (new PrinterBusniess()).SaveAlbumPrintPosition(parentId, item.PrintPhotoOrderPosition);
                                        //CurrentRecord.PrintPhotoOrderPosition.Add(objLstMyItems.PhotoId, objLstMyItems.PrintOrderPosition);
                                    }

                                    String space = string.Empty;
                                    int counter = MaxGap - item.SelectedProductType_Text.Length;
                                    counter = counter + 35;

                                    for (int i = 1; i <= counter; i++)
                                    {
                                        space += " ";
                                    }

                                    ItemDetail.AppendLine(item.SelectedProductType_Text + space + item.UnitPrice.ToString(".00"));

                                    LinetItemsDetails _obj = new LinetItemsDetails();
                                    _obj.Productname = item.SelectedProductType_Text;
                                    _obj.Productprice = item.UnitPrice.ToString("#.00");
                                    _obj.Productquantity = (item.Quantity).ToString();
                                    _obj.Discount = item.TotalDiscountAmount;
                                    _obj.QRCode = qrcode;////changed by latika fro qr code is not printing in receipt
                                                         // _obj.QRCode = children.FirstOrDefault().UniqueCode;
                                    _obj.Productcode = item.SelectedProductCode;  // Added by Anisur Rahman for Dubai frame (Product code + Barcode) receipt requirement
                                    billDetails.Add(_obj);

                                    foreach (LineItem Citem in children)
                                    {
                                        images = string.Empty;
                                        if (!Citem.IsAccessory && Citem.SelectedProductType_ID != 95)
                                        {
                                            foreach (var img in Citem.SelectedImages)
                                            {
                                                if (images == string.Empty)
                                                    images = img;
                                                else
                                                    images += "," + img;
                                            }
                                        }
                                        int id = 0;
                                        string OrderDetailsSyncCode1 = CommonUtility.GetUniqueSynccode(Convert.ToInt32(ApplicationObjectEnum.OrderDetails).ToString().PadLeft(2, '0'), LoginUser.countrycode, LoginUser.Storecode, LoginUser.SubStoreId.ToString());
                                        if (Citem.IssemiOrder)
                                        {
                                            //_objDataLayer.setSemiOrderImageOrderDetails(orderId, images, parentId, LoginUser.SubStoreId);
                                            (new OrderBusiness()).setSemiOrderImageOrderDetails(orderId, images, parentId, LoginUser.SubStoreId, item.TotalDiscount, 0, 0, 0);
                                        }
                                        else
                                        {
                                            if (Citem.SelectedProductType_ID == 84 && new OrderBusiness().chKIsWaterMarkedOrNot(PackageId))
                                            {
                                                List<String> lstGroupImage = RobotImageLoader.GroupImages.Select(g => g.PhotoId.ToString()).ToList();
                                                List<String> lstGroupPrintOnly = Citem.SelectedImages.ToList();
                                                List<String> lstGroupImageOnly = lstGroupImage.Except(lstGroupPrintOnly).ToList();
                                                if (lstGroupImageOnly.Count > 0)
                                                    GroupImageslist = string.Join(",", lstGroupImageOnly);
                                            }
                                            id = (new OrderBusiness()).SaveOrderLineItems(Citem.SelectedProductType_ID, orderId, images, Citem.Quantity, item.TotalDiscount, 0, 0, 0, 0, parentId, LoginUser.SubStoreId, Citem.CodeType, Citem.UniqueCode, OrderDetailsSyncCode1, item.EvoucherCode, GroupImageslist);

                                            List<LstMyItems> selectList = new List<LstMyItems>();
                                            selectList = RobotImageLoader.GroupImages.Where(s => s.IsVosDisplay == true).ToList();
                                            if (Citem.SelectedProductType_ID == 131)
                                            {
                                                RobotImageLoader.GroupImages.Clear();
                                                foreach (var itemsb in selectList)
                                                {
                                                    RobotImageLoader.GroupImages.Add(itemsb);
                                                    _StroyBookId = Convert.ToString(itemsb.Name);
                                                }
                                            }
                                            //selectProductType_Id = Citem.SelectedProductType_ID;
                                            //-----Start---------Mahesh-----------Multiple QRCode----5th Feb 2018----   
                                            //qrCodeprint = Citem.UniqueCode;

                                            ////if (qrCodeprint != "")
                                            ////{
                                            ////    qrCodeprint = qrCodeprint + ":" + Citem.UniqueCode; //baanke
                                            ////}
                                            ////else
                                            ////{
                                            ////    qrCodeprint = Citem.UniqueCode == null ? ":" : Citem.UniqueCode;
                                            ////}
                                            //-----End---------Mahesh-----------Multiple QRCode----5th Feb 2018----
                                            //codeType = item.CodeType;
                                            //Call method to save album print order incase of productType=79
                                            if (Citem.SelectedProductType_ID == 79 && id > 0 && !string.IsNullOrEmpty(images) && Citem.GroupItems.Count() > 0)
                                            {
                                                //_objDataLayer.SaveAlbumPrintPosition(id, Citem.PrintPhotoOrderPosition);
                                                (new PrinterBusniess()).SaveAlbumPrintPosition(id, Citem.PrintPhotoOrderPosition);
                                                //CurrentRecord.PrintPhotoOrderPosition.Add(objLstMyItems.PhotoId, objLstMyItems.PrintOrderPosition);
                                            }
                                        }

                                        if (BillLineItems != string.Empty)
                                            BillLineItems += "," + images;
                                        else
                                            BillLineItems = images;

                                        if (Citem.SelectedProductType_ID == 80 || Citem.SelectedProductType_ID == 81 || Citem.SelectedProductType_ID == 82 || Citem.SelectedProductType_ID == 83 || Citem.SelectedProductType_ID == 41 || Citem.SelectedProductType_ID == 48 || Citem.SelectedProductType_ID == 78 || Citem.SelectedProductType_ID == 36 || Citem.SelectedProductType_ID == 35 || Citem.SelectedProductType_ID == 96 || Citem.SelectedProductType_ID == 97) // Check for CD burning and USB option
                                        {
                                            foreach (var img in Citem.SelectedImages)
                                            {
                                                //Code added to remove duplicate entry (Raised in SKI Dubai)
                                                BurnImagesInfo isExists = burnedImages.Where(x => x.Producttype == Citem.SelectedProductType_ID && x.ImageID == Convert.ToInt32(img)).FirstOrDefault();
                                                if (isExists == null)
                                                {
                                                    BurnImagesInfo _objnew = new BurnImagesInfo();
                                                    _objnew.ImageID = img.ToInt32();
                                                    _objnew.Producttype = Citem.SelectedProductType_ID;
                                                    burnedImages.Add(_objnew);
                                                }
                                            }

                                        }
                                        else
                                        {
                                            //if (!Citem.IsAccessory && Citem.SelectedProductType_ID != 84 && Citem.SelectedProductType_ID != 95 && id > 0) //item acessory & Online Upload does not Insert into Printqueue
                                            if (Citem.SelectedProductType_ID != 127 && Citem.SelectedProductType_ID != 128 && Citem.SelectedProductType_ID != 95 && Citem.SelectedProductType_ID != 84) //item whatsapp, wechat & online ,video does not Insert into Printqueue
                                            {
                                                AddToPrintQueue(Citem, id, Citem.PrintPhotoOrderPosition);
                                            }

                                            //By KCB ON 24 JUL FOR UNIQUE PRINTING
                                            //else if (!Citem.IsAccessory && Citem.SelectedProductType_ID == 84)
                                            //{
                                            //    AddToPrintQueue(Citem, id, Citem.PrintPhotoOrderPosition);
                                            //}
                                            //END--884
                                        }

                                        if (outPutImages == string.Empty)
                                            outPutImages = images;
                                        else
                                            outPutImages += "," + images;

                                        images = string.Empty;
                                    }
                                }   //End Else block Package products have selected Images less then allowed
                            }    //End Else block Package Order Line Items
                        }
                        #endregion


                        buss.SaveOrderTaxDetails(LoginUser.StoreId, orderInfo.DG_Orders_pkey, LoginUser.SubStoreId);
                        orderStatus = "OrderPlaced";
                        #region Condition and Flow of Email, Online Order and CD/USD Orders
                        if (burnedImages.Count > 0)
                        {
                            StringBuilder emailOrderPhotoId = new StringBuilder();
                            string folderName = string.Empty;
                            bool isCdUsb = false;

                            List<string> imageList = burnedImages.Select(x => x.ImageID.ToString()).ToList();
                            string list = string.Join(",", imageList);

                            List<PhotoInfo> PhotoList = (new PhotoBusiness()).GetPhotoRFIDByPhotoIDList(list);
                            string hotfolderPathser = string.Empty;
                            if (String.IsNullOrWhiteSpace(LoginUser.ServerHotFolderPath))
                                hotfolderPathser = LoginUser.DigiFolderPath;
                            else
                                hotfolderPathser = LoginUser.ServerHotFolderPath;
                            foreach (var itemid in burnedImages)
                            {
                                try
                                {
                                    PhotoInfo localPhotoObj = PhotoList.Where(x => x.DG_Photos_pkey == itemid.ImageID).FirstOrDefault();
                                    switch (itemid.Producttype)
                                    {
                                        case 78://Email
                                            folderName = System.IO.Path.Combine(hotfolderPathser, "PrintImages", "Email", orderNumber);
                                            break;
                                        case 80: //Facebook
                                            folderName = System.IO.Path.Combine(hotfolderPathser, "PrintImages", "Facebook", orderNumber);
                                            break;
                                        case 82: //Twitter
                                            folderName = System.IO.Path.Combine(hotfolderPathser, "PrintImages", "Twitter", orderNumber);
                                            break;
                                        case 81://LinkedIn
                                            folderName = System.IO.Path.Combine(hotfolderPathser, "PrintImages", "LinkedIn", orderNumber);
                                            break;
                                        case 83: //Pinterest
                                            folderName = System.IO.Path.Combine(hotfolderPathser, "PrintImages", "Pinterest", orderNumber);
                                            break;
                                        case 35: //CD
                                            isCdUsb = true;
                                            folderName = Environment.CurrentDirectory + "\\DigiOrderdImages\\CDOrders\\" + orderNumber + "\\CD\\";
                                            break;
                                        case 36: //USB
                                            isCdUsb = true;
                                            folderName = Environment.CurrentDirectory + "\\DigiOrderdImages\\USBOrders\\" + orderNumber + "\\USB\\";
                                            break;
                                        case 96: //VideoCD
                                            isCdUsb = true;
                                            folderName = Environment.CurrentDirectory + "\\DigiOrderdImages\\CDOrders\\" + orderNumber + "\\VideoCD\\";
                                            break;
                                        case 97: //VideoUSB
                                            isCdUsb = true;
                                            folderName = Environment.CurrentDirectory + "\\DigiOrderdImages\\USBOrders\\" + orderNumber + "\\VideoUSB\\";
                                            break;
                                    }

                                    if (!Directory.Exists(folderName))
                                        System.IO.Directory.CreateDirectory(folderName);
                                    // PhotoInfo objPhoto = new PhotoInfo();
                                    switch (itemid.Producttype)
                                    {
                                        case 78://Email
                                            // case 35: //CD
                                            // case 36: //USB
                                            //DG_Photos _objphoto = _objDataLayer.GetPhotoDetailsbyPhotoId(itemid.ImageID.ToInt32());
                                            // objPhoto = (new PhotoBusiness()).GetPhotoDetailsbyPhotoId(itemid.ImageID.ToInt32());
                                            emailOrderPhotoId.Append(itemid.ImageID + ",");
                                            string editedImagePath = System.IO.Path.Combine(localPhotoObj.HotFolderPath, "EditedImages", localPhotoObj.DG_Photos_FileName);
                                            if (File.Exists(editedImagePath))
                                                File.Copy(editedImagePath, System.IO.Path.Combine(folderName, itemid.ImageID.ToInt32() + ".jpg"), true);
                                            else
                                                File.Copy(System.IO.Path.Combine(localPhotoObj.HotFolderPath, localPhotoObj.DG_Photos_CreatedOn.ToString("yyyyMMdd"), localPhotoObj.DG_Photos_FileName), System.IO.Path.Combine(folderName, (itemid.ImageID.ToString() + ".jpg")), true);
                                            break;
                                        case 80: //Facebook
                                        case 82: //Twitter
                                        case 81: //LinkedIn
                                        case 83: //Pinterest
                                            //DG_Photos _objPhoto = _objDataLayer.GetPhotoDetailsbyPhotoId(itemid.ImageID.ToInt32());
                                            //objPhoto = (new PhotoBusiness()).GetPhotoDetailsbyPhotoId(itemid.ImageID.ToInt32());
                                            File.Copy(System.IO.Path.Combine(localPhotoObj.HotFolderPath, "Thumbnails_Big", localPhotoObj.DG_Photos_CreatedOn.ToString("yyyyMMdd"), localPhotoObj.DG_Photos_FileName), System.IO.Path.Combine(folderName, (itemid.ImageID.ToString() + ".jpg")));
                                            break;
                                    }
                                }
                                catch (Exception ex)
                                {
                                    string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                                    ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                                }

                                if (outPutImages == string.Empty)
                                    outPutImages = itemid.ImageID.ToString();
                                else
                                    outPutImages += "," + itemid.ImageID;
                            }
                            if (_isEventEmailPackage)
                            {
                                if (!string.IsNullOrEmpty(emailOrderPhotoId.ToString()))
                                {
                                    var objEmailInfo = new EMailInfo
                                    {
                                        StoreId = LoginUser.StoreId.ToString(),
                                        UserId = LoginUser.UserId.ToString(),
                                        OrderId = orderNumber,
                                        Sendername = LoginUser.UserName,
                                        SubstoreId = LoginUser.SubStoreId.ToString(),
                                        OtherMessage = emailOrderPhotoId.ToString().Substring(0, emailOrderPhotoId.Length - 1)
                                    };
                                    MailPopUp.IsEnabled = true;
                                    MailPopUp.ShowHandlerDialog(objEmailInfo);
                                }
                            }

                            //burn orders here
                            if (isCdUsb)
                            {
                                try
                                {
                                    InitiateBurnOrders();
                                }
                                catch (Exception ex)
                                {
                                    orderStatus = "BurnedProductError";
                                    string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                                    ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                                    MessageBox.Show("Your order has been successfully placed. But here is issue in detecting recording media. So, if USB/CD not burned please rewrite USB/CD from Burn Order.", "i-Mix");
                                }
                            }

                        }
                        #endregion

                        #region Print Slip and Add to log

                        if (isWeChat)
                        {
                            PrintSlipWeChat(orderNumber, ItemDetail.ToString(), ((_totalBillDiscount / _totalAmount) * 100), _totalBillDiscount, _totalBillAmount, BillLineItems, billDetails, paymentType, cardNumber, cardHolderName, customerName, hotelName, roomNo, voucherNo, orderId, qrCodeprint, isWeChat);
                        }
                        else
                        {
                            if (outPutImages != string.Empty)
                                PrintSlip(orderNumber, ItemDetail.ToString(), ((_totalBillDiscount / _totalAmount) * 100), _totalBillDiscount, _totalBillAmount, BillLineItems, billDetails, paymentType, cardNumber, cardHolderName, customerName, hotelName, roomNo, voucherNo, orderId, qrCodeprint);

                            else
                                PrintSlip(orderNumber, ItemDetail.ToString(), ((_totalBillDiscount / _totalAmount) * 100), _totalBillDiscount, _totalBillAmount, BillLineItems, billDetails, paymentType, cardNumber, cardHolderName, customerName, hotelName, roomNo, voucherNo, orderId, qrCodeprint);
                        }
                        if (_totalBillDiscount > 0)
                            AuditLog.AddUserLog(Common.LoginUser.UserId, (int)FrameworkHelper.ActionType.GenerateOrder, "Create Order of No :" + orderNumber + " of total Amount " + DefaultCurrency.ToString() + " " + _totalAmount.ToString("0.00") + " including discount of " + DefaultCurrency.ToString() + " " + _totalBillDiscount.ToString("0.00"));
                        else
                            AuditLog.AddUserLog(Common.LoginUser.UserId, (int)FrameworkHelper.ActionType.GenerateOrder, "Create Order of No :" + orderNumber + " of total Amount " + DefaultCurrency.ToString() + " " + _totalAmount.ToString("0.00"));

                        #endregion

                        //***********Added by manoj at 3-Jan-2018 for PC to mobile transfer Start ***************

                        //var shareitProcesses = Process.GetProcesses().
                        //         Where(pr => pr.ProcessName == "SHAREit");

                        //foreach (var shareitprocess in shareitProcesses)
                        //{
                        //    shareitprocess.Kill();
                        //}
                        if (!string.IsNullOrEmpty(InstaMobileProd.InstaMobileProductName) && InstaMobileProd.InstaMobileProductName.ToLower().Contains("instamobile"))
                        {
                            //Start...This condition moved from above to here...VINS
                            var shareitProcesses = Process.GetProcesses().
                                Where(pr => pr.ProcessName == "SHAREit");

                            foreach (var shareitprocess in shareitProcesses)
                            {
                                shareitprocess.Kill();
                            }
                            //End...This condition moved from above to here

                            System.Windows.Forms.Panel _pnlSched = new System.Windows.Forms.Panel();
                            WindowsFormsHost windowsFormsHost1 = new WindowsFormsHost();
                            windowsFormsHost1.Child = _pnlSched;
                            //PaymentPopup.Children.Add(windowsFormsHost1);
                            string path = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
                            path = path + "\\SHAREit\\SHAREit.exe";
                            Process process = Process.Start(path);
                            int processId = process.Id;
                            Process tempProc = Process.GetProcessById(processId);
                            //Payment window = null;
                            //foreach (Window wnd in Application.Current.Windows)
                            //{
                            //    //if (wnd.Title == "ClientView")
                            //    //{
                            //    //    window = (ClientView)wnd;
                            //    //}
                            //    window = (Payment)wnd;
                            //}
                            //if (window == null)
                            //{
                            //    window = new Payment();
                            //    window.WindowStartupLocation = System.Windows.WindowStartupLocation.Manual;
                            //}
                            this.PaymentPopup.Children.Add(windowsFormsHost1);
                            process.WaitForInputIdle(); // true if the associated process has reached an idle state.
                            SetParent(process.MainWindowHandle, _pnlSched.Handle); // loading exe to the wpf window.                                                       
                        }
                        ////***********Added by manoj at 3-Jan-2018 for PC to mobile transfer End ***************

                        return true;
                    }

                }

                burnedImages.Clear();
                burnedImages = null;
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                orderNumber = string.Empty;
                return false;
            }
            finally
            {
                MemoryManagement.FlushMemory();
                GC.RemoveMemoryPressure(20000);
            }
            orderNumber = string.Empty;
            return true;
        }


        /// <summary>
        ///Method added by Vinod Salunke_24thJun2019
        /// </summary>
        /// <param name="QRCode"></param>
        /// <param name="isVisible"></param>
        public void ShowToClientView(string QRCode, bool isVisible)
        {
            try
            {
                ClientView window = null;

                foreach (Window wnd in Application.Current.Windows)
                {
                    if (wnd.Title == "ClientView")
                    {
                        window = (ClientView)wnd;
                    }
                }

                if (window == null)
                {
                    window = new ClientView();
                    window.WindowStartupLocation = System.Windows.WindowStartupLocation.Manual;
                }
                window.imgNext.Visibility = System.Windows.Visibility.Collapsed;
                window.imgNext.Source = null;
                window.txtMainImage.Visibility = System.Windows.Visibility.Collapsed;
                window.stkPrint.Visibility = System.Windows.Visibility.Collapsed;
                window.btnMinimize.Visibility = Visibility.Collapsed;
                window.stkPrevNext.Visibility = Visibility.Collapsed;
                window.testR.Visibility = System.Windows.Visibility.Visible;
                window.GroupView = false;
                window.DefaultView = false;
                window.testR.Fill = null;
                window.imgDefault.Visibility = System.Windows.Visibility.Collapsed;
                window.instructionVideo.Visibility = System.Windows.Visibility.Collapsed;
                window.instructionVideo.Pause();
                // Added for Blur Iamges
                window.imgDefaultBlur.Visibility = System.Windows.Visibility.Collapsed;
                window.instructionVideoBlur.Visibility = System.Windows.Visibility.Collapsed;
                window.instructionVideoBlur.Pause();
                // End Changes
                if (isVisible)
                {
                    window.CtrlWeChat.SetParent(this);
                    var res = window.CtrlWeChat.ShowHandlerDialogClient("WeChatShare", QRCode);
                }
                if (!isVisible)
                {
                    window.CtrlWeChat.Visibility = Visibility.Collapsed;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }

        /// <summary>
        ///Method added by Vinod Salunke_24thJun2019
        /// </summary>
        /// <param name="upFileInfo"></param>
        /// <param name="orderNumber"></param>
        private async void UploadFileToCloud(UploadFileInfo upFileInfo, string orderNumber)
        {
            try
            {
                string errorPhotoId = string.Empty;

                await Task.Run(() =>
                {
                    CloudinaryUploadInfo objCloudinaryUploadInfo = UploadToCloud.UploadFile(upFileInfo, orderNumber, out errorPhotoId);
                    ErrorHandler.ErrorHandler.LogFileWrite("File uploaded for Order =" + orderNumber + " & ImageNumber =" + upFileInfo.ImageNumber);
                    //int progress = 0;
                    //for (; ; )
                    //{
                    //    System.Threading.Thread.Sleep(1);
                    //    progress++;
                    //    ErrorHandler.ErrorHandler.LogFileWrite("for loop cnt = " + progress);
                    //}
                });
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogFileWrite("Error in UploadFileToCloud");
            }
        }


        private void PrintSlipWeChat(string ordernumber, string ItemDetails, double TotalPercentage, double Discount, double NetPrice, string photos, List<LinetItemsDetails> Billdetails, string PaymentType, string Cardnumber, string CardHoldername, string Customername, string Hotelname, string Roomno, string Voucherno, int OrderID, string qrCodeprint, Boolean isWeChat)
        {
            try
            {
                //if (_objDataLayer == null)
                //    _objDataLayer = new DigiPhotoDataServices();
                string[] images = photos.Split(',');
                List<string> photo = new List<string>();
                //List<DG_Photos> PhotoList = _objDataLayer.GetPhotoRFIDByPhotoID(images);
                List<PhotoInfo> PhotoList = (new PhotoBusiness()).GetPhotoRFIDByPhotoIDList(photos);
                if (PhotoList != null)
                {
                    foreach (var img in PhotoList)
                    {
                        if (img != null)
                        {
                            int itemcount = images.Where(t => t.ToString() == img.DG_Photos_pkey.ToString()).ToList().Count;
                            if (itemcount == 1)
                                photo.Add(img.DG_Photos_RFID);
                            else
                                photo.Add("(" + img.DG_Photos_RFID + ")" + "X" + itemcount.ToString());
                        }
                    }
                }
                string imagesToPrint = string.Empty;
                foreach (var prntimg in photo)
                {
                    if (imagesToPrint == string.Empty)
                        imagesToPrint = prntimg;
                    else
                        imagesToPrint += ", " + prntimg;
                }
                if (_isEnableSlipPrint == true)
                {
                    if (!string.IsNullOrEmpty(_StroyBookId))
                    {
                        ordernumber = ordernumber + "\r\n" + "StoryBook Id-" + _StroyBookId;
                        TestBill obj = new TestBill(Common.LoginUser.SubstoreName.ToString(), Common.LoginUser.UserName.ToString(), ordernumber, imagesToPrint, (new RefundBusiness()).GetRefundText(), (Math.Round(_totalBillAmount, 2)).ToString("#0.00"), (Math.Round(_totalCashAmount, 2)).ToString("#0.00"), (Math.Round(_totalBalanceAmount, 2)).ToString("#0.00"), Billdetails, PaymentType, Cardnumber, CardHoldername, Customername, Hotelname, Roomno, Voucherno, DefaultCurrency, OrderID, false, _totalBillDiscount, qrCodeprint, isWeChat);
                        obj = null;
                        _StroyBookId = "";
                    }
                    else
                    {
                        TestBill obj = new TestBill(Common.LoginUser.SubstoreName.ToString(), Common.LoginUser.UserName.ToString(), ordernumber, imagesToPrint, (new RefundBusiness()).GetRefundText(), (Math.Round(_totalBillAmount, 2)).ToString("#0.00"), (Math.Round(_totalCashAmount, 2)).ToString("#0.00"), (Math.Round(_totalBalanceAmount, 2)).ToString("#0.00"), Billdetails, PaymentType, Cardnumber, CardHoldername, Customername, Hotelname, Roomno, Voucherno, DefaultCurrency, OrderID, false, _totalBillDiscount, qrCodeprint, isWeChat);
                        obj = null;
                    }

                }
            }

            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }

        /// <summary>
        /// Method added by Vinod Salunke 3Jun2019
        /// Method for displaying generated QR code on client view
        /// </summary>
        /// <param name="compiledBitmapImage"></param>
        public void ShowToClientView(ImageBrush compiledBitmapImage)
        {
            try
            {
                ClientView window = null;

                foreach (Window wnd in Application.Current.Windows)
                {
                    if (wnd.Title == "ClientView")
                    {
                        window = (ClientView)wnd;
                    }
                }

                if (window == null)
                {
                    window = new ClientView();
                    window.WindowStartupLocation = System.Windows.WindowStartupLocation.Manual;
                }
                window.imgNext.Visibility = System.Windows.Visibility.Collapsed;
                window.imgNext.Source = null;
                window.txtMainImage.Visibility = System.Windows.Visibility.Collapsed;
                window.stkPrint.Visibility = System.Windows.Visibility.Collapsed;
                window.btnMinimize.Visibility = Visibility.Collapsed;
                window.stkPrevNext.Visibility = Visibility.Collapsed;
                window.testR.Visibility = System.Windows.Visibility.Visible;
                window.GroupView = false;
                window.DefaultView = false;
                if (compiledBitmapImage != null)
                {
                    window.testR.Fill = null;
                    compiledBitmapImage.Stretch = Stretch.Uniform;
                    window.imgDefault.Visibility = System.Windows.Visibility.Collapsed;
                    window.instructionVideo.Visibility = System.Windows.Visibility.Collapsed;
                    window.instructionVideo.Pause();
                    // Added for Blur Iamges
                    window.imgDefaultBlur.Visibility = System.Windows.Visibility.Collapsed;
                    window.instructionVideoBlur.Visibility = System.Windows.Visibility.Collapsed;
                    window.instructionVideoBlur.Pause();
                    // End Changes
                    window.testR.Fill = compiledBitmapImage;
                }

            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }

        //Method added for generating QR code and save it in the jpeg format in hotfolder receipt folder
        /// <summary>
        /// Method added by Vinod Salunke 3Jun2019
        /// </summary>
        /// <param name="orderNumber"></param>
        /// <param name="hotFolderPath"></param>
        /// <param name="imgNumbers"></param>
        private void GenerateMyQCCode(string orderNumber, string hotFolderPath, string imgNumbers)
        {
            try
            {
                var QCwriter = new BarcodeWriter();
                QCwriter.Format = BarcodeFormat.QR_CODE;

                string WeChatPortal = ConfigurationManager.AppSettings["WeChatPortal"].ToString();
                //var result = QCwriter.Write("https://yasbeta.mydeievents.com/Home/About?orderNumber=" + orderNumber + "&imgNumbers=" + imgNumbers);
                var result = QCwriter.Write(WeChatPortal + orderNumber + "&imgNumbers=" + imgNumbers);
                string path = hotFolderPath + "\\OrderReceipt\\" + orderNumber + ".jpg";
                var barcodeBitmap = new Bitmap(result);

                using (MemoryStream memory = new MemoryStream())
                {
                    using (FileStream fs = new FileStream(path,
                       FileMode.Create, FileAccess.ReadWrite))
                    {
                        barcodeBitmap.Save(memory, System.Drawing.Imaging.ImageFormat.Jpeg);
                        byte[] bytes = memory.ToArray();
                        fs.Write(bytes, 0, bytes.Length);
                    }
                }

                if (File.Exists(hotFolderPath + "\\OrderReceipt\\" + "QRCode.jpg"))
                {
                    File.Delete(hotFolderPath + "\\OrderReceipt\\" + "QRCode.jpg");
                }

                //save Generated QR code as QRCode.jpg
                File.Copy(path, hotFolderPath + "\\OrderReceipt\\" + "QRCode.jpg", true);
            }
            catch (Exception ex)
            {
            }
        }


        /// <summary>
        /// InstaMobile ordered images moved to picture folder
        /// </summary>
        /// <param name="source"></param>
        /// <param name="orderNumber"></param>
        /// <param name="destination"></param>
        /// <param name="fileName"></param>
        private void CopyDirectoryFiles(string source, string orderNumber, string destination, string fileName)
        {

            try
            {
                if (!string.IsNullOrEmpty(InstaMobileProd.InstaMobileProductName) && InstaMobileProd.InstaMobileProductName.ToLower().Contains("instamobile"))
                {
                    if (!Directory.Exists(destination + "\\" + orderNumber))
                    {
                        // Try to create the directory.
                        DirectoryInfo di = Directory.CreateDirectory(destination + "\\" + orderNumber);
                    }
                    File.Copy(source, destination + "\\" + orderNumber + "\\" + fileName, true);
                }
            }
            catch (Exception ex)
            {

            }

        }
        private void CopyWhtsAppFiles(string source, string orderNumber, string destination, string fileName)
        {

            try
            {
                if (!Directory.Exists(destination + "\\" + orderNumber))
                {
                    // Try to create the directory.
                    DirectoryInfo di = Directory.CreateDirectory(destination + "\\" + orderNumber);
                }
                File.Copy(source, destination + "\\" + orderNumber + "\\" + fileName, true);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }


        double lineItemDiscountTotal = 0;
        double totalAmount = 0;
        private void SetTaxDetails(ref LineItem item)
        {
            try
            {

                StoreInfo store = new StoreInfo();
                TaxBusiness taxBusiness = new TaxBusiness();
                store = taxBusiness.getTaxConfigData();
                ProductBusiness proBiz = new ProductBusiness();
                double lineItemDiscountShare = 0;
                bool IsTaxEnabledOnPackage = (bool)proBiz.GetProductByID(item.SelectedProductType_ID).DG_IsTaxEnabled;
                List<TaxDetailInfo> taxDetails = taxBusiness.GetApplicableTaxes(0);
                double totolTaxPercent = 0;
                if (taxDetails != null && taxDetails.Count > 0)
                    totolTaxPercent = taxDetails.Sum(x => x.TaxPercentage).ToDouble();

                if (store.IsTaxEnabled)
                {
                    //if (!store.IsTaxIncluded)
                    //{
                    if (IsTaxEnabledOnPackage)
                    {
                        if (_totalBillDiscount > lineItemDiscountTotal)
                        {
                            double discountOnTotal = _totalBillDiscount - lineItemDiscountTotal;
                            lineItemDiscountShare = Math.Round(lineItemDiscountShare + ((item.NetPrice / (totalAmount - lineItemDiscountTotal)) * discountOnTotal), 3, MidpointRounding.ToEven);
                        }
                        item.TaxPercent = Convert.ToDouble(totolTaxPercent);
                        if (store.IsTaxIncluded)
                        {
                            item.TaxAmount = (item.NetPrice - lineItemDiscountShare) * totolTaxPercent / (100 + totolTaxPercent);
                            item.IsTaxIncluded = true;
                        }
                        else
                        {
                            item.TaxAmount = (item.NetPrice - lineItemDiscountShare) * totolTaxPercent / 100;
                            //item.NetPrice = item.NetPrice + (double)item.TaxAmount;
                            item.NetPrice = item.NetPrice;// + (double)item.TaxAmount;
                            item.IsTaxIncluded = false;
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }

        private double GetTotalTax()
        {
            double totolTaxAmount = 0;
            try
            {
                var LineItem = from result in ImagesToPrint
                               where result.ParentID == result.ItemNumber
                               select result;

                double totolTaxPercent = 0;
                foreach (LineItem item in LineItem)
                {
                    if (item.IsPackage)
                    {
                        StoreInfo store = new StoreInfo();
                        TaxBusiness taxBusiness = new TaxBusiness();
                        ProductBusiness proBiz = new ProductBusiness();
                        List<TaxDetailInfo> taxDetails = taxBusiness.GetApplicableTaxes(0);
                        store = taxBusiness.getTaxConfigData();
                        double lineItemDiscountShare = 0;
                        totalAmount = LineItem.Sum(x => x.TotalPrice);
                        bool IsTaxEnabledOnPackage = (bool)proBiz.GetProductByID(item.SelectedProductType_ID).DG_IsTaxEnabled;
                        lineItemDiscountTotal = LineItem.Sum(x => x.TotalDiscountAmount);

                        if (store.IsTaxEnabled && !store.IsTaxIncluded)
                        {

                            if (taxDetails != null && taxDetails.Count > 0)
                                totolTaxPercent = taxDetails.Sum(x => x.TaxPercentage).ToDouble();
                            if (IsTaxEnabledOnPackage)
                            {
                                if (_totalBillDiscount > lineItemDiscountTotal)
                                {
                                    double discountOnTotal = _totalBillDiscount - lineItemDiscountTotal;
                                    lineItemDiscountShare = Math.Round(lineItemDiscountShare + ((item.NetPrice / (totalAmount - lineItemDiscountTotal)) * discountOnTotal), 2);
                                    //getLineItemDiscountShare = Math.Round(((_totalBillDiscount - lineItemDiscountTotal) * 100 / item.NetPrice), 4);
                                }
                                //item.TaxAmount -= item.TotalDiscountAmount;
                                //totolTaxAmount += item.TaxAmount =(item.NetPrice) * totolTaxPercent / 100;
                                totolTaxAmount += (item.NetPrice - lineItemDiscountShare) * totolTaxPercent / 100;
                            }

                        }
                    }
                }

            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
            return totolTaxAmount;
        }
        private void InitiateBurnOrders()
        {
            if (BurnOrderElements.isExecuting == false)
            {
                closeExistingBurnOrderWindows();
                Orders.BurnMediaOrderList bm = new Orders.BurnMediaOrderList();
                bm.Width = 0;
                bm.Height = 0;
                bm.WindowState = System.Windows.WindowState.Minimized;
                bm.Show();
                bm.Hide();
                bm._isAutoStart = true;
                bm.AutoExecuteOrders();
            }
        }
        private void closeExistingBurnOrderWindows()
        {
            foreach (Window wnd in Application.Current.Windows)
            {
                if (wnd.Title == "BurnMedia")
                {
                    wnd.Close();
                }
            }
        }
        private void PrintSlip(string ordernumber, string ItemDetails, double TotalPercentage, double Discount, double NetPrice, string photos, List<LinetItemsDetails> Billdetails, string PaymentType, string Cardnumber, string CardHoldername, string Customername, string Hotelname, string Roomno, string Voucherno, int OrderID, string qrCodeprint)
        {
            try
            {
                //if (_objDataLayer == null)r
                //    _objDataLayer = new DigiPhotoDataServices();
                string[] images = photos.Split(',');
                List<string> photo = new List<string>();
                //List<DG_Photos> PhotoList = _objDataLayer.GetPhotoRFIDByPhotoID(images);
                List<PhotoInfo> PhotoList = (new PhotoBusiness()).GetPhotoRFIDByPhotoIDList(photos);
                if (PhotoList != null)
                {
                    foreach (var img in PhotoList)
                    {
                        if (img != null)
                        {
                            int itemcount = images.Where(t => t.ToString() == img.DG_Photos_pkey.ToString()).ToList().Count;
                            if (itemcount == 1)
                                photo.Add(img.DG_Photos_RFID);
                            else
                                photo.Add("(" + img.DG_Photos_RFID + ")" + "X" + itemcount.ToString());
                        }
                    }
                }
                string imagesToPrint = string.Empty;
                foreach (var prntimg in photo)
                {
                    if (imagesToPrint == string.Empty)
                        imagesToPrint = prntimg;
                    else
                        imagesToPrint += ", " + prntimg;
                }
                if (_isEnableSlipPrint == true)
                {
                    string rptChina = ConfigurationManager.AppSettings["IsChina"];
                    if (rptChina == "0")
                    {
                        if (!string.IsNullOrEmpty(_StroyBookId))
                        {
                            ordernumber = ordernumber + "\r\n" + "StoryBook Id-" + _StroyBookId;
                            TestBill obj = new TestBill(Common.LoginUser.SubstoreName.ToString(), Common.LoginUser.UserName.ToString(), ordernumber, imagesToPrint, (new RefundBusiness()).GetRefundText(), (Math.Round(_totalBillAmount, 2)).ToString("#0.00"), (Math.Round(_totalCashAmount, 2)).ToString("#0.00"), (Math.Round(_totalBalanceAmount, 2)).ToString("#0.00"), Billdetails, PaymentType, Cardnumber, CardHoldername, Customername, Hotelname, Roomno, Voucherno, DefaultCurrency, OrderID, false, _totalBillDiscount, qrCodeprint);
                            _StroyBookId = "";
                            obj = null;
                        }
                        else
                        {
                            TestBill obj = new TestBill(Common.LoginUser.SubstoreName.ToString(), Common.LoginUser.UserName.ToString(), ordernumber, imagesToPrint, (new RefundBusiness()).GetRefundText(), (Math.Round(_totalBillAmount, 2)).ToString("#0.00"), (Math.Round(_totalCashAmount, 2)).ToString("#0.00"), (Math.Round(_totalBalanceAmount, 2)).ToString("#0.00"), Billdetails, PaymentType, Cardnumber, CardHoldername, Customername, Hotelname, Roomno, Voucherno, DefaultCurrency, OrderID, false, _totalBillDiscount, qrCodeprint);
                            obj = null;
                        }
                    }
                    else
                    {
                        bool isDeliveryNote = new ConfigBusiness().GetDeliveryNoteStatus(LoginUser.SubStoreId).ToBoolean();
                        if (isDeliveryNote)
                        {

                            if (!string.IsNullOrEmpty(_StroyBookId))
                            {
                                ordernumber = ordernumber + "\r\n" + "StoryBook Id-" + _StroyBookId;
                                DeliveryNote obj = new DeliveryNote(Common.LoginUser.SubstoreName.ToString(), Common.LoginUser.UserId.ToString(), ordernumber, imagesToPrint, (new RefundBusiness()).GetRefundText(), (Math.Round(_totalBillAmount, 2)).ToString("#0.00"), (Math.Round(_totalCashAmount, 2)).ToString("#0.00"), (Math.Round(_totalBalanceAmount, 2)).ToString("#0.00"), Billdetails, PaymentType, Cardnumber, CardHoldername, Customername, Hotelname, Roomno, Voucherno, DefaultCurrency, OrderID, false, _totalBillDiscount, qrCodeprint);
                                obj = null;
                                _StroyBookId = "";
                            }
                            else
                            {
                                DeliveryNote obj = new DeliveryNote(Common.LoginUser.SubstoreName.ToString(), Common.LoginUser.UserId.ToString(), ordernumber, imagesToPrint, (new RefundBusiness()).GetRefundText(), (Math.Round(_totalBillAmount, 2)).ToString("#0.00"), (Math.Round(_totalCashAmount, 2)).ToString("#0.00"), (Math.Round(_totalBalanceAmount, 2)).ToString("#0.00"), Billdetails, PaymentType, Cardnumber, CardHoldername, Customername, Hotelname, Roomno, Voucherno, DefaultCurrency, OrderID, false, _totalBillDiscount, qrCodeprint);
                                obj = null;
                            }
                        }
                        else
                        {
                            if (!string.IsNullOrEmpty(_StroyBookId))
                            {
                                ordernumber = ordernumber + "\r\n" + "StoryBook Id-" + _StroyBookId;
                                TestBill obj = new TestBill(Common.LoginUser.SubstoreName.ToString(), Common.LoginUser.UserName.ToString(), ordernumber, imagesToPrint, (new RefundBusiness()).GetRefundText(), (Math.Round(_totalBillAmount, 2)).ToString("#0.00"), (Math.Round(_totalCashAmount, 2)).ToString("#0.00"), (Math.Round(_totalBalanceAmount, 2)).ToString("#0.00"), Billdetails, PaymentType, Cardnumber, CardHoldername, Customername, Hotelname, Roomno, Voucherno, DefaultCurrency, OrderID, false, _totalBillDiscount, qrCodeprint);
                                _StroyBookId = "";
                                obj = null;
                            }
                            else
                            {
                                TestBill obj = new TestBill(Common.LoginUser.SubstoreName.ToString(), Common.LoginUser.UserName.ToString(), ordernumber, imagesToPrint, (new RefundBusiness()).GetRefundText(), (Math.Round(_totalBillAmount, 2)).ToString("#0.00"), (Math.Round(_totalCashAmount, 2)).ToString("#0.00"), (Math.Round(_totalBalanceAmount, 2)).ToString("#0.00"), Billdetails, PaymentType, Cardnumber, CardHoldername, Customername, Hotelname, Roomno, Voucherno, DefaultCurrency, OrderID, false, _totalBillDiscount, qrCodeprint);
                                obj = null;
                            }
                        }
                    }
                }
            }

            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }
        private void AddToPrintQueue(LineItem liItem, int OrderDetailID, List<PhotoPrintPositionDic> PhotoPrintPositionDicList)
        {
            try
            {
                //DigiPhotoDataServices objDataServices = new DigiPhotoDataServices();
                //int id = objDataServices.AddImageToPrinterQueue(liItem.SelectedProductType_ID, liItem.SelectedImages, OrderDetailID, liItem.IsBundled, false, PhotoPrintPositionDicList);
                int id = (new PrinterBusniess()).AddImageToPrinterQueue(liItem.SelectedProductType_ID, liItem.SelectedImages, OrderDetailID, liItem.IsBundled, false, PhotoPrintPositionDicList, LoginUser.MasterTemplateId);
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }
        private void AddToPrintQueue(LineItem liItem, int OrderDetailID, List<string> currentImages, List<PhotoPrintPositionDic> PhotoPrintPositionDicList)
        {
            try
            {
                //DigiPhotoDataServices objDataServices = new DigiPhotoDataServices();
                //int id = objDataServices.AddImageToPrinterQueue(liItem.SelectedProductType_ID, currentImages, OrderDetailID, liItem.IsBundled, false, PhotoPrintPositionDicList);
                int id = (new PrinterBusniess()).AddImageToPrinterQueue(liItem.SelectedProductType_ID, currentImages, OrderDetailID, liItem.IsBundled, false, PhotoPrintPositionDicList);
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }
        private bool CheckCDAvailable()
        {
            var drives = DriveInfo.GetDrives();
            var cddrive = drives.Where(t => t.DriveType == DriveType.CDRom).FirstOrDefault();
            if (drives != null)
                return true;
            else
                return false;
        }
        private bool CheckUSBAvailable()
        {
            var drives = from drive in DriveInfo.GetDrives()
                         where (drive.DriveType == DriveType.Removable || (drive.DriveType == DriveType.Fixed && drive.DriveFormat == "FAT32")) && drive.IsReady == true
                         select drive;
            if (drives.Count() > 0)
                return true;
            else
                return false;
        }
        private void CalculateSummary()
        {
            try
            {
                int counter = 0;
                Double total = 0;
                double CashAmt = 0;
                foreach (PaymentItem item in _objPayment)
                {
                    DataGridRow row = (DataGridRow)dgPayment.ItemContainerGenerator.ContainerFromIndex(counter);
                    if (row != null)
                    {
                        TextBlock tx = FindByName("txtNetPrice", row) as TextBlock;
                        double amount;
                        if (Double.TryParse(tx.Text, out amount))
                        {
                            total += amount;
                            CashAmt += amount;
                        }
                    }
                    counter++;
                }
                counter = 0;
                foreach (PaymentItem item in _objCardPayment)
                {
                    DataGridRow row = (DataGridRow)dgCardPayment.ItemContainerGenerator.ContainerFromIndex(counter);
                    if (row != null)
                    {
                        TextBox tx = FindByName("txtAmountEntered", row) as TextBox;
                        double amount;
                        if (Double.TryParse(item.PaidAmount.ToString(), out amount))
                        {
                            total += amount;
                            CashAmt += amount;
                        }
                    }
                    counter++;
                }

                counter = 0;
                foreach (PaymentItem item in _objRoomPayment)
                {
                    DataGridRow row = (DataGridRow)dgRoomPayment.ItemContainerGenerator.ContainerFromIndex(counter);
                    if (row != null)
                    {
                        TextBox tx = FindByName("txtNetPrice", row) as TextBox;
                        double amount;
                        if (Double.TryParse(item.PaidAmount.ToString(), out amount))
                            total += amount;
                    }
                    counter++;
                }

                counter = 0;
                foreach (PaymentItem item in _objVoucherPayment)
                {
                    DataGridRow row = (DataGridRow)dgVoucherPayment.ItemContainerGenerator.ContainerFromIndex(counter);
                    if (row != null)
                    {
                        TextBox tx = FindByName("txtNetPrice", row) as TextBox;
                        double amount;
                        if (Double.TryParse(item.PaidAmount.ToString(), out amount))
                            total += amount;
                    }
                    counter++;
                }

                counter = 0;
                foreach (PaymentItem item in _objKVLPayment)
                {
                    DataGridRow row = (DataGridRow)dgKVLPayment.ItemContainerGenerator.ContainerFromIndex(counter);
                    if (row != null)
                    {
                        TextBox tx = FindByName("txtNetPrice", row) as TextBox;
                        double amount;
                        if (Double.TryParse(item.PaidAmount.ToString(), out amount))
                            total += amount;
                    }
                    counter++;
                }

                _totalGrandAmount = _totalCardAmount + total;
                _totalCashAmount = total;
                _totalBalanceAmount = Math.Round(_totalBillAmount - _totalGrandAmount, 2);
                if (_totalBalanceAmount >= 0)
                {
                    txtBalance.Text = DefaultCurrency + " " + _totalBalanceAmount.ToString("N2");//_totalBillAmount.ToString("N3");
                }
                else
                {
                    // double Value = (_totalBalanceAmount < 0 ? Math.Abs(_totalBalanceAmount) : _totalBalanceAmount);
                    txtBalance.Text = DefaultCurrency + " " + ((Math.Abs(_totalBalanceAmount) > CashAmt) ? "-" + CashAmt.ToString("N2") : _totalBalanceAmount.ToString("N2")); //"0.00";
                }
                // txtBalance.Text = DefaultCurrency + " " + _totalBalanceAmount.ToString("N3");
                txtPaid.Text = DefaultCurrency + " " + _totalGrandAmount.ToString("N2");

                if (_totalBalanceAmount > 0)
                {
                    tbAmount.Text = _totalBalanceAmount.ToString("N2");//_totalBillAmount.ToString("N3");
                }
                else
                {
                    tbAmount.Text = "0.00";
                }
                if (_totalBalanceAmount > 0)
                {
                    tbName1.Text = _totalBalanceAmount.ToString("N2");
                }
                else
                {
                    tbName1.Text = "0.00";
                }

                if (_totalBalanceAmount > 0)
                {
                    tbKVL.Text = _totalBalanceAmount.ToString("N2");
                }
                else
                {
                    tbKVL.Text = "0.00";
                }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }
        private void AddNewRow(string result)
        {
            try
            {
                if (result.IndexOf("%#%") >= 0)
                {
                    string[] split = new string[] { "%#%" };
                    string[] data = result.Split(split, StringSplitOptions.None);

                    PaymentItem objItem = new PaymentItem();
                    string itemNumber = System.Guid.NewGuid().ToString();
                    objItem.ItemNumber = itemNumber;
                    objItem.CurrencyList = _lstCurrency;
                    objItem.PaidAmount = data[1].ToDouble();
                    objItem.Currency = data[0].ToInt32();
                    objItem.CurrencySyncCode = _lstCurrency.FindAll(c => c.DG_Currency_pkey == objItem.Currency).ToList().FirstOrDefault().SyncCode;
                    objItem.CurrencyCode = _lstCurrency.FindAll(c => c.DG_Currency_pkey == objItem.Currency).ToList().FirstOrDefault().DG_Currency_Code;
                    _objPayment.Add(objItem);

                    dgPayment.UpdateLayout();

                    DataGridRow row = (DataGridRow)dgPayment.ItemContainerGenerator.ContainerFromIndex(_objPayment.Count - 1);
                    if (row != null)
                    {
                        var CurrencySymbol = (from currlist in _lstCurrency
                                              where currlist.DG_Currency_pkey == data[0].ToInt32()
                                              select currlist.DG_Currency_Symbol).FirstOrDefault();

                        TextBlock tx = FindByName("txtAmountEntered", row) as TextBlock;
                        tx.Text = CurrencySymbol.ToString() + " " + Math.Round(data[1].ToDouble(), 2).ToString("N2");

                        TextBlock tx1 = FindByName("txtNetPrice", row) as TextBlock;
                        tx1.Text = Math.Round(ConvertToDefault(data[0].ToInt32(), data[1].ToDouble()), 2).ToString("N2");

                        TextBlock tx2 = FindByName("txtCurrencyID", row) as TextBlock;
                        tx2.Text = data[0].ToString();
                    }
                    CalculateSummary();
                }
            }

            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }
        protected void AddNewCard(string result)
        {
            try
            {
                if (result.IndexOf("%##%") >= 0)
                {
                    string[] split = new string[] { "%##%" };
                    string[] data = result.Split(split, StringSplitOptions.None);

                    var CurrencySymbol = (from currlist in _lstCurrency
                                          where currlist.DG_Currency_pkey == _defaultCurrencyId.ToInt32()
                                          select currlist).FirstOrDefault();

                    PaymentItem objItem = new PaymentItem();
                    string itemNumber = System.Guid.NewGuid().ToString();
                    objItem.ItemNumber = itemNumber;
                    objItem.CurrencyList = _lstCurrency;
                    objItem.CardType = data[0].ToString();
                    objItem.CardYear = data[1].ToString();
                    objItem.CardMonth = data[2].ToString();
                    objItem.CardNumber = data[3].ToString();
                    objItem.PaidAmount = data[4].ToString().ToDouble();
                    objItem.Currency = _defaultCurrencyId.ToInt32();
                    objItem.CurrencySyncCode = CurrencySymbol.SyncCode;
                    objItem.CurrencyName = CurrencySymbol.DG_Currency_Symbol;
                    objItem.CardExpiryDate = data[2].ToString() + "/" + data[1].ToString();
                    _objCardPayment.Add(objItem);
                    dgCardPayment.UpdateLayout();

                    DataGridRow row = (DataGridRow)dgCardPayment.ItemContainerGenerator.ContainerFromIndex(_objCardPayment.Count - 1);
                    if (row != null)
                    {
                        TextBlock tx = FindByName("txtAmountEntered", row) as TextBlock;
                        tx.Text = CurrencySymbol.DG_Currency_Symbol.ToString() + " " + Math.Round(data[4].ToDouble(), 2).ToString("N2");
                    }
                    CalculateSummary();
                }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }
        private double ConvertToDefault(int currencyid, double ToBeConverted)
        {
            double convertedAmount = 0;

            var amount = (from rate in _lstCurrency
                          where rate.DG_Currency_pkey == currencyid
                          select rate).FirstOrDefault();

            //if (amount != null)
            //    convertedAmount = (amount.DG_Currency_Rate * ToBeConverted);
            if (amount != null)
                convertedAmount = Math.Round(ToBeConverted / amount.DG_Currency_Rate, 2);

            return Math.Round(convertedAmount, 3, MidpointRounding.ToEven);
        }
        private void ClearResources()
        {
            if (RobotImageLoader.PrintImages != null)
            {
                RobotImageLoader.PrintImages.Clear();
            }
            if (RobotImageLoader.robotImages != null)
            {
                RobotImageLoader.robotImages.Clear();
            }

            ImagesToPrint.Clear();
            _lstCurrency = null;
            _objPayment.Clear();
            _objCardPayment.Clear();
            this.SourceParent = null;
            btnExit.Click -= new RoutedEventHandler(btnExit_Click);
            btnNextsimg.Click -= new RoutedEventHandler(btnNextsimg_Click);
            btnPreviousimg_Copy.Click -= new RoutedEventHandler(btnPreviousimg_Click);

            foreach (Window wnd in Application.Current.Windows)
            {
                if (wnd.Title == "Select Printer")
                    wnd.Close();
                if (wnd.Title == "PlaceOrder")
                    wnd.Close();
                if (wnd.Title == "TestBill")
                    wnd.Close();
            }
            GC.Collect();
        }
        private static bool IsTextAllowed(string text)
        {
            Regex regex = new Regex("[^0-9.-]+"); //regex that matches disallowed text
            return !regex.IsMatch(text);
        }
        private FrameworkElement FindByName(string name, FrameworkElement root)
        {
            Stack<FrameworkElement> tree = new Stack<FrameworkElement>();
            tree.Push(root);
            while (tree.Count > 0)
            {
                FrameworkElement current = tree.Pop(); // root is null
                if (current.Name == name)
                    return current;

                int count = VisualTreeHelper.GetChildrenCount(current);
                for (int SupplierCounter = 0; SupplierCounter < count; ++SupplierCounter)
                {
                    DependencyObject child = VisualTreeHelper.GetChild(current, SupplierCounter);
                    if (child is FrameworkElement)
                        tree.Push((FrameworkElement)child);
                }
            }
            return null;
        }
        private void ShowPaymentScreenToClient()
        {
            try
            {
                VisualBrush CB = new VisualBrush(sart);
                SearchResult _objSearchResult = null;
                foreach (Window wnd in Application.Current.Windows)
                {
                    if (wnd.Title == "View/Order Station")
                    {
                        _objSearchResult = (SearchResult)wnd;
                    }
                }
                if (_objSearchResult == null)
                    _objSearchResult = new SearchResult(true);

                //SearchResult objSearchResult = new SearchResult(true);
                _objSearchResult.CompileEffectChanged(CB, -1);
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }
        #endregion

        #region Public Methods
        public void ShowPopup()
        {
            if (_card == true && _cash == false)
                btnAddMore1_Click(btnAddMore1, new RoutedEventArgs());

            if (_card == false && _cash == true)
                btnAddMore_Click(btnAddMore, new RoutedEventArgs());
        }
        #endregion

        #region Events
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            InitializeValue();
            _lstCurrency = (new CurrencyBusiness()).GetCurrencyOnly();
            burnLoadimage();
        }
        private void btnNextsimg_Click(object sender, RoutedEventArgs e)
        {
            btnNextsimg.IsEnabled = false;
            string orderNumber;
            string orderStatus = string.Empty;
            if (SaveOrder(out orderNumber, ref orderStatus) || orderStatus == "OrderPlaced")
            {
                //If(productType = _objLineItem.Where(o => o.SelectedProductType_ID.ToString() == "84").FirstOrDefault();
                //---------------Hari added code for QRcode------------//
                if (!string.IsNullOrEmpty(SelectedImagesQrCode.QRCodeValue))
                {
                    //////////////creted by latika fix issue multiple print qrcode text file////////////////////////////
                    OrderBusiness objOrder = new OrderBusiness();
                    List<QRcodes> QrCodes = new List<QRcodes>();

                    QrCodes = objOrder.GETQRcodesbyOrderNumber(orderNumber);
                    foreach (QRcodes QRlist in QrCodes)
                    {

                        CreateQROnlineOrderImgFIle(QRlist.QRCode);
                        SelectedImagesQrCode.QRCodeValue = string.Empty;  //By KCB ON 24 JUL

                    }
                    ////////end//////////// 
                }
                //-------------Hari ---------------------------------//
                SearchResult window = null;
                foreach (Window wnd in Application.Current.Windows)
                {
                    if (wnd.Title == "View/Order Station")
                        window = (SearchResult)wnd;
                }

                if (window == null)
                    window = new SearchResult();

                if (RobotImageLoader.GroupImages.Count > 0)
                    window.pagename = "MainGroup";
                else if (RobotImageLoader.PrintImages.Count > 0)
                {
                    window.pagename = "Placeback";

                    window.Savebackpid = RobotImageLoader.PrintImages[0].PhotoId.ToString(); //RobotImageLoader.PrintImages[0].Name.ToString();
                }
                var printInGroup = RobotImageLoader.GroupImages.Where(pb => pb.PrintGroup.UriSource.ToString() == @"/images/print-accept.png").ToList();

                for (int pg = 0; pg < printInGroup.Count; pg++)
                {
                    printInGroup[pg].PrintGroup = new BitmapImage(new Uri(@"/images/print-group.png", UriKind.Relative));
                }
                ClearResources();
                window.Show();
                window.flgViewScrl = true;
                window.LoadWindow();
                this.Close();
            }
            else if (orderStatus == "CARDFAILED" || orderStatus == "PAYREMAINING")
            {
                btnNextsimg.IsEnabled = true;
            }
            else if (string.IsNullOrEmpty(orderStatus))
            {
                MessageBox.Show("There is some error in placing order. Please place order again.");
            }
        }
        private void btnPreviousimg_Click(object sender, RoutedEventArgs e)
        {
            if (this.SourceParent is SelectPrinter)
            {
                SelectPrinter obj = (SelectPrinter)this.SourceParent;
                if (obj == null)
                    this.Close();
                else
                {
                    obj.Visibility = System.Windows.Visibility.Visible;
                    obj.Focus();
                    this.Close();
                }
            }
            if (this.SourceParent is PlaceOrder)
            {
                PlaceOrder obj = (PlaceOrder)this.SourceParent;
                if (obj == null)
                    this.Close();
                else
                {
                    obj.CtrlSelectedQrCode.QRCode = string.Empty;
                    obj.Visibility = System.Windows.Visibility.Visible;
                    obj.Focus();
                    this.Close();
                }
            }
            this.Close();
        }
        private void btndelete_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                PaymentItem toBeDeleteditem = (PaymentItem)dgPayment.CurrentItem;

                var item = (from lineitem in _objPayment
                            where lineitem.ItemNumber == toBeDeleteditem.ItemNumber.ToString()
                            select lineitem).FirstOrDefault();

                if (item != null)
                    _objPayment.Remove(item);

                CalculateSummary();
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }
        private void btnCarddelete_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                PaymentItem ToBeDeleteditem = (PaymentItem)dgCardPayment.CurrentItem;
                var item = (from lineitem in _objCardPayment
                            where lineitem.ItemNumber == ToBeDeleteditem.ItemNumber.ToString()
                            select lineitem).FirstOrDefault();

                if (item != null)
                    _objCardPayment.Remove(item);

                CalculateSummary();
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }

        private void btndeleteRoom_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                PaymentItem ToBeDeleteditem = (PaymentItem)dgRoomPayment.CurrentItem;
                var item = (from lineitem in _objRoomPayment
                            where lineitem.ItemNumber == ToBeDeleteditem.ItemNumber.ToString()
                            select lineitem).FirstOrDefault();

                if (item != null)
                    _objRoomPayment.Remove(item);

                CalculateSummary();
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }

        private void btndeleteVoucher_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                PaymentItem ToBeDeleteditem = (PaymentItem)dgVoucherPayment.CurrentItem;
                var item = (from lineitem in _objVoucherPayment
                            where lineitem.ItemNumber == ToBeDeleteditem.ItemNumber.ToString()
                            select lineitem).FirstOrDefault();

                if (item != null)
                    _objVoucherPayment.Remove(item);

                CalculateSummary();
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }

        private void btndeleteKVL_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                PaymentItem ToBeDeleteditem = (PaymentItem)dgKVLPayment.CurrentItem;
                var item = (from lineitem in _objKVLPayment
                            where lineitem.ItemNumber == ToBeDeleteditem.ItemNumber.ToString()
                            select lineitem).FirstOrDefault();

                if (item != null)
                    _objKVLPayment.Remove(item);

                CalculateSummary();
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }

        private void PastingHandler(object sender, DataObjectPastingEventArgs e)
        {
            if (e.DataObject.GetDataPresent(typeof(String)))
            {
                String text = (String)e.DataObject.GetData(typeof(String));
                if (!IsTextAllowed(text)) e.CancelCommand();
            }
            else
                e.CancelCommand();
        }
        private void txtAmountEntered_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = !IsTextAllowed(e.Text);
        }

        private void tbKVL_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = !IsTextAllowed(e.Text);
        }

        private void tbName1L_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = !IsTextAllowed(e.Text);
        }
        private void tbAmount_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = !IsTextAllowed(e.Text);
        }

        private void txtAmountEntered_TextChanged(object sender, TextChangedEventArgs e)
        {
            DataGridRow row = (DataGridRow)dgPayment.ItemContainerGenerator.ContainerFromIndex(dgPayment.SelectedIndex);
            if (row != null)
            {
                double cAmount = 0;
                double totalAmount = 0;

                TextBox tx = FindByName("txtNetPrice", row) as TextBox;
                TextBox tx1 = FindByName("txtAmountEntered", row) as TextBox;
                ComboBox cmbCurrency = FindByName("cmbCurrency", row) as ComboBox;
                totalAmount = _totalBillAmount.ToDouble();

                if (tx1.Text != string.Empty)
                {
                    cAmount = ConvertToDefault(cmbCurrency.SelectedValue.ToInt32(), tx1.Text.ToDouble());
                    tx.Text = cAmount.ToString("N2");
                }
                else
                {
                    cAmount = 0;
                    tx.Text = 0.ToString("N2");
                }
                CalculateSummary();
            }
        }
        private void btnAddMore_Click(object sender, RoutedEventArgs e)
        {
            CtrlCashPayment.SetParent(this);
            btnNextsimg.IsDefault = false;
            CtrlCashPayment.btnSubmit.IsDefault = true;
            var res = CtrlCashPayment.ShowHandlerDialog("Cash");
            btnNextsimg.IsDefault = true;
            CtrlCashPayment.btnSubmit.IsDefault = false;
            if (res != null)
            {
                AddNewRow(res);
            }
        }
        private void btnAddMore1_Click(object sender, RoutedEventArgs e)
        {
            CtrlCardPayment.SetParent(this);
            CtrlCardPayment.DefaultCurrency = this.DefaultCurrency;
            CtrlCardPayment.Focus();
            if (_totalBalanceAmount >= 0)
            {
                if (_totalCashAmount == 0)
                    CtrlCardPayment.TotalBillAmount = _totalBillAmount;
                else
                    CtrlCardPayment.TotalBillAmount = _totalBalanceAmount;
            }
            else if (_totalBalanceAmount < 0)
            {
                CtrlCardPayment.TotalBillAmount = 0;
            }
            CtrlCardPayment.txtAmount.Focus();
            btnNextsimg.IsDefault = false;
            CtrlCardPayment.btnSubmit.IsDefault = true;
            var res = CtrlCardPayment.ShowHandlerDialog("Cash");
            btnNextsimg.IsDefault = true;
            CtrlCardPayment.btnSubmit.IsDefault = false;
            if (res != null)
            {
                AddNewCard(res);
            }
        }

        private void btnAddRoom_Click(object sender, RoutedEventArgs e)
        {
            if (tbAmount.Text.Trim() == "" || Convert.ToDecimal(tbAmount.Text.Trim()) <= 0)
            {
                MessageBox.Show("Please enter valid amount first", "DEI");
                tbAmount.Focus();
                return;
            }
            else if (tbHotelName.Text.Trim() == "")
            {
                MessageBox.Show("Please enter hotel name first", "DEI");
                tbHotelName.Focus();
                return;
            }
            else if (tbRoomNumber.Text.Trim() == "")
            {
                MessageBox.Show("Please enter room number first", "DEI");
                tbRoomNumber.Focus();
                return;
            }
            //CtrlCardPayment.SetParent(this);
            //CtrlCardPayment.DefaultCurrency = this.DefaultCurrency;
            //CtrlCardPayment.Focus();
            //if (_totalBalanceAmount > 0)
            //{
            //    CtrlCardPayment.TotalBillAmount = _totalBalanceAmount;
            //}
            //else
            //{
            //    CtrlCardPayment.TotalBillAmount = _totalBillAmount;
            //}
            //CtrlCardPayment.txtAmount.Focus();
            //btnNextsimg.IsDefault = false;
            //CtrlCardPayment.btnSubmit.IsDefault = true;

            //var res = CtrlCardPayment.ShowHandlerDialog("Cash");
            //btnNextsimg.IsDefault = true;
            //CtrlCardPayment.btnSubmit.IsDefault = false;
            //if (res != null)
            //{
            //    AddNewRoom(res);
            //}

            var CurrencySymbol = (from currlist in _lstCurrency
                                  where currlist.DG_Currency_pkey == _defaultCurrencyId.ToInt32()
                                  select currlist).FirstOrDefault();

            PaymentItem objItem = new PaymentItem();
            string itemNumber = System.Guid.NewGuid().ToString();
            objItem.ItemNumber = itemNumber;
            objItem.CurrencyList = _lstCurrency;
            objItem.HotelName = tbHotelName.Text;
            objItem.RoomNumber = tbRoomNumber.Text;
            objItem.CandidateName = tbName.Text;
            objItem.PaidAmount = Convert.ToDouble(tbAmount.Text);
            objItem.Currency = _defaultCurrencyId.ToInt32();
            objItem.CurrencySyncCode = CurrencySymbol.SyncCode;
            objItem.CurrencyName = CurrencySymbol.DG_Currency_Symbol;
            _objRoomPayment.Add(objItem);
            dgRoomPayment.UpdateLayout();

            DataGridRow row = (DataGridRow)dgRoomPayment.ItemContainerGenerator.ContainerFromIndex(_objRoomPayment.Count - 1);
            if (row != null)
            {
                TextBlock txHName = FindByName("txtHotelName", row) as TextBlock;
                TextBlock txRNumber = FindByName("txtRoomnumber", row) as TextBlock;
                TextBlock txNetPrice = FindByName("txtNetPrice", row) as TextBlock;

                txHName.Text = tbHotelName.Text;
                txRNumber.Text = tbRoomNumber.Text;
                txNetPrice.Text = CurrencySymbol.DG_Currency_Symbol.ToString() + " " + Math.Round(tbAmount.Text.ToDouble(), 3).ToString("N2");

            }

            tbHotelName.Clear();
            tbRoomNumber.Clear();
            tbName.Clear();

            CalculateSummary();
        }

        private void btnAddVoucher_Click(object sender, RoutedEventArgs e)
        {
            //CtrlCardPayment.SetParent(this);
            //CtrlCardPayment.DefaultCurrency = this.DefaultCurrency;
            //CtrlCardPayment.Focus();
            //if (_totalBalanceAmount > 0)
            //{
            //    CtrlCardPayment.TotalBillAmount = _totalBalanceAmount;
            //}
            //else
            //{
            //    CtrlCardPayment.TotalBillAmount = _totalBillAmount;
            //}
            //CtrlCardPayment.txtAmount.Focus();
            //btnNextsimg.IsDefault = false;
            //CtrlCardPayment.btnSubmit.IsDefault = true;

            //var res = CtrlCardPayment.ShowHandlerDialog("Cash");
            //btnNextsimg.IsDefault = true;
            //CtrlCardPayment.btnSubmit.IsDefault = false;
            //if (res != null)
            //{
            //    AddNewRoom(res);
            //}
            if (tbName1.Text.Trim() == "" || Convert.ToDecimal(tbName1.Text.Trim()) <= 0)
            {
                MessageBox.Show("Please enter valid amount first", "DEI");
                return;
            }
            else if (tbRoomNumber1.Text.Trim() == "")
            {
                MessageBox.Show("Please enter voucher number first", "DEI");
                return;
            }
            var CurrencySymbol = (from currlist in _lstCurrency
                                  where currlist.DG_Currency_pkey == _defaultCurrencyId.ToInt32()
                                  select currlist).FirstOrDefault();

            PaymentItem objItem = new PaymentItem();
            string itemNumber = System.Guid.NewGuid().ToString();
            objItem.ItemNumber = itemNumber;
            objItem.CurrencyList = _lstCurrency;
            objItem.CandidateName = tbCustomerName.Text;
            objItem.VoucherNumber = tbRoomNumber1.Text;
            objItem.PaidAmount = Convert.ToDouble(tbName1.Text);
            objItem.Currency = _defaultCurrencyId.ToInt32();
            objItem.CurrencySyncCode = CurrencySymbol.SyncCode;
            objItem.CurrencyName = CurrencySymbol.DG_Currency_Symbol;
            _objVoucherPayment.Add(objItem);
            dgVoucherPayment.UpdateLayout();

            DataGridRow row = (DataGridRow)dgVoucherPayment.ItemContainerGenerator.ContainerFromIndex(_objVoucherPayment.Count - 1);
            if (row != null)
            {
                TextBlock txtVoucherNo = FindByName("txtVoucherNo", row) as TextBlock;
                TextBlock txtName = FindByName("txtName", row) as TextBlock;
                TextBlock txtNetPrice = FindByName("txtNetPrice", row) as TextBlock;

                txtVoucherNo.Text = tbRoomNumber1.Text;
                txtName.Text = tbCustomerName.Text;
                txtNetPrice.Text = CurrencySymbol.DG_Currency_Symbol.ToString() + " " + Math.Round(tbName1.Text.ToDouble(), 3).ToString("N2");
            }
            tbRoomNumber1.Clear();
            tbCustomerName.Clear();
            CalculateSummary();
        }
        private void btnAddKVL_Click(object sender, RoutedEventArgs e)
        {
            if (!double.TryParse(tbKVL.Text.Trim(), out double s))
            {
                MessageBox.Show("Please enter valid amount first", "DEI");
                return;
            }
            if (tbKVL.Text.Trim() == "" || Convert.ToDecimal(tbKVL.Text.Trim()) <= 0)
            {
                MessageBox.Show("Please enter valid amount first", "DEI");
                return;
            }

            var CurrencySymbol = (from currlist in _lstCurrency
                                  where currlist.DG_Currency_pkey == _defaultCurrencyId.ToInt32()
                                  select currlist).FirstOrDefault();

            PaymentItem objItem = new PaymentItem();
            string itemNumber = System.Guid.NewGuid().ToString();
            objItem.ItemNumber = itemNumber;
            objItem.CurrencyList = _lstCurrency;
            //objItem.CandidateName = tbCustomerName.Text;
            //objItem.VoucherNumber = tbRoomNumber1.Text;
            objItem.PaidAmount = Convert.ToDouble(tbKVL.Text);
            objItem.Currency = _defaultCurrencyId.ToInt32();
            objItem.CurrencySyncCode = CurrencySymbol.SyncCode;
            objItem.CurrencyName = CurrencySymbol.DG_Currency_Symbol;
            _objKVLPayment.Add(objItem);
            dgKVLPayment.UpdateLayout();

            DataGridRow row = (DataGridRow)dgKVLPayment.ItemContainerGenerator.ContainerFromIndex(_objKVLPayment.Count - 1);
            if (row != null)
            {
                //TextBlock txtVoucherNo = FindByName("txtVoucherNo", row) as TextBlock;
                //TextBlock txtName = FindByName("txtName", row) as TextBlock;
                TextBlock txtNetPrice = FindByName("txtNetPrice", row) as TextBlock;

                //txtVoucherNo.Text = tbRoomNumber1.Text;
                //txtName.Text = tbCustomerName.Text;
                txtNetPrice.Text = CurrencySymbol.DG_Currency_Symbol.ToString() + " " + Math.Round(tbKVL.Text.ToDouble(), 3).ToString("N2");
            }
            CalculateSummary();
        }

        private void btnExit_Click(object sender, RoutedEventArgs e)
        {
            SearchResult objsearch = null;

            //Commented for fixing issue of refreshing screen

            foreach (Window wnd in Application.Current.Windows)
            {
                if (wnd.Title == "View/Order Station")
                {
                    objsearch = (SearchResult)wnd;
                }
            }

            if (objsearch == null)
                objsearch = new SearchResult();

            if (RobotImageLoader.GroupImages.Count > 0)
            {
                objsearch.pagename = "MainGroup";
            }
            else if (RobotImageLoader.PrintImages.Count > 0)
            {
                objsearch.pagename = "Placeback";
                objsearch.Savebackpid = RobotImageLoader.PrintImages[0].PhotoId.ToString(); //RobotImageLoader.PrintImages[0].Name.ToString();
            }
            long selectedStoryBookId = 0;
            var PrintInGroup = RobotImageLoader.GroupImages.Where(pb => pb.PrintGroup.UriSource.ToString() == @"/images/print-accept.png").ToList();
            for (int pg = 0; pg < PrintInGroup.Count; pg++)
            {
                PrintInGroup[pg].PrintGroup = new BitmapImage(new Uri(@"/images/print-group.png", UriKind.Relative));

                if(PrintInGroup[pg].IsVosDisplay)
                {
                    selectedStoryBookId = PrintInGroup[pg].StoryBookId;
                }
            }
            
            if (selectedStoryBookId > 0)
            {
                var StoryBookPhotos = (new PhotoBusiness()).GetAllStoryBookPhotoBYSTID(selectedStoryBookId);
                if (StoryBookPhotos != null)
                {
                    if (StoryBookPhotos.Count > 0)
                    {
                        var sbPhoto = StoryBookPhotos.Where(x => x.IsVosDisplay == true).FirstOrDefault();
                        //if (sbPhoto != null)
                        //{
                        //    _objSearchResult.txtImageId.Text = sbPhoto.DG_Photos_RFID;
                        //}
                        //Remove storybook childs from group
                        foreach (var storyitem in StoryBookPhotos)
                        {
                            LstMyItems removeItem = RobotImageLoader.GroupImages.Where(o => o.PhotoId == storyitem.DG_Photos_pkey).FirstOrDefault();
                            LstMyItems removeRobotItem = RobotImageLoader.robotImages.Where(o => o.PhotoId == storyitem.DG_Photos_pkey).FirstOrDefault();
                            if (removeItem != null)
                            {
                                if (!storyitem.IsVosDisplay)
                                {
                                    RobotImageLoader.GroupImages.Remove(removeItem);
                                    //Remove from robot images too
                                    if (removeRobotItem != null)
                                    {
                                        RobotImageLoader.robotImages.Remove(removeRobotItem);
                                    }
                                }
                                else
                                {
                                    int indx = RobotImageLoader.GroupImages.IndexOf(removeItem);
                                    RobotImageLoader.GroupImages[indx].StoryBookId = storyitem.StoryBookID;
                                    RobotImageLoader.GroupImages[indx].PageNo = (Int32)storyitem.PageNo;
                                    RobotImageLoader.GroupImages[indx].IsVosDisplay = storyitem.IsVosDisplay;

                                    objsearch.Savebackpid = Convert.ToString(storyitem.DG_Photos_pkey);

                                    //Remove from robot images too
                                    if (removeRobotItem != null)
                                    {
                                        indx = RobotImageLoader.robotImages.IndexOf(removeRobotItem);
                                        RobotImageLoader.robotImages[indx].StoryBookId = storyitem.StoryBookID;
                                        RobotImageLoader.robotImages[indx].PageNo = (Int32)storyitem.PageNo;
                                        RobotImageLoader.robotImages[indx].IsVosDisplay = storyitem.IsVosDisplay;
                                    }
                                }
                            }
                        }
                    }
                }
            }

            //RobotImageLoader.PrintImages = new List<LstMyItems>();
            objsearch.Show();
            objsearch.flgViewScrl = true;
            objsearch.LoadWindow();
            this.Close();
        }
        private void btn_Click(object sender, RoutedEventArgs e)
        {
            Button _objbtn = (Button)sender;
            //_objbtn = (Button)sender;
            switch (_objbtn.Content.ToString())
            {
                case "ENTER":
                    {
                        KeyBorder.Visibility = Visibility.Collapsed;
                        break;
                    }
                case "SPACE":
                    {
                        controlon.Text = controlon.Text + " ";
                        break;
                    }
                case "CLOSE":
                    {
                        KeyBorder.Visibility = Visibility.Collapsed;
                        break;
                    }
                case "Back":
                    {
                        TextBox objtxt = (TextBox)(controlon);
                        if (controlon.Text.Length > 0)
                        {
                            controlon.Text = controlon.Text.Remove(controlon.Text.Length - 1, 1);
                        }
                        break;
                    }
                default:
                    {
                        if (controlon.Name == "tbAmount" || controlon.Name == "tbName1" || controlon.Name == "tbKVL")
                        {
                            if (controlon.Text.Length < 18)
                                controlon.Text = controlon.Text + _objbtn.Content;
                        }
                        else if (controlon.Text.Length < 50)
                        {
                            controlon.Text = controlon.Text + _objbtn.Content;
                        }
                    }
                    break;
            }
        }
        private void tbRoomNumber_GotFocus(object sender, RoutedEventArgs e)
        {
            controlon = (TextBox)sender;
            KeyBorder.Visibility = Visibility.Visible;
        }
        private void tbAmount_GotFocus(object sender, RoutedEventArgs e)
        {
            controlon = (TextBox)sender;
            KeyBorder.Visibility = Visibility.Visible;
        }
        private void tbName1_GotFocus(object sender, RoutedEventArgs e)
        {
            controlon = (TextBox)sender;
            KeyBorder.Visibility = Visibility.Visible;
        }
        private void tbKVL_GotFocus(object sender, RoutedEventArgs e)
        {
            controlon = (TextBox)sender;
            KeyBorder.Visibility = Visibility.Visible;
        }

        #endregion

        #region BurnImage Code
        public List<MsftDiscRecorder2> lstmain;
        public MsftDiscMaster2 discMaster;
        Int64 _totalDiscSize = 0;
        private const string ClientName = "BurnMedia";
        BackgroundWorker backgroundBurnWorker = new BackgroundWorker();

        private bool _isBurning;
        private bool _isFormatting;
        private IMAPI_BURN_VERIFICATION_LEVEL _verificationLevel =
            IMAPI_BURN_VERIFICATION_LEVEL.IMAPI_BURN_VERIFICATION_NONE;
        private bool _closeMedia;
        private bool _ejectMedia;

        private BurnData _burnData = new BurnData();
        public void burnLoadimage()
        {
            AddRecordingDevices();
            MsftDiscMaster2 discMaster = null;
            lstmain = new List<MsftDiscRecorder2>();
            try
            {
                discMaster = new MsftDiscMaster2();

                if (!discMaster.IsSupportedEnvironment)
                    return;
                foreach (string uniqueRecorderId in discMaster)
                {
                    var discRecorder2 = new MsftDiscRecorder2();
                    discRecorder2.InitializeDiscRecorder(uniqueRecorderId);
                    lstmain.Add(discRecorder2);
                }
                if (devicesComboBox.Items.Count > 0)
                {
                    devicesComboBox.SelectedIndex = 0;
                }
            }
            catch (COMException ex)
            {
                return;
            }
            finally
            {
                if (discMaster != null)
                {
                    Marshal.ReleaseComObject(discMaster);
                }
            }
            DateTime now = (new CustomBusineses()).ServerDateTime();
            UpdateCapacity();
        }
        private void AddRecordingDevices()
        {
            Dictionary<string, string> discRecorders = new Dictionary<string, string>();
            discMaster = new MsftDiscMaster2();
            try
            {
                if (!discMaster.IsSupportedEnvironment)
                    return;

                foreach (string uniqueRecorderId in discMaster)
                {
                    int i = 0;
                    var discRecorder2 = new MsftDiscRecorder2();
                    discRecorder2.InitializeDiscRecorder(uniqueRecorderId);
                    var devicePaths = string.Empty;
                    if (discRecorder2 != null)
                    {
                        var volumePath = (string)discRecorder2.VolumePathNames.GetValue(0);

                        foreach (string volPath in discRecorder2.VolumePathNames)
                        {
                            if (!string.IsNullOrEmpty(devicePaths))
                            {
                                devicePaths += ",";
                            }
                            devicePaths += volumePath;
                        }
                    }
                    if (discRecorder2 != null)
                        discRecorders.Add(string.Format("{0} [{1}]", devicePaths, discRecorder2.ProductId), i.ToString());
                }
                devicesComboBox.ItemsSource = discRecorders;
                devicesComboBox.SelectedValue = "0";
            }
            catch (Exception ex)
            {

            }
            finally
            {

            }
        }
        private void UpdateCapacity()
        {
            // Get the text for the Max Size
            if (_totalDiscSize == 0)
            {
                return;
            }

            // Calculate the size of the files
            Int64 totalMediaSize = 0;
            foreach (IMediaItem mediaItem in listBoxFiles.Items)
            {
                totalMediaSize += mediaItem.SizeOnDisc;
            }

            if (totalMediaSize == 0)
            {
            }
            else
            {
                var percent = (int)((totalMediaSize * 100) / _totalDiscSize);
                if (percent > 100)
                {
                }
                else
                {
                }
            }
        }
        private void backgroundBurnWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            MsftDiscRecorder2 discRecorder = null;
            MsftDiscFormat2Data discFormatData = null;

            try
            {
                // Create and initialize the IDiscRecorder2 object
                discRecorder = new MsftDiscRecorder2();
                var burnData = (BurnData)e.Argument;
                discRecorder.InitializeDiscRecorder(burnData.uniqueRecorderId);

                // Create and initialize the IDiscFormat2Data
                discFormatData = new MsftDiscFormat2Data
                {
                    Recorder = discRecorder,
                    ClientName = ClientName,
                    ForceMediaToBeClosed = _closeMedia
                };

                // Set the verification level
                var burnVerification = (IBurnVerification)discFormatData;
                burnVerification.BurnVerificationLevel = _verificationLevel;

                // Check if media is blank, (for RW media)
                object[] multisessionInterfaces = null;
                if (!discFormatData.MediaHeuristicallyBlank)
                {
                    multisessionInterfaces = discFormatData.MultisessionInterfaces;
                }

                // Create the file system
                IStream fileSystem;
                if (!CreateMediaFileSystem(discRecorder, multisessionInterfaces, out fileSystem))
                {
                    e.Result = -1;
                    return;
                }

                // add the Update event handler
                discFormatData.Update += discFormatData_Update;

                // Write the data here
                try
                {
                    discFormatData.Write(fileSystem);
                    e.Result = 0;
                }
                catch (COMException ex)
                {

                }
                finally
                {
                    if (fileSystem != null)
                    {
                        Marshal.FinalReleaseComObject(fileSystem);
                    }
                }

                // remove the Update event handler
                discFormatData.Update -= discFormatData_Update;
                discRecorder.EjectMedia();
            }
            catch (COMException exception)
            {
                // If anything happens during the format, show the message
                System.Windows.Forms.MessageBox.Show(exception.Message);
                e.Result = exception.ErrorCode;
            }
            finally
            {
                if (discRecorder != null)
                {
                    Marshal.ReleaseComObject(discRecorder);
                }

                if (discFormatData != null)
                {
                    Marshal.ReleaseComObject(discFormatData);
                }
            }
        }
        void discFormatData_Update([In, MarshalAs(UnmanagedType.IDispatch)] object sender, [In, MarshalAs(UnmanagedType.IDispatch)] object progress)
        {
            // Check if we've cancelled
            if (backgroundBurnWorker.CancellationPending)
            {
                var format2Data = (IDiscFormat2Data)sender;
                format2Data.CancelWrite();
                return;
            }

            var eventArgs = (IDiscFormat2DataEventArgs)progress;
            _burnData.task = BURN_MEDIA_TASK.BURN_MEDIA_TASK_WRITING;

            // IDiscFormat2DataEventArgs Interface
            _burnData.elapsedTime = eventArgs.ElapsedTime;
            _burnData.remainingTime = eventArgs.RemainingTime;
            _burnData.totalTime = eventArgs.TotalTime;

            // IWriteEngine2EventArgs Interface
            _burnData.currentAction = eventArgs.CurrentAction;
            _burnData.startLba = eventArgs.StartLba;
            _burnData.sectorCount = eventArgs.SectorCount;
            _burnData.lastReadLba = eventArgs.LastReadLba;
            _burnData.lastWrittenLba = eventArgs.LastWrittenLba;
            _burnData.totalSystemBuffer = eventArgs.TotalSystemBuffer;
            _burnData.usedSystemBuffer = eventArgs.UsedSystemBuffer;
            _burnData.freeSystemBuffer = eventArgs.FreeSystemBuffer;

            // Report back to the UI
            backgroundBurnWorker.ReportProgress(0, _burnData);
        }
        private void backgroundBurnWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            _isBurning = false;
        }
        private void backgroundBurnWorker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
        }
        #endregion

        #region File System Process
        private bool CreateMediaFileSystem(IDiscRecorder2 discRecorder, object[] multisessionInterfaces, out IStream dataStream)
        {
            MsftFileSystemImage fileSystemImage = null;
            try
            {
                fileSystemImage = new MsftFileSystemImage();
                fileSystemImage.ChooseImageDefaults(discRecorder);
                fileSystemImage.FileSystemsToCreate =
                    FsiFileSystems.FsiFileSystemJoliet | FsiFileSystems.FsiFileSystemISO9660;

                fileSystemImage.Update += fileSystemImage_Update;

                // If multisessions, then import previous sessions
                if (multisessionInterfaces != null)
                {
                    fileSystemImage.MultisessionInterfaces = multisessionInterfaces;
                    fileSystemImage.ImportFileSystem();
                }

                // Get the image root
                IFsiDirectoryItem rootItem = fileSystemImage.Root;

                // Add Files and Directories to File System Image
                foreach (IMediaItem mediaItem in listBoxFiles.Items)
                {
                    // Check if we've cancelled
                    if (backgroundBurnWorker.CancellationPending)
                    {
                        break;
                    }

                    // Add to File System
                    mediaItem.AddToFileSystem(rootItem);
                }

                fileSystemImage.Update -= fileSystemImage_Update;

                // did we cancel?
                if (backgroundBurnWorker.CancellationPending)
                {
                    dataStream = null;
                    return false;
                }

                dataStream = fileSystemImage.CreateResultImage().ImageStream;
            }
            catch (COMException exception)
            {
                dataStream = null;
                return false;
            }
            finally
            {
                if (fileSystemImage != null)
                {
                    Marshal.ReleaseComObject(fileSystemImage);
                }
            }
            return true;
        }
        void fileSystemImage_Update([In, MarshalAs(UnmanagedType.IDispatch)] object sender,
            [In, MarshalAs(UnmanagedType.BStr)] string currentFile, [In] int copiedSectors, [In] int totalSectors)
        {
            var percentProgress = 0;
            if (copiedSectors > 0 && totalSectors > 0)
            {
                percentProgress = (copiedSectors * 100) / totalSectors;
            }

            if (!string.IsNullOrEmpty(currentFile))
            {
                var fileInfo = new FileInfo(currentFile);
                _burnData.statusMessage = "Adding \"" + fileInfo.Name + "\" to image...";

                // report back to the ui
                _burnData.task = BURN_MEDIA_TASK.BURN_MEDIA_TASK_FILE_SYSTEM;
                backgroundBurnWorker.ReportProgress(percentProgress, _burnData);
            }
        }
        #endregion

        #region Format/Erase the Disc
        private void backgroundFormatWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            MsftDiscRecorder2 discRecorder = null;
            MsftDiscFormat2Erase discFormatErase = null;

            try
            {
                // Create and initialize the IDiscRecorder2
                discRecorder = new MsftDiscRecorder2();
                var activeDiscRecorder = (string)e.Argument;
                discRecorder.InitializeDiscRecorder(activeDiscRecorder);

                // Create the IDiscFormat2Erase and set properties
                discFormatErase = new MsftDiscFormat2Erase
                {
                    Recorder = discRecorder,
                    ClientName = ClientName,
                };

                // Setup the Update progress event handler
                discFormatErase.Update += discFormatErase_Update;

                // Erase the media here
                try
                {
                    discFormatErase.EraseMedia();
                    e.Result = 0;
                }
                catch (COMException ex)
                {

                }

                // Remove the Update progress event handler
                discFormatErase.Update -= discFormatErase_Update;
            }
            catch (COMException exception)
            {
                // If anything happens during the format, show the message
            }
            finally
            {
                if (discRecorder != null)
                {
                    Marshal.ReleaseComObject(discRecorder);
                }

                if (discFormatErase != null)
                {
                    Marshal.ReleaseComObject(discFormatErase);
                }
            }
        }
        void discFormatErase_Update([In, MarshalAs(UnmanagedType.IDispatch)] object sender, int elapsedSeconds, int estimatedTotalSeconds)
        {
            var percent = elapsedSeconds * 100 / estimatedTotalSeconds;
            // Report back to the UI
            //backgroundFormatWorker.ReportProgress(percent);
        }

        private void backgroundFormatWorker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {

        }

        private void backgroundFormatWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            _isFormatting = false;
        }
        #endregion

        private void tbHotelName_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = !IsSpecialCharacterAllowed(e.Text);
        }

        private void tbRoomNumber_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = !IsSpecialCharacterAllowed(e.Text);
        }

        private void tbName_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = !IsSpecialCharacterAllowed(e.Text);
        }
        private static bool IsSpecialCharacterAllowed(string text)
        {
            Regex regex = new Regex("[^0-9a-zA-Z.-]+"); //regex that matches disallowed text
            return !regex.IsMatch(text);
        }

        private void tbRoomNumber1_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = !IsSpecialCharacterAllowed(e.Text);
        }

        private void tbCustomerName_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = !IsSpecialCharacterAllowed(e.Text);
        }

        //------------Hari-------Start-------22st Feb 18-------
        private void CreateQROnlineOrderImgFIle(string QrCode)
        {
            OrderBusiness orderBusiness = new OrderBusiness();
            List<OnlineorderImgDetail> GetOrderNumberandImgLst = orderBusiness.GetOrderNumberandImg(QrCode);

            string FilePath = Common.LoginUser.DigiFolderPath;
            if (!File.Exists(System.IO.Path.Combine(FilePath, "QRCodeDetails")))
            {

                System.IO.Directory.CreateDirectory(System.IO.Path.Combine(FilePath, "QRCodeDetails"));
            }
            FilePath = System.IO.Path.Combine(FilePath, "QRCodeDetails");
            if (!File.Exists(System.IO.Path.Combine(FilePath, DateTime.Now.ToString("ddMMyyyy"))))
            {

                System.IO.Directory.CreateDirectory(System.IO.Path.Combine(FilePath, DateTime.Now.ToString("ddMMyyyy")));
            }
            FilePath = System.IO.Path.Combine(FilePath, DateTime.Now.ToString("ddMMyyyy"));

            FilePath = System.IO.Path.Combine(FilePath, QrCode + ".txt");

            //if (System.IO.File.Exists(FilePath))
            //{
            //    File.Delete(FilePath);
            //    Thread.Sleep(1000);
            //}
            using (System.IO.FileStream fs = new FileStream(FilePath, FileMode.Create, FileAccess.Write))
            {
                StreamWriter writer = new StreamWriter(fs);

                string[] orderNos = GetOrderNumberandImgLst.Select(qr => qr.OnlineOrderNumber).Distinct().ToArray();

                foreach (string OrderNo in orderNos)
                {
                    //writer.WriteLine(Environment.NewLine);
                    writer.WriteLine("-------------------------------------------");
                    writer.WriteLine(OrderNo);
                    writer.WriteLine("-------------------------------------------");

                    foreach (OnlineorderImgDetail item in GetOrderNumberandImgLst.Where(or => or.OnlineOrderNumber == OrderNo).ToList())
                    {
                        writer.WriteLine(item.OnlinePhotoID + "_" + item.OnlinePhotoName);
                    }
                    writer.WriteLine(Environment.NewLine);
                }

                writer.Flush();
                writer.Close();
            }
            return;


        }

        private void ValidateEmpID()
        {
            string empname = new UserBusiness().GetEmployeeName(this.txtEmpID.Text.Trim());
            if (string.IsNullOrEmpty(empname) || empname == "NA")
            {
                this.txtEmpID.Clear();
                if (MessageBox.Show("Employee is not Exist", "Invalid Employye ID", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.No)
                {
                    this.txtEmpID.Focus();
                }
                else
                {
                    this.txtEmpName.Visibility = Visibility.Collapsed;
                }
            }
            else
                this.txtEmpName.Visibility = Visibility.Visible;
            this.txtEmpName.Text = empname;

        }

        private void TxtEmpID_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            this.ValidateEmpID();
        }

        private void TxtEmpID_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
                this.ValidateEmpID();
        }

        private void tbKVL_TextChanged(object sender, TextChangedEventArgs e)
        {            
            try
            {   
                foreach (char ch in tbKVL.Text.Trim())
                {
                    if (!Char.IsDigit(ch) && !ch.Equals('.'))
                    {
                        if (tbKVL.Text.Contains(ch))
                        {
                            tbKVL.Text = tbKVL.Text.Replace(ch.ToString(), "");
                        }
                    }
                    else if (ch.Equals('.'))
                    {
                        TextBox tbSource = (sender as TextBox);
                        int Count = 0;
                        int val = 0;
                        foreach (var item in tbSource.Text)
                        {
                            val++;
                            if (item.Equals('.'))
                            {
                                Count++;
                                if (Count == 2)
                                {
                                    tbKVL.Text = tbKVL.Text.Remove(val - 1, 1);
                                }
                            }
                        }
                    }
                }

            }
            catch (Exception ex)
            {
            }
        }

        private void tbName1_TextChanged(object sender, TextChangedEventArgs e)
        {
            Regex decimalRegex = new Regex(@"[0-9\-]");
            try
            {
                //e.Changes
                foreach (char ch in tbName1.Text.Trim())
                {
                    if (!Char.IsDigit(ch) && !ch.Equals('.'))
                    {
                        if (tbName1.Text.Contains(ch))
                        {
                            tbName1.Text = tbName1.Text.Replace(ch.ToString(), "");
                        }
                    }
                    else if (ch.Equals('.'))
                    {
                        TextBox tbSource = (sender as TextBox);
                        int Count = 0;
                        int val = 0;
                        foreach (var item in tbSource.Text)
                        {
                            val++;
                            if (item.Equals('.'))
                            {
                                Count++;
                                if (Count == 2)
                                {
                                    tbName1.Text = tbName1.Text.Remove(val - 1, 1);

                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {

            }
        }

        private void tbAmount_TextChanged(object sender, TextChangedEventArgs e)
        {
            Regex decimalRegex = new Regex(@"[0-9\-]");
            try
            {
                //e.Changes
                foreach (char ch in tbAmount.Text.Trim())
                {
                    if (!Char.IsDigit(ch) && !ch.Equals('.'))
                    {
                        if (tbAmount.Text.Contains(ch))
                        {
                            tbAmount.Text = tbAmount.Text.Replace(ch.ToString(), "");
                        }
                    }
                    else if (ch.Equals('.'))
                    {
                        TextBox tbSource = (sender as TextBox);
                        int Count = 0;
                        int val = 0;
                        foreach (var item in tbSource.Text)
                        {
                            val++;
                            if (item.Equals('.'))
                            {
                                Count++;
                                if (Count == 2)
                                {
                                    tbAmount.Text = tbAmount.Text.Remove(val - 1, 1);

                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {

            }
        }

        //------------Hari-------end-------22st Feb 18-------
    }

    #region PaymentItem Model
    public class PaymentItem
    {
        public string ItemNumber { get; set; }
        public int PaymentType { get; set; }
        public double PaidAmount { get; set; }
        public int Currency { get; set; }
        public string CurrencySyncCode { get; set; }
        public string CurrencyCode { get; set; }
        public string CurrencyName { get; set; }
        public string CardNumber { get; set; }
        public string CardType { get; set; }
        public string CardYear { get; set; }
        public string CardMonth { get; set; }
        public string CardExpiryDate { get; set; }
        public List<CurrencyInfo> CurrencyList
        {
            get;
            set;
        }
        public string HotelName { get; set; }
        public string RoomNumber { get; set; }
        public string CandidateName { get; set; }
        public string VoucherNumber { get; set; }
    }
    #endregion

    #region BurnImages Model
    //class BurnImages
    //{
    //    private int _ImageID;
    //    public int ImageID
    //    {
    //        get { return _ImageID; }
    //        set { _ImageID = value; }
    //    }

    //    private int producttype;

    //    public int Producttype
    //    {
    //        get { return producttype; }
    //        set { producttype = value; }
    //    }
    //}
    #endregion

    #region LinetItemsDetails Model
    //public class LinetItemsDetails
    //{
    //    string productname;
    //    public string Productname
    //    {
    //        get { return productname; }
    //        set { productname = value; }
    //    }

    //    string productprice;

    //    public string Productprice
    //    {
    //        get { return productprice; }
    //        set { productprice = value; }
    //    }

    //    string productquantity;

    //    public string Productquantity
    //    {
    //        get { return productquantity; }
    //        set { productquantity = value; }
    //    }
    //}
    #endregion

    #region DiscRecorderValueConverter Convertor
    public class DiscRecorderValueConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return null;
        }

    }
    #endregion


}
