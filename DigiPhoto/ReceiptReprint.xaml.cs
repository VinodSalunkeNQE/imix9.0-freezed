﻿using DigiAuditLogger;
using DigiPhoto.Common;
using DigiPhoto.DataLayer;
using DigiPhoto.DataLayer.Model;
using DigiPhoto.IMIX.Business;
using DigiPhoto.IMIX.Model;
using DigiPhoto.Manage;
using DigiPhoto.Orders;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing.Printing;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace DigiPhoto
{
    /// <summary>
    /// Interaction logic for ReceiptReprint.xaml
    /// </summary>
    public partial class ReceiptReprint : Window
    {
        string photoRfids = string.Empty;
        OrderReceiptReprintInfo order = null;
        List<LinetItemsDetails> itemList = null;
        int OrderId = 0;
        // DigiPhotoDataServices _objDataLayer = null;
        List<OrderDetails> _objOrderList;
        public int lineitemId;
        //List<CheckedItems> _objSelectedItems;
        double DiscountOnTotal = 0;
        TextBox controlon;
        public string sDefaultCurrency
        {
            get;
            set;
        }
        public double _NetAmount
        {
            get;
            set;
        }
        public double _TotalAmount
        {
            get;
            set;
        }
        public double _TotalDiscount
        {
            get;
            set;
        }
        public double _RefundAmount
        {
            get;
            set;
        }

        List<Alreadyrefund> AlreadyRefunded;
        public ReceiptReprint()
        {
            InitializeComponent();
            txbUserName.Text = LoginUser.UserName;
            txbStoreName.Text = LoginUser.StoreName;
            btnSearch.IsDefault = true;
            //dgOrderList.Visibility = Visibility.Collapsed;
            //lstItems.Visibility = Visibility.Collapsed;
            txtSearchOrderNO.Focus();
            sDefaultCurrency = GetDefaultCurrency();
            //sDefaultCurrency = (new CurrencyBusiness()).GetDefaultCurrencySymbol().ToString();
            ResetControls();
            GetLastThreeOrders();

        }
        private void btnLogout_Click(object sender, RoutedEventArgs e)
        {
            Login _obj = new Login();
            _obj.Show();
            this.Close();
        }

        private void btnBack_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                ManageHome _objHome = new ManageHome();
                _objHome.Show();
                this.Close();
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }

        private void btnSearch_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                ResetControls();
                if (string.IsNullOrEmpty(txtSearchOrderNO.Text.Trim()))
                {
                    MessageBox.Show("Please enter a valid order number.");
                    return;
                }
                KeyBorderOrder.Visibility = Visibility.Collapsed;
                dgOrderList.Visibility = Visibility.Collapsed;
                GetSearchedDetailData();
                if (dgOrderList.Items.Count <= 0)
                {
                    MessageBox.Show("Please enter a valid order number.");
                }
                else
                {
                    dgOrderList.Visibility = Visibility.Visible;
                    btnViewPhotos.Visibility = Visibility.Visible;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }

        public void ResetControls()
        {
            txt_TotalDiscount.Text = sDefaultCurrency + " " + "0.00";
            txt_NetAmount.Text = sDefaultCurrency + " " + "0.00";
            txt_TotalAmount.Text = sDefaultCurrency + " " + "0.00";
            //txtTotalRefund.Text = sDefaultCurrency + " " + "0.00";
            order = null;
            photoRfids = string.Empty;
            lstItems.ItemsSource = null;
            btnViewPhotos.Visibility = Visibility.Collapsed;
            dgOrderList.ItemsSource = null;
            //txtSearchOrderNO.Text = string.Empty; 
        }
        private void btnClose_Click(object sender, RoutedEventArgs e)
        {
            GrdPrint.Visibility = Visibility.Collapsed;

        }
        void GetSearchedDetailData()
        {
            try
            {
                OrderId = 0;
                AlreadyRefunded = new List<Alreadyrefund>();
                //DigiPhotoDataServices _objDbLayer = new DigiPhotoDataServices();
                _objOrderList = new List<OrderDetails>();
                string OrderNumber = txtSearchOrderNO.Text.ToUpperInvariant();
                if (OrderNumber.IndexOf("DG-", StringComparison.OrdinalIgnoreCase) < 0)
                {
                    OrderNumber = "DG-" + OrderNumber;

                }
                //List<vw_GetOrderDetailsforRefund> _objlst = _objDbLayer.GetOrderDetailsforReceiptReprint(OrderNumber);
                List<OrderDetailInfo> _objlst = (new OrderBusiness()).GetOrderDetailsforRefund(OrderNumber);
                //added by latika to get the claim code
                List<QRcodes> QRcodes = new List<QRcodes>();
                QRcodes= (new OrderBusiness()).GETQRcodesbyOrderNumber(OrderNumber);
                if (_objlst != null && _objlst.Count > 0)
                {
                    itemList = new List<LinetItemsDetails>();
                    decimal? TotalLineItemDiscount = 0;
                    LinetItemsDetails lineItemDetail = null;
                    foreach (var item in _objlst)
                    {
                        lineItemDetail = new LinetItemsDetails();
                        OrderId = Convert.ToInt32(item.DG_Orders_ID);
                        TotalLineItemDiscount = TotalLineItemDiscount + item.DG_Orders_LineItems_DiscountAmount;
                        TotalLineItemDiscount = decimal.Round(Convert.ToDecimal(TotalLineItemDiscount), 4, MidpointRounding.AwayFromZero);
                        OrderDetails _objnew = new OrderDetails();
                        _objnew.DG_LineItemId = item.DG_Orders_LineItems_pkey;
                        _objnew.DG_Orders_Details_Items_TotalCost = item.DG_Orders_Details_Items_UniPrice * item.TotalQuantity;
                        _objnew.DG_Orders_LineItems_DiscountAmount = item.LineItemshare;
                        _objnew.DG_ProductTypeId = item.DG_Orders_Details_ProductType_pkey;
                        if (!(bool)item.DG_Orders_ProductType_IsBundled)
                        {
                            _objnew.DG_Orders_LineItems_Quantity = item.TotalQuantity;
                        }
                        else
                        {
                            _objnew.DG_Orders_LineItems_Quantity = item.TotalQuantity;
                        }
                        //_objnew.DG_LineItem_RefundPrice = decimal.Round(Convert.ToDecimal(_objnew.DG_LineItem_RefundPrice), 4, MidpointRounding.AwayFromZero);
                        _objnew.DG_LineItemUnitPrice = item.DG_Orders_Details_Items_UniPrice;
                        _objnew.DG_Orders_ProductType_Name = item.DG_Orders_ProductType_Name;
                        _objnew.PhotoIds = item.DG_Photos_ID;
                        _objnew.IsBundled = (bool)item.DG_Orders_ProductType_IsBundled;
                        _objnew.IsPackage = (bool)item.DG_IsPackage;
                        _objnew.loopquantity = (int)item.DG_Orders_LineItems_Quantity;

                        lineItemDetail.Productname = item.DG_Orders_ProductType_Name;
                        lineItemDetail.Productcode = item.DG_Orders_ProductCode; // Added by Anisur Rahman for Dubai frame (Product code + Barcode) receipt requirement  
                        lineItemDetail.Productprice = Convert.ToDecimal(item.DG_Orders_Details_Items_UniPrice != null ? item.DG_Orders_Details_Items_UniPrice : 0).ToString("#.00");
                        lineItemDetail.Productquantity = item.TotalQuantity.ToString();
                        lineItemDetail.Discount = Convert.ToDouble(item.DG_Orders_LineItems_DiscountAmount);
                        if (QRcodes.Count > 0)
                        {
                            lineItemDetail.QRCode = QRcodes[0].QRCode;
                        }
                        itemList.Add(lineItemDetail);
                        _objOrderList.Add(_objnew);
                        //if (item.DG_Orders_Details_Items_UniPrice > 0)
                        //{
                        //    _objOrderList.Add(_objnew);
                        //}
                    }
                    //if (Convert.ToDecimal(_TotalDiscount) > 0)
                    //{
                    //    DiscountOnTotal = Convert.ToDecimal(_TotalDiscount) - TotalLineItemDiscount;
                    //    DiscountOnTotal = decimal.Round(Convert.ToDecimal(DiscountOnTotal), 4, MidpointRounding.AwayFromZero);
                    //}
                    dgOrderList.ItemsSource = _objOrderList;
                    GetOrderDetail();
                }
                else
                {
                    dgOrderList.ItemsSource = null;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }

        void GetLastThreeOrders()
        {
            try
            {
                string line;
                string Orders;
                if (System.IO.File.Exists(Environment.CurrentDirectory + "\\on.dat"))
                {
                    using (StreamReader reader = new StreamReader(Environment.CurrentDirectory + "\\on.dat"))
                    {
                        line = reader.ReadLine();
                        Orders = CryptorEngine.Decrypt(line, true);
                        Dictionary<int, string> dic = new Dictionary<int, string>();
                        int counter = Orders.Split(',').Count();
                        foreach (string str in Orders.Split(','))
                        {
                            dic.Add(counter, str);
                            counter--;
                        }
                        lstOrders.ItemsSource = dic.OrderBy(o => o.Key);
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }

        string GetPhotoRFIDs(string imageIds)
        {
            string imagestoprint = string.Empty;
            try
            {
                //if (_objDataLayer == null)
                //    _objDataLayer = new DigiPhotoDataServices();
                List<PhotosDetails> _objPhotoList = new List<PhotosDetails>();
                PhotosDetails _objphotoitem = null;
                string[] images = imageIds.Split(',');
                List<string> photo = new List<string>();
                //List<DG_Photos> PhotoList = _objDataLayer.GetPhotoRFIDByPhotoID(images);
                List<PhotoInfo> PhotoList = (new PhotoBusiness()).GetPhotoRFIDByPhotoIDList(imageIds);
                if (PhotoList != null)
                {
                    foreach (var img in PhotoList)
                    {
                        if (img != null)
                        {
                            int itemcount = images.Where(t => t.ToString() == img.DG_Photos_pkey.ToString()).ToList().Count;
                            if (itemcount == 1)
                                photo.Add(img.DG_Photos_RFID);
                            else
                                photo.Add("(" + img.DG_Photos_RFID + ")" + "X" + itemcount.ToString());
                            //get photo 

                            _objphotoitem = new PhotosDetails();
                            _objphotoitem.PhotoName = itemcount == 1 ? img.DG_Photos_RFID : ("(" + img.DG_Photos_RFID + ")" + "X" + itemcount.ToString());
                            _objphotoitem.PhotoId = Convert.ToString(img.DG_Photos_pkey);
                            _objphotoitem.ImageVisibility = Visibility.Visible;
                            _objphotoitem.OtherVisibility = Visibility.Visible;
                            _objphotoitem.FilePath = System.IO.Path.Combine(img.HotFolderPath, "Thumbnails", img.DG_Photos_CreatedOn.ToString("yyyyMMdd"), img.DG_Photos_FileName);//LoginUser.DigiFolderThumbnailPath + "\\\\" + img.DG_Photos_FileName;
                            _objPhotoList.Add(_objphotoitem);
                        }
                    }
                }
                lstItems.ItemsSource = _objPhotoList;
                imagestoprint = string.Join(",", photo);
                //foreach (var prntimg in photo)
                //{
                //    if (imagestoprint == string.Empty)
                //        imagestoprint = prntimg;
                //    else
                //        imagestoprint += ", " + prntimg;
                //}
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
            return imagestoprint;
        }

        //private string GetDefaultCurrency()
        //{
        //    DigiPhotoDataServices _objDataLayer = new DigiPhotoDataServices();
        //    var currency = (from objcurrency in _objDataLayer.GetCurrencyList()
        //                    where objcurrency.DG_Currency_Default == true
        //                    select objcurrency.DG_Currency_Symbol.ToString()).FirstOrDefault();
        //    return currency;

        //}
        private string GetDefaultCurrency()
        {

            //DigiPhotoDataServices _objDataLayer = new DigiPhotoDataServices();
            var currency = (from objcurrency in (new CurrencyBusiness()).GetCurrencyList()
                            where objcurrency.DG_Currency_Default == true
                            select objcurrency.DG_Currency_Symbol.ToString()).FirstOrDefault();
            return currency;

        }

        private void btnViewPhotos_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                GrdPrint.Visibility = Visibility.Visible;
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }

        private void btn_Click2(object sender, RoutedEventArgs e)
        {
            if (controlon == null)
                return;
            Button _objbtn = new Button();
            _objbtn = (Button)sender;
            switch (_objbtn.Content.ToString())
            {
                case "ENTER":
                    {
                        controlon.Text = controlon.Text + Environment.NewLine;
                        controlon.Focus();
                        break;
                    }
                case "SPACE":
                    {
                        controlon.Text = controlon.Text + " ";
                        controlon.Focus();
                        break;
                    }
                case "CLOSE":
                    {
                        KeyBorderOrder.Visibility = Visibility.Collapsed;
                        break;
                    }
                case "Back":
                    {
                        TextBox objtxt = (TextBox)(controlon);
                        if (controlon.Text.Length > 0 && controlon.SelectionStart > 0)
                        {
                            int index = controlon.SelectionStart;
                            controlon.Text = controlon.Text.Remove(controlon.SelectionStart - 1, 1);
                            controlon.Select(index - 1, 0);
                        }
                        controlon.Focus();
                        break;
                    }
                default:
                    {
                        controlon.Text = controlon.Text + _objbtn.Content;
                        controlon.SelectionStart = controlon.Text.Length;
                        controlon.Focus();
                    }
                    break;
            }
        }

        private void txtSearchOrderNO_LostFocus(object sender, RoutedEventArgs e)
        {
            btnSearch.IsDefault = false;
        }

        private void txtSearchOrderNO_GotFocus(object sender, RoutedEventArgs e)
        {
            controlon = (TextBox)sender;
            btnSearch.IsDefault = true;
            KeyBorderOrder.Visibility = Visibility.Visible;
        }
        /*
        public List<GetRefundedItems_Result> GetRefundedItems(int OrderId)
        {
            DigiPhotoDataServices _objDbLayer = new DigiPhotoDataServices();
            return _objDbLayer.GetRefundedItems(OrderId);
        }
        */
        void GetOrderDetail()
        {
            try
            {
                OrderBusiness orderBuss = new OrderBusiness();
                order = orderBuss.GetOrderDetailForReceipt(OrderId);
                txt_TotalDiscount.Text = order.CurrencySymbol + " " + order.DiscountTotal.ToString("#.00");
                txt_NetAmount.Text = order.CurrencySymbol + " " + order.NetCost.ToString("#.00");
                txt_TotalAmount.Text = order.CurrencySymbol + " " + order.TotalCost.ToString("#.00");
                DiscountOnTotal = order.DiscountTotal;
                photoRfids = GetPhotoRFIDs(order.PhotoIds);
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }

        private void btnPrintReceipt_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (order != null)
                {
                    Payment payment = LoadXml(order.PaymentDetail);
                    if (payment != null && itemList != null && itemList.Count > 0)
                    {
                        //if (_objDataLayer == null)
                        //    _objDataLayer = new DigiPhotoDataServices();
                        TestBill obj = new TestBill(Common.LoginUser.SubstoreName.ToString(), Common.LoginUser.UserName.ToString(),
                          txtSearchOrderNO.Text.Trim(), photoRfids, (new RefundBusiness()).GetRefundText(),
                          (Math.Round(order.NetCost, 3)).ToString("#0.00"),
                          (Math.Round(payment.Amount, 3)).ToString("#0.00"),
                          (Math.Round(order.NetCost - payment.Amount, 3)).ToString("#0.00"),
                          itemList,
                          payment.Mode,
                          payment.CardNumber,
                          payment.CardHolderName,
                          string.IsNullOrEmpty(payment.CustomerName) ? payment.Name : payment.CustomerName,
                          payment.HotelName,
                          payment.RoomNumber,
                          payment.VoucherNumber,
                          order.CurrencySymbol,
                          OrderId, true, DiscountOnTotal, ""
                          );
                        AuditLog.AddUserLog(LoginUser.UserId, 66, "Order No: " + txtSearchOrderNO.Text.Trim());
                        ResetControls();
                        AuditLog.AddUserLog(Common.LoginUser.UserId, 45, "Receipt reprinted.");
                    }
                }
                else
                {
                    MessageBox.Show("Please search a valid order number.");
                    return;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }

        Payment LoadXml(string ImageXml)
        {
            Payment payment = new Payment();
            try
            {
                //bool checkluminiosity = false;
                System.Xml.XmlDocument Xdocument = new System.Xml.XmlDocument();
                //bool checkDigimagic = false;
                Xdocument.LoadXml(ImageXml);
                foreach (var xn in (Xdocument.ChildNodes[0].ChildNodes[0]).Attributes)
                {
                    switch (((System.Xml.XmlAttribute)xn).Name.ToLower())
                    {
                        case "mode":
                            payment.Mode = ((System.Xml.XmlAttribute)xn).Value.ToUpper();
                            break;
                        case "amount":
                            if (!string.IsNullOrEmpty(((System.Xml.XmlAttribute)xn).Value))
                                payment.Amount = Convert.ToDouble(((System.Xml.XmlAttribute)xn).Value);
                            break;
                        case "cardholdername":
                            payment.CardHolderName = ((System.Xml.XmlAttribute)xn).Value.ToString();
                            break;
                        case "cardnumber":
                            payment.CardNumber = ((System.Xml.XmlAttribute)xn).Value.ToString();
                            break;
                        case "cardtype":
                            payment.CardType = ((System.Xml.XmlAttribute)xn).Value.ToString();
                            break;
                        case "currencyid":
                            payment.CurrencyID = ((System.Xml.XmlAttribute)xn).Value.ToString();
                            break;
                        case "customername":
                            payment.CustomerName = ((System.Xml.XmlAttribute)xn).Value.ToString();
                            break;
                        case "hotelname":
                            payment.HotelName = ((System.Xml.XmlAttribute)xn).Value.ToString();
                            break;
                        case "name":
                            payment.Name = ((System.Xml.XmlAttribute)xn).Value.ToString();
                            break;
                        case "roomnumber":
                            payment.RoomNumber = ((System.Xml.XmlAttribute)xn).Value.ToString();
                            break;
                        case "vouchernumber":
                            payment.VoucherNumber = ((System.Xml.XmlAttribute)xn).Value.ToString();
                            break;
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);

            }
            return payment;
        }


        class Payment
        {
            public Payment()
            {
                Mode = CurrencyID = CardHolderName = CardNumber = Name = CardType = HotelName = RoomNumber = VoucherNumber = CustomerName = string.Empty;
            }
            public string Mode { get; set; }
            public double Amount { get; set; }
            public string CurrencyID { get; set; }
            ////Start Card  Mode
            public string CardHolderName { get; set; }
            public string CardNumber { get; set; }
            public string CardType { get; set; }
            ////End Card  Mode
            //Start Room Mode
            public string HotelName { get; set; }

            public string RoomNumber { get; set; }
            public string Name { get; set; }
            //End Room Mode
            //Start Voucher Mode
            public string VoucherNumber { get; set; }
            public string CustomerName { get; set; }
            //End Voucher Mode
        }

        private void lstOrders_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (lstOrders.SelectedItem != null)
            {
                txtSearchOrderNO.Text = ((KeyValuePair<int, string>)lstOrders.SelectedItem).Value;
            }
        }
    }


}




