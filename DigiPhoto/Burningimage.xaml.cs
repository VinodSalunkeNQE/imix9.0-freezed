﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.IO;
using DigiPhoto.Shader;
using DigiPhoto.DataLayer;
using DigiPhoto.DataLayer.Model;
using System.Collections.ObjectModel;
using DigiPhoto.Common;
using System.Xml;
using DigiPhoto.IMIX.Business;
using DigiPhoto.IMIX.Model;
using DigiPhoto.Utility.Repository.ValueType;

namespace DigiPhoto
{
    /// <summary>
    /// Interaction logic for Burningimage.xaml
    /// </summary>
    public partial class Burningimage : Window
    {
        #region Properties

        public Collection<string> ImageArray
        {
            get;
            set;
        }
        public string ImageEffect
        {
            get;
            set;
        }

        public bool PrintJob
        {
            get;
            set;
        }
        public string PhotoName { get; set; }
        private int _photoId;
        public int PhotoId
        {
            get { return _photoId; }
            set
            {
                _photoId = value;
            }
        }
        #endregion

        //private double _currentbrightness = 0;
        //private double _currentcontrast = 0;

        private double _hueshift = 0;
        private double _sharpen = 0;

        private double _currenthueshift = 0;
        private double _currentsharpen = 0;
        private double _cont = 1;
        private double _bright = 0;

        private double _attributeWidth = 10;
        //private double _attributeHeight = 10;
        private string _centerX = "";
        private string _centerY = "";
        private int _flipMode;
        private int _flipModeY;
        private bool _isGreenImage;
      //  public WriteableBitmap _CurrentBitMap;

        private ContrastAdjustEffect _brighteff = new ContrastAdjustEffect();
        private ContrastAdjustEffect _conteff = new ContrastAdjustEffect();
        private MonochromeEffect _colorfiltereff = new MonochromeEffect();
        private ShiftHueEffect _shifthueeff = new ShiftHueEffect();
        private ShEffect _sharpeff = new ShEffect();
        private BloomEffect _bloomeff = new BloomEffect();
        private ToneMappingEffect _defog = new ToneMappingEffect();
        private ChromaKeyHSVEffect _greenscreen = new ChromaKeyHSVEffect();
        private MultiChannelContrastEffect _under = new MultiChannelContrastEffect();
        private RedEyeEffect redEyeEffect = new RedEyeEffect();
        private RedEyeEffect redEyeEffectSecond = new RedEyeEffect();
        private redeyeMultiple redEyeEffectMultiple = new redeyeMultiple();
        private redeyeMultiple redEyeEffectMultiple1 = new redeyeMultiple();

        private int rotateangle = 0;
        private int graphicsTextBoxCount = 0;
        private int graphicsCount = 0;
        private int photoId = 0;
        private bool graphicsBorderApplied = false;
        private bool graphicsframeApplied = false;
        private TransformGroup _transformGroup;
        private TranslateTransform _translateTransform;
        private ScaleTransform _zoomTransform;
        private RotateTransform _rotateTransform;
        private double _zoomFactor = 1;
        ///private double _maxZoomFactor = 4;
        ///private double _graphicsZoomFactor = 1;
        ///private WriteableBitmap _bmp;

        public Burningimage()
        {
            InitializeComponent();
        }
        private void Clear()
        {
            RemoveAllShaderEffects();
            RemoveAllGraphicsEffect();
            //ReloadAllGraphicsEffect();
        }
        private void RemoveAllGraphicsEffect()
        {
            try
            {
                canbackground.Background = null;
                var itemstoremove = new List<UIElement>();
                foreach (UIElement ui in dragCanvas.Children)
                {
                    if (ui is Grid)
                    {
                    }
                    else
                    {
                        itemstoremove.Add(ui);
                    }
                }
                foreach (UIElement ui in itemstoremove)
                {
                    dragCanvas.Children.Remove(ui);
                }


                itemstoremove = new List<UIElement>();
                foreach (UIElement ui in frm.Children)
                {
                    if (ui is OpaqueClickableImage)
                    {
                        itemstoremove.Add(ui);
                    }
                }
                foreach (UIElement ui in itemstoremove)
                {
                    frm.Children.Remove(ui);
                }

                _zoomFactor = 1;
                if (_zoomTransform != null)
                {
                    _zoomTransform.CenterX = mainImage.ActualWidth / 2;
                    _zoomTransform.CenterY = mainImage.ActualHeight / 2;

                    _zoomTransform.ScaleX = _zoomFactor;
                    _zoomTransform.ScaleY = _zoomFactor;
                    _zoomTransform = new ScaleTransform();
                    _transformGroup = new TransformGroup();
                    _rotateTransform = new RotateTransform();
                    //transformGroup.Children.Add(zoomTransform);
                    //transformGroup.Children.Add(rotateTransform);
                    //GrdGreenScreenDefault3.RenderTransform = null;

                    //GrdBrightness.RenderTransform = transformGroup;
                    GrdBrightness.RenderTransform = null;

                    //mainImage.Source = new BitmapImage(new Uri(LoginUser.DigiFolderPath + PhotoName + ".jpg"));

                    // BitmapImage _tempimage = new BitmapImage();
                    //_tempimage = new BitmapImage(new Uri(LoginUser.DigiFolderPath + PhotoName + ".jpg"));
                    //_objMainImage = BitmapImage2Bitmap(_tempimage);
                    //_CurrentBitMap = new WriteableBitmap(_tempimage);
                    Canvas.SetLeft(GrdBrightness, 0);
                    Canvas.SetTop(GrdBrightness, 0);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }
        private void ReloadAllGraphicsEffect()
        {
            try
            {
                canbackground.Background = null;
                List<UIElement> itemstoremove = new List<UIElement>();
                foreach (UIElement ui in dragCanvas.Children)
                {
                    if (ui is Grid)
                    {
                    }
                    else
                    {
                        itemstoremove.Add(ui);
                    }
                }
                foreach (UIElement ui in itemstoremove)
                {
                    dragCanvas.Children.Remove(ui);
                }

                itemstoremove = new List<UIElement>();
                foreach (UIElement ui in frm.Children)
                {
                    if (ui is OpaqueClickableImage)
                    {
                        itemstoremove.Add(ui);
                    }
                }
                foreach (UIElement ui in itemstoremove)
                {
                    frm.Children.Remove(ui);
                }

                if (_flipMode != 0 || _flipModeY != 0)
                {
                    _zoomFactor = 1;

                    _zoomTransform.CenterX = Convert.ToDouble(_centerX);
                    _zoomTransform.CenterY = Convert.ToDouble(_centerY);

                    _zoomTransform.ScaleX = 1;
                    _zoomTransform.ScaleY = 1;

                    _translateTransform = new TranslateTransform();
                    _rotateTransform = new RotateTransform();
                    _transformGroup = new TransformGroup();

                    GrdBrightness.RenderTransform = null;

                    Canvas.SetLeft(GrdBrightness, 0);
                    Canvas.SetTop(GrdBrightness, 0);
                    FlipLoad();
                }
                else
                {
                    _zoomFactor = 1;

                    _zoomTransform.CenterX = mainImage.ActualWidth / 2;
                    _zoomTransform.CenterY = mainImage.ActualHeight / 2;

                    _zoomTransform.ScaleX = _zoomFactor;
                    _zoomTransform.ScaleY = _zoomFactor;


                    _transformGroup = new TransformGroup();
                    _rotateTransform = new RotateTransform();
                    _zoomTransform = new ScaleTransform();


                    //GrdBrightness.RenderTransform = transformGroup;
                    GrdBrightness.RenderTransform = null;

                    Canvas.SetLeft(GrdBrightness, 0);
                    Canvas.SetTop(GrdBrightness, 0);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }
        private void RemoveAllShaderEffects()
        {
            try
            {
                GrdInvert.Effect = null;
                GrdSharpen.Effect = null;
                GrdSketchGranite.Effect = null;
                GrdEmboss.Effect = null;
                Grdcartoonize.Effect = null;
                GrdGreyScale.Effect = null;
                GrdHueShift.Effect = null;
                Grdcolorfilter.Effect = null;
                GrdBrightness.Effect = null;
                GrdSepia.Effect = null;

                GrdRedEyeFirst.Effect = null;
                GrdRedEyeSecond.Effect = null;
                GrdRedEyeMultiple.Effect = null;
                GrdRedEyeMultiple1.Effect = null;
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }
        private void ImageLoader(PhotoInfo objphoto)
        {
            //double height = 0;
            //double width = 0;

            grdZoomCanvas.UpdateLayout();
            GrdSize.UpdateLayout();
            Clear();

            //var objDbLayer = new DigiPhotoDataServices();

            _attributeWidth = 10;

            _centerX = "";
            _centerY = "";
            _flipMode = 0;
            _flipModeY = 0;

            //  txtMainImage.Text = PhotoName;


            //MyInkCanvas.SnapsToDevicePixels = true;
            //MyInkCanvas.SetValue(RenderOptions.EdgeModeProperty, EdgeMode.Aliased);
            mainImage.SnapsToDevicePixels = true;
            RenderOptions.SetEdgeMode(mainImage, EdgeMode.Aliased);

            forWdht.RenderTransform = new RotateTransform();
            PhotoInfo objPhotoDetails = null;
            try
            {
                //BitmapImage _tempimage = new BitmapImage();

                objPhotoDetails = (new PhotoBusiness()).GetPhotoDetailsbyPhotoId(PhotoId);

                //bool redeye = objDbLayer.CheckIsCropedPhotos(PhotoId, "RedEye");
                //bool crop = objDbLayer.CheckIsCropedPhotos(PhotoId, "Crop");
                //_isGreenImage = objDbLayer.CheckIsCropedPhotos(PhotoId, "GreenImage");

                bool redeye = (objPhotoDetails.DG_Photos_IsRedEye != null);
                bool crop = (objPhotoDetails.DG_Photos_IsCroped != null);
                _isGreenImage = (objPhotoDetails.DG_Photos_IsGreen != null);

                if (crop && !_isGreenImage)
                {
                    using (FileStream fileStream = File.OpenRead(System.IO.Path.Combine(objphoto.HotFolderPath, "Croped", objphoto.DG_Photos_FileName)))
                    {
                        var bi = new BitmapImage();
                        using (var ms = new MemoryStream())
                        {
                            fileStream.CopyTo(ms);
                            ms.Seek(0, SeekOrigin.Begin);
                            fileStream.Close();
                            bi.BeginInit();
                            bi.StreamSource = ms;
                            bi.EndInit();
                            // bi.Freeze();
                            mainImage.Source = bi;
                            widthimg.Source = bi;
                            //height = bi.Height;
                            //width = bi.Width;
                            // _tempimage = bi;
                            // _CurrentBitMap = new WriteableBitmap(_tempimage);
                        }

                    }

                }
                else
                {
                    using (FileStream fileStream = File.OpenRead(System.IO.Path.Combine(objphoto.HotFolderPath, objphoto.DG_Photos_CreatedOn.ToString("yyyyMMdd"), objphoto.DG_Photos_FileName)))
                    {
                        var bi = new BitmapImage();
                        using (var ms = new MemoryStream())
                        {
                            fileStream.CopyTo(ms);
                            ms.Seek(0, SeekOrigin.Begin);
                            fileStream.Close();
                            bi.BeginInit();
                            bi.StreamSource = ms;
                            bi.EndInit();

                            mainImage.Source = bi;
                            widthimg.Source = bi;
                            //height = bi.Height;
                            //width = bi.Width;
                        }
                    }

                }


                forWdht.Height = widthimg.Source.Height;
                forWdht.Width = widthimg.Source.Width;

                //if (ImageEffect != null && ImageEffect != string.Empty && !_isGreenImage)
                if (!String.IsNullOrEmpty(ImageEffect) && !_isGreenImage)
                {
                    LoadXml(ImageEffect);
                }

                dragCanvas.AllowDragOutOfView = true;

                GrdBrightness.Margin = new Thickness(0, 0, 0, 0);
                forWdht.HorizontalAlignment = System.Windows.HorizontalAlignment.Center;
                forWdht.VerticalAlignment = System.Windows.VerticalAlignment.Center;

                if (!_isGreenImage)
                {
                    //Zomout();
                    LoadFromDB();
                    Zomout(true);
                }
                else
                {
                    forWdht.Width = widthimg.Source.Width;
                    forWdht.Height = widthimg.Source.Height;
                    GrdGreenScreenDefault3.RenderTransform = null;
                    Canvas.SetLeft(GrdBrightness, 0);
                    Canvas.SetTop(GrdBrightness, 0);
                    GrdBrightness.RenderTransform = null;
                    grdZoomCanvas.RenderTransform = null;
                }

                int sharpenfactor = 3;

                if (System.Configuration.ConfigurationSettings.AppSettings["SharpenFactor"] != null)
                {
                    sharpenfactor = Convert.ToInt32(System.Configuration.ConfigurationSettings.AppSettings["SharpenFactor"]);
                }

                // Apply Default Sharpen
                if (sharpenfactor > 0)
                {
                    _currentsharpen = _currentsharpen + (0.025 * sharpenfactor);
                    _sharpeff.Strength = _currentsharpen;
                    _sharpeff.PixelWidth = 0.0015;
                    _sharpeff.PixelHeight = 0.0015;
                    GrdSharpen.Effect = _sharpeff;
                }

                //if (ProductTypeID == 1) // 6 * 8
                //{
                //    if (width > height)
                //    {
                //        GrdSize.Width = width;
                //        GrdSize.Height = (double)(width * .75);
                //    }
                //    else
                //    {
                //        GrdSize.Height = height;
                //        GrdSize.Width = (double)(height * 1.34);

                //    }
                //}
                //if (ProductTypeID == 2) // 8 * 10
                //{
                //    if (width > height)
                //    {
                //        GrdSize.Width = width;
                //        GrdSize.Height = (double)(width * 0.8);
                //    }
                //    else
                //    {
                //        GrdSize.Height = height;
                //        GrdSize.Width = (double)(height * 1.25);

                //    }
                //}
                //if (ProductTypeID == 30) // 8 * 10
                //{
                //    if (width > height)
                //    {
                //        GrdSize.Width = width;
                //        GrdSize.Height = (double)(width * 0.67);
                //    }
                //    else
                //    {
                //        GrdSize.Height = height;
                //        GrdSize.Width = (double)(height * 1.5);

                //    }
                //}



                _currentsharpen = 0;
                // _bmp = new WriteableBitmap((BitmapSource)mainImage.Source);
                dragCanvas.AllowDragging = false;
                dragCanvas.IsEnabled = false;


            }
            finally
            {
                objPhotoDetails = null;
            }
        }

        public void ResetForm(PhotoInfo objphoto)
        {

            _translateTransform = new TranslateTransform();
            _rotateTransform = new RotateTransform();
            _transformGroup = new TransformGroup();

            ErrorHandler.ErrorHandler.LogFileWrite("Load Photo " + PhotoId);
            if (PhotoId > 0)
            {
                ErrorHandler.ErrorHandler.LogFileWrite("Photo Details Completed");
                TbNumber.Text = objphoto.DG_Photos_RFID;
                photoId = objphoto.DG_Photos_pkey;
                ImageEffect = objphoto.DG_Photos_Effects;
                //Clear();
                ImageLoader(objphoto);
            }
        }

        private void LoadXaml(String inputXml)
        {
            var xdocument = new System.Xml.XmlDocument();
            xdocument.LoadXml(inputXml);

            double translateX = 0;
            double translateY = 0;

            _transformGroup = new TransformGroup();
            _rotateTransform = new RotateTransform();
            try
            {
                if (xdocument.ChildNodes[0] != null)
                {
                    foreach (var xn in (xdocument.ChildNodes[0]).Attributes)
                    {
                        switch (((System.Xml.XmlAttribute) xn).Name.ToLower())
                        {
                            case "border":
                                if (((System.Xml.XmlAttribute) xn).Value != string.Empty)
                                {
                                    BitmapImage img =
                                        new BitmapImage(
                                            new Uri(Common.LoginUser.DigiFolderFramePath + @"\" +
                                                    ((System.Xml.XmlAttribute) xn).Value.ToString()));
                                    string[] tempFileName = img.ToString().Split('/');
                                    OpaqueClickableImage objCurrent = new OpaqueClickableImage();
                                    objCurrent.Uid = "frame";
                                    objCurrent.Source = img;
                                    objCurrent.IsHitTestVisible = false;
                                    objCurrent.Stretch = Stretch.Fill; //temprorary Fix till Crop works properly
                                    objCurrent.Loaded += new RoutedEventHandler(objCurrent_Loaded);

                                    double ratio = 1;
                                    if (img.Height > img.Width)
                                    {
                                        ratio = (double) (img.Width/img.Height);
                                    }
                                    else
                                    {
                                        ratio = (double) (img.Height/img.Width);
                                    }

                                    if (forWdht.Height > forWdht.Width)
                                    {
                                        forWdht.Width = forWdht.Height*ratio;
                                    }
                                    else
                                    {
                                        forWdht.Height = forWdht.Width*ratio;
                                    }

                                    //forWdht.Height = img.Height;
                                    //forWdht.Width = img.Width;
                                    frm.Width = forWdht.Width;
                                    frm.Height = forWdht.Height;
                                    forWdht.InvalidateArrange();
                                    forWdht.InvalidateMeasure();
                                    forWdht.InvalidateVisual();
                                    frm.Children.Add(objCurrent);
                                }
                                break;
                            case "bg":
                                if (((System.Xml.XmlAttribute) xn).Value != string.Empty)
                                {
                                    BitmapImage objCurrent =
                                        new BitmapImage(
                                            new Uri(Common.LoginUser.DigiFolderBackGroundPath + @"\" +
                                                    ((System.Xml.XmlAttribute) xn).Value));
                                    canbackground.Background = new ImageBrush {ImageSource = objCurrent};
                                }
                                break;
                            case "canvasleft":
                                Canvas.SetLeft(GrdBrightness, Convert.ToDouble((((System.Xml.XmlAttribute) xn).Value)));
                                break;
                            case "canvastop":
                                Canvas.SetTop(GrdBrightness, Convert.ToDouble((((System.Xml.XmlAttribute) xn).Value)));
                                break;
                            case "rotatetransform":
                                RotateTransform rtm = new RotateTransform();
                                rtm.CenterX = 0;
                                rtm.CenterY = 0;
                                rtm.Angle = Convert.ToDouble((((System.Xml.XmlAttribute) xn).Value));
                                GrdBrightness.RenderTransformOrigin = new System.Windows.Point(0.5, 0.5);
                                if (rtm.Angle != -1 && rtm.Angle != 0)
                                {
                                    GrdBrightness.RenderTransform = rtm;
                                }
                                break;
                            case "scalecentrex":
                                _centerX = ((System.Xml.XmlAttribute) xn).Value;
                                break;
                            case "scalecentrey":
                                _centerY = ((System.Xml.XmlAttribute) xn).Value;
                                break;
                            case "zoomfactor":
                                _zoomFactor = Convert.ToDouble(((System.Xml.XmlAttribute) xn).Value);
                                break;
                        }
                    }
                }

                if (_centerX != "-1")
                {
                    ScaleTransform st = new ScaleTransform();
                    st.CenterX = Convert.ToDouble(_centerX);
                    st.CenterY = Convert.ToDouble(_centerY);

                    if (_zoomFactor != -1)
                    {
                        st.ScaleX = _zoomFactor;
                        st.ScaleY = _zoomFactor;
                    }

                    if (_flipMode != 0 || _flipModeY != 0)
                    {
                        st.ScaleX = st.ScaleX;
                    }

                    _zoomTransform = st;
                    _transformGroup.Children.Add(st);
                }

                if (translateX != 0)
                {
                    TranslateTransform st = new TranslateTransform();
                    st.X = Convert.ToDouble(translateX);
                    st.Y = Convert.ToDouble(translateY);
                    _translateTransform = st;
                    _transformGroup.Children.Add(st);
                }

                if (GrdGreenScreenDefault3.RenderTransform == null)
                    GrdGreenScreenDefault3.RenderTransform = _transformGroup;

                System.Xml.XmlReader rdr = System.Xml.XmlReader.Create(new System.IO.StringReader(xdocument.InnerXml));
                while (rdr.Read())
                {
                    if (rdr.NodeType == XmlNodeType.Element)
                    {
                        switch (rdr.Name.ToString().ToLower())
                        {
                            case "graphics":
                                Button btngrph = new Button();
                                btngrph.HorizontalAlignment = System.Windows.HorizontalAlignment.Center;
                                btngrph.VerticalAlignment = System.Windows.VerticalAlignment.Center;
                                Style defaultStyle = (Style)FindResource("ButtonStyleGraphic");
                                btngrph.Style = defaultStyle;
                                System.Windows.Controls.Image imgctrl = new System.Windows.Controls.Image();
                                BitmapImage img = new BitmapImage(new Uri(Common.LoginUser.DigiFolderGraphicsPath + @"\" + rdr.GetAttribute("source")));
                                imgctrl.Name = "A" + Guid.NewGuid().ToString().Split('-')[0];
                                btngrph.Name = "btn" + Guid.NewGuid().ToString().Split('-')[0];
                                btngrph.Uid = "uid" + Guid.NewGuid().ToString().Split('-')[0];
                                img.BeginInit();
                                imgctrl.Source = img;
                                img.EndInit();
                                btngrph.Width = 90;
                                btngrph.Height = 90;

                                btngrph.Content = imgctrl;
                                dragCanvas.Children.Add(btngrph);
                                var left = String.Format("{0:0.00}", Convert.ToDouble(rdr.GetAttribute("left")));
                                var top = String.Format("{0:0.00}", Convert.ToDouble(rdr.GetAttribute("top")));
                                Canvas.SetLeft(btngrph, Convert.ToDouble(left));
                                Canvas.SetTop(btngrph, Convert.ToDouble(top));

                                Double ZoomFactor = Convert.ToDouble(rdr.GetAttribute("zoomfactor"));
                                btngrph.Width = Convert.ToDouble(rdr.GetAttribute("wthsource")) != null ? Convert.ToDouble(rdr.GetAttribute("wthsource")) : 90;
                                btngrph.Height = Convert.ToDouble(rdr.GetAttribute("wthsource")) != null ? Convert.ToDouble(rdr.GetAttribute("wthsource")) : 90;
                                btngrph.Tag = ZoomFactor.ToString();
                                TransformGroup tg = new TransformGroup();
                                RotateTransform rotation = new RotateTransform();
                                ScaleTransform scale = new ScaleTransform();
                                scale.CenterX = 0;
                                scale.CenterY = 0;
                                if (rdr.GetAttribute("scalex") != null)
                                {
                                    scale.ScaleX = Convert.ToDouble(rdr.GetAttribute("scalex").ToString());
                                }
                                if (rdr.GetAttribute("scaley") != null)
                                {
                                    scale.ScaleY = Convert.ToDouble(rdr.GetAttribute("scaley").ToString());
                                }

                                btngrph.RenderTransformOrigin = new System.Windows.Point(0.5, 0.5);
                                tg.Children.Add(scale);
                                rotation.CenterX = 0;
                                rotation.CenterY = 0;
                                rotation.Angle = Convert.ToDouble(rdr.GetAttribute("angle").ToString());
                                btngrph.RenderTransformOrigin = new System.Windows.Point(0.5, 0.5);
                                tg.Children.Add(rotation);

                                btngrph.RenderTransform = tg;

                                graphicsCount++;
                                break;
                            case "textbox":

                                TextBox txtTest = new TextBox();
                                txtTest.ContextMenu = dragCanvas.ContextMenu;
                                txtTest.Background = new SolidColorBrush(Colors.Transparent);
                                if (rdr.GetAttribute("foreground").ToString() != string.Empty)
                                {
                                    var converter = new System.Windows.Media.BrushConverter();
                                    var brush = (System.Windows.Media.Brush)converter.ConvertFromString(rdr.GetAttribute("foreground").ToString());
                                    txtTest.Foreground = brush;
                                }
                                else
                                {
                                    txtTest.Foreground = new SolidColorBrush(Colors.DarkRed);
                                }

                                if (rdr.GetAttribute("fontsize").ToString() != string.Empty)
                                {
                                    txtTest.FontSize = Convert.ToDouble(rdr.GetAttribute("fontsize").ToString());
                                }
                                else
                                {
                                    txtTest.FontSize = 20.00;
                                }

                                if (rdr.GetAttribute("_fontfamily").ToString() != string.Empty)
                                {
                                    System.Windows.Media.FontFamily fw = (System.Windows.Media.FontFamily)new FontFamilyConverter().ConvertFromString(rdr.GetAttribute("_fontfamily").ToString());
                                    txtTest.FontFamily = fw;
                                }

                                txtTest.RenderTransformOrigin = new System.Windows.Point(0.5, 0.5);
                                txtTest.Uid = "txtblock";
                                txtTest.FontWeight = FontWeights.Bold;
                                txtTest.BorderBrush = null;
                                txtTest.Style = (Style)FindResource("SearchIDTB");
                                dragCanvas.Children.Add(txtTest);
                                Canvas.SetLeft(txtTest, Convert.ToDouble(rdr.GetAttribute("left").ToString()));
                                Canvas.SetTop(txtTest, Convert.ToDouble(rdr.GetAttribute("top").ToString()));
                                rotation = new RotateTransform();
                                rotation.CenterX = 0;
                                rotation.CenterY = 0;
                                rotation.Angle = Convert.ToDouble(rdr.GetAttribute("angle").ToString());
                                txtTest.RenderTransformOrigin = new System.Windows.Point(0.5, 0.5);
                                txtTest.RenderTransform = rotation;

                                txtTest.Text = rdr.GetAttribute("text").ToString();

                                graphicsTextBoxCount++;
                                break;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }

        private void LoadXml(string imageXml)
        {
            try
            {
                System.Windows.Media.Color color;
                bool checkluminiosity = false;
                System.Xml.XmlDocument Xdocument = new System.Xml.XmlDocument();
                bool checkDigimagic = false;
                Xdocument.LoadXml(imageXml);
                foreach (var xn in (Xdocument.ChildNodes[0]).Attributes)
                {
                    switch (((System.Xml.XmlAttribute)xn).Name.ToLower())
                    {
                        case "brightness":
                            if (((System.Xml.XmlAttribute)xn).Value.ToString() != "0")
                            {
                                _bright = Convert.ToDouble(((System.Xml.XmlAttribute)xn).Value.ToString());
                                _brighteff.Brightness = _bright;
                                _brighteff.Contrast = 1;

                                GrdBrightness.Effect = _brighteff;
                                //currentbrightness = bright;
                                //VisualStateManager.GoToState(btnColorEffects, "Checked", true);
                                checkluminiosity = true;
                            }
                            else
                            {
                                GrdBrightness.Effect = null;
                                //   bright = 0;
                                // currentbrightness = bright;
                                // VisualStateManager.GoToState(btnColorEffects, "Unchecked", true);
                            }
                            break;



                        case "firstredeye":
                            if (((System.Xml.XmlAttribute)xn).Value.ToString() == "true")
                            {
                                redEyeEffect.Radius = .0105;
                                redEyeEffect.RedeyeTrue = 1;

                                GrdRedEyeFirst.Effect = redEyeEffect;
                                redEyeEffect.Center = new Point(.5, .5);

                                //  redeffectfirstApplied = true;
                            }
                            else
                            {
                                GrdRedEyeFirst.Effect = null;
                            }
                            break;
                        case "secondredeye":
                            if (((System.Xml.XmlAttribute)xn).Value.ToString() == "true")
                            {
                                // redeffectsecondApplied = true;
                                redEyeEffectSecond.Radius = .0105;
                                redEyeEffectSecond.RedeyeTrue = 1;

                                GrdRedEyeSecond.Effect = redEyeEffectSecond;
                                redEyeEffectSecond.Center = new Point(.5, .5);
                            }
                            else
                            {
                                GrdRedEyeSecond.Effect = null;
                            }
                            break;
                        case "firstradius":
                            if (((System.Xml.XmlAttribute)xn).Value.ToString() != "0")
                            {
                                redEyeEffect.Radius = Convert.ToDouble(((System.Xml.XmlAttribute)xn).Value);
                            }
                            break;
                        case "aspectratiofirstredeye":
                            if (((System.Xml.XmlAttribute)xn).Value.ToString() != "0")
                            {
                                redEyeEffect.AspectRatio = Convert.ToDouble(((System.Xml.XmlAttribute)xn).Value);
                            }
                            break;
                        case "aspectratiosecondredeye":
                            if (((System.Xml.XmlAttribute)xn).Value.ToString() != "0")
                            {
                                redEyeEffectSecond.AspectRatio = Convert.ToDouble(((System.Xml.XmlAttribute)xn).Value);
                            }
                            break;


                        case "secondradius":
                            if (((System.Xml.XmlAttribute)xn).Value.ToString() != "0")
                            {
                                redEyeEffectSecond.Radius = Convert.ToDouble(((System.Xml.XmlAttribute)xn).Value);
                            }
                            break;
                        case "firstcenterx":
                            if (((System.Xml.XmlAttribute)xn).Value.ToString() != "0")
                            {
                                redEyeEffect.Center = new Point(Convert.ToDouble(((System.Xml.XmlAttribute)xn).Value), .5);
                            }
                            break;
                        case "firstcentery":
                            if (((System.Xml.XmlAttribute)xn).Value.ToString() != "0")
                            {
                                redEyeEffect.Center = new Point(redEyeEffect.Center.X, Convert.ToDouble(((System.Xml.XmlAttribute)xn).Value));
                            }
                            break;
                        case "secondcenterx":
                            if (((System.Xml.XmlAttribute)xn).Value.ToString() != "0")
                            {
                                redEyeEffectSecond.Center = new Point(Convert.ToDouble(((System.Xml.XmlAttribute)xn).Value), .5);
                            }
                            break;
                        case "secondcentery":
                            if (((System.Xml.XmlAttribute)xn).Value.ToString() != "0")
                            {
                                redEyeEffectSecond.Center = new Point(redEyeEffectSecond.Center.X, Convert.ToDouble(((System.Xml.XmlAttribute)xn).Value));
                            }
                            break;
                        //new red eye code..

                        case "multipleredeye1":
                            if (((System.Xml.XmlAttribute)xn).Value.ToString() == "true")
                            {
                                redEyeEffectMultiple.Radius = .0105;
                                redEyeEffectMultiple.RedeyeTrue = 1;

                                GrdRedEyeMultiple.Effect = redEyeEffectMultiple;
                                redEyeEffectMultiple.Center = new Point(.5, .5);

                                // redeffectmultiple1Applied = true;
                            }
                            else
                            {
                                GrdRedEyeMultiple.Effect = null;
                            }
                            break;
                        case "multipleradius1":
                            if (((System.Xml.XmlAttribute)xn).Value.ToString() != "0")
                            {
                                redEyeEffectMultiple.Radius = Convert.ToDouble(((System.Xml.XmlAttribute)xn).Value);
                            }

                            break;
                        case "multiplecenterx1":
                            if (((System.Xml.XmlAttribute)xn).Value.ToString() != "0")
                            {
                                redEyeEffectMultiple.Center = new Point(Convert.ToDouble(((System.Xml.XmlAttribute)xn).Value), .5);
                            }
                            break;
                        case "multiplecentery1":
                            if (((System.Xml.XmlAttribute)xn).Value.ToString() != "0")
                            {
                                redEyeEffectMultiple.Center = new Point(redEyeEffectMultiple.Center.X, Convert.ToDouble(((System.Xml.XmlAttribute)xn).Value));
                            }
                            break;


                        case "multipleredeye2":
                            if (((System.Xml.XmlAttribute)xn).Value.ToString() == "true")
                            {
                                redEyeEffectMultiple.Radius1 = .0105;
                                redEyeEffectMultiple.RedeyeTrue1 = 1;

                                GrdRedEyeMultiple.Effect = redEyeEffectMultiple;
                                redEyeEffectMultiple.Center1 = new Point(.5, .5);

                                // redeffectmultiple2Applied = true;
                            }
                            else
                            {

                            }
                            break;
                        case "multipleradius2":
                            if (((System.Xml.XmlAttribute)xn).Value.ToString() != "0")
                            {
                                redEyeEffectMultiple.Radius1 = Convert.ToDouble(((System.Xml.XmlAttribute)xn).Value);
                            }

                            break;
                        case "multiplecenterx2":
                            if (((System.Xml.XmlAttribute)xn).Value.ToString() != "0")
                            {
                                redEyeEffectMultiple.Center1 = new Point(Convert.ToDouble(((System.Xml.XmlAttribute)xn).Value), .5);
                            }
                            break;
                        case "multiplecentery2":
                            if (((System.Xml.XmlAttribute)xn).Value.ToString() != "0")
                            {
                                redEyeEffectMultiple.Center1 = new Point(redEyeEffectMultiple.Center1.X, Convert.ToDouble(((System.Xml.XmlAttribute)xn).Value));
                            }
                            break;


                        case "multipleredeye3":
                            if (((System.Xml.XmlAttribute)xn).Value.ToString() == "true")
                            {
                                redEyeEffectMultiple.Radius2 = .0105;
                                redEyeEffectMultiple.RedeyeTrue2 = 1;

                                GrdRedEyeMultiple.Effect = redEyeEffectMultiple;
                                redEyeEffectMultiple.Center2 = new Point(.5, .5);

                                // redeffectmultiple3Applied = true;
                            }
                            else
                            {

                            }
                            break;
                        case "multipleradius3":
                            if (((System.Xml.XmlAttribute)xn).Value.ToString() != "0")
                            {
                                redEyeEffectMultiple.Radius2 = Convert.ToDouble(((System.Xml.XmlAttribute)xn).Value);
                            }

                            break;
                        case "multiplecenterx3":
                            if (((System.Xml.XmlAttribute)xn).Value.ToString() != "0")
                            {
                                redEyeEffectMultiple.Center2 = new Point(Convert.ToDouble(((System.Xml.XmlAttribute)xn).Value), .5);
                            }
                            break;
                        case "multiplecentery3":
                            if (((System.Xml.XmlAttribute)xn).Value.ToString() != "0")
                            {
                                redEyeEffectMultiple.Center2 = new Point(redEyeEffectMultiple.Center2.X, Convert.ToDouble(((System.Xml.XmlAttribute)xn).Value));
                            }
                            break;


                        case "multipleredeye4":
                            if (((System.Xml.XmlAttribute)xn).Value.ToString() == "true")
                            {
                                redEyeEffectMultiple1.Radius = .0105;
                                redEyeEffectMultiple1.RedeyeTrue = 1;

                                GrdRedEyeMultiple1.Effect = redEyeEffectMultiple1;
                                redEyeEffectMultiple1.Center = new Point(.5, .5);

                                //  redeffectmultiple4Applied = true;
                            }
                            else
                            {
                                GrdRedEyeMultiple1.Effect = null;
                            }
                            break;
                        case "multipleradius4":
                            if (((System.Xml.XmlAttribute)xn).Value.ToString() != "0")
                            {
                                redEyeEffectMultiple1.Radius = Convert.ToDouble(((System.Xml.XmlAttribute)xn).Value);
                            }

                            break;
                        case "multiplecenterx4":
                            if (((System.Xml.XmlAttribute)xn).Value.ToString() != "0")
                            {
                                redEyeEffectMultiple1.Center = new Point(Convert.ToDouble(((System.Xml.XmlAttribute)xn).Value), .5);
                            }
                            break;
                        case "multiplecentery4":
                            if (((System.Xml.XmlAttribute)xn).Value.ToString() != "0")
                            {
                                redEyeEffectMultiple1.Center = new Point(redEyeEffectMultiple1.Center.X, Convert.ToDouble(((System.Xml.XmlAttribute)xn).Value));
                            }
                            break;


                        case "multipleredeye5":
                            if (((System.Xml.XmlAttribute)xn).Value.ToString() == "true")
                            {
                                redEyeEffectMultiple1.Radius1 = .0105;
                                redEyeEffectMultiple1.RedeyeTrue1 = 1;

                                GrdRedEyeMultiple1.Effect = redEyeEffectMultiple1;
                                redEyeEffectMultiple1.Center1 = new Point(.5, .5);

                                //  redeffectmultiple5Applied = true;
                            }
                            else
                            {

                            }
                            break;
                        case "multipleradius5":
                            if (((System.Xml.XmlAttribute)xn).Value.ToString() != "0")
                            {
                                redEyeEffectMultiple1.Radius1 = Convert.ToDouble(((System.Xml.XmlAttribute)xn).Value);
                            }

                            break;
                        case "multiplecenterx5":
                            if (((System.Xml.XmlAttribute)xn).Value.ToString() != "0")
                            {
                                redEyeEffectMultiple1.Center1 = new Point(Convert.ToDouble(((System.Xml.XmlAttribute)xn).Value), .5);
                            }
                            break;
                        case "multiplecentery5":
                            if (((System.Xml.XmlAttribute)xn).Value.ToString() != "0")
                            {
                                redEyeEffectMultiple1.Center1 = new Point(redEyeEffectMultiple1.Center1.X, Convert.ToDouble(((System.Xml.XmlAttribute)xn).Value));
                            }
                            break;


                        case "multipleredeye6":
                            if (((System.Xml.XmlAttribute)xn).Value.ToString() == "true")
                            {
                                redEyeEffectMultiple1.Radius2 = .0105;
                                redEyeEffectMultiple1.RedeyeTrue2 = 1;

                                GrdRedEyeMultiple1.Effect = redEyeEffectMultiple1;
                                redEyeEffectMultiple1.Center2 = new Point(.5, .5);

                                // redeffectmultiple6Applied = true;
                            }
                            else
                            {

                            }
                            break;
                        case "multipleradius6":
                            if (((System.Xml.XmlAttribute)xn).Value.ToString() != "0")
                            {
                                redEyeEffectMultiple1.Radius2 = Convert.ToDouble(((System.Xml.XmlAttribute)xn).Value);
                            }

                            break;
                        case "multiplecenterx6":
                            if (((System.Xml.XmlAttribute)xn).Value.ToString() != "0")
                            {
                                redEyeEffectMultiple1.Center2 = new Point(Convert.ToDouble(((System.Xml.XmlAttribute)xn).Value), .5);
                            }
                            break;
                        case "multiplecentery6":
                            if (((System.Xml.XmlAttribute)xn).Value.ToString() != "0")
                            {
                                redEyeEffectMultiple1.Center2 = new Point(redEyeEffectMultiple1.Center2.X, Convert.ToDouble(((System.Xml.XmlAttribute)xn).Value));
                            }
                            break;

   
                        case "contrast":
                            if (((System.Xml.XmlAttribute)xn).Value.ToString() != "1")
                            {
                                if (((System.Xml.XmlAttribute)xn).Value.ToString() == "##")
                                {
                                    _cont = 1;
                                }
                                else
                                {
                                    _cont = Convert.ToDouble(((System.Xml.XmlAttribute)xn).Value.ToString());
                                }

                                _conteff.Brightness = _bright;
                                _conteff.Contrast = _cont;
                                GrdBrightness.Effect = _conteff;
                                //currentcontrast = cont;
                                //VisualStateManager.GoToState(btnColorEffects, "Checked", true);
                                checkluminiosity = true;
                            }
                            else
                            {
                                //GrdBrightness.Effect = null;
                                //cont = 1;
                                //currentcontrast = cont;
                                //VisualStateManager.GoToState(btnColorEffects, "Unchecked", true);
                            }
                            break;
                        case "rotate":
                            if (((System.Xml.XmlAttribute)xn).Value.ToString() != "##")
                            {

                                rotateangle = Convert.ToInt32(((System.Xml.XmlAttribute)xn).Value.ToString());
                                Rotate(rotateangle);

                            }
                            else
                            {

                            }

                            break;

                        case "flipmode":
                            if (((System.Xml.XmlAttribute)xn).Value.ToString() != "##")
                            {
                                _flipMode = Convert.ToInt32(((System.Xml.XmlAttribute)xn).Value.ToString());
                            }
                            else
                            {
                                _flipMode = 0;
                            }

                            break;
                        case "flipmodey":
                            if (((System.Xml.XmlAttribute)xn).Value.ToString() != "##")
                            {
                                _flipModeY = Convert.ToInt32(((System.Xml.XmlAttribute)xn).Value.ToString());
                            }
                            else
                            {
                                _flipModeY = 0;
                            }
                            break;
                        case "_centerx":
                            if (((System.Xml.XmlAttribute)xn).Value.ToString() != "##")
                            {
                                _centerX = ((System.Xml.XmlAttribute)xn).Value.ToString();
                            }
                            else
                            {
                                _centerX = "";
                            }
                            break;
                        case "_centery":
                            if (((System.Xml.XmlAttribute)xn).Value.ToString() != "##")
                            {
                                _centerY = ((System.Xml.XmlAttribute)xn).Value.ToString();
                            }
                            else
                            {
                                _centerY = "";
                            }
                            break;
                        case "colourvalue":
                            if (((System.Xml.XmlAttribute)xn).Value.ToString() != "##")
                            {

                                switch (((System.Xml.XmlAttribute)xn).Value.ToString())
                                {
                                    case "red":
                                        _colorfiltereff.FilterColor = Colors.Red;
                                        Grdcolorfilter.Effect = _colorfiltereff;

                                        break;
                                    case "blue":
                                        _colorfiltereff.FilterColor = Colors.Blue;
                                        Grdcolorfilter.Effect = _colorfiltereff;

                                        break;
                                    case "green":
                                        _colorfiltereff.FilterColor = Colors.Green;
                                        Grdcolorfilter.Effect = _colorfiltereff;

                                        break;

                                    case "magenta":
                                        _colorfiltereff.FilterColor = Colors.Magenta;
                                        Grdcolorfilter.Effect = _colorfiltereff;

                                        break;
                                    case "lime":
                                        color = (System.Windows.Media.Color)System.Windows.Media.ColorConverter.ConvertFromString("#FFFFFF00");
                                        _colorfiltereff.FilterColor = color;
                                        Grdcolorfilter.Effect = _colorfiltereff;

                                        break;
                                    case "orange":
                                        color = (System.Windows.Media.Color)System.Windows.Media.ColorConverter.ConvertFromString("#FFFFA81D");
                                        _colorfiltereff.FilterColor = color;
                                        Grdcolorfilter.Effect = _colorfiltereff;
                                        break;

                                    case "yellow":
                                        color = (System.Windows.Media.Color)System.Windows.Media.ColorConverter.ConvertFromString("#FFFFFF96");
                                        _colorfiltereff.FilterColor = color;
                                        Grdcolorfilter.Effect = _colorfiltereff;
                                        break;
                                    case "skyblue":
                                        color = (System.Windows.Media.Color)System.Windows.Media.ColorConverter.ConvertFromString("#FF7CF5F5");
                                        _colorfiltereff.FilterColor = color;
                                        Grdcolorfilter.Effect = _colorfiltereff;
                                        break;

                                    case "lightred":
                                        color = (System.Windows.Media.Color)System.Windows.Media.ColorConverter.ConvertFromString("#FFFFCACB");
                                        _colorfiltereff.FilterColor = color;
                                        Grdcolorfilter.Effect = _colorfiltereff;

                                        break;
                                }
                            }
                            else
                            {
                                Grdcolorfilter.Effect = null;
                            }
                            break;

                    }
                }

                bool coloreffect = false;
                
                foreach (var xn in (Xdocument.ChildNodes[0]).FirstChild.Attributes)
                {
                    switch (((System.Xml.XmlAttribute)xn).Name.ToLower())
                    {
                        case "sharpen":
                            if (((System.Xml.XmlAttribute)xn).Value.ToString() != "##")
                            {
                                _sharpen = Convert.ToDouble(((System.Xml.XmlAttribute)xn).Value.ToString());
                                _sharpeff.Strength = _sharpen;
                                _sharpeff.PixelWidth = 0.0015;
                                _sharpeff.PixelHeight = 0.0015;
                                _currentsharpen = _sharpen;
                                GrdSharpen.Effect = _sharpeff;
                                coloreffect = true;
                            }
                            else
                            {
                                // _sharpeff.Amount = .2;
                                //  GrdSharpen.Effect = _sharpeff;
                                GrdSharpen.Effect = null;
                            }
                            break;

                        case "greyscale":
                            if (((System.Xml.XmlAttribute)xn).Value.ToString() != "0")
                            {
                                GreyScaleEffect _greyscaleeff = new GreyScaleEffect();
                                _greyscaleeff.Desaturation = Convert.ToDouble(((System.Xml.XmlAttribute)xn).Value.ToString());
                                _greyscaleeff.Toned = 0;
                                GrdGreyScale.Effect = _greyscaleeff;

                                coloreffect = true;
                            }
                            else
                            {
                                GrdGreyScale.Effect = null;
                            }
                            break;
                        case "digimagic":
                            if (((System.Xml.XmlAttribute)xn).Value.ToString() != "0")
                            {
                                // _digimagic = "1";
                                _brighteff.Brightness = -0.05;
                                _brighteff.Contrast = 1.3;
                                GrdBrightness.Effect = _brighteff;
                                _bright = -0.05;
                                _cont = 1.3;

                                _sharpen = .050;
                                _sharpeff.Strength = _sharpen;
                                _sharpeff.PixelWidth = 0.0015;
                                _sharpeff.PixelHeight = 0.0015;
                                GrdSharpen.Effect = _sharpeff;
                                //_Sharepen = sharpen.ToString();
                                checkDigimagic = true;
                            }
                            else
                            {
                                if (!checkluminiosity)
                                {
                                    GrdBrightness.Effect = null;
                                    //_digimagic = "0";
                                }
                            }
                            break;
                        case "sepia":
                            if (((System.Xml.XmlAttribute)xn).Value.ToString() != "0")
                            {
                                _colorfiltereff.FilterColor = (System.Windows.Media.Color)System.Windows.Media.ColorConverter.ConvertFromString("#FFE6B34D");
                                GrdSepia.Effect = _colorfiltereff;

                                coloreffect = true;
                            }
                            else
                            {
                                GrdSepia.Effect = null;
                            }
                            break;

                        case "defog":
                            if (((System.Xml.XmlAttribute)xn).Value.ToString() != "0")
                            {
                                //_defoger = "1";
                                _brighteff.Brightness = -0.10;
                                _brighteff.Contrast = 1.09;
                                GrdBrightness.Effect = _brighteff;

                                _bright = -0.10;
                                _cont = 1.09;
                                //VisualStateManager.GoToState(btnDefogger, "Checked", true);
                                //VisualStateManager.GoToState(btnColorEffectsfilters, "Checked", true);
                                coloreffect = true;
                            }
                            else
                            {
                                if (!checkluminiosity && !checkDigimagic)
                                {
                                    GrdBrightness.Effect = null;
                                    //_defoger = "0";
                                }
                            }
                            break;
                        case "underwater":
                            if (((System.Xml.XmlAttribute)xn).Value.ToString() != "0")
                            {
                                _under.FogColor = (System.Windows.Media.Color)System.Windows.Media.ColorConverter.ConvertFromString("#FF4F9CEF");
                                _under.Defog = .40;
                                _under.Contrastr = .67;
                                _under.Contrastg = 1;
                                _under.Contrastb = 1;
                                _under.Exposure = .77;
                                _under.Gamma = .91;
                                GrdUnderWater.Effect = _under;
                                coloreffect = true;
                            }
                            else
                            {
                                GrdUnderWater.Effect = null;
                            }
                            break;


                        case "emboss":
                            if (((System.Xml.XmlAttribute)xn).Value.ToString() != "0")
                            {
                                EmbossedEffect emboseff = new EmbossedEffect();
                                emboseff.Amount = 0.7;
                                emboseff.Width = 0.002;
                                GrdEmboss.Effect = emboseff;

                                coloreffect = true;
                            }
                            else
                            {
                                GrdEmboss.Effect = null;
                            }
                            break;

                        case "invert":
                            if (((System.Xml.XmlAttribute)xn).Value.ToString() != "0")
                            {
                                InvertColorEffect _inverteff = new InvertColorEffect();
                                GrdInvert.Effect = _inverteff;

                                coloreffect = true;
                            }
                            else
                            {
                                GrdInvert.Effect = null;
                            }
                            break;

                        case "granite":
                            if (((System.Xml.XmlAttribute)xn).Value.ToString() != "0")
                            {
                                SketchGraniteEffect sketcheff = new SketchGraniteEffect();
                                sketcheff.BrushSize = .005;
                                GrdSketchGranite.Effect = sketcheff;

                                coloreffect = true;
                            }
                            else
                            {
                                GrdSketchGranite.Effect = null;
                            }
                            break;

                        case "hue":
                            if (((System.Xml.XmlAttribute)xn).Value.ToString() != "##")
                            {

                                _hueshift = Convert.ToDouble(((System.Xml.XmlAttribute)xn).Value.ToString());
                                _shifthueeff.HueShift = _hueshift;
                                GrdHueShift.Effect = _shifthueeff;
                                _currenthueshift = _hueshift;

                                coloreffect = true;
                            }
                            else
                            {
                                GrdHueShift.Effect = null;
                            }
                            break;
                        case "cartoon":
                            if (((System.Xml.XmlAttribute)xn).Value.ToString() != "0")
                            {
                                Cartoonize carteff = new Cartoonize();
                                carteff.Width = 150;
                                carteff.Height = 150;
                                Grdcartoonize.Effect = carteff;

                                coloreffect = true;
                            }
                            else
                            {
                                Grdcartoonize.Effect = null;
                            }
                            break;

                    }
                }

                _zoomTransform = new ScaleTransform();
                if (_flipMode != 0 || _flipModeY != 0)
                {
                    FlipLoad();
                }

            }
            catch
                (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }


        }
        void objCurrent_Loaded(object sender, RoutedEventArgs e)
        {
            Canvas.SetLeft(frm, (dragCanvas.ActualWidth - frm.ActualWidth) / 2); Canvas.SetTop(frm, (dragCanvas.ActualHeight - frm.ActualHeight) / 2);
        }

        private void LoadFromDB()
        {
            PhotoInfo objPhotoDetails = null;
            try
            {
                graphicsTextBoxCount = 0;
                graphicsBorderApplied = false;
                graphicsCount = 0;
                graphicsframeApplied = false;


                //DigiPhotoDataServices digiPhotoDataService = new DigiPhoto.DataLayer.DigiPhotoDataServices();
                //PhotoId = digiPhotoDataService.GetPhotoIDByRFFID(PhotoName);
                //DG_Photos _objPhotoDetails = digiPhotoDataService.GetPhotoDetailsbyPhotoId(PhotoId);

                objPhotoDetails = (new PhotoBusiness()).GetPhotoDetailsbyPhotoId(photoId);

                if (objPhotoDetails.DG_Photos_Layering != null)
                {
                    GrdGreenScreenDefault3.RenderTransform = null;
                    Canvas.SetLeft(GrdBrightness, 0);
                    Canvas.SetTop(GrdBrightness, 0);
                    GrdBrightness.RenderTransform = null;
                    grdZoomCanvas.RenderTransform = null;

                    LoadXaml(objPhotoDetails.DG_Photos_Layering);
                    //if (dragCanvas.Children.Count > 2 || (_ZoomFactor >= .5 && _ZoomFactor != 1) || frm.Children.Count >= 1 || canbackground.Children.Count > 1)
                    //{
                    //    DisableButtonForLayering();   
                    //}

                }
                else
                {
                    forWdht.Width = widthimg.Source.Width;
                    forWdht.Height = widthimg.Source.Height;
                    GrdGreenScreenDefault3.RenderTransform = null;
                    Canvas.SetLeft(GrdBrightness, 0);
                    Canvas.SetTop(GrdBrightness, 0);
                    GrdBrightness.RenderTransform = null;
                    grdZoomCanvas.RenderTransform = null;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
            finally
            {
                objPhotoDetails = null;
            }
        }
        private void Zomout(bool orignal)
        {
            try
            {
                grdZoomCanvas.UpdateLayout();
                double currentwidth = widthimg.Source.Width;
                double currentheight = widthimg.Source.Height;

                double ratiowidth = currentwidth / 600;
                double ratioheight = currentheight / 600;
                ratiowidth = 100 / ratiowidth / 100;
                ratioheight = 100 / ratioheight / 100;

                if (frm.Children.Count == 1)
                {
                    currentwidth = forWdht.Width;
                    currentheight = forWdht.Height;
                    ratiowidth = currentwidth / 600;
                    ratioheight = currentheight / 600;
                    ratiowidth = 100 / ratiowidth / 100;
                    ratioheight = 100 / ratioheight / 100;
                }

                var zoomTransform = new ScaleTransform(); ;
                var transformGroup = new TransformGroup();
                if (currentheight > currentwidth)
                {
                    zoomTransform.ScaleX = ratioheight - .01;
                    zoomTransform.ScaleY = ratioheight - .01;
                }

                if (currentheight < currentwidth)
                {
                    zoomTransform.ScaleX = ratiowidth - .01;
                    zoomTransform.ScaleY = ratiowidth - .01;
                }
                zoomTransform.CenterX = forWdht.ActualWidth / 2;
                zoomTransform.CenterY = forWdht.ActualHeight / 2;
                transformGroup.Children.Add(zoomTransform);
                if (orignal)
                {
                    MyInkCanvas.RenderTransform = null;

                    grdZoomCanvas.LayoutTransform = transformGroup;
                }
                else
                {

                    GrdGreenScreenDefault3.RenderTransform = null;
                    MyInkCanvas.RenderTransform = transformGroup;

                    MyInkCanvas.DefaultDrawingAttributes.StylusTipTransform = new Matrix(zoomTransform.ScaleX, 0, 0, zoomTransform.ScaleY, 0, 0);

                }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
            finally
            {
                DigiPhoto.MemoryManagement.FlushMemory();
            }
        }
        private void FlipLoad()
        {

            try
            {
                //mainImage.RenderTransformOrigin = new System.Windows.Point(0.5, 0.5);

                if (_flipModeY == 0)
                {
                    if (_flipMode == 1)
                    {
                        _zoomTransform.ScaleX = 1;
                    }
                    else
                    {
                        _zoomTransform.ScaleX = 1;
                    }
                }

                _zoomTransform.CenterX = Convert.ToDouble(_centerX);
                _zoomTransform.CenterY = Convert.ToDouble(_centerY);

                _transformGroup = new TransformGroup();
                _transformGroup.Children.Add(_zoomTransform);
                _transformGroup.Children.Add(_translateTransform);
                _transformGroup.Children.Add(_rotateTransform);
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }

        }
        private void Rotate(int angle)
        {

            GrdBrightness.RenderTransformOrigin = new System.Windows.Point(0.5, 0.5);
            var roatateTrans = new RotateTransform();
            int previousvalue = rotateangle;
            if (angle != -1)
            {
                roatateTrans.Angle = angle;
                GrdBrightness.RenderTransform = roatateTrans;
                rotateangle = 360 - angle;
                if (rotateangle == 360)
                    rotateangle = 0;
            }
            else
            {
                try
                {
                    switch (rotateangle)
                    {
                        case 0:
                            {
                                roatateTrans.Angle = 90;
                                rotateangle = 90;


                                //CurrentImage.RotateFlip(RotateFlipType.Rotate90FlipNone);
                                //FlipModeY = 1;
                                break;
                            }
                        case 90:
                            {
                                roatateTrans.Angle = 180;
                                rotateangle = 180;


                                //CurrentImage.RotateFlip(RotateFlipType.Rotate180FlipNone);
                                //FlipModeY = 0;
                                break;
                            }
                        case 180:
                            {
                                roatateTrans.Angle = 270;
                                rotateangle = 270;
                                //CurrentImage.RotateFlip(RotateFlipType.Rotate270FlipNone);
                                //FlipModeY = 1;
                                break;
                            }
                        case 270:
                            {
                                roatateTrans.Angle = 360;
                                rotateangle = 0;
                                //FlipModeY = 0;
                                break;
                            }

                    }
                    GrdBrightness.RenderTransform = roatateTrans;

                    if (roatateTrans.Angle / 90 == 1 || roatateTrans.Angle / 90 == 3)
                    {
                        GrdBrightness.Margin = new Thickness(-(forWdht.Width - forWdht.Height) / 2, (forWdht.Width - forWdht.Height) / 2, 0, 0);

                        double w = forWdht.ActualWidth;
                        double h = forWdht.ActualHeight;

                        forWdht.Width = h;
                        forWdht.Height = w;

                        forWdht.RenderSize = new Size(h, w);

                        dragCanvas.RenderSize = new Size(h, w);
                        canbackground.RenderSize = new Size(h, w);


                        forWdht.InvalidateArrange();
                        forWdht.InvalidateMeasure();
                        forWdht.InvalidateVisual();
                    }
                    else
                    {
                        GrdBrightness.Margin = new Thickness(0, 0, 0, 0);

                        double w = forWdht.ActualWidth;
                        double h = forWdht.ActualHeight;

                        forWdht.Width = h;
                        forWdht.Height = w;

                        forWdht.RenderSize = new Size(h, w);

                        dragCanvas.RenderSize = new Size(h, w);
                        canbackground.RenderSize = new Size(h, w);


                        forWdht.InvalidateArrange();
                        forWdht.InvalidateMeasure();
                        forWdht.InvalidateVisual();
                    }

                    //dragCanvas.InvalidateArrange();
                    //dragCanvas.InvalidateMeasure();
                    //dragCanvas.InvalidateVisual();


                    //canbackground.InvalidateArrange();
                    //canbackground.InvalidateMeasure();
                    //canbackground.InvalidateVisual();

                    //mainImage.LayoutTransform = RoatateTrans;
                    //forWdht.LayoutTransform = RoatateTrans;
                }
                catch (Exception ex)
                {
                    string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                    ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                }
                finally
                {


                }
            }
        }
        //private void ReloadAllGraphicsEffect()
        //{
        //    try
        //    {
        //        canbackground.Background = null;
        //        List<UIElement> itemstoremove = new List<UIElement>();
        //        foreach (UIElement ui in dragCanvas.Children)
        //        {
        //            if (ui is Grid)
        //            {
        //            }
        //            else
        //            {
        //                itemstoremove.Add(ui);
        //            }
        //        }
        //        foreach (UIElement ui in itemstoremove)
        //        {
        //            dragCanvas.Children.Remove(ui);
        //        }

        //        itemstoremove = new List<UIElement>();
        //        foreach (UIElement ui in frm.Children)
        //        {
        //            if (ui is OpaqueClickableImage)
        //            {
        //                itemstoremove.Add(ui);
        //            }
        //        }
        //        foreach (UIElement ui in itemstoremove)
        //        {
        //            frm.Children.Remove(ui);
        //        }

        //        if (FlipMode != 0 || FlipModeY != 0)
        //        {
        //            _ZoomFactor = 1;

        //            zoomTransform.CenterX = Convert.ToDouble(_centerX);
        //            zoomTransform.CenterY = Convert.ToDouble(_centerY);

        //            zoomTransform.ScaleX = 1;
        //            zoomTransform.ScaleY = 1;

        //            translateTransform = new TranslateTransform();
        //            rotateTransform = new RotateTransform();
        //            transformGroup = new TransformGroup();

        //            GrdBrightness.RenderTransform = null;

        //            Canvas.SetLeft(GrdBrightness, 0);
        //            Canvas.SetTop(GrdBrightness, 0);
        //            FlipLoad();
        //        }
        //        else
        //        {
        //            _ZoomFactor = 1;

        //            zoomTransform.CenterX = mainImage.ActualWidth / 2;
        //            zoomTransform.CenterY = mainImage.ActualHeight / 2;

        //            zoomTransform.ScaleX = _ZoomFactor;
        //            zoomTransform.ScaleY = _ZoomFactor;


        //            transformGroup = new TransformGroup();
        //            rotateTransform = new RotateTransform();
        //            zoomTransform = new ScaleTransform();


        //            //GrdBrightness.RenderTransform = transformGroup;
        //            GrdBrightness.RenderTransform = null;

        //            Canvas.SetLeft(GrdBrightness, 0);
        //            Canvas.SetTop(GrdBrightness, 0);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
        //        ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
        //    }
        //}
        public RenderTargetBitmap jCaptureScreen()
        {
            this.InvalidateVisual();
            var bi = mainImage.Source as BitmapImage;
            //Changing the code for the email/CD/USB all the images will be rendered at 96 dpi
            double dpiX =0;
            double dpiY=0;

            //DigiPhotoDataServices _obj = new DigiPhotoDataServices();
            bool isCompressed = (new ConfigBusiness()).GetConfigCompression(LoginUser.SubStoreId).ToBoolean();
            //if ((bool)_obj.GetConfigCompression(LoginUser.SubStoreId))
            if (isCompressed)
            {
                dpiX = 96;
                dpiY = 96;
            }
            else
            {
                 dpiX = bi.DpiX;
                 dpiY = bi.DpiY;
            }

          
            RenderTargetBitmap renderBitmap = null;
            RenderOptions.SetBitmapScalingMode(forWdht, BitmapScalingMode.HighQuality);
            RenderOptions.SetEdgeMode(forWdht, EdgeMode.Aliased);
            try
            {
                if (_zoomFactor > 1)
                {
                    //Size size = new Size(forWdht.ActualWidth, forWdht.ActualHeight);
                    Size size = new Size(forWdht.Width, forWdht.Height);
                    renderBitmap = new RenderTargetBitmap((int)(size.Width * dpiX / 96.0 * (1 / _zoomFactor)), (int)(size.Height * dpiY / 96.0 * (1 / _zoomFactor)),
                        dpiX, dpiY, PixelFormats.Default);// PixelFormats.Pbgra32);
                    RenderOptions.SetEdgeMode(forWdht, EdgeMode.Aliased);
                    //RenderOptions.SetBitmapScalingMode(forWdht, BitmapScalingMode.HighQuality);
                    forWdht.SnapsToDevicePixels = true;
                    forWdht.RenderTransform = new ScaleTransform(1 / _zoomFactor, 1 / _zoomFactor, 0.5, 0.5);
                    forWdht.Measure(size);
                    forWdht.Arrange(new Rect(size));
                    renderBitmap.Render(forWdht);
                    if (renderBitmap.CanFreeze)
                    {
                        renderBitmap.Freeze();
                    }
                    forWdht.RenderTransform = null;
                }
                else
                {

                    //Size size = new Size(forWdht.ActualWidth, forWdht.ActualHeight);
                    Size size = new Size(forWdht.Width, forWdht.Height);
                    renderBitmap = new RenderTargetBitmap((int)(size.Width * dpiY / 96.0),
                        (int)(size.Height * dpiY / 96.0), dpiX, dpiY, PixelFormats.Default);// PixelFormats.Pbgra32);
                    RenderOptions.SetEdgeMode(forWdht, EdgeMode.Aliased);
                    //RenderOptions.SetBitmapScalingMode(forWdht, BitmapScalingMode.HighQuality);
                    forWdht.SnapsToDevicePixels = true;
                    forWdht.RenderTransform = new ScaleTransform(1, 1, 0.5, 0.5);
                    forWdht.Measure(size);
                    forWdht.Arrange(new Rect(size));
                    renderBitmap.Render(forWdht);
                    if (renderBitmap.CanFreeze)
                    {
                        renderBitmap.Freeze();
                    }
                    forWdht.RenderTransform = null;
                }
                return renderBitmap;
            }
            catch (Exception ex)
            {
                Rect bounds = VisualTreeHelper.GetDescendantBounds(GrdBrightness);
                DrawingVisual dv = new DrawingVisual();
                renderBitmap = new RenderTargetBitmap((int)(bounds.Width * dpiX / 96.0),
                                                              (int)(bounds.Height * dpiY / 96.0),
                                                              dpiX,
                                                              dpiY,
                                                              PixelFormats.Default);


                using (DrawingContext ctx = dv.RenderOpen())
                {
                    VisualBrush vb = new VisualBrush(forWdht);
                    ctx.DrawRectangle(vb, null, new Rect(new System.Windows.Point(), bounds.Size));
                }

                renderBitmap.Render(dv);
                return renderBitmap;
            }
        }


        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            var desktopWorkingArea = System.Windows.SystemParameters.WorkArea;
            this.Left = desktopWorkingArea.Right - this.Width;
            this.Top = desktopWorkingArea.Bottom - this.Height;
            this.Topmost = false;
        }
    }
}
