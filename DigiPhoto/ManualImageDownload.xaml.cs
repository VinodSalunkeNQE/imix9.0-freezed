﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Diagnostics;
using DigiPhoto.Common;
using DigiPhoto.DataLayer;
using DigiPhoto.DataLayer.Model;
using System.IO;
using System.Drawing;
using System.Drawing.Imaging;
using DigiAuditLogger;
using System.Windows.Threading;
using ExifLib;
using System.Threading;
using System.ComponentModel;



namespace DigiPhoto
{
    /// <summary>
    /// Interaction logic for ManualImageDownload.xaml
    /// </summary>
    public partial class ManualImageDownload : Window
    {
        #region Declaration

        static int count = 0;
        static int ProcessedCount = 0;
        DigiPhotoDataServices _objDataLayer;
        ReadImageMetaData _objmetdata;
        private Dictionary<string, Int32> _locationList;
        private List<string> ImageName;
        

        public List<string> imagelist
        {
            get { return ImageName; }
            set { ImageName = value; }
        }
        public Dictionary<string, Int32> LocationList
        {
            get { return _locationList; }
            set { _locationList = value; }
        }
        private Dictionary<string, Int32> _photoGrapherList;
        public Dictionary<string, Int32> PhotoGrapherList
        {
            get { return _photoGrapherList; }
            set { _photoGrapherList = value; }
        }

        public string ImageMetaData
        {
            get;
            set;
        }

        BackgroundWorker worker = new BackgroundWorker();
        string filepath;
        #endregion
        public ManualImageDownload(List<string> img)
        {
            try
            {
                InitializeComponent();
                count = 0;
                this.ImageName = img;
                _objDataLayer = new DigiPhotoDataServices();
                txbUserName.Text = LoginUser.UserName;
                txbStoreName.Text = LoginUser.StoreName;
                btnDownload.Content = "Download";
                List<vw_GetUserDetails> lstUsers = new List<vw_GetUserDetails>();
                lstUsers = _objDataLayer.GetPhotoGraphersList(LoginUser.StoreId);
                List<DG_Location> lstlocations = new List<DG_Location>();
                lstlocations = _objDataLayer.GetLocationList(LoginUser.StoreId);
                LocationList = new Dictionary<string, int>();
                LocationList.Add("--Select--", 0);
                PhotoGrapherList = new Dictionary<string, int>();
                PhotoGrapherList.Add("--Select--", 0);
                if (lstUsers != null)
                {
                    foreach (var item in lstUsers)
                    {

                        PhotoGrapherList.Add(item.DG_User_First_Name + " " + item.DG_User_Last_Name + "(" + item.DG_User_Name + ")", item.DG_User_pkey);
                    }
                    CmbPhotographerNo.ItemsSource = PhotoGrapherList;
                    CmbPhotographerNo.SelectedValue = "0";
                }
                if (lstlocations != null)
                {
                    foreach (var item in lstlocations)
                    {
                        LocationList.Add(item.DG_Location_Name, item.DG_Location_pkey);
                    }
                    CmbLocation.ItemsSource = LocationList;
                    CmbLocation.SelectedValue = "0";
                }
                prgbar.Visibility = Visibility.Hidden;
                vw_GetConfigdata config = _objDataLayer.GetConfigurationData(LoginUser.SubStoreId);
                filepath = config.DG_Hot_Folder_Path;
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }

        }

        public BitmapImage GetBitmapImageFromPath(string value)
        {
            // BitmapMetadata bb = new BitmapMetadata(".jpg");

            BitmapImage bi = new BitmapImage();
            if (value != null)
            {
                using (FileStream fileStream = File.OpenRead(value.ToString()))
                {
                    MemoryStream ms = new MemoryStream();
                    fileStream.CopyTo(ms);
                    ms.Seek(0, SeekOrigin.Begin);
                    fileStream.Close();
                    bi.BeginInit();
                    bi.CacheOption = BitmapCacheOption.OnLoad;
                    bi.StreamSource = ms;
                    bi.EndInit();
                    bi.Freeze();
                }
            }
            else
            {
                bi = new BitmapImage();
            }
            return bi;
        }
        private Bitmap BitmapImage2Bitmap(BitmapImage bitmapImage)
        {
            // BitmapImage bitmapImage = new BitmapImage(new Uri("../Images/test.png", UriKind.Relative));

            using (MemoryStream outStream = new MemoryStream())
            {
                BitmapEncoder enc = new BmpBitmapEncoder();
                enc.Frames.Add(BitmapFrame.Create(bitmapImage));
                enc.Save(outStream);
                System.Drawing.Bitmap bitmap = new System.Drawing.Bitmap(outStream);

                // return bitmap; <-- leads to problems, stream is closed/closing ...
                return new Bitmap(bitmap);
            }
        }
        private void btnDownload_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (IsValidData())
                {
                    Int32 PhotoNo = Convert.ToInt32(tbStartingNo.Text);
                    string PhotographerId = CmbPhotographerNo.SelectedValue.ToString();
                    int locationid=Convert.ToInt32(CmbLocation.SelectedValue);
                    Download(PhotoNo, PhotographerId,locationid);

                }
                else
                {
                    MessageBox.Show("Please enter valid data");
                }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }

        }

        private void Download(int PhotoNo, string photographerId,int locationid)
        {
            try
            {
                String s = String.Empty;
                ImageName = new List<string>();
                
                    foreach (var item in ImageName)
                    {
                        try
                        {

                            string name = System.IO.Path.GetFileName(item); 
                           
                            if (name != "Thumbs.db")
                            {
                               
                                string picname = name;
                                s += ", " + picname;
                                string dpivalue = string.Empty;
                               FileInfo infofile = new FileInfo(filepath + "\\Manual_Download\\" + picname);
                                try
                                {

                                    string rawmeta = _objmetdata.GetImageMetaData(item);
                                    string[] meta = rawmeta.Split('@');
                                    string dimensionvalue = "'" + meta[0] + "'";
                                    string sizevalue = "'" + meta[1] + "'";
                                    string titlevalue = "'" + meta[3] + "'";
                                    string subjectvalue = "'" + meta[4] + "'";
                                    string commentvalue = "'" + meta[5] + "'";
                                    string datetakenvalue = "'" + meta[6] + "'";
                                    string cameravalue = "'" + meta[7] + "'";
                                    string copyrightvalue = "'" + meta[8] + "'";
                                    string keywordvalue = "'" + meta[9] + "'";
                                    dpivalue = "'" + meta[2] + "'";
                                    ImageMetaData = "<image Dimensions=" + dimensionvalue + " Size=" + sizevalue + " DPI=" + dpivalue + " Title=" + titlevalue + " Subject=" + subjectvalue + " Comment=" + commentvalue + " DateTaken=" + datetakenvalue + " Camera=" + cameravalue + " Copyright=" + copyrightvalue + " Keywords=" + keywordvalue + "></image>";
                                }
                                catch (Exception ex)
                                {
                                    string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                                    ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                                }
                                PhotoNo = _objDataLayer.GetLatestNumberForWifi("Manual");
                                string filename = _objDataLayer.ServerDateTime().Day.ToString() + _objDataLayer.ServerDateTime().Month.ToString() + PhotoNo.ToString() + ".jpg";
                                if(_objDataLayer.CheckPhotos(filename,1))
                                {
                            
                                  
                                  System.Threading.Thread.Sleep(150);
                                  _objDataLayer.SetPhotoDetails(PhotoNo.ToString(), filename, _objDataLayer.ServerDateTime(), photographerId, ImageMetaData, locationid, "test");
                                 string rfid = PhotoNo.ToString();
                                 _objDataLayer.SetLatestNumberForWifi(PhotoNo, "Manual");

                                }
                                else
                                {
                                    MessageBox.Show(PhotoNo + " already exists for today");
                                    return;
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                            ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                        }
                    }
            }
            catch (Exception ex)
            {

                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);

            }
            finally
            {
               // ImageDownloader _objmdlnd = new ImageDownloader(imagelist);
              //  _objmdlnd.Show();
               // this.Close();
            }

        }
        private bool IsValidData()
        {
            bool retvalue = true;

            if (CmbPhotographerNo.SelectedValue.ToString() == "0")
            {
                retvalue = false;
            }
            else if (CmbLocation.SelectedValue.ToString() == "0")
            {
                retvalue = false;
            }
            try
            {
                Convert.ToInt64(tbStartingNo.Text);
            }
            catch (Exception ex)
            {
                retvalue = false;
            }
            if (tbStartingNo.Text.Trim() == "")
            {
                retvalue = false;
            }

            return retvalue;
        }
        private void CmbLocation_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {

            }
            catch (Exception ex)
            {

            }
        }

        private void btnLogout_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                AuditLog.AddUserLog(Common.LoginUser.UserId, 39, "Logged out at " + (new DigiPhotoDataServices()).ServerDateTime().ToString());
                Login _objLogin = new Login();
                _objLogin.Show();
                this.Close();
            }
            catch (Exception ex)
            {

                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
            finally
            {
                MemoryManagement.FlushMemory();
            }
        }
        private void btnBack_Click(object sender, RoutedEventArgs e)
        {
            Home _objhome = new Home();
            _objhome.Show();
            this.Close();
        }
        private void Window_Unloaded(object sender, RoutedEventArgs e)
        {
            try
            {
                try
                {
                    foreach (var item in (new DirectoryInfo(filepath + "\\Manual_Download\\")).GetFiles())
                    {
                        item.Delete();
                    }
                }
                catch (Exception ex)
                {

                }
                var drives = from drive in DriveInfo.GetDrives()
                             where drive.DriveType == DriveType.Removable
                             select drive;
                foreach (var drivesitem in drives)
                {
                    foreach (var fileitem in Directory.GetFiles(drivesitem.Name + "\\", "*.jpg", SearchOption.AllDirectories).ToList())
                    {
                        FileInfo _objnew = new FileInfo(fileitem.ToString());
                        _objnew.Delete();
                    }
                }
            }
            catch (Exception ex)
            {

            }
        }
        private static RotateFlipType OrientationToFlipType(string orientation)
        {
            switch (int.Parse(orientation))
            {
                case 1:
                    return RotateFlipType.RotateNoneFlipNone;
                    break;
                case 2:
                    return RotateFlipType.RotateNoneFlipX;
                    break;
                case 3:
                    return RotateFlipType.Rotate180FlipNone;
                    break;
                case 4:
                    return RotateFlipType.Rotate180FlipX;
                    break;
                case 5:
                    return RotateFlipType.Rotate90FlipX;
                    break;
                case 6:
                    return RotateFlipType.Rotate90FlipNone;
                    break;
                case 7:
                    return RotateFlipType.Rotate270FlipX;
                    break;
                case 8:
                    return RotateFlipType.Rotate270FlipNone;
                    break;
                default:
                    return RotateFlipType.RotateNoneFlipNone;
            }
        }
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {


        }

        private void CmbPhotographerNo_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

            if (Convert.ToInt32(CmbPhotographerNo.SelectedValue) != 0)
            {
                Lastimageno.Visibility = Visibility.Visible;
                try
                {
                    string LastImageId = _objDataLayer.GetPhotoGrapherLastImageId(Convert.ToInt32(CmbPhotographerNo.SelectedValue));
                    Lastimageno.Text = "Last Photo no. of " + (((System.Collections.Generic.KeyValuePair<string, int>)(CmbPhotographerNo.SelectedItem)).Key) + " is " + LastImageId;
                }
                catch (Exception ex)
                {

                    string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                    ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);

                }
            }
            else
            {
                Lastimageno.Visibility = Visibility.Collapsed;
            }
            

        }
    }
}
