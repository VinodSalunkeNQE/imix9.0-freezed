﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ServiceModel;
//using log4net;
using System.Configuration;
using DigiPhoto.DataLayer;
using DigiPhoto.Common;
using DigiPhoto.IMIX.Business;


namespace DigiPhoto.DataSync.Controller
{
    public class ServiceProxy<T>
    {
        //public static ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        public static void Use(Action<T> action)
        {
            
            if (string.IsNullOrEmpty(App.DataSyncServiceURl))
            {
                //DigiPhotoDataServices _objDataServices = new DigiPhotoDataServices();
                //App.DataSyncServiceURl = _objDataServices.GetOnlineConfigData(ConfigParams.DgServiceURL, LoginUser.SubStoreId);
                App.DataSyncServiceURl = (new ConfigBusiness()).GetOnlineConfigData(ConfigParams.DgServiceURL, LoginUser.SubStoreId);
                if (App.DataSyncServiceURl != null)
                    App.DataSyncServiceURl = App.DataSyncServiceURl.Trim();
                ErrorHandler.ErrorHandler.LogError("Service Proxy Line Number : 28" + App.DataSyncServiceURl);
            }
            string apiLocation = App.DataSyncServiceURl;
            ErrorHandler.ErrorHandler.LogError("Service Proxy Line Number : 30"+apiLocation);
            dynamic binding;
            if(apiLocation.ToLower().Contains("net.tcp"))
            {
                binding = new NetTcpBinding();
                binding.TransferMode = TransferMode.StreamedResponse;
                binding.Security.Mode = SecurityMode.None;
            }
            else
            {
                binding = new BasicHttpBinding();
            }
            ErrorHandler.ErrorHandler.LogError("Service Proxy Line Number : 42");
            //ConfigurationManager.AppSettings["API_URL"].ToString();

            if (typeof(T).Name.StartsWith("I"))
            {
                apiLocation = apiLocation + "/" + typeof(T).Name.Substring(1) + ".svc";
            }
            else
            {
                apiLocation = apiLocation + "/" + typeof(T).Name + ".svc";
            }
            ErrorHandler.ErrorHandler.LogError("Service Proxy Line Number : 53"+ apiLocation);
            EndpointAddress ep = new EndpointAddress(apiLocation);
            string dataSize = string.Empty;
            if (ConfigurationManager.AppSettings["ServiceDataSize"] == null)
            {
                dataSize = Convert.ToString(1024 * 65535);
            }
            else
            {
                dataSize = ConfigurationManager.AppSettings["ServiceDataSize"];
            }
            binding.MaxReceivedMessageSize = Convert.ToInt64(dataSize);
            binding.ReaderQuotas.MaxArrayLength = Convert.ToInt32(dataSize);
            binding.ReaderQuotas.MaxStringContentLength = Convert.ToInt32(dataSize);
            binding.MaxBufferSize = Convert.ToInt32(dataSize);
            binding.OpenTimeout = new TimeSpan(0, 0, 10);
            binding.CloseTimeout = new TimeSpan(0, 0, 10);
            binding.SendTimeout = new TimeSpan(0, 0, 90);   //most important
            binding.ReceiveTimeout = new TimeSpan(0, 0, 10);
            ErrorHandler.ErrorHandler.LogError("Service Proxy Line Number : 72");
            ChannelFactory<T> factory = new ChannelFactory<T>(binding, ep);
            T client = factory.CreateChannel();

            bool sucess = false;
            ErrorHandler.ErrorHandler.LogError("Service Proxy Line Number : 77");
            try
            {
                action(client);
                ((IClientChannel)client).Close();
                factory.Close();
                sucess = true;
            }
            catch (CommunicationException cex)
            {
                ErrorHandler.ErrorHandler.LogError("Service Proxy Line Number : 87"+cex.Message);
                //log.Error(cex);
                throw cex;
            }
            catch (TimeoutException tex)
            {
                ErrorHandler.ErrorHandler.LogError("Service Proxy Line Number : 93" + tex.Message);
                //log.Error(tex);
                throw tex;
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError("Service Proxy Line Number : 98" + ex.Message);
                //log.Error(ex);
                throw ex;
            }
            finally
            {
                if (!sucess)
                {
                    //Abort the Channel if it didn't close sucessfully
                    ((IClientChannel)client).Abort();
                    factory.Abort();
                }
            }
        }
    }
}
