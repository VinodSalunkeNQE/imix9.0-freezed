﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Reflection;
using System.Resources;
using System.Text;

namespace DigiPhoto.Culture
{
    public class ResourceCulture
    {
        public static ResourceManager GetCulture()
        {
            ResourceManager strCulture = null;
            string language = ConfigurationManager.AppSettings["IsArabic"];
            string rptchina = ConfigurationManager.AppSettings["IsChina"];
            if (language == "1")
            {                
                strCulture = new ResourceManager("DigiPhoto.Culture.Resource-ar", Assembly.GetExecutingAssembly());
            }
            if(rptchina == "1")
            {
                strCulture = new ResourceManager("DigiPhoto.Culture.Resource-ch", Assembly.GetExecutingAssembly());
            }
            return strCulture;
        }
    }
}
