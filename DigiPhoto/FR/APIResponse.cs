﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;

namespace DigiPhoto.FR
{
    public class APIResponse
    {
        public APIResponse()
        {
            this.SiteIDs = new List<string>();
        }
        public APIResponse(string response) : this()
        {
            //ErrorHandler.ErrorHandler.LogFileWrite(response);
            this.processRespoinse(response);
        }
        public List<string> SiteIDs { get; set; }

        private void processRespoinse(string apiresponse)
        {
            if (!string.IsNullOrEmpty(apiresponse))
            {
                string apiresonpose = apiresponse;
                apiresonpose = apiresonpose.Replace("{", string.Empty);
                apiresonpose = apiresonpose.Replace("}", string.Empty);
                string[] baseideresponse = apiresonpose.Split(':');
                apiresonpose = baseideresponse[1];
                apiresonpose = apiresonpose.Replace("[", string.Empty);
                apiresonpose = apiresonpose.Replace("]", string.Empty);
                apiresonpose = apiresonpose.Replace("\n", string.Empty);
                baseideresponse = apiresonpose.Split(',');
                if (baseideresponse.Count() == 1)
                {
                    if (String.IsNullOrEmpty(baseideresponse[0]))
                    {
                        this.SiteIDs.Add("0");
                    }
                    else
                        this.SiteIDs = baseideresponse.ToList();
                }
                else
                    this.SiteIDs = baseideresponse.ToList();
            }
            else
            {
                ErrorHandler.ErrorHandler.LogFileWrite("Null response from API");
                this.SiteIDs.Add("0");
            }
        }
    }
}
