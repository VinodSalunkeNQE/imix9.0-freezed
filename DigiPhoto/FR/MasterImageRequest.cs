﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
/// <summary>
/// ADDED BY KAILASH ON 25 SEP 2019 TO SEND REQUEST TO FR EUCLID API
/// </summary>
namespace DigiPhoto.FR
{
    public sealed class MasterImageRequest
    {
        public string ScanImage { get; set; }
        public string StartDateTime { get; set; }
        public string EndDateTime { get; set; }
        public string Location { get; set; }
        public string Country { get; set; }
        //public string Request { get { return this.CreateRequest(); } }
        //public string RequestWithoutBase64 { get; set; }
        public string Threshold { get; set; }
        public string SiteName { get; set; }
        public int ImageID { get; set; }
        //public MasterImageRequest(string path)
        //{
            
        //    //this.ScanImage = "Test";
        //    //this.StartDateTime = "09/24/2019 06:00:00";
        //    //this.EndDateTime = "09/24/2019 23:00:00";

        //    //this.StartDateTime = "18-09-2019 10:00:00";
        //    //this.EndDateTime = "18-09-2019 16:00:00";

        //    //this.CreateRequest();
        //}
        public string CreateRequest(string filepath)
        {
            this.ScanImage = this.ImageToBase64(filepath);

            StringBuilder stringBuilder = new StringBuilder();
            //stringBuilder.Append("\"");
            stringBuilder.Append("{");
            stringBuilder.Append("\"");
            stringBuilder.Append("ScanImage");
            stringBuilder.Append("\"");
            stringBuilder.Append(":");
            stringBuilder.Append("\"");
            stringBuilder.Append(this.ScanImage);
            stringBuilder.Append("\"");
            stringBuilder.Append(",");
            stringBuilder.Append("\"");
            stringBuilder.Append("StartDateTime");
            stringBuilder.Append("\"");
            stringBuilder.Append(":");
            stringBuilder.Append("\"");
            stringBuilder.Append(this.StartDateTime);
            stringBuilder.Append("\"");
            stringBuilder.Append(",");
            stringBuilder.Append("\"");
            stringBuilder.Append("EndDateTime");
            stringBuilder.Append("\"");
            stringBuilder.Append(":");
            stringBuilder.Append("\"");
            stringBuilder.Append(this.EndDateTime);
            stringBuilder.Append("\"");//
            stringBuilder.Append(",");
            stringBuilder.Append("\"");
            stringBuilder.Append("ConfidenceThreshold");
            stringBuilder.Append("\"");
            stringBuilder.Append(":");
            stringBuilder.Append("\"");
            stringBuilder.Append(this.Threshold);
            stringBuilder.Append("\"");
            stringBuilder.Append(",");
            stringBuilder.Append("\"");
            stringBuilder.Append("Site");
            stringBuilder.Append("\"");
            stringBuilder.Append(":");
            stringBuilder.Append("\"");
            stringBuilder.Append(this.SiteName);
            stringBuilder.Append("\"");
            stringBuilder.Append(",");
            stringBuilder.Append("\"");
            stringBuilder.Append("ImageId");
            stringBuilder.Append("\"");
            stringBuilder.Append(":");
            stringBuilder.Append("\"");
            stringBuilder.Append(this.ImageID);
            stringBuilder.Append("\"");
            stringBuilder.Append("}");

           return stringBuilder.ToString();
        }
        public string CreateRequest(int fileID)
        {
            //this.RequestWithoutBase64 = this.CreateRequestWithoutBase64(filepath);
            //ErrorHandler.ErrorHandler.LogFileWrite(" fileID 97:" + fileID.ToString());
            StringBuilder stringBuilder = new StringBuilder();
            //stringBuilder.Append("\"");
            stringBuilder.Append("{");
            stringBuilder.Append("\"");
            stringBuilder.Append("ScanImage");
            stringBuilder.Append("\"");
            stringBuilder.Append(":");
            stringBuilder.Append("\"");
            stringBuilder.Append(string.Empty);
            stringBuilder.Append("\"");
            stringBuilder.Append(",");
            stringBuilder.Append("\"");
            stringBuilder.Append("StartDateTime");
            stringBuilder.Append("\"");
            stringBuilder.Append(":");
            stringBuilder.Append("\"");
            stringBuilder.Append(this.StartDateTime);
            stringBuilder.Append("\"");
            stringBuilder.Append(",");
            stringBuilder.Append("\"");
            stringBuilder.Append("EndDateTime");
            stringBuilder.Append("\"");
            stringBuilder.Append(":");
            stringBuilder.Append("\"");
            stringBuilder.Append(this.EndDateTime);
            stringBuilder.Append("\"");//
            stringBuilder.Append(",");
            stringBuilder.Append("\"");
            stringBuilder.Append("ConfidenceThreshold");
            stringBuilder.Append("\"");
            stringBuilder.Append(":");
            stringBuilder.Append("\"");
            stringBuilder.Append(this.Threshold);
            stringBuilder.Append("\"");
            stringBuilder.Append(",");
            stringBuilder.Append("\"");
            stringBuilder.Append("Site");
            stringBuilder.Append("\"");
            stringBuilder.Append(":");
            stringBuilder.Append("\"");
            stringBuilder.Append(this.SiteName);
            stringBuilder.Append("\"");
            stringBuilder.Append(",");
            stringBuilder.Append("\"");
            stringBuilder.Append("ImageId");
            stringBuilder.Append("\"");
            stringBuilder.Append(":");
            stringBuilder.Append("\"");
            stringBuilder.Append(fileID);
            stringBuilder.Append("\"");
            stringBuilder.Append("}");

            return stringBuilder.ToString();
        }
        private string ImageToBase64(string path)
        {
            //string path = @"D:/DigiImages/20190923/2391312_5.jpg";
            using (System.Drawing.Image image = System.Drawing.Image.FromFile(path))
            {
                using (MemoryStream m = new MemoryStream())
                {
                    image.Save(m, image.RawFormat);
                    byte[] imageBytes = m.ToArray();
                    return Convert.ToBase64String(imageBytes);
                }
            }
        }
    }
}
