﻿using DigiPhoto.IMIX.Business;
using DigiPhoto.IMIX.Model;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Script.Serialization;
/// <summary>
/// ADDED BY KAILASH ON 25 SEP 2019 TO CALL FR APIs
/// </summary>
namespace DigiPhoto.FR
{
    public class APIWrapper
    {
        private static FRAPIDetails fRAPIDetails;// ADDED BY KAILASH ON 25 SEP 2019 TO GET API CONFIGURATION DETAILS.
        public static int SelectedSite = 0;
        public static string SelectedThreshold = "HIGH";
        //public static MasterImageRequest Request;
        public APIWrapper()
        {
            this.GetAPIDetails();
        }
        public bool GetToken()
        {


            bool result = false;
            if (fRAPIDetails.HasError == false)
            {
                try
                {
                    //string client_id = "7AZdYejwmPU6yyOyFP1488aj";
                    //string client_secret = "KOdoehlIWNFIRB9wZFesppWbMahNjqGZDrj7bKRmymahr2Jl ";

                    string client_id = fRAPIDetails.client_id;
                    string client_secret = fRAPIDetails.client_secret;

                    string svcCredentials = Convert.ToBase64String(ASCIIEncoding.ASCII.GetBytes(client_id + ":" + client_secret));

                    //RestClient client = new RestClient("https://10.84.0.167/oauth/token");
                    Uri url = new Uri(fRAPIDetails.TokenPAI);
                    RestClient client = new RestClient(url);
                    client.RemoteCertificateValidationCallback = (sender, certificate, chain, sslPolicyErrors) => true;
                    var request = new RestRequest(Method.POST);
                    request.AddHeader("Authorization", "Basic " + svcCredentials);
                    request.AddHeader("content-type", "application/x-www-form-urlencoded");
                    //request.AddParameter("application/x-www-form-urlencoded", "grant_type=password&scope=embedding&username=dei_user_01&password=password123", ParameterType.RequestBody);
                    request.AddParameter("application/x-www-form-urlencoded", "grant_type=" + fRAPIDetails.grant_type + "&scope=" + fRAPIDetails.scope + "&username=" + fRAPIDetails.username + "&password=" + fRAPIDetails.password, ParameterType.RequestBody);
                    request.RequestFormat = DataFormat.Json;

                    ErrorHandler.ErrorHandler.LogFileWrite("Token Request :" + request);
                    IRestResponse response = client.Execute(request);

                    Console.WriteLine(response.Content);
                    ErrorHandler.ErrorHandler.LogFileWrite("Token Response :" + response.Content + " Status Code : " + response.StatusCode.ToString());


                    if (response.Content.Contains("access_token") == true)
                    {
                        APITokenResponse aPIResponse = new JavaScriptSerializer().Deserialize<APITokenResponse>(response.Content);
                        fRAPIDetails.Token = aPIResponse.access_token;
                        //TimeSpan timeSpan = TimeSpan.FromSeconds(aPIResponse.expires_in);
                        //fRAPI.ExpireDate = DateTime.Now.AddSeconds(timeSpan.Seconds);
                        fRAPIDetails.ExpireDate = DateTime.Now.AddSeconds(aPIResponse.expires_in);
                        result = true;
                    }
                    else
                    {
                        //DigiFasialLibrary.Logger.Error("API Error", new Exception(response.Content));
                        ErrorHandler.ErrorHandler.LogError(new Exception(response.Content));
                        result = false;
                    }
                }


                catch (Exception ex)
                {
                    ErrorHandler.ErrorHandler.LogError(ex);
                    result = false;
                }
            }
            else
            {
                ErrorHandler.ErrorHandler.LogError(new Exception(fRAPIDetails.ErrorMessage));
                result = false;
            }
            return result;
        }
        public APIResponse GetImages(string imagepath, string StartDateTime, string EndDateTime, string threshold, int sitename)
        {
            APIResponse aPIResponse;
            if (fRAPIDetails.HasError == false)
            {
                try
                {
                    //string requestjson = new JavaScriptSerializer().Serialize(new MasterImageRequest(@"D:/DigiImages/20190923/2391312_5.jpg"));
                    MasterImageRequest masterImageRequest = new MasterImageRequest();
                    masterImageRequest.StartDateTime = StartDateTime;
                    masterImageRequest.EndDateTime = EndDateTime;
                    masterImageRequest.Threshold = threshold;
                    masterImageRequest.SiteName =(sitename==0?"ALL":sitename.ToString());

                    //APIWrapper.Request = masterImageRequest;
                    //RestClient client = new RestClient("https://10.84.0.167/euclid");
                    Uri url = new Uri(fRAPIDetails.ImageAPI);
                    RestClient client = new RestClient(url);
                    client.RemoteCertificateValidationCallback = (sender, certificate, chain, sslPolicyErrors) => true;

                    var request = new RestRequest(Method.POST);
                    //request.AddHeader("Authorization", "Bearer " + fRAPI.FRConfigs.Token);

                    request.AddHeader("Authorization", "Bearer " + fRAPIDetails.Token);
                    request.AddHeader("content-type", "application/Json");
                    request.AddParameter("undefined", masterImageRequest.CreateRequest(imagepath), ParameterType.RequestBody);
                    request.RequestFormat = DataFormat.Json;
                    ErrorHandler.ErrorHandler.LogFileWrite("euclid Request[" + DateTime.Now.Ticks.ToString() + "] | " + DateTime.Now + " : " + masterImageRequest.CreateRequest(0));
                    IRestResponse response = client.Execute(request);
                    ErrorHandler.ErrorHandler.LogFileWrite("euclid Response[" + DateTime.Now.Ticks.ToString() + "] | " + DateTime.Now + " : " + response.Content);
                    if (response.Content.Contains("error") == true)
                    {
                        //ErrorHandler.ErrorHandler.LogFileWrite(response.Content);
                        aPIResponse = new APIResponse();
                    }
                    else
                        aPIResponse = new APIResponse(response.Content);

                }
                catch (Exception ex)
                {
                    ErrorHandler.ErrorHandler.LogError(ex);
                    aPIResponse = new APIResponse();
                    return aPIResponse;
                }

            }
            else
            {
                aPIResponse = new APIResponse();
                ErrorHandler.ErrorHandler.LogError(new Exception(fRAPIDetails.ErrorMessage));
            }
            return aPIResponse;

        }
        public APIResponse GetImages(int imageID, string StartDateTime, string EndDateTime, string threshold, int sitename)
        {
            ErrorHandler.ErrorHandler.LogFileWrite(" imageID :" + imageID.ToString());
            APIResponse aPIResponse;
            if (fRAPIDetails.HasError == false)
            {
                try
                {
                    //string requestjson = new JavaScriptSerializer().Serialize(new MasterImageRequest(@"D:/DigiImages/20190923/2391312_5.jpg"));
                    MasterImageRequest masterImageRequest = new MasterImageRequest();
                    masterImageRequest.StartDateTime = StartDateTime;
                    masterImageRequest.EndDateTime = EndDateTime;
                    masterImageRequest.Threshold = threshold;
                    masterImageRequest.SiteName = (sitename == 0 ? "ALL" : sitename.ToString());

                    //RestClient client = new RestClient("https://10.84.0.167/euclid");
                    Uri url = new Uri(fRAPIDetails.ImageAPI);
                    RestClient client = new RestClient(url);
                    client.RemoteCertificateValidationCallback = (sender, certificate, chain, sslPolicyErrors) => true;

                    var request = new RestRequest(Method.POST);
                    //request.AddHeader("Authorization", "Bearer " + fRAPI.FRConfigs.Token);

                    request.AddHeader("Authorization", "Bearer " + fRAPIDetails.Token);
                    request.AddHeader("content-type", "application/Json");
                    request.AddParameter("undefined", masterImageRequest.CreateRequest(imageID), ParameterType.RequestBody);
                    request.RequestFormat = DataFormat.Json;
                    string processid = DateTime.Now.Ticks.ToString();
                    ErrorHandler.ErrorHandler.LogFileWrite("euclid Request[" + processid + "] | " + DateTime.Now + " : " + masterImageRequest.CreateRequest(imageID));
                    IRestResponse response = client.Execute(request);
                    ErrorHandler.ErrorHandler.LogFileWrite("euclid Response[" + processid + "] | " + DateTime.Now + " : " + response.Content);
                    if (response.Content.Contains("error") == true)
                    {
                        //ErrorHandler.ErrorHandler.LogFileWrite(response.Content);
                        aPIResponse = new APIResponse();
                    }
                    else
                        aPIResponse = new APIResponse(response.Content);

                }
                catch (Exception ex)
                {
                    ErrorHandler.ErrorHandler.LogError(ex);
                    aPIResponse = new APIResponse();
                    return aPIResponse;
                }

            }
            else
            {
                aPIResponse = new APIResponse();
                ErrorHandler.ErrorHandler.LogError(new Exception(fRAPIDetails.ErrorMessage));
            }
            return aPIResponse;

        }
        private string CreateTokenRequest()
        {
            APITokenRequest aPITokenRequest = new APITokenRequest();
            aPITokenRequest.password = fRAPIDetails.password;
            aPITokenRequest.username = fRAPIDetails.username;
            aPITokenRequest.scope = "embedding ";
            return new JavaScriptSerializer().Serialize(aPITokenRequest);
        }
        private void GetAPIDetails()
        {
            ErrorHandler.ErrorHandler.LogFileWrite("GetAPIDetails START");
            try
            {
                FRTokenBusiness fRTokenBusiness = new FRTokenBusiness();
                if (fRAPIDetails == null)
                {
                    fRAPIDetails = new FRAPIDetails();
                    fRAPIDetails = fRTokenBusiness.GetToken(fRAPIDetails);
                }
                if (fRAPIDetails.Token == "NA" || fRAPIDetails.ExpireDate <= DateTime.Now)
                {
                    ErrorHandler.ErrorHandler.LogFileWrite("Error in Token");
                    ErrorHandler.ErrorHandler.LogFileWrite("Token : " + fRAPIDetails.Token);
                    ErrorHandler.ErrorHandler.LogFileWrite("ExpireDate : " + fRAPIDetails.ExpireDate);

                    this.GetToken();

                    fRTokenBusiness.SaveNewToken(fRAPIDetails);
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
            }
            ErrorHandler.ErrorHandler.LogFileWrite("GetAPIDetails END");
        }

    }


    public class EmbeddingAPIResponse
    {
        public string ImageID { get; set; }
        public string Message { get; set; }
    }
    public class APITokenRequest
    {
        //public string client_id { get; set; }
        //public string client_secret { get; set; }
        public string grant_type { get; set; }
        public string scope { get; set; }
        public string username { get; set; }
        public string password { get; set; }
        public APITokenRequest()
        {
            this.grant_type = "password";
        }
    }
    public class APITokenResponse
    {
        public string access_token { get; set; }
        public double expires_in { get; set; }
    }
}
