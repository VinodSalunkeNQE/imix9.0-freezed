﻿using DigiPhoto.Common;
using DigiPhoto.IMIX.Model;
using DigiPhoto.Interop;
using FrameworkHelper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
//using VisioForge.Types;
using WMPLib;

namespace DigiPhoto
{
    /// <summary>
    /// Interaction logic for PanControl.xaml
    /// </summary>
    public partial class FrameControl : UserControl
    {
        private UIElement _parent;
        private bool _result = false;
        List<VideoFrames> lstVideoFrames = new List<VideoFrames>();
        BusyWindow bs = new BusyWindow();
        public string LicenseKey = string.Empty;
        public string UserName = string.Empty;
        public string EmailID = string.Empty;
        [System.Runtime.InteropServices.DllImport("gdi32.dll")]
        public static extern bool DeleteObject(IntPtr hObject);
        public FrameControl()
        {
            InitializeComponent();
            Visibility = Visibility.Hidden;
        }
        public void frameControlListAutoFill(List<PanProperty> ppLstMain)
        {
           
        }
        public void SetParent(UIElement parent)
        {
            _parent = (UIElement)parent;
        }
        private void btnClose_Click(object sender, RoutedEventArgs e)
        {
            if(RangeSldGuestVideo.LowerValue==RangeSldGuestVideo.UpperValue)
            {
                MessageBox.Show("Video start time should be less than video end time!");
                return;
            }
            HideHandlerDialog();
            setTimeGrid.Visibility = Visibility.Hidden;
            OnExecuteMethod();
        }
        private void HideHandlerDialog()
        {
            Visibility = Visibility.Collapsed;
            _parent.IsEnabled = true;
        }
        string videoPath = "";
        long extractVideoEndTime = 0;
        public bool ShowPanHandlerDialog(string _videoPath,long _extractVideoEndTime)
        {
            Visibility = Visibility.Visible;
            extractVideoEndTime = _extractVideoEndTime;
            _parent.IsEnabled = false;
            videoPath = _videoPath;
            errorTxt.Visibility = Visibility.Collapsed;
            lstFrames.Visibility = Visibility.Hidden;
            RangeSldGuestVideo.Visibility = Visibility.Hidden;
            lstVideoFrames.Clear();
            RangeSldGuestVideo.Maximum = extractVideoEndTime;
            RangeSldGuestVideo.UpperValue = extractVideoEndTime;
            RangeSldGuestVideo.LowerValue = 1;
            RangeSldGuestVideo.Minimum = 1;
            bs.Show();
            ExtractFrames(videoPath, extractVideoEndTime);
            bs.Hide();
            return _result;
        }
        public event EventHandler ExecuteMethod;
        protected virtual void OnExecuteMethod()
        {
            if (ExecuteMethod != null) ExecuteMethod(this, EventArgs.Empty);
        }
        private void ExtractFrames(string fileName, long len)
        {
            //try
            //{
            //    //VisioForge.Controls.WPF.MediaPlayer VisioMediaPlayerForFrames = new VisioForge.Controls.WPF.MediaPlayer();
            //    //VisioMediaPlayerForFrames.Video_Effects_Clear();
            //    if(len==0)
            //    {
            //        string extension = System.IO.Path.GetExtension(fileName).ToLower();
            //        if (extension.Equals(".flv"))
            //        {
            //            FlvMetaInfo metaInfo = FlvMetaDataReader.GetFlvMetaInfo(fileName);
            //            len = Convert.ToInt64(metaInfo.Duration);
            //        }
            //        else
            //        {
            //            len = (int)(VisioMediaPlayerForFrames.Duration_Time() / 1000.0);
            //        }
            //        if (len == 0)
            //        {
            //            var player = new WindowsMediaPlayer();
            //            var clip = player.newMedia(fileName);
            //            len = string.IsNullOrEmpty(TimeSpan.FromSeconds(clip.duration).ToString()) ? 0 : (long)(TimeSpan.FromSeconds(clip.duration).TotalSeconds);
            //       } 
            //    }
            //    VisioMediaPlayerForFrames.SetLicenseKey(LicenseKey, UserName, EmailID);
            //    VisioMediaPlayerForFrames.Video_Renderer = VFVideoRendererWPF.WPF;
            //    string extension1 = System.IO.Path.GetExtension(fileName);
            //    if (extension1.ToLower().Equals(".wmv") || extension1.ToLower().Equals(".mp3"))
            //    {
            //        VisioMediaPlayerForFrames.Source_Mode = VFMediaPlayerSource.MMS_WMV_DS;
            //        VisioMediaPlayerForFrames.Custom_Video_Decoder = String.Empty;
            //    }
            //    else if (extension1.ToLower().Equals(".mp4") || extension1.ToLower().Equals(".mov") || extension1.ToLower().Equals(".3gp") || extension1.ToLower().Equals(".flv"))
            //    {
            //        VisioMediaPlayerForFrames.Source_Mode = VFMediaPlayerSource.HTTP_RTSP_FFMPEG;
            //        VisioMediaPlayerForFrames.Custom_Video_Decoder = String.Empty;
            //    }
            //    else if (extension1.ToLower().Equals(".avi"))
            //    {
            //        VisioMediaPlayerForFrames.Source_Mode = VFMediaPlayerSource.File_FFMPEG;
            //        VisioMediaPlayerForFrames.Custom_Video_Decoder = "ffdshow Video Decoder";
            //    }
            //    VisioMediaPlayerForFrames.Frame_Save_Resize = true;    
            //    VisioMediaPlayerForFrames.FilenamesOrURL.Capacity = 1;
            //    VisioMediaPlayerForFrames.BackgroundImage_Stretch = Stretch.UniformToFill;
            //    VisioMediaPlayerForFrames.FilenamesOrURL.Add(fileName);
            //    VisioMediaPlayerForFrames.Audio_Play = false;
            //    VisioMediaPlayerForFrames.Play();
               
            //    for (int i = 1; i <= (len); i++)
            //    {
            //        VisioMediaPlayerForFrames.Position_Set_Time(i * 1000);
            //        VideoFrames vf = new VideoFrames();
            //        System.Drawing.Bitmap bdm = VisioMediaPlayerForFrames.Frame_GetCurrent();
            //        IntPtr hBitmap = bdm.GetHbitmap();
            //        vf.bitSource = System.Windows.Interop.Imaging.CreateBitmapSourceFromHBitmap(hBitmap, IntPtr.Zero, Int32Rect.Empty,System.Windows.Media.Imaging.BitmapSizeOptions.FromWidthAndHeight(120, 130)); //loadBitmap(bdm);     
            //        DeleteObject(hBitmap);
            //        vf.frameIndex = i;
            //        if(i==1)
            //        {
            //            vf.StrokeColor = "Red";
            //            vf.visibilityCanvas = Visibility.Visible;
            //        }
            //        else if (i==len)
            //        {
            //            vf.StrokeColor = "Green";
            //            vf.visibilityCanvas = Visibility.Visible;
            //        }
            //        else
            //        {
            //            vf.StrokeColor = "Red";
            //            vf.visibilityCanvas = Visibility.Collapsed;
            //        }
            //        lstVideoFrames.Add(vf);
            //    }      
            //    VisioMediaPlayerForFrames.Stop();
            //    lstFrames.ItemsSource = null;
            //    if (lstVideoFrames.Count > 0)
            //    {
            //        lstFrames.ItemsSource = lstVideoFrames;
            //        lstFrames.UpdateLayout();
            //        RangeSldGuestVideo.Visibility = Visibility.Visible;
            //        lstFrames.Visibility = Visibility.Visible;
            //        setTimeGrid.Visibility = Visibility.Visible;
            //        VideoFrames obj = new VideoFrames();
            //        if (lstVideoFrames.Count > 0)
            //        {
            //            obj = lstVideoFrames[1];
            //        }
            //        lstFrames.SelectedIndex = 1;
            //        lstFrames.ScrollIntoView(obj);
            //    }
            //    else
            //    {
            //        errorTxt.Visibility = Visibility.Visible;
            //        RangeSldGuestVideo.Visibility = Visibility.Hidden;
            //        lstFrames.Visibility = Visibility.Hidden;
            //        setTimeGrid.Visibility = Visibility.Hidden;
            //    }
               
            //}
            //catch (Exception ex)
            //{      
            //    bs.Hide();
            //    errorTxt.Visibility=Visibility.Visible;
            //    RangeSldGuestVideo.Visibility = Visibility.Hidden;
            //    lstFrames.Visibility = Visibility.Hidden;
            //    setTimeGrid.Visibility = Visibility.Hidden;
            //    string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
            //    ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            //}
        }
        public static BitmapSource loadBitmap(System.Drawing.Bitmap source)
        {
            return System.Windows.Interop.Imaging.CreateBitmapSourceFromHBitmap(source.GetHbitmap(), IntPtr.Zero, Int32Rect.Empty,
                System.Windows.Media.Imaging.BitmapSizeOptions.FromWidthAndHeight(120,130));

        }
        string a;
        private void txtnumber_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (string.IsNullOrEmpty(((TextBox)sender).Text))

                a = "";
            else
            {
                int num = 0;
                bool success = int.TryParse(((TextBox)sender).Text, out num);
                if (success & num >= 0)
                {
                    ((TextBox)sender).Text.Trim();
                    a = ((TextBox)sender).Text;
                }
                else
                {
                    ((TextBox)sender).Text = a;
                    ((TextBox)sender).SelectionStart = ((TextBox)sender).Text.Length;
                }
            }
            string txt=(((TextBox)sender).Text);
        }
    
        private bool IsNumeric(string text)
        {
            try
            {
                int output;
                return int.TryParse(text, out output);
            }
            catch
            {
                return false;
            }
        }

        private void txtvideoStart_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (string.IsNullOrEmpty(((TextBox)sender).Text))

                a = "";
            else
            {
                int num = 0;
                bool success = int.TryParse(((TextBox)sender).Text, out num);
                if (success & num >= 0)
                {
                    ((TextBox)sender).Text.Trim();
                    a = ((TextBox)sender).Text;
                }
                else
                {
                    ((TextBox)sender).Text = a;
                    ((TextBox)sender).SelectionStart = ((TextBox)sender).Text.Length;
                }
            }
            if (string.IsNullOrEmpty(((TextBox)sender).Text))
                return;
            string txt = (((TextBox)sender).Text);
            int startTime = Convert.ToInt32(txt);
            updateList(startTime, Convert.ToInt32(RangeSldGuestVideo.UpperValue),true);
        }
        private void txtVideoEnd_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (string.IsNullOrEmpty(((TextBox)sender).Text))

                a = "";
            else
            {
                int num = 0;
                bool success = int.TryParse(((TextBox)sender).Text, out num);
                if (success & num >= 0)
                {
                    ((TextBox)sender).Text.Trim();
                    a = ((TextBox)sender).Text;
                }
                else
                {
                    ((TextBox)sender).Text = a;
                    ((TextBox)sender).SelectionStart = ((TextBox)sender).Text.Length;
                }
            }
            if (string.IsNullOrEmpty(((TextBox)sender).Text))
                return;
            string txt = (((TextBox)sender).Text);
            int endTime = Convert.ToInt32(txt);
            updateList(Convert.ToInt32(RangeSldGuestVideo.LowerValue),endTime,false);
        }
        void updateList(int start,int end, bool isStart)
        {
            try
            {
                int selectedind = 0;
                for (int i = 0; i < lstVideoFrames.Count; i++)
                {
                    if (i == start - 1 && isStart == true)
                    {
                        lstVideoFrames[end - 1].StrokeColor = "Green";
                        lstVideoFrames[end - 1].visibilityCanvas = Visibility.Visible;
                        lstVideoFrames[i].StrokeColor = "Red";
                        lstVideoFrames[i].visibilityCanvas = Visibility.Visible;
                        selectedind = i;
                    }
                    else if (i == end - 1 && isStart == false)
                    {
                        lstVideoFrames[start - 1].StrokeColor = "Red";
                        lstVideoFrames[start - 1].visibilityCanvas = Visibility.Visible;
                        lstVideoFrames[i].StrokeColor = "Green";
                        lstVideoFrames[i].visibilityCanvas = Visibility.Visible;
                        selectedind = i;
                    }
                    else
                    {
                        lstVideoFrames[i].visibilityCanvas = Visibility.Hidden;
                        // selectedind = i;
                    }
                }
                VideoFrames obj = new VideoFrames();
                if (lstVideoFrames.Count > 0)
                {
                    obj = lstVideoFrames[selectedind];
                }
                lstFrames.ItemsSource = null;
                lstFrames.ItemsSource = lstVideoFrames;
                lstFrames.SelectedIndex = selectedind;
                lstFrames.ScrollIntoView(obj);
                lstFrames.UpdateLayout();
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }

        private void UserControl_Unloaded(object sender, RoutedEventArgs e)
        {
            if (lstVideoFrames != null)
                lstVideoFrames = null;
            
            
        }
                   
    }
    
}
