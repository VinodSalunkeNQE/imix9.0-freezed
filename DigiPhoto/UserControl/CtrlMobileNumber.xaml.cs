﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Text.RegularExpressions;
using DigiPhoto.DataLayer;
using System.Windows.Threading;
using System.Threading;
using DigiPhoto.DataLayer.Model;
using DigiPhoto.IMIX.Business;
using System.ComponentModel;
using System.Windows.Media.Animation;
using RestSharp;
using DigiPhoto.IMIX.Model;
using System.Net;
using System.Configuration;

namespace DigiPhoto
{
    /// <summary>
    /// Interaction logic for CtrlMobileNumber.xaml
    /// </summary>
    public partial class CtrlMobileNumber : UserControl
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CtrlMobileNumber"/> class.
        /// </summary>
        public CtrlMobileNumber()
        {
            InitializeComponent();
            Visibility = Visibility.Hidden;
            DataContext = this;
        }


        /// <summary>
        /// The _parent
        /// </summary>
        private UIElement _parent;
        /// <summary>
        /// The _hide request
        /// </summary>
        private bool _hideRequest = false;
        /// <summary>
        /// The _result
        /// </summary>
        private string _result;

        /// <summary>
        /// Sets the parent.
        /// </summary>
        /// <param name="parent">The parent.</param>
        public void SetParent(UIElement parent)
        {
            _parent = (UIElement)parent;
        }
        #region Message

        /// <summary>
        /// Gets or sets the message cash payment.
        /// </summary>
        /// <value>
        /// The message cash payment.
        /// </value>
        public string MessageMobileNumber
        {
            get { return (string)GetValue(MessageMobileNumberProperty); }
            set { SetValue(MessageMobileNumberProperty, value); }
        }

        // Using a DependencyProperty as the backing store for Message.
        // This enables animation, styling, binding, etc...
        /// <summary>
        /// The message cash payment property
        /// </summary>
        public static readonly DependencyProperty MessageMobileNumberProperty =
            DependencyProperty.Register(
                "MessageMobileNumber", typeof(string), typeof(ModalDialog), new UIPropertyMetadata(string.Empty));


        private int nameValue;

        public int Name1
        {
            get
            {
                return nameValue;
            }
            set
            {
                nameValue = value;
            }
        }

        List<LstMyItems> myMobileItems;

        string orderNumber;
        Dictionary<string, string> lstPrinterList;

        /// <summary>
        /// Shows the handler dialog.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <returns></returns>
        public string ShowHandlerDialog(string message, List<LstMyItems> grpItems, string order)
        {
            WhatsAppBusiness whatsApp = new WhatsAppBusiness();
            WhatsAppSettings wtsAppData = whatsApp.GetWhatsAppDetail();
            if (wtsAppData.HostUrl != null)
            {

                FillCountryList();
                TxtMobileNum.Text = String.Empty;
                MessageMobileNumber = message;
                Visibility = Visibility.Visible;
                //LoadCurrency();
                TxtMobileNum.CaretIndex = 0;
                TxtMobileNum.Focus();

                _parent.IsEnabled = true;
                _hideRequest = false;
                TxtMobileNum.Text = String.Empty;
                myMobileItems = grpItems;
                orderNumber = order;




                //Uri uri = new Uri(wtsAppData.HostUrl);
                //WebRequest request = WebRequest.Create(uri);
                //request.Timeout = 3000;
                //WebResponse response;
                //response = request.GetResponse();
                //if (response != null)
                //{
                WhatsMsg1.Text = "WhatsAPP is connected now...";
                WhatsMsg1.Background = Brushes.Green;
                //}
                //else
                //{
                //    WhatsMsg1.Text = "WhatsAPP is Not connected...";
                //    WhatsMsg1.Background = Brushes.Red;
                //}


                if (message.Contains("Cash"))
                {
                    MobileNumberGrid.Visibility = System.Windows.Visibility.Visible;
                    SPSubmitCancel.Visibility = System.Windows.Visibility.Visible;
                    WhatsAppMsg1.Visibility = System.Windows.Visibility.Visible;

                    WhatsAppGrid.Visibility = System.Windows.Visibility.Hidden;
                    ProgressBarMy.Visibility = System.Windows.Visibility.Hidden;
                    WhatsAppMsg2.Visibility = System.Windows.Visibility.Hidden;
                }
                else
                {

                    //Window_ContentRendered();
                    MobileNumberGrid.Visibility = System.Windows.Visibility.Hidden;
                    SPSubmitCancel.Visibility = System.Windows.Visibility.Hidden;
                    WhatsAppMsg1.Visibility = System.Windows.Visibility.Hidden;

                    WhatsAppGrid.Visibility = System.Windows.Visibility.Visible;
                    ProgressBarMy.Visibility = System.Windows.Visibility.Visible;
                    WhatsAppMsg2.Visibility = System.Windows.Visibility.Visible;
                }



                // TxtMobileNum.Text = "0";
                while (!_hideRequest)
                {


                    // HACK: Stop the thread if the application is about to close
                    if (this.Dispatcher.HasShutdownStarted ||
                        this.Dispatcher.HasShutdownFinished)
                    {
                        break;
                    }
                    // HACK: Simulate "DoEvents"
                    this.Dispatcher.Invoke(
                        DispatcherPriority.Background,
                        new ThreadStart(delegate { }));
                    Thread.Sleep(20);

                    //if (!message.Contains("Cash"))
                    //{
                    //    while(Name < 100)
                    //    {
                    //        Thread.Sleep(1050);
                    //        this.Dispatcher.Invoke(() => //Use Dispather to Update UI Immediately  
                    //        {
                    //            Name = Name +10;

                    //        });
                    //    }
                    //}
                }

            }
            else

            {
                MessageBox.Show("Error: Please configure Whats App settings in Digiconfig utility.");
                throw new Exception();
            }
            return _result;
        }

        /// <summary>
        /// Hides the handler dialog.
        /// </summary>
        private void HideHandlerDialog()
        {

            _hideRequest = true;
            Visibility = Visibility.Collapsed;
            _parent.IsEnabled = true;
        }
        #endregion



        /// <summary>
        /// Handles the Click event of the rdoSelect control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void rdoSelect_Click(object sender, RoutedEventArgs e)
        {

        }
        /// <summary>
        /// Finds the visual child.
        /// </summary>
        /// <typeparam name="childItem">The type of the hild item.</typeparam>
        /// <param name="obj">The object.</param>
        /// <returns></returns>
        public static childItem FindVisualChild<childItem>(DependencyObject obj)
    where childItem : DependencyObject
        {
            // Search immediate children
            for (int i = 0; i < VisualTreeHelper.GetChildrenCount(obj); i++)
            {
                DependencyObject child = VisualTreeHelper.GetChild(obj, i);

                if (child is childItem)
                    return (childItem)child;

                else
                {
                    childItem childOfChild = FindVisualChild<childItem>(child);

                    if (childOfChild != null)
                        return childOfChild;
                }
            }

            return null;
        }
        /// <summary>
        /// Handles the Click event of the btnSubmit control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void btnSubmit_Click(object sender, RoutedEventArgs e)
        {
            try
            {


                if (Regex.IsMatch(TxtMobileNum.Text, @"^\d+$") && TxtMobileNum.Text.Length >= 6 && cmbCountryCode.SelectedIndex > 0)
                {

                    //HideHandlerDialog();
                    //ShowHandlerDialog("Trans");

                    //MobileNumberGrid.Visibility = System.Windows.Visibility.Hidden;
                    //SPSubmitCancel.Visibility = System.Windows.Visibility.Hidden;
                    //WhatsAppMsg1.Visibility = System.Windows.Visibility.Hidden;

                    //WhatsAppGrid.Visibility = System.Windows.Visibility.Visible;
                    //ProgressBarMy.Visibility = System.Windows.Visibility.Visible;
                    //WhatsAppMsg2.Visibility = System.Windows.Visibility.Visible;

                    //Duration duration = new Duration(TimeSpan.FromSeconds(1));
                    //DoubleAnimation doubleAnimation = new DoubleAnimation(ProgressBarMobile.Value + 100, duration);
                    //ProgressBarMobile.BeginAnimation(ProgressBar.ValueProperty, doubleAnimation);

                    string destinationPathMobile;

                    bool shareStatus = false;

                    foreach (LstMyItems gitem in myMobileItems)
                    {
                        if (gitem.IsItemSelected)
                        {
                            WhatsAppBusiness whatsApp = new WhatsAppBusiness();
                            WhatsAppSettings wtsAppData = whatsApp.GetWhatsAppDetail();


                            destinationPathMobile = string.Empty;
                            destinationPathMobile = DateTime.Now.ToString("yyyyMMdd") + "/" + orderNumber + "/" + gitem.FileName;

                            destinationPathMobile = wtsAppData.HostUrl + destinationPathMobile;

                            string newCloudURL = @"https://res.cloudinary.com/www-commdel-net/image/upload/";
                            newCloudURL= newCloudURL + ConfigurationManager.AppSettings["CloudinaryOrderImagesPath"].ToString() + "/" + orderNumber + "/" + gitem.FileName;
                            //destinationPathMobile = @"https://aquariaklccmomentsbeta.mydeievents.com/WhatsApp/20190509/DG-2103217524/10525.mp4";
                            string mainurl = System.Web.HttpUtility.UrlEncode(destinationPathMobile);



                            string APIKey = wtsAppData.APIKey;
                            string mobileNumber = TxtMobileNum1.Text.Replace("-", "").Trim() + TxtMobileNum.Text.Trim();


                            //var client = new RestClient("https://panel.apiwha.com/send_message.php?apikey=9LIL8WR2P1QNSBHDH6K8&number=9189768990079&text=" + mainurl);

                            //var client = new RestClient("https://panel.apiwha.com/send_message.php?apikey=" + wtsAppData.APIKey + "&number=" + mobileNumber.Trim() + "&text=" + mainurl.Trim() + "&custom_data=" + orderNumber);

                            //var request = new RestRequest(Method.GET);
                            //IRestResponse response = client.Execute(request);

                            string currentSelectedValue = cmbCountryCode.SelectedValue.ToString();


                            string[] listfolder = gitem.HotFolderPath.Split(new string[] { "\\\\" }, StringSplitOptions.None);
                            string newHotPath = string.Empty;
                            foreach (string strItem in listfolder)
                            {
                                if (strItem.Length > 0)
                                {
                                    newHotPath = newHotPath + strItem + "\\";
                                }
                            }
                            string fileFolderPath = "\\\\" + newHotPath + "WhatsAppProduct\\"
                                + DateTime.Now.ToString("yyyyMMdd") +
                              "\\" + orderNumber + "\\" + gitem.FileName;


                            int isWhatsAppCloudUpload = 0;
                            try
                            {
                                isWhatsAppCloudUpload = Convert.ToInt32(ConfigurationManager.AppSettings["isWhatsAppCloudUpload"].ToString());
                            }
                            catch (Exception)
                            {

                                isWhatsAppCloudUpload = 0;
                            }

                            if (isWhatsAppCloudUpload == 1)
                            {
                                shareStatus = whatsApp.SaveWhatsAppOrders(orderNumber, gitem.FileName, currentSelectedValue,
                                    TxtMobileNum1.Text.Replace("-", "").Trim(), mobileNumber, fileFolderPath,
                                    newCloudURL
                                    );
                            }
                            else
                            {
                                shareStatus = whatsApp.SaveWhatsAppOrders(orderNumber, gitem.FileName, currentSelectedValue,
                                   TxtMobileNum1.Text.Replace("-", "").Trim(), mobileNumber, fileFolderPath,
                                   destinationPathMobile
                                   );
                            }

                            //System.Globalization.CultureInfo[] cobj = System.Globalization.CultureInfo.GetCultures(System.Globalization.CultureTypes.AllCultures);

                        }
                    }

                    if (shareStatus)
                    {
                        MessageBox.Show(" Image Sharing is in-progress.\n Please Check WhatsApp Order status.");
                    }
                    else
                    {
                        MessageBox.Show("Sharing is not completed");
                    }

                    HideHandlerDialog();
                    //while (Name1 < 100)
                    //{
                    //    Thread.Sleep(1050);

                    //    Name1 = Name1 + 10;

                    //}

                    // ProgressBarMobile.Value = 10;
                    //CtrlMobileTransfer.SetParent(this);

                    //CtrlMobileTransfer.btnSubmit.IsDefault = true;
                    //var res = CtrlMobileTransfer.ShowHandlerDialog("Cash");

                    //CtrlMobileTransfer.btnSubmit.IsDefault = false;


                }
                else
                {
                    if (cmbCountryCode.SelectedIndex <= 0)
                    {
                        MessageBox.Show("Please select valid country", "DEI");
                    }
                    else
                    {
                        MessageBox.Show("Please enter valid Mobile number", "DEI");
                    }
                }


            }
            catch (Exception ex)
            {

                MessageBox.Show("Error while placing Order.\n" + ex.Message, "DEI");
            }

        }


        /// <summary>
        /// Handles the Click event of the btnSubmitCancel control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void btnSubmitCancel_Click(object sender, RoutedEventArgs e)
        {

            try
            {

                WhatsAppBusiness whatsApp = new WhatsAppBusiness();
                WhatsAppSettings wtsAppData = whatsApp.GetWhatsAppDetail();

                Uri uri = new Uri(wtsAppData.HostUrl);
                WebRequest request = WebRequest.Create(uri);
                request.Timeout = 3000;
                WebResponse response;
                response = request.GetResponse();
                if (response == null)
                {
                    MessageBox.Show("No Connectivity");
                }
                else
                {
                    MessageBox.Show("Connection Success");
                }

            }
            catch (Exception loi) { MessageBox.Show(loi.Message); }


            //TxtMobileNum.Focus();
            //TxtMobileNum.Text = string.Empty;
        }

        /// <summary>
        /// Pastings the handler.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="DataObjectPastingEventArgs"/> instance containing the event data.</param>
        private void PastingHandler(object sender, DataObjectPastingEventArgs e)
        {
            if (e.DataObject.GetDataPresent(typeof(String)))
            {
                String text = (String)e.DataObject.GetData(typeof(String));
                if (!IsTextAllowed(text)) e.CancelCommand();
            }
            else e.CancelCommand();
        }
        /// <summary>
        /// Determines whether [is text allowed] [the specified text].
        /// </summary>
        /// <param name="text">The text.</param>
        /// <returns></returns>
        private static bool IsTextAllowed(string text)
        {
            Regex regex = new Regex("[^0-9.-]+"); //regex that matches disallowed text
            return !regex.IsMatch(text);
        }
        /// <summary>
        /// Handles the PreviewTextInput event of the txtAmountEntered control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="TextCompositionEventArgs"/> instance containing the event data.</param>
        private void txtAmountEntered_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = !IsTextAllowed(e.Text);


            //if (TxtMobileNum.Text.Contains("-"))
            //{
            //    e.Handled = false;
            //}
            //else
            //{
            //    e.Handled = true;
            //}
            //int currentSelectedIndex = cmbCountryCode.SelectedIndex;
            //if (currentSelectedIndex > 0)
            //{
            //    string currentSelectedValue = cmbCountryCode.SelectedValue.ToString();
            //    int pFrom = currentSelectedValue.IndexOf("+") + 1;
            //    int pTo = currentSelectedValue.LastIndexOf(")");

            //    string result = currentSelectedValue.Substring(pFrom, pTo - pFrom);
            //    if (TxtMobileNum.Text.Contains(result + "-"))
            //    {
            //        e.Handled = false;
            //    }
            //    else
            //    {
            //        e.Handled = true;
            //    }

            //    //TxtMobileNum.Text = result + "-";
            //}

        }
        /// <summary>
        /// Loads the currency.
        /// </summary>
        private void LoadCurrency()
        {
            //DigiPhotoDataServices _objDataLayer = new DigiPhotoDataServices();
            //lstCurrency.ItemsSource = new CurrencyBusiness().GetCurrencyOnly();

        }

        /// <summary>
        /// Handles the Click event of the btnClose control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void btnClose_Click(object sender, RoutedEventArgs e)
        {
            _result = null;
            HideHandlerDialog();
        }

        /// <summary>
        /// Handles the Loaded event of the UserControl control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            TxtMobileNum.Focusable = true;
            TxtMobileNum.Focus();
            FocusManager.SetFocusedElement(this, TxtMobileNum);
            Keyboard.Focus(TxtMobileNum);
            TxtMobileNum.Text = String.Empty;
        }



        /// <summary>
        /// Handles the GotKeyboardFocus event of the TxtMobileNum control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void TxtAmount_GotKeyboardFocus(object sender, RoutedEventArgs e)
        {

        }


        private void Window_ContentRendered()
        {
            BackgroundWorker worker = new BackgroundWorker();
            worker.WorkerReportsProgress = true;
            worker.DoWork += worker_DoWork;
            //worker.ProgressChanged += worker_ProgressChanged;

            worker.RunWorkerAsync();
        }

        void worker_DoWork(object sender, DoWorkEventArgs e)
        {
            //for (int i = 0; i < 100; i++)
            //{
            //    (sender as BackgroundWorker).ReportProgress(i);
            //    Thread.Sleep(1000);
            //}

            while (ProgressBarMobile.Value < 100)
            {
                Thread.Sleep(1050);

                ProgressBarMobile.Value = ProgressBarMobile.Value + 10;


            }
        }

        void worker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            for (int i = 0; i < 100; i++)
            {
                ProgressBarMobile.Value++;
                Thread.Sleep(100);
            }
        }

        private void ProgressBarMobile_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {


        }


        private void FillCountryList()
        {
            try
            {
                lstPrinterList = new Dictionary<string, string>();
                WhatsAppBusiness obj = new WhatsAppBusiness();

                foreach (String item in obj.getGlobalCountryList())
                {
                    lstPrinterList.Add(item.ToString(), item.ToString());
                }
                cmbCountryCode.ItemsSource = lstPrinterList;
                cmbCountryCode.SelectedValue = "--Select Country--";
                int countryID = obj.getDefaultGlobalCountry();
                if(countryID==0)
                {
                    countryID = 95;//default India
                }
                cmbCountryCode.SelectedIndex = countryID;

            }
            catch (Exception ec)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ec);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }

        private void cmbReceiptPrinter_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                int currentSelectedIndex = cmbCountryCode.SelectedIndex;
                if (currentSelectedIndex > 0)
                {
                    string currentSelectedValue = cmbCountryCode.SelectedValue.ToString();
                    int pFrom = currentSelectedValue.IndexOf("+") + 1;
                    int pTo = currentSelectedValue.LastIndexOf(")");

                    string result = currentSelectedValue.Substring(pFrom, pTo - pFrom);
                    TxtMobileNum1.Text = result + "-";
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

            //TxtMobileNum.Text= currentSelectedValue.Substring()


        }

        private void TxtMobileNum_TextChanged(object sender, TextChangedEventArgs e)
        {

        }

        private void TxtMobileNum_KeyDown(object sender, KeyEventArgs e)
        {

        }

        private void TxtMobileNum_KeyUp(object sender, KeyEventArgs e)
        {

        }

        private void TxtAmount_GotKeyboardFocus(object sender, KeyboardFocusChangedEventArgs e)
        {

        }
    }

}
