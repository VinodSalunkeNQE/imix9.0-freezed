﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using System.Threading;
using Xceed.Wpf.Toolkit;
using DigiPhoto.DataLayer;
using DigiPhoto.DataSync.Controller;
using DigiPhoto.DigiSync.Model;
using DigiPhoto.DigiSync.ServiceLibrary.Interface;
using DigiPhoto.Common;
using System.IO;
using System.Reflection;
using DigiPhoto.IMIX.Business;
using DigiPhoto.IMIX.Model;
using System.Text.RegularExpressions;

namespace DigiPhoto
{
    /// <summary>
    /// Interaction logic for SelectedImagesQrCode.xaml
    /// </summary>
    public partial class SelectedImagesQrCode : UserControl
    {
        //DigiPhotoDataServices _objDataServices = null;
        public static Int32 PackageCode;
        /// <summary>
        /// Initializes a new instance of the <see cref="SelectedImageslist"/> class.
        /// </summary>
        public SelectedImagesQrCode()
        {
            // _objDataServices = new DigiPhotoDataServices();
            string ImgId = string.Empty;
            try
            {
                InitializeComponent();
                lstSelectedImage.Items.Clear();
                foreach (var item in RobotImageLoader.PrintImages)
                {
                    ImgId = ImgId + item.PhotoId + ",";
                    lstSelectedImage.Items.Add(item);
                }


                //List<string> objCode = _objDataServices.GetQRCodeBYImages(ImgId).ToList();
                List<string> objCode = (new PhotoBusiness()).GetQRCodeBYImages(ImgId).ToList();
                if (objCode.Count == 1)
                {
                    txtQRCode.Text = objCode[0].ToString();
                }

                if (string.IsNullOrEmpty(App.QRCodeWebUrl))
                {
                    //string webURL = _objDataServices.GetQRCodeWebUrl();
                    string webURL = (new StoreSubStoreDataBusniess()).GetQRCodeWebUrl();
                    App.QRCodeWebUrl = string.IsNullOrEmpty(webURL) ? " " : webURL;
                }
            }
            catch (Exception ex)
            {

            }
        }

        public bool IsQrCodeUsed = false;

        /// <summary>
        /// Have package info
        /// </summary>
        public PackageInfo packageInfo = new PackageInfo();

        /// <summary>
        /// Gets or sets the _ selected image.
        /// </summary>
        /// <value>
        /// The _ selected image.
        /// </value>
        public List<LstMyItems> _SelectedImage
        {
            get;
            set;
        }
        public List<LstMyItems> _lstSelectedImage
        {
            get;
            set;
        }
        /// <summary>
        /// Gets or sets the previous image.
        /// </summary>
        /// <value>
        /// The previous image.
        /// </value>
        public List<String> PreviousImage
        {
            get;
            set;
        }
        /// <summary>
        /// Gets or sets the maximum selected photos.
        /// </summary>
        /// <value>
        /// The maximum selected photos.
        /// </value>
        public int maxSelectedPhotos
        {
            get;
            set;
        }
        /// <summary>
        /// Gets or sets a value indicating whether [is bundled].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [is bundled]; otherwise, <c>false</c>.
        /// </value>
        public Boolean IsBundled
        {
            get;
            set;
        }
        /// <summary>
        /// Gets or sets a value indicating whether [is package].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [is package]; otherwise, <c>false</c>.
        /// </value>
        public Boolean IsPackage
        {
            get;
            set;
        }
        /// <summary>
        /// Gets or sets the product type unique identifier.
        /// </summary>
        /// <value>
        /// The product type unique identifier.
        /// </value>
        public int ProductTypeID
        {
            get;
            set;
        }

        /// <summary>
        /// The _hide request
        /// </summary>
        private bool _hideRequest = false;
        /// <summary>
        /// The _result
        /// </summary>
        private List<LstMyItems> _result;
        /// <summary>
        /// The _parent
        /// </summary>
        private UIElement _parent;

        /// <summary>
        /// Association Code
        /// </summary>
        public string QRCode
        {
            get;
            set;
        }

        public bool ValidateQRCodeChk
        {
            get;
            set;
        }

        //To check if duplicate code is being entered.
        public List<string> lstQrCode = new List<string>();
        //----------------------------Hari -----------------ForQRcode
        public static string QRCodeValue
        {
            get;
            set;
        }
        //--------------------------------End-----------------------Hari
        /// <summary>
        /// Sets the parent.
        /// </summary>
        /// <param name="parent">The parent.</param>
        public void SetParentQrCode(UIElement parent)
        {
            _parent = (UIElement)parent;

            //check for img availability
            if (lstSelectedImage.Items.Count > 0)
                SPSelectAll.Visibility = Visibility.Visible;
            else
                SPSelectAll.Visibility = Visibility.Hidden;
            //CheckSelectedImg();
        }
        #region Message

        /// <summary>
        /// Gets or sets the message2.
        /// </summary>
        /// <value>
        /// The message2.
        /// </value>
        public string MessageQrCode2
        {
            get { return (string)GetValue(MessageQrCode2Property); }
            set { SetValue(MessageQrCode2Property, value); }
        }

        // Using a DependencyProperty as the backing store for Message.
        // This enables animation, styling, binding, etc...
        /// <summary>
        /// The message2 property
        /// </summary>
        public static readonly DependencyProperty MessageQrCode2Property =
            DependencyProperty.Register(
                "MessageQrCode2", typeof(string), typeof(ModalDialog), new UIPropertyMetadata(string.Empty));

        #endregion


        #region Code added by VINS 17Mar2020

       
        /// <summary>
        /// Code added by VINS 17Mar2020
        /// </summary>
        public string _QRCode = null;
        Int32 _QRCodeLenth = 8;
        private Int32 QRCodeLength(Int32 subStoreId)
        {
            List<long> filterValues = new List<long>();
            filterValues.Add((Int32)ConfigParams.QRCodeLengthSetting);
            ConfigBusiness conBiz = new ConfigBusiness();
            iMIXConfigurationInfo ConfigValuesList = conBiz.GetNewConfigValues(subStoreId).Where(o => o.IMIXConfigurationMasterId == (Int32)ConfigParams.QRCodeLengthSetting).FirstOrDefault();
            if (ConfigValuesList != null)
                _QRCodeLenth = ConfigValuesList.ConfigurationValue != null ? Convert.ToInt32(ConfigValuesList.ConfigurationValue) : 0;
            return _QRCodeLenth;
        }

        #endregion

        /// <summary>
        /// Shows the handler dialog.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <returns></returns>
        public List<LstMyItems> ShowQrCodeHandlerDialog(string message)
        {
            //txtQRCode.Focus();

            MessageQrCode2 = message;
            Visibility = Visibility.Visible;
            IsQrCodeUsed = false;
            txtQRCode.Text = QRCode;

            //Added by VINS_13Mar2020 for Unique(4*6)+QR product as per Pete, no need to input QR code manually for this product

            DigiPhoto.Orders.PlaceOrder obj = (DigiPhoto.Orders.PlaceOrder)_parent;
            var pid = from i in obj._objLineItem
                      where i.SelectedProductType_ID == 124
                      select i.SelectedProductType_ID;
            if (pid != null && pid.Count() > 0)
            {
                //_QRCodeLenth = QRCodeLength(LoginUser.SubStoreId);
                //txtQRCode.Text = GenerateQRCode.GetNextQRCode(_QRCodeLenth); 

                string ImgId = string.Empty;
                //txtQRCode.Text = "hello123";
                lstSelectedImage.Items.Clear();
                foreach (var item in RobotImageLoader.PrintImages)
                {
                    ImgId = ImgId + item.PhotoId + ",";
                    lstSelectedImage.Items.Add(item);
                }

                //List<string> objCode = _objDataServices.GetQRCodeBYImages(ImgId).ToList();
                List<string> objCode = (new PhotoBusiness()).GetQRCodeBYImages(ImgId).ToList();
                if (objCode.Count >= 1)
                {
                    txtQRCode.Text = objCode[0].ToString();
                }
            }
            else
            {
                //Set Keyboard focus
                Dispatcher.BeginInvoke(DispatcherPriority.Input,
        new Action(delegate ()
        {
            txtQRCode.Focus();         // Set Logical Focus
        Keyboard.Focus(txtQRCode); // Set Keyboard Focus
    }));
            }
            LoadCheckBox();
            _parent.IsEnabled = true;
            /* -----------------  For Removing DropDownList------------------------
            foreach (LstMyItems lt in RobotImageLoader.PrintImages)
            {
                lt.MaxCount = maxSelectedPhotos;
                if (!IsPackage || (ProductTypeID == 35 || ProductTypeID == 36 || ProductTypeID == 4 || ProductTypeID == 78))
                {
                    lt.ToShownoCopy = false;
                }
                else
                {
                    lt.ToShownoCopy = true;
                }
            }
             *-----------------  For Removing DropDownList------------------------ */

            //SetInitial();
            CheckSelectedImg();
            // CheckSelectedImg();

            _hideRequest = false;
            while (!_hideRequest)
            {
                // HACK: Stop the thread if the application is about to close
                if (this.Dispatcher.HasShutdownStarted ||
                    this.Dispatcher.HasShutdownFinished)
                {
                    break;
                }
                // HACK: Simulate "DoEvents"
                this.Dispatcher.Invoke(
                    DispatcherPriority.Background,
                    new ThreadStart(delegate { }));
                Thread.Sleep(20);

            }

            return _result;
        }

        private void LoadCheckBox() //New method for Gaylord issue of dul=licate images lis while acrylic and spec prod order placing in imix
        {
            lstSelectedImage.Items.Clear();

            //_lstSelectedImage = _lstSelectedImage.Select(o => o.).Distinct().ToList();
            var distinctImages = (from d in _lstSelectedImage select d).Distinct();

            //foreach (var item in _lstSelectedImage)
            foreach (var item in distinctImages)
            {
                if (PreviousImage != null)
                {
                    var phtoId = item.PhotoId.ToString();
                    if (PreviousImage.Contains(phtoId))
                    {
                        item.IsItemSelected = true;
                        // Get the image count by imageid and set the count in listbox default selection 
                        item.updownCount = PreviousImage.Where(o => o == phtoId).Count();
                    }
                    else
                    {
                        item.IsItemSelected = false;
                        item.updownCount = 1;
                    }
                }
                else
                {
                    item.IsItemSelected = false;
                    item.updownCount = 1;
                }
                if (!lstSelectedImage.Items.Contains(item))
                {
                    lstSelectedImage.Items.Add(item);
                }

            }
        }

        private void LoadCheckBox_Old() //renamed by Vins for resolving an issue of multiple duplicate images for Acrylic product and spec print_ 20 Dec 2019
        {
            lstSelectedImage.Items.Clear();

            foreach (var item in _lstSelectedImage)
            {
                if (PreviousImage != null)
                {
                    var phtoId = item.PhotoId.ToString();
                    if (PreviousImage.Contains(phtoId))
                    {
                        item.IsItemSelected = true;
                        // Get the image count by imageid and set the count in listbox default selection 
                        item.updownCount = PreviousImage.Where(o => o == phtoId).Count();

                    }
                    else
                    {
                        item.IsItemSelected = false;
                        item.updownCount = 1;
                    }
                }
                else
                {
                    item.IsItemSelected = false;
                    item.updownCount = 1;
                }
                lstSelectedImage.Items.Add(item);
            }
        }
        /// <summary>
        /// Hides the handler dialog.
        /// </summary>
        private void HideHandlerDialog()
        {
            _hideRequest = true;
            Visibility = Visibility.Collapsed;
            _parent.IsEnabled = true;
            txtQRCode.Text = QRCode;
            KeyBorder1.Visibility = Visibility.Collapsed;

        }
        /// <summary>
        /// Gets the printed image.
        /// </summary>
        public void GetPrintedImage()
        {

            lstSelectedImage.ItemsSource = _SelectedImage;
        }

        /// <summary>
        /// Handles the Loaded event of the UserControl control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {


        }



        /// <summary>
        /// Finds the visual child.
        /// </summary>
        /// <typeparam name="childItem">The type of the hild item.</typeparam>
        /// <param name="obj">The object.</param>
        /// <returns></returns>
        public static childItem FindVisualChild<childItem>(DependencyObject obj) where childItem : DependencyObject
        {
            // Search immediate children
            for (int i = 0; i < VisualTreeHelper.GetChildrenCount(obj); i++)
            {
                DependencyObject child = VisualTreeHelper.GetChild(obj, i);

                if (child is childItem)
                    return (childItem)child;

                else
                {
                    childItem childOfChild = FindVisualChild<childItem>(child);
                    if (childOfChild != null)
                        return childOfChild;
                }
            }
            return null;
        }
        /// <summary>
        /// Sets the initial.
        /// </summary>
        public void SetInitial()
        {
            for (int i = 0; i < lstSelectedImage.Items.Count; i++)
            {
                DependencyObject obj = lstSelectedImage.ItemContainerGenerator.ContainerFromIndex(i);
                if (obj != null)
                {
                    CheckBox chk = FindVisualChild<CheckBox>(obj);// FindByName("rdoSelect", row) as RadioButton;
                    if (chk != null)
                    {
                        chk.IsChecked = false;
                    }
                }
            }

            for (int i = 0; i < lstSelectedImage.Items.Count; i++)
            {
                DependencyObject obj = lstSelectedImage.ItemContainerGenerator.ContainerFromIndex(i);
                if (obj != null)
                {
                    CheckBox chk = FindVisualChild<CheckBox>(obj);// FindByName("rdoSelect", row) as RadioButton;
                    if (chk != null)
                    {
                        if (PreviousImage != null)
                        {
                            var item = (from objSelected in PreviousImage
                                        where objSelected == chk.Uid.ToString()
                                        select objSelected).FirstOrDefault();

                            if (item != null)
                            {
                                chk.IsChecked = true;
                            }
                        }
                    }
                }
            }
        }




        /// <summary>
        /// Handles the Click event of the btnSubmit control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void btnSubmit_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                setQRCode();////created by latika to handle url link of QRCode
                string Code = string.Empty;
                CardLimitInfo objCardLimit = new CardLimitInfo();
                bool IsOnline = false;
                //_objDataServices = new DigiPhotoDataServices();
                int TotalSelectedImages = 0;
                int CardImageLimit = 0;
                int CardImageSold = 0;
                _result = new List<LstMyItems>();

                for (int i = 0; i < lstSelectedImage.Items.Count; i++)
                {
                    DependencyObject obj = lstSelectedImage.ItemContainerGenerator.ContainerFromIndex(i);
                    if (obj != null)
                    {
                        CheckBox rdo = FindVisualChild<CheckBox>(obj);// FindByName("rdoSelect", row) as RadioButton;
                        if (rdo != null)
                        {
                            if (Convert.ToBoolean(rdo.IsChecked))
                            {
                                IntegerUpDown updown = FindVisualChild<IntegerUpDown>(obj);
                                int totalval = (int)updown.Value;
                                TotalSelectedImages = TotalSelectedImages + totalval;
                                int counter = 0;
                                while (counter < totalval)
                                {
                                    _result.Add(((LstMyItems)lstSelectedImage.Items[i]));
                                    counter++;
                                }
                            }
                        }
                    }
                }

                if (_result.Count > 0)
                {
                    if (!CheckQRCodeLength())
                        return;

                    if (string.IsNullOrEmpty(txtQRCode.Text.Trim()) || txtQRCode.Text.Trim().Length < 5)
                    {
                        System.Windows.MessageBox.Show("Invalid QR/Bar code.", "Digiphoto");
                        return;
                    }
                    else
                    {
                        if (lstQrCode.Where(o => string.Compare(o, txtQRCode.Text.Trim(), true) == 0 && string.Compare(o, QRCode, true) != 0).Count() > 0)
                        {
                            System.Windows.MessageBox.Show("You have already used this code.", "Digiphoto");
                            return;
                        }
                        Code = txtQRCode.Text.Trim();
                        //------------------------------Hari---------------ForQRcode
                        QRCodeValue = txtQRCode.Text.Trim();
                    }

                    if (App.IsAnonymousQrCodeEnabled == false)
                    {
                        //check for valid card type
                        //if (!_objDataServices.IsValidCodeType(Code, 405))
                        if (!(new CardBusiness()).IsValidCodeType(Code, 405))
                        {
                            System.Windows.MessageBox.Show("Invalid code.");
                            return;
                        }
                        //Start check for max limit od card images
                        //if (_objDataServices.IsOnlineOpen(Convert.ToInt32(ConfigParams.Online), LoginUser.SubStoreId))
                        if (ReadSystemOnlineStatus())
                        {
                            try
                            {                               
                                ServiceProxy<IDataSyncService>.Use(client =>
                                {
                                    objCardLimit = client.CheckCardLimit(Code);
                                });
                                IsOnline = true;                                
                            }
                            catch (Exception ex)
                            {
                                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                                IsOnline = false;
                            }
                        }

                        if (IsOnline)
                        {
                            if (objCardLimit.ValidCard)
                            {
                                //CardImageLimit = objCardLimit.Allowed;
                                //CardImageSold = objCardLimit.Associated;
                                CardImageLimit = (new CardBusiness()).GetCardImageLimit(Code);
                                if (!ValidateQRCodeChk)
                                    CardImageSold = (new CardBusiness()).GetCardImageSold(Code);
                            }
                            else
                            {
                                System.Windows.MessageBox.Show("Invalid card code.", "Digiphoto");
                                return;
                            }
                        }
                        else
                        {
                            //CardImageLimit = _objDataServices.GetCardImageLimit(Code);
                            //CardImageSold = _objDataServices.GetCardImageSold(Code);
                            CardImageLimit = (new CardBusiness()).GetCardImageLimit(Code);
                            if (!ValidateQRCodeChk)
                                CardImageSold = (new CardBusiness()).GetCardImageSold(Code);
                        }
                    }
                    else
                    {
                        //Start check for max limit od card images
                        if (ReadSystemOnlineStatus())
                        {
                            this.packageInfo.MaxImgQuantity = maxSelectedPhotos;
                            try
                            {
                                ErrorHandler.ErrorHandler.LogError("Line Number : 644");
                                ServiceProxy<IDataSyncService>.Use(client =>
                                {
                                    objCardLimit = client.CheckAnonymousCardLimit(Code, this.packageInfo);
                                });
                                ErrorHandler.ErrorHandler.LogError("Line Number : 644");
                                //if allowed=0 mean package not exists on server, if Associated=0 QRcode not exists.
                                if (objCardLimit.Allowed > 0 && objCardLimit.Associated > 0)
                                    IsOnline = true;
                                else
                                    IsOnline = false;
                            }
                            catch (Exception ex)
                            {
                                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                                IsOnline = false;
                            }
                        }

                        if (IsOnline)
                        {
                            if (objCardLimit.ValidCard)
                            {
                                CardImageLimit = objCardLimit.Allowed;
                                if (!ValidateQRCodeChk)
                                    CardImageSold = objCardLimit.Associated;
                            }
                            else
                            {
                                System.Windows.MessageBox.Show("Invalid card code.", "Digiphoto");
                                return;
                            }
                        }
                        else
                        {
                            CardImageLimit = this.maxSelectedPhotos;
                            //CardImageSold = _objDataServices.GetCardImageSold(Code);
                            if (!ValidateQRCodeChk)
                                CardImageSold = (new CardBusiness()).GetCardImageSold(Code);
                        }
                    }
                    //Set flag to set the package price.
                    if (CardImageSold > 0)
                        IsQrCodeUsed = true;
                    else
                        IsQrCodeUsed = false;

                    if (CardImageSold + _result.Count > CardImageLimit)
                    {
                        int rest = CardImageLimit - CardImageSold;
                        rest = rest < 0 ? 0 : rest;
                        //By KCB on 30 JUL 2018 
                        // System.Windows.MessageBox.Show("This card " + Code + " exceed limit of " + CardImageLimit.ToString() + " images. Please select less than Equal to" + rest.ToString() + " images.", "Digiphoto");
                        System.Windows.MessageBox.Show("This card " + Code + " exceed limit of " + CardImageLimit.ToString() + " images. Please select less than Or equal to " + rest.ToString() + " images.", "Digiphoto");
                        //End
                        return;
                    }
                }
                else
                {
                    if (!new OrderBusiness().chKIsWaterMarkedOrNot(PackageCode))
                    {
                        txtQRCode.Text = string.Empty;
                        IsQrCodeUsed = false;
                    }
                }

                if (IsPackage)
                {
                    if (_result.Count > maxSelectedPhotos)
                    {
                        /*
                        if (System.Windows.MessageBox.Show("You have selected more than " + maxSelectedPhotos + " images, do you want to continue ?", "Digiphoto", MessageBoxButton.YesNo, MessageBoxImage.Warning) == MessageBoxResult.Yes)
                        {
                            HideHandlerDialog();
                        }
                        */
                        System.Windows.MessageBox.Show("You have selected more than " + maxSelectedPhotos + " images. Please select less than " + maxSelectedPhotos + " images.", "Digiphoto");
                        return;
                    }
                    else
                    {
                        QRCode = txtQRCode.Text.Trim();
                        HideHandlerDialog();
                    }
                }
                else
                {
                    QRCode = txtQRCode.Text.Trim();
                    HideHandlerDialog();
                }

            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }

        /// <summary>
        /// Handles the Click event of the btnSubmitCancel control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void btnSubmitCancel_Click(object sender, RoutedEventArgs e)
        {

            SelectDeselectAll(false);

        }


        /// <summary>
        /// Handles the Click event of the btnClose control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void btnClose_Click(object sender, RoutedEventArgs e)
        {
            _result = null;

            HideHandlerDialog();

        }

        /// <summary>
        /// Handles the Checked event of the ChkSelected control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void ChkSelected_Click(object sender, RoutedEventArgs e)
        {
            CheckSelectedImg();
        }

        private void chkSelectAll_Click(object sender, RoutedEventArgs e)
        {
            SelectDeselectAll(chkSelectAll.IsChecked);
        }


        private void SelectDeselectAll(bool? SelectDeselect)
        {
            for (int i = 0; i < lstSelectedImage.Items.Count; i++)
            {
                DependencyObject obj = lstSelectedImage.ItemContainerGenerator.ContainerFromIndex(i);
                if (obj != null)
                {
                    CheckBox chk = FindVisualChild<CheckBox>(obj);// FindByName("rdoSelect", row) as RadioButton;
                    if (chk != null)
                    {
                        chk.IsChecked = SelectDeselect;
                    }


                }
            }
            if (SelectDeselect == false)
            {
                txbImages.Text = "Selected : " + "0" + "/" + lstSelectedImage.Items.Count;
            }
            else
            {

                txbImages.Text = "Selected : " + lstSelectedImage.Items.Count + "/" + lstSelectedImage.Items.Count;
            }


        }

        /// <summary>
        /// Counts the selected images
        /// </summary>
        /// <returns></returns>

        private int SelectedItemCount()
        {
            int selectedCount = 0;
            try
            {
                //for (int i = 0; i < lstSelectedImage.Items.Count; i++)
                //{
                //    DependencyObject obj = lstSelectedImage.ItemContainerGenerator.ContainerFromIndex(i);
                //    if (obj != null)
                //    {

                //        CheckBox chk = FindVisualChild<CheckBox>(obj);// FindByName("rdoSelect", row) as RadioButton;
                //        if (chk != null && chk.IsChecked == true)
                //        {

                //            selectedCount++;
                //        }


                //    }
                //}
                selectedCount = lstSelectedImage.Items.Cast<LstMyItems>().Where(o => o.IsItemSelected).Count();
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
            return selectedCount;
        }

        public void CheckSelectedImg()
        {
            if (lstSelectedImage.Items.Count > 0)
            {
                SPSelectAll.Visibility = Visibility.Visible;
                if (lstSelectedImage.Items.Count == SelectedItemCount())
                    chkSelectAll.IsChecked = true;
                else
                    chkSelectAll.IsChecked = false;
            }
            else
            {
                SPSelectAll.Visibility = Visibility.Hidden;
            }
            txbImages.Text = "Selected : " + SelectedItemCount() + "/" + lstSelectedImage.Items.Count.ToString();
        }

        private void txtCode_GotFocus(object sender, RoutedEventArgs e)
        {
            KeyBorder1.Visibility = Visibility.Visible;
        }

        private void btn_Click_keyboard(object sender, RoutedEventArgs e)
        {
            try
            {

                Button _objbtn = new Button();
                _objbtn = (Button)sender;
                switch (_objbtn.Content.ToString())
                {
                    case "ENTER":
                        {
                            KeyBorder1.Visibility = Visibility.Collapsed;
                            break;
                        }
                    case "SPACE":
                        {
                            txtQRCode.Text = txtQRCode.Text + " ";
                            break;
                        }
                    case "CLOSE":
                        {
                            KeyBorder1.Visibility = Visibility.Collapsed;
                            break;
                        }
                    case "Back":
                        {
                            if (txtQRCode.Text.Length > 0)
                                txtQRCode.Text = txtQRCode.Text.Remove(txtQRCode.Text.Length - 1, 1);
                            break;
                        }
                    default:
                        {
                            txtQRCode.Text = txtQRCode.Text + _objbtn.Content;
                        }
                        break;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }

        private void txtQRCode_KeyDown(object sender, KeyEventArgs e)
        {

            if (e.Key == Key.Enter)
            {
                setQRCode();////////created by latika remove link for QR Code 12-Mar-19
                CheckQRCodeLength();
            }
        }

        /// <summary>
        /// ////////created by latika remove link for QR Code 12-Mar-19
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtQRCode_MouseLeave(object sender, MouseEventArgs e)
        {
            setQRCode();
        }
        private void txtQRCode_LostKeyboardFocus(object sender, KeyboardFocusChangedEventArgs e)
        {
            setQRCode();
        }
        public void setQRCode()
        {
            //ErrorHandler.ErrorHandler.LogError("Line NUmber : 940 : txt SETQRCode Called : " + txtQRCode.Text);
            string[] strTag = txtQRCode.Text.Split('=');
            //ErrorHandler.ErrorHandler.LogError("Line NUmber : 942 : txt SETQRCode Called : " + txtQRCode.Text);
            string tagid = strTag[strTag.Length - 1];
            //ErrorHandler.ErrorHandler.LogError("Line NUmber : 944 : txt SETQRCode Called : " + tagid);
            txtQRCode.Text = tagid.Trim();
            //ErrorHandler.ErrorHandler.LogError("Line NUmber : 947 : txt SETQRCode Called : " + txtQRCode.Text);
        }

        /////end

        private void lstSelectedImage_GotFocus(object sender, RoutedEventArgs e)
        {
            KeyBorder1.Visibility = Visibility.Collapsed;
        }
        private void txtQRCode_TextChanged(object sender, TextChangedEventArgs e)
        {
            //ErrorHandler.ErrorHandler.LogError("Line NUmber : 954 : txt Change event Called : "+txtQRCode.Text);
            txtQRCode.Text = RemoveSpecialCharacters(txtQRCode.Text);///Added by Priyanka to prevent special characters in textbox
            //ErrorHandler.ErrorHandler.LogError("Line NUmber : 956 : txt Change event Called : " + txtQRCode.Text);
            setQRCode();
            //ErrorHandler.ErrorHandler.LogError("Line NUmber : 958 : txt Change event Called : " + txtQRCode.Text);
        }
        /////end
        //////Added by Priyanka to prevent special characters in textbox
        private string RemoveSpecialCharacters(String inString)
        {
            String tmp = inString;
            foreach (char c in inString.ToCharArray())
            {
                if (!IsCharDigit(c))
                {
                    tmp = tmp.Replace(c.ToString(), "");
                }
            }
            return tmp;
        }

        public bool IsCharDigit(char c)
        {
            return (c >= '0' && c <= '9' || c >= 'a' && c <= 'z' || c >= 'A' && c <= 'Z' || c == '=');
        }
        ///End:Added by Priyanka to prevent special characters in textbox
        private bool ReadSystemOnlineStatus()
        {
            bool status = false;
            try
            {
                string pathtosave = System.IO.Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
                if (File.Exists(pathtosave + "\\Config.dat"))
                {
                    string OnlineStatus;
                    using (StreamReader reader = new StreamReader(pathtosave + "\\Config.dat"))
                    {
                        OnlineStatus = reader.ReadLine();
                        status = Convert.ToBoolean(OnlineStatus.Split(',')[1]);
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
            return status;
        }

        private bool CheckQRCodeLength()
        {
            List<long> filterValues = new List<long>();
            Int32 QRCodeLen = 0;
            txtQRCode.Text = txtQRCode.Text.Replace(App.QRCodeWebUrl, string.Empty);
            filterValues.Add((Int32)ConfigParams.QRCodeLengthSetting);
            filterValues.Add((Int32)ConfigParams.ValidateQRCode);
            ConfigBusiness conBiz = new ConfigBusiness();
            List<iMIXConfigurationInfo> ConfigValuesList = conBiz.GetNewConfigValues(LoginUser.SubStoreId).Where(o => filterValues.Contains(o.IMIXConfigurationMasterId)).ToList();
            if (ConfigValuesList != null || ConfigValuesList.Count > 0)
            {
                for (int i = 0; i < ConfigValuesList.Count; i++)
                {
                    switch (ConfigValuesList[i].IMIXConfigurationMasterId)
                    {
                        case (int)ConfigParams.QRCodeLengthSetting:
                            QRCodeLen = ConfigValuesList[i].ConfigurationValue != null ? Convert.ToInt32(ConfigValuesList[i].ConfigurationValue) : 0;
                            break;
                        case (int)ConfigParams.ValidateQRCode:
                            ValidateQRCodeChk = ConfigValuesList[i].ConfigurationValue != null ? Convert.ToBoolean(ConfigValuesList[i].ConfigurationValue) : false;
                            break;
                    }
                }
            }

            if (QRCodeLen == 0)
            {
                System.Windows.MessageBox.Show("Please Configure QRCode Length in Manage Configuration", "Digiphoto");
                return false;
            }

            if ((QRCodeLen > 0) && (txtQRCode.Text.Trim().Length != QRCodeLen))
            {
                //System.Windows.MessageBox.Show("Invalid QR/Bar code Length.", "Digiphoto");
                System.Windows.MessageBox.Show("QR/Bar code length should be " + QRCodeLen + " characters.", "Digiphoto");
                return false;
            }
            return true;
        }

        private void TxtQRCode_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            //ErrorHandler.ErrorHandler.LogError("Line NUmber : 958 : txt Preview Text event Called : " + txtQRCode.Text);
            e.Handled = !IsTextAllowed(e.Text);
        }
        /// <summary>
        /// Added by Priyanka for Qr code validation on 15 july 2019
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        private static bool IsTextAllowed(string text)
        {
            Regex regex = new Regex("[^a-zA-Z0-9=]");
            //ErrorHandler.ErrorHandler.LogError("Line NUmber : 958 : Is Text Allowed Called : " + text);
            return !regex.IsMatch(text);
        }
    }
}
