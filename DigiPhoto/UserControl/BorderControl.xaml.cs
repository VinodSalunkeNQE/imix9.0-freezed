﻿using DigiPhoto.Common;
using DigiPhoto.DataLayer;
using DigiPhoto.IMIX.Model;
using DigiPhoto.Interop;
using DigiPhoto.Manage;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
//using VisioForge.Controls.WPF;

namespace DigiPhoto
{
    /// <summary>
    /// Interaction logic for PanControl.xaml
    /// </summary>
    public partial class BorderControl : UserControl
    {
        private UIElement _parent;
        private bool _result = false;
        public List<SlotProperty> borderslotlist = new List<SlotProperty>();
        int uniqueID = 151;
        public List<VideoTemplateInfo.VideoSlot> slotList;
        public int videoExpLen = 0;
        List<TemplateListItems> BordersList = new List<TemplateListItems>();
        bool IsBorderEdit = false;
        long EditBorderID = 0;
        public BorderControl()
        {
            InitializeComponent();
            Visibility = Visibility.Hidden;
            cmbBorders.SelectedIndex = 0;
        }

        public void SetParent(UIElement parent)
        {
            _parent = (UIElement)parent;
        }
        private void btnClose_Click(object sender, RoutedEventArgs e)
        {
            HideHandlerDialog();
            OnExecuteMethod();
        }
        private void HideHandlerDialog()
        {
            Visibility = Visibility.Collapsed;
            _parent.IsEnabled = true;
        }
        public void ControlListAutoFill()
        {
            DGManageBorders.ItemsSource = borderslotlist;
            DGManageBorders.Items.Refresh();
        }
        public bool ShowHandlerDialog()
        {
            Visibility = Visibility.Visible;
            _parent.IsEnabled = false;
            BordersList = ((VideoEditor)Window.GetWindow(_parent)).objTemplateList.Where(o => o.MediaType == 608).ToList();
            cmbBorders.ItemsSource = BordersList;
            if (slotList != null && slotList.Count > 0)
            {
                btnDefaultBorder.Visibility = Visibility.Visible;
            }
            else
            {
                btnDefaultBorder.Visibility = Visibility.Collapsed;
            }
            txtStopTime.Text = videoExpLen.ToString();
            if (videoExpLen > 0)
            {
                sldRange.Maximum = videoExpLen;
            }
            return _result;
        }

        public event EventHandler ExecuteMethod;
        protected virtual void OnExecuteMethod()
        {
            if (ExecuteMethod != null) ExecuteMethod(this, EventArgs.Empty);
        }

        private void btnDeleteSlots_Click(object sender, RoutedEventArgs e)
        {
            Button btnSender = (Button)sender;
            long SlotId = long.Parse(btnSender.Tag.ToString());
            MessageBoxResult response = MessageBox.Show("Do you want to delete record?", "Digiphoto", MessageBoxButton.YesNo, MessageBoxImage.Question);
            if (response == MessageBoxResult.Yes)
            {
                SlotProperty brdlst = borderslotlist.Where(o => o.ID == SlotId).FirstOrDefault();
                long borderid = (borderslotlist.Where(o => o.ID == SlotId).FirstOrDefault()).ItemID;
                borderslotlist.RemoveAll(o => o.ID == SlotId);
                if (borderslotlist.Where(o => o.ItemID == borderid).Count() == 0)
                {
                    TemplateListItems tmp = ((VideoEditor)Window.GetWindow(_parent)).objTemplateList.Where(o => o.MediaType == 608 && o.Item_ID == borderid).FirstOrDefault();
                    tmp.IsChecked = false;
                }
                //  MessageBox.Show("Item deleted Successfully", "DigiPhoto", MessageBoxButton.OK, MessageBoxImage.Information);
                DGManageBorders.ItemsSource = borderslotlist;
                DGManageBorders.Items.Refresh();
                if (borderslotlist.Count == 0)
                {
                    uniqueID = 151;
                }
            }
        }

        private void txtnumber_TextChanged(object sender, TextChangedEventArgs e)
        {
            string strtemp = string.Empty;
            if (string.IsNullOrEmpty(((TextBox)sender).Text))
                strtemp = "";
            else
            {
                int num = 0;
                bool success = int.TryParse(((TextBox)sender).Text, out num);
                if (success & num >= 0)
                {
                    ((TextBox)sender).Text.Trim();
                    strtemp = ((TextBox)sender).Text;
                }
                else
                {
                    ((TextBox)sender).Text = strtemp;
                    ((TextBox)sender).SelectionStart = ((TextBox)sender).Text.Length;
                }
            }
        }
        private bool CheckSlotValidations()
        {
            if (string.IsNullOrEmpty(txtStartTime.Text.Trim()) || string.IsNullOrEmpty(txtStopTime.Text.Trim()))
            {
                MessageBox.Show("Start time and end time can not be zero or null.", "DigiPhoto", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                return false;
            }
            else if (Convert.ToInt32(txtStartTime.Text.Trim()) >= Convert.ToInt32(txtStopTime.Text.Trim()))
            {
                MessageBox.Show("Start time can not be less then or equal to the End time.", "DigiPhoto", MessageBoxButton.OK, MessageBoxImage.Information);
                return false;
            }
            else
            {
                return true;
            }
        }

        private void UnsubscribeEvents()
        {
            txtStopTime.TextChanged -= new TextChangedEventHandler(txtnumber_TextChanged);
            txtStartTime.TextChanged -= new TextChangedEventHandler(txtnumber_TextChanged);
            btnAddBorders.Click -= new RoutedEventHandler(btnAddBorders_Click);
            btnClose.Click -= new RoutedEventHandler(btnClose_Click);
            btnDefaultBorder.Click -= new RoutedEventHandler(btnDefaultBorder_Click);
            btnClearList.Click -= new RoutedEventHandler(btnClearList_Click);
        }

        private void UserControl_Unloaded(object sender, RoutedEventArgs e)
        {
            UnsubscribeEvents();
        }


        //private void btnAddBorders_Click(object sender, RoutedEventArgs e)
        //{
        //    SlotProperty borderProp = null;
        //    int stopTime = 0;
        //    int startTime = 0;
        //    if (!string.IsNullOrEmpty(txtStartTime.Text))
        //    {
        //        startTime = Convert.ToInt32(txtStartTime.Text);
        //    }
        //    if (!string.IsNullOrEmpty(txtStopTime.Text))
        //    {
        //        stopTime = Convert.ToInt32(txtStopTime.Text);
        //    }
        //    long id = ((TemplateListItems)(cmbBorders.SelectionBoxItem)).Item_ID;
        //    if (IsBorderEdit)
        //    {
        //        MessageBoxResult response = MessageBox.Show("Do you want to save changes?", "Digiphoto", MessageBoxButton.YesNo, MessageBoxImage.Question);

        //        if (response == MessageBoxResult.Yes)
        //        {
        //            borderProp = borderslotlist.Where(o => o.ID == EditBorderID).FirstOrDefault();
        //            // LogoSlotList.RemoveAll(o => o.ID == EditedItemId);
        //            EditBorderID = 0;
        //        }
        //        else
        //        {
        //            cmbBorders.SelectedIndex = 0;
        //            txtStartTime.Text = "0";
        //            txtStopTime.Text = "0";
        //            return;
        //        }
        //    }
        //    else
        //    {
        //        borderProp = new SlotProperty();
        //    }

        //    if (chkApplyBorders.IsChecked == true)
        //    {
        //        if (CheckSlotValidations())
        //        {
        //            bool added = false;

        //            borderProp.ID = uniqueID;
        //            borderProp.ItemID = ((TemplateListItems)(cmbBorders.SelectionBoxItem)).Item_ID;
        //            borderProp.FilePath = ((TemplateListItems)(cmbBorders.SelectionBoxItem)).FilePath;
        //            borderProp.StartTime = Convert.ToInt32(txtStartTime.Text);
        //            borderProp.StopTime = Convert.ToInt32(txtStopTime.Text);

        //            if (borderslotlist.Count == 0)
        //            {
        //                borderslotlist.Add(borderProp);
        //                added = true;
        //            }
        //            else
        //            {
        //                SlotProperty bslItem = borderslotlist.OrderBy(o => o.StartTime).Where(o => o.StartTime <= startTime).LastOrDefault();
        //                SlotProperty bslFirst = borderslotlist.OrderBy(o => o.StartTime).ToList().FirstOrDefault();
        //                if (bslItem != null)
        //                {
        //                    //if (startTime > bslItem.BorderStartTime + (bslItem.BorderStopTime - bslItem.BorderStartTime))
        //                    {
        //                        borderslotlist.Add(borderProp);
        //                        added = true;
        //                    }
        //                    //else
        //                    //{
        //                    //    MessageBox.Show("There is already a border in the given time frame.", "DigiPhoto", MessageBoxButton.OK, MessageBoxImage.Exclamation);
        //                    //}
        //                }
        //                else if (bslFirst != null)
        //                {
        //                    if (startTime + stopTime <= bslFirst.StartTime)
        //                    {
        //                        borderslotlist.Add(borderProp);
        //                        added = true;
        //                    }
        //                    else
        //                    {
        //                        MessageBox.Show("There is already a border in the given time frame.", "DigiPhoto", MessageBoxButton.OK, MessageBoxImage.Exclamation);
        //                    }
        //                }
        //                else
        //                {
        //                    borderslotlist.Add(borderProp);
        //                    added = true;
        //                }
        //            }
        //            if (added)
        //            {
        //                TemplateListItems tmp = ((VideoEditor)Window.GetWindow(_parent)).objTemplateList.Where(o => o.MediaType == 608 && o.Item_ID == id).FirstOrDefault();
        //                tmp.IsChecked = true;
        //            }
        //            DGManageBorders.ItemsSource = borderslotlist.OrderBy(o => o.StartTime);
        //            DGManageBorders.Items.Refresh();
        //            uniqueID++;
        //        }
        //    }
        //    else
        //    {
        //        MessageBox.Show("Please check Apply Borders.", "DigiPhoto", MessageBoxButton.OK, MessageBoxImage.Information);
        //    }
        //}
        private void btnAddBorders_Click(object sender, RoutedEventArgs e)
        {
            if (cmbBorders.Items.Count <= 0)
            {
                MessageBox.Show("Please upload borders.", "Digiphoto", MessageBoxButton.OK, MessageBoxImage.Information);
                return;
            }
            SlotProperty borderProp = new SlotProperty();
            VideoElements videoElement = new VideoElements();
            int stopTime = 0;
            int startTime = 0;
            if (!string.IsNullOrEmpty(txtStartTime.Text))
            {
                startTime = Convert.ToInt32(txtStartTime.Text);
            }
            if (!string.IsNullOrEmpty(txtStopTime.Text))
            {
                stopTime = Convert.ToInt32(txtStopTime.Text);
            }
            long id = ((TemplateListItems)(cmbBorders.SelectionBoxItem)).Item_ID;
            if (CheckSlotValidations())
            {
                if (IsBorderEdit)
                {
                    IsBorderEdit = false;
                    MessageBoxResult response = MessageBox.Show("Do you want to save changes?", "Digiphoto", MessageBoxButton.YesNo, MessageBoxImage.Question);
                    if (response == MessageBoxResult.Yes)
                    {
                        borderProp = borderslotlist.Where(o => o.ID == EditBorderID).FirstOrDefault();
                        borderslotlist.Remove(borderProp);
                        if (videoElement.IsValidSlot(borderslotlist, startTime, stopTime))
                        {
                            //borderProp.ItemID = ((TemplateListItems)(cmbBorders.SelectionBoxItem)).Item_ID;
                            borderProp.ItemID = id;
                            borderProp.FilePath = ((TemplateListItems)(cmbBorders.SelectionBoxItem)).FilePath;
                            borderProp.StartTime = Convert.ToInt32(txtStartTime.Text);
                            borderProp.StopTime = Convert.ToInt32(txtStopTime.Text);
                            borderslotlist.Add(borderProp);
                            EditBorderID = 0;
                        }
                        else
                            borderslotlist.Add(borderProp);
                    }
                    else
                    {
                        cmbBorders.SelectedIndex = 0;
                        txtStartTime.Text = "0";
                        txtStopTime.Text = "0";
                        return;
                    }
                }
                else
                {

                    if (chkApplyBorders.IsChecked == true)
                    {
                        bool added = false;
                        borderProp.ID = uniqueID;
                        borderProp.ItemID = id;
                        borderProp.FilePath = ((TemplateListItems)(cmbBorders.SelectionBoxItem)).FilePath;
                        borderProp.StartTime = Convert.ToInt32(txtStartTime.Text);
                        borderProp.StopTime = Convert.ToInt32(txtStopTime.Text);
                        if (videoElement.IsValidSlot(borderslotlist, startTime, stopTime))
                        {
                            borderslotlist.Add(borderProp);
                            added = true;
                            uniqueID++;
                        }
                        if (added)
                        {
                            TemplateListItems tmp = ((VideoEditor)Window.GetWindow(_parent)).objTemplateList.Where(o => o.MediaType == 608 && o.Item_ID == id).FirstOrDefault();
                            tmp.IsChecked = true;
                        }


                    }
                    else
                    {
                        MessageBox.Show("Please check Apply Borders.", "DigiPhoto", MessageBoxButton.OK, MessageBoxImage.Information);
                    }
                }
                DGManageBorders.ItemsSource = borderslotlist.OrderBy(o => o.StartTime);
                DGManageBorders.Items.Refresh();
            }
        }

        private void btnClearList_Click(object sender, RoutedEventArgs e)
        {
            if (borderslotlist != null)
            {
                borderslotlist.Clear();
                DGManageBorders.ItemsSource = borderslotlist;
                DGManageBorders.Items.Refresh();
                ((VideoEditor)Window.GetWindow(_parent)).objTemplateList.Where(o => o.MediaType == 608 && o.IsChecked == true).ToList().ForEach(t => t.IsChecked = false);
            }

        }

        private void btnDefaultBorder_Click(object sender, RoutedEventArgs e)
        {
            borderslotlist.Clear();
            long id = ((TemplateListItems)(cmbBorders.SelectionBoxItem)).Item_ID;
            if (chkApplyBorders.IsChecked == true)
            {
                if (slotList != null && slotList.Count > 0)
                {
                    borderslotlist.Clear();
                    uniqueID = 151;
                    for (int a = 0; a < slotList.Count; a++)
                    {
                        int startTime = Convert.ToInt32(slotList[a].FrameTimeIn);
                        int stopTime = Convert.ToInt32(slotList[a].PhotoDisplayTime) + Convert.ToInt32(slotList[a].FrameTimeIn);
                        SlotProperty borderProp = new SlotProperty();
                        borderProp.ID = uniqueID;
                        borderProp.ItemID = ((TemplateListItems)(cmbBorders.SelectionBoxItem)).Item_ID;
                        borderProp.FilePath = ((TemplateListItems)(cmbBorders.SelectionBoxItem)).FilePath;
                        borderProp.StartTime = startTime;
                        borderProp.StopTime = stopTime;
                        borderslotlist.Add(borderProp);
                        uniqueID++;
                    }
                    TemplateListItems tmp = ((VideoEditor)Window.GetWindow(_parent)).objTemplateList.Where(o => o.MediaType == 608 && o.Item_ID == id).FirstOrDefault();
                    tmp.IsChecked = true;
                    DGManageBorders.ItemsSource = borderslotlist.OrderBy(o => o.StartTime);
                    DGManageBorders.Items.Refresh();
                }
            }
        }

        private void btnEdit_Click(object sender, RoutedEventArgs e)
        {
            Button btnSender = (Button)sender;
            EditBorderID = long.Parse(btnSender.Tag.ToString());
            SlotProperty borderSlot = borderslotlist.Where(o => o.ID == EditBorderID).FirstOrDefault();
            TemplateListItems border = BordersList.Where(o => o.Item_ID == borderSlot.ItemID).FirstOrDefault();
            cmbBorders.SelectedItem = border;
            txtStartTime.Text = borderSlot.StartTime.ToString();
            txtStopTime.Text = borderSlot.StopTime.ToString();
            IsBorderEdit = true;
        }
    }

}
