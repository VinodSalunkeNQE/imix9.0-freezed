﻿using DigiPhoto.Common;
using DigiPhoto.IMIX.Business;
using DigiPhoto.IMIX.Model;
using FrameworkHelper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace DigiPhoto
{
    /// <summary>
    /// Interaction logic for CtrlOpenCloseForm.xaml
    /// </summary>
    public partial class CtrlOpenCloseForm : UserControl
    {
        public CtrlOpenCloseForm()
        {
            InitializeComponent();
            Visibility = Visibility.Hidden;
        }
        /// <summary>
        /// The _parent
        /// </summary>
        private UIElement _parent;
        /// <summary>
        /// The _hide request
        /// </summary>
        private bool _hideRequest = false;
        /// <summary>
        /// The _result
        /// </summary>
        private string _result;
        //private bool _OpenNext;
        /// <summary>
        /// The controlon
        /// </summary>
        TextBox controlon;
        /// <summary>
        /// Sets the parent.
        /// </summary>
        /// <param name="parent">The parent.</param>
        public void SetParent(UIElement parent)
        {
            _parent = (UIElement)parent;

        }
        public Int32 OpeningProcMendatory { get; set; }
        public Int32 FormTypeID { get; set; }
        public DateTime? TransDate { get; set; }
        public DateTime? BusinessDate { get; set; }
        public DateTime ServerTime{ get; set; }
       // public TimeSpan CloseTime { get; set; }
        
        //public DateTime? FilledOn { get; set; }
        public string ShowHandlerDialog(string message)
        {
           
            Visibility = Visibility.Visible;
            OpenClose();
           
            //_parent.IsEnabled = false;


            _hideRequest = false;
            while (!_hideRequest)
            {
                // HACK: Stop the thread if the application is about to close
                if (this.Dispatcher.HasShutdownStarted ||
                    this.Dispatcher.HasShutdownFinished)
                {
                    break;
                }
                // HACK: Simulate "DoEvents"
                this.Dispatcher.Invoke(
                    DispatcherPriority.Background,
                    new ThreadStart(delegate { }));
                Thread.Sleep(20);
            }

            return _result;
        }

        /// <summary>
        /// Hides the handler dialog.
        /// </summary>
        private void HideHandlerDialog()
        {
            //  _result = null;
            _hideRequest = true;
            Visibility = Visibility.Collapsed;
            _parent.IsEnabled = true;
        }

        public void OpenClose()
        {
            //_OpenNext = false;
            txtMsg.Visibility = System.Windows.Visibility.Hidden;

            //if (FormTypeID != 1 && TransDate.Value.Date < ServerTime.Date)
            if (FormTypeID != 1)
            {
                grdOpening.Visibility = System.Windows.Visibility.Visible;
                grdClosing.Visibility = System.Windows.Visibility.Hidden;
                // _OpenNext = true;
            }
            ///_sageInfo.FormTypeID == 1 && _sageInfo.TransDate.Value.Date < ServerTime.Date && ServerTime.TimeOfDay < ts
           // else if (FormTypeID == 1 && TransDate.Value.Date <= ServerTime.Date)
            else if (FormTypeID == 1)
            {
                grdOpening.Visibility = System.Windows.Visibility.Hidden;
                grdClosing.Visibility = System.Windows.Visibility.Visible;
                //_OpenNext = true;
            }
            //else if (FormTypeID == 2 && TransDate.Value.Date == ServerTime.Date)
            //{
            //    grdOpening.Visibility = System.Windows.Visibility.Hidden;
            //    grdClosing.Visibility = System.Windows.Visibility.Hidden;
            //    txtMsg.Visibility = System.Windows.Visibility.Visible;
            //    txtMsg.Text = "You have already filled the closing form for business date " + string.Format("{0:dd-MMM-yyyy}", BusinessDate.Value);
            //    // _OpenNext = false;
            //}
            

        }
        private void btnOpen_Click(object sender, RoutedEventArgs e)
        {
            //txt6X8StarNumber.Text = String.Empty;
            //txt8X10StarNumber.Text = String.Empty;
            //txtCashFloat.Text = String.Empty;
            
            _result = string.Empty;          
            _result = "Opening From";
            HideHandlerDialog();
        }
        private void btnClose_Click(object sender, RoutedEventArgs e)
        {
            //txt6X8StarNumber.Text = String.Empty;
            //txt8X10StarNumber.Text = String.Empty;
            //txtCashFloat.Text = String.Empty;
            _result = string.Empty;          
            _result = "Closing From";
            HideHandlerDialog();
        }
        private void btnShowhdeForm_Click(object sender, RoutedEventArgs e)
        {
            _result = string.Empty;    
            HideHandlerDialog();
        }
    }
    
}
