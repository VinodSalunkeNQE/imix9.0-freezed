﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Collections.ObjectModel;
using System.Windows.Threading;
using System.Threading;
using Xceed.Wpf.Toolkit;
using DigiPhoto.IMIX.Model;
using DigiPhoto.IMIX.Business;
using FrameworkHelper;
namespace DigiPhoto
{
    /// <summary>
    /// Interaction logic for SelectedImages.xaml
    /// </summary>
    public partial class SelectedProcessedVideoslist : UserControl
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SelectedImageslist"/> class.
        /// </summary>
        /// 
        ///bool islistLoaded = false;
        public SelectedProcessedVideoslist()
        {
            try
            {
                InitializeComponent();
                lstSelectedImage.Items.Clear();
            }
            catch (Exception ex)
            {
            }
        }


        /// <summary>
        /// Gets or sets the _ selected image.
        /// </summary>
        /// <value>
        /// The _ selected image.
        /// </value>
        public List<LstMyItems> _SelectedImage
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the previous image.
        /// </summary>
        /// <value>
        /// The previous image.
        /// </value>
        public List<String> PreviousImage
        {
            get;
            set;
        }
        /// <summary>
        /// Gets or sets the maximum selected photos.
        /// </summary>
        /// <value>
        /// The maximum selected photos.
        /// </value>
        public int maxSelectedPhotos
        {
            get;
            set;
        }
        /// <summary>
        /// Gets or sets a value indicating whether [is bundled].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [is bundled]; otherwise, <c>false</c>.
        /// </value>
        public Boolean IsBundled
        {
            get;
            set;
        }
        /// <summary>
        /// Gets or sets a value indicating whether [is package].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [is package]; otherwise, <c>false</c>.
        /// </value>
        public Boolean IsPackage
        {
            get;
            set;
        }
        /// <summary>
        /// Gets or sets the product type unique identifier.
        /// </summary>
        /// <value>
        /// The product type unique identifier.
        /// </value>
        public int ProductTypeID
        {
            get;
            set;
        }
        public int PackageId
        {
            get;
            set;
        }
        /// <summary>
        /// The _hide request
        /// </summary>
        private bool _hideRequest = false;
        /// <summary>
        /// The _result
        /// </summary>
        private List<LstMyItems> _result;
        /// <summary>
        /// The _parent
        /// </summary>
        private UIElement _parent;

        /// <summary>
        /// Sets the parent.
        /// </summary>
        /// <param name="parent">The parent.</param>
        public void SetParent(UIElement parent)
        {
            _parent = (UIElement)parent;

            //check for img availability
            if (lstSelectedImage.Items.Count > 0)
                SPSelectAll.Visibility = Visibility.Visible;
            else
                SPSelectAll.Visibility = Visibility.Hidden;
            //CheckSelectedImg();
        }
        #region Message

        /// <summary>
        /// Gets or sets the message2.
        /// </summary>
        /// <value>
        /// The message2.
        /// </value>
        public string MessageProcessedVideo2
        {
            get { return (string)GetValue(MessageProcessedVideo2Property); }
            set { SetValue(MessageProcessedVideo2Property, value); }
        }

        // Using a DependencyProperty as the backing store for Message.
        // This enables animation, styling, binding, etc...
        /// <summary>
        /// The message2 property
        /// </summary>
        public static readonly DependencyProperty MessageProcessedVideo2Property =
            DependencyProperty.Register(
                "MessageProcessedVideo2", typeof(string), typeof(ModalDialog), new UIPropertyMetadata(string.Empty));

        #endregion



        /// <summary>
        /// Shows the handler dialog.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <returns></returns>
        public List<LstMyItems> ShowHandlerDialog(string message)
        {
            MessageProcessedVideo2 = message;
            Visibility = Visibility.Visible;
            // if (!islistLoaded)
            {
                LoadProcessedVideos(PackageId);
                //islistLoaded = true;
            }
            _parent.IsEnabled = true;
            foreach (LstMyItems lt in RobotImageLoader.PrintImages)
            {
                lt.MaxCount = maxSelectedPhotos;
                if (!IsPackage || (ProductTypeID == 35 || ProductTypeID == 36 || ProductTypeID == 4 || ProductTypeID == 78))
                {
                    lt.ToShownoCopy = false;
                }
                else
                {
                    lt.ToShownoCopy = true;
                }
            }
            /*
            lstSelectedImage.Items.Clear();
            foreach (var item in RobotImageLoader.PrintImages)
            {
                lstSelectedImage.Items.Add(item);
            }
           */
            // SetInitial();
            CheckSelectedImg();
            // CheckSelectedImg();

            _hideRequest = false;
            while (!_hideRequest)
            {
                // HACK: Stop the thread if the application is about to close
                if (this.Dispatcher.HasShutdownStarted ||
                    this.Dispatcher.HasShutdownFinished)
                {
                    break;
                }
                // HACK: Simulate "DoEvents"
                this.Dispatcher.Invoke(
                    DispatcherPriority.Background,
                    new ThreadStart(delegate { }));
                Thread.Sleep(20);

            }

            return _result;
        }

        /// <summary>
        /// Hides the handler dialog.
        /// </summary>
        private void HideHandlerDialog()
        {
            _hideRequest = true;
            Visibility = Visibility.Collapsed;
            _parent.IsEnabled = true;
        }
        /// <summary>
        /// Gets the printed image.
        /// </summary>
        public void GetPrintedImage()
        {

            lstSelectedImage.ItemsSource = _SelectedImage;
        }

        /// <summary>
        /// Handles the Loaded event of the UserControl control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {


        }



        /// <summary>
        /// Finds the visual child.
        /// </summary>
        /// <typeparam name="childItem">The type of the hild item.</typeparam>
        /// <param name="obj">The object.</param>
        /// <returns></returns>
        public static childItem FindVisualChild<childItem>(DependencyObject obj)
  where childItem : DependencyObject
        {
            // Search immediate children
            for (int i = 0; i < VisualTreeHelper.GetChildrenCount(obj); i++)
            {
                DependencyObject child = VisualTreeHelper.GetChild(obj, i);

                if (child is childItem)
                    return (childItem)child;

                else
                {
                    childItem childOfChild = FindVisualChild<childItem>(child);

                    if (childOfChild != null)
                        return childOfChild;
                }
            }

            return null;
        }

        /// <summary>
        /// Sets the initial.
        /// </summary>
        public void SetInitial()
        {
            for (int i = 0; i < lstSelectedImage.Items.Count; i++)
            {
                DependencyObject obj = lstSelectedImage.ItemContainerGenerator.ContainerFromIndex(i);
                if (obj != null)
                {
                    CheckBox chk = FindVisualChild<CheckBox>(obj);// FindByName("rdoSelect", row) as RadioButton;
                    if (chk != null)
                    {
                        chk.IsChecked = false;
                    }


                }
            }

            for (int i = 0; i < lstSelectedImage.Items.Count; i++)
            {
                DependencyObject obj = lstSelectedImage.ItemContainerGenerator.ContainerFromIndex(i);
                if (obj != null)
                {
                    CheckBox chk = FindVisualChild<CheckBox>(obj);// FindByName("rdoSelect", row) as RadioButton;
                    if (chk != null)
                    {
                        if (PreviousImage != null)
                        {
                            var item = (from objSelected in PreviousImage
                                        where objSelected == chk.Uid.ToString()
                                        select objSelected).FirstOrDefault();

                            if (item != null)
                            {
                                chk.IsChecked = true;
                            }
                        }
                    }

                }
            }
        }
        /// <summary>
        /// Handles the Click event of the btnSubmit control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void btnSubmit_Click(object sender, RoutedEventArgs e)
        {

            int TotalSelectedImages = 0;

            _result = new List<LstMyItems>();

            for (int i = 0; i < lstSelectedImage.Items.Count; i++)
            {
                DependencyObject obj = lstSelectedImage.ItemContainerGenerator.ContainerFromIndex(i);
                if (obj != null)
                {
                    CheckBox rdo = FindVisualChild<CheckBox>(obj);// FindByName("rdoSelect", row) as RadioButton;
                    if (rdo != null)
                    {
                        if (Convert.ToBoolean(rdo.IsChecked))
                        {
                            IntegerUpDown updown = FindVisualChild<IntegerUpDown>(obj);
                            int totalval = 1;
                            if (updown.Value!=null)
                            totalval = (int)updown.Value;
                            TotalSelectedImages = TotalSelectedImages + totalval;
                            int counter = 0;
                            while (counter < totalval)
                            {
                                _result.Add(((LstMyItems)lstSelectedImage.Items[i]));
                                counter++;
                            }
                        }
                    }
                }
            }
            if (ProductTypeID == 4)
            {
                if (TotalSelectedImages != 4)
                {
                    if (TotalSelectedImages > 4 && TotalSelectedImages % 4 == 0)
                    {
                        if (System.Windows.MessageBox.Show("You have selected more than 4 videos, do you want to continue ?", "Digiphoto", MessageBoxButton.YesNo, MessageBoxImage.Warning) == MessageBoxResult.Yes)
                        {
                            HideHandlerDialog();
                        }
                    }
                    else
                    {
                        System.Windows.MessageBox.Show("Please select combination of 4 videos.");
                    }
                }
                else
                {
                    HideHandlerDialog();
                }
            }
            else if (ProductTypeID == 3 || ProductTypeID == 5)
            {
                if (TotalSelectedImages > 1)
                {
                    //System.Windows.MessageBox.Show("You can select only 1 image for this product.");
                    if (System.Windows.MessageBox.Show("You have selected more than 1 videos, do you want to continue ?", "Digiphoto", MessageBoxButton.YesNo, MessageBoxImage.Warning) == MessageBoxResult.Yes)
                    {
                        HideHandlerDialog();
                    }
                }
                else
                {
                    HideHandlerDialog();
                }
            }
            else
            {

                if (IsPackage)
                {
                    if (_result.Count > maxSelectedPhotos)
                    {
                        if (System.Windows.MessageBox.Show("You have selected more than " + maxSelectedPhotos + " videos, do you want to continue ?", "Digiphoto", MessageBoxButton.YesNo, MessageBoxImage.Warning) == MessageBoxResult.Yes)
                        {
                            HideHandlerDialog();
                        }
                    }
                    else
                    {
                        HideHandlerDialog();
                    }
                }
                else
                {
                    HideHandlerDialog();
                }
            }

            //if (IsBundled)
            //{
            //    if(
            //}


        }

        /// <summary>
        /// Handles the Click event of the btnSubmitCancel control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void btnSubmitCancel_Click(object sender, RoutedEventArgs e)
        {

            SelectDeselectAll(false);
            /*
            for (int i = 0; i < lstSelectedImage.Items.Count; i++)
            {
                DependencyObject obj = lstSelectedImage.ItemContainerGenerator.ContainerFromIndex(i);
                if (obj != null)
                {
                    CheckBox chk = FindVisualChild<CheckBox>(obj);// FindByName("rdoSelect", row) as RadioButton;
                    if (chk != null)
                    {
                        chk.IsChecked = false;
                    }


                }
            }
            */
            //HideHandlerDialog();
        }


        /// <summary>
        /// Handles the Click event of the btnClose control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void btnClose_Click(object sender, RoutedEventArgs e)
        {
            _result = null;

            HideHandlerDialog();

        }

        /// <summary>
        /// Handles the Checked event of the ChkSelected control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void ChkSelected_Click(object sender, RoutedEventArgs e)
        {
            CheckSelectedImg();
        }

        private void chkSelectAll_Click(object sender, RoutedEventArgs e)
        {
            SelectDeselectAll(chkSelectAll.IsChecked);
        }


        private void SelectDeselectAll(bool? SelectDeselect)
        {
            for (int i = 0; i < lstSelectedImage.Items.Count; i++)
            {
                DependencyObject obj = lstSelectedImage.ItemContainerGenerator.ContainerFromIndex(i);
                if (obj != null)
                {
                    CheckBox chk = FindVisualChild<CheckBox>(obj);// FindByName("rdoSelect", row) as RadioButton;
                    if (chk != null)
                    {
                        chk.IsChecked = SelectDeselect;
                    }


                }
            }
            if (SelectDeselect == false)
            {
                txbImages.Text = "Selected : " + "0" + "/" + lstSelectedImage.Items.Count;
            }
            else
            {

                txbImages.Text = "Selected : " + lstSelectedImage.Items.Count + "/" + lstSelectedImage.Items.Count;
            }


        }

        /// <summary>
        /// Counts the selected images
        /// </summary>
        /// <returns></returns>

        private int SelectedItemCount()
        {
            int selectedCount = 0;
            try
            {
                selectedCount = lstSelectedImage.Items.Cast<LstMyItems>().Where(o => o.IsChecked).Count();
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
            return selectedCount;
        }

        public void CheckSelectedImg()
        {
            int SelectedCount = SelectedItemCount();
            if (lstSelectedImage.Items.Count > 0)
            {
                SPSelectAll.Visibility = Visibility.Visible;
                if (lstSelectedImage.Items.Count == SelectedCount)
                    chkSelectAll.IsChecked = true;
                else
                    chkSelectAll.IsChecked = false;
            }
            else
            {
                SPSelectAll.Visibility = Visibility.Hidden;
            }
            txbImages.Text = "Selected : " + SelectedCount + "/" + lstSelectedImage.Items.Count.ToString();
            //txbImages.Text =   SelectedItemCount()+" " +"Selected"+ "/" + lstSelectedImage.Items.Count.ToString()+" "+"Images"; 
        }

        private void LoadProcessedVideos(int PackageId)
        {
            lstSelectedImage.Items.Clear();
            ProcessedVideoBusiness objBusiness = new ProcessedVideoBusiness();
            List<ProcessedVideoInfo> objProcessedList = objBusiness.GetProcessedVideosByPackageId(PackageId);
            //Show only Processed Videos
            foreach (var item in RobotImageLoader.GroupImages.Where(p => p.MediaType == 2 || p.MediaType == 3))
            {
                if (objProcessedList.Where(o => o.VideoId == item.PhotoId).ToList().Count > 0)
                {
                    item.IsChecked = false;
                    VideoProcessingClass vpc = new VideoProcessingClass();
                    item.FilePath=item.FilePath.Replace(vpc.SupportedVideoFormats["avi"].ToString(), ".jpg").Replace(vpc.SupportedVideoFormats["mp4"].ToString(), ".jpg").Replace(vpc.SupportedVideoFormats["wmv"].ToString(), ".jpg").Replace(vpc.SupportedVideoFormats["mov"].ToString(), ".jpg").Replace(vpc.SupportedVideoFormats["3gp"].ToString(), ".jpg").Replace(vpc.SupportedVideoFormats["3g2"].ToString(), ".jpg").Replace(vpc.SupportedVideoFormats["m2v"].ToString(), ".jpg").Replace(vpc.SupportedVideoFormats["m4v"].ToString(), ".jpg").Replace(vpc.SupportedVideoFormats["flv"].ToString(), ".jpg").Replace(vpc.SupportedVideoFormats["mpeg"].ToString(), ".jpg").Replace(vpc.SupportedVideoFormats["ffmpeg"].ToString(), ".jpg");
                    if (PreviousImage != null)
                    {
                        var itm = (from objSelected in PreviousImage
                                   where objSelected == item.PhotoId.ToString()
                                   select objSelected).FirstOrDefault();

                        if (itm != null)
                        {
                            item.IsChecked = true;
                        }
                    }
                    lstSelectedImage.Items.Add(item);
                }
            }
        }
    }
}
