﻿using DigiPhoto.IMIX.Business;
using DigiPhoto.IMIX.Model;
using FrameworkHelper;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
//using VisioForge.Tools;
//using VisioForge.Types;
//using MediaPlayer = VisioForge.Controls.WPF.MediaPlayer;
namespace DigiPhoto
{
    /// <summary>
    /// Interaction logic for VideoSlotsTime.xaml
    /// </summary>
    public partial class VideoPlayer : UserControl
    {
        private UIElement _parent;
        DispatcherTimer timer = new DispatcherTimer();
        public static string vsMediaFileName {get;set;}
        public BitmapImage imagesource { get; set; }
        public string Title { get; set; }
        private FileStream memoryFileStream;
        //bool isMuted = false;
        MLMediaPlayer mplayer;
        public event EventHandler ExecuteParentMethod;

        public void SetParent(UIElement parent)
        {
            imgname.Text = Title;
            _parent = (UIElement)parent;
            if (!string.IsNullOrEmpty(vsMediaFileName))
            {
               imgmain.Visibility = Visibility.Collapsed;
               vidoriginal.Visibility = Visibility.Visible;
               MediaStop();
               MediaPlay();
             //  _mediaPlay();
            }
            else
            {
                imgmain.Visibility = Visibility.Visible;
                vidoriginal.Visibility = Visibility.Collapsed;
                imgmain.Source = imagesource;
                ShowToClientView();
            }
           
        }

        private void _mediaPlay()
        {
            mplayer = new MLMediaPlayer(vsMediaFileName, "Search", true);
          
        }
        public void ShowToClientView()
        {
            try
            {
                VisualBrush CB = new VisualBrush(IMGFrame);
                SearchResult objSearchResult = new SearchResult();
                objSearchResult.CompileEffectChanged(CB, -1);
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }
        public VideoPlayer()
        {
            InitializeComponent();      
            mplayer = new MLMediaPlayer(vsMediaFileName, "VideoPlayer");
        }

        private void btnClose_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                MediaStop();
                imagesource = null;
                vsMediaFileName = string.Empty;
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
            HideHandlerDialog();
        }

        private void HideHandlerDialog()
        {
            Visibility = Visibility.Collapsed;
            _parent.IsEnabled = true;
            OnExecuteMethod();
           //code to call parent
        }
        public virtual void OnExecuteMethod()
        {
            if (ExecuteParentMethod != null)
                ExecuteParentMethod(null,null);
        }
        #region MediaPlayer
        ClientView clientWin = null;

        void MediaStop()
        {
            //mpc.VisioMediaPlayer.Stop();
            //gdMediaPlayer.Children.Clear();
            if (mplayer != null)
            {
                mplayer.MediaStop();
                mplayer = null;
            }
            gdMediaPlayer.Children.Clear();
            gdMediaPlayer.Children.Remove(mplayer);
            if (clientWin == null)
            {
                foreach (Window wnd in Application.Current.Windows)
                {
                    if (wnd.Title == "ClientView")
                    {
                        clientWin = (ClientView)wnd;
                        break;
                    }
                }
            }
            if (clientWin != null)
                clientWin.StopMediaPlay();
  
        }
        void MediaPlay()
        {
            //mpc = new MediaPlayerControl(vsMediaFileName, "VideoPlayer");
            //gdMediaPlayer.Children.Add(mpc);
            MediaStop();
    
            
            if (mplayer != null)
            {
                mplayer.Dispose();
            }
            gdMediaPlayer.Dispatcher.BeginInvoke(new Action(
                () =>
                {
                    mplayer = new MLMediaPlayer(vsMediaFileName, "Search", true);
                    gdMediaPlayer.BeginInit();
                    gdMediaPlayer.Children.Clear();
                    gdMediaPlayer.Children.Add(mplayer);
                    gdMediaPlayer.EndInit();
                }));
        }

        #endregion

        private void UserControl_Unloaded(object sender, RoutedEventArgs e)
        {
            MediaStop();
            btnClose.Click -= new RoutedEventHandler(btnClose_Click);
        }

    }
}
