﻿using DigiPhoto.Common;
using DigiPhoto.DataLayer;
using DigiPhoto.IMIX.Model;
using DigiPhoto.Interop;
using DigiPhoto.Manage;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
//using VisioForge.Controls.WPF;

namespace DigiPhoto
{
    /// <summary>
    /// Interaction logic for PanControl.xaml
    /// </summary>
    public partial class ViewFrameControl : UserControl
    {
        private UIElement _parent;
        private bool _result = false;
        public List<fileClass> captureFrameList = new List<fileClass>();
        [System.Runtime.InteropServices.DllImport("gdi32.dll")]
        public static extern bool DeleteObject(IntPtr hObject);
        public ViewFrameControl()
        {
            InitializeComponent();
            Visibility = Visibility.Hidden;
            captureFrameList.Clear();
            lstFrames.ItemsSource = null;
        }
        
        public void SetParent(UIElement parent)
        {
            _parent = (UIElement)parent;
        }
        private void btnClose_Click(object sender, RoutedEventArgs e)
        {
            HideHandlerDialog();
            OnExecuteMethod();
        }
        private void HideHandlerDialog()
        {
            captureFrameList.Clear();  
            lstFrames.ItemsSource = "";
            lstFrames.Items.Refresh();
            imgFrame.Source = null;
            Visibility = Visibility.Collapsed;
            _parent.IsEnabled = true;
        }
        public void ControlListAutoFill()
        {
            string root = Environment.CurrentDirectory;
            string capturedFramePath = root + "\\";
            capturedFramePath = System.IO.Path.Combine(capturedFramePath, "CapturedFrames\\");
            captureFrameList.Clear();
            int i=1;
            if (Directory.Exists(capturedFramePath))
            {
                DirectoryInfo dinfo = new DirectoryInfo(capturedFramePath);
                FileInfo[] fi = dinfo.GetFiles();
                foreach (FileInfo fInfo in fi)
                {
                    fileClass fClass = new fileClass();
                    fClass.ID = i;
                    fClass.filePath = fInfo.FullName;
                    fClass.fileName = fInfo.Name;
                    BitmapImage image = new BitmapImage();
                    image.BeginInit();
                    image.CacheOption = BitmapCacheOption.OnLoad;
                    image.UriSource = new Uri(fInfo.FullName);
                    image.EndInit();
                    fClass.imgThumbnail = image;
                    captureFrameList.Add(fClass);
                    i++;
                }              
            }
            lstFrames.ItemsSource = null;
            lstFrames.ItemsSource = captureFrameList;
            lstFrames.UpdateLayout();
            lstFrames.SelectedIndex = 0;  
        }
        public bool ShowHandlerDialog()
        {
           Visibility = Visibility.Visible;        
           _parent.IsEnabled = false;
           ControlListAutoFill();
           return _result;
        }
      
        public event EventHandler ExecuteMethod;
        protected virtual void OnExecuteMethod()
        {
            if (ExecuteMethod != null) ExecuteMethod(this, EventArgs.Empty);
        }

        private void txtnumber_TextChanged(object sender, TextChangedEventArgs e)
        {
            string strtemp = string.Empty;
            if (string.IsNullOrEmpty(((TextBox)sender).Text))
                strtemp = "";
            else
            {
                int num = 0;
                bool success = int.TryParse(((TextBox)sender).Text, out num);
                if (success & num >= 0)
                {
                    ((TextBox)sender).Text.Trim();
                    strtemp = ((TextBox)sender).Text;
                }
                else
                {
                    ((TextBox)sender).Text = strtemp;
                    ((TextBox)sender).SelectionStart = ((TextBox)sender).Text.Length;
                }
            }
        }
        private void UserControl_Unloaded(object sender, RoutedEventArgs e)
        {
           
        }

        private void lstFrames_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                fileClass fClass = (fileClass)e.AddedItems[0];
                imgFrame.Source = new BitmapImage(new Uri(fClass.filePath));
              
            }
            catch(Exception ex)
            {
                imgFrame.Source = null;
            }
        }

    }
    public class fileClass
    {
        public string fileName { get; set; }
        public int ID { get; set; }
        public string filePath { get; set; }
        public BitmapImage imgThumbnail { get; set; }

    }
   
}
