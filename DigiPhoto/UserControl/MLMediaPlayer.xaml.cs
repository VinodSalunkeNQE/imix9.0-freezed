﻿using DigiPhoto.DataLayer;
using DigiPhoto.IMIX.Business;
using FrameworkHelper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using MPLATFORMLib;
using MControls;
namespace DigiPhoto
{
    /// <summary>
    /// Interaction logic for MediaPlayerControl.xaml
    /// </summary>

    public partial class MLMediaPlayer : UserControl
    {
        public MFileClass mFile;
        string vsMediaFileName = "";
        private System.Windows.Threading.DispatcherTimer dispatcherTimer;
        BackgroundWorker bwCopyFiles = new BackgroundWorker();
        Boolean IsMute = false;
        public MLMediaPlayer()
        {
            InitializeComponent();
        }
     

        public Object SetControlledObject(Object pObject)
        {
            var pOld = (Object)mFile;
            try
            {
                mFile = (MFileClass)pObject;
                UpdateState();
            }
            catch
            { }

            return pOld;
        }
        void MyPlaylist_OnEvent(string bsChannelID, string bsEventName, string bsEventParam, object pEventObject)
        {
            UpdateSeekControl();
            
            
            //mpGrid.UpdateLayout();
            
        }
        ClientView clientWin = null;
        public void myPlaylist_OnFrame(string bschannelid, object pmframe)
        {
            //if (clientWin == null)
            //    LoadClientViewObject();
            //if (clientWin != null)
            //{
            //    clientWin.myPlaylist_OnFrame(bschannelid, pmframe);
            //}
            MFileSeeking1.UpdatePos();

            int pbFullScreen = 0, pbDisplay = 0;
            MPreviewControl1.m_pPreview.PreviewIsFullScreen("", ref pbFullScreen, ref pbDisplay);
            if (clientWin != null && pbFullScreen != 1)
                clientWin.mPreviewControl.m_pPreview.PreviewFullScreen("", 0, -1);

          
            Marshal.ReleaseComObject(pmframe);

        }
        private void LoadClientViewObject()
        {
            foreach (Window wnd in Application.Current.Windows)
            {
                if (wnd.Title == "ClientView")
                {
                    clientWin = (ClientView)wnd;
                    break;
                }
            }
        }
        void UpdateSeekControl()
        {
            MFileSeeking1.SetControlledObject(mFile);
        }


        public MLMediaPlayer(string fileName, string type)
        {
            InitializeComponent();
            mFormatControl.SetControlledObject(mFile);
            mFormatControl.SetControlledObject(MPreviewControl1);
            if (mFormatControl.comboBoxVideo.Items.Count > 0)
                mFormatControl.comboBoxVideo.SelectedIndex = 12;
            if (mFormatControl.comboBoxAudio.Items.Count > 0)
                mFormatControl.comboBoxAudio.SelectedIndex = 8;
            dispatcherTimer = new System.Windows.Threading.DispatcherTimer();
            dispatcherTimer.Tick += DispatcherTimerTick;
            dispatcherTimer.Interval = new TimeSpan(0, 0, 0, 0, 100);
            dispatcherTimer.Start();
            try
            {
                if (mFile == null)
                    mFile = new MFileClass();
            }
            catch (Exception exception)
            {
                return;
            }
            try
            {
                if (string.IsNullOrEmpty(fileName))
                    return;
                vsMediaFileName = fileName;
                bwCopyFiles.DoWork += bwCopyFiles_DoWork;
                bwCopyFiles.RunWorkerCompleted += bwCopyFiles_RunWorkerCompleted;
                MPreviewControl1.SetControlledObject(mFile);
                mFile.FileNameSet(vsMediaFileName, "loop=true");
                if (!string.IsNullOrEmpty(vsMediaFileName))
                {
                    //if (clientWin == null)
                    //{
                    //    LoadClientViewObject();
                    //}
                    MediaPlay();
                    //if (clientWin != null)
                    //    clientWin.PlayVideoOnClient(type, mFile);

                }
                if (type == "VideoEditor")
                {
                    mpGrid.Height = 400;
                }
                else if (type == "VideoPlayer")
                {
                    mpGrid.Height = 550;
                }
                else if (type == "AdvancePreview")
                {
                    mpGrid.Width = 800; mpGrid.Height = 715;
                }
                else
                {
                    mpGrid.Height = 500;
                }
            }
            catch (Exception ex) { }

        }
        public MLMediaPlayer(string fileName, string type, bool playOnclientView)
        {
            InitializeComponent();
            mFormatControl.SetControlledObject(mFile);
            mFormatControl.SetControlledObject(MPreviewControl1);
            if (mFormatControl.comboBoxVideo.Items.Count > 0)
                mFormatControl.comboBoxVideo.SelectedIndex = 12;
            if (mFormatControl.comboBoxAudio.Items.Count > 0)
                mFormatControl.comboBoxAudio.SelectedIndex = 8;
            dispatcherTimer = new System.Windows.Threading.DispatcherTimer();
            dispatcherTimer.Tick += DispatcherTimerTick;
            dispatcherTimer.Interval = new TimeSpan(0, 0, 0, 0, 100);
            dispatcherTimer.Start();
            try
            {
                if (mFile == null)
                    mFile = new MFileClass();
            }
            catch (Exception exception)
            {
                return;
            }
            try
            {
                if (string.IsNullOrEmpty(fileName))
                    return;
                vsMediaFileName = fileName;
                bwCopyFiles.DoWork += bwCopyFiles_DoWork;
                bwCopyFiles.RunWorkerCompleted += bwCopyFiles_RunWorkerCompleted;
                MPreviewControl1.SetControlledObject(mFile);
                mFile.FileNameSet(vsMediaFileName, "loop=true");
                if (!string.IsNullOrEmpty(vsMediaFileName))
                {
                    MediaPlay();
                    if (playOnclientView)
                    {
                        if (clientWin == null)
                        {
                            LoadClientViewObject();
                        }

                        if (clientWin != null)
                            clientWin.PlayVideoOnClient(type, mFile);
                    }
                }
               
                if (type == "VideoEditor")
                {
                    mpGrid.Height = 400;
                }
                else if (type == "VideoPlayer")
                {
                    mpGrid.Height = 550;
                }
                else if (type == "AdvancePreview")
                {
                    mpGrid.Width = 800; mpGrid.Height = 715;
                }
                else
                {
                    mpGrid.Height = 500;
                }
            }
            catch (Exception ex) { }
        }

        private void CreateSecondaryPreview(MFileClass mfile, string source)
        {

        }

        void DispatcherTimerTick(object sender, EventArgs e)
        {
            UpdateState();
        }
        public void UpdateState()
        {
            try
            {
                eMState eState;
                double dblTime;
                mFile.FileStateGet(out eState, out dblTime);
            }
            catch
            { }
        }
        string replayFilePath = string.Empty;

        private void btLed_Click(object sender, RoutedEventArgs e)
        {
            btLed.IsEnabled = false;
            // Add the display utility copy code here
            // CopyVideoToDisplayFolder(vsMediaFileName);
            bwCopyFiles.RunWorkerAsync(vsMediaFileName);

        }
        int folderCount = 0;
        private object CopyVideoToDisplayFolder(string videofile)
        {
            object obj = null;
            try
            {
                if (ConfigManager.IMIXConfigurations.ContainsKey((int)ConfigParams.AutoVidDisplayEnabled) && Convert.ToBoolean(ConfigManager.IMIXConfigurations[(int)ConfigParams.AutoVidDisplayEnabled]) == true)
                {
                    string dispPath = ConfigManager.IMIXConfigurations.ContainsKey((int)ConfigParams.AutoVidDisplayFolder) ? ConfigManager.IMIXConfigurations[(int)ConfigParams.AutoVidDisplayFolder] : null;
                    int NoOfScreen = ConfigManager.IMIXConfigurations.ContainsKey((int)ConfigParams.NoOfDisplayScreens) ? Convert.ToInt32(ConfigManager.IMIXConfigurations[(int)ConfigParams.NoOfDisplayScreens]) : 0;
                    folderCount++;
                    if (NoOfScreen > 0)
                    {
                        for (int i = 1; i <= NoOfScreen; i++)
                        {
                            string Screenfolder = dispPath + "\\Display\\Display" + i.ToString();
                            if (!Directory.Exists(Screenfolder))
                            {
                                Directory.CreateDirectory(Screenfolder);
                            }
                            string pathToCopy = Screenfolder + "\\" + System.IO.Path.GetFileName(videofile);
                            File.Copy(videofile, pathToCopy, true);
                            DateTime dtCreation = DateTime.Now;
                            File.SetCreationTime(pathToCopy, dtCreation);
                        }
                        obj = "Video copied to display folder successfully!";
                    }
                    else obj = "Video display utility settings not found.";
                }
                else
                    obj = "Video display utility settings not found.";
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
            return obj;
        }

        void bwCopyFiles_DoWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                string videoFile = Convert.ToString(e.Argument);
                e.Result = CopyVideoToDisplayFolder(videoFile);

            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }
        void bwCopyFiles_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            btLed.IsEnabled = true;
            string msg = e.Result == null ? "Video copied to display folder successfully!" : e.Result.ToString();
            MessageBox.Show(msg, "Digiphoto", MessageBoxButton.OK, MessageBoxImage.Information);
            //ErrorHandler.ErrorHandler.LogFileWrite("File copied!!");
        }

        private void btReplay_Click(object sender, RoutedEventArgs e)
        {

        }

        private void btStop_Click(object sender, RoutedEventArgs e)
        {
            MediaStop();
            // clientWin.StopMediaPlay(1);
        }

        public void MediaStop()
        {
            try
            {
                if (mFile != null)
                {
                    mFile.FilePosSet(0, 0);
                    mFile.FilePlayStop(0);
                    mFile.OnFrame -=  new IMEvents_OnFrameEventHandler(myPlaylist_OnFrame);
                    mFile.OnEvent -=  new IMEvents_OnEventEventHandler(MyPlaylist_OnEvent);
                    mFile.ObjectClose();
                    //Marshal.ReleaseComObject(mFile);
                    //GC.Collect();
                }
            }
            catch
            { }
        }
        public void UnloadMediaPlayer()
        {
            MediaStop();
            if (mFile != null)
            {
                mFile.ObjectClose();
                //Marshal.ReleaseComObject(mFile);
            }
            //GC.Collect();
        }
        private void btStart_Click(object sender, RoutedEventArgs e)
        {

            MediaPlay();
            // clientWin.PlayVideoOnClient(string.Empty,"");
        }

        private void MediaPlay()
        {
            try
            {
                if (mFile != null)
                {
                    mFile.OnFrame += new IMEvents_OnFrameEventHandler(myPlaylist_OnFrame);
                    mFile.OnEvent += new IMEvents_OnEventEventHandler(MyPlaylist_OnEvent);
                    // Check for emty file
                    string strPath;
                    mFile.FileNameGet(out strPath);
                    if (strPath != null)
                    {
                        mFile.FilePlayStart();
                        //sldVolume_ValueChanged(null, null);
                    }
                }
            }
            catch
            { }
        }

        private void btPause_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (mFile != null)
                    mFile.FilePlayPause(0);
                //  clientWin.PauseVideoPlay();
            }
            catch
            { }
        }
        private void UserControl_Unloaded(object sender, RoutedEventArgs e)
        {
            MediaStop();
            mFile.ObjectClose();
            System.Runtime.InteropServices.Marshal.ReleaseComObject(mFile);
        }
        public void Dispose()
        {
            GC.Collect();
        }

        private void btMute_Click(object sender, RoutedEventArgs e)
        {
            IsMute = IsMute ? false : true;
            IMProps m_pProps = (IMProps)mFile;
            if (!IsMute)
            {
                m_pProps.PropsSet("object::audio_gain", "1");
                imgMute.Source = new BitmapImage(new Uri(@"/images/mute.png", UriKind.Relative));
                btMute.ToolTip = "Mute";
            }
            else
            {
                m_pProps.PropsSet("object::audio_gain", "-100");
                imgMute.Source = new BitmapImage(new Uri(@"/images/mute-on.png", UriKind.Relative));
                btMute.ToolTip = "Unmute";
            }
        }

        private void sldVolume_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            // 0 - full volume, -100 silence
            double dblPos = (double)sldVolume.Value / sldVolume.Maximum;
            if (MPreviewControl1.m_pPreview != null)
            {
                //Thread.Sleep(100);
                //MPreviewControl1.m_pPreview.PreviewAudioVolumeSet("", -1, -100 * (1 - dblPos));
                MPreviewControl1.m_pPreview.PreviewAudioVolumeSet("", -1, -100 * (1 - dblPos));
                //m_objPlaylist.PreviewAudioVolumeSet("", -1, -100 * (1 - dblPos));
            }
            sldVolume.UpdateLayout();
        }

        private void btnFullScreen_Click(object sender, RoutedEventArgs e)
        {
            if (MPreviewControl1.m_pPreview != null)
                MPreviewControl1.m_pPreview.PreviewFullScreen("", 1, -1);

            if (clientWin != null)
            {
                clientWin.mPreviewControl.m_pPreview.PreviewFullScreen("", 1, -1);
                //clientWin.mPreviewControl.isClientFullScreen = true;
            }
        }
        //public MLMediaPlayer(string fileName, string type, string ReplayFilePath, bool isReplay, bool isImage)
        //{
        //    InitializeComponent();
        //    dispatcherTimer = new System.Windows.Threading.DispatcherTimer();
        //    dispatcherTimer.Tick += DispatcherTimerTick;
        //    dispatcherTimer.Interval = new TimeSpan(0, 0, 0, 0, 100);
        //    dispatcherTimer.Start();

        //    try
        //    {
        //        if (mFile == null)
        //            mFile = new MFileClass();
        //    }
        //    catch (Exception exception)
        //    {
        //        return;
        //    }
        //    bwCopyFiles.DoWork += bwCopyFiles_DoWork;
        //    bwCopyFiles.RunWorkerCompleted += bwCopyFiles_RunWorkerCompleted;
        //    vsMediaFileName = fileName;

        //    MPreviewControl1.SetControlledObject(mFile);
        //    MFileSeeking1.SetControlledObject(mFile);
        //    mFile.FileNameSet(vsMediaFileName, "loop=true");

        //    if (isImage)
        //    {
        //        if (type == "VideoEditor")
        //        {

        //            mpGrid.Height = 405;
        //            lowerGrid.Visibility = Visibility.Collapsed;
        //        }

        //        return;
        //    }
        //    if (isReplay)
        //    {
        //        replayFilePath = ReplayFilePath;
        //    }
        //    if (type == "VideoEditor")
        //    {
        //        mpGrid.Height = 380; mpGrid.Width = 500;
        //    }
        //    else if (type == "VideoPlayer")
        //    {
        //        mpGrid.Height = 550;
        //    }
        //    else if (type == "AdvancePreview")
        //    {
        //        mpGrid.Width = 850; mpGrid.Height = 715;
        //    }
        //    else
        //    {
        //        mpGrid.Height = 550;
        //    }
        //}


    }
}
