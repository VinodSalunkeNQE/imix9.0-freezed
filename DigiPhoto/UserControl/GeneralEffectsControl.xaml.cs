﻿using FrameworkHelper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
//using VisioForge.Types;

namespace DigiPhoto
{
    /// <summary>
    /// Interaction logic for GeneralEffects.xaml
    /// </summary>
    public partial class GeneralEffectsControl : UserControl
    {
        private UIElement _parent;
        private bool _result = false;
        public string LicenseKey = string.Empty;
        public string UserName = string.Empty;
        public string EmailID = string.Empty;
        public GeneralEffectsControl()
        {
            InitializeComponent();
            Visibility = Visibility.Hidden;
        }
        public void SetParent(UIElement parent)
        {
            _parent = (UIElement)parent;
        }
        private void HideHandlerDialog()
        {
            Visibility = Visibility.Collapsed;
            _parent.IsEnabled = true;
        }

        public bool ShowPanHandlerDialog()
        {
            Visibility = Visibility.Visible;
            _parent.IsEnabled = false;
            return _result;
        }
        private void btnClose_Click(object sender, RoutedEventArgs e)
        {
            chkPreview.IsChecked = false;
            HideHandlerDialog();

        }

        private void UserControl_Unloaded(object sender, RoutedEventArgs e)
        {
            UnsubscribeEvents();
        }
        private void UnsubscribeEvents()
        {
            btnClose.Click -= new RoutedEventHandler(btnClose_Click);
        }
        private void chkPreview_Checked(object sender, RoutedEventArgs e)
        {
            if (chkPreview.IsChecked == false)
            {
                //if (VisioPreviewMediaPlayer != null && VisioPreviewMediaPlayer.Status == VFMediaPlayerStatus.Play)
                //{
                //    VisioPreviewMediaPlayer.Stop();
                //}
            }
            else if (chkPreview.IsChecked == true && radioStandardPreview.IsChecked == true)
            {
                if (string.IsNullOrEmpty(FileName))
                {
                    MessageBox.Show("Please Select the file.");
                }
                else
                {
                    PlayFile(FileName);
                }
            }
            else if (chkPreview.IsChecked == true && radioTemplatePreview.IsChecked == true)
            {
                if (string.IsNullOrEmpty(FileName))
                {
                    MessageBox.Show("Please Select the file.");
                }
                else
                {
                    PlayFile(FileName);
                }
            }
        }

        private void radioStandardPreview_Checked(object sender, RoutedEventArgs e)
        {
            //if (radioStandardPreview.IsChecked == true)
            //{
            //    if (VisioPreviewMediaPlayer != null && VisioPreviewMediaPlayer.Status == VFMediaPlayerStatus.Play)
            //    {
            //        VisioPreviewMediaPlayer.Stop();
            //    }
            //    FileName = @"C:\Users\Public\Videos\Sample Videos\Wildlife.wmv";
            //    chkPreview.IsChecked = false;
            //}
        }
        private readonly Microsoft.Win32.OpenFileDialog openFileDialog = new Microsoft.Win32.OpenFileDialog();
        string FileName = "";
        private void radioTemplatePreview_Checked(object sender, RoutedEventArgs e)
        {
            if (radioTemplatePreview.IsChecked == true)
            {
                //if (VisioPreviewMediaPlayer != null && VisioPreviewMediaPlayer.Status == VFMediaPlayerStatus.Play)
                //{
                //    VisioPreviewMediaPlayer.Stop();
                //}
                ////Open File Dialog
                if (openFileDialog.ShowDialog() == true)
                {
                    FileName = openFileDialog.FileName;
                    chkPreview.IsChecked = false;
                }
            }
            else
            {
                FileName = "";
            }
        }
        private void PlayFile(string fileName)
        {
            //try
            //{

            //    VisioPreviewMediaPlayer.SetLicenseKey(LicenseKey, UserName, EmailID);
            //    VisioPreviewMediaPlayer.Video_Renderer = VFVideoRendererWPF.WPF;
            //    VisioPreviewMediaPlayer.Custom_Video_Decoder = string.Empty;
            //    VisioPreviewMediaPlayer.Custom_Splitter = string.Empty;
            //    VisioPreviewMediaPlayer.FilenamesOrURL.Capacity = 1;
            //    VisioPreviewMediaPlayer.FilenamesOrURL.Add(fileName);
            //    VisioPreviewMediaPlayer.Video_Effects_Clear();
            //    VisioPreviewMediaPlayer.Audio_Play = true;
            //    VisioPreviewMediaPlayer.Video_Effects_Enabled = true;
            //    //Find File Extension 
            //    string extension1 = System.IO.Path.GetExtension(fileName);
            //    if (extension1.ToLower().Equals(".wmv"))
            //    {
            //        VisioPreviewMediaPlayer.Source_Mode = VFMediaPlayerSource.MMS_WMV_DS;
            //        VisioPreviewMediaPlayer.Custom_Video_Decoder = String.Empty;
            //    }
            //    else if (extension1.ToLower().Equals(".mp4") || extension1.ToLower().Equals(".mov") || extension1.ToLower().Equals(".3gp"))
            //    {
            //        VisioPreviewMediaPlayer.Source_Mode = VFMediaPlayerSource.File_DS;
            //        //VisioPreviewMediaPlayer.Source_Mode = VFMediaPlayerSource.HTTP_RTSP_FFMPEG;
            //        //VisioPreviewMediaPlayer.Custom_Video_Decoder = String.Empty;
            //    }
            //    else if (extension1.ToLower().Equals(".flv"))
            //    {
            //        VisioPreviewMediaPlayer.Source_Mode = VFMediaPlayerSource.HTTP_RTSP_FFMPEG;
            //        VisioPreviewMediaPlayer.Custom_Video_Decoder = "";
            //    }
            //    else if (extension1.ToLower().Equals(".avi"))
            //    {
            //        VisioPreviewMediaPlayer.Source_Mode = VFMediaPlayerSource.File_DS;
            //        VisioPreviewMediaPlayer.Custom_Video_Decoder = "ffdshow Video Decoder";
            //    }
            //    VisioPreviewMediaPlayer.OnError += VisioMediaPlayer_OnError;
            //    VisioPreviewMediaPlayer.OnStop += VisioMediaPlayer_OnStop;

            //    // Other effects
            //    if (tbLightness.Value > 0)
            //    {
            //        VisioPreviewMediaPlayer.Video_Effect_Ex(11, 0, 0, true, VFVideoEffectType.Lightness, tbLightness.Value);
            //    }

            //    if (tbSaturation.Value < 255)
            //    {
            //        VisioPreviewMediaPlayer.Video_Effect_Ex(12, 0, 0, true, VFVideoEffectType.Saturation, tbSaturation.Value);
            //    }

            //    if (tbContrast.Value > 0)
            //    {
            //        VisioPreviewMediaPlayer.Video_Effect_Ex(13, 0, 0, true, VFVideoEffectType.Contrast, tbContrast.Value);
            //    }

            //    if (tbDarkness.Value > 0)
            //    {
            //        VisioPreviewMediaPlayer.Video_Effect_Ex(14, 0, 0, true, VFVideoEffectType.Darkness, tbDarkness.Value);
            //    }

            //    if (chkGrayScale.IsChecked == true)
            //    {
            //        VisioPreviewMediaPlayer.Video_Effect_Ex(15, 0, 0, chkGrayScale.IsChecked == true, VFVideoEffectType.Greyscale, 0);
            //    }

            //    if (chkInvert.IsChecked == true)
            //    {
            //        VisioPreviewMediaPlayer.Video_Effect_Ex(16, 0, 0, chkInvert.IsChecked == true, VFVideoEffectType.Invert, 0);
            //    }
            //    VisioPreviewMediaPlayer.Audio_OutputDevice_Volume_Set(0, 500);
            //    VisioPreviewMediaPlayer.Play();
            //}
            //catch (Exception ex)
            //{

            //}
        }
        //private void VisioMediaPlayer_OnError(object sender, ErrorsEventArgs e)
        //{
        //    if (VisioPreviewMediaPlayer.Status == VFMediaPlayerStatus.Play)
        //    {
        //        VisioPreviewMediaPlayer.Stop();
        //    }
        //    ErrorHandler.ErrorHandler.LogFileWrite("VisioMediaPlayer_OnError:" + e.Message);
        //}
        //private void VisioMediaPlayer_OnStop(object sender, StopEventArgs e)
        //{

        //}
        private void tbLightness_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            //if (VisioPreviewMediaPlayer != null)
            //{
            //    VisioPreviewMediaPlayer.Video_Effect_Ex(11, 0, 0, true, VFVideoEffectType.Lightness, tbLightness.Value);
            //}
        }
        private void tbSaturation_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
        //    if (VisioPreviewMediaPlayer != null)
        //    {
        //        VisioPreviewMediaPlayer.Video_Effect_Ex(12, 0, 0, true, VFVideoEffectType.Saturation, tbSaturation.Value);
        //    }
        }
        private void tbContrast_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            //if (VisioPreviewMediaPlayer != null)
            //{
            //    VisioPreviewMediaPlayer.Video_Effect_Ex(13, 0, 0, true, VFVideoEffectType.Contrast, tbContrast.Value);
            //}
        }
        private void tbDarkness_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            //if (VisioPreviewMediaPlayer != null)
            //{
            //    VisioPreviewMediaPlayer.Video_Effect_Ex(14, 0, 0, true, VFVideoEffectType.Darkness, tbDarkness.Value);
            //}
        }
        private void chkGrayScale_Checked(object sender, RoutedEventArgs e)
        {
            //if (VisioPreviewMediaPlayer != null)
            //{
            //    VisioPreviewMediaPlayer.Video_Effect_Ex(15, 0, 0, chkGrayScale.IsChecked == true, VFVideoEffectType.Greyscale, 0);
            //}
        }
        private void chkInvert_Checked(object sender, RoutedEventArgs e)
        {
            //if (VisioPreviewMediaPlayer != null)
            //{
            //    VisioPreviewMediaPlayer.Video_Effect_Ex(16, 0, 0, chkInvert.IsChecked == true, VFVideoEffectType.Invert, 0);
            //}
        }

    }
}
