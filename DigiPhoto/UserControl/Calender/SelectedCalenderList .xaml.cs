﻿using DigiPhoto.DataLayer;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Threading;
//using DigiPhoto.ViewModel;
using DigiPhoto.Common;
using DigiPhoto.DataLayer.Model;
using System.ComponentModel;
using System.Runtime.Remoting.Messaging;
using System.Threading.Tasks;
using System.Collections.Specialized;



namespace DigiPhoto.Calender
{
    public partial class SelectedCalenderList : UserControl
    {

        private int DropPhotoId = 0;
        private bool _hideRequest = false;
        private bool _intiLoad = false;
        private UIElement _parent;

        public List<LstMyItems> SelectedImage { get; set; }
        public long? SelectedMasterTemplateId { get; set; }
        public int? SelectedIndexId { get; set; }
        public List<String> PreviousImage { get; set; }
        public int maxSelectedPhotos { get; set; }
        public Boolean IsBundled { get; set; }
        public Boolean IsPackage { get; set; }
        public int ProductTypeID { get; set; }



        public static readonly DependencyProperty PrintOrderPageProperty =
        DependencyProperty.Register("PrintOrderPageList", typeof(ObservableCollection<PrintOrderPage>), typeof(SelectedCalenderList), new PropertyMetadata(new ObservableCollection<PrintOrderPage>()));

        public ObservableCollection<PrintOrderPage> PrintOrderPageList
        {
            get { return (ObservableCollection<PrintOrderPage>)GetValue(PrintOrderPageProperty); }
            set { SetValue(PrintOrderPageProperty, value); }
        }

        public static readonly DependencyProperty ItemTemplateCalenderViewProperty =
        DependencyProperty.Register("ItemTemplateCalenderView", typeof(ItemTemplateCalenderView), typeof(SelectedCalenderList), new PropertyMetadata(new ItemTemplateCalenderView()));

        public ItemTemplateCalenderView ItemTemplateCalenderView
        {
            get { return (ItemTemplateCalenderView)GetValue(ItemTemplateCalenderViewProperty); }
            set { SetValue(ItemTemplateCalenderViewProperty, value); }
        }


        public static readonly DependencyProperty MessageProperty =
        DependencyProperty.Register("Message", typeof(string), typeof(SelectedCalenderList), new UIPropertyMetadata(string.Empty));
        public string Message
        {
            get { return (string)GetValue(MessageProperty); }
            set { SetValue(MessageProperty, value); }
        }

        public SelectedCalenderList()
        {
            InitializeComponent();
            if (DesignerProperties.GetIsInDesignMode(this) == false)
            {
                ctrlSelectedCalenderImageEdit.SetParentAlbum(maingrd);
                lstSelectedImage.Items.Clear();
                foreach (var item in RobotImageLoader.PrintImages)
                {
                    lstSelectedImage.Items.Add(item);
                }
                //
                //lstCalenderGrid.SelectedIndex = 1;
                //BindImageToCalenderControlAuto(false);
                //
                lstCalenderList.SelectionChanged += lstCalenderList_SelectionChanged;
                //((INotifyCollectionChanged)lstCalenderGrid.Items).CollectionChanged += lstCalenderGrid_CollectionChanged;
                lstCalenderGrid.DataContextChanged += lstCalenderGrid_DataContextChanged;
                lstCalenderGrid.SelectionChanged += lstCalenderGrid_SelectionChanged;





            }
        }

        void lstCalenderGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (ItemTemplateCalenderView.ItemTemplateCalenderSelected != null && ItemTemplateCalenderView.ItemTemplateCalenderSelected.SubtemplateCount != null)
                if (_intiLoad && lstCalenderGrid.Items.Count == ItemTemplateCalenderView.ItemTemplateCalenderSelected.SubtemplateCount && _intiLoad)
                {
                    _intiLoad = false;
                    lstCalenderGrid.Dispatcher.Invoke(new Action(() =>
                    {
                        //btnClose_Click(object sender, RoutedEventArgs e)
                        object sender1 = new object();
                        RoutedEventArgs ergs = new RoutedEventArgs();
                        // btnAuto_Click(sender1, ergs);
                        BindImageToCalenderControlAuto(false);
                        //BindImageToCalenderControlRandom(false);
                    }));

                    ShowToClientView1();
                }




        }

        void lstCalenderGrid_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            lstCalenderGrid.SelectedIndex = 0;

            //if (ItemTemplateCalenderView.ItemTemplateCalenderSelected != null && ItemTemplateCalenderView.ItemTemplateCalenderSelected.SubtemplateCount != null)
            //    if (_intiLoad && lstCalenderGrid.Items.Count == ItemTemplateCalenderView.ItemTemplateCalenderSelected.SubtemplateCount && _intiLoad)
            //    {
            //        _intiLoad = false;
            //        lstCalenderGrid.Dispatcher.Invoke(new Action(() =>
            //        {
            //            //btnClose_Click(object sender, RoutedEventArgs e)
            //            object sender1 = new object();
            //            RoutedEventArgs ergs = new RoutedEventArgs();
            //            // btnAuto_Click(sender1, ergs);
            //            BindImageToCalenderControlAuto(false);
            //            //BindImageToCalenderControlRandom(false);
            //        }));

            //        ShowToClientView();
            //    }

        }

        //private void lstCalenderGrid_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        //{
        //    if (lstCalenderGrid.SelectedItem == null)
        //    {
        //    }
        //}

        void lstCalenderList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ItemTemplateMaster _ItemTemplateMasterSelected = lstCalenderList.SelectedItem as ItemTemplateMaster;
            ItemTemplateCalenderView.ItemTemplateCalenderSelected = _ItemTemplateMasterSelected;
            if (_ItemTemplateMasterSelected != null)
                LoginUser.MasterTemplateId = _ItemTemplateMasterSelected.Id;
            else
                LoginUser.MasterTemplateId = 1;



        }


        public void SetParentAlbum(UIElement parent)
        {
            _parent = (UIElement)parent;
        }


        /// <summary>
        /// used to show and hide the dialog
        /// </summary>
        /// <param name="message"></param>
        /// <returns></returns>
        public List<LstMyItems> ShowAlbumHandlerDialog(string message)
        {
            BindPageStrips();
            Message = message;
            Visibility = Visibility.Visible;
            _parent.IsEnabled = true;
            _hideRequest = false;
            return SelectedImage;
        }

        private void HideHandlerDialog()
        {
            _hideRequest = true;
            Visibility = Visibility.Collapsed;
            _parent.IsEnabled = true;
        }

        private void btnSubmit_Click(object sender, RoutedEventArgs e)
        {

            SelectedImage = new List<LstMyItems>();
            PreviousImage = new System.Collections.Generic.List<string>();
            PrintOrderPageList = new ObservableCollection<PrintOrderPage>();
            var _itemDetails = lstCalenderGrid.Items;
            if (this.ItemTemplateCalenderView.ItemTemplateCalenderSelected != null)
            {
                this.SelectedMasterTemplateId = this.ItemTemplateCalenderView.ItemTemplateCalenderSelected.Id;
                var masterList = lstCalenderList.Items;
                for (int i = 0; i < masterList.Count; i++)
                {
                    if ((masterList[i] as ItemTemplateMaster).Id == this.SelectedMasterTemplateId.Value)
                        SelectedIndexId = i;
                }
            }
            PrintOrderPage page = new PrintOrderPage();
            foreach (var item in _itemDetails)
            {
                var _itemDetail = item as ItemTemplateDetail;
                if (string.IsNullOrWhiteSpace(_itemDetail.AssociationPhotoFilePath) == false)
                {
                    LstMyItems itemLstMyItems = RobotImageLoader.PrintImages.Where(o => o.PhotoId == _itemDetail.AssociationPhotoId).FirstOrDefault();
                    LstMyItems itemLstMyItemDest = new LstMyItems();
                    itemLstMyItemDest.PhotoId = itemLstMyItems.PhotoId;
                    itemLstMyItemDest.Name = itemLstMyItems.Name;
                    itemLstMyItemDest.FileName = itemLstMyItems.FileName;
                    itemLstMyItemDest.FilePath = itemLstMyItems.FilePath.Replace("Thumbnails", "Thumbnails_Big");
                    itemLstMyItemDest.ItemTemplateHeaderId = _itemDetail.MasterTemplateId;
                    itemLstMyItemDest.ItemTemplateDetailId = _itemDetail.Id;
                    if (_itemDetail.FileOrder > 0)
                        itemLstMyItemDest.PageNo = _itemDetail.FileOrder;

                    if (string.IsNullOrWhiteSpace(page.Name1))
                    {
                        page.PhotoId1 = _itemDetail.AssociationPhotoId;
                        page.FilePath1 = _itemDetail.AssociationPhotoFilePath;
                        page.Name1 = _itemDetail.AssociationPhotoFileName;
                        page.ItemTemplateHeaderId = _itemDetail.MasterTemplateId;
                        page.ItemTemplateDetailId = _itemDetail.Id;
                        if (_itemDetail.FileOrder > 0)
                            page.PageNo = _itemDetail.FileOrder;
                        PrintOrderPageList.Add(page);

                    }

                    itemLstMyItemDest.PhotoPrintPositionList.Add(new PhotoPrintPosition() { PageNo = _itemDetail.FileOrder });
                    // itemLstMyItems.PhotoPrintPositionList.Add(new PhotoPrintPosition() { PageNo = _itemDetail.FileOrder.Value  });
                    SelectedImage.Add(itemLstMyItemDest);
                }

            }


            if (SelectedImage != null && SelectedImage.Count == this.ItemTemplateCalenderView.ItemTemplateCalenderSelected.SubtemplateCount)
            {
                HideHandlerDialog();
            }
            else
            {
                int selected = 0;
                int expected = 0;
                if (SelectedImage != null)
                    selected = SelectedImage.Count;
                if (this.ItemTemplateCalenderView != null && this.ItemTemplateCalenderView.ItemTemplateCalenderSelected != null && this.ItemTemplateCalenderView.ItemTemplateCalenderSelected.SubtemplateCount > 0)
                    expected = this.ItemTemplateCalenderView.ItemTemplateCalenderSelected.SubtemplateCount;

                MessageBox.Show(string.Format("The calender template needs more images to print." + Environment.NewLine + " Selected no of images '{1}' / Expected no of images '{0}'." + Environment.NewLine + " Please select more images before submit.", expected, selected), "Calender Template", MessageBoxButton.OK);

            }
        }

        private void btnSubmitCancel_Click(object sender, RoutedEventArgs e)
        {
            //lstCalenderGrid.DataContext = null;
            //lstCalenderGrid.UpdateLayout();
            //  lstCalenderGrid.Items.Clear();
            // imgCalenderHolderGrid.ite
            //lstCalenderGrid.Items.Clear();
            //  this.ItemTemplateCalenderView.LstMyItems.Clear();
            //lstCalenderGrid.DataContext = this.ItemTemplateCalenderView;
            //lstCalenderGrid.
            if (lstCalenderGrid != null && lstCalenderGrid.Items.Count > 0)
            {
                for (int i = 0; i < lstCalenderGrid.Items.Count; i++)
                {
                    // lstCalenderGrid.DataContext = new object();c
                    ListBoxItem myListBoxItem = (ListBoxItem)(lstCalenderGrid.ItemContainerGenerator.ContainerFromIndex(i));
                    ContentPresenter myContentPresenter = FindVisualChild<ContentPresenter>(myListBoxItem);
                    if (myContentPresenter != null)
                    {
                        DataTemplate myDataTemplate = myContentPresenter.ContentTemplate;
                        Grid itemGrid = (Grid)myDataTemplate.FindName("imgCalenderHolderGrid", myContentPresenter);

                        object imgObject = ((Grid)itemGrid).FindName("imgCalenderHolder");
                        if (imgObject != null)
                        {
                            (imgObject as Image).Source = null;
                        }

                    }
                }
            }


            //lstCalenderGrid.ItemsSource =obj.ItemTemplateCalenderSelected.ItemTemplateDetailList;// new ite ItemTemplateDetailList  new ItemTemplateCalenderView().ItemTemplateCalenderSelected.ItemTemplateDetailList;// ItemTemplateCalenderSelected.ItemTemplateDetailList
            //lstCalenderGrid.DataContext = null;
            //  ResetPageStrip();
            //SelectedImage = new List<LstMyItems>();
        }

        private void ResetPageStrip()
        {
            //  PrintOrderPageList = new ObservableCollection<PrintOrderPage>();
            //this.DropPhotoId = 0;
            if (SelectedImage != null && this.ItemTemplateCalenderView != null && this.ItemTemplateCalenderView.ItemTemplateCalenderSelected != null && SelectedImage.Count == this.ItemTemplateCalenderView.ItemTemplateCalenderSelected.SubtemplateCount)
            {
                //SelectedImage = null;
                HideHandlerDialog();
            }
            else
            {
                int selected = 0;
                int expected = 0;
                if (SelectedImage != null)
                    selected = SelectedImage.Count;
                if (this.ItemTemplateCalenderView != null && this.ItemTemplateCalenderView.ItemTemplateCalenderSelected != null && this.ItemTemplateCalenderView.ItemTemplateCalenderSelected.SubtemplateCount > 0)
                    expected = this.ItemTemplateCalenderView.ItemTemplateCalenderSelected.SubtemplateCount;

                //MessageBoxResult result = MessageBox.Show(string.Format("The calender template needs more images to print." + Environment.NewLine + " Selected no of images '{1}' / Expected no of images '{0}'." + Environment.NewLine + " Do you want to cancel ?", expected, selected), "Calender Template", MessageBoxButton.YesNo);
                // if (result == MessageBoxResult.Yes)
                HideHandlerDialog();
            }
        }

        private void btnClose_Click(object sender, RoutedEventArgs e)
        {
            if (SelectedImage != null && this.ItemTemplateCalenderView != null && this.ItemTemplateCalenderView.ItemTemplateCalenderSelected != null && SelectedImage.Count == this.ItemTemplateCalenderView.ItemTemplateCalenderSelected.SubtemplateCount)
            {
                //SelectedImage = null;
                HideHandlerDialog();
            }
            else
            {
                int selected = 0;
                int expected = 0;
                if (SelectedImage != null)
                    selected = SelectedImage.Count;
                if (this.ItemTemplateCalenderView != null && this.ItemTemplateCalenderView.ItemTemplateCalenderSelected != null && this.ItemTemplateCalenderView.ItemTemplateCalenderSelected.SubtemplateCount > 0)
                    expected = this.ItemTemplateCalenderView.ItemTemplateCalenderSelected.SubtemplateCount;

                //MessageBoxResult result = MessageBox.Show(string.Format("The calender template needs more images to print." + Environment.NewLine + " Selected no of images '{1}' / Expected no of images '{0}'." + Environment.NewLine + " Do you want to cancel ?", expected, selected), "Calender Template", MessageBoxButton.YesNo);
                //if (result == MessageBoxResult.Yes)
                HideHandlerDialog();
            }
        }

        private void Image_PreviewMouseDown(object sender, EventArgs e)
        {
            LstMyItems lstMyItems = (LstMyItems)(sender as Image).DataContext;
            this.DropPhotoId = lstMyItems.PhotoId;
            DragDrop.DoDragDrop((DependencyObject)sender, ((Image)sender).Source, DragDropEffects.Copy);
        }

        private void Selected_Image_Drop(object sender, DragEventArgs e)
        {
            LstMyItems lstMyItem = RobotImageLoader.PrintImages.Where(o => o.PhotoId == this.DropPhotoId).FirstOrDefault();
            string filePath = lstMyItem.FilePath.Replace("Thumbnails", "Thumbnails_Big");
            if (System.IO.File.Exists(filePath) == false)
            {
                ErrorHandler.ErrorHandler.LogFileWrite(string.Format("Calender Template : File not found \"{0}\"", filePath));

            }
            else
            {
                SetCalenderImage(sender as Grid, filePath);
                ItemTemplateDetail destination = ((Grid)sender).DataContext as ItemTemplateDetail;
                ItemTemplateCalenderView.CopyMyListItem(lstMyItem, destination);
            }
        }



        /// <summary>
        /// building and setting the image
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="lstMyItem"></param>
        private void SetCalenderImage(Grid sender, string fileName)
        {
            if (System.IO.File.Exists(fileName) == false)
            {
                ErrorHandler.ErrorHandler.LogFileWrite(string.Format("Calender Template : File not found \"{0}\"", fileName));
            }
            else
            {
                BitmapImage bitMapImg = new BitmapImage();
                bitMapImg.BeginInit();
                bitMapImg.UriSource = new Uri(fileName, UriKind.Relative);
                bitMapImg.DecodePixelWidth = 200;
                bitMapImg.EndInit();
                TransformedBitmap tb = new TransformedBitmap();
                tb.BeginInit();
                tb.Source = bitMapImg;
                tb.EndInit();

                object imgObject = ((Grid)sender).FindName("imgCalenderHolder");
                if (imgObject != null)
                {
                    (imgObject as Image).Source = tb;
                }
            }

        }

        private void SetCalenderImageAsync(Grid sender, string fileName)
        {
            SetCalenderImageHandler _SetCalenderImageHandler = new SetCalenderImageHandler(SetCalenderImage);
            IAsyncResult result = _SetCalenderImageHandler.BeginInvoke(sender, fileName, new AsyncCallback(SetCalenderImageCallback), null);
        }

        delegate void SetCalenderImageHandler(Grid sender, string fileName);

        void SetCalenderImageCallback(IAsyncResult result)
        {
            AsyncResult _result = result as AsyncResult;
            SetCalenderImageHandler _SetCalenderImageHandler = _result.AsyncDelegate as SetCalenderImageHandler;
            _SetCalenderImageHandler.EndInvoke(result);
        }

        private void BindPageStrips()
        {
            if (this.ItemTemplateCalenderView != null)
            {
                //this.ItemTemplateCalenderView.Dispose();
                //this.ItemTemplateCalenderView = new ItemTemplateCalenderView();
            }
            bool isFirstOpen = false;
            if (SelectedImage == null || SelectedImage.Count == 0)
            {
                isFirstOpen = true;
                this.ItemTemplateCalenderView.OperationDoneEvent += ItemTemplateCalenderView_OperationDoneEvent;
                this.ItemTemplateCalenderView.LoadCalenderTemplate(LoginUser.ItemCalenderPath);
                this.DataContext = this.ItemTemplateCalenderView;

            }

            if (isFirstOpen)
            {
                isFirstOpen = false;
                _intiLoad = true;
                lstCalenderList.SelectedIndex = 0;

                ItemTemplatePrintOrder.GetCalenderPages(111, LoginUser.DigiFolderThumbnailPath, LoginUser.ItemCalenderPath);
            }
            else
            {
                //if (this.SelectedIndexId.HasValue)
                //{
                //    lstCalenderList.SelectedIndex = this.SelectedIndexId.Value;
                //    foreach (var calender in this.ItemTemplateCalenderView.ItemTemplateCalender)
                //    {
                //        if (calender.Id == this.SelectedMasterTemplateId.Value)
                //        {
                //            this.ItemTemplateCalenderView.ItemTemplateCalenderSelected = calender;
                //        }
                //    }
                //}
            }
        }

        void ItemTemplateCalenderView_OperationDoneEvent(ItemTemplateCalenderView sender, CalenderViewOperationType operation)
        {
            if (operation == CalenderViewOperationType.LoadCalenderDetailData)
            {
            }
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            ShowToClientView1();
        }

        public void ShowToClientView1()
        {
            try
            {

                VisualBrush CB = new VisualBrush(grdMainView);
                SearchResult _objSearchResult = null;
                //foreach (Window wnd in Application.Current.Windows)
                //{
                //    if (wnd.Title == "View/Order Station")
                //    {
                //        _objSearchResult = (SearchResult)wnd;
                //    }
                //}
                if (_objSearchResult == null)
                    _objSearchResult = new SearchResult();
                //SearchResult objSearchResult = new SearchResult();
                _objSearchResult.CompileEffectChanged(CB, -1);

            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }

        private void btnRandom_Click(object sender, RoutedEventArgs e)
        {
            BindImageToCalenderControlRandom(true);
        }

        private void btnAuto_Click(object sender, RoutedEventArgs e)
        {
            if (ItemTemplateCalenderView != null && ItemTemplateCalenderView.ItemTemplateCalenderSelected != null && ItemTemplateCalenderView.ItemTemplateCalenderSelected.SubtemplateCount > 0)
            {
                Random rand = new Random(DateTime.Now.Millisecond);
                List<int> indexes = new List<int>();
                int totalAvailable = RobotImageLoader.PrintImages.Count;

                int CalenderItemCount = lstCalenderGrid.Items.Count;
                for (int i = 0; i < CalenderItemCount; i++)
                {
                    int selectedIndex = (i % totalAvailable);
                    BindImageToCalenderControl(i, selectedIndex, true);
                }
            }
        }

        private void BindImageToCalenderControlAuto(bool isneedtobindimage)
        {
            if (ItemTemplateCalenderView != null && ItemTemplateCalenderView.ItemTemplateCalenderSelected != null && ItemTemplateCalenderView.ItemTemplateCalenderSelected.SubtemplateCount > 0)
            {
                Random rand = new Random(DateTime.Now.Millisecond);
                List<int> indexes = new List<int>();
                int totalAvailable = RobotImageLoader.PrintImages.Count;
                if (totalAvailable <= 0)
                {
                    MessageBox.Show("please Select Images");
                    return;
                }

                int CalenderItemCount = lstCalenderGrid.Items.Count;
                for (int i = 0; i < CalenderItemCount; i++)
                {
                    int selectedIndex = (i % totalAvailable);
                    BindImageToCalenderControl(i, selectedIndex, isneedtobindimage);
                }
            }
        }

        private void BindImageToCalenderControlRandom(bool isneedtobindimage)
        {
            if (ItemTemplateCalenderView != null && ItemTemplateCalenderView.ItemTemplateCalenderSelected != null && ItemTemplateCalenderView.ItemTemplateCalenderSelected.SubtemplateCount > 0)
            {
                Random rand = new Random(DateTime.Now.Millisecond);
                List<int> indexes = new List<int>();
                int totalAvailable = RobotImageLoader.PrintImages.Count;

                int CalenderItemCount = lstCalenderGrid.Items.Count;
                for (int i = 0; i < CalenderItemCount; i++)
                {
                    int selectedIndex = GetAvailablepool(indexes, totalAvailable, rand);
                    BindImageToCalenderControl(i, selectedIndex, true);
                }
            }
        }

        private void BindImageToCalenderControl(int i, int selectedIndex, bool isneedtobindimage)
        {
            LstMyItems lstMyItem = RobotImageLoader.PrintImages[selectedIndex];
            ListBoxItem myListBoxItem = (ListBoxItem)(lstCalenderGrid.ItemContainerGenerator.ContainerFromIndex(i));
            ContentPresenter myContentPresenter = FindVisualChild<ContentPresenter>(myListBoxItem);
            if (isneedtobindimage)
            {
                if (myContentPresenter != null)
                {
                    DataTemplate myDataTemplate = myContentPresenter.ContentTemplate;
                    Grid itemGrid = (Grid)myDataTemplate.FindName("imgCalenderHolderGrid", myContentPresenter);
                    ItemTemplateDetail destination = (myListBoxItem).DataContext as ItemTemplateDetail;
                    string filePath = lstMyItem.FilePath.Replace("Thumbnails", "Thumbnails_Big");

                    if (System.IO.File.Exists(filePath) == false)
                    {
                        ErrorHandler.ErrorHandler.LogFileWrite(string.Format("Calender Template : File not found \"{0}\"", filePath));
                    }
                    else
                    {
                        if (isneedtobindimage)
                            SetCalenderImage(itemGrid, filePath);
                        ItemTemplateCalenderView.CopyMyListItem(lstMyItem, destination);
                    }
                }
            }
            else
            {
                ItemTemplateDetail destination = (myListBoxItem).DataContext as ItemTemplateDetail;
                ItemTemplateCalenderView.CopyMyListItem(lstMyItem, destination);
            }
        }


        private void imgCalenderHolderGrid_Click(object sender, RoutedEventArgs e)
        {
            ctrlSelectedCalenderImageEdit.ItemTemplateCalenderView = this.ItemTemplateCalenderView;
            ctrlSelectedCalenderImageEdit.ItemTemplateCalenderDetail = ((Grid)sender).DataContext as ItemTemplateDetail;
            ctrlSelectedCalenderImageEdit.Visibility = Visibility.Visible;

            ctrlSelectedCalenderImageEdit.ShowRequest();

            //  ctrlSelectedCalenderImageEdit.SetParentAlbum(this);
            //ctrlSelectedCalenderImageEdit.ShowRequest();
        }

        public int GetAvailablepool(List<int> list, int max, Random rand)
        {
            int result = -1;
            if (list != null && list.Count > 0)
            {
                int index = rand.Next(list.Count - 1);
                result = list[index];
                list.RemoveAt(index);
            }
            else
            {
                for (int i = 0; i < max; i++)
                {
                    list.Add(i);
                }

                for (int i = 0; i < max; i++)
                {
                    int indexSuffle = rand.Next(list.Count - 1);
                    int resultSuffle = list[indexSuffle];
                    list.RemoveAt(indexSuffle);
                    list.Add(resultSuffle);
                }

                int index = rand.Next(list.Count - 1);
                result = list[index];
                list.RemoveAt(index);
            }
            return result;
        }

        private childItem FindVisualChild<childItem>(DependencyObject obj) where childItem : DependencyObject
        {
            for (int i = 0; i < VisualTreeHelper.GetChildrenCount(obj); i++)
            {
                DependencyObject child = VisualTreeHelper.GetChild(obj, i);
                if (child != null && child is childItem)
                    return (childItem)child;
                else
                {
                    childItem childOfChild = FindVisualChild<childItem>(child);
                    if (childOfChild != null)
                        return childOfChild;
                }
            }
            return null;
        }

        private void imgCalenderHolderGrid_Loaded(object sender, RoutedEventArgs e)
        {
            Grid itemGrid = (Grid)sender;
            ItemTemplateDetail destination = (itemGrid).DataContext as ItemTemplateDetail;
            string filePath = destination.AssociationPhotoFilePath;
            if (string.IsNullOrWhiteSpace(destination.AssociationPhotoFilePath) == false)
            {
                if (System.IO.File.Exists(filePath) == false)
                {
                    ErrorHandler.ErrorHandler.LogFileWrite(string.Format("Calender Template : File not found \"{0}\"", filePath));
                }
                else
                    SetCalenderImage(itemGrid, filePath);
            }
            else
            {
                BindImageToCalenderControlAuto(true);
            }
        }

        private void UserControl_IsVisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            //  ShowToClientView1();

        }



    }


}

