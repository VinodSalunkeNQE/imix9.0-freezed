﻿using DigiPhoto.DataLayer;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Threading;
//using DigiPhoto.ViewModel;
using DigiPhoto.Common;
using DigiPhoto.DataLayer.Model;
using System.ComponentModel;
using System.Runtime.Remoting.Messaging;
using System.Threading.Tasks;

namespace DigiPhoto.Calender
{
    public partial class SelectedCalenderImageEdit : UserControl
    {
        public static readonly DependencyProperty ItemTemplateCalenderViewProperty =
        DependencyProperty.Register("ItemTemplateCalenderView", typeof(ItemTemplateCalenderView), typeof(SelectedCalenderImageEdit), new PropertyMetadata(new ItemTemplateCalenderView()));

        public ItemTemplateCalenderView ItemTemplateCalenderView
        {
            get { return (ItemTemplateCalenderView)GetValue(ItemTemplateCalenderViewProperty); }
            set { SetValue(ItemTemplateCalenderViewProperty, value); }
        }

        public static readonly DependencyProperty ItemTemplateCalenderDetailProperty =
        DependencyProperty.Register("ItemTemplateCalenderDetail", typeof(ItemTemplateDetail), typeof(SelectedCalenderImageEdit), new PropertyMetadata(new ItemTemplateDetail()));

        public ItemTemplateDetail ItemTemplateCalenderDetail
        {
            get { return (ItemTemplateDetail)GetValue(ItemTemplateCalenderDetailProperty); }
            set { SetValue(ItemTemplateCalenderDetailProperty, value); }
        }


        public SelectedCalenderImageEdit()
        {
            InitializeComponent();
            if (DesignerProperties.GetIsInDesignMode(this) == false)
            {
                this.IsVisibleChanged += SelectedCalenderImageEdit_IsVisibleChanged;

            }
        }

        void SelectedCalenderImageEdit_IsVisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            if (this.Visibility == System.Windows.Visibility.Visible)
            {
                //GridLength row = grdImageEditOuter.RowDefinitions[0].Height;
                //GridLength column = grdImageEditOuter.ColumnDefinitions[1].Width;
                //double gridWidth = imageCanvas.ActualWidth;
                //double desiredHeigth = (gridWidth / 6.0) * 4.0; 
                ////grdImageEditOuter.RowDefinitions[0].Height = new GridLength(1, GridUnitType.Star);
                //grdImageEditOuter.RowDefinitions[1].Height = new GridLength(desiredHeigth, GridUnitType.Pixel);
                //grdImageEditOuter.RowDefinitions[2].Height = new GridLength(desiredHeigth , GridUnitType.Pixel);
                //Grid.SetColumn(upperImage, 0);
                //Grid.SetColumnSpan(upperImage, 3);
                BuildImages();
                //CalcImageSize();
                // ShowToClientView();
            }
        }
        private void ShowToClientView()
        {
            try
            {

                VisualBrush CB = new VisualBrush(grdCalenderPreview);
                SearchResult _objSearchResult = null;
                //foreach (Window wnd in Application.Current.Windows)
                //{
                //    if (wnd.Title == "View/Order Station")
                //    {
                //        _objSearchResult = (SearchResult)wnd;
                //    }
                //}
                if (_objSearchResult == null)
                    _objSearchResult = new SearchResult();
                //SearchResult objSearchResult = new SearchResult();
                _objSearchResult.CompileEffectChanged(CB, -1);

            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }

        private void CalcImageSize()
        {
            /*
            Rectangle rect = GetLargestRectangle(new Rectangle() { Width = imageCanvas.ActualWidth, Height = imageCanvas.ActualHeight }, 6, 8);
            
            upperImage.Height = double.NaN;
            upperImage.Width = rect.Width;

            if (upperImage.ActualHeight< (rect.Height / 2))
            {                
                upperImage.Height = rect.Height / 2;
                upperImage.Width = double.NaN;
                double left = upperImage.ActualWidth - rect.Width;
                Canvas.SetTop(upperImage, 0);
                Canvas.SetLeft(upperImage, left);
            }
            else
            {
                double top = upperImage.Height - rect.Height;
                Canvas.SetTop(upperImage, top);
                Canvas.SetLeft(upperImage, 0);
            }


            lowerImage.Height = double.NaN;
            lowerImage.Width = rect.Width;

            if (lowerImage.Height > (rect.ActualHeight / 2))
            {
                lowerImage.Height = rect.Height/2;
                lowerImage.Width = double.NaN;
            }

            Canvas.SetLeft(lowerImage, 0);
            Canvas.SetTop(lowerImage, upperImage.ActualHeight );
             * */
        }


        public Rectangle GetLargestRectangle(Rectangle rect, int xratio, int yratio)
        {
            Rectangle result = new Rectangle();

            double height = (rect.Width / xratio) * yratio;
            double width = (rect.Height / yratio) * xratio;

            if (height > rect.Height)
            {
                result.Width = width;
                result.Height = rect.Height;
            }
            else
            {
                result.Width = rect.Width;
                result.Height = height;
            }
            return result;
        }

        private void BuildImages()
        {
            //grdImageEditOuter.RowDefinitions[0].Height = new GridLength(50, GridUnitType.Pixel);
            //grdImageEditOuter.RowDefinitions[1].Height = new GridLength(1, GridUnitType.Star);
            //grdImageEditOuter.RowDefinitions[2].Height = new GridLength(1, GridUnitType.Star);


            //grdImageEditOuter.ColumnDefinitions[0].Width = new GridLength(25, GridUnitType.Star);
            //grdImageEditOuter.ColumnDefinitions[1].Width = new GridLength(50, GridUnitType.Star);
            //grdImageEditOuter.ColumnDefinitions[2].Width = new GridLength(25, GridUnitType.Star);

            if (ItemTemplateCalenderDetail != null)
            {
                lowerImage.Source = GetBitmapImage(System.IO.Path.Combine(LoginUser.ItemCalenderPath + ItemTemplateCalenderDetail.FilePath));
                upperImage.Source = GetBitmapImage(ItemTemplateCalenderDetail.AssociationPhotoFilePath);
            }

        }

        /// <summary>
        /// builds the image from the filename
        /// </summary>
        /// <param name="imagePath"></param>
        /// <returns></returns>
        private ImageSource GetBitmapImage(string imagePath)
        {
            if (string.IsNullOrWhiteSpace(imagePath) == false)
            {
                // imagePath = imagePath.Replace("\\\\", "\\");

                if (System.IO.File.Exists(imagePath) == false)
                {
                    ErrorHandler.ErrorHandler.LogFileWrite(string.Format("Calender Template : File not found \"{0}\"", imagePath));
                    return null;
                }
                else
                    return new BitmapImage(new Uri(imagePath));
            }
            return null;
        }
        //private TransformedBitmap GetBitmapImage(string imagePath)
        //{
        //    TransformedBitmap tb = null;
        //    if (string.IsNullOrWhiteSpace(imagePath) == false)
        //    {
        //        BitmapImage bitMapImg = new BitmapImage();
        //        bitMapImg.BeginInit();
        //        bitMapImg.UriSource = new Uri(imagePath, UriKind.Relative);
        //        //bitMapImg.DecodePixelWidth = 200;
        //        bitMapImg.EndInit();
        //        tb = new TransformedBitmap();
        //        tb.BeginInit();
        //        tb.Source = bitMapImg;
        //        tb.EndInit();
        //    }
        //    return tb;
        //}

        private void btnPrev_Click(object sender, RoutedEventArgs e)
        {
            var items = ItemTemplateCalenderView.ItemTemplateCalenderSelected.ItemTemplateDetailList;
            long currentId = ItemTemplateCalenderDetail.Id;
            for (int i = 0; i < items.Count; i++)
            {
                if (items[i].Id == currentId)
                {
                    if (i >= 1)
                    {
                        ItemTemplateCalenderDetail = items[i - 1];
                        BuildImages();
                        //CalcImageSize();

                    }
                }
            }

        }

        private void btnNext_Click(object sender, RoutedEventArgs e)
        {
            var items = ItemTemplateCalenderView.ItemTemplateCalenderSelected.ItemTemplateDetailList;
            long currentId = ItemTemplateCalenderDetail.Id;
            for (int i = 0; i < items.Count; i++)
            {
                if (items[i].Id == currentId)
                {
                    if ((i + 1) < items.Count)
                    {
                        ItemTemplateCalenderDetail = items[i + 1];
                        BuildImages();
                        //CalcImageSize();
                    }
                }
            }
        }

        private void btnSubmit_Click(object sender, RoutedEventArgs e)
        {
            this.Visibility = Visibility.Hidden;
        }

        private void btnClose_Click(object sender, RoutedEventArgs e)
        {
            this.Visibility = Visibility.Hidden;
            _result = true;

            HideHandlerDialog();

        }
        private bool _result = false;
        public bool ShowRequest()
        {
            Visibility = Visibility.Visible;
            _parent.IsEnabled = false;

            _hideRequest = false;
            while (!_hideRequest)
            {
                // HACK: Stop the thread if the application is about to close
                if (this.Dispatcher.HasShutdownStarted ||
                    this.Dispatcher.HasShutdownFinished)
                {
                    break;
                }

                // HACK: Simulate "DoEvents"
                this.Dispatcher.Invoke(
                    DispatcherPriority.Background,
                    new ThreadStart(delegate { }));
                Thread.Sleep(20);
            }

            return _result;
        }
        private bool _hideRequest = false;

        private UIElement _parent;

        private void HideHandlerDialog()
        {
            _hideRequest = true;
            Visibility = Visibility.Hidden;
            _parent.IsEnabled = true;
        }
        public void SetParentAlbum(UIElement parent)
        {
            _parent = (UIElement)parent;
        }

        private void btnSubmitCancel_Click(object sender, RoutedEventArgs e)
        {
            this.Visibility = Visibility.Hidden;
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {

        }

        private childItem FindVisualChild<childItem>(DependencyObject obj) where childItem : DependencyObject
        {
            for (int i = 0; i < VisualTreeHelper.GetChildrenCount(obj); i++)
            {
                DependencyObject child = VisualTreeHelper.GetChild(obj, i);
                if (child != null && child is childItem)
                    return (childItem)child;
                else
                {
                    childItem childOfChild = FindVisualChild<childItem>(child);
                    if (childOfChild != null)
                        return childOfChild;
                }
            }
            return null;
        }

    }


}

