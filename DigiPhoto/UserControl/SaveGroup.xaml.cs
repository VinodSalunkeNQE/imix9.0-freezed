﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using DigiPhoto.DataLayer;
using DigiPhoto.Common;
using DigiPhoto.IMIX.Model;
using DigiPhoto.IMIX.Business;
using FrameworkHelper;
using DigiPhoto.Utility.Repository.Data;
using System.Linq;

namespace DigiPhoto
{
    /// <summary>
    /// Interaction logic for SaveGroup.xaml
    /// </summary>
    public partial class SaveGroup : UserControl, IDisposable
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SaveGroup"/> class.
        /// </summary>
        public bool overwrite = false;
        //////changed by latika for Merge Two Image Groups in Imix 
        private Dictionary<string, Int32> _locationList;
        List<GroupInfo> GroupInfoList = null;

        public Dictionary<string, Int32> LocationList
        {
            get { return _locationList; }
            set { _locationList = value; }
        }
        ///end
        public SaveGroup()
        {
            this.InitializeComponent();
            ClearControls();
            FillSubstore();
            GetGroupList();////changed by latika for Merge Two Image Groups in Imix 



        }

        public void Dispose()
        {
            Window wnd = Window.GetWindow(this);
            if (wnd != null)
            {
                ((SearchResult)wnd).grdCotrol.Children.Remove(this);
                ((SearchResult)wnd).ModalDialog = null;
            }
            GC.SuppressFinalize(this);
        }
        Dictionary<string, string> lstSubStore;
        private void FillSubstore()
        {
            try
            {

            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }


        private void btnAdd_Click(object sender, RoutedEventArgs e)
        {


        }
        public event EventHandler ExecuteParentMethod;
        protected virtual void OnExecuteMethodML()
        {
            if (ExecuteParentMethod != null) ExecuteParentMethod(this, EventArgs.Empty);
        }
        private void btnClose_Click(object sender, RoutedEventArgs e)
        {

            this.Visibility = Visibility.Collapsed;
            OnExecuteMethodML();

        }
        public event EventHandler ExecuteMethod;
        public event EventHandler ExecuteGroupMethod;

        protected virtual void OnExecuteGroupMethod()
        {
            if (ExecuteGroupMethod != null) ExecuteGroupMethod(this, EventArgs.Empty);
        }
        protected virtual void OnExecuteMethod()
        {
            if (ExecuteMethod != null) ExecuteMethod(this, EventArgs.Empty);
        }

        private void btnClear_Click(object sender, RoutedEventArgs e)
        {
            OnExecuteMethod();
            OnExecuteMethodML();
            this.Visibility = Visibility.Collapsed;

            Dispose();
        }
        //DigiPhotoDataServices _objdblayer = new DigiPhotoDataServices();


        private void AddGroup()
        {
            try
            {
                ////changed by latika for Merge Two Image Groups in Imix 
                string MergeGroupName = GetSelectedDevices();

                Dictionary<string, string> _objphotoId = new Dictionary<string, string>();
                foreach (var item in RobotImageLoader.GroupImages)
                {
                    _objphotoId.Add(item.PhotoId.ToString(), item.Name);
                }
                // _objdblayer.SaveGroupData(_objphotoId, txtGroupName.Text, Convert.ToInt32(cmbSubStoreName.SelectedValue));
                (new PhotoBusiness()).SaveGroupData(_objphotoId, txtGroupName.Text, Convert.ToInt32(cmbSubStoreName.SelectedValue));
                MessageBox.Show("Group Saved Successfully!");
                RobotImageLoader.SearchCriteria = "Group";
                RobotImageLoader.GroupId = txtGroupName.Text;
                RobotImageLoader.LoadImages();
                OnExecuteGroupMethod();
                this.Visibility = Visibility.Collapsed;
                SaveButton.IsDefault = false;

                if (!string.IsNullOrEmpty(MergeGroupName))
                {
                    int RetVal = new PhotoBusiness().SaveGroupMerge(MergeGroupName, txtGroupName.Text);
                    if (RetVal > 0)
                    {
                        MessageBox.Show("Merge Group Name Saved Successfully!");
                        RobotImageLoader.SearchCriteria = "Group";
                        RobotImageLoader.GroupId = txtGroupName.Text;
                        RobotImageLoader.LoadImages();
                        OnExecuteGroupMethod();
                        this.Visibility = Visibility.Collapsed;
                        SaveButton.IsDefault = false;
                    }
                    else if (RetVal < 0)
                    {
                        MessageBox.Show("Group Name is Already Exists.");
                    }
                }
            }////end by latika 
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }
        private void SaveButton_Click(object sender, RoutedEventArgs e)
        {
            if (CheckValidationsGroup())
            {
                if (RadioStack.Visibility == Visibility.Visible)
                {
                    if (rdbAddimg.IsChecked == true)
                    {
                        AddGroup();
                        AddGroupCount(PhotBiz);
                    }
                    else
                    {
                        //_objdblayer.DeletePhotoGroup(txtGroupName.Text);
                        (new PhotoBusiness()).DeletePhotoGroupByName(txtGroupName.Text);
                        AddGroup();
                    }
                    Dispose();
                    OnExecuteMethodML();
                }
                else
                {
                    if (string.IsNullOrEmpty(txtGroupName.Text))
                    {
                        MessageBox.Show("Please enter group name");
                    }
                    else
                    {
                        //var mainGroup=_objdblayer.GetGroupName(txtGroupName.Text);
                        var mainGroup = (new PhotoBusiness()).GetGroupName(txtGroupName.Text);
                        if (mainGroup == null)
                        {
                            AddGroup();
                            ///AddGroupCount(PhotBiz);
                        }
                        else
                        {
                            cmbSubStoreName.SelectedValue = mainGroup.DG_SubstoreId.ToString();
                            tbErrorMsg.Visibility = Visibility.Visible;
                            RadioStack.Visibility = Visibility.Visible;
                        }
                        OnExecuteMethodML();
                    }
                }

            }

        }
        public void ClearControls()
        {
            Grid.SetRow(GrdDetails, 0);
            btnAdd.Visibility = Visibility.Collapsed;
            btnClear.Visibility = Visibility.Visible;
            StackSave.Visibility = Visibility.Visible;
            GrdDetails.Visibility = Visibility.Visible;
            tbErrorMsg.Visibility = Visibility.Collapsed;
            RadioStack.Visibility = Visibility.Collapsed;
            txtGroupName.Focus();
            txtGroupName.Text = string.Empty;

            List<SubStoresInfo> lstStoreSubStore = (new StoreSubStoreDataBusniess()).GetAllSubstoreName();
            CommonUtility.BindComboWithSelect<SubStoresInfo>(cmbSubStoreName, lstStoreSubStore, "DG_SubStore_Name", "DG_SubStore_pkey", 0, ClientConstant.SelectString);
            cmbSubStoreName.SelectedValue = LoginUser.SubStoreId.ToString();

            //DigiPhotoDataServices _objDataLayer = new DigiPhotoDataServices();
            //lstSubStore = new Dictionary<string, string>();
            //foreach (var item in _objDataLayer.GetSubstoreData())
            //{
            //    lstSubStore.Add(item.DG_SubStore_Name, item.DG_SubStore_pkey.ToString());
            //}
            //cmbSubStoreName.ItemsSource = lstSubStore;
            //cmbSubStoreName.SelectedValue = LoginUser.SubStoreId.ToString();
        }
        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            txtGroupName.Focus();

        }


        private bool CheckValidationsGroup()
        {
            bool retvalue = true;
            if (cmbSubStoreName.SelectedValue.ToString() == "0")
            {
                MessageBox.Show("Please enter SubStore name.");
                retvalue = false;
            }
            CheckSameGrpChecked();///chaged by latika 2019-09-27
            //else if(txtGroupName.Text==)
            return retvalue;
        }
        /// <summary>
        /// added by latika for check same group should not select 2019-10-01
        /// </summary>
        /// <returns></returns>
        public bool CheckSameGrpChecked()
        {
            bool RtnVal = false;

            for (int i = 0; i < lstGroup.Items.Count; i++)
            {
                DependencyObject obj = lstGroup.ItemContainerGenerator.ContainerFromIndex(i);
                if (obj != null)
                {
                    CheckBox rdo = FindVisualChild<CheckBox>(obj);// FindByName("rdoSelect", row) as RadioButton;
                                                                  //RadioButton rdo = FindVisualChild<RadioButton>(obj);
                    if (rdo != null && rdo.IsChecked == true && rdo.Content.ToString() == txtGroupName.Text)
                    {
                        MessageBox.Show("Should not check Same Group Name " + txtGroupName.Text + " in Merge Group.");
                        rdo.IsChecked = false;
                    }
                }
            }

            return RtnVal;

        }
        //end
        ////changed by latika for Merge Two Image Groups in Imix 
        void GetGroupList()
        {
            lstGroup.Items.Clear();
            GroupInfoList = new List<GroupInfo>();
            PhotoBusiness objDevice = new PhotoBusiness();
            GroupInfoList = objDevice.GetGroupList();///.Where(o => o.IsActive).ToList();
                                                     /// GroupInfoList.ForEach(o => o.GroupName += "(" + o.SerialNo + ")");
            lstGroup.ItemsSource = GroupInfoList;
            //for (int j = 0; j < lstGroup.Items.Count; j++)
            //{
            //    lstGroup.ScrollIntoView(lstGroup.Items[j]);
            //}
            if (lstGroup.Items.Count > 0)
            {
                lstGroup.ScrollIntoView(lstGroup.Items[lstGroup.Items.Count - 1]);
            }
        }
        string GetSelectedDevices()
        {
            try
            {
                string GroupnameS = string.Empty;
                for (int i = 0; i < lstGroup.Items.Count; i++)
                {
                    DependencyObject obj = lstGroup.ItemContainerGenerator.ContainerFromIndex(i);
                    if (obj != null)
                    {
                        CheckBox rdo = FindVisualChild<CheckBox>(obj);// FindByName("rdoSelect", row) as RadioButton;
                        //RadioButton rdo = FindVisualChild<RadioButton>(obj);
                        if (rdo != null && rdo.IsChecked == true)
                        {
                            GroupnameS += "," + rdo.Content.ToString();
                        }
                    }
                }
                if (GroupnameS.Length > 0)
                    GroupnameS = GroupnameS.Remove(0, 1);
                return GroupnameS;
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
                return string.Empty;
            }
        }
        public static childItem FindVisualChild<childItem>(DependencyObject obj)
  where childItem : DependencyObject
        {
            // Search immediate children
            for (int i = 0; i < VisualTreeHelper.GetChildrenCount(obj); i++)
            {
                DependencyObject child = VisualTreeHelper.GetChild(obj, i);
                if (child is childItem)
                    return (childItem)child;
                else
                {
                    childItem childOfChild = FindVisualChild<childItem>(child);
                    if (childOfChild != null)
                        return childOfChild;
                }
            }
            return null;
        }

        PhotographerBusiness PhotBiz = new PhotographerBusiness();
        private void AddGroupCount(PhotographerBusiness PhotBiz)
        {
            Int32 Operator_ID = LoginUser.UserId;
            var item = PhotBiz.Update_GrpCount(Operator_ID);
        }
    }
}