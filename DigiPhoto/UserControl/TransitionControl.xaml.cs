﻿using DigiPhoto.Common;
using DigiPhoto.IMIX.Model;
using DigiPhoto.Interop;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
//using VisioForge.Controls.WPF;

namespace DigiPhoto
{
    /// <summary>
    /// Interaction logic for PanControl.xaml
    /// </summary>
    public partial class TransitionControl : UserControl
    {
        private UIElement _parent;
        private bool _result = false;
        public List<TransitionProperty> tpLst = new List<TransitionProperty>();
        long EditTransitionID = 0;
        bool IsTransitionEdit = false;
        public TransitionControl()
        {
            InitializeComponent();
            Visibility = Visibility.Hidden;
          //  VideoEdit ve = new VideoEdit();
            //for (int i = 0; i < ve.Video_Transition_Names().Count; i++)
            //{
            //    cbTransitionName.Items.Add(ve.Video_Transition_Names()[i]);
            //}
            cbTransitionName.SelectedIndex = 0;
        }
        public void TransitionControlListAutoFill(List<TransitionProperty> tpLstMain)
        {
            tpLst = tpLstMain;
            DGManageTransition.ItemsSource = tpLst;
            DGManageTransition.Items.Refresh();
        }
        public void SetParent(UIElement parent)
        {
            _parent = (UIElement)parent;
        }
        private void btnClose_Click(object sender, RoutedEventArgs e)
        {
            HideHandlerDialog();
            OnExecuteMethod();
        }
        private void HideHandlerDialog()
        {
            Visibility = Visibility.Collapsed;
            _parent.IsEnabled = true;
        }
        public bool ShowPanHandlerDialog()
        {

            Visibility = Visibility.Visible;
            _parent.IsEnabled = false;
            return _result;
        }

        public event EventHandler ExecuteMethod;
        protected virtual void OnExecuteMethod()
        {
            if (ExecuteMethod != null) ExecuteMethod(this, EventArgs.Empty);
        }

        private void btnDeleteTransitionSlots_Click(object sender, RoutedEventArgs e)
        {
            Button btnSender = (Button)sender;
            long SlotId = long.Parse(btnSender.Tag.ToString());
            MessageBoxResult response = MessageBox.Show("Do you want to delete record?", "Digiphoto", MessageBoxButton.YesNo, MessageBoxImage.Question);
            if (response == MessageBoxResult.Yes)
            {
                tpLst.RemoveAll(o => o.ID == SlotId);
                MessageBox.Show("Item deleted Successfully", "DigiPhoto", MessageBoxButton.OK, MessageBoxImage.Information);
                DGManageTransition.ItemsSource = tpLst;
                DGManageTransition.Items.Refresh();
            }
        }

        string a;
        private void txtnumber_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (string.IsNullOrEmpty(((TextBox)sender).Text))

                a = "";
            else
            {
                int num = 0;
                bool success = int.TryParse(((TextBox)sender).Text, out num);
                if (success & num >= 0)
                {
                    ((TextBox)sender).Text.Trim();
                    a = ((TextBox)sender).Text;
                }
                else
                {
                    ((TextBox)sender).Text = a;
                    ((TextBox)sender).SelectionStart = ((TextBox)sender).Text.Length;
                }
            }
        }
        private bool CheckSlotValidations()
        {
            if (string.IsNullOrEmpty(edTransStartTime.Text.Trim()) || string.IsNullOrEmpty(edTransStopTime.Text.Trim()))
            {
                MessageBox.Show("Start time and end time can not be zero or null.", "DigiPhoto", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                return false;
            }
            else if (Convert.ToInt32(edTransStartTime.Text.Trim()) == Convert.ToInt32(edTransStopTime.Text.Trim()))
            {
                MessageBox.Show("Start time should be less than end time.", "DigiPhoto", MessageBoxButton.OK, MessageBoxImage.Information);
                return false;
            }

            else
            {
                return true;
            }
        }
        private bool IsNumeric(string text)
        {
            try
            {
                int output;
                return int.TryParse(text, out output);
            }
            catch
            {
                return false;
            }
        }

        private void cbTransition_Checked(object sender, RoutedEventArgs e)
        {

        }
        int i = 1;
        //private void btAddTransition_Click(object sender, RoutedEventArgs e)
        //{

        //    TransitionProperty pp = new TransitionProperty();
        //    int stopTime = 0;
        //    int startTime = 0;
        //    if (!string.IsNullOrEmpty(edTransStartTime.Text))
        //    {
        //        startTime = Convert.ToInt32(edTransStartTime.Text);
        //    }


        //    if (!string.IsNullOrEmpty(edTransStopTime.Text))
        //    {
        //        stopTime = Convert.ToInt32(edTransStopTime.Text);
        //    }

        //    if (cbTransition.IsChecked == true)
        //    {
        //        if (CheckSlotValidations())
        //        {
        //            pp.ID = i;
        //            pp.TransitionID = VideoEdit.Video_Transition_GetIDFromName(cbTransitionName.Text);
        //            pp.TransitionName = cbTransitionName.Text;
        //            pp.TransitionStartTime = Convert.ToInt32(edTransStartTime.Text);
        //            pp.TransitionStopTime = Convert.ToInt32(edTransStopTime.Text);

        //            if ((tpLst.Where(o => o.TransitionStartTime == startTime).Count() > 0))
        //            {
        //                MessageBox.Show("There is already a transition in the given time frame.", "DigiPhoto", MessageBoxButton.OK, MessageBoxImage.Exclamation);
        //                return;
        //            }

        //            if (tpLst.Count == 0)
        //            {
        //                tpLst.Add(pp);
        //            }
        //            else
        //            {
        //                TransitionProperty vs = tpLst.OrderBy(o => o.TransitionStartTime).Where(o => o.TransitionStartTime <= startTime).LastOrDefault();
        //                TransitionProperty vs1 = tpLst.OrderBy(o => o.TransitionStartTime).ToList().FirstOrDefault();
        //                if (vs != null)
        //                {
        //                    if (startTime > vs.TransitionStartTime + (vs.TransitionStopTime - vs.TransitionStartTime))
        //                    {
        //                        tpLst.Add(pp);
        //                    }
        //                    else
        //                    {
        //                        MessageBox.Show("There is already a transition in the given time frame.", "DigiPhoto", MessageBoxButton.OK, MessageBoxImage.Exclamation);
        //                    }
        //                }
        //                else if (vs1 != null)
        //                {
        //                    if (startTime + stopTime <= vs1.TransitionStartTime)
        //                    {
        //                        tpLst.Add(pp);
        //                    }
        //                    else
        //                    {
        //                        MessageBox.Show("There is already a transition in the given time frame.", "DigiPhoto", MessageBoxButton.OK, MessageBoxImage.Exclamation);
        //                    }
        //                }
        //                else
        //                {
        //                    tpLst.Add(pp);
        //                }
        //            }
        //            DGManageTransition.ItemsSource = tpLst.OrderBy(o => o.TransitionStartTime);
        //            DGManageTransition.Items.Refresh();
        //            i++;
        //        }
        //    }
        //    else
        //    {
        //        MessageBox.Show("Please enable the transition effect.", "DigiPhoto", MessageBoxButton.OK, MessageBoxImage.Information);
        //    }
        //}
        private void btAddTransition_Click(object sender, RoutedEventArgs e)
        {

            TransitionProperty pp = new TransitionProperty();
            int stopTime = 0;
            int startTime = 0;
            if (!string.IsNullOrEmpty(edTransStartTime.Text))
            {
                startTime = Convert.ToInt32(edTransStartTime.Text);
            }

            if (!string.IsNullOrEmpty(edTransStopTime.Text))
            {
                stopTime = Convert.ToInt32(edTransStopTime.Text);
            }

            if (cbTransition.IsChecked == true)
            {
                if (IsTransitionEdit)
                {
                    IsTransitionEdit = false;
                    MessageBoxResult response = MessageBox.Show("Would you like to save changes?", "Digiphoto", MessageBoxButton.YesNo, MessageBoxImage.Question);
                    if (response == MessageBoxResult.Yes)
                    {
                        pp = tpLst.Where(o => o.ID == EditTransitionID).FirstOrDefault();
                        tpLst.Remove(pp);
                        if (IsValidTransitionSlot(tpLst, startTime, stopTime))
                        {
                           // pp.TransitionID = VideoEdit.Video_Transition_GetIDFromName(cbTransitionName.Text);
                            pp.TransitionName = cbTransitionName.Text;
                            pp.TransitionStartTime = Convert.ToInt32(edTransStartTime.Text);
                            pp.TransitionStopTime = Convert.ToInt32(edTransStopTime.Text);
                            tpLst.Add(pp);
                            EditTransitionID = 0;
                        }
                        else
                            tpLst.Add(pp);
                    }
                    else
                    {
                        edTransStartTime.Text = "0";
                        edTransStopTime.Text = "0";
                        cbTransitionName.SelectedIndex = 0;
                        EditTransitionID = 0;
                    }

                }
                else
                {
                    if (IsValidTransitionSlot(tpLst, startTime, stopTime))
                    {

                        pp.ID = i;
                     //   pp.TransitionID = VideoEdit.Video_Transition_GetIDFromName(cbTransitionName.Text);
                        pp.TransitionName = cbTransitionName.Text;
                        pp.TransitionStartTime = Convert.ToInt32(edTransStartTime.Text);
                        pp.TransitionStopTime = Convert.ToInt32(edTransStopTime.Text);
                        tpLst.Add(pp);
                        i++;
                    }
                }
                DGManageTransition.ItemsSource = tpLst.OrderBy(o => o.TransitionStartTime);
                DGManageTransition.Items.Refresh();

            }
            else
            {
                MessageBox.Show("Please enable the transition effect.", "DigiPhoto", MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }

        private void UnsubscribeEvents()
        {
            cbTransition.Checked -= new RoutedEventHandler(cbTransition_Checked);
            edTransStopTime.TextChanged -= new TextChangedEventHandler(txtnumber_TextChanged);
            edTransStartTime.TextChanged -= new TextChangedEventHandler(txtnumber_TextChanged);
            btAddTransition.Click -= new RoutedEventHandler(btAddTransition_Click);
            btnClose.Click -= new RoutedEventHandler(btnClose_Click);
            if (tpLst.Count > 0)
                tpLst.Clear();
        }

        private void UserControl_Unloaded(object sender, RoutedEventArgs e)
        {
            UnsubscribeEvents();
        }

        private void btnEditTransitionSlots_Click(object sender, RoutedEventArgs e)
        {
            Button btnSender = (Button)sender;
            EditTransitionID = long.Parse(btnSender.Tag.ToString());
            TransitionProperty transition = tpLst.Where(o => o.ID == EditTransitionID).FirstOrDefault();
            edTransStartTime.Text = transition.TransitionStartTime.ToString();
            edTransStopTime.Text = transition.TransitionStopTime.ToString();
            cbTransitionName.Text = transition.TransitionName;
            IsTransitionEdit = true;
        }
        public bool IsValidTransitionSlot(List<TransitionProperty> slotList, int startTime, int stopTime)
        {
            if (string.IsNullOrEmpty(edTransStartTime.Text.Trim()) || string.IsNullOrEmpty(edTransStopTime.Text.Trim()))
            {
                MessageBox.Show("Start time and end time can not be zero or null.", "DigiPhoto", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                return false;
            }
            else if (Convert.ToInt32(edTransStartTime.Text.Trim()) == Convert.ToInt32(edTransStopTime.Text.Trim()))
            {
                MessageBox.Show("Start time should be less than end time.", "DigiPhoto", MessageBoxButton.OK, MessageBoxImage.Information);
                return false;
            }

            else
            {
                TransitionProperty firstItem = slotList.OrderBy(o => o.TransitionStartTime).FirstOrDefault();
                TransitionProperty LastItem = slotList.OrderBy(o => o.TransitionStartTime).LastOrDefault();
                if (firstItem != null)
                {
                    if (stopTime <= firstItem.TransitionStartTime)
                    {
                        return true;
                    }
                    else if (startTime >= LastItem.TransitionStopTime)
                    {
                        return true;
                    }
                    else
                    {
                        List<TransitionProperty> templst = slotList.OrderBy(o => o.TransitionStartTime).ToList();
                        TransitionProperty item = templst.Where(o => o.TransitionStopTime <= startTime).LastOrDefault();
                        TransitionProperty item2 = templst[templst.IndexOf(item) + 1];
                        if (item2 != null)
                        {
                            if (item2.TransitionStartTime >= stopTime)
                            {
                                return true;
                            }
                            else
                            {
                                MessageBox.Show("Selected effect is already applied in the given time frame.", "DigiPhoto", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                                return false;
                            }
                        }
                        else
                        {
                            MessageBox.Show("Selected effect is already applied in the given time frame.", "DigiPhoto", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                            return false;
                        }
                    }
                }
                else
                {
                    return true;
                }
            }
        }
    }


}
