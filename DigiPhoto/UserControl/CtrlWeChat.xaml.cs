﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Text.RegularExpressions;
using DigiPhoto.DataLayer;
using System.Windows.Threading;
using System.Threading;
using DigiPhoto.DataLayer.Model;
using DigiPhoto.IMIX.Business;
using System.ComponentModel;
using System.Windows.Media.Animation;
using RestSharp;
using DigiPhoto.IMIX.Model;
using System.IO;
using System.Configuration;

namespace DigiPhoto
{
    /// <summary>
    /// Interaction logic for CtrlMobileNumber.xaml
    /// </summary>
    public partial class CtrlWeChat : UserControl
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CtrlWeChat"/> class.
        /// </summary>
        public CtrlWeChat()
        {
            InitializeComponent();
            Visibility = Visibility.Collapsed;
            DataContext = this;
        }

        /// <summary>
        /// The _parent
        /// </summary>
        private UIElement _parent;
        /// <summary>
        /// The _hide request
        /// </summary>
        private bool _hideRequest = false;
        /// <summary>
        /// The _result
        /// </summary>
        private string _result;

        /// <summary>
        /// Sets the parent.
        /// </summary>
        /// <param name="parent">The parent.</param>
        public void SetParent(UIElement parent)
        {
            _parent = (UIElement)parent;
        }
        #region Message

        private int nameValue;

        public int Name1
        {
            get
            {
                return nameValue;
            }
            set
            {
                nameValue = value;
            }
        }

        /// <summary>
        /// Shows the handler dialog.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <returns></returns>
        public string ShowHandlerDialog(string message, string orderPath)
        {
            Visibility = Visibility.Visible;

            BitmapImage bitmap = new BitmapImage();
            bitmap.BeginInit();
            bitmap.UriSource = new Uri(orderPath);
            bitmap.EndInit();

            imgQRCode.Source = bitmap;

            _parent.IsEnabled = true;
            _hideRequest = false;

            while (!_hideRequest)
            {
                // HACK: Stop the thread if the application is about to close
                if (this.Dispatcher.HasShutdownStarted ||
                    this.Dispatcher.HasShutdownFinished)
                {
                    break;
                }
                // HACK: Simulate "DoEvents"
                this.Dispatcher.Invoke(
                    DispatcherPriority.Background,
                    new ThreadStart(delegate { }));
                Thread.Sleep(20);

            }
            return _result;
        }


        public string ShowHandlerDialogClient(string message, string orderPath)
        {
            Visibility = Visibility.Visible;

            BitmapImage bitmap = new BitmapImage();
            bitmap.BeginInit();
            bitmap.UriSource = new Uri(orderPath);
            bitmap.EndInit();

            imgQRCode.Source = bitmap;
            _parent.IsEnabled = true;
            _hideRequest = false;

            return _result;
        }


        /// <summary>
        /// Hides the handler dialog.
        /// </summary>
        private void HideHandlerDialog()
        {

            _hideRequest = true;
            Visibility = Visibility.Collapsed;
            _parent.IsEnabled = true;
        }
        #endregion



        /// <summary>
        /// Handles the Click event of the rdoSelect control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void rdoSelect_Click(object sender, RoutedEventArgs e)
        {

        }
        /// <summary>
        /// Finds the visual child.
        /// </summary>
        /// <typeparam name="childItem">The type of the hild item.</typeparam>
        /// <param name="obj">The object.</param>
        /// <returns></returns>
        public static childItem FindVisualChild<childItem>(DependencyObject obj)
    where childItem : DependencyObject
        {
            // Search immediate children
            for (int i = 0; i < VisualTreeHelper.GetChildrenCount(obj); i++)
            {
                DependencyObject child = VisualTreeHelper.GetChild(obj, i);

                if (child is childItem)
                    return (childItem)child;

                else
                {
                    childItem childOfChild = FindVisualChild<childItem>(child);

                    if (childOfChild != null)
                        return childOfChild;
                }
            }

            return null;
        }
        /// <summary>
        /// Handles the Click event of the btnSubmit control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void btnSubmit_Click(object sender, RoutedEventArgs e)
        {
            //if(Regex.IsMatch(TxtAmount.Text, @"^\d+$") && TxtAmount.Text.Length>8)
            //{

            //    //HideHandlerDialog();
            //    //ShowHandlerDialog("Trans");

            //    MobileNumberGrid.Visibility = System.Windows.Visibility.Hidden;
            //    SPSubmitCancel.Visibility = System.Windows.Visibility.Hidden;
            //    WhatsAppMsg1.Visibility = System.Windows.Visibility.Hidden;

            //    WhatsAppGrid.Visibility = System.Windows.Visibility.Visible;
            //    ProgressBarMy.Visibility = System.Windows.Visibility.Visible;
            //    WhatsAppMsg2.Visibility = System.Windows.Visibility.Visible;

            //    Duration duration = new Duration(TimeSpan.FromSeconds(1));
            //    DoubleAnimation doubleAnimation = new DoubleAnimation(ProgressBarMobile.Value + 100, duration);
            //    ProgressBarMobile.BeginAnimation(ProgressBar.ValueProperty, doubleAnimation);

            //    string destinationPathMobile;


            //    foreach (LstMyItems gitem in myMobileItems)
            //    {

            //        WhatsAppBusiness whatsApp = new WhatsAppBusiness();
            //        WhatsAppSettings wtsAppData = whatsApp.GetWhatsAppDetail();

            //        //destinationPathMobile = gitem.HotFolderPath + "\\MobileProduct\\" + DateTime.Now.ToString("yyyyMMdd")+"\\"+ orderNumber+"\\"+gitem.FileName;
            //        destinationPathMobile = string.Empty;
            //        destinationPathMobile = DateTime.Now.ToString("yyyyMMdd") + "/" + orderNumber + "/" + gitem.FileName;

            //        destinationPathMobile = wtsAppData.HostUrl + destinationPathMobile;
            //        string mainurl = System.Web.HttpUtility.UrlEncode(destinationPathMobile);



            //        string APIKey = wtsAppData.APIKey;
            //        string mobileNumber = TxtAmount.Text.Trim();


            //        //var client = new RestClient("https://panel.apiwha.com/send_message.php?apikey=9LIL8WR2P1QNSBHDH6K8&number=9189768990079&text=" + mainurl);

            //        var client = new RestClient("https://panel.apiwha.com/send_message.php?apikey="+ wtsAppData.APIKey+"&number="+ TxtAmount.Text.Trim()+ "&text=" + mainurl);

            //        var request = new RestRequest(Method.GET);
            //        IRestResponse response = client.Execute(request);

            //    }

            //    MessageBox.Show("Sharing is completed");

            //    HideHandlerDialog();
            //while (Name1 < 100)
            //{
            //    Thread.Sleep(1050);

            //    Name1 = Name1 + 10;

            //}

            // ProgressBarMobile.Value = 10;
            //CtrlMobileTransfer.SetParent(this);

            //CtrlMobileTransfer.btnSubmit.IsDefault = true;
            //var res = CtrlMobileTransfer.ShowHandlerDialog("Cash");

            //CtrlMobileTransfer.btnSubmit.IsDefault = false;


            //}
            //else
            //{
            //    MessageBox.Show("Please enter valid Mobile number", "DEI");
            //}




            //string Currency = string.Empty;

            //for (int i = 0; i < lstCurrency.Items.Count; i++)
            //{
            //    DependencyObject obj = lstCurrency.ItemContainerGenerator.ContainerFromIndex(i);
            //    if (obj != null)
            //    {
            //        RadioButton rdo = FindVisualChild<RadioButton>(obj);// FindByName("rdoSelect", row) as RadioButton;
            //        if (rdo != null)
            //        {
            //            if (Convert.ToBoolean(rdo.IsChecked))
            //            {
            //                Currency = rdo.Tag.ToString();
            //            }
            //        }
            //    }
            //}
            //double Amount = 0;

            //if (Currency != string.Empty)
            //{
            //    if(Double.TryParse(TxtAmount.Text, out Amount))
            //    {
            //        _result = Currency + "%#%" + Amount.ToString();
            //        HideHandlerDialog();
            //    }

            //}

        }


        /// <summary>
        /// Handles the Click event of the btnSubmitCancel control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void btnSubmitCancel_Click(object sender, RoutedEventArgs e)
        {
            //TxtAmount.Focus();
            //TxtAmount.Text = string.Empty;
        }

        /// <summary>
        /// Pastings the handler.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="DataObjectPastingEventArgs"/> instance containing the event data.</param>
        private void PastingHandler(object sender, DataObjectPastingEventArgs e)
        {
            if (e.DataObject.GetDataPresent(typeof(String)))
            {
                String text = (String)e.DataObject.GetData(typeof(String));
                if (!IsTextAllowed(text)) e.CancelCommand();
            }
            else e.CancelCommand();
        }
        /// <summary>
        /// Determines whether [is text allowed] [the specified text].
        /// </summary>
        /// <param name="text">The text.</param>
        /// <returns></returns>
        private static bool IsTextAllowed(string text)
        {
            Regex regex = new Regex("[^0-9.-]+"); //regex that matches disallowed text
            return !regex.IsMatch(text);
        }
        /// <summary>
        /// Handles the PreviewTextInput event of the txtAmountEntered control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="TextCompositionEventArgs"/> instance containing the event data.</param>
        private void txtAmountEntered_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = !IsTextAllowed(e.Text);
        }
        /// <summary>
        /// Loads the currency.
        /// </summary>
        private void LoadCurrency()
        {
            //DigiPhotoDataServices _objDataLayer = new DigiPhotoDataServices();
            //lstCurrency.ItemsSource = new CurrencyBusiness().GetCurrencyOnly();

        }

        /// <summary>
        /// Handles the Click event of the btnClose control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void btnClose_Click(object sender, RoutedEventArgs e)
        {
            _result = null;
            HideHandlerDialog();
        }

        /// <summary>
        /// Handles the Loaded event of the UserControl control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            //TxtAmount.Focusable = true;
            //TxtAmount.Focus();
            //FocusManager.SetFocusedElement(this, TxtAmount);
            //Keyboard.Focus(TxtAmount);
            //TxtAmount.Text = String.Empty;
        }



        /// <summary>
        /// Handles the GotKeyboardFocus event of the TxtAmount control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void TxtAmount_GotKeyboardFocus(object sender, RoutedEventArgs e)
        {

        }


        private void Window_ContentRendered()
        {
            BackgroundWorker worker = new BackgroundWorker();
            worker.WorkerReportsProgress = true;
            worker.DoWork += worker_DoWork;
            //worker.ProgressChanged += worker_ProgressChanged;

            worker.RunWorkerAsync();
        }

        void worker_DoWork(object sender, DoWorkEventArgs e)
        {
            //for (int i = 0; i < 100; i++)
            //{
            //    (sender as BackgroundWorker).ReportProgress(i);
            //    Thread.Sleep(1000);
            //}

            //while (ProgressBarMobile.Value < 100)
            //{
            //    Thread.Sleep(1050);

            //     ProgressBarMobile.Value = ProgressBarMobile.Value + 10;


            //}
        }

        void worker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            //for (int i = 0; i < 100; i++)
            //{
            //    ProgressBarMobile.Value++;
            //    Thread.Sleep(100);
            //}
        }

        private void ProgressBarMobile_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {


        }
    }
}
