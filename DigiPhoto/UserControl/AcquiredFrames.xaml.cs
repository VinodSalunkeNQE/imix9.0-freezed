﻿using BarcodeReaderLib;
using DigiPhoto.Common;
using DigiPhoto.DataLayer;
using DigiPhoto.DataLayer.Model;
using DigiPhoto.IMIX.Business;
using DigiPhoto.IMIX.Model;
using DigiPhoto.Interop;
using ExifLib;
using FrameworkHelper;
using LevDan.Exif;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using DigiPhoto.DataLayer;
using DigiPhoto.Common;
using System.Drawing;
using System.IO;
using DigiPhoto.DataLayer.Model;
using System.Windows.Threading;

using System.Threading;
using FrameworkHelper;
using LevDan.Exif;
using ExifLib;
using System.Linq;
using BarcodeReaderLib;
using DigiPhoto.Shader;
using System.Windows.Media;
using DigiPhoto.IMIX.Model;
using DigiPhoto.IMIX.Business;
using System.Xml;
//using VisioForge.Controls;
//using VisioForge.Controls.WPF;
//using VisioForge.Types;

namespace DigiPhoto
{
    /// <summary>
    /// Interaction logic for ManualDownload.xaml
    /// </summary>
    public partial class AcquiredFrames : UserControl
    {
        #region Declaration
        DeviceManager deviceManager = null;
        private DG_SemiOrder_Settings objDG_SemiOrder_Settings;
        //string ImageEffect;
        static bool isrotated = false;
        private String _BorderFolder;
        string Directoryname = string.Empty;
        private bool is_SemiOrder;

        /// <summary>
        /// The path
        /// </summary>

        string path = string.Empty;
        string defaultBrightness = string.Empty;
        string defaultContrast = string.Empty;
        /// <summary>
        /// The thumbnailspath
        /// </summary>
        //string thumbnailspath = string.Empty;
        /// <summary>
        /// The _parent
        /// </summary>
        private UIElement _parent;
        /// <summary>
        /// The _hide request
        /// </summary>
        private bool _hideRequest = false;
        /// <summary>
        /// The _result
        /// </summary>
        private string _result;
        /// <summary>
        /// The is automatic rotate
        /// </summary>
        Nullable<bool> IsAutoRotate;
        /// <summary>
        /// The count
        /// </summary>
        static int count = 0;
        /// <summary>
        /// The processed count
        /// </summary>
        static int ProcessedCount = 0;
        /// <summary>
        /// The _obj data layer
        /// </summary>
        //DigiPhotoDataServices _objDataLayer;
        /// <summary>
        /// The _location list
        /// </summary>
        private Dictionary<string, Int32> _locationList;
        /// <summary>
        /// The image name
        /// </summary>
        private List<MyImageClass> ImageName;
        bool IsBarcodeActive = false;
        Int32 MappingType = 0;
        Int32 scanType = 0;
        bool IsDelete = false;
        bool IsAnonymousCodeActive = false;
        string QRWebURLReplace = string.Empty;

        [System.Runtime.InteropServices.DllImport("gdi32.dll")]
        public static extern bool DeleteObject(IntPtr hObject);
        /// <summary>
        /// Gets or sets the imagelist.
        /// </summary>
        /// <value>
        /// The imagelist.
        /// </value>
        public List<MyImageClass> imagelist
        {
            get { return ImageName; }
            set { ImageName = value; }
        }
        /// <summary>
        /// Gets or sets the location list.
        /// </summary>
        /// <value>
        /// The location list.
        /// </value>
        public Dictionary<string, Int32> LocationList
        {
            get { return _locationList; }
            set { _locationList = value; }
        }
        /// <summary>
        /// The _photo grapher list
        /// </summary>
        private Dictionary<string, Int32> _photoGrapherList;
        /// <summary>
        /// Gets or sets the photo grapher list.
        /// </summary>
        /// <value>
        /// The photo grapher list.
        /// </value>
        public Dictionary<string, Int32> PhotoGrapherList
        {
            get { return _photoGrapherList; }
            set { _photoGrapherList = value; }
        }

        /// <summary>
        /// Gets or sets the image meta data.
        /// </summary>
        /// <value>
        /// The image meta data.
        /// </value>
        public string ImageMetaData
        {
            get;
            set;
        }

        public string DefaultEffects
        {
            get;
            set;
        }
        public Hashtable htVidL = new Hashtable();
        /// <summary>
        /// The thumbnailspath
        /// </summary>
        string thumbnailspath = string.Empty;
        //private readonly BarcodeReader barcodeReader;
        vw_GetConfigdata config = null;
        /// <summary>
        /// The filepath
        /// </summary>
        string filepath;
        string filepathdate;
        #endregion
        /// <summary>
        /// Initializes a new instance of the <see cref="ManualDownload"/> class.
        /// </summary>
        /// 
        #region Video Auto Processing
        string autoProVideoBackground = string.Empty;
        int maxInstant = 2;
        public static Hashtable htVideosToProcess = new Hashtable();
        public static int videoCount = 0;
        public static int ProcessedvideoCount = 0;
        public static int videothreadCount = 0;
        #endregion Video Auto Processing
        public AcquiredFrames()
        {
            try
            {
                this.InitializeComponent();
                DownloadProgress.Minimum = 0;
                count = 0;
                //   this.ImageName = img;
                //_objDataLayer = new DigiPhotoDataServices();
                btnDownload.Content = "Download";
                List<PhotoGraphersInfo> lstUsers = new List<PhotoGraphersInfo>();
                lstUsers = (new UserBusiness()).GetPhotoGraphersList(LoginUser.StoreId);
                List<LocationInfo> lstlocations = new List<LocationInfo>();
                lstlocations = (new LocationBusniess()).GetLocationList(LoginUser.StoreId);
                LocationList = new Dictionary<string, int>();
                LocationList.Add("--Select--", 0);
                PhotoGrapherList = new Dictionary<string, int>();
                PhotoGrapherList.Add("--Select--", 0);
                if (lstUsers != null)
                {
                    foreach (var item in lstUsers)
                    {
                        PhotoGrapherList.Add(item.DG_User_First_Name + " " + item.DG_User_Last_Name, item.DG_User_pkey);
                    }
                    CmbPhotographerNo.ItemsSource = PhotoGrapherList;
                    CmbPhotographerNo.SelectedValue = "0";
                }
                if (lstlocations != null)
                {
                    foreach (var item in lstlocations)
                    {
                        LocationList.Add(item.DG_Location_Name, item.DG_Location_pkey);
                    }
                    CmbLocation.ItemsSource = LocationList;
                    CmbLocation.SelectedValue = "0";
                }

                var config = (new ConfigBusiness()).GetConfigurationData(LoginUser.SubStoreId);
                QRWebURLReplace = (new LocationBusniess()).GetQRCodeWebUrl();
                filepath = config.DG_Hot_Folder_Path;
                filepathdate = Path.Combine(filepath, DateTime.Now.ToString("yyyyMMdd"));
                if (!Directory.Exists(filepathdate))
                    Directory.CreateDirectory(filepathdate);
                _BorderFolder = config.DG_Frame_Path;
                IsAutoRotate = config.DG_IsAutoRotate;
                is_SemiOrder = Convert.ToBoolean(config.DG_SemiOrderMain);
                ////Check if RFID enabled then show photographer's device as selected 
                //if (App.IsRFIDEnabled)
                //{
                //    tbDevice.Visibility = Visibility.Visible;
                //    CmbDevice.Visibility = Visibility.Visible;
                //    BindDevices();
                //}
                //else
                //{
                //    tbDevice.Visibility = Visibility.Collapsed;
                //    CmbDevice.Visibility = Visibility.Collapsed;
                //}
                //barcodeReader = new BarcodeReader
                //{
                //    AutoRotate = true,
                //    TryInverted = true,
                //    Options = new DecodingOptions { TryHarder = true }
                //};

                // VisioEditPlayer.OnStart+=       ;

                //LoadVideoAutoProcessingBackground();
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }

        //void BindDevices()
        //{
        //    try
        //    {
        //        Dictionary<int, string> deviceList = new Dictionary<int, string>();
        //        deviceManager = new DeviceManager();
        //        var devices = deviceManager.GetDeviceList().Where(o => o.IsActive);
        //        deviceList.Add(0, "--Select--");
        //        if (devices != null)
        //        {
        //            foreach (var item in devices)
        //            {
        //                deviceList.Add(item.DeviceId, item.Name+"("+item.SerialNo+")");
        //            }
        //            CmbDevice.ItemsSource = deviceList;
        //            CmbDevice.SelectedValue = "0";
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
        //        ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
        //    }
        //}

        /// <summary>
        /// Sets the parent.
        /// </summary>
        /// <param name="parent">The parent.</param>
        public void SetParent(UIElement parent)
        {
            _parent = (UIElement)parent;
        }

        /// <summary>
        /// Shows the handler dialog.
        /// </summary>
        /// <returns></returns>
        public string ShowHandlerDialog()
        {

            Visibility = Visibility.Visible;

            _hideRequest = false;
            while (!_hideRequest)
            {
                // HACK: Stop the thread if the application is about to close
                if (this.Dispatcher.HasShutdownStarted ||
                    this.Dispatcher.HasShutdownFinished)
                {
                    break;
                }
                // HACK: Simulate "DoEvents"
                this.Dispatcher.Invoke(
                    DispatcherPriority.Background,
                    new ThreadStart(delegate { }));
                Thread.Sleep(20);
            }

            return _result;
        }

        /// <summary>
        /// Hides the handler dialog.
        /// </summary>
        private void HideHandlerDialog()
        {
            try
            {
                _hideRequest = true;
                Visibility = Visibility.Collapsed;
                _parent.IsEnabled = true;
            }
            catch
            {
            }
        }
        /// <summary>
        /// Handles the Click event of the btnDownload control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void btnDownload_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (IsValidData())
                {
                    Go();
                }
                else
                {
                    MessageBox.Show("Please enter valid data");
                }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }

        }

        bool AcquireVideos(string vidFile, long PhotoNo, string PhotographerId, int locationid)
        {
            var drives = from drive in DriveInfo.GetDrives()
                         where drive.DriveType == DriveType.Removable && drive.IsReady == true
                         select drive;
            if (drives.Count() > 0)
            {
                foreach (var drivesitem in drives)
                {
                    var videoitems = Directory.EnumerateFiles(drivesitem.Name, "*.*", SearchOption.AllDirectories).Where(s => s.ToLower().EndsWith(".wmv") || s.ToLower().EndsWith(".mp4") || s.ToLower().EndsWith(".avi") || s.ToLower().EndsWith(".mov") || s.ToLower().EndsWith(".3gp") || s.ToLower().EndsWith(".3g2") || s.ToLower().EndsWith(".m2v") || s.ToLower().EndsWith(".m4v") || s.ToLower().EndsWith(".flv") || s.ToLower().EndsWith(".mpg") || s.ToLower().EndsWith("ffmpeg")).ToList();
                    var tempvid = videoitems.Where(v => v.Contains(vidFile)).FirstOrDefault();
                    if (tempvid != null)
                    {
                        string vidExtension = System.IO.Path.GetExtension(tempvid).ToLower();
                        DateTime varDate = (new CustomBusineses()).ServerDateTime();
                        string filename = varDate.Day + varDate.Month + PhotoNo + "_" + PhotographerId;
                        if ((new PhotoBusiness()).CheckPhotos(filename + vidExtension, Convert.ToInt32(PhotographerId)))
                        {
                            //CopyFile
                            string descfile = filepath + "\\Videos\\" + filename + vidExtension;
                            CopyVideo(tempvid, descfile);
                            string srcThumb = Path.GetDirectoryName(path) + "\\" + Path.GetFileName(tempvid) + ".jpg";
                            File.Copy(srcThumb, filepath + "Thumbnails\\" + filename + ".jpg", true);
                            //Extract thumbnail & resize
                            //Dispatcher.Invoke(new Action(() =>
                            //               ThumbnailExtractor.ExtractThumbnailFromVideo(tempvid.ToString(), 4, 4, filepath + "Thumbnails\\" + filename + ".jpg")
                            //             ));

                            videoCount++;

                            long vidLength = htVidL.ContainsKey(Path.GetFileName(tempvid)) ? Convert.ToInt64(htVidL[Path.GetFileName(tempvid)]) : 0;
                            //DB Entry
                            int medType = 2; //1 for images and 2 for videos

                            int lastPhotoPKey = UpdateVideoDatabase(PhotoNo, filename + vidExtension, filename + ".jpg", LoginUser.UserId, (new CustomBusineses()).ServerDateTime(), 0, medType, Convert.ToString(PhotographerId), locationid, vidLength);
                            if (ConfigManager.IMIXConfigurations.ContainsKey((int)ConfigParams.IsEnableAutoVidProcessing) && Convert.ToBoolean(ConfigManager.IMIXConfigurations[(int)ConfigParams.IsEnableAutoVidProcessing]) == true)
                            {
                                //VideoAutoProcessing(PhotoNo, descfile, vidLength, lastPhotoPKey);
                            }
                        }
                        else
                        {
                            MessageBox.Show(PhotoNo + " already exists for today.");
                            return false;
                        }
                    }
                }
            }
            return true;
        }

        int UpdateVideoDatabase(long vidRFID, string vidName, string vidThumbnail, int createdBy, DateTime dateCreated, int productId, int MediaType, string PhotographerId, int locationid, long vidLength)
        {
            return (new PhotoBusiness()).SetPhotoDetails(LoginUser.SubStoreId,vidRFID.ToString(), vidName, dateCreated, PhotographerId,
                string.Empty, locationid, null, string.Empty,
                null, 0, 1,null,vidLength,true);
            
        }

        private void CopyVideo(string source, string dest)
        {
            if (!Directory.Exists(filepath + "\\Videos"))
            {
                Directory.CreateDirectory(filepath + "\\Videos");
            }
            File.Copy(source, dest, true);
        }
        /// <summary>
        /// Goes this instance.
        /// </summary>
        void Go()
        {

            //_objDataLayer = new DigiPhotoDataServices();
            int getitem = 0;
            DateTime? CaptureDate = new DateTime();
            long PhotoNo = Convert.ToInt64(tbStartingNo.Text);
            string PhotographerId = CmbPhotographerNo.SelectedValue.ToString();
            int locationid = Convert.ToInt32(CmbLocation.SelectedValue);
            ThreadStart start = delegate()
            {
                //this is taking place on the background thread
                //ApplySettings(filename, picname, photoId, substoreId);
                string root = Environment.CurrentDirectory;
                path = root + "\\";
                path = System.IO.Path.Combine(path, "Download\\");
                thumbnailspath = System.IO.Path.Combine(path, "Temp\\");

                String s = String.Empty;

                int i = 0;

                List<MyImageClassList> lstMyImgClassList = new List<MyImageClassList>();

                MyImageClassList myImgClassList = new MyImageClassList();
                string barcode = string.Empty;
                string format = string.Empty;
                //GetNewConfigValues();
                GetNewConfigLocationValues(locationid);

                if (IsBarcodeActive)
                {
                    #region Barcode active
                    if (scanType == Convert.ToInt32(ScanType.PreScan))
                        ImageName = ImageName.Where(y => y.IsChecked == true).OrderBy(x => x.CreatedDate).ToList();
                    else if (scanType == Convert.ToInt32(ScanType.PostScan))
                        ImageName = ImageName.Where(y => y.IsChecked == true).OrderByDescending(x => x.CreatedDate).ToList();
                    for (int j = 0; j < ImageName.Count; j++)
                    {
                        if (ImageName[j].IsChecked)
                        {
                            BarcodeReaderLib.BarcodeList BarCodeList = null;
                            BarcodeReaderLib.BarcodeList QRCodeList = null;
                            BarcodeReaderLib.barcode barcodeInfo = null;
                            string imgName = ImageName[j].Title;
                            try
                            {
                                switch (MappingType)
                                {
                                    case 401:
                                        BarcodeDecoder barcodeDecoder = new BarcodeReaderLib.BarcodeDecoder();
                                        barcodeDecoder.BarcodeTypes = (int)(BarcodeReaderLib.EBarcodeTypes.QRCode);// | BarcodeReaderLib.EBarcodeTypes.QRCodeUnrecognized);
                                        barcodeDecoder.DecodeFile(thumbnailspath + imgName + ".jpg");
                                        QRCodeList = barcodeDecoder.Barcodes;
                                        if (QRCodeList.length > 0)
                                            barcodeInfo = QRCodeList.item(0);
                                        break;

                                    case 402:
                                        BarcodeDecoder barcodeDecoder1 = new BarcodeReaderLib.BarcodeDecoder();
                                        barcodeDecoder1.LinearFindBarcodes = 1;
                                        barcodeDecoder1.LinearShowSymbologyID = false;
                                        barcodeDecoder1.BarcodeTypes = (int)(BarcodeReaderLib.EBarcodeTypes.Code128) | (int)(BarcodeReaderLib.EBarcodeTypes.Code39);
                                        barcodeDecoder1.LinearShowCheckDigit = false;
                                        barcodeDecoder1.DecodeFile(thumbnailspath + imgName + ".jpg");
                                        BarCodeList = barcodeDecoder1.Barcodes;
                                        if (BarCodeList.length > 0)
                                            barcodeInfo = BarCodeList.item(0);
                                        break;

                                    case 405:
                                        BarcodeDecoder barcodeDecoder2 = new BarcodeReaderLib.BarcodeDecoder();
                                        barcodeDecoder2.BarcodeTypes = (int)(BarcodeReaderLib.EBarcodeTypes.QRCode);// | BarcodeReaderLib.EBarcodeTypes.QRCodeUnrecognized);
                                        barcodeDecoder2.DecodeFile(thumbnailspath + imgName + ".jpg");
                                        QRCodeList = barcodeDecoder2.Barcodes;
                                        BarcodeDecoder barcodeDecoder3 = new BarcodeReaderLib.BarcodeDecoder();
                                        barcodeDecoder3.LinearFindBarcodes = 1;
                                        barcodeDecoder3.BarcodeTypes = (int)(BarcodeReaderLib.EBarcodeTypes.Code128) | (int)(BarcodeReaderLib.EBarcodeTypes.Code39);
                                        barcodeDecoder3.LinearShowCheckDigit = false;
                                        barcodeDecoder3.LinearShowSymbologyID = false;
                                        barcodeDecoder3.DecodeFile(thumbnailspath + imgName + ".jpg");
                                        BarCodeList = barcodeDecoder3.Barcodes;
                                        if (QRCodeList.length == 1 && BarCodeList.length == 1)
                                        {
                                            //if((int)BarCodeList.item(0).BarcodeType == (int)BarcodeTypes.LinearUnrecognized && (int)QRCodeList.item(0).BarcodeType != (int)BarcodeTypes.QRCodeUnrecognized)
                                            //{
                                            //    barcodeInfo = QRCodeList.item(0);
                                            //}
                                            //else if((int)BarCodeList.item(0).BarcodeType != (int)BarcodeTypes.LinearUnrecognized && (int)QRCodeList.item(0).BarcodeType == (int)BarcodeTypes.QRCodeUnrecognized)
                                            //{
                                            //    barcodeInfo = BarCodeList.item(0);
                                            //}
                                            //else
                                            //{
                                            barcodeInfo = QRCodeList.item(0);
                                            //}
                                        }
                                        else if (QRCodeList.length == 1 && BarCodeList.length < 1)
                                        {
                                            barcodeInfo = QRCodeList.item(0);
                                        }
                                        else if (QRCodeList.length < 1 && BarCodeList.length == 1)
                                        {
                                            barcodeInfo = BarCodeList.item(0);
                                        }
                                        break;

                                    default:
                                        break;
                                }
                            }
                            catch (Exception ex)
                            {
                                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                            }

                            if (barcodeInfo != null && ((int)barcodeInfo.BarcodeType == (int)BarcodeTypes.Code128 || (int)barcodeInfo.BarcodeType == (int)BarcodeTypes.Code39 || (int)barcodeInfo.BarcodeType == (int)BarcodeTypes.QRCode))
                            {
                                if (myImgClassList.ListMyImageClass.Count > 0)
                                    lstMyImgClassList.Add(myImgClassList);
                                myImgClassList = new MyImageClassList();
                                ImageName[j].IsCodeType = true;
                                myImgClassList.ListMyImageClass.Add(ImageName[j]);
                                if (!String.IsNullOrWhiteSpace(QRWebURLReplace))
                                    myImgClassList.Barcode = barcodeInfo.Text.Replace(QRWebURLReplace, "");
                                else
                                    myImgClassList.Barcode = barcodeInfo.Text;
                                myImgClassList.Format = barcodeInfo.BarcodeType.ToString();
                            }
                            //else if (barcodeInfo != null && ((int)barcodeInfo.BarcodeType == (int)BarcodeTypes.LinearUnrecognized || (int)barcodeInfo.BarcodeType == (int)BarcodeTypes.QRCodeUnrecognized))
                            //{
                            //    if (myImgClassList.ListMyImageClass.Count > 0)
                            //        lstMyImgClassList.Add(myImgClassList);
                            //    myImgClassList = new MyImageClassList();
                            //    ImageName[j].IsCodeType = true;
                            //    myImgClassList.ListMyImageClass.Add(ImageName[j]);
                            //    myImgClassList.Barcode = "1111";
                            //    myImgClassList.Format = "Lost";
                            //}
                            else
                            {
                                ImageName[j].IsCodeType = false;
                                myImgClassList.ListMyImageClass.Add(ImageName[j]);
                                if (j == ImageName.Count - 1)
                                    lstMyImgClassList.Add(myImgClassList);
                            }
                        }
                    }

                    foreach (MyImageClassList myImgList in lstMyImgClassList)
                    {
                        getitem = getitem + myImgList.ListMyImageClass.Count - 1;
                    }
                    foreach (MyImageClassList myImgList in lstMyImgClassList)
                    {
                        for (int p = 0; p < myImgList.ListMyImageClass.Count; p++)
                        {
                            try
                            {
                                i++;
                                //getitem = ImageName.Count;
                                //string name = myImgClass.Title + ".jpg";
                                string name = myImgList.ListMyImageClass[p].Title + ".jpg";
                                if (name != "Thumbs.db")
                                {

                                    string picname = name;
                                    s += ", " + picname;
                                    //if (myImgClass.IsChecked)
                                    if (myImgList.ListMyImageClass[p].IsChecked)
                                    {
                                        try
                                        {
                                            //ExifTagCollection exif = new ExifTagCollection(path + myImgClass.Title + ".jpg");

                                            ExifTagCollection exif = new ExifTagCollection(Path.Combine(myImgList.ListMyImageClass[p].ImagePath, myImgList.ListMyImageClass[p].srcPath) + ".jpg");

                                            string CameraManufacture = "'" + "##" + "'";
                                            string CameraModel = "'" + "##" + "'";
                                            string orientation = "'" + "##" + "'";
                                            string HorizontalResolution = "'" + "##" + "'";
                                            string VerticalResolution = "'" + "##" + "'";
                                            string Datetaken = "'" + "##" + "'";
                                            string dimension = "'" + "##" + "'";
                                            string ISOSpeedRatings = "'" + "##" + "'";
                                            string ExposureMode = "'" + "##" + "'";
                                            string Sharpness = "'" + "##" + "'";

                                            #region Auto-Rotate
                                            //Start:Code for Auto-Rotate
                                            if ((bool)IsAutoRotate)
                                            {
                                                ExifReader reader = null;
                                                string renderedTag = "";
                                                try
                                                {
                                                    //reader = new ExifReader(path + myImgClass.Title + ".jpg");
                                                    reader = new ExifReader(Path.Combine(myImgList.ListMyImageClass[p].ImagePath, myImgList.ListMyImageClass[p].Title) + ".jpg");
                                                    foreach (ushort tagID in Enum.GetValues(typeof(ExifTags)))
                                                    {
                                                        object val;
                                                        if (reader.GetTagValue(tagID, out val))
                                                        {
                                                            renderedTag = val.ToString();
                                                        }
                                                    }
                                                }
                                                catch (Exception ex)
                                                {
                                                    string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                                                    ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                                                }
                                                if (!string.IsNullOrEmpty(renderedTag))
                                                {
                                                    needrotaion = GetRotationValue(renderedTag);
                                                }
                                                else
                                                {
                                                    needrotaion = 0;
                                                }
                                            }
                                            //End:Code for Auto-Rotate
                                            #endregion

                                            //Get real time
                                            if (exif.Where(o => o.Id == 36867).Count() > 0)
                                            {
                                                try
                                                {
                                                    System.Globalization.DateTimeFormatInfo objFormat = new System.Globalization.DateTimeFormatInfo();
                                                    objFormat.ShortDatePattern = @"yyyy/MM/dd HH:mm:ss";
                                                    string datePart = exif[36867].Value.Split(' ').First();
                                                    datePart = datePart.Replace(':', '/');
                                                    string timePart = exif[36867].Value.Split(' ').Last();
                                                    CaptureDate = Convert.ToDateTime(datePart + " " + timePart, objFormat);
                                                }
                                                catch (Exception ex)
                                                {
                                                    CaptureDate = null;
                                                    string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                                                    ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                                                }
                                            }
                                            else
                                                CaptureDate = null;

                                            foreach (ExifTag tag in exif)
                                            {
                                                if (tag.Id == 271)
                                                {
                                                    CameraManufacture = "'" + exif[271].Value + "'";
                                                }
                                                else if (tag.Id == 272)
                                                {
                                                    CameraModel = "'" + exif[272].Value + "'";
                                                }
                                                else if (tag.Id == 274)
                                                {
                                                    orientation = "'" + exif[274].Value + "'";
                                                }
                                                else if (tag.Id == 282)
                                                {
                                                    HorizontalResolution = "'" + exif[282].Value + "'";
                                                }
                                                else if (tag.Id == 283)
                                                {
                                                    VerticalResolution = "'" + exif[283].Value + "'";
                                                }
                                                else if (tag.Id == 36867)
                                                {
                                                    Datetaken = "'" + exif[36867].Value + "'";
                                                }
                                                else if (tag.Id == 40962 || tag.Id == 40963)
                                                {
                                                    dimension = "'" + exif[40962].Value + " x " + exif[40963].Value + "'";
                                                }
                                                else if (tag.Id == 34855)
                                                {
                                                    ISOSpeedRatings = "'" + exif[34855].Value + "'";
                                                }
                                                else if (tag.Id == 41986)
                                                {
                                                    ExposureMode = "'" + exif[41986].Value + "'";
                                                }
                                                else if (tag.Id == 41994)
                                                {
                                                    Sharpness = "'" + exif[41994].Value + "'";
                                                }
                                            }
                                            ImageMetaData = "<image Dimensions=" + dimension + " CameraManufacture=" + CameraManufacture + " HorizontalResolution=" + HorizontalResolution + " VerticalResolution=" + VerticalResolution + " CameraModel=" + CameraModel + " ISO-SpeedRating=" + ISOSpeedRatings + " DateTaken=" + Datetaken + " ExposureMode=" + ExposureMode + " Sharpness=" + Sharpness + " Orientation=" + orientation + "></image>";
                                        }
                                        catch (Exception ex)
                                        {
                                            string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                                            ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                                        }
                                        DateTime serverDateTime = (new CustomBusineses()).ServerDateTime();
                                        string filename = serverDateTime.Day + serverDateTime.Month + PhotoNo + "_" + PhotographerId + ".jpg";
                                        DefaultEffects = "<image brightness = '" + defaultBrightness + "' contrast = '" + defaultContrast + "' Crop='##' colourvalue = '##' rotate='##' ><effects sharpen='##' greyscale='0' digimagic='0' sepia='0' defog='0' underwater='0' emboss='0' invert = '0' granite='0' hue ='##' cartoon = '0'></effects></image>";

                                        if ((new PhotoBusiness()).CheckPhotos(filename, Convert.ToInt32(PhotographerId)))
                                        {
                                            int lastPhotoId = (new PhotoBusiness()).SetPhotoDetails(LoginUser.SubStoreId,PhotoNo.ToString(), filename, (new CustomBusineses()).ServerDateTime(), PhotographerId, ImageMetaData, locationid, PhotoLayer, DefaultEffects, CaptureDate, App.RfidScanType, 1, null, 0,true);
                                            if (p == 0 && myImgList.ListMyImageClass[p].IsCodeType == true)
                                            {
                                                (new PhotoBusiness()).DeletePhotoByPhotoId(lastPhotoId);
                                            }
                                            else if (!String.IsNullOrWhiteSpace(myImgList.Barcode) && !String.IsNullOrWhiteSpace(myImgList.Format))
                                            {
                                                (new PhotoBusiness()).SetImageAssociationInfo(lastPhotoId, myImgList.Format, myImgList.Barcode, IsAnonymousCodeActive);
                                            }
                                            string rfid = PhotoNo.ToString();
                                            File.Copy(Path.Combine(myImgList.ListMyImageClass[p].ImagePath, picname), Path.Combine(filepathdate, filename));
                                            //File.Move(filepath + picname, filepath + filename);
                                            ResizeWPFImage(Path.Combine(filepathdate, filename), 210, filepath + "\\Thumbnails\\" + filename, Path.Combine(filepathdate, filename));
                                            ResizeWPFImage(Path.Combine(filepathdate, filename), 900, filepath + "\\Thumbnails_Big\\" + filename);
                                            if (!IsCodeType(lastPhotoId))
                                            {
                                                ApplySettings(rfid, filename, lastPhotoId, Common.LoginUser.SubStoreId, locationid, filepathdate);
                                            }
                                            CountImagesDownload = CountImagesDownload + 1;
                                            PhotoNo = PhotoNo + 1;

                                            if (IsDelete)
                                            {
                                                try { File.Delete(Path.Combine(myImgList.ListMyImageClass[p].ImagePath, picname)); }
                                                catch { }
                                            }
                                        }
                                        else
                                        {
                                            MessageBox.Show(PhotoNo + " already exists for today");
                                            return;
                                        }
                                    }
                                }
                            }
                            catch (Exception ex)
                            {
                                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                            }
                            object[] obj = new object[] { i, getitem };
                            //this will marshal us back to the UI thread
                            Dispatcher.Invoke(DispatcherPriority.Normal, new Action<object[]>(Update), obj);
                        }
                    }
                    #endregion Barcode Active
                }
                else
                {
                    #region Normal download flow
                    foreach (var item in ImageName)
                    {
                        string ext = System.IO.Path.GetExtension(item.Title).ToLower();

                        try
                        {
                            i++;
                            getitem = ImageName.Count;
                            if (ext == ".jpg")
                            {

                                string name = item.Title;// + ".jpg";
                                if (name != "Thumbs.db")
                                {
                                    string picname = name;
                                    s += ", " + picname;
                                    if (item.IsChecked)
                                    {
                                        try
                                        {
                                            //ExifTagCollection exif = new ExifTagCollection(Path.Combine(item.ImagePath, item.Title) + ".jpg");
                                            ExifTagCollection exif = new ExifTagCollection(item.ImagePath);
                                            string CameraManufacture = "'" + "##" + "'";
                                            string CameraModel = "'" + "##" + "'";
                                            string orientation = "'" + "##" + "'";
                                            string HorizontalResolution = "'" + "##" + "'";
                                            string VerticalResolution = "'" + "##" + "'";
                                            string Datetaken = "'" + "##" + "'";
                                            string dimension = "'" + "##" + "'";
                                            string ISOSpeedRatings = "'" + "##" + "'";
                                            string ExposureMode = "'" + "##" + "'";
                                            string Sharpness = "'" + "##" + "'";

                                            #region Auto-Rotate
                                            //Start:Code for Auto-Rotate
                                            if ((bool)IsAutoRotate)
                                            {
                                                ExifReader reader = null;
                                                string renderedTag = "";
                                                try
                                                {
                                                    reader = new ExifReader(Path.Combine(item.ImagePath, item.Title) + ".jpg");
                                                    foreach (ushort tagID in Enum.GetValues(typeof(ExifTags)))
                                                    {
                                                        object val;
                                                        if (reader.GetTagValue(tagID, out val))
                                                        {
                                                            renderedTag = val.ToString();
                                                        }
                                                    }
                                                }
                                                catch (Exception ex)
                                                {
                                                    string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                                                    ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                                                }
                                                if (!string.IsNullOrEmpty(renderedTag))
                                                {
                                                    needrotaion = GetRotationValue(renderedTag);
                                                }
                                                else
                                                {
                                                    needrotaion = 0;
                                                }
                                            }
                                            //End:Code for Auto-Rotate
                                            #endregion

                                            if (exif.Where(o => o.Id == 36867).Count() > 0)
                                            {
                                                try
                                                {
                                                    System.Globalization.DateTimeFormatInfo objFormat = new System.Globalization.DateTimeFormatInfo();
                                                    objFormat.ShortDatePattern = @"yyyy/MM/dd HH:mm:ss";
                                                    string datePart = exif[36867].Value.Split(' ').First();
                                                    datePart = datePart.Replace(':', '/');
                                                    string timePart = exif[36867].Value.Split(' ').Last();
                                                    CaptureDate = Convert.ToDateTime(datePart + " " + timePart, objFormat);
                                                }
                                                catch (Exception ex)
                                                {
                                                    CaptureDate = null;
                                                    string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                                                    ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                                                }
                                            }
                                            else
                                                CaptureDate = null;

                                            foreach (ExifTag tag in exif)
                                            {
                                                if (tag.Id == 271)
                                                {
                                                    CameraManufacture = "'" + exif[271].Value + "'";
                                                }
                                                else if (tag.Id == 272)
                                                {
                                                    CameraModel = "'" + exif[272].Value + "'";
                                                }
                                                else if (tag.Id == 274)
                                                {
                                                    orientation = "'" + exif[274].Value + "'";
                                                }
                                                else if (tag.Id == 282)
                                                {
                                                    HorizontalResolution = "'" + exif[282].Value + "'";
                                                }
                                                else if (tag.Id == 283)
                                                {
                                                    VerticalResolution = "'" + exif[283].Value + "'";
                                                }
                                                else if (tag.Id == 36867)
                                                {
                                                    Datetaken = "'" + exif[36867].Value + "'";
                                                }
                                                else if (tag.Id == 40962 || tag.Id == 40963)
                                                {
                                                    dimension = "'" + exif[40962].Value + " x " + exif[40963].Value + "'";
                                                }
                                                else if (tag.Id == 34855)
                                                {
                                                    ISOSpeedRatings = "'" + exif[34855].Value + "'";
                                                }
                                                else if (tag.Id == 41986)
                                                {
                                                    ExposureMode = "'" + exif[41986].Value + "'";
                                                }
                                                else if (tag.Id == 41994)
                                                {
                                                    Sharpness = "'" + exif[41994].Value + "'";
                                                }
                                            }
                                            ImageMetaData = "<image Dimensions=" + dimension + " CameraManufacture=" + CameraManufacture + " HorizontalResolution=" + HorizontalResolution + " VerticalResolution=" + VerticalResolution + " CameraModel=" + CameraModel + " ISO-SpeedRating=" + ISOSpeedRatings + " DateTaken=" + Datetaken + " ExposureMode=" + ExposureMode + " Sharpness=" + Sharpness + " Orientation=" + orientation + "></image>";
                                        }
                                        catch (Exception ex)
                                        {
                                            string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                                            ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                                        }
                                        DateTime serverDate = (new CustomBusineses()).ServerDateTime();
                                        string filename = serverDate.Day + serverDate.Month + PhotoNo + "_" + PhotographerId + ".jpg";
                                        //End new config data get
                                        DefaultEffects = "<image brightness = '" + defaultBrightness + "' contrast = '" + defaultContrast + "' Crop='##' colourvalue = '##' rotate='##' ><effects sharpen='##' greyscale='0' digimagic='0' sepia='0' defog='0' underwater='0' emboss='0' invert = '0' granite='0' hue ='##' cartoon = '0'></effects></image>";

                                        if ((new PhotoBusiness()).CheckPhotos(filename, Convert.ToInt32(PhotographerId)))
                                        {

                                            int PhotoId = (new PhotoBusiness()).SetPhotoDetails(LoginUser.SubStoreId, PhotoNo.ToString(), filename, (new CustomBusineses()).ServerDateTime(), PhotographerId, ImageMetaData, locationid, PhotoLayer, DefaultEffects, CaptureDate, App.RfidScanType, 1, null, 0, true);
                                            string rfid = PhotoNo.ToString();
                                            File.Copy(item.ImagePath, Path.Combine(filepathdate, filename));
                                            //File.Copy(Path.Combine(item.ImagePath, picname), Path.Combine(filepathdate,filename));
                                            ResizeWPFImage(Path.Combine(filepathdate, filename), 210, Path.Combine(filepath, "Thumbnails", filename), Path.Combine(filepathdate, filename));
                                            ResizeWPFImage(Path.Combine(filepathdate, filename), 900, Path.Combine(filepath, "Thumbnails_Big", filename));
                                            ApplySettings(rfid, filename, PhotoId, Common.LoginUser.SubStoreId, locationid, filepathdate);

                                            CountImagesDownload = CountImagesDownload + 1;
                                            PhotoNo = PhotoNo + 1;
                                        }
                                        else
                                        {
                                            MessageBox.Show(PhotoNo + " already exists for today");
                                            return;
                                        }
                                    }
                                }
                            }
                            else
                            {
                                if (item.IsChecked)
                                {

                                    if (AcquireVideos(item.Title, PhotoNo, PhotographerId, locationid))
                                    {

                                        CountImagesDownload = CountImagesDownload + 1;
                                        PhotoNo = PhotoNo + 1;
                                    }
                                    else
                                    {
                                        return;
                                    }

                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                            ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                        }
                        object[] obj = new object[] { i, getitem };
                        //this will marshal us back to the UI thread
                        Dispatcher.Invoke(DispatcherPriority.Normal, new Action<object[]>(Update), obj);

                    }//);
                    #endregion End Normal download flow
                }

                if (CountImagesDownload > 0)
                {
                    MessageBox.Show(CountImagesDownload + " File(s)" + " Acquired" + " Successfully");
                    _result = string.Empty;
                    HideHandlerDialog();

                }
            };
            new Thread(start).Start();
        }

        /// <summary>
        /// Updates the specified object.
        /// </summary>
        /// <param name="obj">The object.</param>
        void Update(object[] obj)
        {
            int value = (int)obj[0];
            int maxvalue = (int)obj[1];

            //this is taking place on the UI thread
            btnDownload.IsEnabled = false;
            DownloadProgress.Maximum = maxvalue;
            DownloadProgress.Value = value;

        }
        /// <summary>
        /// The count images download
        /// </summary>
        int CountImagesDownload = 0;

        /// <summary>
        /// The needrotaion
        /// </summary>
        int needrotaion = 0;
        /// <summary>
        /// The photo layer
        /// </summary>
        string PhotoLayer = null;
        /// <summary>
        /// Determines whether [is valid data].
        /// </summary>
        /// <returns></returns>
        private bool IsValidData()
        {
            bool retvalue = true;

            if (CmbPhotographerNo.SelectedValue.ToString() == "0")
            {
                retvalue = false;
            }
            else if (CmbLocation.SelectedValue.ToString() == "0")
            {
                retvalue = false;
            }
            try
            {
                Convert.ToInt64(tbStartingNo.Text);
            }
            catch (Exception ex)
            {
                retvalue = false;
            }
            if (tbStartingNo.Text.Trim() == "")
            {
                retvalue = false;
            }

            return retvalue;
        }

        /// <summary>
        /// Handles the Click event of the btnBack control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void btnBack_Click(object sender, RoutedEventArgs e)
        {
            _result = string.Empty;
            HideHandlerDialog();
        }
        /// <summary>
        /// Handles the SelectionChanged event of the CmbPhotographerNo control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="SelectionChangedEventArgs"/> instance containing the event data.</param>
        private void CmbPhotographerNo_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

            if (Convert.ToInt32(CmbPhotographerNo.SelectedValue) != 0)
            {
                Lastimageno.Visibility = Visibility.Visible;
                try
                {
                    string LastImageId = (new PhotoBusiness()).GetPhotoGrapherLastImageId(Convert.ToInt32(CmbPhotographerNo.SelectedValue));
                    Lastimageno.Text = "Last Photo no. of " + (((System.Collections.Generic.KeyValuePair<string, int>)(CmbPhotographerNo.SelectedItem)).Key) + " is " + LastImageId;
                }
                catch (Exception ex)
                {

                    string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                    ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);

                }
            }
            else
            {
                Lastimageno.Visibility = Visibility.Collapsed;
            }


        }

        private void GetNewConfigValues()
        {
            //_objDataLayer = new DigiPhotoDataServices();
            List<iMIXConfigurationInfo> _objNewConfigList = (new ConfigBusiness()).GetNewConfigValues(LoginUser.SubStoreId);
            foreach (iMIXConfigurationInfo _objNewConfig in _objNewConfigList)
            {
                switch (_objNewConfig.IMIXConfigurationMasterId)
                {
                    case 1:
                        IsBarcodeActive = Convert.ToBoolean(_objNewConfig.ConfigurationValue);
                        break;
                    case 2:
                        MappingType = Convert.ToInt32(_objNewConfig.ConfigurationValue);
                        break;
                    case 3:
                        scanType = Convert.ToInt32(_objNewConfig.ConfigurationValue);
                        break;
                    case 38:
                        IsAnonymousCodeActive = Convert.ToBoolean(_objNewConfig.ConfigurationValue);
                        break;
                    case (int)ConfigParams.Contrast:
                        defaultContrast = string.IsNullOrEmpty(_objNewConfig.ConfigurationValue) ? "1" : _objNewConfig.ConfigurationValue;
                        break;

                    case (int)ConfigParams.Brightness:
                        defaultBrightness = string.IsNullOrEmpty(_objNewConfig.ConfigurationValue) ? "0" : _objNewConfig.ConfigurationValue;
                        break;
                    case (int)ConfigParams.IsDeleteFromUSB:
                        IsDelete = Convert.ToBoolean(_objNewConfig.ConfigurationValue);
                        break;
                    case (int)ConfigParams.IsEnabledRFID:
                        App.IsRFIDEnabled = _objNewConfig.ConfigurationValue != null && _objNewConfig.ConfigurationValue != "" ? Convert.ToBoolean(_objNewConfig.ConfigurationValue) : false;
                        break;
                    case (int)ConfigParams.RFIDScanType:
                        if (App.IsRFIDEnabled == true && _objNewConfig.ConfigurationValue != null && _objNewConfig.ConfigurationValue != "")
                            App.RfidScanType = Convert.ToInt32(_objNewConfig.ConfigurationValue);
                        else
                            App.RfidScanType = null;
                        break;
                    default:
                        break;
                }
            }
        }

        private void GetNewConfigLocationValues(int LocationId)
        {
            ConfigBusiness configBusiness = new ConfigBusiness();

            List<iMixConfigurationLocationInfo> _objNewConfigList = configBusiness.GetConfigLocation(LocationId, LoginUser.SubStoreId);
            foreach (iMixConfigurationLocationInfo _objNewConfig in _objNewConfigList)
            {
                switch (_objNewConfig.IMIXConfigurationMasterId)
                {
                    case (int)ConfigParams.IsBarcodeActive:
                        IsBarcodeActive = Convert.ToBoolean(_objNewConfig.ConfigurationValue);
                        break;
                    case (int)ConfigParams.DefaultMappingCode:
                        MappingType = Convert.ToInt32(_objNewConfig.ConfigurationValue);
                        break;
                    case (int)ConfigParams.ScanType:
                        scanType = Convert.ToInt32(_objNewConfig.ConfigurationValue);
                        break;
                    case (int)ConfigParams.IsAnonymousQrCodeEnabled:
                        IsAnonymousCodeActive = Convert.ToBoolean(_objNewConfig.ConfigurationValue);
                        break;
                    case (int)ConfigParams.Contrast:
                        defaultContrast = string.IsNullOrEmpty(_objNewConfig.ConfigurationValue) ? "1" : _objNewConfig.ConfigurationValue;
                        break;

                    case (int)ConfigParams.Brightness:
                        defaultBrightness = string.IsNullOrEmpty(_objNewConfig.ConfigurationValue) ? "0" : _objNewConfig.ConfigurationValue;
                        break;
                    case (int)ConfigParams.IsDeleteFromUSB:
                        IsDelete = Convert.ToBoolean(_objNewConfig.ConfigurationValue);
                        break;
                    case (int)ConfigParams.IsEnabledRFID:
                        App.IsRFIDEnabled = _objNewConfig.ConfigurationValue != null && _objNewConfig.ConfigurationValue != "" ? Convert.ToBoolean(_objNewConfig.ConfigurationValue) : false;
                        break;
                    case (int)ConfigParams.RFIDScanType:
                        if (App.IsRFIDEnabled == true && _objNewConfig.ConfigurationValue != null && _objNewConfig.ConfigurationValue != "")
                            App.RfidScanType = Convert.ToInt32(_objNewConfig.ConfigurationValue);
                        else
                            App.RfidScanType = null;
                        break;
                    default:
                        break;
                }
            }
        }
        /// <summary>
        /// Resizes the WPF image.
        /// </summary>
        /// <param name="sourceImage">The source image.</param>
        /// <param name="maxHeight">The maximum height.</param>
        /// <param name="saveToPath">The save automatic path.</param>
        /// <param name="rotatePath">The rotate path.</param>
        private void ResizeWPFImage(string sourceImage, int maxHeight, string saveToPath, string rotatePath)
        {
            try
            {
                BitmapImage bi = new BitmapImage();
                BitmapImage bitmapImage = new BitmapImage();
                using (FileStream fileStream = File.OpenRead(sourceImage))
                {
                    MemoryStream ms = new MemoryStream();
                    fileStream.CopyTo(ms);
                    ms.Seek(0, SeekOrigin.Begin);
                    fileStream.Close();
                    bi.BeginInit();
                    bi.StreamSource = ms;
                    bi.EndInit();
                    bi.Freeze();

                    decimal ratio = 0;
                    int newWidth = 0;
                    int newHeight = 0;

                    if (bi.Width >= bi.Height)
                    {
                        ratio = Convert.ToDecimal(bi.Width) / Convert.ToDecimal(bi.Height);
                        newWidth = maxHeight;
                        newHeight = Convert.ToInt32(maxHeight / ratio);
                    }
                    else
                    {
                        ratio = Convert.ToDecimal(bi.Height) / Convert.ToDecimal(bi.Width);
                        newHeight = maxHeight;
                        newWidth = Convert.ToInt32(maxHeight / ratio);
                    }

                    ms.Seek(0, SeekOrigin.Begin);
                    bitmapImage.BeginInit();
                    bitmapImage.StreamSource = ms;
                    bitmapImage.DecodePixelWidth = newWidth;
                    bitmapImage.DecodePixelHeight = newHeight;
                    ImageMagickObject.MagickImage jmagic = new ImageMagickObject.MagickImage();

                    if (needrotaion > 0)
                    {
                        if (needrotaion == 0)
                        {
                            bitmapImage.Rotation = Rotation.Rotate0;
                            isrotated = false;
                        }
                        else if (needrotaion == 90)
                        {
                            bitmapImage.Rotation = Rotation.Rotate90;

                            object[] o = new object[] { "-rotate", " 90 ", rotatePath };
                            jmagic.Mogrify(o);
                            o = null;
                            isrotated = true;
                        }
                        else if (needrotaion == 180)
                        {
                            bitmapImage.Rotation = Rotation.Rotate180;

                            object[] o = new object[] { "-rotate", " 180 ", rotatePath };
                            jmagic.Mogrify(o);
                            o = null;
                            isrotated = false;
                        }
                        else if (needrotaion == 270)
                        {
                            bitmapImage.Rotation = Rotation.Rotate270;

                            object[] o = new object[] { "-rotate", " 270 ", rotatePath };
                            jmagic.Mogrify(o);
                            o = null;
                            isrotated = true;
                        }
                    }

                    jmagic = null;

                    bitmapImage.EndInit();
                    bitmapImage.Freeze();
                    fileStream.Close();
                }

                using (var fileStreamForSave = new FileStream(saveToPath, FileMode.Create, FileAccess.ReadWrite))
                {
                    JpegBitmapEncoder encoder = new JpegBitmapEncoder();
                    encoder.QualityLevel = 94;
                    encoder.Frames.Add(BitmapFrame.Create(bitmapImage));
                    encoder.Save(fileStreamForSave);
                    fileStreamForSave.Close();
                }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }

        /// <summary>
        /// Resizes the WPF image.
        /// </summary>
        /// <param name="sourceImage">The source image.</param>
        /// <param name="maxHeight">The maximum height.</param>
        /// <param name="saveToPath">The save automatic path.</param>
        private void ResizeWPFImage(string sourceImage, int maxHeight, string saveToPath)
        {
            try
            {
                BitmapImage bi = new BitmapImage();
                BitmapImage bitmapImage = new BitmapImage();
                using (FileStream fileStream = File.OpenRead(sourceImage))
                {
                    MemoryStream ms = new MemoryStream();
                    fileStream.CopyTo(ms);
                    ms.Seek(0, SeekOrigin.Begin);
                    fileStream.Close();
                    bi.BeginInit();
                    bi.StreamSource = ms;
                    bi.EndInit();
                    bi.Freeze();

                    decimal ratio = 0;
                    int newWidth = 0;
                    int newHeight = 0;

                    if (bi.Width >= bi.Height)
                    {
                        ratio = Convert.ToDecimal(bi.Width) / Convert.ToDecimal(bi.Height);
                        newWidth = maxHeight;
                        newHeight = Convert.ToInt32(maxHeight / ratio);
                    }
                    else
                    {
                        ratio = Convert.ToDecimal(bi.Height) / Convert.ToDecimal(bi.Width);
                        newHeight = maxHeight;
                        newWidth = Convert.ToInt32(maxHeight / ratio);
                    }

                    ms.Seek(0, SeekOrigin.Begin);
                    bitmapImage.BeginInit();
                    bitmapImage.StreamSource = ms;
                    bitmapImage.DecodePixelWidth = newWidth;
                    bitmapImage.DecodePixelHeight = newHeight;

                    //if (needrotaion > 0)
                    //{
                    //    if (needrotaion == 0)
                    //    {
                    //        bitmapImage.Rotation = Rotation.Rotate0;
                    //    }
                    //    else if (needrotaion == 90)
                    //    {
                    //        bitmapImage.Rotation = Rotation.Rotate90;
                    //    }
                    //    else if (needrotaion == 180)
                    //    {
                    //        bitmapImage.Rotation = Rotation.Rotate180;
                    //    }
                    //    else if (needrotaion == 270)
                    //    {
                    //        bitmapImage.Rotation = Rotation.Rotate270;
                    //    }
                    //}

                    bitmapImage.EndInit();
                    bitmapImage.Freeze();
                    fileStream.Close();
                }

                using (var fileStreamForSave = new FileStream(saveToPath, FileMode.Create, FileAccess.ReadWrite))
                {
                    JpegBitmapEncoder encoder = new JpegBitmapEncoder();
                    encoder.QualityLevel = 94;
                    encoder.Frames.Add(BitmapFrame.Create(bitmapImage));
                    encoder.Save(fileStreamForSave);
                    fileStreamForSave.Close();
                }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }
        /// <summary>
        /// Gets the rotation value.
        /// </summary>
        /// <param name="orientation">The orientation.</param>
        /// <returns></returns>
        private static int GetRotationValue(string orientation)
        {
            switch (int.Parse(orientation))
            {
                case 1:
                    return 0;

                case 2:
                    return 0;

                case 3:
                    return 180;

                case 4:
                    return 180;

                case 5:
                    return 90;

                case 6:
                    return 90;

                case 7:
                    return 270;

                case 8:
                    return 270;
                default:
                    return 0;
            }
        }

        /// <summary>
        /// Orientations the type of the automatic flip.
        /// </summary>
        /// <param name="orientation">The orientation.</param>
        /// <returns></returns>
        private static RotateFlipType OrientationToFlipType(string orientation)
        {
            switch (int.Parse(orientation))
            {
                case 1:
                    return RotateFlipType.RotateNoneFlipNone;

                case 2:
                    return RotateFlipType.RotateNoneFlipX;

                case 3:
                    return RotateFlipType.Rotate180FlipNone;

                case 4:
                    return RotateFlipType.Rotate180FlipX;

                case 5:
                    return RotateFlipType.Rotate90FlipX;

                case 6:
                    return RotateFlipType.Rotate90FlipNone;

                case 7:
                    return RotateFlipType.Rotate270FlipX;

                case 8:
                    return RotateFlipType.Rotate270FlipNone;
                default:
                    return RotateFlipType.RotateNoneFlipNone;
            }
        }

        /// <summary>
        /// Gets the semi order settings.
        /// </summary>
        /// <returns></returns>
        private SemiOrderSettings GetSemiOrderSettings(int substoreId, int locationId)
        {
            try
            {
                //_objDataLayer = new DigiPhotoDataServices();
                SemiOrderSettings objDG_SemiOrder_Settings = Common.LoginUser.ListSemiOrderSettingsSubStoreWise.Where(x => x.DG_LocationId == locationId).FirstOrDefault();
                return objDG_SemiOrder_Settings;
            }
            catch (Exception ex)
            {

                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                return new SemiOrderSettings();
            }

        }

        private double GetActualCropRatio(double m)
        {
            if (m < 0.68 && m > 0.64)
                m = 0.6667;
            else if (m < 0.76 && m > 0.74)
                m = 0.75;
            else if (m < 0.81 && m > 0.79)
                m = 0.8;
            return m;
        }

        /// <summary>
        /// Gets the bitmap image from path.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <returns></returns>
        public BitmapImage GetBitmapImageFromPath(string value)
        {
            BitmapImage bi = new BitmapImage();
            try
            {
                if (value != null)
                {
                    using (FileStream fileStream = File.OpenRead(value))
                    {
                        MemoryStream ms = new MemoryStream();
                        fileStream.CopyTo(ms);
                        ms.Seek(0, SeekOrigin.Begin);
                        fileStream.Close();
                        bi.BeginInit();
                        bi.StreamSource = ms;
                        bi.EndInit();
                    }
                }
                else
                {
                    bi = new BitmapImage();
                }
                return bi;
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                return bi;
            }
        }
        /// <summary>
        /// Saves the XML.
        /// </summary>
        /// <param name="bvalue">The bvalue.</param>
        /// <param name="childnode">if set to <c>true</c> [childnode].</param>
        /// <param name="rfid">The rfid.</param>
        /// <param name="cvalue">The cvalue.</param>
        /// <param name="fvalue">The fvalue.</param>
        /// <param name="fvvalue">The fvvalue.</param>
        /// <param name="BG">The debug.</param>
        /// <param name="glayer">The glayer.</param>
        /// <param name="photoId">The photo unique identifier.</param>
        /// <param name="ZoomDetails">The zoom details.</param>
        public void SaveXml(string bvalue, bool childnode, string rfid, string cvalue, string fvalue, string fvvalue, string BG, string glayer, int photoId, string ZoomDetails, bool isCropActive, int ProductId, string filedatepath)
        {
            string layeringdata = "";
            //DigiPhotoDataServices _objdbservice = new DigiPhotoDataServices();

            System.Xml.XmlDocument Xdocument = new System.Xml.XmlDocument();
            Xdocument.LoadXml(DefaultEffects);

            if (!childnode)
            {

                System.Xml.XmlNodeList list = Xdocument.SelectNodes("//image");

                foreach (System.Xml.XmlElement XElement in list)
                {
                    XElement.SetAttribute("brightness", bvalue);
                }

                foreach (System.Xml.XmlElement XElement in list)
                {
                    XElement.SetAttribute("contrast", cvalue);
                }

                if (isCropActive)
                {
                    foreach (System.Xml.XmlElement XElement in list)
                    {
                        if (ProductId == 1)
                            XElement.SetAttribute("Crop", "6 * 8");
                        else if (ProductId == 2 || ProductId == 3)
                            XElement.SetAttribute("Crop", "8 * 10");
                        else if (ProductId == 30 || ProductId == 5)
                            XElement.SetAttribute("Crop", "4 * 6");
                        else if (ProductId == 98)
                            XElement.SetAttribute("Crop", "3 * 3");
                    }
                }

                BitmapImage _objframe;
                double zoomfact = 1;
                string fname;
                BitmapImage _objPhoto = GetBitmapImageFromPath(Path.Combine(filedatepath, (new PhotoBusiness()).GetFileNameByPhotoID(Convert.ToString(photoId))));
                if (_objPhoto.Height > _objPhoto.Width)
                {
                    if (!String.IsNullOrWhiteSpace(fvvalue))
                    {
                        _objframe = GetBitmapImageFromPath(_BorderFolder + fvvalue);
                    }
                    fname = fvvalue;
                }
                else
                {
                    if (!String.IsNullOrWhiteSpace(fvalue))
                    {
                        _objframe = GetBitmapImageFromPath(_BorderFolder + fvalue);
                    }
                    fname = fvalue;
                }
                string[] zoomdetails = ZoomDetails.Split(',');
                layeringdata = "<photo zoomfactor='" + zoomdetails[0] + "' border='" + fname + "' bg= '" + BG + "' canvasleft='" + zoomdetails[1] + "' canvastop='" + zoomdetails[2] + "' scalecentrex='" + zoomdetails[3] + "' scalecentrey='" + zoomdetails[4] + "'>" + glayer + "</photo>";
                (new PhotoBusiness()).UpdateLayering(photoId, layeringdata);
            }

            DefaultEffects = Xdocument.InnerXml;
            (new PhotoBusiness()).SetEffectsonPhoto(DefaultEffects, photoId,false);
        }

        private bool IsCodeType(int PhotoId)
        {
            //_objDataLayer = new DigiPhotoDataServices();
            return (new PhotoBusiness()).CheckIsCodeType(PhotoId);
        }

        /// <summary>
        /// Resizes the WPF image.
        /// </summary>
        /// <param name="sourceImage">The source image.</param>
        /// <param name="maxHeight">The maximum height.</param>
        /// <param name="saveToPath">The save automatic path.</param>
        /// <param name="rotatePath">The rotate path.</param>
        private void ResizeWPFImageWithoutRotation(string sourceImage, int maxHeight, string saveToPath)
        {
            try
            {
                BitmapImage bi = new BitmapImage();
                BitmapImage bitmapImage = new BitmapImage();
                using (FileStream fileStream = File.OpenRead(sourceImage))
                {
                    MemoryStream ms = new MemoryStream();
                    fileStream.CopyTo(ms);
                    ms.Seek(0, SeekOrigin.Begin);
                    fileStream.Close();
                    bi.BeginInit();
                    bi.StreamSource = ms;
                    bi.EndInit();
                    bi.Freeze();

                    decimal ratio = 0;
                    int newWidth = 0;
                    int newHeight = 0;

                    if (bi.Width >= bi.Height)
                    {
                        ratio = Convert.ToDecimal(bi.Width) / Convert.ToDecimal(bi.Height);
                        newWidth = maxHeight;
                        newHeight = Convert.ToInt32(maxHeight / ratio);
                    }
                    else
                    {
                        ratio = Convert.ToDecimal(bi.Height) / Convert.ToDecimal(bi.Width);
                        newHeight = maxHeight;
                        newWidth = Convert.ToInt32(maxHeight / ratio);
                    }

                    ms.Seek(0, SeekOrigin.Begin);
                    bitmapImage.BeginInit();
                    bitmapImage.StreamSource = ms;
                    bitmapImage.DecodePixelWidth = newWidth;
                    bitmapImage.DecodePixelHeight = newHeight;
                    bitmapImage.EndInit();
                    bitmapImage.Freeze();
                    fileStream.Close();
                }

                using (var fileStreamForSave = new FileStream(saveToPath, FileMode.Create, FileAccess.ReadWrite))
                {
                    JpegBitmapEncoder encoder = new JpegBitmapEncoder();
                    encoder.QualityLevel = 94;
                    encoder.Frames.Add(BitmapFrame.Create(bitmapImage));
                    encoder.Save(fileStreamForSave);
                    fileStreamForSave.Close();
                }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }

        /// <summary>
        /// Applies the settings.
        /// </summary>
        /// <param name="path">The path.</param>
        /// <param name="filename">The filename.</param>
        /// <param name="photoname">The photoname.</param>
        /// <param name="photoId">The photo unique identifier.</param>
        //private void ApplySettings(string path, string filename, string photoname, int photoId, int substoreId)
        private void ApplySettings(string photonameOld, string photonameNew, int photoId, int substoreId, int locationId, string dateFolderPath)
        {
            ContrastAdjustEffect _brighteff = new ContrastAdjustEffect();
            ContrastAdjustEffect _conteff = new ContrastAdjustEffect();
            MonochromeEffect _colorfiltereff = new MonochromeEffect();
            Directoryname = LoginUser.DigiFolderPath;
            try
            {
                if (is_SemiOrder)
                {
                    SemiOrderSettings objDG_SemiOrder_Settings = Common.LoginUser.ListSemiOrderSettingsSubStoreWise.Where(x => x.DG_LocationId == locationId).FirstOrDefault();
                    if (objDG_SemiOrder_Settings != null)
                    {
                        if (objDG_SemiOrder_Settings.DG_SemiOrder_Settings_AutoBright == true)
                        {
                            _brighteff.Brightness = (double)objDG_SemiOrder_Settings.DG_SemiOrder_Settings_AutoBright_Value;
                            //GrdBrightness.Effect = _brighteff;
                        }
                        else
                        {
                            objDG_SemiOrder_Settings.DG_SemiOrder_Settings_AutoBright_Value = Convert.ToDouble(defaultBrightness);
                        }
                        if (objDG_SemiOrder_Settings.DG_SemiOrder_Settings_AutoContrast == true)
                        {
                            _brighteff.Contrast = (double)objDG_SemiOrder_Settings.DG_SemiOrder_Settings_AutoContrast_Value;
                            //GrdContrast.Effect = _brighteff;
                        }
                        else
                        {
                            objDG_SemiOrder_Settings.DG_SemiOrder_Settings_AutoContrast_Value = Convert.ToDouble(defaultContrast);
                        }

                        if (objDG_SemiOrder_Settings.DG_SemiOrder_IsCropActive == true)
                        {
                            Uri URI = new Uri(Path.Combine(dateFolderPath, photonameNew));
                            BitmapSource bs = new BitmapImage(URI);
                            double ratioValue = bs.PixelWidth / bs.Width;
                            //ratioValue = ((double)((int)(ratioValue * 100.0))) / 100.0;
                            CroppedBitmap cb;
                            string[] rectValues;
                            double x;
                            double y;
                            double width;
                            double height;
                            double m;
                            double n;

                            if (bs.Width > bs.Height)
                            {
                                rectValues = objDG_SemiOrder_Settings.HorizontalCropValues.Split(',');
                                if (Convert.ToDouble(rectValues[2]) > Convert.ToDouble(rectValues[3]))
                                    m = Convert.ToDouble(rectValues[3]) / Convert.ToDouble(rectValues[2]);
                                else
                                    m = Convert.ToDouble(rectValues[2]) / Convert.ToDouble(rectValues[3]);
                                x = ratioValue * Convert.ToDouble(rectValues[0]);
                                y = ratioValue * Convert.ToDouble(rectValues[1]);
                                width = ratioValue * Convert.ToDouble(rectValues[2]);
                                height = ratioValue * Convert.ToDouble(rectValues[3]);
                                m = GetActualCropRatio(m);
                                if (bs.PixelHeight < (y + height))
                                {
                                    height = Convert.ToInt32(bs.PixelHeight - y);
                                    if (height > width)
                                        width = m * height;
                                    else
                                        width = height / m;
                                }
                            }
                            else
                            {
                                rectValues = objDG_SemiOrder_Settings.VerticalCropValues.Split(',');
                                if (Convert.ToDouble(rectValues[2]) > Convert.ToDouble(rectValues[3]))
                                    n = Convert.ToDouble(rectValues[3]) / Convert.ToDouble(rectValues[2]);
                                else
                                    n = Convert.ToDouble(rectValues[2]) / Convert.ToDouble(rectValues[3]);
                                x = ratioValue * Convert.ToDouble(rectValues[0]);
                                y = ratioValue * Convert.ToDouble(rectValues[1]);
                                width = ratioValue * Convert.ToDouble(rectValues[2]);
                                height = ratioValue * Convert.ToDouble(rectValues[3]);
                                n = GetActualCropRatio(n);
                                if (bs.PixelWidth < (x + width))
                                {
                                    width = bs.PixelWidth - x;
                                    if (height > width)
                                        width = n * height;
                                    else
                                        width = height / n;
                                }
                            }
                            width = Math.Round(width, 0);
                            height = Math.Round(height, 0);
                            cb = new CroppedBitmap(bs, new Int32Rect(Convert.ToInt32(x), Convert.ToInt32(y), Convert.ToInt32(width), Convert.ToInt32(height)));       //select region rect

                            using (MemoryStream mStream = new MemoryStream())
                            {
                                JpegBitmapEncoder jEncoder = new JpegBitmapEncoder();
                                jEncoder.Frames.Add(BitmapFrame.Create(cb));  //the croppedBitmap is a CroppedBitmap object 
                                jEncoder.QualityLevel = 50;
                                jEncoder.Save(mStream);
                                System.Drawing.Image imgSave = System.Drawing.Image.FromStream(mStream);
                                System.Drawing.Bitmap bmSave = new System.Drawing.Bitmap(imgSave);
                                bmSave.SetResolution(300, 300);
                                bmSave.Save(LoginUser.DigiFolderCropedPath + photonameNew);

                                try
                                {
                                    IntPtr deleteObject = bmSave.GetHbitmap();
                                    DeleteObject(deleteObject);
                                    bmSave.Dispose();

                                }
                                catch { }
                            }

                            ResizeWPFImageWithoutRotation(LoginUser.DigiFolderCropedPath + photonameNew, 250, LoginUser.DigiFolderThumbnailPath + photonameNew);
                            ResizeWPFImageWithoutRotation(LoginUser.DigiFolderCropedPath + photonameNew, 900, LoginUser.DigiFolderBigThumbnailPath + photonameNew);
                            (new PhotoBusiness()).SaveIsCropedPhotos(photoId, true, "Crop");
                        }

                        if (objDG_SemiOrder_Settings.DG_SemiOrder_Settings_ImageFrame != null)
                        {
                            if (objDG_SemiOrder_Settings.DG_SemiOrder_Settings_ImageFrame.Trim() != string.Empty)
                            {
                                String FileName = "";
                                if (isrotated)
                                {
                                    FileName = _BorderFolder + "\\" +
                                                      objDG_SemiOrder_Settings.DG_SemiOrder_Settings_ImageFrame_Vertical;
                                }
                                else
                                {
                                    FileName = _BorderFolder + "\\" +
                                                     objDG_SemiOrder_Settings.DG_SemiOrder_Settings_ImageFrame;
                                }

                                BitmapImage bii = new BitmapImage();

                                using (FileStream fileStream = File.OpenRead(FileName))
                                {

                                    MemoryStream ms = new MemoryStream();
                                    fileStream.CopyTo(ms);
                                    ms.Seek(0, SeekOrigin.Begin);
                                    fileStream.Close();
                                    bii.BeginInit();
                                    bii.StreamSource = ms;
                                    bii.EndInit();
                                    bii.Freeze();
                                }

                                if ((bool)objDG_SemiOrder_Settings.DG_SemiOrder_Settings_IsImageBG)
                                {
                                    SaveXml(Convert.ToString(objDG_SemiOrder_Settings.DG_SemiOrder_Settings_AutoBright_Value), false,
                                       photonameOld,
                                       Convert.ToString(objDG_SemiOrder_Settings.DG_SemiOrder_Settings_AutoContrast_Value),
                                       Convert.ToString(objDG_SemiOrder_Settings.DG_SemiOrder_Settings_ImageFrame), objDG_SemiOrder_Settings.DG_SemiOrder_Settings_ImageFrame_Vertical,
                                       objDG_SemiOrder_Settings.DG_SemiOrder_BG, objDG_SemiOrder_Settings.DG_SemiOrder_Graphics_layer, photoId, objDG_SemiOrder_Settings.DG_SemiOrder_Image_ZoomInfo, (bool)objDG_SemiOrder_Settings.DG_SemiOrder_IsCropActive, Convert.ToInt32(objDG_SemiOrder_Settings.DG_SemiOrder_ProductTypeId), dateFolderPath);
                                }
                                else
                                {
                                    SaveXml(Convert.ToString(objDG_SemiOrder_Settings.DG_SemiOrder_Settings_AutoBright_Value), false,
                                        photonameOld,
                                        Convert.ToString(objDG_SemiOrder_Settings.DG_SemiOrder_Settings_AutoContrast_Value),
                                        Convert.ToString(objDG_SemiOrder_Settings.DG_SemiOrder_Settings_ImageFrame), objDG_SemiOrder_Settings.DG_SemiOrder_Settings_ImageFrame_Vertical,
                                        string.Empty, objDG_SemiOrder_Settings.DG_SemiOrder_Graphics_layer, photoId, objDG_SemiOrder_Settings.DG_SemiOrder_Image_ZoomInfo, (bool)objDG_SemiOrder_Settings.DG_SemiOrder_IsCropActive, Convert.ToInt32(objDG_SemiOrder_Settings.DG_SemiOrder_ProductTypeId), dateFolderPath);
                                }
                                /*
                                for (int i = 0; i < frm.Children.Count; i++)
                                {
                                    frm.Children.RemoveAt(i);
                                }
                                OpaqueClickableImage objCurrent = new OpaqueClickableImage();
                                objCurrent.Uid = "frame";
                                objCurrent.Source = bii;
                                objCurrent.Stretch = Stretch.Fill;
                                frm.Children.Add(objCurrent);
                                */
                            }
                            else
                            {
                                /*
                                for (int i = 0; i < frm.Children.Count; i++)
                                {
                                    frm.Children.RemoveAt(i);
                                }
                                */
                                if ((bool)objDG_SemiOrder_Settings.DG_SemiOrder_Settings_IsImageBG)
                                {
                                    SaveXml(Convert.ToString(objDG_SemiOrder_Settings.DG_SemiOrder_Settings_AutoBright_Value), false,
                                          photonameOld,
                                          Convert.ToString(objDG_SemiOrder_Settings.DG_SemiOrder_Settings_AutoContrast_Value),
                                          Convert.ToString(objDG_SemiOrder_Settings.DG_SemiOrder_Settings_ImageFrame), objDG_SemiOrder_Settings.DG_SemiOrder_Settings_ImageFrame_Vertical,
                                          objDG_SemiOrder_Settings.DG_SemiOrder_BG, objDG_SemiOrder_Settings.DG_SemiOrder_Graphics_layer, photoId, objDG_SemiOrder_Settings.DG_SemiOrder_Image_ZoomInfo, (bool)objDG_SemiOrder_Settings.DG_SemiOrder_IsCropActive, Convert.ToInt32(objDG_SemiOrder_Settings.DG_SemiOrder_ProductTypeId), dateFolderPath);
                                }
                                else
                                {
                                    SaveXml(Convert.ToString(objDG_SemiOrder_Settings.DG_SemiOrder_Settings_AutoBright_Value), false,
                                        photonameOld,
                                        Convert.ToString(objDG_SemiOrder_Settings.DG_SemiOrder_Settings_AutoContrast_Value),
                                        Convert.ToString(objDG_SemiOrder_Settings.DG_SemiOrder_Settings_ImageFrame), objDG_SemiOrder_Settings.DG_SemiOrder_Settings_ImageFrame_Vertical,
                                        string.Empty, objDG_SemiOrder_Settings.DG_SemiOrder_Graphics_layer, photoId, objDG_SemiOrder_Settings.DG_SemiOrder_Image_ZoomInfo, (bool)objDG_SemiOrder_Settings.DG_SemiOrder_IsCropActive, Convert.ToInt32(objDG_SemiOrder_Settings.DG_SemiOrder_ProductTypeId), dateFolderPath);
                                }
                            }
                        }
                    }

                    //DG_SemiOrder_Settings objDG_SemiOrder_Settings = GetSemiOrderSettings(substoreId, LocationId);
                    if (objDG_SemiOrder_Settings != null && !IsCodeType(photoId))
                    {
                        if (is_SemiOrder == true && objDG_SemiOrder_Settings.DG_SemiOrder_Environment == false && (bool)objDG_SemiOrder_Settings.DG_SemiOrder_IsPrintActive)
                        {
                            //_objDataLayer = new DigiPhotoDataServices();
                            string[] prdIdList = objDG_SemiOrder_Settings.DG_SemiOrder_ProductTypeId.Split(',');

                            foreach (string prodList in prdIdList)
                            {
                              int OrderDetailId = (new OrderBusiness()).SetOrderDetails(photoId, prodList, (new LocationBusniess()).GetSubStoreIdbyLocationId(locationId), string.Empty);
                            }
                            //if (OrderDetailId != 0)
                            //{
                            //    (new PrinterBusniess()).SetDataToPrinterQueue(photoId.ToString(), objDG_SemiOrder_Settings.DG_SemiOrder_ProductTypeId, (new PrinterBusniess()).GetAssociatedPrinterIdFromProductTypeId(objDG_SemiOrder_Settings.DG_SemiOrder_ProductTypeId), false, OrderDetailId);
                            //}
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }

    }
}