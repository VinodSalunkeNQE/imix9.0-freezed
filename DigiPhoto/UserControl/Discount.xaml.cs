﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Collections.ObjectModel;
using DigiPhoto.DataLayer;
using System.Windows.Threading;
using System.Threading;
using DigiPhoto.Orders;
using System.Xml;
using System.Xml.Linq;
using DigiPhoto.IMIX.Business;

namespace DigiPhoto
{
    /// <summary>
    /// Interaction logic for Discount.xaml
    /// </summary>
    public partial class Discount : UserControl
    {
        /// <summary>
        /// Gets or sets a value indicating whether [is item level].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [is item level]; otherwise, <c>false</c>.
        /// </value>
        public bool IsItemLevel { get; set; }
        /// <summary>
        /// Gets or sets the discount result.
        /// </summary>
        /// <value>
        /// The discount result.
        /// </value>
        public string DiscountResult { get; set; }
        /// <summary>
        /// Gets or sets the total cost.
        /// </summary>
        /// <value>
        /// The total cost.
        /// </value>
        public double TotalCost { get; set; }
        /// <summary>
        /// Gets or sets the orignal cost.
        /// </summary>
        /// <value>
        /// The orignal cost.
        /// </value>
        public double OrignalCost { get; set; } // Cost of Orignal Order
        /// <summary>
        /// The _obj item discount
        /// </summary>
        private ObservableCollection<DiscountItem> _objItemDiscount;
        /// <summary>
        /// Initializes a new instance of the <see cref="Discount"/> class.
        /// </summary>
        public Discount()
        {
            try
            {
                InitializeComponent();
                Visibility = Visibility.Hidden;
                _objItemDiscount = new ObservableCollection<DiscountItem>();
            }
            catch (Exception ex)
            {
            }
        }

        /// <summary>
        /// The _hide request
        /// </summary>
        private bool _hideRequest = false;
        /// <summary>
        /// The _result
        /// </summary>
        private discount _result;
        /// <summary>
        /// The _parent
        /// </summary>
        private UIElement _parent;

        /// <summary>
        /// Sets the parent.
        /// </summary>
        /// <param name="parent">The parent.</param>
        public void SetParent(UIElement parent)
        {
            _parent = (UIElement)parent;
        }
        #region Message

        /// <summary>
        /// Gets or sets the message discount.
        /// </summary>
        /// <value>
        /// The message discount.
        /// </value>
        public string MessageDiscount
        {
            get { return (string)GetValue(MessageDiscountProperty); }
            set { SetValue(MessageDiscountProperty, value); }
        }

        // Using a DependencyProperty as the backing store for Message.
        // This enables animation, styling, binding, etc...
        /// <summary>
        /// The message discount property
        /// </summary>
        public static readonly DependencyProperty MessageDiscountProperty =
            DependencyProperty.Register(
                "MessageDiscount", typeof(string), typeof(ModalDialog), new UIPropertyMetadata(string.Empty));

        #endregion
        /// <summary>
        /// Shows the handler dialog.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <returns></returns>
        public discount ShowHandlerDialog(string message)
        {
            MessageDiscount = message;
            Visibility = Visibility.Visible;

            _parent.IsEnabled = true;
            GetDiscountType(false);
            _hideRequest = false;
            while (!_hideRequest)
            {
                // HACK: Stop the thread if the application is about to close
                if (this.Dispatcher.HasShutdownStarted ||
                    this.Dispatcher.HasShutdownFinished)
                {
                    break;
                }
                // HACK: Simulate "DoEvents"
                this.Dispatcher.Invoke(
                    DispatcherPriority.Background,
                    new ThreadStart(delegate { }));
                Thread.Sleep(20);
            }

            return _result;
        }

        /// <summary>
        /// Hides the handler dialog.
        /// </summary>
        private void HideHandlerDialog()
        {
            _hideRequest = true;
            Visibility = Visibility.Collapsed;
            _parent.IsEnabled = true;
        }
        /// <summary>
        /// Gets the type of the discount.
        /// </summary>
        /// <param name="onCancel">if set to <c>true</c> [configuration cancel].</param>
        public void GetDiscountType(bool onCancel)
        {
            _objItemDiscount.Clear();
            //DigiPhotoDataServices _objDataLayer = new DigiPhotoDataServices();
            if (this.IsItemLevel)
            {
                var Result = from discount in new DiscountBusiness().GetDiscountType() 
                             where discount.DG_Orders_DiscountType_ItemLevel == IsItemLevel && discount.DG_Orders_DiscountType_Active == true
                             select discount;

                foreach (var item in Result)
                {
                    DiscountItem ditem = new DiscountItem();
                    if ((bool)item.DG_Orders_DiscountType_AsPercentage)
                    {
                        ditem.DiscountName = item.DG_Orders_DiscountType_Name.ToString() + " (%): ";
                    }
                    else
                    {
                        ditem.DiscountName = item.DG_Orders_DiscountType_Name.ToString() + ": ";
                    }
                    ditem.DiscountID = item.DG_Orders_DiscountType_Pkey;
                    ditem.IsPercentageDiscount = (bool)item.DG_Orders_DiscountType_AsPercentage;
                    ditem.IsSecure = (bool)item.DG_Orders_DiscountType_Secure;
                    ditem.DiscountSyncCode = item.SyncCode;
                  //   ditem.DiscountAmount = 0;
                    _objItemDiscount.Add(ditem);

                }
                dgDiscount.ItemsSource = _objItemDiscount;
            }
            else
            {
                DiscountBusiness sa = new DiscountBusiness();
                
                var Result = from discount in new DiscountBusiness().GetDiscountType()
                             where discount.DG_Orders_DiscountType_Active == true && discount.DG_Orders_DiscountType_ItemLevel == false
                             select discount;

                foreach (var item in Result)
                {
                    DiscountItem ditem = new DiscountItem();
                    if ((bool)item.DG_Orders_DiscountType_AsPercentage)
                    {
                        ditem.DiscountName = item.DG_Orders_DiscountType_Name.ToString() + " (%): ";
                    }
                    else
                    {
                        ditem.DiscountName = item.DG_Orders_DiscountType_Name.ToString() + ": ";
                    }
                    ditem.DiscountID = item.DG_Orders_DiscountType_Pkey;
                    ditem.IsPercentageDiscount = (bool)item.DG_Orders_DiscountType_AsPercentage;
                    ditem.IsSecure = (bool)item.DG_Orders_DiscountType_Secure;
                    ditem.DiscountAmount = 0;
                    ditem.DiscountSyncCode = item.SyncCode;
                    _objItemDiscount.Add(ditem);

                }
                dgDiscount.ItemsSource = _objItemDiscount;
            }
            if (!onCancel)
            {
                ShowDiscountDetail();
            }
        }

        /// <summary>
        /// Handles the Loaded event of the UserControl control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            GetDiscountType(false);
        }

        /// <summary>
        /// Finds the name of the by.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <param name="root">The root.</param>
        /// <returns></returns>
        private FrameworkElement FindByName(string name, FrameworkElement root)
        {
            Stack<FrameworkElement> tree = new Stack<FrameworkElement>();
            tree.Push(root);
            while (tree.Count > 0)
            {
                FrameworkElement current = tree.Pop(); // root is null
                if (current.Name == name)
                    return current;

                int count = VisualTreeHelper.GetChildrenCount(current);
                for (int SupplierCounter = 0; SupplierCounter < count; ++SupplierCounter)
                {
                    DependencyObject child = VisualTreeHelper.GetChild(current, SupplierCounter);
                    if (child is FrameworkElement)
                        tree.Push((FrameworkElement)child);
                }
            }
            return null;
        }

        /// <summary>
        /// Shows the discount detail.
        /// </summary>
        private void ShowDiscountDetail()
        {
            String xml = this.DiscountResult;
            if(xml != null)
            {
                if (xml != string.Empty)
                {
                
                    XDocument XDoc = XDocument.Parse(xml);
                    int counter = 0;
                    foreach (var item in _objItemDiscount)
                   {

                       var result = XDoc.Element("Discount").Elements("Option").Single(x => (int?)x.Attribute("discountid") == item.DiscountID);
                       Double discount  = Convert.ToDouble(result.Attribute("discount").Value.ToString());
                       DataGridRow row = (DataGridRow)dgDiscount.ItemContainerGenerator.ContainerFromIndex(counter);
                        if(row != null)
                        {
                            TextBox tx = FindByName("txtquantity", row) as TextBox;
                            tx.Text = discount.ToString();
                        }
                        item.DiscountAmount = discount;
                       counter++;
                    }
                } 


            }
            else
            {
            }
        }
        /// <summary>
        /// Handles the Click event of the btnSubmit control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void btnSubmit_Click(object sender, RoutedEventArgs e)
        {
            double TotalDiscount = 0;
            double amount = 0;
            int counter = 0;

            StringBuilder sb = new StringBuilder();
            sb.Append("<Discount TotalPrice = '" + TotalCost + "'>");
            foreach (var item in _objItemDiscount)
            {

                DataGridRow row = (DataGridRow)dgDiscount.ItemContainerGenerator.ContainerFromIndex(counter);
                TextBox tx = FindByName("txtquantity", row) as TextBox;

                amount = 0;
                double amt = 0;
                if (Double.TryParse(tx.Text, out amt))
                {
                    if (item.IsPercentageDiscount)
                    {
                        amount = TotalCost * (amt / 100);
                        TotalDiscount = TotalDiscount + amount;
                    }
                    else
                    {


                        TotalDiscount = TotalDiscount + amt;
                    }


                }
                sb.Append("<Option Name='" + item.DiscountName.ToString() + "' discount='" + amt + "'  InPercentmode='" + item.IsPercentageDiscount.ToString() + "' discountid = '" + item.DiscountID.ToString() + "' discountSyncCode = '" + item.DiscountSyncCode.ToString() + "'/>");
                counter++;
            }
            sb.Append("</Discount>");
            _result = new discount();
            _result.DiscountDetail = sb.ToString();
            _result.TotalDiscountAmount = TotalDiscount;
            HideHandlerDialog();

        }

        /// <summary>
        /// Handles the Click event of the btnSubmitCancel control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void btnSubmitCancel_Click(object sender, RoutedEventArgs e)
        {
            GetDiscountType(true);
        }

        /// <summary>
        /// Handles the Click event of the btnClose control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void btnClose_Click(object sender, RoutedEventArgs e)
        {
            _result = null;
            HideHandlerDialog();
        }

        /// <summary>
        /// Handles the CurrentCellChanged event of the dgDiscount control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void dgDiscount_CurrentCellChanged(object sender, EventArgs e)
        {
            //LineItem drv = (LineItem)dgDiscount.CurrentCell.Item;
            //System.Data.DataRowView drv1 = dgDiscount.CurrentCell.Item as System.Data.DataRowView;
            //if (drv != null)
            //{
            //    //textBox1.Text = drv.Row[0].ToString();
            //    //textBox2.Text = drv.Row[1].ToString();
            //}
        }

    }
    public class DiscountItem
    {
        /// <summary>
        /// Gets or sets the name of the discount.
        /// </summary>
        /// <value>
        /// The name of the discount.
        /// </value>
        public string DiscountName { get; set; }
        /// <summary>
        /// Gets or sets the discount unique identifier.
        /// </summary>
        /// <value>
        /// The discount unique identifier.
        /// </value>
        public int DiscountID { get; set; }

        public string DiscountSyncCode { get; set; }
        /// <summary>
        /// Gets or sets the selected product type_ unique identifier.
        /// </summary>
        /// <value>
        /// The selected product type_ unique identifier.
        /// </value>
        public int SelectedProductType_ID { get; set; }
        /// <summary>
        /// Gets or sets the discount amount.
        /// </summary>
        /// <value>
        /// The discount amount.
        /// </value>
        public Double DiscountAmount { get; set; }
        /// <summary>
        /// Gets or sets a value indicating whether [is percentage discount].
        /// </summary>
        /// <value>
        /// <c>true</c> if [is percentage discount]; otherwise, <c>false</c>.
        /// </value>
        public bool IsPercentageDiscount { get; set; }
        /// <summary>
        /// Gets or sets a value indicating whether [is secure].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [is secure]; otherwise, <c>false</c>.
        /// </value>
        public bool IsSecure { get; set; }





    }
}
