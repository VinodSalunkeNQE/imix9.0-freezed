﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Text.RegularExpressions;
using DigiPhoto.DataLayer;
using System.Windows.Threading;
using System.Threading;
using DigiPhoto.DataLayer.Model;
using DigiPhoto.IMIX.Business;
namespace DigiPhoto
{
    /// <summary>
    /// Interaction logic for CtrlCashPayment.xaml
    /// </summary>
    public partial class CtrlCashPayment : UserControl
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CtrlCashPayment"/> class.
        /// </summary>
        public CtrlCashPayment()
        {
            InitializeComponent();
            Visibility = Visibility.Hidden;
        }


        /// <summary>
        /// The _parent
        /// </summary>
        private UIElement _parent;
        /// <summary>
        /// The _hide request
        /// </summary>
        private bool _hideRequest = false;
        /// <summary>
        /// The _result
        /// </summary>
        private string _result;

        /// <summary>
        /// Sets the parent.
        /// </summary>
        /// <param name="parent">The parent.</param>
        public void SetParent(UIElement parent)
        {
            _parent = (UIElement)parent;
        }
        #region Message

        /// <summary>
        /// Gets or sets the message cash payment.
        /// </summary>
        /// <value>
        /// The message cash payment.
        /// </value>
        public string MessageCashPayment
        {
            get { return (string)GetValue(MessageCashPaymentProperty); }
            set { SetValue(MessageCashPaymentProperty, value); }
        }

        // Using a DependencyProperty as the backing store for Message.
        // This enables animation, styling, binding, etc...
        /// <summary>
        /// The message cash payment property
        /// </summary>
        public static readonly DependencyProperty MessageCashPaymentProperty =
            DependencyProperty.Register(
                "MessageCashPayment", typeof(string), typeof(ModalDialog), new UIPropertyMetadata(string.Empty));



        /// <summary>
        /// Shows the handler dialog.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <returns></returns>
        public string ShowHandlerDialog(string message)
        {
            TxtAmount.Text = String.Empty;
            MessageCashPayment = message;
            Visibility = Visibility.Visible;
            LoadCurrency();
            TxtAmount.CaretIndex = 0;
            TxtAmount.Focus();
            _parent.IsEnabled = true;
            _hideRequest = false;
            TxtAmount.Text = String.Empty;
            
            
            

           // TxtAmount.Text = "0";
            while (!_hideRequest)
            {
                // HACK: Stop the thread if the application is about to close
                if (this.Dispatcher.HasShutdownStarted ||
                    this.Dispatcher.HasShutdownFinished)
                {
                    break;
                }
                // HACK: Simulate "DoEvents"
                this.Dispatcher.Invoke(
                    DispatcherPriority.Background,
                    new ThreadStart(delegate { }));
                Thread.Sleep(20);
            }
            
            return _result;
        }

        /// <summary>
        /// Hides the handler dialog.
        /// </summary>
        private void HideHandlerDialog()
        {
         
            _hideRequest = true;
            Visibility = Visibility.Collapsed;
            _parent.IsEnabled = true;
        }
        #endregion



        /// <summary>
        /// Handles the Click event of the rdoSelect control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void rdoSelect_Click(object sender, RoutedEventArgs e)
        {

        }
        /// <summary>
        /// Finds the visual child.
        /// </summary>
        /// <typeparam name="childItem">The type of the hild item.</typeparam>
        /// <param name="obj">The object.</param>
        /// <returns></returns>
        public static childItem FindVisualChild<childItem>(DependencyObject obj)
    where childItem : DependencyObject
        {
            // Search immediate children
            for (int i = 0; i < VisualTreeHelper.GetChildrenCount(obj); i++)
            {
                DependencyObject child = VisualTreeHelper.GetChild(obj, i);

                if (child is childItem)
                    return (childItem)child;

                else
                {
                    childItem childOfChild = FindVisualChild<childItem>(child);

                    if (childOfChild != null)
                        return childOfChild;
                }
            }

            return null;
        }
        /// <summary>
        /// Handles the Click event of the btnSubmit control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void btnSubmit_Click(object sender, RoutedEventArgs e)
        {
            string Currency = string.Empty;

            for (int i = 0; i < lstCurrency.Items.Count; i++)
            {
                DependencyObject obj = lstCurrency.ItemContainerGenerator.ContainerFromIndex(i);
                if (obj != null)
                {
                    RadioButton rdo = FindVisualChild<RadioButton>(obj);// FindByName("rdoSelect", row) as RadioButton;
                    if (rdo != null)
                    {
                        if (Convert.ToBoolean(rdo.IsChecked))
                        {
                            Currency = rdo.Tag.ToString();
                        }
                    }
                }
            }
            double Amount = 0;

            if (Currency != string.Empty)
            {
                if(Double.TryParse(TxtAmount.Text, out Amount))
                {
                    _result = Currency + "%#%" + Amount.ToString();
                    HideHandlerDialog();
                }

            }

        }

        /// <summary>
        /// Handles the Click event of the btnSubmitCancel control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void btnSubmitCancel_Click(object sender, RoutedEventArgs e)
        {
            TxtAmount.Focus();
            TxtAmount.Text = string.Empty;
        }

        /// <summary>
        /// Pastings the handler.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="DataObjectPastingEventArgs"/> instance containing the event data.</param>
        private void PastingHandler(object sender, DataObjectPastingEventArgs e)
        {
            if (e.DataObject.GetDataPresent(typeof(String)))
            {
                String text = (String)e.DataObject.GetData(typeof(String));
                if (!IsTextAllowed(text)) e.CancelCommand();
            }
            else e.CancelCommand();
        }
        /// <summary>
        /// Determines whether [is text allowed] [the specified text].
        /// </summary>
        /// <param name="text">The text.</param>
        /// <returns></returns>
        private static bool IsTextAllowed(string text)
        {
            Regex regex = new Regex("[^0-9.-]+"); //regex that matches disallowed text
            return !regex.IsMatch(text);
        }
        /// <summary>
        /// Handles the PreviewTextInput event of the txtAmountEntered control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="TextCompositionEventArgs"/> instance containing the event data.</param>
        private void txtAmountEntered_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = !IsTextAllowed(e.Text);
        }
        /// <summary>
        /// Loads the currency.
        /// </summary>
        private void LoadCurrency()
        {
            //DigiPhotoDataServices _objDataLayer = new DigiPhotoDataServices();
            lstCurrency.ItemsSource = new CurrencyBusiness().GetCurrencyOnly();
            
        }

        /// <summary>
        /// Handles the Click event of the btnClose control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void btnClose_Click(object sender, RoutedEventArgs e)
        {
            _result = null;
            HideHandlerDialog();
        }

        /// <summary>
        /// Handles the Loaded event of the UserControl control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            TxtAmount.Focusable = true;
            TxtAmount.Focus();
            FocusManager.SetFocusedElement(this, TxtAmount);
            Keyboard.Focus(TxtAmount);
            TxtAmount.Text = String.Empty;
        }



        /// <summary>
        /// Handles the GotKeyboardFocus event of the TxtAmount control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void TxtAmount_GotKeyboardFocus(object sender, RoutedEventArgs e)
        {

        }
    
    }
}
