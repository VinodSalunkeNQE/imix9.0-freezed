﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Threading;
using System.Windows.Threading;
using DigiPhoto.IMIX.Model;
using System.Text.RegularExpressions;
using DigiPhoto.IMIX.Business;
using DigiAuditLogger;
using System.Text;

namespace DigiPhoto
{
    /// <summary>
    /// Interaction logic for SendEmailOrder.xaml
    /// </summary>

    public partial class SendEmailOrder : UserControl
    {
        public SendEmailOrder()
        {
            InitializeComponent();
            Visibility = Visibility.Hidden;
        }

        private void btnok_Click(object sender, RoutedEventArgs e)
        {
            _result = true;
            HideHandlerDialog();
        }
        private bool _hideRequest = false;
        private bool _result = false;
        TextBox controlon;
        private UIElement _parent;

        public void SetParent(UIElement parent)
        {
            _parent = parent;
        }

        #region Message

        public EMailInfo emailInfo
        {
            get { return (EMailInfo)GetValue(mailInfoProperty); }
            set { SetValue(mailInfoProperty, value); }
        }

        public static readonly DependencyProperty mailInfoProperty =
            DependencyProperty.Register(
                "mailInfo", typeof(EMailInfo), typeof(SendEmailOrder),
                new UIPropertyMetadata(new EMailInfo()));

        #endregion

        public bool ShowHandlerDialog(EMailInfo info)
        {
            emailInfo = info;
            this.IsEnabled = true;
            Visibility = Visibility.Visible;
            _parent.IsEnabled = false;
            _hideRequest = false;
            while (!_hideRequest)
            {
                // HACK: Stop the thread if the application is about to close
                if (this.Dispatcher.HasShutdownStarted ||
                    this.Dispatcher.HasShutdownFinished)
                {
                    break;
                }

                // HACK: Simulate "DoEvents"
                this.Dispatcher.Invoke(
                    DispatcherPriority.Background,
                    new ThreadStart(delegate { }));
                Thread.Sleep(20);
            }

            return _result;
        }

        private void HideHandlerDialog()
        {
            _hideRequest = true;
            Visibility = Visibility.Hidden;
            _parent.IsEnabled = true;
        }
        private void btnSendMail_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (IsValid())
                {
                    StringBuilder OtherEmailMessage = new StringBuilder();
                    string[] OrderImgId = emailInfo.OtherMessage.Split(',');
                    foreach (string item in OrderImgId)
                    {
                        OtherEmailMessage.Append(item + "[" + txtMessage.Text + "{");
                    }
                    EMailInfo objEmailInfo = new EMailInfo()
                    {
                        Emailto = txtReciepentsEmail.Text,
                        EmailBcc = txtBCCEmail.Text == string.Empty ? string.Empty : txtBCCEmail.Text,
                        EmailIsSent = "0",
                        MailSubject = txtEmailSubject.Text == string.Empty ? string.Empty : txtEmailSubject.Text,
                        OrderId = emailInfo.OrderId,
                        EmailMessage = txtEmailBody.Text == string.Empty ? string.Empty : txtEmailBody.Text,
                        Sendername = emailInfo.Sendername,
                        MediaName = "EM",
                        OtherMessage = OtherEmailMessage.ToString().Substring(0, OtherEmailMessage.Length-1)
                    };

                    EmailBusniess objBusiness = new EmailBusniess();
                    bool result = objBusiness.InsertEmailDetails(objEmailInfo);

                    if (result)
                    {
                        DigiAuditLogger.AuditLog.AddUserLog(Common.LoginUser.UserId, (int)FrameworkHelper.ActionType.EmailFinancialReport, "Finance Report Email Sent At ");
                        //MessageBox.Show("E-Mail Sent Successfully");
                        _result = true;
                        HideHandlerDialog();
                        txtBCCEmail.Text = string.Empty;
                        txtEmailBody.Text = string.Empty;
                        txtEmailSubject.Text = string.Empty;
                        txtMessage.Text = string.Empty;
                        txtReciepentsEmail.Text = string.Empty;
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }

        }
        private bool IsValid()
        {
            Regex regex = new Regex(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$");
            Match match = regex.Match(txtReciepentsEmail.Text);

            if (txtReciepentsEmail.Text == "")
            {
                MessageBox.Show("Please Enter Reciepent MailId");
                txtReciepentsEmail.Focus();
                return false;
            }
            else if (!match.Success)
            {
                MessageBox.Show("Please Enter Vaild Email Address");
                txtReciepentsEmail.Focus();
                return false;
            }
            else if (!string.IsNullOrEmpty(txtBCCEmail.Text) && (!string.IsNullOrWhiteSpace(txtBCCEmail.Text)))
            {
                match = regex.Match(txtBCCEmail.Text);
                if (!match.Success)
                {
                    MessageBox.Show("Please Enter Vaild Email Address");
                    txtBCCEmail.Focus();
                    return false;
                }
                return true;
            }
            else
            {
                return true;
            }
        }
        private void btn_Click(object sender, RoutedEventArgs e)
        {
            Button _objbtn = (Button)sender;
            //_objbtn = (Button)sender;
            switch (_objbtn.Content.ToString())
            {
                case "ENTER":
                    {
                        KeyBorder.Visibility = Visibility.Hidden;
                        break;
                    }
                case "SPACE":
                    {
                        controlon.Text = controlon.Text + " ";
                        break;
                    }
                case "CLOSE":
                    {
                        KeyBorder.Visibility = Visibility.Hidden;
                        break;
                    }
                case "Back":
                    {
                        TextBox objtxt = (TextBox)(controlon);
                        if (controlon.Text.Length > 0)
                        {
                            controlon.Text = controlon.Text.Remove(controlon.Text.Length - 1, 1);
                        }
                        break;
                    }
                default:
                    {
                        controlon.Text = controlon.Text + _objbtn.Content;
                    }
                    break;
            }
        }

        private void txtReciepentsEmail_GotFocus(object sender, RoutedEventArgs e)
        {
            controlon = (TextBox)sender;
            KeyBorder.Visibility = Visibility.Visible;
        }

        private void txtBCCEmail_GotFocus(object sender, RoutedEventArgs e)
        {
            controlon = (TextBox)sender;
            KeyBorder.Visibility = Visibility.Visible;
        }

        private void txtEmailSubject_GotFocus(object sender, RoutedEventArgs e)
        {
            controlon = (TextBox)sender;
            KeyBorder.Visibility = Visibility.Visible;
        }

        private void txtMessage_GotFocus(object sender, RoutedEventArgs e)
        {
            controlon = (TextBox)sender;
            KeyBorder.Visibility = Visibility.Visible;
        }

        private void txtEmailBody_GotFocus(object sender, RoutedEventArgs e)
        {
            controlon = (TextBox)sender;
            KeyBorder.Visibility = Visibility.Visible;
        }
    }



}
