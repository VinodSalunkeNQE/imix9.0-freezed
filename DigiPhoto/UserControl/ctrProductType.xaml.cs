﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Collections.ObjectModel;
using DigiPhoto.Orders;
using System.Windows.Threading;
using System.Threading;
using DigiPhoto.IMIX.Model;
using DigiPhoto.IMIX.Business;
using FrameworkHelper;
using System.IO;
using DigiPhoto.Common;
using static DigiPhoto.IMIX.Model.Product;

namespace DigiPhoto
{
    /// <summary>
    /// Interaction logic for ProductType.xaml
    /// </summary>
    public partial class ctrProductType : UserControl
    {


        /// <summary>
        /// Gets or sets the type of the _ product.
        /// </summary>
        /// <value>
        /// The type of the _ product.
        /// </value>
        public List<Product> _ProductType
        {
            get;
            set;
        }

        private bool _IsActive;
        public bool IsActive
        {
            get { return _IsActive; }
            set
            {
                _IsActive = value;
                if (_IsActive == true)
                {
                    // this.IsVisibleChanged += new DependencyPropertyChangedEventHandler(LoginControl_IsVisibleChanged);
                    txtVoucher.Focus();

                }
            }
        }
        /// <summary>
        /// Gets or sets the item number.
        /// </summary>
        /// <value>
        /// The item number.
        /// </value>
        public string ItemNumber
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the _ selected product unique identifier.
        /// </summary>
        /// <value>
        /// The _ selected product unique identifier.
        /// </value>
        public int _SelectedProductID
        {
            get;
            set;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ctrProductType"/> class.
        /// </summary>
        public ctrProductType()
        {

            if (!System.ComponentModel.DesignerProperties.GetIsInDesignMode(this))
            {
                InitializeComponent();
                FillGroupCombo();
                this.lstProductType.ItemsSource = _ProductType;
                EvoucherDisplay(); ////end by latika for evoucher on 4the Feb2020
                txtVoucher.AcceptsTab = true;
                txtVoucher.Focus();
                txtVoucher.Focusable = true;
                Keyboard.Focus(txtVoucher);
                FocusManager.SetFocusedElement(this, txtVoucher);
                // this.IsVisibleChanged += new DependencyPropertyChangedEventHandler(LoginControl_IsVisibleChanged);
            }
        }
        /// <summary>
        /// ///////changed by latika for Evoucher 
        /// 
        public String _EvoucherCode
        {
            get;
            set;
        }
        public int _EvoucherPerc
        {
            get;
            set;
        }
        /// </summary> end by latika 
        private void FillGroupCombo()
        {
            //ProductBusiness proBiz = new ProductBusiness();
            //var GroupList = proBiz.GetGroupList();
            //// ... Assign the ItemsSource to the List.
            //CommonUtility.BindComboWithSelect<GroupInfo>(CmbGroup, GroupList, "GroupName", "GroupID", 0, DigiPhoto.Utility.Repository.Data.ClientConstant.SelectString);
            //// ... Make the first item selected.
            ////
            ////CmbGroup.SelectedValue = "0";

            List<SubStoresInfo> lstStoreSubStore = (new StoreSubStoreDataBusniess()).GetLogicalSubStore();
            //List<SubStoresInfo> lstStoreSubStore1 = new List<SubStoresInfo>();
            //lstStoreSubStore1 = lstStoreSubStore.Where(t => t.IsLogicalSubStore == true).ToList();
            CommonUtility.BindComboWithSelect<SubStoresInfo>(CmbGroup, lstStoreSubStore, "DG_SubStore_Name", "DG_SubStore_pkey", 0, DigiPhoto.Utility.Repository.Data.ClientConstant.SelectString);

            //Here we are finding the logical substoreid and then setting the selected value

            SubStoresInfo _objSubstore = (new StoreSubStoreDataBusniess()).GetSubstoreData(LoginUser.SubStoreId);

            //if (_objSubstore != null)
            //{
            //    var LogicalSubStoreID = _objSubstore.
            //}
            //else
            //{
            if (_objSubstore != null)
                CmbGroup.SelectedValue = (_objSubstore.LogicalSubStoreID == 0) ? LoginUser.SubStoreId.ToString() : _objSubstore.LogicalSubStoreID.ToString();
            else
                CmbGroup.SelectedValue = LoginUser.SubStoreId.ToString();
            //}

            //string strprint = string.Empty;
            //string readFilePath = Environment.CurrentDirectory;
            //if (File.Exists(readFilePath + "\\slipPrinter.dat"))
            //{
            //    using (StreamReader reader = new StreamReader(readFilePath + "\\slipPrinter.dat"))
            //    {
            //        strprint = reader.ReadLine();
            //    }
            //}

            //if (!string.IsNullOrEmpty(strprint))
            //{
            string index = CmbGroup.SelectedValue.ToString();
            if (!string.IsNullOrEmpty(index) && Convert.ToInt16(index) > 0)
            {

                // CmbGroup.SelectedValue = index;
                FillProductType(Convert.ToInt32(index));

            }
            else
            {
                CmbGroup.SelectedValue = "0";
                FillProductType(0);
            }
            //}
        }

        private void CmbGroup_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            // ... Get the ComboBox.
            var comboBox = sender as ComboBox;
            // ... Set SelectedItem as Window Title.
            if (comboBox.SelectedValue != null)
            {
                string value = comboBox.SelectedValue.ToString();
                int groupID = 0;
                if (int.TryParse(value, out groupID))
                {
                    FillProductType(Convert.ToInt32(value));
                }
                //else
                //{
                //    FillProductType(0);
                //}
            }
        }

        private void FillProductType(int SubStoreID)
        {
            try
            {
                List<ProductTypeInfo> productObj = (new ProductBusiness()).GetProductTypeforOrder(SubStoreID);
                var product = productObj.FindAll(t => t.DG_Orders_ProductNumber != 0).OrderBy(t => t.DG_Orders_ProductNumber);
                _ProductType = new List<Product>();
                foreach (ProductTypeInfo pType in product)
                {
                    Product item = new Product();
                    item.ProductID = pType.DG_Orders_ProductType_pkey;
                    item.ProductCode = pType.DG_Orders_ProductType_ProductCode; // Added by Anisur Rahman for Dubai frame (Product code + Barcode) receipt requirement
                    item.ProductName = pType.DG_Orders_ProductType_Name.ToString();
                    //Updated by Anand on 24-March-2015
                    bool isBundled = false;
                    if (pType.DG_Orders_ProductType_IsBundled.HasValue)
                    {
                        isBundled = pType.DG_Orders_ProductType_IsBundled.Value;
                    }
                    item.IsBundled = isBundled;
                    item.ProductIcon = pType.DG_Orders_ProductType_Image.ToString();
                    item.DiscountOption = (bool)pType.DG_Orders_ProductType_DiscountApplied;
                    item.IsPackage = (bool)pType.DG_IsPackage;
                    item.IsAccessory = (bool)pType.DG_IsAccessory;
                    item.MaxQuantity = pType.DG_MaxQuantity;
                    item.IsPrintType = pType.IsPrintType;
                    _ProductType.Add(item);
                }
                this.lstProductType.ItemsSource = _ProductType;
            }
            catch { }
        }
        /// <summary>
        /// The _parent
        /// </summary>
        private UIElement _parent;
        /// <summary>
        /// The _hide request
        /// </summary>
        private bool _hideRequest = false;
        /// <summary>
        /// The _result
        /// </summary>
        private int _result;

        /// <summary>
        /// Sets the parent.
        /// </summary>
        /// <param name="parent">The parent.</param>
        public void SetParent(UIElement parent)
        {
            _parent = (UIElement)parent;
        }
        #region Message

        /// <summary>
        /// Gets or sets the message1.
        /// </summary>
        /// <value>
        /// The message1.
        /// </value>
        public string Message1
        {
            get { return (string)GetValue(Message1Property); }
            set { SetValue(Message1Property, value); }
        }

        // Using a DependencyProperty as the backing store for Message.
        // This enables animation, styling, binding, etc...
        /// <summary>
        /// The message1 property
        /// </summary>
        public static readonly DependencyProperty Message1Property =
            DependencyProperty.Register(
                "Message1", typeof(string), typeof(ModalDialog), new UIPropertyMetadata(string.Empty));



        /// <summary>
        /// Shows the handler dialog.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <returns></returns>
        public int ShowHandlerDialog(string message)
        {
            Message1 = message;
            Visibility = Visibility.Visible;
            this.lstProductType.ItemsSource = _ProductType;
            SetInitial();
            _parent.IsEnabled = true;
            _hideRequest = false;
            while (!_hideRequest)
            {
                // HACK: Stop the thread if the application is about to close
                if (this.Dispatcher.HasShutdownStarted ||
                    this.Dispatcher.HasShutdownFinished)
                {
                    break;
                }
                // HACK: Simulate "DoEvents"
                this.Dispatcher.Invoke(
                    DispatcherPriority.Background,
                    new ThreadStart(delegate { }));
                Thread.Sleep(20);
            }

            return _result;
        }

        /// <summary>
        /// Hides the handler dialog.
        /// </summary>
        private void HideHandlerDialog()
        {
            _hideRequest = true;
            Visibility = Visibility.Collapsed;
            _parent.IsEnabled = true;
        }
        #endregion

        /// <summary>
        /// Finds the name of the by.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <param name="root">The root.</param>
        /// <returns></returns>
        private FrameworkElement FindByName(string name, FrameworkElement root)
        {
            Stack<FrameworkElement> tree = new Stack<FrameworkElement>();
            tree.Push(root);
            while (tree.Count > 0)
            {
                FrameworkElement current = tree.Pop(); // root is null
                if (current.Name == name)
                    return current;

                int count = VisualTreeHelper.GetChildrenCount(current);
                for (int SupplierCounter = 0; SupplierCounter < count; ++SupplierCounter)
                {
                    DependencyObject child = VisualTreeHelper.GetChild(current, SupplierCounter);
                    if (child is FrameworkElement)
                        tree.Push((FrameworkElement)child);
                }
            }
            return null;
        }

        /// <summary>
        /// Sets the initial.
        /// </summary>
        public void SetInitial()
        {
            for (int i = 0; i < lstProductType.Items.Count; i++)
            {
                DependencyObject obj = lstProductType.ItemContainerGenerator.ContainerFromIndex(i);
                if (obj != null)
                {
                    RadioButton rdo = FindVisualChild<RadioButton>(obj);
                    // FindByName("rdoSelect", row) as RadioButton;
                    if (rdo != null)
                    {
                        rdo.IsChecked = false;
                    }

                    Product item = (Product)lstProductType.Items[i];

                    if (_SelectedProductID != null)
                    {
                        if (item.ProductID == _SelectedProductID)
                        {
                            rdo.IsChecked = true;
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Finds the visual child.
        /// </summary>
        /// <typeparam name="childItem">The type of the hild item.</typeparam>
        /// <param name="obj">The object.</param>
        /// <returns></returns>
        public static childItem FindVisualChild<childItem>(DependencyObject obj)
    where childItem : DependencyObject
        {
            // Search immediate children
            for (int i = 0; i < VisualTreeHelper.GetChildrenCount(obj); i++)
            {
                DependencyObject child = VisualTreeHelper.GetChild(obj, i);

                if (child is childItem)
                    return (childItem)child;

                else
                {
                    childItem childOfChild = FindVisualChild<childItem>(child);

                    if (childOfChild != null)
                        return childOfChild;
                }
            }

            return null;
        }
        /// <summary>
        /// Handles the Click event of the btnSubmit control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void btnSubmit_Click(object sender, RoutedEventArgs e)
        {
            int productid = 0;
            string productName = string.Empty;
            for (int i = 0; i < lstProductType.Items.Count; i++)
            {
                DependencyObject obj = lstProductType.ItemContainerGenerator.ContainerFromIndex(i);
                if (obj != null)
                {
                    RadioButton rdo = FindVisualChild<RadioButton>(obj);// FindByName("rdoSelect", row) as RadioButton;
                    if (rdo != null)
                    {
                        if (Convert.ToBoolean(rdo.IsChecked))
                        {
                            Product item = (Product)lstProductType.Items[i];
                            productid = item.ProductID;
                            productName = item.ProductName;
                        }
                    }
                }
            }
            if (productName.ToLower().Contains("instamobile"))
            {
                InstaMobileProd.InstaMobileProductName = productName;
            }
            else
            {
                InstaMobileProd.InstaMobileProductName = string.Empty;
            }
            _result = productid;
            HideHandlerDialog();


            //int counter = 0;
            //int productid = 0;
            //StringBuilder sb = new StringBuilder();

            //foreach (var item in _ProductType)
            //{
            //    ListBoxItem row = (ListBoxItem)lstProductType.ItemContainerGenerator.ContainerFromIndex(counter);
            //    RadioButton rdo = FindByName("rdoSelect", row) as RadioButton;
            //    if (rdo != null)
            //    {
            //        if (Convert.ToBoolean(rdo.IsChecked))
            //        {
            //            productid = item.ProductID;
            //        }
            //    }
            //    counter++;
            //}

            //_result = productid;
            //HideHandlerDialog(); 


        }

        /// <summary>
        /// Handles the Click event of the btnSubmitCancel control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void btnSubmitCancel_Click(object sender, RoutedEventArgs e)
        {
            for (int i = 0; i < lstProductType.Items.Count; i++)
            {
                DependencyObject obj = lstProductType.ItemContainerGenerator.ContainerFromIndex(i);
                if (obj != null)
                {
                    RadioButton rdo = FindVisualChild<RadioButton>(obj);// FindByName("rdoSelect", row) as RadioButton;
                    if (rdo != null)
                    {
                        rdo.IsChecked = false;
                    }

                    //Product item = (Product)lstProductType.Items[i];

                    //if (_SelectedProductID != null)
                    //{
                    //    if (item.ProductID == _SelectedProductID)
                    //    {
                    //        rdo.IsChecked = true;
                    //    }
                    //}
                }
            }
            //  HideHandlerDialog();
        }

        /// <summary>
        /// Handles the Click event of the btnClose control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void btnClose_Click(object sender, RoutedEventArgs e)
        {
            _result = -2;
            HideHandlerDialog();
        }

        /// <summary>
        /// Handles the Click event of the rdoSelect control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void rdoSelect_Click(object sender, RoutedEventArgs e)
        {
            btnSubmit_Click(null, null);
        }
        public void EvoucherDisplay()
        {
            ConfigBusiness conBiz = new ConfigBusiness();
            bool IsEvoucherActive = false;
            try
            {
                var EvoucherMst = (ConfigManager.IMIXConfigurations.ToDictionary(k => k.Key, k => k.Value).Where(k => k.Key.Equals((int)conBiz.GETEvoucherID()))).ToList();
                IsEvoucherActive = Convert.ToBoolean(EvoucherMst[0].Value);
            }
            catch { }
            if (IsEvoucherActive)
            {


                txtVoucher.Visibility = Visibility.Visible;
                txtVoucher.Focus();
                txtVoucher.Focusable = true;
                Keyboard.Focus(txtVoucher);
                Voucherbl.Visibility = Visibility.Visible;
                CmbGroup.Margin = new Thickness(430, 0, 0, 16);
            }
            else
            {
                txtVoucher.Visibility = Visibility.Hidden;
                Voucherbl.Visibility = Visibility.Hidden;
                CmbGroup.Margin = new Thickness(0, 0, 0, 16);
            }
        }



        public void Evoucher()
        {
            lblEvoucherErr.Text = "";
            if (txtVoucher.Text.Length > 7 && txtVoucher.Text.Length < 9)
            {

                ConfigBusiness conBiz = new ConfigBusiness();
                bool IsEvoucherActive = false;
                try
                {
                    var EvoucherMst = (ConfigManager.IMIXConfigurations.ToDictionary(k => k.Key, k => k.Value).Where(k => k.Key.Equals((int)conBiz.GETEvoucherID()))).ToList();
                    IsEvoucherActive = Convert.ToBoolean(EvoucherMst[0].Value);
                }
                catch { }

                if (!string.IsNullOrEmpty(txtVoucher.Text))
                {
                    if (IsEvoucherActive)
                    {
                        EvoucherManager objevchr = new EvoucherManager();
                        EvoucherClaiminfo objevClm = new EvoucherClaiminfo();
                        objevClm = objevchr.EvoucherClaim(txtVoucher.Text);
                        if (objevClm.Result == "YES")
                        {

                            string index = CmbGroup.SelectedValue.ToString();
                            if (!string.IsNullOrEmpty(index) && Convert.ToInt16(index) > 0)
                            {////, objevClm.Barcode ,Convert.ToInt32(objevClm.Amount)
                                _EvoucherCode = objevClm.Barcode;
                                _EvoucherPerc = Convert.ToInt32(objevClm.Amount);
                                FillProductTypeEvoucher(Convert.ToInt32(index));

                            }
                            else
                            {
                                CmbGroup.SelectedValue = "0";
                                _EvoucherCode = objevClm.Barcode;
                                _EvoucherPerc = 0;
                                FillProductTypeEvoucher(0);
                            }
                        }
                        else
                        {
                            ///lblEvoucherErr.Text = "This Evoucher Code(" + objevClm.Barcode + ") is used kindly enter another code.";
                            if (!string.IsNullOrEmpty(objevClm.Result))
                                lblEvoucherErr.Text = objevClm.Result;
                            else lblEvoucherErr.Text = "Evoucher Code(" + txtVoucher.Text + ") is Not Valid kindly Use Valid Code.";
                            //txtVoucher.Text = "";
                        }
                    }
                    ///else { lblEvoucherErr.Text= "IsEvoucherActive settings are not active from Digiconfig utility in substore tab. kindly active test again".  }
                }
                else { FillGroupCombo(); }
            }

            else if (string.IsNullOrEmpty(txtVoucher.Text))
            {
                FillGroupCombo();
            }
        }



        private void FillProductTypeEvoucher(int SubStoreID)
        {
            try
            {

                List<ProductTypeInfo> productObj = (new ProductBusiness()).GetProductTypeforOrderEvoucher(SubStoreID, _EvoucherCode);
                //var product = productObj.FindAll(t => t.DG_Orders_ProductNumber != 0).OrderBy(t => t.DG_Orders_ProductNumber);
                ///var product = productObj.FindAll(t => t.IsEvoucherDisc == true).OrderBy(t => t.DG_Orders_ProductNumber);
                _ProductType = new List<Product>();
                foreach (ProductTypeInfo pType in productObj)
                {
                    Product item = new Product();
                    item.ProductID = pType.DG_Orders_ProductType_pkey;
                    item.ProductCode = pType.DG_Orders_ProductType_ProductCode; // Added by Anisur Rahman for Dubai frame (Product code + Barcode) receipt requirement
                    item.ProductName = pType.DG_Orders_ProductType_Name.ToString();
                    //Updated by Anand on 24-March-2015
                    bool isBundled = false;
                    if (pType.DG_Orders_ProductType_IsBundled.HasValue)
                    {
                        isBundled = pType.DG_Orders_ProductType_IsBundled.Value;
                    }
                    item.IsBundled = isBundled;
                    item.ProductIcon = pType.DG_Orders_ProductType_Image.ToString();
                    item.DiscountOption = (bool)pType.DG_Orders_ProductType_DiscountApplied;
                    item.IsPackage = (bool)pType.DG_IsPackage;
                    item.IsAccessory = (bool)pType.DG_IsAccessory;
                    item.MaxQuantity = pType.DG_MaxQuantity;
                    item.IsPrintType = pType.IsPrintType;
                    item.IsEvoucherDisc = true;////added by latika for Evoucher
                    item.EvoucherPer = _EvoucherPerc;
                    item.EvoucherCode = _EvoucherCode;

                    _ProductType.Add(item);
                }
                this.lstProductType.ItemsSource = _ProductType;
            }
            catch { }
        }
        private void TxtVoucher_MouseLeave(object sender, MouseEventArgs e)
        {
            Evoucher();
        }

        private void TxtVoucher_LostKeyboardFocus(object sender, KeyboardFocusChangedEventArgs e)
        {
            Evoucher();
        }
        private void txtVoucher_TextChanged(object sender, TextChangedEventArgs e)
        {
            lblEvoucherErr.Text = "";
            if (txtVoucher.Text.Length > 7 && txtVoucher.Text.Length < 9) { Evoucher(); }
            else if (string.IsNullOrEmpty(txtVoucher.Text)) { FillGroupCombo(); }
        }

        private void txtVoucher_KeyDown(object sender, KeyEventArgs e)
        {
            Evoucher();
        }

        private void txtVoucher_GotFocus(object sender, RoutedEventArgs e)
        {

            Evoucher();
        }

        private void txtVoucher_IsKeyboardFocusWithinChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            Evoucher();
        }


        private void txtVoucher_LostFocus(object sender, RoutedEventArgs e)
        {
            Evoucher();
        }
        //void LoginControl_IsVisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
        //{
        //    if ((bool)e.NewValue == true)
        //    {
        //        Dispatcher.BeginInvoke(
        //        DispatcherPriority.ContextIdle,
        //        new Action(delegate ()
        //        {
        //            //txtVoucher.Focus();
        //            txtVoucher.TabIndex = 0;
        //            txtVoucher.Text = "sfdsadf"; 
        //        }));
        //    }
        //}
        /////end by latika
    }
}

