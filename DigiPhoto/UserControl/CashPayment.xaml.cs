﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Text.RegularExpressions;
using System.Windows.Threading;
using System.Threading;
using DigiPhoto.DataLayer;
using DigiPhoto.IMIX.Business;

namespace DigiPhoto
{
    /// <summary>
    /// Interaction logic for CashPayment.xaml
    /// </summary>
    public partial class CashPayment : UserControl
    {
        public CashPayment()
        {
            InitializeComponent();
        }
        private static bool IsTextAllowed(string text)
        {
            Regex regex = new Regex("[^0-9.-]+"); //regex that matches disallowed text
            return !regex.IsMatch(text);
        }
        private void txtAmountEntered_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = !IsTextAllowed(e.Text);
        }
        private FrameworkElement FindByName(string name, FrameworkElement root)
        {
            Stack<FrameworkElement> tree = new Stack<FrameworkElement>();
            tree.Push(root);
            while (tree.Count > 0)
            {
                FrameworkElement current = tree.Pop(); // root is null
                if (current.Name == name)
                    return current;

                int count = VisualTreeHelper.GetChildrenCount(current);
                for (int SupplierCounter = 0; SupplierCounter < count; ++SupplierCounter)
                {
                    DependencyObject child = VisualTreeHelper.GetChild(current, SupplierCounter);
                    if (child is FrameworkElement)
                        tree.Push((FrameworkElement)child);
                }
            }
            return null;
        }
        private void PastingHandler(object sender, DataObjectPastingEventArgs e)
        {
            if (e.DataObject.GetDataPresent(typeof(String)))
            {
                String text = (String)e.DataObject.GetData(typeof(String));
                if (!IsTextAllowed(text)) e.CancelCommand();
            }
            else e.CancelCommand();
        }
       
        private void txtAmountEntered_TextChanged(object sender, TextChangedEventArgs e)
        {
            
        }
        private bool _hideRequest = false;
        private String _result = string.Empty;
        private UIElement _parent;

        public void SetParent(UIElement parent)
        {
            _parent = (UIElement)parent;
        }
        #region Message

        public string Message2
        {
            get { return (string)GetValue(Message2Property); }
            set { SetValue(Message2Property, value); }
        }

        // Using a DependencyProperty as the backing store for Message.
        // This enables animation, styling, binding, etc...
        public static readonly DependencyProperty Message2Property =
            DependencyProperty.Register(
                "Message2", typeof(string), typeof(ModalDialog), new UIPropertyMetadata(string.Empty));

        #endregion
        private void HideHandlerDialog()
        {
            _hideRequest = true;
            Visibility = Visibility.Collapsed;
            _parent.IsEnabled = true;
        }


        private void LoadCurrency()
        {
            //DigiPhotoDataServices obj = new DigiPhotoDataServices();
            //lstCurrency.ItemsSource = obj.GetCurrencyList();
            lstCurrency.ItemsSource= (new CurrencyBusiness()).GetCurrencyList();
        }        
        public String ShowHandlerDialog(string message)
        {
            Message2 = message;
            Visibility = Visibility.Visible;
            LoadCurrency();
            _parent.IsEnabled = true;
           
            _hideRequest = false;
            while (!_hideRequest)
            {
                // HACK: Stop the thread if the application is about to close
                if (this.Dispatcher.HasShutdownStarted ||
                    this.Dispatcher.HasShutdownFinished)
                {
                    break;
                }
                // HACK: Simulate "DoEvents"
                this.Dispatcher.Invoke(
                    DispatcherPriority.Background,
                    new ThreadStart(delegate { }));
                Thread.Sleep(20);
            }

            return _result;
        }

        private void btnClose_Click(object sender, RoutedEventArgs e)
        {
            HideHandlerDialog();
        }

        private void btnSubmit_Click(object sender, RoutedEventArgs e)
        {

        }

        private void btnSubmitCancel_Click(object sender, RoutedEventArgs e)
        {
            HideHandlerDialog();
        }

    }
}
