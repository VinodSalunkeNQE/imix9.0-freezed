﻿using DigiPhoto.Manage;
using DigiPhoto.Common;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace DigiPhoto
{
    /// <summary>
    /// Interaction logic for DynamicImgCrop.xaml
    /// </summary>
    public partial class DynamicImgCrop : UserControl
    {
        #region Declarations
        String _cropSize = string.Empty;
        System.Windows.Media.Brush _brOriginal;
        CroppingAdorner _clp;
        FrameworkElement _felCur = null;
        //UIElement _shapeToRemove;
        //private int _drawX = 0;
        //private int _drawY = 0;
        double _canvasTop;
        double _canvasLeft;
        int _rotateangle;
        private int _flipMode;
        private int _flipModeY;
        private TransformGroup _transformGroup;
        private TranslateTransform _translateTransform;
        private ScaleTransform _zoomTransform;
        private RotateTransform _rotateTransform;
        //string _centerX = "";
        //string _centerY = "";
        private UIElement _parent;
        public void SetParent(UIElement parent)
        {
            _parent = (UIElement)parent;
        }

        #endregion

        #region Constructor
        public DynamicImgCrop()
        {
            InitializeComponent();
            Onload();

            dragCanvas.IsEnabled = true;

            GrdsubCrop.Visibility = Visibility.Visible;
            GrdFlip.LayoutTransform = new TransformGroup();
            GrdRotate.LayoutTransform = new TransformGroup();
            grdZoomCanvas.Visibility = System.Windows.Visibility.Collapsed;
            GrdRotateCropParent.Visibility = System.Windows.Visibility.Visible;

            DragCanvas.SetCanBeDragged(Opacitymsk, false);
            DragCanvas.SetCanBeDragged(mainImage, false);

            MyInkCanvas.SnapsToDevicePixels = true;
            MyInkCanvas.SetValue(RenderOptions.EdgeModeProperty, EdgeMode.Aliased);

            mainImage.SnapsToDevicePixels = true;
            mainImage.SetValue(RenderOptions.EdgeModeProperty, EdgeMode.Aliased);
            mainImage.OverridesDefaultStyle = true;

            Canvas.SetZIndex(imageundoGrid, 0);
            Canvas.SetZIndex(canbackgroundParent, 4);
            Canvas.SetZIndex(Opacitymsk, 2);

            canbackgroundParent.IsHitTestVisible = false;
            canbackground.IsHitTestVisible = false;
        }

        #endregion

        #region Cropping Click Buttons
        private void btnSelectCrop8By10_Click(object sender, RoutedEventArgs e)
        {

            btnSelectReverse.IsEnabled = true;
            btnenlargeminus.IsEnabled = true;
            btnenlargeplus.IsEnabled = true;
            Button eventsender = (Button)sender;
            if ((mainImage.Source).Width < (mainImage.Source).Height)
            {
                _cropSize = "8 * 10";
                try
                {
                    decimal aspectRatio = 0.8M;
                    if (eventsender.Tag != null)
                    {
                        AddCropToElement(GrdCrop, true, mainImage, aspectRatio);
                    }
                    else
                    {
                        AddCropToElement(GrdCrop, false, mainImage, aspectRatio);
                    }
                    _brOriginal = _clp.Fill;

                }
                catch (Exception ex)
                {
                    string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                    ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                }
            }
            else
            {
                _cropSize = "10 * 8";
                try
                {
                    decimal aspectRatio = 1.25M;
                    if (eventsender.Tag != null)
                    {
                        AddCropToElement(GrdCrop, true, mainImage, aspectRatio);
                    }
                    else
                    {
                        AddCropToElement(GrdCrop, false, mainImage, aspectRatio);
                    }
                    _brOriginal = _clp.Fill;

                }
                catch (Exception ex)
                {
                    string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                    ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                }
            }

        }
        private void btnSelectCrop6By8_Click(object sender, RoutedEventArgs e)
        {

            btnSelectReverse.IsEnabled = true;
            btnenlargeminus.IsEnabled = true;
            btnenlargeplus.IsEnabled = true;
            if ((mainImage.Source).Width < (mainImage.Source).Height)
            {
                _cropSize = "6 * 8";
                try
                {
                    Button eventsender = (Button)sender;
                    {
                        decimal aspectRatio = 0.75M;
                        if (eventsender.Tag != null)
                        {
                            AddCropToElement(GrdCrop, true, mainImage, aspectRatio);
                        }
                        else
                        {
                            AddCropToElement(GrdCrop, false, mainImage, aspectRatio);
                        }
                        _brOriginal = _clp.Fill;
                    }
                }
                catch (Exception ex)
                {
                    string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                    ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                }
            }
            else
            {
                _cropSize = "8 * 6";
                try
                {
                    Button eventsender = (Button)sender;
                    {
                        decimal aspectRatio = 1.33M;
                        if (eventsender.Tag != null)
                        {
                            AddCropToElement(GrdCrop, true, mainImage, aspectRatio);
                        }
                        else
                        {
                            AddCropToElement(GrdCrop, false, mainImage, aspectRatio);
                        }
                        _brOriginal = _clp.Fill;
                    }
                }
                catch (Exception ex)
                {
                    string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                    ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                }
            }
        }
        private void btnSelectCrop4by6_Click(object sender, RoutedEventArgs e)
        {

            btnSelectReverse.IsEnabled = true;
            btnenlargeminus.IsEnabled = true;
            btnenlargeplus.IsEnabled = true;
            if ((mainImage.Source).Width < (mainImage.Source).Height)
            {
                _cropSize = "4 * 6";
                try
                {
                    Button eventsender = (Button)sender;
                    {
                        decimal aspectRatio = .66M;
                        if (eventsender.Tag != null)
                        {
                            AddCropToElement(GrdCrop, true, mainImage, aspectRatio);
                        }
                        else
                        {
                            AddCropToElement(GrdCrop, false, mainImage, aspectRatio);
                        }
                        _brOriginal = _clp.Fill;
                    }
                }
                catch (Exception ex)
                {
                    string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                    ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                }
            }
            else
            {
                _cropSize = "6 * 4";
                try
                {
                    Button eventsender = (Button)sender;
                    {
                        decimal aspectRatio = 1.5M;
                        if (eventsender.Tag != null)
                        {
                            AddCropToElement(GrdCrop, true, mainImage, aspectRatio);
                        }
                        else
                        {
                            AddCropToElement(GrdCrop, false, mainImage, aspectRatio);
                        }
                        _brOriginal = _clp.Fill;
                    }
                }
                catch (Exception ex)
                {
                    string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                    ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                }
            }
        }
        private void btnSelectCrop3By3_Click(object sender, RoutedEventArgs e)
        {
            _cropSize = "3 * 3";
            btnenlargeminus.IsEnabled = true;
            btnenlargeplus.IsEnabled = true;
            try
            {
                Button eventsender = (Button)sender;
                {
                    decimal aspectRatio = 1M;
                    if (eventsender.Tag != null)
                    {
                        AddCropToElement(GrdCrop, true, mainImage, aspectRatio);
                    }
                    else
                    {
                        AddCropToElement(GrdCrop, false, mainImage, aspectRatio);
                    }
                    _brOriginal = _clp.Fill;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }
		// Arvind - 23/02/2016 - 5 * 7 Crop
        private void btnSelectCrop5By7_Click(object sender, RoutedEventArgs e)
        {

            btnSelectReverse.IsEnabled = true;
            btnenlargeminus.IsEnabled = true;
            btnenlargeplus.IsEnabled = true;
            if ((mainImage.Source).Width < (mainImage.Source).Height)
            {
                _cropSize = "5 * 7";
                try
                {
                    Button eventsender = (Button)sender;
                    {
                        decimal aspectRatio = .71M;
                        if (eventsender.Tag != null)
                        {
                            AddCropToElement(GrdCrop, true, mainImage, aspectRatio);
                        }
                        else
                        {
                            AddCropToElement(GrdCrop, false, mainImage, aspectRatio);
                        }
                        _brOriginal = _clp.Fill;
                    }
                }

                catch (Exception ex)
                {
                    string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                    ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                }
            }
            else
            {
                _cropSize = "7 * 5";
                try
                {
                    Button eventsender = (Button)sender;
                    {
                        decimal aspectRatio = 1.4M;
                        if (eventsender.Tag != null)
                        {
                            AddCropToElement(GrdCrop, true, mainImage, aspectRatio);
                        }
                        else
                        {
                            AddCropToElement(GrdCrop, false, mainImage, aspectRatio);
                        }
                        _brOriginal = _clp.Fill;
                    }
                }
                catch (Exception ex)
                {
                    string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                    ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                }
            }
        }
        private void btnSelectReverse_Click(object sender, RoutedEventArgs e)
        {
            if (_cropSize == "8 * 10")
            {
                _cropSize = "10 * 8";
                try
                {
                    Button eventsender = (Button)sender;
                    {
                        decimal aspectRatio = 1.25M;// 0.772727272727273M;
                        if (eventsender.Tag != null)
                        {
                            AddCropToElement(GrdCrop, true, mainImage, aspectRatio);
                        }
                        else
                        {
                            AddCropToElement(GrdCrop, false, mainImage, aspectRatio);
                        }
                        _brOriginal = _clp.Fill;
                    }
                }
                catch (Exception ex)
                {
                    string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                    ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                }
            }
            else if (_cropSize == "10 * 8")
            {
                _cropSize = "8 * 10";
                try
                {
                    Button eventsender = (Button)sender;
                    {
                        decimal aspectRatio = 0.8M;// 0.772727272727273M;
                        if (eventsender.Tag != null)
                        {
                            AddCropToElement(GrdCrop, true, mainImage, aspectRatio);
                        }
                        else
                        {
                            AddCropToElement(GrdCrop, false, mainImage, aspectRatio);
                        }
                        _brOriginal = _clp.Fill;
                    }
                }
                catch (Exception ex)
                {
                    string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                    ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                }
            }
            else if (_cropSize == "6 * 8")
            {
                _cropSize = "8 * 6";
                try
                {
                    Button eventsender = (Button)sender;
                    {
                        decimal aspectRatio = 1.33M;// 0.772727272727273M;
                        if (eventsender.Tag != null)
                        {
                            AddCropToElement(GrdCrop, true, mainImage, aspectRatio);
                        }
                        else
                        {
                            AddCropToElement(GrdCrop, false, mainImage, aspectRatio);
                        }
                        _brOriginal = _clp.Fill;
                    }
                }
                catch (Exception ex)
                {
                    string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                    ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                }
            }
            else if (_cropSize == "8 * 6")
            {
                _cropSize = "6 * 8";
                try
                {
                    Button eventsender = (Button)sender;
                    {
                        decimal aspectRatio = 0.75M;// 0.772727272727273M;
                        if (eventsender.Tag != null)
                        {
                            AddCropToElement(GrdCrop, true, mainImage, aspectRatio);
                        }
                        else
                        {
                            AddCropToElement(GrdCrop, false, mainImage, aspectRatio);
                        }
                        _brOriginal = _clp.Fill;
                    }
                }
                catch (Exception ex)
                {
                    string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                    ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                }
            }
            else if (_cropSize == "4 * 6")
            {
                _cropSize = "6 * 4";
                try
                {
                    Button eventsender = (Button)sender;
                    {
                        decimal aspectRatio = 1.5M;// 0.772727272727273M;
                        if (eventsender.Tag != null)
                        {
                            AddCropToElement(GrdCrop, true, mainImage, aspectRatio);
                        }
                        else
                        {
                            AddCropToElement(GrdCrop, false, mainImage, aspectRatio);
                        }
                        _brOriginal = _clp.Fill;
                    }
                }
                catch (Exception ex)
                {
                    string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                    ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                }
            }
            else if (_cropSize == "6 * 4")
            {
                _cropSize = "4 * 6";
                try
                {
                    Button eventsender = (Button)sender;
                    {
                        decimal aspectRatio = .66M;// 0.772727272727273M;
                        if (eventsender.Tag != null)
                        {
                            AddCropToElement(GrdCrop, true, mainImage, aspectRatio);
                        }
                        else
                        {
                            AddCropToElement(GrdCrop, false, mainImage, aspectRatio);
                        }
                        _brOriginal = _clp.Fill;
                    }
                }
                catch (Exception ex)
                {
                    string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                    ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                }
            }
            else if (_cropSize == "5 * 7") // Arvind - 23/02/2016 - 5 * 7 Crop
            {
                _cropSize = "7 * 5";
                try
                {
                    Button eventsender = (Button)sender;
                    {
                        decimal aspectRatio = 1.4M;
                        if (eventsender.Tag != null)
                        {
                            AddCropToElement(GrdCrop, true, mainImage, aspectRatio);
                        }
                        else
                        {
                            AddCropToElement(GrdCrop, false, mainImage, aspectRatio);
                        }
                        _brOriginal = _clp.Fill;
                    }
                }
                catch (Exception ex)
                {
                    string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                    ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                }
            }
            else if (_cropSize == "7 * 5")
            {
                _cropSize = "5 * 7";
                try
                {
                    Button eventsender = (Button)sender;
                    {
                        decimal aspectRatio = 0.71M;
                        if (eventsender.Tag != null)
                        {
                            AddCropToElement(GrdCrop, true, mainImage, aspectRatio);
                        }
                        else
                        {
                            AddCropToElement(GrdCrop, false, mainImage, aspectRatio);
                        }
                        _brOriginal = _clp.Fill;
                    }
                }
                catch (Exception ex)
                {
                    string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                    ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                }
            }
        }
        private void btnenlargeplus_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (_clp == null)
                    return;

                Rect rcInterior = _clp.ClippingRectangle;

                double grdActualWidth = GrdCrop.ActualWidth;
                double grdActualheight = GrdCrop.ActualHeight;

                rcInterior = new Rect(rcInterior.X, rcInterior.Y, rcInterior.Width, rcInterior.Height);

                double x = rcInterior.X;
                double y = rcInterior.Y;
                AdornerLayer aly = AdornerLayer.GetAdornerLayer(GrdCrop);
                rcInterior.Scale(1.01, 1.01);
                rcInterior.X = x - x * 0.01 / 2;
                rcInterior.Y = y - y * 0.01 / 2;

                if (rcInterior.X + rcInterior.Width > grdActualWidth)
                {
                    return;
                }

                if (rcInterior.Y + rcInterior.Height > grdActualheight)
                {
                    return;
                }

                if (_felCur != null)
                {
                    RemoveCropFromCur();
                }

                if (_clp == null)
                    return;

                if (rcInterior.X < -3)
                {
                    rcInterior.X = -3;
                }

                if (rcInterior.Y < -3)
                {
                    rcInterior.Y = -3;
                }

                _clp = new CroppingAdorner(GrdCrop, rcInterior);

                aly.Add(_clp);
                _felCur = GrdCrop;
                SetClipColorGrey();
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }
        private void btnenlargeminus_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (_felCur != null)
                {
                    RemoveCropFromCur();
                }

                if (_clp == null)
                    return;

                Rect rcInterior = _clp.ClippingRectangle;

                rcInterior = new Rect(rcInterior.X, rcInterior.Y, rcInterior.Width, rcInterior.Height);

                double x = rcInterior.X;
                double y = rcInterior.Y;
                AdornerLayer aly = AdornerLayer.GetAdornerLayer(GrdCrop);
                rcInterior.Width -= rcInterior.Width * 0.01;
                rcInterior.Height -= rcInterior.Height * 0.01;

                rcInterior.X = x + x * 0.01 / 2;
                rcInterior.Y = y + y * 0.01 / 2;

                if (rcInterior.Width <= 200)
                {
                    rcInterior.Width = 200;
                    rcInterior.X = x;
                    //fix the width of rectangle.
                }
                if (rcInterior.Height <= 200)
                {
                    rcInterior.Height = 200;
                    rcInterior.Y = y;
                }

                _clp = new CroppingAdorner(GrdCrop, rcInterior);

                aly.Add(_clp);
                _felCur = GrdCrop;
                SetClipColorGrey();
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }

        #endregion

        #region Common Functions

        private void AddCropToElement(FrameworkElement fel, bool IsRedeye, System.Windows.Controls.Image cropImage, decimal aspectRatio)
        {
            try
            {
                if (_felCur != null)
                {
                    RemoveCropFromCur();
                }
                if (!IsRedeye)
                {
                    Rect rcInterior = new Rect();
                    rcInterior = GetCropRectangle(aspectRatio);
                    AdornerLayer aly = AdornerLayer.GetAdornerLayer(GrdCrop);
                    _clp = new CroppingAdorner(GrdCrop, rcInterior);
                    aly.Add(_clp);
                    _felCur = GrdCrop;
                    SetClipColorGrey();
                }
                else
                {
                    Rect rcInterior = new Rect(
                      62.671083040935912,
                     38.358400000000017,
                      60.671083040935912,
                     38.358400000000017);
                    AdornerLayer aly = AdornerLayer.GetAdornerLayer(GrdCrop);
                    _clp = new CroppingAdorner(GrdCrop, rcInterior);
                    aly.Add(_clp);
                    _felCur = GrdCrop;
                    SetClipColorGrey();
                }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
            finally
            {
                MemoryManagement.FlushMemory();
            }
        }
        private void AddCropToElementDefault(FrameworkElement fel, System.Windows.Controls.Image cropImage)
        {
            try
            {
                Rect rcInterior = new Rect();
                if (cmbImgOrientation.SelectedIndex == 0)
                {
                    if (!String.IsNullOrWhiteSpace(Configuration.HorizontalCropCoordinates))
                    {
                        string[] rectValues = Configuration.HorizontalCropCoordinates.Split(',');
                        rcInterior = new Rect(Convert.ToDouble(rectValues[0]), Convert.ToDouble(rectValues[1]), Convert.ToDouble(rectValues[2]), Convert.ToDouble(rectValues[3]));
                        AdornerLayer aly = AdornerLayer.GetAdornerLayer(GrdCrop);
                        _clp = new CroppingAdorner(GrdCrop, rcInterior);
                        aly.Add(_clp);
                        _felCur = GrdCrop;
                        SetClipColorGrey();
                        btnenlargeplus.IsEnabled = true;
                        btnenlargeminus.IsEnabled = true;
                    }
                }
                else if (cmbImgOrientation.SelectedIndex == 1)
                {
                    if (!String.IsNullOrWhiteSpace(Configuration.VerticalCropCoordinates))
                    {
                        string[] rectValues = Configuration.VerticalCropCoordinates.Split(',');
                        rcInterior = new Rect(Convert.ToDouble(rectValues[0]), Convert.ToDouble(rectValues[1]), Convert.ToDouble(rectValues[2]), Convert.ToDouble(rectValues[3]));
                        AdornerLayer aly = AdornerLayer.GetAdornerLayer(GrdCrop);
                        _clp = new CroppingAdorner(GrdCrop, rcInterior);
                        aly.Add(_clp);
                        _felCur = GrdCrop;
                        SetClipColorGrey();
                        btnenlargeplus.IsEnabled = true;
                        btnenlargeminus.IsEnabled = true;
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
            finally
            {
                MemoryManagement.FlushMemory();
            }
        }
        private void RemoveCropFromCur()
        {
            try
            {
                if (_felCur != null)
                {
                AdornerLayer aly = AdornerLayer.GetAdornerLayer(_felCur);
                aly.Remove(_clp);
                aly.ReleaseAllTouchCaptures();
                _felCur.ReleaseAllTouchCaptures();
                _clp.ReleaseAllTouchCaptures();
                _clp.ReleaseResources(_felCur);
            }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }
        private Rect GetCropRectangle(decimal constraintRatio)
        {
            Rect tempRcInterior = new Rect();
            Rect rcInterior = new Rect();
            try
            {
                double cropImageActualWidth = GrdCrop.ActualWidth;
                double cropImageActualHeight = GrdCrop.ActualHeight;

                double imageRatio = GrdCrop.ActualWidth / GrdCrop.ActualHeight;

                if (constraintRatio == 0.8M)
                {
                    double rectheight = cropImageActualHeight / 2;
                    double rectweight = ((double)8 / 10) * rectheight;

                    rcInterior = new Rect(0, 0, rectweight, rectheight);
                    tempRcInterior = new Rect(0, 0, rectweight, rectheight);
                    while (!(rcInterior.X + rcInterior.Width > GrdCrop.ActualWidth) && !(rcInterior.Y + rcInterior.Height > GrdCrop.ActualHeight))
                    {
                        if ((tempRcInterior.X + tempRcInterior.Width > GrdCrop.ActualWidth) || (tempRcInterior.Y + tempRcInterior.Height > GrdCrop.ActualHeight))
                            break;
                        else
                            rcInterior.Scale(1.01, 1.01);
                    }
                }
                else if (constraintRatio == 0.7142857142857142857142857143M)
                {
                    constraintRatio = 0.7142157142857142857142857143M;
                    double rectweight = (double)constraintRatio * cropImageActualWidth;
                    double rectheight = (double)constraintRatio * cropImageActualHeight;
                    rcInterior = new Rect(0, 0, rectweight, rectheight);
                    tempRcInterior = new Rect(0, 0, rectweight, rectheight);
                    while (!(rcInterior.X + rcInterior.Width > GrdCrop.ActualWidth) && !(rcInterior.Y + rcInterior.Height > GrdCrop.ActualHeight))
                    {
                        if ((tempRcInterior.X + tempRcInterior.Width > GrdCrop.ActualWidth) || (tempRcInterior.Y + tempRcInterior.Height > GrdCrop.ActualHeight))
                            break;
                        else
                            rcInterior.Scale(1.01, 1.01);
                    }
                }
                else if (constraintRatio == 0.75M)
                {
                    double rectheight = cropImageActualHeight / 2;
                    double rectweight = ((double)6 / 8) * rectheight;

                    rcInterior = new Rect(0, 0, rectweight, rectheight);
                    tempRcInterior = new Rect(0, 0, rectweight, rectheight);
                    while (!(rcInterior.X + rcInterior.Width > GrdCrop.ActualWidth) && !(rcInterior.Y + rcInterior.Height > GrdCrop.ActualHeight))
                    {
                        tempRcInterior.Scale(1.01, 1.01);
                        if ((tempRcInterior.X + tempRcInterior.Width > GrdCrop.ActualWidth) || (tempRcInterior.Y + tempRcInterior.Height > GrdCrop.ActualHeight))
                            break;
                        else
                            rcInterior.Scale(1.01, 1.01);
                    }
                }
                else if (constraintRatio == 0.66M)
                {
                    double rectheight = cropImageActualHeight / 2;
                    double rectweight = ((double)2 / 3) * rectheight;

                    rcInterior = new Rect(0, 0, rectweight, rectheight);
                    tempRcInterior = new Rect(0, 0, rectweight, rectheight);
                    while (!(rcInterior.X + rcInterior.Width > GrdCrop.ActualWidth) && !(rcInterior.Y + rcInterior.Height > GrdCrop.ActualHeight))
                    {
                        tempRcInterior.Scale(1.01, 1.01);
                        if ((tempRcInterior.X + tempRcInterior.Width > GrdCrop.ActualWidth) || (tempRcInterior.Y + tempRcInterior.Height > GrdCrop.ActualHeight))
                            break;
                        else
                            rcInterior.Scale(1.01, 1.01);
                    }
                }
                else if (constraintRatio == 1.5M)
                {
                    double rectheight = cropImageActualHeight / 2;
                    double rectweight = ((double)6 / 4) * rectheight;

                    rcInterior = new Rect(0, 0, rectweight, rectheight);
                    tempRcInterior = new Rect(0, 0, rectweight, rectheight);
                    while (!(rcInterior.X + rcInterior.Width > GrdCrop.ActualWidth) && !(rcInterior.Y + rcInterior.Height > GrdCrop.ActualHeight))
                    {
                        tempRcInterior.Scale(1.01, 1.01);
                        if ((tempRcInterior.X + tempRcInterior.Width > GrdCrop.ActualWidth) || (tempRcInterior.Y + tempRcInterior.Height > GrdCrop.ActualHeight))
                            break;
                        else
                            rcInterior.Scale(1.01, 1.01);
                    }
                }
                else if (constraintRatio == 1.25M)
                {
                    double rectheight = cropImageActualHeight / 2;
                    double rectweight = ((double)10 / 8) * rectheight;

                    rcInterior = new Rect(0, 0, rectweight, rectheight);
                    tempRcInterior = new Rect(0, 0, rectweight, rectheight);
                    while (!(rcInterior.X + rcInterior.Width > GrdCrop.ActualWidth) && !(rcInterior.Y + rcInterior.Height > GrdCrop.ActualHeight))
                    {
                        tempRcInterior.Scale(1.01, 1.01);
                        if ((tempRcInterior.X + tempRcInterior.Width > GrdCrop.ActualWidth) || (tempRcInterior.Y + tempRcInterior.Height > GrdCrop.ActualHeight))
                            break;
                        else
                            rcInterior.Scale(1.01, 1.01);
                    }
                }
                else if (constraintRatio == 1.33M)
                {
                    double rectheight = cropImageActualHeight / 2;
                    double rectweight = ((double)8 / 6) * rectheight;
                    rcInterior = new Rect(0, 0, rectweight, rectheight);
                    tempRcInterior = new Rect(0, 0, rectweight, rectheight);
                    while (!(rcInterior.X + rcInterior.Width > GrdCrop.ActualWidth) && !(rcInterior.Y + rcInterior.Height > GrdCrop.ActualHeight))
                    {
                        tempRcInterior.Scale(1.01, 1.01);
                        if ((tempRcInterior.X + tempRcInterior.Width > GrdCrop.ActualWidth) || (tempRcInterior.Y + tempRcInterior.Height > GrdCrop.ActualHeight))
                            break;
                        else
                            rcInterior.Scale(1.01, 1.01);
                    }
                }
                else if (constraintRatio == 1M)
                {
                    double rectheight = cropImageActualHeight / 2;
                    double rectweight = ((double)3 / 3) * rectheight;
                    rcInterior = new Rect(0, 0, rectweight, rectheight);
                    tempRcInterior = new Rect(0, 0, rectweight, rectheight);
                    while (!(rcInterior.X + rcInterior.Width > GrdCrop.ActualWidth) && !(rcInterior.Y + rcInterior.Height > GrdCrop.ActualHeight))
                    {
                        tempRcInterior.Scale(1.01, 1.01);
                        if ((tempRcInterior.X + tempRcInterior.Width > GrdCrop.ActualWidth) || (tempRcInterior.Y + tempRcInterior.Height > GrdCrop.ActualHeight))
                            break;
                        else
                            rcInterior.Scale(1.01, 1.01);
                    }
                }
                 // Arvind Start - 5 * 7 - 23/02/2016
                else if (constraintRatio == 0.71M)
                {
                    double rectheight = cropImageActualHeight / 2;
                    double rectweight = ((double)5 / 7) * rectheight;

                    rcInterior = new Rect(0, 0, rectweight, rectheight);
                    tempRcInterior = new Rect(0, 0, rectweight, rectheight);
                    while (!(rcInterior.X + rcInterior.Width > GrdCrop.ActualWidth) && !(rcInterior.Y + rcInterior.Height > GrdCrop.ActualHeight))
                    {
                        tempRcInterior.Scale(1.01, 1.01);
                        if ((tempRcInterior.X + tempRcInterior.Width > GrdCrop.ActualWidth) || (tempRcInterior.Y + tempRcInterior.Height > GrdCrop.ActualHeight))
                            break;
                        else
                            rcInterior.Scale(1.01, 1.01);
                    }
                }
                else if (constraintRatio == 1.4M)
                {
                    double rectheight = cropImageActualHeight / 2;
                    double rectweight = ((double)7 / 5) * rectheight;

                    rcInterior = new Rect(0, 0, rectweight, rectheight);
                    tempRcInterior = new Rect(0, 0, rectweight, rectheight);
                    while (!(rcInterior.X + rcInterior.Width > GrdCrop.ActualWidth) && !(rcInterior.Y + rcInterior.Height > GrdCrop.ActualHeight))
                    {
                        tempRcInterior.Scale(1.01, 1.01);
                        if ((tempRcInterior.X + tempRcInterior.Width > GrdCrop.ActualWidth) || (tempRcInterior.Y + tempRcInterior.Height > GrdCrop.ActualHeight))
                            break;
                        else
                            rcInterior.Scale(1.01, 1.01);
                    }
                } // Arvind End - 5 * 7 - 23/02/2016
                else
                {
                    rcInterior = new Rect(0, 0, GrdCrop.ActualWidth, GrdCrop.ActualHeight);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                rcInterior = new Rect(0, 0, GrdCrop.ActualWidth, GrdCrop.ActualHeight);
            }
            finally
            {
                MemoryManagement.FlushMemory();
            }

            return rcInterior;
        }
        private void SetClipColorGrey()
        {
            try
            {
                if (_clp != null)
                {
                    System.Windows.Media.Color clr = Colors.Black;
                    clr.A = 110;
                    _clp.Fill = new SolidColorBrush(clr);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }
        public void Zomout(bool orignal)
        {
            try
            {
                double currentwidth = widthimg.Source.Width;
                double currentheight = widthimg.Source.Height;

                double ratiowidth = currentwidth / 600;
                double ratioheight = currentheight / 600;
                ratiowidth = 100 / ratiowidth / 100;
                ratioheight = 100 / ratioheight / 100;

                ScaleTransform zoomTransform = new ScaleTransform();
                TransformGroup transformGroup = new TransformGroup();
                if (currentheight > currentwidth)
                {
                    zoomTransform.ScaleX = ratioheight - .01;
                    zoomTransform.ScaleY = ratioheight - .01;
                }
                else //if (currentheight < currentwidth)
                {
                    zoomTransform.ScaleX = ratiowidth - .01;
                    zoomTransform.ScaleY = ratiowidth - .01;
                }

                zoomTransform.CenterX = forWdht.ActualWidth / 2;
                zoomTransform.CenterY = forWdht.ActualHeight / 2;
                transformGroup.Children.Add(zoomTransform);
                GrdZomout.LayoutTransform = transformGroup;
                grdZoomCanvas.LayoutTransform = transformGroup;

            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
            finally
            {
                MemoryManagement.FlushMemory();
            }
        }

        #endregion

        #region Events
        private void BrowseButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                System.Windows.Forms.OpenFileDialog ofDialog = new System.Windows.Forms.OpenFileDialog();
                var result = ofDialog.ShowDialog();
                if (result == System.Windows.Forms.DialogResult.OK)
                {
                    cleanResource();
                    txtImgPath.Text = ofDialog.FileName;

                    using (FileStream fileStream = File.OpenRead(txtImgPath.Text))
                    {

                        BitmapImage bi = new BitmapImage();

                        MemoryStream ms = new MemoryStream();
                        fileStream.CopyTo(ms);
                        ms.Seek(0, SeekOrigin.Begin);
                        fileStream.Close();
                        bi.BeginInit();
                        bi.StreamSource = ms;
                        bi.EndInit();
                        mainImage.Source = bi;
                        if(cmbImgOrientation.SelectedIndex == 0 && mainImage.Source.Height > mainImage.Source.Width)
                        {
                            MessageBox.Show("Please Select Horizontal/Landscape Image");
                            mainImageundo.Source = mainImage.Source = null;
                            cmbImgOrientation.IsEnabled = false;
                            txtImgPath.Text = string.Empty;
                            return;
                        }
                        else if(cmbImgOrientation.SelectedIndex == 1 && mainImage.Source.Height < mainImage.Source.Width)
                        {
                            MessageBox.Show("Please Select Vertical/Portrait Image");
                            mainImageundo.Source = mainImage.Source = null;
                            cmbImgOrientation.IsEnabled = false;
                            txtImgPath.Text = string.Empty;
                            return;
                        }
                        else
                        {
                            mainImageundo.Source = bi;
                            widthimg.Source = bi;
                            CroppingAdorner.s_dpiX = bi.DpiX;
                            CroppingAdorner.s_dpiY = bi.DpiY;
                            imgRotateCrop.Source = bi;
                        }
                    }
                    mainImage.UpdateLayout();

                    forWdht.Height = widthimg.Source.Height;
                    forWdht.Width = widthimg.Source.Width;
                    Zomout(true);
                    btnselect8By10.IsEnabled = true;
                    btnselect6By8.IsEnabled = true;
                    btnSelect4by6.IsEnabled = true;
                    btnselect3By3.IsEnabled = true;
                    btnselect5By7.IsEnabled = true;
                    btnenlargeminus.IsEnabled = true;
                    btnenlargeplus.IsEnabled = true;
                    OkButton.IsEnabled = true;
                    cmbImgOrientation.IsEnabled = false;
                    AddCropToElementDefault(GrdCrop, mainImage);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }
        private void ResetButton_Click(object sender, RoutedEventArgs e)
        {
            if (cmbImgOrientation.SelectedIndex == 0)
                Configuration.HorizontalCropCoordinates = "";
            else if (cmbImgOrientation.SelectedIndex == 1)
                Configuration.VerticalCropCoordinates = "";
            cleanResource();
        }

        private void cleanResource()
        {
            Onload();

            dragCanvas.IsEnabled = true;

            GrdsubCrop.Visibility = Visibility.Visible;
            GrdFlip.LayoutTransform = new TransformGroup();
            GrdRotate.LayoutTransform = new TransformGroup();
            grdZoomCanvas.Visibility = System.Windows.Visibility.Collapsed;
            GrdRotateCropParent.Visibility = System.Windows.Visibility.Visible;

            DragCanvas.SetCanBeDragged(Opacitymsk, false);
            DragCanvas.SetCanBeDragged(mainImage, false);

            MyInkCanvas.SnapsToDevicePixels = true;
            MyInkCanvas.SetValue(RenderOptions.EdgeModeProperty, EdgeMode.Aliased);

            mainImage.SnapsToDevicePixels = true;
            mainImage.SetValue(RenderOptions.EdgeModeProperty, EdgeMode.Aliased);
            mainImage.OverridesDefaultStyle = true;

            Canvas.SetZIndex(imageundoGrid, 0);
            Canvas.SetZIndex(canbackgroundParent, 4);
            Canvas.SetZIndex(Opacitymsk, 2);

            canbackgroundParent.IsHitTestVisible = false;
            canbackground.IsHitTestVisible = false;
            RemoveCropFromCur();
            btnselect8By10.IsEnabled = false;
            btnselect6By8.IsEnabled = false;
            btnSelect4by6.IsEnabled = false;
            btnselect3By3.IsEnabled = false;
            btnselect5By7.IsEnabled = false;
            btnSelectReverse.IsEnabled = false;
            btnenlargeminus.IsEnabled = false;
            btnenlargeplus.IsEnabled = false;
        }
        private void OkButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                btnSelectReverse.IsEnabled = false;

                Crop();
                btnrotate.IsEnabled = true;
                btnrotate.IsEnabled = true;
                btnflip.IsEnabled = true;

                if (_rotateangle > 0)
                {
                    VisualStateManager.GoToState(btnrotate, "Checked", true);
                    VisualStateManager.GoToState(btnantirotate, "Checked", true);
                }
                else
                {
                    VisualStateManager.GoToState(btnrotate, "Unchecked", true);
                    VisualStateManager.GoToState(btnantirotate, "Unchecked", true);
                }
                btnantirotate.IsEnabled = true;
                imgRotateCrop.UpdateLayout();
                GrdZomout.InvalidateArrange();

                forWdht.Height = widthimg.Source.Height;
                forWdht.Width = widthimg.Source.Width;

                grdZoomCanvas.Visibility = System.Windows.Visibility.Visible;
                GrdRotateCropParent.Visibility = System.Windows.Visibility.Collapsed;

                Zomout(true);
                btnantirotate.IsEnabled = false;
                btnrotate.IsEnabled = false;
                btnenlargeplus.IsEnabled = false;
                btnenlargeminus.IsEnabled = false;
                btnflip.IsEnabled = false;
                OkButton.IsEnabled = false;
                GrdRotate.LayoutTransform = new TransformGroup();
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
            finally
            {
                MemoryManagement.FlushMemory();
            }
        }
        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            InitializeComponent();
            Onload();

            dragCanvas.IsEnabled = true;

            GrdsubCrop.Visibility = Visibility.Visible;
            GrdFlip.LayoutTransform = new TransformGroup();
            GrdRotate.LayoutTransform = new TransformGroup();
            grdZoomCanvas.Visibility = System.Windows.Visibility.Collapsed;
            GrdRotateCropParent.Visibility = System.Windows.Visibility.Visible;

            DragCanvas.SetCanBeDragged(Opacitymsk, false);
            DragCanvas.SetCanBeDragged(mainImage, false);

            MyInkCanvas.SnapsToDevicePixels = true;
            MyInkCanvas.SetValue(RenderOptions.EdgeModeProperty, EdgeMode.Aliased);

            mainImage.SnapsToDevicePixels = true;
            mainImage.SetValue(RenderOptions.EdgeModeProperty, EdgeMode.Aliased);
            mainImage.OverridesDefaultStyle = true;

            Canvas.SetZIndex(imageundoGrid, 0);
            Canvas.SetZIndex(canbackgroundParent, 4);
            Canvas.SetZIndex(Opacitymsk, 2);

            canbackgroundParent.IsHitTestVisible = false;
            canbackground.IsHitTestVisible = false;
            RemoveCropFromCur();
            _felCur = null;
            _clp = null;
            this.Visibility = Visibility.Collapsed;
            _parent.IsEnabled = true;
            _parent.Opacity = 1;
        }
        private void GrdBrightness_LayoutUpdated(object sender, EventArgs e)
        {
            if (MyInkCanvas.EditingMode == InkCanvasEditingMode.Ink)
                return;
            double canvasTop1 = (double)Opacitymsk.GetValue(Canvas.TopProperty);
            double canvasLeft1 = (double)Opacitymsk.GetValue(Canvas.LeftProperty);
            if (!_canvasTop.Equals(canvasTop1) || !_canvasLeft.Equals(canvasLeft1))
            {
                _canvasTop = canvasTop1;
                _canvasLeft = canvasLeft1;
                canbackgroundParent.SetValue(Canvas.TopProperty, _canvasTop);
                canbackgroundParent.SetValue(Canvas.LeftProperty, _canvasLeft);
            }
        }

        public void Onload()
        {
            canbackgroundParent.Visibility = System.Windows.Visibility.Collapsed;
            Canvas.SetZIndex(imageundoGrid, 0);
            Canvas.SetZIndex(canbackgroundParent, 4);
            Canvas.SetZIndex(Opacitymsk, 2);
            _flipMode = 0;
            _flipModeY = 0;
            MyInkCanvas.SnapsToDevicePixels = true;
            MyInkCanvas.SetValue(RenderOptions.EdgeModeProperty, EdgeMode.Aliased);
            mainImage.SnapsToDevicePixels = true;
            RenderOptions.SetEdgeMode(mainImage, EdgeMode.Aliased);

            forWdht.RenderTransform = new RotateTransform();
            try
            {
                mainImageundo.Source = mainImage.Source = null;
                widthimg.Source = null;
                imgRotateCrop.Source = null;
                txtImgPath.Text = "";
                mainImage.UpdateLayout();
                MyInkCanvas.EditingMode = InkCanvasEditingMode.None;
                canbackground.RenderTransform = MyInkCanvas.RenderTransform = new MatrixTransform();
                canbackground.RenderTransform = GrdBrightness.RenderTransform = null;
                canbackgroundParent.Margin = Opacitymsk.Margin = new Thickness(0, 0, 0, 0);
                dragCanvas.AllowDragOutOfView = true;
                forWdht.HorizontalAlignment = System.Windows.HorizontalAlignment.Center;
                forWdht.VerticalAlignment = System.Windows.VerticalAlignment.Center;
                dragCanvas.AllowDragging = false;
                dragCanvas.IsEnabled = false;
                MyInkCanvasParent.Effect = null;

                if (MyInkCanvas.Children.Count > 1)
                    MyInkCanvas.Children.RemoveRange(1, MyInkCanvas.Children.Count);
                if (MyInkCanvas.Strokes.Count > 0)
                    MyInkCanvas.Strokes.Clear();

                btnselect8By10.IsEnabled = false;
                btnselect6By8.IsEnabled = false;
                btnSelect4by6.IsEnabled = false;
                btnselect3By3.IsEnabled = false;
                btnselect5By7.IsEnabled = false;
                btnSelectReverse.IsEnabled = false;
                btnenlargeminus.IsEnabled = false;
                btnenlargeplus.IsEnabled = false;
                OkButton.IsEnabled = false;
                cmbImgOrientation.IsEnabled = true;
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
            finally
            {
                MemoryManagement.FlushMemory();
            }
        }

        private void Crop()
        {
            try
            {
                if (_clp == null)
                    return;
                Rect rc = _clp.ClippingRectangle;
                BitmapSource bitmapsource = _clp.BpsCrop();
                rc.X = Math.Round(rc.X, 0);
                rc.Y = Math.Round(rc.Y, 0);
                rc.Width = Math.Round(rc.Width, 0);
                rc.Height = Math.Round(rc.Height, 0);
                if (cmbImgOrientation.SelectedIndex == 0)
                    Configuration.HorizontalCropCoordinates = rc.ToString();
                else if (cmbImgOrientation.SelectedIndex == 1)
                    Configuration.VerticalCropCoordinates = rc.ToString();
                widthimg.Source = bitmapsource;
                mainImageundo.Source = mainImage.Source = bitmapsource;
                imgRotateCrop.Source = bitmapsource;
                imgRotateCrop.UpdateLayout();
                string outputFileName = string.Empty;
                if (bitmapsource.Height < bitmapsource.Width)
                    outputFileName = "HorizontalSpecPrintCrop" + Configuration.SelectedLocationId.ToString() + DateTime.Now.ToString("yyyyMMddhhmmssss") + ".jpg";
                else
                    outputFileName = "VerticalSpecPrintCrop" + Configuration.SelectedLocationId.ToString() + DateTime.Now.ToString("yyyyMMddhhmmssss") + ".jpg";
                using (var fileStream = new FileStream(System.IO.Path.Combine(LoginUser.DigiFolderCropedPath, outputFileName), FileMode.Create, FileAccess.ReadWrite))
                {
                    JpegBitmapEncoder encoder = new JpegBitmapEncoder();
                    encoder.QualityLevel = 99;
                    encoder.Frames.Add(BitmapFrame.Create(bitmapsource));
                    encoder.Save(fileStream);
                }

                forWdht.HorizontalAlignment = System.Windows.HorizontalAlignment.Center;
                forWdht.VerticalAlignment = System.Windows.VerticalAlignment.Center;

                GrdBrightness.VerticalAlignment = System.Windows.VerticalAlignment.Center;
                GrdBrightness.HorizontalAlignment = System.Windows.HorizontalAlignment.Center;

                RemoveCropFromCur();
                _felCur = null;
                _clp = null;
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
            finally
            {
                //MemoryManagement.FlushMemory();
            }
        }

        #endregion

        #region extra commented functions of Flip & Rotate
        private void btnRotateclick_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                AntiRotate(-1);
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }
        private void btnantirotate_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Rotate(-1);
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }
        private void btnflip_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Flip(-1);
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }

        }
        private void AntiRotate(int angle)
        {
            if (angle != -1)
            {
                _rotateangle = angle;
            }

            GrdRotate.RenderTransformOrigin = new System.Windows.Point(0.5, 0.5);
            RotateTransform RoatateTrans = new RotateTransform();
            int previousvalue = _rotateangle;
            try
            {
                switch (_rotateangle)
                {
                    case 0:
                        {
                            RoatateTrans.Angle = 270;
                            _rotateangle = 270;
                            break;
                        }
                    case 90:
                        {
                            RoatateTrans.Angle = 0;
                            _rotateangle = 0;
                            break;
                        }
                    case 180:
                        {
                            RoatateTrans.Angle = 90;
                            _rotateangle = 90;
                            break;
                        }
                    case 270:
                        {
                            RoatateTrans.Angle = 180;
                            _rotateangle = 180;
                            break;
                        }
                }
                GrdRotate.LayoutTransform = RoatateTrans;
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
            finally
            {
                if (_rotateangle > 0)
                {
                    VisualStateManager.GoToState(btnrotate, "Checked", true);
                    VisualStateManager.GoToState(btnantirotate, "Checked", true);
                }
                else
                {
                    VisualStateManager.GoToState(btnrotate, "Unchecked", true);
                    VisualStateManager.GoToState(btnantirotate, "Unchecked", true);
                }
            }
        }
        private void Rotate(int angle)
        {
            GrdRotate.RenderTransformOrigin = new System.Windows.Point(0.5, 0.5);
            RotateTransform RoatateTrans = new RotateTransform();
            int previousvalue = _rotateangle;
            if (angle != -1)
            {
                RoatateTrans.Angle = angle;
                GrdRotate.LayoutTransform = RoatateTrans;
                _rotateangle = 360 - angle;
                if (_rotateangle == 360)
                    _rotateangle = 0;
            }
            else
            {
                try
                {
                    switch (_rotateangle)
                    {
                        case 0:
                            {
                                RoatateTrans.Angle = 90;
                                _rotateangle = 90;
                                break;
                            }
                        case 90:
                            {
                                RoatateTrans.Angle = 180;
                                _rotateangle = 180;
                                break;
                            }
                        case 180:
                            {
                                RoatateTrans.Angle = 270;
                                _rotateangle = 270;
                                break;
                            }
                        case 270:
                            {
                                RoatateTrans.Angle = 360;
                                _rotateangle = 0;
                                break;
                            }
                    }
                    GrdRotate.LayoutTransform = RoatateTrans;
                }
                catch (Exception ex)
                {
                    string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                    ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                }
                finally
                {
                    if (_rotateangle > 0)
                    {
                        VisualStateManager.GoToState(btnrotate, "Checked", true);
                        VisualStateManager.GoToState(btnantirotate, "Checked", true);
                    }
                    else
                    {
                        VisualStateManager.GoToState(btnrotate, "Unchecked", true);
                        VisualStateManager.GoToState(btnantirotate, "Unchecked", true);
                    }
                }
            }
        }
        private void Flip(int mode)
        {
            _zoomTransform = new ScaleTransform();
            _translateTransform = new TranslateTransform();
            _rotateTransform = new RotateTransform();
            if (mode != -1)
            {
                _rotateangle = mode;
            }

            int previousvalue = _flipMode;
            try
            {
                if (_flipModeY == 0)
                {
                    if (_flipMode == 0)
                    {
                        _zoomTransform.CenterX = imgRotateCrop.ActualWidth / 2;// mainImage.ActualWidth / 2;
                        _zoomTransform.CenterY = imgRotateCrop.ActualHeight / 2;//mainImage.ActualHeight / 2;
                        _zoomTransform.ScaleX = -1;
                        _flipMode = 1;
                    }
                    else
                    {
                        _zoomTransform.CenterX = imgRotateCrop.ActualWidth / 2;// mainImage.ActualWidth / 2;
                        _zoomTransform.CenterY = imgRotateCrop.ActualHeight / 2;//mainImage.ActualHeight / 2;
                        _zoomTransform.ScaleX = 1;
                        _flipMode = 0;
                    }
                }
                else
                {
                    if (_flipMode == 0)
                    {
                        _zoomTransform.ScaleY = -1;
                        _flipMode = 1;
                    }
                    else
                    {
                        _zoomTransform.ScaleY = 1;
                        _flipMode = 0;
                    }
                }
                _transformGroup = new TransformGroup();
                _transformGroup.Children.Add(_zoomTransform);
                _transformGroup.Children.Add(_translateTransform);
                _transformGroup.Children.Add(_rotateTransform);
                GrdFlip.LayoutTransform = _zoomTransform;
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }

            if (_flipMode != 0 || _flipModeY != 0)
            {
                VisualStateManager.GoToState(btnflip, "Checked", true);
            }
            else
            {
                VisualStateManager.GoToState(btnflip, "Unchecked", true);
            }
        }
        #endregion
    }
}
