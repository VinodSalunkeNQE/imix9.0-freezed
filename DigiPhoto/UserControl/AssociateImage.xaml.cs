﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using DigiPhoto.DataLayer;
using DigiPhoto.Common;
using DigiPhoto.DataLayer.Model;
using DigiPhoto.IMIX.Business;
using System.Net.Mail;
using System.Text.RegularExpressions;

namespace DigiPhoto
{
    /// <summary>
    /// Interaction logic for AssociateImage.xaml
    /// </summary>
    public partial class AssociateImage : UserControl, IDisposable
    {

        //  DigiPhotoDataServices _objservice = null;
        private UIElement _parent;
        string _CardCode = string.Empty;
        public event EventHandler ExecuteParentMethod;
        public AssociateImage()
        {
            try
            {
                InitializeComponent();
                BindCodeType();
                BindNationality();
                MsgBox.SetParent(OuterBorder);
                CtrlCodePopup.SetParent(OuterBorder);
                if (string.IsNullOrEmpty(App.QRCodeWebUrl.Trim()))
                {

                    string webURL = new LocationBusniess().GetQRCodeWebUrl();
                    App.QRCodeWebUrl = string.IsNullOrEmpty(webURL) ? " " : webURL;
                }
                cmbQRCodeType.SelectedValue = App.SelectedCodeType;
                LastGrid1.Height = new GridLength(0);
                LastGrid2.Height = new GridLength(0);
                CodeTypeSelectionChange();
            }
            catch (Exception ex)
            {

                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }
        protected virtual void OnExecuteMethod()
        {
            if (ExecuteParentMethod != null) ExecuteParentMethod(this, EventArgs.Empty);
        }
        /// <summary>
        /// changed by latika for presold changes 3May2019
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnClose_Click(object sender, RoutedEventArgs e)
        {
            ClosAssociationWindow();
        }
        /// <summary>
        /// end
        /// </summary>
        private void HideHandlerDialog()
        {
            try
            {
                this.txtQRCode.Text = string.Empty;
                this.txtQRCode.IsEnabled = true;
                //this.cmbQRCodeType.SelectedValue = 401;
                this.Visibility = Visibility.Collapsed;
                _parent.IsEnabled = true;
                OnExecuteMethod();
                KeyBorder1.Visibility = Visibility.Collapsed;
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }
        private void txtCode_GotFocus(object sender, RoutedEventArgs e)
        {
            try
            {
                int cardType = 0;
                if (cmbQRCodeType.SelectedValue != null)
                    cardType = (int)cmbQRCodeType.SelectedValue;
                if (cardType != 0 && cardType != 404)
                    KeyBorder1.Visibility = Visibility.Visible;
                else
                    KeyBorder1.Visibility = Visibility.Collapsed;
            }
            catch (Exception ex)
            {

                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }
        private void BindCodeType()
        {
            try
            {


                var CodeType = new CardBusiness().GetCardCodeTypes();
                Dictionary<string, int> dicItems = new Dictionary<string, int>();
                dicItems.Add("--Select--", 0);
                foreach (var itm in CodeType)
                {
                    //406 is rfid, visible only if it is enabled 
                    //if (itm.Value != 406 || (itm.Value == 406 && App.IsRFIDEnabled))
                    dicItems.Add(itm.Key, itm.Value);
                }


                cmbQRCodeType.ItemsSource = dicItems;
                cmbQRCodeType.SelectedValue = 405;
                //get card code of 404
                _CardCode = new CardBusiness().GetCardCode(404);
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }

        private void BindNationality()
        {
            try
            {


                var CountyType = new CardBusiness().getGlobalCountryList();
                Dictionary<string, int> dicItems = new Dictionary<string, int>();
                dicItems.Add("--Select--", 0);
                foreach (var itm in CountyType)
                {
                    dicItems.Add(itm.Key, itm.Value);
                }


                cmbQRCountryType.ItemsSource = dicItems;
                cmbQRCountryType.SelectedValue = 0;

            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }

        private bool IsValidEmail(string emailaddress)
        {
            try
            {

                Regex regex = new Regex(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$");
                Match match = regex.Match(emailaddress);

                if (match.Success)
                    return true;
                else
                    return false;


            }
            catch (FormatException)
            {
                return false;
            }
        }

        private void btnAssociate_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                setQRCode();////created by latika to handle url link of QRCode
                if (cmbQRCodeType.SelectedIndex <= 0)
                {
                    MessageBox.Show("Please select card type.");
                    return;
                }


                int codeTypeId = (int)cmbQRCodeType.SelectedValue;

                if (codeTypeId == 407)
                {
                    if (txtQRCode.Text.Trim().Length > 0)
                    {
                        if (!IsValidEmail(txtQRCode.Text.Trim()))
                        {
                            MessageBox.Show("Enter valid Email ID.");
                            return;
                        }
                      
                    }
                    if (cmbQRCountryType.SelectedIndex <= 0)
                    {
                        MessageBox.Show("Please select Nationality.");
                        return;
                    }
                }

                RobotImageLoader.IsAnonymousQrCodeEnabled = App.IsAnonymousQrCodeEnabled == true && (codeTypeId == 405 || codeTypeId == 407) ? true : false;
                string UniqueCode = txtQRCode.Text.Trim();
                string emailid = txtEailID.Text.Trim();
                
                bool IsValid = false;
                int OverWritesStatus = 0;
                if (codeTypeId > 0)
                {
                    if (string.IsNullOrEmpty(txtQRCode.Text.Trim()))
                    {
                        MessageBox.Show("Enter " + cmbQRCodeType.Text + ((codeTypeId != 405 && codeTypeId != 407) ? " Code." : "."));
                        return;
                    }
                    if (txtQRCode.Text.Trim().Length < 5 && codeTypeId != 404)
                    {
                        MessageBox.Show("Code length must be more than 4 digit.");
                        return;
                    }
                    if (RobotImageLoader.GroupImages.Count < 1)
                    {
                        MessageBox.Show("No Image is selected to associate.");
                        return;
                    }
                    //   _objservice = new DigiPhotoDataServices();
                    string PhotoIds = string.Empty;
                    RobotImageLoader.GroupImages.ForEach(o => PhotoIds += "," + o.PhotoId);
                    if (PhotoIds.Length > 0)
                        PhotoIds = PhotoIds.Substring(1, PhotoIds.Length - 1);
                    KeyBorder1.Visibility = Visibility.Collapsed;

                    if (codeTypeId == 403)
                        UniqueCode = "0000" + UniqueCode;
                    if (codeTypeId == 406)
                        UniqueCode = "2222" + UniqueCode;
                    if (App.IsAnonymousQrCodeEnabled == false && !new CardBusiness().IsValidCodeType(UniqueCode, codeTypeId))
                    {
                        MessageBox.Show("Invalid code.");
                        if (RobotImageLoader.CodeType != 404)
                            txtQRCode.Text = string.Empty;
                        return;
                    }
                    //if unique code exists and operator chose yes then associate with same code
                    if (codeTypeId != 404 && new AssociateImageBusiness().IsUniqueCodeExists(UniqueCode, RobotImageLoader.IsAnonymousQrCodeEnabled))
                    {
                        string codeTxt = cmbQRCodeType.Text + ((codeTypeId != 405 && codeTypeId != 407) ? " Code" : "");
                        string msg = "This " + codeTxt + " already exists. Do you want to associate with the same  " + codeTxt + "?";
                        var res = CtrlCodePopup.ShowHandlerDialog(msg);
                        if (res)
                        {
                            if (CtrlCodePopup.IsToOverwrite)
                                OverWritesStatus = 1;
                            IsValid = true;
                        }
                        else
                        {
                            IsValid = false;
                        }
                    }
                    else
                    {
                        IsValid = true;
                    }
                    if (IsValid)
                    {
                        ////KCB for email id and nationality field in RFID TAG
                        if (codeTypeId == 407)
                        {
                           //Start Author Bhavin Udani. Created on 21 December 2020. Email Id Association Bug in  Presold 

                            emailid = UniqueCode;
                            emailid = CryptorEngine.Encrypt(emailid, true);
                           //End Author Bhavin Udani. Created on 21 December 2020. Email Id Association Bug in  Presold 
                        }

                        string result = new AssociateImageBusiness().AssociateImage(codeTypeId, UniqueCode, PhotoIds, OverWritesStatus, RobotImageLoader.IsAnonymousQrCodeEnabled, (int)cmbQRCountryType.SelectedValue,emailid);
                        if (result == "1")
                        {
                            string msg = "Selected images associated with the " + cmbQRCodeType.Text;
                            MsgBox.ShowHandlerDialog(msg, DigiMessageBox.DialogType.OK);
                            ViewInGroup(UniqueCode, codeTypeId);
                        }
                        else
                            MessageBox.Show(result);
                    }
                    KeyBorder1.Visibility = Visibility.Visible;
                }
                else
                {
                    MessageBox.Show("Select card type.");
                }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }

        }
        private void ViewInGroup(string Code, int CodeType)
        {
            try
            {
                RobotImageLoader.SearchCriteria = "QRCODEGROUP";
                RobotImageLoader.Code = Code;
                RobotImageLoader.CodeType = CodeType;
                SearchResult _objSearchResult = new SearchResult();
                _objSearchResult.pagename = "MainGroup";
                _objSearchResult.Show();
                ////////Calling logic of searching 
                _objSearchResult.LoadWindow();
                HideHandlerDialog();
                //close the containing current window

                Window parentWindow = Window.GetWindow(this);
                ((SearchResult)parentWindow).grdCotrol.Children.Remove(this);
                parentWindow.Close();
                Dispose();

            }
            catch (Exception ex)
            {

                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }
        public void SetParent(UIElement parent)
        {
            _parent = (UIElement)parent;

        }
        private void btn_Click_keyboard(object sender, RoutedEventArgs e)
        {
            try
            {
                Button _objbtn = new Button();
                _objbtn = (Button)sender;
                switch (_objbtn.Content.ToString())
                {
                    case "ENTER":
                        {
                            KeyBorder1.Visibility = Visibility.Collapsed;
                            break;
                        }
                    case "SPACE":
                        {

                            //txtgroupname.Text = txtgroupname.Text + " ";
                            txtQRCode.Text = txtQRCode.Text + " ";


                            break;
                        }
                    case "CLOSE":
                        {
                            KeyBorder1.Visibility = Visibility.Collapsed;
                            break;
                        }
                    case "Back":
                        {

                            //txtgroupname.Text = txtgroupname.Text.Remove(txtgroupname.Text.Length - 1, 1);
                            if (txtQRCode.Text.Length > 0)
                                txtQRCode.Text = txtQRCode.Text.Remove(txtQRCode.Text.Length - 1, 1);


                            break;
                        }
                    default:
                        {

                            //txtgroupname.Text = txtgroupname.Text + _objbtn.Content;
                            txtQRCode.Text = txtQRCode.Text + _objbtn.Content;

                        }
                        break;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }
        private void btnSearch_Click(object sender, RoutedEventArgs e)
        {
            setQRCode();///made changes by latika for URL of Qrcode functionality 1-apr-19
            try
            {
                if (cmbQRCodeType.SelectedIndex <= 0)
                {
                    MessageBox.Show("Please select card type.");
                    return;
                }
                RobotImageLoader.CodeType = (int)cmbQRCodeType.SelectedValue;
                RobotImageLoader.IsAnonymousQrCodeEnabled = App.IsAnonymousQrCodeEnabled == true && (RobotImageLoader.CodeType == 405 || RobotImageLoader.CodeType == 407) ? true : false;
                if (!string.IsNullOrEmpty(txtQRCode.Text.Trim()))
                {
                    RobotImageLoader.SearchCriteria = "QRCODEGROUP";
                    RobotImageLoader.Code = txtQRCode.Text;
                    if (RobotImageLoader.CodeType == 403)
                        RobotImageLoader.Code = "0000" + RobotImageLoader.Code;
                    if (RobotImageLoader.CodeType == 406)
                        RobotImageLoader.Code = "2222" + RobotImageLoader.Code;
                    if (App.IsAnonymousQrCodeEnabled == false && !new CardBusiness().IsValidCodeType(RobotImageLoader.Code, RobotImageLoader.CodeType))
                    {
                        MessageBox.Show("Invalid code.");
                        if (RobotImageLoader.CodeType != 404)
                        {
                            txtQRCode.Text = string.Empty;
                            txtQRCode.Focus();
                        }
                        return;
                    }

                    if (IsImageExists(RobotImageLoader.Code))
                    {
                        SearchResult _objSearchResult = null;
                        //MainWindow _mainWindow=null;
                        //foreach (Window wnd in Application.Current.Windows)
                        //{
                        //    if (wnd.Title == "View/Order Station")
                        //    {
                        //        _objSearchResult = (SearchResult)wnd;
                        //        break;
                        //    }
                        //}

                        if (_objSearchResult == null)
                            _objSearchResult = new SearchResult();

                        //_objSearchResult.pagename = "";
                        //_objSearchResult.Show();
                        //_objSearchResult.LoadWindow();
                        ////close the containing current window
                        //Window parentWindow = Window.GetWindow(this);
                        //((SearchResult)parentWindow).grdCotrol.Children.Remove(this);
                        //_parent.IsEnabled = true;
                        //Dispose();

                        _objSearchResult.pagename = "MainGroup";
                        _objSearchResult.Show();
                        ////////Calling logic of searching 
                        _objSearchResult.LoadWindow();
                        HideHandlerDialog();
                        //close the containing current window

                        Window parentWindow = Window.GetWindow(this);
                        ((SearchResult)parentWindow).grdCotrol.Children.Remove(this);
                        parentWindow.Close();
                        Dispose();

                    }
                    else
                    {

                        MessageBox.Show("No image is associated with this code." + txtQRCode.Text);
                        if (RobotImageLoader.CodeType != 404)
                        {
                            txtQRCode.Text = string.Empty;
                            txtQRCode.Focus();
                        }
                    }
                }
                else
                    MessageBox.Show("Enter " + cmbQRCodeType.Text + (RobotImageLoader.CodeType != 405 ? " Code." : "."));
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }
        private bool IsImageExists(string Code)
        {
            try
            {
                //  _objservice = new DigiPhotoDataServices();
                if (new PhotoBusiness().GetImagesBYQRCode(Code, RobotImageLoader.IsAnonymousQrCodeEnabled).Count > 0)
                    return true;
                else
                    return false;
            }
            catch (Exception ex)
            {

                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                return false;
            }
        }
        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {

        }
        private void txtQRCode_KeyDown(object sender, KeyEventArgs e)
        {

            if (e.Key == Key.Enter)
            {
                txtQRCode.Text = txtQRCode.Text.Replace(App.QRCodeWebUrl, string.Empty);
                setQRCode();
                if (txtQRCode.Text.Trim().Length > 20)
                {
                    txtQRCode.Text = txtQRCode.Text.Trim().Remove(0, 20);
                    txtQRCode.SelectionStart = txtQRCode.Text.Length;
                }
                btnSearch_Click(sender, e);
            }
        }
        private void UserControl_GotFocus(object sender, RoutedEventArgs e)
        {
            // txtQRCode.Focus();
        }
        private void UserControl_IsVisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            // txtQRCode.Focus();
        }
        private void cmbQRCodeType_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                CodeTypeSelectionChange();
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }
        private void ClearResources()
        {
            cmbQRCodeType.SelectionChanged -= new SelectionChangedEventHandler(cmbQRCodeType_SelectionChanged);
            this.IsVisibleChanged -= new DependencyPropertyChangedEventHandler(UserControl_IsVisibleChanged);
            this.GotFocus -= new RoutedEventHandler(UserControl_GotFocus);
            this.KeyDown -= new KeyEventHandler(txtQRCode_KeyDown);
            this.Loaded -= new RoutedEventHandler(UserControl_Loaded);
            btnSearch.Click -= new RoutedEventHandler(btnSearch_Click);
            btnClose.Click -= new RoutedEventHandler(btnClose_Click); //btn_Click_keyboard
            btnAssociate.Click -= new RoutedEventHandler(btnAssociate_Click);
            txtQRCode.GotFocus -= new RoutedEventHandler(txtCode_GotFocus);
            btnW.Click -= new RoutedEventHandler(btn_Click_keyboard);
            btnE.Click -= new RoutedEventHandler(btn_Click_keyboard);
            btnT.Click -= new RoutedEventHandler(btn_Click_keyboard);
            btnR.Click -= new RoutedEventHandler(btn_Click_keyboard);
            btnY.Click -= new RoutedEventHandler(btn_Click_keyboard);
            btnU.Click -= new RoutedEventHandler(btn_Click_keyboard);
            btnI.Click -= new RoutedEventHandler(btn_Click_keyboard);
            btnO.Click -= new RoutedEventHandler(btn_Click_keyboard);
            btnP.Click -= new RoutedEventHandler(btn_Click_keyboard);
            Delete.Click -= new RoutedEventHandler(btn_Click_keyboard);
            btnA.Click -= new RoutedEventHandler(btn_Click_keyboard);
            btnS.Click -= new RoutedEventHandler(btn_Click_keyboard);
            btnD.Click -= new RoutedEventHandler(btn_Click_keyboard);
            btnF.Click -= new RoutedEventHandler(btn_Click_keyboard);
            btnG.Click -= new RoutedEventHandler(btn_Click_keyboard);
            btnH.Click -= new RoutedEventHandler(btn_Click_keyboard);
            btnJ.Click -= new RoutedEventHandler(btn_Click_keyboard);
            btnK.Click -= new RoutedEventHandler(btn_Click_keyboard);
            btnL.Click -= new RoutedEventHandler(btn_Click_keyboard);
            btnEnter.Click -= new RoutedEventHandler(btn_Click_keyboard);
            btnDoller.Click -= new RoutedEventHandler(btn_Click_keyboard);
            btnZ.Click -= new RoutedEventHandler(btn_Click_keyboard);
            btnX.Click -= new RoutedEventHandler(btn_Click_keyboard);
            btnC.Click -= new RoutedEventHandler(btn_Click_keyboard);
            btnV.Click -= new RoutedEventHandler(btn_Click_keyboard);
            btnB.Click -= new RoutedEventHandler(btn_Click_keyboard);
            btnN.Click -= new RoutedEventHandler(btn_Click_keyboard);
            btnM.Click -= new RoutedEventHandler(btn_Click_keyboard);
            btnHash.Click -= new RoutedEventHandler(btn_Click_keyboard);
            btnAtRate.Click -= new RoutedEventHandler(btn_Click_keyboard);
            btnAstrick.Click -= new RoutedEventHandler(btn_Click_keyboard);
            btnUnderscore.Click -= new RoutedEventHandler(btn_Click_keyboard);
            btnSpace.Click -= new RoutedEventHandler(btn_Click_keyboard);
            btnCloseKey.Click -= new RoutedEventHandler(btn_Click_keyboard);
            btnSlash.Click -= new RoutedEventHandler(btn_Click_keyboard);
            btn4.Click -= new RoutedEventHandler(btn_Click_keyboard);
            btn5.Click -= new RoutedEventHandler(btn_Click_keyboard);
            btn6.Click -= new RoutedEventHandler(btn_Click_keyboard);
            btn7.Click -= new RoutedEventHandler(btn_Click_keyboard);
            btn8.Click -= new RoutedEventHandler(btn_Click_keyboard);
            btn9.Click -= new RoutedEventHandler(btn_Click_keyboard);
            btn1.Click -= new RoutedEventHandler(btn_Click_keyboard);
            btn2.Click -= new RoutedEventHandler(btn_Click_keyboard);
            btn3.Click -= new RoutedEventHandler(btn_Click_keyboard);
            btnDash.Click -= new RoutedEventHandler(btn_Click_keyboard);
            btnDot.Click -= new RoutedEventHandler(btn_Click_keyboard);
            btnPlus.Click -= new RoutedEventHandler(btn_Click_keyboard);
            btnW.Click -= new RoutedEventHandler(btn_Click_keyboard);
            btnW.Click -= new RoutedEventHandler(btn_Click_keyboard);
            OuterBorder = null;
        }
        // By KCB ON 02 JULY 2020 for RFID EMAILID AND NATIONALITY FIELD.
        private void CodeTypeSelectionChange()
        {
            if (cmbQRCodeType.SelectedValue != null)
            {
                switch (cmbQRCodeType.SelectedValue.ToString())
                {
                    case "407":
                        {
                            LastGrid1.Height = new GridLength(0);
                            LastGrid2.Height = new GridLength(0);
                            LastGrid3.Height = new GridLength(40);
                            LastGrid4.Height = new GridLength(60);
                            CodeTextBlock.Text = "Enter Email ID :";
                            break;
                        }
                    case "406":
                        {
                            LastGrid1.Height = new GridLength(40);
                            LastGrid2.Height = new GridLength(60);
                            LastGrid3.Height = new GridLength(40);
                            LastGrid4.Height = new GridLength(60);
                            CodeTextBlock.Text = "Enter Code :";
                            break;
                        }
                    case "404":
                        {
                            CodeTextBlock.Text = "Enter Code :";
                            txtQRCode.Text = _CardCode;
                            txtQRCode.IsEnabled = false;
                            KeyBorder1.Visibility = Visibility.Collapsed;
                            break;
                        }
                    default:
                        {
                            CodeTextBlock.Text = "Enter Code :";

                            txtQRCode.Text = string.Empty;
                            txtQRCode.IsEnabled = true;
                            txtQRCode.Focus();
                            KeyBorder1.Visibility = Visibility.Visible;

                            LastGrid1.Height = new GridLength(0);
                            LastGrid2.Height = new GridLength(0);
                            LastGrid3.Height = new GridLength(0);
                            LastGrid4.Height = new GridLength(0);
                            break;
                        }
                }
            }
            if (cmbQRCodeType.SelectedValue != null)
                App.SelectedCodeType = (int)cmbQRCodeType.SelectedValue;
        }
        //
        //private void CodeTypeSelectionChange()
        //{

        //    if (cmbQRCodeType.SelectedValue != null && string.Compare(cmbQRCodeType.SelectedValue.ToString(), "407", true) == 0)
        //    {
        //        LastGrid1.Height = new GridLength(40);
        //        LastGrid2.Height = new GridLength(60);
        //        CodeTextBlock.Text = "Enter Email ID :";
        //    }
        //    else
        //    {
        //        LastGrid1.Height = new GridLength(0);
        //        LastGrid2.Height = new GridLength(0);
        //        CodeTextBlock.Text = "Enter Code :";
        //    }


        //    if (cmbQRCodeType.SelectedValue != null && string.Compare(cmbQRCodeType.SelectedValue.ToString(), "404", true) == 0)
        //    {
        //        txtQRCode.Text = _CardCode;
        //        txtQRCode.IsEnabled = false;
        //        KeyBorder1.Visibility = Visibility.Collapsed;
        //    }
        //    else
        //    {
        //        txtQRCode.Text = string.Empty;
        //        txtQRCode.IsEnabled = true;
        //        txtQRCode.Focus();
        //        KeyBorder1.Visibility = Visibility.Visible;

        //    }
        //    if (cmbQRCodeType.SelectedValue != null)
        //        App.SelectedCodeType = (int)cmbQRCodeType.SelectedValue;
        //}
        //End
        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }
        /// <summary>
        /// ////////created by latika remove link for QR Code 12-Mar-19
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtQRCode_MouseLeave(object sender, MouseEventArgs e)
        {
            setQRCode();
        }
        public void setQRCode()
        {
            string[] strTag = txtQRCode.Text.Split('=');
            string tagid = strTag[strTag.Length - 1];
            txtQRCode.Text = tagid.Trim();
        }

        private void txtQRCode_LostKeyboardFocus(object sender, KeyboardFocusChangedEventArgs e)
        {
            setQRCode();
        }

        private void txtQRCode_TextChanged(object sender, TextChangedEventArgs e)
        {
            setQRCode();
        }
        /// <summary>
        /// //end
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        /// <summary>
        /// //added by latika for presold changes 3May2019
        /// </summary>
        private void btnRemoveAssociation_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                setQRCode();
                int codeTypeId = (int)cmbQRCodeType.SelectedValue;
                string UniqueCode = txtQRCode.Text;
                bool IsValid = true;
                string PhotoIds = string.Empty;
                RobotImageLoader.GroupImages.ForEach(o => PhotoIds += "," + o.PhotoId);
                if (PhotoIds.Length > 0)
                    PhotoIds = PhotoIds.Substring(1, PhotoIds.Length - 1);
                KeyBorder1.Visibility = Visibility.Collapsed;


                //if unique code exists and operator chose yes then associate with same code
                int count = new AssociateImageBusiness().RemoveAssociateImage(UniqueCode, PhotoIds, true);
                string msg = "";
                if (count > 0)
                {
                    msg = "Do you want to remove the association of selected images and its same photoID images (Total count-" + count + ") ?";
                    if (UniqueCode.Length > 3)
                    {
                        msg = "Do you want to remove the association of selected images (Total count-" + count + ")?";
                    }

                    System.Windows.Forms.DialogResult dialogResult = System.Windows.Forms.MessageBox.Show(msg, "DigiPhoto", System.Windows.Forms.MessageBoxButtons.YesNo);
                    if (dialogResult == System.Windows.Forms.DialogResult.Yes)
                    {

                        IsValid = true;
                    }
                    else
                    {
                        IsValid = false;
                    }
                    if (IsValid)
                    {
                        int result = new AssociateImageBusiness().RemoveAssociateImage(UniqueCode, PhotoIds, false);
                        if (result == 1)
                        {
                            msg = "Selected images are removed from association.";
                            MsgBox.ShowHandlerDialog(msg, DigiMessageBox.DialogType.OK);
                            ClosAssociationWindow();
                        }
                        else
                            MessageBox.Show("Selected images are not associated with any QRcode.");
                    }


                }
                else { MessageBox.Show("Selected images are not associated with any QRcode."); }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }
        public void ClosAssociationWindow()
        {
            try
            {
                // HideHandlerDialog();
                this.Visibility = Visibility.Collapsed;
                _parent.IsEnabled = true;
                KeyBorder1.Visibility = Visibility.Collapsed;
                if (cmbQRCodeType.SelectedValue != null && string.Compare(cmbQRCodeType.SelectedValue.ToString(), "404", true) == 0)
                {
                    txtQRCode.Text = _CardCode;
                    txtQRCode.IsEnabled = false;
                }
                else
                {
                    txtQRCode.Text = string.Empty;
                    txtQRCode.IsEnabled = true;
                    txtQRCode.Focus();
                }
                OnExecuteMethod();
                ((Grid)this.Parent).Children.Remove(this);
                //((SearchResult)Window.GetWindow(this)).uctlAssociateImage = null;
                Dispose();
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
            finally
            {
                //ClearResources();
            }
        }
        /// <summary>
        /// //End by latika 
        /// </summary>

    }
}
