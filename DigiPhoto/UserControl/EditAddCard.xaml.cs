﻿using DigiAuditLogger;
using DigiPhoto.DataLayer;
using DigiPhoto.IMIX.Business;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using DigiPhoto.Utility.Repository.ValueType;
using DigiPhoto.IMIX.Model;
using FrameworkHelper;

namespace DigiPhoto
{
    /// <summary>
    /// Interaction logic for EditAddCard.xaml
    /// </summary>
    public partial class EditAddCard : UserControl
    {
        #region Declaration (CardType)
        //DigiPhotoDataServices _objDataService;
        /// <summary>
        /// The _hide request
        /// </summary>
        private bool _hideRequest = false;
        /// <summary>
        /// The _result
        /// </summary>
        private string _result;

        /// <summary>
        /// The _parent
        /// </summary>
        private UIElement _parent;

        /// <summary>
        /// Gets or sets the product type unique identifier.
        /// </summary>
        /// <value>
        /// The Card Id unique identifier.
        /// </value>
        public int cardId
        {
            get;
            set;
        }
        public string Name
        {
            get;
            set;
        }
        public string CardIdentificationDigit
        {
            get;
            set;
        }
        public int ImageIdentificationType
        {
            get;
            set;
        }
        public int MaxImage
        {
            get;
            set;
        }
        public string CardDesc
        {
            get;
            set;
        }
        public bool IsActive
        {
            get;
            set;
        }
        #endregion
        public EditAddCard()
        {
            InitializeComponent();
            LoadComboValues();
            BindPackageTypes();
        }
        public void SetParent(UIElement parent)
        {
            _parent = (UIElement)parent;

        }
        /// <summary>
        /// Handles the Click event of the btnSave control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                string error = string.Empty;
                if (Isvalidate(out error))
                {
                    if (chkIsPrePaid.IsChecked == false)
                    {
                        cmbPackage.SelectedIndex = 0;
                    }
                    CardBusiness carBiz = new CardBusiness();
                    if (cardId <= 0)
                    {
                        int imageIdentification = cmbCardFormatType.SelectedValue.ToInt32();
                        //_objDataService = new DigiPhotoDataServices();

                        //if (!_objDataService.IsCardSeriesExits(txtCardIdentificationDigit.Text))
                        if (carBiz.IsCardSeriesExits(txtCardIdentificationDigit.Text))
                        {
                            //if (_objDataService.SetCardTypeInfo(cardId, txtName.Text, txtCardIdentificationDigit.Text, imageIdentification, chkIsActive.IsChecked, Convert.ToInt32(txtMaxImage.Text), txtCardDesc.Text, (bool)chkIsPrePaid.IsChecked, (int)cmbPackage.SelectedValue))
                            if (carBiz.SetCardTypeInfo(cardId, txtName.Text, txtCardIdentificationDigit.Text, imageIdentification, chkIsActive.IsChecked, txtMaxImage.Text.ToInt32(), txtCardDesc.Text, chkIsPrePaid.IsChecked.ToBoolean(), cmbPackage.SelectedValue.ToInt32(), chkIsWaterMark.IsChecked.ToBoolean()))
                            {
                                MessageBoxResult response = MessageBox.Show("[" + txtName.Text + "] Card details has been created successfully", "Digiphoto", MessageBoxButton.OK);

                                if (response == MessageBoxResult.OK)
                                {
                                    ((CardType)_parent).GetCardTypeList();
                                    ClearControls();
                                    HideHandlerDialog();
                                    cardId = 0;
                                }

                            }
                        }
                        else
                        {
                            MessageBox.Show("[" + txtCardIdentificationDigit.Text + "] Card Series already exits , duplicate Card Series not allowed.");
                        }
                        cardId = 0;
                    }
                    else
                    {
                        int imageIdentification = cmbCardFormatType.SelectedValue.ToInt32();
                        //_objDataService = new DigiPhotoDataServices();

                        if (carBiz.SetCardTypeInfo(cardId, txtName.Text, txtCardIdentificationDigit.Text, imageIdentification, chkIsActive.IsChecked, txtMaxImage.Text.ToInt32(), txtCardDesc.Text, chkIsPrePaid.IsChecked.ToBoolean(), cmbPackage.SelectedValue.ToInt32(), chkIsWaterMark.IsChecked.ToBoolean()))
                        {
                            //GetCardTypeList();                              
                            MessageBoxResult response = MessageBox.Show("[" + txtName.Text + "] Card details has been updated successfully", "Digiphoto", MessageBoxButton.OK);
                            if (response == MessageBoxResult.OK)
                            {
                                ((CardType)_parent).GetCardTypeList();
                                ClearControls();
                                HideHandlerDialog();
                                cardId = 0;
                            }
                        }

                    }
                    cardId = 0;

                }
                else
                {
                    if (!string.IsNullOrEmpty(error))
                    {
                        MessageBox.Show(error);
                    }
                    else
                        MessageBox.Show("Please enter all required values");
                }
            }

            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
                if (ex.ToString().ToUpper().Contains("UQ_CARDIDENTIFICATIONDIGIT"))
                    if (string.IsNullOrEmpty(txtCardIdentificationDigit.Text))
                        MessageBox.Show("Card having WaterMark has already exist");
                    else
                        MessageBox.Show("Card Series Starting 4 Digits has already exist");
                return;
            }
        }

        #region Common Method (Card Type)
        /// <summary>
        /// Isvalidates this instance.
        /// </summary>
        /// <returns></returns>
        private bool Isvalidate(out string error)
        {
            error = string.Empty;
            bool isvalid = true;
            if (string.IsNullOrEmpty(txtName.Text))
            {
                isvalid = false;
                txtName.Focus();
            }
            if (chkIsWaterMark.IsChecked == false)
            {
                if (string.IsNullOrEmpty(txtCardIdentificationDigit.Text) || txtCardIdentificationDigit.Text.Length <= 1 || txtCardIdentificationDigit.Text.Length > 16)
                {
                    //isvalid = false;
                    txtCardIdentificationDigit.Focus();
                    error = "Card Series length should be between 2 to 16 characters ";
                    return false;
                }
            }

            if (string.IsNullOrEmpty(txtMaxImage.Text))
            {
                isvalid = false;
                txtMaxImage.Focus();
            }
            else if (chkIsPrePaid.IsChecked == true && cmbPackage.SelectedIndex <= 0)
            {
                //isvalid = false;
                cmbPackage.Focus();
                error = "Select package name.";
                return false;
            }
            if (chkIsWaterMark.IsChecked == true)
            {
                if (chkIsPrePaid.IsChecked == false)
                {
                    error = "Check IsPrepaid option.";
                    return false;
                }
            }


            return isvalid;
        }
        /// <summary>
        /// Clears the controls.
        /// </summary>
        private void ClearControls()
        {
            txtName.Text = string.Empty;
            txtCardDesc.Text = string.Empty;
            txtCardIdentificationDigit.Text = string.Empty;
            txtMaxImage.Text = string.Empty;
            chkIsActive.IsChecked = false;
            cmbCardFormatType.SelectedIndex = 0;
            chkIsPrePaid.IsChecked = false;
            chkIsWaterMark.IsChecked = false;
            txtCardIdentificationDigit.IsEnabled = true;
            cmbPackage.SelectedIndex = 0;
        }
        private void LoadComboValues()
        {
            Dictionary<string, int> CardTypeList = new Dictionary<string, int>();
            CardTypeList.Add("--Select--", 0);
            CardTypeList.Add("QRCode", 401);
            CardTypeList.Add("Barcode", 402);
            CardTypeList.Add("QRCode+Barcode", 405);
            // CardTypeList.Add("RFID", 406);
            cmbCardFormatType.ItemsSource = CardTypeList;
        }
        #endregion Common Method (Card Type)

        private void BindPackageTypes()
        {
            try
            {
                // _objDataService = new DigiPhotoDataServices();
                //ProductBusiness prdBiz = new ProductBusiness();
                //Dictionary<string, int> List1 = _objDataService.GetPackageNames().Where(o => o.DG_IsPackage && o.DG_IsActive == true).ToDictionary(a => a.DG_Orders_ProductType_Name, b => b.DG_Orders_ProductType_pkey);
                //Dictionary<string, int> List = prdBiz.GetPackageType().Where(o => o.DG_IsActive == true).ToDictionary(a => a.DG_Orders_ProductType_Name, b => b.DG_Orders_ProductType_pkey);
                //Dictionary<string, int> PackageList = new Dictionary<string, int>();
                //PackageList.Add("--Select--", 0);
                //foreach (var item in List)
                //{
                //    PackageList.Add(item.Key, item.Value);
                //}
                //cmbPackage.ItemsSource = PackageList;
                //cmbPackage.SelectedIndex = 0;

                List<ProductTypeInfo> PackageList = (new ProductBusiness()).GetPackageType().Where(o => o.DG_IsActive == true).ToList();
                CommonUtility.BindComboWithSelect<ProductTypeInfo>(cmbPackage, PackageList, "DG_Orders_ProductType_Name", "DG_Orders_ProductType_pkey", 0, DigiPhoto.Utility.Repository.Data.ClientConstant.SelectString);
                cmbPackage.SelectedValue = "0";
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }

        /// <summary>
        /// Handles the Click event of the btnCancel control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                ClearControls();
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }

        /// <summary>
        /// Handles the Click event of the btnBack control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void btnClose_Click(object sender, RoutedEventArgs e)
        {
            ClearControls();
            HideHandlerDialog();


            // this.Close();
        }
        /// <summary>
        /// Hides the handler dialog.
        /// </summary>
        /// /// <summary>

        private void HideHandlerDialog()
        {
            try
            {
                _hideRequest = true;
                Visibility = Visibility.Collapsed;
                _parent.IsEnabled = true;
            }
            catch
            {
            }
        }

        private void chkIsPrePaid_Checked(object sender, RoutedEventArgs e)
        {
            stkPackage.Visibility = Visibility.Visible;
        }

        private void chkIsPrePaid_Unchecked(object sender, RoutedEventArgs e)
        {
            stkPackage.Visibility = Visibility.Collapsed;
        }
        private void chkIsWaterMark_Checked(object sender, RoutedEventArgs e)
        {
            txtCardIdentificationDigit.IsEnabled = false;
            txtCardIdentificationDigit.Text = string.Empty;
        }

        private void chkIsWaterMark_Unchecked(object sender, RoutedEventArgs e)
        {
            txtCardIdentificationDigit.IsEnabled = true;
        }

    }
}
