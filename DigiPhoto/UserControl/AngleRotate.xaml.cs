﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace DigiPhoto
{
    /// <summary>
    /// Interaction logic for AngleRotate.xaml
    /// </summary>
    public partial class AngleRotate : UserControl
    {
        #region Fields

        // Using a DependencyProperty as the backing store for Angle.  This enables animation, styling, binding, etc...
        /// <summary>
        /// The angle property
        /// </summary>
        public static readonly DependencyProperty AngleProperty =
            DependencyProperty.Register("Angle", typeof(double), typeof(AngleRotate), new FrameworkPropertyMetadata(0.0, FrameworkPropertyMetadataOptions.AffectsRender | FrameworkPropertyMetadataOptions.BindsTwoWayByDefault, null, coerceValueCallback));

        /// <summary>
        /// The arrow center point
        /// </summary>
        public Point arrowCenterPoint;

        /// <summary>
        /// The is mouse rotating
        /// </summary>
        bool isMouseRotating;

        /// <summary>
        /// The mouse down angle
        /// </summary>
        double mouseDownAngle;

        /// <summary>
        /// The mouse down vector
        /// </summary>
        private Vector mouseDownVector;

        #endregion Fields 
        /// <summary>
        /// Initializes a new instance of the <see cref="AngleRotate"/> class.
        /// </summary>
        public AngleRotate()
        {
            InitializeComponent();
        }
        #region Properties


        /// <summary>
        /// 
        /// </summary>
        public delegate void AngleChangedDelegate();
        /// <summary>
        /// Occurs when [angle changed].
        /// </summary>
        public event AngleChangedDelegate AngleChanged;


        /// <summary>
        /// Gets or sets the angle.
        /// </summary>
        /// <value>
        /// The angle.
        /// </value>
        public double Angle
        {
            get { return (double)GetValue(AngleProperty); }
            set { 
                SetValue(AngleProperty, value);
                AngleChanged();
            }
        }

        #endregion Properties

        #region Methods


        /// <summary>
        /// Invoked when an unhandled <see cref="E:System.Windows.Input.Mouse.LostMouseCapture" /> attached event reaches an element in its route that is derived from this class. Implement this method to add class handling for this event.
        /// </summary>
        /// <param name="e">The <see cref="T:System.Windows.Input.MouseEventArgs" /> that contains event data.</param>
        protected override void OnLostMouseCapture(MouseEventArgs e)
        {
            isMouseRotating = false;
            base.OnLostMouseCapture(e);
        }

        /// <summary>
        /// Invoked when an unhandled <see cref="E:System.Windows.Input.Mouse.MouseDown" /> attached event reaches an element in its route that is derived from this class. Implement this method to add class handling for this event.
        /// </summary>
        /// <param name="e">The <see cref="T:System.Windows.Input.MouseButtonEventArgs" /> that contains the event data. This event data reports details about the mouse button that was pressed and the handled state.</param>
        protected override void OnMouseDown(MouseButtonEventArgs e)
        {
            var mouseDownPoint = e.GetPosition(this);
            mouseDownVector = mouseDownPoint - arrowCenterPoint;
            mouseDownAngle = Angle;
            e.MouseDevice.Capture(this);
            isMouseRotating = true;
            base.OnMouseDown(e);
        }

        /// <summary>
        /// Invoked when an unhandled <see cref="E:System.Windows.Input.Mouse.MouseMove" /> attached event reaches an element in its route that is derived from this class. Implement this method to add class handling for this event.
        /// </summary>
        /// <param name="e">The <see cref="T:System.Windows.Input.MouseEventArgs" /> that contains the event data.</param>
        protected override void OnMouseMove(MouseEventArgs e)
        {
            if (isMouseRotating)
            {
                Point curPos = e.GetPosition(this);
                Vector currentVector = curPos - arrowCenterPoint;
                Angle = Vector.AngleBetween(mouseDownVector, currentVector) + mouseDownAngle;
               // Debug.WriteLine("Angle: " + Angle.ToString());
            }
            base.OnMouseMove(e);
        }

        /// <summary>
        /// Invoked when an unhandled <see cref="E:System.Windows.Input.Mouse.MouseUp" /> routed event reaches an element in its route that is derived from this class. Implement this method to add class handling for this event.
        /// </summary>
        /// <param name="e">The <see cref="T:System.Windows.Input.MouseButtonEventArgs" /> that contains the event data. The event data reports that the mouse button was released.</param>
        protected override void OnMouseUp(MouseButtonEventArgs e)
        {
            if (isMouseRotating)
            {
                e.MouseDevice.Capture(null);
                isMouseRotating = false;
            }
            base.OnMouseUp(e);
        }

        /// <summary>
        /// Raises the <see cref="E:System.Windows.FrameworkElement.SizeChanged" /> event, using the specified information as part of the eventual event data.
        /// </summary>
        /// <param name="sizeInfo">Details of the old and new size involved in the change.</param>
        protected override void OnRenderSizeChanged(SizeChangedInfo sizeInfo)
        {
            base.OnRenderSizeChanged(sizeInfo);
            arrowCenterPoint = new Point(ActualWidth / 2, ActualHeight / 2);
        }

        /// <summary>
        /// Coerces the value callback.
        /// </summary>
        /// <param name="d">The command.</param>
        /// <param name="baseValue">The base value.</param>
        /// <returns></returns>
        static object coerceValueCallback(DependencyObject d, object baseValue)
        {
            var angle = (double)baseValue % 360;
            if (angle < 0)
                return angle + 360;
            else
                return angle;
        }

        #endregion Methods 
    }
}
