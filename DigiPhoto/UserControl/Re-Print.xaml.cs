﻿using DigiAuditLogger;
using DigiPhoto.Common;
using DigiPhoto.DataLayer;
using DigiPhoto.IMIX.Business;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Threading;
using DigiPhoto.IMIX.Model;
/// <summary>
/// 
/// </summary>
namespace DigiPhoto
{
    /// <summary>
    /// Interaction logic for Re_Print.xaml
    /// </summary>
    public partial class Re_Print : UserControl
    {
        /// <summary>
        /// The _parent
        /// </summary>
        private UIElement _parent;
        /// <summary>
        /// The _hide request
        /// </summary>
        private bool _hideRequest = false;
        /// <summary>
        /// The _result
        /// </summary>
        private string _result;
        /// <summary>
        /// The _obj data layer
        /// </summary>
        //DigiPhotoDataServices _objDataLayer = new DigiPhotoDataServices();
        /// <summary>
        /// The printerqueueid
        /// </summary>
        List<string> printerqueueid = new System.Collections.Generic.List<string>();
        /// <summary>
        /// Initializes a new instance of the <see cref="Re_Print"/> class.
        /// </summary>
        public Re_Print()
        {
            InitializeComponent();

          
        }
        /// <summary>
        /// Sets the parent.
        /// </summary>
        /// <param name="parent">The parent.</param>
        public void SetParent(UIElement parent)
        {
            _parent = (UIElement)parent;
        }

        /// <summary>
        /// Shows the handler dialog.
        /// </summary>
        /// <returns></returns>
        public string ShowHandlerDialog()
        {
         
            Visibility = Visibility.Visible;
            txtOrderno.Text = "";
            txtOrderno.Focus();
            _hideRequest = false;
            while (!_hideRequest)
            {
                // HACK: Stop the thread if the application is about to close
                if (this.Dispatcher.HasShutdownStarted ||
                    this.Dispatcher.HasShutdownFinished)
                {
                    break;
                }
                // HACK: Simulate "DoEvents"
                this.Dispatcher.Invoke(
                    DispatcherPriority.Background,
                    new ThreadStart(delegate { }));
                Thread.Sleep(20);
            }

            return _result;
        }

        /// <summary>
        /// Hides the handler dialog.
        /// </summary>
        private void HideHandlerDialog()
        {

            _hideRequest = true;
            Visibility = Visibility.Collapsed;
            _parent.IsEnabled = true;
        }

        /// <summary>
        /// Handles the Click event of the btnClose control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void btnClose_Click(object sender, RoutedEventArgs e)
        {
            _result = string.Empty;
            HideHandlerDialog();
        }

        /// <summary>
        /// Handles the Click event of the btnSearchReprint control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void btnSearchReprint_Click(object sender, RoutedEventArgs e)
        {
            if (CheckValidation())
            {
                GetPrinterQueueDetails();
                
            }
        }
        /// <summary>
        /// Gets the printer queue details.
        /// </summary>
        public void GetPrinterQueueDetails()
        {
            string orderno = txtOrderno.Text.ToUpperInvariant();

            if (orderno.IndexOf("DG-", StringComparison.OrdinalIgnoreCase) < 0)
            {
                orderno = "DG-" + orderno;
            }

            List<AllPrinterQueue> _objprinterdetails = new System.Collections.Generic.List<AllPrinterQueue>();
            try
            {
                //DigiPhotoDataServices _objDataLayer = new DigiPhotoDataServices();
                //var items1 = _objDataLayer.GetPrinterQueueDetails(orderno);
                var items = new PrinterBusniess().GetPrinterQueueDetails(orderno);
                if (items.Count != 0)
                {
                    foreach (var item in items)
                    {
                        AllPrinterQueue _objnew = new AllPrinterQueue();
                        _objnew.Imagname1 = item.DG_Photos_RFID;
                        //_objnew.Filepath1 = LoginUser.DigiFolderThumbnailPath + _objDataLayer.GetFileNameByPhotoID(item.DG_Photos_pKey);
                        PhotoInfo objPhoto = (new PhotoBusiness()).GetPhotoByPhotoID(item.DG_Photos_pKey);
                        if (objPhoto == null)
                            _objnew.Filepath1 = System.IO.Path.Combine(objPhoto.HotFolderPath, "Thumbnails");
                        else
                            _objnew.Filepath1 = System.IO.Path.Combine(objPhoto.HotFolderPath, "Thumbnails", objPhoto.DG_Photos_CreatedOn.ToString("yyyyMMdd"), objPhoto.DG_Photos_FileName);
                        _objnew.Printerkey = item.DG_PrinterQueue_Pkey;
                        _objprinterdetails.Add(_objnew);
                    }

                    dgReprint.ItemsSource = _objprinterdetails;
                    dgReprint.Visibility = Visibility.Visible;
                }
                else
                {
                    MessageBox.Show("No records found, please check the order number");
                }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }
        /// <summary>
        /// Checks the validation.
        /// </summary>
        /// <returns></returns>
        private bool CheckValidation()
        {
            if (txtOrderno.Text =="")
            {
                MessageBox.Show("Please enter the Order number first");
                return false;
            }
            else
                return true;
        }

        /// <summary>
        /// Handles the Click event of the btnReprint control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void btnReprint_Click(object sender, RoutedEventArgs e)
        {

            if (printerqueueid.Count != 0)
            {
                foreach (var items in printerqueueid)
                {
                    //var QDetails = _objDataLayer.GetQueueDetail(Convert.ToInt32(items));
                    var QDetails = new PrinterBusniess().GetQueueDetail(Convert.ToInt32(items));
                   // _objDataLayer.SetPrinterQueueForReprint(Convert.ToInt32(items));
                    new PrinterBusniess().UpdatePrintCountForReprint(Convert.ToInt32(items));
                    //_objDataLayer.SetOrderDetailsForReprint(QDetails.DG_Orders_LineItems_pkey, LoginUser.SubStoreId);
                    new OrderBusiness().SetOrderDetailsForReprint(QDetails.DG_Orders_LineItems_pkey, LoginUser.SubStoreId);
                    AuditLog.AddUserLog(Common.LoginUser.UserId, (int)FrameworkHelper.ActionType.RePrint, "Reprint " + QDetails.DG_Orders_ProductType_Name.ToString() + " ( image no:" + QDetails.DG_Photos_RFID.ToString() + " ) of Order No " + QDetails.DG_Orders_Number.ToString() + " from substore " + LoginUser.SubstoreName);  
                }
                printerqueueid = new System.Collections.Generic.List<string>();
                MessageBox.Show("Images sent to re-print succesfully");
                HideHandlerDialog();
            }
            else
            {
                MessageBox.Show("Please select any image to Re-Print");
            }
        }

        /// <summary>
        /// Handles the Checked event of the IsChecked control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void IsChecked_Checked(object sender, RoutedEventArgs e)
        {
            CheckBox checkboxSender = (CheckBox)sender;
           printerqueueid.Add(checkboxSender.CommandParameter.ToString());
        }

        /// <summary>
        /// Handles the Click event of the btncancel control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void btncancel_Click(object sender, RoutedEventArgs e)
        {
            txtOrderno.Text = "";
            txtOrderno.Focus();
            dgReprint.Visibility = Visibility.Collapsed;
        }

        private void txtOrderno_GotFocus(object sender, RoutedEventArgs e)
        {
            btnSearchReprint.IsDefault = true;
        }

        private void txtOrderno_LostFocus(object sender, RoutedEventArgs e)
        {
            btnSearchReprint.IsDefault = false;
        }
    }

    #region Class for PrinterQueueDetails
    class AllPrinterQueue
    {
        private string Imagname;

        public string Imagname1
        {
            get { return Imagname; }
            set { Imagname = value; }
        }

        private string Filepath;

        public string Filepath1
        {
            get { return Filepath; }
            set { Filepath = value; }
        }
        private int printerkey;

        public int Printerkey
        {
            get { return printerkey; }
            set { printerkey = value; }
        }
    }
    #endregion
}
