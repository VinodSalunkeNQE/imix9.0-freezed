﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using DigiPhoto.IMIX.Model;

namespace DigiPhoto
{
    /// <summary>
    /// Interaction logic for CtrlProfileRate.xaml
    /// </summary>
    public partial class CtrlProfileRate : UserControl
    {
        DigiPhoto.IMIX.Business.CurrencyExchangeBussiness _objCurrencyBusiness = null;
        public CtrlProfileRate()
        {
            InitializeComponent();
            Visibility = Visibility.Hidden;
        }
        /// <summary>
        /// The _parent
        /// </summary>
        private UIElement _parent;
        /// <summary>
        /// The _hide request
        /// </summary>
        private bool _hideRequest = false;
        /// <summary>
        /// The _result
        /// </summary>
        private string _result;
        /// <summary>
        /// The controlon
        /// </summary>
        TextBox controlon;
        /// <summary>
        /// Sets the parent.
        /// </summary>
        /// <param name="parent">The parent.</param>
        public void SetParent(UIElement parent)
        {
            _parent = (UIElement)parent;

        }


        #region Message

        /// <summary>
        /// Gets or sets the message card payment.
        /// </summary>
        /// <value>
        /// The message card payment.
        /// </value>

        public long ProfileID { get; set; }



        // public Int64 TenEndNumber { get; set; }

        // Using a DependencyProperty as the backing store for Message.
        // This enables animation, styling, binding, etc...
        /// <summary>
        /// The message card payment property
        /// </summary>
        //public static readonly DependencyProperty MessageProfileRateFormProperty =
        //    DependencyProperty.Register(
        //        "MessageProfileRateForm", typeof(string), typeof(ModalDialog), new UIPropertyMetadata(string.Empty));



        /// <summary>
        /// Shows the handler dialog.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <returns></returns>
        public string ShowHandlerDialog(string message)
        {
           
            Visibility = Visibility.Visible;
            //txt6X8Westage.Text = SixEightWestage;
            // txt8X10Westage.Text = EightTenWestage;
            FillGrid(ProfileID);
            _hideRequest = false;
            while (!_hideRequest)
            {
                // HACK: Stop the thread if the application is about to close
                if (this.Dispatcher.HasShutdownStarted ||
                    this.Dispatcher.HasShutdownFinished)
                {
                    break;
                }
                // HACK: Simulate "DoEvents"
                this.Dispatcher.Invoke(
                    DispatcherPriority.Background,
                    new ThreadStart(delegate { }));
                Thread.Sleep(20);
            }
            
            return _result;
        }

        /// <summary>
        /// Hides the handler dialog.
        /// </summary>
        private void HideHandlerDialog()
        {
            //  _result = null;
            _hideRequest = true;
            Visibility = Visibility.Collapsed;
            _parent.IsEnabled = true;
        }
        
        #endregion

       
        

        /// <summary>
        /// Handles the Click event of the btnSubmit control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>

        private void btnClose_Click(object sender, RoutedEventArgs e)
        {
            _result = string.Empty;
            HideHandlerDialog();
        }

        //private void txt6X8Westage_SelectionChanged(object sender, RoutedEventArgs e)
        //{
        //    //txt6X8Westage.Text = SixEightWestage;
        //    SetSixEightWestage(FromDate, ToDate, SubStoreID, SixEightStartingNumber, Convert.ToInt32(txt6X8CloseNumber.Text));
        //}

        public void FillGrid(long ProfileID)
        {
            _objCurrencyBusiness = new DigiPhoto.IMIX.Business.CurrencyExchangeBussiness();
            List<RateDetailInfo> _profilerateList = new List<RateDetailInfo>();
            _profilerateList = _objCurrencyBusiness.GetProfilerateDetailList(ProfileID);
            dgRateDetail.ItemsSource = _profilerateList;
        }


    }

}

