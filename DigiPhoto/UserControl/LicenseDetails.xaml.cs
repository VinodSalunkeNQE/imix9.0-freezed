﻿using DigiPhoto.IMIX.Business;
using DigiPhoto.IMIX.Model;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
//using DigiPhoto.IMIX.Business;




namespace DigiPhoto
{
    /// <summary>
    /// Interaction logic for LicenseDetails.xaml
    /// </summary>
    public partial class LicenseDetails : Window
    {
        static LicenseDetails _licenseInfoDetails;
        string _textFont, _licNoTextFont;
        int _fontSize, _licNoFontSize;
        static Dictionary<string, Control> _controls = new Dictionary<string, Control>();
        static Canvas _canvas;
        int _photoId;
        static bool _isLicenseDetailsSavedToDB = false;
        public static bool _isLicenseDetailsWindowDestroyed = false;
        public static bool _isCancelPressed = false;
        private bool IsCapsOn = false;
        private string _controlOn;

        public static double canvasInitialHeight = 0;
        public static double canvasInitialWidth = 0;


        TextBlock _txtName;// = GetLicenseProductTextBlock(licenseInfo.txtName.Text, new FontFamily("Arial Bold"), 30);
        TextBlock _txtAge;// = GetLicenseProductTextBlock(licenseInfo.txtAge.Text, new FontFamily("Arial Bold"), 30);
        TextBlock _txtIssueDate;// = GetLicenseProductTextBlock(DateTime.Now.ToString("dd-MM-yyyy"), new FontFamily("Arial Bold"), 30);
        TextBlock _txtExpireDate;// = GetLicenseProductTextBlock(DateTime.Now.AddMonths(Int32.Parse(System.Configuration.ConfigurationManager.AppSettings["LicenseExpireInMonths"].ToString())).ToString("dd-MM-yyyy"), new FontFamily("Arial Black"), 30);
        TextBlock _txtLicenseNo;// = GetLicenseProductTextBlock((new LicenseProductBusiness()).GetLicenseNextNumber(), new FontFamily("Arial Black"), 40, Brushes.Red);

        private LicenseDetails()
        {
            InitializeComponent();

            _textFont = "Arial";
            _licNoTextFont = "Arial Black";


            _fontSize = 50;
            _licNoFontSize = 58;
            _isCancelPressed = false;

            //if(_licenseInfoDetails == null)
            //    _licenseInfoDetails = new LicenseInfoDetails();        
        }

        private LicenseDetails(Canvas canvas) : this()
        {
            _canvas = canvas;
            this.UpdateField(canvas);
            //_canvas.Background = Brushes.Red;
        }

        private void btnSubmit_Click(object sender, RoutedEventArgs e)
        {
            _isCancelPressed = false;

            if (string.IsNullOrEmpty(this.txtName.Text))
            {
                this.txtName.Focus();
                return;
            }
            if (string.IsNullOrEmpty(this.txtAge.Text))
            {
                this.txtAge.Focus();
                return;
            }

            _isLicenseDetailsWindowDestroyed = true;
            _isCancelPressed = false;
            this.DialogResult = true;
        }

        private void btnSubmitCancel_Click(object sender, RoutedEventArgs e)
        {
            this.Visibility = Visibility.Collapsed;
            _isCancelPressed = true;
            this.DialogResult = false;
        }

        void SetPositionInCanvas(UIElement control, int left, int top, int zIndex)
        {
            Canvas.SetZIndex(control, zIndex);
            Canvas.SetLeft(control, left);
            Canvas.SetTop(control, top);
        }

        public static bool? ShowDialog(Canvas canvas)
        {
            //string textFont, int fontSize, string licNoTextFont, int licNoFontSize
            //if (_licenseInfoDetails == null || _isLicenseDetailsWindowDestroyed)

            if (_licenseInfoDetails == null)
            {
                _licenseInfoDetails = new LicenseDetails(canvas);
                _licenseInfoDetails.Closing += _licenseInfoDetails._licenseInfoDetails_Closing;
                _licenseInfoDetails.Visibility = Visibility.Visible;
            }
            else
            {
                _canvas = canvas;
                _licenseInfoDetails.Closing += _licenseInfoDetails._licenseInfoDetails_Closing;
                _licenseInfoDetails.Visibility = Visibility.Visible;
                _licenseInfoDetails.UpdateField(canvas);
            }
            return (_licenseInfoDetails.ShowDialog());
        }
        private void UpdateField(Canvas canvas)
        {

            foreach (UIElement child in canvas.Children)
            {
                if (child.GetType() == typeof(TextBlock))
                {
                    TextBlock tb = (TextBlock)child;
                    if (tb.Name == "txtName")
                    {
                        txtName.Text = tb.Text;
                    }
                    if (tb.Name == "txtAge")
                    {
                        txtAge.Text = tb.Text;
                    }
                }
            }
        }
        private void _licenseInfoDetails_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (string.IsNullOrEmpty(this.txtName.Text) || string.IsNullOrEmpty(this.txtAge.Text) || _isCancelPressed)
            {
                _isCancelPressed = true;
            }
            else
            {
                _isCancelPressed = false;
            }
            e.Cancel = true;
            this.Visibility = Visibility.Collapsed;
            ///_isLicenseDetailsWindowDestroyed = true; //Need to check
        }

        //public static void DisplayOnLicenseBorder(int zIndex, int photoId)
        //{
        //    if (_licenseInfoDetails == null) return;
        //    _licenseInfoDetails.SetTextBlocks(zIndex, (new LicenseProductBusiness()).GetLicenseNumber(photoId));
        //    foreach (Control control in _controls.Values)
        //    {
        //        _canvas.Children.Add(control.UIElement);
        //        _licenseInfoDetails.SetPositionInCanvas(control.UIElement, control.Left, control.Top, control.ZIndex);
        //    }
        //    _licenseInfoDetails.UpdateLicenseDetailsInDB(photoId);
        //}
        //{

        //}

        public static void DisplayOnLicenseBorder(int zIndex, int photoId, bool useDetailsInDB = false)
        {
            try
            {
                if (_licenseInfoDetails == null) return;
                _licenseInfoDetails._photoId = photoId;
                LicenseProductBusiness lpb = new LicenseProductBusiness();

                if (useDetailsInDB)
                {
                    _licenseInfoDetails.SetTextBlocks(zIndex, string.Empty, lpb.GetLicenseDetails(photoId));
                    lpb.LicenseDetailsToggleOnCanvas(false, photoId);
                    //lpb.LicenseDetailsToggleOnCanvas(photoId, false);
                }
                else
                    _licenseInfoDetails.SetTextBlocks(zIndex, lpb.GetLicenseNumber(photoId));

                foreach (Control control in _controls.Values)
                {
                    _canvas.Children.Add(control.UIElement);
                    _licenseInfoDetails.SetPositionInCanvas(control.UIElement, control.Left, control.Top, control.ZIndex);
                }
                //_licenseInfoDetails.UpdateLicenseDetailsInDB(photoId);
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogFileWrite("DisplayOnLicenseBorder exception " + Convert.ToString(ex.InnerException));
            }
        }

        //public static void SaveLicenseDetailsToDB()
        //{
        //    if (_licenseInfoDetails == null) throw (new Exception("LicenseDetails Window instance is null. Can not save license details to the database!"));
        //    _licenseInfoDetails.UpdateLicenseDetailsInDB();
        //    //_licenseInfoDetails.Close();
        //    _licenseInfoDetails = null;
        //    _isLicenseDetailsWindowDestroyed = true;
        //}

        public static void SaveLicenseDetailsToDB()
        {
            //BY KCB ON 11 MAY 2018 For handling null issue 
            //if (_licenseInfoDetails != null)
            // throw (new Exception("LicenseDetails Window instance is null. Can not save license details to the database!"));
            //    _licenseInfoDetails.UpdateLicenseDetailsInDB();
            //    //_licenseInfoDetails.Close();
            //    _licenseInfoDetails = null;
            //    //_isLicenseDetailsWindowDestroyed = true;
            //end
            if (_licenseInfoDetails != null)
            {
                //throw (new Exception("LicenseDetails Window instance is null. Can not save license details to the database!"));
                _licenseInfoDetails.UpdateLicenseDetailsInDB();
                //_licenseInfoDetails.Close();
                _licenseInfoDetails = null;
                //_isLicenseDetailsWindowDestroyed = true;
            }
        }


        void UpdateLicenseDetailsInDB()
        {
            try
            {
                //By Anisur on 10 MAY 2018 for rectifieng license saving issue when age and name is not given;
                //int age = int.Parse(_controls["Age"].UserInput);
                int age = 0;
                if (!string.IsNullOrEmpty(_controls["Age"].UserInput))
                {
                    age = Convert.ToInt32(_controls["Age"].UserInput);
                }
                //end

                DateTime issuesDate = DateTime.ParseExact(_controls["IssueDate"].UserInput, "dd-MM-yyyy", null);// DateTime.Parse(_controls["IssueDate"].UserInput);
                DateTime expireDate = DateTime.ParseExact(_controls["ExpireDate"].UserInput, "dd-MM-yyyy", null);// DateTime.Parse(_controls["ExpireDate"].UserInput);
                string licenseNo = _controls["LicenseNo"].UserInput;
                int licensePrefix = -1, licenseSufix = -1;
                if (!string.IsNullOrEmpty(licenseNo))
                {
                    //start
                    //By Anisur on 10 MAY 2018 for rectifieng license saving issue when age and name is not given;
                    //int hypenIndex = licenseNo.IndexOf('-');
                    //licensePrefix = int.Parse(licenseNo.Substring(0, hypenIndex));
                    //licenseSufix = int.Parse(licenseNo.Substring(hypenIndex + 1));
                    string[] licensedetails = licenseNo.Split('-');
                    licensePrefix = int.Parse(licensedetails[0]);
                    licenseSufix = int.Parse(licensedetails[1]);
                    //end 
                }
                if (licensePrefix != -1)
                {
                    LicenseProductBusiness lpb = new LicenseProductBusiness();
                    lpb.UpdateLicenseDetails(_photoId, _controls["Name"].UserInput, age, licensePrefix, licenseSufix, issuesDate, expireDate);
                    _isLicenseDetailsSavedToDB = true;
                }
            }
            catch (Exception ex)
            {
            }
        }

        public static void AddControlsToCanvasIfLicenseInfoInDB(Canvas canvas, int zIndex, int photoId)
        {
            _licenseInfoDetails = new LicenseDetails(canvas);
            DisplayOnLicenseBorder(zIndex, photoId, true);
            //_licenseInfoDetails = null;
        }

        public static void RemoveControlsFromCanvas(int? photoId = null)
        {
            if (_controls == null)
                return;
            foreach (Control control in _controls.Values)
                _canvas.Children.Remove(control.UIElement);

            _controls.Clear();
            if (photoId == null || photoId < 1)
                return;
            LicenseProductBusiness lpb = new LicenseProductBusiness();
            lpb.LicenseDetailsToggleOnCanvas(true, photoId);
            //lpb.LicenseDetailsToggleOnCanvas(photoId, true);
        }

        public static bool IsLicenseDetailsRemovedFromCanvas(int photoId)
        {
            LicenseProductBusiness lpb = new LicenseProductBusiness();
            return (lpb.IsLicenseDetailsRemoved(photoId));
        }


        /// <summary>
        /// Vinod Salunke
        /// Licence Product
        /// </summary>
        /// <param name="zIndex"></param>///ZIndex to make control over the form
        /// <param name="licenseNo"></param> ///License number to be displayed on print
        /// <param name="lud"></param>User details like name, age etc..
        void SetTextBlocks(int zIndex, string licenseNo, LicenseUserDetails lud = null)
        {
            try
            {
                //Remove previous texts for Name and Age from backend before setting new text values
                RemoveControlsFromCanvas();
                //int zIndex = Canvas.GetZIndex(_canvas) + 1;               
                //int leftValue = Convert.ToInt32(ConfigurationManager.AppSettings["LicLeft"].ToString()); //547
                //int topValue = Convert.ToInt32(ConfigurationManager.AppSettings["LicTop"].ToString()); //190

                //double canvasHeight = _canvas.ActualHeight;
                //double canvasWidth = _canvas.ActualWidth;
                if (Convert.ToString(_canvas.Height) == "NaN" || Convert.ToString(_canvas.Width) == "NaN")
                {
                    _canvas.Height = _canvas.ActualHeight;
                    _canvas.Width = _canvas.ActualWidth;
                }
                double canvasHeight = _canvas.Height;
                double canvasWidth = _canvas.Width;
                int fontSizeRatio = 1;
                //if (Convert.ToInt32(canvasHeight) == 0 && Convert.ToInt32(canvasWidth) == 0)
                //{
                //    canvasHeight = 1440;
                //    canvasWidth = 1920;
                //}
                //else if (Convert.ToInt32(canvasHeight) == 1440 && Convert.ToInt32(canvasWidth) == 1920)
                //{
                //    canvasHeight = 1440;
                //    canvasWidth = 1920;
                //}
                //else if (Convert.ToInt32(canvasHeight) > 1440 && Convert.ToInt32(canvasWidth) >= 1920)
                //{
                //}
                //else
                //{
                //    canvasHeight = 718;
                //    canvasWidth = 957;
                //}
                if (canvasHeight < 900)
                {
                    //_fontSize = 30;
                    //_licNoFontSize = 40;
                    fontSizeRatio = Convert.ToInt32(Math.Round(Convert.ToDouble(canvasWidth / 1000), 2));
                    fontSizeRatio = (Convert.ToInt32(3.144 * canvasWidth) / 100);
                    _fontSize = fontSizeRatio;
                    _licNoFontSize = _fontSize + 10;
                    //_licNoFontSize = fontSizeRatio * 40;
                }
                else
                {
                    ///fontSizeRatio = Convert.ToInt32(Math.Round(Convert.ToDouble(canvasWidth / 1000), 2));

                    fontSizeRatio = (Convert.ToInt32(3.144 * canvasWidth) / 100);
                    _fontSize = fontSizeRatio;
                    _licNoFontSize = _fontSize + 10;
                    //_fontSize = fontSizeRatio * 25;
                    //_licNoFontSize = fontSizeRatio * 40;
                }

                int percWidth = (Convert.ToInt32(3.65 * canvasWidth) / 100) + 40;
                int percHeight = 0;
                if (canvasHeight <= 640)
                {
                    percHeight = Convert.ToInt32(25.22 * canvasHeight) / 100;
                }
                else
                {
                    percHeight = Convert.ToInt32(22.22 * canvasHeight) / 100;
                }


                if (canvasInitialHeight != 0 && canvasHeight <= 0)
                {
                    canvasHeight = canvasInitialHeight;
                }

                if (Convert.ToInt32(canvasHeight) > 0)
                {
                    _txtName = GetLicenseProductTextBlock(lud == null ? this.txtName.Text : lud.Name, new FontFamily(_textFont), _fontSize);
                    _txtName.Uid = "Name";
                    ////Vinod Salunke 15Aug2018
                    _txtName.HorizontalAlignment = HorizontalAlignment.Center;
                    _txtName.VerticalAlignment = VerticalAlignment.Top;
                    //Thickness mg = new Thickness(0, 10, 0, 0);
                    //_txtName.Margin = mg;
                    _txtName.UpdateLayout();
                    double left = (canvasWidth) / 2;
                    Canvas.SetLeft(_txtName, left);
                    Canvas.SetTop(_txtName, 0);
                    //40 is 4.629% of 864 i.e. Width
                    double leftMargin = Math.Round((4.629 * canvasWidth) / 100);//Calculate margin as per aspect ratio
                    double topMargin = Math.Round((22.3765 * canvasHeight) / 100);//Calculate margin as per aspect ratio
                    //_controls.Add("Name", new Control(_txtName, Convert.ToInt32(canvasWidth / 2) + percWidth, ((Convert.ToInt32(canvasHeight / 2) - percHeight) + 5), zIndex)); //Current working
                    ////_controls.Add("Name", new Control(_txtName, (int)left + 40, (int)(canvasHeight / 2) - 155, zIndex));
                    _controls.Add("Name", new Control(_txtName, (int)(left + leftMargin), (int)((canvasHeight / 2) - topMargin), zIndex));

                    _txtAge = GetLicenseProductTextBlock(lud == null ? this.txtAge.Text : lud.Age.ToString(), new FontFamily(_textFont), _fontSize);
                    _txtAge.Uid = "Age";

                    _txtAge.HorizontalAlignment = HorizontalAlignment.Center;
                    _txtAge.VerticalAlignment = VerticalAlignment.Top;
                    //_txtAge.Margin = mg;
                    _txtAge.UpdateLayout();
                    Canvas.SetLeft(_txtAge, left);
                    Canvas.SetTop(_txtAge, 0);
                    //_controls.Add("Age", new Control(_txtAge, Convert.ToInt32(canvasWidth / 2) + percWidth, Convert.ToInt32(canvasHeight / 2) - ((Convert.ToInt32(13.33 * canvasHeight) / 100) - 10), zIndex));
                    topMargin = Math.Round((13.2 * canvasHeight) / 100);//Calculate margin as per aspect ratio //100 //110
                    _controls.Add("Age", new Control(_txtAge, (int)(left + leftMargin), (int)((canvasHeight / 2) - topMargin), zIndex));


                    _txtIssueDate = GetLicenseProductTextBlock(lud == null ? DateTime.Now.ToString("dd-MM-yyyy") : lud.IssueDate.ToString("dd-MM-yyyy"), new FontFamily(_textFont), _fontSize);
                    _txtIssueDate.Uid = "IssueDate";

                    _txtIssueDate.HorizontalAlignment = HorizontalAlignment.Center;
                    _txtIssueDate.VerticalAlignment = VerticalAlignment.Top;
                    //_txtIssueDate.Margin = mg;
                    _txtIssueDate.UpdateLayout();
                    Canvas.SetLeft(_txtIssueDate, left);
                    Canvas.SetTop(_txtIssueDate, 0);

                    int expireInMonths = Int32.Parse(System.Configuration.ConfigurationManager.AppSettings["LicenseExpireInMonths"].ToString());
                    _txtExpireDate = GetLicenseProductTextBlock(lud == null ? DateTime.Now.AddMonths(expireInMonths).ToString("dd-MM-yyyy") : lud.ExpiryDate.ToString("dd-MM-yyyy"), new FontFamily(_textFont), _fontSize);
                    _txtExpireDate.Uid = "ExpireDate";

                    _txtExpireDate.HorizontalAlignment = HorizontalAlignment.Center;
                    _txtExpireDate.VerticalAlignment = VerticalAlignment.Top;
                    //_txtExpireDate.Margin = mg;
                    _txtExpireDate.UpdateLayout();
                    Canvas.SetLeft(_txtExpireDate, left);
                    Canvas.SetTop(_txtExpireDate, 0);

                    if (lud == null && string.IsNullOrEmpty(this.txtName.Text) && string.IsNullOrEmpty(this.txtAge.Text))
                    {
                        ///Below 2 statements are commented because on load window if no data entered then dont display data on image
                        //_controls.Add("IssueDate", new Control(_txtIssueDate, Convert.ToInt32(canvasWidth / 2) + percWidth + 28, Convert.ToInt32(canvasHeight / 2) - ((Convert.ToInt32(4.0 * canvasHeight) / 100) - 45), zIndex)); 
                        //_controls.Add("ExpireDate", new Control(_txtExpireDate, Convert.ToInt32(canvasWidth / 2) + percWidth + 28, Convert.ToInt32(canvasHeight / 2) + ((Convert.ToInt32(6.4 * canvasHeight) / 100) + 53), zIndex));
                    }
                    else
                    {
                        //_controls.Add("IssueDate", new Control(_txtIssueDate, Convert.ToInt32(canvasWidth / 2) + percWidth, Convert.ToInt32(canvasHeight / 2) - ((Convert.ToInt32(4.0 * canvasHeight) / 100) - 10), zIndex));
                        //_controls.Add("ExpireDate", new Control(_txtExpireDate, Convert.ToInt32(canvasWidth / 2) + percWidth, Convert.ToInt32(canvasHeight / 2) + ((Convert.ToInt32(6.4 * canvasHeight) / 100) + 10), zIndex));
                        topMargin = Math.Round((3.858 * canvasHeight) / 100);//Calculate margin as per aspect ratio
                        _controls.Add("IssueDate", new Control(_txtIssueDate, (int)(left + leftMargin), (int)((canvasHeight / 2) - topMargin), zIndex));
                        topMargin = Math.Round((5.401 * canvasHeight) / 100);//Calculate margin as per aspect ratio
                        _controls.Add("ExpireDate", new Control(_txtExpireDate, (int)(left + leftMargin), (int)((canvasHeight / 2) + topMargin), zIndex));

                        _txtLicenseNo = GetLicenseProductTextBlock(lud == null ? licenseNo : LicenseProductBusiness.GetLiceseNumberFromPrefixSufix(lud.LicensePrefix, lud.LicenseSufix), new FontFamily(_licNoTextFont), _licNoFontSize, Brushes.Red);
                        _txtLicenseNo.Uid = "LicenseNo";

                        _txtLicenseNo.HorizontalAlignment = HorizontalAlignment.Center;
                        _txtLicenseNo.VerticalAlignment = VerticalAlignment.Top;
                        //_txtLicenseNo.Margin = mg;
                        _txtLicenseNo.UpdateLayout();
                        Canvas.SetLeft(_txtLicenseNo, left);
                        Canvas.SetTop(_txtLicenseNo, 0);
                        //100 is 11.574% of 864 i.e. Width
                        //_controls.Add("LicenseNo", new Control(_txtLicenseNo, Convert.ToInt32(canvasWidth / 2) + (Convert.ToInt32(11.35 * canvasWidth) / 100) + 5, Convert.ToInt32(canvasHeight / 2) + ((Convert.ToInt32(29.17 * canvasHeight) / 100) + 5), zIndex));
                        leftMargin = Math.Round((11.574 * canvasWidth) / 100);//Calculate margin as per aspect ratio
                        topMargin = Math.Round((28.703 * canvasHeight) / 100);
                        _controls.Add("LicenseNo", new Control(_txtLicenseNo, (int)(left + leftMargin), (int)((canvasHeight / 2) + topMargin), zIndex));
                    }

                    //_txtLicenseNo = GetLicenseProductTextBlock(lud == null ? licenseNo : LicenseProductBusiness.GetLiceseNumberFromPrefixSufix(lud.LicensePrefix, lud.LicenseSufix), new FontFamily(_licNoTextFont), _licNoFontSize, Brushes.Red);
                    //_txtLicenseNo.Uid = "LicenseNo";
                    //_controls.Add("LicenseNo", new Control(_txtLicenseNo, Convert.ToInt32(canvasWidth / 2) + (Convert.ToInt32(11.35 * canvasWidth) / 100) + 5, Convert.ToInt32(canvasHeight / 2) + ((Convert.ToInt32(29.17 * canvasHeight) / 100) + 5), zIndex)); //+218 //+420
                }
                else
                {
                    _txtName = GetLicenseProductTextBlock(lud == null ? this.txtName.Text : lud.Name, new FontFamily(_textFont), _fontSize);
                    _txtName.Uid = "Name";
                    _controls.Add("Name", new Control(_txtName, 547, 190, zIndex)); //547,190
                    _txtAge = GetLicenseProductTextBlock(lud == null ? this.txtAge.Text : lud.Age.ToString(), new FontFamily(_textFont), _fontSize);
                    _txtAge.Uid = "Age";
                    _controls.Add("Age", new Control(_txtAge, 547, 263, zIndex)); //547,263
                    _txtIssueDate = GetLicenseProductTextBlock(lud == null ? DateTime.Now.ToString("dd-MM-yyyy") : lud.IssueDate.ToString("dd-MM-yyyy"), new FontFamily(_textFont), _fontSize);
                    _txtIssueDate.Uid = "IssueDate";
                    if (!string.IsNullOrEmpty(this.txtName.Text) && !string.IsNullOrEmpty(this.txtAge.Text))
                    {
                        _controls.Add("IssueDate", new Control(_txtIssueDate, 547, 336, zIndex));
                        int expireInMonths = Int32.Parse(System.Configuration.ConfigurationManager.AppSettings["LicenseExpireInMonths"].ToString());
                        _txtExpireDate = GetLicenseProductTextBlock(lud == null ? DateTime.Now.AddMonths(expireInMonths).ToString("dd-MM-yyyy") : lud.ExpiryDate.ToString("dd-MM-yyyy"), new FontFamily(_textFont), _fontSize);
                        _txtExpireDate.Uid = "ExpireDate";
                        _controls.Add("ExpireDate", new Control(_txtExpireDate, 547, 402, zIndex));
                        _txtLicenseNo = GetLicenseProductTextBlock(lud == null ? licenseNo : LicenseProductBusiness.GetLiceseNumberFromPrefixSufix(lud.LicensePrefix, lud.LicenseSufix), new FontFamily(_licNoTextFont), _licNoFontSize, Brushes.Red);
                        _txtLicenseNo.Uid = "LicenseNo";
                        _controls.Add("LicenseNo", new Control(_txtLicenseNo, 547, 562, zIndex));
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogFileWrite("SetTextBlocks() exception" + ex.InnerException.ToString());
            }
        }

        private void txtAge_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            string onlyNumeric = @"^([0-9]+(.[0-9]+)?)$";
            Regex regex = new Regex(onlyNumeric);
            e.Handled = !regex.IsMatch(e.Text);
        }
        public int LicenceAge { get; set; }
        public string LicenceName { get; set; }

        #region KeyBoard Logic

        private void ToggleKey()
        {
            if (IsCapsOn)
            {
                btnA.Content = "A";
                btnB.Content = "B";
                btnC.Content = "C";
                btnD.Content = "D";
                btnE.Content = "E";
                btnF.Content = "F";
                btnG.Content = "G";
                btnH.Content = "H";
                btnI.Content = "I";
                btnJ.Content = "J";
                btnK.Content = "K";
                btnL.Content = "L";
                btnM.Content = "M";
                btnN.Content = "N";
                btnO.Content = "O";
                btnP.Content = "P";
                btnQ.Content = "Q";
                btnR.Content = "R";
                btnS.Content = "S";
                btnT.Content = "T";
                btnU.Content = "U";
                btnV.Content = "V";
                btnW.Content = "W";
                btnX.Content = "X";
                btnY.Content = "Y";
                btnZ.Content = "Z";
            }
            else
            {
                btnA.Content = "a";
                btnB.Content = "b";
                btnC.Content = "c";
                btnD.Content = "d";
                btnE.Content = "e";
                btnF.Content = "f";
                btnG.Content = "g";
                btnH.Content = "h";
                btnI.Content = "i";
                btnJ.Content = "j";
                btnK.Content = "k";
                btnL.Content = "l";
                btnM.Content = "m";
                btnN.Content = "n";
                btnO.Content = "o";
                btnP.Content = "p";
                btnQ.Content = "q";
                btnR.Content = "r";
                btnS.Content = "s";
                btnT.Content = "t";
                btnU.Content = "u";
                btnV.Content = "v";
                btnW.Content = "w";
                btnX.Content = "x";
                btnY.Content = "y";
                btnZ.Content = "z";
            }
        }

        private void btn_Click(object sender, RoutedEventArgs e)
        {
            Button _objbtn = new Button();
            _objbtn = (Button)sender;
            switch (_objbtn.Content.ToString())
            {
                case "ENTER":
                    {
                        break;
                    }
                case "SPACE":
                    {
                        txtName.Text = txtName.Text + " ";
                        break;
                    }
                case "CLOSE":
                    {
                        KeyBorder.Visibility = Visibility.Collapsed;
                        break;
                    }
                case "Back":
                    {
                        if (txtName.Text.Length > 0)
                        {
                            txtName.Text = txtName.Text.Remove(txtName.Text.Length - 1, 1);
                        }
                        break;
                    }
                default:
                    {
                        txtName.Text = txtName.Text + _objbtn.Content;

                    }
                    break;
            }
        }

        private void btnCapsLock_Click(object sender, RoutedEventArgs e)
        {
            IsCapsOn = !IsCapsOn;
            ToggleKey();
        }

        #endregion
        private void txtName_GotFocus(object sender, RoutedEventArgs e)
        {
            KeyBorder.Visibility = Visibility.Visible;

            key1.Visibility = Visibility.Collapsed;
            _controlOn = "LicenceProduct";
        }

        private void txtAge_GotFocus(object sender, RoutedEventArgs e)
        {
            key1.Visibility = Visibility.Visible;
            KeyBorder.Visibility = Visibility.Collapsed;
        }

        /// <summary>
        /// Determines whether [is text allowed] [the specified text].
        /// </summary>
        /// <param name="text">The text.</param>
        /// <returns></returns>
        private static bool IsTextAllowed(string text)
        {
            Regex regex = new Regex("[^0-9.-]+"); //regex that matches disallowed text
            return !regex.IsMatch(text);
        }

        /// <summary>
        /// Pastings the handler.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="DataObjectPastingEventtxtAmountEntered_PreviewTextInputArgs"/> instance containing the event data.</param>
        private void PastingHandler(object sender, DataObjectPastingEventArgs e)
        {
            if (e.DataObject.GetDataPresent(typeof(String)))
            {
                String text = (String)e.DataObject.GetData(typeof(String));
                if (!IsTextAllowed(text)) e.CancelCommand();
            }
            else e.CancelCommand();
        }

        private void txtAmountEntered_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = !IsTextAllowed(e.Text);
        }

        private void txtName_LostFocus(object sender, RoutedEventArgs e)
        {
            KeyBorder.Visibility = Visibility.Visible;
            _controlOn = "LicenceProduct";
        }

        private void txtName_KeyDown(object sender, KeyEventArgs e)
        {
            //===================to accept only charactrs & space/backspace=============================================

            if (e.Key >= Key.A && e.Key <= Key.Z)
            {
            }
            else if (e.Key == Key.Space || e.Key == Key.Back || e.Key == Key.Tab)
            {
            }
            else
            {
                e.Handled = true;
            }
        }

        TextBlock GetLicenseProductTextBlock(string text, FontFamily fontFamily, double fontSize, Brush brush = null)
        {
            TextBlock txtBlock = new TextBlock() { Text = text };
            txtBlock.FontFamily = fontFamily;
            txtBlock.FontSize = fontSize;
            txtBlock.FontWeight = FontWeights.Normal;
            if (brush != null)
                txtBlock.Foreground = brush;
            return (txtBlock);
        }

        class Control
        {
            UIElement _uiElement;
            public UIElement UIElement { get { return (_uiElement); } }
            int _left, _top, _zIndex;
            public int Left { get { return (_left); } }
            public int Top { get { return (_top); } }
            public int ZIndex { get { return (_zIndex); } }
            public string UserInput
            {
                get
                {
                    if (_uiElement is TextBlock) return (((TextBlock)_uiElement).Text);
                    return (string.Empty);
                }
                set { if (_uiElement is TextBlock) ((TextBlock)_uiElement).Text = value; }
            }
            public Control(UIElement uiElement, int left, int top, int zIndex)
            {
                _uiElement = uiElement;
                _left = left;
                _top = top;
                _zIndex = zIndex;
            }
        }

    }

}
