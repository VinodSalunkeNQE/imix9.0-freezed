﻿using DigiPhoto.DataLayer;
//using DigiPhoto.DataLayer.Model;
using DigiPhoto.DigiSync.Model;
using DigiPhoto.IMIX.Business;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace DigiPhoto
{
    /// <summary>
    /// Interaction logic for ImportSpecProductImages.xaml
    /// </summary>
    public partial class ImportSpecProductImages : UserControl
    {
        public ImportSpecProductImages()
        {
            InitializeComponent();

          //  _objDataServices = new DigiPhotoDataServices();
            string ImgId = string.Empty;
            try
            {
                InitializeComponent();
                lstSelectedImage.Items.Clear();
                foreach (var item in RobotImageLoader.PrintImages)
                {
                    ImgId = ImgId + item.PhotoId + ",";
                    lstSelectedImage.Items.Add(item);
                }
            }
            catch (Exception ex)
            {

            }
        }

        #region Declarations
        /// <summary>
        /// The _hide request
        /// </summary>
        private bool _hideRequest = false;

        public int _photoId;
        /// <summary>
        /// The _result
        /// </summary>
        private List<IMIX.Model.PhotoInfo> _result;
        /// <summary>
        /// The _parent
        /// </summary>
        private UIElement _parent;
        public void SetParent(UIElement parent)
        {
            _parent = parent;
        }

        //DigiPhotoDataServices _objDataServices = null;

        public List<String> PreviousImage
        {
            get;
            set;
        }

        public bool IsSpecPrintOrderAcrossSites
        {
            get;
            set;
        }
        #endregion


        /// <summary>
        /// Shows the handler dialog.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <returns></returns>
        public List<IMIX.Model.PhotoInfo> ShowSpecImagesDialog(string message)
        {
            Visibility = Visibility.Visible;
            //Set Keyboard focus
            Dispatcher.BeginInvoke(DispatcherPriority.Input,
            new Action(delegate()
            {
                txtRFID.Focus();         // Set Logical Focus
                Keyboard.Focus(txtRFID); // Set Keyboard Focus
            }));


            _parent.IsEnabled = true;

            _hideRequest = false;
            while (!_hideRequest)
            {
                // HACK: Stop the thread if the application is about to close
                if (this.Dispatcher.HasShutdownStarted ||
                    this.Dispatcher.HasShutdownFinished)
                {
                    break;
                }
                // HACK: Simulate "DoEvents"
                this.Dispatcher.Invoke(
                    DispatcherPriority.Background,
                    new ThreadStart(delegate { }));
                Thread.Sleep(20);

            }

            return _result;
        }

        /// <summary>
        /// Hides the handler dialog.
        /// </summary>
        private void HideHandlerDialog()
        {
            _hideRequest = true;
            Visibility = Visibility.Collapsed;
            _parent.IsEnabled = true;
            KeyBorder1.Visibility = Visibility.Collapsed;
        }

        private void btnSearch_Click(object sender, RoutedEventArgs e)
        {
            string ImgId = string.Empty;
            lstSelectedImage.Items.Clear();
            
            List<IMIX.Model.PhotoInfo> LoadImages = new PhotoBusiness().GetPhotosBasedonRFID(Common.LoginUser.SubStoreId, txtRFID.Text.ToString(), IsSpecPrintOrderAcrossSites);
            if (LoadImages.Count > 1)
            {
                foreach (var item in LoadImages)
                {
                    // *** Code Added by Anis for Magic Shot Kitted on 26th Nov 18 ***//
                    //item.DG_Photos_FileName = System.IO.Path.Combine(Common.LoginUser.DigiFolderThumbnailPath, item.DG_Photos_CreatedOn.ToString("yyyyMMdd"), item.DG_Photos_FileName);
                    //lstSelectedImage.Items.Add(item);
                    if (item.DG_Photos_FileName.EndsWith(".jpg"))
                    {
                        LstMyItems _myitem = new LstMyItems();
                        _myitem.MediaType = item.DG_MediaType;
                        _myitem.FileName = item.DG_Photos_FileName;
                        _myitem.PhotoId = item.DG_Photos_pkey;
                        _myitem.CreatedOn = item.DG_Photos_CreatedOn;
                        _myitem.PhotoLocation = item.DG_Location_Id;
                        _myitem.SemiOrderProfileId = item.SemiOrderProfileId;
                        _myitem.BmpImageGroup = new BitmapImage(new Uri(@"/images/view-accept.png", UriKind.Relative));
                        _myitem.FilePath = System.IO.Path.Combine(Common.LoginUser.DigiFolderThumbnailPath, item.DG_Photos_CreatedOn.ToString("yyyyMMdd"), _myitem.FileName);
                        lstSelectedImage.Items.Add(_myitem);
                    }
                    else if (item.DG_Photos_FileName.EndsWith(".mp4"))
                    {
                        LstMyItems _myitemV = new LstMyItems();
                        _myitemV.MediaType = item.DG_MediaType;
                        _myitemV.FileName = item.DG_Photos_FileName;
                        var tempFileName = item.DG_Photos_FileName.Split('.');
                        var fileName = tempFileName[1].Replace("mp4", ".jpg");
                        _myitemV.PhotoId = item.DG_Photos_pkey;
                        _myitemV.PhotoLocation = item.DG_Location_Id;
                        _myitemV.SemiOrderProfileId = item.SemiOrderProfileId;
                        _myitemV.CreatedOn = item.DG_Photos_CreatedOn;
                        _myitemV.BmpImageGroup = new BitmapImage(new Uri(@"/images/view-accept.png", UriKind.Relative));
                        _myitemV.FilePath = System.IO.Path.Combine(Common.LoginUser.DigiFolderThumbnailPath, item.DG_Photos_CreatedOn.ToString("yyyyMMdd"), tempFileName[0] + fileName);
                        lstSelectedImage.Items.Add(_myitemV);
                    }
                    // *** End Here ***//
                }
                SetInitial();
            }
            else if(LoadImages.Count==1)
            {
                _result = new List<IMIX.Model.PhotoInfo>();
                _result.Add(LoadImages[0]);
                HideHandlerDialog();
            }
        }

        private void btnClose_Click(object sender, RoutedEventArgs e)
        {
            _result = null;
            HideHandlerDialog();
        }

        private void txtRFID_GotFocus(object sender, RoutedEventArgs e)
        {
            KeyBorder1.Visibility = Visibility.Visible;
        }

        private void btnSubmit_Click(object sender, RoutedEventArgs e)
        {
            int TotalSelectedImages = 0;

            _result = new List<IMIX.Model.PhotoInfo>();
            // *** Code Added by Anis for Magic Shot Kitted on 26th Nov 18 ***//
            List<LstMyItems> lstItem = new List<LstMyItems>();
            // *** End Here ***//

            for (int i = 0; i < lstSelectedImage.Items.Count; i++)
            {
                DependencyObject obj = lstSelectedImage.ItemContainerGenerator.ContainerFromIndex(i);
                if (obj != null)
                {
                    CheckBox rdo = FindVisualChild<CheckBox>(obj);// FindByName("rdoSelect", row) as RadioButton;
                    if (rdo != null)
                    {
                        if (Convert.ToBoolean(rdo.IsChecked))
                        {
                            // *** Code Added by Anis for Magic Shot Kitted on 26th Nov 18 ***//
                            //_result.Add((IMIX.Model.PhotoInfo)lstSelectedImage.Items[i]);
                            lstItem.Add((LstMyItems)lstSelectedImage.Items[i]);
                            // *** End Here *** //
                        }
                    }
                }
            }
            // *** Code Added by Anis for Magic Shot Kitted on 23rd Nov 18 ***//
            _result = GetPhotoInfo(lstItem);
            // *** End Here *** //
            HideHandlerDialog();
        }

        // *** Code Added by Anis for Magic Shot kitted on 26th Nov 18 ***//
        public List<IMIX.Model.PhotoInfo> GetPhotoInfo(List<LstMyItems> lstMyItems)
        {
            List<IMIX.Model.PhotoInfo> lstPhotoInfo = new List<IMIX.Model.PhotoInfo>();
            foreach (var item in lstMyItems)
            {
                IMIX.Model.PhotoInfo objPhotoInfo = new IMIX.Model.PhotoInfo();
                objPhotoInfo.DG_MediaType = item.MediaType;
                objPhotoInfo.DG_Photos_FilePath = item.FilePath;
                objPhotoInfo.DG_Photos_FileName = item.FileName;
                objPhotoInfo.DG_Photos_pkey = item.PhotoId;
                objPhotoInfo.DG_Location_Id = item.PhotoLocation;
                objPhotoInfo.SemiOrderProfileId = item.SemiOrderProfileId;
                lstPhotoInfo.Add(objPhotoInfo);
            }

            return lstPhotoInfo;

        }
        // *** End Here ***//

        public static childItem FindVisualChild<childItem>(DependencyObject obj)
  where childItem : DependencyObject
        {
            // Search immediate children
            for (int i = 0; i < VisualTreeHelper.GetChildrenCount(obj); i++)
            {
                DependencyObject child = VisualTreeHelper.GetChild(obj, i);

                if (child is childItem)
                    return (childItem)child;

                else
                {
                    childItem childOfChild = FindVisualChild<childItem>(child);

                    if (childOfChild != null)
                        return childOfChild;
                }
            }

            return null;
        }

        public void SetInitial()
        {
            for (int i = 0; i < lstSelectedImage.Items.Count; i++)
            {
                DependencyObject obj = lstSelectedImage.ItemContainerGenerator.ContainerFromIndex(i);
                if (obj != null)
                {
                    CheckBox chk = FindVisualChild<CheckBox>(obj);// FindByName("rdoSelect", row) as RadioButton;
                    if (chk != null)
                    {
                        chk.IsChecked = false;
                    }
                }
            }

            for (int i = 0; i < lstSelectedImage.Items.Count; i++)
            {
                DependencyObject obj = lstSelectedImage.ItemContainerGenerator.ContainerFromIndex(i);
                if (obj != null)
                {
                    CheckBox chk = FindVisualChild<CheckBox>(obj);// FindByName("rdoSelect", row) as RadioButton;
                    if (chk != null)
                    {
                        if (PreviousImage != null)
                        {
                            var item = (from objSelected in PreviousImage
                                        where objSelected == chk.Uid.ToString()
                                        select objSelected).FirstOrDefault();

                            if (item != null)
                            {
                                chk.IsChecked = true;
                            }
                        }
                    }

                }
            }
        }

        private void btn_Click_keyboard(object sender, RoutedEventArgs e)
        {
            try
            {

                Button _objbtn = new Button();
                _objbtn = (Button)sender;
                switch (_objbtn.Content.ToString())
                {
                    case "ENTER":
                        {
                            KeyBorder1.Visibility = Visibility.Collapsed;
                            break;
                        }
                    case "SPACE":
                        {
                            txtRFID.Text = txtRFID.Text + " ";
                            break;
                        }
                    case "CLOSE":
                        {
                            KeyBorder1.Visibility = Visibility.Collapsed;
                            break;
                        }
                    case "Back":
                        {
                            if (txtRFID.Text.Length > 0)
                                txtRFID.Text = txtRFID.Text.Remove(txtRFID.Text.Length - 1, 1);
                            break;
                        }
                    default:
                        {
                            txtRFID.Text = txtRFID.Text + _objbtn.Content;
                        }
                        break;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }
    }
}
