﻿using DigiPhoto.Interop;
using DigiPhoto.Manage;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace DigiPhoto
{
    /// <summary>
    /// Interaction logic for Logo.xaml
    /// </summary>
    public partial class LogoControl : UserControl
    {
        private UIElement _parent;
        public int Left { get; set; }
        public int Top { get; set; }
        private bool _result = false;
        public int videoExpLen = 0;
        public List<SlotProperty> LogoSlotList = new List<SlotProperty>();
        public int uniqueID = 51;
        List<TemplateListItems> lstGraphics = new List<TemplateListItems>();
        public LogoControl()
        {
            InitializeComponent();
            Visibility = Visibility.Hidden;
            this.fontDialog.Color = System.Drawing.Color.White;
            this.fontDialog.Font = new System.Drawing.Font("Arial", 32F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.fontDialog.ShowColor = true;
            cmbCheckedGraphic.SelectedIndex = 0;
        }
        public void SetParent(UIElement parent)
        {
            _parent = (UIElement)parent;
        }
        private void btnClose_Click(object sender, RoutedEventArgs e)
        {
            HideHandlerDialog();
            OnExecuteMethod();
        }
        private void HideHandlerDialog()
        {
            Visibility = Visibility.Collapsed;
            _parent.IsEnabled = true;
        }
        public void ControlListAutoFill()
        {
            DGManageLogo.ItemsSource = LogoSlotList;
            DGManageLogo.Items.Refresh();
        }
        public bool ShowPanHandlerDialog()
        {
            try
            {
                Visibility = Visibility.Visible;
                _parent.IsEnabled = false;
                lstGraphics = ((DigiPhoto.Manage.VideoEditor)Window.GetWindow(_parent)).objTemplateList.Where(o => o.MediaType == 606 && o.IsChecked == true).ToList();
                if (lstGraphics.Count == 0)
                    chkGraphicLogo.IsChecked = false;
                cmbCheckedGraphic.ItemsSource = lstGraphics;
                txtStopTime.Text = videoExpLen.ToString();
                txtGraphicLogoTop.Text = lstGraphics.FirstOrDefault().TopPositon.ToString();
                txtGraphicLogoLeft.Text = lstGraphics.FirstOrDefault().LeftPositon.ToString();
                if (videoExpLen > 0)
                {
                    sldRange.Maximum = videoExpLen;
                }
                DGManageLogo.ItemsSource = null;
                DGManageLogo.ItemsSource = LogoSlotList;
                return _result;
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                return _result;
            }
        }
        public event EventHandler ExecuteMethod;
        protected virtual void OnExecuteMethod()
        {
            if (ExecuteMethod != null) ExecuteMethod(this, EventArgs.Empty);
        }

        public System.Windows.Forms.FontDialog fontDialog = new System.Windows.Forms.FontDialog();
        private void btFont_Click(object sender, RoutedEventArgs e)
        {
            if (fontDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                OnExecuteMethod();
            }
        }


        private void txtnumber_TextChanged(object sender, TextChangedEventArgs e)
        {
            string a = "";
            if (string.IsNullOrEmpty(((TextBox)sender).Text))

                a = "";
            else
            {
                int num = 0;
                bool success = int.TryParse(((TextBox)sender).Text, out num);
                if (success & num >= 0)
                {
                    ((TextBox)sender).Text.Trim();
                    a = ((TextBox)sender).Text;
                }
                else
                {
                    ((TextBox)sender).Text = a;
                    ((TextBox)sender).SelectionStart = ((TextBox)sender).Text.Length;
                }
            }
        }

        long EditedItemId = 0;
        bool IsEdited = false;
        //private void btnAdd_Click(object sender, RoutedEventArgs e)
        //{
        //    try
        //    {  
        //        SlotProperty logoSlot=null;
        //        if (IsEdited)
        //        {
        //            MessageBoxResult response = MessageBox.Show("Do you want to save changes?", "Digiphoto", MessageBoxButton.YesNo, MessageBoxImage.Question);

        //            if (response == MessageBoxResult.Yes)
        //            {
        //                logoSlot = LogoSlotList.Where(o => o.ID == EditedItemId).FirstOrDefault();
        //               // LogoSlotList.RemoveAll(o => o.ID == EditedItemId);
        //                EditedItemId = 0;
        //            }
        //            else
        //            {
        //                cmbCheckedGraphic.SelectedIndex = 0;
        //                txtStartTime.Text = "0";
        //                txtStopTime.Text = "0";
        //                txtGraphicLogoLeft.Text = "0";
        //                txtGraphicLogoTop.Text = "0";
        //                return;
        //            }
        //        }
        //        else
        //        {
        //            logoSlot = new SlotProperty();
        //        }

        //        int stopTime = 0;
        //        int startTime = 0;
        //        if (!string.IsNullOrEmpty(txtStartTime.Text))
        //        {
        //            startTime = Convert.ToInt32(txtStartTime.Text);
        //        }
        //        if (!string.IsNullOrEmpty(txtStopTime.Text))
        //        {
        //            stopTime = Convert.ToInt32(txtStopTime.Text);
        //        }
        //        if (chkGraphicLogo.IsChecked == true)
        //        {
        //            if (CheckSlotValidations())
        //            {
        //                bool added = false;
        //                long id = ((TemplateListItems)(cmbCheckedGraphic.SelectionBoxItem)).Item_ID;
        //                logoSlot.ID = uniqueID;
        //                logoSlot.ItemID = id;
        //                logoSlot.FilePath = ((TemplateListItems)(cmbCheckedGraphic.SelectionBoxItem)).FilePath;
        //                logoSlot.StartTime = Convert.ToInt32(txtStartTime.Text);
        //                logoSlot.StopTime = Convert.ToInt32(txtStopTime.Text);
        //                logoSlot.Left = Convert.ToInt32(txtGraphicLogoLeft.Text);
        //                logoSlot.Top = Convert.ToInt32(txtGraphicLogoTop.Text);
        //                if (!IsEdited)
        //                {
        //                    List<SlotProperty> lst = LogoSlotList.Where(o => o.ItemID == id).OrderBy(o => o.StartTime).ToList();
        //                    if (lst != null && lst.Count > 0)
        //                    {
        //                        SlotProperty lsptemp = lst.Where(o => o.StartTime >= startTime).FirstOrDefault();
        //                        if (lsptemp != null)
        //                        {
        //                            if (stopTime <= lsptemp.StopTime)
        //                            {
        //                                MessageBox.Show("Selected graphic is already applied in the given time frame.", "DigiPhoto", MessageBoxButton.OK, MessageBoxImage.Exclamation);
        //                            }
        //                            else
        //                            {
        //                                LogoSlotList.Add(logoSlot);
        //                                added = true;
        //                            }
        //                        }
        //                        else
        //                        {
        //                            LogoSlotList.Add(logoSlot);
        //                            added = true;
        //                        }
        //                    }
        //                    else
        //                    {
        //                        LogoSlotList.Add(logoSlot);
        //                        added = true;
        //                    }
        //                    uniqueID++;
        //                }
        //                DGManageLogo.ItemsSource = LogoSlotList.OrderBy(o => o.StartTime);
        //                DGManageLogo.Items.Refresh();

        //            }
        //        }
        //        else
        //        {
        //            MessageBox.Show("Please check Apply graphic logo.", "DigiPhoto", MessageBoxButton.OK, MessageBoxImage.Information);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
        //        ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
        //    }
        //}
        private void btnAdd_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (cmbCheckedGraphic.Items.Count <= 0)
                {
                    MessageBox.Show("Please upload graphics.", "Digiphoto", MessageBoxButton.OK, MessageBoxImage.Information);
                    return;
                }
                SlotProperty logoSlot = new SlotProperty();
                VideoElements videoElement = new VideoElements();
                int stopTime = 0;
                int startTime = 0;

                if (!string.IsNullOrEmpty(txtStartTime.Text))
                {
                    startTime = Convert.ToInt32(txtStartTime.Text);
                }
                if (!string.IsNullOrEmpty(txtStopTime.Text))
                {
                    stopTime = Convert.ToInt32(txtStopTime.Text);
                }

                long id = ((TemplateListItems)(cmbCheckedGraphic.SelectionBoxItem)).Item_ID;

                if (CheckSlotValidations())
                {
                    if (IsEdited)
                    {
                        MessageBoxResult response = MessageBox.Show("Do you want to save changes?", "Digiphoto", MessageBoxButton.YesNo, MessageBoxImage.Question);
                        IsEdited = false;
                        if (response == MessageBoxResult.Yes)
                        {

                            logoSlot = LogoSlotList.Where(o => o.ID == EditedItemId).FirstOrDefault();
                            LogoSlotList.Remove(logoSlot);
                            if (videoElement.IsValidSlot(LogoSlotList, startTime, stopTime, id))
                            {

                                logoSlot.ItemID = id;
                                logoSlot.ID = Convert.ToInt32(EditedItemId);
                                logoSlot.FilePath = ((TemplateListItems)(cmbCheckedGraphic.SelectionBoxItem)).FilePath;
                                logoSlot.StartTime = Convert.ToInt32(txtStartTime.Text);
                                logoSlot.StopTime = Convert.ToInt32(txtStopTime.Text);
                                logoSlot.Left = Convert.ToInt32(txtGraphicLogoLeft.Text);
                                logoSlot.Top = Convert.ToInt32(txtGraphicLogoTop.Text);
                                LogoSlotList.Add(logoSlot);
                                EditedItemId = 0;
                            }
                            else
                                LogoSlotList.Add(logoSlot);
                        }
                        else
                        {
                            cmbCheckedGraphic.SelectedIndex = 0;
                            txtStartTime.Text = "0";
                            txtStopTime.Text = "0";
                            txtGraphicLogoLeft.Text = "0";
                            txtGraphicLogoTop.Text = "0";
                            return;
                        }
                    }
                    else
                    {
                        if (chkGraphicLogo.IsChecked == true)
                        {
                            if (videoElement.IsValidSlot(LogoSlotList, startTime, stopTime, id))
                            {
                                logoSlot.ID = uniqueID;
                                logoSlot.ItemID = id;
                                logoSlot.FilePath = ((TemplateListItems)(cmbCheckedGraphic.SelectionBoxItem)).FilePath;
                                logoSlot.StartTime = Convert.ToInt32(txtStartTime.Text);
                                logoSlot.StopTime = Convert.ToInt32(txtStopTime.Text);
                                logoSlot.Left = Convert.ToInt32(txtGraphicLogoLeft.Text);
                                logoSlot.Top = Convert.ToInt32(txtGraphicLogoTop.Text);
                                LogoSlotList.Add(logoSlot);
                                uniqueID++;
                            }
                        }
                        else
                        {
                            MessageBox.Show("Please check Apply graphic logo.", "DigiPhoto", MessageBoxButton.OK, MessageBoxImage.Information);
                        }
                    }
                    DGManageLogo.ItemsSource = LogoSlotList.OrderBy(o => o.StartTime);
                    DGManageLogo.Items.Refresh();
                }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }

        private bool CheckSlotValidations()
        {
            if (string.IsNullOrEmpty(txtStartTime.Text.Trim()) || string.IsNullOrEmpty(txtStopTime.Text.Trim()))
            {
                MessageBox.Show("Start/End time can not be zero or null.", "DigiPhoto", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                return false;
            }
            else if (Convert.ToInt32(txtStartTime.Text.Trim()) >= Convert.ToInt32(txtStopTime.Text.Trim()))
            {
                MessageBox.Show("Start time should be less then End time.", "DigiPhoto", MessageBoxButton.OK, MessageBoxImage.Information);
                return false;
            }

            else if (string.IsNullOrEmpty(txtGraphicLogoLeft.Text.Trim()) || string.IsNullOrEmpty(txtGraphicLogoTop.Text.Trim()))
            {
                MessageBox.Show("Top/Left value can not be zero or null.", "DigiPhoto", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                return false;
            }
            else if (Convert.ToInt32(txtGraphicLogoLeft.Text.Trim()) > Top || Convert.ToInt32(txtGraphicLogoTop.Text.Trim()) > Left)
            {
                MessageBox.Show("Top/Left value should be as per aspect ratio [" + Left + " X " + Top + "].", "DigiPhoto", MessageBoxButton.OK, MessageBoxImage.Information);
                return false;
            }
            else
            {
                return true;
            }
        }
        private void btnClearList_Click(object sender, RoutedEventArgs e)
        {
            if (LogoSlotList != null)
            {
                LogoSlotList.Clear();
                DGManageLogo.ItemsSource = LogoSlotList;
                DGManageLogo.Items.Refresh();
            }
        }
        private void btnDeleteSlots_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Button btnSender = (Button)sender;
                long SlotId = long.Parse(btnSender.Tag.ToString());
                MessageBoxResult response = MessageBox.Show("Do you want to delete record?", "Digiphoto", MessageBoxButton.YesNo, MessageBoxImage.Question);
                if (response == MessageBoxResult.Yes)
                {
                    SlotProperty brdlst = LogoSlotList.Where(o => o.ID == SlotId).FirstOrDefault();
                    long id = (LogoSlotList.Where(o => o.ID == SlotId).FirstOrDefault()).ItemID;
                    LogoSlotList.RemoveAll(o => o.ID == SlotId);
                    DGManageLogo.ItemsSource = LogoSlotList;
                    DGManageLogo.Items.Refresh();
                    if (LogoSlotList.Count == 0)
                    {
                        uniqueID = 51;
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }

        private void cmbCheckedGraphic_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (cmbCheckedGraphic.SelectedIndex >= 0 && cmbCheckedGraphic.SelectionBoxItem != "")
            {
                txtGraphicLogoLeft.Text = ((TemplateListItems)(cmbCheckedGraphic.SelectionBoxItem)).LeftPositon.ToString();
                txtGraphicLogoTop.Text = ((TemplateListItems)(cmbCheckedGraphic.SelectionBoxItem)).TopPositon.ToString();
            }
        }

        private void btnEdit_Click(object sender, RoutedEventArgs e)
        {
            Button btnSender = (Button)sender;
            EditedItemId = long.Parse(btnSender.Tag.ToString());
            IsEdited = true;
            SlotProperty brdlst = LogoSlotList.Where(o => o.ID == EditedItemId).FirstOrDefault();
            long id = (LogoSlotList.Where(o => o.ID == EditedItemId).FirstOrDefault()).ItemID;
            cmbCheckedGraphic.SelectedItem = null;
            TemplateListItems lstItem = lstGraphics.Where(o => o.Item_ID == id).FirstOrDefault();
            cmbCheckedGraphic.SelectedItem = lstItem;
            txtGraphicLogoLeft.Text = brdlst.Left.ToString();
            txtGraphicLogoTop.Text = brdlst.Top.ToString();
            txtStartTime.Text = brdlst.StartTime.ToString();
            txtStopTime.Text = brdlst.StopTime.ToString();

        }

    }
}
