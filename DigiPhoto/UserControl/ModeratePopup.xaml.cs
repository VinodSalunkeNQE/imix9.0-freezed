﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using System.Threading;
using DigiPhoto.Common;

namespace DigiPhoto
{
    public partial class ModalDialog : UserControl,IDisposable
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ModalDialog"/> class.
        /// </summary>
        public ModalDialog()
        {
            InitializeComponent();
            Visibility = Visibility.Hidden;
        }

        public void Dispose()
        {
            Window wnd = Window.GetWindow(this);
            if (wnd != null)
            {
                ((SearchResult)wnd).grdCotrol.Children.Remove(this);
                ((SearchResult)wnd).ModalDialog = null;
            }
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// The _hide request
        /// </summary>
        private bool _hideRequest = false;
        /// <summary>
        /// The _result
        /// </summary>
        private bool _result = false;
        /// <summary>
        /// The _parent
        /// </summary>
        private UIElement _parent;

        /// <summary>
        /// Sets the parent.
        /// </summary>
        /// <param name="parent">The parent.</param>
        public void SetParent(UIElement parent)
        {
            _parent = parent;
        }

        #region Message

        /// <summary>
        /// Gets or sets the message.
        /// </summary>
        /// <value>
        /// The message.
        /// </value>
        public string Message
        {
            get { return (string)GetValue(MessageProperty); }
            set { SetValue(MessageProperty, value); }
        }

        // Using a DependencyProperty as the backing store for Message.
        // This enables animation, styling, binding, etc...
        /// <summary>
        /// The message property
        /// </summary>
        public static readonly DependencyProperty MessageProperty =
            DependencyProperty.Register(
                "Message", typeof(string), typeof(ModalDialog), new UIPropertyMetadata(string.Empty));

        #endregion

        /// <summary>
        /// Shows the handler dialog.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <returns></returns>
        public bool ShowHandlerDialog(string message)
        {
            Message = message;
            Visibility = Visibility.Visible;
            txtPassword.Focus();
            txtPassword.Password = "";
            _parent.IsEnabled = false;

            _hideRequest = false;
            while (!_hideRequest)
            {
                // HACK: Stop the thread if the application is about to close
                if (this.Dispatcher.HasShutdownStarted ||
                    this.Dispatcher.HasShutdownFinished)
                {
                    break;
                }
                // HACK: Simulate "DoEvents"
                this.Dispatcher.Invoke(
                    DispatcherPriority.Background,
                    new ThreadStart(delegate { }));
                Thread.Sleep(20);
            }

            return _result;
        }

        /// <summary>
        /// Hides the handler dialog.
        /// </summary>
        private void HideHandlerDialog()
        {
            _hideRequest = true;
            Visibility = Visibility.Collapsed;
            _parent.IsEnabled = true;
            //Remove object and dispose it
        }

        /// <summary>
        /// Handles the Click event of the OkButton control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void OkButton_Click(object sender, RoutedEventArgs e)
        {
            PasswordValidate();
            
        }

        /// <summary>
        /// Handles the Click event of the CancelButton control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            txtPassword.Password = "";
            txtPassword.Focus();
            txbMessage.Text = "";
        }

        /// <summary>
        /// Handles the Click event of the btnClose control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void btnClose_Click(object sender, RoutedEventArgs e)
        {
            _result = false;
            HideHandlerDialog();
        }

        /// <summary>
        /// Handles the KeyDown event of the txtPassword control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="KeyEventArgs"/> instance containing the event data.</param>
        private void txtPassword_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                PasswordValidate();
            }
        }
        #region Common Methods
        /// <summary>
        /// Passwords the validate.
        /// </summary>
        private void PasswordValidate()
        {
            if (string.IsNullOrEmpty(txtPassword.Password))
            {
                txbMessage.Text = "Please enter a password.";
                txtPassword.Focus();
            }
            else
            {
                if (txtPassword.Password == LoginUser.ModPassword)
                {
                    _result = true;
                    txbMessage.Text = "";
                    HideHandlerDialog();
                }
                else
                {
                    txbMessage.Text = "Please enter a valid password.";
                    txtPassword.Password = "";
                    txtPassword.Focus();

                }
            }
        }
        #endregion

        /// <summary>
        /// Handles the Checked event of the rdbTemporarily control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void rdbTemporarily_Checked(object sender, RoutedEventArgs e)
        {
            try
            {
                txtPassword.Focus();
            }
            catch (Exception ex)
            {
            }
        }

        /// <summary>
        /// Handles the Checked event of the rdbPermanantly control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void rdbPermanantly_Checked(object sender, RoutedEventArgs e)
        {
            txtPassword.Focus();
        }
    }
}
