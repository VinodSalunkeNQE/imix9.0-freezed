﻿using DigiPhoto.Common;
using DigiPhoto.IMIX.Business;
using DigiPhoto.IMIX.Model;
using FrameworkHelper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace DigiPhoto
{
    /// <summary>
    /// Interaction logic for CtrlOpeningForm.xaml
    /// </summary>
    public partial class CtrlOpeningForm : UserControl
    {
        public CtrlOpeningForm()
        {
            InitializeComponent();
           
            Visibility = Visibility.Hidden;
            MonthlyCalendar.DisplayDateStart = DateTime.Now;
            MonthlyCalendar.DisplayDateEnd = DateTime.Now.AddDays(1);
            
        }
        //public void fillGrid()
        //{
        //    // List<AssociatedPrintersInfo> GetAssociatedPrintersName(LoginUser.SubStoreId);
        //    List<AssociatedPrintersInfo> lstprinters = (new PrinterBusniess()).GetAssociatedPrintersName(LoginUser.SubStoreId);

        //    grdPrintCount.ItemsSource = lstprinters;
        //    grdPrintCount.UpdateLayout();
        //    // grdPrintCount.
        //}
        /// <summary>
        /// The _parent
        /// </summary>
        private UIElement _parent;
        /// <summary>
        /// The _hide request
        /// </summary>
        private bool _hideRequest = false;
        /// <summary>
        /// 

        /// The _result
        /// </summary>
        private string _result;
        /// <summary>
        /// The controlon
        /// </summary>
        TextBox controlon;
        /// <summary>
        /// Sets the parent.
        /// </summary>
        /// <param name="parent">The parent.</param>
        public void SetParent(UIElement parent)
        {
            _parent = (UIElement)parent;

        }




        #region Message
        public DateTime FromDate { get; set; }
        public bool IsPermission { get; set; }
        public Int64 printAutoStart6850 { get; set; }
        public Int64 printAutoStart8810 { get; set; }
        public Int64 printAutoStart6900 { get; set; }
        /// <summary>
        /// Gets or sets the message card payment.
        /// </summary>
        /// <value>
        /// The message card payment.
        /// </value>
        public string MessageOpeningForm
        {
            get { return (string)GetValue(MessageOpeningFormProperty); }
            set { SetValue(MessageOpeningFormProperty, value); }
        }

        // Using a DependencyProperty as the backing store for Message.
        // This enables animation, styling, binding, etc...
        /// <summary>
        /// The message card payment property
        /// </summary>
        public static readonly DependencyProperty MessageOpeningFormProperty =
            DependencyProperty.Register(
                "MessageOpeningForm", typeof(string), typeof(ModalDialog), new UIPropertyMetadata(string.Empty));



        /// <summary>
        /// Shows the handler dialog.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <returns></returns>
        public string ShowHandlerDialog(string message)
        {
            MessageOpeningForm = message;
            Visibility = Visibility.Visible;
            // fillGrid();
            txt6X8StarNumber.Focus();
            //txtHeader.Text = "Opening Form for " + ((FromDate != DateTime.MinValue) ? string.Format("{0:dd-MMM-yyyy}", FromDate) : string.Format("{0:dd-MMM-yyyy}", DateTime.Now)); 
            //_parent.IsEnabled = false;
            txt6X8AutoStarNumber.Text = printAutoStart6850.ToString();
            txt8X10AutoStarNumber.Text = printAutoStart8810.ToString();
            txt6900AutoStartingNumber.Text = printAutoStart6900.ToString();


            _hideRequest = false;
            while (!_hideRequest)
            {
                // HACK: Stop the thread if the application is about to close
                if (this.Dispatcher.HasShutdownStarted ||
                    this.Dispatcher.HasShutdownFinished)
                {
                    break;
                }
                // HACK: Simulate "DoEvents"
                this.Dispatcher.Invoke(
                    DispatcherPriority.Background,
                    new ThreadStart(delegate { }));
                Thread.Sleep(20);
            }

            return _result;
        }

        /// <summary>
        /// Hides the handler dialog.
        /// </summary>
        private void HideHandlerDialog()
        {
            //  _result = null;
            _hideRequest = true;
           Visibility = Visibility.Collapsed;
            //_parent.IsEnabled = true;  -Commented By Bhagyashree dhobale fot Restrict 
           
        }
        #endregion

        private void PastingHandler(object sender, DataObjectPastingEventArgs e)
        {
            if (e.DataObject.GetDataPresent(typeof(String)))
            {
                String text = (String)e.DataObject.GetData(typeof(String));
                if (!IsTextAllowed(text)) e.CancelCommand();
            }
            else e.CancelCommand();
        }

        private static bool IsTextAllowed(string text)
        {
            Regex regex = new Regex("[^0-9]+"); //regex that matches disallowed text
            return !regex.IsMatch(text);
        }
        private void txtAmountEntered_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = !IsTextAllowed(e.Text);

        }
        /// <summary>
        /// Handles the Click event of the btnSubmit control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void btnSubmit_Click(object sender, RoutedEventArgs e)
        {
            //txt6X8StarNumber.Text = String.Empty;
            //txt8X10StarNumber.Text = String.Empty;
            //txtCashFloat.Text = String.Empty;
            _result = ""; //string.Empty;

            if (txt6X8StarNumber.Text == string.Empty)
            {
                //Validation.Text = CommonUtility.getRequiredMessage(UIConstant.CardType);
                MessageBox.Show("Please enter 6X8 printer starting number", "DEI");
                txt6X8StarNumber.Focus();
                txt6X8StarNumber.BorderBrush = Brushes.Red;
            }
            else if (txt8X10StarNumber.Text == string.Empty)
            {
                //here i will put the code  to get from the db
                //foreach (System.Data.DataRowView dr in grdPrintCount.ItemsSource)
                //{
                //   //DataGridRow row = (DataGridRow)dataGrid.ItemContainerGenerator.ContainerFromItem(rv);
                //    TextBlock tx = FindByName("txtAmountEntered", dr) as TextBlock;
                //}

                //DataGridRow row = (DataGridRow)dgPayment.ItemContainerGenerator.ContainerFromIndex(counter);
                //if (row != null)
                //{
                //    double amount;
                //    TextBlock tx = FindByName("txtAmountEntered", row) as TextBlock;
                //    TextBlock tx1 = FindByName("txtNetPrice", row) as TextBlock;
                //    TextBlock tx2 = FindByName("txtCurrencyID", row) as TextBlock;

                //    if (Double.TryParse(tx1.Text, out amount))
                //    {
                //        paymentDetails += "<Payment Mode = 'cash' Amount = '" + tx1.Text.ToString() + "' OrignalAmount = '" + item.PaidAmount.ToString() + "' CurrencyID = '" + tx2.Text + "' CurrencySyncCode = '" + item.CurrencySyncCode + "'/>";
                //    }
                //}
                //counter++;
                //Validation.Text = CommonUtility.getRequiredMessage(UIConstant.CustomerName);
                // int counter = 0;
                //string value = "";        
                //if (grdPrintCount.Items.Count != 0)
                //{
                //   int abc=grdPrintCount.Items.Count;
                //    for (int i=0;i<abc;i++)
                //    {
                //        DataGridRow row = (DataGridRow)grdPrintCount.ItemContainerGenerator.ContainerFromIndex(i);

                //        if (row != null)
                //        {

                //            Int32 Printerid = ((DigiPhoto.IMIX.Model.AssociatedPrintersInfo)(row.Item)).DG_AssociatedPrinters_Pkey;
                //           // grdPrintCount[1, e.RowIndex].Value
                //            TextBox tx = FindByName("txtCount", row) as TextBox;

                //           // DataGridTextColumn hdnPrinterPkey = FindByName("hdnPrinterPkey", row) as DataGridTextColumn;
                //            _result = _result + tx.Text + "@@@" + Printerid + "%##%";
                //        }
                //        //counter++;
                //    }
                //}

                //_result = _result.TrimEnd(_result[_result.Length - 4]);// txt6X8StarNumber.Text.ToString() + "%##%" + txt8X10StarNumber.Text + "%##%" + txtCashFloat.Text;
                //HideHandlerDialog();
                MessageBox.Show("Please enter 8X10 printer starting number", "DEI");
                txt8X10StarNumber.Focus();
                txt8X10StarNumber.BorderBrush = Brushes.Red;
            }
            else if (txtPosterStarNumber.Text == string.Empty)
            {
                //Validation.Text = CommonUtility.getRequiredMessage(UIConstant.CardNumber);
                MessageBox.Show("Please enter poster printer starting number", "DEI");
                txtPosterStarNumber.Focus();
                txtPosterStarNumber.BorderBrush = Brushes.Red;
            }

            else if (txt8by20StartingNumber.Text == string.Empty)
            {
                //Validation.Text = CommonUtility.getRequiredMessage(UIConstant.CardNumber);
                MessageBox.Show("Please enter 8 x 20 starting number", "DEI");
                txt8by20StartingNumber.Focus();
                txt8by20StartingNumber.BorderBrush = Brushes.Red;
            }
            else if (txt6by20StartingNumber.Text == string.Empty)
            {
                //Validation.Text = CommonUtility.getRequiredMessage(UIConstant.CardNumber);
                MessageBox.Show("Please enter 6 x 20 starting number", "DEI");
                txt6by20StartingNumber.Focus();
                txt6by20StartingNumber.BorderBrush = Brushes.Red;
            }


            else if (txtCashFloat.Text == string.Empty)
            {
                //Validation.Text = CommonUtility.getRequiredMessage(UIConstant.CardNumber);
                MessageBox.Show("Please enter cash float amount", "DEI");
                txtCashFloat.Focus();
                txtCashFloat.BorderBrush = Brushes.Red;
            }
            else
             {
                DateTime OpeningFormDate = MonthlyCalendar.SelectedDate.Value;
                OpeningFormDate = OpeningFormDate.AddSeconds(DateTime.Now.TimeOfDay.TotalSeconds);
                //here i will check that already filled or not for choosed date 

                // By KCB ON 27 MAR 2018 FOR implementation NoOfTransaction field
                //_result = txt6X8StarNumber.Text.ToString() + "%##%" + txt8X10StarNumber.Text + "%##%" + txtPosterStarNumber.Text + "%##%" + txtCashFloat.Text + "%##%" + MonthlyCalendar.SelectedDate.ToString();
                _result = txt6X8StarNumber.Text.ToString() + "%##%" + txt8X10StarNumber.Text + "%##%" + txt6by20StartingNumber.Text.ToString() + "%##%" + txt8by20StartingNumber.Text.ToString() + "%##%" + txtPosterStarNumber.Text + "%##%" + txtCashFloat.Text + "%##%" + MonthlyCalendar.SelectedDate.ToString() + "%##%" + (string.IsNullOrEmpty(txt6900StartingNumber.Text.Trim()) == false ? txt6900StartingNumber.Text.Trim():"0");
                HideHandlerDialog();
                //end
            }
        }

        private FrameworkElement FindByName(string name, FrameworkElement root)
        {
            Stack<FrameworkElement> tree = new Stack<FrameworkElement>();
            tree.Push(root);
            while (tree.Count > 0)
            {
                FrameworkElement current = tree.Pop(); // root is null
                if (current.Name == name)
                    return current;

                int count = VisualTreeHelper.GetChildrenCount(current);
                for (int SupplierCounter = 0; SupplierCounter < count; ++SupplierCounter)
                {
                    DependencyObject child = VisualTreeHelper.GetChild(current, SupplierCounter);
                    if (child is FrameworkElement)
                        tree.Push((FrameworkElement)child);
                }
            }
            return null;
        }
        /// <summary>
        /// Handles the Click event of the btnSubmitCancel control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void btnSubmitCancel_Click(object sender, RoutedEventArgs e)
        {
            //cmbCardType.SelectedIndex = 0;
            txt6X8StarNumber.Text = String.Empty;
            txt8X10StarNumber.Text = String.Empty;
            txtCashFloat.Text = String.Empty;
            txtPosterStarNumber.Text = string.Empty;
            // cmbCardType.Focus();
            _result = string.Empty;
        }

        /// <summary>
        /// Handles the Click event of the btnClose control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void btnClose_Click(object sender, RoutedEventArgs e)
        {
            _result = string.Empty;
            HideHandlerDialog();
        }



        /// <summary>
        /// Pastings the handler.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="DataObjectPastingEventArgs"/> instance containing the event data.</param>
        //private void PastingHandler(object sender, DataObjectPastingEventArgs e)
        //{
        //    if (e.DataObject.GetDataPresent(typeof(String)))
        //    {
        //        String text = (String)e.DataObject.GetData(typeof(String));
        //        if (!IsTextAllowed(text)) e.CancelCommand();
        //    }
        //    else e.CancelCommand();
        //}

        /// <summary>
        /// Handles the Loaded event of the UserControl control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            txtCashFloat.Focus();
        }

        private void txtCashFloat_GotFocus(object sender, RoutedEventArgs e)
        {
            controlon = (TextBox)sender;
            //KeyBorder.Visibility = Visibility.Visible;
        }

        //private void btn_Click(object sender, RoutedEventArgs e)
        //{
        //    Button _objbtn = (Button)sender;
        //    //_objbtn = (Button)sender;
        //    switch (_objbtn.Content.ToString())
        //    {
        //        case "ENTER":
        //            {
        //                KeyBorder.Visibility = Visibility.Hidden;
        //                break;
        //            }
        //        //case "SPACE":
        //        //    {
        //        //        controlon.Text = controlon.Text + " ";
        //        //        break;
        //        //    }
        //        case "CLOSE":
        //            {
        //                KeyBorder.Visibility = Visibility.Hidden;
        //                break;
        //            }
        //        case "Back":
        //            {
        //                TextBox objtxt = (TextBox)(controlon);
        //                if (controlon.Text.Length > 0)
        //                {
        //                    controlon.Text = controlon.Text.Remove(controlon.Text.Length - 1, 1);
        //                }
        //                break;
        //            }
        //        default:
        //            {
        //                if (controlon.Text.Count() < 10)
        //                {
        //                    string[] numbers = { "0", "1", "2", "3", "4", "5", "6", "7", "8", "9","." };
        //                    //var b = Array.contains(a, "red");
        //                    int pos = Array.IndexOf(numbers, _objbtn.Content);
        //                    if (pos > -1)
        //                    {
        //                         //int count = (controlon.Text + _objbtn.Content).Split('.').Length;
        //                         string[] afterDecimal = (controlon.Text + _objbtn.Content).Split('.');
        //                         if (afterDecimal.Length == 2 && afterDecimal[1].Length <= 2)
        //                         {
        //                             controlon.Text = controlon.Text + _objbtn.Content;
        //                         }else if (afterDecimal.Length < 2)
        //                         {
        //                             controlon.Text = controlon.Text + _objbtn.Content;
        //                         }
        //                    }
        //                }
        //            }
        //            break;
        //    }
        //}
        private void txt6X8StarNumber_GotFocus(object sender, RoutedEventArgs e)
        {
            controlon = (TextBox)sender;
            //  KeyBorder.Visibility = Visibility.Visible;
            //if (txt6X8StarNumber.Text == "")
            //    txt6X8StarNumber.BorderBrush = Brushes.Red;
        }

        private void txt8X10StarNumber_GotFocus(object sender, RoutedEventArgs e)
        {
            controlon = (TextBox)sender;
            //KeyBorder.Visibility = Visibility.Visible;

        }

        private void txtPosterStarNumber_GotFocus(object sender, RoutedEventArgs e)
        {
            controlon = (TextBox)sender;
            //KeyBorder.Visibility = Visibility.Visible;            
        }

        private void txt6X8StarNumber_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Space)
                e.Handled = true;
            else if (e.Key == Key.Decimal)
                e.Handled = !IsTextAllowed(".");
        }

        private void txt8X10StarNumber_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Space)
                e.Handled = true;
            else if (e.Key == Key.Decimal)
                e.Handled = !IsTextAllowed(".");
        }

        private void txtPosterStarNumber_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Space)
                e.Handled = true;
            else if (e.Key == Key.Decimal)
                e.Handled = !IsTextAllowed(".");
        }

        private void txtCashFloat_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Space)
                e.Handled = true;
        }
        #region Added by ajay for the panorama printer

        private void txt8by20StartingNumber_GotFocus(object sender, RoutedEventArgs e)
        {
            controlon = (TextBox)sender;
        }

        private void txt8by20StartingNumber_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Space)
                e.Handled = true;
            else if (e.Key == Key.Decimal)
                e.Handled = !IsTextAllowed(".");
        }

        private void txt8by20StartingNumber_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = !IsTextAllowed(e.Text);
        }

        private void txt8by20AutoStartingNumber_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Space)
                e.Handled = true;
            else if (e.Key == Key.Decimal)
                e.Handled = !IsTextAllowed(".");
        }

        private void txt8by20AutoStartingNumber_GotFocus(object sender, RoutedEventArgs e)
        {
            controlon = (TextBox)sender;
        }

        private void txt8by20AutoStartingNumber_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = !IsTextAllowed(e.Text);
        }

        private void txt6by20StartingNumber_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Space)
                e.Handled = true;
            else if (e.Key == Key.Decimal)
                e.Handled = !IsTextAllowed(".");
        }

        private void txt6by20StartingNumber_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = !IsTextAllowed(e.Text);
        }

        private void txt6by20StartingNumber_GotFocus(object sender, RoutedEventArgs e)
        {
            controlon = (TextBox)sender;
        }

        private void txt6by20AutoStartingNumber_GotFocus(object sender, RoutedEventArgs e)
        {
            controlon = (TextBox)sender;
        }

        private void txt6by20AutoStartingNumber_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Space)
                e.Handled = true;
            else if (e.Key == Key.Decimal)
                e.Handled = !IsTextAllowed(".");
        }

        private void txt6by20AutoStartingNumber_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = !IsTextAllowed(e.Text);
        }
        #endregion

        private void txt6900StartingNumber_GotFocus(object sender, RoutedEventArgs e)
        {
            controlon = (TextBox)sender;
        }

        private void txt6900AutoStartingNumber_GotFocus(object sender, RoutedEventArgs e)
        {
            controlon = (TextBox)sender;
        }
    }
}
