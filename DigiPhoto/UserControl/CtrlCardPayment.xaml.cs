﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using System.Threading;
using System.Text.RegularExpressions;
using FrameworkHelper;

namespace DigiPhoto
{
    /// <summary>
    /// Interaction logic for CtrlCardPayment.xaml
    /// </summary>
    public partial class CtrlCardPayment : UserControl
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CtrlCardPayment"/> class.
        /// </summary>
        public CtrlCardPayment()
        {
            InitializeComponent();
            Visibility = Visibility.Hidden;
        }

        /// <summary>
        /// The _parent
        /// </summary>
        private UIElement _parent;
        /// <summary>
        /// The _hide request
        /// </summary>
        private bool _hideRequest = false;
        /// <summary>
        /// The _result
        /// </summary>
        private string _result;
        /// <summary>
        /// The controlon
        /// </summary>
        TextBox controlon;
        /// <summary>
        /// Sets the parent.
        /// </summary>
        /// <param name="parent">The parent.</param>
        public void SetParent(UIElement parent)
        {
            _parent = (UIElement)parent;
        }

       /// <summary>
        /// Gets or sets the default currency.
        /// </summary>
        /// <value>
        /// The default currency.
        /// </value>
        public string DefaultCurrency
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the total bill amount.
        /// </summary>
        /// <value>
        /// The total bill amount.
        /// </value>
        public double TotalBillAmount
        {
            get;
            set;
        }


        #region Message

        /// <summary>
        /// Gets or sets the message card payment.
        /// </summary>
        /// <value>
        /// The message card payment.
        /// </value>
        public string MessageCardPayment
        {
            get { return (string)GetValue(MessageCardPaymentProperty); }
            set { SetValue(MessageCardPaymentProperty, value); }
        }

        // Using a DependencyProperty as the backing store for Message.
        // This enables animation, styling, binding, etc...
        /// <summary>
        /// The message card payment property
        /// </summary>
        public static readonly DependencyProperty MessageCardPaymentProperty =
            DependencyProperty.Register(
                "MessageCardPayment", typeof(string), typeof(ModalDialog), new UIPropertyMetadata(string.Empty));



        /// <summary>
        /// Shows the handler dialog.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <returns></returns>
        public string ShowHandlerDialog(string message)
        {
            MessageCardPayment = message;
            Visibility = Visibility.Visible;
            cmbCardType.Focus();
            FillYear();
            FillMonth();
            cmbCardType.SelectedIndex = 0;
            txtDefault.Text = DefaultCurrency;
            txtCardNumber.Text = string.Empty;//Empty on show handler
            TxtAmount.Text = TotalBillAmount.ToString("0.00"); 
             _parent.IsEnabled = true;
         
            _hideRequest = false;
            while (!_hideRequest)
            {
                // HACK: Stop the thread if the application is about to close
                if (this.Dispatcher.HasShutdownStarted ||
                    this.Dispatcher.HasShutdownFinished)
                {
                    break;
                }
                // HACK: Simulate "DoEvents"
                this.Dispatcher.Invoke(
                    DispatcherPriority.Background,
                    new ThreadStart(delegate { }));
                Thread.Sleep(20);
            }

            return _result;
        }

        /// <summary>
        /// Hides the handler dialog.
        /// </summary>
        private void HideHandlerDialog()
        {
          //  _result = null;
            _hideRequest = true;
            Visibility = Visibility.Collapsed;
            _parent.IsEnabled = true;
        }
        #endregion

        /// <summary>
        /// Handles the Click event of the btn control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void btn_Click(object sender, RoutedEventArgs e)
        {

            Button _objbtn = new Button();
            _objbtn = (Button)sender;
            switch (_objbtn.Content.ToString())
            {
                case "ENTER":
                    {
                        KeyBorder.Visibility = Visibility.Collapsed;
                        break;
                    }
                case "SPACE":
                    {

                        controlon.Text = controlon.Text + " ";


                        break;
                    }
                case "CLOSE":
                    {
                        KeyBorder.Visibility = Visibility.Collapsed;
                        break;
                    }
                case "Back":
                    {
                        TextBox objtxt = (TextBox)(controlon);
                        if (controlon.Text.Length > 0)
                        {
                            controlon.Text = controlon.Text.Remove(controlon.Text.Length - 1, 1);

                        }
                        break;
                    }
                default:
                    {


                        if (controlon.Text.Count() < 18)
                        {
                            string[] numbers = { "0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "." };
                            //var b = Array.contains(a, "red");
                            int pos = Array.IndexOf(numbers, _objbtn.Content);
                            if (pos > -1)
                            {
                                //int count = (controlon.Text + _objbtn.Content).Split('.').Length;
                                string[] afterDecimal = (controlon.Text + _objbtn.Content).Split('.');
                                if (afterDecimal.Length == 2 && afterDecimal[1].Length <= 2)
                                {
                        controlon.Text = controlon.Text + _objbtn.Content;
                                }
                                else if (afterDecimal.Length < 2)
                                {
                                    controlon.Text = controlon.Text + _objbtn.Content;
                                }
                            }
                        }


                        //controlon.Text = controlon.Text + _objbtn.Content;

                    }
                    break;
            }
        }
        /// <summary>
        /// Handles the Click event of the btnSubmit control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void btnSubmit_Click(object sender, RoutedEventArgs e)
        {
            
            _result = string.Empty;
            if (cmbCardType.SelectedIndex == 0)
            {
                Validation.Text = CommonUtility.getRequiredMessageForDdl(UIConstant.CardType);
                MessageBox.Show(Validation.Text,"DEI");
                cmbCardType.Focus();
            }/*6_15_2017 Remove chekc for month and year */
            //else if (cmbMonth.SelectedIndex == 0)
            //{
            //    Validation.Text = CommonUtility.getRequiredMessageForDdl(UIConstant.Month);
            //    MessageBox.Show(Validation.Text, "DEI");
            //    cmbMonth.Focus(); ;
            //}
            //else if (cmbYear.SelectedIndex == 0)
            //{
            //    Validation.Text = CommonUtility.getRequiredMessageForDdl(UIConstant.year);
            //    MessageBox.Show(Validation.Text, "DEI");
            //    cmbYear.Focus();
            //}
            else if (txtCardNumber.Text.Trim() == string.Empty)
            {
                Validation.Text = CommonUtility.getRequiredMessage(UIConstant.CardNumber);
                MessageBox.Show(Validation.Text, "DEI");
                txtCardNumber.Focus();
            }
            else if (txtCardNumber.Text.Trim().Length != 4)
            {
                Validation.Text = CommonUtility.getRequiredMessage(UIConstant.CardNumberLength);
                MessageBox.Show(Validation.Text, "DEI");
                txtCardNumber.Focus();
            }
            else if (TxtAmount.Text.Trim() == string.Empty || TxtAmount.Text.Trim() == "0")
            {
                Validation.Text = CommonUtility.getRequiredMessage(UIConstant.Amount);
                MessageBox.Show(Validation.Text, "DEI");
                TxtAmount.Focus();
            }
            //code cooment ashu
            //else if (cmbMonth.SelectedIndex > 0 && cmbYear.SelectedIndex > 0)
            //{
            //    if (new DateTime(Convert.ToInt32(DateTime.Now.Year),
            //     Convert.ToInt32(DateTime.Now.Month),
            //     DateTime.DaysInMonth(DateTime.Now.Year, DateTime.Now.Month)) >
            //     new DateTime(Convert.ToInt32(cmbYear.Text), Convert.ToInt32(cmbMonth.Text),
            //     DateTime.DaysInMonth(Convert.ToInt32(cmbYear.Text), Convert.ToInt32(cmbMonth.Text))))
            //    {                    
            //        MessageBox.Show("Please enter valid Expiry Date", "DEI");
            //        cmbMonth.Focus();
            //    }
            //    else
            //    {
            //        _result = ((ComboBoxItem)cmbCardType.SelectedItem).Content.ToString() + "%##%" + cmbYear.SelectedItem.ToString() + "%##%" + cmbMonth.SelectedItem.ToString() + "%##%" + txtCardNumber.Text + "%##%" + TxtAmount.Text;
            //        HideHandlerDialog();
            //    }
            //}
            else
            {
                _result = ((ComboBoxItem)cmbCardType.SelectedItem).Content.ToString() + "%##%" + cmbYear.SelectedItem.ToString() + "%##%" + cmbMonth.SelectedItem.ToString() + "%##%" + txtCardNumber.Text + "%##%" + TxtAmount.Text;
                HideHandlerDialog();
            }
        }



        public void FillYear()
        {
            cmbYear.Items.Clear();
            cmbYear.Items.Add("Year");
            for (int i = DateTime.Now.Year; i <= DateTime.Now.Year + 15; i++)
            {
                cmbYear.Items.Add(i);
            }
            cmbYear.SelectedIndex = 0;
        }
        public void FillMonth()
        {
            cmbMonth.Items.Clear();
            cmbMonth.Items.Add("Month");           
            for (int i = 1; i <= 12; i++)
            {
                cmbMonth.Items.Add(i);
            }
            cmbMonth.SelectedIndex = 0;
        }
        /// <summary>
        /// Handles the Click event of the btnSubmitCancel control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void btnSubmitCancel_Click(object sender, RoutedEventArgs e)
        {
            cmbCardType.SelectedIndex = 0;
            cmbYear.SelectedIndex = 0;
            cmbMonth.SelectedIndex = 0;
            txtCardNumber.Text = string.Empty;
            //txtCustomerName.Text = String.Empty;
            /*Payment entry -> Month and year made Non Mandatory 12_27_2017 */
            //TxtAmount.Text = String.Empty; //On clicking the reset button on the CR Card payment screen, the amount will not get cleared
            //txtName.Text = string.Empty;
            cmbCardType.Focus();
            _result = string.Empty; 
        }

        /// <summary>
        /// Handles the Click event of the btnClose control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void btnClose_Click(object sender, RoutedEventArgs e)
        {            
            txtCardNumber.Text = string.Empty;
            _result = string.Empty;
            HideHandlerDialog();
}

        /// <summary>
        /// Handles the PreviewTextInput event of the txtAmountEntered control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="TextCompositionEventArgs"/> instance containing the event data.</param>
        private void txtAmountEntered_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = !IsTextAllowed(e.Text);
        }

        private void txtAmount_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = !IsTextAllowed(e.Text);
        }


        private void txtCardNumber_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = !IsTextAllowedForlast4Digit(e.Text);
        }
        /// <summary>
        /// Determines whether [is text allowed] [the specified text].
        /// </summary>
        /// <param name="text">The text.</param>
        /// <returns></returns>
        private static bool IsTextAllowed(string text)
        {
            Regex regex = new Regex("[^0-9.-]+"); //regex that matches disallowed text
            return !regex.IsMatch(text);
        }

        private static bool IsTextAllowedForlast4Digit(string text)
        {
            Regex regex = new Regex("[^0-9]+"); //regex that matches disallowed text
            return !regex.IsMatch(text);
        }
        /// <summary>
        /// Pastings the handler.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="DataObjectPastingEventArgs"/> instance containing the event data.</param>
        private void PastingHandler(object sender, DataObjectPastingEventArgs e)
        {
            if (e.DataObject.GetDataPresent(typeof(String)))
            {
                String text = (String)e.DataObject.GetData(typeof(String));
                if (!IsTextAllowed(text)) e.CancelCommand();
            }
            else e.CancelCommand();
        }

        /// <summary>
        /// Handles the Loaded event of the UserControl control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            txtDefault.Text = DefaultCurrency;
            TxtAmount.Focus();
            TxtAmount.Text = TotalBillAmount.ToString("0.00");
          //  TxtAmount.IsEnabled = false;
        }

        /// <summary>
        /// Handles the GotFocus event of the txtCardType control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void txtCardType_GotFocus(object sender, RoutedEventArgs e)
        {
            controlon = (TextBox)sender;
            KeyBorder.Visibility = Visibility.Visible;

        }

        /// <summary>
        /// Handles the GotFocus event of the txtCustomerName control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void txtCustomerName_GotFocus(object sender, RoutedEventArgs e)
        {
            controlon = (TextBox)sender;
            KeyBorder.Visibility = Visibility.Visible;
        }

        /// <summary>
        /// Handles the GotFocus event of the txtCardNumber control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void txtCardNumber_GotFocus(object sender, RoutedEventArgs e)
        {
            controlon = (TextBox)sender;
            KeyBorder.Visibility = Visibility.Collapsed;
        }

        /// <summary>
        /// Handles the GotFocus event of the TxtAmount control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void TxtAmount_GotFocus(object sender, RoutedEventArgs e)
        {
            controlon = (TextBox)sender;
            KeyBorder.Visibility = Visibility.Visible;
        }

        private void txtCardNumber_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Space)
            {
                e.Handled = true;
            }
        }

        private void TxtAmount_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Space)
            {
                e.Handled = true;
            }
        }
        
    }
}
