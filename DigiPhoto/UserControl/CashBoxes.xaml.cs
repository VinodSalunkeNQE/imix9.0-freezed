﻿using DigiAuditLogger;
using DigiPhoto.Common;
using DigiPhoto.DataLayer;
using DigiPhoto.IMIX.Business;
using DigiPhoto.IMIX.Model;
using DigiPhoto.Manage;
using FrameworkHelper;
using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;


namespace DigiPhoto
{
    /// <summary>
    /// Interaction logic for CashBox.xaml
    /// </summary>
    public partial class CashBoxes : UserControl
    {
        TextBox controlon;
        //DigiPhotoDataServices _objDataLayer = null;
        bool isEnableSlipPrint = true;
        public CashBoxes()
        {
            if (!System.ComponentModel.DesignerProperties.GetIsInDesignMode(this))
            {
                InitializeComponent();
                GrdPrint.Visibility = Visibility.Visible;

                //if (_objDataLayer == null)
                // _objDataLayer = new DigiPhotoDataServices();        
                List<iMIXConfigurationInfo> objList = (new ConfigBusiness()).GetNewConfigValues(Convert.ToInt32(LoginUser.SubStoreId));
                foreach (iMIXConfigurationInfo imixConfigValue in objList)
                {
                    switch (imixConfigValue.IMIXConfigurationMasterId)
                    {
                        case (int)ConfigParams.IsEnableSlipPrint:
                            isEnableSlipPrint = string.IsNullOrWhiteSpace(imixConfigValue.ConfigurationValue) ? true : Convert.ToBoolean(imixConfigValue.ConfigurationValue);
                            break;
                    }
                }
            }
        }

        //public static readonly RoutedEvent ShowMainWindowGridEvent = EventManager.RegisterRoutedEvent("ShowMainWindowGrid", RoutingStrategy.Bubble, typeof(RoutedEventHandler), typeof(CashBox));

        //public event RoutedEventHandler ShowMainWindowGrid
        //{
        //    add { AddHandler(CashBox.ShowMainWindowGridEvent, value); }
        //    remove { RemoveHandler(CashBox.ShowMainWindowGridEvent, value); }
        //}

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                fillCombo();
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        private void fillCombo()
        {
            //Dictionary<string, string> lstProductList;
            List<string> lstProductList;
            lstProductList = new List<string>();
            lstProductList.Add("--Select--");

            try
            {
                //DigiPhotoDataServices _objdbLayer = new DigiPhotoDataServices();
                //var list = _objdbLayer.GetReasonType((int)DigiPhoto.DataLayer.DigiPhotoDataServices.ValueTypeGroupEnum.CashBoxOpenReason);
                
                var list = new ValueTypeBusiness().GetReasonType((int)ValueTypeGroupEnum.CashBoxOpenReason);
                if (list == null)
                {
                    list = new List<ValueTypeInfo>();
                }
                if(!(LoginUser.RoleId==7 || LoginUser.RoleId==101))
                    list.RemoveAll(x => x.ValueTypeId == 101);
                list.Add(new ValueTypeInfo { Name = "Others", ValueTypeId = -1 });
                CommonUtility.BindComboWithSelect<ValueTypeInfo>(CmbReasonType, list, "Name", "ValueTypeId", 0, DigiPhoto.Utility.Repository.Data.ClientConstant.SelectString); 
                //foreach (var item in list.ToList())
                //{
                //    lstProductList.Add(item.Name);
                //}
                //lstProductList.Add("Others");
                //CmbReasonType.ItemsSource = lstProductList;

                CmbReasonType.SelectedIndex = 0;
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }

        private void btnClose_Click(object sender, RoutedEventArgs e)
        {
            GrdPrint.Visibility = Visibility.Hidden;
            ManageHome objManageHome = new ManageHome();
            objManageHome.brdMain.IsEnabled = true;
            objManageHome.Show();
            Window parentWindow = Window.GetWindow(this);
            parentWindow.Close();
        }

        /*
        private void CmbReasonType_SelectedIndexChanged(object sender, EventArgs e)
        {
            ComboBox cb = (ComboBox)sender;
            if (cb.SelectedItem == "Others")
            {
                txtreason.IsEnabled = true;
            }

            else
            {
                txtreason.IsEnabled = false;

            }
            // Here is your Code for selection change
        }
        */
        private void btnSubmitCancel_Click(object sender, RoutedEventArgs e)
        {
            GrdPrint.Visibility = Visibility.Hidden;
            ManageHome _objhome = new ManageHome();
            _objhome.Show();
            Window parentWindow = Window.GetWindow(this);
            parentWindow.Close();
        }

        private void btnsubmit_click(object sender, RoutedEventArgs e)
        {
            string reason = string.Empty;
            ValueTypeInfo reasonType = (ValueTypeInfo)CmbReasonType.SelectedItem;
            if (reasonType.Name == "Others")
            {
                if (string.IsNullOrWhiteSpace(txtreason.Text.Trim()))
                {
                    MessageBox.Show("Please enter the Comment.");
                    return;
                }
                reason = reasonType.Name + "-" + txtreason.Text.Trim();
            }
            else
            {             
                if (string.IsNullOrWhiteSpace(txtreason.Text.Trim()))  
                {
                    reason = reasonType.Name;
                }
                else
                    reason = reasonType.Name + "-" + txtreason.Text.Trim();
                if (string.IsNullOrEmpty(reason))
                {
                    MessageBox.Show("Please select reason to open cash drawer.");
                    return;
                }

                if (reasonType.Name == "--Select--")
                {

                    MessageBox.Show("Please select reason to open cash drawer.");
                    return;
                }

                
            }
            //DigiPhotoDataServices _objServices = new DigiPhotoDataServices();
            //_objServices.SetcashBoxReason((new CustomBusineses()).ServerDateTime(), LoginUser.UserId, reason);
            new CashBoxBusiness().SetcashBoxReason((new CustomBusineses()).ServerDateTime(), LoginUser.UserId, reason);
            //_objServices.SetcashBoxReason(new CustomBusineses().ServerDateTime(), LoginUser.UserId, reason);
            txtreason.Clear();
            GrdPrint.Visibility = Visibility.Hidden;
            MessageBox.Show("Comment saved successfully.");

            ManageHome objManageHome = new ManageHome();

            objManageHome.brdMain.IsEnabled = true;
            objManageHome.Show();
            Window parentWindow = Window.GetWindow(this);
            parentWindow.Close();

            AuditLog.AddUserLog(Common.LoginUser.UserId, (int)FrameworkHelper.ActionType.CashDrawer, "Action: Cash Drawer opened Reason: " + reason + " Opened at ");
            //Print dummy receipt.
            if(isEnableSlipPrint)
            {
            DigiPhoto.Orders.TestBill objTestBill = new Orders.TestBill(Common.LoginUser.SubstoreName.ToString(), Common.LoginUser.UserName.ToString(), reason);
        }
        }

        private void txtreason_GotFocus(object sender, RoutedEventArgs e)
        {
            controlon = (TextBox)sender;
            KeyBorder.Visibility = Visibility.Visible;
        }

        private void btn_Click(object sender, RoutedEventArgs e)
        {

            Button _objbtn = new Button();
            _objbtn = (Button)sender;
            switch (_objbtn.Content.ToString())
            {
                case "ENTER":
                    {
                        controlon.Text = controlon.Text + Environment.NewLine;                       
                        break;
                    }
                case "SPACE":
                    {

                        controlon.Text = controlon.Text + " ";


                        break;
                    }
                case "CLOSE":
                    {
                        KeyBorder.Visibility = Visibility.Collapsed;
                        break;
                    }
                case "Back":
                    {
                        TextBox objtxt = (TextBox)(controlon);
                        if (controlon.Text.Length > 0)
                        {
                            controlon.Text = controlon.Text.Remove(controlon.Text.Length - 1, 1);
                        }
                        break;
                    }
                default:
                    {
                        controlon.Text = controlon.Text + _objbtn.Content;
                    }
                    break;
            }
        }
    }
}
