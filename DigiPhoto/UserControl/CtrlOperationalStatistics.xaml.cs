﻿using DigiPhoto.IMIX.Business;
using DigiPhoto.IMIX.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace DigiPhoto
{
    /// <summary>
    /// Interaction logic for CtrlOperationalStatistics.xaml
    /// </summary>
    public partial class CtrlOperationalStatistics : UserControl
    {
        public CtrlOperationalStatistics()
        {
            InitializeComponent();
            Visibility = Visibility.Hidden;
        }
        /// <summary>
        /// The _parent
        /// </summary>
        private UIElement _parent;
        /// <summary>
        /// The _hide request
        /// </summary>
        private bool _hideRequest = false;
        /// <summary>
        /// The _result
        /// </summary>
        private string _result;
        /// <summary>
        /// The controlon
        /// </summary>
        TextBox controlon;
        /// <summary>
        /// Sets the parent.
        /// </summary>
        /// <param name="parent">The parent.</param>
        private SageInfoClosing sageInfoClosing = null;
        public void SetParent(UIElement parent)
        {
            _parent = (UIElement)parent;

        }


        public DateTime FromDate { get; set; }
        public DateTime BusinessDate { get; set; }
        public DateTime ToDate { get; set; }
        public int SubStoreID { get; set; }
        public int Back { get; set; }
        // public Int64 SixEightStartingNumber { get; set; }

        #region Message

        /// <summary>
        /// Gets or sets the message card payment.
        /// </summary>
        /// <value>
        /// The message card payment.
        /// </value>
        public string MessagOperationalStatForm
        {
            get { return (string)GetValue(MessageOperationalStatFormProperty); }
            set { SetValue(MessageOperationalStatFormProperty, value); }
        }

        // Using a DependencyProperty as the backing store for Message.
        // This enables animation, styling, binding, etc...
        /// <summary>
        /// The message card payment property
        /// </summary>
        public static readonly DependencyProperty MessageOperationalStatFormProperty =
            DependencyProperty.Register(
                "MessageOperationalStatForm", typeof(string), typeof(ModalDialog), new UIPropertyMetadata(string.Empty));



        /// <summary>
        /// Shows the handler dialog.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <returns></returns>
        public string ShowHandlerDialog(string message)
        {
            Back = 0;
            MessagOperationalStatForm = message;
            Visibility = Visibility.Visible;
            SetNoOfCapture(FromDate, ToDate, SubStoreID);
            SetNoOfPreview(FromDate, ToDate, SubStoreID);
            SetNoOfImageSold(FromDate, ToDate, SubStoreID);
            SetTotalRevenueDetails(FromDate, ToDate, SubStoreID);
            txtAttendance.Focus();
            txtHeader.Text = "Closing Form for " + ((FromDate != DateTime.MinValue) ? string.Format("{0:dd-MMM-yyyy}", FromDate) : string.Format("{0:dd-MMM-yyyy}", DateTime.Now));
            txtSubHeader.Text = "Operational Statistics";
            // _parent.IsEnabled = false;



            _hideRequest = false;
            while (!_hideRequest)
            {
                // HACK: Stop the thread if the application is about to close
                if (this.Dispatcher.HasShutdownStarted ||
                    this.Dispatcher.HasShutdownFinished)
                {
                    break;
                }
                // HACK: Simulate "DoEvents"
                this.Dispatcher.Invoke(
                    DispatcherPriority.Background,
                    new ThreadStart(delegate { }));
                Thread.Sleep(20);
            }

            return _result;
        }

        /// <summary>
        /// Hides the handler dialog.
        /// </summary>
        private void HideHandlerDialog()
        {
            //  _result = null;
            _hideRequest = true;
            Visibility = Visibility.Collapsed;
         //   _parent.IsEnabled = true;
        }
        #endregion

        private void PastingHandler(object sender, DataObjectPastingEventArgs e)
        {
            if (e.DataObject.GetDataPresent(typeof(String)))
            {
                String text = (String)e.DataObject.GetData(typeof(String));
                if (!IsTextAllowed(text)) e.CancelCommand();
            }
            else e.CancelCommand();
        }
        private static bool IsTextAllowed(string text)
        {
            Regex regex = new Regex("[^0-9]+"); //regex that matches disallowed text
            return !regex.IsMatch(text);
        }
        private void txtAmountEntered_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = !IsTextAllowed(e.Text);
        }
        /// <summary>
        /// Handles the Click event of the btnSubmit control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void btnSubmit_Click(object sender, RoutedEventArgs e)
        {
            //txt6X8StarNumber.Text = String.Empty;
            //txt8X10StarNumber.Text = String.Empty;
            //txtCashFloat.Text = String.Empty;
            _result = string.Empty;
            if (txtAttendance.Text == string.Empty)
            {
                //Validation.Text = CommonUtility.getRequiredMessage(UIConstant.CardType);
                MessageBox.Show("Please enter attendance", "DEI");
                txtAttendance.Focus();
                txtAttendance.BorderBrush = Brushes.Red;
            }
            else
                if (txtLabourHour.Text == string.Empty)
                {
                    MessageBox.Show("Please enter labour hour", "DEI");
                    //Validation.Text = CommonUtility.getRequiredMessage(UIConstant.CustomerName);
                    txtLabourHour.Focus();
                    txtLabourHour.BorderBrush = Brushes.Red;
                }
                else if (txtNoOfCapture.Text == string.Empty)
                {
                    //Validation.Text = CommonUtility.getRequiredMessage(UIConstant.CardNumber);
                    txtNoOfCapture.Focus();
                }
                else if (txtNoOfPreviews.Text == string.Empty)
                {
                    //Validation.Text = CommonUtility.getRequiredMessage(UIConstant.CardNumber);
                    txtNoOfPreviews.Focus();
                }
                else if (txtNoOfImageSold.Text == string.Empty)
                {
                    //Validation.Text = CommonUtility.getRequiredMessage(UIConstant.CardNumber);
                    txtNoOfImageSold.Focus();
                }
                else if (txtComment.Text == string.Empty)
                {
                    MessageBox.Show("Please enter comment", "DEI");
                    //Validation.Text = CommonUtility.getRequiredMessage(UIConstant.CardNumber);
                    txtComment.Focus();
                    txtComment.BorderBrush = Brushes.Red;
                }
                else
                {
                    string noOfTransaction = sageInfoClosing != null ? sageInfoClosing.NoOfTransactions.ToString() : "0.00";
                    _result = txtAttendance.Text.ToString() + "%##%" + txtLabourHour.Text + "%##%" + txtNoOfCapture.Text + "%##%" + txtNoOfPreviews.Text + "%##%" + txtNoOfImageSold.Text + "%##%" + txtComment.Text + "%##%" + noOfTransaction;
                    HideHandlerDialog();
                }
        }
        private void btnClose_Click(object sender, RoutedEventArgs e)
        {
            _result = string.Empty;
            HideHandlerDialog();
        }

        public void SetNoOfCapture(DateTime FromDate, DateTime ToDate, int SubStoreID)
        {
            int Capture = (new SageBusiness()).GetCaptureBySubStoreAndDateRange(FromDate, ToDate, SubStoreID);
            txtNoOfCapture.Text = Capture.ToString();
        }
        public void SetNoOfPreview(DateTime FromDate, DateTime ToDate, int SubStoreID)
        {
            int Preview = (new SageBusiness()).GetPreviewBySubStoreAndDateRange(FromDate, ToDate, SubStoreID);
            txtNoOfPreviews.Text = Preview.ToString();
        }
        public void SetNoOfImageSold(DateTime FromDate, DateTime ToDate, int SubStoreID)
        {
            Int64 ImageSold = (new SageBusiness()).GetTotalSoldBySubStoreAndDateRange(FromDate, ToDate, SubStoreID);
            txtNoOfImageSold.Text = ImageSold.ToString();
        }

        public void SetTotalRevenueDetails(DateTime FromDate, DateTime ToDate, int SubStoreID)
        {
            sageInfoClosing = (new SageBusiness()).GetRevenueDetails(FromDate, ToDate, SubStoreID);
        }


        private void btn_Click(object sender, RoutedEventArgs e)
        {
            Button _objbtn = (Button)sender;
            //_objbtn = (Button)sender;
            switch (_objbtn.Content.ToString())
            {
                case "ENTER":
                    {
                        KeyBorder.Visibility = Visibility.Hidden;
                        break;
                    }
                case "SPACE":
                    {
                        controlon.Text = controlon.Text + " ";
                        break;
                    }
                case "CLOSE":
                    {
                        KeyBorder.Visibility = Visibility.Hidden;
                        break;
                    }
                case "Back":
                    {
                        TextBox objtxt = (TextBox)(controlon);
                        if (controlon.Text.Length > 0)
                        {
                            controlon.Text = controlon.Text.Remove(controlon.Text.Length - 1, 1);
                        }
                        break;
                    }
                default:
                    {
                        //if (controlon.Text.Count() < 15)
                        //{
                        string[] numbers = { "0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "." };
                        //var b = Array.contains(a, "red");
                        if (controlon.Name != "txtComment")
                        {
                            if (controlon.Text.Count() < 15)
                            {
                                int pos = Array.IndexOf(numbers, _objbtn.Content);
                                if (pos > -1)
                                {
                                    string[] afterDecimal = (controlon.Text + _objbtn.Content).Split('.');
                                    if (afterDecimal.Length == 2 && afterDecimal[1].Length <= 2)
                                    {
                                        controlon.Text = controlon.Text + _objbtn.Content;
                                    }
                                    else if (afterDecimal.Length < 2)
                                    {
                                        controlon.Text = controlon.Text + _objbtn.Content;
                                    }
                                    //controlon.Text = controlon.Text + _objbtn.Content;
                                }
                            }
                        }
                        else if (controlon.Text.Count() <= 59)
                        {
                            controlon.Text = controlon.Text + _objbtn.Content;
                        }


                        //}
                    }
                    break;
            }
        }

        private void txtAttendance_GotFocus(object sender, RoutedEventArgs e)
        {
            controlon = (TextBox)sender;
            KeyBorder.Visibility = Visibility.Visible;
        }

        private void txtLabourHour_GotFocus(object sender, RoutedEventArgs e)
        {
            controlon = (TextBox)sender;
            KeyBorder.Visibility = Visibility.Visible;
        }

        private void txtNoOfCapture_GotFocus(object sender, RoutedEventArgs e)
        {
            controlon = (TextBox)sender;
            KeyBorder.Visibility = Visibility.Visible;
        }

        private void btnback_Click(object sender, RoutedEventArgs e)
        {
            Back = 1;
            _result = string.Empty;
            HideHandlerDialog();
            //Home ob = new Home();
            //ob.btnOpenClose_Click(null, null);

            //var Res1 = CtrlInventoryConsumables.ShowHandlerDialog("Closing Form");
        }
        private void txtAttendance_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Space)
            {
                e.Handled = true;
            }
        }

        private void txtComment_GotFocus(object sender, RoutedEventArgs e)
        {
            controlon = (TextBox)sender;
            KeyBorder.Visibility = Visibility.Visible;
        }
    }
}
