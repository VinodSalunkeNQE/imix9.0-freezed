﻿using System;
using System.Collections.Generic;
using System.Linq;
using FrameworkHelper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using DigiPhoto.IMIX.Model;
using DigiPhoto.IMIX.Business;
using DigiPhoto.Common;
using System.Collections.ObjectModel;

namespace DigiPhoto
{
    /// <summary>
    /// Interaction logic for CtrlInventoryConsumables.xaml
    /// </summary>
    /// private ObservableCollection<PaymentItem> _objPayment;
    public partial class CtrlInventoryConsumables : UserControl
    {

        #region Variables
        public bool _issemiorder;
        private ObservableCollection<InventoryConsumables> _objInventory;
        #endregion
        public CtrlInventoryConsumables()
        {
            if (!System.ComponentModel.DesignerProperties.GetIsInDesignMode(this))
            {
                _objInventory = new ObservableCollection<InventoryConsumables>();
                InitializeComponent();
                FillInventory();
                dgInventory.ItemsSource = _objInventory;
                Visibility = Visibility.Hidden;
            }
        }


        /// <summary>
        /// The _parent
        /// </summary>
        private UIElement _parent;
        /// <summary>
        /// The _hide request
        /// </summary>
        private bool _hideRequest = false;
        /// <summary>
        /// The _result
        /// </summary>
        private string _result;
        /// <summary>
        /// The controlon
        /// </summary>
        TextBox controlon;
        /// <summary>
        /// Sets the parent.
        /// </summary>
        /// <param name="parent">The parent.</param>
        public void SetParent(UIElement parent)
        {
            _parent = (UIElement)parent;

        }


        #region Message

        /// <summary>
        /// Gets or sets the message card payment.
        /// </summary>
        /// <value>
        /// The message card payment.
        /// </value>
        public string MessagInventoryConsForm
        {
            get { return (string)GetValue(MessageInventoryConsFormProperty); }
            set { SetValue(MessageInventoryConsFormProperty, value); }
        }

        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public int SubStoreID { get; set; }

        public DateTime BusinessDate { get; set; }
        public Int64 SixEightStartingNumber { get; set; }
        //public Int64 SixEndNumber { get; set; }
        public Int64 EightTenStartingNumber { get; set; }
        public Int64 PosterStartingNumber { get; set; }
        public Int64 SixEightPrintCount { get; set; }
        public Int64 EightTenPrintCount { get; set; }
        public Int64 PosterPrintCount { get; set; }
        public List<InventoryConsumables> lstinventoryItem { get; set; }

        public Int64 SixEightAutoStartingNumber { get; set; }
        public Int64 EightTenAutoStartingNumber { get; set; }
        public Int64 SixEightAutoClosingNumber { get; set; }
        public Int64 EightTenAutoClosingNumber { get; set; }

        // BY KCB ON 26 MARCH 2018 FOR storing panorama printer inventry

        public Int64 SixTwentyStartingNumber { get; set; }
        public Int64 EightTwentyStartingNumber { get; set; }
        public Int64 SixTwentyAutoStartingNumber { get; set; }
        public Int64 EightTwemtyAutoStartingNumber { get; set; }
        public Int64 SixTwemtyAutoClosingNumber { get; set; }
        public Int64 EightTwemtyAutoClosingNumber { get; set; }
        //END
        // BY KCB ON 08 AUG 2019 FOR storing panorama printer inventory
        public Int64 K6900StartingNumber { get; set; }
        public Int64 K6900AutoStartingNumber { get; set; }
        public Int64 K6900AutoClosingNumber { get; set; }
        //END
        // public Int64 TenEndNumber { get; set; }

        // Using a DependencyProperty as the backing store for Message.
        // This enables animation, styling, binding, etc...
        /// <summary>
        /// The message card payment property
        /// </summary>
        public static readonly DependencyProperty MessageInventoryConsFormProperty =
            DependencyProperty.Register(
                "MessageInventoryConsForm", typeof(string), typeof(ModalDialog), new UIPropertyMetadata(string.Empty));



        /// <summary>
        /// Shows the handler dialog.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <returns></returns>
        public string ShowHandlerDialog(string message)
        {
            MessagInventoryConsForm = message;
            Visibility = Visibility.Visible;
            //txt6X8CloseNumber.Text = "";
            //txt8X10CloseNumber.Text = "";
            //txtPosterCloseNumber.Text = "";
            //txt6X8CloseNumber.Focus();
            //  txtHeader.Text = "Inventory Consumables used for " + ((FromDate != DateTime.MinValue) ? string.Format("{0:dddd, MMMM d, yyyy}", FromDate) : string.Format("{0:dddd, MMMM d, yyyy}", DateTime.Now));  //string.Format("{0:ddd, MMM d, yyyy}", FromDate);
            txtHeader.Text = "Closing Form for " + ((BusinessDate != DateTime.MinValue) ? string.Format("{0:dd-MMM-yyyy}", BusinessDate) : string.Format("{0:dd-MMM-yyyy}", DateTime.Now));  //string.Format("{0:ddd, MMM d, yyyy}", FromDate);
            txtSubHeader.Text = "Inventory Consumables";            // _parent.IsEnabled = false;

            txtAuto6X8AutoClos.Text = Convert.ToString(SixEightAutoClosingNumber);
            txtAuto8X10AutoClos.Text = Convert.ToString(EightTenAutoClosingNumber);
            txt6900AutoClosing.Text = Convert.ToString(K6900AutoClosingNumber);


            // BY KCB ON 26 MARCH 2018 FOR storing panorama printer inventry
            txt620AutoClosing.Text = Convert.ToString(SixTwemtyAutoClosingNumber);
            txt820AutoClosing.Text = Convert.ToString(EightTwemtyAutoClosingNumber);
            //END

            SetSixEightAutoWestage(FromDate, ToDate, SubStoreID, SixEightAutoStartingNumber, SixEightAutoClosingNumber);
            SetEightTenAutoWestage(FromDate, ToDate, SubStoreID, EightTenAutoStartingNumber, EightTenAutoClosingNumber);

            txtAuto820Westage.Text = "0";
            txtAuto620Westage.Text = "0";
            txtAuto6900Westage.Text = "0";

            _hideRequest = false;
            while (!_hideRequest)
            {
                // HACK: Stop the thread if the application is about to close
                if (this.Dispatcher.HasShutdownStarted ||
                    this.Dispatcher.HasShutdownFinished)
                {
                    break;
                }
                // HACK: Simulate "DoEvents"
                this.Dispatcher.Invoke(
                    DispatcherPriority.Background,
                    new ThreadStart(delegate { }));
                Thread.Sleep(20);
            }
            lstinventoryItem = _objInventory.ToList();
            return _result;
        }

        /// <summary>
        /// Hides the handler dialog.
        /// </summary>
        private void HideHandlerDialog()
        {
            //  _result = null;
            _hideRequest = true;
            Visibility = Visibility.Collapsed;
            //_parent.IsEnabled = true;   

        }
        private void FillInventory()
        {

            SubStoresInfo _objSubstore = (new StoreSubStoreDataBusniess()).GetSubstoreData(LoginUser.SubStoreId);
            int SubStoreID = 0;
            if (_objSubstore != null)
                SubStoreID = (_objSubstore.LogicalSubStoreID == 0) ? LoginUser.SubStoreId : Convert.ToInt32(_objSubstore.LogicalSubStoreID);

            ProductBusiness proBiz = new ProductBusiness();
            var list = proBiz.GetProductType().Where(t => t.DG_IsAccessory == true && t.DG_SubStore_pkey == SubStoreID && t.DG_IsActive == true).ToList();
            CommonUtility.BindCombo<ProductTypeInfo>(cmbInventory, list, "DG_Orders_ProductType_Name", "DG_Orders_ProductType_pkey");
            cmbInventory.SelectedValue = "0";
        }
        #endregion

        private void PastingHandler(object sender, DataObjectPastingEventArgs e)
        {
            if (e.DataObject.GetDataPresent(typeof(String)))
            {
                String text = (String)e.DataObject.GetData(typeof(String));
                if (!IsTextAllowed(text)) e.CancelCommand();
            }
            else e.CancelCommand();
        }
        private static bool IsTextAllowed(string text)
        {
            //text = Regex.Replace(text, @"\s", ">");
            Regex regex = new Regex("[^0-9]+"); //regex that matches disallowed text
            return !regex.IsMatch(text);
        }
        private void txtAmountEntered_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = !IsTextAllowed(e.Text.Trim());
        }
        /// <summary>
        /// Handles the Click event of the btnSubmit control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void btnSubmit_Click(object sender, RoutedEventArgs e)
        {
            //txt6X8StarNumber.Text = String.Empty;
            //txt8X10StarNumber.Text = String.Empty;
            //txtCashFloat.Text = String.Empty;
            _result = string.Empty;
            if (txt6X8CloseNumber.Text.Trim() == string.Empty)
            {
                //Validation.Text = CommonUtility.getRequiredMessage(UIConstant.CardType);
                MessageBox.Show("Please enter 6X8 printer closing number", "DEI");
                txt6X8CloseNumber.Focus();
                txt6X8CloseNumber.BorderBrush = Brushes.Red;
                return;
            }
            else if (txt8X10CloseNumber.Text.Trim() == string.Empty)
            {
                //Validation.Text = CommonUtility.getRequiredMessage(UIConstant.CardType);
                MessageBox.Show("Please enter 8X10 printer closing number", "DEI");
                txt8X10CloseNumber.Focus();
                txt8X10CloseNumber.BorderBrush = Brushes.Red;
                return;
            }

            // BY KCB ON 26 MARCH 2018 FOR storing panorama printer inventry
            if (txt620ClosingNumber.Text.Trim() == string.Empty)
            {
                //Validation.Text = CommonUtility.getRequiredMessage(UIConstant.CardType);
                MessageBox.Show("Please enter 620 printer closing number", "DEI");
                txt620ClosingNumber.Focus();
                txt620ClosingNumber.BorderBrush = Brushes.Red;
                return;
            }
            else if (txt820ClosingNumber.Text.Trim() == string.Empty)
            {
                //Validation.Text = CommonUtility.getRequiredMessage(UIConstant.CardType);
                MessageBox.Show("Please enter 820 printer closing number", "DEI");
                txt820ClosingNumber.Focus();
                txt8X10CloseNumber.BorderBrush = Brushes.Red;
                return;
            }
            //END
            else if (txtPosterCloseNumber.Text.Trim() == string.Empty)
            {
                //Validation.Text = CommonUtility.getRequiredMessage(UIConstant.CardType);
                MessageBox.Show("Please enter poster printer closing number", "DEI");
                txtPosterCloseNumber.Focus();
                txtPosterCloseNumber.BorderBrush = Brushes.Red;
                return;
            }
            else if (txt6900ClosingNumber.Text.Trim() == string.Empty)
            {
                //Validation.Text = CommonUtility.getRequiredMessage(UIConstant.CardType);
                MessageBox.Show("Please enter 6900 printer closing number", "DEI");
                txt6900ClosingNumber.Focus();
                txt6900ClosingNumber.BorderBrush = Brushes.Red;
                return;
            }
            //else if (txt6X8Folder.Text.Trim() == string.Empty)
            //{
            //    //Validation.Text = CommonUtility.getRequiredMessage(UIConstant.CardType);
            //    MessageBox.Show("Please enter value", "DEI");
            //    txt6X8Folder.Focus();
            //    txt6X8Folder.BorderBrush = Brushes.Red;
            //    return;
            //}
            //else if (txt8X10Folder.Text.Trim() == string.Empty)
            //    {
            //        //Validation.Text = CommonUtility.getRequiredMessage(UIConstant.CustomerName);
            //        MessageBox.Show("Please enter value", "DEI");
            //        txt8X10Folder.Focus();
            //        txt8X10Folder.BorderBrush = Brushes.Red;
            //        return;
            //    }
            //else if (txt6X8FolderCash.Text.Trim() == string.Empty)
            //    {
            //        //Validation.Text = CommonUtility.getRequiredMessage(UIConstant.CardNumber);
            //        MessageBox.Show("Please enter value", "DEI");
            //        txt6X8FolderCash.Focus();
            //        txt6X8FolderCash.BorderBrush = Brushes.Red;
            //        return;
            //    }
            //else if (txt8X10FolderCash.Text.Trim() == string.Empty)
            //    {
            //        //Validation.Text = CommonUtility.getRequiredMessage(UIConstant.CardNumber);
            //        MessageBox.Show("Please enter value", "DEI");
            //        txt8X10FolderCash.Focus();
            //        txt8X10FolderCash.BorderBrush = Brushes.Red;
            //        return;
            //    }
            //else if (txtRovingTicket.Text.Trim() == string.Empty)
            //    {
            //        //Validation.Text = CommonUtility.getRequiredMessage(UIConstant.CardNumber);
            //        MessageBox.Show("Please enter value", "DEI");
            //        txtRovingTicket.Focus();
            //        txtRovingTicket.BorderBrush = Brushes.Red;
            //        return;
            //    }
            //else if (txtRovingBand.Text.Trim() == string.Empty)
            //    {
            //        //Validation.Text = CommonUtility.getRequiredMessage(UIConstant.CardNumber);
            //        MessageBox.Show("Please enter value", "DEI");
            //        txtRovingBand.Focus();
            //        txtRovingBand.BorderBrush = Brushes.Red;
            //        return;
            //    }
            //else if (txtPlasticBag.Text.Trim() == string.Empty)
            //    {
            //        //Validation.Text = CommonUtility.getRequiredMessage(UIConstant.CardNumber);
            //        MessageBox.Show("Please enter value", "DEI");
            //        txtPlasticBag.Focus();
            //        txtPlasticBag.BorderBrush = Brushes.Red;
            //        return;
            //    }
            else if (txt6X8Westage.Text.Trim() == string.Empty)
            {
                //Validation.Text = CommonUtility.getRequiredMessage(UIConstant.CardNumber);
                MessageBox.Show("Please enter value", "DEI");
                txt6X8Westage.Focus();
                txt6X8Westage.BorderBrush = Brushes.Red;
                return;
            }
            else if (txt8X10Westage.Text.Trim() == string.Empty)
            {
                //Validation.Text = CommonUtility.getRequiredMessage(UIConstant.CardNumber);
                MessageBox.Show("Please enter value", "DEI");
                txt8X10Westage.Focus();
                txt8X10Westage.BorderBrush = Brushes.Red;
                return;
            }
            else
            {

                //+ txt6X8Folder.Text.ToString() + "%##%" + txt8X10Folder.Text + "%##%" + txt6X8FolderCash.Text + "%##%" + txt8X10FolderCash.Text + "%##%" + txtRovingTicket.Text + "%##%" + txtRovingBand.Text + "%##%" + txtPlasticBag.Text + "%##%"

                // BY KCB ON 26 MARCH 2018 FOR storing panorama printer inventry
                // _result = txt6X8CloseNumber.Text.ToString() + "%##%" + txt8X10CloseNumber.Text.ToString() + "%##%" + txtPosterCloseNumber.Text.ToString() + "%##%"
                //+ txt6X8Westage.Text + "%##%" + txt8X10Westage.Text + "%##%" + txtPosterWestage.Text;
                _result = txt6X8CloseNumber.Text.ToString() + "%##%" + txt8X10CloseNumber.Text.ToString() + "%##%" + txt620ClosingNumber.Text.ToString() + "%##%" + txt820ClosingNumber.Text.ToString() + "%##%" + txtPosterCloseNumber.Text.ToString() + "%##%"
               + txt6X8Westage.Text + "%##%" + txt8X10Westage.Text + "%##%" + txtAuto620Westage.Text + "%##%" + txtAuto820Westage.Text + "%##%" + txtPosterWestage.Text + "%##%" + txt6900ClosingNumber.Text.Trim() + "%##%" + txtAuto6900Westage.Text.Trim();
                //END
                HideHandlerDialog();
            }
        }
        private void btnClose_Click(object sender, RoutedEventArgs e)
        {
            _result = string.Empty;
            HideHandlerDialog();
        }

        //private void txt6X8Westage_SelectionChanged(object sender, RoutedEventArgs e)
        //{
        //    //txt6X8Westage.Text = SixEightWestage;
        //    SetSixEightWestage(FromDate, ToDate, SubStoreID, SixEightStartingNumber, Convert.ToInt32(txt6X8CloseNumber.Text));
        //}

        public void SetSixEightAutoWestage(DateTime FromDate, DateTime ToDate, int SubStoreID, Int64 StartingNumber, Int64 EndNumber)
        {
            List<SageInfoWestage> _lstsageInfoWest = (new SageBusiness()).ProductTypeWestage(FromDate, ToDate, SubStoreID);
            if (_lstsageInfoWest.Count() > 0)
            {
                var sixeightwest = _lstsageInfoWest.Where(o => o.ProductType == 1).FirstOrDefault();
                var sixeightwest3 = _lstsageInfoWest.Where(o => o.ProductType == 3).FirstOrDefault();
                var sixeightwest4 = _lstsageInfoWest.Where(o => o.ProductType == 4).FirstOrDefault();
                var sixeightwest5 = _lstsageInfoWest.Where(o => o.ProductType == 5).FirstOrDefault();
                var sixeightwest30 = _lstsageInfoWest.Where(o => o.ProductType == 30).FirstOrDefault();
                var sixeightwest98 = _lstsageInfoWest.Where(o => o.ProductType == 98).FirstOrDefault();
                var sixeightwest104 = _lstsageInfoWest.Where(o => o.ProductType == 104).FirstOrDefault();

                SixEightPrintCount = (sixeightwest != null ? sixeightwest.Printed : 0) + (sixeightwest3 != null ? sixeightwest3.Printed : 0) + (sixeightwest4 != null ? sixeightwest4.Printed : 0) + (sixeightwest5 != null ? sixeightwest5.Printed : 0) + (sixeightwest30 != null ? sixeightwest30.Printed : 0) + (sixeightwest98 != null ? sixeightwest98.Printed : 0) + (sixeightwest104 != null ? sixeightwest104.Printed : 0);

                Int64 SixEightAllPrintCount = 2 * ((sixeightwest != null ? sixeightwest.Printed : 0) + (sixeightwest3 != null ? sixeightwest3.Printed : 0) + (sixeightwest4 != null ? sixeightwest4.Printed : 0) + (sixeightwest5 != null ? sixeightwest5.Printed : 0) + (sixeightwest104 != null ? sixeightwest104.Printed : 0)) + (sixeightwest30 != null ? sixeightwest30.Printed : 0) + (sixeightwest98 != null ? sixeightwest98.Printed : 0);
                // var eighttenwest = _lstsageInfoWest.Where(o => o.ProductType == 2).FirstOrDefault();
                txtAuto6X8Westage.Text = ((EndNumber - StartingNumber) - SixEightAllPrintCount) > 0 ? ((EndNumber - StartingNumber) - SixEightAllPrintCount).ToString() : "0";
                //txt8X10Westage.Text = ((EndNumber - StartingNumber) - eighttenwest.Printed).ToString();
                // txt8X10Westage.Text = sixeightwest.Printed.ToString();
                //  txt8X10Westage.Text = eighttenwest.Printed.ToString();

            }
            else
            {
                txtAuto6X8Westage.Text = (EndNumber - StartingNumber) > 0 ? (EndNumber - StartingNumber).ToString() : "0";
            }
        }
        public void SetEightTenAutoWestage(DateTime FromDate, DateTime ToDate, int SubStoreID, Int64 StartingNumber, Int64 EndNumber)
        {
            List<SageInfoWestage> _lstsageInfoWest = (new SageBusiness()).ProductTypeWestage(FromDate, ToDate, SubStoreID);
            if (_lstsageInfoWest.Count() > 0)
            {
                //var sixeightwest = _lstsageInfoWest.Where(o => o.ProductType == 1).FirstOrDefault();
                var eighttenwest = _lstsageInfoWest.Where(o => o.ProductType == 2).FirstOrDefault();
                if (eighttenwest != null)
                    EightTenPrintCount = eighttenwest.Printed;
                else
                    EightTenPrintCount = 0;
                //txt6X8Westage.Text = ((EndNumber - StartingNumber) - sixeightwest.Printed).ToString();
                txtAuto8X10Westage.Text = ((EndNumber - StartingNumber) - EightTenPrintCount) > 0 ? ((EndNumber - StartingNumber) - EightTenPrintCount).ToString() : "0";

                // txt8X10Westage.Text = sixeightwest.Printed.ToString();
                //  txt8X10Westage.Text = eighttenwest.Printed.ToString();

            }
            else
            {
                txtAuto8X10Westage.Text = (EndNumber - StartingNumber) > 0 ? (EndNumber - StartingNumber).ToString() : "0";
            }
        }

        public void SetSixEightWestage(DateTime FromDate, DateTime ToDate, int SubStoreID, Int64 StartingNumber, Int64 EndNumber)
        {
            List<SageInfoWestage> _lstsageInfoWest = (new SageBusiness()).ProductTypeWestage(FromDate, ToDate, SubStoreID);
            if (_lstsageInfoWest.Count() > 0)
            {
                var sixeightwest = _lstsageInfoWest.Where(o => o.ProductType == 1).FirstOrDefault();
                var sixeightwest3 = _lstsageInfoWest.Where(o => o.ProductType == 3).FirstOrDefault();
                var sixeightwest4 = _lstsageInfoWest.Where(o => o.ProductType == 4).FirstOrDefault();
                var sixeightwest5 = _lstsageInfoWest.Where(o => o.ProductType == 5).FirstOrDefault();
                var sixeightwest30 = _lstsageInfoWest.Where(o => o.ProductType == 30).FirstOrDefault();
                var sixeightwest98 = _lstsageInfoWest.Where(o => o.ProductType == 98).FirstOrDefault();
                var sixeightwest104 = _lstsageInfoWest.Where(o => o.ProductType == 104).FirstOrDefault();

                SixEightPrintCount = (sixeightwest != null ? sixeightwest.Printed : 0) + (sixeightwest3 != null ? sixeightwest3.Printed : 0) + (sixeightwest4 != null ? sixeightwest4.Printed : 0) + (sixeightwest5 != null ? sixeightwest5.Printed : 0) + (sixeightwest30 != null ? sixeightwest30.Printed : 0) + (sixeightwest98 != null ? sixeightwest98.Printed : 0) + (sixeightwest104 != null ? sixeightwest104.Printed : 0);


                Int64 SixEightAllPrintCount = 2 * ((sixeightwest != null ? sixeightwest.Printed : 0) + (sixeightwest3 != null ? sixeightwest3.Printed : 0) + (sixeightwest4 != null ? sixeightwest4.Printed : 0) + (sixeightwest5 != null ? sixeightwest5.Printed : 0) + (sixeightwest104 != null ? sixeightwest104.Printed : 0)) + (sixeightwest30 != null ? sixeightwest30.Printed : 0) + (sixeightwest98 != null ? sixeightwest98.Printed : 0);

                txt6X8Westage.Text = ((EndNumber - StartingNumber) - SixEightAllPrintCount) > 0 ? ((EndNumber - StartingNumber) - SixEightAllPrintCount).ToString() : "0";
                //txt8X10Westage.Text = ((EndNumber - StartingNumber) - eighttenwest.Printed).ToString();
                // txt8X10Westage.Text = sixeightwest.Printed.ToString();
                //  txt8X10Westage.Text = eighttenwest.Printed.ToString();

            }
            else
            {
                txt6X8Westage.Text = (EndNumber - StartingNumber) > 0 ? (EndNumber - StartingNumber).ToString() : "0";
            }
        }
        public void SetEightTenWestage(DateTime FromDate, DateTime ToDate, int SubStoreID, Int64 StartingNumber, Int64 EndNumber)
        {
            List<SageInfoWestage> _lstsageInfoWest = (new SageBusiness()).ProductTypeWestage(FromDate, ToDate, SubStoreID);
            if (_lstsageInfoWest.Count() > 0)
            {
                //var sixeightwest = _lstsageInfoWest.Where(o => o.ProductType == 1).FirstOrDefault();
                var eighttenwest = _lstsageInfoWest.Where(o => o.ProductType == 2).FirstOrDefault();
                if (eighttenwest != null)
                    EightTenPrintCount = eighttenwest.Printed;
                else
                    EightTenPrintCount = 0;
                //txt6X8Westage.Text = ((EndNumber - StartingNumber) - sixeightwest.Printed).ToString();
                txt8X10Westage.Text = ((EndNumber - StartingNumber) - EightTenPrintCount) > 0 ? ((EndNumber - StartingNumber) - EightTenPrintCount).ToString() : "0";

                // txt8X10Westage.Text = sixeightwest.Printed.ToString();
                //  txt8X10Westage.Text = eighttenwest.Printed.ToString();

            }
            else
            {
                txt8X10Westage.Text = (EndNumber - StartingNumber) > 0 ? (EndNumber - StartingNumber).ToString() : "0";
            }
        }
        // BY KCB ON 08 AUG 2019 FOR storing panorama printer inventory
        public void Set6900Westage(DateTime FromDate, DateTime ToDate, int SubStoreID, Int64 StartingNumber, Int64 EndNumber)
        {
            List<SageInfoWestage> _lstsageInfoWest = (new SageBusiness()).ProductTypeWestage(FromDate, ToDate, SubStoreID);
            if (_lstsageInfoWest.Count() > 0)
            {
                var K6900Westage = _lstsageInfoWest.Where(o => o.ProductType == 1).FirstOrDefault();
                var K6900Westage3 = _lstsageInfoWest.Where(o => o.ProductType == 3).FirstOrDefault();
                var K6900Westage4 = _lstsageInfoWest.Where(o => o.ProductType == 4).FirstOrDefault();
                var K6900Westage5 = _lstsageInfoWest.Where(o => o.ProductType == 5).FirstOrDefault();
                var K6900Westage30 = _lstsageInfoWest.Where(o => o.ProductType == 30).FirstOrDefault();
                var K6900Westage101 = _lstsageInfoWest.Where(o => o.ProductType == 101).FirstOrDefault();
                var K6900Westage102 = _lstsageInfoWest.Where(o => o.ProductType == 102).FirstOrDefault();
                var K6900Westage103 = _lstsageInfoWest.Where(o => o.ProductType == 103).FirstOrDefault();
                var K6900Westage104 = _lstsageInfoWest.Where(o => o.ProductType == 104).FirstOrDefault();
                var K6900Westage105 = _lstsageInfoWest.Where(o => o.ProductType == 105).FirstOrDefault();
                var K6900Westage123 = _lstsageInfoWest.Where(o => o.ProductType == 123).FirstOrDefault();
                var K6900Westage124 = _lstsageInfoWest.Where(o => o.ProductType == 124).FirstOrDefault();
                var K6900Westage125 = _lstsageInfoWest.Where(o => o.ProductType == 125).FirstOrDefault();



                long allK6900Westage =
                    (K6900Westage != null ? K6900Westage.Printed : 0) +
                     (K6900Westage3 != null ? K6900Westage3.Printed : 0) +
                     (K6900Westage4 != null ? K6900Westage4.Printed : 0) +
                      (K6900Westage5 != null ? K6900Westage5.Printed : 0) +
                    (K6900Westage30 != null ? K6900Westage30.Printed : 0) +
                     (K6900Westage101 != null ? K6900Westage101.Printed : 0) +
                    (K6900Westage102 != null ? K6900Westage102.Printed : 0) +
                    (K6900Westage103 != null ? K6900Westage103.Printed : 0) +
                    (K6900Westage104 != null ? K6900Westage104.Printed : 0) +
                    (K6900Westage105 != null ? K6900Westage105.Printed : 0) +
                    (K6900Westage123 != null ? K6900Westage123.Printed : 0) +
                    (K6900Westage124 != null ? K6900Westage124.Printed : 0) +
                    (K6900Westage125 != null ? K6900Westage125.Printed : 0);

                txtAuto6900Westage.Text = ((EndNumber - StartingNumber) - allK6900Westage) > 0 ? ((EndNumber - StartingNumber) - allK6900Westage).ToString() : "0";


            }
            else
            {
                //StartingNumber = K6900AutoClosingNumber - K6900AutoStartingNumber;
                txtAuto6900Westage.Text = (EndNumber - StartingNumber) > 0 ? (EndNumber - StartingNumber).ToString() : "0";
            }

        }
        public void SetPosterWestage(DateTime FromDate, DateTime ToDate, int SubStoreID, Int64 StartingNumber, Int64 EndNumber)
        {
            List<SageInfoWestage> _lstsageInfoWest = (new SageBusiness()).ProductTypeWestage(FromDate, ToDate, SubStoreID);
            if (_lstsageInfoWest.Count() > 0)
            {
                var PosterPrint = _lstsageInfoWest.Where(o => o.ProductType == 6).FirstOrDefault();
                if (PosterPrint != null)
                {
                    PosterPrintCount = PosterPrint.Printed;
                }
                else
                {
                    PosterPrintCount = 0;
                }
                if (PosterPrint != null)
                    txtPosterWestage.Text = ((EndNumber - StartingNumber) - PosterPrint.Printed) > 0 ? ((EndNumber - StartingNumber) - PosterPrint.Printed).ToString() : "0";
                else
                    txtPosterWestage.Text = (EndNumber - StartingNumber) > 0 ? (EndNumber - StartingNumber).ToString() : "0";
            }
            else
            {
                txtPosterWestage.Text = (EndNumber - StartingNumber) > 0 ? (EndNumber - StartingNumber).ToString() : "0";
            }
        }

        //private void txt6X8CloseNumber_SelectionChanged(object sender, RoutedEventArgs e)
        //{
        //    //SetSixEightWestage(FromDate, ToDate, SubStoreID, SixEightStartingNumber, (txt6X8CloseNumber.Text == "") ? 0 : Convert.ToInt64(txt6X8CloseNumber.Text));
        //}

        //private void txt8X10CloseNumber_SelectionChanged(object sender, RoutedEventArgs e)
        //{
        //   // SetEightTenWestage(FromDate, ToDate, SubStoreID, EightTenStartingNumber, (txt8X10CloseNumber.Text == "") ? 0 : Convert.ToInt64(txt8X10CloseNumber.Text));
        //}

        //private void txtPosterCloseNumber_SelectionChanged(object sender, RoutedEventArgs e)
        //{
        //    //SetPosterWestage(FromDate, ToDate, SubStoreID, PosterStartingNumber, (txtPosterCloseNumber.Text == "") ? 0 : Convert.ToInt64(txtPosterCloseNumber.Text));
        //}

        //private void txt8X10Westage_SelectionChanged(object sender, RoutedEventArgs e)
        //{
        //    SetEightTenWestage(FromDate, ToDate, SubStoreID, EightTenStartingNumber, Convert.ToInt32(txt8X10CloseNumber.Text));
        //}
        //private void btn_Click(object sender, RoutedEventArgs e)
        //{
        //    Button _objbtn = (Button)sender;
        //    //_objbtn = (Button)sender;
        //    switch (_objbtn.Content.ToString())
        //    {
        //        case "ENTER":
        //            {
        //                KeyBorder.Visibility = Visibility.Hidden;
        //                break;
        //            }
        //        //case "SPACE":
        //        //    {
        //        //        controlon.Text = controlon.Text + " ";
        //        //        break;
        //        //    }
        //        case "CLOSE":
        //            {
        //                KeyBorder.Visibility = Visibility.Hidden;
        //                break;
        //            }
        //        case "Back":
        //            {
        //                TextBox objtxt = (TextBox)(controlon);
        //                if (controlon.Text.Length > 0)
        //                {
        //                    controlon.Text = controlon.Text.Remove(controlon.Text.Length - 1, 1);
        //                }
        //                break;
        //            }
        //        default:
        //            {
        //                if (controlon.Text.Count() < 10)
        //                {
        //                    string[] numbers = { "0", "1", "2", "3", "4", "5", "6", "7", "8", "9" };
        //                    //var b = Array.contains(a, "red");
        //                    int pos = Array.IndexOf(numbers, _objbtn.Content);
        //                    if (pos > -1)
        //                    {
        //                        controlon.Text = controlon.Text + _objbtn.Content;
        //                    }
        //                }
        //            }
        //            break;
        //    }
        //}



        private void txtPosterCloseNumber_GotFocus(object sender, RoutedEventArgs e)
        {
            controlon = (TextBox)sender;
            // KeyBorder.Visibility = Visibility.Visible;
            Calculation();
        }

        private void txt6X8Folder_GotFocus(object sender, RoutedEventArgs e)
        {
            controlon = (TextBox)sender;
            // KeyBorder.Visibility = Visibility.Visible;
            Calculation();
        }

        private void txt8X10Folder_GotFocus(object sender, RoutedEventArgs e)
        {
            controlon = (TextBox)sender;
            // KeyBorder.Visibility = Visibility.Visible;
            Calculation();
        }

        private void txt6X8FolderCash_GotFocus(object sender, RoutedEventArgs e)
        {
            controlon = (TextBox)sender;
            //  KeyBorder.Visibility = Visibility.Visible;
            Calculation();
        }

        private void txt8X10FolderCash_GotFocus(object sender, RoutedEventArgs e)
        {
            controlon = (TextBox)sender;
            // KeyBorder.Visibility = Visibility.Visible;
            Calculation();
        }

        private void txtRovingTicket_GotFocus(object sender, RoutedEventArgs e)
        {
            controlon = (TextBox)sender;
            // KeyBorder.Visibility = Visibility.Visible;
            Calculation();
        }

        private void txtRovingBand_GotFocus(object sender, RoutedEventArgs e)
        {
            controlon = (TextBox)sender;
            // KeyBorder.Visibility = Visibility.Visible;
            Calculation();
        }

        private void txtPlasticBag_GotFocus(object sender, RoutedEventArgs e)
        {
            controlon = (TextBox)sender;
            //  KeyBorder.Visibility = Visibility.Visible;
            Calculation();
        }

        private void txt6X8CloseNumber_GotFocus(object sender, RoutedEventArgs e)
        {
            controlon = (TextBox)sender;
            // KeyBorder.Visibility = Visibility.Visible;
            Calculation();
        }

        private void txt8X10CloseNumber_GotFocus(object sender, RoutedEventArgs e)
        {
            controlon = (TextBox)sender;
            // KeyBorder.Visibility = Visibility.Visible;
            Calculation();
        }

        private void txt6X8CloseNumber_LostFocus(object sender, RoutedEventArgs e)
        {
            if (txt6X8CloseNumber.Text != "")
                SetSixEightWestage(FromDate, ToDate, SubStoreID, SixEightStartingNumber, (txt6X8CloseNumber.Text.Trim() == "") ? 0 : Convert.ToInt64(txt6X8CloseNumber.Text));
        }

        private void txt8X10CloseNumber_LostFocus(object sender, RoutedEventArgs e)
        {
            if (txt8X10CloseNumber.Text != "")
                SetEightTenWestage(FromDate, ToDate, SubStoreID, EightTenStartingNumber, (txt8X10CloseNumber.Text.Trim() == "") ? 0 : Convert.ToInt64(txt8X10CloseNumber.Text));
        }

        private void txtPosterCloseNumber_LostFocus(object sender, RoutedEventArgs e)
        {
            if (txtPosterCloseNumber.Text != "")
                SetPosterWestage(FromDate, ToDate, SubStoreID, PosterStartingNumber, (txtPosterCloseNumber.Text == "") ? 0 : Convert.ToInt64(txtPosterCloseNumber.Text));
        }

        private void txt6X8CloseNumber_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Space)
            {
                e.Handled = true;
            }
        }

        private void Calculation()
        {
            //if (txtPosterCloseNumber.Text != "")
            SetPosterWestage(FromDate, ToDate, SubStoreID, PosterStartingNumber, (txtPosterCloseNumber.Text == "") ? 0 : Convert.ToInt64(txtPosterCloseNumber.Text));
            //if (txt8X10CloseNumber.Text != "")
            SetEightTenWestage(FromDate, ToDate, SubStoreID, EightTenStartingNumber, (txt8X10CloseNumber.Text.Trim() == "") ? 0 : Convert.ToInt64(txt8X10CloseNumber.Text));
            // if (txt6X8CloseNumber.Text != "")
            SetSixEightWestage(FromDate, ToDate, SubStoreID, SixEightStartingNumber, (txt6X8CloseNumber.Text.Trim() == "") ? 0 : Convert.ToInt64(txt6X8CloseNumber.Text));
            // BY KCB ON 08 AUG 2019 FOR storing panorama printer inventory
            Set6900Westage(FromDate, ToDate, SubStoreID, K6900AutoStartingNumber, (txt6900AutoClosing.Text==""?(txt6900ClosingNumber.Text.Trim() == "" ? 0 : Convert.ToInt64(txt6900ClosingNumber.Text)): Convert.ToInt64(txt6900AutoClosing.Text)));
            //Endk
        }

        private void btnAdd_Click(object sender, RoutedEventArgs e)
        {
            //here we will put the code to fill the grid
            if (cmbInventory.SelectedIndex >= 0)
            {
                if (txtConsume.Text != "" && txtConsume.Text != "0")
                {

                    var list = _objInventory.Where(t => t.AccessoryID == Convert.ToInt64(cmbInventory.SelectedValue)).ToList();
                    if (list.Count == 0)
                    {

                        InventoryConsumables objItem = new InventoryConsumables();
                        objItem.AccessoryID = Convert.ToInt64(cmbInventory.SelectedValue);
                        objItem.AccessoryName = cmbInventory.Text;
                        objItem.ConsumeValue = Convert.ToInt64(txtConsume.Text);

                        _objInventory.Add(objItem);
                        dgInventory.ItemsSource = _objInventory;
                        dgInventory.UpdateLayout();

                        // MessageBox.Show("Inventory value added successfully.", "DEI");
                        txtConsume.Text = "";

                        //DataGridRow row = (DataGridRow)dgInventory.ItemContainerGenerator.ContainerFromIndex(_objInventory.Count - 1);
                        //if (row != null)
                        //{
                        //    TextBlock txtInventory = FindByName("txtInventory", row) as TextBlock;
                        //    TextBlock txtInventoryConsume = FindByName("txtInventoryConsume", row) as TextBlock;
                        //    txtInventory.Text = cmbInventory.Text;//CurrencySymbol.DG_Currency_Symbol.ToString() + " " + Math.Round(tbKVL.Text.ToDouble(), 3).ToString("N2");
                        //    txtInventoryConsume.Text = txtConsume.Text;
                        //}
                    }
                    else
                    {
                        MessageBox.Show("Selected inventory is already added", "DEI");
                    }
                }
                else
                {
                    MessageBox.Show("Please enter a valid inventory Consumable", "DEI");
                    txtConsume.Focus();
                }
            }
            else
            {
                MessageBox.Show("Please select inventory", "DEI");
                cmbInventory.Focus();
            }
        }

        private void btndeleteInventory_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                InventoryConsumables ToBeDeleteditem = (InventoryConsumables)dgInventory.CurrentItem;
                var item = (from lineitem in _objInventory
                            where lineitem.AccessoryID == ToBeDeleteditem.AccessoryID
                            select lineitem).FirstOrDefault();

                if (item != null)
                    _objInventory.Remove(item);


            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }
        private FrameworkElement FindByName(string name, FrameworkElement root)
        {
            Stack<FrameworkElement> tree = new Stack<FrameworkElement>();
            tree.Push(root);
            while (tree.Count > 0)
            {
                FrameworkElement current = tree.Pop(); // root is null
                if (current.Name == name)
                    return current;

                int count = VisualTreeHelper.GetChildrenCount(current);
                for (int SupplierCounter = 0; SupplierCounter < count; ++SupplierCounter)
                {
                    DependencyObject child = VisualTreeHelper.GetChild(current, SupplierCounter);
                    if (child is FrameworkElement)
                        tree.Push((FrameworkElement)child);
                }
            }
            return null;
        }

        private void txtConsume_GotFocus(object sender, RoutedEventArgs e)
        {
            controlon = (TextBox)sender;
            // KeyBorder.Visibility = Visibility.Visible;
        }


        private void txtConsume_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Space)
            {
                e.Handled = true;
            }
        }

        private void txtConsume_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = !IsTextAllowed(e.Text.Trim());
        }
        // BY KCB ON 08 AUG 2019 FOR storing panorama printer inventory
        private void txt6900CloseNumber_LostFocus(object sender, RoutedEventArgs e)
        {
            //if (txt6900ClosingNumber.Text != "")
            //    Set6900Westage(FromDate, ToDate, SubStoreID, K6900AutoStartingNumber, (txt6900ClosingNumber.Text == "") ? 0 : Convert.ToInt64(txt6900ClosingNumber.Text));
            Set6900Westage(FromDate, ToDate, SubStoreID, K6900AutoStartingNumber, (txt6900AutoClosing.Text == "" ? (txt6900ClosingNumber.Text.Trim() == "" ? 0 : Convert.ToInt64(txt6900ClosingNumber.Text)) : Convert.ToInt64(txt6900AutoClosing.Text)));
        }
    }
    //public class InventoryItem
    //{
    //    public string InventoryName { get; set; }
    //    public Int32 InventoryID { get; set; }
    //    public Int64 InventoryConsume { get; set; }
    //}

}
