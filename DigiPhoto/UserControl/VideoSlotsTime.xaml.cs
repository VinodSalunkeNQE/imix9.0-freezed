﻿using DigiPhoto.IMIX.Business;
using DigiPhoto.IMIX.Model;
using DigiPhoto.Manage;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;


namespace DigiPhoto
{
    /// <summary>
    /// Interaction logic for VideoSlotsTime.xaml
    /// </summary>
    public partial class VideoSlotsTime : UserControl
    {
        string a;
        public long VideoTemplateId = 0;
        public long VideoLength = 0;
        public string VideoName = "";
        private UIElement _parent;
        List<VideoTemplateInfo.VideoSlot> slotList ;
        public bool isGuestVideoTemplate = false;
        public void SetParent(UIElement parent)
        {
            _parent = (UIElement)parent;
            slotList = new List<VideoTemplateInfo.VideoSlot> ();
            if (!isGuestVideoTemplate)
            {
                GetVideoSlotsTimeFrames(VideoTemplateId);
            }
            txtSlotFrameTime.MaxLength = VideoLength.ToString().Length;
           // tbVName.Text = VideoName;
            tbVLength.Text = VideoLength.ToString();
        }

        public VideoSlotsTime()
        {           
            InitializeComponent();
        }

        private void GetVideoSlotsTimeFrames(long VideoTemplateId)
        {
            try
            {
                //if list is null get from DB
                if (slotList == null || slotList.Count==0)
                {
                    slotList = new List<VideoTemplateInfo.VideoSlot>();
                    ConfigBusiness configBusiness = new ConfigBusiness();
                    slotList = configBusiness.GetVideoTemplate(VideoTemplateId).videoSlots;
                    if (slotList == null)
                        slotList = new List<VideoTemplateInfo.VideoSlot>();
                }
                DGManageVideo.ItemsSource = slotList.OrderBy(o => o.FrameTimeIn);
                DGManageVideo.Items.Refresh();

            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }

        private void btnAdd_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (CheckSlotValidations())
                {
                    long FrameDiff = 0;
                    long FrameTimeIn = 0;
                    long SlotInterval = 0;
                    int PhotoDisplayTime = int.Parse(txtPhotoDisplayTime.Text.Trim());
                    
                    

                    if (!string.IsNullOrEmpty(txtSlotFrameTime.Text))
                    {
                        FrameTimeIn = Convert.ToInt64(txtSlotFrameTime.Text.Trim());
                    }
                    if (!string.IsNullOrEmpty(txtSlotInterval.Text.Trim()))
                    {

                        SlotInterval = Convert.ToInt64(txtSlotInterval.Text.Trim());
                        FrameDiff = SlotInterval;
                    }
                    if (string.IsNullOrEmpty(txtSlotInterval.Text) && (slotList.Where(o => o.FrameTimeIn == FrameTimeIn).Count() > 0))
                    {
                        MessageBox.Show("Same frame time already exists.", "DigiPhoto", MessageBoxButton.OK);
                        return;
                    }
                    if (FrameTimeIn > VideoLength)
                    {
                        MessageBox.Show("Slot time frame should be less than the total video length " + VideoLength.ToString() + "s .", "DigiPhoto", MessageBoxButton.OK);
                        return;
                    }
                    if (SlotInterval > VideoLength)
                    {
                        MessageBox.Show("Slot interval should be less than the total video length " + VideoLength.ToString() + "s .", "DigiPhoto", MessageBoxButton.OK);
                        return;
                    }

                    if (FrameTimeIn + PhotoDisplayTime > VideoLength)
                    {
                        MessageBox.Show("The output video time exceeds the template video length " + VideoLength.ToString() + "s .", "DigiPhoto", MessageBoxButton.OK);
                        return;
                    }
                    if (SlotInterval + PhotoDisplayTime > VideoLength)
                    {
                        MessageBox.Show("The output video time exceeds the template video length " + VideoLength.ToString() + "s .", "DigiPhoto", MessageBoxButton.OK);
                        return;
                    }

                    else
                    {
                        if (!string.IsNullOrEmpty(txtSlotInterval.Text))
                        {
                            slotList.Clear();
                            if (PhotoDisplayTime > SlotInterval)
                            {
                                MessageBox.Show("Photo display time should be less than slot interval", "DigiPhoto", MessageBoxButton.OK);
                                return;
                            }
                            else
                            {
                                while (VideoLength > SlotInterval + PhotoDisplayTime)
                                {
                                    slotList.Add(new VideoTemplateInfo.VideoSlot(slotList.Count() + 1, SlotInterval, PhotoDisplayTime));
                                    SlotInterval += FrameDiff;
                                }
                            }
                            
                        }
                        else
                        {
                            //Manualy add intervals
                            if (slotList.Count == 0)
                            {
                                slotList.Add(new VideoTemplateInfo.VideoSlot(slotList.Count() + 1, FrameTimeIn, PhotoDisplayTime));
                            }
                            else
                            {
                                VideoTemplateInfo.VideoSlot vs = slotList.OrderBy(o=>o.FrameTimeIn).Where(o => o.FrameTimeIn < FrameTimeIn).LastOrDefault();
                                VideoTemplateInfo.VideoSlot vs1 = slotList.OrderBy(o => o.FrameTimeIn).ToList().FirstOrDefault();
                                if (vs != null)
                                {
                                    if (FrameTimeIn >= vs.FrameTimeIn + vs.PhotoDisplayTime)
                                    {
                                        slotList.Add(new VideoTemplateInfo.VideoSlot(slotList.Count() + 1, FrameTimeIn, PhotoDisplayTime));
                                    }                                   
                                    else
                                    {
                                        MessageBox.Show("There is already a slot in given time frame.", "DigiPhoto", MessageBoxButton.OK);
                                    }
                                }
                                else if (vs1!=null)
                                {
                                    if(FrameTimeIn + PhotoDisplayTime <= vs1.FrameTimeIn)
                                    {
                                        slotList.Add(new VideoTemplateInfo.VideoSlot(slotList.Count() + 1, FrameTimeIn, PhotoDisplayTime));
                                    }
                                    else
                                    {
                                        MessageBox.Show("There is already a slot in given time frame.", "DigiPhoto", MessageBoxButton.OK); ;//,"\nAdd new slot after ";// + (videoslot[videoslot.Count - 1].FrameTimeIn + videoslot[videoslot.Count - 1].PhotoDisplayTime) + " sec or before " + videoslot[0].FrameTimeIn + " sec.", "DigiPhoto", MessageBoxButton.OK);
                                    }
                                }
                                else
                                {
                                    slotList.Add(new VideoTemplateInfo.VideoSlot(slotList.Count() + 1, FrameTimeIn, PhotoDisplayTime));
                                }
                            }
                            
                        }
                        GetVideoSlotsTimeFrames(VideoTemplateId);
                        ResetControls();
                    }
                }

            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }

        private void btnDeleteSlot_Click(object sender, RoutedEventArgs e)
        {
            Button btnSender = (Button)sender;
            long videoSlotId = long.Parse(btnSender.Tag.ToString());
            MessageBoxResult response = MessageBox.Show("Do you want to delete record?", "Digiphoto", MessageBoxButton.YesNo);
            if (response == MessageBoxResult.Yes)
            {
                slotList.RemoveAll(o => o.VideoSlotId == videoSlotId);
                GetVideoSlotsTimeFrames(VideoTemplateId);
                MessageBox.Show("Item deleted Successfully", "DigiPhoto", MessageBoxButton.OK);
            }
            ResetControls();
        }


        /// <summary>
        /// Checks the border validations.
        /// </summary>
        /// <returns></returns>
        private bool CheckSlotValidations()
        {
            if (string.IsNullOrEmpty(txtSlotFrameTime.Text.Trim()) && string.IsNullOrEmpty(txtSlotInterval.Text.Trim()))
            {
                MessageBox.Show("Enter value for slot time frame or slot interval.", "DigiPhoto", MessageBoxButton.OK);
                return false;
            }
            else if (string.IsNullOrEmpty(txtPhotoDisplayTime.Text.Trim()) || (Convert.ToInt32(txtPhotoDisplayTime.Text) <= 0))
            {
                MessageBox.Show("Photo display time can not be zero or null.", "DigiPhoto", MessageBoxButton.OK);
                return false;
            }
            else if (!IsNumeric(txtSlotFrameTime.Text.Trim()) && !IsNumeric(txtSlotInterval.Text.Trim()))
            {
                MessageBox.Show("Only numeric values are allowed for slot time frame and slot interval.", "DigiPhoto", MessageBoxButton.OK);
                return false;
            }
            else if (string.IsNullOrEmpty(txtSlotFrameTime.Text.Trim()) && Convert.ToInt32(txtSlotInterval.Text) <= 0)
            {
                MessageBox.Show("Slot interval can not be zero.", "DigiPhoto", MessageBoxButton.OK);
                return false;
            }
            else
            {
                return true;
            }
        }

        private bool SaveSlotTimeFrameDetails(long videoTemplateId, List<VideoTemplateInfo.VideoSlot> videoSlots)
        {
            try
            {
                ConfigBusiness configBusiness = new ConfigBusiness();
                VideoTemplateInfo videoTemplate = new VideoTemplateInfo();
                videoTemplate.VideoTemplateId = videoTemplateId;
                videoTemplate.videoSlots = videoSlots;

                return configBusiness.SaveVideoSlot(videoTemplate);
            }
            catch
            {
                return false;
            }
        }

        private bool IsNumeric(string text)
        {
            try
            {
                int output;
                return int.TryParse(text, out output);
            }
            catch
            {
                return false;
            }
        }

        private void btnClose_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (!isGuestVideoTemplate)
                {
                    bool isSaved = SaveSlotTimeFrameDetails(VideoTemplateId, slotList);
                    slotList.Clear();
                }
                else
                {
                    ((VideoEditor)_parent).slotList = slotList;
                    ((VideoEditor)_parent).LoadGuestVideoSlots();
                    DGManageVideo.ItemsSource = null;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
            HideHandlerDialog();
        }

        private void HideHandlerDialog()
        {
            Visibility = Visibility.Collapsed;
            _parent.IsEnabled = true;
        }

        private void txtSlotFrameTime_KeyUp(object sender, KeyEventArgs e)
        {
            if (string.IsNullOrEmpty(txtSlotFrameTime.Text))
            {
                txtSlotInterval.IsEnabled = true;
            }
            else
            {
                txtSlotInterval.IsEnabled = false;
            }


        }

        private void txtSlotInterval_KeyUp(object sender, KeyEventArgs e)
        {
            if (string.IsNullOrEmpty(txtSlotInterval.Text))
            {
                txtSlotFrameTime.IsEnabled = true;
            }
            else
            {
                txtSlotFrameTime.IsEnabled = false;
            }
        }

        private void txtSlotFrameTime_MouseLeave(object sender, MouseEventArgs e)
        {
            if (string.IsNullOrEmpty(txtSlotFrameTime.Text))
            {
                txtSlotInterval.IsEnabled = true;
            }
        }

        private void txtSlotInterval_MouseLeave(object sender, MouseEventArgs e)
        {
            if (string.IsNullOrEmpty(txtSlotInterval.Text))
            {
                txtSlotFrameTime.IsEnabled = true;
            }
        }

        private void txtSlotFrameTime_KeyDown(object sender, KeyEventArgs e)
        {
            if ((e.Key == Key.Tab) && string.IsNullOrEmpty(txtSlotFrameTime.Text))
            {
                txtSlotInterval.IsEnabled = true;
                txtSlotFrameTime.IsEnabled = false;

            }
        }
        private void ResetControls()
        {
            txtPhotoDisplayTime.Text = string.Empty;
            txtSlotFrameTime.Text = string.Empty;
            txtSlotInterval.Text = string.Empty;
            txtSlotFrameTime.IsEnabled = true;
            txtSlotInterval.IsEnabled = true;
        }
        private void txtnumber_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (string.IsNullOrEmpty(((TextBox)sender).Text))

                a = "";
            else
            {
                double num = 0;
                bool success = double.TryParse(((TextBox)sender).Text, out num);
                if (success & num >= 0)
                {
                    ((TextBox)sender).Text.Trim();
                    a = ((TextBox)sender).Text;
                }
                else
                {
                    ((TextBox)sender).Text = a;
                    ((TextBox)sender).SelectionStart = ((TextBox)sender).Text.Length;
                }
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            slotList.Clear();
            DGManageVideo.ItemsSource = slotList;
            DGManageVideo.Items.Refresh();
        }

        private void UserControl_Unloaded(object sender, RoutedEventArgs e)
        {
            UnsubscribeEvents();
        }
        private void UnsubscribeEvents()
        {
            txtSlotFrameTime.KeyUp -= new KeyEventHandler(txtSlotFrameTime_KeyUp);
            txtSlotFrameTime.MouseLeave -= new MouseEventHandler(txtSlotFrameTime_MouseLeave);
            txtSlotFrameTime.KeyDown -= new KeyEventHandler(txtSlotFrameTime_KeyDown);
            txtSlotFrameTime.TextChanged -= new TextChangedEventHandler(txtnumber_TextChanged);
            txtSlotInterval.KeyUp -= new KeyEventHandler(txtSlotInterval_KeyUp);
            txtSlotInterval.MouseLeave -= new MouseEventHandler(txtSlotInterval_MouseLeave);
            txtSlotInterval.TextChanged -= new TextChangedEventHandler(txtnumber_TextChanged);
            txtPhotoDisplayTime.TextChanged -= new TextChangedEventHandler(txtnumber_TextChanged);
            btnAdd.Click -= new RoutedEventHandler(btnAdd_Click);
            btnClear.Click -= new RoutedEventHandler(Button_Click);
            btnClose.Click -= new RoutedEventHandler(btnClose_Click);
            if(slotList!=null)
            if (slotList.Count > 0)
                slotList.Clear();
        }
       
    }
}
