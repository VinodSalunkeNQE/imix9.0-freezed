﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace DigiPhoto
{
    /// <summary>
    /// Interaction logic for WhatsAppStatistics.xaml
    /// </summary>
    public partial class WhatsAppStatistics : UserControl
    {
        public WhatsAppStatistics()
        {
            InitializeComponent();
            Visibility = Visibility.Hidden;


        }
        private UIElement _parent;
        private bool _result = false;
        public event EventHandler ExecuteMethod;
        public static  int SucessOrderCounts { get; set; }
        public static  int ImageFailedCounts { get; set; }
        public static  int ImageUploadedCounts { get; set; }
        public static  int TotalOrderCounts { get; set; }



        public void ShowPopUp(int SucessOrderCount, int ImageFailedCount, int ImageUploadedCount, int TotalOrderCount)
        {
            //txtSucessOrder.Text = SucessOrderCount.ToString();
            //txtTotalImageFailed.Text = ImageFailedCount.ToString();
            //txtTotalImageUploaded.Text = ImageUploadedCount.ToString();
            //txtTotalOrderCount.Text = TotalOrderCount.ToString();


        }
        public void SetParent(UIElement parent)
        {
            _parent = (UIElement)parent;

        }
        //public bool ShowPanHandlerDialog(int SucessOrderCount, int ImageFailedCount, int ImageUploadedCount, int TotalOrderCount)
        // {
        //     Visibility = Visibility.Visible;
        //     SucessOrderCounts = SucessOrderCount;
        //     ImageFailedCounts = ImageFailedCount;
        //     ImageUploadedCounts = ImageUploadedCount;
        //     TotalOrderCounts = TotalOrderCount;
        //     ShowPopUp(SucessOrderCounts, ImageFailedCounts, ImageUploadedCounts, TotalOrderCounts);

        //     //_parent.IsEnabled = false;
        //     return _result;
        // }


        public bool ShowPanHandlerDialog()
        {
            Visibility = Visibility.Visible;
            txtSucessOrder.Text = SucessOrderCounts.ToString();
            txtTotalImageFailed.Text = ImageFailedCounts.ToString();
            txtTotalImageUploaded.Text = ImageUploadedCounts.ToString();
            txtTotalOrderCount.Text = TotalOrderCounts.ToString();
            //_parent.IsEnabled = false;
            return _result;
        }


        private void HideHandlerDialog()
        {
            try
            {
                Visibility = Visibility.Collapsed;
                //_parent.IsEnabled = true;
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
            }

        }

        protected virtual void OnExecuteMethod()
        {
            if (ExecuteMethod != null) ExecuteMethod(this, EventArgs.Empty);
        }

        private void btnClose_Click(object sender, RoutedEventArgs e)
        {
            HideHandlerDialog();
            OnExecuteMethod();



        }
    }
}
