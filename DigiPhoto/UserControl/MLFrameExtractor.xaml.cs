﻿using MPLATFORMLib;
using System;
using System.ComponentModel;
using System.IO;
using System.Runtime.InteropServices;
using System.Windows;
using System.Windows.Controls;

//using Microsoft.Windows.Media.MediaProperties;
namespace DigiPhoto
{
    /// <summary>
    /// MLFrameExtractor used to extract frames from video.
    /// Created by : Shailee
    /// </summary>
    public partial class MLFrameExtractor : UserControl
    {
        public MFileClass mFile;
        string vsMediaFileName = "";
        private System.Windows.Threading.DispatcherTimer dispatcherTimer;
        BackgroundWorker bwCopyFiles = new BackgroundWorker();
        Boolean IsMute = false;
        UIElement _parent;
        public MLFrameExtractor()
        {
            InitializeComponent();
        }

        public void SetParent(UIElement parent)
        {
            _parent = parent;
            _parent.IsEnabled = false;
        }
        public Object SetControlledObject(Object pObject)
        {
            var pOld = (Object)mFile;
            try
            {
                mFile = (MFileClass)pObject;
                UpdateState();
            }
            catch
            { }

            return pOld;
        }
        void MyPlaylist_OnEvent(string bsChannelID, string bsEventName, string bsEventParam, object pEventObject)
        {
            UpdateSeekControl();
        }
        ClientView clientWin = null;
        public void myPlaylist_OnFrame(string bschannelid, object pmframe)
        {
            MFileSeeking1.UpdatePos();
            int pbFullScreen = 0, pbDisplay = 0;
            MPreviewControl1.m_pPreview.PreviewIsFullScreen("", ref pbFullScreen, ref pbDisplay);
            if (clientWin != null && pbFullScreen != 1)
                clientWin.mPreviewControl.m_pPreview.PreviewFullScreen("", 0, -1);
            Marshal.ReleaseComObject(pmframe);
        }
        private void LoadClientViewObject()
        {
            foreach (Window wnd in Application.Current.Windows)
            {
                if (wnd.Title == "ClientView")
                {
                    clientWin = (ClientView)wnd;
                    break;
                }
            }
        }
        void UpdateSeekControl()
        {
            MFileSeeking1.SetControlledObject(mFile);
        }

        string SubstoreName = string.Empty;
        string HotFolderPath = string.Empty;
        int MediaType = 0;
        string RFID = string.Empty;
        int PhotographerId = 0;
        string CropRatio = string.Empty;
        double frameRate = 0;


        public MLFrameExtractor(string fileName, string type, bool playOnclientView, int mediaType, string hotFolderPath, string rfid, string subStoreName, int photographerId, string cropRatio)
        {
            InitializeComponent();
            SubstoreName = subStoreName;
            HotFolderPath = hotFolderPath;
            MediaType = mediaType;
            RFID = rfid;
            PhotographerId = photographerId;
            CropRatio = cropRatio;
            mFormatControl.SetControlledObject(mFile);
            mFormatControl.SetControlledObject(MPreviewControl1);
            if (mFormatControl.comboBoxVideo.Items.Count > 0)
                mFormatControl.comboBoxVideo.SelectedIndex = 12;
            if (mFormatControl.comboBoxAudio.Items.Count > 0)
                mFormatControl.comboBoxAudio.SelectedIndex = 8;
            dispatcherTimer = new System.Windows.Threading.DispatcherTimer();
            dispatcherTimer.Tick += DispatcherTimerTick;
            dispatcherTimer.Interval = new TimeSpan(0, 0, 0, 0, 100);
            dispatcherTimer.Start();

            try
            {
                if (mFile == null)
                    mFile = new MFileClass();
            }
            catch (Exception exception)
            {
                return;
            }
            try
            {
                if (string.IsNullOrEmpty(fileName))
                    return;
                vsMediaFileName = fileName;
                MPreviewControl1.SetControlledObject(mFile);
                mFile.FileNameSet(vsMediaFileName, "loop=true");
                IMProps m_pProps = (IMProps)mFile;
                m_pProps.PropsSet("object::audio_gain", "-100");
                //  IMProps m_pProps = (IMProps)mFile;


                if (!string.IsNullOrEmpty(vsMediaFileName))
                {

                    MediaPlay();
                    string frameCount = string.Empty, strframeRate = string.Empty;
                    m_pProps.PropsGet("file::info::video.0::nb_frames", out frameCount);
                    m_pProps.PropsGet("file::info::video.0::codec_frame_rate", out strframeRate);
                    if (!string.IsNullOrEmpty(frameCount))
                    {
                        MFileSeeking1.totalframe = Convert.ToDouble(frameCount);
                        MFileSeeking1.frameRate = Convert.ToDouble(strframeRate);
                        frameRate = Convert.ToDouble(strframeRate);
                        txtTotalFrame.Text = frameCount.ToString();
                    }
                    if (playOnclientView)
                    {
                        if (clientWin == null)
                        {
                            LoadClientViewObject();
                        }

                        if (clientWin != null)
                            clientWin.PlayVideoOnClient(type, mFile, Convert.ToInt32(RFID));
                    }
                }

                //if (type == "AdvancePreview")
                //{
                //    mpGrid.Width = 900; mpGrid.Height = 815;
                //}
                //else
                //{
                //    mpGrid.Height = 500;
                //}
            }
            catch (Exception ex) { }
        }


        void DispatcherTimerTick(object sender, EventArgs e)
        {
            UpdateState();
        }
        public void UpdateState()
        {
            try
            {
                eMState eState;
                double dblTime;
                mFile.FileStateGet(out eState, out dblTime);
            }
            catch
            { }
        }
        string replayFilePath = string.Empty;

        public void MediaStop()
        {
            try
            {
                if (mFile != null)
                {
                    mFile.FilePosSet(0, 0);
                    mFile.FilePlayStop(0);
                    mFile.OnFrame -= new IMEvents_OnFrameEventHandler(myPlaylist_OnFrame);
                    mFile.OnEvent -= new IMEvents_OnEventEventHandler(MyPlaylist_OnEvent);
                    mFile.ObjectClose();
                    //Marshal.ReleaseComObject(mFile);
                    //GC.Collect();
                    if (clientWin != null)
                        clientWin.StopMediaPlay(true);
                }
            }
            catch
            { }
        }
        public void UnloadMediaPlayer()
        {
            MediaStop();
            if (mFile != null)
            {
                mFile.ObjectClose();
            }
        }
        private void btStart_Click(object sender, RoutedEventArgs e)
        {

            MediaPlay();
        }

        private void MediaPlay()
        {
            try
            {
                if (mFile != null)
                {
                    mFile.OnFrame += new IMEvents_OnFrameEventHandler(myPlaylist_OnFrame);
                    mFile.OnEvent += new IMEvents_OnEventEventHandler(MyPlaylist_OnEvent);
                    // Check for emty file
                    string strPath;
                    mFile.FileNameGet(out strPath);
                    if (strPath != null)
                    {
                        // mFile.FilePlayPause(0);
                        mFile.FileRateSet(1.0);
                        mFile.FilePlayStart();
                    }
                }
            }
            catch
            { }
        }

        private void btPause_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (mFile != null)
                    mFile.FilePlayPause(0);
            }
            catch
            { }
        }
        private void UserControl_Unloaded(object sender, RoutedEventArgs e)
        {
            MediaStop();
            mFile.ObjectClose();
            System.Runtime.InteropServices.Marshal.ReleaseComObject(mFile);
            btnClose.Click -= new RoutedEventHandler(btnClose_Click);
        }
        public void Dispose()
        {
            GC.Collect();
        }


        private void btnFullScreen_Click(object sender, RoutedEventArgs e)
        {
            if (MPreviewControl1.m_pPreview != null)
                MPreviewControl1.m_pPreview.PreviewFullScreen("", 1, -1);
            if (clientWin != null)
            {
                clientWin.mPreviewControl.m_pPreview.PreviewFullScreen("", 1, -1);
            }
        }

        private void btPrev_Click(object sender, RoutedEventArgs e)
        {
            mFile.FileRateSet(-1.0);
            mFile.FilePlayPause(0);
        }

        private void btNext_Click(object sender, RoutedEventArgs e)
        {
            mFile.FileRateSet(1.0);
            mFile.FilePlayPause(0);
        }

        private void btnClose_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                MediaStop();
                mFile.ObjectClose();
                System.Runtime.InteropServices.Marshal.ReleaseComObject(mFile);
                vsMediaFileName = string.Empty;
                OnExecuteMethodML();
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
            HideHandlerDialog();

        }
        private void HideHandlerDialog()
        {
            this.Visibility = Visibility.Collapsed;
            _parent.IsEnabled = true;
            _parent.Visibility = Visibility.Visible;
            // SearchResult _objSearchResult = null;
            // //foreach (Window wnd in Application.Current.Windows)
            // //{
            // //    if (wnd.Title == "View/Order Station")
            // //    {
            // //        _objSearchResult = (SearchResult)wnd;
            // //        break;
            // //    }
            // //}
            // if (_objSearchResult == null)
            //     _objSearchResult = new SearchResult();

            // _objSearchResult.pagename = "MainGroup";
            // _objSearchResult.Show();
            // ////////Calling logic of searching 
            //_objSearchResult.LoadWindow();
            // //close the containing current window

            // //Window parentWindow = Window.GetWindow(this);
            // //((SearchResult)parentWindow).grdCotrol.Children.Remove(this);
            // //parentWindow.Close();
            // //Dispose();

        }
        string tempFolderpath = Environment.CurrentDirectory + "\\DigiProcessVideoTemp\\";
        static int counter = 1;
        private void btnExtract_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (!ValidFrame)
                {
                    MessageBox.Show("Requested frame does not exist!", "IMIX", MessageBoxButton.OK, MessageBoxImage.Error, MessageBoxResult.OK);
                    txtPos.Text = "0";
                    return;
                }
                else
                {
                    double dblTime;
                    mFile.FilePosGet(out dblTime);
                    MFrame mf1;
                    mFile.FileFrameGet(dblTime, 0, out mf1);
                    //            string path = tempFolderpath + "MFrame" + counter + ".jpg";
                    string filename = RFID.ToString() + "_" + counter + "#" + MediaType.ToString() + "@" + PhotographerId + ".jpg";
                    string path = tempFolderpath + filename;
                    counter++;
                    if (File.Exists(path))
                    {
                        File.Delete(path);
                    }
                    mf1.FrameVideoSaveToFile(path);
                    string resizedImage = FrameworkHelper.Common.MLHelpers.ExtractFrame(CropRatio, filename, tempFolderpath, path);

                    if (MoveFrames(resizedImage, filename))
                    {
                        MessageBox.Show("Frame extracted successfully and moved to camera folder.", "IMIX", MessageBoxButton.OK, MessageBoxImage.Information, MessageBoxResult.OK);
                        txtPos.Text = "0";
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }

        private bool MoveFrames(string path, string filename)
        {
            try
            {
                if (!String.IsNullOrEmpty(path))
                {
                    //string movePath = HotFolderPath + "\\" + "Download" + "\\" + SubstoreName + "\\";
                    string movePath = System.IO.Path.Combine(HotFolderPath,"Download",SubstoreName);
                    if (File.Exists(System.IO.Path.Combine(movePath,filename)))
                    {
                        File.Delete(System.IO.Path.Combine(movePath, filename));
                    }
                    File.Move(path, System.IO.Path.Combine(movePath, filename));

                    return true;
                }
                else
                    return false;
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                return false;
            }
        }
        bool isUpdated = false;
        bool ValidFrame = true;
        private void txtPos_TextChanged(object sender, TextChangedEventArgs e)
        {
            string strPos = txtPos.Text; double startTime, stopTime, VideoLength;
            if (mFile != null)
                mFile.FilePlayPause(0);
            if (IsDouble(strPos))
            {
                ValidFrame = true;
               
                mFile.FileInOutGet(out startTime, out stopTime, out VideoLength);
                if (Convert.ToDouble(strPos) <= VideoLength)
                    mFile.FilePosSet(Convert.ToDouble(strPos) + 0.03, 0.0);
                else
                {
                    //  txtPos.Text = "0";          
                    ValidFrame = false;
                    mFile.FilePosSet(VideoLength, 0.0);
                }
            }
            else
            {
                ValidFrame = false;
               txtPos.Text = "";
                mFile.FilePosSet(0, 0.0);
            }
            //// dblPos * frameRate
            //if (!isUpdated)
            //{
            //    TextBox tb = (TextBox)sender;
            //    string strPos = string.Empty; string strFrameNum = string.Empty;
            //    if (IsDouble(txtPos.Text) || IsDouble(txtFrame.Text))
            //    {
            //        //if (tb.Name == "txtPos")
            //        //{
            //        //    isUpdated = true;
            //        //    txtFrame.Text = Convert.ToInt32((Convert.ToDouble(txtPos.Text) * frameRate)).ToString();
            //        //}
            //        //else
            //        //{
            //        //    isUpdated = true;
            //        //    txtPos.Text = Convert.ToInt32((Convert.ToDouble(txtFrame.Text) / frameRate)).ToString();
                        
            //        //}
            //        strPos = txtPos.Text;
            //        double startTime, stopTime, VideoLength;
            //        if (mFile != null)
            //            mFile.FilePlayPause(0);
            //        if (IsDouble(strPos))
            //        {
            //            mFile.FileInOutGet(out startTime, out stopTime, out VideoLength);
            //            if (Convert.ToDouble(strPos) <= VideoLength)
            //                mFile.FilePosSet(Convert.ToDouble(strPos), 0.0);
            //            else
            //            {
            //                txtPos.Text = "0";
            //                mFile.FilePosSet(VideoLength, 0.0);
            //            }
            //            isUpdated = false;
            //        }
            //        else
            //        {
            //            txtPos.Text = "0";
            //            mFile.FilePosSet(0, 0.0);
            //            isUpdated = false;
            //        }
            //    }
            //    else
            //    {
            //        txtPos.Text = "0";
            //        txtFrame.Text = "0";
            //        mFile.FilePosSet(0, 0.0);
            //        isUpdated = false;
            //    }
            //}
        }
        private bool IsDouble(string value)
        {
            try
            {
                double price;
                bool isDouble = Double.TryParse(value, out price);
                if (isDouble)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            { return false; }

        }
        public event EventHandler ExecuteParentMethod;
        protected virtual void OnExecuteMethodML()
        {
            if (ExecuteParentMethod != null) ExecuteParentMethod(this, EventArgs.Empty);
        }

        //private void txtPos_TextChanged(object sender, TextChangedEventArgs e)
        //{

        //}

        //private void txtPos_TextChanged(object sender, TextCompositionEventArgs e)
        //{

        //}

       
    }
}