﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using System.Configuration;

namespace DigiPhoto
{
    /// <summary>
    /// Interaction logic for ImageCalculator.xaml
    /// </summary>
    public partial class ImageCalculator : UserControl
    {
        public string DGconn = ConfigurationManager.ConnectionStrings["DigiConnectionString"].ConnectionString;
       
        public ImageCalculator()
        {
            InitializeComponent();
            //this.Dispatcher.Invoke(DispatcherPriority.Normal, new Action(delegate()
            //{
                // Do all the ui thread updates here
               // busyindicator.IsBusy = true;
                //timer = new DispatcherTimer();
                //timer.Interval = new TimeSpan(0, 0, 1);
                //timer.Tick += timer_Tick;
                //timer.Start();

            //}));
        }

        #region Calculating Images
        void timer_Tick(object sender, EventArgs e)
        {
           // timer.Stop();
           // GetNewImagesList();
           //// GetTotalImages();
           // GetLastImageTime();
           // //busyindicator.IsBusy = false;
           // timer.Start();
        }

        private void GetNewImagesList()
        {
            try
            {



                using (SqlConnection cn = new SqlConnection(DGconn))
                {
                    using (SqlCommand cmd = cn.CreateCommand())
                    {
                        cmd.CommandType = CommandType.Text;

                        cmd.CommandText =
                            "SELECT  count(DISTINCT [DG_Photos_FileName])  from [dbo].[DG_Photos]  where datediff(dd,DG_Photos_CreatedOn,GETDATE())=0";

                        cn.Open();

                        using (SqlDataReader dr = cmd.ExecuteReader(CommandBehavior.CloseConnection))
                        {
                            while (dr.Read())
                            {
                                //newimages.Text = dr.GetValue(0).ToString();

                            }
                        }
                        cn.Close();
                    }
                }
            }
            catch (Exception ex)
            {

            }
            finally
            {
                MemoryManagement.FlushMemory();
            }
        }

        private void GetTotalImages()
        {
            //SqlDependency.Stop(DGconn);
            //SqlDependency.Start(DGconn);

            using (SqlConnection cn = new SqlConnection(DGconn))
            {
                using (SqlCommand cmd = cn.CreateCommand())
                {
                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = "select count(DISTINCT [DG_Photos_FileName])  from [Digiphoto].[dbo].[DG_Photos]";
                    //cmd.CommandText = "select [DG_Photos_FileName] from [Digiphoto].[dbo].[DigiTest]";
                    //cmd.Notification = null;

                    //  creates a new dependency for the SqlCommand


                    cn.Open();

                    //SqlDependency dep = new SqlDependency(cmd);

                    //bool havchenge = dep.HasChanges;
                    // dep.OnChange += new OnChangeEventHandler(dep_OnChange);

                    using (SqlDataReader dr = cmd.ExecuteReader(CommandBehavior.CloseConnection))
                    {
                        while (dr.Read())
                        {
                            //Totalimages.Text = dr.GetValue(0).ToString();
                            //Totalimages.Content = "Total Images : " + dr.GetString(0);

                        }
                    }
                    cn.Close();
                }
            }
        }

        private void GetLastImageTime()
        {
            try
            {


                using (SqlConnection cn = new SqlConnection(DGconn))
                {
                    using (SqlCommand cmd = cn.CreateCommand())
                    {
                        cmd.CommandType = CommandType.Text;

                        cmd.CommandText =
                            "select DG_Photos_CreatedOn from [Digiphoto].[dbo].[DG_Photos] where DG_Photos_pkey=(select max(DG_Photos_pkey) from [Digiphoto].[dbo].[DG_Photos])";

                        cn.Open();

                        using (SqlDataReader dr = cmd.ExecuteReader(CommandBehavior.CloseConnection))
                        {
                            while (dr.Read())
                            {
                                //LastimagesloadTime.Text = dr.GetDateTime(0).ToString();

                            }
                        }
                        cn.Close();
                    }
                }
            }
            catch(Exception ex)
            {
                
            }
            finally
            {
                MemoryManagement.FlushMemory();
            }
        }
        #endregion
    }
}
