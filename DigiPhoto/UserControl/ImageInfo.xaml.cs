﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Threading;
using System.Windows.Threading;
using DigiPhoto.IMIX.Model;
using System.Xml;

namespace DigiPhoto
{
    /// <summary>
    /// Interaction logic for ImageInfo.xaml
    /// </summary>
    public partial class ImageInfo : UserControl
    {
        public event EventHandler ExecuteParentMethod;
        public ImageInfo()
        {
            InitializeComponent();
            Visibility = Visibility.Hidden;
            this.DataContext = captureInfodetails;
            
            
        }
        protected virtual void OnExecuteMethod()
        {
            if (ExecuteParentMethod != null) ExecuteParentMethod(this, EventArgs.Empty);
        }
        private void btnok_Click(object sender, RoutedEventArgs e)
        {
            _result = true;
            HideHandlerDialog();

        }
        private bool _hideRequest = false;
        private bool _result = false;
        private UIElement _parent;
        public bool IsVideo = false;

        public void SetParent(UIElement parent)
        {
            _parent = parent;
        }

        #region Message

        public PhotoCaptureInfo captureInfodetails
        {
            get { return (PhotoCaptureInfo)GetValue(captureInfoProperty); }
            set { SetValue(captureInfoProperty, value); }
        }

        // Using a DependencyProperty as the backing store for Message.
        // This enables animation, styling, binding, etc...
        public static readonly DependencyProperty captureInfoProperty =
            DependencyProperty.Register(
                "captureInfo", typeof(PhotoCaptureInfo), typeof(ImageInfo),
                new UIPropertyMetadata(new PhotoCaptureInfo()));

        #endregion

        public bool ShowHandlerDialog(PhotoCaptureInfo info)
        {
            if (IsVideo)
                btnCameraDetails.Visibility = Visibility.Collapsed;
            else
                btnCameraDetails.Visibility = Visibility.Visible;
            captureInfodetails = info;
            Visibility = Visibility.Visible;
            ImageAttribute.DataContext = captureInfodetails;


            //  this.DataContext = captureInfodetails;
            _parent.IsEnabled = false;

            _hideRequest = false;
            while (!_hideRequest)
            {
                // HACK: Stop the thread if the application is about to close
                if (this.Dispatcher.HasShutdownStarted ||
                    this.Dispatcher.HasShutdownFinished)
                {
                    break;
                }

                // HACK: Simulate "DoEvents"
                this.Dispatcher.Invoke(
                    DispatcherPriority.Background,
                    new ThreadStart(delegate { }));
                Thread.Sleep(20);
            }

            return _result;
        }

        private void HideHandlerDialog()
        {
            _hideRequest = true;
           this.Visibility = Visibility.Collapsed;
            _parent.IsEnabled = true;
            OnExecuteMethod();
        }

        private void OkButton_Click(object sender, RoutedEventArgs e)
        {
            _result = true;
            HideHandlerDialog();
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            _result = false;
            HideHandlerDialog();
        }

        private void btnCameraDetails_Click(object sender, RoutedEventArgs e)
        {

            try
            {
                XmlDocument xml = new XmlDocument();
                xml.LoadXml(captureInfodetails.MetaData);

                XmlNode nodeList = xml.SelectSingleNode("image");

                if (nodeList != null)
                {
                    CameraAttribute obj = new DigiPhoto.CameraAttribute
                    {
                        CameraModel = nodeList.Attributes["CameraModel"].Value ?? string.Empty,
                        CameraManufacture = nodeList.Attributes["CameraManufacture"].Value ?? string.Empty,
                        DateTaken = nodeList.Attributes["DateTaken"].Value ?? string.Empty,
                        Dimensions = nodeList.Attributes["Dimensions"].Value ?? string.Empty,
                        ExposureMode = nodeList.Attributes["ExposureMode"].Value ?? string.Empty,
                        ISO = nodeList.Attributes["ISO-SpeedRating"].Value ?? string.Empty,
                        Orientation = nodeList.Attributes["Orientation"].Value ?? string.Empty,
                        Sharpness = nodeList.Attributes["Sharpness"].Value ?? string.Empty,
                        HorizontalResolution= nodeList.Attributes["HorizontalResolution"].Value ?? string.Empty,
                        ExposureTime = nodeList.Attributes["ExposureTime"].Value ?? string.Empty,
                        ApertureValue = nodeList.Attributes["ApertureValue"].Value ?? string.Empty,
         //             //////////created by latika for table work flow
					    //TableName = nodeList.Attributes["TableName"].Value ?? string.Empty,
         //               GuestName = DigiPhoto.CryptorEngine.Decrypt((nodeList.Attributes["GuestName"].Value ?? string.Empty),true),
         //               EmailID = DigiPhoto.CryptorEngine.Decrypt((nodeList.Attributes["EmailID"].Value ?? string.Empty), true),
         //               ContactNo = DigiPhoto.CryptorEngine.Decrypt((nodeList.Attributes["ContactNo"].Value ?? string.Empty), true)
         //           /////////end
					};

                    CameraAttribute.DataContext = obj;

                }
                CameraAttribute.Visibility = Visibility.Visible;
                ImageAttribute.Visibility = Visibility.Collapsed;


            }
            catch (Exception ex)
            {

            }

        }

        private void btnCameraOk_Click(object sender, RoutedEventArgs e)
        {
            CameraAttribute.Visibility = Visibility.Collapsed;
            ImageAttribute.Visibility = Visibility.Visible;

        }
    }

    public class CameraAttribute
    {
        public string Dimensions { get; set; }
        public string CameraManufacture { get; set; }
        public string HorizontalResolution { get; set; }
        public string CameraModel { get; set; }
        public string ISO { get; set; }
        public string DateTaken { get; set; }
        public string ExposureMode { get; set; }
        public string Sharpness { get; set; }
        public string Orientation { get; set; }
        public string ExposureTime { get; set; }
        public string ApertureValue { get; set; }
		//////////////////created by latika for table work flow
        public string TableName { get; set; }
        public string GuestName { get; set; }
        public string EmailID { get; set; }
        public string ContactNo { get; set; }

    }
}
