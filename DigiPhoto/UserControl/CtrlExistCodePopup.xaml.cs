﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using DigiPhoto.DataLayer;
using DigiPhoto.Common;
using System.Threading;
using System.Windows.Threading;
namespace DigiPhoto
{
	/// <summary>
	/// Interaction logic for SaveGroup.xaml
	/// </summary>
	public partial class CtrlExistCodePopup : UserControl
	{
        /// <summary>
        /// Initializes a new instance of the <see cref="SaveGroup"/> class.
        /// </summary>
        public bool IsToOverwrite=false;
		public CtrlExistCodePopup()
		{
			this.InitializeComponent();
            Visibility = Visibility.Hidden;
		}

        private bool _hideRequest = false;
        private bool _result = false;
        private UIElement _parent;

        public void SetParent(UIElement parent)
        {
            _parent = parent;
        }

        #region Message

        public string Message
        {
            get { return (string)GetValue(MessageProperty); }
            set { SetValue(MessageProperty, value); }
        }

        // Using a DependencyProperty as the backing store for Message.
        // This enables animation, styling, binding, etc...
        public static readonly DependencyProperty MessageProperty =
            DependencyProperty.Register(
                "Message", typeof(string), typeof(CtrlExistCodePopup), new UIPropertyMetadata(string.Empty));
        #endregion
      
        private void btnClose_Click(object sender, RoutedEventArgs e)
        {
            _result = false;
            HideHandlerDialog();
        }
		public bool ShowHandlerDialog(string message)
		{

            MessageTextBlock.Text = message;
			//Message = message;
			Visibility = Visibility.Visible;

			_parent.IsEnabled = false;

			_hideRequest = false;
			while (!_hideRequest)
			{
				// HACK: Stop the thread if the application is about to close
				if (this.Dispatcher.HasShutdownStarted ||
					this.Dispatcher.HasShutdownFinished)
				{
					break;
				}

				// HACK: Simulate "DoEvents"
				this.Dispatcher.Invoke(
					DispatcherPriority.Background,
					new ThreadStart(delegate { }));
				Thread.Sleep(20);
			}
			return _result;
		}
		
		private void HideHandlerDialog()
		{
			_hideRequest = true;
			Visibility = Visibility.Hidden;
			_parent.IsEnabled = true;
		}

        private void SaveButton_Click(object sender, RoutedEventArgs e)
		{
            IsToOverwrite = rdbOverwriteimg.IsChecked == true ? true : false;
			_result = true;
			HideHandlerDialog();
		}

        private void btnCancel_Click(object sender, RoutedEventArgs e)
		{
			_result = false;
			HideHandlerDialog();
		}
	}
}

