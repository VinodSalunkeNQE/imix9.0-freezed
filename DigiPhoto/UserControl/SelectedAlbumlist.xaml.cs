﻿using DigiPhoto.DataLayer;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace DigiPhoto
{
    public partial class SelectedAlbumlist : UserControl
    {
        public int ActivePageNo = 1;
        public int DropPhotoId = 0;
        public int SelectedPhotoId1 = 0;
        public int SelectedPhotoId2 = 0;
        int rotateAngleLeft = 0;
        int rotateAngleRight = 0;
        public ObservableCollection<PrintOrderPage> PrintOrderPageList
        {
            get
            {
                return (ObservableCollection<PrintOrderPage>)
               GetValue(PrintOrderPageProperty);
            }
            set { SetValue(PrintOrderPageProperty, value); }
        }

        // Using a DependencyProperty as the backing store for MyProperty.This enables animation, styling, binding, etc...
        public static readonly DependencyProperty PrintOrderPageProperty =
        DependencyProperty.Register("PrintOrderPageList",
        typeof(ObservableCollection<PrintOrderPage>), typeof(SelectedAlbumlist),
            new PropertyMetadata(new ObservableCollection<PrintOrderPage>()));



        public SelectedAlbumlist()
        {
            try
            {
                InitializeComponent();
                lstSelectedImage.Items.Clear();
                lstSelectedPages.Items.Clear();

                foreach (var item in RobotImageLoader.PrintImages)
                {
                    lstSelectedImage.Items.Add(item);
                }

                lstSelectedPages.ItemsSource = PrintOrderPageList;
            }
            catch
            {
            }
        }

        public List<LstMyItems> _SelectedImage
        {
            get;
            set;
        }

        public List<String> PreviousImage
        {
            get;
            set;
        }

        public int maxSelectedPhotos
        {
            get;
            set;
        }

        public Boolean IsBundled
        {
            get;
            set;
        }

        public Boolean IsPackage
        {
            get;
            set;
        }

        public int ProductTypeID
        {
            get;
            set;
        }

        private bool _hideRequest = false;

        private List<LstMyItems> _result;

        private UIElement _parent;

        public void SetParentAlbum(UIElement parent)
        {
            _parent = (UIElement)parent;
        }

        #region Message

        public string MessageAlbum2
        {
            get { return (string)GetValue(MessageAlbum2Property); }
            set { SetValue(MessageAlbum2Property, value); }
        }

        public static readonly DependencyProperty MessageAlbum2Property =
            DependencyProperty.Register(
                "MessageAlbum2", typeof(string), typeof(ModalDialog), new UIPropertyMetadata(string.Empty));

        #endregion

        public List<LstMyItems> ShowAlbumHandlerDialog(string message)
        {
            BindPageStrips();

            MessageAlbum2 = message;
            Visibility = Visibility.Visible;
            _parent.IsEnabled = true;

            _hideRequest = false;
            while (!_hideRequest)
            {
                // HACK: Stop the thread if the application is about to close
                if (this.Dispatcher.HasShutdownStarted ||
                    this.Dispatcher.HasShutdownFinished)
                {
                    break;
                }
                // HACK: Simulate "DoEvents"
                this.Dispatcher.Invoke(
                    DispatcherPriority.Background,
                    new ThreadStart(delegate { }));
                Thread.Sleep(20);

            }
            return _result;
        }

        private void HideHandlerDialog()
        {
            _hideRequest = true;
            Visibility = Visibility.Collapsed;
            _parent.IsEnabled = true;
        }

        /*
        public void GetPrintedImage()
        {
            lstSelectedImage1.ItemsSource = _SelectedImage;
        }

        */

        private void btnSubmit_Click(object sender, RoutedEventArgs e)
        {
            //int TotalSelectedImages = 0;
            _result = new List<LstMyItems>();
            PreviousImage = new System.Collections.Generic.List<string>();
            foreach (PrintOrderPage printPage in PrintOrderPageList)
            {
                if (printPage.FilePath1 != null && printPage.PhotoId1 > 0)
                {
                    LstMyItems item = RobotImageLoader.PrintImages.Where(o => o.PhotoId == printPage.PhotoId1).FirstOrDefault();
                    if (item != null)
                    {
                       // item.PhotoPrintPositionList.Clear();
                        if (item.PhotoPrintPositionList.Where(o => o.PageNo == printPage.PageNo && o.PhotoPosition == printPage.PhotoPosition1).FirstOrDefault() == null)
                            item.PhotoPrintPositionList.Add(new PhotoPrintPosition(printPage.PageNo, printPage.PhotoPosition1, printPage.RotationAngle1));
                        _result.Add(item);
                        PreviousImage.Add(item.PhotoId.ToString());
                    }
                }
                if (printPage.FilePath2 != null && printPage.PhotoId2 > 0)
                {
                    LstMyItems item = RobotImageLoader.PrintImages.Where(o => o.PhotoId == printPage.PhotoId2).FirstOrDefault();
                    if (item != null)
                    {
                        //item.PhotoPrintPositionList.Clear();
                        if (item.PhotoPrintPositionList.Where(o => o.PageNo == printPage.PageNo && o.PhotoPosition == printPage.PhotoPosition2).FirstOrDefault() == null)
                            item.PhotoPrintPositionList.Add(new PhotoPrintPosition(printPage.PageNo, printPage.PhotoPosition2, printPage.RotationAngle2));
                        _result.Add(item);
                        PreviousImage.Add(item.PhotoId.ToString());
                        
                    }
                }
            }
            if (IsPackage)
            {
                if (_result.Count > maxSelectedPhotos)
                {
                    System.Windows.MessageBox.Show("You can't select more than " + maxSelectedPhotos + " images");
                }
                else
                {
                    HideHandlerDialog();
                }
            }
            else
            {
                HideHandlerDialog();
            }

        }

        private void btnSubmitCancel_Click(object sender, RoutedEventArgs e)
        {
            ResetPageStrip();
        }

        private void ResetPageStrip()
        {
            //PrintOrderPageList.ForEach(o =>

            foreach (PrintOrderPage o in PrintOrderPageList)
            {
                o.ImageSource1 = null;
                o.ImageSource2 = null;
                o.Name1 = string.Empty;
                o.Name2 = string.Empty;
                o.PhotoId1 = o.PhotoId2 = 0;
            }

            img1.Source = null;
            img2.Source = null;
            tbkActivePageNo.Text = "1";
            this.ActivePageNo = 1;
            this.DropPhotoId = 0;
            lstSelectedPages.SelectedIndex = 0;
            rotateAngleLeft = rotateAngleRight = 0;
            //BindPageStrips();
        }

        private void btnClose_Click(object sender, RoutedEventArgs e)
        {
            //ResetPageStrip();
            _result = null;
            HideHandlerDialog();
        }

        private void Image_PreviewMouseDown(object sender, EventArgs e)
        {
            LstMyItems lstMyItems = (LstMyItems)(sender as Image).DataContext;
            this.DropPhotoId = lstMyItems.PhotoId;
            DragDrop.DoDragDrop((DependencyObject)sender, ((Image)sender).Source, DragDropEffects.Copy);
        }

        private void Image_Drop(object sender, DragEventArgs e)
        {
            /*
            foreach (var format in e.Data.GetFormats())
            {
                */
            LstMyItems lstMyItem = RobotImageLoader.PrintImages.Where(o => o.PhotoId == this.DropPhotoId).FirstOrDefault();
            BitmapImage bitMapImg = new BitmapImage();
            bitMapImg.BeginInit();
            bitMapImg.UriSource = new Uri(lstMyItem.FilePath.Replace("Thumbnails", "Thumbnails_Big"), UriKind.Relative);
            bitMapImg.DecodePixelWidth = 200;
            bitMapImg.EndInit();
            /*
            ImageSource imageSource = e.Data.GetData(format) as ImageSource;
            BitmapImage bitMapImg = ((BitmapImage)imageSource);
            */


            string recName = ((Rectangle)sender).Name;
            object objectName = ((Rectangle)sender).FindName("ViewBox1");

            //if (imageSource != null)
            if (true)
            {
                if (recName == "btn1")
                {
                    object item1 = ((Viewbox)objectName).FindName("img1");
                    if (item1 is Image)
                    {
                        Image img1 = (Image)item1;

                        //bind to strip page
                        //LstMyItems lstMyItem = RobotImageLoader.PrintImages.Where(o => o.PhotoId == this.DropPhotoId).FirstOrDefault();
                        var page = PrintOrderPageList.Where(o => o.PageNo == this.ActivePageNo).FirstOrDefault();
                        page.FilePath1 = lstMyItem.FilePath.Replace("Thumbnails", "Thumbnails_Big");
                        page.Name1 = lstMyItem.Name;
                        page.PhotoPosition1 = 1;
                        page.PhotoId1 = lstMyItem.PhotoId;
                        this.SelectedPhotoId1 = this.DropPhotoId;
                        if (bitMapImg.PixelWidth > bitMapImg.PixelHeight)
                        {
                            // Create the TransformedBitmap to use as the Image source.
                            TransformedBitmap tb = new TransformedBitmap();
                            // Properties must be set between BeginInit and EndInit calls.
                            tb.BeginInit();
                            tb.Source = bitMapImg;
                            // Set image rotation.
                            this.rotateAngleLeft = 270;
                            RotateTransform transform = new RotateTransform(this.rotateAngleLeft);
                            tb.Transform = transform;
                            tb.EndInit();
                            // Set the Image source.
                            img1.Source = tb;
                            page.ImageSource1 = tb;

                        }
                        else
                        {
                            img1.Source = bitMapImg;
                            page.ImageSource1 = bitMapImg;
                            this.rotateAngleLeft = 0;
                        }
                        page.RotationAngle1 = this.rotateAngleLeft;
                    }
                }
                else
                {
                    object item2 = ((Rectangle)sender).FindName("img2");
                    if (item2 is Image)
                    {
                        Image img2 = (Image)item2;
                       // LstMyItems lstMyItem = RobotImageLoader.PrintImages.Where(o => o.PhotoId == this.DropPhotoId).FirstOrDefault();
                        var page = PrintOrderPageList.Where(o => o.PageNo == this.ActivePageNo).FirstOrDefault();
                        page.FilePath2 = lstMyItem.FilePath.Replace("Thumbnails", "Thumbnails_Big");
                        page.Name2 = lstMyItem.Name;
                        page.PhotoPosition2 = 2;
                        page.PhotoId2 = lstMyItem.PhotoId;
                        this.SelectedPhotoId2 = this.DropPhotoId;
                        if (bitMapImg.PixelWidth > bitMapImg.PixelHeight)
                        {
                            // Create the TransformedBitmap to use as the Image source.
                            TransformedBitmap tb = new TransformedBitmap();
                            // Properties must be set between BeginInit and EndInit calls.
                            tb.BeginInit();
                            tb.Source = bitMapImg;
                            // Set image rotation.
                            this.rotateAngleRight = 270;
                            RotateTransform transform = new RotateTransform(this.rotateAngleRight);
                            tb.Transform = transform;
                            tb.EndInit();
                            // Set the Image source.
                            img2.Source = tb;
                            page.ImageSource2 = tb;
                        }
                        else
                        {
                            img2.Source = bitMapImg;
                            page.ImageSource2 = bitMapImg;
                            this.rotateAngleRight = 0;
                        }
                        page.RotationAngle2 = this.rotateAngleRight;
                    }
                }
                //}
            }
        }

        private void lstSelectedPages_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                if (lstSelectedPages.Items.Count > 0)
                {
                    lstSelectedPages.ScrollIntoView(lstSelectedPages.SelectedItem);
                    if (lstSelectedPages.SelectedItems.Count > 0)
                    {
                        this.ActivePageNo = ((DigiPhoto.PrintOrderPage)(lstSelectedPages.SelectedItem)).PageNo;
                        PrintOrderPage printOrderPage = (DigiPhoto.PrintOrderPage)(lstSelectedPages.SelectedItem);
                        ImageSource filePath1 = printOrderPage.ImageSource1;
                        ImageSource filePath2 = printOrderPage.ImageSource2;
                        this.rotateAngleLeft = printOrderPage.RotationAngle1;
                        this.rotateAngleRight = printOrderPage.RotationAngle2;
                        this.SelectedPhotoId1 = printOrderPage.PhotoId1;
                        this.SelectedPhotoId2 = printOrderPage.PhotoId2;
                   
                    

                    //BitmapSource bitmap = imageSource as BitmapSource;
                    //if (!string.IsNullOrEmpty(filePath1))
                    //    img1.Source = new BitmapImage(new Uri(filePath1));
                    //else
                    //    img1.Source = null;
                    //if(filePath1!=null)
                        img1.Source = filePath1;
                    //if (filePath2 != null)
                        img2.Source = filePath2;
                    //    if (!string.IsNullOrEmpty(filePath2))
                    //    img2.Source = new BitmapImage(new Uri(filePath2));
                    //else
                    //    img2.Source = null;
                    tbkActivePageNo.Text = this.ActivePageNo.ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
            finally
            {
                MemoryManagement.FlushMemory();
            }
        }

        private void BindPageStrips()
        {
            try
            {
                //lstSelectedPages.Items.Clear();
                if ( PrintOrderPageList.Count()==0)
                {
                    img1.Source = null;
                    img2.Source = null;
                    int NoOfPageStrip = (this.maxSelectedPhotos / 2 + this.maxSelectedPhotos % 2);
                    PrintOrderPage objPrintOrderPage = null;
                    for (int i = 1; i <= NoOfPageStrip; i++)
                    {
                        objPrintOrderPage = new PrintOrderPage();
                        objPrintOrderPage.PageNo = i;
                        objPrintOrderPage.PhotoPosition1 = 1;
                        objPrintOrderPage.PhotoPosition2 = 2;
                        if (objPrintOrderPage != null)
                            PrintOrderPageList.Add(objPrintOrderPage);
                    }                    
                }
                lstSelectedPages.ItemsSource = PrintOrderPageList;
                //Set

                this.ActivePageNo = 1;
                PrintOrderPage printOrderPage = PrintOrderPageList.Where(o => o.PageNo == 1).FirstOrDefault();
                ImageSource filePath1 = printOrderPage.ImageSource1;
                ImageSource filePath2 = printOrderPage.ImageSource2;
                this.rotateAngleLeft = printOrderPage.RotationAngle1;
                this.rotateAngleRight = printOrderPage.RotationAngle2;
                this.SelectedPhotoId1 = printOrderPage.PhotoId1;
                this.SelectedPhotoId2 = printOrderPage.PhotoId2;

                //BitmapSource bitmap = imageSource as BitmapSource;
                //if (!string.IsNullOrEmpty(filePath1))
                //    img1.Source = new BitmapImage(new Uri(filePath1));
                //else
                //    img1.Source = null;
                img1.Source = filePath1;
                img2.Source = filePath2;
                //    if (!string.IsNullOrEmpty(filePath2))
                //    img2.Source = new BitmapImage(new Uri(filePath2));
                //else
                //    img2.Source = null;
                tbkActivePageNo.Text = this.ActivePageNo.ToString();
                //Set
            }
            catch
            {
            }
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            ShowToClientView();
        }

        private void ShowToClientView()
        {
            try
            {
                VisualBrush CB = new VisualBrush(ThumbPreview);
                SearchResult _objSearchResult = null;
                foreach (Window wnd in Application.Current.Windows)
                {
                    if (wnd.Title == "View/Order Station")
                    {
                        _objSearchResult = (SearchResult)wnd;
                    }
                }
                if (_objSearchResult == null)
                    _objSearchResult = new SearchResult();
                //SearchResult objSearchResult = new SearchResult();
                _objSearchResult.CompileEffectChanged(CB, -1);

            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }

        /*
        protected void SelectCurrentItem(object sender, KeyboardFocusChangedEventArgs e)
        {
            ListBoxItem item = (ListBoxItem)sender;
            item.IsSelected = true;
        }
        */


        private void btnRotateLeft_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                this.rotateAngleLeft += 90;
                this.rotateAngleLeft %= 360;

                LstMyItems lstMyItem = RobotImageLoader.PrintImages.Where(o => o.PhotoId == this.SelectedPhotoId1).FirstOrDefault();
                BitmapImage bitMapImg = new BitmapImage();
                bitMapImg.BeginInit();
                bitMapImg.UriSource = new Uri(lstMyItem.FilePath.Replace("Thumbnails", "Thumbnails_Big"), UriKind.Relative);
                bitMapImg.DecodePixelWidth = 200;
                bitMapImg.EndInit();

                // Create the TransformedBitmap to use as the Image source.
                TransformedBitmap tb = new TransformedBitmap();
                // Properties must be set between BeginInit and EndInit calls.
                tb.BeginInit();
                tb.Source = bitMapImg;
                // Set image rotation.

                RotateTransform transform = new RotateTransform(this.rotateAngleLeft);
                tb.Transform = transform;
                tb.EndInit();
                // Set the Image source.
                img1.Source = tb;

                var page = PrintOrderPageList.Where(o => o.PageNo == this.ActivePageNo).FirstOrDefault();
                page.ImageSource1 = tb;
                page.RotationAngle1 = this.rotateAngleLeft;
            }
            catch
            {
            }
        }

        private void btnRotateRight_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                this.rotateAngleRight += 90;
                this.rotateAngleRight %= 360;

                LstMyItems lstMyItem = RobotImageLoader.PrintImages.Where(o => o.PhotoId == this.SelectedPhotoId2).FirstOrDefault();
                BitmapImage bitMapImg = new BitmapImage();
                bitMapImg.BeginInit();
                bitMapImg.UriSource = new Uri(lstMyItem.FilePath.Replace("Thumbnails", "Thumbnails_Big"), UriKind.Relative);
                bitMapImg.DecodePixelWidth = 200;
                bitMapImg.EndInit();

                // Create the TransformedBitmap to use as the Image source.
                TransformedBitmap tb = new TransformedBitmap();
                // Properties must be set between BeginInit and EndInit calls.
                tb.BeginInit();
                tb.Source = bitMapImg;
                // Set image rotation.

                RotateTransform transform = new RotateTransform(this.rotateAngleRight);
                tb.Transform = transform;
                tb.EndInit();
                // Set the Image source.
                img2.Source = tb;

                var page = PrintOrderPageList.Where(o => o.PageNo == this.ActivePageNo).FirstOrDefault();
                page.ImageSource2 = tb;
                page.RotationAngle2 = this.rotateAngleRight;
            }
            catch
            {
            }
        }

    }
}

