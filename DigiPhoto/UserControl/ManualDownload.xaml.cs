﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using DigiPhoto.DataLayer;
using DigiPhoto.Common;
using System.Drawing;
using System.IO;
using DigiPhoto.DataLayer.Model;
using System.Windows.Threading;
using System.Threading;
using FrameworkHelper;
using LevDan.Exif;
using ExifLib;
using System.Linq;
using BarcodeReaderLib;
using DigiPhoto.Shader;
using System.Windows.Media;
using DigiPhoto.IMIX.Model;
using DigiPhoto.IMIX.Business;
using DigiPhoto.Utility.Repository.ValueType;
using DigiPhoto.Utility.Repository.Data;
using System.Text;
using System.Threading.Tasks;
using System.Collections;
using System.Threading;
using System.ComponentModel;
using System.Reflection;
using System.Xml.Linq;
using System.Configuration;
using DigiPhoto.IMIX.Model.Base;

namespace DigiPhoto
{
    /// <summary>
    /// Interaction logic for ManualDownload.xaml
    /// </summary>
    public partial class ManualDownload : UserControl
    {
        #region Declaration

        //const string _metaDataFormat = "<image Dimensions={0} CameraManufacture={1} HorizontalResolution={2} VerticalResolution={3} CameraModel={4} ISO-SpeedRating={5} DateTaken={6} ExposureMode={7} Sharpness={8} Orientation={9} GPSLatitude={10} GPSLongitude={11}></image>";
        //const string _defaultEffects = "<image brightness = '{0}' contrast = '{1}' Crop='##' colourvalue = '##' rotate='##' ><effects sharpen='##' greyscale='0' digimagic='0' sepia='0' defog='0' underwater='0' emboss='0' invert = '0' granite='0' hue ='##' cartoon = '0'></effects></image>";
        //const string _layeringdata = "<photo zoomfactor='{0}' border='{1}' bg= '{2}' canvasleft='{3}' canvastop='{4}' scalecentrex='{5}' scalecentrey='{6}'>{7}</photo>";
        const string _rootPath = "\\";
        bool _IsEnabledSessionSelect = false;
        bool _IsEnabledSelectEndPoint = false;
        public int _locationId = 0;
        public int _substoreId = 0;
        public static int _photogrpherIdS;
        public static Int64 _startingNoS;
        public static int _locationIdS;
        public static int _substoreIdS;
        string jpgExtension = ConfigurationManager.AppSettings["jpgFileExtension"];
        string pngExtension = ConfigurationManager.AppSettings["pngFileExtension"];



        //bool isResetDPIRequired = false;
        Dictionary<string, string> lstSubStore;
        //DeviceManager deviceManager = null;
        //private DG_SemiOrder_Settings objDG_SemiOrder_Settings;
        //static bool _isrotated = false;
        private String _BorderFolder;
        string _Directoryname = string.Empty;
        private bool _is_SemiOrder;
        BackgroundWorker bwProgress = new BackgroundWorker();
        /// <summary>
        /// The path
        /// </summary>
        string _path = string.Empty;
        //string _defaultBrightness = string.Empty;
        //string _defaultContrast = string.Empty;
        char valueSeparator = '-';
        /// <summary>
        /// The thumbnailspath
        /// </summary>
        //string thumbnailspath = string.Empty;
        /// <summary>
        /// The _parent
        /// </summary>
        private UIElement _parent;
        /// <summary>
        /// The _hide request
        /// </summary>
        private bool _hideRequest = false;
        /// <summary>
        /// The _result
        /// </summary>
        private string _result;
        /// <summary>
        /// The is automatic rotate
        /// </summary>
        Nullable<bool> _IsAutoRotate;
        /// <summary>
        /// The count
        /// </summary>
        static int _count = 0;
        /// <summary>
        /// The _obj data layer
        /// </summary>
        //DigiPhotoDataServices _objDataLayer;
        /// <summary>
        /// The _location list
        /// </summary>
        private Dictionary<string, Int32> _locationList;
        /// <summary>
        /// The image name
        /// </summary>
        private List<MyImageClass> imagelist;
        bool _IsBarcodeActive = false;
        Int32 _MappingType = 0;
        Int32 _scanType = 0;
        bool _IsDelete = false;
        bool _IsAnonymousCodeActive = false;
        string _QRWebURLReplace = string.Empty;
        [System.Runtime.InteropServices.DllImport("gdi32.dll")]
        public static extern bool DeleteObject(IntPtr hObject);
        /// <summary>
        /// Gets or sets the imagelist.
        /// </summary>
        /// <value>
        /// The imagelist.
        /// </value>
        public List<MyImageClass> Imagelist
        {
            get { return imagelist; }
            set { imagelist = value; }
        }
        /// <summary>
        /// Gets or sets the location list.
        /// </summary>
        /// <value>
        /// The location list.
        /// </value>
        public Dictionary<string, Int32> LocationList
        {
            get { return _locationList; }
            set { _locationList = value; }
        }
        /// <summary>
        /// The _photo grapher list
        /// </summary>
        private Dictionary<string, Int32> _photoGrapherList;
        /// <summary>
        /// Gets or sets the photo grapher list.
        /// </summary>
        /// <value>
        /// The photo grapher list.
        /// </value>
        public Dictionary<string, Int32> PhotoGrapherList
        {
            get { return _photoGrapherList; }
            set { _photoGrapherList = value; }
        }

        /// <summary>
        /// Gets or sets the image meta data.
        /// </summary>
        /// <value>
        /// The image meta data.
        /// </value>
        public string ImageMetaData
        {
            get;
            set;
        }

        public string DefaultEffects
        {
            get;
            set;
        }
        public Hashtable htVidL = new Hashtable();
        /// <summary>
        /// The thumbnailspath
        /// </summary>
        string _thumbnailsPath = string.Empty;
        /// <summary>
        /// The filepath
        /// </summary>
        string _filepath;
        string _filepathdate;
        string _thumbnailFilePathDate;
        string _bigThumbnailFilePathDate;
        long _photoNo;
        string _photographerId = string.Empty;
        int _locationid;
        string _locationName = string.Empty;
        int _subStoreid;
        int _characterid;
        string _subStoreName = string.Empty;
        ///private string _networkPath = @"\\192.168.29.179\DigiImages\ManualDownload";
        ///private string _networkBackupPath = @"\\192.168.29.179\DigiImages\ManualDownlaodBackup";
        List<PhotoGraphersInfo> lstUsers;
        PhotoBusiness photoBusiness = new PhotoBusiness(); // Added by Suraj for Display Number Sequance...
        private System.ComponentModel.BackgroundWorker _manualDownloadWorkerProcessor = new System.ComponentModel.BackgroundWorker();

        bool RunManualDownloadLocWise = false;
        #endregion
        ///string filepath;
        ///bool isAutoVideoProcessingActive = false;
        public bool _isEnabledImageEditingMDownload = false;
        /// <summary>
        /// Initializes a new instance of the <see cref="ManualDownload"/> class.
        /// </summary>
        public ManualDownload()
        {
#if DEBUG
            var watch = FrameworkHelper.CommonUtility.Watch();
            watch.Start();
#endif
            try
            {

                this.InitializeComponent();
                MsgBox.SetParent(MainPanel);
                DownloadProgress.Minimum = 0;
                _count = 0;
                btnDownload.Content = "Download";

                lstUsers = (new UserBusiness()).GetPhotoGraphersList(LoginUser.StoreId);
                CommonUtility.BindComboWithSelect<PhotoGraphersInfo>(CmbPhotographerNo, lstUsers, "DG_User_Name", "DG_User_pkey", 0, ClientConstant.SelectString);
                CmbPhotographerNo.SelectedValue = "0";

                LocationBusniess locBiz = new LocationBusniess();
                List<LocationInfo> lstlocations = locBiz.GetLocationList(LoginUser.StoreId);

                CharacterBusiness charBiz = new CharacterBusiness();
                List<CharacterInfo> lstCharacters = charBiz.GetCharacter();
                CommonUtility.BindComboWithSelect<LocationInfo>(CmbLocation, lstlocations, "DG_Location_Name", "DG_Location_pkey", 0, ClientConstant.SelectString);
                CmbLocation.SelectedValue = "0";

                CommonUtility.BindComboWithSelect<CharacterInfo>(CmbCharacter, lstCharacters, "DG_Character_Name", "DG_Character_Pkey", 0, ClientConstant.SelectString);
                CmbCharacter.SelectedValue = "0";
                lstSubStore = new Dictionary<string, string>();
                //DigiPhotoDataServices _objDataLayer = new DigiPhotoDataServices();
                foreach (var item in new LocationBusniess().GetSubstoreData())
                {
                    lstSubStore.Add(item.DG_SubStore_Name, item.DG_SubStore_pkey.ToString());
                }
                CmbSubstore.ItemsSource = lstSubStore;

                CmbSubstore.SelectedValue = LoginUser.SubStoreId.ToString();
                ConfigurationInfo config = (new ConfigBusiness()).GetConfigurationData(LoginUser.SubStoreId);
                _QRWebURLReplace = locBiz.GetQRCodeWebUrl();
                _filepath = config.DG_Hot_Folder_Path;
                _filepathdate = Path.Combine(_filepath, DateTime.Now.ToString("yyyyMMdd"));
                _thumbnailFilePathDate = Path.Combine(_filepath, "Thumbnails", DateTime.Now.ToString("yyyyMMdd"));
                _bigThumbnailFilePathDate = Path.Combine(_filepath, "Thumbnails_Big", DateTime.Now.ToString("yyyyMMdd"));
                if (!Directory.Exists(_filepathdate))
                    Directory.CreateDirectory(_filepathdate);
                if (!Directory.Exists(_thumbnailFilePathDate))
                    Directory.CreateDirectory(_thumbnailFilePathDate);
                if (!Directory.Exists(_bigThumbnailFilePathDate))
                    Directory.CreateDirectory(_bigThumbnailFilePathDate);
                if (!Directory.Exists(Path.Combine(_filepath, "ManualDownload")))
                    Directory.CreateDirectory(Path.Combine(_filepath, "ManualDownload"));
                if (!Directory.Exists(Path.Combine(_filepath, "ManualDownloadBackup")))
                    Directory.CreateDirectory(Path.Combine(_filepath, "ManualDownloadBackup"));
                _BorderFolder = config.DG_Frame_Path;
                _IsAutoRotate = config.DG_IsAutoRotate;
                _is_SemiOrder = config.DG_SemiOrderMain.ToBoolean();
                showBeforeImage();
                bwProgress.DoWork += bwProgress_DoWork;
                bwProgress.RunWorkerCompleted += bwProgress_RunWorkerCompleted;
                ///////commented by latika for locationwise setting 2019-07-04
                StoreInfo store = new StoreInfo();
                TaxBusiness taxBusiness = new TaxBusiness();
                //store = taxBusiness.getTaxConfigData();

                //RunManualDownloadLocWise = store.RunManualDownloadLocationWise;
                ////end
                //////////changed by latika 
               
                store = taxBusiness.getTaxConfigDataBySystemID(Environment.MachineName);////changed by latika for locationwise settings

                RunManualDownloadLocWise = store.RunManualDownloadLocationWise;

                ///commented by latika as Per the requirement
                //if (RunManualDownloadLocWise == false)
                //{
                //    MessageBox.Show("Manual Download is not configured in Locationwise setting with this System-" + Environment.MachineName + ". \n Currently running sitewise for faster process Configure Locationwise setting from DigConfig-VOS .");
                //    // return;
                //}
                ///else { MessageBox.Show("Manual Download is  -" + Environment.MachineName + " ,hence it will run substore level."); }
                /////end
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
            }

#if DEBUG
            if (watch != null)
                FrameworkHelper.CommonUtility.WatchStop(this.GetType().FullName + "." + System.Reflection.MethodBase.GetCurrentMethod().Name + "|", watch);
#endif
        }

        void bwProgress_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            throw new NotImplementedException();
        }

        void bwProgress_DoWork(object sender, DoWorkEventArgs e)
        {
            throw new NotImplementedException();
        }

        void showBeforeImage()
        {
            btnOk.Visibility = Visibility.Visible;
            btnDownload.Visibility = Visibility.Collapsed;
            //btnBack.Visibility = Visibility.Collapsed;
            //tbStartingNo.IsEnabled = false;
            Lastimageno.Visibility = Visibility.Visible;
            DownloadProgress.Visibility = Visibility.Collapsed;
            tblStartingNo.Visibility = Visibility.Visible;
            tbStartingNo.Visibility = Visibility.Visible;
            //MainPanel.Height = 400; 
        }

        void showAfterImage()
        {
            btnOk.Visibility = Visibility.Collapsed;
            btnDownload.Visibility = Visibility.Visible;
            //btnBack.Visibility = Visibility.Visible;
            //tbStartingNo.IsEnabled = true;
            CmbPhotographerNo.IsEnabled = false;
            CmbLocation.IsEnabled = false;
            tbStartingNo.IsEnabled = false;
            HideHandlerDialog();
            Lastimageno.Visibility = Visibility.Visible;
            DownloadProgress.Visibility = Visibility.Visible;
            tblStartingNo.Visibility = Visibility.Visible;
            tbStartingNo.Visibility = Visibility.Visible;
            MainPanel.Height = 600;
        }



        /// <summary>
        /// Sets the parent.
        /// </summary>
        /// <param name="parent">The parent.</param>
        public void SetParent(UIElement parent)
        {
            _parent = (UIElement)parent;
        }

        /// <summary>
        /// Shows the handler dialog.
        /// </summary>
        /// <returns></returns>
        public string ShowHandlerDialog()
        {
            try
            {
#if DEBUG
                var watch = FrameworkHelper.CommonUtility.Watch();
                watch.Start();
#endif
                Visibility = Visibility.Visible;

                _hideRequest = false;
                while (!_hideRequest)
                {
                    // HACK: Stop the thread if the application is about to close
                    if (this.Dispatcher.HasShutdownStarted ||
                        this.Dispatcher.HasShutdownFinished)
                    {
                        break;
                    }
                    // HACK: Simulate "DoEvents"
                    this.Dispatcher.Invoke(
                        DispatcherPriority.Background,
                        new ThreadStart(delegate { }));
                    Thread.Sleep(20);
                }

#if DEBUG
                if (watch != null)
                    FrameworkHelper.CommonUtility.WatchStop(this.GetType().FullName + "." + System.Reflection.MethodBase.GetCurrentMethod().Name + "|", watch);
#endif
            }
            catch (Exception en)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(en);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
            _result = "DownloadDone";
            return _result;
        }

        /// <summary>
        /// Hides the handler dialog.
        /// </summary>
        private void HideHandlerDialog()
        {
            try
            {
                _hideRequest = true;
                Visibility = Visibility.Collapsed;
                _parent.IsEnabled = true;
            }
            catch (Exception en)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(en);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }
        /// <summary>
        /// Handles the Click event of the btnDownload control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void btnDownload_Click(object sender, RoutedEventArgs e)
        {
#if DEBUG
            var watch = FrameworkHelper.CommonUtility.Watch();
            watch.Start();
#endif
            try
            {
                if (IsValidData())
                {
                    btnDownload.IsEnabled = false;
                    btnBack.IsEnabled = false;
                    //Go();
                    //BusyWindow bWindow = new BusyWindow();
                    //bWindow.Show();
                    //bWindow.BringIntoView();

                    string errorMessages = string.Empty;
                    if (CheckForRFIDEnabled(out errorMessages))
                    {
                        MoveFiles();
                    }
                    else
                    {
                        MessageBoxResult response = MessageBox.Show(errorMessages + "\n Do you still want to continue?", "Digiphoto", MessageBoxButton.YesNo, MessageBoxImage.Question);
                        if (response == MessageBoxResult.Yes)
                        {
                            MoveFiles();
                        }
                        else
                        {
                            //MessageBox.Show(errorMessages);
                            btnDownload.IsEnabled = true;
                            btnBack.IsEnabled = true;
                        }
                    }
                    //bWindow.Hide();
                    //bWindow.Close();
                }
                else
                {
                    //if (!IsSemiprofileValidation)
                    //{
                    //    //MessageBox.Show(ErrMessageSpecVal);
                    //}
                    //else
                    //{
                        MessageBox.Show("Please enter valid data");

                   // }
                }
            }
            catch (Exception ex)
            {
                btnDownload.IsEnabled = true;
                btnBack.IsEnabled = true;
                ErrorHandler.ErrorHandler.LogError(ex);

            }
#if DEBUG
            if (watch != null)
                FrameworkHelper.CommonUtility.WatchStop(this.GetType().FullName + "." + System.Reflection.MethodBase.GetCurrentMethod().Name + "|", watch);
#endif
        }

        private static volatile object _object = new Object();
        private static volatile object _object1 = new Object();



        /// <summary>
        /// The count images download
        /// </summary>
        int CountImagesDownload = 0;

        /// <summary>
        /// The needrotaion
        /// </summary>
        int needrotaion = 0;
        /// <summary>
        /// The photo layer
        /// </summary>
        string PhotoLayer = null;
        /// <summary>
        /// Determines whether [is valid data].
        /// </summary>
        /// <returns></returns>
        /// 
        bool IsSemiprofileValidation = false;
        private bool IsValidData()
        {
            bool retvalue = true;
            //IsSemiprofileValidation = checkSemiorderSpecValidation();
            if (CmbPhotographerNo.SelectedValue.ToString() == "0")
            {
                retvalue = false;
            }
            else if (CmbLocation.SelectedValue.ToString() == "0")
            {
                retvalue = false;
            }
            try
            {
                tbStartingNo.Text.ToInt64();
            }
            catch (Exception ex)
            {
                retvalue = false;
            }
            if (tbStartingNo.Text.Trim() == "")
            {
                retvalue = false;
            }
            //if (!IsSemiprofileValidation)
            //{
            //    retvalue = false; ;
            //}

            return retvalue;
        }

        /// <summary>
        /// Handles the Click event of the btnBack control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void btnBack_Click(object sender, RoutedEventArgs e)
        {
#if DEBUG
            var watch = FrameworkHelper.CommonUtility.Watch();
            watch.Start();
#endif
            if (btnOk.Visibility == Visibility.Visible)
            {
                ((ImageDownloader)Window.GetWindow(this)).btnBack_Click(sender, e);

                _hideRequest = true;

            }
            else
            {
                ((ImageDownloader)Window.GetWindow(this)).isImageAcquired = false;
                _result = string.Empty;
                HideHandlerDialog();
            }
#if DEBUG
            if (watch != null)
                FrameworkHelper.CommonUtility.WatchStop(this.GetType().FullName + "." + System.Reflection.MethodBase.GetCurrentMethod().Name + "|", watch);
#endif
        }
        /// <summary>
        /// Handles the SelectionChanged event of the CmbPhotographerNo control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="SelectionChangedEventArgs"/> instance containing the event data.</param>
        private void CmbPhotographerNo_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (CmbPhotographerNo.SelectedValue.ToInt32() > 0)
            {
                Lastimageno.Visibility = Visibility.Visible;
                try
                {
                    int locationId = 0;
                    int _subStoreID = 0;
                    if (GetRfidStatus(Convert.ToInt32(CmbPhotographerNo.SelectedValue), out locationId, out _subStoreID))
                    {
                        CmbSubstore.SelectedValue = _subStoreID;
                        CmbLocation.SelectedValue = locationId;
                        CmbLocation.IsEnabled = false;
                        CmbSubstore.IsEnabled = false;
                    }
                    else
                    {
                        CmbSubstore.SelectedValue = _subStoreID;
                        CmbLocation.SelectedValue = locationId;
                        CmbLocation.IsEnabled = true;
                        CmbSubstore.IsEnabled = true;
                    }
                    _subStoreName = ((System.Collections.Generic.KeyValuePair<string, string>)(CmbSubstore.SelectedItem)).Key.ToString();
                    PhotoBusiness photoBiz = new PhotoBusiness();
                    string LastImageId = photoBiz.GetPhotoGrapherLastImageId(CmbPhotographerNo.SelectedValue.ToInt32());
                    Lastimageno.Text = "Last Photo no. of " + (((PhotoGraphersInfo)(CmbPhotographerNo.SelectedItem)).FullName) + " is " + LastImageId;
                }
                catch (Exception ex)
                {
                    ErrorHandler.ErrorHandler.LogError(ex);

                }
            }
            else
            {
                Lastimageno.Text = string.Empty;
                CmbLocation.SelectedValue = 0;
                CmbSubstore.IsEnabled = true;
                CmbLocation.IsEnabled = true;
            }
        }
        private bool GetRfidStatus(int userId, out int locationId, out int _subStoreID)
        {
            locationId = 0;
            _subStoreID = 0;
            bool _isRfidEnabled = false;
            try
            {
                var _selctedUser = lstUsers.Where(r => r.DG_User_pkey == userId).FirstOrDefault();
                if (_selctedUser != null)
                {
                    int _locationId = _selctedUser.DG_Location_pkey;
                    int subStoreId = _selctedUser.DG_Substore_ID;
                    ConfigBusiness configBusiness = new ConfigBusiness();
                    List<iMixConfigurationLocationInfo> _objNewConfigList = configBusiness.GetConfigLocation(_locationId, subStoreId);
                    var list = _objNewConfigList.FirstOrDefault(x => x.IMIXConfigurationMasterId == 49);
                    if (list != null)
                        _isRfidEnabled = list.ConfigurationValue.ToBoolean();
                    if (_isRfidEnabled == true)
                    {
                        _subStoreID = subStoreId;
                        locationId = _locationId;
                        return true;
                    }
                    else
                    {
                        _subStoreID = subStoreId;
                        locationId = _locationId;
                        return false;
                    }
                }
            }
            catch (Exception)
            {

            }
            return false;
        }
        //private void GetNewConfigValues()
        //{
        //    //_objDataLayer = new DigiPhotoDataServices();
        //    ConfigBusiness config = new ConfigBusiness();
        //    List<iMIXConfigurationInfo> _objNewConfigList = config.GetNewConfigValues(LoginUser.SubStoreId);
        //    foreach (iMIXConfigurationInfo _objNewConfig in _objNewConfigList)
        //    {
        //        switch (_objNewConfig.IMIXConfigurationMasterId)
        //        {
        //            case 1:
        //                _IsBarcodeActive = _objNewConfig.ConfigurationValue.ToBoolean();
        //                break;
        //            case 2:
        //                _MappingType = _objNewConfig.ConfigurationValue.ToInt32();
        //                break;
        //            case 3:
        //                _scanType = _objNewConfig.ConfigurationValue.ToInt32();
        //                break;
        //            case 38:
        //                _IsAnonymousCodeActive = _objNewConfig.ConfigurationValue.ToBoolean();
        //                break;
        //            case (int)ConfigParams.Contrast:
        //                _defaultContrast = string.IsNullOrEmpty(_objNewConfig.ConfigurationValue) ? "1" : _objNewConfig.ConfigurationValue;
        //                break;

        //            case (int)ConfigParams.Brightness:
        //                _defaultBrightness = string.IsNullOrEmpty(_objNewConfig.ConfigurationValue) ? "0" : _objNewConfig.ConfigurationValue;
        //                break;
        //            case (int)ConfigParams.IsDeleteFromUSB:
        //                _IsDelete = _objNewConfig.ConfigurationValue.ToBoolean();
        //                break;
        //            case (int)ConfigParams.IsEnabledRFID:
        //                App.IsRFIDEnabled = _objNewConfig.ConfigurationValue != null && _objNewConfig.ConfigurationValue != "" ? _objNewConfig.ConfigurationValue.ToBoolean() : false;
        //                break;
        //            case (int)ConfigParams.RFIDScanType:
        //                if (App.IsRFIDEnabled == true && _objNewConfig.ConfigurationValue != null && _objNewConfig.ConfigurationValue != "")
        //                    App.RfidScanType = _objNewConfig.ConfigurationValue.ToInt32();
        //                else
        //                    App.RfidScanType = null;
        //                break;
        //            default:
        //                break;
        //        }
        //    }
        //}

        private void GetNewConfigLocationValues(int LocationId, int SubstoreId)
        {
            ConfigBusiness configBusiness = new ConfigBusiness();

            //Updated by Anand - Get Location and Substore specific value.
            List<iMixConfigurationLocationInfo> _objNewConfigList = configBusiness.GetConfigLocation(LocationId, SubstoreId);
            //List<iMixConfigurationLocationInfo> _objNewConfigList = configBusiness.GetConfigBasedOnLocation(LocationId);


            foreach (iMixConfigurationLocationInfo _objNewConfig in _objNewConfigList)
            {
                switch (_objNewConfig.IMIXConfigurationMasterId)
                {
                    case (int)ConfigParams.IsBarcodeActive:
                        _IsBarcodeActive = _objNewConfig.ConfigurationValue.ToBoolean();
                        break;
                    case (int)ConfigParams.DefaultMappingCode:
                        _MappingType = _objNewConfig.ConfigurationValue.ToInt32();
                        break;
                    case (int)ConfigParams.ScanType:
                        _scanType = _objNewConfig.ConfigurationValue.ToInt32();
                        break;
                    case (int)ConfigParams.IsAnonymousQrCodeEnabled:
                        _IsAnonymousCodeActive = _objNewConfig.ConfigurationValue.ToBoolean();
                        break;
                    //case (int)ConfigParams.Contrast:
                    //    _defaultContrast = string.IsNullOrEmpty(_objNewConfig.ConfigurationValue) ? "1" : _objNewConfig.ConfigurationValue;
                    //    break;

                    //case (int)ConfigParams.Brightness:
                    //    _defaultBrightness = string.IsNullOrEmpty(_objNewConfig.ConfigurationValue) ? "0" : _objNewConfig.ConfigurationValue;
                    //    break;
                    case (int)ConfigParams.IsDeleteFromUSB:
                        _IsDelete = _objNewConfig.ConfigurationValue.ToBoolean();
                        break;
                    case (int)ConfigParams.IsEnabledRFID:
                        App.IsRFIDEnabled = _objNewConfig.ConfigurationValue != null && _objNewConfig.ConfigurationValue != "" ? _objNewConfig.ConfigurationValue.ToBoolean() : false;
                        break;
                    case (int)ConfigParams.RFIDScanType:
                        if (App.IsRFIDEnabled == true && _objNewConfig.ConfigurationValue != null && _objNewConfig.ConfigurationValue != "")
                            App.RfidScanType = _objNewConfig.ConfigurationValue.ToInt32();
                        else
                            App.RfidScanType = null;
                        break;
                    case (int)ConfigParams.IsEnabledSessionSelect:
                        _IsEnabledSessionSelect = string.IsNullOrEmpty(_objNewConfig.ConfigurationValue) ? false : _objNewConfig.ConfigurationValue.ToBoolean();
                        break;
                    case (int)ConfigParams.IsEnabledSelectEnd:
                        _IsEnabledSelectEndPoint = string.IsNullOrEmpty(_objNewConfig.ConfigurationValue) ? false : _objNewConfig.ConfigurationValue.ToBoolean();
                        break;
                    case (int)ConfigParams.IsEnableImageEditingMDownload:
                        _isEnabledImageEditingMDownload = string.IsNullOrEmpty(_objNewConfig.ConfigurationValue) ? false : _objNewConfig.ConfigurationValue.ToBoolean();
                        break;

                    default:
                        break;
                }
            }
        }


        private void btnOk_Click(object sender, RoutedEventArgs e)
        {
#if DEBUG
            var watch = FrameworkHelper.CommonUtility.Watch();
            watch.Start();
#endif

            PhotoBusiness phBiz = new PhotoBusiness();
            string LastImageId = phBiz.GetPhotoGrapherLastImageId(CmbPhotographerNo.SelectedValue.ToInt32());
            if (string.IsNullOrEmpty(tbStartingNo.Text))
            {
                MessageBox.Show("Please enter starting number");
                return;
            }

            DateTime serverDateTime = (new CustomBusineses()).ServerDateTime();
            Int64 photoNo = tbStartingNo.Text.ToInt64();
            string filename = serverDateTime.Day.ToString() + serverDateTime.Month.ToString() + photoNo.ToString() + "_" + CmbPhotographerNo.SelectedValue + jpgExtension;
            PhotoBusiness phBIZ = new PhotoBusiness();
            int photoCount = phBIZ.PhotoCountCurrentDate(RFID: photoNo.ToString(), photographerID: Convert.ToInt32(CmbPhotographerNo.SelectedValue), mediaType: 0);
            //if (!phBIZ.CheckPhotos(filename, CmbPhotographerNo.SelectedValue.ToInt32()))
            if (photoCount > 0)
            {
                //bool result = MsgBox.ShowHandlerDialog(photoNo + " already exists for today.Do you still want to continue?", DigiMessageBox.DialogType.Confirm);
                MessageBoxResult result = MessageBox.Show(photoNo + " already exists for today.Do you still want to continue?", "Digiphoto", MessageBoxButton.YesNo, MessageBoxImage.Warning);
                if (result == MessageBoxResult.No)
                    return;
            }

            if (CmbLocation.SelectedValue != null && CmbPhotographerNo.SelectedValue != null && CmbLocation.SelectedValue.ToInt32() > 0 && CmbPhotographerNo.SelectedValue.ToInt32() > 0)
            {
                //Photographer RFID Enabled Location Check
                RfidBusiness business = new RfidBusiness();
                int? photographerLocationID = business.GetPhotographerRFIDEnabledLocation(CmbPhotographerNo.SelectedValue.ToInt32());
                if (photographerLocationID.HasValue)
                {
                    if (photographerLocationID.Value != CmbLocation.SelectedValue.ToInt32())
                    {
                        string msg = "The location you have selected is not correct.If you continue your association of images will not be correct.Do you wish to continue??";
                        bool result = MsgBox.ShowHandlerDialog(msg, DigiMessageBox.DialogType.Confirm);
                        if (!result)
                        {
                            return;
                        }
                    }
                }
                _photogrpherIdS = CmbPhotographerNo.SelectedValue.ToInt32();
                _startingNoS = tbStartingNo.Text.ToInt64();
                _locationIdS = CmbLocation.SelectedValue.ToInt32();
                _substoreIdS = CmbSubstore.SelectedValue.ToInt32();
                _locationId = CmbLocation.SelectedValue.ToInt32();
                _substoreId = CmbSubstore.SelectedValue.ToInt32();
                if (_locationId > 0 && _substoreId > 0)
                    GetNewConfigLocationValues(_locationId, _substoreId);

                if (chkCaptureAfterDummyScan.IsChecked == true)
                {

                    ConfigBusiness configBiz = new ConfigBusiness();
                    if (!configBiz.CheckDummyScan(Convert.ToInt64(CmbPhotographerNo.SelectedValue)))
                    {
                        MsgBox.ShowHandlerDialog("You cannot download capture for dummy scan as your dummy scan delay time is expired, Please try again with new dummy scanning process", DigiMessageBox.DialogType.OK);
                        //  MessageBox.Show("You can not download capture for dummy scan as your dummy scan is not processed yet, please try again after some time..", "DEI");
                        return;
                    }


                }
                ((ImageDownloader)Window.GetWindow(this)).IsEnabledSessionSelect = _IsEnabledSessionSelect;
                ((ImageDownloader)Window.GetWindow(this)).IsEnabledSelectEndPoint = _IsEnabledSelectEndPoint;
                showAfterImage();
            }
            else
            {
                MessageBox.Show("Select photographer and location.");
            }

#if DEBUG
            if (watch != null)
                FrameworkHelper.CommonUtility.WatchStop(this.GetType().FullName + "." + System.Reflection.MethodBase.GetCurrentMethod().Name + "|", watch);
#endif
        }

        private void CmbSubstore_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            int subStoreId = ((System.Collections.Generic.KeyValuePair<string, string>)(CmbSubstore.SelectedItem)).Value.ToInt32();
            List<LocationInfo> lstlocations = (new StoreSubStoreDataBusniess()).GetLocationSubstoreWise(subStoreId);
            CommonUtility.BindComboWithSelect<LocationInfo>(CmbLocation, lstlocations, "DG_Location_Name", "DG_Location_pkey", 0, ClientConstant.SelectString);
            CmbLocation.SelectedValue = "0";
            _subStoreName = ((System.Collections.Generic.KeyValuePair<string, string>)(CmbSubstore.SelectedItem)).Key.ToString();
            //FillLocationBysubstore( );
        }
        void MoveFiles()
        {
#if DEBUG
            var watch = FrameworkHelper.CommonUtility.Watch();
            watch.Start();
#endif

            _photoNo = tbStartingNo.Text.ToInt64();
            _photographerId = CmbPhotographerNo.SelectedValue.ToString();
            _locationid = CmbLocation.SelectedValue.ToInt32();
            _locationName = CmbLocation.Text;
            _subStoreid = Convert.ToInt32(CmbSubstore.SelectedValue);
            _characterid = Convert.ToInt32(CmbCharacter.SelectedValue);

            var newList = (from c in imagelist
                           where c.IsChecked == true
                           select c).OrderBy(p => p.CreatedDate).ToList();
            int imageCount = newList.Count();
            PhotoBusiness photoBiz = new PhotoBusiness();
            DateTime serverDate = (new CustomBusineses()).ServerDateTime();
            //string filename = serverDate.Day.ToString() + serverDate.Month.ToString() + _photoNo.ToString() + "_" + _photographerId.ToString() + ".jpg";
            string filename = serverDate.Day.ToString() + serverDate.Month.ToString() + _photoNo.ToString() + "_" + _photographerId.ToString() + jpgExtension;
            long lastPhotoNum = Convert.ToInt64(_photoNo) + imageCount;
            int photoCount = photoBiz.PhotoCountCurrentDate(RFID: lastPhotoNum.ToString(), photographerID: Convert.ToInt32(_photographerId), mediaType: 0, seriesFirstNum: Convert.ToInt64(_photoNo), seriesLastNum: lastPhotoNum);
            if (photoCount > 0)
            {
                //bool result = MsgBox.ShowHandlerDialog("Given RFID series overlaps with existing RFID.Do you still want to continue?", DigiMessageBox.DialogType.Confirm);
                MessageBoxResult result = MessageBox.Show("Entered starting No already exists.Do you still want to download with entered starting series.", "Digiphoto", MessageBoxButton.YesNo, MessageBoxImage.Warning);
                if (result == MessageBoxResult.No)
                {
                    btnDownload.IsEnabled = true;
                    btnBack.IsEnabled = true;
                    return;
                }
            }
            //if (!photoBiz.CheckPhotos(filename, _photographerId.ToInt32()))
            //{
            //    MessageBox.Show(_photoNo + " already exists for today");
            //    return;
            //}

            var photoNo = _photoNo;
            //----Start---------Hari------------PNG task---------19th Jn2018-------------
            List<MyImageClass> newLst = new List<MyImageClass>();
            //----End---------Hari------------PNG task---------19th Jn2018-------------
            foreach (var item in newList)
            {
                if (item.Image != null)
                {
                    if (Path.GetExtension(item.Image.ToString()) == ".png")
                    {
                        item.FileExtension = ".png";
                    }
                }
                item.PhotoNumber = photoNo;
                photoNo++;
                newLst.Add(item);
            }
            // Added by Suraj For Display Number Sequance ...
            List<ManualDisplayOrder> ImagesList = new List<ManualDisplayOrder>();
            long displayOrderNumber = 0;
            int batchCount = newList.Count();
            displayOrderNumber = photoBusiness.GetPhotosDisplayOrder(batchCount);

            foreach (var item in newList)
            {
                displayOrderNumber += 1;
                ImagesList.Add(new ManualDisplayOrder() { PhotoNumber = item.PhotoNumber,FileName = item.Title, DisplayOrder = displayOrderNumber });
            }

            photoBusiness.SaveManualDownloadOrderNumber(ImagesList);


            // End Changes ...
            ParallelOptions parOptions = new ParallelOptions();
            parOptions.MaxDegreeOfParallelism = 5; //use max (5) threads that are allowed.
            _photoNo--;

            //  DownloadProgress.Maximum = newList.Count;
            DownloadProgress.Maximum = newLst.Count;
            DownloadProgress.Value = 1;

            //Thread.Sleep(100);
            var taskDownload = Task.Factory.StartNew(() =>
            //ParallelDownload(newList)
             ParallelDownload(newLst)
                );

            taskDownload.ContinueWith(
                t =>
                {
                    //if (newList.Count > 0)
                    if (newLst.Count > 0)
                    {
                        MessageBox.Show(newLst.Count + " File(s)" + " Acquired" + " Successfully");
                        photoBiz.SaveDownloadSummary((lastPhotoNum - 1).ToString(), _photographerId);
                        _result = string.Empty;
                        HideHandlerDialog();
                    }
                }, TaskScheduler.FromCurrentSynchronizationContext()
                );
#if DEBUG
            if (watch != null)
                FrameworkHelper.CommonUtility.WatchStop(this.GetType().FullName + "." + System.Reflection.MethodBase.GetCurrentMethod().Name + "|", watch);
#endif
        }
        private void ParallelDownload(List<MyImageClass> newList)
        {
            ParallelOptions parOptions = new ParallelOptions();
            parOptions.MaxDegreeOfParallelism = 1; //use max (5) threads that are allowed.

            Parallel.For(0, newList.Count, parOptions, i =>
            {
                var info = new List<MyImageClass>();
                var j = i;
                //var count = newList.Count;

                info.Add(newList[j]);
                //Interlocked.Add(ref _photoNo, 1);
                CopyFromSourceToDestination(info, newList[j].PhotoNumber, _photographerId, _locationid, _subStoreid, _characterid);
                DownloadProgress.Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Input
                                       , new DispatcherOperationCallback(delegate
                                       {
                                           DownloadProgress.Value = DownloadProgress.Value + j;

                                           Thread.Sleep(50);
                                           //do what you need to do on UI Thread
                                           return null;
                                       }), null);



            });

        }
        private void CopyFromSourceToDestination(List<MyImageClass> imagelist, long PhotoNo, string PhotographerId, int locationid, int subStoreid, int characterid)
        {
            //1. File copy from USB to local drive
            //2. Get the USB Free
            //3. Now start processing from local to USB with db save 
            try
            {
                var item = imagelist[0];
                string updatexml = string.Empty;
                Assembly assemblyname = Assembly.GetExecutingAssembly();
                string directoryPath = System.IO.Path.GetDirectoryName(assemblyname.Location);
                XDocument doc = XDocument.Load(directoryPath + "\\" + "ImageEditingManualDownload.xml");
                //string directoryPath = @"\\DEIINDEV24\d\7.2.1.1_finalBuild\DigiPhotoEnhancedCode\DigiPhoto";
                //XDocument doc = XDocument.Load(directoryPath + @"\" + "ImageEditingManualDownload.xml");
                if (item.SettingStatus == SettingStatus.SpecUpdated)
                    updatexml = "_2";
                else if (item.SettingStatus == SettingStatus.Spec)
                    updatexml = "_1";
                else
                    updatexml = "_0";

                ///BELOW IF CONDITION ADDED FOR eDIT Image while manual download _by VinS_07092018
                //if (item.SettingStatus == SettingStatus.SpecUpdated && _isEnabledImageEditingMDownload)
                //    updatexml = "_0";

                DateTime serverDate = (new CustomBusineses()).ServerDateTime();
                string filename = string.Empty;// serverDate.Day.ToString() + serverDate.Month.ToString() + PhotoNo.ToString() + "_" + PhotographerId.ToString() + ".jpg";
                filename = serverDate.Day.ToString() + serverDate.Month.ToString() + "_" + PhotoNo.ToString() + "_"
                    + PhotographerId.ToString() + "_" + subStoreid.ToString() + "_" + locationid.ToString() + "_" + characterid.ToString() + getTagId(item.Title) + updatexml + item.FileExtension.ToLower();
                var picname = item.Title + item.FileExtension;
                if (!Directory.Exists(Path.Combine(_filepath, "ManualDownload", _subStoreName)))
                    Directory.CreateDirectory(Path.Combine(_filepath, "ManualDownload", _subStoreName));
                if (RunManualDownloadLocWise == true)
                {
                    if (!Directory.Exists(Path.Combine(_filepath, "ManualDownload", _subStoreName, _locationName)))
                        Directory.CreateDirectory(Path.Combine(_filepath, "ManualDownload", _subStoreName, _locationName));
                }
                if ((item.FileExtension == jpgExtension || item.FileExtension == pngExtension) && _isEnabledImageEditingMDownload)
                {
                    File.Copy(item.ImagePath.Replace("Thumbnails\\", ""), Path.Combine(_filepath, "ManualDownload", filename), true);
                }
                else
                {
                    if (RunManualDownloadLocWise != true)
                        File.Copy(Path.Combine(item.ImagePath, picname), Path.Combine(_filepath, "ManualDownload", _subStoreName, filename), true);
                    else
                    {
                        File.Copy(Path.Combine(item.ImagePath, picname), Path.Combine(_filepath, "ManualDownload", _subStoreName, _locationName, filename), true);
                    }
                }
                if (item.SettingStatus == SettingStatus.SpecUpdated)
                {
                    if (!string.IsNullOrWhiteSpace(item.NewEffectsXML))
                        doc.Element("root").Element("Effects").Value = item.NewEffectsXML.ToString();
                    if (!string.IsNullOrWhiteSpace(item.NewLayeringXML))
                        doc.Element("root").Element("Layering").Value = item.NewLayeringXML.ToString();

                    if (RunManualDownloadLocWise != true && _isEnabledImageEditingMDownload)
                    {
                        doc.Save(Path.Combine(_filepath, "ManualDownload", filename.Replace(jpgExtension, "").Replace(pngExtension, "") + ".xml"));
                    }
                    else
                        doc.Save(Path.Combine(_filepath, "ManualDownload", filename.Replace(jpgExtension, "").Replace(pngExtension, "") + ".xml"));

                    ////for saving png and jpg 
                    //if (RunManualDownloadLocWise != true)
                    //    doc.Save(Path.Combine(_filepath, "ManualDownload", _subStoreName, filename.Replace(jpgExtension, "").Replace(pngExtension, "") + ".xml"));
                    //else
                    //    doc.Save(Path.Combine(_filepath, "ManualDownload", filename.Replace(jpgExtension, "").Replace(pngExtension, "") + ".xml"));
                    //doc.Save(Path.Combine(_filepath, "ManualDownload", _subStoreName, _locationName, filename.Replace(".jpg", "") + ".xml"));
                }
                string cropManualDPath = string.Empty;
                if (RunManualDownloadLocWise != true)
                    cropManualDPath = Path.Combine(_filepath, "ManualDownload", _subStoreName, "Croped");
                else
                    cropManualDPath = Path.Combine(_filepath, "ManualDownload", _subStoreName, _locationName, "Croped");

                if (item.IsCropped.Equals(true))
                {
                    if (!Directory.Exists(cropManualDPath))
                        Directory.CreateDirectory(cropManualDPath);
                    File.Copy(item.ImagePath.Replace("Thumbnails\\", "Croped\\"), Path.Combine(cropManualDPath, filename));
                }

                Interlocked.Add(ref CountImagesDownload, 1);
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
            }

        }
        string getTagId(string imageName)
        {
            string tagId = string.Empty;
            //img name contains tags id in first part separeted by -
            tagId = imageName.Split(valueSeparator).FirstOrDefault();
            //In some cases the img name starts with -
            if (string.IsNullOrEmpty(tagId) && imageName.Split(valueSeparator).Length > 1)
                tagId = imageName.Split(valueSeparator)[1];

            if (!string.IsNullOrEmpty(tagId) && tagId.Length > 1 && imageName.Split(valueSeparator).Length > 1)
                tagId = "_" + tagId;
            else
                tagId = string.Empty;
            return tagId;
        }
        private void txtnumber_TextChanged(object sender, TextChangedEventArgs e)
        {
            string str = string.Empty;
            if (string.IsNullOrEmpty(((TextBox)sender).Text))

                str = string.Empty;
            else
            {
                double num = 0;
                bool success = double.TryParse(((TextBox)sender).Text, out num);
                if (success & num >= 0)
                {
                    ((TextBox)sender).Text.Trim();
                    str = ((TextBox)sender).Text;
                }
                else
                {
                    ((TextBox)sender).Text = str;
                    ((TextBox)sender).SelectionStart = ((TextBox)sender).Text.Length;
                }
            }
        }
        private void NumericOnly(System.Object sender, System.Windows.Input.TextCompositionEventArgs e)
        {
            e.Handled = IsTextNumeric(e.Text);

        }
        private static bool IsTextNumeric(string str)
        {
            System.Text.RegularExpressions.Regex reg = new System.Text.RegularExpressions.Regex("[^0-9]");
            return reg.IsMatch(str);

        }
        string GetCameraSerialNumber(MyImageClass image)
        {
            try
            {
                if (image != null)
                {
                    DateTime x = DateTime.Now;
                    string imageName = image.Title + jpgExtension;
                    string imagePath = image.ImagePath + @"\" + imageName;

                    if (File.Exists(imagePath))
                    {
                        //var filePath = Path.GetFullPath(image.ImagePath);
                        var directories = MetadataExtractor.ImageMetadataReader.ReadMetadata(imagePath);
                        foreach (var directory in directories)
                        {
                            foreach (var tag in directory.Tags)
                            {
                                if (tag.Name.Contains("Camera Serial Number"))
                                {
                                    if (tag.DirectoryName.ToLower().Contains("canon") || tag.DirectoryName.ToLower().Contains("nikon"))
                                    {
                                        return tag.Description;
                                        //double sec = (DateTime.Now.TimeOfDay.TotalMilliseconds - x.TimeOfDay.TotalMilliseconds);
                                        //MessageBox.Show(directory.Name + "-: " + tag.Name + "=" + tag.ToString().Split('-')[1].Split('(')[0].Trim() + " Time in Millisec = " + sec.ToString());
                                    }
                                    else
                                    {
                                        return tag.Description;
                                        //int sec = (DateTime.Now.TimeOfDay - x.TimeOfDay).Seconds;
                                        //MessageBox.Show(directory.Name + "-: " + tag.Name + "=" + tag.Description + " Time in Millisec = " + sec.ToString());
                                    }
                                }
                            }

                        }
                    }
                }
            }
            catch (Exception ex)
            {

            }
            return string.Empty;
        }
        bool CheckForRFIDEnabled(out string errorMessage)
        {
            errorMessage = string.Empty;
            try
            {
                ConfigBusiness configBusiness = new ConfigBusiness();
                CameraBusiness cameraBusiness = new CameraBusiness();
                var locationId = CmbLocation.SelectedValue.ToInt32();
                var photographerId = Convert.ToInt32(CmbPhotographerNo.SelectedValue.ToString());
                bool isRFEnabled = configBusiness.IsLocationRFIDEnabled(locationId);
                var cameraInfo = cameraBusiness.GetCameraByPhotographerId(photographerId);
                if (!isRFEnabled)
                    return true;
                if (cameraInfo != null)
                {
                    if (!string.IsNullOrEmpty(cameraInfo.SerialNo))
                    {

                        var newList = (from c in imagelist
                                       where c.IsChecked == true
                                       select c).OrderBy(p => p.CreatedDate).ToList();
                        if (newList != null && newList.Count > 0)
                        {
                            var firstImage = newList.FirstOrDefault();
                            if (firstImage != null)
                            {
                                var cameraSerialNumber = GetCameraSerialNumber(firstImage);
                                if (cameraInfo.SerialNo.Trim().ToLower().EndsWith(cameraSerialNumber.Trim().ToLower()) || cameraSerialNumber.Trim().ToLower().Equals(cameraInfo.SerialNo.Trim().ToLower()))
                                    return true;
                                else
                                {
                                    //errorMessage = "Invalid camera is selected by user! Please ensure that the attached camera is same as the selected camera.";
                                    errorMessage = "The camera of the selected location is different from the camera attached.";//Please ensure that the attached camera is same as the selected camera.";
                                    return false;
                                }
                            }
                            else
                            {
                                errorMessage = "Please select one or more image to continue.";
                            }
                        }
                        else
                        {
                            errorMessage = "Please select one or more image to continue.";
                        }
                    }
                    else
                    {
                        errorMessage = "The camera of the selected location doesn't have a serial number.";// Please add a serial number for the camera.";
                    }
                }
                else
                {
                    errorMessage = "The selected location doesn't have a camera.";// Please add a camera to the specified location.";
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
                errorMessage = "There was an exception while moving the files from Camera to the site";
            }
            return false;
        }
        #region added by Ajay sinha to identify the images exist in the folders for the spec profile on 12 Nov 18
        //scan BG and Frames folder for the file :4_6VBorder100000.jpg"
        //if the file does not exist then the message should be the file does not exist in the particalar folder
        //and will show the folder path.
        string ErrMessageSpecVal = null;
        bool checkSemiorderSpecValidation()
        {
            //check the spec related images(borders/background) 
            //is avalable in the folder while manual download for a particular location.
            ErrMessageSpecVal = string.Empty;
            var locationId = CmbLocation.SelectedValue.ToInt32();
            SemiOrderBusiness semiOrderBusiness = new SemiOrderBusiness();
            var orderSettings = semiOrderBusiness.GetSemiOrderSettings(_substoreId, locationId);
            foreach (var item in orderSettings)
            {
                //check frames exist in the frame folder.
                //get all the bac

                var ImageFrame = item.ImageFrame_Horizontal;// _BorderFolder
                if (ImageFrame != "")
                {
                    if (!File.Exists(_BorderFolder + ImageFrame))
                    {
                        ErrMessageSpecVal += "\n Border does not exist in the path " + _BorderFolder + ImageFrame + "..\n   Please verify..\n"; ;
                    }
                    if (!File.Exists(_BorderFolder + "\\Thumbnails\\" + ImageFrame))
                    {
                        ErrMessageSpecVal += " \n Border does not exist at the path " + _BorderFolder + ImageFrame + "..\n   Please verify"; ;
                    }
                }
                var bg = item.DG_SemiOrder_BG;// _BorderFolder
                if (bg != "")
                {
                    //get all the folders 
                    string BGPath = _BorderFolder.Replace("Frames", "BG");
                    string BGProductfolder = item.ProductName.Replace('*', 'x').Replace(" ", String.Empty);
                    if (!File.Exists(BGPath + "\\" + BGProductfolder + "\\" + bg))
                    {
                        ErrMessageSpecVal += "\n Backround does not exist at the path. " + BGPath + BGProductfolder + "\\" + bg + "\n Please add the background again for the spec profile";
                        IsSemiprofileValidation = false;
                    }
                }

                var graphics = item.DG_SemiOrder_Graphics_layer;// _BorderFolder
                if (graphics != "")
                {
                    //get all the folders 
                    string graphicsPath = _BorderFolder.Replace("Frames", "Graphics");

                    XElement myEle = XElement.Parse("<data>" + graphics + "</data>");
                    foreach (XElement itemlst in myEle.Elements())
                    {
                        List<XAttribute> obj = itemlst.Attributes().ToList();
                        string name = Convert.ToString(obj[1].Value);
                        if (!File.Exists(graphicsPath + "\\" + name))
                        {
                            ErrMessageSpecVal += "\n Graphics does not exist at the path. " + graphicsPath + "\\" + name + "\n Please add the graphics again for the spec profile";
                            IsSemiprofileValidation = false;
                        }
                    }

                }
                //check background exist in the frame folder.
            }
            if (ErrMessageSpecVal != "")
            {
                MessageBoxResult messageBoxResult = System.Windows.MessageBox.Show(ErrMessageSpecVal + "\n Are you sure want to Continue?", "Delete Confirmation", System.Windows.MessageBoxButton.YesNo);
                if (messageBoxResult == MessageBoxResult.No)
                {
                    return false;
                }
            }
            return true;
        }
        #endregion
    }
}
