﻿using DigiPhoto.Utility.Repository.Data;
using DigiPhoto.IMIX.Business;
using DigiPhoto.IMIX.Model;
using DigiPhoto.Manage;
using FrameworkHelper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Xml;
using DigiAuditLogger;
using DigiPhoto.Common;
using DigiPhoto.Shader;
using System.IO;
using System.ServiceProcess;
using System.Diagnostics;
using System.Management;
using System.Text.RegularExpressions;
using System.Reflection;


namespace DigiPhoto
{
    /// <summary>
    /// Interaction logic for AddEditSpecPrintProfile.xaml
    /// </summary>
    public partial class AddEditSpecPrintProfile : UserControl
    {
        #region Constructor
        public AddEditSpecPrintProfile()
        {
            InitializeComponent();
            BindBorders();
            FillProductCombo();
            LoadGraphics();
            ucDynamicImgCrop.SetParent(grdSpecProf);
            if (chkBorder.IsChecked == false)
            {
                cmbVerticalBorder.IsEnabled = false;
                cmbborder.IsEnabled = false;
            }
            if (chkBG.IsChecked == false)
            {
                cmbHorizontalBG.IsEnabled = false;
                cmbVerticalBG.IsEnabled = false;
            }
            loadProductType();
            AddDefaultFontSize();
            lstTextCollection = new List<TextContent>();
            lstTextContent = new List<TextContent>();
            if (IsHorizontal)
            {
                cmbImageOrientation.SelectedIndex = 0;
            }
            else
            {
                cmbImageOrientation.SelectedIndex = 1;
            }
        }
        #endregion

        #region Declaration
        List<TextContent> lstTextCollection;
        List<TextContent> lstTextContent;
        private UIElement elementForContextMenu;
        Dictionary<string, string> lstBGList = null;
        string SpecFileName = string.Empty;
        public SemiOrderSettings semiOrderSettings { get; set; }
        private int _numValue = 0;
        public int NumValue
        {
            get { return _numValue; }
            set
            {
                _numValue = value;
                txtAngle.Text = value.ToString();
            }
        }
        private double _ZoomFactor = 1;
        private double _GraphicsZoomFactor = 1;
        private TransformGroup transformGroup;
        private ScaleTransform zoomTransform = new ScaleTransform();
        private UIElement _parentConfig;
        public void SetParentConfig(UIElement parent)
        {
            _parentConfig = (UIElement)parent;
        }
        int selectedProductType = 0;
        List<BorderInfo> lstBorderList;
        ContrastAdjustEffect _brighteff = new ContrastAdjustEffect();
        ContrastAdjustEffect _conteff = new ContrastAdjustEffect();
        double saturation = .38;
        double lightness = .15;
        public int LocationId { get; set; }
        public int SemiOrderPrimaryKey { get; set; }
        string itemNo;
        string HorizontalBorder = string.Empty;
        StringBuilder Graphics_HorizontalXml = new StringBuilder();
        string textLogo_Horizontal = string.Empty;
        string strzoomHorizontaldetails = string.Empty;
        bool IsHorizontalSettingsSaved = false;
        bool IsVerticalSettingsSaved = false;
        bool IsHorizontal = false;
        string HorizontalBackground = string.Empty;
        string VerticalBackground = string.Empty;
        string VerticalBorder = string.Empty;
        StringBuilder Graphics_VerticalXml = new StringBuilder();
        string textLogo_Vertical = string.Empty;
        string strzoomVerticalDetails = string.Empty;
        #endregion

        #region Events
        /// <summary>
        /// Handles the MouseLeftButtonUp event of the mainImage control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="MouseButtonEventArgs"/> instance containing the event data.</param>
        private void mainImage_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            if (e.LeftButton == MouseButtonState.Released)
            {
                this.elementForContextMenu = null;
            }
        }
        /// <summary>
        /// Handles the MouseLeftButtonUp event of the mainImage_Size control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="MouseButtonEventArgs"/> instance containing the event data.</param>
        private void mainImage_Size_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            if (e.LeftButton == MouseButtonState.Released)
            {
                this.elementForContextMenu = null;
            }
        }
        /// <summary>
        /// Handles the SelectionChanged event of the cmbProductType control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="SelectionChangedEventArgs"/> instance containing the event data.</param>
        //private void cmbProductType_SelectionChanged(object sender, SelectionChangedEventArgs e)
        //{
        //    if (cmbProductType.SelectedValue != null)
        //    {
        //        loadProductType(Convert.ToInt32(cmbProductType.SelectedValue));
        //        if (cmbProductType.SelectedValue.ToString() == "1")
        //        {
        //            canbackground.Height = 401.25;
        //            canbackground.Width = 535;
        //            canbackground.InvalidateArrange();
        //            canbackground.InvalidateMeasure();
        //            canbackground.UpdateLayout();
        //        }
        //        else if (cmbProductType.SelectedValue.ToString() == "2")
        //        {
        //            canbackground.Height = 428;
        //            canbackground.Width = 535;
        //            canbackground.InvalidateArrange();
        //            canbackground.InvalidateMeasure();
        //            canbackground.UpdateLayout();
        //        }
        //        else if(cmbProductType.SelectedValue.ToString() == "98")
        //        {
        //            canbackground.Height = 535;
        //            canbackground.Width = 535;
        //            canbackground.InvalidateArrange();
        //            canbackground.InvalidateMeasure();
        //            canbackground.UpdateLayout();
        //        }
        //        else
        //        {
        //            canbackground.Height = 356.67;
        //            canbackground.Width = 535;
        //            canbackground.InvalidateArrange();
        //            canbackground.InvalidateMeasure();
        //            canbackground.UpdateLayout();
        //        }
        //    }
        //}
        private void chkCrop_Checked(object sender, RoutedEventArgs e)
        {
            btnCropEdit.IsEnabled = true;
        }
        private void chkCrop_Unchecked(object sender, RoutedEventArgs e)
        {
            btnCropEdit.IsEnabled = false;
        }
        /// <summary>
        /// Handles the Click event of the btnSaveAutoCorrect control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        /// 
        private void btnSaveAutoCorrect_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                string productIds = GetSelectedProducts();
                if (string.IsNullOrWhiteSpace(productIds))
                {
                    MessageBox.Show("Please select product type.");
                    return;
                }
                if (!IsHorizontalSettingsSaved)
                {
                    MessageBox.Show("Please save settings for horizontal image.");
                    return;
                }
                if (!IsVerticalSettingsSaved)
                {
                    MessageBox.Show("Please save settings for vertical image.");
                    return;
                }
                if (chkBG.IsChecked == true && cmbHorizontalBG.SelectedIndex == 0)
                {
                    MessageBox.Show("Please select background.");
                    return;
                }
                if (chkCrop.IsChecked == true && String.IsNullOrWhiteSpace(Configuration.HorizontalCropCoordinates) && String.IsNullOrWhiteSpace(Configuration.VerticalCropCoordinates))
                {
                    MessageBox.Show("Please Set the cropping Coordinates");
                    return;
                }

                string bgvalue = String.Empty;
                if (cmbHorizontalBG.SelectedItem != null && cmbHorizontalBG.SelectedIndex > 0)
                {
                    bgvalue = ((System.Collections.Generic.KeyValuePair<string, string>)(cmbHorizontalBG.SelectedItem)).Key;
                }

                SemiOrderSettings objSemi = new SemiOrderSettings();
                objSemi.DG_SemiOrder_Settings_Pkey = SemiOrderPrimaryKey;
                objSemi.DG_SemiOrder_Settings_AutoBright = chkbrightness.IsChecked;
                objSemi.DG_SemiOrder_Settings_AutoBright_Value = Convert.ToDouble(txtbrightness.Text == "" ? "0" : txtbrightness.Text);
                objSemi.DG_SemiOrder_Settings_AutoContrast = chkcontrast.IsChecked;
                objSemi.DG_SemiOrder_Settings_AutoContrast_Value = Convert.ToDouble(txtcontrast.Text);
                objSemi.ImageFrame_Horizontal = HorizontalBorder;
                objSemi.DG_SemiOrder_Settings_IsImageFrame = chkBorder.IsChecked;
                objSemi.DG_SemiOrder_ProductTypeId = productIds;
                objSemi.ImageFrame_Vertical = VerticalBorder;
                objSemi.DG_SemiOrder_Environment = chkEnableSOrder.IsChecked;
                objSemi.Background_Horizontal = bgvalue;
                objSemi.DG_SemiOrder_Settings_IsImageBG = chkBG.IsChecked;
                objSemi.Graphics_layer_Horizontal = Graphics_HorizontalXml.ToString();
                objSemi.Graphics_layer_Vertical = Graphics_VerticalXml.ToString();
                objSemi.ZoomInfo_Horizontal = strzoomHorizontaldetails;
                objSemi.ZoomInfo_Vertical = strzoomVerticalDetails;
                objSemi.DG_SubStoreId = LoginUser.SubStoreId;
                objSemi.DG_SemiOrder_IsPrintActive = chkPrintActive.IsChecked;
                objSemi.DG_SemiOrder_IsCropActive = chkCrop.IsChecked;
                objSemi.VerticalCropValues = Configuration.VerticalCropCoordinates;
                objSemi.HorizontalCropValues = Configuration.HorizontalCropCoordinates;
                objSemi.DG_LocationId = LocationId; //cmbLocation.SelectedValue.ToInt32();
                objSemi.ChromaColor = cmbChromaClr.SelectedValue.ToString();
                objSemi.ColorCode = txtColorCode.Text;
                objSemi.ClrTolerance = txtChromaTol.Text;
                objSemi.TextLogo_Horizontal = textLogo_Horizontal;
                objSemi.TextLogo_Vertical = textLogo_Vertical;
                int id = (new ConfigBusiness()).SetSemiorderConfigurationData(objSemi);

                if (id > 0)
                {
                    System.Windows.Forms.DialogResult dialogResult = System.Windows.Forms.MessageBox.Show("Spec Printing settings has been saved successfully.\n\nYou need to restart the following services and application for this site.\nClick on Yes to re-start automatically.\n\n1) DigiWatchManualProcess service. \n\n2) DigiWatcher application. \n\n3) ImageProcessingEngine application.", "I-MIX", System.Windows.Forms.MessageBoxButtons.YesNo);

                    if (dialogResult == System.Windows.Forms.DialogResult.Yes)
                    {
                        string Message = "Spec Printing settings has been saved successfully.";
                        Message = "1) " + ServiceStart("DigiWatcher", true);
                        Message = Message + "\n\n2) " + ServiceStart("ImageProcessingEngine", true);
                        Message = Message + "\n\n3) " + ServiceStart("DigiWatchManualProcess", false);
                        System.Windows.Forms.MessageBox.Show("Spec settings are saved.\n\n" + Message, "I-MIX", System.Windows.Forms.MessageBoxButtons.OK);
                        AuditLog.AddUserLog(Common.LoginUser.UserId, (int)FrameworkHelper.ActionType.AddEditSpecPrintData, "Add/Edit spec print data at:- ");
                        ServicePosInfoBusiness svcPosinfoBusiness = new ServicePosInfoBusiness();
                        string ret = svcPosinfoBusiness.ServiceStart(true);
                    }

                    backAndClose();
                }
                RemoveTextFields();
                lstTextCollection.Clear();
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }

        private void btnSaveHorizontal_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                string productIds = GetSelectedProducts();
                if (string.IsNullOrWhiteSpace(productIds))
                {
                    MessageBox.Show("Please select product type.");
                    return;
                }
                if (chkBorder.IsChecked == true && cmbborder.SelectedIndex == 0)
                {
                    MessageBox.Show("Please select border for Landscape Mode");
                    return;
                }
                if (cmbborder.SelectedItem != null)
                {
                    HorizontalBorder = ((BorderInfo)(cmbborder.SelectedItem)).DG_Border;
                    if (HorizontalBorder.Equals("--Select--", StringComparison.CurrentCultureIgnoreCase))
                    {
                        HorizontalBorder = String.Empty;
                    }
                }
                else
                {
                    HorizontalBorder = String.Empty;
                }

                if (chkBG.IsChecked == true && cmbHorizontalBG.SelectedIndex == 0)
                {
                    MessageBox.Show("Please select background.");
                    return;
                }
                if (chkCrop.IsChecked == true && String.IsNullOrWhiteSpace(Configuration.HorizontalCropCoordinates))
                {
                    MessageBox.Show("Please Set the cropping Coordinates for Horizontal Images");
                    return;
                }

                if (cmbHorizontalBG.SelectedItem != null && cmbHorizontalBG.SelectedIndex > 0)
                {
                    HorizontalBackground = ((System.Collections.Generic.KeyValuePair<string, string>)(cmbHorizontalBG.SelectedItem)).Key;
                }

                Graphics_HorizontalXml.Length = 0;//////////added by latika for removing duplicate entries  
                foreach (var ctrl in dragCanvas.Children)
                {
                    if (ctrl is Button)
                    {
                        Button cid = (Button)ctrl;
                        string Angle = "-1";
                        try
                        {
                            Angle = ((System.Windows.Media.RotateTransform)(cid.RenderTransform)).Angle.ToString();
                        }
                        catch (Exception)
                        {

                        }
                        double top1 = Canvas.GetTop(cid);
                        double left1 = Canvas.GetLeft(cid);
                        string source = ((System.Windows.Controls.Image)(cid.Content)).Source.ToString();
                        int segment = source.Split('/').Count();
                        source = source.Split('/')[segment - 1].ToString();

                        TransformGroup tg = cid.GetValue(Canvas.RenderTransformProperty) as TransformGroup;
                        RotateTransform rotation = new RotateTransform();
                        ScaleTransform scale = new ScaleTransform();

                        if (tg != null)
                        {
                            if (tg.Children.Count > 0)
                            {
                                if (tg.Children[0] is ScaleTransform)
                                {
                                    scale = (ScaleTransform)tg.Children[0];
                                }
                                else if (tg.Children[0] is RotateTransform)
                                {
                                    rotation = (RotateTransform)tg.Children[0];
                                    Angle = rotation.Angle.ToString();
                                }
                            }
                            if (tg.Children.Count > 1)
                            {
                                if (tg.Children[1] is ScaleTransform)
                                {
                                    scale = (ScaleTransform)tg.Children[1];
                                }
                                else if (tg.Children[1] is RotateTransform)
                                {
                                    rotation = (RotateTransform)tg.Children[1];
                                    Angle = rotation.Angle.ToString();
                                }
                            }

                        }
                        Graphics_HorizontalXml.Append("<graphics wthsource ='" + cid.Width + "' source ='" + source + "' angle='" + Angle + "' top ='" + top1 + "' left='" + left1 + "' scalex ='" + scale.ScaleX.ToString() + "' scaley ='" + scale.ScaleY.ToString() + "' zoomfactor = '" + cid.Tag.ToString() + "' zindex='4'></graphics>");
                    }
                }
                string photo_outputBorder = System.Windows.Markup.XamlWriter.Save(GrdBrightness);
                System.Xml.XmlDocument XdocumentBorder = new System.Xml.XmlDocument();
                XdocumentBorder.LoadXml(photo_outputBorder);
                StringBuilder ResultXmlBorder = new StringBuilder();
                string _canvasleft = "-1";
                string _canvastop = "-1";
                string _rotatetransform = "-1";
                string _scalecentrex = "-1";
                string _scalecentrey = "-1";
                foreach (var xn in (XdocumentBorder.ChildNodes[0]).Attributes)
                {
                    switch (((System.Xml.XmlAttribute)xn).Name.ToLower())
                    {
                        case "canvas.left":
                            _canvasleft = ((System.Xml.XmlAttribute)xn).Value.ToString();
                            break;
                        case "canvas.top":
                            _canvastop = ((System.Xml.XmlAttribute)xn).Value.ToString();
                            break;
                    }
                }

                System.Xml.XmlDocument Xdocument = new System.Xml.XmlDocument();
                Xdocument.LoadXml(photo_outputBorder);
                System.Xml.XmlReader rdr = System.Xml.XmlReader.Create(new System.IO.StringReader(Xdocument.InnerXml.ToString()));
                while (rdr.Read())
                {
                    if (rdr.NodeType == XmlNodeType.Element)
                    {
                        switch (rdr.Name.ToString().ToLower())
                        {
                            case "scaletransform":
                                if (rdr.GetAttribute("CenterX") != null)
                                {
                                    _scalecentrex = rdr.GetAttribute("CenterX").ToString();
                                }
                                if (rdr.GetAttribute("CenterY") != null)
                                {
                                    _scalecentrey = rdr.GetAttribute("CenterY").ToString();
                                }
                                break;
                        }
                    }
                }
                textLogo_Horizontal = GetAllTextLogos();
                strzoomHorizontaldetails = _ZoomFactor.ToString() + "," + _canvasleft.ToString() + "," + _canvastop + "," + _scalecentrex + "," + _scalecentrey.ToString();
                SemiOrderSettings objSemi = new SemiOrderSettings();
                objSemi.DG_SemiOrder_Settings_Pkey = SemiOrderPrimaryKey;
                objSemi.DG_SemiOrder_Settings_AutoBright = chkbrightness.IsChecked;
                objSemi.DG_SemiOrder_Settings_AutoBright_Value = Convert.ToDouble(txtbrightness.Text == "" ? "0" : txtbrightness.Text);
                objSemi.DG_SemiOrder_Settings_AutoContrast = chkcontrast.IsChecked;
                objSemi.DG_SemiOrder_Settings_AutoContrast_Value = Convert.ToDouble(txtcontrast.Text);
                objSemi.ImageFrame_Horizontal = HorizontalBorder;
                objSemi.DG_SemiOrder_Settings_IsImageFrame = chkBorder.IsChecked;
                objSemi.DG_SemiOrder_ProductTypeId = productIds;
                objSemi.ImageFrame_Vertical = VerticalBorder;
                objSemi.DG_SemiOrder_Environment = chkEnableSOrder.IsChecked;
                objSemi.Background_Horizontal = HorizontalBackground;
                objSemi.Background_Vertical = VerticalBackground;
                objSemi.DG_SemiOrder_Settings_IsImageBG = chkBG.IsChecked;
                objSemi.Graphics_layer_Horizontal = Graphics_HorizontalXml.ToString();
                objSemi.Graphics_layer_Vertical = Graphics_VerticalXml.ToString();
                objSemi.ZoomInfo_Horizontal = strzoomHorizontaldetails;
                objSemi.ZoomInfo_Vertical = strzoomVerticalDetails;
                objSemi.DG_SubStoreId = LoginUser.SubStoreId;
                objSemi.DG_SemiOrder_IsPrintActive = chkPrintActive.IsChecked;
                objSemi.DG_SemiOrder_IsCropActive = chkCrop.IsChecked;
                objSemi.VerticalCropValues = Configuration.VerticalCropCoordinates;
                objSemi.HorizontalCropValues = Configuration.HorizontalCropCoordinates;
                objSemi.DG_LocationId = LocationId; //cmbLocation.SelectedValue.ToInt32();
                objSemi.ChromaColor = cmbChromaClr.SelectedValue.ToString();
                objSemi.ColorCode = txtColorCode.Text;
                objSemi.ClrTolerance = txtChromaTol.Text;
                objSemi.TextLogo_Horizontal = textLogo_Horizontal;
                objSemi.TextLogo_Vertical = textLogo_Vertical;

                SemiOrderPrimaryKey = (new ConfigBusiness()).SetSemiorderConfigurationData(objSemi);
                if (SemiOrderPrimaryKey > 0)
                {
                    System.Windows.Forms.DialogResult dialogResult = System.Windows.Forms.MessageBox.Show("Settings for Horizontal Images(Landscape Mode) saved successfully.\n\n Click YES if you want to save Vertical(Portrait) settings also.\n\n Click NO if already saved Vertical Settings or if do not want to save Vertical Settings.", "I-MIX", System.Windows.Forms.MessageBoxButtons.YesNo);
                    if (dialogResult == System.Windows.Forms.DialogResult.No)
                    {
                        System.Windows.Forms.DialogResult dialogResult1 = System.Windows.Forms.MessageBox.Show("Spec Printing settings has been saved successfully.\n\nYou need to restart the following services and application for this site.\nClick on Yes to re-start automatically.\n\n1) DigiWatchManualProcess service. \n\n2) DigiWatcher application. \n\n3) ImageProcessingEngine application.", "I-MIX", System.Windows.Forms.MessageBoxButtons.YesNo);

                        if (dialogResult1 == System.Windows.Forms.DialogResult.Yes)
                        {
                            string Message = "Spec Printing settings has been saved successfully.";
                            Message = "1) " + ServiceStart("DigiWatcher", true);
                            Message = Message + "\n\n2) " + ServiceStart("ImageProcessingEngine", true);
                            Message = Message + "\n\n3) " + ServiceStart("DigiWatchManualProcess", false);
                            System.Windows.Forms.MessageBox.Show("Spec settings are saved.\n\n" + Message, "I-MIX", System.Windows.Forms.MessageBoxButtons.OK);
                            AuditLog.AddUserLog(Common.LoginUser.UserId, (int)FrameworkHelper.ActionType.AddEditSpecPrintData, "Add/Edit spec print data at:- ");
                            ServicePosInfoBusiness svcPosinfoBusiness = new ServicePosInfoBusiness();
                            string ret = svcPosinfoBusiness.ServiceStart(true);
                        }
                        RemoveFieldsFromGrid();
                        RemoveTextFields();
                        ResetGridEffects();
                        lstTextCollection.Clear();
                        backAndClose();
                    }
                    else if (dialogResult == System.Windows.Forms.DialogResult.Yes)
                    {
                        cmbImageOrientation.SelectedIndex = 1;
                        chkBG.IsChecked = false;
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }

        private void RemoveFieldsFromGrid()
        {
            if (frm.Children.Count > 0)
            {
                for (int i = 0; i < frm.Children.Count; i++)
                {
                    frm.Children.RemoveAt(i);
                }
            }

            for (int j = 0; j < dragCanvas.Children.Count; j++)
            {
                if (dragCanvas.Children[j] is Button)
                {
                    dragCanvas.Children.RemoveAt(j);
                    j--;
                }
            }
        }

        private void btnSaveVertical_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                string productIds = GetSelectedProducts();
                if (string.IsNullOrWhiteSpace(productIds))
                {
                    MessageBox.Show("Please select product type.");
                    return;
                }
                //check validation
                if (chkBorder.IsChecked == true && cmbVerticalBorder.SelectedIndex == 0)
                {
                    MessageBox.Show("Please select border for Portrait Mode");
                    return;
                }
                if (cmbVerticalBorder.SelectedItem != null)
                {
                    VerticalBorder = ((BorderInfo)(cmbVerticalBorder.SelectedItem)).DG_Border;
                    if (VerticalBorder.Equals("--Select--", StringComparison.CurrentCultureIgnoreCase))
                    {
                        VerticalBorder = String.Empty;
                    }
                }
                else
                {
                    VerticalBorder = String.Empty;
                }
                if (chkBG.IsChecked == true && cmbVerticalBG.SelectedIndex == 0)
                {
                    MessageBox.Show("Please select background.");
                    return;
                }
                if (chkCrop.IsChecked == true && String.IsNullOrWhiteSpace(Configuration.VerticalCropCoordinates))
                {
                    MessageBox.Show("Please Set the cropping Coordinates for Horizontal Images");
                    return;
                }

                if (cmbVerticalBG.SelectedItem != null && cmbVerticalBG.SelectedIndex > 0)
                {
                    VerticalBackground = ((System.Collections.Generic.KeyValuePair<string, string>)(cmbVerticalBG.SelectedItem)).Key;
                }
                Graphics_VerticalXml.Length = 0;//////////added by latika for removing duplicate entries  
                foreach (var ctrl in dragCanvas.Children)
                {
                    if (ctrl is Button)
                    {
                        Button cid = (Button)ctrl;
                        string Angle = "-1";
                        try
                        {
                            Angle = ((System.Windows.Media.RotateTransform)(cid.RenderTransform)).Angle.ToString();
                        }
                        catch (Exception)
                        {

                        }
                        double top1 = Canvas.GetTop(cid);
                        double left1 = Canvas.GetLeft(cid);
                        string source = ((System.Windows.Controls.Image)(cid.Content)).Source.ToString();
                        int segment = source.Split('/').Count();
                        source = source.Split('/')[segment - 1].ToString();

                        TransformGroup tg = cid.GetValue(Canvas.RenderTransformProperty) as TransformGroup;
                        RotateTransform rotation = new RotateTransform();
                        ScaleTransform scale = new ScaleTransform();

                        if (tg != null)
                        {
                            if (tg.Children.Count > 0)
                            {
                                if (tg.Children[0] is ScaleTransform)
                                {
                                    scale = (ScaleTransform)tg.Children[0];
                                }
                                else if (tg.Children[0] is RotateTransform)
                                {
                                    rotation = (RotateTransform)tg.Children[0];
                                    Angle = rotation.Angle.ToString();
                                }
                            }
                            if (tg.Children.Count > 1)
                            {
                                if (tg.Children[1] is ScaleTransform)
                                {
                                    scale = (ScaleTransform)tg.Children[1];
                                }
                                else if (tg.Children[1] is RotateTransform)
                                {
                                    rotation = (RotateTransform)tg.Children[1];
                                    Angle = rotation.Angle.ToString();
                                }
                            }

                        }
                        Graphics_VerticalXml.Append("<graphics wthsource ='" + cid.Width + "' source ='" + source + "' angle='" + Angle + "' top ='" + top1 + "' left='" + left1 + "' scalex ='" + scale.ScaleX.ToString() + "' scaley ='" + scale.ScaleY.ToString() + "' zoomfactor = '" + cid.Tag.ToString() + "' zindex='4'></graphics>");
                    }
                }
                string photo_outputBorder = System.Windows.Markup.XamlWriter.Save(GrdBrightness);
                System.Xml.XmlDocument XdocumentBorder = new System.Xml.XmlDocument();
                XdocumentBorder.LoadXml(photo_outputBorder);
                StringBuilder ResultXmlBorder = new StringBuilder();
                string _canvasleft = "-1";
                string _canvastop = "-1";
                string _rotatetransform = "-1";
                string _scalecentrex = "-1";
                string _scalecentrey = "-1";
                foreach (var xn in (XdocumentBorder.ChildNodes[0]).Attributes)
                {
                    switch (((System.Xml.XmlAttribute)xn).Name.ToLower())
                    {
                        case "canvas.left":
                            _canvasleft = ((System.Xml.XmlAttribute)xn).Value.ToString();
                            break;
                        case "canvas.top":
                            _canvastop = ((System.Xml.XmlAttribute)xn).Value.ToString();
                            break;
                    }
                }

                System.Xml.XmlDocument Xdocument = new System.Xml.XmlDocument();
                Xdocument.LoadXml(photo_outputBorder);
                System.Xml.XmlReader rdr = System.Xml.XmlReader.Create(new System.IO.StringReader(Xdocument.InnerXml.ToString()));
                while (rdr.Read())
                {
                    if (rdr.NodeType == XmlNodeType.Element)
                    {
                        switch (rdr.Name.ToString().ToLower())
                        {
                            case "scaletransform":
                                if (rdr.GetAttribute("CenterX") != null)
                                {
                                    _scalecentrex = rdr.GetAttribute("CenterX").ToString();
                                }
                                if (rdr.GetAttribute("CenterY") != null)
                                {
                                    _scalecentrey = rdr.GetAttribute("CenterY").ToString();
                                }
                                break;
                        }
                    }
                }
                textLogo_Vertical = GetAllTextLogos();
                strzoomVerticalDetails = _ZoomFactor.ToString() + "," + _canvasleft.ToString() + "," + _canvastop + "," + _scalecentrex + "," + _scalecentrey.ToString();
                SemiOrderSettings objSemi = new SemiOrderSettings();
                objSemi.DG_SemiOrder_Settings_Pkey = SemiOrderPrimaryKey;
                objSemi.DG_SemiOrder_Settings_AutoBright = chkbrightness.IsChecked;
                objSemi.DG_SemiOrder_Settings_AutoBright_Value = Convert.ToDouble(txtbrightness.Text == "" ? "0" : txtbrightness.Text);
                objSemi.DG_SemiOrder_Settings_AutoContrast = chkcontrast.IsChecked;
                objSemi.DG_SemiOrder_Settings_AutoContrast_Value = Convert.ToDouble(txtcontrast.Text);
                objSemi.ImageFrame_Horizontal = HorizontalBorder;
                objSemi.DG_SemiOrder_Settings_IsImageFrame = chkBorder.IsChecked;
                objSemi.DG_SemiOrder_ProductTypeId = productIds;
                objSemi.ImageFrame_Vertical = VerticalBorder;
                objSemi.DG_SemiOrder_Environment = chkEnableSOrder.IsChecked;
                objSemi.Background_Vertical = VerticalBackground;
                objSemi.Background_Horizontal = HorizontalBackground;
                objSemi.DG_SemiOrder_Settings_IsImageBG = chkBG.IsChecked;
                objSemi.Graphics_layer_Horizontal = Graphics_HorizontalXml.ToString();
                objSemi.Graphics_layer_Vertical = Graphics_VerticalXml.ToString();
                objSemi.ZoomInfo_Horizontal = strzoomHorizontaldetails;
                objSemi.ZoomInfo_Vertical = strzoomVerticalDetails;
                objSemi.DG_SubStoreId = LoginUser.SubStoreId;
                objSemi.DG_SemiOrder_IsPrintActive = chkPrintActive.IsChecked;
                objSemi.DG_SemiOrder_IsCropActive = chkCrop.IsChecked;
                objSemi.VerticalCropValues = Configuration.VerticalCropCoordinates;
                objSemi.HorizontalCropValues = Configuration.HorizontalCropCoordinates;
                objSemi.DG_LocationId = LocationId;
                objSemi.ChromaColor = cmbChromaClr.SelectedValue.ToString();
                objSemi.ColorCode = txtColorCode.Text;
                objSemi.ClrTolerance = txtChromaTol.Text;
                objSemi.TextLogo_Horizontal = textLogo_Horizontal;
                objSemi.TextLogo_Vertical = textLogo_Vertical;
                SemiOrderPrimaryKey = (new ConfigBusiness()).SetSemiorderConfigurationData(objSemi);
                if (SemiOrderPrimaryKey > 0)
                {
                    System.Windows.Forms.DialogResult dialogResult = System.Windows.Forms.MessageBox.Show("Settings for Vertical Images(Portrait Mode) saved successfully.\n\n Click YES if you want to save Horizontal(Landscape) settings also. \n\n Click NO if Horizontal Settings already saved or if do not want to save horizontal Profile.", "I-MIX", System.Windows.Forms.MessageBoxButtons.YesNo);
                    if (dialogResult == System.Windows.Forms.DialogResult.No)
                    {
                        System.Windows.Forms.DialogResult dialogResult1 = System.Windows.Forms.MessageBox.Show("Spec Printing settings has been saved successfully.\n\nYou need to restart the following services and application for this site.\nClick on Yes to re-start automatically.\n\n1) DigiWatchManualProcess service. \n\n2) DigiWatcher application. \n\n3) ImageProcessingEngine application.", "I-MIX", System.Windows.Forms.MessageBoxButtons.YesNo);

                        if (dialogResult1 == System.Windows.Forms.DialogResult.Yes)
                        {
                            string Message = "Spec Printing settings has been saved successfully.";
                            Message = "1) " + ServiceStart("DigiWatcher", true);
                            Message = Message + "\n\n2) " + ServiceStart("ImageProcessingEngine", true);
                            Message = Message + "\n\n3) " + ServiceStart("DigiWatchManualProcess", false);
                            System.Windows.Forms.MessageBox.Show("Spec settings are saved.\n\n" + Message, "I-MIX", System.Windows.Forms.MessageBoxButtons.OK);
                            AuditLog.AddUserLog(Common.LoginUser.UserId, (int)FrameworkHelper.ActionType.AddEditSpecPrintData, "Add/Edit spec print data at:- ");
                            ServicePosInfoBusiness svcPosinfoBusiness = new ServicePosInfoBusiness();
                            string ret = svcPosinfoBusiness.ServiceStart(true);
                        }
                        RemoveFieldsFromGrid();
                        RemoveTextFields();
                        ResetGridEffects();
                        lstTextCollection.Clear();
                        backAndClose();
                    }
                    else if (dialogResult == System.Windows.Forms.DialogResult.Yes)
                    {
                        cmbImageOrientation.SelectedIndex = 0;
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }
        public string ServiceStart(string serviceName, bool IsExe)
        {
            try
            {
                bool restarted = false;
                string SystemName = getSystemName();
                string servicename = serviceName;
                //log.Error("servicename" + servicename + "SystemName" + SystemName);

                bool _Status = true;
                int Count = 0;
                string CreatedBy = "itadmin";
                List<GetServiceStatus> getServiceStatus = new List<GetServiceStatus>();
                ServicePosInfoBusiness servicePosBusiness = new ServicePosInfoBusiness();
                ServiceController serviceController = new ServiceController(servicename, SystemName);
                getServiceStatus = servicePosBusiness.GetServiceStatusBusiness(SystemName, servicename);

                Int32 ServiceId = 0;
                long SubstoreId = 0;
                Int32 RunLevel = 0;
                long newImixPosId = 0;

                if (getServiceStatus != null)
                {
                    ServiceId = getServiceStatus.Select(p => p.ServiceId).FirstOrDefault();
                    SubstoreId = getServiceStatus.Select(p => p.SubStoreID).FirstOrDefault();
                    RunLevel = getServiceStatus.Select(p => p.Runlevel).FirstOrDefault();
                    newImixPosId = getServiceStatus.Select(p => p.iMixPosId).FirstOrDefault();
                }
                string Value = servicePosBusiness.CheckRunnignServiceBusiness(ServiceId, SubstoreId, RunLevel, SystemName);

                //Process process = new Process();

                if (IsExe)
                {
                    if (Value == "")
                    {

                        // long ImixPosDetailId = ((DigiPhoto.IMIX.Model.ImixPOSDetail)(CmbPOSDetail.SelectedItem)).ImixPOSDetailID;
                        //ServicePosInfoBusiness servicePosBusiness = new ServicePosInfoBusiness();
                        servicePosBusiness.StartStopServiceByPosIdBusiness(ServiceId, SubstoreId, newImixPosId, false, "Webusers");

                        string filePath = new Uri(Assembly.GetExecutingAssembly().CodeBase).LocalPath;
                        filePath = System.IO.Path.GetDirectoryName(filePath);


                        //string fileNameToFilter = filePath + "\\" + serviceName + ".exe";

                        foreach (Process p in Process.GetProcesses())
                        {
                            if (servicename == "DigiWatcher" && p.ProcessName.Contains("DigiWifiImageProcessing"))
                            {
                                p.Kill();
                            }
                            if (p.ProcessName.Contains(servicename))
                            {
                                p.Kill();
                                restarted = true;
                                break;
                            }
                        }


                        Process process = Process.Start(filePath + "\\" + serviceName + ".exe");

                        if (restarted)
                        {
                            return serviceName + " application has been re-started successfully.";
                        }
                        else
                        {
                            return serviceName + " application has been started successfully.";
                        }

                    }
                    else
                    {
                        return "Please re-start " + serviceName + " application manually.";
                    }
                }
                else
                {
                    ServiceController service = new ServiceController(serviceName, SystemName);

                    if (Value == "")
                    {
                        Count = servicePosBusiness.StartStopServiceByPosIdBusiness(ServiceId, SubstoreId, newImixPosId, _Status, CreatedBy);

                        if (service.Status == ServiceControllerStatus.Running)
                        {
                            restarted = true;
                            service.Stop();
                            serviceController.WaitForStatus(ServiceControllerStatus.Stopped);
                            service.Start();
                        }
                        else
                        {
                            service.Start();
                        }

                        if (restarted)
                        {
                            return serviceName + " service has been started.";
                        }
                        else
                        {
                            return serviceName + " service has been re-started.";
                        }
                    }
                    else
                    {
                        return serviceName + " service.";
                    }
                }

            }
            catch (Exception ex)
            {
                return "Please re-start " + serviceName + " manually.";
            }
        }
        public string getSystemName()
        {
            object getSystemName = "";


            ManagementObjectSearcher searcher =
                    new ManagementObjectSearcher("root\\CIMV2",
                    "SELECT Name FROM Win32_ComputerSystem");
            foreach (ManagementObject queryObj in searcher.Get())
            {
                return queryObj["Name"].ToString();
            }
            return "";
        }

        private string GetAllTextLogos()
        {
            string allTextBoxes = string.Empty; int countTextLogo = 0;
            //allTextBoxes = "<TextLogos>";
            try
            {
                foreach (var ctrl in dragCanvas.Children)
                {
                    string textContent = string.Empty, textFontFamily = string.Empty,
                        textFontSize = string.Empty, textColor = string.Empty, textTopPosition = string.Empty,
                        textLeftPosition = string.Empty, textName = string.Empty,
                        textAngleRotation = string.Empty, textZIndex = string.Empty;
                    if (ctrl is TextBox)
                    {
                        countTextLogo++;
                        TextBox cid = (TextBox)ctrl;
                        textContent = Convert.ToString(cid.Text);
                        textFontFamily = Convert.ToString(cid.FontFamily);
                        textFontSize = Convert.ToString(cid.FontSize);
                        textColor = Convert.ToString(cid.Foreground);
                        textTopPosition = Convert.ToString(Canvas.GetTop(cid));
                        textLeftPosition = Convert.ToString(Canvas.GetLeft(cid));
                        textName = Convert.ToString(countTextLogo);
                        textZIndex = "25";//This needs to be made dynamic
                        TransformGroup tg = cid.GetValue(Canvas.RenderTransformProperty) as TransformGroup;
                        RotateTransform rotation = new RotateTransform();
                        ScaleTransform scale = new ScaleTransform();
                        if (tg != null)
                        {
                            if (tg.Children.Count > 0)
                            {
                                if (tg.Children[0] is ScaleTransform)
                                {
                                    scale = (ScaleTransform)tg.Children[0];
                                }
                                else if (tg.Children[0] is RotateTransform)
                                {
                                    rotation = (RotateTransform)tg.Children[0];
                                    textAngleRotation = rotation.Angle.ToString();
                                }
                            }
                            if (tg.Children.Count > 1)
                            {
                                if (tg.Children[1] is ScaleTransform)
                                {
                                    scale = (ScaleTransform)tg.Children[1];
                                }
                                else if (tg.Children[1] is RotateTransform)
                                {
                                    rotation = (RotateTransform)tg.Children[1];
                                    textAngleRotation = rotation.Angle.ToString();
                                }
                            }
                        }
                        textAngleRotation = !String.IsNullOrEmpty(textAngleRotation) ? textAngleRotation : "0";
                        allTextBoxes += "<TextLogo Name='" + textName + "' FontFamily='" + textFontFamily + "' FontColor='" + textColor + "' FontSize='" + textFontSize + "' Top='" + textTopPosition + "' Left='" + textLeftPosition + "' Angle='" + textAngleRotation + "' ZIndex='" + textZIndex + "' Content='" + textContent + "' />";
                    }
                }
            }
            catch
            { }
            //allTextBoxes += "</TextLogos>";
            return allTextBoxes;
        }

        string GetSelectedProducts()
        {
            try
            {
                string ProductIds = string.Empty;
                for (int i = 0; i < lstboxProductTypes.Items.Count; i++)
                {
                    lstboxProductTypes.ScrollIntoView(lstboxProductTypes.Items[i]);
                    lstboxProductTypes.UpdateLayout();  
                    DependencyObject obj = lstboxProductTypes.ItemContainerGenerator.ContainerFromIndex(i);
                    if (obj != null)
                    {
                        CheckBox rdo = FindVisualChild<CheckBox>(obj);// FindByName("rdoSelect", row) as RadioButton;
                        if (rdo != null && rdo.IsChecked == true)
                        {
                            ProductIds += "," + rdo.Tag.ToString();
                        }
                    }
                }
                if (ProductIds.Length > 0)
                    ProductIds = ProductIds.Remove(0, 1);
                return ProductIds;
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
                return string.Empty;
            }
        }

        private void btnCropEdit_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                mainImage.Source = null;
                mainImage_Size.Source = null;
                mainImagesize.Source = null;
                Uri ImagePathUri = new Uri(@"/DigiPhoto;component/images/125412626.jpg", UriKind.RelativeOrAbsolute);
                mainImage.Source = new BitmapImage(ImagePathUri);
                mainImage_Size.Source = new BitmapImage(ImagePathUri);
                mainImagesize.Source = new BitmapImage(ImagePathUri);
                OpenAssociateWindow();
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }
        /// <summary>
        /// Handles the Click event of the btnPreviewAutoCorrect control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void btnPreviewAutoCorrect_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                string productIds = GetSelectedProducts();
                string[] productID = productIds.Split(',');
                if (chkBorder.IsChecked == true && (cmbborder.SelectedIndex > 0 || cmbVerticalBorder.SelectedIndex > 0))
                {
                    for (int i = 0; i < frm.Children.Count; i++)
                    {
                        frm.Children.RemoveAt(i);
                    }
                    BitmapImage img;
                    if (mainImage.ActualWidth > mainImage.ActualHeight && cmbborder.SelectedIndex > 0)
                        img = new BitmapImage(new Uri(LoginUser.DigiFolderFramePath + "\\" + ((BorderInfo)(cmbborder.SelectionBoxItem)).DG_Border));
                    else if (mainImage.ActualWidth < mainImage.ActualHeight && cmbVerticalBorder.SelectedIndex > 0)
                        img = new BitmapImage(new Uri(LoginUser.DigiFolderFramePath + "\\" + ((BorderInfo)(cmbVerticalBorder.SelectionBoxItem)).DG_Border));
                    else
                    {
                        MessageBox.Show("Please select border in the dropdown");
                        return;
                    }

                    OpaqueClickableImage objCurrent = new OpaqueClickableImage();
                    objCurrent.Uid = "frame";
                    objCurrent.Source = img;
                    objCurrent.Stretch = Stretch.Fill;
                    frm.Children.Add(objCurrent);
                    //DragCanvas.SetCanBeDragged(frm, false);

                    if (mainImage.ActualWidth > mainImage.ActualHeight)
                    {
                        if (productID[0] == "1")
                        {
                            frm.Width = mainImage.ActualWidth;
                            frm.Height = frm.Width * 0.75;
                            canbackground.Height = canbackground.Width * 0.75;
                            dragCanvas.Height = frm.Height;
                            dragCanvas.Width = frm.Width;
                        }
                        else if (productID[0] == "2")
                        {
                            frm.Width = mainImage.ActualWidth;
                            frm.Height = frm.Width * 0.8;
                            canbackground.Height = canbackground.Width * 0.8;
                            dragCanvas.Height = frm.Height;
                            dragCanvas.Width = frm.Width;
                        }
                        else if (productID[0] == "30" || productID[0] == "104" || productID[0] == "5" || productID[0] == "102")
                        {
                            frm.Width = mainImage.ActualWidth;
                            frm.Height = frm.Width * 0.667;
                            canbackground.Height = canbackground.Width * 0.667;
                            dragCanvas.Height = frm.Height;
                            dragCanvas.Width = frm.Width;
                        }
                        else if (productID[0] == "103")
                        {
                            frm.Width = mainImage.ActualWidth;
                            frm.Height = frm.Width * 0.714;
                            canbackground.Height = canbackground.Width * 0.714;
                            dragCanvas.Height = frm.Height;
                            dragCanvas.Width = frm.Width;
                        }
                        else if (productID[0] == "3")
                        {
                            frm.Width = mainImage.ActualWidth;
                            frm.Height = frm.Width * 0.83;
                            canbackground.Height = canbackground.Width * 0.83;
                            dragCanvas.Height = frm.Height;
                            dragCanvas.Width = frm.Width;
                        }
                        else
                        {
                            frm.Height = mainImage.ActualHeight;
                            frm.Width = mainImage.ActualWidth;
                        }
                    }
                    else
                    {
                        if (productID[0] == "1")
                        {
                            frm.Height = mainImage.ActualHeight;
                            frm.Width = frm.Height * 0.75;
                            canbackground.Width = canbackground.Height * 0.75;
                            dragCanvas.Width = frm.Height * 0.75;
                            dragCanvas.Height = frm.Height;
                        }
                        else if (productID[0] == "2")
                        {
                            frm.Height = mainImage.ActualHeight;
                            frm.Width = frm.Height * 0.8;
                            canbackground.Width = canbackground.Height * 0.8;
                            dragCanvas.Width = frm.Height * 0.8;
                            dragCanvas.Height = frm.Height;
                        }
                        else if (productID[0] == "30" || productID[0] == "104" || productID[0] == "5" || productID[0] == "102")
                        {
                            frm.Height = mainImage.ActualHeight;
                            frm.Width = frm.Height * 0.667;
                            canbackground.Width = canbackground.Height * 0.667;
                            dragCanvas.Width = frm.Height * 0.667;
                            dragCanvas.Height = frm.Height;
                        }
                        else if (productID[0] == "103")
                        {
                            frm.Height = mainImage.ActualHeight;
                            frm.Width = frm.Height * 0.714;
                            canbackground.Width = canbackground.Height * 0.714;
                            dragCanvas.Width = frm.Height * 0.714;
                            dragCanvas.Height = frm.Height;
                        }
                        else if (productID[0] == "3")
                        {
                            frm.Height = mainImage.ActualHeight;
                            frm.Width = frm.Height * 0.83;
                            canbackground.Width = canbackground.Height * 0.83;
                            dragCanvas.Width = frm.Height * 0.83;
                            dragCanvas.Height = frm.Height;
                        }
                        else
                        {
                            frm.Height = mainImage.ActualHeight;
                            frm.Width = mainImage.ActualWidth;
                            dragCanvas.Width = frm.Width;
                            dragCanvas.Height = frm.Height;
                        }
                    }
                }
                if (chkbrightness.IsChecked == true)
                {
                    _brighteff.Brightness = Convert.ToDouble(txtbrightness.Text);
                    _brighteff.Contrast = 1;
                    GrdBrightness.Effect = _brighteff;
                }
                if (chkcontrast.IsChecked == true)
                {
                    _conteff.Contrast = Convert.ToDouble(txtcontrast.Text);
                    GrdContrast.Effect = _conteff;
                }
                //if (chklogo.IsChecked == true)
                //{

                //}
                if (chkBG.IsChecked == true)
                {
                    string fileName;
                    if (mainImage.ActualWidth > mainImage.ActualHeight && cmbHorizontalBG.SelectedIndex > 0)
                        fileName = ((System.Collections.Generic.KeyValuePair<string, string>)(cmbHorizontalBG.SelectedItem)).Key;
                    else if (mainImage.ActualWidth < mainImage.ActualHeight && cmbVerticalBG.SelectedIndex > 0)
                        fileName = ((System.Collections.Generic.KeyValuePair<string, string>)(cmbVerticalBG.SelectedItem)).Key;
                    else
                    {
                        MessageBox.Show("Please select background in the dropdown");
                        return;
                    }

                    string BGfolderPathspecproductType = string.Empty;
                    switch (Convert.ToString(productID[0]))
                    {
                        case "120":
                            {
                                BGfolderPathspecproductType = "8X24";
                                if (mainImage.ActualWidth > mainImage.ActualHeight)
                                {
                                    canbackground.Width = 609;
                                    canbackground.Height = 203;
                                }
                                else
                                {

                                }
                                break;
                            }
                        case "121":
                            {
                                BGfolderPathspecproductType = "8X26";
                                if (mainImage.ActualWidth > mainImage.ActualHeight)
                                {
                                    canbackground.Width = 600;
                                    canbackground.Height = 203;
                                }
                                else
                                {

                                }
                                break;
                            }
                        case "122":
                            {
                                BGfolderPathspecproductType = "8X18";
                                if (mainImage.ActualWidth > mainImage.ActualHeight)
                                {
                                    canbackground.Width = 457;
                                    canbackground.Height = 203;
                                }
                                else
                                {

                                }
                                break;
                            }
                        case "123":
                            {
                                BGfolderPathspecproductType = "6X14";
                                if (mainImage.ActualWidth > mainImage.ActualHeight)
                                {
                                    canbackground.Width = 355;
                                    canbackground.Height = 152;
                                }
                                break;
                            }
                        case "125":
                            {
                                BGfolderPathspecproductType = "6X20";
                                if (mainImage.ActualWidth > mainImage.ActualHeight)
                                {
                                    canbackground.Width = 508;
                                    canbackground.Height = 152;
                                }
                                else
                                {

                                }
                                break;
                            }
                        default:
                            BGfolderPathspecproductType = "8X10";
                            break;

                    }
                    Uri uri = new Uri(LoginUser.DigiFolderBackGroundPath + BGfolderPathspecproductType + "\\" + fileName);
                    BitmapImage objCurrent = new BitmapImage(uri);
                    canbackground.Background = new ImageBrush { ImageSource = objCurrent };
                    if (mainImage.ActualWidth > mainImage.ActualHeight)
                    {
                        if (productID[0] == "1")
                        {
                            canbackground.Height = canbackground.Width * 0.75;
                        }
                        else if (productID[0] == "2")
                        {
                            canbackground.Height = canbackground.Width * 0.8;
                        }
                        else if (productID[0] == "30" || productID[0] == "104" || productID[0] == "5" || productID[0] == "102")
                        {
                            canbackground.Height = canbackground.Width * 0.667;
                        }
                        else if (productID[0] == "103")
                        {
                            canbackground.Height = canbackground.Width * 0.714;
                        }
                        else if (productID[0] == "3")
                        {
                            canbackground.Height = canbackground.Width * 0.83;
                        }
                    }
                    else
                    {
                        if (productID[0] == "1")
                        {
                            canbackground.Width = canbackground.Height * 0.75;
                        }
                        else if (productID[0] == "2")
                        {
                            canbackground.Width = canbackground.Height * 0.8;
                        }
                        else if (productID[0] == "30" || productID[0] == "104" || productID[0] == "5" || productID[0] == "102")
                        {
                            canbackground.Width = canbackground.Height * 0.667;
                        }
                        else if (productID[0] == "103")
                        {
                            canbackground.Width = canbackground.Height * 0.714;
                        }
                        else if (productID[0] == "3")
                        {
                            canbackground.Width = canbackground.Height * 0.83;
                        }
                    }
                }

                // if()//////////added by latika for removing duplicate entries  
                if (cmbImageOrientation.Text == "Horizontal")
                {
                    setCanves(Graphics_HorizontalXml);
                }
                else
                {
                    setCanves(Graphics_VerticalXml);
                }
                /////////////////////end/////added by latika for removing duplicate entries  
                DragCanvas.SetCanBeDragged(frm, false);
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }
        /// <summary>
        /// Handles the Checked event of the chkBorder control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void chkBorder_Checked(object sender, RoutedEventArgs e)
        {
            try
            {
                switch (((CheckBox)sender).Name)
                {
                    case "chkBorder":
                        {
                            cmbborder.IsEnabled = true;
                            cmbborder.Focus();
                            cmbVerticalBorder.IsEnabled = true;
                            break;
                        }
                    case "chkbrightness":
                        {
                            txtbrightness.Text = "0";
                            txtbrightness.IsEnabled = true;
                            txtbrightness.Focus();
                            break;
                        }
                    case "chkcontrast":
                        {
                            txtcontrast.IsEnabled = true;
                            txtcontrast.Focus();
                            break;
                        }
                    case "chkBG":
                        {
                            if (cmbImageOrientation.SelectedIndex == 0)
                            {
                                cmbHorizontalBG.IsEnabled = true;
                                cmbVerticalBG.IsEnabled = false;
                                cmbHorizontalBG.Focus();
                            }
                            else
                            {
                                cmbHorizontalBG.IsEnabled = false;
                                cmbVerticalBG.IsEnabled = true;
                                cmbVerticalBG.Focus();
                            }
                            if (!String.IsNullOrWhiteSpace(SpecFileName))
                                btnChroma.IsEnabled = true;
                            btndelete.IsEnabled = false;
                            btngraphics1.IsEnabled = false;
                            btnPreview.IsEnabled = false;

                            break;
                        }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }
        /// <summary>
        /// Handles the Unchecked event of the chkBorder control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void chkBorder_Unchecked(object sender, RoutedEventArgs e)
        {
            try
            {
                switch (((CheckBox)sender).Name)
                {
                    case "chkBorder":
                        {
                            cmbborder.SelectedValue = "0";
                            cmbVerticalBorder.SelectedValue = "0";
                            List<UIElement> ListToBeRemoved = new List<UIElement>();
                            foreach (UIElement ui in frm.Children)
                            {
                                ListToBeRemoved.Add(ui);
                            }
                            foreach (UIElement ui in ListToBeRemoved)
                            {
                                frm.Children.Remove(ui);
                            }
                            cmbborder.IsEnabled = false;
                            cmbVerticalBorder.IsEnabled = false;
                            break;
                        }
                    case "chkbrightness":
                        {
                            txtbrightness.Text = "0";
                            txtbrightness.IsEnabled = false;
                            _brighteff.Brightness = Convert.ToDouble(txtbrightness.Text);
                            _brighteff.Contrast = 1;
                            GrdBrightness.Effect = _brighteff;
                            break;
                        }
                    case "chkcontrast":
                        {
                            txtcontrast.Text = "1";
                            _conteff.Contrast = Convert.ToDouble(txtcontrast.Text);
                            GrdContrast.Effect = _conteff;
                            txtcontrast.IsEnabled = false;
                            break;
                        }
                    case "chkfilter":
                        {
                            Grdcolorfilter.Effect = null;
                            break;
                        }

                    case "chkBG":
                        {
                            cmbHorizontalBG.IsEnabled = false;
                            cmbVerticalBG.IsEnabled = false;
                            btnChroma.IsEnabled = false;
                            btndelete.IsEnabled = true;
                            btngraphics1.IsEnabled = true;
                            btnPreview.IsEnabled = true;
                            cmbHorizontalBG.SelectedValue = "0";
                            cmbVerticalBG.SelectedValue = "0";
                            canbackground.Background = null;
                            Grdcartoonize.Effect = null;

                            if (!String.IsNullOrWhiteSpace(SpecFileName))
                            {
                                mainImage.Source = null;
                                mainImage_Size.Source = null;
                                mainImagesize.Source = null;
                                string path = SpecFileName.Replace("png", "jpg").Replace("PNG", "JPG");
                                Uri uri1 = new Uri(path);
                                mainImage.Source = new BitmapImage(uri1);
                                mainImage_Size.Source = new BitmapImage(uri1);
                                mainImagesize.Source = new BitmapImage(uri1);
                            }
                            canbackground.UpdateLayout();
                            break;
                        }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }
        private void cmbBG_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                if (cmbImageOrientation.SelectedIndex == 0 && cmbHorizontalBG.SelectedItem != null && cmbHorizontalBG.SelectedIndex > 0)
                {
                    string bgvalue = ((System.Collections.Generic.KeyValuePair<string, string>)(cmbHorizontalBG.SelectedItem)).Key;
                    string backgroundThumbnailPath = System.IO.Path.Combine(LoginUser.DigiFolderBackGroundPath, "Thumbnails", bgvalue);
                    string backgroundPanoramaThumbnailPath = System.IO.Path.Combine(LoginUser.DigiFolderBackGroundPath, "Thumbnails\\Panorama", bgvalue);


                    if (File.Exists(backgroundThumbnailPath))
                    {
                        imgBackImage.Source = new BitmapImage(new Uri(backgroundThumbnailPath, UriKind.Absolute));
                    }
                    else if (File.Exists(backgroundPanoramaThumbnailPath))
                    {
                        imgBackImage.Source = new BitmapImage(new Uri(backgroundPanoramaThumbnailPath, UriKind.Absolute));
                    }

                }

                else if (cmbImageOrientation.SelectedIndex == 1 && cmbVerticalBG.SelectedItem != null && cmbVerticalBG.SelectedIndex > 0)
                {
                    string bgvalue = ((System.Collections.Generic.KeyValuePair<string, string>)(cmbVerticalBG.SelectedItem)).Key;
                    string backgroundThumbnailPath = System.IO.Path.Combine(LoginUser.DigiFolderBackGroundPath, "Thumbnails", bgvalue);
                    string backgroundPanoramaThumbnailPath = System.IO.Path.Combine(LoginUser.DigiFolderBackGroundPath, "Thumbnails\\Panorama", bgvalue);

                    if (File.Exists(backgroundThumbnailPath))
                    {
                        imgBackImage.Source = new BitmapImage(new Uri(backgroundThumbnailPath, UriKind.Absolute));
                    }
                    else if (File.Exists(backgroundPanoramaThumbnailPath))
                    {
                        imgBackImage.Source = new BitmapImage(new Uri(backgroundPanoramaThumbnailPath, UriKind.Absolute));
                    }

                }
                else
                {
                    imgBackImage.Source = null;
                    
                }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }

        }
        private void btnChroma_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (chkBG.IsChecked == true)
                {
                    if ((cmbImageOrientation.SelectedIndex == 0 && cmbHorizontalBG.SelectedIndex > 0) || (cmbImageOrientation.SelectedIndex == 1 && cmbVerticalBG.SelectedIndex > 0))
                    {
                        string path = SpecFileName.Replace("jpg", "png").Replace("JPG", "PNG");

                        switch (cmbChromaClr.SelectedValue.ToString())
                        {
                            case "Green":
                                ChromaKeyHSVEffect _greenscreendefault3 = new ChromaKeyHSVEffect();
                                _greenscreendefault3.HueMin = 0.2;
                                _greenscreendefault3.HueMax = 0.5;
                                _greenscreendefault3.LightnessShift = lightness;
                                _greenscreendefault3.SaturationShift = saturation;
                                Grdcartoonize.Effect = _greenscreendefault3;
                                break;

                            case "Blue":
                            case "Red":
                            case "Gray":
                                ColorKeyAlphaEffect ckae = new ColorKeyAlphaEffect();
                                ckae.ColorKey = (Color)ColorConverter.ConvertFromString(txtColorCode.Text);
                                ckae.Tolerance = Convert.ToDouble(txtChromaTol.Text);
                                Grdcartoonize.Effect = ckae;
                                //ChromaEffectAllColor _colorscreendefault = new ChromaEffectAllColor();
                                //_colorscreendefault.ColorKey = (Color)ColorConverter.ConvertFromString(txtColorCode.Text);
                                //_colorscreendefault.Tolerance = float.Parse(txtChromaTol.Text);
                                //Grdcartoonize.Effect = _colorscreendefault;
                                break;

                            default:
                                break;
                        }
                        Grdcartoonize.UpdateLayout();
                        RenderTargetBitmap _objeditedImage = jCaptureScreen(Grdcartoonize);
                        using (var fileStream = new FileStream(path, FileMode.Create, FileAccess.ReadWrite))
                        {
                            PngBitmapEncoder encoder = new PngBitmapEncoder();
                            encoder.Interlace = PngInterlaceOption.On;
                            encoder.Frames.Add(BitmapFrame.Create(_objeditedImage));
                            encoder.Save(fileStream);
                        }
                        mainImage.Source = null;
                        mainImage_Size.Source = null;
                        mainImagesize.Source = null;
                        canbackground.UpdateLayout();
                        Uri uri1 = new Uri(path);

                        mainImage.Source = new BitmapImage(uri1);
                        mainImage_Size.Source = new BitmapImage(uri1);
                        mainImagesize.Source = new BitmapImage(uri1);

                        btnChroma.IsEnabled = false;
                        btndelete.IsEnabled = true;
                        btngraphics1.IsEnabled = true;
                        btnPreview.IsEnabled = true;
                    }
                    else
                    {
                        MessageBox.Show("Please select Background");
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }
        private void cmbChromaClr_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (cmbChromaClr.SelectedValue.ToString() == "Green")
            {
                txtColorCode.Text = "#00FF00";
                txtChromaTol.Text = "1";
            }
            else if (cmbChromaClr.SelectedValue.ToString() == "Blue")
            {
                txtColorCode.Text = "#0080FF";
                txtChromaTol.Text = "0.5";
            }
            else if (cmbChromaClr.SelectedValue.ToString() == "Red")
            {
                txtColorCode.Text = "#FF0000";
                txtChromaTol.Text = "0.4";
            }
            else if (cmbChromaClr.SelectedValue.ToString() == "Gray")
            {
                txtColorCode.Text = "#A9A9A9";
                txtChromaTol.Text = "0.15";
            }
        }
        /// <summary>
        /// Handles the Click event of the btnBack control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void btnBack_Click(object sender, RoutedEventArgs e)
        {
            lstTextCollection.Clear();
            RemoveTextFields();
            backAndClose();
        }

        private void backAndClose()
        {
            ((Configuration)_parentConfig).loadSpecGrid(LocationId);
            this.Visibility = Visibility.Collapsed;
            ((Configuration)_parentConfig).btn.Opacity = 1;
            ((Configuration)_parentConfig).btn.IsEnabled = true;
        }
        /// <summary>
        /// Handles the TextChanged event of the txtAngle control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="TextChangedEventArgs"/> instance containing the event data.</param>
        private void txtAngle_TextChanged(object sender, TextChangedEventArgs e)
        {
            int oldNum = _numValue;

            if (!int.TryParse(txtAngle.Text, out _numValue))
                txtAngle.Text = _numValue.ToString();

            if (_numValue > 360 || _numValue < 0)
            {
                txtAngle.Text = oldNum.ToString();
                _numValue = oldNum;
            }

            if (!string.IsNullOrEmpty(txtAngle.Text))
            {
                jangleAltitudeSelector1_AngleChanged();
            }
        }

        /// <summary>
        /// Handles the GotFocus event of the btngrph control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        void btngrph_GotFocus(object sender, RoutedEventArgs e)
        {
            this.elementForContextMenu = (Button)sender;
        }

        /// <summary>
        /// Handles the 1 event of the Button_Click control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            Button btngrph = new Button();
            Button btn = (Button)(sender);
            btngrph.HorizontalAlignment = System.Windows.HorizontalAlignment.Center;
            btngrph.VerticalAlignment = System.Windows.VerticalAlignment.Center;
            Style defaultStyle = (Style)FindResource("ButtonStyleGraphic");
            btngrph.Style = defaultStyle;
            System.Windows.Controls.Image imgctrl = new System.Windows.Controls.Image();
            BitmapImage img = new BitmapImage(new Uri(btn.Tag.ToString()));
            imgctrl.Name = "A" + Guid.NewGuid().ToString().Split('-')[0].ToString();
            btngrph.Name = "btn" + Guid.NewGuid().ToString().Split('-')[0].ToString();
            btngrph.Uid = "uid" + Guid.NewGuid().ToString().Split('-')[0].ToString();
            imgctrl.Source = img;
            btngrph.Tag = "1";

            double wdt = 0, hgt = 0;

            if (wdt != 0)
            {
                if (wdt < 1)
                {
                    btngrph.Width = 90 / wdt;
                    btngrph.Height = 90 / hgt;
                }
                else
                {
                    btngrph.Width = 90 * wdt;
                    btngrph.Height = 90 * hgt;
                }
            }
            else
            {
                btngrph.Width = 90;
                btngrph.Height = 90;
            }


            btngrph.Content = imgctrl;
            dragCanvas.Children.Add(btngrph);
            Canvas.SetLeft(btngrph, GrdBrightness.ActualWidth / 2);
            Canvas.SetTop(btngrph, GrdBrightness.ActualHeight / 2);
            imgctrl.MouseLeftButtonUp += new MouseButtonEventHandler(SelectObject);
            btngrph.MouseLeftButtonUp += new MouseButtonEventHandler(SelectObject);
            btngrph.KeyDown += new KeyEventHandler(MyInkCanvas_KeyDown);
            btngrph.GotFocus += new RoutedEventHandler(btngrph_GotFocus);
            btngrph.Focus();
            Canvas.SetZIndex(btngrph, 4);
        }
        /// <summary>
        /// Selects the object.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="MouseButtonEventArgs"/> instance containing the event data.</param>
        private void SelectObject(object sender, MouseButtonEventArgs e)
        {
            try
            {
                if (e.LeftButton == MouseButtonState.Released)
                {

                    if (sender is Button)
                    {
                        this.elementForContextMenu = (Button)sender;
                    }
                    else if (sender is TextBox)
                    {
                        TextBox tb = (TextBox)sender;
                        double top = Convert.ToDouble(tb.GetValue(Canvas.TopProperty));
                        double left = Convert.ToDouble(tb.GetValue(Canvas.LeftProperty));

                        tb.BorderBrush = System.Windows.Media.Brushes.OrangeRed;
                        this.elementForContextMenu = tb;
                        txtContent.Text = tb.Text;
                        txtContent.Focus();
                        tb.Focus();
                        //IsSelectedMainImage = false;

                        //RotateTransform rotation = tb.GetValue(Canvas.RenderTransformProperty) as RotateTransform;
                        //if (rotation != null)
                        //{
                        //    jrotate.Angle = rotation.Angle;
                        //}
                        //else
                        //{
                        //    jrotate.Angle = 0;
                        //}
                        cmbFont.SelectedItem = (System.Windows.Media.FontFamily)tb.FontFamily;
                        CmbFontSize.Text = tb.FontSize.ToString();
                        CmbColor.Text = ((System.Windows.Media.SolidColorBrush)tb.Foreground).ToString();
                    }
                    else if (sender is System.Windows.Controls.Image)
                    {
                        object objParent = (System.Windows.Controls.Image)sender;
                        for (int i = 0; i < 3; i++)
                        {
                            objParent = VisualTreeHelper.GetParent(objParent as DependencyObject);
                            if (objParent is Button)
                            {
                                TransformGroup tg = null;
                                if (this.elementForContextMenu != null)
                                {
                                    tg = this.elementForContextMenu.GetValue(Canvas.RenderTransformProperty) as TransformGroup;
                                }
                                RotateTransform rotation = new RotateTransform();
                                ScaleTransform scaling = new ScaleTransform();

                                if (tg != null)
                                {
                                    if (tg.Children != null && tg.Children.Count > 0)
                                    {
                                        if (tg.Children[0] is RotateTransform)
                                        {
                                            rotation = (RotateTransform)tg.Children[0];
                                        }
                                        else if (tg.Children[0] is ScaleTransform)
                                            scaling = (ScaleTransform)tg.Children[0];
                                    }
                                    if (tg.Children != null && tg.Children.Count > 1)
                                    {
                                        if (tg.Children[1] is RotateTransform)
                                        {
                                            rotation = (RotateTransform)tg.Children[1];
                                        }
                                        else if (tg.Children[0] is ScaleTransform)
                                            scaling = (ScaleTransform)tg.Children[0];
                                    }
                                }
                            }
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }

        /// <summary>
        /// Handles the KeyDown event of the MyInkCanvas control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="KeyEventArgs"/> instance containing the event data.</param>
        private void MyInkCanvas_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Delete)
            {
                DeleteGraphics();
            }
        }

        ///<summary>
        ///Jangles the altitude selector1_ angle changed.
        ///</summary>
        private void jangleAltitudeSelector1_AngleChanged()
        {
            if (this.elementForContextMenu != null)
            {
                if ((this.elementForContextMenu is Button))
                {
                    Button btn = (Button)this.elementForContextMenu;

                    TransformGroup tgnew = new TransformGroup();
                    TransformGroup tg = this.elementForContextMenu.GetValue(Canvas.RenderTransformProperty) as TransformGroup;

                    if (tg != null)
                    {
                        if (tg.Children.Count > 0)
                        {
                            if (tg.Children[0] is ScaleTransform)
                            {
                                tgnew.Children.Add(tg.Children[0]);
                            }
                            if (tg.Children.Count > 1)
                            {
                                if (tg.Children[1] is ScaleTransform)
                                {
                                    tgnew.Children.Add(tg.Children[1]);
                                }
                            }
                        }
                    }

                    RotateTransform rotation = new RotateTransform();
                    btn.RenderTransformOrigin = new System.Windows.Point(0.5, 0.5);
                    rotation.CenterX = 0;
                    rotation.CenterY = 0;
                    rotation.Angle = Convert.ToDouble(txtAngle.Text);
                    tgnew.Children.Add(rotation);
                    btn.RenderTransform = tgnew;
                }
                else if ((this.elementForContextMenu is TextBox))
                {
                    TextBox txtLogo = (TextBox)this.elementForContextMenu;

                    TransformGroup tgnew = new TransformGroup();
                    TransformGroup tg = this.elementForContextMenu.GetValue(Canvas.RenderTransformProperty) as TransformGroup;
                    RotateTransform rotation = new RotateTransform();
                    txtLogo.RenderTransformOrigin = new System.Windows.Point(0.5, 0.5);
                    rotation.CenterX = 0;
                    rotation.CenterY = 0;
                    rotation.Angle = Convert.ToDouble(txtAngle.Text);
                    tgnew.Children.Add(rotation);
                    txtLogo.RenderTransform = tgnew;
                }
            }
            else
            {
                RotateTransform rtm = new RotateTransform();
                rtm.CenterX = 0;
                rtm.CenterY = 0;
            }
        }
        /// <summary>
        /// Handles the Click event of the cmdUp control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void cmdUp_Click(object sender, RoutedEventArgs e)
        {
            if (NumValue >= 360)
            {
                return;
            }
            NumValue++;
        }
        /// <summary>
        /// Handles the Click event of the cmdDown control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void cmdDown_Click(object sender, RoutedEventArgs e)
        {
            if (NumValue <= 0)
            {
                return;
            }
            NumValue--;
        }
        /// <summary>
        /// Handles the Click event of the ZoomInButton control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void ZoomInButton_Click(object sender, RoutedEventArgs e)
        {
            RenderOptions.SetEdgeMode(mainImage, EdgeMode.Aliased);
            try
            {
                if (this.elementForContextMenu is Button)
                {

                    Button b = (Button)this.elementForContextMenu;
                    if (b.Tag != null)
                    {
                        _GraphicsZoomFactor = Convert.ToDouble(b.Tag);
                        _GraphicsZoomFactor += .025;
                    }
                    else
                    {
                        _GraphicsZoomFactor = 1;
                    }
                    TransformGroup grp = new TransformGroup();
                    TransformGroup tg = this.elementForContextMenu.GetValue(Canvas.RenderTransformProperty) as TransformGroup;
                    RotateTransform rotation = new RotateTransform();
                    b.RenderTransformOrigin = new System.Windows.Point(0.5, 0.5);
                    rotation.CenterX = 0;
                    rotation.CenterY = 0;
                    ScaleTransform scale = new ScaleTransform();

                    if (tg != null)
                    {
                        if (tg.Children.Count > 0)
                        {
                            if (tg.Children[0] is ScaleTransform)
                            {
                                scale = (ScaleTransform)tg.Children[0];
                            }
                            else if (tg.Children[0] is RotateTransform)
                            {
                                rotation = (RotateTransform)tg.Children[0];

                            }
                        }
                        if (tg.Children.Count > 1)
                        {
                            if (tg.Children[1] is ScaleTransform)
                            {
                                scale = (ScaleTransform)tg.Children[1];
                            }
                            else if (tg.Children[1] is RotateTransform)
                            {
                                rotation = (RotateTransform)tg.Children[1];

                            }
                        }
                    }


                    if (scale == null)
                    {
                        scale = new ScaleTransform();
                        scale.ScaleX = _GraphicsZoomFactor;
                        scale.ScaleY = _GraphicsZoomFactor;
                        scale.CenterX = 0;
                        scale.CenterY = 0;
                    }
                    else
                    {
                        scale.ScaleX = _GraphicsZoomFactor;
                        scale.ScaleY = _GraphicsZoomFactor;
                        scale.CenterX = 0;
                        scale.CenterY = 0;
                    }

                    grp.Children.Add(scale);
                    if (rotation != null)
                    {
                        grp.Children.Add(rotation);


                    }
                    b.RenderTransformOrigin = new System.Windows.Point(0.5, 0.5);
                    b.Tag = _GraphicsZoomFactor.ToString();
                    b.RenderTransform = grp;
                    this.elementForContextMenu = b;
                }

                else if (this.elementForContextMenu == null)
                {
                    if (_ZoomFactor >= 4)
                    {
                        _ZoomFactor = 4;

                        return;
                    }

                    _ZoomFactor += .025;


                    if (zoomTransform != null)
                    {

                        zoomTransform.CenterX = mainImage.ActualWidth / 2;
                        zoomTransform.CenterY = mainImage.ActualHeight / 2;

                        zoomTransform.ScaleX = _ZoomFactor;
                        zoomTransform.ScaleY = _ZoomFactor;

                        transformGroup = new TransformGroup();
                        transformGroup.Children.Add(zoomTransform);

                        GrdBrightness.RenderTransform = transformGroup;
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                MessageBox.Show(ex.Message);
            }
            finally
            {
                MemoryManagement.FlushMemory();
            }
        }
        /// <summary>
        /// Handles the Click event of the ZoomOutButton control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void ZoomOutButton_Click(object sender, RoutedEventArgs e)
        {
            RenderOptions.SetEdgeMode(mainImage, EdgeMode.Aliased);
            try
            {
                if (this.elementForContextMenu is Button)
                {

                    Button b = (Button)this.elementForContextMenu;
                    if (b.Tag != null)
                    {
                        _GraphicsZoomFactor = Convert.ToDouble(b.Tag);
                    }

                    if (_GraphicsZoomFactor >= .625)
                        _GraphicsZoomFactor -= .025;
                    else
                    {
                        return;
                    }

                    TransformGroup grp = new TransformGroup();
                    TransformGroup tg = this.elementForContextMenu.GetValue(Canvas.RenderTransformProperty) as TransformGroup;
                    RotateTransform rotation = new RotateTransform();
                    ScaleTransform scale = new ScaleTransform();

                    if (tg != null)
                    {
                        if (tg.Children.Count > 0)
                        {
                            if (tg.Children[0] is ScaleTransform)
                            {
                                scale = (ScaleTransform)tg.Children[0];
                            }
                            else if (tg.Children[0] is RotateTransform)
                            {
                                rotation = (RotateTransform)tg.Children[0];
                            }
                        }
                        if (tg.Children.Count > 1)
                        {
                            if (tg.Children[1] is ScaleTransform)
                            {
                                scale = (ScaleTransform)tg.Children[1];
                            }
                            else if (tg.Children[1] is RotateTransform)
                            {
                                rotation = (RotateTransform)tg.Children[1];
                            }
                        }
                    }

                    if (scale == null)
                    {
                        return;
                    }
                    else
                    {
                        scale.ScaleX = scale.ScaleX - 0.025;
                        scale.ScaleY = scale.ScaleY - 0.025;
                        scale.CenterX = 0;
                        scale.CenterY = 0;

                        grp.Children.Add(scale);
                        if (rotation != null)
                        {
                            grp.Children.Add(rotation);
                        }

                        b.RenderTransformOrigin = new System.Windows.Point(0.5, 0.5);
                        b.Tag = _GraphicsZoomFactor;
                        b.RenderTransform = grp;
                        this.elementForContextMenu = b;
                    }
                }
                else if (this.elementForContextMenu == null)
                {
                    if (_ZoomFactor >= .525)
                        _ZoomFactor -= .025;
                    else
                    {
                        _ZoomFactor = .5;
                        return;
                    }

                    if (zoomTransform != null)
                    {
                        zoomTransform.CenterX = mainImage.ActualWidth / 2;
                        zoomTransform.CenterY = mainImage.ActualHeight / 2;

                        zoomTransform.ScaleX = _ZoomFactor;
                        zoomTransform.ScaleY = _ZoomFactor;

                        transformGroup = new TransformGroup();
                        transformGroup.Children.Add(zoomTransform);

                        GrdBrightness.RenderTransform = transformGroup;
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }

        /// <summary>
        /// Handles the Loaded event of the objCurrent control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        void objCurrent_Loaded(object sender, RoutedEventArgs e)
        {
            Canvas.SetLeft(frm, (dragCanvas.ActualWidth - frm.ActualWidth) / 2);
            Canvas.SetTop(frm, (dragCanvas.ActualHeight - frm.ActualHeight) / 2);
        }
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            GrdPrint.Visibility = Visibility.Collapsed;
            if (dragCanvas.Children.Count > 2)
            {
                spZoompanel.Visibility = Visibility.Visible;
            }
        }
        /// <summary>
        /// Handles the Click event of the btnGraphics control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void btnGraphics_Click(object sender, RoutedEventArgs e)
        {
            GrdPrint.Visibility = Visibility.Visible;
        }
        /// <summary>
        /// Handles the Click event of the btnFileOpen control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void btnFileOpen_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Microsoft.Win32.OpenFileDialog fileDialog = new Microsoft.Win32.OpenFileDialog();
                fileDialog.Multiselect = false;
                fileDialog.Filter = "All Image Files | *.*";
                //fileDialog.InitialDirectory = @"C:";
                if ((bool)fileDialog.ShowDialog())
                {
                    ResetSpecPrintEffects();
                    string fileName = fileDialog.SafeFileName;
                    SpecFileName = fileDialog.FileName;
                    if (chkBG.IsChecked == true && File.Exists(fileDialog.FileName.Replace("jpg", "png")))
                        SpecFileName = SpecFileName.Replace("jpg", "png");
                    Uri uri = new Uri(SpecFileName);
                    mainImage.Source = new BitmapImage(uri);
                    mainImage_Size.Source = new BitmapImage(uri);
                    mainImagesize.Source = new BitmapImage(uri);
                    if (cmbImageOrientation.SelectedIndex == 0 && mainImage.Source.Height > mainImage.Source.Width)
                    {
                        btnPreview.IsEnabled = false;
                        mainImage.Source = null;
                        mainImage_Size.Source = null;
                        mainImagesize.Source = null;
                        Uri ImagePathUri = new Uri(@"/DigiPhoto;component/images/125412626.jpg", UriKind.RelativeOrAbsolute);
                        mainImage.Source = new BitmapImage(ImagePathUri);
                        mainImage_Size.Source = new BitmapImage(ImagePathUri);
                        mainImagesize.Source = new BitmapImage(ImagePathUri);
                        MessageBox.Show("Please select Horizontal/Landscape Image for preview");
                        return;
                    }
                    else if (cmbImageOrientation.SelectedIndex == 1 && mainImage.Source.Height < mainImage.Source.Width)
                    {
                        btnPreview.IsEnabled = false;
                        mainImage.Source = null;
                        mainImage_Size.Source = null;
                        mainImagesize.Source = null;
                        Uri ImagePathUri = new Uri(@"/DigiPhoto;component/images/125412627.jpg", UriKind.RelativeOrAbsolute);
                        mainImage.Source = new BitmapImage(ImagePathUri);
                        mainImage_Size.Source = new BitmapImage(ImagePathUri);
                        mainImagesize.Source = new BitmapImage(ImagePathUri);
                        MessageBox.Show("Please select Vertical/Portrait Image for preview");
                        return;
                    }
                    else
                    {
                        btnPreview.IsEnabled = true;
                    }
                    if (mainImage.Source.Height > mainImage.Source.Width)
                    {
                        string productIds = GetSelectedProducts();
                        string[] productID = productIds.Split(',');
                        if (productID[0] == "1")
                        {
                            canbackground.Height = 535;
                            canbackground.Width = 401.25;
                            canbackground.InvalidateArrange();
                            canbackground.InvalidateMeasure();
                            canbackground.UpdateLayout();
                        }
                        else if (productID[0] == "2")
                        {
                            canbackground.Height = 535;
                            canbackground.Width = 428;
                            canbackground.InvalidateArrange();
                            canbackground.InvalidateMeasure();
                            canbackground.UpdateLayout();
                        }
                        else if (productID[0] == "30")
                        {
                            canbackground.Height = 535;
                            canbackground.Width = 356.84;
                            canbackground.InvalidateArrange();
                            canbackground.InvalidateMeasure();
                            canbackground.UpdateLayout();
                        }
                        else
                        {
                            canbackground.Height = 535;
                            canbackground.Width = 356.84;
                            canbackground.InvalidateArrange();
                            canbackground.InvalidateMeasure();
                            canbackground.UpdateLayout();
                        }
                        dragCanvas.Effect = null;
                        dragCanvas.RenderTransform = null;
                        Canvas.SetLeft(dragCanvas, 0);
                        Canvas.SetTop(dragCanvas, 0);
                        dragCanvas.UpdateLayout();
                    }
                    //if (chkBG.IsChecked == true && !SpecFileName.Contains(".png"))
                    if (chkBG.IsChecked == true)
                    {
                        btnChroma.IsEnabled = true;
                        btngraphics1.IsEnabled = true;
                    }
                    else
                    {
                        btnChroma.IsEnabled = false;
                        btndelete.IsEnabled = true;
                        btngraphics1.IsEnabled = true;
                        btnPreview.IsEnabled = true;
                    }         
                }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }
        /// <summary>
        /// Handles the 1 event of the btndelete_Click control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void btndelete_Click_1(object sender, RoutedEventArgs e)
        {
            DeleteGraphics();
        }
        private void RepeatButton_MouseWheel(object sender, MouseWheelEventArgs e)
        {
            if (e.Delta > 0)
            {
                ZoomInButton_Click(sender, new RoutedEventArgs());
            }
            else
            {

                ZoomOutButton_Click(sender, new RoutedEventArgs());
            }
        }
        private void GrdBrightn_MouseWheel(object sender, MouseWheelEventArgs e)
        {
            if (Grdcartoonize.IsMouseOver == true)
            {
                if (e.Delta > 0)
                {
                    ZoomInButton_Click(sender, new RoutedEventArgs());
                }
                else
                {

                    ZoomOutButton_Click(sender, new RoutedEventArgs());
                }
            }
        }
        private void txtChromaTol_KeyUp(object sender, KeyEventArgs e)
        {
            {
                if (char.IsLetter((char)KeyInterop.VirtualKeyFromKey(e.Key)) ||
                    char.IsSymbol((char)KeyInterop.VirtualKeyFromKey(e.Key)) ||
                    char.IsWhiteSpace((char)KeyInterop.VirtualKeyFromKey(e.Key)) ||
                    char.IsPunctuation((char)KeyInterop.VirtualKeyFromKey(e.Key)))
                    e.Handled = true;
            }

            {
                //allows only numbers between 1 and 2
                string value = txtChromaTol.Text;

                if (txtChromaTol.Text != "")
                {
                    if (value[0] == '.')
                    {
                        value = "0.";
                        txtChromaTol.Text = "0.";
                    }
                    if (Decimal.Parse(value) < 0)
                    {
                        MessageBox.Show("This Value should be a decimal number between 0 and 2");
                        txtChromaTol.Text = "";
                    }
                    else if (Decimal.Parse(value) > 2)
                    {
                        MessageBox.Show("This Value should be a decimal number between 0 and 2");
                        txtChromaTol.Text = "";
                    }
                }
            }
        }

        private void cmbImageOrientation_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            SetOrientatin();
        }

        public void SetOrientatin()
        {
            string productIds = GetSelectedProducts();
            string[] productID = productIds.Split(',');
            string[] PanoramicSizes = (new CommonBusiness()).GetPanoramicSizesCode();
            var strFound = Array.FindAll(PanoramicSizes, str => str.ToLower().Contains(Convert.ToString(productID[0])));

            //if(cmbImageOrientation.SelectedIndex == 0)
            //{
            //    IsHorizontal = true;
            // }
            //else
            //{
            //    IsHorizontal = false;
            //}

            //if(IsHorizontal)
            //{
            //    cmbImageOrientation.SelectedIndex = 0;
            //}
            //else if(IsVertical)
            //{
            //    cmbImageOrientation.SelectedIndex = 1;
            //}
            if (cmbImageOrientation.SelectedIndex == 0)
            {
                stpnlBorderHorizontal.Visibility = Visibility.Visible;
                stpnlBorderVertical.Visibility = Visibility.Collapsed;
                cmbVerticalBG.Visibility = Visibility.Collapsed;
                cmbHorizontalBG.Visibility = Visibility.Visible;
                if (chkBG.IsChecked == true)
                    cmbHorizontalBG.IsEnabled = true;
                else
                    cmbHorizontalBG.IsEnabled = false;
                btnSaveHorizontal.Visibility = Visibility.Visible;
                btnSaveVertical.Visibility = Visibility.Collapsed;
                if (productID[0] == "1")
                {
                    canbackground.Height = 401.25;
                    canbackground.Width = 535;
                    canbackground.InvalidateArrange();
                    canbackground.InvalidateMeasure();
                    canbackground.UpdateLayout();
                }
                else if (productID[0] == "2")
                {
                    canbackground.Height = 428;
                    canbackground.Width = 535;
                    canbackground.InvalidateArrange();
                    canbackground.InvalidateMeasure();
                    canbackground.UpdateLayout();
                }
                else if (productID[0] == "30")
                {
                    canbackground.Height = 356.84;
                    canbackground.Width = 535;
                    canbackground.InvalidateArrange();
                    canbackground.InvalidateMeasure();
                    canbackground.UpdateLayout();
                }
                else
                {
                    canbackground.Height = 356.84;
                    canbackground.Width = 535;
                    canbackground.InvalidateArrange();
                    canbackground.InvalidateMeasure();
                    canbackground.UpdateLayout();
                }
                ResetSpecPrintEffects();
                if (semiOrderSettings != null)
                    DisplayTextFields(semiOrderSettings.TextLogo_Horizontal);
                if (cmbHorizontalBG.SelectedIndex == 0)
                    imgBackImage.Source = null;
                else
                {
                    string bgvalue = ((System.Collections.Generic.KeyValuePair<string, string>)(cmbHorizontalBG.SelectedItem)).Key;
                    string backgroundThumbnailPath = System.IO.Path.Combine(LoginUser.DigiFolderBackGroundPath, "Thumbnails", bgvalue);
                    string backgroundThumbnailPanoramaPath = System.IO.Path.Combine(LoginUser.DigiFolderBackGroundPath, "Thumbnails\\Panorama", bgvalue);
                    spBackImage.Width = 138;
                    spBackImage.Height = 110;
                    if (strFound != null && File.Exists(backgroundThumbnailPanoramaPath))
                    {
                        imgBackImage.Source = new BitmapImage(new Uri(backgroundThumbnailPanoramaPath, UriKind.Absolute));
                    }
                    else if (File.Exists(backgroundThumbnailPath))
                    {
                        imgBackImage.Source = new BitmapImage(new Uri(backgroundThumbnailPath, UriKind.Absolute));

                    }

                }
            }
            else
            {
                stpnlBorderHorizontal.Visibility = Visibility.Collapsed;
                stpnlBorderVertical.Visibility = Visibility.Visible;
                btnSaveHorizontal.Visibility = Visibility.Collapsed;
                btnSaveVertical.Visibility = Visibility.Visible;
                cmbVerticalBG.Visibility = Visibility.Visible;
                if (chkBG.IsChecked == true)
                    cmbVerticalBG.IsEnabled = true;
                else
                    cmbVerticalBG.IsEnabled = false;
                cmbHorizontalBG.Visibility = Visibility.Collapsed;
                ResetSpecPrintEffects();
                mainImage.Source = null;
                mainImage_Size.Source = null;
                mainImagesize.Source = null;
                if (productID[0] == "1")
                {
                    canbackground.Height = 535;
                    canbackground.Width = 401.25;
                    canbackground.InvalidateArrange();
                    canbackground.InvalidateMeasure();
                    canbackground.UpdateLayout();
                }
                else if (productID[0] == "2")
                {
                    canbackground.Height = 535;
                    canbackground.Width = 428;
                    canbackground.InvalidateArrange();
                    canbackground.InvalidateMeasure();
                    canbackground.UpdateLayout();
                }
                else if (productID[0] == "30")
                {
                    canbackground.Height = 535;
                    canbackground.Width = 356.84;
                    canbackground.InvalidateArrange();
                    canbackground.InvalidateMeasure();
                    canbackground.UpdateLayout();
                }
                else
                {
                    canbackground.Height = 535;
                    canbackground.Width = 356.84;
                    canbackground.InvalidateArrange();
                    canbackground.InvalidateMeasure();
                    canbackground.UpdateLayout();
                }
                Uri ImagePathUri = new Uri(@"/DigiPhoto;component/images/125412627.jpg", UriKind.RelativeOrAbsolute);
                mainImage.Source = new BitmapImage(ImagePathUri);
                mainImage_Size.Source = new BitmapImage(ImagePathUri);
                mainImagesize.Source = new BitmapImage(ImagePathUri);
                canbackground.UpdateLayout();
                if (semiOrderSettings != null)
                    DisplayTextFields(semiOrderSettings.TextLogo_Vertical);
                if (cmbVerticalBG.SelectedIndex == 0)
                    imgBackImage.Source = null;
                else
                {
                    string bgvalue = ((System.Collections.Generic.KeyValuePair<string, string>)(cmbVerticalBG.SelectedItem)).Key;
                    string backgroundThumbnailPath = System.IO.Path.Combine(LoginUser.DigiFolderBackGroundPath, "Thumbnails", bgvalue);
                    string backgroundThumbnailPanoramaPath = System.IO.Path.Combine(LoginUser.DigiFolderBackGroundPath, "Thumbnails\\Panorama", bgvalue);
                    spBackImage.Height = 110;
                    spBackImage.Width = 88;
                    //imgBackImage.Source = new BitmapImage(new Uri(backgroundThumbnailPath, UriKind.Absolute));
                    if (strFound != null && File.Exists(backgroundThumbnailPanoramaPath))
                    {
                        imgBackImage.Source = new BitmapImage(new Uri(backgroundThumbnailPanoramaPath, UriKind.Absolute));
                    }
                    else if (File.Exists(backgroundThumbnailPath))
                    {
                        imgBackImage.Source = new BitmapImage(new Uri(backgroundThumbnailPath, UriKind.Absolute));
                    }
                }
            }
        }
        #endregion

        #region Common Functions
        /// <summary>
        /// Gets the semi order settings.
        /// </summary>
        public void GetSemiOrderSettings()
        {
            if (semiOrderSettings != null)
            {
                txtbrightness.Text = Convert.ToString(semiOrderSettings.DG_SemiOrder_Settings_AutoBright_Value);
                chkBorder.IsChecked = semiOrderSettings.DG_SemiOrder_Settings_IsImageFrame == null ? false : semiOrderSettings.DG_SemiOrder_Settings_IsImageFrame;
                chkbrightness.IsChecked = semiOrderSettings.DG_SemiOrder_Settings_AutoBright == null ? false : semiOrderSettings.DG_SemiOrder_Settings_AutoBright;
                chkcontrast.IsChecked = semiOrderSettings.DG_SemiOrder_Settings_AutoContrast == null ? false : semiOrderSettings.DG_SemiOrder_Settings_AutoContrast;
                SemiOrderPrimaryKey = semiOrderSettings.DG_SemiOrder_Settings_Pkey;
                BindProfileProductAssociation(SemiOrderPrimaryKey, semiOrderSettings.ProductName);
                chkPrintActive.IsChecked = semiOrderSettings.DG_SemiOrder_IsPrintActive == null ? false : semiOrderSettings.DG_SemiOrder_IsPrintActive;
                chkCrop.IsChecked = semiOrderSettings.DG_SemiOrder_IsCropActive == null ? false : semiOrderSettings.DG_SemiOrder_IsCropActive;
                Configuration.HorizontalCropCoordinates = semiOrderSettings.HorizontalCropValues;
                Configuration.VerticalCropCoordinates = semiOrderSettings.VerticalCropValues;
                cmbChromaClr.SelectedValue = semiOrderSettings.ChromaColor.ToString();
                txtColorCode.Text = semiOrderSettings.ColorCode;
                txtChromaTol.Text = semiOrderSettings.ClrTolerance;
                var horizentalBorder = lstBorderList.Where(t => t.DG_Border == semiOrderSettings.ImageFrame_Horizontal).FirstOrDefault();
                var verticalBorder = lstBorderList.Where(t => t.DG_Border == semiOrderSettings.ImageFrame_Vertical).FirstOrDefault();
                if (horizentalBorder != null)
                {
                    cmbImageOrientation.SelectedIndex = 0;
                    cmbborder.SelectedValue = lstBorderList.Where(t => t.DG_Border == semiOrderSettings.ImageFrame_Horizontal).FirstOrDefault().DG_Borders_pkey;
                    HorizontalBorder = semiOrderSettings.ImageFrame_Horizontal;
                    if(verticalBorder == null)
                    {
                        VerticalBorder = "";
                    }
                    IsHorizontal = true;
                }
                
                if (verticalBorder != null)
                {
                    cmbImageOrientation.SelectedIndex = 1;
                    cmbVerticalBorder.SelectedValue = lstBorderList.Where(t => t.DG_Border == semiOrderSettings.ImageFrame_Vertical).FirstOrDefault().DG_Borders_pkey;
                    VerticalBorder = semiOrderSettings.ImageFrame_Vertical;
                    if(horizentalBorder ==null)
                    {
                        HorizontalBorder = "";
                    }
                    IsHorizontal = false;
                }
                chkBG.IsChecked = semiOrderSettings.DG_SemiOrder_Settings_IsImageBG == null ? false : semiOrderSettings.DG_SemiOrder_Settings_IsImageBG;
                var horizontalBG = lstBGList.Where(t => t.Key == semiOrderSettings.Background_Horizontal).FirstOrDefault().Value;
                var verticalBG = lstBGList.Where(t => t.Key == semiOrderSettings.Background_Vertical).FirstOrDefault().Value;
                if (horizontalBG != null)
                {
                    cmbImageOrientation.SelectedIndex = 0;
                    cmbHorizontalBG.SelectedValue = horizontalBG;
                    HorizontalBackground = semiOrderSettings.Background_Horizontal;
                    if (verticalBG == null)
                    {
                        VerticalBackground = ""; //----Added on sep 14
                    }
                    IsHorizontal = true;
                }
                else
                    cmbHorizontalBG.SelectedIndex = 0;

                
                if (verticalBG != null)
                {
                    cmbImageOrientation.SelectedIndex = 1;
                    cmbVerticalBG.SelectedValue = verticalBG;
                    VerticalBackground = semiOrderSettings.Background_Vertical;
                    if (horizontalBG == null)
                    {
                        HorizontalBackground = "";//----Added on sep 14
                    }
                    IsHorizontal = false;
                }
                else
                    cmbVerticalBG.SelectedIndex = 0;

                chkEnableSOrder.IsChecked = semiOrderSettings.DG_SemiOrder_Environment == null ? true : semiOrderSettings.DG_SemiOrder_Environment;
                Graphics_HorizontalXml = new StringBuilder();
                Graphics_HorizontalXml = Graphics_HorizontalXml.Append(semiOrderSettings.Graphics_layer_Horizontal.ToString());
                textLogo_Horizontal = semiOrderSettings.TextLogo_Horizontal;
                strzoomHorizontaldetails = semiOrderSettings.ZoomInfo_Horizontal;
                IsHorizontalSettingsSaved = true;
                IsVerticalSettingsSaved = true;
                Graphics_VerticalXml = new StringBuilder();
                Graphics_VerticalXml = Graphics_VerticalXml.Append(semiOrderSettings.Graphics_layer_Vertical.ToString());
                textLogo_Vertical = semiOrderSettings.TextLogo_Vertical;
                strzoomVerticalDetails = semiOrderSettings.ZoomInfo_Vertical;
                SemiOrderPrimaryKey = semiOrderSettings.DG_SemiOrder_Settings_Pkey;
                DisplayTextFields(semiOrderSettings.TextLogo_Horizontal);

                if (cmbborder.SelectedValue == null)
                    cmbborder.SelectedIndex = 0;
                if (cmbVerticalBorder.SelectedValue == null)
                    cmbVerticalBorder.SelectedIndex = 0;
                string prdId = string.Empty;
                if (semiOrderSettings.DG_SemiOrder_ProductTypeId.Contains(','))
                    prdId = "1";
                else
                    prdId = semiOrderSettings.DG_SemiOrder_ProductTypeId;

                if (prdId == "1")
                {
                    canbackground.Height = 401.25;
                    canbackground.Width = 535;
                    canbackground.InvalidateArrange();
                    canbackground.InvalidateMeasure();
                    canbackground.UpdateLayout();
                }
                else if (prdId == "2")
                {
                    canbackground.Height = 428;
                    canbackground.Width = 535;
                    canbackground.InvalidateArrange();
                    canbackground.InvalidateMeasure();
                    canbackground.UpdateLayout();
                }
                if (chkBorder.IsChecked != true)
                {
                    cmbborder.IsEnabled = false;
                    cmbVerticalBorder.IsEnabled = false;
                }
            }
            else
            {
                SemiOrderPrimaryKey = 0;
                chkEnableSOrder.IsChecked = false;
                chkBorder.IsChecked = false;
                chkBG.IsChecked = false;
                chkbrightness.IsChecked = false;
                chkcontrast.IsChecked = false;
                cmbborder.SelectedValue = "0";
                cmbVerticalBorder.SelectedValue = "0";
                chkEnableSOrder.IsChecked = false;
                chkCrop.IsChecked = false;
                Configuration.HorizontalCropCoordinates = string.Empty;
                Configuration.VerticalCropCoordinates = string.Empty;
                HorizontalBorder = string.Empty;
                //Graphics_HorizontalXml = new StringBuilder();
                textLogo_Horizontal = string.Empty;
                strzoomHorizontaldetails = string.Empty;
                IsHorizontalSettingsSaved = false;
                IsVerticalSettingsSaved = false;
                VerticalBorder = string.Empty;
                Graphics_VerticalXml = new StringBuilder();
                textLogo_Vertical = string.Empty;
                strzoomVerticalDetails = string.Empty;
                HorizontalBackground = string.Empty;
                VerticalBackground = string.Empty;
            }
            if (IsHorizontal)
            {
                cmbImageOrientation.SelectedIndex = 0;
            }
            else
            {
                cmbImageOrientation.SelectedIndex = 1;
            }
            SetOrientatin();
            Uri ImagePathUri = new Uri(@"/DigiPhoto;component/images/125412626.jpg", UriKind.RelativeOrAbsolute);
            mainImage.Source = new BitmapImage(ImagePathUri);
            mainImage_Size.Source = new BitmapImage(ImagePathUri);
            mainImagesize.Source = new BitmapImage(ImagePathUri);
            canbackground.UpdateLayout();
        }

        class TextContent
        {
            public string Id { get; set; }
            public string Name { get; set; }
            public int FontSize { get; set; }
            public string FontColor { get; set; }
            public string FontFamily { get; set; }
        }

        private void DisplayTextFields(string XmlValue)
        {
            txtContent.Text = "Enter Text...";
            cmbFont.SelectedIndex = 0;
            CmbColor.SelectedIndex = 2;
            CmbFontSize.SelectedIndex = 11;
            XmlValue = "<XmlRoot>" + XmlValue + "</XmlRoot>";
            System.Xml.XmlDocument Xdocument = new XmlDocument();
            Xdocument.LoadXml(XmlValue);
            XmlNodeList xmllst = Xdocument.GetElementsByTagName("TextLogo");
            System.Xml.XmlReader rdr = System.Xml.XmlReader.Create(new System.IO.StringReader(Xdocument.InnerXml.ToString()));
            lstTextContent.Clear();

            while (rdr.Read())
            {
                if (rdr.NodeType == XmlNodeType.Element)
                {
                    switch (rdr.Name.ToString().ToLower())
                    {
                        case "textlogo":
                            lstTextContent.Add(new TextContent()
                            {
                                Id = "txt" + Convert.ToString(rdr.GetAttribute("Name")),
                                Name = Convert.ToString(rdr.GetAttribute("Content")),
                                FontSize = Convert.ToInt32(rdr.GetAttribute("FontSize")),
                                FontFamily = Convert.ToString(rdr.GetAttribute("FontFamily")),
                                FontColor = System.Drawing.ColorTranslator.FromHtml(Convert.ToString(rdr.GetAttribute("FontColor"))).Name
                            });

                            lstTextCollection.Add(new TextContent()
                            {
                                Id = "txt" + Convert.ToString(rdr.GetAttribute("Name")),
                                Name = Convert.ToString(rdr.GetAttribute("Content")),
                                FontSize = Convert.ToInt32(rdr.GetAttribute("FontSize")),
                                FontFamily = Convert.ToString(rdr.GetAttribute("FontFamily")),
                                FontColor = Convert.ToString(rdr.GetAttribute("FontColor"))
                            });

                            StackPanel spTextField = new StackPanel();
                            TextBox txtLogo = new TextBox();
                            if (rdr.GetAttribute("FontFamily").ToString() != string.Empty)
                            {
                                System.Windows.Media.FontFamily fw = (System.Windows.Media.FontFamily)new FontFamilyConverter().ConvertFromString(rdr.GetAttribute("FontFamily").ToString());
                                txtLogo.FontFamily = fw;
                            }
                            txtLogo.Background = new SolidColorBrush(Colors.Transparent);
                            // FontColor
                            if (rdr.GetAttribute("FontColor").ToString() != string.Empty)
                            {
                                var converter = new System.Windows.Media.BrushConverter();
                                var brush = (System.Windows.Media.Brush)converter.ConvertFromString(rdr.GetAttribute("FontColor").ToString());
                                txtLogo.Foreground = brush;
                            }
                            else
                            {
                                txtLogo.Foreground = new SolidColorBrush(Colors.DarkRed);
                            }
                            if (rdr.GetAttribute("FontSize").ToString() != string.Empty)
                            {
                                txtLogo.FontSize = Convert.ToDouble(rdr.GetAttribute("FontSize").ToString());
                            }
                            else
                            {
                                txtLogo.FontSize = 36.00;
                            }
                            // Top // Left // Angle // ZIndex
                            if (rdr.GetAttribute("Content").ToString() != string.Empty)
                            {
                                txtLogo.Text = rdr.GetAttribute("Content").ToString();
                            }
                            txtLogo.Uid = "txtLogoBlock";

                            if (rdr.GetAttribute("Name").ToString() != string.Empty)
                            {
                                txtLogo.Name = "txt" + rdr.GetAttribute("Name").ToString();
                            }
                            dragCanvas.Children.Add(txtLogo);
                            Canvas.SetLeft(txtLogo, Convert.ToDouble(rdr.GetAttribute("Left").ToString()));
                            Canvas.SetTop(txtLogo, Convert.ToDouble(rdr.GetAttribute("Top").ToString()));
                            if (rdr.GetAttribute("ZIndex").ToString() != string.Empty)
                            {
                                Canvas.SetZIndex(txtLogo, Convert.ToInt32(rdr.GetAttribute("ZIndex").ToString()));
                                Canvas.SetZIndex(txtLogo, Convert.ToInt32(rdr.GetAttribute("ZIndex").ToString()));
                            }
                            else
                            {
                                Canvas.SetZIndex(txtLogo, 4);
                            }


                            txtLogo.MouseLeftButtonUp += new MouseButtonEventHandler(SelectObject);
                            break;
                    }
                    grdTextField.ItemsSource = null;
                    grdTextField.ItemsSource = lstTextContent;
                }
                if (lstTextContent == null || lstTextContent.Count == 0)
                {
                    grdTextField.Visibility = Visibility.Collapsed;
                }
                else
                    grdTextField.Visibility = Visibility.Visible;
            }
        }

        private void RemoveTextFields()
        {
            List<UIElement> itemstoremove = new List<UIElement>();
            foreach (UIElement ui in dragCanvas.Children)
            {
                if (ui is TextBox)
                {
                    itemstoremove.Add(ui);
                }

            }
            foreach (UIElement ui in itemstoremove)
            {
                dragCanvas.Children.Remove(ui);
            }

            //HorizontalBackground = string.Empty;
            //VerticalBackground = string.Empty;
            //VerticalBorder = string.Empty;
            //HorizontalBorder = string.Empty;

        }

        /// <summary>
        /// This method made public accessible so we can access it from other Forms throughout the application. This was required to get latest added borders in Spec Print tab dropdownlist
        /// </summary>
        /// Changes made by Vins 1Aug2018_11:21AM
        public void BindBorders()
        {
            lstBorderList = (new BorderBusiness()).GetBorderDetails();
            lstBorderList = lstBorderList.Where(o => o.DG_IsActive == true).ToList();
            CommonUtility.BindComboWithSelect<BorderInfo>(cmbborder, lstBorderList, "DG_Border", "DG_Borders_pkey", 0, ClientConstant.SelectString);
            CommonUtility.BindCombo<BorderInfo>(cmbVerticalBorder, lstBorderList, "DG_Border", "DG_Borders_pkey");
            cmbborder.SelectedValue = "0";
            cmbVerticalBorder.SelectedValue = "0";
        }
        /// <summary>
        /// Deletes the graphics.
        /// </summary>
        public void DeleteGraphics()
        {
            if (this.elementForContextMenu is Button)
            {
                Button parent = (Button)this.elementForContextMenu;
                if (parent != null)
                {
                    dragCanvas.Children.Remove(parent);
                }
            }
        }
        private void OpenAssociateWindow()
        {
            grdSpecProf.IsEnabled = false;
            //grdSpecProf.Background = Brushes.Black;
            grdSpecProf.Opacity = 0.4;
            ucDynamicImgCrop.Visibility = Visibility.Visible;
            ucDynamicImgCrop.cmbImgOrientation.IsEnabled = false;
            if (cmbImageOrientation.SelectedIndex == 0)
                ucDynamicImgCrop.cmbImgOrientation.SelectedIndex = 0;
            else
                ucDynamicImgCrop.cmbImgOrientation.SelectedIndex = 1;
            ucDynamicImgCrop.txtImgPath.Focus();
            FocusManager.SetFocusedElement(this, ucDynamicImgCrop.txtImgPath);
            Keyboard.Focus(ucDynamicImgCrop.txtImgPath);
        }
        public RenderTargetBitmap jCaptureScreen(Grid forWdht)
        {
            RenderTargetBitmap renderBitmap = null;
            try
            {
                this.InvalidateVisual();
                BitmapImage bi = mainImage.Source as BitmapImage;
                double dpiX = bi.DpiX;
                double dpiY = bi.DpiY;
                RenderOptions.SetBitmapScalingMode(forWdht, BitmapScalingMode.HighQuality);
                RenderOptions.SetEdgeMode(forWdht, EdgeMode.Aliased);
                try
                {
                    if (_ZoomFactor > 1.4)
                    {
                        Size size = new Size(forWdht.ActualWidth, forWdht.ActualHeight);
                        renderBitmap = new RenderTargetBitmap((int)(size.Width * dpiX / 96.0 * (1 / _ZoomFactor)), (int)(size.Height * dpiY / 96.0 * (1 / _ZoomFactor)),
                            dpiX, dpiY, PixelFormats.Default);
                        RenderOptions.SetEdgeMode(forWdht, EdgeMode.Aliased);
                        forWdht.SnapsToDevicePixels = true;
                        forWdht.RenderTransform = new ScaleTransform(1 / _ZoomFactor, 1 / _ZoomFactor, 0.5, 0.5);
                        forWdht.Measure(size);
                        forWdht.Arrange(new Rect(size));
                        renderBitmap.Render(forWdht);
                        forWdht.RenderTransform = null;
                    }
                    else
                    {
                        Size size = new Size(forWdht.ActualWidth, forWdht.ActualHeight);
                        renderBitmap = new RenderTargetBitmap((int)(size.Width * dpiY / 96.0),
                            (int)(size.Height * dpiY / 96.0), dpiX, dpiY, PixelFormats.Default);
                        RenderOptions.SetEdgeMode(forWdht, EdgeMode.Aliased);
                        forWdht.SnapsToDevicePixels = true;
                        forWdht.RenderTransform = new ScaleTransform(1, 1, 0.5, 0.5);
                        forWdht.Measure(size);
                        forWdht.Arrange(new Rect(size));
                        renderBitmap.Render(forWdht);
                        forWdht.RenderTransform = null;
                    }
                    return renderBitmap;
                }
                catch (Exception ex)
                {
                    Rect bounds = VisualTreeHelper.GetDescendantBounds(GrdBrightness);
                    DrawingVisual dv = new DrawingVisual();
                    renderBitmap = new RenderTargetBitmap((int)(bounds.Width * dpiX / 96.0),
                                                                  (int)(bounds.Height * dpiY / 96.0),
                                                                  dpiX,
                                                                  dpiY,
                                                                  PixelFormats.Default);

                    using (DrawingContext ctx = dv.RenderOpen())
                    {
                        VisualBrush vb = new VisualBrush(forWdht);
                        ctx.DrawRectangle(vb, null, new Rect(new System.Windows.Point(), bounds.Size));
                    }

                    renderBitmap.Render(dv);
                    return renderBitmap;
                }
            }
            catch (Exception ex1)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex1);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                return renderBitmap;
            }
        }

        public void ResetSpecControlValues()
        {
            txtAngle.Text = "0";
            chkbrightness.IsChecked = false;
            chkcontrast.IsChecked = false;
            txtbrightness.Text = "0";
            txtcontrast.Text = "1";
            chkBorder.IsChecked = false;
            cmbborder.SelectedIndex = 0;
            cmbVerticalBorder.SelectedIndex = 0;
            chkCrop.IsChecked = false;
            cmbChromaClr.SelectedIndex = 0;
            cmbImageOrientation.SelectedIndex = 0;
            txtColorCode.Text = string.Empty;
            txtChromaTol.Text = string.Empty;
            chkBG.IsChecked = false;
            cmbHorizontalBG.SelectedIndex = 0;
            cmbVerticalBG.SelectedIndex = 0;
            chkPrintActive.IsChecked = false;
            chkEnableSOrder.IsChecked = true;
            Configuration.HorizontalCropCoordinates = string.Empty;
            Configuration.VerticalCropCoordinates = string.Empty;
            this.semiOrderSettings = null;
            SemiOrderPrimaryKey = 0;
            for (int i = 0; i < lstboxProductTypes.Items.Count; i++)
            {
                DependencyObject obj = lstboxProductTypes.ItemContainerGenerator.ContainerFromIndex(i);
                if (obj != null)
                {
                    CheckBox rdo = FindVisualChild<CheckBox>(obj);
                    if (rdo != null)
                        rdo.IsChecked = false;
                }
            }

        }
        public void ResetVariables()
        {
            HorizontalBorder = string.Empty;
            Graphics_HorizontalXml = new StringBuilder();
            textLogo_Horizontal = string.Empty;
            strzoomHorizontaldetails = string.Empty;
            IsHorizontalSettingsSaved = false;
            IsVerticalSettingsSaved = false;
            VerticalBorder = string.Empty;
            Graphics_VerticalXml = new StringBuilder();
            textLogo_Vertical = string.Empty;
            strzoomVerticalDetails = string.Empty;
            HorizontalBackground = string.Empty;
            VerticalBackground = string.Empty;
        }
        private void ResetGridEffects()
        {
            Grdcartoonize.Effect = null;
            Grdcartoonize.RenderTransform = null;
            Canvas.SetLeft(Grdcartoonize, 0);
            Canvas.SetTop(Grdcartoonize, 0);
            GrdContrast.Effect = null;
            GrdContrast.RenderTransform = null;
            Canvas.SetLeft(GrdContrast, 0);
            Canvas.SetTop(GrdContrast, 0);
            GrdBrightness.Effect = null;
            GrdBrightness.RenderTransform = null;
            Canvas.SetLeft(GrdBrightness, 0);
            Canvas.SetTop(GrdBrightness, 0);
            canbackground.Effect = null;
            canbackground.RenderTransform = null;
            Canvas.SetLeft(canbackground, 0);
            Canvas.SetTop(canbackground, 0);
            canbackground.Background = null;
            dragCanvas.Effect = null;
            dragCanvas.RenderTransform = null;
            Canvas.SetLeft(dragCanvas, 0);
            Canvas.SetTop(dragCanvas, 0);
            frm.Effect = null;
            frm.RenderTransform = null;
            Canvas.SetLeft(frm, 0);
            Canvas.SetTop(frm, 0);
            mainImage.Source = null;
            mainImage_Size.Source = null;
            mainImagesize.Source = null;
            canbackground.UpdateLayout();
        }
        public void ResetSpecPrintEffects()
        {
            SpecFileName = string.Empty;
            if (!String.IsNullOrWhiteSpace(SpecFileName))
                btnChroma.IsEnabled = true;
            btndelete.IsEnabled = false;
            btngraphics1.IsEnabled = false;
            btnPreview.IsEnabled = false;

            Grdcartoonize.Effect = null;
            Grdcartoonize.RenderTransform = null;
            Canvas.SetLeft(Grdcartoonize, 0);
            Canvas.SetTop(Grdcartoonize, 0);
            GrdContrast.Effect = null;
            GrdContrast.RenderTransform = null;
            Canvas.SetLeft(GrdContrast, 0);
            Canvas.SetTop(GrdContrast, 0);
            GrdBrightness.Effect = null;
            GrdBrightness.RenderTransform = null;
            Canvas.SetLeft(GrdBrightness, 0);
            Canvas.SetTop(GrdBrightness, 0);
            canbackground.Effect = null;
            canbackground.RenderTransform = null;
            Canvas.SetLeft(canbackground, 0);
            Canvas.SetTop(canbackground, 0);
            canbackground.Background = null;
            dragCanvas.Effect = null;
            dragCanvas.RenderTransform = null;
            Canvas.SetLeft(dragCanvas, 0);
            Canvas.SetTop(dragCanvas, 0);
            frm.Effect = null;
            frm.RenderTransform = null;
            Canvas.SetLeft(frm, 0);
            Canvas.SetTop(frm, 0);
            if (frm.Children.Count > 0)
            {
                for (int i = 0; i < frm.Children.Count; i++)
                {
                    frm.Children.RemoveAt(i);
                }
            }

            for (int j = 0; j < dragCanvas.Children.Count; j++)
            {
                if (dragCanvas.Children[j] is Button)
                {
                    dragCanvas.Children.RemoveAt(j);
                    j--;
                }
            }
            #region Reset TextField
            txtContent.Text = "Enter Text...";
            cmbFont.SelectedIndex = 0;
            CmbColor.SelectedIndex = 0;
            CmbFontSize.SelectedIndex = 6;
            grdTextField.ItemsSource = null;
            grdTextField.Visibility = Visibility.Collapsed;
            RemoveTextFields();
            #endregion
            frm.Effect = null;
            frm.RenderTransform = null;

            mainImage.Source = null;
            mainImage_Size.Source = null;
            mainImagesize.Source = null;
            Uri ImagePathUri = new Uri(@"/DigiPhoto;component/images/125412626.jpg", UriKind.RelativeOrAbsolute);
            mainImage.Source = new BitmapImage(ImagePathUri);
            mainImage_Size.Source = new BitmapImage(ImagePathUri);
            mainImagesize.Source = new BitmapImage(ImagePathUri);
            canbackground.UpdateLayout();
        }
        //Method to enlist backgrounds
        /// <summary>
        /// Method made public to make it accessible fro any windows
        /// Chnages made by Vins_1Aug2018
        /// </summary>
        public void loadProductType()
        {
            lstBGList = new Dictionary<string, string>();
            try
            {
                lstBGList.Add("--Select--", "0");
                BackgroundBusiness bacBiz = new BackgroundBusiness();
                foreach (var item in bacBiz.GetBackgoundDetailsALL())
                {
                    if (item.DG_Background_IsActive.HasValue && item.DG_Background_IsActive.Value)
                    {
                        lstBGList.Add(item.DG_BackGround_Image_Name.ToString(), item.DG_Background_pkey.ToString());
                    }
                }
                cmbHorizontalBG.ItemsSource = lstBGList;
                cmbHorizontalBG.SelectedValue = "0";
                cmbVerticalBG.ItemsSource = lstBGList;
                cmbVerticalBG.SelectedValue = "0";
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }
        List<ProductTypeInfo> lstproductTypes;
        private void FillProductCombo()
        {
            try
            {
                lstproductTypes = (new ProductBusiness()).GetProductType().Where(t => t.DG_IsPrimary == true && t.DG_Orders_ProductType_pkey != 4 && t.DG_Orders_ProductType_pkey != 101 && t.DG_Orders_ProductType_pkey != 105 && t.DG_Orders_ProductType_pkey != 1091 && t.DG_Orders_ProductType_pkey != 100).ToList();
                lstboxProductTypes.ItemsSource = lstproductTypes;
                //for (int j = 0; j < lstboxProductTypes.Items.Count; j++)
                //{
                lstboxProductTypes.ScrollIntoView(lstboxProductTypes.Items[0]);
                //}
                //CommonUtility.BindComboWithSelect<ProductTypeInfo>(cmbProductType, primaryProduct, "DG_Orders_ProductType_Name", "DG_Orders_ProductType_pkey", 0, ClientConstant.SelectString);
                //cmbProductType.SelectedValue = "0";
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }
        public void LoadGraphics()
        {
            try
            {
                string pathgraphics = LoginUser.DigiFolderGraphicsPath + "\\";
                lstGraphics.Items.Clear();
                var objGraphicsDetails = (new GraphicsBusiness()).GetGraphicsDetails();
                foreach (var item in objGraphicsDetails)
                {
                    if (item.DG_Graphics_IsActive.HasValue && item.DG_Graphics_IsActive.Value)
                    {
                        Uri uri = new Uri(pathgraphics + item.DG_Graphics_Name);
                        LstMyItems _objnew = new LstMyItems();
                        BitmapImage bmp = new BitmapImage(uri);
                        _objnew.BmpImage = bmp;
                        _objnew.Name = item.DG_Graphics_Name;
                        _objnew.Photoname1 = item.DG_Graphics_Displayname;
                        _objnew.FilePath = pathgraphics + item.DG_Graphics_Name;
                        lstGraphics.Items.Add(_objnew);
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }
        public static childItem FindVisualChild<childItem>(DependencyObject obj)
where childItem : DependencyObject
        {
            // Search immediate children
            for (int i = 0; i < VisualTreeHelper.GetChildrenCount(obj); i++)
            {
                DependencyObject child = VisualTreeHelper.GetChild(obj, i);
                if (child is childItem)
                    return (childItem)child;
                else
                {
                    childItem childOfChild = FindVisualChild<childItem>(child);
                    if (childOfChild != null)
                        return childOfChild;
                }
            }
            return null;
        }
        public string BindProfileProductAssociation(int profileId, string ProfileName)
        {
            string productNames = string.Empty;
            if (lstproductTypes != null)
            {
                foreach (var item in lstproductTypes)
                {
                    string[] arr = ProfileName.Split(',');
                    if (arr.Contains(item.DG_Orders_ProductType_Name))
                    {
                        item.IsChecked = true;
                    }
                    else
                    { item.IsChecked = false; }
                }
                lstboxProductTypes.ItemsSource = null;
                lstboxProductTypes.ItemsSource = lstproductTypes;
                lstboxProductTypes.UpdateLayout();
                //for (int j = 0; j < lstboxProductTypes.Items.Count; j++)
                //{
                lstboxProductTypes.ScrollIntoView(lstboxProductTypes.Items[0]);
                //}
            }
            return productNames;
        }
        #endregion

        #region Text Logo
        /// <summary>
        /// Adds the default items.
        /// </summary>
        private void AddDefaultFontSize()
        {
            CmbFontSize.Items.Clear();
            CmbFontSize.Items.Add("10");
            CmbFontSize.Items.Add("11");
            CmbFontSize.Items.Add("12");
            CmbFontSize.Items.Add("14");
            CmbFontSize.Items.Add("16");
            CmbFontSize.Items.Add("18");
            CmbFontSize.Items.Add("20");
            CmbFontSize.Items.Add("22");
            CmbFontSize.Items.Add("24");
            CmbFontSize.Items.Add("26");
            CmbFontSize.Items.Add("28");
            CmbFontSize.Items.Add("36");
            CmbFontSize.Items.Add("48");
            CmbFontSize.Items.Add("72");
            CmbFontSize.Items.Add("80");
            CmbFontSize.Items.Add("90");
            CmbFontSize.Items.Add("100");

        }
        private void btnAddTextLogo_Click(object sender, RoutedEventArgs e)
        {
            //this.elementForContextMenu = (Button)sender;
            this.elementForContextMenu = null;
            AddTextLogo();

        }
        private void AddTextLogo()
        {
            TextBox txtTest = new TextBox();
            txtTest.ContextMenu = dragCanvas.ContextMenu;
            txtTest.Foreground = new SolidColorBrush(Colors.White);
            txtTest.Background = new SolidColorBrush(Colors.Transparent);
            txtTest.FontWeight = FontWeights.Bold;
            txtTest.MaxLength = 200;
            txtTest.FontSize = 36;
            CmbFontSize.SelectedIndex = 11;
            cmbFont.SelectedIndex = 0;
            CmbColor.SelectedIndex = 2;


            txtTest.FontFamily = (System.Windows.Media.FontFamily)new FontFamilyConverter().ConvertFromString("Arial");
            txtTest.Text = "Enter Text...";
            txtTest.RenderTransformOrigin = new System.Windows.Point(0.5, 0.5);
            txtTest.Uid = "txtblock";
            txtTest.BorderBrush = null;
            txtTest.Style = (Style)FindResource("SearchIDTB");
            RotateTransform rtm = new RotateTransform();
            txtTest.RenderTransform = rtm;
            txtTest.MouseLeftButtonUp += new MouseButtonEventHandler(SelectObject);
            txtTest.LostFocus += new RoutedEventHandler(txtContent_LostFocus);
            txtTest.GotFocus += new RoutedEventHandler(txtContent_GotFocus);
            txtTest.TextChanged += new TextChangedEventHandler(txtTest_TextChanged);
            txtContent.Text = txtTest.Text;
            dragCanvas.Children.Add(txtTest);
            Canvas.SetLeft(txtTest, GrdBrightness.ActualWidth / 2);
            Canvas.SetTop(txtTest, GrdBrightness.ActualHeight / 2);
            Canvas.SetZIndex(txtTest, 11);
            txtContent.Focus();
            this.elementForContextMenu = txtTest;

        }
        /// <summary>
        /// Handles the TextChanged event of the txtTest control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="TextChangedEventArgs"/> instance containing the event data.</param>
        void txtTest_TextChanged(object sender, TextChangedEventArgs e)
        {
            if ((sender is TextBox))
            {
                TextBox GraphicText = (TextBox)sender;
                txtContent.Text = GraphicText.Text;
            }
        }

        /// <summary>
        /// Handles the LostFocus event of the txtContent control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void txtContent_LostFocus(object sender, RoutedEventArgs e)
        {
            if ((this.elementForContextMenu is TextBox))
            {
                TextBox GraphicText = (TextBox)this.elementForContextMenu;
                GraphicText.BorderBrush = System.Windows.Media.Brushes.Transparent;
            }
            if (txtContent.Text == "")
            {
                //Enter text...
                txtContent.Text = "Enter Text...";
            }
        }
        /// <summary>
        /// Handles the GotFocus event of the txtContent control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void txtContent_GotFocus(object sender, RoutedEventArgs e)
        {
            if ((this.elementForContextMenu is TextBox))
            {
                TextBox GraphicText = (TextBox)this.elementForContextMenu;
                GraphicText.BorderBrush = System.Windows.Media.Brushes.OrangeRed;
            }
            if (txtContent.Text == "Enter Text...")
            {
                //Enter text...
                txtContent.Text = "";
            }
        }
        /// <summary>
        /// Handles the TextChanged event of the txtContent control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="TextChangedEventArgs"/> instance containing the event data.</param>
        private void txtContent_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (this.elementForContextMenu != null)
            {
                if ((this.elementForContextMenu is TextBox))
                {

                    TextBox GraphicText = (TextBox)this.elementForContextMenu;
                    GraphicText.Text = txtContent.Text;
                }
            }
        }
        /// <summary>
        /// Handles the SelectionChanged event of the cmbFont control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="SelectionChangedEventArgs"/> instance containing the event data.</param>
        private void cmbFont_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (this.elementForContextMenu != null)
            {
                if ((this.elementForContextMenu is TextBox))
                {
                    if (cmbFont.SelectedValue != null)
                    {
                        TextBox GraphicText = (TextBox)this.elementForContextMenu;
                        GraphicText.FontFamily = (System.Windows.Media.FontFamily)cmbFont.SelectedValue;
                    }
                }
            }
        }
        /// <summary>
        /// Handles the SelectionChanged event of the CmbColor control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="SelectionChangedEventArgs"/> instance containing the event data.</param>
        private void CmbColor_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (this.elementForContextMenu != null)
            {
                if ((this.elementForContextMenu is TextBox))
                {
                    if (CmbColor.SelectedValue != null)
                    {
                        TextBox GraphicText = (TextBox)this.elementForContextMenu;

                        GraphicText.Foreground = (System.Windows.Media.SolidColorBrush)CmbColor.SelectedValue;
                    }
                }
            }
        }
        /// <summary>
        /// Handles the SelectionChanged event of the CmbFontSize control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="SelectionChangedEventArgs"/> instance containing the event data.</param>
        private void CmbFontSize_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (this.elementForContextMenu != null)
            {
                if ((this.elementForContextMenu is TextBox))
                {
                    if (CmbFontSize.SelectedValue != null)
                    {
                        TextBox GraphicText = (TextBox)this.elementForContextMenu;
                        GraphicText.FontSize = Convert.ToDouble(CmbFontSize.SelectedValue.ToString());
                    }
                }
            }
        }
        private void DeleteTextLogo()
        {
            try
            {
                if (this.elementForContextMenu != null)
                {
                    if (this.elementForContextMenu is TextBox)
                    {
                        TextBox parent = (TextBox)this.elementForContextMenu;
                        if (parent != null)
                        {
                            dragCanvas.Children.Remove(parent);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }

        private void btnDeleteTextLogo_Click(object sender, RoutedEventArgs e)
        {
            DeleteTextLogo();
            var stuffToRemove = lstTextContent.SingleOrDefault(s => s.Id == itemNo);
            lstTextContent.Remove(stuffToRemove);
            grdTextField.ItemsSource = null;
            grdTextField.ItemsSource = lstTextContent;

            txtContent.Text = "Enter text...";
            CmbFontSize.SelectedIndex = 6;
            cmbFont.SelectedIndex = 0;
            CmbColor.SelectedIndex = 0;
        }
        #endregion Text Logo

        private void chkPrintActive_Checked(object sender, RoutedEventArgs e)
        {
            if (chkEnableSOrder != null)
                chkEnableSOrder.IsChecked = false;
        }

        private void chkEnableSOrder_Checked(object sender, RoutedEventArgs e)
        {
            if (chkPrintActive != null)
                chkPrintActive.IsChecked = false;
        }

        private void PastingHandler(object sender, DataObjectPastingEventArgs e)
        {
            if (e.DataObject.GetDataPresent(typeof(String)))
            {
                String text = (String)e.DataObject.GetData(typeof(String));
                if (!IsTextAllowed(text)) e.CancelCommand();
            }
            else e.CancelCommand();
        }
        private static bool IsTextAllowed(string text)
        {
            //text = Regex.Replace(text, @"\s", ">");
            Regex regex = new Regex("[< >]+"); //regex that matches disallowed text
            return !regex.IsMatch(text);
        }

        private void txtContent_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = !IsTextAllowed(e.Text.Trim());
        }

        private void grdTextField_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (grdTextField.SelectedItem != null)
            {
                string selectedText = ((DigiPhoto.AddEditSpecPrintProfile.TextContent)(grdTextField.SelectedItem)).Name;
                foreach (var item in lstTextCollection)
                {
                    if (selectedText == item.Name)
                    {
                        LocateTextBox(item.Id);
                        itemNo = item.Id;
                        txtContent.Text = item.Name;
                        cmbFont.Text = item.FontFamily;
                        CmbFontSize.Text = Convert.ToString(item.FontSize);
                        //    System.Drawing.Color col = System.Drawing.ColorTranslator.FromHtml(item.FontColor);
                        CmbColor.Text = item.FontColor;
                    }
                }
            }
        }

        private void LocateTextBox(string TBNum)
        {
            TextBox foundTextBox = FindChild<TextBox>(dragCanvas, TBNum);
            if (foundTextBox != null)
            {
                this.elementForContextMenu = foundTextBox;
            }
        }
        /// <summary>
        /// Finds a Child of a given item in the visual tree. 
        /// </summary>
        /// <param name="parent">A direct parent of the queried item.</param>
        /// <typeparam name="T">The type of the queried item.</typeparam>
        /// <param name="childName">x:Name or Name of child. </param>
        /// <returns>The first parent item that matches the submitted type parameter. 
        /// If not matching item can be found, 
        /// a null parent is being returned.</returns>
        public static T FindChild<T>(DependencyObject parent, string childName)
           where T : DependencyObject
        {
            // Confirm parent and childName are valid. 
            if (parent == null) return null;

            T foundChild = null;

            int childrenCount = VisualTreeHelper.GetChildrenCount(parent);
            for (int i = 0; i < childrenCount; i++)
            {
                var child = VisualTreeHelper.GetChild(parent, i);
                // If the child is not of the request child type child
                T childType = child as T;
                if (childType == null)
                {
                    // recursively drill down the tree
                    foundChild = FindChild<T>(child, childName);

                    // If the child is found, break so we do not overwrite the found child. 
                    if (foundChild != null) break;
                }
                else if (!string.IsNullOrEmpty(childName))
                {
                    var frameworkElement = child as FrameworkElement;
                    // If the child's name is set for search
                    if (frameworkElement != null && frameworkElement.Name == childName)
                    {
                        // if the child's name is of the request name
                        foundChild = (T)child;
                        break;
                    }
                }
                else
                {
                    // child element found.
                    foundChild = (T)child;
                    break;
                }
            }

            return foundChild;
        }
        /// <summary>
        /// ////////////////////////////////////new method created by latika to set the graphics while edit
        /// </summary>
        /// <param name="strGraphicpath"></param>
        public void setCanves(StringBuilder strGraphicpath)
        {
            string[] ArgGr = (strGraphicpath.ToString()).Replace("'", "").Split('/');
            for (int Lop = 0; Lop < ArgGr.Length - 1; Lop++)
            {
                string[] ArgGr2 = ((ArgGr[Lop].Replace("<graphics", "")).Replace("graphics>", "")).Replace("><", "").Split('=');
                Button btngrph = new Button();
                Uri uris = new Uri(LoginUser.DigiFolderGraphicsPath + "\\" + ArgGr2[2].Replace(" angle", ""));
                BitmapImage img = new BitmapImage(uris);
                //Button btn = (Button)(sender);
                btngrph.HorizontalAlignment = System.Windows.HorizontalAlignment.Center;
                btngrph.VerticalAlignment = System.Windows.VerticalAlignment.Center;
                Style defaultStyle = (Style)FindResource("ButtonStyleGraphic");
                btngrph.Style = defaultStyle;
                System.Windows.Controls.Image imgctrl = new System.Windows.Controls.Image();
                //itmapImage img = new BitmapImage(new Uri(btn.Tag.ToString()));
                imgctrl.Name = "A" + Guid.NewGuid().ToString().Split('-')[0].ToString();
                btngrph.Name = "btn" + Guid.NewGuid().ToString().Split('-')[0].ToString();
                btngrph.Uid = "uid" + Guid.NewGuid().ToString().Split('-')[0].ToString();
                imgctrl.Source = img;
                btngrph.Tag = "1";

                double wdt = 1, hgt = 1;

                if (wdt != 0)
                {
                    if (wdt < 1)
                    {
                        btngrph.Width = 90 / wdt;
                        btngrph.Height = 90 / hgt;
                    }
                    else
                    {
                        btngrph.Width = 90 * wdt;
                        btngrph.Height = 90 * hgt;
                    }
                }
                else
                {
                    btngrph.Width = wdt;
                    btngrph.Height = hgt;
                }
                btngrph.Width = Convert.ToDouble(ArgGr2[1].Replace(" source", ""));
                double scalex = Convert.ToDouble(ArgGr2[6].Replace(" scaley", ""));
                double scaley = Convert.ToDouble(ArgGr2[7].Replace(" zoomfactor", ""));
                double left = Convert.ToDouble(ArgGr2[5].Replace(" scalex", ""));
                double top = Convert.ToDouble(ArgGr2[4].Replace(" left", ""));
                double tag = Convert.ToDouble(ArgGr2[8].Replace(" zindex", ""));
                double Angle = Convert.ToDouble(ArgGr2[3].Replace(" top", ""));
                btngrph.Tag = tag;

                btngrph.Content = imgctrl;
                dragCanvas.Children.Add(btngrph);
                Canvas.SetLeft(btngrph, left);
                Canvas.SetTop(btngrph, top);

                ////////////////
                // Set the center point of the transforms.
                btngrph.RenderTransformOrigin = new Point(0.5, 0.5);

                // Create a transform to scale the size of the button.
                ScaleTransform myScaleTransform = new ScaleTransform();

                // Set the transform to triple the scale in the Y direction.
                myScaleTransform.ScaleY = scalex;
                myScaleTransform.ScaleX = scaley;


                // Create a transform to rotate the button
                RotateTransform myRotateTransform = new RotateTransform();

                // Set the rotation of the transform to 45 degrees.
                myRotateTransform.Angle = Angle;

                // Create a TransformGroup to contain the transforms
                // and add the transforms to it.
                TransformGroup myTransformGroup = new TransformGroup();
                myTransformGroup.Children.Add(myScaleTransform);
                myTransformGroup.Children.Add(myRotateTransform);

                // Associate the transforms to the button.
                btngrph.RenderTransform = myTransformGroup;

                // Create a StackPanel which will contain the Button.

                ////////////////

                imgctrl.MouseLeftButtonUp += new MouseButtonEventHandler(SelectObject);
                btngrph.MouseLeftButtonUp += new MouseButtonEventHandler(SelectObject);
                btngrph.KeyDown += new KeyEventHandler(MyInkCanvas_KeyDown);
                btngrph.GotFocus += new RoutedEventHandler(btngrph_GotFocus);
                btngrph.Focus();
                Canvas.SetZIndex(btngrph, 4);


            }
        }
        /////////latika // end 

    }
}

