﻿using DigiPhoto.Common;
using DigiPhoto.IMIX.Business;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace DigiPhoto
{
    /// <summary>
    /// Interaction logic for CtrlOfflineClosingForm.xaml
    /// </summary>
    public partial class CtrlOfflineClosingForm : UserControl
    {
        public CtrlOfflineClosingForm()
        {
            InitializeComponent();
            Visibility = Visibility.Hidden;
        }        
        /// <summary>
        /// The _parent
        /// </summary>
        private UIElement _parent;
        /// <summary>
        /// The _hide request
        /// </summary>
        private bool _hideRequest = false;
        /// <summary>
        /// The _result
        /// </summary>
        private string _result;
        //private bool _OpenNext;
        /// <summary>
        /// The controlon
        /// </summary>
        TextBox controlon;
        /// <summary>
        /// Sets the parent.
        /// </summary>
        /// <param name="parent">The parent.</param>
        public void SetParent(UIElement parent)
        {
            _parent = (UIElement)parent;

        }
        public string encData { get; set; }
        public string filename { get; set; }
       
        public string ShowHandlerDialog(string message)
        {
           
            Visibility = Visibility.Visible;           
           
            //_parent.IsEnabled = false;


            _hideRequest = false;
            while (!_hideRequest)
            {
                // HACK: Stop the thread if the application is about to close
                if (this.Dispatcher.HasShutdownStarted ||
                    this.Dispatcher.HasShutdownFinished)
                {
                    break;
                }
                // HACK: Simulate "DoEvents"
                this.Dispatcher.Invoke(
                    DispatcherPriority.Background,
                    new ThreadStart(delegate { }));
                Thread.Sleep(20);
            }

            return _result;
        }

        /// <summary>
        /// Hides the handler dialog.
        /// </summary>
        private void HideHandlerDialog()
        {
            //  _result = null;
            _hideRequest = true;
            Visibility = Visibility.Collapsed;
            _parent.IsEnabled = true;
        }

      
       
        private void btnShowhdeForm_Click(object sender, RoutedEventArgs e)
        {
            _result = string.Empty;    
            HideHandlerDialog();
        }

        Microsoft.Win32.SaveFileDialog dlg = new Microsoft.Win32.SaveFileDialog();
        private void btnDownload_Click(object sender, RoutedEventArgs e)
        {
            try
            {

                dlg.FileName = filename;
                dlg.DefaultExt = ".xml";
                dlg.Filter = "Xml documents (.xml)|*.xml";

                if (IsEnabledExportToAnyDrive() == false)
                {
                    dlg.FileOk += dlg_FileOk;
                    DriveInfo drives = DriveInfo.GetDrives().Where(drive => drive.DriveType == DriveType.Removable && drive.IsReady == true).FirstOrDefault();
                    if (drives != null)
                    {
                        dlg.InitialDirectory = drives.Name;
                    }
                    else
                    {
                        MessageBox.Show("No removable device found.");
                        return;
                    }
                }

                Nullable<bool> result = dlg.ShowDialog();
                if (result == true)
                {
                    string filename2 = dlg.FileName;

                    //List<SageInfoClosing> lst = new List<SageInfoClosing>();
                    //lst.Add(_sageInfoClose);
                    //string dataXML = string.Empty;
                    //dataXML = serialize.SerializeObject<List<SageInfoClosing>>(lst);
                    //string strenc = CryptorEngine.Encrypt(dataXML, true);
                    File.WriteAllText(filename2, encData);
                }
            } 
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
            }
            finally
            {
                dlg.FileOk -= dlg_FileOk; 
            }
        }
        bool IsEnabledExportToAnyDrive()
        {
            bool returnValue = false;
            var item = new ConfigBusiness().GetConfigurationData(LoginUser.SubStoreId);
            if (item != null)
                returnValue = Convert.ToBoolean(item.IsExportReportToAnyDrive);
            return returnValue;
        }
        private void dlg_FileOk(object sender, System.ComponentModel.CancelEventArgs e)
        {
            FileInfo f = new FileInfo(dlg.FileName);
            string s = f.Directory.Root.ToString();
            if (s.Length > 0 && s.Substring(0, 1) == @"\")
            {
                MessageBox.Show("You can save to removable device only.");
                e.Cancel = true;
            }
            else
            {
                DriveInfo df = new DriveInfo(s);
                if (df.DriveType != DriveType.Removable)
                {
                    MessageBox.Show("You can save to removable device only.");
                    e.Cancel = true;
                }
            }
        }
    }
}
