﻿using DigiPhoto.Common;
using DigiPhoto.DataLayer;
using DigiPhoto.IMIX.Business;
using DigiPhoto.IMIX.Model;
using DigiPhoto.Interop;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace DigiPhoto
{
    /// <summary>
    /// Interaction logic for PanControl.xaml
    /// </summary>
    public partial class PanControl : UserControl
    {
        private UIElement _parent;
        private bool _result = false;
        public List<PanProperty> ppLst = new List<PanProperty>();
        int ppID = 101;
        int pandestfocuswidth = 300;
        int panDestFocusHeight = 250;
        public List<VideoTemplateInfo.VideoSlot> slotList;
        public int videoExpLen = 0;
        int EditedPanID = 0;
        bool IsPanEdit = false;
        public PanControl()
        {
            InitializeComponent();
            Visibility = Visibility.Hidden;
            int configWeight = ConfigManager.IMIXConfigurations.ContainsKey((int)ConfigParams.ResizeWidth) ? Convert.ToInt32(ConfigManager.IMIXConfigurations[(int)ConfigParams.ResizeWidth]) : 720;
            int configHeight = ConfigManager.IMIXConfigurations.ContainsKey((int)ConfigParams.ResizeHeight) ? Convert.ToInt32(ConfigManager.IMIXConfigurations[(int)ConfigParams.ResizeHeight]) : 576;
            edPanSourceLeft.Text = "0";
            edPanSourceTop.Text = "0";
            edPanSourceWidth.Text = configWeight.ToString();
            edPanSourceHeight.Text = configHeight.ToString();
            edPanDestLeft.Text = pandestfocuswidth.ToString();
            edPanDestTop.Text = "0";
            edPanDestWidth.Text = (configWeight - pandestfocuswidth).ToString();
            edPanDestHeight.Text = (configHeight - panDestFocusHeight).ToString();
        }
        public void PanControlListAutoFill(List<PanProperty> ppLstMain)
        {
            ppLst = ppLstMain;
            DGManagePan.ItemsSource = ppLst;
            DGManagePan.Items.Refresh();
        }
        public void SetParent(UIElement parent)
        {
            _parent = (UIElement)parent;
        }
        private void btnClose_Click(object sender, RoutedEventArgs e)
        {
            HideHandlerDialog();
            OnExecuteMethod();
        }
        private void HideHandlerDialog()
        {
            Visibility = Visibility.Collapsed;
            _parent.IsEnabled = true;
        }
        public bool ShowPanHandlerDialog()
        {
            Visibility = Visibility.Visible;
            _parent.IsEnabled = false;
            txtPanStopTime.Text = videoExpLen.ToString();
            if (videoExpLen > 0)
            {
                RangeSliderPan.Maximum = videoExpLen;
            }
            if (slotList != null && slotList.Count > 0)
            {
                btnDefaultPan.Visibility = Visibility.Visible;
            }
            else
            {
                btnDefaultPan.Visibility = Visibility.Collapsed;
            }
            return _result;
        }

        public event EventHandler ExecuteMethod;
        protected virtual void OnExecuteMethod()
        {
            if (ExecuteMethod != null) ExecuteMethod(this, EventArgs.Empty);
        }


        //private void btnAdd_Click(object sender, RoutedEventArgs e)
        //{
        //    PanProperty pp = new PanProperty();
        //    int stopTime = 0;
        //    int startTime = 0;
        //    if (!string.IsNullOrEmpty(txtPanStartTime.Text))
        //    {
        //        startTime = Convert.ToInt32(txtPanStartTime.Text);
        //    }

        //    if (!string.IsNullOrEmpty(txtPanStopTime.Text))
        //    {
        //        stopTime = Convert.ToInt32(txtPanStopTime.Text);
        //    }

        //    if (cbPan.IsChecked == true)
        //    {
        //        if (CheckSlotValidations())
        //        {

        //            pp.PanID = ppID;
        //            pp.PanStartTime = Convert.ToInt32(txtPanStartTime.Text);
        //            pp.PanStopTime = Convert.ToInt32(txtPanStopTime.Text);
        //            pp.PanSourceLeft = Convert.ToInt32(edPanSourceLeft.Text);
        //            pp.PanSourceTop = Convert.ToInt32(edPanSourceTop.Text);
        //            pp.PanSourceWidth = Convert.ToInt32(edPanSourceWidth.Text);
        //            pp.PanSourceHeight = Convert.ToInt32(edPanSourceHeight.Text);
        //            pp.PanDestLeft = Convert.ToInt32(edPanDestLeft.Text);
        //            pp.PanDestTop = Convert.ToInt32(edPanDestTop.Text);
        //            pp.PanDestWidth = Convert.ToInt32(edPanDestWidth.Text);
        //            pp.PanDestHeight = Convert.ToInt32(edPanDestHeight.Text);
        //            ppID++;

        //            if ((ppLst.Where(o => o.PanStartTime == startTime).Count() > 0))
        //            {
        //                MessageBox.Show("There is already a pan effect in the given time frame!", "DigiPhoto", MessageBoxButton.OK, MessageBoxImage.Exclamation);
        //                return;
        //            }
        //            if (ppLst.Count == 0)
        //            {
        //                ppLst.Add(pp);
        //            }

        //            else
        //            {
        //                PanProperty vs = ppLst.OrderBy(o => o.PanStartTime).Where(o => o.PanStartTime <= startTime).LastOrDefault();
        //                PanProperty vs1 = ppLst.OrderBy(o => o.PanStartTime).ToList().FirstOrDefault();
        //                if (vs != null)
        //                {
        //                    if (startTime > vs.PanStartTime + (vs.PanStopTime - vs.PanStartTime))
        //                    {
        //                        ppLst.Add(pp);
        //                    }
        //                    else
        //                    {
        //                        MessageBox.Show("There is already a pan effect in the given time frame.", "DigiPhoto", MessageBoxButton.OK, MessageBoxImage.Exclamation);
        //                    }
        //                }
        //                else if (vs1 != null)
        //                {
        //                    if (startTime + stopTime <= vs1.PanStartTime)
        //                    {
        //                        ppLst.Add(pp);
        //                    }
        //                    else
        //                    {
        //                        MessageBox.Show("There is already a pan effect in the given time frame.", "DigiPhoto", MessageBoxButton.OK, MessageBoxImage.Exclamation);
        //                    }
        //                }
        //                else
        //                {
        //                    ppLst.Add(pp);
        //                }
        //            }

        //            //ppLst.Add(pp);
        //            DGManagePan.ItemsSource = ppLst.OrderBy(o => o.PanStartTime);
        //            DGManagePan.Items.Refresh();
        //        }
        //    }
        //    else
        //    {
        //        MessageBox.Show("Please enable the pan effect.", "DigiPhoto", MessageBoxButton.OK, MessageBoxImage.Information);
        //    }
        //}
        private void btnAdd_Click(object sender, RoutedEventArgs e)
        {
            PanProperty pp = new PanProperty();
            int stopTime = 0;
            int startTime = 0;
            if (!string.IsNullOrEmpty(txtPanStartTime.Text))
            {
                startTime = Convert.ToInt32(txtPanStartTime.Text);
            }

            if (!string.IsNullOrEmpty(txtPanStopTime.Text))
            {
                stopTime = Convert.ToInt32(txtPanStopTime.Text);
            }

            if (cbPan.IsChecked == true)
            {
                if (IsPanEdit)
                {
                    IsPanEdit = false;
                    MessageBoxResult response = MessageBox.Show("Would you like to save changes?", "Digiphoto", MessageBoxButton.YesNo, MessageBoxImage.Question);
                    if (response == MessageBoxResult.Yes)
                    {
                        pp = ppLst.Where(o => o.PanID == EditedPanID).FirstOrDefault();
                        ppLst.Remove(pp);
                        if (CheckSlotValidations(ppLst, startTime, stopTime))
                        {
                            pp.PanStartTime = Convert.ToInt32(txtPanStartTime.Text);
                            pp.PanStopTime = Convert.ToInt32(txtPanStopTime.Text);
                            pp.PanSourceLeft = Convert.ToInt32(edPanSourceLeft.Text);
                            pp.PanSourceTop = Convert.ToInt32(edPanSourceTop.Text);
                            pp.PanSourceWidth = Convert.ToInt32(edPanSourceWidth.Text);
                            pp.PanSourceHeight = Convert.ToInt32(edPanSourceHeight.Text);
                            pp.PanDestLeft = Convert.ToInt32(edPanDestLeft.Text);
                            pp.PanDestTop = Convert.ToInt32(edPanDestTop.Text);
                            pp.PanDestWidth = Convert.ToInt32(edPanDestWidth.Text);
                            pp.PanDestHeight = Convert.ToInt32(edPanDestHeight.Text);
                            ppLst.Add(pp);
                        }
                        else
                            ppLst.Add(pp);
                    }
                }
                else
                {
                    if (CheckSlotValidations(ppLst, startTime, stopTime))
                    {
                        pp.PanID = ppID;
                        pp.PanStartTime = Convert.ToInt32(txtPanStartTime.Text);
                        pp.PanStopTime = Convert.ToInt32(txtPanStopTime.Text);
                        pp.PanSourceLeft = Convert.ToInt32(edPanSourceLeft.Text);
                        pp.PanSourceTop = Convert.ToInt32(edPanSourceTop.Text);
                        pp.PanSourceWidth = Convert.ToInt32(edPanSourceWidth.Text);
                        pp.PanSourceHeight = Convert.ToInt32(edPanSourceHeight.Text);
                        pp.PanDestLeft = Convert.ToInt32(edPanDestLeft.Text);
                        pp.PanDestTop = Convert.ToInt32(edPanDestTop.Text);
                        pp.PanDestWidth = Convert.ToInt32(edPanDestWidth.Text);
                        pp.PanDestHeight = Convert.ToInt32(edPanDestHeight.Text);
                        ppLst.Add(pp);
                        ppID++;
                    }
                    //ppLst.Add(pp);
                }
                DGManagePan.ItemsSource = ppLst.OrderBy(o => o.PanStartTime);
                DGManagePan.Items.Refresh();
            }
            else
            {
                MessageBox.Show("Please enable the pan effect.", "DigiPhoto", MessageBoxButton.OK, MessageBoxImage.Information);
            }
            ResetToDefault();
        }
        private void btnDeletePanSlots_Click(object sender, RoutedEventArgs e)
        {
            Button btnSender = (Button)sender;
            long SlotId = long.Parse(btnSender.Tag.ToString());
            MessageBoxResult response = MessageBox.Show("Do you want to delete record?", "Digiphoto", MessageBoxButton.YesNo, MessageBoxImage.Question);
            if (response == MessageBoxResult.Yes)
            {
                ppLst.RemoveAll(o => o.PanID == SlotId);
                // MessageBox.Show("Item deleted Successfully", "DigiPhoto", MessageBoxButton.OK,MessageBoxImage.Information);
                DGManagePan.ItemsSource = ppLst;
                DGManagePan.Items.Refresh();
            }
        }
        private void txtnumber_TextChanged(object sender, TextChangedEventArgs e)
        {
            string strtemp = string.Empty;
            if (string.IsNullOrEmpty(((TextBox)sender).Text))
                strtemp = "";
            else
            {
                int num = 0;
                bool success = int.TryParse(((TextBox)sender).Text, out num);
                if (success & num >= 0)
                {
                    ((TextBox)sender).Text.Trim();
                    strtemp = ((TextBox)sender).Text;
                }
                else
                {
                    ((TextBox)sender).Text = strtemp;
                    ((TextBox)sender).SelectionStart = ((TextBox)sender).Text.Length;
                }
            }
        }
        private bool CheckSlotValidations(List<PanProperty> slotList, int startTime, int stopTime)
        {
            if (string.IsNullOrEmpty(txtPanStartTime.Text.Trim()) || string.IsNullOrEmpty(txtPanStopTime.Text.Trim()))
            {
                MessageBox.Show("Enter value for start time or end time.", "DigiPhoto", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                return false;
            }
            else if (Convert.ToInt32(txtPanStartTime.Text.Trim()) == Convert.ToInt32(txtPanStopTime.Text.Trim()))
            {
                MessageBox.Show("Start time and stop time can not be same.", "DigiPhoto", MessageBoxButton.OK, MessageBoxImage.Information);
                return false;
            }

            else
            {
                PanProperty firstItem = slotList.OrderBy(o => o.PanStartTime).FirstOrDefault();
                PanProperty LastItem = slotList.OrderBy(o => o.PanStartTime).LastOrDefault();
                if (firstItem != null)
                {
                    if (stopTime <= firstItem.PanStartTime)
                    {
                        return true;
                    }
                    else if (startTime >= LastItem.PanStopTime)
                    {
                        return true;
                    }
                    else
                    {
                        List<PanProperty> templst = slotList.OrderBy(o => o.PanStartTime).ToList();
                        PanProperty item = templst.Where(o => o.PanStopTime <= startTime).LastOrDefault();
                        PanProperty item2 = templst[templst.IndexOf(item) + 1];
                        if (item2 != null)
                        {
                            if (item2.PanStartTime >= stopTime)
                            {
                                return true;
                            }
                            else
                            {
                                MessageBox.Show("Effect is already applied in the given time frame.", "DigiPhoto", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                                return false;
                            }
                        }
                        else
                        {
                            MessageBox.Show("Effect is already applied in the given time frame.", "DigiPhoto", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                            return false;
                        }
                    }
                }
                else
                {
                    return true;
                }
            }
        }

        private void UnsubscribeEvents()
        {
            txtPanStopTime.TextChanged -= new TextChangedEventHandler(txtnumber_TextChanged);
            txtPanStartTime.TextChanged -= new TextChangedEventHandler(txtnumber_TextChanged);

            edPanSourceLeft.TextChanged -= new TextChangedEventHandler(txtnumber_TextChanged);
            edPanSourceTop.TextChanged -= new TextChangedEventHandler(txtnumber_TextChanged);
            edPanSourceHeight.TextChanged -= new TextChangedEventHandler(txtnumber_TextChanged);
            edPanSourceWidth.TextChanged -= new TextChangedEventHandler(txtnumber_TextChanged);

            edPanDestLeft.TextChanged -= new TextChangedEventHandler(txtnumber_TextChanged);
            edPanDestTop.TextChanged -= new TextChangedEventHandler(txtnumber_TextChanged);
            edPanDestHeight.TextChanged -= new TextChangedEventHandler(txtnumber_TextChanged);
            edPanDestWidth.TextChanged -= new TextChangedEventHandler(txtnumber_TextChanged);

            btnAdd.Click -= new RoutedEventHandler(btnAdd_Click);
            btnClose.Click -= new RoutedEventHandler(btnClose_Click);
            btnDefaultPan.Click -= new RoutedEventHandler(btnDefaultPan_Click);
            btnClearList.Click -= new RoutedEventHandler(btnClearList_Click);
            if (ppLst.Count > 0)
                ppLst.Clear();
        }

        private void UserControl_Unloaded(object sender, RoutedEventArgs e)
        {
            UnsubscribeEvents();
        }

        private void btnClearList_Click(object sender, RoutedEventArgs e)
        {
            if (ppLst != null)
            {
                ppLst.Clear();
                DGManagePan.ItemsSource = ppLst;
                DGManagePan.Items.Refresh();
            }
        }

        private void btnDefaultPan_Click(object sender, RoutedEventArgs e)
        {
            if (cbPan.IsChecked == true)
            {
                if (slotList != null && slotList.Count > 0)
                {
                    ppLst.Clear();
                    ppID = 101;
                    for (int a = 0; a < slotList.Count; a++)
                    {
                        PanProperty pp = new PanProperty();
                        int startTime = Convert.ToInt32(slotList[a].FrameTimeIn);
                        int stopTime = Convert.ToInt32(slotList[a].PhotoDisplayTime) + Convert.ToInt32(slotList[a].FrameTimeIn);
                        pp.PanID = ppID;
                        pp.PanStartTime = startTime;
                        pp.PanStopTime = stopTime;
                        pp.PanSourceLeft = Convert.ToInt32(edPanSourceLeft.Text);
                        pp.PanSourceTop = Convert.ToInt32(edPanSourceTop.Text);
                        pp.PanSourceWidth = Convert.ToInt32(edPanSourceWidth.Text);
                        pp.PanSourceHeight = Convert.ToInt32(edPanSourceHeight.Text);
                        pp.PanDestLeft = Convert.ToInt32(edPanDestLeft.Text);
                        pp.PanDestTop = Convert.ToInt32(edPanDestTop.Text);
                        pp.PanDestWidth = Convert.ToInt32(edPanDestWidth.Text);
                        pp.PanDestHeight = Convert.ToInt32(edPanDestHeight.Text);
                        ppID++;
                        ppLst.Add(pp);
                    }
                    DGManagePan.ItemsSource = ppLst;
                    DGManagePan.Items.Refresh();
                }
                else
                {
                    MessageBox.Show("No template selected.", "DigiPhoto", MessageBoxButton.OK, MessageBoxImage.Information);
                }
            }
            else
            {
                MessageBox.Show("Please enable the pan effect.", "DigiPhoto", MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }

        private void btnEditPanSlots_Click(object sender, RoutedEventArgs e)
        {
            Button btnSender = (Button)sender;
            EditedPanID = Convert.ToInt32(btnSender.Tag.ToString());
            PanProperty pan = ppLst.Where(o => o.PanID == EditedPanID).FirstOrDefault();
            txtPanStartTime.Text = pan.PanStartTime.ToString();
            txtPanStopTime.Text = pan.PanStopTime.ToString();
            edPanSourceLeft.Text = pan.PanSourceLeft.ToString();
            edPanSourceTop.Text = pan.PanSourceTop.ToString();
            edPanSourceHeight.Text = pan.PanSourceHeight.ToString();
            edPanSourceWidth.Text = pan.PanSourceWidth.ToString();
            edPanDestHeight.Text = pan.PanDestHeight.ToString();
            edPanDestLeft.Text = pan.PanDestLeft.ToString();
            edPanDestWidth.Text = pan.PanDestWidth.ToString();
            edPanDestTop.Text = pan.PanDestTop.ToString();
            IsPanEdit = true;

        }
        private void ResetToDefault()
        {
            edPanSourceLeft.Text = "0";
            edPanSourceTop.Text = "0";
            edPanSourceHeight.Text = "480";
            edPanSourceWidth.Text = "640";
            edPanDestHeight.Text = "240";
            edPanDestLeft.Text = "0";
            edPanDestWidth.Text = "320";
            edPanDestTop.Text = "0";
        }


    }

}
