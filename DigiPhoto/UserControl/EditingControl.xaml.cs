﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using DigiPhoto.IMIX.Model;
using DigiPhoto.Shader;
using DigiPhoto.Common;
using System.Collections;
using System.Collections.ObjectModel;
using System.IO;
using FrameworkHelper;
using DigiPhoto.IMIX.Business;
using DigiAuditLogger;
using DigiPhoto.DataLayer;
using System.Windows.Ink;
using System.Text.RegularExpressions;
using System.Xml;
using DigiPhoto.Cache.Repository;
using DigiPhoto.Cache.DataCache;
using DigiPhoto.Cache.MasterDataCache;
using System.Windows.Threading;
using System.Threading;

namespace DigiPhoto
{
    /// <summary>
    /// Interaction logic for EditingControl.xaml
    /// </summary>
    public partial class EditingControl : UserControl
    {
        #region Constructor
        public EditingControl()
        {
            InitializeComponent();
            SerialLog = new Stack();
            EffectLog = new Stack();
            EnabledAllButtons();

            DragCanvas.SetCanBeDragged(Opacitymsk, false);
            DragCanvas.SetCanBeDragged(mainImage, false);
            LoadFeatures();

            mainImage.MouseLeftButtonUp += mainImage_MouseLeftButtonUp;

            IsEffectChange = false;
            IsGraphicsChange = false;
            squre.Cursor = Cursors.None;

            MyInkCanvas.SnapsToDevicePixels = true;
            MyInkCanvas.SetValue(RenderOptions.EdgeModeProperty, EdgeMode.Aliased);

            mainImage.SnapsToDevicePixels = true;
            mainImage.SetValue(RenderOptions.EdgeModeProperty, EdgeMode.Aliased);
            mainImage.OverridesDefaultStyle = true;

            GrdGreenScreenDefault.SetValue(RenderOptions.EdgeModeProperty, EdgeMode.Aliased);

            Canvas.SetZIndex(imageundoGrid, 0);
            Canvas.SetZIndex(canbackgroundParent, 4);
            Canvas.SetZIndex(Opacitymsk, 2);
            Canvas.SetZIndex(frm, 6);

            canbackgroundParent.IsHitTestVisible = false;
            canbackground.IsHitTestVisible = false;
            FillProductCombo();

            GetBackImageSettings();
            IsImageDirtyState = false;

            lstStrip.ItemContainerGenerator.StatusChanged += ItemContainerGenerator_StatusChanged;
        }
        #endregion

        #region Declaration
        #region Shader Effects Class Initializations
        /// <summary>
        /// The _brighteff
        /// </summary>
        public ContrastAdjustEffect _brighteff = new ContrastAdjustEffect();

        /// <summary>
        /// The _colorfiltereff
        /// </summary>
        MonochromeEffect _colorfiltereff = new MonochromeEffect();

        /// <summary>
        /// The _shifthueeff
        /// </summary>
        ShiftHueEffect _shifthueeff = new ShiftHueEffect();

        ShEffect _sharpeff = new ShEffect();

        /// <summary>
        /// The _under
        /// </summary>
        MultiChannelContrastEffect _under = new MultiChannelContrastEffect();

        #endregion
        #region Private Variables
        bool IsLoad = false;
        bool IsMoreImages = true;
        int NoOfDisplayItem = 4;
        int lastVisibleIndex = 0;
        //Checks If default background image enabled on edit
        bool IsDefaultBackgroundEnabled = false;
        //Background image path 
        string DefaultBackgroundImagePath = string.Empty;
        string BackgroundDBValue = string.Empty;
        string BackgroundSelectedValue = string.Empty;
        bool isPrintButtonsVisible = false;
        string borderName;
        private static MainWindow instance;
        private Dictionary<string, decimal?> chromaDefaultInfo = new Dictionary<string, decimal?>();
        private static string ChromaColorDefault = string.Empty;

        /// <summary>
        /// The index
        /// </summary>
        int index = 0;
        double canvasTop;
        double canvasLeft;
        bool isGreenImage = false;
        /// <summary>
        /// Variable to check whether it is coming On load i.e. image is selected for editing 
        /// <summary>
        bool IsComingOnLoad = false;
        /// <summary>
        /// The hueshift
        /// </summary>
        double hueshift = 0;
        /// <summary>
        /// The sharpen
        /// </summary>
        double sharpen = 0;
        string ColorCode = string.Empty;
        /// <summary>
        /// The currenthueshift
        /// </summary>
        double currenthueshift = 0;
        /// <summary>
        /// The currentsharpen
        /// </summary>
        double currentsharpen = 0;
        /// <summary>
        /// The cont
        /// </summary>
        double cont = 1;
        /// <summary>
        /// The bright
        /// </summary>
        double bright = 0;
        /// <summary>
        /// The currentbrightness
        /// </summary>
        double currentbrightness = 0;
        /// <summary>
        /// The currentcontrast
        /// </summary>
        double currentcontrast = 0;
        /// <summary>
        /// The LST grid effects
        /// </summary>
        List<string> LstGridEffects = new List<string>();
        /// <summary>
        /// The color
        /// </summary>
        System.Windows.Media.Color color;
        /// <summary>
        /// The currentcolor
        /// </summary>
        System.Windows.Media.Color currentcolor;

        string MktImgPath = string.Empty;
        int mktImgTime = 0;

        /// <summary>
        /// The _CLP
        /// </summary>
        CroppingAdorner _clp;
        /// <summary>
        /// The _fel current
        /// </summary>
        FrameworkElement _felCur = null;
        /// <summary>
        /// The _BR original
        /// </summary>
        System.Windows.Media.Brush _brOriginal;
        //for red eye
        /// <summary>
        /// The _x
        /// </summary>
        int _x, _y;
        // public WriteableBitmap _CurrentBitMap;
        /// <summary>
        /// The set redeye
        /// </summary>
        bool SetRedeye = false;
        /// <summary>
        /// The executable
        /// </summary>
        double x = 0;
        /// <summary>
        /// The asynchronous
        /// </summary>
        double y = 0;

        /// <summary>
        /// The serial log
        /// </summary>
        Stack SerialLog;
        /// <summary>
        /// The effect log
        /// </summary>
        Stack EffectLog;
        /// <summary>
        /// The rotateangle
        /// </summary>
        int rotateangle = 0;
        /// <summary>
        /// The flip mode
        /// </summary>
        private int FlipMode;
        /// <summary>
        /// The flip mode asynchronous
        /// </summary>
        private int FlipModeY;
        /// <summary>
        /// The is graphics change
        /// </summary>
        private bool IsGraphicsChange = false;
        /// <summary>
        /// The is effect change
        /// </summary>
        private bool IsEffectChange = false;
        /// <summary>
        /// The is moderate
        /// </summary>
        private bool IsModerate = false;

        private bool IsChromaChanged = false;
        bool ImageClick = true;
        bool _hideRequest = false;
        string _result = "done";
        #endregion
        #region Public Variables
        public SearchDetailInfo searchDetails = new SearchDetailInfo();
        //Start Have Original config data
        public double dbContr = 0.0;
        public double dbBrit = 0.0;
        //End Have Original config data
        public bool IsSceneApply = false;
        public string borderfilename = string.Empty;
        public string selectedbordername = string.Empty;
        public long MaxPhotoIdCriteria = 0;
        public long MinPhotoIdCriteria = 0;
        public int NewReord = 0;

        /// <summary>
        /// The is goupped
        /// </summary>
        public string IsGoupped;

        [System.Runtime.InteropServices.DllImport("gdi32.dll")]
        public static extern bool DeleteObject(IntPtr hObject);
        public bool isSingleScreenPreview = false;
        public string specproductType = string.Empty;
        public bool IsRestoreRideClick = false;
        #endregion
        #endregion

        #region Properties
        public string PhotoName { get; set; }
        public string ImageEffect { get; set; }
        public string SpecImageEffect { get; set; }
        public string SpecLayeringHorizontal { get; set; }
        public string SpecLayeringVertical { get; set; }
        public string SpecLayeringUpdated { get; set; }
        public string GraphicEffect { get; set; }
        public string DateFolder { get; set; }
        public string HotFolderPath { get; set; }
        public string CropFolderPath { get; set; }
        public List<MyImageClass> lstMyImageClass { get; set; }
        public bool IsImageDirtyState { get; set; }
        public int selectedIndex { get; set; }
        private UIElement _parent;
        public void SetParent(UIElement parent)
        {
            _parent = (UIElement)parent;
        }

        #endregion

        #region Public Common Methods
        public void ShowStripImages()
        {
            BindStrip();
            btnThumbnails_Click(new object(), new RoutedEventArgs());
            IsLoad = true;
            IsMoreImages = true;
        }

        /// <summary>
        /// Shows the handler dialog.
        /// </summary>
        /// <returns></returns>
        public string ShowHandlerDialog(bool IsCrop, bool IsGreen, string Layering)
        {
#if DEBUG
            var watch = FrameworkHelper.CommonUtility.Watch();
            watch.Start();
#endif
            Visibility = Visibility.Visible;
            Onload(false, IsGreen, Layering, false);
            ShowStripImages();
            if (IsCrop)
            {
                ImageClick = true;
                this.Onload(IsCrop, IsGreen, Layering, false);
                this.lstStrip.Visibility = System.Windows.Visibility.Visible;
                btnThumbnails_Click(new object(), new RoutedEventArgs());
            }
            _hideRequest = false;
            while (!_hideRequest)
            {
                // HACK: Stop the thread if the application is about to close
                if (this.Dispatcher.HasShutdownStarted ||
                    this.Dispatcher.HasShutdownFinished)
                {
                    break;
                }
                // HACK: Simulate "DoEvents"
                this.Dispatcher.Invoke(
                    DispatcherPriority.Background,
                    new ThreadStart(delegate { }));
                Thread.Sleep(20);
            }

#if DEBUG
            if (watch != null)
                FrameworkHelper.CommonUtility.WatchStop(this.GetType().FullName + "." + System.Reflection.MethodBase.GetCurrentMethod().Name + "|", watch);
#endif
            return _result;
        }
        public void SetEffect()
        {
            GrdBrightness.Effect = _brighteff;
        }


        #endregion

        #region Private Common Methods
        private void BindStrip()
        {
            lstStrip.ItemsSource = null;
            lstStrip.ItemsSource = lstMyImageClass.OrderBy(o => o.PhotoNumber);
            lstStrip.Items.Refresh();
        }
        /// <summary>
        /// Enables the side button.
        /// </summary>
        private void EnableSideButton()
        {
            btnSaveBack.IsEnabled = true;
        }

        /// <summary>
        /// Disables the side button.
        /// </summary>
        private void DisableSideButton()
        {
            btnSaveBack.IsEnabled = false;
        }

        /// <summary>
        /// Zomouts the specified orignal.
        /// </summary>
        /// <param name="orignal">if set to <c>true</c> [orignal].</param>
        private void Zomout(bool orignal)
        {
#if DEBUG
            var watch = FrameworkHelper.CommonUtility.Watch();
            watch.Start();
#endif
            try
            {
                if (widthimg.Source == null)
                    return;
                double currentwidth = widthimg.Source.Width;
                double currentheight = widthimg.Source.Height;

                double ratiowidth = currentwidth / 600;
                double ratioheight = currentheight / 600;
                ratiowidth = 100 / ratiowidth / 100;
                ratioheight = 100 / ratioheight / 100;

                if (frm.Children.Count == 1)
                {
                    currentwidth = forWdht.Width;
                    currentheight = forWdht.Height;
                    ratiowidth = currentwidth / 600;
                    ratioheight = currentheight / 600;
                    ratiowidth = 100 / ratiowidth / 100;
                    ratioheight = 100 / ratioheight / 100;
                }

                ScaleTransform zoomTransform = new ScaleTransform(); ;
                TransformGroup transformGroup = new TransformGroup();
                if (currentheight > currentwidth)
                {
                    zoomTransform.ScaleX = ratioheight - .01;
                    zoomTransform.ScaleY = ratioheight - .01;
                }
                else
                {
                    zoomTransform.ScaleX = ratiowidth - .01;
                    zoomTransform.ScaleY = ratiowidth - .01;
                }

                zoomTransform.CenterX = forWdht.ActualWidth / 2;
                zoomTransform.CenterY = forWdht.ActualHeight / 2;
                transformGroup.Children.Add(zoomTransform);
                GrdZomout.LayoutTransform = transformGroup;
                grdZoomCanvas.LayoutTransform = transformGroup;

            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
            finally
            {
                // MemoryManagement.FlushMemory();
            }
#if DEBUG
            if (watch != null)
                FrameworkHelper.CommonUtility.WatchStop(this.GetType().FullName + "." + System.Reflection.MethodBase.GetCurrentMethod().Name + "|", watch);
#endif
        }

        /// <summary>
        /// Disables all buttons.
        /// </summary>
        private void disableAllButtons()
        {
            btnrotate.IsEnabled = false;
            btnantirotate.IsEnabled = false;
            btnflip.IsEnabled = false;
            btnCrop.IsEnabled = false;
            //btnundo.IsEnabled = false;
            btnAddgraphics.IsEnabled = false;
            btnColorEffects.IsEnabled = false;
            btnColorEffectsfilters.IsEnabled = false;
            //RedEye.IsEnabled = false;
            btnrestoremainimg.IsEnabled = false;
        }

        /// <summary>
        /// Enableds all buttons.
        /// </summary>
        private void EnabledAllButtons()
        {
            btnrotate.IsEnabled = true;
            btnantirotate.IsEnabled = true;
            btnflip.IsEnabled = true;
            btnCrop.IsEnabled = true;
            //btnundo.IsEnabled = true;
            btnAddgraphics.IsEnabled = true;
            btnColorEffects.IsEnabled = true;
            btnColorEffectsfilters.IsEnabled = true;
            //RedEye.IsEnabled = true;
            btnrestoremainimg.IsEnabled = true;
        }
        private void EnableButtonForLayering()
        {
            btnrotate.IsEnabled = true;
            btnantirotate.IsEnabled = true;
            btnflip.IsEnabled = true;
            btnCrop.IsEnabled = true;
        }
        private void DisableButtonForLayering()
        {
            btnrotate.IsEnabled = false;
            btnantirotate.IsEnabled = false;
            btnflip.IsEnabled = false;
            btnCrop.IsEnabled = false;
        }

        /// <summary>
        /// Flips the specified mode.
        /// </summary>
        /// <param name="mode">The mode.</param>
        private void Flip(int mode)
        {
#if DEBUG
            var watch = FrameworkHelper.CommonUtility.Watch();
            watch.Start();
#endif
            if (mode != -1)
            {
                rotateangle = mode;
            }

            int previousvalue = FlipMode;
            try
            {
                if (FlipModeY == 0)
                {
                    if (FlipMode == 0)
                    {
                        zoomTransform.CenterX = mainImage.ActualWidth / 2;
                        zoomTransform.CenterY = mainImage.ActualHeight / 2;
                        zoomTransform.ScaleX = -1;
                        FlipMode = 1;
                        //CurrentImage.RotateFlip(RotateFlipType.RotateNoneFlipY);
                    }
                    else
                    {
                        zoomTransform.CenterX = mainImage.ActualWidth / 2;
                        zoomTransform.CenterY = mainImage.ActualHeight / 2;
                        zoomTransform.ScaleX = 1;
                        FlipMode = 0;
                        //CurrentImage.RotateFlip(RotateFlipType.RotateNoneFlipY);
                    }
                }
                else
                {
                    if (FlipMode == 0)
                    {
                        zoomTransform.ScaleY = -1;
                        FlipMode = 1;
                        // CurrentImage.RotateFlip(RotateFlipType.RotateNoneFlipY);
                    }
                    else
                    {
                        zoomTransform.ScaleY = 1;
                        FlipMode = 0;
                        //CurrentImage.RotateFlip(RotateFlipType.RotateNoneFlipY);
                    }
                }


                transformGroup = new TransformGroup();
                transformGroup.Children.Add(zoomTransform);
                transformGroup.Children.Add(translateTransform);
                transformGroup.Children.Add(rotateTransform);
                GrdFlip.LayoutTransform = zoomTransform;

                //mainImage.RenderTransform = flipTrans;
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);

            }
            finally
            {
                LogObject obj = new LogObject();
                obj.value = previousvalue;
                obj.opName = "flip";
                //obj.LogImage = null;
                LogOperation(obj);
                // SaveXml("_centerx", (mainImage.ActualWidth / 2).ToString(), false);
                // SaveXml("_centery", (mainImage.ActualHeight / 2).ToString(), false);
                // SaveXml("flipMode", FlipMode.ToString(), false);
                // SaveXml("flipModeY", FlipModeY.ToString(), false);
            }

            if (FlipMode != 0 || FlipModeY != 0)
            {
                IsEffectChange = true;
                VisualStateManager.GoToState(btnflip, "Checked", true);
                //RedEye.IsEnabled = false;
            }
            else
            {
                IsGraphicsChange = false;
                VisualStateManager.GoToState(btnflip, "Unchecked", true);
                //RedEye.IsEnabled = true;
            }
#if DEBUG
            if (watch != null)
                FrameworkHelper.CommonUtility.WatchStop(this.GetType().FullName + "." + System.Reflection.MethodBase.GetCurrentMethod().Name + "|", watch);
#endif
        }

        /// <summary>
        /// Rotates the specified angle.
        /// </summary>
        /// <param name="angle">The angle.</param>
        private void Rotate(int angle)
        {
#if DEBUG
            var watch = FrameworkHelper.CommonUtility.Watch();
            watch.Start();
#endif
            GrdRotate.RenderTransformOrigin = new System.Windows.Point(0.5, 0.5);
            RotateTransform RoatateTrans = new RotateTransform();
            int previousvalue = rotateangle;
            if (angle != -1)
            {
                RoatateTrans.Angle = angle;
                GrdRotate.LayoutTransform = RoatateTrans;
                rotateangle = 360 - angle;
                if (rotateangle == 360)
                    rotateangle = 0;
            }
            else
            {
                try
                {
                    switch (rotateangle)
                    {
                        case 0:
                            {
                                RoatateTrans.Angle = 90;
                                rotateangle = 90;


                                //CurrentImage.RotateFlip(RotateFlipType.Rotate90FlipNone);
                                //FlipModeY = 1;
                                break;
                            }
                        case 90:
                            {
                                RoatateTrans.Angle = 180;
                                rotateangle = 180;


                                //CurrentImage.RotateFlip(RotateFlipType.Rotate180FlipNone);
                                //FlipModeY = 0;
                                break;
                            }
                        case 180:
                            {
                                RoatateTrans.Angle = 270;
                                rotateangle = 270;
                                //CurrentImage.RotateFlip(RotateFlipType.Rotate270FlipNone);
                                //FlipModeY = 1;
                                break;
                            }
                        case 270:
                            {
                                RoatateTrans.Angle = 360;
                                rotateangle = 0;
                                //FlipModeY = 0;
                                break;
                            }

                    }
                    GrdRotate.LayoutTransform = RoatateTrans;

                    //dragCanvas.InvalidateArrange();
                    //dragCanvas.InvalidateMeasure();
                    //dragCanvas.InvalidateVisual();


                    //canbackground.InvalidateArrange();
                    //canbackground.InvalidateMeasure();
                    //canbackground.InvalidateVisual();

                    //mainImage.LayoutTransform = RoatateTrans;
                    //forWdht.LayoutTransform = RoatateTrans;

                }
                catch (Exception ex)
                {
                    string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                    ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                }
                finally
                {

                    LogObject obj = new LogObject();
                    obj.value = previousvalue;
                    obj.opName = "rotate";
                    //obj.LogImage = null;
                    LogOperation(obj);

                    //SaveXml("rotatewidth", frwidth.ToString(), false);
                    //SaveXml("rotateheight", frheight.ToString(), false);
                    //SaveXml("rotate", rotateangle.ToString(), false);
                    if (rotateangle > 0)
                    {
                        IsEffectChange = true;
                        VisualStateManager.GoToState(btnrotate, "Checked", true);
                        VisualStateManager.GoToState(btnantirotate, "Checked", true);
                        //RedEye.IsEnabled = false;
                    }
                    else
                    {
                        VisualStateManager.GoToState(btnrotate, "Unchecked", true);
                        VisualStateManager.GoToState(btnantirotate, "Unchecked", true);
                        //RedEye.IsEnabled = true;
                    }
                }
            }

#if DEBUG
            if (watch != null)
                FrameworkHelper.CommonUtility.WatchStop(this.GetType().FullName + "." + System.Reflection.MethodBase.GetCurrentMethod().Name + "|", watch);
#endif
        }

        /// <summary>
        /// Antis the rotate.
        /// </summary>
        /// <param name="angle">The angle.</param>
        private void AntiRotate(int angle)
        {
#if DEBUG
            var watch = FrameworkHelper.CommonUtility.Watch();
            watch.Start();
#endif

            if (angle != -1)
            {
                rotateangle = angle;
            }

            GrdRotate.RenderTransformOrigin = new System.Windows.Point(0.5, 0.5);
            // mainImage.RenderTransformOrigin = new System.Windows.Point(0.5, 0.5);
            RotateTransform RoatateTrans = new RotateTransform();
            int previousvalue = rotateangle;
            try
            {
                switch (rotateangle)
                {
                    case 0:
                        {
                            RoatateTrans.Angle = 270;
                            rotateangle = 270;
                            //CurrentImage.RotateFlip(RotateFlipType.Rotate270FlipNone);
                            //FlipModeY = 1;
                            break;
                        }
                    case 90:
                        {
                            RoatateTrans.Angle = 0;
                            rotateangle = 0;
                            //CurrentImage.RotateFlip(RotateFlipType.Rotate180FlipNone);
                            //FlipModeY = 0;
                            break;
                        }
                    case 180:
                        {
                            RoatateTrans.Angle = 90;
                            rotateangle = 90;
                            //CurrentImage.RotateFlip(RotateFlipType.Rotate90FlipNone);
                            //FlipModeY = 1;
                            break;
                        }
                    case 270:
                        {
                            RoatateTrans.Angle = 180;
                            rotateangle = 180;
                            //CurrentImage.RotateFlip(RotateFlipType.Rotate180FlipNone);
                            //FlipModeY = 0;
                            break;
                        }

                }


                GrdRotate.LayoutTransform = RoatateTrans;



                //   GrdGreenScreenDefault3.LayoutTransform = RoatateTrans;
                //  mainImage.RenderTransform = RoatateTrans;
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
            finally
            {
                LogObject obj = new LogObject();
                obj.value = previousvalue;
                obj.opName = "antirotate";
                //obj.LogImage = null;
                LogOperation(obj);
                //SaveXml("rotatewidth", frwidth.ToString(), false);
                //SaveXml("rotateheight", frheight.ToString(), false);
                //SaveXml("rotate", rotateangle.ToString(), false);

                if (rotateangle > 0)
                {
                    IsEffectChange = true;
                    VisualStateManager.GoToState(btnrotate, "Checked", true);
                    VisualStateManager.GoToState(btnantirotate, "Checked", true);
                    //RedEye.IsEnabled = false;
                }
                else
                {
                    VisualStateManager.GoToState(btnrotate, "Unchecked", true);
                    VisualStateManager.GoToState(btnantirotate, "Unchecked", true);
                    //RedEye.IsEnabled = true;
                }
            }
#if DEBUG
            if (watch != null)
                FrameworkHelper.CommonUtility.WatchStop(this.GetType().FullName + "." + System.Reflection.MethodBase.GetCurrentMethod().Name + "|", watch);
#endif
        }

        /// <summary>
        /// Undoes this instance.
        /// </summary>
        private void Undo()
        {
            try
            {
                if (SerialLog.Count > 0)
                {
                    LogObject obj = (LogObject)SerialLog.Pop();

                    switch (obj.opName)
                    {
                        case "flip":
                            Flip((int)obj.value);
                            break;
                        case "rotate":
                            Rotate((int)obj.value);
                            break;
                        case "antirotate":
                            Rotate((int)obj.value);
                            break;
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }

        /// <summary>
        /// Undoes the effect.
        /// </summary>
        private void UndoEffect()
        {
#if DEBUG
            var watch = FrameworkHelper.CommonUtility.Watch();
            watch.Start();
#endif
            try
            {
                if (EffectLog.Count > 0)
                {
                    LogEffect obj = (LogEffect)EffectLog.Pop();

                    switch (obj.optname)
                    {
                        case "sharpen":

                            _sharpeff.PixelWidth = 0.0015;
                            _sharpeff.PixelHeight = 0.0015;
                            _sharpeff.Strength = obj.effvalue;
                            GrdSharpen.Effect = _sharpeff;
                            sharpen = obj.effvalue;
                            break;

                        case "hue":
                            _shifthueeff.HueShift = obj.effvalue;
                            GrdHueShift.Effect = _shifthueeff;
                            hueshift = obj.effvalue;
                            break;

                        case "greyscale":
                            if (obj.effvalue == 1)
                                GrdGreyScale.Effect = null;
                            _GreyScale = "0";
                            break;
                        case "sepia":
                            if (obj.effvalue == 1)
                                GrdSepia.Effect = null;
                            _sepia = "0";
                            break;
                        case "defog":
                            if (obj.effvalue == 1)

                                _defoger = "0";
                            GrdBrightness.Effect = null;
                            break;
                        case "underwater":
                            if (obj.effvalue == 1)
                                GrdUnderWater.Effect = null;
                            _underwater = "0";
                            break;
                        case "granite":
                            if (obj.effvalue == 1)
                                GrdSketchGranite.Effect = null;
                            _granite = "0";
                            break;
                        case "emboss":
                            if (obj.effvalue == 1)
                                GrdEmboss.Effect = null;
                            _emboss = "0";
                            break;
                        case "cartoon":
                            if (obj.effvalue == 1)
                                Grdcartoonize.Effect = null;
                            _cartoon = "0";
                            break;
                        case "invert":
                            if (obj.effvalue == 1)
                                GrdInvert.Effect = null;
                            _invert = "0";
                            break;
                        case "digimagic":
                            if (obj.effvalue == 1)
                                GrdBrightness.Effect = null;
                            GrdSharpen.Effect = null;
                            _digimagic = "0";
                            break;


                            //case "brightplus":
                            //    _brighteff.Brightness = obj.effvalue;

                            //    GrdBrightness.Effect = _brighteff;
                            //    break;

                            //case "brightminus":
                            //    _brighteff.Brightness = obj.effvalue;
                            //    GrdBrightness.Effect = _brighteff;
                            //    break;

                            //case "contrastplus":
                            //    _brighteff.Contrast = obj.effvalue;
                            //    GrdContrast.Effect = _conteff;
                            //    break;

                            //case "contrastminus":
                            //    _brighteff.Contrast = obj.effvalue;
                            //    GrdContrast.Effect = _conteff;
                            //    break;
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
#if DEBUG
            if (watch != null)
                FrameworkHelper.CommonUtility.WatchStop(this.GetType().FullName + "." + System.Reflection.MethodBase.GetCurrentMethod().Name + "|", watch);
#endif
        }

        /// <summary>
        /// Logs the operation.
        /// </summary>
        /// <param name="objOperation">The object operation.</param>
        private void LogOperation(LogObject objOperation)
        {
            try
            {
                SerialLog.Push(objOperation);
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }
        /// <summary>
        /// Effects the log operation.
        /// </summary>
        /// <param name="objeffectOperation">The objeffect operation.</param>
        private void EffectLogOperation(LogEffect objeffectOperation)
        {
            try
            {
                EffectLog.Push(objeffectOperation);
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }
        private void CompleteRestore()
        {
#if DEBUG
            var watch = FrameworkHelper.CommonUtility.Watch();
            watch.Start();
#endif
            try
            {
                try
                {
                    EffectsSender = null;
                    CmbProductType.Visibility = Visibility.Collapsed;
                    string path = HotFolderPath + "\\" + DateFolder + "\\" + tempfilename;
                    FileInfo _objnewinfo = new FileInfo(path);
                }
                catch
                {
                }

                BitmapImage image = CommonUtility.GetImageFromPath(System.IO.Path.Combine(HotFolderPath, DateFolder, tempfilename));
                widthimg.Source = image;
                imgRotateCrop.Source = image;
                mainImage.Source = image;
                mainImageundo.Source = image;
                if (((BitmapImage)mainImage.Source).DpiX > 0)
                {
                    CroppingAdorner.s_dpiX = ((BitmapImage)mainImage.Source).DpiX;
                    CroppingAdorner.s_dpiY = ((BitmapImage)mainImage.Source).DpiY;
                }
                else
                {
                    CroppingAdorner.s_dpiX = 300;
                    CroppingAdorner.s_dpiY = 300;
                }

                try
                {
                    if (File.Exists(System.IO.Path.Combine(CropFolderPath, tempfilename)))
                        File.Delete(System.IO.Path.Combine(CropFolderPath, tempfilename));
                }
                catch (Exception)
                {
                }
                finally
                {
                    MemoryManagement.FlushMemory();
                }
                RemoveAllGraphicsEffect();
                UncheckGraphicsButton();
                FrameworkHelper.Common.ContantValueForMainWindow.RedEyeSize = .0205;
                ellipse.Visibility = Visibility.Collapsed;
                greenEraser.Visibility = System.Windows.Visibility.Collapsed;
                squre.Visibility = System.Windows.Visibility.Hidden;
                mainImage.Cursor = Cursors.Arrow;
                MyInkCanvas.EditingMode = InkCanvasEditingMode.None;
                IsGraphicsChange = true;
                IsEffectChange = true;

                RemoveAllGraphicsEffect();
                EnableButtonForLayering();
                RemoveAllShaderEffects();

                this.elementForContextMenu = null;

                //Setting all the values to 0
                hueshift = 0.5;
                sharpen = 0.5;
                cont = 1;
                bright = 0;
                UpdateLayout();
                rotateangle = 0;
                FlipMode = 0;
                FlipModeY = 0;
                IsZoomed = false;

                SetRedeye = false;
                Noeffect();
                forWdht.RenderTransform = new RotateTransform();
                GrdFlip.LayoutTransform = new TransformGroup();
                GrdRotate.LayoutTransform = new TransformGroup();

                LoadPhotoAlbum();

                ImageEffect = "<image brightness = '0' contrast = '1' Crop='##' colourvalue = '##' rotatewidth='##' rotateheight='##' rotate='##' flipMode='0' flipModeY='0' _centerx ='0' _centery='0'><effects sharpen='##' greyscale='0' digimagic='0' sepia='0' defog='0' underwater='0' emboss='0' invert = '0' granite='0' hue ='##' cartoon = '0' firstredeye= 'false' firstradius='.0105' firstcenterx='.5' firstcentery='.5' secondredeye= 'false' secondradius='.0105' secondcenterx='.5' secondcentery='.5' multipleredeye1='false' multipleredeye2='false' multipleredeye3='false' multipleredeye4='false' multipleredeye5='false' multipleredeye6='false' multipleradius1='.0105' multipleradius2='0' multipleradius3='0' multipleradius4='0.0125' multipleradius5='0' multipleradius6='0' multiplecenterx1='.5' multiplecentery1='.5' multiplecenterx2='0' multiplecentery2='0' multiplecenterx3='0' multiplecentery3='0' multiplecenterx4='.5' multiplecentery4='.5' multiplecenterx5='0' multiplecentery5='0' multiplecenterx6='0' multiplecentery6='0'></effects></image>";
                VisualStateManager.GoToState(btnColorEffects, "Unchecked", true);
                VisualStateManager.GoToState(btnrotate, "Unchecked", true);
                VisualStateManager.GoToState(btnantirotate, "Unchecked", true);
                VisualStateManager.GoToState(btnCrop, "Unchecked", true);
                //Graphics button
                UncheckGraphicsButton();

                //Effectsbutton
                UncheckEffectsButton();

                color = new Color();
                currentcolor = new Color();

                SerialLog.Clear();
                EffectLog.Clear();

                graphicsBorderApplied = false;
                graphicsCount = 0;
                graphicsTextBoxCount = 0;
                gumballTextCount = 0;
                jrotate.Angle = 0;
                lblzoomplus.Content = 100 + " % ";
                selectedborder = string.Empty;

                MyInkCanvasParent.Effect = null;
                canbackground.RenderTransform = MyInkCanvas.RenderTransform = new MatrixTransform();
                attributeWidth = attributeHeight = 20;
                greenEraser.Visibility = Visibility.Collapsed;
                MyInkCanvas.EditingMode = InkCanvasEditingMode.None;
                IsEraserActive = false;
                IsEraserDrawEllipseActive = false;
                IsEraserDrawRectangleActive = false;
                IsGreenCorrection = false;
                isChromaApplied = false;
                _ZoomFactorGreen = 1;

                if (MyInkCanvas.Children.Count > 1)
                    MyInkCanvas.Children.RemoveRange(1, MyInkCanvas.Children.Count);
                if (MyInkCanvas.Strokes.Count > 0)
                    MyInkCanvas.Strokes.Clear();

                Opacitymsk.Margin = new Thickness(0, 0, 0, 0);

                forWdht.Width = widthimg.Source.Width;
                forWdht.Height = widthimg.Source.Height;

                Zomout(true);
                IsCropped = false;
                ((FrameworkHelper.MyImageClass)lstStrip.Items[selectedIndex]).IsCropped = false;
                graphicsframeApplied = false;
                if (image != null)
                {
                    image.Freeze();
                }
                IsImageDirtyState = true;
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
            finally
            {
                MemoryManagement.FlushMemory();
            }

#if DEBUG
            if (watch != null)
                FrameworkHelper.CommonUtility.WatchStop(this.GetType().FullName + "." + System.Reflection.MethodBase.GetCurrentMethod().Name + "|", watch);
#endif
        }
        private void ControlCleanup()
        {
            RemoveAllGraphicsEffect();
            UncheckGraphicsButton();
            EnableButtonForLayering();
            RemoveAllShaderEffects();
            this.elementForContextMenu = null;

            hueshift = 0.5;
            sharpen = 0.5;
            cont = 1;
            bright = 0;
            UpdateLayout();
            rotateangle = 0;
            FlipMode = 0;
            FlipModeY = 0;
            IsZoomed = false;
            Noeffect();
            forWdht.RenderTransform = new RotateTransform();
            GrdFlip.LayoutTransform = new TransformGroup();
            GrdRotate.LayoutTransform = new TransformGroup();
            LoadPhotoAlbum();
            VisualStateManager.GoToState(btnColorEffects, "Unchecked", true);
            VisualStateManager.GoToState(btnrotate, "Unchecked", true);
            VisualStateManager.GoToState(btnantirotate, "Unchecked", true);
            VisualStateManager.GoToState(btnCrop, "Unchecked", true);
            //Graphics button
            UncheckGraphicsButton();

            //Effectsbutton
            UncheckEffectsButton();

            color = new Color();
            currentcolor = new Color();
            SerialLog.Clear();
            EffectLog.Clear();
            graphicsBorderApplied = false;
            graphicsCount = 0;
            graphicsTextBoxCount = 0;
            gumballTextCount = 0;
            jrotate.Angle = 0;
            lblzoomplus.Content = 100 + " % ";
            selectedborder = string.Empty;
            IsCropped = false;
            graphicsframeApplied = false;
        }
        /// <summary>
        /// Removes all shader effects.
        /// </summary>
        private void RemoveAllShaderEffects()
        {
            try
            {
                GrdInvert.Effect = null;
                GrdSharpen.Effect = null;
                GrdSketchGranite.Effect = null;
                GrdEmboss.Effect = null;
                Grdcartoonize.Effect = null;
                GrdGreyScale.Effect = null;
                GrdHueShift.Effect = null;
                Grdcolorfilter.Effect = null;
                GrdBrightness.Effect = null;
                GrdSepia.Effect = null;
                GrdUnderWater.Effect = null;
                GrdRedEyeFirst.Effect = null;
                GrdRedEyeSecond.Effect = null;
                GrdRedEyeMultiple.Effect = null;
                GrdRedEyeMultiple1.Effect = null;
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }

        /// <summary>
        /// Unchecks the graphics button.
        /// </summary>
        private void UncheckGraphicsButton()
        {
            try
            {
                VisualStateManager.GoToState(btnAddgraphics, "Unchecked", true);
                VisualStateManager.GoToState(btnBorder, "Unchecked", true);
                VisualStateManager.GoToState(btnGraphicsText, "Unchecked", true);
                VisualStateManager.GoToState(btngraphics, "Unchecked", true);
                //VisualStateManager.GoToState(btnBackground, "Unchecked", true);
                VisualStateManager.GoToState(btnflip, "Unchecked", true);
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }
        private void RemoveAllGraphicsEffect()
        {
#if DEBUG
            var watch = FrameworkHelper.CommonUtility.Watch();
            watch.Start();
#endif
            try
            {
                imageundoGrid.Background = null;
                List<UIElement> itemstoremove = new List<UIElement>();
                foreach (UIElement ui in dragCanvas.Children)
                {
                    if (ui is Grid)
                    {
                    }
                    else if (ui is Ellipse)
                    {
                    }
                    else if (ui is Rectangle)
                    {
                    }
                    else if (ui is System.Windows.Shapes.Path)
                    {

                    }
                    else
                    {
                        itemstoremove.Add(ui);
                    }
                }
                foreach (UIElement ui in itemstoremove)
                {
                    dragCanvas.Children.Remove(ui);
                }

                itemstoremove = new List<UIElement>();
                foreach (UIElement ui in frm.Children)
                {
                    if (ui is OpaqueClickableImage)
                    {
                        itemstoremove.Add(ui);
                    }
                }
                foreach (UIElement ui in itemstoremove)
                {
                    frm.Children.Remove(ui);
                }

                _ZoomFactor = 1;
                if (zoomTransform != null)
                {
                    zoomTransform.CenterX = mainImage.ActualWidth / 2;
                    zoomTransform.CenterY = mainImage.ActualHeight / 2;

                    zoomTransform.ScaleX = _ZoomFactor;
                    zoomTransform.ScaleY = _ZoomFactor;
                    zoomTransform = new ScaleTransform();
                    transformGroup = new TransformGroup();
                    rotateTransform = new RotateTransform();

                    canbackground.RenderTransform = GrdGreenScreenDefault3.RenderTransform = null;
                    GrdBrightness.RenderTransform = null;
                    canbackground.RenderTransform = null;
                    Canvas.SetLeft(GrdBrightness, 0);
                    Canvas.SetTop(GrdBrightness, 0);
                    Canvas.SetLeft(Opacitymsk, 0);
                    Canvas.SetTop(Opacitymsk, 0);


                    Canvas.SetLeft(canbackground, 0);
                    Canvas.SetTop(canbackground, 0);
                    Canvas.SetLeft(canbackgroundParent, 0);
                    Canvas.SetTop(canbackgroundParent, 0);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }

#if DEBUG
            if (watch != null)
                FrameworkHelper.CommonUtility.WatchStop(this.GetType().FullName + "." + System.Reflection.MethodBase.GetCurrentMethod().Name + "|", watch);
#endif
        }
        /// <summary>
        /// Reloads all graphics effect.
        /// </summary>
        private void ReloadAllGraphicsEffect()
        {
#if DEBUG
            var watch = FrameworkHelper.CommonUtility.Watch();
            watch.Start();
#endif
            try
            {
                imageundoGrid.Background = null;
                List<UIElement> itemstoremove = new List<UIElement>();
                foreach (UIElement ui in dragCanvas.Children)
                {
                    if (ui is Grid)
                    {
                    }
                    else if (ui is Ellipse)
                    {
                    }
                    else if (ui is Rectangle)
                    {
                    }
                    else if (ui is System.Windows.Shapes.Path)
                    {

                    }
                    else
                    {
                        itemstoremove.Add(ui);
                    }
                }
                foreach (UIElement ui in itemstoremove)
                {
                    dragCanvas.Children.Remove(ui);
                }

                itemstoremove = new List<UIElement>();
                foreach (UIElement ui in frm.Children)
                {
                    if (ui is OpaqueClickableImage)
                    {
                        itemstoremove.Add(ui);
                    }
                }
                foreach (UIElement ui in itemstoremove)
                {
                    frm.Children.Remove(ui);
                }

                if (FlipMode != 0 || FlipModeY != 0)
                {
                    _ZoomFactor = 1;

                    zoomTransform.CenterX = Convert.ToDouble(_centerX);
                    zoomTransform.CenterY = Convert.ToDouble(_centerY);
                    zoomTransform.ScaleX = -1;
                    zoomTransform.ScaleY = 1;

                    translateTransform = new TranslateTransform();
                    rotateTransform = new RotateTransform();
                    transformGroup = new TransformGroup();

                    canbackground.RenderTransform = GrdGreenScreenDefault3.RenderTransform = transformGroup;

                    Canvas.SetLeft(GrdBrightness, 0);
                    Canvas.SetTop(GrdBrightness, 0);
                    Canvas.SetLeft(Opacitymsk, 0);
                    Canvas.SetTop(Opacitymsk, 0);
                }
                else
                {
                    _ZoomFactor = 1;

                    zoomTransform.CenterX = mainImage.ActualWidth / 2;
                    zoomTransform.CenterY = mainImage.ActualHeight / 2;
                    zoomTransform.ScaleX = _ZoomFactor;
                    zoomTransform.ScaleY = _ZoomFactor;

                    transformGroup = new TransformGroup();
                    rotateTransform = new RotateTransform();
                    zoomTransform = new ScaleTransform();
                    canbackground.RenderTransform = GrdGreenScreenDefault3.RenderTransform = null;

                    Canvas.SetLeft(GrdBrightness, 0);
                    Canvas.SetTop(GrdBrightness, 0);
                    Canvas.SetLeft(Opacitymsk, 0);
                    Canvas.SetTop(Opacitymsk, 0);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
#if DEBUG
            if (watch != null)
                FrameworkHelper.CommonUtility.WatchStop(this.GetType().FullName + "." + System.Reflection.MethodBase.GetCurrentMethod().Name + "|", watch);
#endif
        }
        /// <summary>
        /// Hides the handler dialog.
        /// </summary>
        private void HideHandlerDialog()
        {
            try
            {
                _hideRequest = true;
                Visibility = Visibility.Collapsed;
                _parent.IsEnabled = true;
                ImageClick = true;
            }
            catch
            {
            }
        }

        private void SaveOnImageChange()
        {
#if DEBUG
            var watch = FrameworkHelper.CommonUtility.Watch();
            watch.Start();
#endif
            bool isEffectApplied = false;
            try
            {
                EffectsSender = null;
                CmbProductType.Visibility = Visibility.Collapsed;
                imageundoGrid.IsHitTestVisible = true;

                if (!isChromaApplied && IsGreenRemove)
                {
                    if (MyInkCanvas.Children.Count > 1)
                        MyInkCanvas.Children.RemoveRange(1, MyInkCanvas.Children.Count);
                    if (MyInkCanvas.Strokes.Count > 0)
                        MyInkCanvas.Strokes.Clear();
                }

                string input = string.Empty;

                bool graphicChange = false;

                if (IsGraphicsChange || isChromaApplied)
                {
                    input = SaveXaml(ref graphicChange);
                }

                if (IsEffectChange || IsGraphicsChange)
                    isEffectApplied = true;

                if (IsCropped)
                    ((FrameworkHelper.MyImageClass)lstStrip.Items[selectedIndex]).IsCropped = IsCropped;

                if (!IsModerate && IsEffectChange)
                {
                    ((FrameworkHelper.MyImageClass)lstStrip.Items[selectedIndex]).NewEffectsXML = ImageEffect;
                }

                if (!IsModerate && (IsGraphicsChange || IsEffectChange || IsChromaChanged))
                {
                    if (string.IsNullOrWhiteSpace(input))
                    {
                        if (((FrameworkHelper.MyImageClass)lstStrip.Items[selectedIndex]).SettingStatus == SettingStatus.Spec)
                        {
                            ((FrameworkHelper.MyImageClass)lstStrip.Items[selectedIndex]).NewLayeringXML = SpecLayeringUpdated;
                            ((FrameworkHelper.MyImageClass)lstStrip.Items[selectedIndex]).NewEffectsXML = SpecImageEffect;
                        }
                        else if (((FrameworkHelper.MyImageClass)lstStrip.Items[selectedIndex]).SettingStatus == SettingStatus.None)
                        {
                            ((FrameworkHelper.MyImageClass)lstStrip.Items[selectedIndex]).NewLayeringXML = null;
                        }
                    }
                    else
                    {
                        ((FrameworkHelper.MyImageClass)lstStrip.Items[selectedIndex]).NewLayeringXML = input;
                        ((FrameworkHelper.MyImageClass)lstStrip.Items[selectedIndex]).NewEffectsXML = ImageEffect;
                        ((FrameworkHelper.MyImageClass)lstStrip.Items[selectedIndex]).SettingStatus = SettingStatus.SpecUpdated;
                    }
                }

                if (IsRestoreRideClick)
                {
                    ((FrameworkHelper.MyImageClass)lstStrip.Items[selectedIndex]).NewEffectsXML = "<image brightness = '0' contrast = '1' Crop='##' colourvalue = '##' rotate='##' ><effects sharpen='##' greyscale='0' digimagic='0' sepia='0' defog='0' underwater='0' emboss='0' invert = '0' granite='0' hue ='##' cartoon = '0'></effects></image>";
                    ((FrameworkHelper.MyImageClass)lstStrip.Items[selectedIndex]).NewLayeringXML = null;
                    ((FrameworkHelper.MyImageClass)lstStrip.Items[selectedIndex]).IsCropped = false;
                }

                if (isChromaApplied && !IsModerate && (IsChromaChanged || isEffectApplied))
                {
                    if (IsChromaChanged)
                        IsChromaChanged = false;
                }
                ChromaGridLeft = 0;
                ChromaGridTop = 0;
                ChromaCenterX = 0;
                ChromaCenterY = 0;
                ChromaZoomFactor = 0;
                ChromaBorderPath = string.Empty;
                OriginalBorder = string.Empty;
                IsImageDirtyState = false;
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
            finally
            {
                MemoryManagement.FlushMemory();
                GC.AddMemoryPressure(10000);
                lstStrip.Items.Refresh();
                lstStrip.UpdateLayout();
            }
#if DEBUG
            if (watch != null)
                FrameworkHelper.CommonUtility.WatchStop(this.GetType().FullName + "." + System.Reflection.MethodBase.GetCurrentMethod().Name + "|", watch);
#endif
        }

        /// <summary>
        /// Saves the crop asynchronous red eye image.
        /// </summary>
        /// <param name="mainImageSouce">The main image souce.</param>
        /// <param name="operation">The operation.</param>
        private void SaveCropNRedEyeImage(BitmapSource mainImageSouce, string operation)
        {
#if DEBUG
            var watch = FrameworkHelper.CommonUtility.Watch();
            watch.Start();
#endif
            try
            {
                PhotoBusiness phBiz = new PhotoBusiness();
                if (!System.IO.Directory.Exists(CropFolderPath))
                    System.IO.Directory.CreateDirectory(CropFolderPath);
                using (var fileStream = new FileStream(System.IO.Path.Combine(CropFolderPath, tempfilename), FileMode.Create))
                {
                    JpegBitmapEncoder encoder = new JpegBitmapEncoder();
                    encoder.QualityLevel = 98;
                    encoder.Frames.Add(BitmapFrame.Create(mainImageSouce));
                    encoder.Save(fileStream);
                    DisposableResource DispObj = new DisposableResource(fileStream);
                    DispObj.DoSomethingWithResource();
                }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
            finally
            {
                MemoryManagement.FlushMemory();
            }

#if DEBUG
            if (watch != null)
                FrameworkHelper.CommonUtility.WatchStop(this.GetType().FullName + "." + System.Reflection.MethodBase.GetCurrentMethod().Name + "|", watch);
#endif
        }
        private void GetMktImgInfo()
        {
#if DEBUG
            var watch = FrameworkHelper.CommonUtility.Watch();
            watch.Start();
#endif
            try
            {
                List<long> objList = new List<long>();
                objList.Add((long)ConfigParams.MktImgPath);
                objList.Add((long)ConfigParams.MktImgTimeInSec);
                ConfigBusiness configBiz = new ConfigBusiness();
                List<iMIXConfigurationInfo> ConfigValuesList = configBiz.GetNewConfigValues(LoginUser.SubStoreId).Where(o => objList.Contains(o.IMIXConfigurationMasterId)).ToList();
                if (ConfigValuesList != null || ConfigValuesList.Count > 0)
                {
                    for (int i = 0; i < ConfigValuesList.Count; i++)
                    {
                        switch (ConfigValuesList[i].IMIXConfigurationMasterId)
                        {
                            case (int)ConfigParams.MktImgPath:
                                MktImgPath = ConfigValuesList[i].ConfigurationValue;
                                break;
                            case (int)ConfigParams.MktImgTimeInSec:
                                mktImgTime = (ConfigValuesList[i].ConfigurationValue != null ? Convert.ToInt32(ConfigValuesList[i].ConfigurationValue) : 10) * 1000;
                                break;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
#if DEBUG
            if (watch != null)
                FrameworkHelper.CommonUtility.WatchStop(this.GetType().FullName + "." + System.Reflection.MethodBase.GetCurrentMethod().Name + "|", watch);
#endif
        }

        private void GetBackImageSettings()
        {
#if DEBUG
            var watch = FrameworkHelper.CommonUtility.Watch();
            watch.Start();
#endif
            try
            {
                //to filter 
                List<long> objList = new List<long>();
                objList.Add((long)ConfigParams.IsDefaultBackImageEnabled);
                objList.Add((long)ConfigParams.DefaultBackImagePath);
                objList.Add((long)ConfigParams.ProductPreview);
                objList.Add((long)ConfigParams.IsEnableSingleScreenPreview);
                ConfigBusiness configBiz = new ConfigBusiness();
                List<iMIXConfigurationInfo> ConfigValuesList = configBiz.GetNewConfigValues(LoginUser.SubStoreId).Where(o => objList.Contains(o.IMIXConfigurationMasterId)).ToList();
                if (ConfigValuesList != null || ConfigValuesList.Count > 0)
                {
                    for (int i = 0; i < ConfigValuesList.Count; i++)
                    {
                        switch (ConfigValuesList[i].IMIXConfigurationMasterId)
                        {
                            //33
                            case (int)ConfigParams.IsDefaultBackImageEnabled:
                                IsDefaultBackgroundEnabled = Convert.ToBoolean(ConfigValuesList[i].ConfigurationValue);
                                break;
                            case (int)ConfigParams.DefaultBackImagePath:
                                DefaultBackgroundImagePath = ConfigValuesList[i].ConfigurationValue != null ? ConfigValuesList[i].ConfigurationValue : "10";
                                break;
                            case (int)ConfigParams.ProductPreview:
                                CmbProductType.SelectedValue = string.IsNullOrEmpty(ConfigValuesList[i].ConfigurationValue) == false ? Convert.ToString(ConfigValuesList[i].ConfigurationValue) : "1";
                                break;
                            case (int)ConfigParams.IsEnableSingleScreenPreview:
                                isSingleScreenPreview = ConfigValuesList[i].ConfigurationValue != null ? Convert.ToBoolean(ConfigValuesList[i].ConfigurationValue) : false;
                                break;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }

#if DEBUG
            if (watch != null)
                FrameworkHelper.CommonUtility.WatchStop(this.GetType().FullName + "." + System.Reflection.MethodBase.GetCurrentMethod().Name + "|", watch);
#endif
        }

        private void FillProductCombo()
        {
#if DEBUG
            var watch = FrameworkHelper.CommonUtility.Watch();
            watch.Start();
#endif
            Dictionary<string, string> lstProductList = new Dictionary<string, string>();
            try
            {
                ProductBusiness prdBiz = new ProductBusiness();
                List<ProductTypeInfo> objPrdList = prdBiz.GetProductType();

                foreach (var item in objPrdList.Where(t => t.DG_Orders_ProductType_pkey == 1 || t.DG_Orders_ProductType_pkey == 2 || t.DG_Orders_ProductType_pkey == 98 || t.DG_Orders_ProductType_pkey == 30 || t.DG_Orders_ProductType_pkey == 103))
                {
                    lstProductList.Add(item.DG_Orders_ProductType_Name, item.DG_Orders_ProductType_pkey.ToString());
                }
                CmbProductType.ItemsSource = lstProductList;
                CmbProductType.SelectedIndex = 0;
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }

#if DEBUG
            if (watch != null)
                FrameworkHelper.CommonUtility.WatchStop(this.GetType().FullName + "." + System.Reflection.MethodBase.GetCurrentMethod().Name + "|", watch);
#endif
        }
        public void Onload(bool crop, bool isGreenImageParam, string graphicEffect, bool IsGumBallshow)
        {
#if DEBUG
            var watch = FrameworkHelper.CommonUtility.Watch();
            watch.Start();
#endif
            IsImageDirtyState = false;
            IsComingOnLoad = true;
            PhotoBusiness phBiz = new PhotoBusiness();
            isGreenImage = isGreenImageParam;
            ChromaGridLeft = 0;
            ChromaGridTop = 0;
            ChromaCenterX = 0;
            ChromaCenterY = 0;
            ChromaZoomFactor = 0;
            ChromaBorderPath = string.Empty;
            OriginalBorder = String.Empty;
            BackgroundDBValue = string.Empty;
            BackgroundSelectedValue = string.Empty;
            this.elementForContextMenu = null;
            canbackgroundParent.Visibility = System.Windows.Visibility.Collapsed;
            canbackgroundRotate.RenderTransform = null;
            Canvas.SetZIndex(imageundoGrid, 0);
            Canvas.SetZIndex(canbackgroundParent, 4);
            Canvas.SetZIndex(Opacitymsk, 2);
            Canvas.SetZIndex(frm, 6);
            IsZoomed = false;
            GraphicEffect = graphicEffect;
            GrdsubCrop.Visibility = System.Windows.Visibility.Hidden;
            GrdsubColoreffects.Visibility = System.Windows.Visibility.Hidden;
            GrdsubEffects.Visibility = System.Windows.Visibility.Hidden;
            GrdSubGraphics.Visibility = System.Windows.Visibility.Hidden;
            {
                attributeWidth = attributeHeight = 10;
                _centerX = "";
                _centerY = "";
                rotateangle = 0;
                FlipMode = 0;
                FlipModeY = 0;
                txtMainImage.Text = PhotoName;

                MyInkCanvas.SnapsToDevicePixels = true;
                MyInkCanvas.SetValue(RenderOptions.EdgeModeProperty, EdgeMode.Aliased);
                mainImage.SnapsToDevicePixels = true;
                RenderOptions.SetEdgeMode(mainImage, EdgeMode.Aliased);
                forWdht.RenderTransform = new RotateTransform();
                VisualStateManager.GoToState(btnCrop, "Unchecked", true);
                try
                {
                    if (crop)
                        VisualStateManager.GoToState(btnCrop, "Checked", true);

                    if (crop && !isGreenImage)
                    {
                        IsModerate = false;
                        EnabledAllButtons();

                        using (FileStream fileStream = File.OpenRead(System.IO.Path.Combine(CropFolderPath, tempfilename)))
                        {
                            BitmapImage bi = new BitmapImage();
                            MemoryStream ms = new MemoryStream();
                            fileStream.CopyTo(ms);
                            ms.Seek(0, SeekOrigin.Begin);
                            fileStream.Close();
                            bi.BeginInit();
                            bi.StreamSource = ms;
                            bi.EndInit();
                            mainImageundo.Source = mainImage.Source = bi;
                            widthimg.Source = bi;
                            imgRotateCrop.Source = bi;
                            imgoriginal.Source = bi;
                            CroppingAdorner.s_dpiX = bi.DpiX;
                            CroppingAdorner.s_dpiY = bi.DpiY;
                            bi.Freeze();
                        }
                    }
                    else
                    {
                        mainImageundo.Source = mainImage.Source = null;
                        canbackground.Background = null;
                        widthimg.Source = null;
                        widthimg.UpdateLayout();
                        EnabledAllButtons();
                        IsModerate = false; string tempPNGname = string.Empty;
                        if (isGreenImage)
                            tempPNGname = tempfilename.ToLower().EndsWith(".jpg") ? tempfilename.Replace(".jpg", ".png") : tempfilename;
                        else
                            tempPNGname = tempfilename;

                        using (FileStream fileStream = File.OpenRead(System.IO.Path.Combine(HotFolderPath, DateFolder, tempfilename)))
                        {
                            BitmapImage bi = new BitmapImage();
                            MemoryStream ms = new MemoryStream();
                            fileStream.CopyTo(ms);
                            ms.Seek(0, SeekOrigin.Begin);
                            fileStream.Close();
                            bi.BeginInit();
                            bi.StreamSource = ms;
                            bi.EndInit();
                            mainImageundo.Source = bi;
                            widthimg.Source = bi;
                            imgoriginal.Source = bi;
                            mainImage.Source = bi;
                            CroppingAdorner.s_dpiX = bi.DpiX;
                            CroppingAdorner.s_dpiY = bi.DpiY;
                            imgRotateCrop.Source = bi;
                            bi.Freeze();
                        }
                        mainImageundo.Visibility = Visibility.Hidden;
                    }

                    forWdht.Height = widthimg.Source.Height;//check for  
                    forWdht.Width = widthimg.Source.Width;
                    LoadPhotoAlbum();

                    MyInkCanvas.EditingMode = InkCanvasEditingMode.None;
                    canbackground.RenderTransform = MyInkCanvas.RenderTransform = new MatrixTransform();
                    canbackground.RenderTransform =
                    GrdBrightness.RenderTransform = null;
                    canbackgroundParent.Margin = Opacitymsk.Margin = new Thickness(0, 0, 0, 0);

                    txtContent.Text = string.Empty;

                    if (!IsModerate)
                    {
                        LoadXml(ImageEffect);
                    }
                    dragCanvas.AllowDragOutOfView = true;
                    GrdEffects.Visibility = System.Windows.Visibility.Hidden;

                    forWdht.HorizontalAlignment = System.Windows.HorizontalAlignment.Center;
                    forWdht.VerticalAlignment = System.Windows.VerticalAlignment.Center;
                    if (!IsModerate || !isGreenImage)
                    {
                        LoadFromDB();
                    }

                    dragCanvas.AllowDragging = false;
                    dragCanvas.IsEnabled = false;
                    IsEffectChange = false;
                    IsGraphicsChange = false;

                    jrotate.Visibility = System.Windows.Visibility.Hidden;

                    MyInkCanvasParent.Effect = null;

                    if (MyInkCanvas.Children.Count > 1)
                        MyInkCanvas.Children.RemoveRange(1, MyInkCanvas.Children.Count);
                    if (MyInkCanvas.Strokes.Count > 0)
                        MyInkCanvas.Strokes.Clear();

                    if (FlipMode != 0 || FlipModeY != 0)
                    {
                        btnCrop.IsEnabled = false;
                    }
                    if (rotateangle > 0)
                    {
                        btnCrop.IsEnabled = false;
                    }
                    if (crop)
                    {
                        btnCrop.IsEnabled = false;
                    }

                }
                catch (Exception ex)
                {
                    string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                    ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                }
                finally
                {
                    MemoryManagement.FlushMemory();
                    Zomout(true);
                    IsMoveEnabled = false;
                    MoveImageStartStop();
                }
            }
#if DEBUG
            if (watch != null)
                FrameworkHelper.CommonUtility.WatchStop(this.GetType().FullName + "." + System.Reflection.MethodBase.GetCurrentMethod().Name + "|", watch);
#endif
        }
        private void ClearResources()
        {
#if DEBUG
            var watch = FrameworkHelper.CommonUtility.Watch();
            watch.Start();
#endif
            btnrestoremainimg.Click -= new RoutedEventHandler(btnrestoremainimg_Click);
            btnRestoreGraphics.Click -= new RoutedEventHandler(btnRestoreGraphics_Click);
            btnAddgraphics.Click -= new RoutedEventHandler(btnAddgraphics_Click);
            CancelButton.Click -= new RoutedEventHandler(CancelButton_Click);
            btnRevert.Click -= new RoutedEventHandler(btnRevert_Click);
            btnantirotate.Click -= new RoutedEventHandler(btnantirotate_Click);
            btnSaveBack.Click -= new RoutedEventHandler(btnSaveBack_Click);
            btnrotate.Click -= new RoutedEventHandler(btnRotateclick_Click);
            btnflip.Click -= new RoutedEventHandler(btnflip_Click);
            this.Unloaded -= new RoutedEventHandler(Window_Unloaded);
            btnBringToFront.Click -= new RoutedEventHandler(BringToFront_Click);
            btnSendToBack.Click -= new RoutedEventHandler(SendToBack_Click);
            BrightnessGreen.LayoutUpdated -= new EventHandler(GrdBrightness_LayoutUpdated);
            btnCrop.Click -= new RoutedEventHandler(btnCrop_Click);
            OkButton.Click -= new RoutedEventHandler(OkButton_Click);
            btnenlargeplus.Click -= new RoutedEventHandler(btnenlargeplus_Click);
            btnenlargeminus.Click -= new RoutedEventHandler(btnenlargeminus_Click);
            btnselect.Click -= new RoutedEventHandler(btnselect_Click);
            btnselect8By11.Click -= new RoutedEventHandler(btnselectEightByEleven_Click);
            btnSelectReverse.Click -= new RoutedEventHandler(btnSelectReverse_Click);
            btnSelectCrop4by6.Click -= new RoutedEventHandler(btnSelectCrop4by6_Click);
            btnSelectCrop5by7.Click -= new RoutedEventHandler(btnSelectCrop5by7_Click);
            btnselected.Click -= new RoutedEventHandler(btnSelectCrop3by3_Click);
            btnColorEffects.Click -= new RoutedEventHandler(btnColorEffects_Click);
            BrightnessPlus.Click -= new RoutedEventHandler(BrightnessPlus_Click);
            BrightnessMinus.Click -= new RoutedEventHandler(BrightnessMinus_Click);
            ContrastPlus.Click -= new RoutedEventHandler(ContrastPlus_Click);
            ContrastMinus.Click -= new RoutedEventHandler(ContrastMinus_Click);
            OkButtoncolor.Click -= new RoutedEventHandler(OkButtoncolor_Click);
            CancelButtoncolor.Click -= new RoutedEventHandler(CancelButtoncolor_Click);
            btnRestoreBrightCont.Click -= new RoutedEventHandler(btnRestoreBrightCont_Click);
            mainImage.MouseMove -= new MouseEventHandler(mainImage_MouseMove_1);
            //ellipse.MouseLeftButtonDown -= new MouseButtonEventHandler(ellipse_MouseLeftButtonDown);
            btnColorEffectsfilters.Click -= new RoutedEventHandler(btnColorEffectsfilters_Click);
            CancelButtoncoloreffect.Click -= new RoutedEventHandler(CancelButtoncoloreffect_Click);
            OkButtoncoloreffect.Click -= new RoutedEventHandler(OkButtoncoloreffect_Click);
            btnSharpen.Click -= new RoutedEventHandler(btnSharpen_Click);
            btnInvert.Click -= new RoutedEventHandler(btnInvert_Click);
            btnGreyScale.Click -= new RoutedEventHandler(btnGreyScale_Click);
            btnEdgeDetect.Click -= new RoutedEventHandler(btnEdgeDetect_Click);
            btnEmboss.Click -= new RoutedEventHandler(btnEmboss_Click);
            btnSepia.Click -= new RoutedEventHandler(btnSepia_Click);
            btnDefogger.Click -= new RoutedEventHandler(btnDefogger_Click);
            btnDigiMagic.Click -= new RoutedEventHandler(btnDigiMagic_Click);
            btnUnderWater.Click -= new RoutedEventHandler(btnUnderWater_Click);
            btnundocoloreffects.Click -= new RoutedEventHandler(btnundocoloreffects_Click);
            btnRestore.Click -= new RoutedEventHandler(btnRestore_Click);
            Cartoonize.Click -= new RoutedEventHandler(Cartoonize_Click);
            btnHue.Click -= new RoutedEventHandler(btnHue_Click);
            jrotate.AngleChanged -= new JRotate.JAngleChangedDelegate(angleAltitudeSelector1_AngleChanged);
            txtContent.TextChanged -= new TextChangedEventHandler(txtContent_TextChanged);
            cmbFont.SelectionChanged -= new SelectionChangedEventHandler(cmbFont_SelectionChanged);
            CmbColor.SelectionChanged -= new SelectionChangedEventHandler(CmbColor_SelectionChanged);
            CmbFontSize.SelectionChanged -= new SelectionChangedEventHandler(CmbFontSize_SelectionChanged);
            btnrotateGraphics.Click -= new RoutedEventHandler(btnrotateGraphics_Click);
            btnRotateInButton.Click -= new RoutedEventHandler(RotateInButton_Click);
            btnAntiRotate.Click -= new RoutedEventHandler(AntiRotateButton_Click);
            btnExit.Click -= new RoutedEventHandler(btnExit_Click);
            btnClose1.Click -= new RoutedEventHandler(btnClose1_Click);
            MyInkCanvas.KeyDown -= new KeyEventHandler(MyInkCanvas_KeyDown);
            MyInkCanvas.MouseEnter -= new MouseEventHandler(MyInkCanvas_MouseEnter);
            CmbProductType.SelectionChanged -= new SelectionChangedEventHandler(CmbProductType_SelectionChanged);
            canbackgroundold.MouseWheel -= new MouseWheelEventHandler(canbackgroundold_MouseWheel);
            jrotate.MouseWheel -= new MouseWheelEventHandler(jrotate_MouseWheel);
#if DEBUG
            if (watch != null)
                FrameworkHelper.CommonUtility.WatchStop(this.GetType().FullName + "." + System.Reflection.MethodBase.GetCurrentMethod().Name + "|", watch);
#endif
        }
        private void MoveImageStartStop()
        {
            if (!IsMoveEnabled)
            {
                DragCanvas.SetCanBeDragged(mainImage, true);
                DragCanvas.SetCanBeDragged(GrdBrightness, true);
                DragCanvas.SetCanBeDragged(Opacitymsk, true);
                DragCanvas.SetCanBeDragged(imageundoGrid, false);
                dragCanvas.AllowDragging = true;
                dragCanvas.IsEnabled = true;
                IsMoveEnabled = true;
            }
            else
            {
                DragCanvas.SetCanBeDragged(mainImage, false);
                DragCanvas.SetCanBeDragged(GrdBrightness, false);
                DragCanvas.SetCanBeDragged(Opacitymsk, false);
                DragCanvas.SetCanBeDragged(imageundoGrid, false);
                dragCanvas.AllowDragging = false;
                dragCanvas.IsEnabled = false;
                IsMoveEnabled = false;
            }
        }
        private void LoadBackgroundInGrid(BitmapImage objCurrent)
        {
#if DEBUG
            var watch = FrameworkHelper.CommonUtility.Watch();
            watch.Start();
#endif

            canbackgroundParent.Visibility = System.Windows.Visibility.Visible;
            canbackground.Visibility = System.Windows.Visibility.Visible;
            imageundoGrid.Visibility = System.Windows.Visibility.Visible;
            imageundoGrid.Background = new ImageBrush { ImageSource = objCurrent };
            //VisualStateManager.GoToState(btnBackground, "Checked", true);
            VisualStateManager.GoToState(btnAddgraphics, "Checked", true);
            graphicsBorderApplied = true;

            if (canbackgroundold.Background != null)
            {
                canbackgroundold.Background.Opacity = 1;
            }

            if (frm.Children.Count == 0)
            {

                double ratio = 1;
                if (objCurrent.Height > objCurrent.Width)
                {
                    ratio = (double)(objCurrent.Width / objCurrent.Height);
                }
                else
                {
                    ratio = (double)(objCurrent.Height / objCurrent.Width);
                }

                if (forWdht.Height > forWdht.Width)
                {
                    forWdht.Width = forWdht.Height * ratio;
                }
                else
                {
                    forWdht.Height = forWdht.Width * ratio;
                }

                forWdht.InvalidateArrange();
                forWdht.InvalidateMeasure();
                forWdht.InvalidateVisual();
                imageundoGrid.Height = forWdht.Height;
                imageundoGrid.Width = forWdht.Width;
                imageundoGrid.InvalidateArrange();
                imageundoGrid.InvalidateMeasure();
                imageundoGrid.InvalidateVisual();
                Zomout(true);
            }
            objCurrent.Freeze();

#if DEBUG
            if (watch != null)
                FrameworkHelper.CommonUtility.WatchStop(this.GetType().FullName + "." + System.Reflection.MethodBase.GetCurrentMethod().Name + "|", watch);
#endif
        }
        #endregion

        #region Events

        /// <summary>
        /// Handles the Click event of the btnrestoremainimg control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void btnrestoremainimg_Click(object sender, RoutedEventArgs e)
        {
            CompleteRestore();
            IsRestoreRideClick = true;
            if (IsImageDirtyState)
                SaveOnImageChange();

            //((FrameworkHelper.MyImageClass)lstStrip.Items[selectedIndex]).SettingStatus = SettingStatus.None;
            IsImageDirtyState = false;
            IsRestoreRideClick = false;
        }

        private void ellipse_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {

        }

        /// <summary>
        /// Handles the Click event of the btnRestoreGraphics control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void btnRestoreGraphics_Click(object sender, RoutedEventArgs e)
        {
#if DEBUG
            var watch = FrameworkHelper.CommonUtility.Watch();
            watch.Start();
#endif

            try
            {
                CmbProductType.Visibility = Visibility.Collapsed;
                jrotate.Angle = 0;
                this.elementForContextMenu = null;
                RemoveAllGraphicsEffect();
                UncheckGraphicsButton();
                IsGraphicsChange = true;
                graphicsBorderApplied = false;
                graphicsCount = 0;
                graphicsframeApplied = false;
                graphicsTextBoxCount = 0;
                // FlipLoad();
                lblzoomplus.Content = 100 + " % ";
                selectedborder = string.Empty;
                GrdEffects.Visibility = System.Windows.Visibility.Collapsed;
                jrotate.Visibility = System.Windows.Visibility.Hidden;
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
#if DEBUG
            if (watch != null)
                FrameworkHelper.CommonUtility.WatchStop(this.GetType().FullName + "." + System.Reflection.MethodBase.GetCurrentMethod().Name + "|", watch);
#endif
        }

        /// <summary>
        /// Handles the Click event of the btnAddgraphics control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void btnAddgraphics_Click(object sender, RoutedEventArgs e)
        {
#if DEBUG
            var watch = FrameworkHelper.CommonUtility.Watch();
            watch.Start();
#endif
            try
            {
                if (SetRedeye)
                {
                    ellipse.Visibility = Visibility.Collapsed;
                    mainImage.Cursor = Cursors.Arrow;
                    SetRedeye = false;
                }
                greenEraser.Visibility = System.Windows.Visibility.Collapsed;
                MyInkCanvas.EditingMode = InkCanvasEditingMode.None;
                GrdSubGraphics.Visibility = Visibility.Visible;
                DragCanvas.SetCanBeDragged(mainImage, true);
                DragCanvas.SetCanBeDragged(GrdBrightness, true);
                DragCanvas.SetCanBeDragged(Opacitymsk, true);
                DragCanvas.SetCanBeDragged(imageundoGrid, false);
                imageundoGrid.IsHitTestVisible = false;


                dragCanvas.AllowDragging = true;
                dragCanvas.IsEnabled = true;
                LoadXml(ImageEffect);
                ImageClick = true;
                IsSelectedMainImage = true;
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                MessageBox.Show(ex.Message);
            }

#if DEBUG
            if (watch != null)
                FrameworkHelper.CommonUtility.WatchStop(this.GetType().FullName + "." + System.Reflection.MethodBase.GetCurrentMethod().Name + "|", watch);
#endif
        }
        /// <summary>
        /// Handles the Click event of the CancelButton control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
#if DEBUG
            var watch = FrameworkHelper.CommonUtility.Watch();
            watch.Start();
#endif
            CmbProductType.Visibility = Visibility.Collapsed;
            this.elementForContextMenu = null;
            imageundoGrid.IsHitTestVisible = true;
            rotateangle = 0;
            FlipMode = 0;
            jrotate.Visibility = System.Windows.Visibility.Hidden;
            try
            {

                if (_felCur != null)
                {
                    RemoveCropFromCur();
                    _felCur = null;
                    _clp = null;
                }
                btnSelectReverse.IsEnabled = false;
                GrdsubColoreffects.Visibility = Visibility.Collapsed;
                GrdSubGraphics.Visibility = Visibility.Collapsed;
                GrdsubCrop.Visibility = Visibility.Hidden;
                lstStrip.Visibility = Visibility.Visible;
                GrdEffects.Visibility = Visibility.Visible;

                ColorEffectsIMG.Visibility = Visibility.Collapsed;
                IMGFrame.Visibility = Visibility.Visible;
                //   GrdEffects.Visibility = Visibility.Collapsed;
                btnrotate.IsEnabled = true;
                btnrotate.IsEnabled = true;
                btnflip.IsEnabled = true;
                btnCrop.IsEnabled = true;
                btnantirotate.IsEnabled = true;
                btnAddgraphics.IsEnabled = true;

                dragCanvas.AllowDragging = false;
                dragCanvas.IsEnabled = false;
                if (dragCanvas.Children.Count == 4 && _ZoomFactor == 1 && frm.Children.Count == 0 && !((graphicsBorderApplied || graphicsCount > 0 || graphicsframeApplied || graphicsTextBoxCount > 0 || gumballTextCount > 0 || _ZoomFactor > 1 || _ZoomFactor < 1 || jrotate.Angle != 0)))
                {
                    EnableButtonForLayering();
                }
                else
                {
                    DisableButtonForLayering();
                }

                if (graphicsBorderApplied || graphicsCount > 0 || graphicsframeApplied || graphicsTextBoxCount > 0 || gumballTextCount > 0 || _ZoomFactor > 1 || _ZoomFactor < 1)
                {
                    VisualStateManager.GoToState(btnAddgraphics, "Checked", true);
                }
                else
                {
                    VisualStateManager.GoToState(btnAddgraphics, "Unchecked", true);
                }
                grdZoomCanvas.Visibility = System.Windows.Visibility.Visible;
                GrdRotateCropParent.Visibility = System.Windows.Visibility.Collapsed;
                if (IsCropped || FlipMode != 0 || FlipModeY != 0 || rotateangle > 0)
                {
                    btnCrop.IsEnabled = false;
                }

                VisualStateManager.GoToState(btnrotate, "Unchecked", true);
                VisualStateManager.GoToState(btnantirotate, "Unchecked", true);
                VisualStateManager.GoToState(btnflip, "Unchecked", true);
                btnThumbnails_Click(new object(), new RoutedEventArgs());
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }

#if DEBUG
            if (watch != null)
                FrameworkHelper.CommonUtility.WatchStop(this.GetType().FullName + "." + System.Reflection.MethodBase.GetCurrentMethod().Name + "|", watch);
#endif
        }

        /// <summary>
        /// Handles the Click event of the btnantirotate control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void btnantirotate_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (SetRedeye)
                {
                    ellipse.Visibility = Visibility.Collapsed;
                    mainImage.Cursor = Cursors.Arrow;
                    SetRedeye = false;
                }
                Rotate(-1);
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }

        }
        /// <summary>
        /// Handles the Click event of the btnSaveBack control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void btnSaveBack_Click(object sender, RoutedEventArgs e)
        {
#if DEBUG
            var watch = FrameworkHelper.CommonUtility.Watch();
            watch.Start();
#endif
            bool isEffectApplied = false;
            try
            {
                EffectsSender = null;
                CmbProductType.Visibility = Visibility.Collapsed;
                imageundoGrid.IsHitTestVisible = true;

                if (!isChromaApplied && IsGreenRemove)
                {
                    if (MyInkCanvas.Children.Count > 1)
                        MyInkCanvas.Children.RemoveRange(1, MyInkCanvas.Children.Count);
                    if (MyInkCanvas.Strokes.Count > 0)
                        MyInkCanvas.Strokes.Clear();
                }

                string input = string.Empty;
                bool graphicChange = false;

                if (IsGraphicsChange || isChromaApplied)
                {
                    input = SaveXaml(ref graphicChange);
                }

                if (!graphicChange && !isChromaApplied)
                    input = null;

                if (input == null && ((FrameworkHelper.MyImageClass)(lstStrip.SelectedItem)).SettingStatus == SettingStatus.Spec)
                {
                    input = SpecLayeringUpdated;
                }

                if (IsEffectChange || IsGraphicsChange)
                    isEffectApplied = true;

                if (IsCropped)
                    ((FrameworkHelper.MyImageClass)lstStrip.Items[selectedIndex]).IsCropped = IsCropped;

                if (!IsModerate && IsEffectChange)
                {
                    //To Set EffectsXML in Image Effects Property
                    ((FrameworkHelper.MyImageClass)lstStrip.Items[selectedIndex]).NewEffectsXML = ImageEffect;
                    ((FrameworkHelper.MyImageClass)(lstStrip.SelectedItem)).SettingStatus = SettingStatus.SpecUpdated;

                    //SaveEffectsintoDB();
                }

                if (!IsModerate && (IsGraphicsChange || IsEffectChange || IsChromaChanged))
                {
                    if (string.IsNullOrWhiteSpace(input))
                    {
                        if (((FrameworkHelper.MyImageClass)(lstStrip.SelectedItem)).SettingStatus == SettingStatus.Spec)
                        {
                            ((FrameworkHelper.MyImageClass)(lstStrip.SelectedItem)).NewLayeringXML = SpecLayeringUpdated;
                            ((FrameworkHelper.MyImageClass)(lstStrip.SelectedItem)).NewEffectsXML = SpecImageEffect;
                        }
                        else if (((FrameworkHelper.MyImageClass)(lstStrip.SelectedItem)).SettingStatus == SettingStatus.None)
                        {
                            ((FrameworkHelper.MyImageClass)(lstStrip.SelectedItem)).NewLayeringXML = null;
                        }
                    }
                    else
                    {
                        ((FrameworkHelper.MyImageClass)(lstStrip.SelectedItem)).NewLayeringXML = input;
                        ((FrameworkHelper.MyImageClass)lstStrip.Items[selectedIndex]).NewEffectsXML = ImageEffect;
                        ((FrameworkHelper.MyImageClass)(lstStrip.SelectedItem)).SettingStatus = SettingStatus.SpecUpdated;
                        //SaveChangesToDB(input);
                    }
                }

                if (isChromaApplied && !IsModerate && (IsChromaChanged || isEffectApplied))
                {
                    //SaveGreenScreenImage();
                    if (IsChromaChanged)
                        IsChromaChanged = false;
                }
                ChromaGridLeft = 0;
                ChromaGridTop = 0;
                ChromaCenterX = 0;
                ChromaCenterY = 0;
                ChromaZoomFactor = 0;
                ChromaBorderPath = string.Empty;
                OriginalBorder = string.Empty;

                IsImageDirtyState = false;
                specproductType = null;
                ((ImageDownloader)Window.GetWindow(this)).UpdatedSelectedList = new ObservableCollection<MyImageClass>(lstMyImageClass);
                HideHandlerDialog();
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
            finally
            {
                MemoryManagement.FlushMemory();
                GC.AddMemoryPressure(10000);
            }

#if DEBUG
            if (watch != null)
                FrameworkHelper.CommonUtility.WatchStop(this.GetType().FullName + "." + System.Reflection.MethodBase.GetCurrentMethod().Name + "|", watch);
#endif
        }
        private void btnRevert_Click(object sender, RoutedEventArgs e)
        {
#if DEBUG
            var watch = FrameworkHelper.CommonUtility.Watch();
            watch.Start();
#endif
            if (IsImageDirtyState == true)
            {
                string message = "Do you want to revert the effects of the selected image to original effects?";
                string caption = "Alert Box";
                System.Windows.Forms.MessageBoxButtons buttons = System.Windows.Forms.MessageBoxButtons.YesNo;
                System.Windows.Forms.DialogResult result;
                result = System.Windows.Forms.MessageBox.Show(message, caption, buttons);

                if (result == System.Windows.Forms.DialogResult.Yes)
                {
                    if (semiOrderSettings != null && semiOrderSettings.DG_SemiOrder_Settings_Pkey > 0)
                    {
                        ((FrameworkHelper.MyImageClass)(lstStrip.SelectedItem)).NewLayeringXML = SpecLayeringUpdated;
                        ((FrameworkHelper.MyImageClass)(lstStrip.SelectedItem)).NewEffectsXML = SpecImageEffect;
                        ((FrameworkHelper.MyImageClass)(lstStrip.SelectedItem)).SettingStatus = SettingStatus.Spec;
                        if (semiOrderSettings.DG_SemiOrder_IsCropActive == true)
                            ((FrameworkHelper.MyImageClass)(lstStrip.SelectedItem)).IsCropped = true;
                    }
                    else
                    {
                        ((FrameworkHelper.MyImageClass)(lstStrip.SelectedItem)).NewLayeringXML = null;
                        ((FrameworkHelper.MyImageClass)(lstStrip.SelectedItem)).NewEffectsXML = "<image brightness = '0' contrast = '1' Crop='##' colourvalue = '##' rotate='##' ><effects sharpen='##' greyscale='0' digimagic='0' sepia='0' defog='0' underwater='0' emboss='0' invert = '0' granite='0' hue ='##' cartoon = '0'></effects></image>";
                        ((FrameworkHelper.MyImageClass)(lstStrip.SelectedItem)).SettingStatus = SettingStatus.None;
                        ((FrameworkHelper.MyImageClass)(lstStrip.SelectedItem)).IsCropped = false;
                    }
                    FrameworkHelper.MyImageClass objItem = ((FrameworkHelper.MyImageClass)(lstStrip.SelectedItem));
                    this.selectedIndex = lstStrip.Items.IndexOf(objItem);
                    ControlCleanup();
                    EditImage(objItem);
                }
            }
#if DEBUG
            if (watch != null)
                FrameworkHelper.CommonUtility.WatchStop(this.GetType().FullName + "." + System.Reflection.MethodBase.GetCurrentMethod().Name + "|", watch);
#endif
        }
        /// <summary>
        /// Handles the Click event of the btnRotateclick control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void btnRotateclick_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (SetRedeye)
                {
                    ellipse.Visibility = Visibility.Collapsed;
                    mainImage.Cursor = Cursors.Arrow;
                    SetRedeye = false;
                }

                AntiRotate(-1);
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }

        /// <summary>
        /// Handles the Click event of the btnrotateGraphics control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void btnrotateGraphics_Click(object sender, RoutedEventArgs e)
        {
            CmbProductType.Visibility = Visibility.Collapsed;
            if (jrotate.IsVisible)
            {
                jrotate.Visibility = System.Windows.Visibility.Hidden;
                spRotatepanel.Visibility = System.Windows.Visibility.Hidden;
            }
            else
            {
                jrotate.Visibility = System.Windows.Visibility.Visible;
                spRotatepanel.Visibility = System.Windows.Visibility.Visible;
            }
        }

        /// <summary>
        /// Handles the Click event of the RotateInButton control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void RotateInButton_Click(object sender, RoutedEventArgs e)
        {
            //strSender = "Button";
            CmbProductType.Visibility = Visibility.Collapsed;
            if (jrotate.IsVisible)
            {
                jrotate.Angle += 5;
            }
        }

        /// <summary>
        /// Handles the Click event of the AntiRotateButton control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void AntiRotateButton_Click(object sender, RoutedEventArgs e)
        {
            // strSender = "Button";
            CmbProductType.Visibility = Visibility.Collapsed;
            if (jrotate.IsVisible)
            {
                jrotate.Angle -= 5;
            }
        }

        /// <summary>
        /// Handles the Click event of the btnflip control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void btnflip_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (SetRedeye)
                {
                    ellipse.Visibility = Visibility.Collapsed;
                    mainImage.Cursor = Cursors.Arrow;
                    SetRedeye = false;
                }
                Flip(-1);
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }

        }

        private void Window_Closed(object sender, EventArgs e)
        {
            ClearResources();
            MemoryManagement.FlushMemory();
        }
        private void Window_Unloaded(object sender, RoutedEventArgs e)
        {
            MemoryManagement.FlushMemory();
        }
        /// <summary>
        /// Handles the Click event of the BringToFront control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void BringToFront_Click(object sender, RoutedEventArgs e)
        {
#if DEBUG
            var watch = FrameworkHelper.CommonUtility.Watch();
            watch.Start();
#endif
            CmbProductType.Visibility = Visibility.Collapsed;
            int count = dragCanvas.Children.Count;
            if (this.elementForContextMenu != null && this.elementForContextMenu is Button)
            {
                int ii = Canvas.GetZIndex(this.elementForContextMenu);
                if (ii == 1)
                {
                    Canvas.SetZIndex(this.elementForContextMenu, 5);
                }
                else
                {
                    Canvas.SetZIndex(this.elementForContextMenu, 7);
                }

                index++;
            }
            else if (this.elementForContextMenu != null && this.elementForContextMenu is TextBox)
            {
                int ii = Canvas.GetZIndex(this.elementForContextMenu);
                if (ii == 1)
                {
                    Canvas.SetZIndex(this.elementForContextMenu, 5);
                }
                else
                {
                    Canvas.SetZIndex(this.elementForContextMenu, 7);
                }
                index++;
            }
            else if (IsSelectedMainImage)
            {
                Canvas.SetZIndex(frm, 1);
                IsGraphicsChange = true;
                IsImageDirtyState = true;
            }

#if DEBUG
            if (watch != null)
                FrameworkHelper.CommonUtility.WatchStop(this.GetType().FullName + "." + System.Reflection.MethodBase.GetCurrentMethod().Name + "|", watch);
#endif
        }

        /// <summary>
        /// Handles the Click event of the SendToBack control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void SendToBack_Click(object sender, RoutedEventArgs e)
        {
#if DEBUG
            var watch = FrameworkHelper.CommonUtility.Watch();
            watch.Start();
#endif
            CmbProductType.Visibility = Visibility.Collapsed;
            if (this.elementForContextMenu != null && (this.elementForContextMenu is Button))
            {
                int ii = Canvas.GetZIndex(this.elementForContextMenu);

                if (ii == 7)
                {
                    Canvas.SetZIndex(this.elementForContextMenu, 5);
                }
                else
                {
                    Canvas.SetZIndex(this.elementForContextMenu, 1);
                }
            }
            else if (this.elementForContextMenu != null && (this.elementForContextMenu is TextBox))
            {
                int ii = Canvas.GetZIndex(this.elementForContextMenu);
                if (ii == 7)
                {
                    Canvas.SetZIndex(this.elementForContextMenu, 5);
                }
                else
                {
                    Canvas.SetZIndex(this.elementForContextMenu, 1);
                }
            }
            else if (IsSelectedMainImage)
            {
                Canvas.SetZIndex(frm, 6);
                IsGraphicsChange = true;
                IsImageDirtyState = true;
            }

#if DEBUG
            if (watch != null)
                FrameworkHelper.CommonUtility.WatchStop(this.GetType().FullName + "." + System.Reflection.MethodBase.GetCurrentMethod().Name + "|", watch);
#endif
        }

        private void btnStartMove_Click(object sender, RoutedEventArgs e)
        {
            dragCanvas.AllowDragging = true;
            dragCanvas.IsEnabled = true;
            IsGreenCorrection = false;
            MyInkCanvas.EditingMode = InkCanvasEditingMode.None;
            IsEraserActive = false;
            IsEraserDrawEllipseActive = false;
            IsEraserDrawRectangleActive = false;
        }

        private void btnMoveImage_Click(object sender, RoutedEventArgs e)
        {
            MoveImageStartStop();
        }
        private void btnClose1_Click(object sender, RoutedEventArgs e)
        {
            GrdPrint.Visibility = Visibility.Collapsed;
        }
        private void GrdBrightness_LayoutUpdated(object sender, EventArgs e)
        {
            if (MyInkCanvas.EditingMode == InkCanvasEditingMode.Ink)
                return;
            double canvasTop1 = (double)Opacitymsk.GetValue(Canvas.TopProperty);
            double canvasLeft1 = (double)Opacitymsk.GetValue(Canvas.LeftProperty);
            if (!canvasTop.Equals(canvasTop1) || !canvasLeft.Equals(canvasLeft1))
            {
                canvasTop = canvasTop1;
                canvasLeft = canvasLeft1;
                canbackgroundParent.SetValue(Canvas.TopProperty, canvasTop);
                canbackgroundParent.SetValue(Canvas.LeftProperty, canvasLeft);
            }
            //GetBriConData();
        }

        /// <summary>
        /// Handles the MouseEnter event of the MyInkCanvas control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="MouseEventArgs"/> instance containing the event data.</param>
        private void MyInkCanvas_MouseEnter(object sender, MouseEventArgs e)
        {
            mainImage.Cursor = Cursors.Arrow;
            return;
        }

        /// <summary>
        /// Handles the MouseEnter event of the CanvasInkInkCanvas control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="MouseEventArgs"/> instance containing the event data.</param>
        private void CanvasInkInkCanvas_MouseEnter(object sender, MouseEventArgs e)
        {
            TransformGroup tg = MyInkCanvas.GetValue(Canvas.RenderTransformProperty) as TransformGroup;

            if (tg != null && IsZoomed)
            {
                if (tg.Children.Count > 0)
                {
                    foreach (var grp in tg.Children)
                    {
                        if (grp is ScaleTransform)
                        {
                            inkCanvasEllipse.Width = inkCanvasRectangle.Width = attribute.Width * ((ScaleTransform)grp).Value.Determinant;
                            inkCanvasEllipse.Height = inkCanvasRectangle.Height = attribute.Height * ((ScaleTransform)grp).Value.Determinant;
                        }
                    }
                }
            }
            else if (tg != null)
            {
                if (tg.Children.Count > 0)
                {
                    foreach (var grp in tg.Children)
                    {
                        if (grp is ScaleTransform)
                        {
                            inkCanvasEllipse.Width = inkCanvasRectangle.Width = attribute.Width * ((ScaleTransform)grp).ScaleX;
                            inkCanvasEllipse.Height = inkCanvasRectangle.Height = attribute.Height * ((ScaleTransform)grp).ScaleY;
                        }
                    }
                }
            }

            inkCanvasEllipse.Fill = Brushes.Red;
            inkCanvasEllipse.StrokeThickness = 2;
            inkCanvasEllipse.Stroke = Brushes.Black;
            inkCanvasRectangle.Fill = Brushes.Red;
            MyInkCanvas.UseCustomCursor = true;

            if (!rectangleEraser)
            {
                MyInkCanvas.Cursor = CursorHelper.CreateCursor(inkCanvasEllipse as UIElement);
            }
            else
            {
                MyInkCanvas.Cursor = CursorHelper.CreateCursor(inkCanvasRectangle as UIElement);
            }
        }

        private void CmbProductType_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (previouscounter == -1)// && EffectsSender == null)
            {
                for (int index = 0; index < lstFrame.Items.Count; index++)
                {
                    DependencyObject obj = lstFrame.ItemContainerGenerator.ContainerFromIndex(index);
                    if (obj != null)
                    {
                        DependencyObject obj1 = FindVisualChild1<Grid>(obj);
                        if (obj1 != null)
                            EffectsSender = FindVisualChild<Button>(obj1, borderName);
                        if (EffectsSender != null)
                        {
                            lstFrame.SelectedIndex = index;
                            break;
                        }
                    }
                }
            }
            previouscounter = -1;
            Effects_Click(EffectsSender, new RoutedEventArgs());
        }
        #endregion

        #region CropWindow
        public string CropSize = "##";
        /// <summary>
        /// Handles the Click event of the btnCrop control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void btnCrop_Click(object sender, RoutedEventArgs e)
        {
#if DEBUG
            var watch = FrameworkHelper.CommonUtility.Watch();
            watch.Start();
#endif
            try
            {
                dragCanvas.IsEnabled = true;
                if (SetRedeye)
                {
                    ellipse.Visibility = Visibility.Collapsed;
                    mainImage.Cursor = Cursors.Arrow;
                    SetRedeye = false;
                }
                GrdsubCrop.Visibility = Visibility.Visible;
                lstStrip.Visibility = Visibility.Collapsed;
                GrdEffects.Visibility = Visibility.Collapsed;
                GrdsubColoreffects.Visibility = System.Windows.Visibility.Hidden;

                GrdFlip.LayoutTransform = new TransformGroup();
                GrdRotate.LayoutTransform = new TransformGroup();
                grdZoomCanvas.Visibility = System.Windows.Visibility.Collapsed;
                GrdRotateCropParent.Visibility = System.Windows.Visibility.Visible;
                Zomout(true);
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
            finally
            {
                MemoryManagement.FlushMemory();
            }

#if DEBUG
            if (watch != null)
                FrameworkHelper.CommonUtility.WatchStop(this.GetType().FullName + "." + System.Reflection.MethodBase.GetCurrentMethod().Name + "|", watch);
#endif
        }
        bool IsCropped = false;
        /// <summary>
        /// Crops this instance.
        /// </summary>
        private void Crop()
        {
#if DEBUG
            var watch = FrameworkHelper.CommonUtility.Watch();
            watch.Start();
#endif
            try
            {
                if (_clp == null)
                {
                    if (rotateangle > 0 || FlipMode != 0)
                        IsCropped = true;
                    return;
                }
                VisualStateManager.GoToState(btnCrop, "Checked", true);
                Rect rc = _clp.ClippingRectangle;
                BitmapSource bitmapsource = _clp.BpsCrop();

                widthimg.Source = bitmapsource;
                mainImageundo.Source = mainImage.Source = bitmapsource;
                imgRotateCrop.Source = bitmapsource;
                imgRotateCrop.UpdateLayout();

                forWdht.HorizontalAlignment = System.Windows.HorizontalAlignment.Center;
                forWdht.VerticalAlignment = System.Windows.VerticalAlignment.Center;

                GrdBrightness.VerticalAlignment = System.Windows.VerticalAlignment.Center;
                GrdBrightness.HorizontalAlignment = System.Windows.HorizontalAlignment.Center;

                RemoveCropFromCur();
                _felCur = null;

                IsCropped = true;
                _clp = null;
                AuditLog.AddUserLog(Common.LoginUser.UserId, (int)FrameworkHelper.ActionType.EditPhoto, "Crop " + txtMainImage.Text + " image.");
                IsGraphicsChange = true;
                bitmapsource = null;
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }

#if DEBUG
            if (watch != null)
                FrameworkHelper.CommonUtility.WatchStop(this.GetType().FullName + "." + System.Reflection.MethodBase.GetCurrentMethod().Name + "|", watch);
#endif
        }

        /// <summary>
        /// Handles the Click event of the OkButton control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void OkButton_Click(object sender, RoutedEventArgs e)
        {
#if DEBUG
            var watch = FrameworkHelper.CommonUtility.Watch();
            watch.Start();
#endif
            try
            {
                IsImageDirtyState = true;
                btnSelectReverse.IsEnabled = false;
                Crop();
                btnrotate.IsEnabled = true;
                btnrotate.IsEnabled = true;
                btnflip.IsEnabled = true;

                if (rotateangle > 0)
                {
                    VisualStateManager.GoToState(btnrotate, "Checked", true);
                    VisualStateManager.GoToState(btnantirotate, "Checked", true);
                    btnCrop.IsEnabled = false;
                }
                else
                {
                    VisualStateManager.GoToState(btnrotate, "Unchecked", true);
                    VisualStateManager.GoToState(btnantirotate, "Unchecked", true);
                }
                btnantirotate.IsEnabled = true;
                btnAddgraphics.IsEnabled = true;
                GrdsubCrop.Visibility = Visibility.Hidden;
                lstStrip.Visibility = Visibility.Visible;
                GrdEffects.Visibility = Visibility.Visible;
                SaveXml("Crop", CropSize, false);
                if (IsCropped)
                {
                    //SaveCropEffectsintoDB();
                    btnCrop.IsEnabled = false;
                    IsEffectChange = true;

                }
                imgRotateCrop.UpdateLayout();
                GrdZomout.InvalidateArrange();
                if (IsCropped || FlipMode != 0 || FlipModeY != 0 || rotateangle > 0)//change done..
                {
                    RenderTargetBitmap rtb1 = CaptureScreenForCrop(GrdZomout, CroppingAdorner.s_dpiX, CroppingAdorner.s_dpiY, false);
                    SaveCropNRedEyeImage(rtb1, "Crop");

                    using (FileStream fileStream = File.OpenRead((System.IO.Path.Combine(CropFolderPath, tempfilename))))
                    {
                        BitmapImage bi = new BitmapImage();
                        MemoryStream ms = new MemoryStream();
                        fileStream.CopyTo(ms);
                        ms.Seek(0, SeekOrigin.Begin);
                        fileStream.Close();
                        bi.BeginInit();
                        bi.StreamSource = ms;
                        bi.EndInit();

                        mainImageundo.Source = mainImage.Source = bi;
                        widthimg.Source = bi;
                        imgRotateCrop.Source = bi;
                        imgoriginal.Source = bi;
                        CroppingAdorner.s_dpiX = bi.DpiX;
                        CroppingAdorner.s_dpiY = bi.DpiY;
                        bi.Freeze();
                        //DisposeImage(bi);
                    }
                }

                forWdht.Height = widthimg.Source.Height;
                forWdht.Width = widthimg.Source.Width;

                grdZoomCanvas.Visibility = System.Windows.Visibility.Visible;
                GrdRotateCropParent.Visibility = System.Windows.Visibility.Collapsed;
                Zomout(true);
                //Window_Loaded(sender, e);
                GrdRotate.LayoutTransform = new TransformGroup();
                //GrdZomout.RenderTransform = new TransformGroup();

                if (FlipMode != 0 || FlipModeY != 0)
                {
                    btnCrop.IsEnabled = false;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
            finally
            {
                MemoryManagement.FlushMemory();
            }
            //GrdsubMain.Visibility = Visibility.Visible;
#if DEBUG
            if (watch != null)
                FrameworkHelper.CommonUtility.WatchStop(this.GetType().FullName + "." + System.Reflection.MethodBase.GetCurrentMethod().Name + "|", watch);
#endif
        }
        /// <summary>
        /// Adds the crop automatic element.
        /// </summary>
        /// <param name="fel">The fel.</param>
        /// <param name="IsRedeye">if set to <c>true</c> [is redeye].</param>
        /// <param name="cropImage">The crop image.</param>
        /// <param name="aspectRatio">The aspect ratio.</param>
        private void AddCropToElement(FrameworkElement fel, bool IsRedeye, System.Windows.Controls.Image cropImage, decimal aspectRatio)
        {
#if DEBUG
            var watch = FrameworkHelper.CommonUtility.Watch();
            watch.Start();
#endif

            try
            {
                if (_felCur != null)
                {
                    RemoveCropFromCur();
                }
                if (!IsRedeye)
                {
                    Rect rcInterior = new Rect();
                    rcInterior = GetCropRectangle(aspectRatio);
                    AdornerLayer aly = AdornerLayer.GetAdornerLayer(GrdCrop);
                    _clp = new CroppingAdorner(GrdCrop, rcInterior);
                    aly.Add(_clp);
                    _felCur = GrdCrop;
                    SetClipColorGrey();
                }
                else
                {
                    Rect rcInterior = new Rect(
                      62.671083040935912,
                     38.358400000000017,
                      60.671083040935912,
                     38.358400000000017);
                    AdornerLayer aly = AdornerLayer.GetAdornerLayer(GrdCrop);
                    _clp = new CroppingAdorner(GrdCrop, rcInterior);
                    aly.Add(_clp);
                    _felCur = GrdCrop;
                    SetClipColorGrey();
                }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
            finally
            {
                MemoryManagement.FlushMemory();
            }
#if DEBUG
            if (watch != null)
                FrameworkHelper.CommonUtility.WatchStop(this.GetType().FullName + "." + System.Reflection.MethodBase.GetCurrentMethod().Name + "|", watch);
#endif
        }

        /// <summary>
        /// Sets the clip color grey.
        /// </summary>
        private void SetClipColorGrey()
        {
            try
            {
                if (_clp != null)
                {
                    System.Windows.Media.Color clr = Colors.Black;
                    clr.A = 110;
                    _clp.Fill = new SolidColorBrush(clr);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }

        /// <summary>
        /// Removes the crop from current.
        /// </summary>
        private void RemoveCropFromCur()
        {
            try
            {
                AdornerLayer aly = AdornerLayer.GetAdornerLayer(_felCur);
                aly.Remove(_clp);
                aly.ReleaseAllTouchCaptures();
                _felCur.ReleaseAllTouchCaptures();
                _clp.ReleaseAllTouchCaptures();
                _clp.ReleaseResources(_felCur);
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }

        /// <summary>
        /// Gets the crop rectangle.
        /// </summary>
        /// <param name="constraintRatio">The constraint ratio.</param>
        /// <returns></returns>
        private Rect GetCropRectangle(decimal constraintRatio)
        {
#if DEBUG
            var watch = FrameworkHelper.CommonUtility.Watch();
            watch.Start();
#endif
            Rect rcInterior = new Rect();
            try
            {
                double cropImageActualWidth = GrdCrop.ActualWidth;
                double cropImageActualHeight = GrdCrop.ActualHeight;
                double imageRatio = GrdCrop.ActualWidth / GrdCrop.ActualHeight;

                if (constraintRatio == 0.8M) //for 8*10 crop
                {
                    double rectheight = cropImageActualHeight / 2;
                    double rectweight = ((double)8 / 10) * rectheight;

                    rcInterior = new Rect(0, 0, rectweight, rectheight);

                    while (!(rcInterior.X + rcInterior.Width > GrdCrop.ActualWidth) && !(rcInterior.Y + rcInterior.Height > GrdCrop.ActualHeight))
                    {
                        rcInterior.Scale(1.01, 1.01);
                    }
                }
                else if (constraintRatio == 0.75M) // for 6*8 crop
                {
                    double rectheight = cropImageActualHeight / 2;
                    double rectweight = ((double)6 / 8) * rectheight;

                    rcInterior = new Rect(0, 0, rectweight, rectheight);
                    while (!(rcInterior.X + rcInterior.Width > GrdCrop.ActualWidth) && !(rcInterior.Y + rcInterior.Height > GrdCrop.ActualHeight))
                    {
                        rcInterior.Scale(1.01, 1.01);
                    }
                }
                else if (constraintRatio == 0.66M) // for 4*6 crop
                {
                    double rectheight = cropImageActualHeight / 2;
                    double rectweight = ((double)2 / 3) * rectheight;

                    rcInterior = new Rect(0, 0, rectweight, rectheight);

                    while (!(rcInterior.X + rcInterior.Width > GrdCrop.ActualWidth) && !(rcInterior.Y + rcInterior.Height > GrdCrop.ActualHeight))
                    {
                        rcInterior.Scale(1.01, 1.01);
                    }
                }

                else if (constraintRatio == 1.0M) // for 3*3 crop
                {
                    double rectheight = cropImageActualHeight / 2;
                    double rectweight = ((double)3 / 3) * rectheight;

                    rcInterior = new Rect(0, 0, rectweight, rectheight);

                    while (!(rcInterior.X + rcInterior.Width > GrdCrop.ActualWidth) && !(rcInterior.Y + rcInterior.Height > GrdCrop.ActualHeight))
                    {
                        rcInterior.Scale(1.01, 1.01);
                    }
                }
                else if (constraintRatio == 1.5M) // For 6 * 4 crop
                {
                    double rectheight = cropImageActualHeight / 2;
                    double rectweight = ((double)6 / 4) * rectheight;

                    rcInterior = new Rect(0, 0, rectweight, rectheight);

                    while (!(rcInterior.X + rcInterior.Width > GrdCrop.ActualWidth) && !(rcInterior.Y + rcInterior.Height > GrdCrop.ActualHeight))
                    {
                        rcInterior.Scale(1.01, 1.01);
                    }
                }
                else if (constraintRatio == 1.25M) // For 10 * 8 crop
                {
                    double rectheight = cropImageActualHeight / 2;
                    double rectweight = ((double)10 / 8) * rectheight;

                    rcInterior = new Rect(0, 0, rectweight, rectheight);

                    while (!(rcInterior.X + rcInterior.Width > GrdCrop.ActualWidth) && !(rcInterior.Y + rcInterior.Height > GrdCrop.ActualHeight))
                    {
                        rcInterior.Scale(1.01, 1.01);
                    }
                }
                else if (constraintRatio == 1.33M) // For 8*6 crop
                {
                    double rectheight = cropImageActualHeight / 2;
                    double rectweight = ((double)8 / 6) * rectheight;

                    rcInterior = new Rect(0, 0, rectweight, rectheight);

                    while (!(rcInterior.X + rcInterior.Width > GrdCrop.ActualWidth) && !(rcInterior.Y + rcInterior.Height > GrdCrop.ActualHeight))
                    {
                        rcInterior.Scale(1.01, 1.01);
                    }
                }
                else if (constraintRatio == 0.714M) //For 5*7 crop
                {
                    double rectheight = cropImageActualHeight / 2;
                    double rectweight = ((double)5 / 7) * rectheight;

                    rcInterior = new Rect(0, 0, rectweight, rectheight);

                    while (!(rcInterior.X + rcInterior.Width > GrdCrop.ActualWidth) && !(rcInterior.Y + rcInterior.Height > GrdCrop.ActualHeight))
                    {
                        rcInterior.Scale(1.01, 1.01);
                    }
                }
                else if (constraintRatio == 1.4M)// For 7*5 crop
                {
                    double rectheight = cropImageActualHeight / 2;
                    double rectweight = ((double)7 / 5) * rectheight;

                    rcInterior = new Rect(0, 0, rectweight, rectheight);

                    while (!(rcInterior.X + rcInterior.Width > GrdCrop.ActualWidth) && !(rcInterior.Y + rcInterior.Height > GrdCrop.ActualHeight))
                    {
                        rcInterior.Scale(1.01, 1.01);
                    }
                }
                else
                {
                    rcInterior = new Rect(0, 0, GrdCrop.ActualWidth, GrdCrop.ActualHeight);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                rcInterior = new Rect(0, 0, GrdCrop.ActualWidth, GrdCrop.ActualHeight);
            }
            finally
            {
                MemoryManagement.FlushMemory();
#if DEBUG
                if (watch != null)
                    FrameworkHelper.CommonUtility.WatchStop(this.GetType().FullName + "." + System.Reflection.MethodBase.GetCurrentMethod().Name + "|", watch);
#endif
            }
            return rcInterior;
        }

        /// <summary>
        /// Handles the Click event of the btnenlargeplus control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void btnenlargeplus_Click(object sender, RoutedEventArgs e)
        {
#if DEBUG
            var watch = FrameworkHelper.CommonUtility.Watch();
            watch.Start();
#endif

            try
            {
                if (_clp == null)
                    return;

                Rect rcInterior = _clp.ClippingRectangle;
                double grdActualWidth = GrdCrop.ActualWidth;
                double grdActualheight = GrdCrop.ActualHeight;

                rcInterior = new Rect(rcInterior.X, rcInterior.Y, rcInterior.Width, rcInterior.Height);

                double x = rcInterior.X;
                double y = rcInterior.Y;
                AdornerLayer aly = AdornerLayer.GetAdornerLayer(GrdCrop);
                rcInterior.Scale(1.01, 1.01);
                rcInterior.X = x - x * 0.01 / 2;
                rcInterior.Y = y - y * 0.01 / 2;

                if (rcInterior.X + rcInterior.Width > grdActualWidth)
                {
                    return;
                }

                if (rcInterior.Y + rcInterior.Height > grdActualheight)
                {
                    return;
                }

                if (_felCur != null)
                {
                    RemoveCropFromCur();
                }

                if (_clp == null)
                    return;

                if (rcInterior.X < -3)
                {
                    rcInterior.X = -3;
                }

                if (rcInterior.Y < -3)
                {
                    rcInterior.Y = -3;
                }

                _clp = new CroppingAdorner(GrdCrop, rcInterior);

                aly.Add(_clp);
                _felCur = GrdCrop;
                SetClipColorGrey();
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
#if DEBUG
            if (watch != null)
                FrameworkHelper.CommonUtility.WatchStop(this.GetType().FullName + "." + System.Reflection.MethodBase.GetCurrentMethod().Name + "|", watch);
#endif
        }

        /// <summary>
        /// Handles the Click event of the btnenlargeminus control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void btnenlargeminus_Click(object sender, RoutedEventArgs e)
        {
#if DEBUG
            var watch = FrameworkHelper.CommonUtility.Watch();
            watch.Start();
#endif
            try
            {
                if (_felCur != null)
                {
                    RemoveCropFromCur();
                }

                if (_clp == null)
                    return;

                Rect rcInterior = _clp.ClippingRectangle;

                rcInterior = new Rect(rcInterior.X, rcInterior.Y, rcInterior.Width, rcInterior.Height);

                double x = rcInterior.X;
                double y = rcInterior.Y;
                AdornerLayer aly = AdornerLayer.GetAdornerLayer(GrdCrop);
                rcInterior.Width -= rcInterior.Width * 0.01;
                rcInterior.Height -= rcInterior.Height * 0.01;

                rcInterior.X = x + x * 0.01 / 2;
                rcInterior.Y = y + y * 0.01 / 2;

                if (rcInterior.Width <= 200)
                {
                    rcInterior.Width = 200;
                    rcInterior.X = x;
                    //fix the width of rectangle.
                }
                if (rcInterior.Height <= 200)
                {
                    rcInterior.Height = 200;
                    rcInterior.Y = y;
                }

                _clp = new CroppingAdorner(GrdCrop, rcInterior);

                aly.Add(_clp);
                _felCur = GrdCrop;
                SetClipColorGrey();
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
#if DEBUG
            if (watch != null)
                FrameworkHelper.CommonUtility.WatchStop(this.GetType().FullName + "." + System.Reflection.MethodBase.GetCurrentMethod().Name + "|", watch);
#endif
        }

        /// <summary>
        /// Handles the Click event of the btnselect control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void btnselect_Click(object sender, RoutedEventArgs e)
        {
#if DEBUG
            var watch = FrameworkHelper.CommonUtility.Watch();
            watch.Start();
#endif
            btnSelectReverse.IsEnabled = true;
            if (mainImage.ActualWidth < mainImage.ActualHeight)
            {
                CropSize = "6 * 8";
                try
                {
                    Button eventsender = (Button)sender;

                    {
                        decimal aspectRatio = 0.75M;

                        if (eventsender.Tag != null)
                        {
                            AddCropToElement(GrdCrop, true, mainImage, aspectRatio);
                        }
                        else
                        {
                            AddCropToElement(GrdCrop, false, mainImage, aspectRatio);
                        }
                        _brOriginal = _clp.Fill;

                    }
                }
                catch (Exception ex)
                {
                    string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                    ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                }
            }
            else
            {
                CropSize = "8 * 6";

                try
                {
                    Button eventsender = (Button)sender;

                    {
                        decimal aspectRatio = 1.33M;

                        if (eventsender.Tag != null)
                        {
                            AddCropToElement(GrdCrop, true, mainImage, aspectRatio);
                        }
                        else
                        {
                            AddCropToElement(GrdCrop, false, mainImage, aspectRatio);
                        }
                        _brOriginal = _clp.Fill;
                    }
                }
                catch (Exception ex)
                {
                    string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                    ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                }
            }
#if DEBUG
            if (watch != null)
                FrameworkHelper.CommonUtility.WatchStop(this.GetType().FullName + "." + System.Reflection.MethodBase.GetCurrentMethod().Name + "|", watch);
#endif
        }
        /// <summary>
        /// Handles the Click event of the btnselectfourBySix control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void btnselectfourBySix_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Button eventsender = (Button)sender;

                {

                    decimal aspectRatio = 0.8M;// 0.666666666666667M;
                    if (eventsender.Tag != null)
                    {
                        AddCropToElement(GrdCrop, true, mainImage, aspectRatio);
                    }
                    else
                    {
                        AddCropToElement(GrdCrop, false, mainImage, aspectRatio);
                    }
                    _brOriginal = _clp.Fill;

                }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }
        /// <summary>
        /// Handles the Click event of the btnselectEightByEleven control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void btnselectEightByEleven_Click(object sender, RoutedEventArgs e)
        {
#if DEBUG
            var watch = FrameworkHelper.CommonUtility.Watch();
            watch.Start();
#endif
            btnSelectReverse.IsEnabled = true;
            Button eventsender = (Button)sender;
            if (mainImage.ActualWidth < mainImage.ActualHeight)
            {
                CropSize = "8 * 10";
                try
                {
                    decimal aspectRatio = 0.8M;// 0.772727272727273M;
                    if (eventsender.Tag != null)
                    {
                        AddCropToElement(GrdCrop, true, mainImage, aspectRatio);
                    }
                    else
                    {
                        AddCropToElement(GrdCrop, false, mainImage, aspectRatio);
                    }
                    _brOriginal = _clp.Fill;
                }
                catch (Exception ex)
                {
                    string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                    ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                }
            }
            else
            {
                CropSize = "10 * 8";
                try
                {
                    decimal aspectRatio = 1.25M;// 0.772727272727273M;
                    if (eventsender.Tag != null)
                    {
                        AddCropToElement(GrdCrop, true, mainImage, aspectRatio);
                    }
                    else
                    {
                        AddCropToElement(GrdCrop, false, mainImage, aspectRatio);
                    }
                    _brOriginal = _clp.Fill;
                }
                catch (Exception ex)
                {
                    string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                    ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                }
            }
#if DEBUG
            if (watch != null)
                FrameworkHelper.CommonUtility.WatchStop(this.GetType().FullName + "." + System.Reflection.MethodBase.GetCurrentMethod().Name + "|", watch);
#endif
        }

        /// <summary>
        /// Handles the Click event of the btnSelectReverse control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void btnSelectReverse_Click(object sender, RoutedEventArgs e)
        {
#if DEBUG
            var watch = FrameworkHelper.CommonUtility.Watch();
            watch.Start();
#endif
            if (CropSize == "8 * 10")
            {
                CropSize = "10 * 8";
                try
                {
                    Button eventsender = (Button)sender;

                    {
                        decimal aspectRatio = 1.25M;// 0.772727272727273M;
                        if (eventsender.Tag != null)
                        {
                            AddCropToElement(Grdcartoonize, true, mainImage, aspectRatio);
                        }
                        else
                        {
                            AddCropToElement(Grdcartoonize, false, mainImage, aspectRatio);
                        }
                        _brOriginal = _clp.Fill;

                    }
                }
                catch (Exception ex)
                {
                    string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                    ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                }
            }
            else if (CropSize == "10 * 8")
            {
                CropSize = "8 * 10";
                try
                {
                    Button eventsender = (Button)sender;

                    {
                        decimal aspectRatio = 0.8M;// 0.772727272727273M;
                        if (eventsender.Tag != null)
                        {
                            AddCropToElement(Grdcartoonize, true, mainImage, aspectRatio);
                        }
                        else
                        {
                            AddCropToElement(Grdcartoonize, false, mainImage, aspectRatio);
                        }
                        _brOriginal = _clp.Fill;

                    }
                }
                catch (Exception ex)
                {
                    string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                    ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                }
            }
            else if (CropSize == "6 * 8")
            {
                CropSize = "8 * 6";
                try
                {
                    Button eventsender = (Button)sender;

                    {
                        decimal aspectRatio = 1.33M;// 0.772727272727273M;
                        if (eventsender.Tag != null)
                        {
                            AddCropToElement(Grdcartoonize, true, mainImage, aspectRatio);
                        }
                        else
                        {
                            AddCropToElement(Grdcartoonize, false, mainImage, aspectRatio);
                        }
                        _brOriginal = _clp.Fill;

                    }
                }
                catch (Exception ex)
                {
                    string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                    ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                }
            }
            else if (CropSize == "8 * 6")
            {
                CropSize = "6 * 8";
                try
                {
                    Button eventsender = (Button)sender;

                    {
                        decimal aspectRatio = 0.75M;// 0.772727272727273M;
                        if (eventsender.Tag != null)
                        {
                            AddCropToElement(Grdcartoonize, true, mainImage, aspectRatio);
                        }
                        else
                        {
                            AddCropToElement(Grdcartoonize, false, mainImage, aspectRatio);
                        }
                        _brOriginal = _clp.Fill;

                    }
                }
                catch (Exception ex)
                {
                    string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                    ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                }
            }
            else if (CropSize == "4 * 6")
            {
                CropSize = "6 * 4";
                try
                {
                    Button eventsender = (Button)sender;

                    {
                        decimal aspectRatio = 1.5M;// 0.772727272727273M;
                        if (eventsender.Tag != null)
                        {
                            AddCropToElement(Grdcartoonize, true, mainImage, aspectRatio);
                        }
                        else
                        {
                            AddCropToElement(Grdcartoonize, false, mainImage, aspectRatio);
                        }
                        _brOriginal = _clp.Fill;

                    }
                }
                catch (Exception ex)
                {
                    string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                    ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                }
            }
            else if (CropSize == "6 * 4")
            {
                CropSize = "4 * 6";
                try
                {
                    Button eventsender = (Button)sender;
                    {
                        decimal aspectRatio = .66M;// 0.772727272727273M;
                        if (eventsender.Tag != null)
                        {
                            AddCropToElement(Grdcartoonize, true, mainImage, aspectRatio);
                        }
                        else
                        {
                            AddCropToElement(Grdcartoonize, false, mainImage, aspectRatio);
                        }
                        _brOriginal = _clp.Fill;

                    }
                }
                catch (Exception ex)
                {
                    string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                    ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                }
            }
            else if (CropSize == "5 * 7")
            {
                CropSize = "7 * 5";
                try
                {
                    Button eventsender = (Button)sender;
                    {
                        decimal aspectRatio = 1.4M;// 0.772727272727273M;
                        if (eventsender.Tag != null)
                        {
                            AddCropToElement(Grdcartoonize, true, mainImage, aspectRatio);
                        }
                        else
                        {
                            AddCropToElement(Grdcartoonize, false, mainImage, aspectRatio);
                        }
                        _brOriginal = _clp.Fill;
                    }
                }
                catch (Exception ex)
                {
                    string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                    ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                }
            }
            else if (CropSize == "7 * 5")
            {
                CropSize = "5 * 7";
                try
                {
                    Button eventsender = (Button)sender;
                    {
                        decimal aspectRatio = .714M;// 0.772727272727273M;
                        if (eventsender.Tag != null)
                        {
                            AddCropToElement(Grdcartoonize, true, mainImage, aspectRatio);
                        }
                        else
                        {
                            AddCropToElement(Grdcartoonize, false, mainImage, aspectRatio);
                        }
                        _brOriginal = _clp.Fill;
                    }
                }
                catch (Exception ex)
                {
                    string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                    ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                }
            }
            else if (CropSize == "3 * 3")
            {
                CropSize = "3 * 3";
                try
                {
                    Button eventsender = (Button)sender;

                    {
                        decimal aspectRatio = 1.0M;// 0.772727272727273M;
                        if (eventsender.Tag != null)
                        {
                            AddCropToElement(Grdcartoonize, true, mainImage, aspectRatio);
                        }
                        else
                        {
                            AddCropToElement(Grdcartoonize, false, mainImage, aspectRatio);
                        }
                        _brOriginal = _clp.Fill;

                    }
                }
                catch (Exception ex)
                {
                    string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                    ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                }
            }


#if DEBUG
            if (watch != null)
                FrameworkHelper.CommonUtility.WatchStop(this.GetType().FullName + "." + System.Reflection.MethodBase.GetCurrentMethod().Name + "|", watch);
#endif


        }

        /// <summary>
        /// Handles the Click event of the btnSelectCrop4by6 control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void btnSelectCrop4by6_Click(object sender, RoutedEventArgs e)
        {
#if DEBUG
            var watch = FrameworkHelper.CommonUtility.Watch();
            watch.Start();
#endif

            Button eventsender = (Button)sender;
            btnSelectReverse.IsEnabled = true;
            if (mainImage.ActualWidth < mainImage.ActualHeight)
            {
                CropSize = "4 * 6";
                try
                {
                    decimal aspectRatio = .66M;// 0.772727272727273M;
                    if (eventsender.Tag != null)
                    {
                        AddCropToElement(Grdcartoonize, true, mainImage, aspectRatio);
                    }
                    else
                    {
                        AddCropToElement(Grdcartoonize, false, mainImage, aspectRatio);
                    }
                    _brOriginal = _clp.Fill;
                }
                catch (Exception ex)
                {
                    string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                    ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                }
            }
            else
            {
                CropSize = "6 * 4";
                try
                {
                    decimal aspectRatio = 1.5M;// 0.772727272727273M;
                    if (eventsender.Tag != null)
                    {
                        AddCropToElement(Grdcartoonize, true, mainImage, aspectRatio);
                    }
                    else
                    {
                        AddCropToElement(Grdcartoonize, false, mainImage, aspectRatio);
                    }
                    _brOriginal = _clp.Fill;
                }
                catch (Exception ex)
                {
                    string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                    ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                }
            }


#if DEBUG
            if (watch != null)
                FrameworkHelper.CommonUtility.WatchStop(this.GetType().FullName + "." + System.Reflection.MethodBase.GetCurrentMethod().Name + "|", watch);
#endif
        }

        /// <summary>
        /// Handles the Click event of the btnSelectCrop5by7 control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void btnSelectCrop5by7_Click(object sender, RoutedEventArgs e)
        {
#if DEBUG
            var watch = FrameworkHelper.CommonUtility.Watch();
            watch.Start();
#endif

            Button eventsender = (Button)sender;
            btnSelectReverse.IsEnabled = true;
            if (mainImage.ActualWidth < mainImage.ActualHeight)
            {
                CropSize = "5 * 7";
                try
                {
                    decimal aspectRatio = .714M;
                    if (eventsender.Tag != null)
                    {
                        AddCropToElement(Grdcartoonize, true, mainImage, aspectRatio);
                    }
                    else
                    {
                        AddCropToElement(Grdcartoonize, false, mainImage, aspectRatio);
                    }
                    _brOriginal = _clp.Fill;
                }
                catch (Exception ex)
                {
                    string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                    ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                }
            }
            else
            {
                CropSize = "7 * 5";
                try
                {
                    decimal aspectRatio = 1.4M;
                    if (eventsender.Tag != null)
                    {
                        AddCropToElement(Grdcartoonize, true, mainImage, aspectRatio);
                    }
                    else
                    {
                        AddCropToElement(Grdcartoonize, false, mainImage, aspectRatio);
                    }
                    _brOriginal = _clp.Fill;
                }
                catch (Exception ex)
                {
                    string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                    ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                }
            }
#if DEBUG
            if (watch != null)
                FrameworkHelper.CommonUtility.WatchStop(this.GetType().FullName + "." + System.Reflection.MethodBase.GetCurrentMethod().Name + "|", watch);
#endif
        }

        private void btnSelectCrop3by3_Click(object sender, RoutedEventArgs e)
        {
            CropSize = "3 * 3";
            btnSelectReverse.IsEnabled = true;

            try
            {
                Button eventsender = (Button)sender;

                {
                    decimal aspectRatio = 1.0M;
                    if (eventsender.Tag != null)
                    {
                        AddCropToElement(Grdcartoonize, true, mainImage, aspectRatio);
                    }
                    else
                    {
                        AddCropToElement(Grdcartoonize, false, mainImage, aspectRatio);
                    }
                    _brOriginal = _clp.Fill;

                }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }

        #endregion

        #region Brightness and Contrast
        /// <summary>
        /// Handles the Click event of the btnColorEffects control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void btnColorEffects_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (SetRedeye)
                {
                    ellipse.Visibility = Visibility.Collapsed;
                    mainImage.Cursor = Cursors.Arrow;
                    SetRedeye = false;
                }
                GrdsubColoreffects.Visibility = Visibility.Visible;
                GrdsubCrop.Visibility = Visibility.Hidden;
                ColorEffectsIMG.Visibility = Visibility.Collapsed;
                IMGFrame.Visibility = Visibility.Visible;
                //CurrentSelectedImage = CurrentImage;
                //using (FileStream fileStream = File.OpenRead(System.IO.Path.Combine(BigThumnailFolderPath, DateFolder, tempfilename)))
                //{
                //    BitmapImage bi = new BitmapImage();
                //    MemoryStream ms = new MemoryStream();
                //    fileStream.CopyTo(ms);
                //    ms.Seek(0, SeekOrigin.Begin);
                //    fileStream.Close();
                //    bi.BeginInit();
                //    bi.StreamSource = ms;
                //    bi.EndInit();
                //    bi.Freeze();
                //    imgoriginal.Source = bi;
                //    //DisposeImage(bi);
                //}
                DisableSideButton();
                //GetBriConData();
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
            finally
            {
                MemoryManagement.FlushMemory();
            }
        }

        /// <summary>
        /// Handles the Click event of the BrightnessPlus control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void BrightnessPlus_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                bright = bright + 0.01;
                _brighteff.Brightness = bright;
                _brighteff.Contrast = cont;
                GrdBrightness.Effect = _brighteff;
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
            finally
            {
                MemoryManagement.FlushMemory();
            }
        }


        /// <summary>
        /// Handles the Click event of the BrightnessMinus control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void BrightnessMinus_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                bright = bright - 0.01;
                _brighteff.Brightness = bright;
                _brighteff.Contrast = cont;
                GrdBrightness.Effect = _brighteff;
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
            finally
            {
                MemoryManagement.FlushMemory();
            }
        }
        /// <summary>
        /// Handles the Click event of the ContrastPlus control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void ContrastPlus_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (cont == 0)
                {
                    cont = 1;
                }
                cont = cont + .05;
                _brighteff.Contrast = cont;

                GrdBrightness.Effect = _brighteff;
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
            finally
            {
                MemoryManagement.FlushMemory();
            }
        }
        /// <summary>
        /// Handles the Click event of the ContrastMinus control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void ContrastMinus_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (cont > 0)
                {
                    cont = cont - .05;
                    _brighteff.Contrast = cont;

                    GrdBrightness.Effect = _brighteff;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
            finally
            {
                MemoryManagement.FlushMemory();
            }

        }

        /// <summary>
        /// Handles the Click event of the OkButtoncolor control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void OkButtoncolor_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                IsImageDirtyState = true;
                GrdsubColoreffects.Visibility = Visibility.Collapsed;

                GrdsubColoreffects.Visibility = Visibility.Collapsed;
                ColorEffectsIMG.Visibility = Visibility.Collapsed;
                IMGFrame.Visibility = Visibility.Visible;
                SaveXml("brightness", bright.ToString(), false);
                SaveXml("contrast", cont.ToString(), false);

                currentbrightness = bright;
                currentcontrast = cont;

                IsEffectChange = true;

                if (bright == 0 && cont == 1)
                {
                    VisualStateManager.GoToState(btnColorEffects, "Unchecked", true);
                }
                else
                {
                    VisualStateManager.GoToState(btnColorEffects, "Checked", true);
                }

                EnableSideButton();
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }
        /// <summary>
        /// Handles the Click event of the CancelButtoncolor control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void CancelButtoncolor_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                GrdsubColoreffects.Visibility = Visibility.Collapsed;
                GrdsubCrop.Visibility = Visibility.Hidden;
                ColorEffectsIMG.Visibility = Visibility.Collapsed;
                IMGFrame.Visibility = Visibility.Visible;

                //CurrentImage = CurrentSelectedImage;

                if (bright > 0 && currentbrightness != 0)
                {
                    bright = currentbrightness;
                    _brighteff.Brightness = bright;
                    _brighteff.Contrast = 1;
                    GrdBrightness.Effect = _brighteff;
                }
                else
                {
                    GrdBrightness.Effect = null;
                    bright = 0;
                }
                if (cont > 0 && currentcontrast != 0)
                {
                    cont = currentcontrast;
                    //_conteff.Contrast = cont;

                }
                else
                {

                    cont = 1;

                }
                EnableSideButton();
                btnThumbnails_Click(new object(), new RoutedEventArgs());
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
            finally
            {
                MemoryManagement.FlushMemory();
            }
        }
        /// <summary>
        /// Handles the Click event of the btnRestoreBrightCont control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void btnRestoreBrightCont_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                GrdBrightness.Effect = null;

                bright = 0;
                cont = 1;
                currentbrightness = 0;
                currentcontrast = 1;

                VisualStateManager.GoToState(btnColorEffects, "Unchecked", true);
                VisualStateManager.GoToState(btnDigiMagic, "Unchecked", true);
                VisualStateManager.GoToState(btnColorEffectsfilters, "Unchecked", true);
                checkDigimagic = false;

            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }

        }
        #endregion

        #region Effects

        /// <summary>
        /// Handles the Click event of the btnColorEffectsfilters control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void btnColorEffectsfilters_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                //if (CurrentImage == null)
                //    CurrentImage = new Bitmap(LoginUser.DigiFolderPath + PhotoName + ".jpg");
                //else
                //    CurrentSelectedImage = CurrentImage;
                GrdsubEffects.Visibility = Visibility.Visible;

                GrdsubColoreffects.Visibility = System.Windows.Visibility.Hidden;
                ImageClick = false;
                btnThumbnails_Click(new object(), new RoutedEventArgs());
            }
            catch (Exception)
            {
            }
            DisableSideButton();
        }

        /// <summary>
        /// Handles the Click event of the CancelButtoncoloreffect control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void CancelButtoncoloreffect_Click(object sender, RoutedEventArgs e)
        {
            GrdsubColoreffects.Visibility = Visibility.Collapsed;
            GrdsubEffects.Visibility = Visibility.Collapsed;
            GrdsubCrop.Visibility = Visibility.Hidden;
            //GrdsubMain.Visibility = Visibility.Visible;
            ColorEffectsIMG.Visibility = Visibility.Collapsed;
            IMGFrame.Visibility = Visibility.Visible;

            if (sharpen > 0 && currentsharpen != 0)
            {
                _sharpeff.Strength = sharpen;
                _sharpeff.PixelWidth = 0.0015;
                _sharpeff.PixelHeight = 0.0015;
                GrdSharpen.Effect = _sharpeff;
            }
            else
            {
                GrdSharpen.Effect = null;
                sharpen = 0;
            }
            if (hueshift > 0 && currenthueshift > 0)
            {
                _shifthueeff.HueShift = hueshift;
                GrdHueShift.Effect = _shifthueeff;
            }
            else
            {
                GrdHueShift.Effect = null;
                hueshift = 0;
            }

            foreach (var item in LstGridEffects)
            {
                if (item == "invert")
                {
                    InvertEffect();
                }
                else if (item == "greyscale")
                {
                    GreyScaleeffect();
                }
                else if (item == "sepia")
                {
                    Sepia();
                }
                else if (item == "defog")
                {
                    Defogger();
                }
                else if (item == "edgedetect")
                {
                    EdgeDetecteffect();
                }
                else if (item == "emboss")
                {
                    Embosseffect();
                }
                else if (item == "noeffect")
                {
                    Noeffect();
                }
                else if (item == "emboss")
                {
                    Cartoonizeeffect();
                }
                else if (item == "digimagic")
                {
                    DigiMagic();
                }

            }
            LstGridEffects.Clear();
            EnableSideButton();
            btnThumbnails_Click(new object(), new RoutedEventArgs());
        }

        /// <summary>
        /// Handles the Click event of the OkButtoncoloreffect control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void OkButtoncoloreffect_Click(object sender, RoutedEventArgs e)
        {
            IsImageDirtyState = true;
            GrdsubCrop.Visibility = Visibility.Hidden;
            GrdsubEffects.Visibility = Visibility.Collapsed;
            GrdsubColoreffects.Visibility = Visibility.Collapsed;

            SaveXml("greyscale", _GreyScale, true);
            SaveXml("sharpen", _Sharepen, true);
            SaveXml("emboss", _emboss, true);
            SaveXml("invert", _invert, true);
            SaveXml("hue", _hue, true);
            SaveXml("cartoon", _cartoon, true);
            SaveXml("granite", _granite, true);
            SaveXml("sepia", _sepia, true);
            SaveXml("defog", _defoger, true);
            SaveXml("underwater", _underwater, true);
            SaveXml("digimagic", _digimagic, true);
            currentsharpen = sharpen;
            currenthueshift = hueshift;
            LstGridEffects.Clear();
            IsEffectChange = true;
            ChangeCheckedEffect();
            EnableSideButton();
            ImageClick = true;
            btnThumbnails_Click(new object(), new RoutedEventArgs());
        }

        /// <summary>
        /// Changes the checked effect.
        /// </summary>
        private void ChangeCheckedEffect()
        {
            if (_GreyScale != "0")
            {
                VisualStateManager.GoToState(btnGreyScale, "Checked", true);
                VisualStateManager.GoToState(btnColorEffectsfilters, "Checked", true);
            }
            if (_sepia != "0")
            {
                VisualStateManager.GoToState(btnSepia, "Checked", true);
                VisualStateManager.GoToState(btnColorEffectsfilters, "Checked", true);
            }
            if (_defoger != "0")
            {
                VisualStateManager.GoToState(btnDefogger, "Checked", true);
                VisualStateManager.GoToState(btnColorEffectsfilters, "Checked", true);
            }
            if (_underwater != "0")
            {
                VisualStateManager.GoToState(btnUnderWater, "Checked", true);
                VisualStateManager.GoToState(btnColorEffectsfilters, "Checked", true);
            }

            if (_invert != "0")
            {
                VisualStateManager.GoToState(btnInvert, "Checked", true);
                VisualStateManager.GoToState(btnColorEffectsfilters, "Checked", true);
            }
            if (_emboss != "0")
            {
                VisualStateManager.GoToState(btnEmboss, "Checked", true);
                VisualStateManager.GoToState(btnColorEffectsfilters, "Checked", true);
            }
            if (_granite != "0")
            {
                VisualStateManager.GoToState(btnEdgeDetect, "Checked", true);
                VisualStateManager.GoToState(btnColorEffectsfilters, "Checked", true);
            }
            if (_cartoon != "0")
            {
                VisualStateManager.GoToState(Cartoonize, "Checked", true);
                VisualStateManager.GoToState(btnColorEffectsfilters, "Checked", true);
            }
            if (_Sharepen != "##")
            {
                if (sharpen != 0.050 && checkDigimagic == false)
                {
                    VisualStateManager.GoToState(btnSharpen, "Checked", true);
                    VisualStateManager.GoToState(btnColorEffectsfilters, "Checked", true);
                }
            }
            if (_hue != "##")
            {
                VisualStateManager.GoToState(btnHue, "Checked", true);
                VisualStateManager.GoToState(btnColorEffectsfilters, "Checked", true);
            }
            if (_digimagic != "##")
            {
                if (checkDigimagic)
                {
                    VisualStateManager.GoToState(btnDigiMagic, "Checked", true);
                    VisualStateManager.GoToState(btnColorEffectsfilters, "Checked", true);
                }
            }
        }


        /// <summary>
        /// Handles the Click event of the btnSharpen control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void btnSharpen_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                sharpen = sharpen + 0.025;
                _sharpeff.Strength = sharpen;
                _sharpeff.PixelWidth = 0.0015;
                _sharpeff.PixelHeight = 0.0015;
                GrdSharpen.Effect = _sharpeff;
                _Sharepen = sharpen.ToString();
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
            finally
            {

                LogEffect obj = new LogEffect();
                obj.effvalue = sharpen;
                obj.optname = "sharpen";
                EffectLogOperation(obj);


            }
        }


        /// <summary>
        /// Handles the Click event of the btnInvert control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void btnInvert_Click(object sender, RoutedEventArgs e)
        {
            InvertEffect();
            LstGridEffects.Add("invert");
        }

        /// <summary>
        /// Inverts the effect.
        /// </summary>
        private void InvertEffect()
        {
            try
            {
                if (_invert == "0")
                {
                    InvertColorEffect _inverteff = new InvertColorEffect();
                    GrdInvert.Effect = _inverteff;
                    _invert = "1";

                }
                else
                {
                    _invert = "0";
                    GrdInvert.Effect = null;
                }

            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
            finally
            {

                LogEffect obj = new LogEffect();
                obj.effvalue = Convert.ToDouble(_invert);
                obj.optname = "invert";
                EffectLogOperation(obj);

            }
        }

        /// <summary>
        /// The _ grey scale
        /// </summary>
        string _GreyScale = "0";
        /// <summary>
        /// The _invert
        /// </summary>
        string _invert = "0";
        /// <summary>
        /// The _emboss
        /// </summary>
        string _emboss = "0";
        /// <summary>
        /// The _granite
        /// </summary>
        string _granite = "0";
        /// <summary>
        /// The _cartoon
        /// </summary>
        string _cartoon = "0";
        /// <summary>
        /// The _ sharepen
        /// </summary>
        string _Sharepen = "##";
        /// <summary>
        /// The _hue
        /// </summary>
        string _hue = "##";
        /// <summary>
        /// The _sepia
        /// </summary>
        string _sepia = "0";
        /// <summary>
        /// The _defoger
        /// </summary>
        string _defoger = "0";
        /// <summary>
        /// The _underwater
        /// </summary>
        string _underwater = "0";
        /// <summary>
        /// The _digimagic
        /// </summary>
        string _digimagic = "0";

        /// <summary>
        /// Handles the Click event of the btnGreyScale control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void btnGreyScale_Click(object sender, RoutedEventArgs e)
        {
            GreyScaleeffect();
            LstGridEffects.Add("greyscale");
        }
        /// <summary>
        /// Greys the scaleeffect.
        /// </summary>
        private void GreyScaleeffect()
        {
            try
            {
                if (_GreyScale == "0")
                {
                    _GreyScale = "1";
                    GreyScaleEffect _greyscaleeff = new GreyScaleEffect();
                    _greyscaleeff.Desaturation = 1;
                    _greyscaleeff.Toned = 0;
                    GrdGreyScale.Effect = _greyscaleeff;
                }
                else
                {
                    _GreyScale = "0";
                    GrdGreyScale.Effect = null;
                }
            }
            catch (Exception)
            {

            }
            finally
            {
                LogEffect obj = new LogEffect();
                obj.effvalue = Convert.ToDouble(_GreyScale);
                obj.optname = "greyscale";
                EffectLogOperation(obj);

            }
        }

        /// <summary>
        /// Handles the Click event of the btnEdgeDetect control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void btnEdgeDetect_Click(object sender, RoutedEventArgs e)
        {
            EdgeDetecteffect();
            LstGridEffects.Add("edgedetect");
        }

        /// <summary>
        /// Edges the detecteffect.
        /// </summary>
        private void EdgeDetecteffect()
        {
            try
            {
                if (_granite == "0")
                {
                    _granite = "1";
                    SketchGraniteEffect sketcheff = new SketchGraniteEffect();
                    sketcheff.BrushSize = .005;
                    GrdSketchGranite.Effect = sketcheff;
                }
                else
                {
                    _granite = "0";
                    GrdSketchGranite.Effect = null;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
            finally
            {
                LogEffect obj = new LogEffect();
                obj.effvalue = Convert.ToDouble(_granite);
                obj.optname = "granite";
                EffectLogOperation(obj);

            }
        }

        /// <summary>
        /// Handles the Click event of the btnEmboss control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void btnEmboss_Click(object sender, RoutedEventArgs e)
        {

            Embosseffect();
            LstGridEffects.Add("emboss");
        }

        /// <summary>
        /// Embosseffects this instance.
        /// </summary>
        private void Embosseffect()
        {
            try
            {
                if (_emboss == "0")
                {
                    _emboss = "1";
                    EmbossedEffect emboseff = new EmbossedEffect();
                    emboseff.Amount = 0.7;
                    emboseff.Width = 0.002;
                    GrdEmboss.Effect = emboseff;
                }
                else
                {
                    _emboss = "0";
                    GrdEmboss.Effect = null;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
            finally
            {

                LogEffect obj = new LogEffect();
                obj.effvalue = Convert.ToDouble(_emboss);
                obj.optname = "emboss";
                EffectLogOperation(obj);

            }
        }

        /// <summary>
        /// Handles the Click event of the btnSepia control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void btnSepia_Click(object sender, RoutedEventArgs e)
        {

            Sepia();
            LstGridEffects.Add("sepia");
        }

        /// <summary>
        /// Sepias this instance.
        /// </summary>
        private void Sepia()
        {
            try
            {
                if (_sepia == "0")
                {
                    _sepia = "1";
                    _colorfiltereff.FilterColor = (System.Windows.Media.Color)System.Windows.Media.ColorConverter.ConvertFromString("#FFE6B34D");
                    GrdSepia.Effect = _colorfiltereff;
                }
                else
                {
                    _sepia = "0";
                    GrdSepia.Effect = null;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
            finally
            {
                LogEffect obj = new LogEffect();
                obj.effvalue = Convert.ToDouble(_sepia);
                obj.optname = "sepia";
                EffectLogOperation(obj);
            }
        }

        /// <summary>
        /// Handles the Click event of the btnDefogger control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void btnDefogger_Click(object sender, RoutedEventArgs e)
        {
            Defogger();
            LstGridEffects.Add("defog");
        }

        /// <summary>
        /// Defoggers this instance.
        /// </summary>
        private void Defogger()
        {
            try
            {
                if (_defoger == "0")
                {
                    _defoger = "1";
                    _brighteff.Brightness = -0.10;
                    _brighteff.Contrast = 1.09;
                    GrdBrightness.Effect = _brighteff;

                    bright = -0.10;
                    cont = 1.09;

                }
                else
                {
                    _defoger = "0";

                    GrdBrightness.Effect = null;

                }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
            finally
            {
                LogEffect obj = new LogEffect();
                obj.effvalue = Convert.ToDouble(_defoger);
                obj.optname = "defog";
                EffectLogOperation(obj);

            }
        }
        /// <summary>
        /// Handles the Click event of the btnDigiMagic control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void btnDigiMagic_Click(object sender, RoutedEventArgs e)
        {
            DigiMagic();
        }
        /// <summary>
        /// The check digimagic
        /// </summary>
        bool checkDigimagic = false;
        /// <summary>
        /// Digis the magic.
        /// </summary>
        /// 


        private void DigiMagic()
        {
            try
            {
                if (_digimagic == "0")
                {
                    _digimagic = "1";
                    //if (_objDbLayer == null)
                    //    _objDbLayer = new DigiPhotoDataServices();
                    ConfigBusiness configBiz = new ConfigBusiness();
                    List<iMIXConfigurationInfo> _objdata = configBiz.GetNewConfigValues(LoginUser.SubStoreId);
                    foreach (iMIXConfigurationInfo imixConfigValue in _objdata)
                    {
                        switch (imixConfigValue.IMIXConfigurationMasterId)
                        {
                            case 18:
                                _brighteff.Brightness = Convert.ToDouble(imixConfigValue.ConfigurationValue);


                                break;
                            case 19:
                                _brighteff.Contrast = Convert.ToDouble(imixConfigValue.ConfigurationValue);

                                break;
                            case 20:
                                sharpen = Convert.ToDouble(imixConfigValue.ConfigurationValue);

                                break;
                            case 94:
                                ColorCode = imixConfigValue.ConfigurationValue;
                                break;
                            default:
                                break;
                        }
                    }
                    // _brighteff.Brightness = -0.01;
                    // _brighteff.Contrast = 1.3;
                    GrdBrightness.Effect = _brighteff;
                    bright = _brighteff.Brightness;
                    cont = _brighteff.Contrast;
                    // sharpen = .050;
                    _sharpeff.Strength = sharpen;
                    _sharpeff.PixelWidth = 0.0015;
                    _sharpeff.PixelHeight = 0.0015;
                    GrdSharpen.Effect = _sharpeff;
                    _Sharepen = sharpen.ToString();
                    checkDigimagic = true;
                    if (String.IsNullOrWhiteSpace(bright.ToString()))
                        bright = 0;
                    if (String.IsNullOrWhiteSpace(cont.ToString()))
                        cont = 1;
                    ImageEffect = "<image brightness = '" + bright.ToString() + "' contrast = '" + cont.ToString() + "' Crop='##' colourvalue = '##' rotatewidth='##' rotateheight='##' rotate='##' flipMode='0' flipModeY='0' _centerx ='0' _centery='0'><effects sharpen='" + sharpen + "' greyscale='0' digimagic='0' sepia='0' defog='0' underwater='0' emboss='0' invert = '0' granite='0' hue ='##' cartoon = '0' firstredeye= 'false' firstradius='.0105' firstcenterx='.5' firstcentery='.5' secondredeye= 'false' secondradius='.0105' secondcenterx='.5' secondcentery='.5' multipleredeye1='false' multipleredeye2='false' multipleredeye3='false' multipleredeye4='false' multipleredeye5='false' multipleredeye6='false' multipleradius1='.0105' multipleradius2='0' multipleradius3='0' multipleradius4='0.0125' multipleradius5='0' multipleradius6='0' multiplecenterx1='.5' multiplecentery1='.5' multiplecenterx2='0' multiplecentery2='0' multiplecenterx3='0' multiplecentery3='0' multiplecenterx4='.5' multiplecentery4='.5' multiplecenterx5='0' multiplecentery5='0' multiplecenterx6='0' multiplecentery6='0'></effects></image>";
                    //_objDbLayer = null;
                }
                else
                {
                    _digimagic = "0";
                    GrdBrightness.Effect = null;
                    GrdSharpen.Effect = null;
                    checkDigimagic = false;
                    ImageEffect = "<image brightness = '0' contrast = '1' Crop='##' colourvalue = '##' rotatewidth='##' rotateheight='##' rotate='##' flipMode='0' flipModeY='0' _centerx ='0' _centery='0'><effects sharpen='##' greyscale='0' digimagic='0' sepia='0' defog='0' underwater='0' emboss='0' invert = '0' granite='0' hue ='##' cartoon = '0' firstredeye= 'false' firstradius='.0105' firstcenterx='.5' firstcentery='.5' secondredeye= 'false' secondradius='.0105' secondcenterx='.5' secondcentery='.5' multipleredeye1='false' multipleredeye2='false' multipleredeye3='false' multipleredeye4='false' multipleredeye5='false' multipleredeye6='false' multipleradius1='.0105' multipleradius2='0' multipleradius3='0' multipleradius4='0.0125' multipleradius5='0' multipleradius6='0' multiplecenterx1='.5' multiplecentery1='.5' multiplecenterx2='0' multiplecentery2='0' multiplecenterx3='0' multiplecentery3='0' multiplecenterx4='.5' multiplecentery4='.5' multiplecenterx5='0' multiplecentery5='0' multiplecenterx6='0' multiplecentery6='0'></effects></image>";
                }
            }
            catch (Exception)
            {

            }
            finally
            {
                LogEffect obj = new LogEffect();
                obj.effvalue = Convert.ToDouble(_digimagic);
                obj.optname = "digimagic";
                EffectLogOperation(obj);
            }
        }
        /// <summary>
        /// Handles the Click event of the btnUnderWater control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void btnUnderWater_Click(object sender, RoutedEventArgs e)
        {
            UnderWater();
            LstGridEffects.Add("underwater");
        }
        /// <summary>
        /// Unders the water.
        /// </summary>
        private void UnderWater()
        {
            try
            {
                if (_underwater == "0")
                {
                    _underwater = "1";
                    _under.FogColor = (System.Windows.Media.Color)System.Windows.Media.ColorConverter.ConvertFromString("#FF4F9CEF");
                    _under.Defog = .28;
                    _under.Contrastr = .56;
                    _under.Contrastg = .87;
                    _under.Contrastb = .89;
                    _under.Exposure = .70;
                    _under.Gamma = 1.62;
                    GrdUnderWater.Effect = _under;
                }
                else
                {
                    _underwater = "0";
                    GrdUnderWater.Effect = null;
                }
            }
            catch (Exception)
            {

            }
            finally
            {
                LogEffect obj = new LogEffect();
                obj.effvalue = Convert.ToDouble(_underwater);
                obj.optname = "underwater";
                EffectLogOperation(obj);

            }
        }

        /// <summary>
        /// Handles the Click event of the btnundocoloreffects control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void btnundocoloreffects_Click(object sender, RoutedEventArgs e)
        {
            UndoEffect();
        }

        /// <summary>
        /// Handles the Click event of the btnRestore control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void btnRestore_Click(object sender, RoutedEventArgs e)
        {
            Noeffect();
            LstGridEffects.Add("noeffect");
            UncheckEffectsButton();
            EffectLog.Clear();

        }

        /// <summary>
        /// Unchecks the effects button.
        /// </summary>
        private void UncheckEffectsButton()
        {
            VisualStateManager.GoToState(btnColorEffectsfilters, "Unchecked", true);
            VisualStateManager.GoToState(btnEmboss, "Unchecked", true);
            VisualStateManager.GoToState(btnEdgeDetect, "Unchecked", true);
            VisualStateManager.GoToState(btnGreyScale, "Unchecked", true);
            VisualStateManager.GoToState(btnHue, "Unchecked", true);
            VisualStateManager.GoToState(btnInvert, "Unchecked", true);
            VisualStateManager.GoToState(Cartoonize, "Unchecked", true);
            VisualStateManager.GoToState(btnSharpen, "Unchecked", true);
            VisualStateManager.GoToState(btnSepia, "Unchecked", true);
            VisualStateManager.GoToState(btnDefogger, "Unchecked", true);
            VisualStateManager.GoToState(btnUnderWater, "Unchecked", true);
            VisualStateManager.GoToState(btnDigiMagic, "Unchecked", true);
            checkDigimagic = false;
        }
        /// <summary>
        /// Noeffects this instance.
        /// </summary>
        private void Noeffect()
        {
            GrdInvert.Effect = null;
            GrdSharpen.Effect = null;
            GrdSketchGranite.Effect = null;
            GrdEmboss.Effect = null;
            Grdcartoonize.Effect = null;
            GrdGreyScale.Effect = null;
            GrdHueShift.Effect = null;
            GrdSepia.Effect = null;
            GrdUnderWater.Effect = null;
            GrdBrightness.Effect = null;
            GrdSharpen.Effect = null;
            //Setting all the values to 0
            hueshift = 0;
            sharpen = 0;
            _GreyScale = "0";
            _invert = "0";
            _emboss = "0";
            _granite = "0";
            _cartoon = "0";
            _Sharepen = "##";
            _hue = "##";
            _sepia = "0";
            _defoger = "0";
            _underwater = "0";
            _digimagic = "0";
            UpdateLayout();
        }


        /// <summary>
        /// Handles the Click event of the Cartoonize control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void Cartoonize_Click(object sender, RoutedEventArgs e)
        {
            Cartoonizeeffect();
            LstGridEffects.Add("cartoonize");
        }

        /// <summary>
        /// Cartoonizeeffects this instance.
        /// </summary>
        private void Cartoonizeeffect()
        {
            try
            {
                if (_cartoon == "0")
                {
                    _cartoon = "1";
                    Cartoonize carteff = new Cartoonize();
                    carteff.Width = 150;
                    carteff.Height = 150;
                    Grdcartoonize.Effect = carteff;
                }
                else
                {
                    _cartoon = "0";
                    Grdcartoonize.Effect = null;
                }
            }
            catch (Exception)
            {
            }
            finally
            {

                LogEffect obj = new LogEffect();
                obj.effvalue = Convert.ToDouble(_cartoon);
                obj.optname = "cartoon";
                EffectLogOperation(obj);

            }
        }

        /// <summary>
        /// Handles the Click event of the btnHue control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void btnHue_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                hueshift = hueshift + 0.02;
                _shifthueeff.HueShift = hueshift;
                GrdHueShift.Effect = _shifthueeff;
                _hue = hueshift.ToString();
            }
            catch (Exception)
            {

            }
            finally
            {
                LogEffect obj = new LogEffect();
                obj.effvalue = hueshift;
                obj.optname = "hue";
                EffectLogOperation(obj);
            }
        }
        #endregion

        #region PhotoComplier

        /// <summary>
        /// Saves the effectsinto database.
        /// </summary>
        public void SaveCropEffectsintoDB()
        {
#if DEBUG
            var watch = FrameworkHelper.CommonUtility.Watch();
            watch.Start();
#endif

            //if (_objDbLayer == null)
            //    _objDbLayer = new DigiPhotoDataServices();
            PhotoBusiness phBiz = new PhotoBusiness();
            phBiz.SaveCroppedPhotoInfo(PhotoId, true, ImageEffect);
#if DEBUG
            if (watch != null)
                FrameworkHelper.CommonUtility.WatchStop(this.GetType().FullName + "." + System.Reflection.MethodBase.GetCurrentMethod().Name + "|", watch);
#endif
        }

        /// <summary>
        /// Saves the effectsinto database.
        /// </summary>
        public void SaveEffectsintoDB()
        {
#if DEBUG
            var watch = FrameworkHelper.CommonUtility.Watch();
            watch.Start();
#endif
            //if (_objDbLayer == null)
            //    _objDbLayer = new DigiPhotoDataServices();
            PhotoBusiness phBiz = new PhotoBusiness();
            //ToDo Save the parameters which are saving in database inside this function
            //Commented as db save nor required.
            //phBiz.SetEffectsonPhoto(ImageEffect, PhotoId, gumshowDb);
#if DEBUG
            if (watch != null)
                FrameworkHelper.CommonUtility.WatchStop(this.GetType().FullName + "." + System.Reflection.MethodBase.GetCurrentMethod().Name + "|", watch);
#endif
        }

        /// <summary>
        /// Loads the XML.
        /// </summary>
        /// <param name="ImageXml">The image XML.</param>
        public void LoadXml(string ImageXml)
        {
#if DEBUG
            var watch = FrameworkHelper.CommonUtility.Watch();
            watch.Start();
#endif
            try
            {
                System.Windows.Media.Color color;
                bool checkluminiosity = false;
                System.Xml.XmlDocument Xdocument = new System.Xml.XmlDocument();
                Xdocument.LoadXml(ImageXml);
                foreach (var xn in (Xdocument.ChildNodes[0]).Attributes)
                {
                    switch (((System.Xml.XmlAttribute)xn).Name.ToLower())
                    {
                        case "brightness":
                            if (((System.Xml.XmlAttribute)xn).Value.ToString() != "0" && ((System.Xml.XmlAttribute)xn).Value.ToString() != "")
                            {
                                bright = Convert.ToDouble(((System.Xml.XmlAttribute)xn).Value.ToString());
                                _brighteff.Brightness = bright;
                                _brighteff.Contrast = 1;

                                GrdBrightness.Effect = _brighteff;
                                currentbrightness = bright;
                                checkluminiosity = true;
                            }
                            else
                            {
                                GrdBrightness.Effect = null;
                                bright = 0;
                                currentbrightness = bright;
                                VisualStateManager.GoToState(btnColorEffects, "Unchecked", true);
                            }
                            break;
                        case "crop":
                            {
                                string path = Common.LoginUser.DigiFolderFramePath + "\\Thumbnails\\";
                                string pathbackground = Common.LoginUser.DigiFolderBackGroundPath + "\\Thumbnails\\";
                                Frames.Clear();
                                lstBackground.Items.Clear();
                                if (((System.Xml.XmlAttribute)xn).Value.ToString() != "##")
                                {
                                    string productname = ((System.Xml.XmlAttribute)xn).Value.ToString();
                                    ProductBusiness prdBiz = new ProductBusiness();
                                    int productid = prdBiz.GetProductID(productname);

                                    try
                                    {
                                        BorderBusiness bordBiz = new BorderBusiness();
                                        var borders = bordBiz.GetBorderDetails().Where(t => t.DG_IsActive == true);
                                        foreach (var item in borders)
                                        {

                                            LstMyItems _objnew = new LstMyItems();
                                            _objnew.FilePath = path + item.DG_Border;
                                            _objnew.Name = item.DG_Border;
                                            _objnew.Photoname1 = item.DG_Border;
                                            Frames.Add(_objnew);


                                        }

                                        BackgroundBusiness bakgBiz = new BackgroundBusiness();
                                        if (bakgBiz.GetBackgoundDetails(productid) != null)
                                        {
                                            foreach (var item in bakgBiz.GetBackgoundDetailsALL())
                                            {
                                                if (item.DG_Background_IsActive.HasValue && item.DG_Background_IsActive.Value)
                                                {
                                                    Uri uri = new Uri(pathbackground + item.DG_BackGround_Image_Name);
                                                    LstMyItems _objnew = new LstMyItems();
                                                    if (File.Exists(uri.ToString()))
                                                    {
                                                        BitmapImage bmp = new BitmapImage(uri);
                                                        _objnew.BmpImage = bmp;
                                                    }
                                                    _objnew.Name = item.DG_BackGround_Image_Name;
                                                    _objnew.Photoname1 = item.DG_BackGround_Image_Display_Name;
                                                    _objnew.FilePath = pathbackground + item.DG_BackGround_Image_Name;

                                                    lstBackground.Items.Add(_objnew);
                                                }

                                            }
                                        }
                                        else
                                        {
                                            foreach (var item in bakgBiz.GetBackgoundDetailsALL())
                                            {
                                                if (item.DG_Background_IsActive.HasValue && item.DG_Background_IsActive.Value)
                                                {
                                                    Uri uri = new Uri(pathbackground + item.DG_BackGround_Image_Name);
                                                    LstMyItems _objnew = new LstMyItems();
                                                    if (File.Exists(uri.ToString()))
                                                    {
                                                        BitmapImage bmp = new BitmapImage(uri);
                                                        _objnew.BmpImage = bmp;
                                                    }
                                                    _objnew.Name = item.DG_BackGround_Image_Name;
                                                    _objnew.Photoname1 = item.DG_BackGround_Image_Display_Name;
                                                    _objnew.FilePath = pathbackground + item.DG_BackGround_Image_Name;

                                                    lstBackground.Items.Add(_objnew);
                                                }

                                            }
                                        }

                                    }
                                    catch (Exception)
                                    {

                                    }
                                }
                                else
                                {
                                    BorderBusiness bordBiz = new BorderBusiness();
                                    var borders = bordBiz.GetBorderDetails().Where(t => t.DG_IsActive == true);
                                    foreach (var item in borders)
                                    {

                                        LstMyItems _objnew = new LstMyItems();
                                        _objnew.FilePath = path + item.DG_Border;
                                        _objnew.Name = item.DG_Border;
                                        _objnew.Photoname1 = item.DG_Border;
                                        Frames.Add(_objnew);

                                    }

                                    BackgroundBusiness backgBiz = new BackgroundBusiness();
                                    foreach (var item in backgBiz.GetBackgoundDetailsALL())
                                    {
                                        #region CommentCode
                                        //if (greenScreenSpecPrintingEnable)
                                        //{
                                        //    //load background based on spec config
                                        //    //DigiPhotoDataServices _objdataLayer = new DigiPhotoDataServices();
                                        //    DG_SemiOrder_Settings objDG_SemiOrder_Settings = _objDbLayer.GetSemiOrderSetting();
                                        //    //  string pathbackground = Common.LoginUser.DigiFolderBackGroundPath + "\\Thumbnails\\";
                                        //    lstBackground.Items.Clear();
                                        //    List<DG_BackGround> lstBackgrounds = _objDbLayer.GetBackGroundProductwise((int)objDG_SemiOrder_Settings.DG_SemiOrder_ProductTypeId);
                                        //    foreach (var items in lstBackgrounds)
                                        //    {
                                        //        Uri uri = new Uri(pathbackground + items.DG_BackGround_Image_Name);
                                        //        LstMyItems _objnew = new LstMyItems();
                                        //        //BitmapImage bmp = new BitmapImage(uri);
                                        //        _objnew.BmpImage = new BitmapImage(uri);
                                        //        _objnew.Name = items.DG_BackGround_Image_Name;
                                        //        _objnew.Photoname1 = items.DG_BackGround_Image_Display_Name;
                                        //        _objnew.FilePath = pathbackground + items.DG_BackGround_Image_Name;
                                        //        lstBackground.Items.Add(_objnew);
                                        //    }
                                        //}
                                        //else

                                        #endregion
                                        if (item.DG_Background_IsActive.HasValue && item.DG_Background_IsActive.Value)
                                        {
                                            Uri uri = new Uri(pathbackground + item.DG_BackGround_Image_Name);
                                            LstMyItems _objnew = new LstMyItems();
                                            if (File.Exists(uri.ToString()))
                                            {
                                                BitmapImage bmp = new BitmapImage(uri);
                                                _objnew.BmpImage = bmp;
                                            }
                                            _objnew.Name = item.DG_BackGround_Image_Name;
                                            _objnew.Photoname1 = item.DG_BackGround_Image_Display_Name;
                                            _objnew.FilePath = pathbackground + item.DG_BackGround_Image_Name;

                                            lstBackground.Items.Add(_objnew);
                                        }
                                    }
                                }
                                break;
                            }
                        case "contrast":
                            if (((System.Xml.XmlAttribute)xn).Value.ToString() != "1" && ((System.Xml.XmlAttribute)xn).Value.ToString() != "")
                            {
                                if (((System.Xml.XmlAttribute)xn).Value.ToString() == "##")
                                {
                                    cont = 1;
                                }
                                else
                                {
                                    cont = Convert.ToDouble(((System.Xml.XmlAttribute)xn).Value.ToString());
                                }
                                ContrastAdjustEffect _conteff = new ContrastAdjustEffect();
                                _conteff.Brightness = bright;
                                _conteff.Contrast = cont;
                                GrdBrightness.Effect = _conteff;
                                _conteff = null;
                                currentcontrast = cont;
                                _sharpeff.Strength = sharpen;
                                checkluminiosity = true;
                            }
                            else
                            {

                                cont = 1;
                                currentcontrast = cont;
                                VisualStateManager.GoToState(btnColorEffects, "Unchecked", true);
                            }
                            break;
                        case "rotate":
                            if (((System.Xml.XmlAttribute)xn).Value.ToString() != "##")
                            {
                                VisualStateManager.GoToState(btnrotate, "Checked", true);
                                VisualStateManager.GoToState(btnantirotate, "Checked", true);
                            }
                            else
                            {
                                VisualStateManager.GoToState(btnrotate, "Unchecked", true);
                                VisualStateManager.GoToState(btnantirotate, "Unchecked", true);
                            }

                            break;
                        case "rotatewidth":
                            if (((System.Xml.XmlAttribute)xn).Value.ToString() != "##")
                            {

                                //frwidth = Convert.ToDouble(((System.Xml.XmlAttribute)xn).Value.ToString());

                            }
                            break;
                        case "rotateheight":
                            if (((System.Xml.XmlAttribute)xn).Value.ToString() != "##")
                            {

                                //frheight = Convert.ToDouble(((System.Xml.XmlAttribute)xn).Value.ToString());
                            }
                            break;

                        case "flipmode":
                            if (((System.Xml.XmlAttribute)xn).Value.ToString() != "##")
                            {
                                //  FlipMode = Convert.ToInt32(((System.Xml.XmlAttribute)xn).Value.ToString());
                            }
                            else
                            {
                                // FlipMode = 0;
                            }

                            break;
                        case "flipmodey":
                            if (((System.Xml.XmlAttribute)xn).Value.ToString() != "##")
                            {
                                // FlipModeY = Convert.ToInt32(((System.Xml.XmlAttribute)xn).Value.ToString());
                            }
                            else
                            {
                                //  FlipModeY = 0;
                            }
                            break;
                        case "_centerx":
                            if (((System.Xml.XmlAttribute)xn).Value.ToString() != "##")
                            {
                                _centerX = ((System.Xml.XmlAttribute)xn).Value.ToString();
                            }
                            else
                            {
                                _centerX = "";
                            }
                            break;
                        case "_centery":
                            if (((System.Xml.XmlAttribute)xn).Value.ToString() != "##")
                            {
                                _centerY = ((System.Xml.XmlAttribute)xn).Value.ToString();
                            }
                            else
                            {
                                _centerY = "";
                            }
                            break;
                    }
                }
                bool coloreffect = false;
                foreach (var xn in (Xdocument.ChildNodes[0]).FirstChild.Attributes)
                {
                    switch (((System.Xml.XmlAttribute)xn).Name.ToLower())
                    {
                        case "sharpen":
                            if (((System.Xml.XmlAttribute)xn).Value.ToString() != "##")
                            {
                                sharpen = Convert.ToDouble(((System.Xml.XmlAttribute)xn).Value.ToString());
                                _sharpeff.Strength = sharpen;
                                _sharpeff.PixelWidth = 0.0015;
                                _sharpeff.PixelHeight = 0.0015;
                                GrdSharpen.Effect = _sharpeff;
                                currentsharpen = sharpen;
                                VisualStateManager.GoToState(btnSharpen, "Checked", true);
                                VisualStateManager.GoToState(btnColorEffectsfilters, "Checked", true);
                                coloreffect = true;
                            }
                            else
                            {
                                GrdSharpen.Effect = null;
                                _Sharepen = "##";
                                VisualStateManager.GoToState(btnSharpen, "Unchecked", true);
                            }
                            break;

                        case "greyscale":
                            if (((System.Xml.XmlAttribute)xn).Value.ToString() != "0")
                            {
                                GreyScaleEffect _greyscaleeff = new GreyScaleEffect();
                                _greyscaleeff.Desaturation = Convert.ToDouble(((System.Xml.XmlAttribute)xn).Value.ToString());
                                _greyscaleeff.Toned = 0;
                                GrdGreyScale.Effect = _greyscaleeff;
                                VisualStateManager.GoToState(btnGreyScale, "Checked", true);
                                VisualStateManager.GoToState(btnColorEffectsfilters, "Checked", true);
                                coloreffect = true;
                            }
                            else
                            {
                                GrdGreyScale.Effect = null;
                                _GreyScale = "0";
                                VisualStateManager.GoToState(btnGreyScale, "Unchecked", true);
                            }
                            break;
                        case "digimagic":
                            if (((System.Xml.XmlAttribute)xn).Value.ToString() != "0")
                            {
                                _digimagic = "1";
                                //_brighteff.Brightness = -0.05;
                                //_brighteff.Contrast = 1.21;
                                _brighteff.Brightness = bright;// -0.13;
                                _brighteff.Contrast = cont;// 1.04;
                                GrdBrightness.Effect = _brighteff;
                                //bright = -0.04;
                                //cont = 1.2;

                                //sharpen = .040;
                                _sharpeff.Strength = sharpen;
                                _sharpeff.PixelWidth = 0.001;
                                _sharpeff.PixelHeight = 0.0015;
                                GrdSharpen.Effect = _sharpeff;
                                _Sharepen = sharpen.ToString();
                                checkDigimagic = true;


                                VisualStateManager.GoToState(btnDigiMagic, "Checked", true);
                                VisualStateManager.GoToState(btnColorEffectsfilters, "Checked", true);
                                VisualStateManager.GoToState(btnColorEffects, "Unchecked", true);
                                VisualStateManager.GoToState(btnSharpen, "Unchecked", true);
                                coloreffect = true;
                            }
                            else
                            {
                                if (!checkluminiosity)
                                {
                                    GrdBrightness.Effect = null;
                                    _digimagic = "0";
                                    VisualStateManager.GoToState(btnDigiMagic, "Unchecked", true);
                                }

                            }
                            break;
                        case "sepia":
                            if (((System.Xml.XmlAttribute)xn).Value.ToString() != "0")
                            {
                                _colorfiltereff.FilterColor = (System.Windows.Media.Color)System.Windows.Media.ColorConverter.ConvertFromString("#FFE6B34D");
                                GrdSepia.Effect = _colorfiltereff;
                                VisualStateManager.GoToState(btnSepia, "Checked", true);
                                VisualStateManager.GoToState(btnColorEffectsfilters, "Checked", true);
                                coloreffect = true;
                            }
                            else
                            {
                                GrdSepia.Effect = null;
                                _sepia = "0";
                                VisualStateManager.GoToState(btnSepia, "Unchecked", true);

                            }
                            break;

                        case "defog":
                            if (((System.Xml.XmlAttribute)xn).Value.ToString() != "0")
                            {
                                // _objdefog.Brightness = -0.10;
                                //_objdefog.Contrast = 1.09;

                                _defoger = "1";
                                _brighteff.Brightness = bright;// -0.10;
                                _brighteff.Contrast = cont;// 1.09;
                                GrdBrightness.Effect = _brighteff;

                                //bright = -0.10;
                                //cont = 1.09;
                                VisualStateManager.GoToState(btnDefogger, "Checked", true);
                                VisualStateManager.GoToState(btnColorEffectsfilters, "Checked", true);
                                VisualStateManager.GoToState(btnColorEffects, "Unchecked", true);
                                coloreffect = true;
                            }
                            else
                            {
                                if (!checkluminiosity && !checkDigimagic)
                                {
                                    GrdBrightness.Effect = null;
                                    _defoger = "0";
                                    VisualStateManager.GoToState(btnDefogger, "Unchecked", true);
                                }
                            }
                            break;
                        case "underwater":
                            if (((System.Xml.XmlAttribute)xn).Value.ToString() != "0")
                            {
                                _under.FogColor = (System.Windows.Media.Color)System.Windows.Media.ColorConverter.ConvertFromString("#FF4F9CEF");
                                _under.Defog = .40;
                                _under.Contrastr = .5;
                                _under.Contrastg = 1;
                                _under.Contrastb = 1;
                                _under.Exposure = .7;
                                _under.Gamma = .81;
                                GrdUnderWater.Effect = _under;
                                VisualStateManager.GoToState(btnUnderWater, "Checked", true);
                                VisualStateManager.GoToState(btnColorEffectsfilters, "Checked", true);
                                coloreffect = true;
                            }
                            else
                            {
                                GrdUnderWater.Effect = null;
                                _underwater = "0";
                                VisualStateManager.GoToState(btnUnderWater, "Unchecked", true);
                            }
                            break;


                        case "emboss":
                            if (((System.Xml.XmlAttribute)xn).Value.ToString() != "0")
                            {
                                EmbossedEffect emboseff = new EmbossedEffect();
                                emboseff.Amount = 0.7;
                                emboseff.Width = 0.002;
                                GrdEmboss.Effect = emboseff;
                                VisualStateManager.GoToState(btnEmboss, "Checked", true);
                                VisualStateManager.GoToState(btnColorEffectsfilters, "Checked", true);
                                coloreffect = true;
                            }
                            else
                            {
                                GrdEmboss.Effect = null;
                                _emboss = "0";
                                VisualStateManager.GoToState(btnEmboss, "Unchecked", true);
                            }
                            break;

                        case "invert":
                            if (((System.Xml.XmlAttribute)xn).Value.ToString() != "0")
                            {
                                InvertColorEffect _inverteff = new InvertColorEffect();
                                GrdInvert.Effect = _inverteff;
                                VisualStateManager.GoToState(btnInvert, "Checked", true);
                                VisualStateManager.GoToState(btnColorEffectsfilters, "Checked", true);
                                coloreffect = true;
                            }
                            else
                            {
                                GrdInvert.Effect = null;
                            }
                            break;

                        case "granite":
                            if (((System.Xml.XmlAttribute)xn).Value.ToString() != "0")
                            {
                                SketchGraniteEffect sketcheff = new SketchGraniteEffect();
                                sketcheff.BrushSize = .005;
                                GrdSketchGranite.Effect = sketcheff;
                                VisualStateManager.GoToState(btnEdgeDetect, "Checked", true);
                                VisualStateManager.GoToState(btnColorEffectsfilters, "Checked", true);
                                coloreffect = true;
                            }
                            else
                            {
                                GrdSketchGranite.Effect = null;
                                _granite = "0";
                                VisualStateManager.GoToState(btnEdgeDetect, "Unhecked", true);
                            }
                            break;

                        case "hue":
                            if (((System.Xml.XmlAttribute)xn).Value.ToString() != "##")
                            {

                                hueshift = Convert.ToDouble(((System.Xml.XmlAttribute)xn).Value.ToString());
                                _shifthueeff.HueShift = hueshift;
                                GrdHueShift.Effect = _shifthueeff;
                                currenthueshift = hueshift;
                                VisualStateManager.GoToState(btnHue, "Checked", true);
                                VisualStateManager.GoToState(btnColorEffectsfilters, "Checked", true);
                                coloreffect = true;
                            }
                            else
                            {
                                GrdHueShift.Effect = null;
                            }
                            break;
                        case "cartoon":
                            if (((System.Xml.XmlAttribute)xn).Value.ToString() != "0")
                            {
                                Cartoonize carteff = new Cartoonize();
                                carteff.Width = 150;
                                carteff.Height = 150;
                                Grdcartoonize.Effect = carteff;
                                VisualStateManager.GoToState(Cartoonize, "Checked", true);
                                VisualStateManager.GoToState(btnColorEffectsfilters, "Checked", true);
                                coloreffect = true;
                            }
                            else
                            {
                                Grdcartoonize.Effect = null;
                                _cartoon = "0";
                                VisualStateManager.GoToState(Cartoonize, "Unhecked", true);
                            }
                            break;

                    }
                }


                if (!coloreffect)
                {
                    VisualStateManager.GoToState(btnColorEffectsfilters, "Unchecked", true);
                }
                if (checkluminiosity)
                {
                    VisualStateManager.GoToState(btnColorEffects, "Checked", true);
                }
                else
                {
                    VisualStateManager.GoToState(btnColorEffects, "Unchecked", true);
                }
                zoomTransform = new ScaleTransform();
                if (FlipMode != 0 || FlipModeY != 0)
                {
                    // FlipLoad();
                }
                else
                {
                    VisualStateManager.GoToState(btnflip, "Unchecked", true);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
#if DEBUG
                if (watch != null)
                    FrameworkHelper.CommonUtility.WatchStop(this.GetType().FullName + "." + System.Reflection.MethodBase.GetCurrentMethod().Name + "|", watch);
#endif
            }


        }
        /// <summary>
        /// Saves the XML.
        /// </summary>
        /// <param name="operation">The operation.</param>
        /// <param name="value">The value.</param>
        /// <param name="childnode">if set to <c>true</c> [childnode].</param>
        public void SaveXml(string operation, string value, bool childnode)
        {
#if DEBUG
            var watch = FrameworkHelper.CommonUtility.Watch();
            watch.Start();
#endif
            System.Xml.XmlDocument Xdocument = new System.Xml.XmlDocument();
            Xdocument.LoadXml(ImageEffect);

            if (!childnode)
            {

                System.Xml.XmlNodeList list = Xdocument.SelectNodes("//image");

                switch (operation)
                {
                    case "colourValue":
                        foreach (System.Xml.XmlElement XElement in list)
                        {
                            if (value != "restore" && value != "none")
                            {
                                XElement.SetAttribute("colourvalue", value);
                            }
                            else
                            {
                                if (value != "none")
                                {
                                    XElement.SetAttribute("colourvalue", "##");
                                }
                            }

                        }
                        break;
                    case "brightness":
                        foreach (System.Xml.XmlElement XElement in list)
                        {
                            XElement.SetAttribute("brightness", value);

                        }
                        break;


                    case "contrast":
                        foreach (System.Xml.XmlElement XElement in list)
                        {
                            XElement.SetAttribute("contrast", value);
                        }
                        break;

                    case "rotatewidth":
                        foreach (System.Xml.XmlElement XElement in list)
                        {
                            XElement.SetAttribute("rotatewidth", value);
                        }
                        break;

                    case "rotateheight":
                        foreach (System.Xml.XmlElement XElement in list)
                        {
                            XElement.SetAttribute("rotateheight", value);
                        }
                        break;

                    case "rotate":
                        foreach (System.Xml.XmlElement XElement in list)
                        {
                            XElement.SetAttribute("rotate", value);

                        }
                        break;

                    case "_centerx":
                        foreach (System.Xml.XmlElement XElement in list)
                        {
                            XElement.SetAttribute("_centerx", value);

                        }
                        break;

                    case "_centery":
                        foreach (System.Xml.XmlElement XElement in list)
                        {
                            XElement.SetAttribute("_centery", value);

                        }
                        break;

                    case "Crop":
                        foreach (System.Xml.XmlElement XElement in list)
                        {
                            XElement.SetAttribute("Crop", value);

                        }
                        break;

                    case "firstredeye":
                        foreach (System.Xml.XmlElement XElement in list)
                        {
                            XElement.SetAttribute("firstredeye", value);

                        }
                        break;

                    case "firstradius":
                        foreach (System.Xml.XmlElement XElement in list)
                        {
                            XElement.SetAttribute("firstradius", value);

                        }
                        break;

                    case "Aspectratiofirstredeye":
                        foreach (System.Xml.XmlElement XElement in list)
                        {
                            XElement.SetAttribute("Aspectratiofirstredeye", value);

                        }
                        break;

                    case "Aspectratiosecondredeye":
                        foreach (System.Xml.XmlElement XElement in list)
                        {
                            XElement.SetAttribute("Aspectratiosecondredeye", value);

                        }
                        break;

                    case "firstcenterx":
                        foreach (System.Xml.XmlElement XElement in list)
                        {
                            XElement.SetAttribute("firstcenterx", value);

                        }
                        break;


                    //if (operation == "flipMode")
                    //{
                    //    foreach (System.Xml.XmlElement XElement in list)
                    //    {
                    //        XElement.SetAttribute("flipMode", value);

                    //    }
                    //}
                    //if (operation == "flipModeY")
                    //{
                    //    foreach (System.Xml.XmlElement XElement in list)
                    //    {
                    //        XElement.SetAttribute("flipModeY", value);

                    //    }
                    //}


                    case "firstcentery":

                        foreach (System.Xml.XmlElement XElement in list)
                        {
                            XElement.SetAttribute("firstcentery", value);

                        }
                        break;

                    case "secondredeye":

                        foreach (System.Xml.XmlElement XElement in list)
                        {
                            XElement.SetAttribute("secondredeye", value);

                        }
                        break;

                    case "secondradius":
                        {

                            foreach (System.Xml.XmlElement XElement in list)
                            {
                                XElement.SetAttribute("secondradius", value);

                            }
                        }
                        break;

                    case "secondcenterx":
                        {

                            foreach (System.Xml.XmlElement XElement in list)
                            {
                                XElement.SetAttribute("secondcenterx", value);

                            }
                        }
                        break;

                    case "secondcentery":
                        {

                            foreach (System.Xml.XmlElement XElement in list)
                            {
                                XElement.SetAttribute("secondcentery", value);

                            }
                        }
                        break;

                    //new red eye code
                    case "multipleredeye1":
                        {

                            foreach (System.Xml.XmlElement XElement in list)
                            {
                                XElement.SetAttribute("multipleredeye1", value);

                            }
                        }
                        break;

                    case "multipleradius1":
                        {

                            foreach (System.Xml.XmlElement XElement in list)
                            {
                                XElement.SetAttribute("multipleradius1", value);

                            }
                        }
                        break;
                    case "multiplecenterx1":
                        {

                            foreach (System.Xml.XmlElement XElement in list)
                            {
                                XElement.SetAttribute("multiplecenterx1", value);

                            }
                        }
                        break;

                    case "multiplecentery1":
                        {

                            foreach (System.Xml.XmlElement XElement in list)
                            {
                                XElement.SetAttribute("multiplecentery1", value);

                            }
                        }
                        break;
                    case "multipleredeye2":
                        {

                            foreach (System.Xml.XmlElement XElement in list)
                            {
                                XElement.SetAttribute("multipleredeye2", value);

                            }
                        }
                        break;
                    case "multipleradius2":
                        {

                            foreach (System.Xml.XmlElement XElement in list)
                            {
                                XElement.SetAttribute("multipleradius2", value);

                            }
                        }
                        break;
                    case "multiplecenterx2":
                        {

                            foreach (System.Xml.XmlElement XElement in list)
                            {
                                XElement.SetAttribute("multiplecenterx2", value);

                            }
                        }
                        break;

                    case "multiplecentery2":
                        {

                            foreach (System.Xml.XmlElement XElement in list)
                            {
                                XElement.SetAttribute("multiplecentery2", value);

                            }
                        }
                        break;
                    case "multipleredeye3":
                        {

                            foreach (System.Xml.XmlElement XElement in list)
                            {
                                XElement.SetAttribute("multipleredeye3", value);

                            }
                        }
                        break;

                    case "multipleradius3":
                        {

                            foreach (System.Xml.XmlElement XElement in list)
                            {
                                XElement.SetAttribute("multipleradius3", value);

                            }
                        }
                        break;
                    case "multiplecenterx3":
                        {

                            foreach (System.Xml.XmlElement XElement in list)
                            {
                                XElement.SetAttribute("multiplecenterx3", value);

                            }
                        }
                        break;

                    case "multiplecentery3":
                        {

                            foreach (System.Xml.XmlElement XElement in list)
                            {
                                XElement.SetAttribute("multiplecentery3", value);

                            }
                        }
                        break;

                    case "multipleredeye4":
                        {

                            foreach (System.Xml.XmlElement XElement in list)
                            {
                                XElement.SetAttribute("multipleredeye4", value);

                            }
                        }
                        break;

                    case "multipleradius4":
                        {

                            foreach (System.Xml.XmlElement XElement in list)
                            {
                                XElement.SetAttribute("multipleradius4", value);

                            }
                        }
                        break;

                    case "multiplecenterx4":
                        {

                            foreach (System.Xml.XmlElement XElement in list)
                            {
                                XElement.SetAttribute("multiplecenterx4", value);

                            }
                        }
                        break;

                    case "multiplecentery4":
                        {

                            foreach (System.Xml.XmlElement XElement in list)
                            {
                                XElement.SetAttribute("multiplecentery4", value);

                            }
                        }
                        break;

                    case "multipleredeye5":
                        {

                            foreach (System.Xml.XmlElement XElement in list)
                            {
                                XElement.SetAttribute("multipleredeye5", value);

                            }
                        }
                        break;

                    case "multipleradius5":
                        {

                            foreach (System.Xml.XmlElement XElement in list)
                            {
                                XElement.SetAttribute("multipleradius5", value);

                            }
                        }
                        break;

                    case "multiplecenterx5":
                        {

                            foreach (System.Xml.XmlElement XElement in list)
                            {
                                XElement.SetAttribute("multiplecenterx5", value);

                            }
                        }
                        break;

                    case "multiplecentery5":
                        {

                            foreach (System.Xml.XmlElement XElement in list)
                            {
                                XElement.SetAttribute("multiplecentery5", value);

                            }
                        }

                        break;

                    case "multipleredeye6":
                        {

                            foreach (System.Xml.XmlElement XElement in list)
                            {
                                XElement.SetAttribute("multipleredeye6", value);

                            }
                        }
                        break;

                    case "multipleradius6":
                        {

                            foreach (System.Xml.XmlElement XElement in list)
                            {
                                XElement.SetAttribute("multipleradius6", value);

                            }
                        }
                        break;

                    case "multiplecenterx6":
                        {

                            foreach (System.Xml.XmlElement XElement in list)
                            {
                                XElement.SetAttribute("multiplecenterx6", value);

                            }
                        }
                        break;

                    case "multiplecentery6":
                        {

                            foreach (System.Xml.XmlElement XElement in list)
                            {
                                XElement.SetAttribute("multiplecentery6", value);

                            }
                        }
                        break;

                    default:
                        break;
                }


            }
            else
            {
                System.Xml.XmlNodeList list = Xdocument.SelectNodes("//image//effects");
                if (operation == "greyscale")
                {
                    foreach (System.Xml.XmlElement XElement in list)
                    {
                        if (value != "restore" && value != "none")
                        {
                            XElement.SetAttribute("greyscale", value);
                        }
                    }
                }
                if (operation == "sepia")
                {
                    foreach (System.Xml.XmlElement XElement in list)
                    {
                        if (value != "restore" && value != "none")
                        {
                            XElement.SetAttribute("sepia", value);
                        }
                    }
                }

                if (operation == "defog")
                {
                    foreach (System.Xml.XmlElement XElement in list)
                    {
                        if (value != "restore" && value != "none")
                        {
                            XElement.SetAttribute("defog", value);
                        }
                    }
                }
                if (operation == "underwater")
                {
                    foreach (System.Xml.XmlElement XElement in list)
                    {
                        if (value != "restore" && value != "none")
                        {
                            XElement.SetAttribute("underwater", value);
                        }
                    }
                }
                if (operation == "digimagic")
                {
                    foreach (System.Xml.XmlElement XElement in list)
                    {
                        if (value != "restore" && value != "none")
                        {
                            XElement.SetAttribute("digimagic", value);
                        }
                    }
                }
                if (operation == "emboss")
                {
                    foreach (System.Xml.XmlElement XElement in list)
                    {
                        if (value != "restore" && value != "none")
                        {
                            XElement.SetAttribute("emboss", value);
                        }
                    }
                }
                if (operation == "invert")
                {
                    foreach (System.Xml.XmlElement XElement in list)
                    {
                        if (value != "restore" && value != "none")
                        {
                            XElement.SetAttribute("invert", value);
                        }
                    }
                }
                if (operation == "granite")
                {
                    foreach (System.Xml.XmlElement XElement in list)
                    {
                        if (value != "restore" && value != "none")
                        {
                            XElement.SetAttribute("granite", value);
                        }
                    }
                }
                if (operation == "cartoon")
                {
                    foreach (System.Xml.XmlElement XElement in list)
                    {
                        if (value != "restore" && value != "none")
                        {
                            XElement.SetAttribute("cartoon", value);
                        }
                    }
                }
                if (operation == "hue")
                {
                    foreach (System.Xml.XmlElement XElement in list)
                    {
                        if (value != "restore" && value != "none")
                        {
                            XElement.SetAttribute("hue", value);
                        }
                    }
                }
                if (operation == "sharpen")
                {
                    foreach (System.Xml.XmlElement XElement in list)
                    {
                        if (value != "restore" && value != "none")
                        {
                            XElement.SetAttribute("sharpen", value);
                        }
                    }
                }
            }
            ImageEffect = Xdocument.InnerXml.ToString();
#if DEBUG
            if (watch != null)
                FrameworkHelper.CommonUtility.WatchStop(this.GetType().FullName + "." + System.Reflection.MethodBase.GetCurrentMethod().Name + "|", watch);
#endif
        }

        #endregion

        #region PhotoAlbum

        /// <summary>
        /// The is selected main image
        /// </summary>
        private bool IsSelectedMainImage = false;

        #region Constructor

        /// <summary>
        /// Loads the photo album.
        /// </summary>
        public void LoadPhotoAlbum()
        {
#if DEBUG
            var watch = FrameworkHelper.CommonUtility.Watch();
            watch.Start();
#endif
            AddDefaultItems();

            translateTransform = new TranslateTransform();
            zoomTransform = new ScaleTransform();
            transformGroup = new TransformGroup();
            rotateTransform = new RotateTransform();

            canbackground.RenderTransform = GrdGreenScreenDefault3.RenderTransform = null;

#if DEBUG
            if (watch != null)
                FrameworkHelper.CommonUtility.WatchStop(this.GetType().FullName + "." + System.Reflection.MethodBase.GetCurrentMethod().Name + "|", watch);
#endif
        }

        #endregion

        #region Properties & Declarations
        /// <summary>
        /// The _photo unique identifier
        /// </summary>
        private long _photoId;
        //private int _PhotoId;
        /// <summary>
        /// Gets or sets the photo unique identifier.
        /// </summary>
        /// <value>
        /// The photo unique identifier.
        /// </value>
        public long PhotoId
        {
            get { return _photoId; }
            set
            {
                _photoId = value;
            }
        }
        //public int PhotoId
        //{
        //    get { return _PhotoId; }
        //    set
        //    {
        //        _PhotoId = value;
        //    }
        //}


        private int _previousPhotoId;
        public int PreviousPhotoId
        {
            get { return _previousPhotoId; }
            set
            {
                _previousPhotoId = value;
            }
        }

        /// <summary>
        /// The _temp file name
        /// </summary>
        private string _tempFileName;
        /// <summary>
        /// Gets or sets the tempfilename.
        /// </summary>
        /// <value>
        /// The tempfilename.
        /// </value>
        public string tempfilename
        {
            get { return _tempFileName; }
            set
            {
                _tempFileName = value;
            }
        }


        private int _semiOrderProfileId;
        public int semiOrderProfileId
        {
            get { return _semiOrderProfileId; }
            set
            {
                _semiOrderProfileId = value;
            }
        }
        public SemiOrderSettings semiOrderSettings { get; set; }

        /// <summary>
        /// The frames
        /// </summary>
        List<LstMyItems> Frames = new List<LstMyItems>();
        /// <summary>
        /// The graphics
        /// </summary>
        List<LstMyItems> Graphics = new List<LstMyItems>();
        /// <summary>
        /// The element for context menu
        /// </summary>
        private UIElement elementForContextMenu;
        #endregion

        #region context
        /// <summary>
        /// Handles the MouseLeftButtonDown event of the Window control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="MouseButtonEventArgs"/> instance containing the event data.</param>
        private void Window_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (sender is DragCanvas)
            {
                IsSelectedMainImage = true;
            }
        }

        /// <summary>
        /// Angles the altitude selector1_ angle changed.
        /// </summary>
        private void angleAltitudeSelector1_AngleChanged()
        {
#if DEBUG
            var watch = FrameworkHelper.CommonUtility.Watch();
            watch.Start();
#endif
            //jrotate.Visibility = System.Windows.Visibility.Visible;
            //jrotate.BringIntoView();
            //jrotate.Focus();


            IsGraphicsChange = true;
            if (this.elementForContextMenu != null)
            {
                if (IsSelectedMainImage)
                {
                    RotateTransform rtm = new RotateTransform();
                    rtm.CenterX = 0;
                    rtm.CenterY = 0;
                    rtm.Angle = jrotate.Angle;
                    canbackground.RenderTransformOrigin =
                           GrdBrightness.RenderTransformOrigin = new System.Windows.Point(0.5, 0.5);
                    canbackground.RenderTransform =
                        GrdBrightness.RenderTransform = rtm;
                }
                else
                {
                    if ((this.elementForContextMenu is TextBox))
                    {

                        TextBox GraphicText = (TextBox)this.elementForContextMenu;
                        RotateTransform rtm = new RotateTransform();
                        rtm.CenterX = 0;
                        rtm.CenterY = 0;
                        rtm.Angle = jrotate.Angle;
                        GraphicText.RenderTransformOrigin = new System.Windows.Point(0.5, 0.5);
                        GraphicText.RenderTransform = rtm;
                    }
                    if ((this.elementForContextMenu is Button))
                    {

                        Button btn = (Button)this.elementForContextMenu;

                        TransformGroup tgnew = new TransformGroup();
                        TransformGroup tg = this.elementForContextMenu.GetValue(Canvas.RenderTransformProperty) as TransformGroup;

                        if (tg != null)
                        {
                            if (tg.Children.Count > 0)
                            {
                                if (tg.Children[0] is ScaleTransform)
                                {
                                    tgnew.Children.Add(tg.Children[0]);
                                }
                                if (tg.Children.Count > 1)
                                {

                                    if (tg.Children[1] is ScaleTransform)
                                    {
                                        tgnew.Children.Add(tg.Children[1]);


                                    }
                                }
                            }

                        }


                        RotateTransform rotation = new RotateTransform();
                        btn.RenderTransformOrigin = new System.Windows.Point(0.5, 0.5);
                        rotation.CenterX = 0;
                        rotation.CenterY = 0;
                        rotation.Angle = jrotate.Angle;
                        tgnew.Children.Add(rotation);
                        btn.RenderTransform = tgnew;
                    }
                }
            }
            else
            {
                RotateTransform rtm = new RotateTransform();
                rtm.CenterX = 0;
                rtm.CenterY = 0;
                rtm.Angle = jrotate.Angle;
                canbackground.RenderTransformOrigin =
                    GrdBrightness.RenderTransformOrigin = new System.Windows.Point(0.5, 0.5);
                canbackground.RenderTransform =
                   GrdBrightness.RenderTransform = rtm;
            }
#if DEBUG
            if (watch != null)
                FrameworkHelper.CommonUtility.WatchStop(this.GetType().FullName + "." + System.Reflection.MethodBase.GetCurrentMethod().Name + "|", watch);
#endif
        }

        /// <summary>
        /// Jangles the altitude selector1_ angle changed.
        /// </summary>
        private void jangleAltitudeSelector1_AngleChanged()
        //{
        //    IsGraphicsChange = true;
        //    if (!string.IsNullOrEmpty(strSender))
        //        SetRotation();
        //    strSender = string.Empty;
        //}

        //public void SetRotation()
        {
            if (!IsComingOnLoad)
                IsImageDirtyState = true;
            IsComingOnLoad = false;
            IsGraphicsChange = true;
            if (this.elementForContextMenu != null)
            {
                if (IsSelectedMainImage)
                {
                    RotateTransform rtm = new RotateTransform();
                    rtm.CenterX = 0;
                    rtm.CenterY = 0;
                    rtm.Angle = jrotate.Angle;
                    canbackgroundRotate.RenderTransformOrigin =
                        GrdBrightness.RenderTransformOrigin = new System.Windows.Point(0.5, 0.5);

                    canbackgroundRotate.RenderTransform =
                        GrdBrightness.RenderTransform = rtm;
                }
                else
                {
                    if ((this.elementForContextMenu is TextBox))
                    {

                        TextBox GraphicText = (TextBox)this.elementForContextMenu;
                        RotateTransform rtm = new RotateTransform();
                        rtm.CenterX = 0;
                        rtm.CenterY = 0;
                        rtm.Angle = jrotate.Angle;
                        GraphicText.RenderTransformOrigin = new System.Windows.Point(0.5, 0.5);
                        GraphicText.RenderTransform = rtm;
                    }
                    if ((this.elementForContextMenu is Button))
                    {

                        Button btn = (Button)this.elementForContextMenu;

                        TransformGroup tgnew = new TransformGroup();
                        TransformGroup tg = this.elementForContextMenu.GetValue(Canvas.RenderTransformProperty) as TransformGroup;

                        if (tg != null)
                        {
                            if (tg.Children.Count > 0)
                            {
                                if (tg.Children[0] is ScaleTransform)
                                {
                                    tgnew.Children.Add(tg.Children[0]);
                                }
                                if (tg.Children.Count > 1)
                                {

                                    if (tg.Children[1] is ScaleTransform)
                                    {
                                        tgnew.Children.Add(tg.Children[1]);


                                    }
                                }
                            }

                        }


                        RotateTransform rotation = new RotateTransform();
                        btn.RenderTransformOrigin = new System.Windows.Point(0.5, 0.5);
                        rotation.CenterX = 0;
                        rotation.CenterY = 0;
                        rotation.Angle = jrotate.Angle;
                        tgnew.Children.Add(rotation);
                        btn.RenderTransform = tgnew;
                    }
                }

            }
            else
            {
                RotateTransform rtm = new RotateTransform();
                rtm.CenterX = 0;
                rtm.CenterY = 0;
                rtm.Angle = jrotate.Angle;
                canbackgroundRotate.RenderTransformOrigin =
                    GrdBrightness.RenderTransformOrigin = new System.Windows.Point(0.5, 0.5);
                canbackgroundRotate.RenderTransform =
                    GrdBrightness.RenderTransform = rtm;
            }
        }

        #region Window1_PreviewMouseRightButtonDown

        /// <summary>
        /// Handles the PreviewMouseRightButtonDown event of the Window1 control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="MouseButtonEventArgs"/> instance containing the event data.</param>
        void Window1_PreviewMouseRightButtonDown(object sender, MouseButtonEventArgs e)
        {
            // If the user right-clicks while dragging an element, assume that they want 
            // to manipulate the z-index of the element being dragged (even if it is  
            // behind another element at the time).
            if (this.dragCanvas.ElementBeingDragged != null)
            {
                this.elementForContextMenu = this.dragCanvas.ElementBeingDragged;
                if ((this.elementForContextMenu is TextBox))
                {
                    TextBox GraphicText = (TextBox)this.elementForContextMenu;

                }
            }
            else
                this.elementForContextMenu =
                    this.dragCanvas.FindCanvasChild(e.Source as DependencyObject);

            IsSelectedMainImage = false;

        }

        #endregion

        /// <summary>
        /// Handles the TextChanged event of the txtContent control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="TextChangedEventArgs"/> instance containing the event data.</param>
        private void txtContent_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (this.elementForContextMenu != null)
            {
                if ((this.elementForContextMenu is TextBox))
                {

                    TextBox GraphicText = (TextBox)this.elementForContextMenu;
                    GraphicText.Text = txtContent.Text;
                }
            }

        }
        /// <summary>
        /// Handles the SelectionChanged event of the cmbFont control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="SelectionChangedEventArgs"/> instance containing the event data.</param>
        private void cmbFont_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (this.elementForContextMenu != null)
            {
                if ((this.elementForContextMenu is TextBox))
                {
                    if (cmbFont.SelectedValue != null)
                    {
                        TextBox GraphicText = (TextBox)this.elementForContextMenu;
                        GraphicText.FontFamily = (System.Windows.Media.FontFamily)cmbFont.SelectedValue;
                    }
                }
            }
        }
        /// <summary>
        /// Handles the SelectionChanged event of the CmbColor control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="SelectionChangedEventArgs"/> instance containing the event data.</param>
        private void CmbColor_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (this.elementForContextMenu != null)
            {
                if ((this.elementForContextMenu is TextBox))
                {
                    if (CmbColor.SelectedValue != null)
                    {
                        TextBox GraphicText = (TextBox)this.elementForContextMenu;

                        GraphicText.Foreground = (System.Windows.Media.SolidColorBrush)CmbColor.SelectedValue;
                    }
                }
            }
        }
        /// <summary>
        /// Handles the SelectionChanged event of the CmbFontSize control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="SelectionChangedEventArgs"/> instance containing the event data.</param>
        private void CmbFontSize_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (this.elementForContextMenu != null)
            {
                if ((this.elementForContextMenu is TextBox))
                {
                    if (CmbFontSize.SelectedValue != null)
                    {
                        TextBox GraphicText = (TextBox)this.elementForContextMenu;
                        GraphicText.FontSize = Convert.ToDouble(CmbFontSize.SelectedValue.ToString());
                    }
                }
            }
        }

        #endregion

        #region Events
        /// <summary>
        /// Handles the Click event of the btnup control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void btnup_Click(object sender, RoutedEventArgs e)
        {
            up();
        }
        /// <summary>
        /// Handles the Click event of the btnright control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void btnright_Click(object sender, RoutedEventArgs e)
        {
            right();
        }
        /// <summary>
        /// Handles the Click event of the btnleft control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void btnleft_Click(object sender, RoutedEventArgs e)
        {
            left();
        }
        /// <summary>
        /// Handles the Click event of the btndown control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void btndown_Click(object sender, RoutedEventArgs e)
        {
#if DEBUG
            var watch = FrameworkHelper.CommonUtility.Watch();
            watch.Start();
#endif
            down();


#if DEBUG
            if (watch != null)
                FrameworkHelper.CommonUtility.WatchStop(this.GetType().FullName + "." + System.Reflection.MethodBase.GetCurrentMethod().Name + "|", watch);
#endif

        }
        /// <summary>
        /// Loads the features.
        /// </summary>
        private void LoadFeatures()
        {
#if DEBUG
            var watch = FrameworkHelper.CommonUtility.Watch();
            watch.Start();
#endif
            LoadFrames();
            LoadGraphics();
            LoadBackground();
#if DEBUG
            if (watch != null)
                FrameworkHelper.CommonUtility.WatchStop(this.GetType().FullName + "." + System.Reflection.MethodBase.GetCurrentMethod().Name + "|", watch);
#endif
        }
        /// <summary>
        /// Loads the imagesto list.
        /// </summary>
        private void loadImagestoList()
        {
#if DEBUG
            var watch = FrameworkHelper.CommonUtility.Watch();
            watch.Start();
#endif
            lstFrame.Items.Clear();
            ImgLoader();// = new Thread(ImgLoader);

            //ImageLoader.Start();
#if DEBUG
            if (watch != null)
                FrameworkHelper.CommonUtility.WatchStop(this.GetType().FullName + "." + System.Reflection.MethodBase.GetCurrentMethod().Name + "|", watch);
#endif
        }


        /// <summary>
        /// Imgs the loader.
        /// </summary>
        void ImgLoader()
        {
#if DEBUG
            var watch = FrameworkHelper.CommonUtility.Watch();
            watch.Start();
#endif
            ListBoxItem listBoxItem = null;
            foreach (var img in Frames)
            {

                lstFrame.Items.Add(img);
                if (selectedborder != string.Empty)
                {
                    if (img.FilePath == selectedborder)
                    {
                        lstFrame.SelectedItem = img;
                        lstFrame.ScrollIntoView(lstFrame.SelectedItem);
                    }
                }
            }

            if (listBoxItem != null)
            {
                listBoxItem = (ListBoxItem)lstFrame.ItemContainerGenerator.ContainerFromItem(lstFrame.SelectedItem);
                listBoxItem.Focus();
            }
#if DEBUG
            if (watch != null)
                FrameworkHelper.CommonUtility.WatchStop(this.GetType().FullName + "." + System.Reflection.MethodBase.GetCurrentMethod().Name + "|", watch);
#endif
        }

        /// <summary>
        /// Handles the Click event of the btnClose control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void btnClose_Click(object sender, RoutedEventArgs e)
        {
            CmbProductType.Visibility = Visibility.Collapsed;
            GrdEffects.Visibility = Visibility.Collapsed;
            ImageClick = true;
        }

        /// <summary>
        /// The previouscounter
        /// </summary>
        int previouscounter = -1;
        /// <summary>
        /// The graphics text box count
        /// </summary>
        int graphicsTextBoxCount = 0;
        int gumballTextCount = 0;
        /// <summary>
        /// The graphics count
        /// </summary>
        int graphicsCount = 0;
        /// <summary>
        /// The graphics border applied
        /// </summary>
        bool graphicsBorderApplied = false;
        /// <summary>
        /// The graphicsframe applied
        /// </summary>
        bool graphicsframeApplied = false;
        /// <summary>
        /// The selectedborder
        /// </summary>
        string selectedborder = string.Empty;
        /// <summary>
        /// The selectedbackground
        /// </summary>
        string selectedbackground = string.Empty;
        /// <summary>
        /// The producttypefor arguments
        /// </summary>
        int ProducttypeforGS = 0;
        /// <summary>
        /// Handles the Click event of the Effects control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        /// 
        object EffectsSender = null;
        int borderSelectedIndex = -1;
        private void RemoveBorder(string FileName, string selectedborder, int ProductType, object sender)
        {
            #region Borders Selected
            {
                //ChromaBorderPath = FileName;
                //borderName = System.IO.Path.GetFileName(FileName);
                //Uri uri = new Uri(FileName);
                List<UIElement> itemstoremove = new List<UIElement>();

                if (previouscounter == lstFrame.SelectedIndex)
                {
                    previouscounter = -1;
                    foreach (UIElement ui in frm.Children)
                    {
                        if (ui.Uid.StartsWith("frame"))
                        {
                            itemstoremove.Add(ui);
                        }
                    }
                    foreach (UIElement ui in itemstoremove)
                    {
                        frm.Children.Remove(ui);
                        selectedborder = string.Empty;
                        graphicsframeApplied = false;
                        VisualStateManager.GoToState(btnBorder, "Unchecked", true);

                        forWdht.Height = GrdSize.ActualHeight;
                        forWdht.Width = GrdSize.ActualWidth;
                        forWdht.InvalidateArrange();
                        forWdht.InvalidateMeasure();
                        forWdht.InvalidateVisual();

                        Zomout(true);
                    }
                    if (_ZoomFactor == .95)
                    {
                        _ZoomFactor = 1;

                        if (zoomTransform != null && _ZoomFactor >= .5)
                        {
                            if (FlipMode != 0 || FlipModeY != 0)
                            {
                                zoomTransform.CenterX = mainImage.ActualWidth / 2;
                                zoomTransform.CenterY = mainImage.ActualHeight / 2;

                                zoomTransform.ScaleX = _ZoomFactor;
                                zoomTransform.ScaleY = _ZoomFactor;
                                transformGroup = new TransformGroup();
                                transformGroup.Children.Add(zoomTransform);
                                transformGroup.Children.Add(translateTransform);
                                transformGroup.Children.Add(rotateTransform);
                                canbackground.RenderTransform = GrdGreenScreenDefault3.RenderTransform = transformGroup;
                                IsGraphicsChange = true;
                            }
                            else
                            {
                                zoomTransform.CenterX = mainImage.ActualWidth / 2;
                                zoomTransform.CenterY = mainImage.ActualHeight / 2;

                                zoomTransform.ScaleX = _ZoomFactor;
                                zoomTransform.ScaleY = _ZoomFactor;
                                transformGroup = new TransformGroup();
                                transformGroup.Children.Add(zoomTransform);
                                transformGroup.Children.Add(translateTransform);
                                transformGroup.Children.Add(rotateTransform);
                                canbackground.RenderTransform = GrdGreenScreenDefault3.RenderTransform = transformGroup;
                                IsGraphicsChange = true;
                            }
                            lblzoomplus.Content = _ZoomFactor * 100 + " % ";
                        }
                    }
                }

                #endregion
            }
        }
        private void ApplyBorder(string FileName, string selectedborder, int ProductType, object sender)
        {
            #region Borders Selected
            {
                if (!(ChromaBorderPath == FileName))
                {
                    OriginalBorder = ChromaBorderPath;
                    ChromaBorderPath = FileName;
                }

                borderName = System.IO.Path.GetFileName(FileName);
                Uri uri = new Uri(FileName);
                List<UIElement> itemstoremove = new List<UIElement>();

                if (previouscounter == lstFrame.SelectedIndex)
                {
                    previouscounter = -1;
                    foreach (UIElement ui in frm.Children)
                    {
                        if (ui.Uid.StartsWith("frame"))
                        {
                            itemstoremove.Add(ui);
                        }
                    }
                    foreach (UIElement ui in itemstoremove)
                    {
                        frm.Children.Remove(ui);
                        selectedborder = string.Empty;
                        graphicsframeApplied = false;
                        VisualStateManager.GoToState(btnBorder, "Unchecked", true);

                        forWdht.Height = GrdSize.ActualHeight;
                        forWdht.Width = GrdSize.ActualWidth;
                        forWdht.InvalidateArrange();
                        forWdht.InvalidateMeasure();
                        forWdht.InvalidateVisual();

                        Zomout(true);
                    }
                    if (_ZoomFactor == .95)
                    {
                        _ZoomFactor = 1;

                        if (zoomTransform != null && _ZoomFactor >= .5)
                        {
                            if (FlipMode != 0 || FlipModeY != 0)
                            {
                                zoomTransform.CenterX = mainImage.ActualWidth / 2;
                                zoomTransform.CenterY = mainImage.ActualHeight / 2;

                                zoomTransform.ScaleX = _ZoomFactor;
                                zoomTransform.ScaleY = _ZoomFactor;
                                transformGroup = new TransformGroup();
                                transformGroup.Children.Add(zoomTransform);
                                transformGroup.Children.Add(translateTransform);
                                transformGroup.Children.Add(rotateTransform);
                                canbackground.RenderTransform = GrdGreenScreenDefault3.RenderTransform = transformGroup;
                                IsGraphicsChange = true;
                            }
                            else
                            {
                                zoomTransform.CenterX = mainImage.ActualWidth / 2;
                                zoomTransform.CenterY = mainImage.ActualHeight / 2;

                                zoomTransform.ScaleX = _ZoomFactor;
                                zoomTransform.ScaleY = _ZoomFactor;
                                transformGroup = new TransformGroup();
                                transformGroup.Children.Add(zoomTransform);
                                transformGroup.Children.Add(translateTransform);
                                transformGroup.Children.Add(rotateTransform);
                                canbackground.RenderTransform = GrdGreenScreenDefault3.RenderTransform = transformGroup;
                                IsGraphicsChange = true;
                            }
                            lblzoomplus.Content = _ZoomFactor * 100 + " % ";
                        }
                    }
                }
                else
                {
                    previouscounter = lstFrame.SelectedIndex;
                }
                if (IsBtnBackgroundClicked == true && IsbtnBackgroundWithoutChroma == true)
                {
                    ApplyEffectsAgainAfterChroma();
                }

                #endregion
            }
        }
        int ProductTypeId = 0;
        private void Effects_Click(object sender, RoutedEventArgs e)
        {
#if DEBUG
            var watch = FrameworkHelper.CommonUtility.Watch();
            watch.Start();
#endif
            try
            {
                if (sender == null)
                    return;

                EffectsSender = sender;
                jrotate.Visibility = System.Windows.Visibility.Hidden;

                IsGraphicsChange = true;
                Button btn = (Button)sender;
                if (btn.Tag == null && (IsDefaultBackgroundEnabled || isGreenImage))
                {
                    if (IsDefaultBackgroundEnabled)
                    {
                        btn.Tag = DefaultBackgroundImagePath;
                        btn.CommandParameter = DefaultBackgroundImagePath.Split('\\').Last();
                    }
                    else if (isGreenImage)
                    {
                        btn.Tag = Common.LoginUser.DigiFolderBackGroundPath + "\\8x10\\" + BackgroundDBValue;
                        btn.CommandParameter = btn.Tag.ToString().Split('\\').Last();
                    }
                }
                if (btn.CommandParameter == "" && IsDefaultBackgroundEnabled &&
                    btn.Tag != "")
                {
                    btn.CommandParameter = DefaultBackgroundImagePath.Split('\\').Last();
                }

                string[] tempFileName = btn.Tag.ToString().Split('\\');


                string FileName = LoginUser.DigiFolderFramePath + "\\" + tempFileName[tempFileName.Count() - 1];
                #region Borders Selected
                if (lstFrame.Visibility == System.Windows.Visibility.Visible)
                {
                    if (!File.Exists(FileName))
                        return;
                    IsImageDirtyState = true;
                    ChromaBorderPath = FileName;
                    selectedborder = LoginUser.DigiFolderFramePath + "\\Thumbnails\\" + tempFileName[tempFileName.Count() - 1];


                    Uri uri = new Uri(FileName);
                    DependencyObject dep = (DependencyObject)sender;
                    while ((dep != null) && !(dep is ListBoxItem))
                    {
                        dep = VisualTreeHelper.GetParent(dep);

                    }
                    List<UIElement> itemstoremove = new List<UIElement>();
                    ListBoxItem objmyitem = (ListBoxItem)dep;
                    objmyitem.IsSelected = true;
                    //Start

                    if (previouscounter == lstFrame.SelectedIndex)
                    {
                        previouscounter = -1;
                        objmyitem.IsSelected = false;
                        //lstFrame.SelectedIndex = -1;
                        foreach (UIElement ui in frm.Children)
                        {
                            if (ui.Uid.StartsWith("frame"))
                            {
                                itemstoremove.Add(ui);
                            }
                        }
                        foreach (UIElement ui in itemstoremove)
                        {
                            frm.Children.Remove(ui);
                            selectedborder = string.Empty;
                            graphicsframeApplied = false;
                            VisualStateManager.GoToState(btnBorder, "Unchecked", true);

                            forWdht.Height = GrdSize.ActualHeight;
                            forWdht.Width = GrdSize.ActualWidth;
                            forWdht.InvalidateArrange();
                            forWdht.InvalidateMeasure();
                            forWdht.InvalidateVisual();

                            Zomout(true);
                        }
                        if (_ZoomFactor == .95)
                        {
                            _ZoomFactor = 1;

                            if (zoomTransform != null && _ZoomFactor >= .5)
                            {
                                if (FlipMode != 0 || FlipModeY != 0)
                                {
                                    zoomTransform.CenterX = mainImage.ActualWidth / 2;
                                    zoomTransform.CenterY = mainImage.ActualHeight / 2;

                                    zoomTransform.ScaleX = _ZoomFactor;
                                    zoomTransform.ScaleY = _ZoomFactor;
                                    transformGroup = new TransformGroup();
                                    transformGroup.Children.Add(zoomTransform);
                                    transformGroup.Children.Add(translateTransform);
                                    transformGroup.Children.Add(rotateTransform);
                                    canbackground.RenderTransform = GrdGreenScreenDefault3.RenderTransform = transformGroup;
                                    IsGraphicsChange = true;
                                }
                                else
                                {
                                    zoomTransform.CenterX = mainImage.ActualWidth / 2;
                                    zoomTransform.CenterY = mainImage.ActualHeight / 2;

                                    zoomTransform.ScaleX = _ZoomFactor;
                                    zoomTransform.ScaleY = _ZoomFactor;
                                    transformGroup = new TransformGroup();
                                    transformGroup.Children.Add(zoomTransform);
                                    transformGroup.Children.Add(translateTransform);
                                    transformGroup.Children.Add(rotateTransform);
                                    canbackground.RenderTransform = GrdGreenScreenDefault3.RenderTransform = transformGroup;
                                    IsGraphicsChange = true;
                                }
                                lblzoomplus.Content = _ZoomFactor * 100 + " % ";
                            }
                        }
                        return;
                    }
                    else
                    {
                        previouscounter = lstFrame.SelectedIndex;
                    }

                    //End

                    foreach (UIElement ui in frm.Children)
                    {
                        if (ui.Uid.StartsWith("frame"))
                        {
                            itemstoremove.Add(ui);
                        }
                    }
                    foreach (UIElement ui in itemstoremove)
                    {
                        frm.Children.Remove(ui);
                    }

                    int ProductType = Convert.ToInt32(CmbProductType.SelectedValue);
                    BitmapImage img = new BitmapImage();
                    BitmapImage img1 = new BitmapImage();
                    img1.BeginInit();
                    img1.UriSource = uri;
                    img1.EndInit();
                    img1.Freeze();
                    img.BeginInit();
                    img.UriSource = uri;
                    double ratio = 1;
                    if (ProductType == 1)
                    {
                        if (img1.Height > img1.Width)
                        {
                            img.DecodePixelHeight = 2400;
                            img.DecodePixelWidth = 1800;
                            img.EndInit();
                            ratio = (double)((double)img.PixelWidth / (double)img.PixelHeight);
                        }
                        else
                        {
                            img.DecodePixelHeight = 1800;
                            img.DecodePixelWidth = 2400;
                            img.EndInit();
                            ratio = (double)((double)img.PixelHeight / (double)img.PixelWidth);
                        }
                        if (widthimg.ActualHeight > widthimg.ActualWidth)
                        {
                            forWdht.Width = widthimg.ActualHeight * ratio;
                            forWdht.Height = widthimg.ActualHeight;
                        }
                        else
                        {
                            forWdht.Height = widthimg.ActualWidth * ratio;
                            forWdht.Width = widthimg.ActualWidth;
                        }
                    }
                    else if (ProductType == 2)
                    {
                        if (img1.Height > img1.Width)
                        {
                            img.DecodePixelHeight = 3000;
                            img.DecodePixelWidth = 2400;
                            img.EndInit();
                            ratio = (double)((double)img.PixelWidth / (double)img.PixelHeight);
                        }
                        else
                        {
                            img.DecodePixelHeight = 2400;
                            img.DecodePixelWidth = 3000;
                            img.EndInit();
                            ratio = (double)((double)img.PixelHeight / (double)img.PixelWidth);
                        }
                        if (widthimg.ActualHeight > widthimg.ActualWidth)
                        {
                            forWdht.Width = widthimg.ActualHeight * ratio;
                            forWdht.Height = widthimg.ActualHeight;
                        }
                        else
                        {
                            forWdht.Height = widthimg.ActualWidth * ratio;
                            forWdht.Width = widthimg.ActualWidth;
                        }
                    }
                    else if (ProductType == 30)
                    {
                        if (img1.Height > img1.Width)
                        {
                            img.DecodePixelHeight = 2700;
                            img.DecodePixelWidth = 1800;
                            img.EndInit();
                            ratio = (double)((double)img.PixelWidth / (double)img.PixelHeight);
                        }
                        else
                        {
                            img.DecodePixelHeight = 1800;
                            img.DecodePixelWidth = 2700;
                            img.EndInit();
                            ratio = (double)((double)img.PixelHeight / (double)img.PixelWidth);
                        }
                        if (widthimg.ActualHeight > widthimg.ActualWidth)
                        {
                            forWdht.Width = widthimg.ActualHeight * ratio;
                            forWdht.Height = widthimg.ActualHeight;
                        }
                        else
                        {
                            forWdht.Height = widthimg.ActualWidth * ratio;
                            forWdht.Width = widthimg.ActualWidth;
                        }
                    }
                    else if (ProductType == 98)
                    {
                        img.DecodePixelHeight = 900;
                        img.DecodePixelWidth = 900;
                        img.EndInit();
                        ratio = (double)((double)img.PixelWidth / (double)img.PixelHeight);
                        if (widthimg.ActualHeight > widthimg.ActualWidth)
                        {
                            forWdht.Width = widthimg.ActualHeight * ratio;
                            forWdht.Height = widthimg.ActualHeight;
                        }
                        else
                        {
                            forWdht.Height = widthimg.ActualWidth * ratio;
                            forWdht.Width = widthimg.ActualWidth;
                        }
                    }
                    else if (ProductType == 103)
                    {
                        if (img1.Height > img1.Width)
                        {
                            img.DecodePixelHeight = 2800;
                            img.DecodePixelWidth = 2000;
                            img.EndInit();
                            ratio = (double)((double)img.PixelWidth / (double)img.PixelHeight);
                        }
                        else
                        {
                            img.DecodePixelHeight = 2000;
                            img.DecodePixelWidth = 2800;
                            img.EndInit();
                            ratio = (double)((double)img.PixelHeight / (double)img.PixelWidth);
                        }
                        if (widthimg.ActualHeight > widthimg.ActualWidth)
                        {
                            forWdht.Width = widthimg.ActualHeight * ratio;
                            forWdht.Height = widthimg.ActualHeight;
                        }
                        else
                        {
                            forWdht.Height = widthimg.ActualWidth * ratio;
                            forWdht.Width = widthimg.ActualWidth;
                        }
                    }
                    else
                    {
                        if (img.Height > img.Width)  ////Img Border
                        {
                            ratio = (double)(img.Width / img.Height);
                        }
                        else
                        {
                            ratio = (double)(img.Height / img.Width);
                        }

                        if (forWdht.Height > forWdht.Width)
                        {
                            forWdht.Width = forWdht.Height * ratio;
                        }
                        else
                        {
                            forWdht.Height = forWdht.Width * ratio;
                        }
                        img.EndInit();
                    }
                    specproductType = Convert.ToString(ProductType);
                    OpaqueClickableImage objCurrent = new OpaqueClickableImage();
                    objCurrent.Uid = "frame";
                    objCurrent.Source = img;
                    objCurrent.Stretch = Stretch.Fill; //temprorary Fix till Crop works properly
                    objCurrent.IsHitTestVisible = false;
                    objCurrent.Loaded += new RoutedEventHandler(objCurrent_Loaded);
                    frm.Children.Add(objCurrent);
                    //if (ProductType == 98)
                    //{
                    //    forWdht.Height = img.Height;
                    //    forWdht.Width = img.Width;
                    //}
                    //forWdht.Height = img.Height;
                    //forWdht.Width = img.Width;
                    frm.Width = forWdht.Width;
                    frm.Height = forWdht.Height;
                    forWdht.InvalidateArrange();
                    forWdht.InvalidateMeasure();
                    forWdht.InvalidateVisual();
                    Zomout(true);
                    if (_ZoomFactor == 1)
                    {
                        _ZoomFactor = .95;

                        if (zoomTransform != null && _ZoomFactor >= .5)
                        {
                            if (FlipMode != 0 || FlipModeY != 0)
                            {
                                zoomTransform.CenterX = mainImage.ActualWidth / 2;
                                zoomTransform.CenterY = mainImage.ActualHeight / 2;

                                //zoomTransform.ScaleX = _ZoomFactor;
                                //zoomTransform.ScaleY = _ZoomFactor;
                                transformGroup = new TransformGroup();
                                transformGroup.Children.Add(zoomTransform);
                                transformGroup.Children.Add(translateTransform);
                                transformGroup.Children.Add(rotateTransform);
                                canbackground.RenderTransform = GrdGreenScreenDefault3.RenderTransform = transformGroup;
                                IsGraphicsChange = true;
                            }
                            else
                            {
                                zoomTransform.CenterX = mainImage.ActualWidth / 2;
                                zoomTransform.CenterY = mainImage.ActualHeight / 2;

                                zoomTransform.ScaleX = _ZoomFactor;
                                zoomTransform.ScaleY = _ZoomFactor;
                                transformGroup = new TransformGroup();
                                transformGroup.Children.Add(zoomTransform);
                                transformGroup.Children.Add(translateTransform);
                                transformGroup.Children.Add(rotateTransform);
                                canbackground.RenderTransform = GrdGreenScreenDefault3.RenderTransform = transformGroup;
                                IsGraphicsChange = true;
                            }
                            lblzoomplus.Content = _ZoomFactor * 100 + " % ";
                        }
                    }
                    img1.Freeze();
                    img.Freeze();
                    //DisposeImage(img);
                    // DisposeImage(img1);
                    VisualStateManager.GoToState(btnBorder, "Checked", true);
                    VisualStateManager.GoToState(btnAddgraphics, "Checked", true);
                    graphicsframeApplied = true;
                    IsSelectedMainImage = true;
                }
                #endregion
                #region Graphic Selected
                else if (lstGraphics.Visibility == System.Windows.Visibility.Visible)
                {
                    IsImageDirtyState = true;
                    Button btngrph = new Button();
                    btngrph.HorizontalAlignment = System.Windows.HorizontalAlignment.Center;
                    btngrph.VerticalAlignment = System.Windows.VerticalAlignment.Center;
                    Style defaultStyle = (Style)FindResource("ButtonStyleGraphic");
                    btngrph.Style = defaultStyle;
                    System.Windows.Controls.Image imgctrl = new System.Windows.Controls.Image();
                    BitmapImage img = new BitmapImage(new Uri(btn.Tag.ToString()));
                    imgctrl.Name = "A" + Guid.NewGuid().ToString().Split('-')[0].ToString();
                    btngrph.Name = "btn" + Guid.NewGuid().ToString().Split('-')[0].ToString();
                    btngrph.Uid = "uid" + Guid.NewGuid().ToString().Split('-')[0].ToString();
                    imgctrl.Source = img;
                    btngrph.Tag = "1";

                    TransformGroup scaltrsm = GrdZomout.LayoutTransform as TransformGroup;
                    ScaleTransform scTrans;
                    double wdt = 0, hgt = 0;
                    TransformCollection transformCol = scaltrsm.Children;
                    foreach (var test in transformCol)
                    {
                        scTrans = test as ScaleTransform;
                        if (scTrans != null)
                        {
                            wdt = scTrans.ScaleX;
                            hgt = scTrans.ScaleY;
                            break;
                        }
                    }

                    if (wdt != 0)
                    {
                        if (wdt < 1)
                        {
                            btngrph.Width = 90 / wdt;
                            btngrph.Height = 90 / hgt;
                        }
                        else
                        {
                            btngrph.Width = 90 * wdt;
                            btngrph.Height = 90 * hgt;
                        }
                    }
                    else
                    {
                        btngrph.Width = 90;
                        btngrph.Height = 90;
                    }

                    btngrph.Click += new RoutedEventHandler(btngrph_Click);
                    btngrph.Content = imgctrl;
                    dragCanvas.Children.Add(btngrph);
                    Canvas.SetLeft(btngrph, GrdBrightness.ActualWidth / 2);
                    Canvas.SetTop(btngrph, GrdBrightness.ActualHeight / 2);
                    imgctrl.MouseLeftButtonUp += new MouseButtonEventHandler(SelectObject);
                    btngrph.MouseLeftButtonUp += new MouseButtonEventHandler(SelectObject);
                    btngrph.GotFocus += new RoutedEventHandler(btngrph_GotFocus);
                    btngrph.Focus();
                    Canvas.SetZIndex(btngrph, 4);
                    VisualStateManager.GoToState(btngraphics, "Checked", true);
                    VisualStateManager.GoToState(btnAddgraphics, "Checked", true);
                    graphicsCount++;
                    img.Freeze();

                }
                #endregion
                #region Background Selected
                else if (lstBackground.Visibility == System.Windows.Visibility.Visible)
                {

                    DependencyObject dep = (DependencyObject)sender;
                    while ((dep != null) && !(dep is ListBoxItem))
                    {
                        dep = VisualTreeHelper.GetParent(dep);
                    }
                    List<UIElement> itemstoremove = new List<UIElement>();
                    ListBoxItem objmyitem = (ListBoxItem)dep;

                    bool borderTOBeApplied = true;
                    if (graphicsBorderApplied)
                    {
                        // Load Background on first click of chroma
                        //canbackgroundParent.Visibility = System.Windows.Visibility.Collapsed;
                        //canbackground.Visibility = System.Windows.Visibility.Collapsed;
                        //imageundoGrid.Visibility = System.Windows.Visibility.Collapsed;
                        imageundoGrid.Background = null;
                        graphicsBorderApplied = false;
                        //VisualStateManager.GoToState(btnBackground, "Unchecked", true);
                        ProducttypeforGS = 0;

                        // checking if it is clicked on same background
                        if (selectedbackground == (LoginUser.DigiFolderBackGroundPath + "\\8x10\\" + btn.CommandParameter.ToString()))
                        {
                            if (!isChromaApplied)
                                borderTOBeApplied = false;
                        }
                        else
                        {
                            borderTOBeApplied = true;
                        }
                        if (objmyitem != null)
                            objmyitem.IsSelected = false;
                    }

                    if (borderTOBeApplied)
                    {
                        selectedbackground = System.IO.Path.Combine(LoginUser.DigiFolderBackGroundPath, "8x10", tempFileName[tempFileName.Count() - 1]);
                        //selectedbackground = LoginUser.DigiFolderBackGroundPath + "\\" +"8x10"+"\\"+ tempFileName[tempFileName.Count() - 1];
                        BackgroundSelectedValue = tempFileName[tempFileName.Count() - 1];
                        if (string.IsNullOrEmpty(BackgroundSelectedValue) && string.IsNullOrWhiteSpace(BackgroundSelectedValue))
                        {
                            return;
                        }
                        if (objmyitem != null)
                            objmyitem.IsSelected = true;
                        canbackgroundParent.Visibility = System.Windows.Visibility.Visible;
                        canbackground.Visibility = System.Windows.Visibility.Visible;
                        imageundoGrid.Visibility = System.Windows.Visibility.Visible;

                        Uri uri = new Uri(System.IO.Path.Combine(LoginUser.DigiFolderBackGroundPath, "8x10", btn.CommandParameter.ToString()));
                        BackgroundBusiness backgBiz = new BackgroundBusiness();
                        ProducttypeforGS = backgBiz.GetProductTypeforBackgorund(btn.CommandParameter.ToString());
                        BitmapImage objCurrent = new BitmapImage(uri);
                        imageundoGrid.Background = new ImageBrush { ImageSource = objCurrent };
                        //VisualStateManager.GoToState(btnBackground, "Checked", true);
                        VisualStateManager.GoToState(btnAddgraphics, "Checked", true);
                        graphicsBorderApplied = true;
                        IsImageDirtyState = true;


                        if (canbackgroundold.Background != null)
                        {
                            canbackgroundold.Background.Opacity = 1;
                        }

                        if (frm.Children.Count == 0)
                        {

                            double ratio = 1;
                            if (objCurrent.Height > objCurrent.Width)
                            {
                                ratio = (double)(objCurrent.Width / objCurrent.Height);
                            }
                            else
                            {
                                ratio = (double)(objCurrent.Height / objCurrent.Width);
                            }

                            if (forWdht.Height > forWdht.Width)
                            {
                                forWdht.Width = forWdht.Height * ratio;
                            }
                            else
                            {
                                forWdht.Height = forWdht.Width * ratio;
                            }

                            forWdht.InvalidateArrange();
                            forWdht.InvalidateMeasure();
                            forWdht.InvalidateVisual();
                            imageundoGrid.Height = forWdht.Height;
                            imageundoGrid.Width = forWdht.Width;

                            imageundoGrid.InvalidateArrange();
                            imageundoGrid.InvalidateMeasure();
                            imageundoGrid.InvalidateVisual();

                            Zomout(true);
                        }
                        objCurrent.Freeze();
                    }
                    #region scene

                    ICacheRepository imixBackGroundCache = DataCacheFactory.GetFactory<ICacheRepository>(typeof(BackgroundCache).FullName);
                    ICacheRepository imixBorderCache = DataCacheFactory.GetFactory<ICacheRepository>(typeof(BorderCaches).FullName);
                    ICacheRepository imixSceneCache = DataCacheFactory.GetFactory<ICacheRepository>(typeof(SceneCache).FullName);
                    var dataBackground = (List<BackGroundInfo>)imixBackGroundCache.GetData();
                    var dataBorder = (List<BorderInfo>)imixBorderCache.GetData();
                    var dataScene = (List<SceneInfo>)imixSceneCache.GetData();
                    var bgDetails = dataBackground.Where(x => x.DG_BackGround_Image_Name.ToLower() == tempFileName[tempFileName.Count() - 1].ToLower()).FirstOrDefault();
                    var bgId = 0;
                    if (bgDetails != null)
                    {
                        bgId = bgDetails.DG_Background_pkey;
                        // ProductTypeId = bgDetails.DG_Product_Id;
                        ProductTypeId = 2;
                    }
                    dataScene = dataScene.Where(x => x.BackGroundId == bgId && x.IsActive == true).ToList();
                    if (IsSceneApply)
                    {
                        RemoveBorder(borderfilename, selectedbordername, ProductTypeId, sender);
                    }
                    if (dataScene != null && dataScene.Count > 0)
                    {
                        string finalBorderPath = string.Empty;
                        var sceneDetails = dataScene.FirstOrDefault();
                        var checkBorder = dataBorder.Where(x => x.DG_Borders_pkey == sceneDetails.BorderId).FirstOrDefault();
                        if (checkBorder != null)
                        {
                            finalBorderPath = checkBorder.DG_Border ?? string.Empty;
                            borderfilename = LoginUser.DigiFolderFramePath + "\\" + finalBorderPath;
                            selectedbordername = LoginUser.DigiFolderFramePath + "\\Thumbnails\\" + finalBorderPath;
                        }
                        else
                        {
                            ChromaBorderPath = string.Empty;
                        }



                        if (lstFrame.Visibility != System.Windows.Visibility.Visible && (!string.IsNullOrEmpty(finalBorderPath)))
                        {
                            ApplyBorder(borderfilename, selectedbordername, ProductTypeId, sender);
                            IsSceneApply = true;

                        }
                    }
                    else
                    {
                        IsSceneApply = false;
                    }
                    #endregion
                    if (islstBackgroundVisible)
                    {
                        //Load Background on first click of chroma
                        //GrdEffects.Visibility = System.Windows.Visibility.Collapsed;
                        //lstBackground.Visibility = System.Windows.Visibility.Collapsed;

                        islstBackgroundVisible = false;
                    }
                }
                #endregion
                #region Image Strip
                else if (lstStrip.Visibility == System.Windows.Visibility.Visible)
                {
                    DependencyObject dep = (DependencyObject)sender;
                    while ((dep != null) && !(dep is ListBoxItem))
                    {
                        dep = VisualTreeHelper.GetParent(dep);
                    }
                    ListBoxItem objmyitem = (ListBoxItem)dep;
                    objmyitem.IsSelected = true;
                    FrameworkHelper.MyImageClass objItem = ((FrameworkHelper.MyImageClass)(lstStrip.SelectedItem));
                    if (IsImageDirtyState && IsGraphicsChange)
                    {
                        SaveOnImageChange();
                        IsGraphicsChange = false;
                        if (IsImageDirtyState)
                            ((FrameworkHelper.MyImageClass)lstStrip.Items[selectedIndex]).SettingStatus = SettingStatus.SpecUpdated;
                    }
                    this.selectedIndex = lstStrip.Items.IndexOf(objItem);
                    ControlCleanup();
                    EditImage(objItem);
                }
                #endregion
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                MessageBox.Show(ex.Message.ToString(), "DigiPhoto", MessageBoxButton.OK);
            }
            finally
            {
                MemoryManagement.FlushMemory();
                // ChromaBorderPath = string.Empty;
            }
#if DEBUG
            if (watch != null)
                FrameworkHelper.CommonUtility.WatchStop(this.GetType().FullName + "." + System.Reflection.MethodBase.GetCurrentMethod().Name + "|", watch);
#endif
        }

        public void btnExit_Click(object sender, EventArgs e)
        {
#if DEBUG
            var watch = FrameworkHelper.CommonUtility.Watch();
            watch.Start();
#endif
            if (IsImageDirtyState == true)
            {
                string message = "Do you want to save the effects of the selected image?";
                string caption = "Save Box";
                System.Windows.Forms.MessageBoxButtons buttons = System.Windows.Forms.MessageBoxButtons.YesNo;
                System.Windows.Forms.DialogResult result;

                // Displays the MessageBox.

                result = System.Windows.Forms.MessageBox.Show(message, caption, buttons);

                if (result == System.Windows.Forms.DialogResult.Yes)
                {
                    SaveOnImageChange();
                }
                else
                {
                    if (IsChromaChanged)
                    {
                        CompleteRestore();
                    }
                }
            }
            else
            {
                if (IsChromaChanged)
                {
                    CompleteRestore();
                    SaveOnImageChange();
                }
            }
            CmbProductType.Visibility = Visibility.Collapsed;
            EffectsSender = null;
            ((ImageDownloader)Window.GetWindow(this)).UpdatedSelectedList = new ObservableCollection<MyImageClass>(lstMyImageClass);
            HideHandlerDialog();
#if DEBUG
            if (watch != null)
                FrameworkHelper.CommonUtility.WatchStop(this.GetType().FullName + "." + System.Reflection.MethodBase.GetCurrentMethod().Name + "|", watch);
#endif
        }

        /// <summary>
        /// Handles the Loaded event of the objCurrent control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        void objCurrent_Loaded(object sender, RoutedEventArgs e)
        {
            Canvas.SetLeft(frm, (dragCanvas.ActualWidth - frm.ActualWidth) / 2);
            Canvas.SetTop(frm, (dragCanvas.ActualHeight - frm.ActualHeight) / 2);
        }
        /// <summary>
        /// Handles the GotFocus event of the btngrph control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        void btngrph_GotFocus(object sender, RoutedEventArgs e)
        {
            this.elementForContextMenu = (Button)sender;
            IsSelectedMainImage = false;
        }
        /// <summary>
        /// Handles the Click event of the btngrph control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        void btngrph_Click(object sender, RoutedEventArgs e)
        {
            jrotate.Visibility = System.Windows.Visibility.Hidden;
            this.elementForContextMenu = (Button)sender;
            IsSelectedMainImage = false;
        }
        /// <summary>
        /// Selects the object.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="MouseButtonEventArgs"/> instance containing the event data.</param>
        private void SelectObject(object sender, MouseButtonEventArgs e)
        {
#if DEBUG
            var watch = FrameworkHelper.CommonUtility.Watch();
            watch.Start();
#endif
            try
            {
                if (e.LeftButton == MouseButtonState.Released)
                {
                    RemoveAllBorders();
                    if (sender is Button)
                    {
                        this.elementForContextMenu = (Button)sender;
                        IsSelectedMainImage = false;

                        RotateTransform rotation = this.elementForContextMenu.GetValue(Canvas.LayoutTransformProperty) as RotateTransform;
                        if (rotation != null)
                        {
                            jrotate.Angle = rotation.Angle;
                        }
                    }
                    else if (sender is TextBox)
                    {
                        TextBox tb = (TextBox)sender;
                        double top = Convert.ToDouble(tb.GetValue(Canvas.TopProperty));
                        double left = Convert.ToDouble(tb.GetValue(Canvas.LeftProperty));

                        tb.BorderBrush = System.Windows.Media.Brushes.OrangeRed;
                        this.elementForContextMenu = tb;
                        txtContent.Text = tb.Text;
                        txtContent.Focus();
                        tb.Focus();
                        IsSelectedMainImage = false;

                        RotateTransform rotation = tb.GetValue(Canvas.RenderTransformProperty) as RotateTransform;
                        if (rotation != null)
                        {
                            jrotate.Angle = rotation.Angle;
                        }
                        else
                        {
                            jrotate.Angle = 0;
                        }
                        cmbFont.SelectedItem = (System.Windows.Media.FontFamily)tb.FontFamily;
                        CmbFontSize.Text = tb.FontSize.ToString();
                        CmbColor.Text = ((System.Windows.Media.SolidColorBrush)tb.Foreground).ToString();
                        IsImageDirtyState = true;
                    }
                    else if (sender is System.Windows.Controls.Image)
                    {
                        object objParent = (System.Windows.Controls.Image)sender;
                        IsSelectedMainImage = false;
                        for (int i = 0; i < 3; i++)
                        {
                            objParent = VisualTreeHelper.GetParent(objParent as DependencyObject);
                            if (objParent is Button)
                            {
                                this.elementForContextMenu = (UIElement)objParent;
                                TransformGroup tg = this.elementForContextMenu.GetValue(Canvas.RenderTransformProperty) as TransformGroup;
                                RotateTransform rotation = new RotateTransform();

                                if (tg != null)
                                {
                                    if (tg.Children.Count > 0)
                                    {
                                        if (tg.Children[0] is RotateTransform)
                                        {
                                            rotation = (RotateTransform)tg.Children[0];
                                        }
                                    }
                                    if (tg.Children.Count > 1)
                                    {
                                        if (tg.Children[1] is RotateTransform)
                                        {
                                            rotation = (RotateTransform)tg.Children[1];
                                        }
                                    }
                                }

                                if (rotation != null)
                                {
                                    jrotate.Angle = rotation.Angle;
                                }
                                else
                                {
                                    jrotate.Angle = 0;
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }

#if DEBUG
            if (watch != null)
                FrameworkHelper.CommonUtility.WatchStop(this.GetType().FullName + "." + System.Reflection.MethodBase.GetCurrentMethod().Name + "|", watch);
#endif
        }

        /// <summary>
        /// Handles the Click event of the Button control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void Button_Click(object sender, RoutedEventArgs e)
        {
#if DEBUG
            var watch = FrameworkHelper.CommonUtility.Watch();
            watch.Start();
#endif

            CmbProductType.Visibility = Visibility.Collapsed;
            this.elementForContextMenu = null;
            jrotate.Visibility = System.Windows.Visibility.Hidden;
            TextBox txtTest = new TextBox();
            txtTest.ContextMenu = dragCanvas.ContextMenu;
            txtTest.Foreground = new SolidColorBrush(Colors.DarkRed);
            txtTest.Background = new SolidColorBrush(Colors.Transparent);
            txtTest.FontWeight = FontWeights.Bold;
            txtTest.FontSize = 20;
            CmbFontSize.SelectedIndex = 6;
            cmbFont.SelectedIndex = 0;
            CmbColor.SelectedIndex = 0;


            txtTest.FontFamily = (System.Windows.Media.FontFamily)new FontFamilyConverter().ConvertFromString("Arial");
            txtTest.Text = "Enter Text...";
            txtTest.RenderTransformOrigin = new System.Windows.Point(0.5, 0.5);
            txtTest.Uid = "txtblock";
            txtTest.BorderBrush = null;
            txtTest.Style = (Style)FindResource("SearchIDTB");
            RotateTransform rtm = new RotateTransform();
            txtTest.RenderTransform = rtm;
            txtTest.MouseLeftButtonUp += new MouseButtonEventHandler(SelectObject);
            txtTest.LostFocus += new RoutedEventHandler(txtContent_LostFocus);
            txtTest.GotFocus += new RoutedEventHandler(txtContent_GotFocus);
            txtTest.TextChanged += new TextChangedEventHandler(txtTest_TextChanged);
            txtContent.Text = txtTest.Text;
            dragCanvas.Children.Add(txtTest);
            Canvas.SetLeft(txtTest, GrdBrightness.ActualWidth / 2);
            Canvas.SetTop(txtTest, GrdBrightness.ActualHeight / 2);
            Canvas.SetZIndex(txtTest, 4);
            txtContent.Focus();
            VisualStateManager.GoToState(btnGraphicsText, "Checked", true);
            VisualStateManager.GoToState(btnAddgraphics, "Checked", true);
            graphicsTextBoxCount++;
            GrdEffects.Visibility = System.Windows.Visibility.Collapsed;

#if DEBUG
            if (watch != null)
                FrameworkHelper.CommonUtility.WatchStop(this.GetType().FullName + "." + System.Reflection.MethodBase.GetCurrentMethod().Name + "|", watch);
#endif
        }

        /// <summary>
        /// Handles the TextChanged event of the txtTest control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="TextChangedEventArgs"/> instance containing the event data.</param>
        void txtTest_TextChanged(object sender, TextChangedEventArgs e)
        {
            if ((sender is TextBox))
            {
                TextBox GraphicText = (TextBox)sender;
                txtContent.Text = GraphicText.Text;
            }
        }

        /// <summary>
        /// Handles the LostFocus event of the txtContent control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void txtContent_LostFocus(object sender, RoutedEventArgs e)
        {
            if ((this.elementForContextMenu is TextBox))
            {
                TextBox GraphicText = (TextBox)this.elementForContextMenu;
                GraphicText.BorderBrush = System.Windows.Media.Brushes.Transparent;
            }
        }
        /// <summary>
        /// Handles the GotFocus event of the txtContent control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void txtContent_GotFocus(object sender, RoutedEventArgs e)
        {
            if ((this.elementForContextMenu is TextBox))
            {
                TextBox GraphicText = (TextBox)this.elementForContextMenu;
                GraphicText.BorderBrush = System.Windows.Media.Brushes.OrangeRed;
            }
        }
        /// <summary>
        /// Handles the Click event of the btnSendtoBack control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void btnSendtoBack_Click(object sender, RoutedEventArgs e)
        {
            CmbProductType.Visibility = Visibility.Collapsed;
            if (this.elementForContextMenu != null)
            {
                this.dragCanvas.SendToBack(this.elementForContextMenu);
            }
        }
        /// <summary>
        /// Handles the Click event of the btnbringtofront control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void btnbringtofront_Click(object sender, RoutedEventArgs e)
        {
            CmbProductType.Visibility = Visibility.Collapsed;
            if (this.elementForContextMenu != null)
            {
                this.dragCanvas.BringToFront(this.elementForContextMenu);
            }
        }
        /// <summary>
        /// Handles the Click event of the btnframe control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void btnframe_Click(object sender, RoutedEventArgs e)
        {
            CmbProductType.Visibility = Visibility.Visible;
            jrotate.Visibility = System.Windows.Visibility.Hidden;
            lstBackground.Visibility = System.Windows.Visibility.Collapsed;
            lstGraphics.Visibility = System.Windows.Visibility.Collapsed;
            lstStrip.Visibility = System.Windows.Visibility.Collapsed;
            lstFrame.Visibility = System.Windows.Visibility.Visible;
            GrdEffects.Visibility = System.Windows.Visibility.Visible;
            loadImagestoList();
        }

        /// <summary>
        /// Handles the Click event of the btngraphics control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void btngraphics_Click(object sender, RoutedEventArgs e)
        {
            CmbProductType.Visibility = Visibility.Collapsed;
            jrotate.Visibility = System.Windows.Visibility.Hidden;
            lstBackground.Visibility = System.Windows.Visibility.Collapsed;
            lstFrame.Visibility = System.Windows.Visibility.Collapsed;
            lstStrip.Visibility = System.Windows.Visibility.Collapsed;
            lstGraphics.Visibility = System.Windows.Visibility.Visible;
            GrdEffects.Visibility = System.Windows.Visibility.Visible;

        }
        bool IsBtnBackgroundClicked = false;
        bool IsbtnBackgroundWithoutChroma = false;
        /// <summary>
        /// Handles the Click event of the btnBackground control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void btnBackground_Click(object sender, RoutedEventArgs e)
        {
            CmbProductType.Visibility = Visibility.Collapsed;
            jrotate.Visibility = System.Windows.Visibility.Hidden;
            lstFrame.Visibility = System.Windows.Visibility.Collapsed;
            lstGraphics.Visibility = System.Windows.Visibility.Collapsed;
            lstStrip.Visibility = System.Windows.Visibility.Collapsed;
            lstBackground.Visibility = System.Windows.Visibility.Visible;
            GrdEffects.Visibility = System.Windows.Visibility.Visible;
            IsbtnBackgroundWithoutChroma = true;
        }
        private void btnThumbnails_Click(object sender, RoutedEventArgs e)
        {
            if (ImageClick)
            {
                CmbProductType.Visibility = Visibility.Collapsed;
                jrotate.Visibility = System.Windows.Visibility.Hidden;
                lstFrame.Visibility = System.Windows.Visibility.Collapsed;
                lstGraphics.Visibility = System.Windows.Visibility.Collapsed;
                lstBackground.Visibility = Visibility.Collapsed;
                lstStrip.Visibility = Visibility.Visible;
                GrdEffects.Visibility = Visibility.Visible;
                ImageClick = false;
            }
            else
            {
                CmbProductType.Visibility = Visibility.Collapsed;
                jrotate.Visibility = System.Windows.Visibility.Hidden;
                lstFrame.Visibility = System.Windows.Visibility.Collapsed;
                lstGraphics.Visibility = System.Windows.Visibility.Collapsed;
                lstBackground.Visibility = System.Windows.Visibility.Collapsed;
                lstStrip.Visibility = System.Windows.Visibility.Collapsed;
                GrdEffects.Visibility = System.Windows.Visibility.Collapsed;
                ImageClick = true;
            }
        }

        #endregion

        #region Common Methods

        /// <summary>
        /// Adds the default items.
        /// </summary>
        private void AddDefaultItems()
        {
            CmbFontSize.Items.Clear();
            CmbFontSize.Items.Add("10");
            CmbFontSize.Items.Add("11");
            CmbFontSize.Items.Add("12");
            CmbFontSize.Items.Add("14");
            CmbFontSize.Items.Add("16");
            CmbFontSize.Items.Add("18");
            CmbFontSize.Items.Add("20");
            CmbFontSize.Items.Add("22");
            CmbFontSize.Items.Add("24");
            CmbFontSize.Items.Add("26");
            CmbFontSize.Items.Add("28");
            CmbFontSize.Items.Add("36");
            CmbFontSize.Items.Add("48");
            CmbFontSize.Items.Add("72");
            CmbFontSize.Items.Add("80");
            CmbFontSize.Items.Add("90");
            CmbFontSize.Items.Add("100");

        }

        /// <summary>
        /// Loads the frames.
        /// </summary>
        public void LoadFrames()
        {
            //if (_objDbLayer == null)
            //    _objDbLayer = new DigiPhotoDataServices();
            //DigiPhotoDataServices _objdataLayer = new DigiPhotoDataServices();
            string path = Common.LoginUser.DigiFolderFramePath + "\\Thumbnails\\";
            Frames.Clear();
            BorderBusiness bordBiz = new BorderBusiness();
            var borders = bordBiz.GetBorderDetails().Where(t => t.DG_IsActive == true);
            foreach (var item in borders)
            {

                LstMyItems _objnew = new LstMyItems();
                _objnew.FilePath = path + item.DG_Border;
                _objnew.Name = item.DG_Border;
                _objnew.Photoname1 = item.DG_Border;
                Frames.Add(_objnew);

            }

        }

        /// <summary>
        /// Loads the background.
        /// </summary>
        public void LoadBackground()
        {
            //if (IsGreenScreenPrintingEnabled())
            //{
            //    //load background based on spec config
            //    //DigiPhotoDataServices _objdataLayer = new DigiPhotoDataServices();
            //    DG_SemiOrder_Settings objDG_SemiOrder_Settings = _objDbLayer.GetSemiOrderSetting();
            //    string pathbackground = Common.LoginUser.DigiFolderBackGroundPath + "\\Thumbnails\\";
            //    lstBackground.Items.Clear();
            //    List<DG_BackGround> lstBackgrounds = _objDbLayer.GetBackGroundProductwise((int)objDG_SemiOrder_Settings.DG_SemiOrder_ProductTypeId);
            //    foreach (var item in lstBackgrounds)
            //    {
            //        Uri uri = new Uri(pathbackground + item.DG_BackGround_Image_Name);
            //        LstMyItems _objnew = new LstMyItems();
            //        //BitmapImage bmp = new BitmapImage(uri);
            //        _objnew.BmpImage = new BitmapImage(uri);
            //        _objnew.Name = item.DG_BackGround_Image_Name;
            //        _objnew.Photoname1 = item.DG_BackGround_Image_Display_Name;
            //        _objnew.FilePath = pathbackground + item.DG_BackGround_Image_Name;
            //        lstBackground.Items.Add(_objnew);
            //    }
            //}
            //else
            //{
            //DigiPhotoDataServices _objdataLayer = new DigiPhotoDataServices();
            //if (_objDbLayer == null)
            //    _objDbLayer = new DigiPhotoDataServices();
            string pathbackground = Common.LoginUser.DigiFolderBackGroundPath + "\\Thumbnails\\";
            lstBackground.Items.Clear();
            BackgroundBusiness backgBiz = new BackgroundBusiness();
            foreach (var item in backgBiz.GetBackgoundDetailsALL())
            {
                if (item.DG_Background_IsActive.HasValue && item.DG_Background_IsActive.Value)
                {
                    Uri uri = new Uri(pathbackground + item.DG_BackGround_Image_Name);
                    LstMyItems _objnew = new LstMyItems();
                    if (File.Exists(uri.ToString()))
                    {
                        BitmapImage bmp = new BitmapImage(uri);
                        _objnew.BmpImage = bmp;
                    }
                    _objnew.Name = item.DG_BackGround_Image_Name;
                    _objnew.Photoname1 = item.DG_BackGround_Image_Display_Name;
                    _objnew.FilePath = pathbackground + item.DG_BackGround_Image_Name;
                    lstBackground.Items.Add(_objnew);
                }
            }
            //}
        }
        /// <summary>
        /// Loads the graphics.
        /// </summary>
        public void LoadGraphics()
        {
            //DigiPhotoDataServices _objdataLayer = new DigiPhotoDataServices();
            string pathgraphics = LoginUser.DigiFolderGraphicsPath + "\\";
            lstGraphics.Items.Clear();
            GraphicsBusiness grpBiz = new GraphicsBusiness();
            foreach (var item in grpBiz.GetGraphicsDetails().Where(t => t.DG_Graphics_IsActive == true))
            {
                Uri uri = new Uri(pathgraphics + item.DG_Graphics_Name);
                LstMyItems _objnew = new LstMyItems();
                if (File.Exists(uri.ToString()))
                {
                    BitmapImage bmp = new BitmapImage(uri);
                    _objnew.BmpImage = bmp;
                }
                _objnew.Name = item.DG_Graphics_Name;
                _objnew.Photoname1 = item.DG_Graphics_Displayname;
                _objnew.FilePath = pathgraphics + item.DG_Graphics_Name;
                lstGraphics.Items.Add(_objnew);
            }


        }

        /// <summary>
        /// Ups this instance.
        /// </summary>
        void up()
        {
            y = y - 2;
            Canvas.SetTop(dragCanvas, y);
        }
        /// <summary>
        /// Downs this instance.
        /// </summary>
        void down()
        {
            y = y + 2;
            Canvas.SetTop(dragCanvas, y);
        }
        /// <summary>
        /// Lefts this instance.
        /// </summary>
        void left()
        {
            x = x - 2;
            Canvas.SetLeft(dragCanvas, x);
        }
        /// <summary>
        /// Rights this instance.
        /// </summary>
        void right()
        {
            x = x + 2;
            Canvas.SetLeft(dragCanvas, x);
        }

        /// <summary>
        /// Removes all borders.
        /// </summary>
        public void RemoveAllBorders()
        {
            List<UIElement> itemstoremove = new List<UIElement>();
            foreach (UIElement ui in dragCanvas.Children)
            {
                if (ui.Uid.StartsWith("brdr"))
                {
                    itemstoremove.Add(ui);
                }
                else if (ui is TextBox)
                {
                    TextBox tx = (TextBox)ui;
                    tx.BorderBrush = null;
                }
            }
            foreach (UIElement ui in itemstoremove)
            {
                double top = Convert.ToDouble(ui.GetValue(Canvas.TopProperty));
                double left = Convert.ToDouble(ui.GetValue(Canvas.LeftProperty));
                string[] splitter = new string[] { "@#@" };
                string Name = ui.Uid.ToString().Split(splitter, StringSplitOptions.None)[1];

                string strUri2 = @"D:\\Projects\\DigiPhoto\\Images\\" + Name.ToString();

                RotateTransform rt = ui.RenderTransform as RotateTransform;

                OpaqueClickableImage img = new OpaqueClickableImage();
                img.Source = new BitmapImage(new Uri(strUri2));
                img.SetValue(Canvas.TopProperty, top);
                img.SetValue(Canvas.LeftProperty, left);
                img.MouseLeftButtonUp += new MouseButtonEventHandler(SelectObject);

                if (rt != null)
                {
                    RotateTransform rtm = new RotateTransform();
                    rtm.CenterX = 0;
                    rtm.CenterY = 0;
                    rtm.Angle = rt.Angle;
                    img.RenderTransform = rtm;
                }
                dragCanvas.Children.Remove(ui);
                dragCanvas.Children.Add(img);
                img.Source = null;
            }
        }
        /// <summary>
        /// Resets the z order.
        /// </summary>
        private void ResetZOrder()
        {
            // Set the z-index of every visible child in the Canvas.
            int index = 0;
            for (int i = 0; i < this.dragCanvas.Children.Count; ++i)
                if (this.dragCanvas.Children[i].Visibility == Visibility.Visible)
                    Canvas.SetZIndex(this.dragCanvas.Children[i], index++);
        }
        #endregion

        /// <summary>
        /// Converts the automatic bitmap.
        /// </summary>
        /// <returns></returns>
        public BitmapImage ConvertToBitmap()
        {
#if DEBUG
            var watch = FrameworkHelper.CommonUtility.Watch();
            watch.Start();
#endif
            //  System.Diagnostics.Stopwatch sw = new System.Diagnostics.Stopwatch();
            //   sw.Start();


            JpegBitmapEncoder png = new JpegBitmapEncoder();
            BitmapImage bmapImage = new BitmapImage();
            MemoryStream stream = new MemoryStream();

            var target = new RenderTargetBitmap((int)(forWdht.RenderSize.Width), (int)(forWdht.RenderSize.Height), 96, 96, PixelFormats.Pbgra32);
            var brush = new VisualBrush(forWdht);

            var visual = new DrawingVisual();
            var drawingContext = visual.RenderOpen();


            drawingContext.DrawRectangle(brush, null, new Rect(new Point(0, 0),
                new Point(forWdht.RenderSize.Width, forWdht.RenderSize.Height)));

            drawingContext.Close();

            target.Render(visual);


            png.Frames.Add(BitmapFrame.Create(target));
            png.Save(stream);

            stream.Position = 0;
            byte[] data = new byte[stream.Length];
            stream.Read(data, 0, Convert.ToInt32(stream.Length));
            bmapImage.BeginInit();
            bmapImage.StreamSource = stream;
            bmapImage.CacheOption = BitmapCacheOption.OnLoad;
            bmapImage.EndInit();

            bmapImage.Freeze();
            //DisposeImage(bmapImage);
            target.Freeze();


#if DEBUG
            if (watch != null)
                FrameworkHelper.CommonUtility.WatchStop(this.GetType().FullName + "." + System.Reflection.MethodBase.GetCurrentMethod().Name + "|", watch);
#endif
            return bmapImage;
        }

        /// <summary>
        /// Compilephotoes the specified dpi.
        /// </summary>
        /// <param name="dpi">The dpi.</param>
        /// <param name="PrintJob">if set to <c>true</c> [print job].</param>
        /// <returns></returns>
        public BitmapImage compilephoto(double dpi, bool PrintJob)
        {
#if DEBUG
            var watch = FrameworkHelper.CommonUtility.Watch();
            watch.Start();
#endif
            MemoryManagement.FlushMemory();

            TiffBitmapEncoder png = new TiffBitmapEncoder();
            BitmapImage bmapImage = new BitmapImage();
            MemoryStream stream = new MemoryStream();
            RenderTargetBitmap renderTargetBitmap;

            try
            {

                Transform tg = forWdht.RenderTransform;

                if (tg is RotateTransform)
                {
                    RotateTransform rt = (RotateTransform)tg;
                    if (rt.Angle == 270 || rt.Angle == 90)
                    {
                        renderTargetBitmap = CaptureScreen(forWdht, dpi, dpi, true);
                    }
                    else
                    {
                        renderTargetBitmap = CaptureScreen(forWdht, dpi, dpi, false);
                    }

                }
                else
                {
                    renderTargetBitmap = CaptureScreen(forWdht, dpi, dpi, false);
                }

                renderTargetBitmap.Freeze();
                png.Frames.Add(BitmapFrame.Create(renderTargetBitmap));
                png.Save(stream);

                stream.Position = 0;
                byte[] data = new byte[stream.Length];
                stream.Read(data, 0, Convert.ToInt32(stream.Length));
                bmapImage.BeginInit();
                bmapImage.StreamSource = stream;
                bmapImage.CacheOption = BitmapCacheOption.OnLoad;
                bmapImage.EndInit();

                bmapImage.Freeze();
                //DisposeImage(bmapImage);

                return bmapImage;
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
            finally
            {
                bmapImage = null;
                png = null;
                stream = null;
                renderTargetBitmap = null;
                MemoryManagement.FlushMemory();
            }

#if DEBUG
            if (watch != null)
                FrameworkHelper.CommonUtility.WatchStop(this.GetType().FullName + "." + System.Reflection.MethodBase.GetCurrentMethod().Name + "|", watch);
#endif
            return bmapImage;
        }

        /// <summary>
        /// Captures the screen.
        /// </summary>
        /// <param name="target">The target.</param>
        /// <param name="dpiX">The dpi executable.</param>
        /// <param name="dpiY">The dpi asynchronous.</param>
        /// <param name="Isrotate">if set to <c>true</c> [isrotate].</param>
        /// <returns></returns>
        private static RenderTargetBitmap CaptureScreen(Visual target, double dpiX, double dpiY, bool Isrotate)
        {
#if DEBUG
            var watch = FrameworkHelper.CommonUtility.Watch();
            watch.Start();
#endif
            DrawingVisual dv = new DrawingVisual();
            //TiffBitmapEncoder png = new TiffBitmapEncoder();
            RenderTargetBitmap rtb = null;
            MemoryManagement.FlushMemory();
            try
            {
                if (target == null)
                {
                    return null;
                }

                RenderOptions.SetBitmapScalingMode(target, BitmapScalingMode.HighQuality);
                RenderOptions.SetEdgeMode(target, EdgeMode.Aliased);


                Rect bounds = VisualTreeHelper.GetDescendantBounds(target);

                if (Isrotate)
                {
                    rtb = new RenderTargetBitmap((int)(bounds.Height * CroppingAdorner.s_dpiY / 96.0),
                                                                    (int)(bounds.Width * CroppingAdorner.s_dpiX / 96.0),
                                                                    CroppingAdorner.s_dpiY,
                                                                    CroppingAdorner.s_dpiX,
                                                                    PixelFormats.Default);

                }
                else
                {
                    rtb = new RenderTargetBitmap((int)(bounds.Width * CroppingAdorner.s_dpiX / 96.0),
                                                                  (int)(bounds.Height * CroppingAdorner.s_dpiY / 96.0),
                                                                  CroppingAdorner.s_dpiX,
                                                                  CroppingAdorner.s_dpiY,
                                                                  PixelFormats.Default);

                    // rtb = ImageHelper.RenderVisualToImageSource(target);
                }




                using (DrawingContext ctx = dv.RenderOpen())
                {
                    VisualBrush vb = new VisualBrush(target);
                    if (Isrotate)
                    {
                        ctx.DrawRectangle(vb, null, new Rect(new System.Windows.Point(), new Size(bounds.Size.Height, bounds.Size.Width)));
                    }
                    else
                    {
                        ctx.DrawRectangle(vb, null, new Rect(new System.Windows.Point(), bounds.Size));

                    }
                }

                rtb.Render(dv);
                // MemoryStream stream = new MemoryStream();
                //png.Compression = TiffCompressOption.None;
                //png.Frames.Add(BitmapFrame.Create(rtb));
                //png.Save(stream);



                //using (var fileStream = new FileStream(@"E:\renderTargetBitmap.png", FileMode.Create, FileAccess.ReadWrite))
                //{
                //    PngBitmapEncoder encoder = new PngBitmapEncoder();
                //    encoder.Interlace = PngInterlaceOption.On;
                //    encoder.Frames.Add(BitmapFrame.Create(rtb));
                //    encoder.Save(fileStream);
                //}

#if DEBUG
                if (watch != null)
                    FrameworkHelper.CommonUtility.WatchStop(typeof(MainWindow).Name + "." + System.Reflection.MethodBase.GetCurrentMethod().Name + "|", watch);
#endif
                return rtb;
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                return rtb;
            }
            finally
            {

                dv = null;
                target = null;
                MemoryManagement.FlushMemory();
            }

        }

        /// <summary>
        /// Captures the screen for crop.
        /// </summary>
        /// <param name="grdzoomOut">The grdzoom out.</param>
        /// <param name="dpiX">The dpi executable.</param>
        /// <param name="dpiY">The dpi asynchronous.</param>
        /// <param name="Isrotate">if set to <c>true</c> [isrotate].</param>
        /// <returns></returns>
        private RenderTargetBitmap CaptureScreenForCrop(Grid grdzoomOut, double dpiX, double dpiY, bool Isrotate)
        {
#if DEBUG
            var watch = FrameworkHelper.CommonUtility.Watch();
            watch.Start();
#endif

            RenderTargetBitmap renderBitmap = null;
            try
            {

                RenderOptions.SetBitmapScalingMode(grdzoomOut, BitmapScalingMode.NearestNeighbor);
                RenderOptions.SetEdgeMode(grdzoomOut, EdgeMode.Aliased);

                //TiffBitmapEncoder png = new TiffBitmapEncoder();
                Size size = new Size(grdzoomOut.ActualWidth, grdzoomOut.ActualHeight);

                renderBitmap = new RenderTargetBitmap((int)(size.Width * CroppingAdorner.s_dpiX / 96), (int)(size.Height * CroppingAdorner.s_dpiY / 96), CroppingAdorner.s_dpiX, CroppingAdorner.s_dpiY, PixelFormats.Default);// PixelFormats.Pbgra32);
                RenderOptions.SetEdgeMode(grdzoomOut, EdgeMode.Aliased);
                RenderOptions.SetBitmapScalingMode(grdzoomOut, BitmapScalingMode.NearestNeighbor);
                grdzoomOut.SnapsToDevicePixels = true;
                grdzoomOut.RenderTransform = new ScaleTransform(1, 1, 0.5, 0.5);
                grdzoomOut.Measure(size);
                grdzoomOut.Arrange(new Rect(size));
                renderBitmap.Render(grdzoomOut);

                grdzoomOut.RenderTransform = null;
#if DEBUG
                if (watch != null)
                    FrameworkHelper.CommonUtility.WatchStop(this.GetType().FullName + "." + System.Reflection.MethodBase.GetCurrentMethod().Name + "|", watch);
#endif
                return renderBitmap;

            }
            catch (Exception ex)
            {
                //TiffBitmapEncoder png = new TiffBitmapEncoder();

                //Size size = new Size(grdzoomOut.ActualWidth * 1, grdzoomOut.ActualHeight * 1);
                //renderBitmap = new RenderTargetBitmap((int)(size.Width * CroppingAdorner.s_dpiY / 96.0), (int)(size.Height * CroppingAdorner.s_dpiY / 96.0), CroppingAdorner.s_dpiX, CroppingAdorner.s_dpiY, PixelFormats.Default);// PixelFormats.Pbgra32);
                //RenderOptions.SetEdgeMode(grdzoomOut, EdgeMode.Aliased);
                //RenderOptions.SetBitmapScalingMode(grdzoomOut, BitmapScalingMode.HighQuality);
                //grdzoomOut.SnapsToDevicePixels = true;
                //grdzoomOut.RenderTransform = new ScaleTransform(1, 1, 0.5, 0.5);
                //grdzoomOut.Measure(size);
                //grdzoomOut.Arrange(new Rect(size));

                //renderBitmap.Render(grdzoomOut);
                //grdzoomOut.RenderTransform = null;

                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                return renderBitmap;
            }
            finally
            {
                MemoryManagement.FlushMemory();
            }

        }

        /// <summary>
        /// Resizes the WPF image.
        /// </summary>
        /// <param name="sourceImage">The source image.</param>
        /// <param name="maxHeight">The maximum height.</param>
        /// <param name="saveToPath">The save automatic path.</param>
        private void ResizeWPFImage(BitmapSource sourceImage, int maxHeight, string saveToPath)
        {
            try
            {

                //BitmapImage bi = new BitmapImage();
                //BitmapImage bitmapImage = new BitmapImage();
                //using (FileStream fileStream = File.OpenRead(sourceImage.ToString()))
                //{
                //    MemoryStream ms = new MemoryStream();
                //    fileStream.CopyTo(ms);
                //    ms.Seek(0, SeekOrigin.Begin);
                //    fileStream.Close();
                //    bi.BeginInit();
                //    bi.StreamSource = ms;
                //    bi.EndInit();
                //    bi.Freeze();

                //    decimal ratio = Convert.ToDecimal(bi.Width) / Convert.ToDecimal(bi.Height);

                //    int newWidth = Convert.ToInt32(maxHeight * ratio);
                //    int newHeight = maxHeight;

                //    ms.Seek(0, SeekOrigin.Begin);
                //    bitmapImage.BeginInit();
                //    bitmapImage.StreamSource = ms;
                //    bitmapImage.DecodePixelWidth = newWidth;
                //    bitmapImage.DecodePixelHeight = newHeight;
                //    bitmapImage.EndInit();
                //    bitmapImage.Freeze();

                //}

                using (var fileStreamForSave = new FileStream(saveToPath, FileMode.Create, FileAccess.ReadWrite))
                {
                    JpegBitmapEncoder encoder = new JpegBitmapEncoder();

                    encoder.QualityLevel = 94;
                    encoder.Frames.Add(BitmapFrame.Create(sourceImage));
                    encoder.Save(fileStreamForSave);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }

        /// <summary>
        /// Captures the screen for green screen.
        /// </summary>
        /// <param name="forWdht">For WDHT.</param>
        /// <param name="dpiX">The dpi executable.</param>
        /// <param name="dpiY">The dpi asynchronous.</param>
        /// <param name="Isrotate">if set to <c>true</c> [isrotate].</param>
        /// <returns></returns>
        private RenderTargetBitmap CaptureScreenForGreenScreen(Grid forWdht, double dpiX, double dpiY, bool Isrotate)
        {
#if DEBUG
            var watch = FrameworkHelper.CommonUtility.Watch();
            watch.Start();
#endif

            RenderTargetBitmap renderBitmap = null;
            try
            {
                RenderOptions.SetBitmapScalingMode(forWdht, BitmapScalingMode.HighQuality);
                RenderOptions.SetEdgeMode(forWdht, EdgeMode.Aliased);
                Size size = new Size(forWdht.ActualWidth, forWdht.ActualHeight);

                if (_ZoomFactor > 1.4)
                {
                    renderBitmap = new RenderTargetBitmap((int)(size.Width * CroppingAdorner.s_dpiY / 96.0 * (1 / (_ZoomFactor))), (int)(size.Height * CroppingAdorner.s_dpiY / 96.0 * (1 / (_ZoomFactor))), CroppingAdorner.s_dpiX, CroppingAdorner.s_dpiY, PixelFormats.Default);// PixelFormats.Pbgra32);
                    RenderOptions.SetEdgeMode(forWdht, EdgeMode.Aliased);
                    forWdht.SnapsToDevicePixels = true;
                    forWdht.RenderTransform = new ScaleTransform(1 / (_ZoomFactor), 1 / (_ZoomFactor), 0.5, 0.5);
                }
                else
                {
                    if (_ZoomFactor > .1)
                    {
                        renderBitmap = new RenderTargetBitmap((int)(size.Width * CroppingAdorner.s_dpiY / 96.0), (int)(size.Height * CroppingAdorner.s_dpiY / 96.0), CroppingAdorner.s_dpiX, CroppingAdorner.s_dpiY, PixelFormats.Pbgra32);// PixelFormats.Pbgra32);
                        RenderOptions.SetEdgeMode(forWdht, EdgeMode.Aliased);
                        forWdht.SnapsToDevicePixels = true;
                        forWdht.RenderTransform = new ScaleTransform(1, 1, 0.5, 0.5);
                    }
                    else
                    {
                        renderBitmap = new RenderTargetBitmap((int)(size.Width * CroppingAdorner.s_dpiY / 96.0 * _ZoomFactor), (int)(size.Height * CroppingAdorner.s_dpiY / 96.0 * _ZoomFactor), CroppingAdorner.s_dpiX, CroppingAdorner.s_dpiY, PixelFormats.Default);// PixelFormats.Pbgra32);
                        RenderOptions.SetEdgeMode(forWdht, EdgeMode.Aliased);
                        forWdht.SnapsToDevicePixels = true;
                        forWdht.RenderTransform = new ScaleTransform(_ZoomFactor, _ZoomFactor, 0.5, 0.5);
                    }
                }

                forWdht.Measure(size);
                forWdht.Arrange(new Rect(size));
                renderBitmap.Render(forWdht);
                forWdht.RenderTransform = null;
#if DEBUG
                if (watch != null)
                    FrameworkHelper.CommonUtility.WatchStop(this.GetType().FullName + "." + System.Reflection.MethodBase.GetCurrentMethod().Name + "|", watch);
#endif
                return renderBitmap;
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                return renderBitmap;
            }
            finally
            {
                //MemoryManagement.FlushMemory();
            }
        }

        private RenderTargetBitmap CaptureScreenForPNGImage(Grid forWdht, double dpiX, double dpiY, bool Isrotate)
        {
            RenderTargetBitmap renderBitmap = null;
            try
            {
                RenderOptions.SetBitmapScalingMode(forWdht, BitmapScalingMode.HighQuality);
                RenderOptions.SetEdgeMode(forWdht, EdgeMode.Aliased);
                Size size = new Size(forWdht.Width, forWdht.Height);

                if (_ZoomFactor > 1.4)
                {
                    renderBitmap = new RenderTargetBitmap((int)(size.Width * CroppingAdorner.s_dpiY / 96.0 * (1 / (_ZoomFactor))), (int)(size.Height * CroppingAdorner.s_dpiY / 96.0 * (1 / (_ZoomFactor))), CroppingAdorner.s_dpiX, CroppingAdorner.s_dpiY, PixelFormats.Default);// PixelFormats.Pbgra32);
                    RenderOptions.SetEdgeMode(forWdht, EdgeMode.Aliased);
                    forWdht.SnapsToDevicePixels = true;
                    forWdht.RenderTransform = new ScaleTransform(1 / (_ZoomFactor), 1 / (_ZoomFactor), 0.5, 0.5);
                }
                else
                {
                    if (_ZoomFactor > .1)
                    {
                        renderBitmap = new RenderTargetBitmap((int)(size.Width * CroppingAdorner.s_dpiY / 96.0), (int)(size.Height * CroppingAdorner.s_dpiY / 96.0), CroppingAdorner.s_dpiX, CroppingAdorner.s_dpiY, PixelFormats.Pbgra32);// PixelFormats.Pbgra32);
                        RenderOptions.SetEdgeMode(forWdht, EdgeMode.Aliased);
                        forWdht.SnapsToDevicePixels = true;
                        forWdht.RenderTransform = new ScaleTransform(1, 1, 0.5, 0.5);
                    }
                    else
                    {
                        renderBitmap = new RenderTargetBitmap((int)(size.Width * CroppingAdorner.s_dpiY / 96.0 * _ZoomFactor), (int)(size.Height * CroppingAdorner.s_dpiY / 96.0 * _ZoomFactor), CroppingAdorner.s_dpiX, CroppingAdorner.s_dpiY, PixelFormats.Default);// PixelFormats.Pbgra32);
                        RenderOptions.SetEdgeMode(forWdht, EdgeMode.Aliased);
                        forWdht.SnapsToDevicePixels = true;
                        forWdht.RenderTransform = new ScaleTransform(_ZoomFactor, _ZoomFactor, 0.5, 0.5);
                    }
                }

                forWdht.Measure(size);
                forWdht.Arrange(new Rect(size));
                renderBitmap.Render(forWdht);
                forWdht.RenderTransform = null;
                return renderBitmap;
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                return renderBitmap;
            }
            finally
            {
                //MemoryManagement.FlushMemory();
            }
        }

        /// <summary>
        /// Compiles the effect changed.
        /// </summary>
        /// <param name="compiledBitmapImage">The compiled bitmap image.</param>
        /// <param name="ProductType">Type of the product.</param>
        /// <param name="Height">The height.</param>
        /// <param name="Width">The width.</param>
        public void CompileEffectChanged(VisualBrush compiledBitmapImage, int ProductType, int Height, int Width)
        {
#if DEBUG
            var watch = FrameworkHelper.CommonUtility.Watch();
            watch.Start();
#endif

            /// CompileEffectChanged(BitmapImage compiledBitmapImage, int ProductType)
            try
            {
                ClientView window = null;

                foreach (Window wnd in Application.Current.Windows)
                {
                    if (wnd.Title == "ClientView")
                    {
                        window = (ClientView)wnd;
                    }
                }

                if (window == null)
                {

                    window = new ClientView();
                    window.WindowStartupLocation = System.Windows.WindowStartupLocation.Manual;

                }


                window.GroupView = false;
                window.DefaultView = false;

                if (compiledBitmapImage != null)
                {
                    window.imgDefault.Visibility = System.Windows.Visibility.Collapsed;
                    window.instructionVideo.Visibility = System.Windows.Visibility.Collapsed;
                    window.instructionVideo.Pause();

                    window.imgDefaultBlur.Visibility = System.Windows.Visibility.Collapsed;
                    window.instructionVideoBlur.Visibility = System.Windows.Visibility.Collapsed;
                    window.instructionVideoBlur.Pause();

                    window.testR.Fill = null;
                    compiledBitmapImage.Stretch = Stretch.Uniform;
                    window.testR.Fill = compiledBitmapImage;
                    window.Photoname = PhotoName;

                }
                else
                {
                    GetMktImgInfo();
                    if (!(MktImgPath == "" || mktImgTime == 0))
                    {
                        window.instructionVideo.Visibility = System.Windows.Visibility.Visible;
                        window.instructionVideo.Play();
                        window.instructionVideoBlur.Visibility = System.Windows.Visibility.Visible;
                        window.instructionVideoBlur.Play();
                    }
                    else
                    {
                        window.imgDefault.Visibility = System.Windows.Visibility.Visible;
                        window.imgDefaultBlur.Visibility = System.Windows.Visibility.Visible;
                    }
                    window.testR.Fill = null;
                    window.DefaultView = true;
                }
                if (window.instructionVideo.Visibility == System.Windows.Visibility.Visible)
                    window.instructionVideo.Play();
                else
                    window.instructionVideo.Pause();

                if (window.instructionVideoBlur.Visibility == System.Windows.Visibility.Visible)
                    window.instructionVideoBlur.Play();
                else
                    window.instructionVideoBlur.Pause();

                System.Windows.Forms.Screen[] screens = System.Windows.Forms.Screen.AllScreens;
                if (screens.Length > 1)
                {
                    if (screens[0].Primary)
                    {
                        System.Windows.Forms.Screen s2 = System.Windows.Forms.Screen.AllScreens[1];
                        System.Drawing.Rectangle r2 = s2.WorkingArea;

                        window.Top = r2.Top;
                        window.Left = r2.Left;
                        //
                        window.Show();
                    }
                    else
                    {
                        System.Drawing.Rectangle r2 = screens[0].WorkingArea;
                        window.Top = r2.Top;
                        window.Left = r2.Left;
                        window.Show();
                    }

                }
                else
                {
                    window.Show();
                }

            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
            finally
            {
                MemoryManagement.FlushMemory();
            }

#if DEBUG
            if (watch != null)
                FrameworkHelper.CommonUtility.WatchStop(this.GetType().FullName + "." + System.Reflection.MethodBase.GetCurrentMethod().Name + "|", watch);
#endif
        }

        /// <summary>
        /// Handles the Click event of the btndelete control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void btndelete_Click(object sender, RoutedEventArgs e)
        {
#if DEBUG
            var watch = FrameworkHelper.CommonUtility.Watch();
            watch.Start();
#endif
            try
            {
                CmbProductType.Visibility = Visibility.Collapsed;
                if (this.elementForContextMenu != null)
                {
                    if (this.elementForContextMenu is TextBox)
                    {
                        TextBox parent = (TextBox)this.elementForContextMenu;
                        if (parent != null)
                        {
                            dragCanvas.Children.Remove(parent);
                            if (graphicsTextBoxCount >= 1)
                            {
                                graphicsTextBoxCount--;
                            }
                            if (graphicsTextBoxCount == 0)
                            {
                                VisualStateManager.GoToState(btnGraphicsText, "Unchecked", true);
                            }
                        }
                    }
                    else if (this.elementForContextMenu is Button)
                    {
                        Button parent = (Button)this.elementForContextMenu;
                        if (parent != null)
                        {
                            dragCanvas.Children.Remove(parent);
                            if (graphicsCount >= 1)
                            {
                                graphicsCount--;
                            }
                            if (graphicsCount == 0)
                            {
                                VisualStateManager.GoToState(btngraphics, "Unchecked", true);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
#if DEBUG
            if (watch != null)
                FrameworkHelper.CommonUtility.WatchStop(this.GetType().FullName + "." + System.Reflection.MethodBase.GetCurrentMethod().Name + "|", watch);
#endif

        }

        /// <summary>
        /// The draw executable
        /// </summary>
        private int drawX = 0;
        /// <summary>
        /// The draw asynchronous
        /// </summary>
        private int drawY = 0;

        #region MainImage Mouse Events
        /// <summary>
        /// Handles the MouseDown event of the mainImage control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="MouseButtonEventArgs"/> instance containing the event data.</param>
        private void mainImage_MouseDown(object sender, MouseButtonEventArgs e)
        {
            try
            {
                if (e.LeftButton == MouseButtonState.Released)
                {
                    IsSelectedMainImage = true;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }

        /// <summary>
        /// Handles the TouchDown event of the mainImage control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="TouchEventArgs"/> instance containing the event data.</param>
        private void mainImage_TouchDown(object sender, TouchEventArgs e)
        {
            IsSelectedMainImage = true;
        }
        UIElement shapeToRemove;
        /// <summary>
        /// Handles the MouseLeftButtonDown event of the mainImage control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="MouseButtonEventArgs"/> instance containing the event data.</param>
        private void mainImage_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (shapeToRemove != null && MyInkCanvas.Children.Contains(shapeToRemove))
            {
                shapeToRemove.Opacity = 1;

                if (shapeToRemove is Rectangle)
                {
                    Rectangle rect = new Rectangle();
                    System.Windows.Media.Color clr = Colors.LimeGreen; //(System.Windows.Media.Color)System.Windows.Media.ColorConverter.ConvertFromString(primaryColor);
                    rect.Fill = new SolidColorBrush(clr);
                    rect.Width = ((Rectangle)shapeToRemove).Width;
                    rect.Height = ((Rectangle)shapeToRemove).Height;
                    rect.SetValue(InkCanvas.LeftProperty, ((Rectangle)shapeToRemove).GetValue(InkCanvas.LeftProperty));
                    rect.SetValue(InkCanvas.TopProperty, ((Rectangle)shapeToRemove).GetValue(InkCanvas.TopProperty));
                    MyInkCanvas.Children.Add(rect);
                }
                else
                {
                    Ellipse elps = new Ellipse();
                    System.Windows.Media.Color clr = (System.Windows.Media.Color)System.Windows.Media.ColorConverter.ConvertFromString(primaryColor);
                    elps.Fill = new SolidColorBrush(clr);
                    elps.Width = ((Ellipse)shapeToRemove).Width;
                    elps.Height = ((Ellipse)shapeToRemove).Height;
                    elps.SetValue(InkCanvas.LeftProperty, ((Ellipse)shapeToRemove).GetValue(InkCanvas.LeftProperty));
                    elps.SetValue(InkCanvas.TopProperty, ((Ellipse)shapeToRemove).GetValue(InkCanvas.TopProperty));
                    MyInkCanvas.Children.Add(elps);
                }

                // MyInkCanvas.Children.Add(shapeToRemove);

            }

            //ptnred = e.GetPosition(mainImage);
            //if (move)
            //{
            //    move = false;
            //}
            //else
            //{
            //    move = true;
            //}

            if (IsEraserActive)
            {

            }
            if (IsEraserDrawRectangleActive)
            {
                //e.MouseDevice.Capture(this);
                drawX = (int)e.GetPosition(mainImage).X;
                drawY = (int)e.GetPosition(mainImage).Y;
            }
            else if (IsEraserDrawEllipseActive)
            {
                //e.MouseDevice.Capture(this);
                drawX = (int)e.GetPosition(mainImage).X;
                drawY = (int)e.GetPosition(mainImage).Y;
            }
        }

        /// <summary>
        /// Handles the MouseLeftButtonUp event of the mainImage control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="MouseButtonEventArgs"/> instance containing the event data.</param>
        /// 
        private void mainImage_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
#if DEBUG
            var watch = FrameworkHelper.CommonUtility.Watch();
            watch.Start();
#endif
            try
            {
                if (e.LeftButton == MouseButtonState.Released)
                {
                    IsSelectedMainImage = true;
                    this.elementForContextMenu = null;
                    RotateTransform rotation = GrdBrightness.GetValue(Canvas.RenderTransformProperty) as RotateTransform;
                    if (rotation != null)
                    {
                        jrotate.Angle = rotation.Angle;
                    }
                    else
                    {
                        jrotate.Angle = 0;
                    }
                }
                mainImage.ReleaseMouseCapture();

                if (MyInkCanvas.Children.Contains(shapeToRemove) && shapeToRemove.Opacity != 1)
                {
                    MyInkCanvas.Children.Remove(shapeToRemove);
                }
                else
                {
                    shapeToRemove = null;
                }

                int _x = (int)e.MouseDevice.GetPosition(mainImage).X;
                int _y = (int)e.MouseDevice.GetPosition(mainImage).Y;

                // Translate pixel coordinates to WriteableBitmap coordinates
                double wratio = forWdht.Width / mainImage.ActualWidth;
                double hratio = forWdht.Height / mainImage.ActualHeight;

                drawX = (int)(drawX * wratio);
                _x = (int)(_x * wratio);
                drawY = (int)(drawY * hratio);
                _y = (int)(_y * hratio);

                if (IsEraserDrawRectangleActive)
                {
                    if ((e.MouseDevice.LeftButton == MouseButtonState.Released || e.MouseDevice.RightButton == MouseButtonState.Released))
                    {
                        mainImageundo.Visibility = Visibility.Hidden;
                        Rectangle rect = new Rectangle();
                        //rect.RadiusX = 15;
                        //rect.RadiusY = 15;
                        rect.Width = Math.Abs(_x - drawX);
                        rect.Height = Math.Abs(_y - drawY);
                        System.Windows.Media.Color clr = Colors.LimeGreen; //(System.Windows.Media.Color)System.Windows.Media.ColorConverter.ConvertFromString(primaryColor);

                        rect.MouseLeftButtonDown += new MouseButtonEventHandler(mainImage_MouseLeftButtonDown);
                        rect.MouseLeftButtonUp += new MouseButtonEventHandler(mainImage_MouseLeftButtonUp);

                        rect.Fill = new SolidColorBrush(clr);

                        StrokeCollection strokeCollection = new StrokeCollection();
                        StylusPointCollection stylusPointCollection = new StylusPointCollection();
                        DrawingAttributes drawingAttributes = new DrawingAttributes();
                        drawingAttributes.Color = clr;
                        drawingAttributes.StylusTip = StylusTip.Rectangle;
                        drawingAttributes.FitToCurve = true;
                        drawingAttributes.Height = rect.Height;
                        drawingAttributes.Width = rect.Width;

                        rect.SnapsToDevicePixels = true;
                        MyInkCanvas.SnapsToDevicePixels = true;
                        rect.SetValue(RenderOptions.EdgeModeProperty, EdgeMode.Aliased);
                        MyInkCanvas.SetValue(RenderOptions.EdgeModeProperty, EdgeMode.Aliased);

                        double startX = 0;
                        double startY = 0;

                        if (_x > drawX)
                        {
                            rect.SetValue(InkCanvas.LeftProperty, (double)drawX);
                            startX = drawX;
                        }
                        else
                        {
                            rect.SetValue(InkCanvas.LeftProperty, (double)_x);
                            startX = _x;
                        }
                        if (_y > drawY)
                        {
                            rect.SetValue(InkCanvas.TopProperty, (double)drawY);
                            startY = drawY;
                        }
                        else
                        {
                            rect.SetValue(InkCanvas.TopProperty, (double)_y);
                            startY = _y;
                        }

                        //MyInkCanvas.Children.Add(rect);
                        stylusPointCollection.Add(new StylusPoint(startX + rect.Width / 2, startY + rect.Height / 2));

                        strokeCollection.Add(new Stroke(stylusPointCollection, drawingAttributes));
                        MyInkCanvas.Strokes.Add(new StrokeCollection(strokeCollection));



                    }
                }
                else if (IsEraserDrawEllipseActive)
                {
                    if ((e.MouseDevice.LeftButton == MouseButtonState.Released || e.MouseDevice.RightButton == MouseButtonState.Released))
                    {
                        Ellipse ellipseToInsert = new Ellipse();

                        //rect.RadiusX = 50;
                        //rect.RadiusY = 50;

                        ellipseToInsert.Width = Math.Abs(_x - drawX);

                        ellipseToInsert.Height = Math.Abs(_y - drawY);

                        ellipseToInsert.MouseLeftButtonDown += new MouseButtonEventHandler(mainImage_MouseLeftButtonDown);

                        ellipseToInsert.MouseLeftButtonUp += new MouseButtonEventHandler(mainImage_MouseLeftButtonUp);

                        System.Windows.Media.Color clr = (System.Windows.Media.Color)System.Windows.Media.ColorConverter.ConvertFromString(primaryColor);

                        ellipseToInsert.Fill = new SolidColorBrush(clr);

                        StrokeCollection strokeCollection = new StrokeCollection();
                        StylusPointCollection stylusPointCollection = new StylusPointCollection();
                        DrawingAttributes drawingAttributes = new DrawingAttributes();
                        drawingAttributes.Color = clr;
                        drawingAttributes.StylusTip = StylusTip.Ellipse;
                        drawingAttributes.FitToCurve = true;
                        drawingAttributes.Height = ellipseToInsert.Height;
                        drawingAttributes.Width = ellipseToInsert.Width;

                        ellipseToInsert.SnapsToDevicePixels = true;
                        MyInkCanvas.SnapsToDevicePixels = true;
                        ellipseToInsert.SetValue(RenderOptions.EdgeModeProperty, EdgeMode.Aliased);
                        MyInkCanvas.SetValue(RenderOptions.EdgeModeProperty, EdgeMode.Aliased);

                        double startX = 0;
                        double startY = 0;

                        if (_x > drawX)
                        {
                            ellipseToInsert.SetValue(InkCanvas.LeftProperty, (double)drawX);
                            startX = drawX;
                        }
                        else
                        {
                            ellipseToInsert.SetValue(InkCanvas.LeftProperty, (double)_x);
                            startX = _x;
                        }
                        if (_y > drawY)
                        {
                            ellipseToInsert.SetValue(InkCanvas.TopProperty, (double)drawY);
                            startY = drawY;
                        }
                        else
                        {
                            ellipseToInsert.SetValue(InkCanvas.TopProperty, (double)_y);
                            startY = _y;
                        }

                        //System.Windows.Int32Rect rcFrom = new System.Windows.Int32Rect(drawX, drawY, _x, _y);
                        //MyInkCanvas.Children.Add(ellipseToInsert);

                        //e.MouseDevice.Capture(null);

                        stylusPointCollection.Add(new StylusPoint(startX + ellipseToInsert.Width / 2, startY + ellipseToInsert.Height / 2));



                        strokeCollection.Add(new Stroke(stylusPointCollection, drawingAttributes));
                        MyInkCanvas.Strokes.Add(new StrokeCollection(strokeCollection));
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }

#if DEBUG
            if (watch != null)
                FrameworkHelper.CommonUtility.WatchStop(this.GetType().FullName + "." + System.Reflection.MethodBase.GetCurrentMethod().Name + "|", watch);
#endif
        }

        private void mainImage_MouseLeftButtonUp1(object sender, MouseButtonEventArgs e)
        {
#if DEBUG
            var watch = FrameworkHelper.CommonUtility.Watch();
            watch.Start();
#endif

            try
            {
                if (e.LeftButton == MouseButtonState.Released)
                {
                    IsSelectedMainImage = true;
                    this.elementForContextMenu = null;
                    RotateTransform rotation = GrdBrightness.GetValue(Canvas.RenderTransformProperty) as RotateTransform;
                    if (rotation != null)
                    {
                        jrotate.Angle = rotation.Angle;
                    }
                    else
                    {
                        jrotate.Angle = 0;
                    }
                }
                mainImage.ReleaseMouseCapture();
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
#if DEBUG
            if (watch != null)
                FrameworkHelper.CommonUtility.WatchStop(this.GetType().FullName + "." + System.Reflection.MethodBase.GetCurrentMethod().Name + "|", watch);
#endif
        }

        /// <summary>
        /// Handles the 1 event of the mainImage_MouseMove control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="MouseEventArgs"/> instance containing the event data.</param>
        private void mainImage_MouseMove_1(object sender, MouseEventArgs e)
        {
            try
            {
                if (IsEraserActive)
                {
                    var pos = e.GetPosition(mainImage);
                    double left = pos.X;
                    double top = pos.Y;
                    greenEraser.Margin = new Thickness(left + 1, top + 1, 0, 0);
                }

                int _x1 = (int)e.MouseDevice.GetPosition(mainImage).X;
                int _y1 = (int)e.MouseDevice.GetPosition(mainImage).Y;
                if (IsEraserDrawRectangleActive)
                {
                    if ((e.MouseDevice.LeftButton == MouseButtonState.Pressed))
                    {
                        Rectangle rect = new Rectangle();
                        if (shapeToRemove != null)
                            rect = shapeToRemove as Rectangle;
                        if (rect == null)
                            rect = new Rectangle();
                        rect.Opacity = .3;
                        rect.Width = Math.Abs(_x1 - drawX);
                        rect.Height = Math.Abs(_y1 - drawY);

                        System.Windows.Media.Color clr = (System.Windows.Media.Color)System.Windows.Media.ColorConverter.ConvertFromString(primaryColor);

                        rect.Fill = new SolidColorBrush(clr);

                        if (_x1 > drawX)
                        {
                            rect.SetValue(InkCanvas.LeftProperty, (double)drawX);
                        }
                        else
                        {
                            rect.SetValue(InkCanvas.LeftProperty, (double)_x1);
                        }
                        if (_y1 > drawY)
                        {
                            rect.SetValue(InkCanvas.TopProperty, (double)drawY);
                        }
                        else
                        {
                            rect.SetValue(InkCanvas.TopProperty, (double)_y1);
                        }
                        shapeToRemove = rect;
                        if (!MyInkCanvas.Children.Contains(shapeToRemove))
                            MyInkCanvas.Children.Add(shapeToRemove);
                    }
                }
                else if (IsEraserDrawEllipseActive)
                {
                    if ((e.MouseDevice.LeftButton == MouseButtonState.Pressed))
                    {
                        Ellipse ellipseToInsert = new Ellipse();
                        if (shapeToRemove != null)
                            ellipseToInsert = shapeToRemove as Ellipse;
                        if (ellipseToInsert == null)
                            ellipseToInsert = new Ellipse();
                        ellipseToInsert.Opacity = .3;
                        ellipseToInsert.Width = Math.Abs(_x1 - drawX);

                        ellipseToInsert.Height = Math.Abs(_y1 - drawY);

                        System.Windows.Media.Color clr = (System.Windows.Media.Color)System.Windows.Media.ColorConverter.ConvertFromString(primaryColor);

                        ellipseToInsert.Fill = new SolidColorBrush(clr);

                        if (_x1 > drawX)
                        {
                            ellipseToInsert.SetValue(InkCanvas.LeftProperty, (double)drawX);
                        }
                        else
                        {
                            ellipseToInsert.SetValue(InkCanvas.LeftProperty, (double)_x1);
                        }
                        if (_y1 > drawY)
                        {
                            ellipseToInsert.SetValue(InkCanvas.TopProperty, (double)drawY);
                        }
                        else
                        {
                            ellipseToInsert.SetValue(InkCanvas.TopProperty, (double)_y1);
                        }

                        shapeToRemove = ellipseToInsert;
                        if (!MyInkCanvas.Children.Contains(shapeToRemove))
                            MyInkCanvas.Children.Add(shapeToRemove);
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
            finally
            {
                //MemoryManagement.FlushMemory();
            }
        }

        #endregion

        #region Zoom Code
        /// <summary>
        /// The transform group
        /// </summary>
        private TransformGroup transformGroup;
        /// <summary>
        /// The translate transform
        /// </summary>
        private TranslateTransform translateTransform;
        /// <summary>
        /// The zoom transform
        /// </summary>
        private ScaleTransform zoomTransform;
        /// <summary>
        /// The rotate transform
        /// </summary>
        private RotateTransform rotateTransform;
        /// <summary>
        /// The _ zoom factor
        /// </summary>
        private double _ZoomFactor = 1;
        /// <summary>
        /// The _ zoom factor green
        /// </summary>
        private double _ZoomFactorGreen = 1;
        /// <summary>
        /// The _max zoom factor
        /// </summary>
        private double _maxZoomFactor = 4;
        /// <summary>
        /// The _ graphics zoom factor
        /// </summary>
        private double _GraphicsZoomFactor = 1;
        /// <summary>
        /// Handles the Click event of the ZoomInButton control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void ZoomInButton_Click(object sender, RoutedEventArgs e)
        {
#if DEBUG
            var watch = FrameworkHelper.CommonUtility.Watch();
            watch.Start();
#endif
            IsImageDirtyState = true;
            //strSender = "Button";
            RenderOptions.SetEdgeMode(mainImage, EdgeMode.Aliased);
            try
            {
                CmbProductType.Visibility = Visibility.Collapsed;
                if (this.elementForContextMenu == null)
                {
                    if (_ZoomFactor >= _maxZoomFactor)
                    {
                        _ZoomFactor = 4;
                        IsGraphicsChange = true;
                        return;
                    }

                    _ZoomFactor += .025;


                    if (zoomTransform != null)
                    {
                        if (FlipMode != 0 || FlipModeY != 0)
                        {
                            zoomTransform.CenterX = mainImage.ActualWidth / 2;
                            zoomTransform.CenterY = mainImage.ActualHeight / 2;

                            zoomTransform.ScaleX = _ZoomFactor;
                            zoomTransform.ScaleY = _ZoomFactor;
                            transformGroup = new TransformGroup();
                            transformGroup.Children.Add(zoomTransform);
                            transformGroup.Children.Add(translateTransform);
                            transformGroup.Children.Add(rotateTransform);
                            MyInkCanvas.DefaultDrawingAttributes.StylusTipTransform = new Matrix(_ZoomFactor, 0, 0, _ZoomFactor, 0, 0);
                            MyInkCanvas.RenderTransform = null;
                            canbackground.RenderTransform = GrdGreenScreenDefault3.RenderTransform = transformGroup;
                            IsGraphicsChange = true;
                        }
                        else
                        {
                            zoomTransform.CenterX = mainImage.ActualWidth / 2;
                            zoomTransform.CenterY = mainImage.ActualHeight / 2;

                            zoomTransform.ScaleX = _ZoomFactor;
                            zoomTransform.ScaleY = _ZoomFactor;

                            transformGroup = new TransformGroup();
                            transformGroup.Children.Add(zoomTransform);
                            transformGroup.Children.Add(translateTransform);
                            transformGroup.Children.Add(rotateTransform);
                            MyInkCanvas.RenderTransform = null;
                            canbackground.RenderTransform = GrdGreenScreenDefault3.RenderTransform = transformGroup;
                            IsGraphicsChange = true;
                        }
                        lblzoomplus.Content = _ZoomFactor * 100 + " % ";
                    }
                }
                else if (this.elementForContextMenu is Button)
                {

                    Button b = (Button)this.elementForContextMenu;
                    if (b.Tag != null)
                    {
                        _GraphicsZoomFactor = Convert.ToDouble(b.Tag.ToString());
                        _GraphicsZoomFactor += .025;
                    }
                    else
                    {
                        _GraphicsZoomFactor = 1;
                    }
                    TransformGroup grp = new TransformGroup();
                    TransformGroup tg = this.elementForContextMenu.GetValue(Canvas.RenderTransformProperty) as TransformGroup;
                    RotateTransform rotation = new RotateTransform();
                    ScaleTransform scale = new ScaleTransform();

                    if (tg != null)
                    {
                        if (tg.Children.Count > 0)
                        {
                            if (tg.Children[0] is ScaleTransform)
                            {
                                scale = (ScaleTransform)tg.Children[0];
                            }
                            else if (tg.Children[0] is RotateTransform)
                            {
                                rotation = (RotateTransform)tg.Children[0];

                            }
                        }
                        if (tg.Children.Count > 1)
                        {
                            if (tg.Children[1] is ScaleTransform)
                            {
                                scale = (ScaleTransform)tg.Children[1];
                            }
                            else if (tg.Children[1] is RotateTransform)
                            {
                                rotation = (RotateTransform)tg.Children[1];

                            }
                        }
                    }


                    if (scale == null)
                    {
                        scale = new ScaleTransform();
                        scale.ScaleX = _GraphicsZoomFactor;
                        scale.ScaleY = _GraphicsZoomFactor;
                        scale.CenterX = 0;
                        scale.CenterY = 0;
                    }
                    else
                    {
                        scale.ScaleX = _GraphicsZoomFactor;
                        scale.ScaleY = _GraphicsZoomFactor;
                        scale.CenterX = 0;
                        scale.CenterY = 0;
                    }

                    grp.Children.Add(scale);
                    if (rotation != null)
                    {
                        grp.Children.Add(rotation);


                    }
                    b.Tag = _GraphicsZoomFactor.ToString();
                    b.RenderTransform = grp;
                    this.elementForContextMenu = b;
                }
                IsGraphicsChange = true;



                if (_ZoomFactor >= 1)
                {
                    attribute.Width = attributeWidth / _ZoomFactor;
                    attribute.Height = attributeHeight / _ZoomFactor;
                }
                else
                {
                    attribute.Width = attributeWidth * _ZoomFactor;
                    attribute.Height = attributeHeight * _ZoomFactor;
                }


            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                MessageBox.Show(ex.Message);
            }
            finally
            {
                MemoryManagement.FlushMemory();
            }

            IsGreenCorrection = false;

#if DEBUG
            if (watch != null)
                FrameworkHelper.CommonUtility.WatchStop(this.GetType().FullName + "." + System.Reflection.MethodBase.GetCurrentMethod().Name + "|", watch);
#endif
        }
        /// <summary>
        /// Handles the Click event of the ZoomOutButton control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void ZoomOutButton_Click(object sender, RoutedEventArgs e)
        {
#if DEBUG
            var watch = FrameworkHelper.CommonUtility.Watch();
            watch.Start();
#endif
            IsImageDirtyState = true;
            //strSender = "Button";
            CmbProductType.Visibility = Visibility.Collapsed;
            RenderOptions.SetEdgeMode(mainImage, EdgeMode.Aliased);
            try
            {
                if (this.elementForContextMenu == null)
                {
                    if (_ZoomFactor >= .525)
                        _ZoomFactor -= .025;
                    else
                    {
                        _ZoomFactor = .5;
                        return;
                    }

                    if (zoomTransform != null && _ZoomFactor >= .5)
                    {
                        if (FlipMode != 0 || FlipModeY != 0)
                        {
                            zoomTransform.CenterX = mainImage.ActualWidth / 2;
                            zoomTransform.CenterY = mainImage.ActualHeight / 2;

                            zoomTransform.ScaleX = _ZoomFactor;
                            zoomTransform.ScaleY = _ZoomFactor;
                            transformGroup = new TransformGroup();
                            transformGroup.Children.Add(zoomTransform);
                            transformGroup.Children.Add(translateTransform);
                            transformGroup.Children.Add(rotateTransform);

                            MyInkCanvas.RenderTransform = null;
                            canbackground.RenderTransform = GrdGreenScreenDefault3.RenderTransform = transformGroup;

                            IsGraphicsChange = true;
                        }
                        else
                        {
                            zoomTransform.CenterX = mainImage.ActualWidth / 2;
                            zoomTransform.CenterY = mainImage.ActualHeight / 2;

                            zoomTransform.ScaleX = _ZoomFactor;
                            zoomTransform.ScaleY = _ZoomFactor;
                            transformGroup = new TransformGroup();
                            transformGroup.Children.Add(zoomTransform);
                            transformGroup.Children.Add(translateTransform);
                            transformGroup.Children.Add(rotateTransform);

                            MyInkCanvas.RenderTransform = null;
                            canbackground.RenderTransform = GrdGreenScreenDefault3.RenderTransform = transformGroup;
                            IsGraphicsChange = true;
                        }
                        lblzoomplus.Content = _ZoomFactor * 100 + " % ";
                    }
                }
                else if (this.elementForContextMenu is Button)
                {

                    Button b = (Button)this.elementForContextMenu;
                    if (b.Tag != null)
                    {
                        _GraphicsZoomFactor = Convert.ToDouble(b.Tag.ToString());

                    }

                    if (_GraphicsZoomFactor >= .625)
                        _GraphicsZoomFactor -= .025;
                    else
                    {
                        //  _GraphicsZoomFactor = 1;
                        return;
                    }


                    TransformGroup grp = new TransformGroup();
                    TransformGroup tg = this.elementForContextMenu.GetValue(Canvas.RenderTransformProperty) as TransformGroup;
                    RotateTransform rotation = new RotateTransform();
                    ScaleTransform scale = new ScaleTransform();

                    if (tg != null)
                    {
                        if (tg.Children.Count > 0)
                        {
                            if (tg.Children[0] is ScaleTransform)
                            {
                                scale = (ScaleTransform)tg.Children[0];
                            }
                            else if (tg.Children[0] is RotateTransform)
                            {
                                rotation = (RotateTransform)tg.Children[0];

                            }
                        }
                        if (tg.Children.Count > 1)
                        {
                            if (tg.Children[1] is ScaleTransform)
                            {
                                scale = (ScaleTransform)tg.Children[1];
                            }
                            else if (tg.Children[1] is RotateTransform)
                            {
                                rotation = (RotateTransform)tg.Children[1];

                            }
                        }
                    }


                    if (scale == null)
                    {
                        return;
                    }
                    else
                    {
                        scale.ScaleX = scale.ScaleX - 0.025;
                        scale.ScaleY = scale.ScaleY - 0.025;
                        scale.CenterX = 0;
                        scale.CenterY = 0;

                        grp.Children.Add(scale);
                        if (rotation != null)
                        {
                            grp.Children.Add(rotation);
                        }

                        b.Tag = _GraphicsZoomFactor;
                        b.RenderTransform = grp;
                        this.elementForContextMenu = b;


                        IsGraphicsChange = true;
                    }

                }

                if (_ZoomFactor >= 1)
                {
                    attribute.Width = attributeWidth / _ZoomFactor;
                    attribute.Height = attributeHeight / _ZoomFactor;
                }
                else
                {
                    attribute.Width = attributeWidth * _ZoomFactor;
                    attribute.Height = attributeHeight * _ZoomFactor;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
            finally
            {
                MemoryManagement.FlushMemory();
            }
            IsGreenCorrection = false;
#if DEBUG
            if (watch != null)
                FrameworkHelper.CommonUtility.WatchStop(this.GetType().FullName + "." + System.Reflection.MethodBase.GetCurrentMethod().Name + "|", watch);
#endif
        }
        /// <summary>
        /// Handles the Click1 event of the ZoomInButton control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        /// 



        private void ZoomInButton_Click1(object sender, RoutedEventArgs e)
        {
            IsImageDirtyState = true;
            ZoomInButtonClick1();
        }

        private void ZoomInButtonClick1()
        {
#if DEBUG
            var watch = FrameworkHelper.CommonUtility.Watch();
            watch.Start();
#endif

            IsZoomed = true;
            RenderOptions.SetEdgeMode(mainImage, EdgeMode.Aliased);
            try
            {
                if (this.elementForContextMenu == null)
                {
                    if (_ZoomFactor >= 2.5)
                    {
                        _ZoomFactor = 2.5;
                        IsGraphicsChange = true;
                        return;
                    }

                    _ZoomFactor += .025;


                    if (zoomTransform != null)
                    {
                        if (FlipMode != 0 || FlipModeY != 0)
                        {
                            zoomTransform.CenterX = mainImage.ActualWidth / 2;
                            zoomTransform.CenterY = mainImage.ActualHeight / 2;

                            zoomTransform.ScaleX = _ZoomFactor;
                            zoomTransform.ScaleY = _ZoomFactor;
                            transformGroup = new TransformGroup();
                            transformGroup.Children.Add(zoomTransform);
                            transformGroup.Children.Add(translateTransform);
                            transformGroup.Children.Add(rotateTransform);
                            IsGraphicsChange = true;
                        }
                        else
                        {
                            zoomTransform.CenterX = mainImage.ActualWidth / 2;
                            zoomTransform.CenterY = mainImage.ActualHeight / 2;

                            zoomTransform.ScaleX = _ZoomFactor;
                            zoomTransform.ScaleY = _ZoomFactor;

                            transformGroup = new TransformGroup();
                            transformGroup.Children.Add(zoomTransform);
                            transformGroup.Children.Add(translateTransform);
                            transformGroup.Children.Add(rotateTransform);

                            MyInkCanvas.RenderTransform = null;
                            canbackground.RenderTransform = GrdGreenScreenDefault3.RenderTransform = transformGroup;
                            IsGraphicsChange = true;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                MessageBox.Show(ex.Message);
            }
            finally
            {
                //MemoryManagement.FlushMemory();
            }
            IsGreenCorrection = false;
#if DEBUG
            if (watch != null)
                FrameworkHelper.CommonUtility.WatchStop(this.GetType().FullName + "." + System.Reflection.MethodBase.GetCurrentMethod().Name + "|", watch);
#endif
        }
        /// <summary>
        /// Handles the Click1 event of the ZoomOutButton control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void ZoomOutButton_Click1(object sender, RoutedEventArgs e)
        {
#if DEBUG
            var watch = FrameworkHelper.CommonUtility.Watch();
            watch.Start();
#endif

            IsZoomed = true;
            RenderOptions.SetEdgeMode(mainImage, EdgeMode.Aliased);
            try
            {
                CmbProductType.Visibility = Visibility.Collapsed;
                if (this.elementForContextMenu == null)
                {
                    if (_ZoomFactor >= .325)
                        _ZoomFactor -= .025;
                    else
                    {
                        _ZoomFactor = .3;
                        return;
                    }

                    if (zoomTransform != null && _ZoomFactor >= .3)
                    {
                        if (FlipMode != 0 || FlipModeY != 0)
                        {
                            zoomTransform.CenterX = mainImage.ActualWidth / 2;
                            zoomTransform.CenterY = mainImage.ActualHeight / 2;

                            zoomTransform.ScaleX = _ZoomFactor;
                            zoomTransform.ScaleY = _ZoomFactor;
                            transformGroup = new TransformGroup();
                            transformGroup.Children.Add(zoomTransform);
                            transformGroup.Children.Add(translateTransform);
                            transformGroup.Children.Add(rotateTransform);
                            MyInkCanvas.RenderTransform = null;
                            canbackground.RenderTransform = GrdGreenScreenDefault3.RenderTransform = transformGroup;

                            IsGraphicsChange = true;
                        }
                        else
                        {
                            zoomTransform.CenterX = mainImage.ActualWidth / 2;
                            zoomTransform.CenterY = mainImage.ActualHeight / 2;

                            zoomTransform.ScaleX = _ZoomFactor;
                            zoomTransform.ScaleY = _ZoomFactor;
                            transformGroup = new TransformGroup();

                            transformGroup.Children.Add(zoomTransform);
                            transformGroup.Children.Add(translateTransform);
                            transformGroup.Children.Add(rotateTransform);
                            MyInkCanvas.RenderTransform = null;
                            canbackground.RenderTransform = GrdGreenScreenDefault3.RenderTransform = transformGroup;
                            IsGraphicsChange = true;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
            finally
            {
                // MemoryManagement.FlushMemory();
            }
            IsGreenCorrection = false;
#if DEBUG
            if (watch != null)
                FrameworkHelper.CommonUtility.WatchStop(this.GetType().FullName + "." + System.Reflection.MethodBase.GetCurrentMethod().Name + "|", watch);
#endif
        }
        /// <summary>
        /// Handles the Click event of the ZoomInButtonGreenScreen control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void ZoomInButtonGreenScreen_Click(object sender, RoutedEventArgs e)
        {
#if DEBUG
            var watch = FrameworkHelper.CommonUtility.Watch();
            watch.Start();
#endif

            RenderOptions.SetEdgeMode(mainImage, EdgeMode.Aliased);
            try
            {
                if (this.elementForContextMenu == null)
                {
                    if (_ZoomFactorGreen >= 1)
                    {
                        //_ZoomFactorGreen = 1;
                        IsGraphicsChange = true;
                        //  return;
                    }

                    _ZoomFactorGreen += .025;

                    if (zoomTransform != null)
                    {
                        zoomTransform.CenterX = mainImage.ActualWidth / 2;
                        zoomTransform.CenterY = mainImage.ActualHeight / 2;
                        zoomTransform.ScaleX = _ZoomFactorGreen;
                        zoomTransform.ScaleY = _ZoomFactorGreen;

                        transformGroup = new TransformGroup();
                        transformGroup.Children.Add(zoomTransform);
                        transformGroup.Children.Add(translateTransform);
                        transformGroup.Children.Add(rotateTransform);
                        canbackground.RenderTransform = MyInkCanvas.RenderTransform = null;
                        GrdGreenScreenDefault3.RenderTransform = transformGroup;
                        IsGraphicsChange = true;
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                MessageBox.Show(ex.Message);
            }
            finally
            {
                MemoryManagement.FlushMemory();
            }
#if DEBUG
            if (watch != null)
                FrameworkHelper.CommonUtility.WatchStop(this.GetType().FullName + "." + System.Reflection.MethodBase.GetCurrentMethod().Name + "|", watch);
#endif
        }
        /// <summary>
        /// Handles the Click event of the ZoomOutButtonGreenScreen control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void ZoomOutButtonGreenScreen_Click(object sender, RoutedEventArgs e)
        {
#if DEBUG
            var watch = FrameworkHelper.CommonUtility.Watch();
            watch.Start();
#endif

            RenderOptions.SetEdgeMode(mainImage, EdgeMode.Aliased);
            try
            {

                if (this.elementForContextMenu == null)
                {
                    if (_ZoomFactorGreen >= .15)
                        _ZoomFactorGreen -= .025;
                    else
                    {
                        _ZoomFactorGreen = .15;
                        return;
                    }

                    _ZoomFactorGreen = 1;

                    if (zoomTransform != null && _ZoomFactorGreen >= .15)
                    {
                        zoomTransform.CenterX = mainImage.ActualWidth / 2;
                        zoomTransform.CenterY = mainImage.ActualHeight / 2;

                        zoomTransform.ScaleX = _ZoomFactorGreen;
                        zoomTransform.ScaleY = _ZoomFactorGreen;
                        transformGroup = new TransformGroup();
                        transformGroup.Children.Add(zoomTransform);
                        transformGroup.Children.Add(translateTransform);
                        transformGroup.Children.Add(rotateTransform);
                        canbackground.RenderTransform = MyInkCanvas.RenderTransform = null;
                        GrdGreenScreenDefault3.RenderTransform = transformGroup;
                        IsGraphicsChange = true;
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
            IsGreenCorrection = false;
#if DEBUG
            if (watch != null)
                FrameworkHelper.CommonUtility.WatchStop(this.GetType().FullName + "." + System.Reflection.MethodBase.GetCurrentMethod().Name + "|", watch);
#endif
        }
        /// <summary>
        /// Handles the MouseLeftButtonUp1 event of the mainImage control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="MouseButtonEventArgs"/> instance containing the event data.</param>


        #endregion

        /// <summary>
        /// Loads from database.
        /// </summary>
        private void LoadFromDB()
        {
#if DEBUG
            var watch = FrameworkHelper.CommonUtility.Watch();
            watch.Start();
#endif

            try
            {
                //if (_objDbLayer == null)
                //    _objDbLayer = new DigiPhotoDataServices();
                VisualStateManager.GoToState(btnAddgraphics, "Unchecked", true);
                graphicsTextBoxCount = 0;
                gumballTextCount = 0;
                graphicsBorderApplied = false;
                graphicsCount = 0;
                graphicsframeApplied = false;
                VisualStateManager.GoToState(btngraphics, "Unchecked", true);
                VisualStateManager.GoToState(btnGraphicsText, "Unchecked", true);
                VisualStateManager.GoToState(btnBorder, "Unchecked", true);
                //VisualStateManager.GoToState(btnBackground, "Unchecked", true);

                if (GraphicEffect != null && !GraphicEffect.Equals("test", StringComparison.CurrentCultureIgnoreCase))
                {
                    ReloadAllGraphicsEffect();
                    LoadXaml(GraphicEffect);
                    //Bikram
                    if (!GraphicEffect.Contains("border"))
                        borderName = null;
                    if (dragCanvas.Children.Count > 4 || (_ZoomFactor >= .5 && _ZoomFactor != 1) || frm.Children.Count >= 1 || canbackground.Children.Count > 1 || jrotate.Angle != 0)
                    {
                        DisableButtonForLayering();
                    }
                }
                else
                {
                    PhotoBusiness phBiz = new PhotoBusiness();
                    if (!phBiz.GetModeratePhotos(PhotoId))
                    {
                        EnableButtonForLayering();
                    }
                    ReloadAllGraphicsEffect();

                    lblzoomplus.Content = _ZoomFactor * 100 + " % ";
                }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
            finally
            {
                // MemoryManagement.FlushMemory();
            }
#if DEBUG
            if (watch != null)
                FrameworkHelper.CommonUtility.WatchStop(this.GetType().FullName + "." + System.Reflection.MethodBase.GetCurrentMethod().Name + "|", watch);
#endif
        }

        /// <summary>
        /// Saveintoes the database.
        /// </summary>
        /// <param name="input">The input.</param>
        private void SaveintoDB(string input)
        {
            try
            {
                //if (_objDbLayer == null)
                //    _objDbLayer = new DigiPhotoDataServices();
                PhotoBusiness phBiz = new PhotoBusiness();
                //ToDo: Save these parameters in list and not in database
                //phBiz.UpdateLayering(PhotoId, input);
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }

        }

        /// <summary>
        /// The _center executable
        /// </summary>
        string _centerX = "";
        /// <summary>
        /// The _center asynchronous
        /// </summary>
        string _centerY = "";
        /// <summary>
        /// Loads the xaml.
        /// </summary>
        /// <param name="inputXml">The input XML.</param>
        /// 

        double ChromaGridLeft = 0;
        double ChromaGridTop = 0;
        double ChromaCenterX = 0;
        double ChromaCenterY = 0;
        double ChromaZoomFactor = 0;
        string ChromaBorderPath = string.Empty;
        string OriginalBorder = String.Empty;
        private void LoadXaml(string inputXml)
        {
#if DEBUG
            var watch = FrameworkHelper.CommonUtility.Watch();
            watch.Start();
#endif
            if (string.IsNullOrEmpty(inputXml))
                return;
            //if (_objDbLayer == null)
            //    _objDbLayer = new DigiPhotoDataServices();
            System.Xml.XmlDocument Xdocument = new System.Xml.XmlDocument();
            Xdocument.LoadXml(inputXml);

            double translateX = 0;
            double translateY = 0;
            transformGroup = new TransformGroup();
            rotateTransform = new RotateTransform();
            try
            {
                BackgroundDBValue = ((System.Xml.XmlAttribute)((Xdocument.ChildNodes[0]).Attributes["bg"])).Value.ToLower();
                string borderValue = ((System.Xml.XmlAttribute)((Xdocument.ChildNodes[0]).Attributes["border"])).Value.ToLower();
                foreach (var xn in (Xdocument.ChildNodes[0]).Attributes)
                {
                    switch (((System.Xml.XmlAttribute)xn).Name.ToLower())
                    {
                        case "producttype":
                            if (((System.Xml.XmlAttribute)xn).Value.ToString() != string.Empty)
                                specproductType = ((System.Xml.XmlAttribute)xn).Value;
                            break;
                        case "border":
                            #region
                            if (((System.Xml.XmlAttribute)xn).Value.ToString() != string.Empty)
                            {
                                if (!string.IsNullOrEmpty(BackgroundDBValue))
                                {
                                    BitmapImage objBGCurrent = new BitmapImage(new Uri(Common.LoginUser.DigiFolderBackGroundPath + "\\8x10" + @"\" + BackgroundDBValue));

                                    if (canbackgroundold.Background != null)
                                        canbackgroundold.Background.Opacity = 0;
                                    canbackground.Background = new ImageBrush { ImageSource = objBGCurrent };

                                    LoadBackgroundInGrid(objBGCurrent);
                                    objBGCurrent.Freeze();
                                }
                                ChromaBorderPath = Common.LoginUser.DigiFolderFramePath + @"\" + ((System.Xml.XmlAttribute)xn).Value.ToString();
                                OriginalBorder = Common.LoginUser.DigiFolderFramePath + @"\" + ((System.Xml.XmlAttribute)xn).Value.ToString();
                                BitmapImage img = new BitmapImage(new Uri(Common.LoginUser.DigiFolderFramePath + @"\" + ((System.Xml.XmlAttribute)xn).Value.ToString()));
                                selectedborder = System.IO.Path.GetFileName(img.ToString());
                                OpaqueClickableImage objCurrent = new OpaqueClickableImage();
                                objCurrent.Uid = "frame";
                                objCurrent.Source = img;
                                objCurrent.IsHitTestVisible = false;
                                objCurrent.Stretch = Stretch.Fill; //temprorary Fix till Crop works properly
                                objCurrent.Loaded += new RoutedEventHandler(objCurrent_Loaded);
                                objCurrent.Opacity = 1;
                                borderName = ((System.Xml.XmlAttribute)xn).Value.ToString();

                                //double ratio = 1;
                                //if (img.Height > img.Width)
                                //    ratio = (double)(img.Width / img.Height);
                                //else
                                //    ratio = (double)(img.Height / img.Width);

                                double ratio = 1;
                                if (specproductType.Equals("1"))
                                {
                                    ratio = 0.75;
                                    CmbProductType.SelectedIndex = 0;
                                }
                                else if (specproductType.Equals("2"))
                                {
                                    ratio = 0.8;
                                    CmbProductType.SelectedIndex = 1;
                                }
                                else if (specproductType.Equals("30"))
                                {
                                    ratio = 0.67;
                                    CmbProductType.SelectedIndex = 2;
                                }
                                else
                                {
                                    if (img.Height > img.Width)
                                        ratio = (double)(img.Width / img.Height);
                                    else
                                        ratio = (double)(img.Height / img.Width);
                                }

                                if (forWdht.Height > forWdht.Width)
                                    forWdht.Width = forWdht.Height * ratio;
                                else
                                    forWdht.Height = forWdht.Width * ratio;

                                frm.Width = forWdht.Width;
                                frm.Height = forWdht.Height;
                                forWdht.InvalidateArrange();
                                forWdht.InvalidateMeasure();
                                forWdht.InvalidateVisual();
                                frm.Children.Add(objCurrent);

                                graphicsframeApplied = true;
                                VisualStateManager.GoToState(btnBorder, "Checked", true);
                                VisualStateManager.GoToState(btnAddgraphics, "Checked", true);
                                img.Freeze();
                            }
                            else
                            {
                                ChromaBorderPath = string.Empty;
                                OriginalBorder = string.Empty;
                            }
                            #endregion
                            break;
                        case "bg":
                            if (!string.IsNullOrEmpty(borderValue))
                                break;
                            if (((System.Xml.XmlAttribute)xn).Value.ToString() != string.Empty)
                            {
                                BitmapImage objCurrent = new BitmapImage(new Uri(Common.LoginUser.DigiFolderBackGroundPath + "\\8x10" + @"\" + ((System.Xml.XmlAttribute)xn).Value.ToString()));

                                if (canbackgroundold.Background != null)
                                    canbackgroundold.Background.Opacity = 0;
                                canbackground.Background = new ImageBrush { ImageSource = objCurrent };

                                LoadBackgroundInGrid(objCurrent);
                                objCurrent.Freeze();
                            }
                            break;
                        case "canvasleft":
                            Canvas.SetLeft(Opacitymsk, Convert.ToDouble((((System.Xml.XmlAttribute)xn).Value)));
                            Canvas.SetLeft(canbackgroundParent, Convert.ToDouble((((System.Xml.XmlAttribute)xn).Value)));
                            ChromaGridLeft = Convert.ToDouble((((System.Xml.XmlAttribute)xn).Value));
                            break;
                        case "canvastop":
                            Canvas.SetTop(Opacitymsk, Convert.ToDouble((((System.Xml.XmlAttribute)xn).Value)));
                            Canvas.SetTop(canbackgroundParent, Convert.ToDouble((((System.Xml.XmlAttribute)xn).Value)));
                            ChromaGridTop = Convert.ToDouble((((System.Xml.XmlAttribute)xn).Value));
                            break;
                        case "rotatetransform":
                            RotateTransform rtm = new RotateTransform();
                            rtm.CenterX = 0;
                            rtm.CenterY = 0;
                            rtm.Angle = Convert.ToDouble((((System.Xml.XmlAttribute)xn).Value));
                            canbackground.RenderTransformOrigin =
                                  GrdBrightness.RenderTransformOrigin = new System.Windows.Point(0.5, 0.5);
                            if (rtm.Angle != -1 && rtm.Angle != 0)
                            {
                                canbackground.RenderTransform =
                                  GrdBrightness.RenderTransform = rtm;
                                jrotate.Angle = rtm.Angle;
                                VisualStateManager.GoToState(btnAddgraphics, "Checked", true);
                            }
                            break;
                        case "scalecentrex":
                            _centerX = ((System.Xml.XmlAttribute)xn).Value.ToString();
                            ChromaCenterX = Convert.ToDouble(_centerX);
                            break;
                        case "scalecentrey":
                            _centerY = ((System.Xml.XmlAttribute)xn).Value.ToString();
                            ChromaCenterY = Convert.ToDouble(_centerY);
                            break;
                        case "zoomfactor":
                            _ZoomFactor = Convert.ToDouble(((System.Xml.XmlAttribute)xn).Value.ToString());
                            ChromaZoomFactor = _ZoomFactor;
                            break;

                        case "frmzindex":
                            Canvas.SetZIndex(frm, Convert.ToInt32(((System.Xml.XmlAttribute)xn).Value.ToString()));
                            break;
                    }
                }


                if (frm.Children.Count == 0 && (ImageBrush)imageundoGrid.Background != null && !isGreenImage)
                {
                    double ratio = 1;
                    double hgt = (((ImageBrush)imageundoGrid.Background).ImageSource).Height;
                    double wdt = (((ImageBrush)imageundoGrid.Background).ImageSource).Width;
                    if (hgt > wdt)
                    {
                        ratio = (double)(wdt / hgt);
                    }
                    else
                    {
                        ratio = (double)(hgt / wdt);
                    }

                    if (forWdht.Height > forWdht.Width)
                    {
                        forWdht.Width = forWdht.Height * ratio;
                    }
                    else
                    {
                        forWdht.Height = forWdht.Width * ratio;
                    }
                    forWdht.InvalidateArrange();
                    forWdht.InvalidateMeasure();
                    forWdht.InvalidateVisual();
                    Zomout(true);
                }

                if (_centerX != "-1")
                {
                    ScaleTransform st = new ScaleTransform();
                    st.CenterX = Convert.ToDouble(_centerX);
                    st.CenterY = Convert.ToDouble(_centerY);

                    if (_ZoomFactor != -1)
                    {
                        st.ScaleX = _ZoomFactor;
                        st.ScaleY = _ZoomFactor;
                        if (_ZoomFactor != 1)
                            VisualStateManager.GoToState(btnAddgraphics, "Checked", true);
                        lblzoomplus.Content = _ZoomFactor * 100 + " % ";
                    }


                    if (FlipMode != 0 || FlipModeY != 0)
                    {
                        st.ScaleX = st.ScaleX;
                    }

                    zoomTransform = st;
                    transformGroup.Children.Add(st);
                }
                if (_ZoomFactor == 1)
                    lblzoomplus.Content = _ZoomFactor * 100 + " % ";

                if (translateX != 0)
                {
                    TranslateTransform st = new TranslateTransform();
                    st.X = Convert.ToDouble(translateX);
                    st.Y = Convert.ToDouble(translateY);
                    translateTransform = st;
                    transformGroup.Children.Add(st);
                }

                if (GrdGreenScreenDefault3.RenderTransform == null)
                {
                    MyInkCanvas.RenderTransform = null;
                    canbackground.RenderTransform = GrdGreenScreenDefault3.RenderTransform = transformGroup;
                }
                System.Xml.XmlReader rdr = System.Xml.XmlReader.Create(new System.IO.StringReader(Xdocument.InnerXml.ToString()));
                while (rdr.Read())
                {
                    if (rdr.NodeType == XmlNodeType.Element)
                    {
                        switch (rdr.Name.ToString().ToLower())
                        {
                            #region Graphics
                            case "graphics":
                                Button btngrph = new Button();
                                btngrph.HorizontalAlignment = System.Windows.HorizontalAlignment.Center;
                                btngrph.VerticalAlignment = System.Windows.VerticalAlignment.Center;
                                Style defaultStyle = (Style)FindResource("ButtonStyleGraphic");
                                btngrph.Style = defaultStyle;
                                System.Windows.Controls.Image imgctrl = new System.Windows.Controls.Image();
                                BitmapImage img = new BitmapImage(new Uri(Common.LoginUser.DigiFolderGraphicsPath + @"\" + rdr.GetAttribute("source").ToString()));
                                imgctrl.Name = "A" + Guid.NewGuid().ToString().Split('-')[0].ToString();
                                btngrph.Name = "btn" + Guid.NewGuid().ToString().Split('-')[0].ToString();
                                btngrph.Uid = "uid" + Guid.NewGuid().ToString().Split('-')[0].ToString();
                                imgctrl.Source = img;
                                btngrph.Width = 90;
                                btngrph.Height = 90;
                                btngrph.Click += new RoutedEventHandler(btngrph_Click);
                                btngrph.Content = imgctrl;
                                dragCanvas.Children.Add(btngrph);
                                imgctrl.MouseLeftButtonUp += new MouseButtonEventHandler(SelectObject);
                                btngrph.MouseLeftButtonUp += new MouseButtonEventHandler(SelectObject);
                                btngrph.GotFocus += new RoutedEventHandler(btngrph_GotFocus);
                                var left = string.Format("{0:0.00}", Convert.ToDouble(rdr.GetAttribute("left").ToString()));
                                var top = string.Format("{0:0.00}", Convert.ToDouble(rdr.GetAttribute("top").ToString()));
                                Canvas.SetLeft(btngrph, Convert.ToDouble(left));
                                Canvas.SetTop(btngrph, Convert.ToDouble(top));

                                Double ZoomFactor = Convert.ToDouble(rdr.GetAttribute("zoomfactor").ToString());
                                btngrph.Width = Convert.ToDouble(rdr.GetAttribute("wthsource").ToString()) != null ? Convert.ToDouble(rdr.GetAttribute("wthsource").ToString()) : 90;
                                btngrph.Height = Convert.ToDouble(rdr.GetAttribute("wthsource").ToString()) != null ? Convert.ToDouble(rdr.GetAttribute("wthsource").ToString()) : 90;
                                btngrph.Tag = ZoomFactor.ToString();
                                TransformGroup tg = new TransformGroup();
                                RotateTransform rotation = new RotateTransform();
                                ScaleTransform scale = new ScaleTransform();
                                scale.CenterX = 0;
                                scale.CenterY = 0;
                                if (rdr.GetAttribute("scalex") != null)
                                {
                                    scale.ScaleX = Convert.ToDouble(rdr.GetAttribute("scalex").ToString());
                                }
                                if (rdr.GetAttribute("scaley") != null)
                                {
                                    scale.ScaleY = Convert.ToDouble(rdr.GetAttribute("scaley").ToString());
                                }

                                if (rdr.GetAttribute("zindex").ToString() != string.Empty)
                                {
                                    Canvas.SetZIndex(btngrph, Convert.ToInt32(rdr.GetAttribute("zindex").ToString()));
                                }
                                else
                                {
                                    Canvas.SetZIndex(btngrph, 4);
                                }

                                btngrph.RenderTransformOrigin = new System.Windows.Point(0.5, 0.5);
                                tg.Children.Add(scale);
                                rotation.CenterX = 0;
                                rotation.CenterY = 0;
                                rotation.Angle = Convert.ToDouble(rdr.GetAttribute("angle").ToString());
                                btngrph.RenderTransformOrigin = new System.Windows.Point(0.5, 0.5);
                                tg.Children.Add(rotation);

                                btngrph.RenderTransform = tg;
                                VisualStateManager.GoToState(btngraphics, "Checked", true);
                                VisualStateManager.GoToState(btnAddgraphics, "Checked", true);
                                graphicsCount++;
                                img.Freeze();
                                break;
                            #endregion
                            #region TextLogo
                            case "textlogo":
                                TextBox txtLogo = new TextBox();
                                txtLogo.ContextMenu = dragCanvas.ContextMenu;
                                txtLogo.Background = new SolidColorBrush(Colors.Transparent);
                                if (rdr.GetAttribute("FontColor").ToString() != string.Empty)
                                {
                                    var converter = new System.Windows.Media.BrushConverter();
                                    var brush = (System.Windows.Media.Brush)converter.ConvertFromString(rdr.GetAttribute("FontColor").ToString());
                                    txtLogo.Foreground = brush;
                                }
                                else
                                {
                                    txtLogo.Foreground = new SolidColorBrush(Colors.DarkRed);
                                }

                                if (rdr.GetAttribute("FontSize").ToString() != string.Empty)
                                {
                                    txtLogo.FontSize = Convert.ToDouble(rdr.GetAttribute("FontSize").ToString());
                                }
                                else
                                {
                                    txtLogo.FontSize = 20.00;
                                }

                                if (rdr.GetAttribute("FontFamily").ToString() != string.Empty)
                                {
                                    System.Windows.Media.FontFamily fw = (System.Windows.Media.FontFamily)new FontFamilyConverter().ConvertFromString(rdr.GetAttribute("FontFamily").ToString());
                                    txtLogo.FontFamily = fw;
                                }

                                txtLogo.RenderTransformOrigin = new System.Windows.Point(0.5, 0.5);
                                txtLogo.Uid = "txtLogoBlock";
                                txtLogo.FontWeight = FontWeights.Bold;
                                txtLogo.BorderBrush = null;
                                txtLogo.Style = (Style)FindResource("SearchIDTB");
                                dragCanvas.Children.Add(txtLogo);
                                Canvas.SetLeft(txtLogo, Convert.ToDouble(rdr.GetAttribute("Left").ToString()));
                                Canvas.SetTop(txtLogo, Convert.ToDouble(rdr.GetAttribute("Top").ToString()));
                                Canvas.SetZIndex(txtLogo, Convert.ToInt32(rdr.GetAttribute("ZIndex")));
                                rotation = new RotateTransform();
                                rotation.CenterX = 0;
                                rotation.CenterY = 0;
                                rotation.Angle = Convert.ToDouble(rdr.GetAttribute("Angle").ToString());
                                txtLogo.RenderTransformOrigin = new System.Windows.Point(0.5, 0.5);
                                txtLogo.RenderTransform = rotation;
                                txtLogo.Text = rdr.GetAttribute("Content").ToString();
                                txtLogo.MouseLeftButtonUp += new MouseButtonEventHandler(SelectObject);
                                VisualStateManager.GoToState(btnGraphicsText, "Checked", true);
                                VisualStateManager.GoToState(btnAddgraphics, "Checked", true);
                                graphicsTextBoxCount++;
                                break;
                            #endregion TextLogo
                            #region Textbox
                            case "textbox":

                                TextBox txtTest = new TextBox();
                                txtTest.ContextMenu = dragCanvas.ContextMenu;
                                txtTest.Background = new SolidColorBrush(Colors.Transparent);
                                if (rdr.GetAttribute("foreground").ToString() != string.Empty)
                                {
                                    var converter = new System.Windows.Media.BrushConverter();
                                    var brush = (System.Windows.Media.Brush)converter.ConvertFromString(rdr.GetAttribute("foreground").ToString());
                                    txtTest.Foreground = brush;
                                }
                                else
                                {
                                    txtTest.Foreground = new SolidColorBrush(Colors.DarkRed);
                                }

                                if (rdr.GetAttribute("zindex").ToString() != string.Empty)
                                {
                                    Canvas.SetZIndex(txtTest, Convert.ToInt32(rdr.GetAttribute("zindex").ToString()));
                                }
                                else
                                {
                                    Canvas.SetZIndex(txtTest, 4);
                                }

                                if (rdr.GetAttribute("fontsize").ToString() != string.Empty)
                                {
                                    txtTest.FontSize = Convert.ToDouble(rdr.GetAttribute("fontsize").ToString());
                                }
                                else
                                {
                                    txtTest.FontSize = 20.00;
                                }

                                if (rdr.GetAttribute("_fontfamily").ToString() != string.Empty)
                                {
                                    System.Windows.Media.FontFamily fw = (System.Windows.Media.FontFamily)new FontFamilyConverter().ConvertFromString(rdr.GetAttribute("_fontfamily").ToString());
                                    txtTest.FontFamily = fw;
                                }

                                txtTest.RenderTransformOrigin = new System.Windows.Point(0.5, 0.5);
                                txtTest.Uid = "txtblock";
                                txtTest.FontWeight = FontWeights.Bold;
                                txtTest.BorderBrush = null;
                                txtTest.Style = (Style)FindResource("SearchIDTB");
                                dragCanvas.Children.Add(txtTest);
                                Canvas.SetLeft(txtTest, Convert.ToDouble(rdr.GetAttribute("left").ToString()));
                                Canvas.SetTop(txtTest, Convert.ToDouble(rdr.GetAttribute("top").ToString()));
                                rotation = new RotateTransform();
                                rotation.CenterX = 0;
                                rotation.CenterY = 0;
                                rotation.Angle = Convert.ToDouble(rdr.GetAttribute("angle").ToString());
                                txtTest.RenderTransformOrigin = new System.Windows.Point(0.5, 0.5);
                                txtTest.RenderTransform = rotation;
                                txtTest.MouseLeftButtonUp += new MouseButtonEventHandler(SelectObject);

                                txtTest.Text = rdr.GetAttribute("text").ToString();
                                VisualStateManager.GoToState(btnGraphicsText, "Checked", true);
                                VisualStateManager.GoToState(btnAddgraphics, "Checked", true);
                                graphicsTextBoxCount++;
                                break;
                                #endregion
                        }
                    }
                }
            }
            catch (Exception ex)
            {

                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
            finally
            {
#if DEBUG
                if (watch != null)
                    FrameworkHelper.CommonUtility.WatchStop(this.GetType().FullName + "." + System.Reflection.MethodBase.GetCurrentMethod().Name + "|", watch);
#endif
            }

        }

        void ApplyEffectsAgainAfterChroma()
        {
            Canvas.SetLeft(Opacitymsk, ChromaGridLeft);
            Canvas.SetLeft(canbackgroundParent, ChromaGridLeft);
            Canvas.SetTop(Opacitymsk, ChromaGridTop);
            Canvas.SetTop(canbackgroundParent, ChromaGridTop);

            if (ChromaCenterX > 0)
            {
                ScaleTransform st = new ScaleTransform();
                st.CenterX = ChromaCenterX;
                st.CenterY = ChromaCenterY;

                if (ChromaZoomFactor != -1)
                {
                    st.ScaleX = ChromaZoomFactor;
                    st.ScaleY = ChromaZoomFactor;
                    if (ChromaZoomFactor != 1)
                        VisualStateManager.GoToState(btnAddgraphics, "Checked", true);
                    lblzoomplus.Content = ChromaZoomFactor * 100 + " % ";
                }


                if (FlipMode != 0 || FlipModeY != 0)
                {
                    st.ScaleX = st.ScaleX;
                }

                zoomTransform = st;
                transformGroup.Children.Add(st);
                if (GrdGreenScreenDefault3.RenderTransform == null)
                {
                    canbackground.RenderTransform = GrdGreenScreenDefault3.RenderTransform = transformGroup;
                }
            }

            if (!String.IsNullOrWhiteSpace(ChromaBorderPath))
            {
                Uri uri = new Uri(ChromaBorderPath);
                BitmapImage img = new BitmapImage();
                BitmapImage img1 = new BitmapImage();
                img1.BeginInit();
                img1.UriSource = uri;
                img1.EndInit();
                img1.Freeze();
                img.BeginInit();
                img.UriSource = uri;
                double ratio = 1;
                if (ProductTypeId == 1)
                {
                    if (img1.Height > img1.Width)
                    {
                        img.DecodePixelHeight = 2400;
                        img.DecodePixelWidth = 1800;
                        img.EndInit();
                        ratio = (double)((double)img.PixelWidth / (double)img.PixelHeight);
                    }
                    else
                    {
                        img.DecodePixelHeight = 1800;
                        img.DecodePixelWidth = 2400;
                        img.EndInit();
                        ratio = (double)((double)img.PixelHeight / (double)img.PixelWidth);
                    }
                    if (widthimg.ActualHeight > widthimg.ActualWidth)
                    {
                        forWdht.Width = widthimg.ActualHeight * ratio;
                        forWdht.Height = widthimg.ActualHeight;
                    }
                    else
                    {
                        forWdht.Height = widthimg.ActualWidth * ratio;
                        forWdht.Width = widthimg.ActualWidth;
                    }
                }
                else if (ProductTypeId == 2)
                {
                    if (img1.Height > img1.Width)
                    {
                        img.DecodePixelHeight = 3000;
                        img.DecodePixelWidth = 2400;
                        img.EndInit();
                        ratio = (double)((double)img.PixelWidth / (double)img.PixelHeight);
                    }
                    else
                    {
                        img.DecodePixelHeight = 2400;
                        img.DecodePixelWidth = 3000;
                        img.EndInit();
                        ratio = (double)((double)img.PixelHeight / (double)img.PixelWidth);
                    }
                    if (widthimg.ActualHeight > widthimg.ActualWidth)
                    {
                        forWdht.Width = widthimg.ActualHeight * ratio;
                        forWdht.Height = widthimg.ActualHeight;
                    }
                    else
                    {
                        forWdht.Height = widthimg.ActualWidth * ratio;
                        forWdht.Width = widthimg.ActualWidth;
                    }
                }
                else if (ProductTypeId == 30)
                {
                    if (img1.Height > img1.Width)
                    {
                        img.DecodePixelHeight = 2700;
                        img.DecodePixelWidth = 1800;
                        img.EndInit();
                        ratio = (double)((double)img.PixelWidth / (double)img.PixelHeight);
                    }
                    else
                    {
                        img.DecodePixelHeight = 1800;
                        img.DecodePixelWidth = 2700;
                        img.EndInit();
                        ratio = (double)((double)img.PixelHeight / (double)img.PixelWidth);
                    }
                    if (widthimg.ActualHeight > widthimg.ActualWidth)
                    {
                        forWdht.Width = widthimg.ActualHeight * ratio;
                        forWdht.Height = widthimg.ActualHeight;
                    }
                    else
                    {
                        forWdht.Height = widthimg.ActualWidth * ratio;
                        forWdht.Width = widthimg.ActualWidth;
                    }
                }
                else if (ProductTypeId == 98)
                {
                    img.DecodePixelHeight = 900;
                    img.DecodePixelWidth = 900;
                    img.EndInit();
                    ratio = (double)((double)img.PixelWidth / (double)img.PixelHeight);
                    if (widthimg.ActualHeight > widthimg.ActualWidth)
                    {
                        forWdht.Width = widthimg.ActualHeight * ratio;
                        forWdht.Height = widthimg.ActualHeight;
                    }
                    else
                    {
                        forWdht.Height = widthimg.ActualWidth * ratio;
                        forWdht.Width = widthimg.ActualWidth;
                    }
                }
                else
                {
                    if (img.Height > img.Width)  ////Img Border
                    {
                        ratio = (double)(img.Width / img.Height);
                    }
                    else
                    {
                        ratio = (double)(img.Height / img.Width);
                    }

                    if (forWdht.Height > forWdht.Width)
                    {
                        forWdht.Width = forWdht.Height * ratio;
                    }
                    else
                    {
                        forWdht.Height = forWdht.Width * ratio;
                    }
                    img.EndInit();
                }
                selectedborder = System.IO.Path.GetFileName(img.ToString());
                OpaqueClickableImage objCurrent = new OpaqueClickableImage();
                objCurrent.Uid = "frame";
                objCurrent.Source = img;
                objCurrent.IsHitTestVisible = false;
                objCurrent.Stretch = Stretch.Fill; //temprorary Fix till Crop works properly
                objCurrent.Loaded += new RoutedEventHandler(objCurrent_Loaded);
                objCurrent.Opacity = 1;

                frm.Width = forWdht.Width;
                frm.Height = forWdht.Height;
                forWdht.InvalidateArrange();
                forWdht.InvalidateMeasure();
                forWdht.InvalidateVisual();
                frm.Children.Add(objCurrent);
                img.Freeze();
            }

            MemoryManagement.FlushMemory();
        }

        /// <summary>
        /// Saves the xaml.
        /// </summary>
        /// <param name="graphicsChange">if set to <c>true</c> [graphics change].</param>
        /// <returns></returns>
        private string SaveXaml(ref bool graphicsChange)
        {
#if DEBUG
            var watch = FrameworkHelper.CommonUtility.Watch();
            watch.Start();
#endif

            StringBuilder ResultXml = new StringBuilder();
            try
            {
                string photo_outputBorder = System.Windows.Markup.XamlWriter.Save(Opacitymsk);
                System.Xml.XmlDocument XdocumentBorder = new System.Xml.XmlDocument();
                XdocumentBorder.LoadXml(photo_outputBorder);

                StringBuilder ResultXmlBorder = new StringBuilder();
                string _canvasleft = "-1";
                string _canvastop = "-1";
                string _rotatetransform = "-1";
                string _scalecentrex = "-1";
                string _scalecentrey = "-1";
                foreach (var xn in (XdocumentBorder.ChildNodes[0]).Attributes)
                {
                    switch (((System.Xml.XmlAttribute)xn).Name.ToLower())
                    {
                        case "canvas.left":
                            _canvasleft = ((System.Xml.XmlAttribute)xn).Value.ToString();
                            break;
                        case "canvas.top":
                            _canvastop = ((System.Xml.XmlAttribute)xn).Value.ToString();
                            break;
                    }
                }

                graphicsChange = true;
                if ((_canvasleft == "-1" && _canvastop == "-1"))
                {
                    graphicsChange = false;
                }

                if (_canvasleft == "0" && _canvastop == "0")
                {
                    graphicsChange = false;
                }

                System.Xml.XmlReader rdrBorder = System.Xml.XmlReader.Create(new System.IO.StringReader(XdocumentBorder.InnerXml.ToString()));
                while (rdrBorder.Read())
                {
                    if (rdrBorder.NodeType == XmlNodeType.Element)
                    {
                        switch (rdrBorder.Name.ToString().ToLower())
                        {
                            case "rotatetransform":
                                if (rdrBorder.GetAttribute("Angle") != null)
                                {
                                    _rotatetransform = rdrBorder.GetAttribute("Angle").ToString();
                                }

                                break;
                        }
                    }
                }


                string photo_output = System.Windows.Markup.XamlWriter.Save(canbackground);
                System.Xml.XmlDocument Xdocument = new System.Xml.XmlDocument();
                Xdocument.LoadXml(photo_output);
                System.Xml.XmlReader rdr = System.Xml.XmlReader.Create(new System.IO.StringReader(Xdocument.InnerXml.ToString()));
                while (rdr.Read())
                {
                    if (rdr.NodeType == XmlNodeType.Element)
                    {
                        switch (rdr.Name.ToString().ToLower())
                        {
                            case "scaletransform":
                                if (rdr.GetAttribute("CenterX") != null)
                                {
                                    _scalecentrex = rdr.GetAttribute("CenterX").ToString();
                                }
                                if (rdr.GetAttribute("CenterY") != null)
                                {
                                    _scalecentrey = rdr.GetAttribute("CenterY").ToString();
                                }
                                break;
                        }
                    }
                }

                string bgfilename = string.Empty;
                if (imageundoGrid.Background != null && ((System.Windows.Media.Imaging.BitmapImage)(((System.Windows.Media.ImageBrush)(imageundoGrid.Background)).ImageSource)) != null)
                {
                    string Absolutepath = ((System.Windows.Media.Imaging.BitmapImage)(((System.Windows.Media.ImageBrush)(imageundoGrid.Background)).ImageSource)).UriSource.LocalPath.ToString();
                    int segment = Absolutepath.Split('\\').Count();
                    bgfilename = Absolutepath.Split('\\')[segment - 1].ToString();
                    graphicsChange = true;

                }

                string FrameFileName = string.Empty;
                foreach (var chid in frm.Children)
                {

                    if (chid is System.Windows.Controls.Image)
                    {
                        System.Windows.Controls.Image FrontFrame = (System.Windows.Controls.Image)chid;
                        if (FrontFrame.Uid.ToString() == "frame")
                        {
                            string Absolutepath = FrontFrame.Source.ToString();
                            int segment = Absolutepath.Split('/').Count();
                            FrameFileName = Absolutepath.Split('/')[segment - 1].ToString();
                            graphicsChange = true;
                        }
                    }
                }

                if ((jrotate.Angle != 0) || (_ZoomFactor >= .5 && _ZoomFactor != 1) || !string.IsNullOrEmpty(FrameFileName) || !string.IsNullOrEmpty(bgfilename))
                {
                    graphicsChange = true;
                }

                int frmZindex = Canvas.GetZIndex(frm);


                ResultXml.Append("<photo  producttype = '" + specproductType + "' frmZindex = '" + frmZindex + "' zoomfactor = '" + _ZoomFactor + "' border='" + FrameFileName + "' bg='" + bgfilename + "' canvasleft='" + _canvasleft + "' canvastop='" + _canvastop + "' rotatetransform='" + _rotatetransform + "' scalecentrex='" + _scalecentrex + "' scalecentrey='" + _scalecentrey + "'>");


                foreach (var ctrl in dragCanvas.Children)
                {
                    if (ctrl is Button)
                    {
                        graphicsChange = true;
                        Button cid = (Button)ctrl;
                        string Angle = "-1";
                        try
                        {
                            Angle = ((System.Windows.Media.RotateTransform)(cid.RenderTransform)).Angle.ToString();
                        }
                        catch (Exception)
                        {

                        }

                        int zindex = (int)cid.GetValue(Canvas.ZIndexProperty);
                        double top1 = Canvas.GetTop(cid);
                        double left1 = Canvas.GetLeft(cid);
                        string source = ((System.Windows.Controls.Image)(cid.Content)).Source.ToString();
                        int segment = source.Split('/').Count();
                        source = source.Split('/')[segment - 1].ToString();

                        TransformGroup tg = cid.GetValue(Canvas.RenderTransformProperty) as TransformGroup;
                        RotateTransform rotation = new RotateTransform();
                        ScaleTransform scale = new ScaleTransform();

                        if (tg != null)
                        {
                            if (tg.Children.Count > 0)
                            {
                                if (tg.Children[0] is ScaleTransform)
                                {
                                    scale = (ScaleTransform)tg.Children[0];
                                }
                                else if (tg.Children[0] is RotateTransform)
                                {
                                    rotation = (RotateTransform)tg.Children[0];
                                    Angle = rotation.Angle.ToString();
                                }
                            }
                            if (tg.Children.Count > 1)
                            {
                                if (tg.Children[1] is ScaleTransform)
                                {
                                    scale = (ScaleTransform)tg.Children[1];
                                }
                                else if (tg.Children[1] is RotateTransform)
                                {
                                    rotation = (RotateTransform)tg.Children[1];
                                    Angle = rotation.Angle.ToString();
                                }
                            }

                        }

                        ResultXml.Append("<graphics zindex ='" + zindex + "' wthsource ='" + cid.Width + "' source ='" + source + "' angle='" + Angle + "' top ='" + top1 + "' left='" + left1 + "' scalex ='" + scale.ScaleX.ToString() + "' scaley ='" + scale.ScaleY.ToString() + "' zoomfactor = '" + cid.Tag.ToString() + "'></graphics>");
                    }
                    if (ctrl is TextBox)
                    {
                        graphicsChange = true;
                        TextBox cid = (TextBox)ctrl;
                        string Text = cid.Text;
                        Text = Text.Replace("&", "&amp;");
                        Text = Text.Replace("<", "&lt;");
                        Text = Text.Replace(">", "&gt;");
                        Text = Text.Replace("\"", "&quot;");
                        Text = Text.Replace("'", "&apos;");
                        string output = System.Windows.Markup.XamlWriter.Save(cid);
                        Xdocument = new System.Xml.XmlDocument();
                        Xdocument.LoadXml(output);

                        string _textleft = "-1";
                        string _texttop = "-1";
                        string _foreground = "-1";
                        string _fontfamily = "-1";
                        string _fontsize = "-1";
                        string _Angle = "-1";
                        int zindex = (int)cid.GetValue(Canvas.ZIndexProperty);
                        foreach (var xn in (Xdocument.ChildNodes[0]).Attributes)
                        {
                            switch (((System.Xml.XmlAttribute)xn).Name.ToLower())
                            {
                                case "canvas.left":
                                    _textleft = ((System.Xml.XmlAttribute)xn).Value.ToString();
                                    break;
                                case "canvas.top":
                                    _texttop = ((System.Xml.XmlAttribute)xn).Value.ToString();
                                    break;
                                case "foreground":
                                    _foreground = ((System.Xml.XmlAttribute)xn).Value.ToString();
                                    break;
                                case "fontfamily":
                                    _fontfamily = ((System.Xml.XmlAttribute)xn).Value.ToString();
                                    break;
                                case "fontsize":
                                    _fontsize = ((System.Xml.XmlAttribute)xn).Value.ToString();
                                    break;
                            }
                        }

                        rdr = System.Xml.XmlReader.Create(new System.IO.StringReader(Xdocument.InnerXml.ToString()));

                        while (rdr.Read())
                        {
                            if (rdr.NodeType == XmlNodeType.Element)
                            {
                                switch (rdr.Name.ToString().ToLower())
                                {
                                    case "rotatetransform":
                                        if (rdr.GetAttribute("Angle") != null)
                                        {
                                            _Angle = rdr.GetAttribute("Angle").ToString();
                                        }

                                        break;
                                }
                            }
                        }
                        ResultXml.Append("<textbox zindex ='" + zindex + "'  text ='" + Text + "' angle='" + _Angle + "' top ='" + _texttop + "' left='" + _textleft + "' foreground='" + _foreground + "' _fontfamily='" + _fontfamily + "'  fontsize='" + _fontsize + "' ></textbox>");
                    }
                }
                ResultXml.Append("</photo>");
#if DEBUG
                if (watch != null)
                    FrameworkHelper.CommonUtility.WatchStop(this.GetType().FullName + "." + System.Reflection.MethodBase.GetCurrentMethod().Name + "|", watch);
#endif
                return ResultXml.ToString();
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                return ResultXml.ToString();
            }

        }

        /// <summary>
        /// Resizes the WPF image.
        /// </summary>
        /// <param name="sourceImage">The source image.</param>
        /// <param name="maxHeight">The maximum height.</param>
        /// <param name="saveToPath">The save automatic path.</param>
        private void ResizeWPFImage(string sourceImage, int maxHeight, string saveToPath)
        {
#if DEBUG
            var watch = FrameworkHelper.CommonUtility.Watch();
            watch.Start();
#endif

            try
            {
                BitmapImage bi = new BitmapImage();
                BitmapImage bitmapImage = new BitmapImage();
                using (FileStream fileStream = File.OpenRead(sourceImage.ToString()))
                {
                    MemoryStream ms = new MemoryStream();
                    fileStream.CopyTo(ms);
                    ms.Seek(0, SeekOrigin.Begin);
                    fileStream.Close();
                    bi.BeginInit();
                    bi.StreamSource = ms;
                    bi.EndInit();
                    bi.Freeze();


                    decimal ratio = 0;
                    int newWidth = 0;
                    int newHeight = 0;

                    if (bi.Width >= bi.Height)
                    {
                        ratio = Convert.ToDecimal(bi.Width) / Convert.ToDecimal(bi.Height);
                        newWidth = maxHeight;
                        newHeight = Convert.ToInt32(maxHeight / ratio);
                    }
                    else
                    {
                        ratio = Convert.ToDecimal(bi.Height) / Convert.ToDecimal(bi.Width);
                        newHeight = maxHeight;
                        newWidth = Convert.ToInt32(maxHeight / ratio);
                    };

                    ms.Seek(0, SeekOrigin.Begin);
                    bitmapImage.BeginInit();
                    bitmapImage.StreamSource = ms;
                    bitmapImage.DecodePixelWidth = newWidth;
                    bitmapImage.DecodePixelHeight = newHeight;
                    bitmapImage.EndInit();
                    bitmapImage.Freeze();
                }

                using (var fileStreamForSave = new FileStream(saveToPath, FileMode.Create, FileAccess.ReadWrite))
                {
                    JpegBitmapEncoder encoder = new JpegBitmapEncoder();
                    encoder.QualityLevel = 88;//Changed to reduce the size and time
                    encoder.Frames.Add(BitmapFrame.Create(bitmapImage));
                    encoder.Save(fileStreamForSave);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
#if DEBUG
            if (watch != null)
                FrameworkHelper.CommonUtility.WatchStop(this.GetType().FullName + "." + System.Reflection.MethodBase.GetCurrentMethod().Name + "|", watch);
#endif
        }

        #endregion

        #region Eraser

        /// <summary>
        /// The is eraser active
        /// </summary>
        bool IsEraserActive = false;
        /// <summary>
        /// The is eraser draw rectangle active
        /// </summary>
        bool IsEraserDrawRectangleActive = false;
        /// <summary>
        /// The is eraser draw ellipse active
        /// </summary>
        bool IsEraserDrawEllipseActive = false;
        /// <summary>
        /// The is green correction
        /// </summary>
        bool IsGreenCorrection = false;

        #endregion

        /// <summary>
        /// Interface
        /// </summary>
        /// <value>
        /// The selected drawing attributes.
        /// </value>
        public DrawingAttributes SelectedDrawingAttributes
        {
            get
            {
                return attribute;
            }
            set
            {
                attribute = value;
            }
        }

        /// <summary>
        /// The attribute width
        /// </summary>
        double attributeWidth = 10;
        /// <summary>
        /// The attribute height
        /// </summary>
        double attributeHeight = 10;
        bool IsMoveEnabled = false;

        #region GreenScreen
        /// <summary>
        /// The islst background visible
        /// </summary>
        bool islstBackgroundVisible = false;

        private void HideGraphics()
        {
            foreach (UIElement ui in dragCanvas.Children)
            {
                if (ui is Grid)
                {
                }
                else if (ui is Ellipse)
                {
                }
                else if (ui is Rectangle)
                {
                }
                else if (ui is System.Windows.Shapes.Path)
                {

                }
                else
                {
                    ui.Visibility = Visibility.Hidden;
                }
            }
        }

        private void ShowGraphics()
        {
            foreach (UIElement ui in dragCanvas.Children)
            {
                if (ui is Grid)
                {
                }
                else if (ui is Ellipse)
                {
                }
                else if (ui is Rectangle)
                {
                }
                else if (ui is System.Windows.Shapes.Path)
                {

                }
                else
                {
                    ui.Visibility = Visibility.Visible;
                }
            }
        }

        /// <summary>
        /// Handles the Click event of the btnGreenScreenBackGround control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void btnGreenScreenBackGround_Click(object sender, RoutedEventArgs e)
        {
            if (GrdEffects.Visibility == System.Windows.Visibility.Visible)
            {
                GrdEffects.Visibility = System.Windows.Visibility.Collapsed;
                lstBackground.Visibility = System.Windows.Visibility.Collapsed;
                lstFrame.Visibility = Visibility.Collapsed;
                lstGraphics.Visibility = Visibility.Collapsed;
            }
            else
            {
                GrdEffects.Visibility = System.Windows.Visibility.Visible;
                lstBackground.Visibility = System.Windows.Visibility.Visible;
                lstFrame.Visibility = Visibility.Collapsed;
                lstGraphics.Visibility = Visibility.Collapsed;
            }
        }

        /// <summary>
        /// The _green undo effect
        /// </summary>
        UndoBrushEffect _greenUndoEffect = new UndoBrushEffect();

        /// <summary>
        /// The saturation
        /// </summary>
        static double saturation = .38;
        /// <summary>
        /// The lightness
        /// </summary>
        static double lightness = .15;
        /// <summary>
        /// The is green remove
        /// </summary>
        bool IsGreenRemove = false;
        /// <summary>
        /// The is chroma applied
        /// </summary>
        bool isChromaApplied = false;



        #endregion

        #region Chroma Code


        /// <summary>
        /// The rectangle eraser
        /// </summary>
        bool rectangleEraser = false;
        /// <summary>
        /// The undo eraser
        /// </summary>
        bool undoEraser = false;
        /// <summary>
        /// The fill size
        /// </summary>
        int fillSize = 5;

        /// <summary>
        /// The primary color
        /// </summary>
        string primaryColor = string.Empty;
        string primaryColorName = string.Empty;
        float tolerance = float.Parse("0");

        string primaryColorDefault = string.Empty;
        float toleranceDefault = float.Parse("0");

        /// <summary>
        /// Handles the KeyDown event of the MyInkCanvas control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="KeyEventArgs"/> instance containing the event data.</param>
        private void MyInkCanvas_KeyDown(object sender, KeyEventArgs e)
        {
            if (GrdGreenScreenDefault.IsHitTestVisible == false)
            {
                if (e.Key == Key.OemPlus)
                {
                }

                if (e.Key == Key.OemMinus)
                {
                }
            }
            else
            {
                if (e.Key == Key.OemPlus)
                {
                    attribute.Width++;
                    attribute.Height++;
                }

                if (e.Key == Key.OemMinus)
                {
                    if (attribute.Width > 2)
                        attribute.Width--;
                    attribute.Height--;
                }
            }

        }

        #endregion

        /// <summary>
        /// The WDT
        /// </summary>
        double wdt = 10;
        /// <summary>
        /// The HGT
        /// </summary>
        double hgt = 10;
        /// <summary>
        /// The ink canvas ellipse
        /// </summary>
        static Ellipse inkCanvasEllipse = new Ellipse();
        /// <summary>
        /// The ink canvas rectangle
        /// </summary>
        static Rectangle inkCanvasRectangle = new Rectangle();
        bool IsZoomed = false;

        static public childItem FindVisualChild1<childItem>(DependencyObject obj)
            where childItem : DependencyObject
        {
            // Search immediate children
            for (int i = 0; i < VisualTreeHelper.GetChildrenCount(obj); i++)
            {
                DependencyObject child = VisualTreeHelper.GetChild(obj, i);

                if (child is childItem)
                    return (childItem)child;

                else
                {
                    childItem childOfChild = FindVisualChild1<childItem>(child);

                    if (childOfChild != null)
                        return childOfChild;
                }
            }

            return null;
        }

        private Button FindVisualChild<item>(DependencyObject obj, string borderName)
            where item : DependencyObject
        {

            for (int i = 0; i < VisualTreeHelper.GetChildrenCount(obj); i++)
            {
                DependencyObject child = VisualTreeHelper.GetChild(obj, i);

                if (child != null && child is Button)
                {
                    // DependencyObject childText = VisualTreeHelper.GetChild(obj, i + 1);
                    Button btn = (Button)(object)child;

                    if (string.Compare(btn.CommandParameter.ToString(), borderName, true) == 0)
                    {
                        return btn;
                    }
                }
                else
                {
                    Button childOfChild = FindVisualChild<item>(child, borderName);
                    if (childOfChild != null && string.Compare(childOfChild.CommandParameter.ToString(), borderName, true) == 0)
                        return childOfChild;
                }
            }
            return null;
        }

        private void RepeatButton_MouseWheel(object sender, MouseWheelEventArgs e)
        {
            if (e.Delta > 0)
            {
                ZoomInButton_Click(sender, new RoutedEventArgs());
            }
            else
            {
                ZoomOutButton_Click(sender, new RoutedEventArgs());
            }
        }

        private void canbackgroundold_MouseWheel(object sender, MouseWheelEventArgs e)
        {
            if ((jrotate.Visibility != Visibility.Visible) && (spRotatepanel.Visibility != Visibility.Visible))
            {
                IsSelectedMainImage = true;
                if (e.Delta > 0)
                {
                    ZoomInButton_Click(sender, new RoutedEventArgs());
                }
                else
                {
                    ZoomOutButton_Click(sender, new RoutedEventArgs());
                }
            }
        }

        private void RepeatButton_MouseWheel_1(object sender, MouseWheelEventArgs e)
        {
            if (e.Delta > 0)
            {
                ZoomInButton_Click1(sender, new RoutedEventArgs());
            }
            else
            {
                ZoomOutButton_Click1(sender, new RoutedEventArgs());
            }
        }

        private void RepeatButton_MouseWheel_2(object sender, MouseWheelEventArgs e)
        {
            if (e.Delta > 0)
            {
                RotateInButton_Click(sender, new RoutedEventArgs());
            }
            else
            {
                AntiRotateButton_Click(sender, new RoutedEventArgs());
            }
        }

        private void jrotate_MouseWheel(object sender, MouseWheelEventArgs e)
        {
            if ((jrotate.Visibility == Visibility.Visible) && (spRotatepanel.Visibility == Visibility.Visible))
            {
                if (jrotate.IsMouseOver == true)
                {
                    IsSelectedMainImage = true;
                    if (e.Delta > 0)
                    {
                        RotateInButton_Click(sender, new RoutedEventArgs());
                    }
                    else
                    {
                        AntiRotateButton_Click(sender, new RoutedEventArgs());
                    }
                }
            }
        }


        private void lstBackground_IsVisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            if (lstBackground.Visibility == Visibility.Collapsed)
            {
                IsBtnBackgroundClicked = false;
                IsbtnBackgroundWithoutChroma = false;
            }
            else if (lstBackground.Visibility == Visibility.Visible)
                IsBtnBackgroundClicked = true;
        }

        private void GrdEffects_IsVisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            if (GrdEffects.Visibility == Visibility.Collapsed)
            {
                IsbtnBackgroundWithoutChroma = false;
                IsBtnBackgroundClicked = false;
            }
        }
        private void EditImage(FrameworkHelper.MyImageClass objItem)
        {
            try
            {
                IsImageDirtyState = false;
                this.PhotoName = objItem.Title;
                this.PhotoId = objItem.PhotoNumber;
                this._brighteff.Brightness = dbBrit;
                this._brighteff.Contrast = dbContr;
                this.SetEffect();

                PhotoBusiness photoBiz = new PhotoBusiness();
                this.tempfilename = objItem.Title + ".jpg";
                this.DateFolder = DateTime.Now.ToString("yyyyMMdd");
                bool iscrop, isgreen = false;
                string Layering = string.Empty;

                if (objItem.SettingStatus == SettingStatus.None)
                {
                    this.ImageEffect = "<image brightness='0' contrast='1' Crop='##' colourvalue = '##' rotate='##' ><effects sharpen='##' greyscale='0' digimagic='0' sepia='0' defog='0' underwater='0' emboss='0' invert = '0' granite='0' hue ='##' cartoon = '0'></effects></image>";
                    iscrop = false;
                    isGreenImage = false;
                }
                else if (objItem.SettingStatus == SettingStatus.Spec)
                {
                    if (String.IsNullOrWhiteSpace(dbBrit.ToString()))
                        dbBrit = 0;
                    if (String.IsNullOrWhiteSpace(dbContr.ToString()))
                        dbContr = 1;
                    this.ImageEffect = "<image brightness = '" + dbBrit.ToString() + "' contrast = '" + dbContr.ToString() + "' Crop='##' colourvalue = '##' rotate='##' ><effects sharpen='##' greyscale='0' digimagic='0' sepia='0' defog='0' underwater='0' emboss='0' invert = '0' granite='0' hue ='##' cartoon = '0'></effects></image>";
                    iscrop = objItem.IsCropped == null ? false : objItem.IsCropped;
                    isGreenImage = semiOrderSettings.DG_SemiOrder_Settings_IsImageBG == null ? false : Convert.ToBoolean(semiOrderSettings.DG_SemiOrder_Settings_IsImageBG);
                    System.Drawing.Bitmap img = null;
                    if (iscrop == true)
                        img = new System.Drawing.Bitmap(System.IO.Path.Combine(CropFolderPath, tempfilename));
                    else
                        img = new System.Drawing.Bitmap(System.IO.Path.Combine(HotFolderPath, DateFolder, tempfilename));
                    var imageHeight = img.Height;
                    var imageWidth = img.Width;
                    //Layering = SpecLayering;
                    //string SpecBorder = GetSpecBorderName();
                    if (imageHeight > imageWidth)
                    {
                        Layering = SpecLayeringVertical;
                    }
                    else
                    {
                        Layering = SpecLayeringHorizontal;
                    }
                    SpecLayeringUpdated = Layering;
                    img.Dispose();
                    img = null;
                }
                else
                {
                    if (objItem.NewEffectsXML == null)
                        this.ImageEffect = "<image brightness='0' contrast='1' Crop='##' colourvalue = '##' rotate='##' ><effects sharpen='##' greyscale='0' digimagic='0' sepia='0' defog='0' underwater='0' emboss='0' invert = '0' granite='0' hue ='##' cartoon = '0'></effects></image>";
                    else
                        this.ImageEffect = objItem.NewEffectsXML;
                    iscrop = objItem.IsCropped;
                    isgreen = objItem.IsGreen;
                    Layering = objItem.NewLayeringXML;
                }
                ImageClick = true;
                this.Onload(iscrop, isgreen, Layering, false);
                this.lstStrip.Visibility = Visibility.Visible;
                btnThumbnails_Click(new object(), new RoutedEventArgs());
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }

        private void lstStrip_VisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            if (((bool)e.NewValue) == true)
            {
                btnPreviousimg.Visibility = Visibility.Visible;
                btnNextsimg.Visibility = Visibility.Visible;
            }
            else
            {
                btnPreviousimg.Visibility = Visibility.Collapsed;
                btnNextsimg.Visibility = Visibility.Collapsed;
            }
        }

        void ItemContainerGenerator_StatusChanged(object sender, EventArgs e)
        {
            if (lstStrip.ItemContainerGenerator.Status == System.Windows.Controls.Primitives.GeneratorStatus.ContainersGenerated)
            {
                Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Input, new Action(DelayedAction));

            }
        }
        void DelayedAction()
        {
            lstStrip.SelectedIndex = lstStrip.Items.IndexOf(lstMyImageClass.Where(o => o.PhotoNumber == PhotoId).FirstOrDefault());
            if (IsLoad == true)
            {
                lstStrip.ScrollIntoView(lstStrip.SelectedItem);
                IsLoad = false;
            }
            //SetLastVisibleIndex();
            //SetFocus();
        }

    }
}
