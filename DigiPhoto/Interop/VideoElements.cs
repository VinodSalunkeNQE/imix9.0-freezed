﻿using DigiPhoto.IMIX.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace DigiPhoto.Interop
{

    //Video Elements Model
    public class VideoElements
    {
        //private BitmapImage _bitMapGuestImg;
        ///// <summary>
        ///// Guest Image Path
        ///// </summary>
        //public BitmapImage bitMapGuestImg
        //{
        //    get { return _bitMapGuestImg; }
        //    set { _bitMapGuestImg = value; }
        //}
        public VideoElements()
        {
            isVideoTemplate = Visibility.Collapsed;
        }

        private string _GuestImagePath;
        /// <summary>
        /// Guest Image Path
        /// </summary>
        public string GuestImagePath
        {
            get { return _GuestImagePath; }
            set { _GuestImagePath = value; }
        }
        private string _VideoFilePath;
        /// <summary>
        /// Guest Image Caption
        /// </summary>
        public string VideoFilePath
        {
            get { return _VideoFilePath; }
            set { _VideoFilePath = value; }
        }
        public int PhotoId;

        private string _Name;
        public string Name
        {
            get { return _Name; }
            set { _Name = value; }
        }
        /// <summary>
        /// The media type - Image 1 and Video 2
        /// </summary>
        private int _mediaType;
        /// <summary>
        /// Gets the media type - 1 for Image and 2 for Video
        /// </summary>
        public int MediaType
        {
            get { return _mediaType; }
            set { _mediaType = value; }
        }
        /// <summary>
        /// Play button visibility
        /// </summary>
        private Visibility _playVisible;
        public Visibility PlayVisible
        {
            get
            {
                //return _playVisible; 
                if (MediaType == 2 || MediaType == 3)
                {
                    return Visibility.Visible;
                }
                else
                {
                    return Visibility.Collapsed;
                }
            }
            set
            {
                _playVisible = value;
            }
        }
        private Visibility _ProcessedVisible;
        public Visibility ProcessedVisible
        {
            get
            {
                //return _playVisible; 
                if (MediaType == 3)
                {
                    return Visibility.Visible;
                }
                else
                {
                    return Visibility.Hidden;
                }
            }
            set
            {
                _ProcessedVisible = value;
            }
        }
        public long videoLength { get; set; }
        public int PageNo { get; set; }
        public Visibility isVideoTemplate { get; set; }
        public DateTime CreatedDate { get; set; }
        public bool IsValidSlot(List<SlotProperty> slotList, int startTime, int stopTime, long currentItemID)
        {
            SlotProperty firstItem = slotList.OrderBy(o => o.StartTime).Where(o => o.ItemID == currentItemID).FirstOrDefault();
            SlotProperty LastItem = slotList.OrderBy(o => o.StartTime).Where(o => o.ItemID == currentItemID).LastOrDefault();
            if (firstItem != null)
            {
                if (stopTime <= firstItem.StartTime)
                {
                    return true;
                }
                else if (startTime >= LastItem.StopTime)
                {
                    return true;
                }
                else
                {
                    List<SlotProperty> templst = slotList.OrderBy(o => o.StartTime).Where(o => o.ItemID == currentItemID).ToList();
                    SlotProperty item = templst.Where(o => o.ItemID == currentItemID && o.StopTime <= startTime).LastOrDefault();
                    SlotProperty item2 = templst[templst.IndexOf(item) + 1];
                    if (item2 != null)
                    {
                        if (item2.StartTime >= stopTime)
                        {
                            return true;
                        }
                        else
                        {
                            MessageBox.Show("Selected item is already applied in the given time frame.", "DigiPhoto", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                            return false;
                        }
                    }
                    else
                    {
                        MessageBox.Show("Selected item is already applied in the given time frame.", "DigiPhoto", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                        return false;
                    }
                }
            }
            else
            {
                return true;
            }
        }
        public bool IsValidSlot(List<SlotProperty> slotList, int startTime, int stopTime)
        {
            SlotProperty firstItem = slotList.OrderBy(o => o.StartTime).FirstOrDefault();
            SlotProperty LastItem = slotList.OrderBy(o => o.StartTime).LastOrDefault();
            if (firstItem != null)
            {
                if (stopTime <= firstItem.StartTime)
                {
                    return true;
                }
                else if (startTime >= LastItem.StopTime)
                {
                    return true;
                }
                else
                {
                    List<SlotProperty> templst = slotList.OrderBy(o => o.StartTime).ToList();
                    SlotProperty item = templst.Where(o => o.StopTime <= startTime).LastOrDefault();
                    SlotProperty item2 = templst[templst.IndexOf(item) + 1];
                    if (item2 != null)
                    {
                        if (item2.StartTime >= stopTime)
                        {
                            return true;
                        }
                        else
                        {
                            MessageBox.Show("Selected item is already applied in the given time frame.", "DigiPhoto", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                            return false;
                        }
                    }
                    else
                    {
                        MessageBox.Show("Selected item is already applied in the given time frame.", "DigiPhoto", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                        return false;
                    }
                }
            }
            else
            {
                return true;
            }
        }

        /// <summary>
        /// Resizes the WPF image.
        /// </summary>
        /// <param name="sourceImage">The source image.</param>
        /// <param name="maxHeight">The maximum height.</param>
        /// <param name="saveToPath">The save automatic path.</param>
        public void ResizeWPFImage(string sourceImage, int maxHeight, string saveToPath)
        {
            try
            {
                BitmapImage bi = new BitmapImage();
                BitmapImage bitmapImage = new BitmapImage();
                using (FileStream fileStream = File.OpenRead(sourceImage.ToString()))
                {
                    MemoryStream ms = new MemoryStream();
                    fileStream.CopyTo(ms);
                    ms.Seek(0, SeekOrigin.Begin);
                    fileStream.Close();
                    bi.BeginInit();
                    bi.StreamSource = ms;
                    bi.EndInit();
                    bi.Freeze();
                    decimal ratio = Convert.ToDecimal(bi.Width) / Convert.ToDecimal(bi.Height);
                    int newWidth = Convert.ToInt32(maxHeight * ratio);
                    int newHeight = maxHeight;
                    ms.Seek(0, SeekOrigin.Begin);
                    bitmapImage.BeginInit();
                    bitmapImage.StreamSource = ms;
                    bitmapImage.DecodePixelWidth = newWidth;
                    bitmapImage.DecodePixelHeight = newHeight;
                    bitmapImage.EndInit();
                    bitmapImage.Freeze();
                }
                using (var fileStreamForSave = new FileStream(saveToPath, FileMode.Create, FileAccess.ReadWrite, FileShare.ReadWrite))
                {
                    JpegBitmapEncoder encoder = new JpegBitmapEncoder();
                    encoder.QualityLevel = 94;
                    encoder.Frames.Add(BitmapFrame.Create(bitmapImage));
                    encoder.Save(fileStreamForSave);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
            finally
            {
                //if (DateTime.Now.Subtract(lastmemoryUpdateTime).Seconds > 30)
                //{
                //    MemoryManagement.FlushMemory();
                //    lastmemoryUpdateTime = DateTime.Now;
                //}
            }
        }
    }
    //Video Frames
    public class VideoFrames : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        public ProcessedVideoDetailsInfo objPVDetails = new ProcessedVideoDetailsInfo();
        public void OnPropertyChanged(PropertyChangedEventArgs e)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, e);
            }
        }

        private BitmapImage _bitFrame;
        public BitmapImage bitFrame
        {
            get { return _bitFrame; }
            set { _bitFrame = value; this.OnPropertyChanged(new PropertyChangedEventArgs("bitFrame")); }
        }
        private string _frameInstance;
        public string frameInstance
        {
            get { return _frameInstance; }
            set { _frameInstance = value; this.OnPropertyChanged(new PropertyChangedEventArgs("frameInstance")); }
        }

        private int _frameIndex;
        public int frameIndex
        {
            get { return _frameIndex; }
            set { _frameIndex = value; this.OnPropertyChanged(new PropertyChangedEventArgs("frameIndex")); }
        }
        private BitmapSource _bitSource;
        public BitmapSource bitSource
        {
            get { return _bitSource; }
            set { _bitSource = value; this.OnPropertyChanged(new PropertyChangedEventArgs("bitSource")); }
        }
        private string _StrokeColor;
        public string StrokeColor
        {
            get { return _StrokeColor; }
            set { _StrokeColor = value; this.OnPropertyChanged(new PropertyChangedEventArgs("StrokeColor")); }
        }
        private Visibility _visibilityCanvas;
        public Visibility visibilityCanvas
        {
            get { return _visibilityCanvas; }
            set
            {
                _visibilityCanvas = value;
                this.OnPropertyChanged(new PropertyChangedEventArgs("visibilityCanvas"));
            }
        }

    }
    //Video Page Model
    public class VideoPage : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        public ProcessedVideoDetailsInfo objPVDetails = new ProcessedVideoDetailsInfo();
        public void OnPropertyChanged(PropertyChangedEventArgs e)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, e);
            }
        }

        private int _PageNo;
        public int PageNo
        {
            get { return this._PageNo; }
            set
            {
                if (this._PageNo != value)
                {
                    this._PageNo = value;
                    this.OnPropertyChanged(new PropertyChangedEventArgs("PageNo"));
                }
            }
        }
        private string _Name;
        public string Name
        {
            get { return this._Name; }
            set
            {
                if (this._Name != value)
                {
                    this._Name = value;
                    this.OnPropertyChanged(new PropertyChangedEventArgs("Name"));
                }
            }
        }

        private string _FilePath;
        public string FilePath
        {
            get { return this._FilePath; }
            set
            {
                if (this._FilePath != value)
                {
                    this._FilePath = value;
                    this.OnPropertyChanged(new PropertyChangedEventArgs("FilePath"));
                }
            }
        }

        private int _PhotoId;
        public int PhotoId
        {
            get { return this._PhotoId; }
            set
            {
                if (this._PhotoId != value)
                {
                    this._PhotoId = value;
                    objPVDetails.MediaId = this._PhotoId;
                    this.OnPropertyChanged(new PropertyChangedEventArgs("PhotoId"));
                }
            }
        }

        private int _ImageDisplayTime;
        public int ImageDisplayTime
        {
            get { return this._ImageDisplayTime; }
            set
            {
                if (this._ImageDisplayTime != value)
                {
                    this._ImageDisplayTime = value;
                    objPVDetails.DisplayTime = this._ImageDisplayTime;
                    this.OnPropertyChanged(new PropertyChangedEventArgs("ImageDisplayTime"));
                }
            }
        }

        private int _ProcessedVideoId;
        public int ProcessedVideoId
        {
            get { return this._ProcessedVideoId; }
            set
            {
                if (this._ProcessedVideoId != value)
                {
                    this._ProcessedVideoId = value;
                    objPVDetails.ProcessedVideoId = this._ProcessedVideoId;
                    this.OnPropertyChanged(new PropertyChangedEventArgs("ProcessedVideoId"));
                }
            }
        }

        private int _MediaType;
        public int MediaType
        {
            get { return this._MediaType; }
            set
            {
                if (this._MediaType != value)
                {
                    this._MediaType = value;
                    objPVDetails.MediaType = this._MediaType;
                    this.OnPropertyChanged(new PropertyChangedEventArgs("MediaType"));
                }
            }
        }
        //private int _JoiningOrder;
        //public int JoiningOrder
        //{
        //    get { return this._JoiningOrder; }
        //    set
        //    {
        //        if (this._JoiningOrder != value)
        //        {
        //            this._JoiningOrder = value;
        //            objPVDetails.JoiningOrder = this._JoiningOrder;
        //            this.OnPropertyChanged(new PropertyChangedEventArgs("JoiningOrder"));
        //        }
        //    }
        //}

        private bool _IsGuestImage;
        public bool IsGuestImage
        {
            get { return this._IsGuestImage; }
            set
            {
                if (this._IsGuestImage != value)
                {
                    this._IsGuestImage = value;
                    this.OnPropertyChanged(new PropertyChangedEventArgs("IsGuestImage"));
                }
            }
        }
        /// <summary>
        /// Play button visibility
        /// </summary>
        private Visibility _playVisible;
        public Visibility PlayVisible
        {
            get
            {
                //return _playVisible; 
                if (MediaType == 602 || MediaType == 607)// Guest Video & Processed Video
                {
                    return Visibility.Visible;
                }
                else
                {
                    return Visibility.Collapsed;
                }
            }
            set
            {
                _playVisible = value;
            }
        }

        private string _DropVideoPath;
        /// <summary>
        ///Drop Image Caption
        /// </summary>
        public string DropVideoPath
        {
            get { return _DropVideoPath; }
            set { _DropVideoPath = value; }
        }

        public string slotTime { get; set; }
        public long videoLength { get; set; }
        public int videoStartTime { get; set; }
        public int videoEndTime { get; set; }
        //public double  InTime { get; set; }
        //public double OutTime { get; set; }
        public double InsertTime { get; set; }
        private Visibility _ProcessedVisible;
        public Visibility ProcessedVisible
        {
            get
            {
                //return _playVisible; 
                if (MediaType == 607)
                {
                    return Visibility.Visible;
                }
                else
                {
                    return Visibility.Hidden;
                }
            }
            set
            {
                _ProcessedVisible = value;
            }
        }

        public Visibility _processPlayVisible;
        public Visibility ProcessPlayVisible
        {
            get
            {
                //return _playVisible; 
                if (MediaType != 0)
                {
                    return Visibility.Visible;
                }
                else
                {
                    return Visibility.Hidden;
                }
            }
            set
            {
                _processPlayVisible = value;
            }
        }
        public string tooltip { get; set; }
    }
    //Template List Items Model
    public class TemplateListItems
    {
        private Visibility _CheckedBoxVisible;
        public Visibility CheckedBoxVisible
        {
            get
            {
                return _CheckedBoxVisible;
            }
            set
            {
                _CheckedBoxVisible = value;
            }
        }
        private long _Item_ID;
        public long Item_ID
        {
            get { return _Item_ID; }
            set { _Item_ID = value; }
        }
        private bool _IsChecked;
        public bool IsChecked
        {
            get
            {
                return _IsChecked;
            }
            set
            {
                _IsChecked = value;
            }
        }
        private string _DisplayName;
        public string DisplayName
        {
            get { return _DisplayName; }
            set { _DisplayName = value; }
        }
        private string _Name;
        public string Name
        {
            get { return _Name; }
            set { _Name = value; }
        }
        private string _FilePath;
        public string FilePath
        {
            get { return _FilePath; }
            set { _FilePath = value; }
        }
        private int _MediaType;
        public int MediaType
        {
            get { return this._MediaType; }
            set
            {
                this._MediaType = value;
            }
        }
        private long _Length;
        public long Length
        {
            get { return this._Length; }
            set
            {
                this._Length = value;
            }
        }
        private string _Tooltip;
        public string Tooltip
        {
            get { return this._Tooltip; }
            set { this._Tooltip = value; }
        }
        public int StartTime { get; set; }
        public int EndTime { get; set; }
        public int InsertTime { get; set; }
        public bool isActive { get; set; }
        public string GuestVideoPath { get; set; }
        public int LeftPositon { get; set; }
        public int TopPositon { get; set; }
    }
    public class VideoEffects
    {
        public string GetVideoEffectsXML(ProcessedVideoInfo obj, List<PanProperty> PanPropertyLst, List<TransitionProperty> TransitionPropertyLst, List<SlotProperty> borderslotlist, List<SlotProperty> logoSlotList)
        {
            string outXML = string.Empty;
            outXML += "<effects>";
            outXML += "<lightness>";
            outXML += obj.lightness;
            outXML += "</lightness>";
            outXML += "<saturation>";
            outXML += obj.saturation;
            outXML += "</saturation>";
            outXML += "<contrast>";
            outXML += obj.contrast;
            outXML += "</contrast>";
            outXML += "<darkness>";
            outXML += obj.darkness;
            outXML += "</darkness>";
            outXML += "<greyScale>";
            outXML += obj.greyScale.ToString();
            outXML += "</greyScale>";
            outXML += "<invert>";
            outXML += obj.invert.ToString();
            outXML += "</invert>";
            outXML += "<textLogo textLogoPosition=\"" + obj.textLogoPosition + "\" textfontName=\"" + obj.textfontName + "\" textfontColor=\"" + obj.textfontColor + "\" textfontSize=\"" + obj.textfontSize + "\" textfontStyle=\"" + obj.textfontStyle + "\">";
            outXML += obj.textLogo;
            outXML += "</textLogo>";
            //outXML += "<graphicLogo graphicLogoPosition=\"" + obj.graphicLogoPosition + "\">";
            //outXML += obj.graphicLogo;
            //outXML += "</graphicLogo>";
            outXML += "<zoom>";
            outXML += obj.zoom;
            outXML += "</zoom>";
            outXML += "<fadeInOut>";
            outXML += obj.fadeInOut;
            outXML += "</fadeInOut>";
            outXML += "<chroma chromaKeyColor=\"" + obj.chromaKeyColor + "\" chromaKeyBG=\"" + obj.chromaKeyBG + "\" >";
            outXML += obj.chroma.ToString();
            outXML += "</chroma>";
            outXML += "<audio amplify=\"" + obj.amplify + "\" equal1=\"" + obj.equal1 + "\" equal2=\"" + obj.equal2 + "\" equal3=\"" + obj.equal3 + "\" equal4=\"" + obj.equal4 + "\" equal5=\"" + obj.equal5 + "\" equal6=\"" + obj.equal6 + "\">";
            outXML += obj.audio;
            outXML += "</audio>";
            if (PanPropertyLst != null && PanPropertyLst.Count > 0)
            {
                outXML += "<Pans>";
                foreach (PanProperty pp in PanPropertyLst)
                {
                    outXML += "<Pan>";
                    outXML += "<PanID>" + pp.PanID + "</PanID>";
                    outXML += "<PanStartTime>" + pp.PanStartTime + "</PanStartTime>";
                    outXML += "<PanStopTime>" + pp.PanStopTime + "</PanStopTime>";
                    outXML += "<PanSourceLeft>" + pp.PanSourceLeft + "</PanSourceLeft>";
                    outXML += "<PanSourceTop>" + pp.PanSourceTop + "</PanSourceTop>";
                    outXML += "<PanSourceWidth>" + pp.PanSourceWidth + "</PanSourceWidth>";
                    outXML += "<PanSourceHeight>" + pp.PanSourceHeight + "</PanSourceHeight>";
                    outXML += "<PanDestLeft>" + pp.PanDestLeft + "</PanDestLeft>";
                    outXML += "<PanDestTop>" + pp.PanDestTop + "</PanDestTop>";
                    outXML += "<PanDestWidth>" + pp.PanDestWidth + "</PanDestWidth>";
                    outXML += "<PanDestHeight>" + pp.PanDestHeight + "</PanDestHeight>";
                    outXML += "</Pan>";
                }
                outXML += "</Pans>";
            }
            else
            {
                outXML += "<Pans>";
                outXML += "</Pans>";
            }
            if (TransitionPropertyLst != null && TransitionPropertyLst.Count > 0)
            {
                outXML += "<Transitions>";
                foreach (TransitionProperty tp in TransitionPropertyLst)
                {
                    outXML += "<Transition>";
                    outXML += "<ID>" + tp.ID + "</ID>";
                    outXML += "<TransitionID>" + tp.TransitionID + "</TransitionID>";
                    outXML += "<TransitionStartTime>" + tp.TransitionStartTime + "</TransitionStartTime>";
                    outXML += "<TransitionStopTime>" + tp.TransitionStopTime + "</TransitionStopTime>";
                    outXML += "<TransitionName>" + tp.TransitionName + "</TransitionName>";
                    outXML += "</Transition>";
                }
                outXML += "</Transitions>";
            }
            else
            {
                outXML += "<Transitions>";
                outXML += "</Transitions>";
            }
            if (borderslotlist != null && borderslotlist.Count > 0)
            {
                outXML += "<Borders>";
                foreach (SlotProperty bp in borderslotlist)
                {
                    outXML += "<Border>";
                    outXML += "<ItemID>" + bp.ItemID + "</ItemID>";
                    outXML += "<Name>" + Path.GetFileName(bp.FilePath) + "</Name>";
                    outXML += "<StartTime>" + bp.StartTime + "</StartTime>";
                    outXML += "<StopTime>" + bp.StopTime + "</StopTime>";
                    outXML += "</Border>";
                }
                outXML += "</Borders>";
            }
            else
            {
                outXML += "<Borders>";
                outXML += "</Borders>";
            }
            if (logoSlotList != null && logoSlotList.Count > 0)
            {
                outXML += "<graphics>";
                foreach (SlotProperty lsp in logoSlotList)
                {
                    outXML += "<graphic>";
                    outXML += "<ItemID>" + lsp.ItemID + "</ItemID>";
                    outXML += "<Name>" + Path.GetFileName(lsp.FilePath) + "</Name>";
                    outXML += "<StartTime>" + lsp.StartTime + "</StartTime>";
                    outXML += "<StopTime>" + lsp.StopTime + "</StopTime>";
                    outXML += "<PositionTop>" + lsp.Top + "</PositionTop>";
                    outXML += "<PositionLeft>" + lsp.Left + "</PositionLeft>";
                    outXML += "</graphic>";
                }
                outXML += "</graphics>";
            }
            else
            {
                outXML += "<graphics>";
                outXML += "</graphics>";
            }
            outXML += "</effects>";
            return outXML;
        }
    }
    public class PanProperty
    {
        public int PanID { get; set; }
        public int PanStartTime { get; set; }
        public int PanStopTime { get; set; }
        public int PanSourceLeft { get; set; }
        public int PanSourceTop { get; set; }
        public int PanSourceWidth { get; set; }
        public int PanSourceHeight { get; set; }
        public int PanDestLeft { get; set; }
        public int PanDestTop { get; set; }
        public int PanDestWidth { get; set; }
        public int PanDestHeight { get; set; }
    }

    public class TransitionProperty
    {
        public int ID { get; set; }
        public int TransitionID { get; set; }
        public int TransitionStartTime { get; set; }
        public int TransitionStopTime { get; set; }
        public string TransitionName { get; set; }
    }
    public class SlotProperty
    {
        public int ID { get; set; }
        public long ItemID { get; set; }
        public int StartTime { get; set; }
        public int StopTime { get; set; }
        public string FilePath { get; set; }
        public string Name { get; set; }
        public int Left { get; set; }
        public int Top { get; set; }
        public string settings { get; set; }
    }
    public class PhotoLayering
    {
        public double canvasLeft { get; set; }
        public double canvasTop { get; set; }
        public double zoomfactor { get; set; }
        public string scaleCenterX { get; set; }
        public string scaleCenterY { get; set; }
    }
}
