﻿using DigiAuditLogger;
using FrameworkHelper;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace DigiPhoto
{
    /// <summary>
    /// Interaction logic for FullClientPreview.xaml
    /// </summary>
    public partial class FullClientPreview : Window
    {
        public int _currentImageId = 0;
        ///FileInfo[] files = null;
        ///int index;
        public FullClientPreview()
        {
            InitializeComponent();
        }

        private void btnNext_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (RobotImageLoader.GroupImages != null && RobotImageLoader.GroupImages.Count != 0)
                {
                    var myitem = RobotImageLoader.GroupImages.OrderByDescending(o => o.PhotoId).LastOrDefault(o => o.PhotoId > _currentImageId);

                    if (myitem != null)
                    {
                        LoadImage(myitem);
                        var mainitem = RobotImageLoader.PrintImages.FirstOrDefault(t => t.PhotoId == _currentImageId);
                        if (mainitem == null)
                        {
                            imgprintgroup.Source = new BitmapImage(new Uri(@"/images/print-group.png", UriKind.Relative));
                            //imgselectgroup.Source = new BitmapImage(new Uri(@"/images/Select-group.png", UriKind.Relative));
                            imgselectgroup.Source = new BitmapImage(new Uri(@"/images/RemoveGroupN.png", UriKind.Relative));// Added By KCB on 26 JUL 2018 for image selection
                        }
                        else
                        {
                            imgprintgroup.Source = new BitmapImage(new Uri(@"/images/print-accept.png", UriKind.Relative));
                            //imgselectgroup.Source = new BitmapImage(new Uri(@"/images/view-accept.png", UriKind.Relative));
                            imgselectgroup.Source = new BitmapImage(new Uri(@"/images/view-accept.png", UriKind.Relative));// Added By KCB on 26 JUL 2018 for image selection
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ///////////////Entry in error log for scope error
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }

        private void btnPrev_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (RobotImageLoader.GroupImages != null && RobotImageLoader.GroupImages.Count != 0)
                {
                    var myitem = RobotImageLoader.GroupImages.OrderByDescending(o => o.PhotoId).FirstOrDefault(o => o.PhotoId < _currentImageId);
                    if (myitem != null)
                    {
                        LoadImage(myitem);
                        var mainitem = RobotImageLoader.PrintImages.FirstOrDefault(t => t.PhotoId == _currentImageId);
                        if (mainitem == null)
                        {
                            imgprintgroup.Source = new BitmapImage(new Uri(@"/images/print-group.png", UriKind.Relative));
                            //imgselectgroup.Source = new BitmapImage(new Uri(@"/images/Select-group.png", UriKind.Relative));//remove-N
                            imgselectgroup.Source = new BitmapImage(new Uri(@"/images/RemoveGroupN.png", UriKind.Relative));// Added By KCB on 26 JUL 2018 for image selection
                        }
                        else
                        {
                            imgprintgroup.Source = new BitmapImage(new Uri(@"/images/print-accept.png", UriKind.Relative));
                            //imgselectgroup.Source = new BitmapImage(new Uri(@"/images/view-accept.png", UriKind.Relative));
                            imgselectgroup.Source = new BitmapImage(new Uri(@"/images/view-accept.png", UriKind.Relative));// Added By KCB on 26 JUL 2018 for image selection
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ///////////////Entry in error log for scope error
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }
        private childItem FindVisualChild<childItem>(DependencyObject obj)
                                                where childItem : DependencyObject
        {
            for (int i = 0; i < VisualTreeHelper.GetChildrenCount(obj); i++)
            {
                DependencyObject child = VisualTreeHelper.GetChild(obj, i);
                if (child != null && child is childItem)
                    return (childItem)child;
                else
                {
                    childItem childOfChild = FindVisualChild<childItem>(child);
                    if (childOfChild != null)
                        return childOfChild;
                }
            }
            return null;
        }
        private void btnPrintGroup_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                LstMyItems curItem;
                ////////finding the image from the list for selection and preview
                var senderImage = (System.Windows.Controls.Image)btnPrintGroup.Content;
                curItem = RobotImageLoader.GroupImages.First(t => t.PhotoId == _currentImageId);
                SearchResult window = System.Windows.Application.Current.Windows.Cast<Window>().Where(wnd => wnd.Title == "View/Order Station").Cast<SearchResult>().FirstOrDefault();

                if (window == null) return;

                ////////Checking whether selected item already is in Print group or not               
                var mainitem = RobotImageLoader.PrintImages.FirstOrDefault(t => t.PhotoId == _currentImageId);
                if (mainitem == null)
                {
                    ////////if Item not exists adding item to printgroup collection
                    var myitem = new LstMyItems();
                    myitem = curItem;


                    var listItems = window.lstImages.Items;
                    foreach (LstMyItems item in listItems)
                    {
                        if (item.PhotoId == curItem.PhotoId)
                        {

                            window.lstImages.SelectedItem = item;
                            break;
                        }
                    }

                    var listBoxItem = (ListBoxItem)window.lstImages.ItemContainerGenerator.ContainerFromItem(window.lstImages.SelectedItem);
                    /////list image replace
                    if (listBoxItem != null)
                    {
                        myitem.FilePath = curItem.FilePath;
                        myitem.PrintGroup = new BitmapImage(new Uri(@"/images/print-accept.png", UriKind.Relative));
                        RobotImageLoader.PrintImages.Add(curItem);

                        senderImage.Source = new BitmapImage(new Uri(@"/images/print-accept.png", UriKind.Relative));
                        curItem.PrintGroup = new BitmapImage(new Uri(@"/images/print-accept.png", UriKind.Relative));

                        var myContentPresenter = FindVisualChild<ContentPresenter>(listBoxItem);
                        // Finding textBlock from the DataTemplate that is set on that ContentPresenter
                        DataTemplate myDataTemplate = myContentPresenter.ContentTemplate;

                        var myTextBlock = (Grid)myDataTemplate.FindName("grdMain", myContentPresenter);
                        ((System.Windows.Controls.Image)(((System.Windows.Controls.Button)(((System.Windows.Controls.Panel)((((Grid)(myTextBlock.FindName("Printbtns")))))).Children[0])).Content)).Source = null;
                        ((System.Windows.Controls.Image)(((System.Windows.Controls.Button)(((System.Windows.Controls.Panel)((((Grid)(myTextBlock.FindName("Printbtns")))))).Children[0])).Content)).Source = new BitmapImage(new Uri(@"/images/print-accept.png", UriKind.Relative));
                        ((System.Windows.Controls.Image)(((System.Windows.Controls.Button)(((System.Windows.Controls.Panel)((((Grid)(myTextBlock.FindName("Printbtns")))))).Children[0])).Content)).UpdateLayout();
                        window.imgprintgroup.Source = new BitmapImage(new Uri(@"/images/print-accept.png", UriKind.Relative));
                    }
                }
                else
                {
                    var listItems = window.lstImages.Items;
                    foreach (LstMyItems item in listItems)
                    {
                        if (item.PhotoId == curItem.PhotoId)
                        {

                            window.lstImages.SelectedItem = item;
                            break;
                        }
                    }

                    var listBoxItem = (ListBoxItem)window.lstImages.ItemContainerGenerator.ContainerFromItem(window.lstImages.SelectedItem);
                    /////list image replace
                    if (listBoxItem != null)
                    {
                        RobotImageLoader.PrintImages.Remove(mainitem);
                        senderImage.Source = new BitmapImage(new Uri(@"/images/print-group.png", UriKind.Relative));
                        curItem.PrintGroup = new BitmapImage(new Uri(@"/images/print-group.png", UriKind.Relative));
                        var myContentPresenter = FindVisualChild<ContentPresenter>(listBoxItem);
                        // Finding textBlock from the DataTemplate that is set on that ContentPresenter
                        DataTemplate myDataTemplate = myContentPresenter.ContentTemplate;
                        var myTextBlock = (Grid)myDataTemplate.FindName("grdMain", myContentPresenter);
                        ((System.Windows.Controls.Image)(((System.Windows.Controls.Button)(((System.Windows.Controls.Panel)((((Grid)(myTextBlock.FindName("Printbtns")))))).Children[0])).Content)).Source = null;
                        ((System.Windows.Controls.Image)(((System.Windows.Controls.Button)(((System.Windows.Controls.Panel)((((Grid)(myTextBlock.FindName("Printbtns")))))).Children[0])).Content)).Source = new BitmapImage(new Uri(@"/images/print-group.png", UriKind.Relative));
                        ((System.Windows.Controls.Image)(((System.Windows.Controls.Button)(((System.Windows.Controls.Panel)((((Grid)(myTextBlock.FindName("Printbtns")))))).Children[0])).Content)).UpdateLayout();
                        window.imgprintgroup.Source = new BitmapImage(new Uri(@"/images/print-group.png", UriKind.Relative));
                    }

                }
                if (window.vwGroup.Text == "View All")
                {
                    var count = (from groupImages in RobotImageLoader.GroupImages
                                 join printImages in RobotImageLoader.PrintImages
                                 on groupImages.PhotoId equals printImages.PhotoId
                                 select groupImages).Count();


                    window.txtSelectImages.Text = "Selected : " + count.ToString() + "/" + RobotImageLoader.GroupImages.Count.ToString();
                    if (RobotImageLoader.GroupImages.Count == RobotImageLoader.PrintImages.Count)
                        window.chkSelectAll.IsChecked = true;
                    else window.chkSelectAll.IsChecked = false;
                }
                else if (window.vwGroup.Text == "View Group")
                {
                    window.txtSelectImages.Visibility = Visibility.Collapsed;
                    window.txtSelectedImages.Visibility = Visibility.Visible;
                    window.txtSelectedImages.Text = "Grouped : " + RobotImageLoader.GroupImages.Count;
                }
            }
            catch (Exception ex)
            {
                ///////////////Entry in error log for scope error
                ErrorHandler.ErrorHandler.LogError(ex);
            }
        }
        //today  
        private void btnSelectGroup_Click_Old(object sender, RoutedEventArgs e)
        {
            {
                try
                {
                    LstMyItems curItem;
                    ////////finding the image from the list for selection and preview
                    var senderImage = (System.Windows.Controls.Image)btnSelectGroup.Content;
                    curItem = RobotImageLoader.GroupImages.First(t => t.PhotoId == _currentImageId);
                    SearchResult window = System.Windows.Application.Current.Windows.Cast<Window>().Where(wnd => wnd.Title == "View/Order Station").Cast<SearchResult>().FirstOrDefault();

                    if (window == null) return;

                    ////////Checking whether selected item already is in Print group or not               
                    var mainitem = RobotImageLoader.PrintImages.FirstOrDefault(t => t.PhotoId == _currentImageId);
                    if (mainitem == null)
                    {
                        ////////if Item not exists adding item to printgroup collection
                        var myitem = new LstMyItems();
                        myitem = curItem;


                        var listItems = window.lstImages.Items;
                        foreach (LstMyItems item in listItems)
                        {
                            if (item.PhotoId == curItem.PhotoId)
                            {
                                //senderImage.Source = new BitmapImage(new Uri(@"/images/view-accept.png", UriKind.Relative));
                                window.lstImages.SelectedItem = item;
                                break;
                            }
                        }

                        var listBoxItem = (ListBoxItem)window.lstImages.ItemContainerGenerator.ContainerFromItem(window.lstImages.SelectedItem);
                        /////list image replace
                        if (listBoxItem != null)
                        {
                            myitem.FilePath = curItem.FilePath;
                            myitem.PrintGroup = new BitmapImage(new Uri(@"/images/view-accept.png", UriKind.Relative));
                            RobotImageLoader.PrintImages.Add(curItem);

                            senderImage.Source = new BitmapImage(new Uri(@"/images/view-accept.png", UriKind.Relative));
                            curItem.PrintGroup = new BitmapImage(new Uri(@"/images/view-accept.png", UriKind.Relative));

                            var myContentPresenter = FindVisualChild<ContentPresenter>(listBoxItem);
                            // Finding textBlock from the DataTemplate that is set on that ContentPresenter
                            DataTemplate myDataTemplate = myContentPresenter.ContentTemplate;

                            var myTextBlock = (Grid)myDataTemplate.FindName("grdMain", myContentPresenter);
                            ((System.Windows.Controls.Image)(((System.Windows.Controls.Button)(((System.Windows.Controls.Panel)((((Grid)(myTextBlock.FindName("Printbtns")))))).Children[0])).Content)).Source = null;
                            ((System.Windows.Controls.Image)(((System.Windows.Controls.Button)(((System.Windows.Controls.Panel)((((Grid)(myTextBlock.FindName("Printbtns")))))).Children[0])).Content)).Source = new BitmapImage(new Uri(@"/images/print-accept.png", UriKind.Relative));//for fullclient photo
                            ((System.Windows.Controls.Image)(((System.Windows.Controls.Button)(((System.Windows.Controls.Panel)((((Grid)(myTextBlock.FindName("Printbtns")))))).Children[0])).Content)).UpdateLayout();
                            window.imgprintgroup.Source = new BitmapImage(new Uri(@"/images/view-accept.png", UriKind.Relative));
                        }
                    }
                    else
                    {
                        var listItems = window.lstImages.Items;
                        foreach (LstMyItems item in listItems)
                        {
                            if (item.PhotoId == curItem.PhotoId)
                            {

                                window.lstImages.SelectedItem = item;

                                break;
                            }
                        }

                        var listBoxItem = (ListBoxItem)window.lstImages.ItemContainerGenerator.ContainerFromItem(window.lstImages.SelectedItem);
                        ///list image replace
                        if (listBoxItem != null)
                        {
                            RobotImageLoader.PrintImages.Remove(mainitem);

                            //senderImage.Source = new BitmapImage(new Uri(@"/images/RemoveGroupN.png", UriKind.Relative)); //for flip page
                            senderImage.Source = new BitmapImage(new Uri(@"/images/Select-group.png", UriKind.Relative)); //for flip page
                            
                            curItem.PrintGroup = new BitmapImage(new Uri(@"/images/Select-group.png", UriKind.Relative));//print-group
                            var myContentPresenter = FindVisualChild<ContentPresenter>(listBoxItem);
                            // Finding textBlock from the DataTemplate that is set on that ContentPresenter
                            DataTemplate myDataTemplate = myContentPresenter.ContentTemplate;
                            var myTextBlock = (Grid)myDataTemplate.FindName("grdMain", myContentPresenter);
                            ((System.Windows.Controls.Image)(((System.Windows.Controls.Button)(((System.Windows.Controls.Panel)((((Grid)(myTextBlock.FindName("Printbtns")))))).Children[0])).Content)).Source = null;
                            ((System.Windows.Controls.Image)(((System.Windows.Controls.Button)(((System.Windows.Controls.Panel)((((Grid)(myTextBlock.FindName("Printbtns")))))).Children[0])).Content)).Source = new BitmapImage(new Uri(@"/images/print-group.png", UriKind.Relative));// for fullclinet not to touch
                            ((System.Windows.Controls.Image)(((System.Windows.Controls.Button)(((System.Windows.Controls.Panel)((((Grid)(myTextBlock.FindName("Printbtns")))))).Children[0])).Content)).UpdateLayout();
                            //window.imgprintgroup.Source = new BitmapImage(new Uri(@"/images/RemoveGroupN.png", UriKind.Relative));
                            window.imgprintgroup.Source = new BitmapImage(new Uri(@"/images/Select-group.png", UriKind.Relative));
                        }



                    }
                    if (window.vwGroup.Text == "View All")
                    {
                        var count = (from groupImages in RobotImageLoader.GroupImages
                                     join printImages in RobotImageLoader.PrintImages
                                     on groupImages.PhotoId equals printImages.PhotoId
                                     select groupImages).Count();


                        window.txtSelectImages.Text = "Selected : " + count.ToString() + "/" + RobotImageLoader.GroupImages.Count.ToString();
                        if (RobotImageLoader.GroupImages.Count == RobotImageLoader.PrintImages.Count)
                            window.chkSelectAll.IsChecked = true;
                        else window.chkSelectAll.IsChecked = false;
                    }
                    else
                        if (window.vwGroup.Text == "View Group")
                        {
                            window.txtSelectImages.Visibility = Visibility.Collapsed;
                            window.txtSelectedImages.Visibility = Visibility.Visible;
                            window.txtSelectedImages.Text = "Grouped : " + RobotImageLoader.GroupImages.Count;
                        }
                }
                catch (Exception ex)
                {
                    ///////////////Entry in error log for scope error
                    ErrorHandler.ErrorHandler.LogError(ex);
                }
            }
        }


        //end


        public void LoadImage(LstMyItems myitem)
        {
            _currentImageId = myitem.PhotoId;
            string imgpath = myitem.FilePath.Replace("Thumbnails", "Thumbnails_Big");
            imgPreview.Source = CommonUtility.GetImageFromPath(imgpath);
            imgPreview.Visibility = System.Windows.Visibility.Visible;
            txtblkImageId.Text = myitem.Name;
            SearchResult window = null;
            foreach (Window wnd in System.Windows.Application.Current.Windows)
            {
                if (wnd.Title == "View/Order Station")
                {
                    window = (SearchResult)wnd;
                    break;
                }
            }
            window.lstImages.SelectedItem = myitem;
            if (myitem != null && !String.IsNullOrEmpty(myitem.Name))
                AuditLog.AddUserLog(Common.LoginUser.UserId, (int)FrameworkHelper.ActionType.PreviewPhoto, "Show Preview of " + ((DigiPhoto.LstMyItems)(myitem)).Name.ToString() + " image.", ((DigiPhoto.LstMyItems)(myitem)).PhotoId);
        }
        public int FilpFrom = 0;
        private void CloseWindow()
        {
            
            if (FilpFrom == 1)
            {
                SearchResult window = System.Windows.Application.Current.Windows.Cast<Window>().Where(wnd => wnd.Title == "View/Order Station").Cast<SearchResult>().FirstOrDefault();
                if (window == null) return;

                if ((imgPreview.Visibility == Visibility.Visible) && window.isSingleScreenPreview)
                {
                    var grouptestR = new TransformGroup();
                    grouptestR.Children.Add(new RotateTransform(0));
                    imgPreview.LayoutTransform = grouptestR;

                    var group = new TransformGroup();
                    group.Children.Add(new RotateTransform(0));
                    window.ContentContainer.LayoutTransform = group;
                    window.btnchkpreview.IsChecked = false;
                    window.btnchkthumbpreview.IsChecked = false;
                    window.PreviewPhoto();
                }
                if (clientWin == null)
                {
                    foreach (Window wnd in Application.Current.Windows)
                    {
                        if (wnd.Title == "ClientView")
                        {
                            clientWin = (ClientView)wnd;
                            clientWin.WindowState = System.Windows.WindowState.Minimized;
                            //clientWin.Close();
                            break;
                        }
                    }
                }
                else
                {
                    clientWin.WindowState = System.Windows.WindowState.Minimized;
                    //clientWin.Close();
                }
                window.WindowState = WindowState.Maximized;
            }
            else if (FilpFrom == 2)
            {
                MainWindow mainWin = null;
                foreach (Window wnd in System.Windows.Application.Current.Windows)
                {
                    if (wnd.Title == "DigiMain")
                    {
                        mainWin = (MainWindow)wnd;
                        break;
                    }
                }
                if (mainWin != null)
                {
                    if ((imgPreview.Visibility == Visibility.Visible) && mainWin.isSingleScreenPreview)
                    {
                        TransformGroup grouptestR = new TransformGroup();
                        grouptestR.Children.Add(new RotateTransform(0));
                        imgPreview.LayoutTransform = grouptestR;
                        TransformGroup group = new TransformGroup();
                        group.Children.Add(new RotateTransform(0));
                        mainWin.ContentContainer.LayoutTransform = group;
                        mainWin.btnchkpreview.IsChecked = false;
                    }
                }
            }
            this.Close();
            //this.WindowState = System.Windows.WindowState.Minimized;
        }

        ClientView clientWin;
        private void btnClose_Click(object sender, RoutedEventArgs e)
        {
            CloseWindow();
            //if (clientWin == null)
            //{
            //    foreach (Window wnd in Application.Current.Windows)
            //    {
            //        if (wnd.Title == "ClientView")
            //        {
            //            clientWin = (ClientView)wnd;
            //            if (clientWin.IsRotationRequired)
            //            {
            //                clientWin.imgNext.Visibility = System.Windows.Visibility.Visible;
            //                clientWin.testR.Fill = null;
            //                clientWin.testR.Visibility = System.Windows.Visibility.Collapsed;
            //                clientWin.stkPrevNext.Visibility = Visibility.Visible;
            //                clientWin.stkPrint.Visibility = System.Windows.Visibility.Visible;
            //                clientWin.btnMinimize.Visibility = Visibility.Visible;
            //                TransformGroup grouptestR = new TransformGroup();
            //                grouptestR.Children.Add(new RotateTransform(clientWin.RotationAngle));
            //                grouptestR.Children.Add(new TranslateTransform(clientWin.img12.ActualWidth, clientWin.img12.ActualHeight));
            //                clientWin.img12.LayoutTransform = grouptestR;
            //                if (RobotImageLoader.GroupImages != null && RobotImageLoader.GroupImages.Count != 0)
            //                {
            //                    var myitem = RobotImageLoader.GroupImages.OrderByDescending(o => o.PhotoId).LastOrDefault(o => o.PhotoId == _currentImageId);

            //                    if (myitem != null)
            //                    {
            //                        clientWin.LoadImage(myitem);
            //                        var mainitem = RobotImageLoader.PrintImages.FirstOrDefault(t => t.PhotoId == _currentImageId);
            //                        if (mainitem == null)
            //                        {
            //                            clientWin.imgprintgroup.Source = new BitmapImage(new Uri(@"/images/print-group.png", UriKind.Relative));
            //                        }
            //                        else
            //                        {
            //                            clientWin.imgprintgroup.Source = new BitmapImage(new Uri(@"/images/print-accept.png", UriKind.Relative));
            //                        }
            //                    }
            //                }
            //            }
            //            else
            //            {
            //                clientWin.IsRotationRequired = false;
            //                clientWin.RotationAngle = 0;
            //            }
            //            clientWin.WindowState = WindowState.Maximized;
            //            break;
            //        }
            //    }
            //}
            //this.Close();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            //this.WindowStartupLocation = WindowStartupLocation.Manual;
            //WindowState = WindowState.Maximized;
        }

        //private void funct()
        //{
        //    clientWin.imgNext.Visibility = System.Windows.Visibility.Visible;
        //    clientWin.testR.Fill = null;
        //    clientWin.testR.Visibility = System.Windows.Visibility.Collapsed;
        //    clientWin.stkPrevNext.Visibility = Visibility.Visible;
        //    clientWin.stkPrint.Visibility = System.Windows.Visibility.Visible;
        //    clientWin.btnMinimize.Visibility = Visibility.Visible;
        //        clientWin.LoadImage(selectedItem);
        //        if (RobotImageLoader.PrintImages.Where(o => o.PhotoId == selectedItem.PhotoId).FirstOrDefault() != null)
        //        {
        //            clientWin.imgprintgroup.Source = new BitmapImage(new Uri(@"/images/print-accept.png", UriKind.Relative));

        //        }
        //        else
        //        {
        //            clientWin.imgprintgroup.Source = new BitmapImage(new Uri(@"/images/print-group.png", UriKind.Relative));
        //        }
        //        txtMainImage.Text = selectedItem.Name;
        //        lstImages.SelectedItem = selectedItem;
        //        clientWin._currentImageId = selectedItem.PhotoId;

        //    AngleValue += 720;
        //    AngleValue %= 360;
        //    TransformGroup group = new TransformGroup();
        //    group.Children.Add(new RotateTransform(AngleValue));
        //    group.Children.Add(new TranslateTransform(this.ContentContainer.ActualWidth, this.ContentContainer.ActualHeight));
        //    ContentContainer.LayoutTransform = group;

        //    TransformGroup grouptestR = new TransformGroup();
        //    grouptestR.Children.Add(new RotateTransform(clientWin.RotationAngle));
        //    grouptestR.Children.Add(new TranslateTransform(clientWin.img12.ActualWidth, clientWin.img12.ActualHeight));
        //    clientWin.img12.LayoutTransform = grouptestR;

        //    if (clientWin != null)
        //    {
        //        //clientWin._currentImageId = _currentImageId;                                         
        //        clientWin.WindowState = System.Windows.WindowState.Maximized;
        //    }
        //    clientWin.IsRotationRequired = true;
        //    clientWin.RotationAngle = AngleValue;
        //}

        /// <summary>
        /// Added By KCB on 26 JUL 2018 for image selection
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSelectGroup_Click(object sender, RoutedEventArgs e)
        {
            {
                try
                {
                    LstMyItems curItem;
                    ////////finding the image from the list for selection and preview
                    var senderImage = (System.Windows.Controls.Image)btnSelectGroup.Content;
                    curItem = RobotImageLoader.GroupImages.First(t => t.PhotoId == _currentImageId);
                    SearchResult window = System.Windows.Application.Current.Windows.Cast<Window>().Where(wnd => wnd.Title == "View/Order Station").Cast<SearchResult>().FirstOrDefault();

                    if (window == null) return;

                    ////////Checking whether selected item already is in Print group or not               
                    var mainitem = RobotImageLoader.PrintImages.FirstOrDefault(t => t.PhotoId == _currentImageId);
                    if (mainitem == null)
                    {
                        ////////if Item not exists adding item to printgroup collection
                        var myitem = new LstMyItems();
                        myitem = curItem;


                        var listItems = window.lstImages.Items;
                        foreach (LstMyItems item in listItems)
                        {
                            if (item.PhotoId == curItem.PhotoId)
                            {
                                //senderImage.Source = new BitmapImage(new Uri(@"/images/view-accept.png", UriKind.Relative));
                                window.lstImages.SelectedItem = item;
                                break;
                            }
                        }

                        var listBoxItem = (ListBoxItem)window.lstImages.ItemContainerGenerator.ContainerFromItem(window.lstImages.SelectedItem);
                        /////list image replace
                        if (listBoxItem != null)
                        {
                            myitem.FilePath = curItem.FilePath;
                            myitem.SelectGroup = new BitmapImage(new Uri(@"/images/view-accept.png", UriKind.Relative));
                            RobotImageLoader.PrintImages.Add(curItem);

                            senderImage.Source = new BitmapImage(new Uri(@"/images/view-accept.png", UriKind.Relative));
                            curItem.SelectGroup = new BitmapImage(new Uri(@"/images/view-accept.png", UriKind.Relative));

                            var myContentPresenter = FindVisualChild<ContentPresenter>(listBoxItem);
                            // Finding textBlock from the DataTemplate that is set on that ContentPresenter
                            DataTemplate myDataTemplate = myContentPresenter.ContentTemplate;

                            var myTextBlock = (Grid)myDataTemplate.FindName("grdMain", myContentPresenter);
                            ((System.Windows.Controls.Image)(((System.Windows.Controls.Button)(((System.Windows.Controls.Panel)((((Grid)(myTextBlock.FindName("Printbtns")))))).Children[0])).Content)).Source = null;
                            ((System.Windows.Controls.Image)(((System.Windows.Controls.Button)(((System.Windows.Controls.Panel)((((Grid)(myTextBlock.FindName("Printbtns")))))).Children[0])).Content)).Source = new BitmapImage(new Uri(@"/images/print-accept.png", UriKind.Relative));//for fullclient photo
                            ((System.Windows.Controls.Image)(((System.Windows.Controls.Button)(((System.Windows.Controls.Panel)((((Grid)(myTextBlock.FindName("Printbtns")))))).Children[0])).Content)).UpdateLayout();
                            window.imgprintgroup.Source = new BitmapImage(new Uri(@"/images/view-accept.png", UriKind.Relative));
                        }
                    }
                    else
                    {
                        var listItems = window.lstImages.Items;
                        foreach (LstMyItems item in listItems)
                        {
                            if (item.PhotoId == curItem.PhotoId)
                            {

                                window.lstImages.SelectedItem = item;

                                break;
                            }
                        }

                        var listBoxItem = (ListBoxItem)window.lstImages.ItemContainerGenerator.ContainerFromItem(window.lstImages.SelectedItem);
                        ///list image replace
                        if (listBoxItem != null)
                        {
                            RobotImageLoader.PrintImages.Remove(mainitem);

                            senderImage.Source = new BitmapImage(new Uri(@"/images/RemoveGroupN.png", UriKind.Relative)); //for flip page
                            curItem.SelectGroup = new BitmapImage(new Uri(@"/images/Select-group.png", UriKind.Relative));//print-group
                            var myContentPresenter = FindVisualChild<ContentPresenter>(listBoxItem);
                            // Finding textBlock from the DataTemplate that is set on that ContentPresenter
                            DataTemplate myDataTemplate = myContentPresenter.ContentTemplate;
                            var myTextBlock = (Grid)myDataTemplate.FindName("grdMain", myContentPresenter);
                            ((System.Windows.Controls.Image)(((System.Windows.Controls.Button)(((System.Windows.Controls.Panel)((((Grid)(myTextBlock.FindName("Printbtns")))))).Children[0])).Content)).Source = null;
                            ((System.Windows.Controls.Image)(((System.Windows.Controls.Button)(((System.Windows.Controls.Panel)((((Grid)(myTextBlock.FindName("Printbtns")))))).Children[0])).Content)).Source = new BitmapImage(new Uri(@"/images/print-group.png", UriKind.Relative));// for fullclinet not to touch
                            ((System.Windows.Controls.Image)(((System.Windows.Controls.Button)(((System.Windows.Controls.Panel)((((Grid)(myTextBlock.FindName("Printbtns")))))).Children[0])).Content)).UpdateLayout();
                            window.imgprintgroup.Source = new BitmapImage(new Uri(@"/images/RemoveGroupN.png", UriKind.Relative));
                        }



                    }
                    if (window.vwGroup.Text == "View All")
                    {
                        var count = (from groupImages in RobotImageLoader.GroupImages
                                     join printImages in RobotImageLoader.PrintImages
                                     on groupImages.PhotoId equals printImages.PhotoId
                                     select groupImages).Count();


                        window.txtSelectImages.Text = "Selected : " + count.ToString() + "/" + RobotImageLoader.GroupImages.Count.ToString();
                        if (RobotImageLoader.GroupImages.Count == RobotImageLoader.PrintImages.Count)
                            window.chkSelectAll.IsChecked = true;
                        else window.chkSelectAll.IsChecked = false;
                    }
                    else
                    if (window.vwGroup.Text == "View Group")
                    {
                        window.txtSelectImages.Visibility = Visibility.Collapsed;
                        window.txtSelectedImages.Visibility = Visibility.Visible;
                        window.txtSelectedImages.Text = "Grouped : " + RobotImageLoader.GroupImages.Count;
                    }
                }
                catch (Exception ex)
                {
                    ///////////////Entry in error log for scope error
                    ErrorHandler.ErrorHandler.LogError(ex);
                }
            }
        }
    }
}
