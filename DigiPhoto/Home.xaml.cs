﻿using System;
using System.Linq;
using System.Windows;
using DigiPhoto.Common;
using DigiPhoto.Manage;
using DigiAuditLogger;
using System.IO;
using DigiPhoto.IMIX.Business;
using DigiPhoto.IMIX.Model;
using System.Collections.Generic;
using DigiPhoto.DataLayer;
using FrameworkHelper;
using System.Xml.Serialization;
using System.Text;
using System.Security.Cryptography;
using System.Net;
namespace DigiPhoto
{
    /// <summary>
    /// Interaction logic for home.xaml
    /// </summary>
    /// 
    public partial class Home : Window
    {
        #region Variables
        Int64 TotalCount8810 = 0;
        Int64 TotalCount6850 = 0;
        Int64 TotalCount6900 = 0;
        List<ImixPOSDetail> lstPosDetail = new List<ImixPOSDetail>();
        List<Printer6850> lst6850Printer = new List<Printer6850>();
        List<Printer8810> lst8810Printer = new List<Printer8810>();
        #endregion

        /// <summary>
        /// Initializes a new instance of the <see cref="Home"/> class.
        /// </summary>
        public Home()
        {
            InitializeComponent();
            txbUserName.Text = LoginUser.UserName;
            txbStoreName.Text = LoginUser.StoreName;
            RobotImageLoader.IsZeroSearchNeeded = false;
            RobotImageLoader.StartIndex = 0;
            RobotImageLoader.StopIndex = 0;
            CtrlOpeningForm.SetParent(modelparent);
            CtrlInventoryConsumables.SetParent(modelparent);
            CtrlOperationalStatistics.SetParent(modelparent);
            CtrlOpenCloseForm.SetParent(modelparent);
            CtrlOfflineClosingForm.SetParent(modelparent);
            grdRFID.Visibility = Visibility.Visible;
            GetPrintServerDetails();
        }
        /// <summary>
        /// Handles the Click event of the btnLogout control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void btnLogout_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                AuditLog.AddUserLog(Common.LoginUser.UserId, (int)FrameworkHelper.ActionType.Logout, "Logged out at ");
                var objLogin = new Login();
                objLogin.Show();
                this.Close();
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
            }
        }

        private string pathtosave = Environment.CurrentDirectory + "\\ss.dat";
        /// <summary>
        /// Handles the Click event of the btnImageDownLoad control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        /// 

        private Boolean IspathExist()
        {
            Boolean flag = false;
            ConfigurationInfo Config = (new ConfigBusiness()).GetConfigurationData(LoginUser.SubStoreId);
            if (Config != null)
            {
                if (new DirectoryInfo(Path.GetFullPath(Config.DG_Hot_Folder_Path)).Exists)
                    flag = true;
            }
            return flag;
        }

        private void btnImageDownLoad_Click(object sender, RoutedEventArgs e)
        {
            bool open = false;
            DateTime ServerTime;
            TimeSpan ts = new TimeSpan(4, 0, 0);
            DateTime ClosingFormTime = new DateTime();

            try
            {
                RolePermissionsBusniess rolBiz = new RolePermissionsBusniess();
                List<PermissionRoleInfo> _objPList = rolBiz.GetPermissionData(LoginUser.RoleId);
                List<iMIXConfigurationInfo> lstConfig = (new ConfigBusiness()).GetNewConfigValues(LoginUser.SubStoreId);
                var ClosingProcTime = lstConfig.Where(o => o.IMIXConfigurationMasterId == Convert.ToInt32(ConfigParams.ClosingProcTime)).FirstOrDefault();
                // ASHU
                if (ClosingProcTime != null)
                {
                    string[] arr = ClosingProcTime.ConfigurationValue.ToString().Split(' ');
                    ts = new TimeSpan(Convert.ToInt32(correctHour(ClosingProcTime.ConfigurationValue)), Convert.ToInt32(arr[1]), 0);
                    if (Convert.ToInt32(correctHour(ClosingProcTime.ConfigurationValue)) > 11)
                    {
                        ClosingFormTime = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, Convert.ToInt32(correctHour(ClosingProcTime.ConfigurationValue)), Convert.ToInt32(arr[1]), 0);
                    }
                    else
                    {
                        //ClosingFormTime = new DateTime(DateTime.Now.Year, DateTime.Now.Month, (DateTime.Now.Day + 1), Convert.ToInt32(correctHour(ClosingProcTime.ConfigurationValue)), Convert.ToInt32(arr[1]), 0);
                        ClosingFormTime = DateTime.Now.AddDays(1).Date.AddHours(Convert.ToInt32(correctHour(ClosingProcTime.ConfigurationValue))).AddMinutes(Convert.ToInt32(arr[1]));
                    }
                }

                SageInfo _sageInfo = (new SageBusiness()).GetOpenCloseProcDetail(LoginUser.SubStoreId);
                if (_sageInfo == null)
                    _sageInfo = new SageInfo();
                ServerTime = _sageInfo.ServerTime;
                string OpeningSyncCode = CommonUtility.GetUniqueSynccode(Convert.ToInt32(ApplicationObjectEnum.OpeningFormDetail).ToString().PadLeft(2, '0'), LoginUser.countrycode, LoginUser.Storecode, LoginUser.SubStoreId.ToString().PadLeft(2, '0'));
                string[] stime = ClosingFormTime.ToString().Split(' ');
                ////changed by latika for setting 4am closing form 2019-10-24
                DateTime dt = new DateTime(_sageInfo.TransDate.Value.Date.Year, _sageInfo.TransDate.Value.Date.Month, _sageInfo.TransDate.Value.Date.Day);

                dt = Convert.ToDateTime(dt.ToShortDateString() + " " + stime[1] + " " + stime[2]);
                DateTime TrnsDateNew = Convert.ToDateTime(dt);
                DateTime ClosingFrmDateNew = Convert.ToDateTime(dt);
                if (stime[2] == "AM")
                {
                    ClosingFrmDateNew = Convert.ToDateTime(dt).AddDays(1);
                }

                ///end by latika

                //if opening is  mendatory then we will push him for opening
                if (_sageInfo.FormTypeID != 1 && _sageInfo.TransDate.Value.Date < ServerTime.Date)
                {
                    var Useritem = _objPList.Where(t => t.DG_Permission_Id == 41).FirstOrDefault();
                    if (Useritem == null)
                    {
                        MessageBox.Show("You are not an authorized user to fill opening form, Please contact administrator");
                        return;
                    }

                    //AutoCount6850();
                    //AutoCount8810();
                    CtrlOpeningForm.FromDate = ServerTime.Date;

                    CtrlOpeningForm.printAutoStart6850 = TotalCount6850;
                    CtrlOpeningForm.printAutoStart8810 = TotalCount8810;
                    // BY KCB ON 08 AUG 2019 FOR storing panorama printer inventory
                    K6900Printer.K6900Driver k6900Driver = new K6900Printer.K6900Driver();
                    string[] serialNumbers = new string[1];
                    if (k6900Driver.GetSerialNumbers(out serialNumbers) == true)
                    {

                        K6900Printer.Entity.PrintCountInfo printCountInfo;
                        if (k6900Driver.GetPrintCountInfo(serialNumbers[0], out printCountInfo))
                        {
                            if (printCountInfo != null)
                            {
                                TotalCount6900 = printCountInfo.Cutter;
                                CtrlOpeningForm.printAutoStart6900 = TotalCount6900;
                            }
                        }
                    }
                    //END  
                    var res = CtrlOpeningForm.ShowHandlerDialog("Opening Form");
                    if (res != "")
                    {
                        open = true;
                        //here save the data to db after operation and move ahead
                        string[] split = new string[] { "%##%" };
                        string[] data = res.Split(split, StringSplitOptions.None);
                        _sageInfo = new SageInfo();
                        _sageInfo.sixEightStartingNumber = Convert.ToInt64(data[0]);
                        _sageInfo.eightTenStartingNumber = Convert.ToInt64(data[1]);
                        _sageInfo.sixTwentyStartingNumber = Convert.ToInt64(data[2]);
                        _sageInfo.eightTwentyStartingNumber = Convert.ToInt64(data[3]);
                        _sageInfo.PosterStartingNumber = Convert.ToInt64(data[4]);
                        _sageInfo.CashFloatAmount = Convert.ToDecimal(data[5]);
                        _sageInfo.BusinessDate = Convert.ToDateTime(data[6]);
                        _sageInfo.K6900StartingNumber = Convert.ToInt64(data[7]); // BY KCB ON 08 AUG 2019 FOR storing panorama printer inventory

                        _sageInfo.SubStoreID = LoginUser.SubStoreId;
                        _sageInfo.OpeningDate = ServerTime;
                        _sageInfo.FilledBy = LoginUser.UserId;
                        _sageInfo.FormTypeID = 1;
                        _sageInfo.SyncCode = OpeningSyncCode;
                        _sageInfo.eightTenAutoStartingNumber = TotalCount8810;
                        _sageInfo.sixEightAutoStartingNumber = TotalCount6850;
                        _sageInfo.K6900AutoStartingNumber = TotalCount6900;// BY KCB ON 08 AUG 2019 FOR storing panorama printer inventory
                        if (_sageInfo.BusinessDate.Value.Date > ServerTime.Date)
                        {
                            if (MessageBox.Show("Do you want to fill the opening form for future date " + string.Format("{0:dd-MMM-yyyy}", _sageInfo.BusinessDate.Value) + " ?", "Confirm opening form", MessageBoxButton.YesNo) == MessageBoxResult.Yes)
                            {
                                string retval = (new SageBusiness()).SaveOpeningForm(_sageInfo, lst6850Printer, lst8810Printer);
                                string[] splitRet = new string[] { "%##%" };
                                string[] RetData = retval.Split(splitRet, StringSplitOptions.None);
                                if (RetData[0] == "Success")
                                {
                                    string strmsg = "Opening form filled successfully for " + RetData[1] + " for business date " + string.Format("{0:dd MMM yyyy}", _sageInfo.BusinessDate.Value);
                                    AuditLog.AddUserLog(Common.LoginUser.UserId, (int)FrameworkHelper.ActionType.OpeningForm, strmsg);
                                    MessageBox.Show("Opening form filled successfully for business date " + string.Format("{0:dd-MMM-yyyy}", _sageInfo.BusinessDate.Value));
                                }
                                else if (retval == "Already Exist")
                                {
                                    MessageBox.Show("You have already filled the opening form for selected business date, change the date and try again.");
                                }
                                else
                                {
                                    MessageBox.Show("There is some error in opening form. Please try again.");
                                }
                            }
                        }
                        else
                        {
                            //here i will put a confirmation box based on the condition
                            string retval = (new SageBusiness()).SaveOpeningForm(_sageInfo, lst6850Printer, lst8810Printer);
                            string[] splitRet = new string[] { "%##%" };
                            string[] RetData = retval.Split(splitRet, StringSplitOptions.None);
                            if (RetData[0] == "Success")
                            {
                                string strmsg = "Opening form filled successfully for " + RetData[1] + " for business date " + string.Format("{0:dd MMM yyyy}", _sageInfo.BusinessDate.Value);
                                AuditLog.AddUserLog(Common.LoginUser.UserId, (int)FrameworkHelper.ActionType.OpeningForm, strmsg);
                                MessageBox.Show("Opening form filled successfully for business date " + string.Format("{0:dd-MMM-yyyy}", _sageInfo.BusinessDate.Value));
                            }
                            else if (retval == "Already Exist")
                            {
                                MessageBox.Show("You have already filled the opening form for selected business date, change the date and try again.");
                            }
                            else
                            {
                                MessageBox.Show("There is some error in opening form. Please try again.");
                            }
                        }
                    }

                    //here will save the file to db and move ahead
                } //if opening is  mendatory and user has filled the opening form then we will push him for closing
                  //else if (_sageInfo.FormTypeID == 1 && _sageInfo.TransDate.Value.Date < ServerTime.Date && ((ServerTime.Date - _sageInfo.TransDate.Value.Date).TotalDays > 1 || ServerTime.TimeOfDay > ts))
                  ////changed by latika for setting 4am closing form 2019-10-24  
                  // else if (_sageInfo.FormTypeID == 1 && _sageInfo.TransDate.Value.Date < ServerTime.Date || (_sageInfo.FormTypeID == 1 && ServerTime > ClosingFormTime))
                else if (_sageInfo.FormTypeID == 1 && _sageInfo.TransDate.Value.Date < ServerTime && (_sageInfo.FormTypeID == 1 && ServerTime > ClosingFrmDateNew))
                ///end by latika
                {
                    var Useritem = _objPList.Where(t => t.DG_Permission_Id == 42).FirstOrDefault();
                    if (Useritem == null)
                    {
                        MessageBox.Show("You are not an authorized user to fill closing form, please contact administrator");
                        return;
                    }
                    //AutoCount6850();
                    //AutoCount8810();
                    CtrlInventoryConsumables.FromDate = _sageInfo.FilledOn.Value;
                    CtrlInventoryConsumables.BusinessDate = _sageInfo.TransDate.Value;
                    CtrlInventoryConsumables.ToDate = ServerTime;
                    CtrlInventoryConsumables.SubStoreID = LoginUser.SubStoreId;
                    //this starting number and end number will come from the opening form if opening mendatory otherwise from closing form
                    CtrlInventoryConsumables.SixEightStartingNumber = _sageInfo.sixEightStartingNumber;
                    CtrlInventoryConsumables.EightTenStartingNumber = _sageInfo.eightTenStartingNumber;
                    CtrlInventoryConsumables.PosterStartingNumber = _sageInfo.PosterStartingNumber;

                    // BY KCB ON 08 AUG 2019 FOR storing panorama printer inventory
                    CtrlInventoryConsumables.K6900StartingNumber = _sageInfo.K6900StartingNumber;
                    CtrlInventoryConsumables.K6900AutoStartingNumber = (_sageInfo.K6900AutoStartingNumber == 0 ? _sageInfo.K6900StartingNumber : _sageInfo.K6900AutoStartingNumber);
                    K6900Printer.K6900Driver k6900Driver = new K6900Printer.K6900Driver();
                    string[] serialNumbers = new string[1];
                    if (k6900Driver.GetSerialNumbers(out serialNumbers) == true)
                    {

                        K6900Printer.Entity.PrintCountInfo printCountInfo;
                        if (k6900Driver.GetPrintCountInfo(serialNumbers[0], out printCountInfo))
                        {
                            if (printCountInfo != null)
                            {
                                TotalCount6900 = printCountInfo.Cutter;
                                CtrlOpeningForm.printAutoStart6900 = TotalCount6900;
                            }
                        }
                    }
                    //END

                    CtrlInventoryConsumables.EightTenAutoStartingNumber = _sageInfo.eightTenAutoStartingNumber;
                    CtrlInventoryConsumables.SixEightAutoStartingNumber = _sageInfo.sixEightAutoStartingNumber;
                    CtrlInventoryConsumables.EightTenAutoClosingNumber = TotalCount8810;
                    CtrlInventoryConsumables.SixEightAutoClosingNumber = TotalCount6850;
                    CtrlInventoryConsumables.K6900AutoClosingNumber = TotalCount6900;
                    var Res1 = CtrlInventoryConsumables.ShowHandlerDialog("Closing Form");
                    string[] split = new string[] { "%##%" };
                    string[] data = Res1.Split(split, StringSplitOptions.None);
                    SageInfoClosing _sageInfoClose = new SageInfoClosing();

                    List<InventoryConsumables> lstinventoryItem = new List<InventoryConsumables>();
                    if (Res1 != "")
                    {
                        lstinventoryItem = CtrlInventoryConsumables.lstinventoryItem;
                        _sageInfoClose.sixEightClosingNumber = Convert.ToInt64(data[0]);
                        _sageInfoClose.eightTenClosingNumber = Convert.ToInt64(data[1]);

                        // BY KCB ON 26 MARCH 2018 FOR storing panorama printer inventry
                        _sageInfoClose.SixTwentyClosingNumber = Convert.ToInt64(data[2]);
                        _sageInfoClose.EightTwentyClosingNumber = Convert.ToInt64(data[3]);
                        //END
                        _sageInfoClose.PosterClosingNumber = Convert.ToInt64(data[3]);

                        // BY KCB ON 26 MARCH 2018 FOR storing panorama printer inventry
                        _sageInfoClose.SixTwentytWestage = Convert.ToInt64(data[4]);
                        _sageInfoClose.EightTwentyWestage = Convert.ToInt64(data[5]);
                        //END


                        _sageInfoClose.SixEightWestage = Convert.ToInt64(data[6]);
                        _sageInfoClose.EightTenWestage = Convert.ToInt64(data[7]);
                        _sageInfoClose.PosterWestage = Convert.ToInt64(data[8]);

                        // BY KCB ON 08 AUG 2019 FOR storing panorama printer inventory
                        _sageInfoClose.K6900ClosingNumber = Convert.ToInt64(data[10]);
                        _sageInfoClose.K6900AutoWestage = Convert.ToInt64(data[11]);
                        _sageInfoClose.K6900AutoClosingNumber = CtrlInventoryConsumables.K6900AutoClosingNumber;// BY KCB ON 08 AUG 2019 FOR storing panorama printer inventory

                        //END

                        _sageInfoClose.objSubStore = new SubStoresInfo();
                        _sageInfoClose.objSubStore.DG_SubStore_pkey = LoginUser.SubStoreId;
                        _sageInfoClose.ClosingDate = ServerTime;
                        _sageInfoClose.FilledBy = LoginUser.UserId;
                        _sageInfoClose.FormTypeID = 2;
                        _sageInfoClose.SixEightPrintCount = CtrlInventoryConsumables.SixEightPrintCount;
                        _sageInfoClose.EightTenPrintCount = CtrlInventoryConsumables.EightTenPrintCount;
                        _sageInfoClose.PosterPrintCount = CtrlInventoryConsumables.PosterPrintCount;
                        _sageInfoClose.TransDate = CtrlInventoryConsumables.BusinessDate;
                        _sageInfoClose.OpeningDate = _sageInfo.FilledOn.Value;

                        CtrlOperationalStatistics.BusinessDate = (_sageInfo.TransDate != null && _sageInfo.TransDate != DateTime.MinValue) ? _sageInfo.TransDate.Value : Convert.ToDateTime("1/1/1753");
                        CtrlOperationalStatistics.FromDate = _sageInfo.FilledOn.Value;

                        CtrlOperationalStatistics.ToDate = ServerTime;
                        CtrlOperationalStatistics.SubStoreID = LoginUser.SubStoreId;

                        var Res2 = CtrlOperationalStatistics.ShowHandlerDialog("Closing Form");
                        if (Res2 != "")
                        {
                            //open = true;
                            string[] split1 = new string[] { "%##%" };
                            string[] data1 = Res2.Split(split1, StringSplitOptions.None);
                            _sageInfoClose.Attendance = Convert.ToInt32(data1[0]);
                            _sageInfoClose.LaborHour = Convert.ToDecimal(data1[1]);
                            _sageInfoClose.NoOfCapture = Convert.ToInt64(data1[2]);
                            _sageInfoClose.NoOfPreview = Convert.ToInt64(data1[3]);
                            _sageInfoClose.NoOfImageSold = Convert.ToInt64(data1[4]);
                            _sageInfoClose.Comments = Convert.ToString(data1[5]);
                            _sageInfoClose.NoOfTransactions = Convert.ToInt32(data1[6]);
                            //_sageInfoClose.TotalRevenue = Convert.ToDecimal(data1[7]);
                            string ClosingSyncCode = CommonUtility.GetUniqueSynccode(Convert.ToInt32(ApplicationObjectEnum.ClosingFormDetail).ToString().PadLeft(2, '0'), LoginUser.countrycode, LoginUser.Storecode, LoginUser.SubStoreId.ToString().PadLeft(2, '0'));
                            _sageInfoClose.SyncCode = ClosingSyncCode;
                            _sageInfoClose.InventoryConsumable = lstinventoryItem;

                            SageOpenClose objOpenClose = new SageOpenClose();
                            objOpenClose.objClose = _sageInfoClose;
                            bool isSaved = false;
                            try
                            {
                                isSaved = (new SageBusiness()).SaveClosingForm(objOpenClose, lst6850Printer, lst8810Printer);
                            }
                            catch (Exception en)
                            {
                                MessageBox.Show(en.Message);
                                ErrorHandler.ErrorHandler.LogError(en);
                                return;
                            }
                            if (isSaved)
                            {
                                string filename = "Closing Form" + "_" + string.Format("{0:dd-MMM-yyyy}", _sageInfo.TransDate.Value) + "_" + _sageInfoClose.objSubStore.DG_SubStore_Name;

                                StoreSubStoreDataBusniess StoreData = new StoreSubStoreDataBusniess();
                                var store = StoreData.GetStore();
                                //bool isonline = store.IsOnline;
                                #region Sync Online check - Ashirwad
                                bool isonline = StoreData.GetSyncIsOnline(LoginUser.SubStoreId);
                                #endregion
                                if (!isonline)
                                {
                                    string dataXML = string.Empty;
                                    dataXML = serialize.SerializeObject<SageOpenClose>(objOpenClose);
                                    string strenc = CryptorEngine.Encrypt(dataXML, true);
                                    CtrlOfflineClosingForm.encData = strenc;
                                    CtrlOfflineClosingForm.filename = filename;
                                    CtrlOfflineClosingForm.ShowHandlerDialog("Offline Closing Form");
                                }
                                string strmsg = "Closing form saved successfully for " + _sageInfoClose.objSubStore.DG_SubStore_Name + " for business date " + string.Format("{0:dd MMM yyyy}", _sageInfo.TransDate.Value);
                                AuditLog.AddUserLog(Common.LoginUser.UserId, (int)FrameworkHelper.ActionType.OpeningForm, strmsg);
                                MessageBox.Show("Closing form saved successfully for business date " + string.Format("{0:dd-MMM-yyyy}", _sageInfo.TransDate.Value));
                            }
                            else
                            {
                                MessageBox.Show("There is some error in closing form. Please try again.");
                            }
                        }
                        else if (CtrlOperationalStatistics.Back == 1)
                        {
                            btnImageDownLoad_Click(sender, e);
                        }
                    }
                }   //if opening is not mendatory and user has filled the opening form then we will push him for closing
                else if (_sageInfo.BusinessDate.Value.Date == ServerTime.Date && _sageInfo.FormTypeID == 2)
                {
                    MessageBox.Show("You have already filled the closing form for business date " + string.Format("{0:dd-MMM-yyyy}" + ", if you want to fill the opening form for next business date please go to open/close procedure section.", _sageInfo.BusinessDate.Value));
                }
                else
                {
                    open = true;
                    ImageDownload();
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
            }
            finally
            {
                if (open)
                {
                    if (IspathExist())
                    {
                        if (File.Exists(pathtosave))
                        {
                            var objmdlnd = new ImageDownloader();
                            if (objmdlnd.IsShoworHide())
                            {
                                objmdlnd.Show();
                            }
                            this.Close();
                        }
                        else
                        {
                            MessageBox.Show("Please set the Substore", "i-Mix", MessageBoxButton.OK, MessageBoxImage.Stop);
                        }
                    }
                }
            }
        }
        public void ImageDownload()
        {
            if (!IspathExist())
            {
                MessageBox.Show("Hot Folder Path ia not accessible. Please try later!");
                return;
            }
            string root = Environment.CurrentDirectory;
            string path = root + "\\";
            path = System.IO.Path.Combine(path, "Download\\");
            if (Directory.Exists(path))
            {
                Directory.Delete(path, true);
            }
        }
        private void btnRfidDown_Click(object sender, RoutedEventArgs e)
        {
            (new RfidManualDownload()).Show();
            this.Close();
        }

        public void btnOpenClose_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                DateTime ServerTime;
                RolePermissionsBusniess rolBiz = new RolePermissionsBusniess();
                List<PermissionRoleInfo> _objPList = rolBiz.GetPermissionData(LoginUser.RoleId);

                SageInfo _sageInfo = (new SageBusiness()).GetOpenCloseProcDetail(LoginUser.SubStoreId);
                if (_sageInfo == null)
                    _sageInfo = new SageInfo();
                ServerTime = _sageInfo.ServerTime;
                var resCtrl = string.Empty;
                // BY KCB ON 08 AUG 2019 FOR storing panorama printer inventory
                K6900Printer.K6900Driver k6900Driver = new K6900Printer.K6900Driver();
                string[] serialNumbers = new string[1];
                if (k6900Driver.GetSerialNumbers(out serialNumbers) == true)
                {

                    K6900Printer.Entity.PrintCountInfo printCountInfo;
                    if (k6900Driver.GetPrintCountInfo(serialNumbers[0], out printCountInfo))
                    {
                        if (printCountInfo != null)
                        {
                            TotalCount6900 = printCountInfo.Cutter;

                        }
                    }
                }
                //END 
                if (CtrlOperationalStatistics.Back != 1)
                {
                    CtrlOpenCloseForm.FormTypeID = Convert.ToInt32(_sageInfo.FormTypeID);
                    CtrlOpenCloseForm.BusinessDate = _sageInfo.BusinessDate;
                    CtrlOpenCloseForm.ServerTime = ServerTime;
                    CtrlOpenCloseForm.TransDate = _sageInfo.TransDate;
                    resCtrl = CtrlOpenCloseForm.ShowHandlerDialog("Check Form");
                }
                else
                {
                    resCtrl = "Closing From";
                }

                if (resCtrl == "Opening From")
                {
                    //AutoCount8810();
                    //AutoCount6850();

                    var Useritem = _objPList.Where(t => t.DG_Permission_Id == 41).FirstOrDefault();
                    if (Useritem == null)
                    {
                        MessageBox.Show("You are not an authorized user to fill opening form, please contact administrator");
                        return;
                    }

                    CtrlOpeningForm.FromDate = ServerTime.Date;
                    //here we will set the detailds to the opening form for auto count
                    CtrlOpeningForm.printAutoStart6850 = TotalCount6850;
                    CtrlOpeningForm.printAutoStart8810 = TotalCount8810;
                    CtrlOpeningForm.printAutoStart6900 = TotalCount6900;


                    var res = CtrlOpeningForm.ShowHandlerDialog("Opening Form");
                    if (res != "")
                    {
                        //here save the data to db after operation and move ahead
                        string[] split = new string[] { "%##%" };
                        string[] data = res.Split(split, StringSplitOptions.None);
                        _sageInfo = new SageInfo();
                        _sageInfo.sixEightStartingNumber = Convert.ToInt64(data[0]);
                        _sageInfo.eightTenStartingNumber = Convert.ToInt64(data[1]);
                        _sageInfo.sixTwentyStartingNumber = Convert.ToInt64(data[2]);
                        _sageInfo.eightTwentyStartingNumber = Convert.ToInt64(data[3]);
                        _sageInfo.PosterStartingNumber = Convert.ToInt64(data[4]);
                        _sageInfo.CashFloatAmount = Convert.ToDecimal(data[5]);
                        _sageInfo.BusinessDate = Convert.ToDateTime(data[6]);
                        _sageInfo.K6900StartingNumber = Convert.ToInt64(data[7]); // BY KCB ON 08 AUG 2019 FOR storing panorama printer inventory
                        _sageInfo.SubStoreID = LoginUser.SubStoreId;
                        _sageInfo.OpeningDate = ServerTime; //it should be the first date of the order
                        _sageInfo.FilledBy = LoginUser.UserId;
                        _sageInfo.FormTypeID = 1;
                        string OpeningSyncCode = CommonUtility.GetUniqueSynccode(Convert.ToInt32(ApplicationObjectEnum.OpeningFormDetail).ToString().PadLeft(2, '0'), LoginUser.countrycode, LoginUser.Storecode, LoginUser.SubStoreId.ToString().PadLeft(2, '0'));
                        _sageInfo.SyncCode = OpeningSyncCode;
                        _sageInfo.eightTenAutoStartingNumber = TotalCount8810;
                        _sageInfo.sixEightAutoStartingNumber = TotalCount6850;
                        _sageInfo.K6900AutoStartingNumber = TotalCount6900;// BY KCB ON 08 AUG 2019 FOR storing panorama printer inventory

                        if (_sageInfo.BusinessDate.Value.Date > ServerTime.Date)
                        {
                            if (MessageBox.Show("Do you want to fill the opening form for future date " + string.Format("{0:dd-MMM-yyyy}", _sageInfo.BusinessDate.Value) + " ?", "Confirm opening form", MessageBoxButton.YesNo) == MessageBoxResult.Yes)
                            {
                                string retval = (new SageBusiness()).SaveOpeningForm(_sageInfo, lst6850Printer, lst8810Printer);
                                string[] splitRet = new string[] { "%##%" };
                                string[] RetData = retval.Split(splitRet, StringSplitOptions.None);

                                if (RetData[0] == "Success")
                                {
                                    string strmsg = "Opening form filled successfully for " + RetData[1] + " for business date " + string.Format("{0:dd MMM yyyy}", _sageInfo.BusinessDate.Value);
                                    AuditLog.AddUserLog(Common.LoginUser.UserId, (int)FrameworkHelper.ActionType.OpeningForm, strmsg);
                                    MessageBox.Show("Opening form filled successfully for business date " + string.Format("{0:dd-MMM-yyyy}", _sageInfo.BusinessDate.Value));
                                }
                                else if (retval == "Already Exist")
                                {
                                    MessageBox.Show("You have already filled the opening form for selected business date, change the date and try again.");
                                }
                                else
                                {
                                    MessageBox.Show("There is some error in opening form. Please try again.");
                                }
                            }
                        }
                        else
                        {
                            //here i will put a confirmation box based on the condition
                            string retval = (new SageBusiness()).SaveOpeningForm(_sageInfo, lst6850Printer, lst8810Printer);

                            string[] splitRet = new string[] { "%##%" };
                            string[] RetData = retval.Split(splitRet, StringSplitOptions.None);

                            if (RetData[0] == "Success")
                            {
                                string strmsg = "Opening form filled successfully for " + RetData[1] + " for business date " + string.Format("{0:dd MMM yyyy}", _sageInfo.BusinessDate.Value);
                                AuditLog.AddUserLog(Common.LoginUser.UserId, (int)FrameworkHelper.ActionType.OpeningForm, strmsg);
                                MessageBox.Show("Opening form filled successfully for business date " + string.Format("{0:dd-MMM-yyyy}", _sageInfo.BusinessDate.Value));
                            }
                            else if (retval == "Already Exist")
                            {
                                MessageBox.Show("You have already filled the opening form for selected business date, change the date and try again.");
                            }
                            else
                            {
                                MessageBox.Show("There is some error in opening form. Please try again.");
                            }
                        }
                    }
                }
                else if (resCtrl == "Closing From")
                {
                    var Useritem = _objPList.Where(t => t.DG_Permission_Id == 42).FirstOrDefault();
                    if (Useritem == null)
                    {
                        MessageBox.Show("You are not an authorized user to fill closing form, please contact administrator");
                        return;
                    }

                    if (_sageInfo.TransDate.Value.Date > ServerTime.Date)
                    {
                        MessageBox.Show("You can not fill the closing form for future date " + string.Format("{0:dd-MMM-yyyy}", _sageInfo.TransDate.Value));
                        return;
                    }
                    //AutoCount8810();
                    //AutoCount6850();
                    CtrlInventoryConsumables.FromDate = (_sageInfo.FilledOn != null && _sageInfo.FilledOn.Value != DateTime.MinValue) ? _sageInfo.FilledOn.Value : ServerTime.Date;
                    CtrlInventoryConsumables.BusinessDate = _sageInfo.TransDate.Value;

                    CtrlInventoryConsumables.ToDate = ServerTime;
                    CtrlInventoryConsumables.SubStoreID = LoginUser.SubStoreId;
                    //this starting number and end number will come from the opening form if opening mendatory otherwise from closing form
                    CtrlInventoryConsumables.SixEightStartingNumber = _sageInfo.sixEightStartingNumber;
                    CtrlInventoryConsumables.EightTenStartingNumber = _sageInfo.eightTenStartingNumber;

                    // BY KCB ON 26 MARCH 2018 FOR storing panorama printer inventry
                    CtrlInventoryConsumables.SixTwentyStartingNumber = _sageInfo.sixTwentyStartingNumber;
                    CtrlInventoryConsumables.EightTwentyStartingNumber = _sageInfo.eightTwentyStartingNumber;
                    //END
                    CtrlInventoryConsumables.PosterStartingNumber = _sageInfo.PosterStartingNumber;

                    CtrlInventoryConsumables.EightTenAutoStartingNumber = _sageInfo.eightTenAutoStartingNumber;
                    CtrlInventoryConsumables.SixEightAutoStartingNumber = _sageInfo.sixEightAutoStartingNumber;
                    // BY KCB ON 26 MARCH 2018 FOR storing panorama printer inventry
                    CtrlInventoryConsumables.SixTwentyAutoStartingNumber = _sageInfo.sixTwentyAutoStartingNumber;
                    CtrlInventoryConsumables.EightTwemtyAutoStartingNumber = _sageInfo.eightTwentyAutoStartingNumber;
                    //END
                    CtrlInventoryConsumables.EightTenAutoClosingNumber = TotalCount8810;
                    CtrlInventoryConsumables.SixEightAutoClosingNumber = TotalCount6850;
                    // BY KCB ON 08 AUG 2019 FOR storing panorama printer inventory
                    CtrlInventoryConsumables.K6900StartingNumber = _sageInfo.K6900StartingNumber;
                    CtrlInventoryConsumables.K6900AutoStartingNumber = (_sageInfo.K6900AutoStartingNumber == 0 ? _sageInfo.K6900StartingNumber : _sageInfo.K6900AutoStartingNumber);
                    CtrlInventoryConsumables.K6900AutoClosingNumber = TotalCount6900;

                    //END
                    // BY KCB ON 26 MARCH 2018 FOR storing panorama printer inventry
                    CtrlInventoryConsumables.EightTwemtyAutoClosingNumber = 0;
                    CtrlInventoryConsumables.SixTwemtyAutoClosingNumber = 0;

                    CtrlInventoryConsumables.SixTwemtyAutoClosingNumber = 0;
                    //END

                    var Res1 = CtrlInventoryConsumables.ShowHandlerDialog("Closing Form");
                    string[] split = new string[] { "%##%" };
                    string[] data = Res1.Split(split, StringSplitOptions.None);
                    SageInfoClosing _sageInfoClose = new SageInfoClosing();
                    List<InventoryConsumables> lstinventoryItem = new List<InventoryConsumables>();
                    if (Res1 != "")
                    {
                        lstinventoryItem = CtrlInventoryConsumables.lstinventoryItem;

                        // ClosingFormDetailID
                        _sageInfoClose.sixEightClosingNumber = Convert.ToInt64(data[0]);
                        _sageInfoClose.eightTenClosingNumber = Convert.ToInt64(data[1]);

                        // BY KCB ON 26 MARCH 2018 FOR storing panorama printer inventry
                        _sageInfoClose.SixTwentyClosingNumber = Convert.ToInt64(data[2]);
                        _sageInfoClose.EightTwentyClosingNumber = Convert.ToInt64(data[3]);
                        //end
                        _sageInfoClose.PosterClosingNumber = Convert.ToInt64(data[4]);
                        _sageInfoClose.SixEightWestage = Convert.ToInt64(data[5]);
                        _sageInfoClose.EightTenWestage = Convert.ToInt64(data[6]);

                        // BY KCB ON 26 MARCH 2018 FOR storing panorama printer inventry
                        _sageInfoClose.SixTwentytWestage = Convert.ToInt64(data[7]);
                        _sageInfoClose.EightTwentyWestage = Convert.ToInt64(data[8]);
                        //end

                        _sageInfoClose.PosterWestage = Convert.ToInt64(data[9]);
                        // BY KCB ON 08 AUG 2019 FOR storing panorama printer inventory
                        _sageInfoClose.K6900ClosingNumber = Convert.ToInt64(data[10]);
                        _sageInfoClose.K6900AutoWestage = Convert.ToInt64(data[11]);
                        _sageInfoClose.K6900AutoClosingNumber = CtrlInventoryConsumables.K6900AutoClosingNumber;
                        //END

                        _sageInfoClose.objSubStore = new SubStoresInfo();
                        _sageInfoClose.objSubStore.DG_SubStore_pkey = LoginUser.SubStoreId;
                        _sageInfoClose.ClosingDate = ServerTime;
                        _sageInfoClose.FilledBy = LoginUser.UserId;
                        _sageInfoClose.FormTypeID = 2;
                        _sageInfoClose.SixEightPrintCount = CtrlInventoryConsumables.SixEightPrintCount;
                        _sageInfoClose.EightTenPrintCount = CtrlInventoryConsumables.EightTenPrintCount;
                        _sageInfoClose.PosterPrintCount = CtrlInventoryConsumables.PosterPrintCount;
                        _sageInfoClose.TransDate = CtrlInventoryConsumables.BusinessDate;
                        _sageInfoClose.OpeningDate = _sageInfo.FilledOn.Value;

                        CtrlOperationalStatistics.BusinessDate = (_sageInfo.TransDate != null && _sageInfo.TransDate != DateTime.MinValue) ? _sageInfo.TransDate.Value : ServerTime.Date;
                        CtrlOperationalStatistics.FromDate = _sageInfo.FilledOn.Value;
                        CtrlOperationalStatistics.ToDate = ServerTime;
                        CtrlOperationalStatistics.SubStoreID = LoginUser.SubStoreId;
                        var Res2 = CtrlOperationalStatistics.ShowHandlerDialog("Closing Form");
                        if (Res2 != "")
                        {
                            string[] split1 = new string[] { "%##%" };
                            string[] data1 = Res2.Split(split1, StringSplitOptions.None);
                            _sageInfoClose.Attendance = Convert.ToInt32(data1[0]);
                            _sageInfoClose.LaborHour = Convert.ToDecimal(data1[1]);
                            _sageInfoClose.NoOfCapture = Convert.ToInt64(data1[2]);
                            _sageInfoClose.NoOfPreview = Convert.ToInt64(data1[3]);
                            _sageInfoClose.NoOfImageSold = Convert.ToInt64(data1[4]);
                            _sageInfoClose.Comments = Convert.ToString(data1[5]);
                            _sageInfoClose.NoOfTransactions = Convert.ToInt32(data1[6]);
                            //_sageInfoClose.TotalRevenue = Convert.ToDecimal(data1[7]);
                            string ClosingSyncCode = CommonUtility.GetUniqueSynccode(Convert.ToInt32(ApplicationObjectEnum.ClosingFormDetail).ToString().PadLeft(2, '0'), LoginUser.countrycode, LoginUser.Storecode, LoginUser.SubStoreId.ToString().PadLeft(2, '0'));
                            _sageInfoClose.SyncCode = ClosingSyncCode;
                            _sageInfoClose.InventoryConsumable = lstinventoryItem;
                            _sageInfoClose.eightTenAutoClosingNumber = TotalCount8810;
                            _sageInfoClose.sixEightAutoClosingNumber = TotalCount6850;
                            _sageInfoClose.K6900AutoClosingNumber = TotalCount6900;// BY KCB ON 08 AUG 2019 FOR storing panorama printer inventory
                            _sageInfoClose.SixEightAutoWestage = 0;
                            _sageInfoClose.EightTenAutoWestage = 0;

                            SageOpenClose objOpenClose = new SageOpenClose();
                            objOpenClose.objClose = _sageInfoClose;

                            bool isSaved = false;
                            try
                            {
                                isSaved = (new SageBusiness()).SaveClosingForm(objOpenClose, lst6850Printer, lst8810Printer);
                            }
                            catch (Exception en)
                            {
                                MessageBox.Show(en.Message);
                                ErrorHandler.ErrorHandler.LogError(en);
                                return;
                            }
                            if (isSaved)
                            {
                                string filename = "Closing Form" + "_" + string.Format("{0:dd-MMM-yyyy}", _sageInfo.TransDate.Value) + "_" + _sageInfoClose.objSubStore.DG_SubStore_Name;

                                StoreSubStoreDataBusniess StoreData = new StoreSubStoreDataBusniess();
                                var store = StoreData.GetStore();
                                //bool isonline = store.IsOnline;
                                bool isonline = StoreData.GetSyncIsOnline(LoginUser.SubStoreId);

                                if (!isonline)
                                {
                                    string dataXML = string.Empty;
                                    dataXML = serialize.SerializeObject<SageOpenClose>(objOpenClose);
                                    string strenc = CryptorEngine.Encrypt(dataXML, true);
                                    CtrlOfflineClosingForm.encData = strenc;
                                    CtrlOfflineClosingForm.filename = filename;
                                    CtrlOfflineClosingForm.ShowHandlerDialog("Offline Closing Form");
                                }
                                string strmsg = "Closing form saved successfully for " + _sageInfoClose.objSubStore.DG_SubStore_Name + " for business date " + string.Format("{0:dd MMM yyyy}", _sageInfo.TransDate.Value);
                                AuditLog.AddUserLog(Common.LoginUser.UserId, (int)FrameworkHelper.ActionType.ClosingForm, strmsg);
                                MessageBox.Show("Closing form saved successfully for business date " + string.Format("{0:dd-MMM-yyyy}", _sageInfo.TransDate.Value));
                            }
                            else
                            {
                                MessageBox.Show("There is some error in form. Please try again.");
                            }
                        }
                        else if (CtrlOperationalStatistics.Back == 1)
                        {
                            btnOpenClose_Click(sender, e);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
            }
        }

        // ASHU
        string correctHour(string time)
        {
            var ampmLen = 2;
            var ampm = time.Substring(time.Length - ampmLen, ampmLen);
            var hourIndex = 0;
            var hour = time.Split(' ')[hourIndex];
            var h = hour;
            if (ampm.Equals("PM"))
            {
                h = (int.Parse(hour) + 12).ToString();
            }
            if (hour.Equals("12") || hour.Equals("24"))
            {
                if (ampm.Equals("AM"))
                {
                    h = "00";
                }
                else if (ampm.Equals("PM"))
                {
                    h = "12";
                }
            }
            return h;
        }

        private void btnOrderStation_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                //here i will write the code for open close form
                TimeSpan ts = new TimeSpan(4, 0, 0);
                DateTime ServerTime;
                DateTime ClosingFormTime = new DateTime();
                RolePermissionsBusniess rolBiz = new RolePermissionsBusniess();
                List<PermissionRoleInfo> _objPList = rolBiz.GetPermissionData(LoginUser.RoleId);
                int strMin = 0;
                List<iMIXConfigurationInfo> lstConfig = (new ConfigBusiness()).GetNewConfigValues(LoginUser.SubStoreId);
                var ClosingProcTime = lstConfig.Where(o => o.IMIXConfigurationMasterId == Convert.ToInt32(ConfigParams.ClosingProcTime)).FirstOrDefault();
                // ASHU
                //if (ClosingProcTime != null)
                if (ClosingProcTime != null && (!ClosingProcTime.ConfigurationValue.ToString().Contains("Hour Minute AM")))////changed by latika for closing for err 25 march20202
                {
                    {
                        string[] arr = ClosingProcTime.ConfigurationValue.ToString().Split(' ');
                        strMin = Convert.ToInt32(arr[1]);

                        ts = new TimeSpan(Convert.ToInt32(correctHour(ClosingProcTime.ConfigurationValue)), Convert.ToInt32(arr[1]), 0);
                        if (Convert.ToInt32(correctHour(ClosingProcTime.ConfigurationValue)) > 11)
                        {
                            ClosingFormTime = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, Convert.ToInt32(correctHour(ClosingProcTime.ConfigurationValue)), Convert.ToInt32(arr[1]), 0);

                        }
                        else
                        {

                            //ClosingFormTime = new DateTime(DateTime.Now.Year, DateTime.Now.Month, (DateTime.Now.AddDays(1).Day), Convert.ToInt32(correctHour(ClosingProcTime.ConfigurationValue)), Convert.ToInt32(arr[1]), 0);
                            ClosingFormTime = DateTime.Now.AddDays(1).Date.AddHours(Convert.ToInt32(correctHour(ClosingProcTime.ConfigurationValue))).AddMinutes(Convert.ToInt32(arr[1]));

                        }
                    }
                    // BY KCB ON 08 AUG 2019 FOR storing panorama printer inventory
                    K6900Printer.K6900Driver k6900Driver = new K6900Printer.K6900Driver();
                    string[] serialNumbers = new string[1];
                    if (k6900Driver.GetSerialNumbers(out serialNumbers) == true)
                    {

                        K6900Printer.Entity.PrintCountInfo printCountInfo;
                        if (k6900Driver.GetPrintCountInfo(serialNumbers[0], out printCountInfo))
                        {
                            if (printCountInfo != null)
                                TotalCount6900 = printCountInfo.Cutter;
                        }
                    }
                    //END  
                    SageInfo _sageInfo = (new SageBusiness()).GetOpenCloseProcDetail(LoginUser.SubStoreId);

                    if (_sageInfo == null)
                        _sageInfo = new SageInfo() { TransDate = DateTime.MinValue, BusinessDate = DateTime.MinValue };

                    ServerTime = _sageInfo.ServerTime;

                    string[] stime = ClosingFormTime.ToString().Split(' ');

                    if (!ClosingFormTime.ToString().Contains("AM"))
                    {
                        string dateval = ClosingFormTime.ToString() + " " + "AM";
                        stime = dateval.ToString().Split(' ');
                    }
                    ////changed by latika for setting 4am closing form 2019-10-24
                    DateTime dt = new DateTime(_sageInfo.TransDate.Value.Date.Year, _sageInfo.TransDate.Value.Date.Month, _sageInfo.TransDate.Value.Date.Day);

                    ErrorHandler.ErrorHandler.LogFileWrite("Time 1 :" + dt.ToShortDateString() + "Stime1 : " + stime[1].ToString() + " stime2 : " + stime[2].ToString());
                    dt = Convert.ToDateTime(dt.ToShortDateString() + " " + stime[1] + " " + stime[2]);
                    DateTime TrnsDateNew = Convert.ToDateTime(dt);
                    DateTime ClosingFrmDateNew = Convert.ToDateTime(dt);
                    if (stime[2] == "AM")
                    {
                        ClosingFrmDateNew = Convert.ToDateTime(dt).AddDays(1);
                    }
                    ///end by latika
                    //.AddHours(Convert.ToInt32(correctHour(ClosingProcTime.ConfigurationValue))).AddMinutes(Convert.ToInt32(strMin)); ;
                    //TrnsDateNew=
                    if (_sageInfo.FormTypeID != 1 && _sageInfo.TransDate.Value.Date < ServerTime.Date)
                    {
                        var Useritem = _objPList.Where(t => t.DG_Permission_Id == 41 | t.DG_Permission_Id == 1).FirstOrDefault();
                        if (Useritem == null)
                        {
                            MessageBox.Show("You are not an authorized user to fill opening form, Please contact administrator");
                            return;
                        }
                        //AutoCount6850();
                        //AutoCount8810();
                        CtrlOpeningForm.FromDate = ServerTime.Date;
                        CtrlOpeningForm.printAutoStart6850 = TotalCount6850;
                        CtrlOpeningForm.printAutoStart8810 = TotalCount8810;
                        CtrlOpeningForm.printAutoStart6900 = TotalCount6900; // BY KCB ON 08 AUG 2019 FOR storing panorama printer inventory
                        var res = CtrlOpeningForm.ShowHandlerDialog("Opening Form");
                        if (res != "")
                        {
                            //here save the data to db after operation and move ahead
                            string[] split = new string[] { "%##%" };
                            string[] data = res.Split(split, StringSplitOptions.None);
                            _sageInfo = new SageInfo();
                            _sageInfo.sixEightStartingNumber = Convert.ToInt64(data[0]);
                            _sageInfo.eightTenStartingNumber = Convert.ToInt64(data[1]);
                            _sageInfo.sixTwentyStartingNumber = Convert.ToInt64(data[2]);
                            _sageInfo.eightTwentyStartingNumber = Convert.ToInt64(data[3]);
                            _sageInfo.PosterStartingNumber = Convert.ToInt64(data[4]);
                            _sageInfo.CashFloatAmount = Convert.ToDecimal(data[5]);
                            _sageInfo.BusinessDate = Convert.ToDateTime(data[6]);
                            _sageInfo.K6900StartingNumber = Convert.ToInt64(data[7]); // BY KCB ON 08 AUG 2019 FOR storing panorama printer inventory

                            _sageInfo.SubStoreID = LoginUser.SubStoreId;
                            _sageInfo.OpeningDate = ServerTime;
                            _sageInfo.FilledBy = LoginUser.UserId;
                            _sageInfo.FormTypeID = 1;
                            _sageInfo.eightTenAutoStartingNumber = TotalCount8810;
                            _sageInfo.sixEightAutoStartingNumber = TotalCount6850;
                            _sageInfo.K6900AutoStartingNumber = TotalCount6900;// BY KCB ON 08 AUG 2019 FOR storing panorama printer inventory

                            string OpeningSyncCode = CommonUtility.GetUniqueSynccode(Convert.ToInt32(ApplicationObjectEnum.OpeningFormDetail).ToString().PadLeft(2, '0'), LoginUser.countrycode, LoginUser.Storecode, LoginUser.SubStoreId.ToString().PadLeft(2, '0'));
                            _sageInfo.SyncCode = OpeningSyncCode;

                            if (_sageInfo.BusinessDate.Value.Date > ServerTime.Date)
                            {
                                if (MessageBox.Show("Do you want to fill the opening form for future date " + string.Format("{0:dd-MMM-yyyy}", _sageInfo.BusinessDate.Value) + " ?", "Confirm opening form", MessageBoxButton.YesNo) == MessageBoxResult.Yes)
                                {
                                    string retval = (new SageBusiness()).SaveOpeningForm(_sageInfo, lst6850Printer, lst8810Printer);
                                    string[] splitRet = new string[] { "%##%" };
                                    string[] RetData = retval.Split(splitRet, StringSplitOptions.None);
                                    if (RetData[0] == "Success")
                                    {
                                        string strmsg = "Opening form filled successfully for " + RetData[1] + " for business date " + string.Format("{0:dd MMM yyyy}", _sageInfo.BusinessDate.Value);
                                        AuditLog.AddUserLog(Common.LoginUser.UserId, (int)FrameworkHelper.ActionType.OpeningForm, strmsg);
                                        MessageBox.Show("Opening form filled successfully for business date " + string.Format("{0:dd-MMM-yyyy}", _sageInfo.BusinessDate.Value));
                                    }
                                    else if (retval == "Already Exist")
                                    {
                                        MessageBox.Show("You have already filled the opening form for selected business date, change the date and try again.");
                                    }
                                    else
                                    {
                                        MessageBox.Show("There is some error in opening form. Please try again.");
                                    }
                                }
                            }
                            else
                            {
                                string retval = (new SageBusiness()).SaveOpeningForm(_sageInfo, lst6850Printer, lst8810Printer);
                                string[] splitRet = new string[] { "%##%" };
                                string[] RetData = retval.Split(splitRet, StringSplitOptions.None);
                                if (RetData[0] == "Success")
                                {
                                    string strmsg = "Opening form filled successfully for " + RetData[1] + " for business date " + string.Format("{0:dd MMM yyyy}", _sageInfo.BusinessDate.Value);
                                    AuditLog.AddUserLog(Common.LoginUser.UserId, (int)FrameworkHelper.ActionType.OpeningForm, strmsg);
                                    MessageBox.Show("Opening form filled successfully for business date " + string.Format("{0:dd-MMM-yyyy}", _sageInfo.BusinessDate.Value));
                                }
                                else if (retval == "Already Exist")
                                {
                                    MessageBox.Show("You have already filled the opening form for selected business date, change the date and try again.");
                                }
                                else
                                {
                                    MessageBox.Show("There is some error in opening form. Please try again.");
                                }
                            }
                        }
                    }
                    //Here we will push the condition for ime hatever is fixed for db based on that we will fialize the closing form fill mendatory
                    //_sageInfo.TransDate.Value.Date
                    ////changed by latika for setting 4am closing form 2019-10-24
                    // else if (_sageInfo.FormTypeID == 1 && _sageInfo.TransDate.Value.Date < ServerTime.Date || (_sageInfo.FormTypeID == 1 && ServerTime > ClosingFormTime))
                    //else if (_sageInfo.FormTypeID == 1 && _sageInfo.TransDate.Value.Date < ServerTime.Date && ((ServerTime.Date - _sageInfo.TransDate.Value.Date).TotalDays > 1 || ServerTime.TimeOfDay > ts))
                    else if (_sageInfo.FormTypeID == 1 && _sageInfo.TransDate.Value.Date < ServerTime && (_sageInfo.FormTypeID == 1 && ServerTime > ClosingFrmDateNew))
                    ///end by latika
                    //
                    {
                        //AutoCount6850();
                        //AutoCount8810();
                        var Useritem = _objPList.Where(t => t.DG_Permission_Id == 42).FirstOrDefault();
                        if (Useritem == null)
                        {
                            MessageBox.Show("You are not an authorized user to fill closing form, Please contact administrator");
                            return;
                        }
                        CtrlInventoryConsumables.FromDate = _sageInfo.FilledOn.Value;
                        CtrlInventoryConsumables.BusinessDate = _sageInfo.TransDate.Value;
                        CtrlInventoryConsumables.ToDate = ServerTime;
                        CtrlInventoryConsumables.SubStoreID = LoginUser.SubStoreId;
                        //this starting number and end number will come from the opening form if opening mendatory otherwise from closing form
                        CtrlInventoryConsumables.SixEightStartingNumber = _sageInfo.sixEightStartingNumber;
                        CtrlInventoryConsumables.EightTenStartingNumber = _sageInfo.eightTenStartingNumber;
                        CtrlInventoryConsumables.PosterStartingNumber = _sageInfo.PosterStartingNumber;
                        // BY KCB ON 08 AUG 2019 FOR storing panorama printer inventory
                        CtrlInventoryConsumables.K6900StartingNumber = _sageInfo.K6900StartingNumber;
                        CtrlInventoryConsumables.K6900AutoStartingNumber = (_sageInfo.K6900AutoStartingNumber == 0 ? _sageInfo.K6900StartingNumber : _sageInfo.K6900AutoStartingNumber);
                        CtrlInventoryConsumables.K6900AutoClosingNumber = TotalCount6900;
                        //END
                        CtrlInventoryConsumables.EightTenAutoStartingNumber = _sageInfo.eightTenAutoStartingNumber;
                        CtrlInventoryConsumables.SixEightAutoStartingNumber = _sageInfo.sixEightAutoStartingNumber;
                        CtrlInventoryConsumables.EightTenAutoClosingNumber = TotalCount8810;
                        CtrlInventoryConsumables.SixEightAutoClosingNumber = TotalCount6850;


                        // BY KCB ON 26 MARCH 2018 FOR storing panorama printer inventry
                        CtrlInventoryConsumables.EightTenStartingNumber = _sageInfo.eightTenAutoStartingNumber;
                        CtrlInventoryConsumables.SixTwentyAutoStartingNumber = _sageInfo.sixEightAutoStartingNumber;
                        CtrlInventoryConsumables.EightTwemtyAutoClosingNumber = 0;
                        CtrlInventoryConsumables.SixTwemtyAutoClosingNumber = TotalCount6850;
                        //end

                        //CtrlInventoryConsumables.txt6900ClosingNumber

                        var Res1 = CtrlInventoryConsumables.ShowHandlerDialog("Closing Form");
                        string[] split = new string[] { "%##%" };
                        string[] data = Res1.Split(split, StringSplitOptions.None);
                        SageInfoClosing _sageInfoClose = new SageInfoClosing();

                        List<InventoryConsumables> lstinventoryItem = new List<InventoryConsumables>();
                        if (Res1 != "")
                        {
                            lstinventoryItem = CtrlInventoryConsumables.lstinventoryItem;
                            _sageInfoClose.sixEightClosingNumber = Convert.ToInt64(data[0]);
                            _sageInfoClose.eightTenClosingNumber = Convert.ToInt64(data[1]);

                            // BY KCB ON 26 MARCH 2018 FOR storing panorama printer inventry
                            _sageInfoClose.SixEightWestage = Convert.ToInt64(data[2]);
                            _sageInfoClose.EightTenWestage = Convert.ToInt64(data[3]);
                            //end
                            _sageInfoClose.PosterClosingNumber = Convert.ToInt64(data[4]);
                            _sageInfoClose.SixEightWestage = Convert.ToInt64(data[5]);
                            _sageInfoClose.EightTenWestage = Convert.ToInt64(data[6]);
                            // BY KCB ON 26 MARCH 2018 FOR storing panorama printer inventry
                            _sageInfoClose.SixEightWestage = Convert.ToInt64(data[7]);
                            _sageInfoClose.EightTenWestage = Convert.ToInt64(data[8]);
                            //end
                            _sageInfoClose.PosterWestage = Convert.ToInt64(data[9]);
                            // BY KCB ON 08 AUG 2019 FOR storing panorama printer inventory
                            _sageInfoClose.K6900ClosingNumber = Convert.ToInt64(data[10]);
                            _sageInfoClose.K6900AutoWestage = Convert.ToInt64(data[11]);
                            _sageInfoClose.K6900AutoClosingNumber = CtrlInventoryConsumables.K6900AutoClosingNumber;
                            //END
                            _sageInfoClose.objSubStore = new SubStoresInfo();
                            _sageInfoClose.objSubStore.DG_SubStore_pkey = LoginUser.SubStoreId;
                            _sageInfoClose.ClosingDate = ServerTime;
                            _sageInfoClose.FilledBy = LoginUser.UserId;
                            _sageInfoClose.FormTypeID = 2;
                            _sageInfoClose.SixEightPrintCount = CtrlInventoryConsumables.SixEightPrintCount;
                            _sageInfoClose.EightTenPrintCount = CtrlInventoryConsumables.EightTenPrintCount;
                            _sageInfoClose.PosterPrintCount = CtrlInventoryConsumables.PosterPrintCount;
                            _sageInfoClose.TransDate = CtrlInventoryConsumables.BusinessDate;
                            _sageInfoClose.OpeningDate = _sageInfo.FilledOn.Value;

                            CtrlOperationalStatistics.BusinessDate = (_sageInfo.TransDate != null && _sageInfo.TransDate != DateTime.MinValue) ? _sageInfo.TransDate.Value : Convert.ToDateTime("1/1/1753");
                            CtrlOperationalStatistics.FromDate = _sageInfo.FilledOn.Value;
                            CtrlOperationalStatistics.ToDate = ServerTime;
                            CtrlOperationalStatistics.SubStoreID = LoginUser.SubStoreId;

                            var Res2 = CtrlOperationalStatistics.ShowHandlerDialog("Closing Form");
                            if (Res2 != "")
                            {
                                string[] split1 = new string[] { "%##%" };
                                string[] data1 = Res2.Split(split1, StringSplitOptions.None);
                                _sageInfoClose.Attendance = Convert.ToInt32(data1[0]);
                                _sageInfoClose.LaborHour = Convert.ToDecimal(data1[1]);
                                _sageInfoClose.NoOfCapture = Convert.ToInt64(data1[2]);
                                _sageInfoClose.NoOfPreview = Convert.ToInt64(data1[3]);
                                _sageInfoClose.NoOfImageSold = Convert.ToInt64(data1[4]);
                                _sageInfoClose.Comments = Convert.ToString(data1[5]);
                                _sageInfoClose.NoOfTransactions = Convert.ToInt32(data1[6]);
                                //_sageInfoClose.TotalRevenue = Convert.ToDecimal(data1[7]);
                                _sageInfoClose.K6900AutoClosingNumber = TotalCount6900;// BY KCB ON 08 AUG 2019 FOR storing panorama printer inventory
                                string ClosingSyncCode = CommonUtility.GetUniqueSynccode(Convert.ToInt32(ApplicationObjectEnum.ClosingFormDetail).ToString().PadLeft(2, '0'), LoginUser.countrycode, LoginUser.Storecode, LoginUser.SubStoreId.ToString().PadLeft(2, '0'));
                                _sageInfoClose.SyncCode = ClosingSyncCode;
                                _sageInfoClose.InventoryConsumable = lstinventoryItem;
                                SageOpenClose objOpenClose = new SageOpenClose();
                                objOpenClose.objClose = _sageInfoClose;
                                bool isSaved = false;
                                try
                                {
                                    isSaved = (new SageBusiness()).SaveClosingForm(objOpenClose, lst6850Printer, lst8810Printer);
                                }
                                catch (Exception en)
                                {
                                    MessageBox.Show(en.Message);
                                    ErrorHandler.ErrorHandler.LogError(en);
                                    return;
                                }
                                if (isSaved)
                                {
                                    string filename = "Closing Form" + "_" + string.Format("{0:dd-MMM-yyyy}", _sageInfo.TransDate.Value) + "_" + _sageInfoClose.objSubStore.DG_SubStore_Name;

                                    StoreSubStoreDataBusniess StoreData = new StoreSubStoreDataBusniess();
                                    var store = StoreData.GetStore();
                                    //bool isonline = store.IsOnline;
                                    bool isonline = StoreData.GetSyncIsOnline(LoginUser.SubStoreId);

                                    if (!isonline)
                                    {
                                        string dataXML = string.Empty;

                                        //here i will get the substore id and sync code
                                        SubStoresInfo _objSubstore = (new StoreSubStoreDataBusniess()).GetSubstoreData(LoginUser.SubStoreId);

                                        _sageInfoClose.objSubStore.DG_SubStore_pkey = (_objSubstore.LogicalSubStoreID == 0) ? LoginUser.SubStoreId : Convert.ToInt32(_objSubstore.LogicalSubStoreID);
                                        dataXML = serialize.SerializeObject<SageOpenClose>(objOpenClose);
                                        string strenc = CryptorEngine.Encrypt(dataXML, true);
                                        CtrlOfflineClosingForm.encData = strenc;
                                        CtrlOfflineClosingForm.filename = filename;
                                        CtrlOfflineClosingForm.ShowHandlerDialog("Offline Closing Form");
                                    }
                                    string strmsg = "Closing form saved successfully for " + _sageInfoClose.objSubStore.DG_SubStore_Name + " for business date " + string.Format("{0:dd MMM yyyy}", _sageInfo.TransDate.Value);
                                    AuditLog.AddUserLog(Common.LoginUser.UserId, (int)FrameworkHelper.ActionType.ClosingForm, strmsg);
                                    MessageBox.Show("Closing form saved successfully for business date " + string.Format("{0:dd-MMM-yyyy}", _sageInfo.TransDate.Value));
                                }
                                else
                                {
                                    MessageBox.Show("There is some error in closing form. Please try again.");
                                }
                            }
                            else if (CtrlOperationalStatistics.Back == 1)
                            {
                                btnOrderStation_Click(sender, e);
                            }
                        }
                    }
                    else if (_sageInfo.BusinessDate.Value.Date == ServerTime.Date && _sageInfo.FormTypeID == 2)
                    {
                        MessageBox.Show("You have already filled the closing form for business date " + string.Format("{0:dd-MMM-yyyy}" + ", if you want to fill the opening form for next business date please go to open/close procedure section.", _sageInfo.BusinessDate.Value));
                    }
                    else
                    {
                        OrderStation();
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
            }
        }
        public void OrderStation()
        {
            if (File.Exists(pathtosave))
            {
                SearchByPhoto objsearchPhoto;
                Window win = null;
                foreach (Window wnd in Application.Current.Windows)
                {
                    if (wnd.Title == "Search")
                    {
                        win = wnd as SearchByPhoto;
                    }
                }

                if (win != null)
                {
                    objsearchPhoto = (SearchByPhoto)win;
                }
                else
                {
                    objsearchPhoto = new SearchByPhoto();
                }
                this.Close();
                objsearchPhoto.Show();
                objsearchPhoto.LoadData();
            }
            else
            {
                MessageBox.Show("Please set the Substore", "i-Mix", MessageBoxButton.OK, MessageBoxImage.Stop);
            }
        }

        /// <summary>
        /// Handles the Click event of the btnManage control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void btnManage_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                (new ManageHome()).Show();
                this.Close();
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
            }
        }

        /// <summary>
        /// Handles the Click event of the btncheckcd control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void btncheckcd_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                (new ImagePreviewer()).Show();
                this.Close();
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
            }
        }

        public void AutoCount8810()
        {
            TotalCount8810 = 0;
            foreach (ImixPOSDetail objPosDetail in lstPosDetail)
            {
                try
                {
                    string url = "http://" + objPosDetail.SystemName + ":8000/GEt8810PrintersInfo";
                    var client = new WebClient();
                    string str8810Printer = client.DownloadString(url);

                    lst8810Printer = serialize.DeserializeXML<List<Printer8810>>(str8810Printer);
                    if (lst8810Printer.Count == 0)
                    {
                        ErrorHandler.ErrorHandler.LogFileWrite("No Printer Found by Kodak SDK");
                    }
                    else
                    {
                        foreach (Printer8810 objPrinter in lst8810Printer)
                        {
                            if (objPrinter.ErrorMessage != "")
                            {
                                ErrorHandler.ErrorHandler.LogFileWrite(objPrinter.ErrorMessage);
                            }
                            else
                            {
                                TotalCount8810 = TotalCount8810 + objPrinter.ImageCount;
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    ErrorHandler.ErrorHandler.LogError(ex);
                }
            }
        }

        public void AutoCount6850()
        {
            TotalCount6850 = 0;
            foreach (ImixPOSDetail objPosDetail in lstPosDetail)
            {
                try
                {
                    //here i will put the loop for printer server means i will get all the details from the  machine 
                    //here i will pass the logical substore id and will get all the name of the physical associated printservernames

                    string url = "http://" + objPosDetail.SystemName + ":8000/Get6850PrintersInfo";
                    var client = new WebClient();
                    string str6850Printer = client.DownloadString(url);

                    lst6850Printer = serialize.DeserializeXML<List<Printer6850>>(str6850Printer);

                    if (lst6850Printer.Count == 0)
                    {
                        ErrorHandler.ErrorHandler.LogFileWrite("No Printer Found by Kodak SDK");
                    }
                    else
                    {
                        foreach (Printer6850 objPrinter in lst6850Printer)
                        {
                            if (objPrinter.ErrorMessage != "")
                            {
                                ErrorHandler.ErrorHandler.LogFileWrite(objPrinter.ErrorMessage);
                            }
                            else
                            {
                                TotalCount6850 = objPrinter.ImageCount;
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    ErrorHandler.ErrorHandler.LogError(ex);
                }
            }
        }

        public void GetPrintServerDetails()
        {
            lstPosDetail = (new SageBusiness()).GetPrintServerDetails(LoginUser.SubStoreId);
        }
        //ashu code for format drive
        private void btnFormatDrive_Click(object sender, RoutedEventArgs e)
        {
            try
            {

                var drives = from drive in DriveInfo.GetDrives()
                             where drive.DriveType == DriveType.Removable && drive.IsReady == true
                             select drive;
                if (drives.Count() > 0)
                {
                    if (MessageBox.Show("Do you want to Format drive(s)?", "Confirm Format drive(s)", MessageBoxButton.YesNo) == MessageBoxResult.Yes)
                    {
                        foreach (var drivesitem in drives)
                        {

                            System.IO.DirectoryInfo di = new DirectoryInfo(drivesitem.Name);

                            foreach (FileInfo file in di.GetFiles())
                            {
                                file.Delete();
                            }
                            foreach (DirectoryInfo dir in di.GetDirectories())
                            {
                                dir.Delete(true);
                            }

                            MessageBox.Show(drivesitem.Name + " drive Formated successfully.");
                        }
                    }
                }
                else
                {
                    MessageBox.Show("No drive(s) find.");
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show("Error while drive Formating.");
                ErrorHandler.ErrorHandler.LogError(ex);
            }
        }
    }
    public static class serialize
    {
        public static string SerializeObject<T>(this T toSerialize)
        {
            XmlSerializer xmlSerializer = new XmlSerializer(toSerialize.GetType());
            StringWriter textWriter = new StringWriter();
            xmlSerializer.Serialize(textWriter, toSerialize);
            return textWriter.ToString().Replace("<?xml version=\"1.0\" encoding=\"utf-16\"?>", "");
        }

        public static T DeserializeXML<T>(this string xmlString)
        {
            xmlString = xmlString.Replace("xmlns=\"http://schemas.datacontract.org/2004/07/Digiphoto.PrinterSDKWrapper\" xmlns:i=\"http://www.w3.org/2001/XMLSchema-instance\"", "");
            T returnValue = default(T);
            XmlSerializer serial = new XmlSerializer(typeof(T));
            StringReader reader = new StringReader(xmlString);
            object result = serial.Deserialize(reader);
            if (result != null && result is T)
            {
                returnValue = ((T)result);
            }
            reader.Close();
            return returnValue;
        }
    }
}
