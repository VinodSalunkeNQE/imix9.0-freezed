﻿#pragma checksum "..\..\..\UserControl\PopupKeyboard.xaml" "{8829d00f-11b8-4213-878b-770e8597ac16}" "0D146ACF852BBA3658BB955E83925503FA7CB577F7557FFA59E0CEF6741303D8"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using RootLibrary.WPF.Localization;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms.Integration;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace DigiPhoto {
    
    
    internal partial class PopupKeyboardUserControl : System.Windows.Controls.UserControl, System.Windows.Markup.IComponentConnector {
        
        
        #line 8 "..\..\..\UserControl\PopupKeyboard.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal DigiPhoto.PopupKeyboardUserControl UserControl;
        
        #line default
        #line hidden
        
        
        #line 78 "..\..\..\UserControl\PopupKeyboard.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Border bdMainBorder;
        
        #line default
        #line hidden
        
        
        #line 94 "..\..\..\UserControl\PopupKeyboard.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Border bdKeyboard;
        
        #line default
        #line hidden
        
        
        #line 104 "..\..\..\UserControl\PopupKeyboard.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid grdNumericKeyboard;
        
        #line default
        #line hidden
        
        
        #line 119 "..\..\..\UserControl\PopupKeyboard.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btn010100;
        
        #line default
        #line hidden
        
        
        #line 122 "..\..\..\UserControl\PopupKeyboard.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btn010101;
        
        #line default
        #line hidden
        
        
        #line 125 "..\..\..\UserControl\PopupKeyboard.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btn010102;
        
        #line default
        #line hidden
        
        
        #line 128 "..\..\..\UserControl\PopupKeyboard.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btn010103;
        
        #line default
        #line hidden
        
        
        #line 139 "..\..\..\UserControl\PopupKeyboard.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btn010200;
        
        #line default
        #line hidden
        
        
        #line 142 "..\..\..\UserControl\PopupKeyboard.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btn010201;
        
        #line default
        #line hidden
        
        
        #line 145 "..\..\..\UserControl\PopupKeyboard.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btn010202;
        
        #line default
        #line hidden
        
        
        #line 149 "..\..\..\UserControl\PopupKeyboard.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btn010203;
        
        #line default
        #line hidden
        
        
        #line 170 "..\..\..\UserControl\PopupKeyboard.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btn010300;
        
        #line default
        #line hidden
        
        
        #line 173 "..\..\..\UserControl\PopupKeyboard.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btn010301;
        
        #line default
        #line hidden
        
        
        #line 176 "..\..\..\UserControl\PopupKeyboard.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btn010302;
        
        #line default
        #line hidden
        
        
        #line 180 "..\..\..\UserControl\PopupKeyboard.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btn010303;
        
        #line default
        #line hidden
        
        
        #line 208 "..\..\..\UserControl\PopupKeyboard.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btn010400;
        
        #line default
        #line hidden
        
        
        #line 211 "..\..\..\UserControl\PopupKeyboard.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btn010401;
        
        #line default
        #line hidden
        
        
        #line 215 "..\..\..\UserControl\PopupKeyboard.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btn010402;
        
        #line default
        #line hidden
        
        
        #line 234 "..\..\..\UserControl\PopupKeyboard.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnCloseButton;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/DigiPhoto;component/usercontrol/popupkeyboard.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\..\UserControl\PopupKeyboard.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.UserControl = ((DigiPhoto.PopupKeyboardUserControl)(target));
            return;
            case 2:
            this.bdMainBorder = ((System.Windows.Controls.Border)(target));
            return;
            case 3:
            this.bdKeyboard = ((System.Windows.Controls.Border)(target));
            return;
            case 4:
            this.grdNumericKeyboard = ((System.Windows.Controls.Grid)(target));
            return;
            case 5:
            this.btn010100 = ((System.Windows.Controls.Button)(target));
            
            #line 119 "..\..\..\UserControl\PopupKeyboard.xaml"
            this.btn010100.Click += new System.Windows.RoutedEventHandler(this.cmdNumericButton_Click);
            
            #line default
            #line hidden
            return;
            case 6:
            this.btn010101 = ((System.Windows.Controls.Button)(target));
            
            #line 122 "..\..\..\UserControl\PopupKeyboard.xaml"
            this.btn010101.Click += new System.Windows.RoutedEventHandler(this.cmdNumericButton_Click);
            
            #line default
            #line hidden
            return;
            case 7:
            this.btn010102 = ((System.Windows.Controls.Button)(target));
            
            #line 125 "..\..\..\UserControl\PopupKeyboard.xaml"
            this.btn010102.Click += new System.Windows.RoutedEventHandler(this.cmdNumericButton_Click);
            
            #line default
            #line hidden
            return;
            case 8:
            this.btn010103 = ((System.Windows.Controls.Button)(target));
            
            #line 128 "..\..\..\UserControl\PopupKeyboard.xaml"
            this.btn010103.Click += new System.Windows.RoutedEventHandler(this.cmdNumericButton_Click);
            
            #line default
            #line hidden
            return;
            case 9:
            this.btn010200 = ((System.Windows.Controls.Button)(target));
            
            #line 139 "..\..\..\UserControl\PopupKeyboard.xaml"
            this.btn010200.Click += new System.Windows.RoutedEventHandler(this.cmdNumericButton_Click);
            
            #line default
            #line hidden
            return;
            case 10:
            this.btn010201 = ((System.Windows.Controls.Button)(target));
            
            #line 142 "..\..\..\UserControl\PopupKeyboard.xaml"
            this.btn010201.Click += new System.Windows.RoutedEventHandler(this.cmdNumericButton_Click);
            
            #line default
            #line hidden
            return;
            case 11:
            this.btn010202 = ((System.Windows.Controls.Button)(target));
            
            #line 145 "..\..\..\UserControl\PopupKeyboard.xaml"
            this.btn010202.Click += new System.Windows.RoutedEventHandler(this.cmdNumericButton_Click);
            
            #line default
            #line hidden
            return;
            case 12:
            this.btn010203 = ((System.Windows.Controls.Button)(target));
            
            #line 149 "..\..\..\UserControl\PopupKeyboard.xaml"
            this.btn010203.Click += new System.Windows.RoutedEventHandler(this.cmdNumericButton_Click);
            
            #line default
            #line hidden
            return;
            case 13:
            this.btn010300 = ((System.Windows.Controls.Button)(target));
            
            #line 170 "..\..\..\UserControl\PopupKeyboard.xaml"
            this.btn010300.Click += new System.Windows.RoutedEventHandler(this.cmdNumericButton_Click);
            
            #line default
            #line hidden
            return;
            case 14:
            this.btn010301 = ((System.Windows.Controls.Button)(target));
            
            #line 173 "..\..\..\UserControl\PopupKeyboard.xaml"
            this.btn010301.Click += new System.Windows.RoutedEventHandler(this.cmdNumericButton_Click);
            
            #line default
            #line hidden
            return;
            case 15:
            this.btn010302 = ((System.Windows.Controls.Button)(target));
            
            #line 176 "..\..\..\UserControl\PopupKeyboard.xaml"
            this.btn010302.Click += new System.Windows.RoutedEventHandler(this.cmdNumericButton_Click);
            
            #line default
            #line hidden
            return;
            case 16:
            this.btn010303 = ((System.Windows.Controls.Button)(target));
            
            #line 180 "..\..\..\UserControl\PopupKeyboard.xaml"
            this.btn010303.Click += new System.Windows.RoutedEventHandler(this.cmdNumericButton_Click);
            
            #line default
            #line hidden
            return;
            case 17:
            this.btn010400 = ((System.Windows.Controls.Button)(target));
            
            #line 208 "..\..\..\UserControl\PopupKeyboard.xaml"
            this.btn010400.Click += new System.Windows.RoutedEventHandler(this.cmdNumericButton_Click);
            
            #line default
            #line hidden
            return;
            case 18:
            this.btn010401 = ((System.Windows.Controls.Button)(target));
            
            #line 211 "..\..\..\UserControl\PopupKeyboard.xaml"
            this.btn010401.Click += new System.Windows.RoutedEventHandler(this.cmdNumericButton_Click);
            
            #line default
            #line hidden
            return;
            case 19:
            this.btn010402 = ((System.Windows.Controls.Button)(target));
            
            #line 215 "..\..\..\UserControl\PopupKeyboard.xaml"
            this.btn010402.Click += new System.Windows.RoutedEventHandler(this.cmdNumericButton_Click);
            
            #line default
            #line hidden
            return;
            case 20:
            this.btnCloseButton = ((System.Windows.Controls.Button)(target));
            
            #line 239 "..\..\..\UserControl\PopupKeyboard.xaml"
            this.btnCloseButton.Click += new System.Windows.RoutedEventHandler(this.CloseButton_Click);
            
            #line default
            #line hidden
            return;
            }
            this._contentLoaded = true;
        }
    }
}

