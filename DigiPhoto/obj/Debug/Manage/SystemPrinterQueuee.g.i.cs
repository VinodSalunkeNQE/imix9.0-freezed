﻿#pragma checksum "..\..\..\Manage\SystemPrinterQueuee.xaml" "{8829d00f-11b8-4213-878b-770e8597ac16}" "4ADF7F6FB8A98275D95D87809C52AFFCE49C88A9864FC8B648135992B2AC6D77"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using DigiPhoto;
using DigiPhoto.Manage;
using RootLibrary.WPF.Localization;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms.Integration;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace DigiPhoto.Manage {
    
    
    /// <summary>
    /// SystemPrinterQueuee
    /// </summary>
    public partial class SystemPrinterQueuee : System.Windows.Window, System.Windows.Markup.IComponentConnector, System.Windows.Markup.IStyleConnector {
        
        
        #line 119 "..\..\..\Manage\SystemPrinterQueuee.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid LayoutRoot;
        
        #line default
        #line hidden
        
        
        #line 126 "..\..\..\Manage\SystemPrinterQueuee.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.StackPanel bgContainer;
        
        #line default
        #line hidden
        
        
        #line 127 "..\..\..\Manage\SystemPrinterQueuee.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Viewbox vb2;
        
        #line default
        #line hidden
        
        
        #line 128 "..\..\..\Manage\SystemPrinterQueuee.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.StackPanel bg_withlogo1;
        
        #line default
        #line hidden
        
        
        #line 129 "..\..\..\Manage\SystemPrinterQueuee.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Image bg1;
        
        #line default
        #line hidden
        
        
        #line 133 "..\..\..\Manage\SystemPrinterQueuee.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.StackPanel FooterredLine;
        
        #line default
        #line hidden
        
        
        #line 139 "..\..\..\Manage\SystemPrinterQueuee.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid ContentContainer;
        
        #line default
        #line hidden
        
        
        #line 146 "..\..\..\Manage\SystemPrinterQueuee.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid PageContent;
        
        #line default
        #line hidden
        
        
        #line 164 "..\..\..\Manage\SystemPrinterQueuee.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ListBox mylist;
        
        #line default
        #line hidden
        
        
        #line 172 "..\..\..\Manage\SystemPrinterQueuee.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnBack;
        
        #line default
        #line hidden
        
        
        #line 173 "..\..\..\Manage\SystemPrinterQueuee.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnReprint;
        
        #line default
        #line hidden
        
        
        #line 178 "..\..\..\Manage\SystemPrinterQueuee.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnUserName;
        
        #line default
        #line hidden
        
        
        #line 179 "..\..\..\Manage\SystemPrinterQueuee.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock txbUserName;
        
        #line default
        #line hidden
        
        
        #line 182 "..\..\..\Manage\SystemPrinterQueuee.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock txbStoreName;
        
        #line default
        #line hidden
        
        
        #line 183 "..\..\..\Manage\SystemPrinterQueuee.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnLogout;
        
        #line default
        #line hidden
        
        
        #line 190 "..\..\..\Manage\SystemPrinterQueuee.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal DigiPhoto.Re_Print ReprintCtrl;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/DigiPhoto;component/manage/systemprinterqueuee.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\..\Manage\SystemPrinterQueuee.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal System.Delegate _CreateDelegate(System.Type delegateType, string handler) {
            return System.Delegate.CreateDelegate(delegateType, this, handler);
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            
            #line 9 "..\..\..\Manage\SystemPrinterQueuee.xaml"
            ((DigiPhoto.Manage.SystemPrinterQueuee)(target)).Loaded += new System.Windows.RoutedEventHandler(this.Window_Loaded);
            
            #line default
            #line hidden
            return;
            case 3:
            this.LayoutRoot = ((System.Windows.Controls.Grid)(target));
            return;
            case 4:
            this.bgContainer = ((System.Windows.Controls.StackPanel)(target));
            return;
            case 5:
            this.vb2 = ((System.Windows.Controls.Viewbox)(target));
            return;
            case 6:
            this.bg_withlogo1 = ((System.Windows.Controls.StackPanel)(target));
            return;
            case 7:
            this.bg1 = ((System.Windows.Controls.Image)(target));
            return;
            case 8:
            this.FooterredLine = ((System.Windows.Controls.StackPanel)(target));
            return;
            case 9:
            this.ContentContainer = ((System.Windows.Controls.Grid)(target));
            return;
            case 10:
            this.PageContent = ((System.Windows.Controls.Grid)(target));
            return;
            case 11:
            this.mylist = ((System.Windows.Controls.ListBox)(target));
            return;
            case 12:
            this.btnBack = ((System.Windows.Controls.Button)(target));
            
            #line 172 "..\..\..\Manage\SystemPrinterQueuee.xaml"
            this.btnBack.Click += new System.Windows.RoutedEventHandler(this.btnBack_Click);
            
            #line default
            #line hidden
            return;
            case 13:
            this.btnReprint = ((System.Windows.Controls.Button)(target));
            
            #line 173 "..\..\..\Manage\SystemPrinterQueuee.xaml"
            this.btnReprint.Click += new System.Windows.RoutedEventHandler(this.btnReprint_Click);
            
            #line default
            #line hidden
            return;
            case 14:
            this.btnUserName = ((System.Windows.Controls.Button)(target));
            return;
            case 15:
            this.txbUserName = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 16:
            this.txbStoreName = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 17:
            this.btnLogout = ((System.Windows.Controls.Button)(target));
            
            #line 183 "..\..\..\Manage\SystemPrinterQueuee.xaml"
            this.btnLogout.Click += new System.Windows.RoutedEventHandler(this.btnLogout_Click);
            
            #line default
            #line hidden
            return;
            case 18:
            this.ReprintCtrl = ((DigiPhoto.Re_Print)(target));
            return;
            }
            this._contentLoaded = true;
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        void System.Windows.Markup.IStyleConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 2:
            
            #line 29 "..\..\..\Manage\SystemPrinterQueuee.xaml"
            ((System.Windows.Input.CommandBinding)(target)).CanExecute += new System.Windows.Input.CanExecuteRoutedEventHandler(this.CommandBinding_CanExecute);
            
            #line default
            #line hidden
            
            #line 29 "..\..\..\Manage\SystemPrinterQueuee.xaml"
            ((System.Windows.Input.CommandBinding)(target)).Executed += new System.Windows.Input.ExecutedRoutedEventHandler(this.CommandBinding_Executed);
            
            #line default
            #line hidden
            break;
            }
        }
    }
}

