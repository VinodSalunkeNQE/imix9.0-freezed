﻿#pragma checksum "..\..\..\..\Manage\PrintLog.xaml" "{8829d00f-11b8-4213-878b-770e8597ac16}" "A21C57EAB24A6E892E7CFDA5E8FC3248AE8BA72C2342855A12117FA4AFF76B48"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using RootLibrary.WPF.Localization;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms.Integration;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace DigiPhoto.Manage {
    
    
    /// <summary>
    /// PrintLog
    /// </summary>
    public partial class PrintLog : System.Windows.Window, System.Windows.Markup.IComponentConnector {
        
        
        #line 13 "..\..\..\..\Manage\PrintLog.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.StackPanel bgContainer;
        
        #line default
        #line hidden
        
        
        #line 14 "..\..\..\..\Manage\PrintLog.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Viewbox vb2;
        
        #line default
        #line hidden
        
        
        #line 15 "..\..\..\..\Manage\PrintLog.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.StackPanel bg_withlogo1;
        
        #line default
        #line hidden
        
        
        #line 16 "..\..\..\..\Manage\PrintLog.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Image bg1;
        
        #line default
        #line hidden
        
        
        #line 20 "..\..\..\..\Manage\PrintLog.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DataGrid DGPrintLog;
        
        #line default
        #line hidden
        
        
        #line 32 "..\..\..\..\Manage\PrintLog.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnExport;
        
        #line default
        #line hidden
        
        
        #line 33 "..\..\..\..\Manage\PrintLog.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.StackPanel FooterredLine;
        
        #line default
        #line hidden
        
        
        #line 38 "..\..\..\..\Manage\PrintLog.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnback;
        
        #line default
        #line hidden
        
        
        #line 45 "..\..\..\..\Manage\PrintLog.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnUserName;
        
        #line default
        #line hidden
        
        
        #line 46 "..\..\..\..\Manage\PrintLog.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock txbUserName;
        
        #line default
        #line hidden
        
        
        #line 49 "..\..\..\..\Manage\PrintLog.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock txbStoreName;
        
        #line default
        #line hidden
        
        
        #line 50 "..\..\..\..\Manage\PrintLog.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnLogout;
        
        #line default
        #line hidden
        
        
        #line 57 "..\..\..\..\Manage\PrintLog.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DatePicker dtselect;
        
        #line default
        #line hidden
        
        
        #line 67 "..\..\..\..\Manage\PrintLog.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox cmbPhotographer;
        
        #line default
        #line hidden
        
        
        #line 68 "..\..\..\..\Manage\PrintLog.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnSearch;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/DigiPhoto;component/manage/printlog.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\..\..\Manage\PrintLog.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.bgContainer = ((System.Windows.Controls.StackPanel)(target));
            return;
            case 2:
            this.vb2 = ((System.Windows.Controls.Viewbox)(target));
            return;
            case 3:
            this.bg_withlogo1 = ((System.Windows.Controls.StackPanel)(target));
            return;
            case 4:
            this.bg1 = ((System.Windows.Controls.Image)(target));
            return;
            case 5:
            this.DGPrintLog = ((System.Windows.Controls.DataGrid)(target));
            return;
            case 6:
            this.btnExport = ((System.Windows.Controls.Button)(target));
            return;
            case 7:
            this.FooterredLine = ((System.Windows.Controls.StackPanel)(target));
            return;
            case 8:
            this.btnback = ((System.Windows.Controls.Button)(target));
            
            #line 38 "..\..\..\..\Manage\PrintLog.xaml"
            this.btnback.Click += new System.Windows.RoutedEventHandler(this.btnback_Click);
            
            #line default
            #line hidden
            return;
            case 9:
            this.btnUserName = ((System.Windows.Controls.Button)(target));
            return;
            case 10:
            this.txbUserName = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 11:
            this.txbStoreName = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 12:
            this.btnLogout = ((System.Windows.Controls.Button)(target));
            
            #line 50 "..\..\..\..\Manage\PrintLog.xaml"
            this.btnLogout.Click += new System.Windows.RoutedEventHandler(this.btnLogout_Click);
            
            #line default
            #line hidden
            return;
            case 13:
            this.dtselect = ((System.Windows.Controls.DatePicker)(target));
            return;
            case 14:
            this.cmbPhotographer = ((System.Windows.Controls.ComboBox)(target));
            return;
            case 15:
            this.btnSearch = ((System.Windows.Controls.Button)(target));
            
            #line 68 "..\..\..\..\Manage\PrintLog.xaml"
            this.btnSearch.Click += new System.Windows.RoutedEventHandler(this.btnSearch_Click);
            
            #line default
            #line hidden
            return;
            }
            this._contentLoaded = true;
        }
    }
}

