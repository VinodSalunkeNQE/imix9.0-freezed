﻿using Baracoda.Cameleon.PC.Common;
using Baracoda.Cameleon.PC.Modularity.Connectivity;
using Baracoda.Cameleon.PC.Readers;
using Baracoda.Cameleon.PC.Readers.DataParsers;
using System;
using System.Linq;
using System.Collections.Generic;
using Baracoda;
using Baracoda.Cameleon.PC.Modularity.GuiFeatures;
using System.Windows.Threading;
using FrameworkHelper.RfidLib;

namespace DigiPhoto
{
    public class MainModel : SdkModelBase
    {
        /// <summary>
        /// Creates new instance of this class
        /// </summary>
        public MainModel()
        {
            InitUsb();
        }

        /// <summary>
        /// Is Usb available?
        /// </summary>
        public bool IsUsbAvailable
        {
            get { return isUsbAvailable; }
            set
            {
                if (isUsbAvailable == value) return;
                isUsbAvailable = value;
                SendPropertyChanged(() => IsUsbAvailable);
            }
        }

        void InitUsb()
        {
            IsBusy = true;
            // initialize USB manager
            UsbManager.CreateAndInitialize(OnUsbInit);
        }

        void OnUsbInit()
        {
            var usbMan = UsbManager.Instance;

            IsUsbAvailable = usbMan.IsUsbAvailable;

            if (usbMan.IsUsbAvailable)
            {
                try
                {
                    var usbAuto = usbMan.UsbRepository.Devices;
                    //if (usbAuto.Where(o => registeredDevices.Contains(o.Id)).Count() > 0)
                    //{
                    //    CloseForced();
                    //}
                    //else
                    //{
                    usbMan.UsbRepository.UsbDevicesStateChanged += OnUsbDevicesChanged;
                    if (usbAuto != null && usbAuto.Count > 0)
                    {
                        usbAuto.ApplyAction(x =>
                        {
                            var usbReader = new BaracodaUsbReader(x);
                            //if (registeredDevices.Contains(usbReader.Retrieved.SerialNumber))
                            //{
                            //usbReader.Retrieved.SerialNumber
                            Readers.Add(usbReader);
                            //usbReader.RawDataReceived += OnRawDataReceived;
                            usbReader.DataReceived += OnDataReceived;
                            usbReader.ConnectionStateChanged += OnConnectionStateChanged;
                            //}
                        });
                    }
                    //}
                }
                catch (Exception x)
                {
                    OnException(x);
                }
            }
            IsBusy = false;
        }
        void OnDataReceived(object sender, DataReceivedEventArgs e)
        {
            var device = (BaracodaReaderBase)sender;
            if (device == null)
                return;

            if (e.NeedsAck)
                device.AckData(true, e.Id);

            var item = new DataContainer
            {
                AckId = e.Id.ToString(),
                Id = device.Retrieved.SerialNumber,// device.BaseDevice.Id.ToString(),
                Time = e.ReceivedTime,
                Content = e.Text
            };
            var data = item;
            var ev = DataReceived;
            if (ev != null)
                ev(device, new DataEventArgs { RfidData = item });
            //s += item.Id + ":-" + item.Content;

            //OnGui.Run(() => DataList.Add(item));
        }

        void RaiseRefreshNeeded()
        {
            // notify that the list should be refreshed
            var ev = ListRefreshNeeded;
            if (ev != null)
                ev(this, EventArgs.Empty);
        }

        void OnConnectionStateChanged(object sender, ConnectionStateEventArgs e)
        {
            RaiseRefreshNeeded();
        }

        void OnUsbDevicesChanged(object sender, UsbDevicesStateChangedEventArgs e)
        {
            // remove readers from the list of readers
            if (e.RemovedDevices != null)
            {
                var toRemove = Readers.Where(x => e.RemovedDevices.Any(d => d.Id == x.Id)).ToList();
                Readers.RemoveRange(toRemove);
                foreach (var reader in toRemove)
                {
                    reader.ConnectionStateChanged -= OnConnectionStateChanged;
                    reader.DataReceived -= OnDataReceived;
                    Readers.Remove(reader);
                }
            }

            // add readers to the list of readers
            if (e.AddedDevices != null)
            {
                var added = e.AddedDevices.Select(d => new BaracodaUsbReader(d));
                foreach (var reader in added)
                {
                    Readers.Add(reader);
                    reader.ConnectionStateChanged += OnConnectionStateChanged;
                    reader.DataReceived += OnDataReceived;
                }
            }
            RaiseRefreshNeeded();
        }

        /// <summary>
        /// Occurs when list refresh is needed
        /// </summary>
        public event EventHandler ListRefreshNeeded;
        public event EventHandler<DataEventArgs> DataReceived;

        /// <summary>
        /// Forces this model to release resources
        /// </summary>
        public new void CloseForced()
        {
            UsbManager.Instance.Dispose();
        }

        private bool isUsbAvailable = true;

    }

    public class DataEventArgs : EventArgs
    {
        /// <summary>
        /// The list of added readers
        /// </summary>
        public DataContainer RfidData { get; set; }
    }
}
