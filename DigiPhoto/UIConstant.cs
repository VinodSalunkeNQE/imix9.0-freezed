﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DigiPhoto
{
    internal class UIConstant
    {
        public static readonly string MobileNumberNumeric = "Mobile number can only be numeric";
        public static readonly string EmptyPassword = "Please enter password";
        public static readonly string MinimunPassword = "Password must be of minimum 6 characters";
        public static readonly string MaximumPassword = "Password must be of maximum 12 characters";
        public static readonly string ValidEmail = "Please Enter a valid email";
        public static readonly string AlphanumericPassword = "Password must be alphanumeric";
        public static readonly string NotAuthorized = "You are not authorized! please contact admin";
        public static readonly string UsernameAlreadyExists = "Username already exists.";
        public static readonly string UserDeleted = "User has been deleted";
        public static readonly string RecordSuccessfullyUpdated = "Record has been successfully updated";
        public static readonly string PleaseCheckAllEnteredInformation = "Please check all entered information";
        public static readonly string NoResultFound = "No result found";
        public static readonly string Error = "Error";
        public static readonly string SelectSubstoreAndLocation= "Please select a substore and a selected location";
        public static readonly string SelectSubstore= "Please select a substore";
        public static readonly string SubstoreSavedSuccessfully = "Substore info saved successfully";
        public static readonly string SelectFolderFirst= "Please select the folder first";
        public static readonly string ImagesArchivedSuccessfully= "Images Archived Successfully";
        public static readonly string ErrorinArchive= "Error in Archive ";
        public static readonly string AlredyArchivedImages = "No file(s) to archive, You have already archived the images!";
        public static readonly string DeviceUpdatedSuccessfully = "Device updated successfully.";
        public static readonly string ProblemInUpdatingDevice= "Some problem in updating the device.";
        public static readonly string PleaseEnterDeviceName= "Please enter Device Name.";
        public static readonly string PleaseEnterDeviceSerialNumber= "Please enter device Serial Number";
        public static readonly string PleaseSelectDeviceType= "Please select Device Type.";
        public static readonly string DeviceSerialNumberAlreadyExists= "Device serial number already exists.";
        public static readonly string DeviceDeletedSuccessfully= "Device deleted successfully.";
        public static readonly string TriedToAccessDeviceManager= "Tried to access Device Manager";
        public static readonly string TriedToAccessPrintManager= "Tried to access Print Manager";
        public static readonly string TriedToAccessAonfiguration= "Tried to access configuration";
        public static readonly string TriedToAccessUserManagement = "Tried to access User Management";
        public static readonly string TriedToAccessCashBox= "Tried to access cashBox";
        public static readonly string LocationSuccessfullyDeleted= "Location successfully deleted";
        public static readonly string LocationSavedSuccessfully= "Location saved successfully";
        public static readonly string LocationNameCannotBeBlank= "Location name cannot be blank";
        public static readonly string SelectPrinterType = "Select printer type.";
        public static readonly string SelectProductType= "Select product type.";
        public static readonly string RecordSavedSuccessfully= "Record saved successfully";
        public static readonly string DoYouWantToDeleteThisPrinter = "Do you want to delete this printer?";
        public static readonly string PrinterDeletedSuccessfully= "Printer deleted successfully";
        public static readonly string PrinterTypeCouldNotBeDeleted= "Printer type could not be deleted!";
        public static readonly string PrinterTypeSavedSuccessfull= "Printer type saved successfully.";
        public static readonly string PrinterTypeCouldNotBeSavedDueToSomeError= "Printer type could not be saved due to some error!";
        public static readonly string PrinterTypeKeywordCannotBeEmpty= "Printer type keyword cannot be empty!";
        public static readonly string PleaseEnterCardTypeFirst= "Please enter card type first.";
        public static readonly string PleaseEnterCustomerNameFirst= "Please enter customer name first.";
        public static readonly string PleaseEnterCardNumberFirst= "Please enter card number first.";
        public static readonly string PleaseEnterAmountFirst= "Please enter amount first.";

        public static readonly string requiredParam= "Please enter {0} first.";

    }
    internal enum ActionType
    {
        PreviewPhoto = 1,
        PreviewPhotoHighResolution = 2,
        ModeratePhoto = 3,
        EditPhoto = 4,
        UnlockPhoto = 5,
        DeleteCurrency = 7,
        EditCurrency = 8,
        ChangeCurrencyRate = 9,
        ChangeDefaultCurrency = 10,
        AddBorder = 11,
        RemoveBorder = 12,
        AddBackGround = 13,
        RemoveBackGround = 14,
        AddPrinter = 15,
        ChangePrinter = 16,
        DeletePrinter = 17,
        AddUser = 18,
        EditUser = 19,
        DeleteUser = 20,
        ChangeUserRole = 21,
        AddRole = 22,
        DeleteRole = 23,
        AddPermission = 24,
        DeletePermission = 25,
        AddProduct = 26,
        DeleteProduct = 27,
        EditProduct = 28,
        CreatePackage = 29,
        ChangePackage = 30,
        DeletePackage = 31,
        ChangePricing = 32,
        ChangeModeratePassword = 33,
        AllowPreview300 = 34,
        DisablePreview300 = 35,
        Login = 38,
        Logout = 39,
        RefundOrder = 40,
        ShutDown = 41,
        GenerateOrder = 42,
        CancelOrder = 43,
        RePrint = 44,
        Loggingtowindows = 46,
        AddEditSynchronizationData = 47,
        AddGraphics = 48,
        AddBackground = 49,
        //AddBorder = 50,
        PrinterAdded = 51,
        PrinterDeleted = 52,
        CreateRole = 54,
        //DeleteRole = 55,
        AddEditSpecPrintData = 56,
        DeleteGraphics = 57,
        DeleteBackground = 58,
        DeleteBorder = 59,
        AddSubStore = 60,
        EditSubStore = 61,
        ChangesinLocationAllocationtoSubstore = 62,
        MovePrint = 63,
        EditaPackage = 64,
        CashDrawer = 65,
    }
}
