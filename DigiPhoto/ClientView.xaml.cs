﻿using System;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Windows;
using System.Windows.Media.Imaging;
using System.IO;
using System.Windows.Controls;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using DigiPhoto.DataLayer;
using DigiPhoto.DataLayer.Model;
using DigiPhoto.Common;
using System.Collections.Generic;
using System.ServiceProcess;
using System.Windows.Media;
using DigiPhoto.IMIX.Business;
using DigiPhoto.IMIX.Model;
using FrameworkHelper;
using DigiPhoto.Utility.Repository.DirectoryInfoExt;
using MPLATFORMLib;
using MControls;
using System.Diagnostics;
using DigiAuditLogger;

namespace DigiPhoto
{
    /// <summary>
    /// Interaction logic for ClientView.xaml
    /// </summary>
    public partial class ClientView : Window
    {
        Timer tmr;
        int mktImgcount = 0;
        string retPath = "";
        public int _currentImageId = 0;
        public BitmapImage ChildImage
        {
            get;
            set;
        }

        public bool DefaultView
        {
            get;
            set;
        }
        public bool GroupView
        {
            get;
            set;
        }
        public string Photoname
        {
            get;
            set;
        }


        public string ProductType
        {
            get;
            set;
        }

        public ClientView()
        {
            InitializeComponent();
        }


        public Boolean Preview;
        string MktImgPath = string.Empty;
        int mktImgTime = 0;
        List<FileInfo> imgfilesInfo = null;
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
               // ErrorHandler.ErrorHandler.LogFileWrite("Client View  : Window Loaded : Step -1 : line :80  ");
                GetMktImgInfo();

                //this.imgDefaultBlur.Visibility = Visibility.Visible;
                this.WindowState = WindowState.Maximized;

                if (!Preview)
                {
                    if (!GroupView)
                    {
                        thumbPreview.Visibility = System.Windows.Visibility.Collapsed;
                        if (DefaultView)
                        {
                            //ErrorHandler.ErrorHandler.LogFileWrite(" Client View  : Window Loaded : Step -2 : line :93 ");
                            //imgDefault.Source = new BitmapImage(new Uri(@"/images/all_frames_new.png", UriKind.Relative));
                            imgDefault.Source = new BitmapImage(new Uri(@"/images/Clientimg.png", UriKind.Relative));
                            #region added by Ajay on 6 Mar 20 for blured background 
                            //imgDefaultBlur.Source = new BitmapImage(new Uri(@"/images/all_frames_new.png", UriKind.Relative));
                            imgDefaultBlur.Source = new BitmapImage(new Uri(@"/images/Clientimg.png", UriKind.Relative));

                            #endregion
                            //ErrorHandler.ErrorHandler.LogFileWrite("  ");
                            imgMain.Visibility = System.Windows.Visibility.Collapsed;
                            watermark.Visibility = System.Windows.Visibility.Collapsed;
                            //ErrorHandler.ErrorHandler.LogFileWrite(" Client View  : Window Loaded : Step -3 : line :93 Path :"+MktImgPath +"   Time :"+mktImgTime);
                            if (!(MktImgPath == "" || mktImgTime == 0))
                            {
                                instructionVideo.Visibility = System.Windows.Visibility.Visible;
                                #region added by Ajay on 6 Mar 20 for blured background
                                instructionVideoBlur.Visibility = System.Windows.Visibility.Visible;
                                #endregion

                                //ErrorHandler.ErrorHandler.LogFileWrite(" Client View  : Window Loaded : Step -4 : line :109 ");
                                StartSaver();
                                SetMediaSource(MktImgPath);
                            }
                            else
                            {
                                #region added by Ajay on 6 Mar 20 for blured background
                                imgDefaultBlur.Visibility = System.Windows.Visibility.Visible;
                                #endregion
                                imgDefault.Visibility = System.Windows.Visibility.Visible;
                            }
                        }
                    }
                    else
                    {
                        thumbPreview.Visibility = System.Windows.Visibility.Visible;
                        lstImages.Items.Clear();
                        foreach (var photoItem in RobotImageLoader.GroupImages)
                        {
                            lstImages.Items.Add(photoItem);
                        }
                        imgMain.Visibility = System.Windows.Visibility.Collapsed;
                        watermark.Visibility = System.Windows.Visibility.Collapsed;
                        imgDefault.Visibility = System.Windows.Visibility.Collapsed;
                        imgDefaultBlur.Visibility = System.Windows.Visibility.Collapsed;

                        if (tmr.Enabled)
                            tmr.Stop();
                        instructionVideo.Visibility = System.Windows.Visibility.Collapsed;
                        instructionVideoBlur.Visibility = System.Windows.Visibility.Collapsed;
                    }
                }
                else
                {
                    thumbPreview.Visibility = System.Windows.Visibility.Visible;
                    lstImages.Items.Clear();
                    foreach (var photoItem in RobotImageLoader.GroupImages)
                    {
                        lstImages.Items.Add(photoItem);
                    }
                    imgMain.Visibility = System.Windows.Visibility.Collapsed;
                    watermark.Visibility = System.Windows.Visibility.Collapsed;
                    imgDefault.Visibility = System.Windows.Visibility.Collapsed;
                    imgDefaultBlur.Visibility = System.Windows.Visibility.Collapsed;

                    if (tmr.Enabled)
                        tmr.Stop();
                    instructionVideo.Visibility = System.Windows.Visibility.Collapsed;
                    instructionVideoBlur.Visibility = System.Windows.Visibility.Collapsed;
                }
                // this.WindowState = System.Windows.WindowState.Maximized;
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }

        private void StartSaver()
        {
           // ErrorHandler.ErrorHandler.LogFileWrite(" Client View  : Start Saver : Step -1 : line :152 ");
            tmr = new Timer();
            tmr.Interval = mktImgTime;
            tmr.Tick += new EventHandler(tmr_Tick);
        }

        void tmr_Tick(object sender, EventArgs e)
        {
            SetMediaSource(MktImgPath);
        }

        private void SetMediaSource(string mSource)
        {
            try
            {
                //ErrorHandler.ErrorHandler.LogFileWrite(" Client View  : Set Media Source : Step -1 : line :165 ");
                PlayMediaNow(mSource);

                instructionVideo.Source = new Uri(retPath, UriKind.Relative);
                #region added by Ajay on 6 Mar 20 for blured background
                instructionVideoBlur.Source = new Uri(retPath, UriKind.Relative);
                #endregion
                string exten = System.IO.Path.GetExtension(instructionVideo.Source.ToString());

                #region added by Ajay on 6 Mar 20 for blured background
                string extenBlur = System.IO.Path.GetExtension(instructionVideoBlur.Source.ToString());
                #endregion

                if (string.Compare(exten, ".JPG", StringComparison.CurrentCultureIgnoreCase) != 0 &&
                    string.Compare(exten, ".PNG", StringComparison.CurrentCultureIgnoreCase) != 0)
                {
                    if (tmr.Enabled)
                    {
                        tmr.Stop();
                    }
                }
                //ErrorHandler.ErrorHandler.LogFileWrite(" Client View  : Set Media Source : Step -2 : line :189 ");
                if (string.Compare(extenBlur, ".JPG", StringComparison.CurrentCultureIgnoreCase) != 0 &&
                  string.Compare(extenBlur, ".PNG", StringComparison.CurrentCultureIgnoreCase) != 0)
                {
                    if (tmr.Enabled)
                    {
                        tmr.Stop();
                    }
                }
                instructionVideo.Play();
                #region added by Ajay on 6 Mar 20 for blured background
                instructionVideoBlur.Play();
                #endregion
                if (string.Compare(exten, ".JPG", StringComparison.CurrentCultureIgnoreCase) == 0 &&
                    string.Compare(exten, ".PNG", StringComparison.CurrentCultureIgnoreCase) == 0)
                {
                    if (tmr.Enabled == false)
                    {
                        tmr.Start();
                    }
                }

                #region added by Ajay on 6 Mar 20 for blured background
                if (string.Compare(extenBlur, ".JPG", StringComparison.CurrentCultureIgnoreCase) == 0 &&
                       string.Compare(extenBlur, ".PNG", StringComparison.CurrentCultureIgnoreCase) == 0)
                {
                    if (tmr.Enabled == false)
                    {
                        tmr.Start();
                    }
                }
                #endregion
                //ErrorHandler.ErrorHandler.LogFileWrite(" Client View  : Set Media Source : Step -3 : line :221 ");
            }
            catch (Exception ex)
            {

            }
        }

        private void instructionVideo_MediaEnded(object sender, RoutedEventArgs e)
        {
            if (!tmr.Enabled)
            {
                tmr.Start();
            }
        }

        #region added by Ajay on 6 Mar 20 for blured background
        private void instructionVideoblur_MediaEnded(object sender, RoutedEventArgs e)
        {
            if (!tmr.Enabled)
            {
                tmr.Start();
            }
        }
        #endregion



        private void PlayMediaNow(string clrDirectoryPath)
        {
            try
            {
                //ErrorHandler.ErrorHandler.LogFileWrite(" Client View  : Play Media Now : Step -1 : line :253 ");
                if (Directory.Exists(clrDirectoryPath))
                {
                    var myDirInfo = new DirectoryInfo(clrDirectoryPath);
                    imgfilesInfo = myDirInfo.GetFilesByExtensions(".jpg", ".JPG", ".png", ".PNG", ".mp4",
                                                    ".wmv", ".mpg", ".avi", ".3gp").ToList();

                    if (mktImgcount == imgfilesInfo.Count)
                    {
                        mktImgcount = 0;
                    }
                   // ErrorHandler.ErrorHandler.LogFileWrite(" Client View  : Play Media Now : Step -2 : line :264    mktCount :" + mktImgcount);
                    for (int k = mktImgcount; k < imgfilesInfo.Count; k++)
                    {
                        retPath = imgfilesInfo[k].FullName;
                        mktImgcount++;
                        break;
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }

        private void GetMktImgInfo()
        {
            try
            {
                //ErrorHandler.ErrorHandler.LogFileWrite(" Client View  : GetMktImgInfo : Step -1 : line :284 ");
                string subStoreId = "0";
                string line;
                using (var reader = new StreamReader(Environment.CurrentDirectory + "\\ss.dat"))
                {
                    line = reader.ReadLine();
                    subStoreId = CryptorEngine.Decrypt(line, true);
                    LoginUser.SubStoreId = Convert.ToInt32(subStoreId.Split(',')[0]);
                }
                //ErrorHandler.ErrorHandler.LogFileWrite(" Client View  : GetMktImgInfo : Step -2 : line :292 ");
                var objList = new List<long>() { (long)ConfigParams.MktImgPath, (long)ConfigParams.MktImgTimeInSec };

                var conObj = new ConfigBusiness();
                var configValuesList = conObj.GetNewConfigValues(LoginUser.SubStoreId).Where(o => objList.Contains(o.IMIXConfigurationMasterId)).ToList();
               // ErrorHandler.ErrorHandler.LogFileWrite(" Client View  : GetMktImgInfo : Step -3 : line :298 ");
                if (configValuesList != null || configValuesList.Count > 0)
                {
                    foreach (iMIXConfigurationInfo t in configValuesList)
                    {
                        switch (t.IMIXConfigurationMasterId)
                        {
                            case (int)ConfigParams.MktImgPath:
                                MktImgPath = t.ConfigurationValue;
                                break;
                            case (int)ConfigParams.MktImgTimeInSec:
                                mktImgTime = (t.ConfigurationValue != null ? Convert.ToInt32(t.ConfigurationValue) : 10) * 1000;
                                break;
                        }
                    }
                }
               // ErrorHandler.ErrorHandler.LogFileWrite(" Client View  : GetMktImgInfo : Step -4 : line :314");
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }

        private void ExitSlideShow_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
        private void btnSelectPrinter_Click(object sender, RoutedEventArgs e)
        {

        }
        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            try
            {
                var value = System.Windows.MessageBox.Show("Are you sure you want to close the application?", "Confirm shutdown",
                                           MessageBoxButton.YesNo, MessageBoxImage.Question).ToString();
                if (value == "Yes")
                {
                    StopWindowService("FontCache3.0.0.0");
                    Process thisProc = Process.GetCurrentProcess();
                    // Check how many total processes have the same name as the current one
                    if (Process.GetProcessesByName(thisProc.ProcessName).Length >= 1)
                    {
                        System.Windows.Application.Current.Shutdown();
                        thisProc.Kill();
                    }
                }
                else
                {
                    e.Cancel = true;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }

        #region Stop Window Service
        /// <summary>
        /// Stop the given window service if current status is running
        /// </summary>
        /// <param name="servicename"></param>
        private void StopWindowService(string servicename)
        {
            try
            {
                var myService = new ServiceController()
                {
                    ServiceName = servicename
                };

                myService.Refresh();

                string svcStatus = myService.Status.ToString();
                if (svcStatus == "Running")
                {
                    myService.Stop();
                }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }
        #endregion Stop Window Service
        public void SetEffect()
        {

        }
        private void btnNext_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (RobotImageLoader.GroupImages != null && RobotImageLoader.GroupImages.Count != 0)
                {
                    var myitem = RobotImageLoader.GroupImages.OrderByDescending(o => o.PhotoId).LastOrDefault(o => o.PhotoId > _currentImageId);

                    if (myitem != null)
                    {
                        LoadImage(myitem);
                        var mainitem = RobotImageLoader.PrintImages.FirstOrDefault(t => t.PhotoId == _currentImageId);
                        if (mainitem == null)
                        {
                            imgprintgroup.Source = new BitmapImage(new Uri(@"/images/print-group.png", UriKind.Relative));
                        }
                        else
                        {
                            imgprintgroup.Source = new BitmapImage(new Uri(@"/images/print-accept.png", UriKind.Relative));
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }
        public void LoadImage(LstMyItems myitem)
        {
            _currentImageId = myitem.PhotoId;
            string imgpath = myitem.FilePath.Replace("Thumbnails", "Thumbnails_Big");
            imgNext.Source = CommonUtility.GetImageFromPath(imgpath);
            imgNext.Visibility = System.Windows.Visibility.Visible;
            txtMainImage.Visibility = System.Windows.Visibility.Visible;
            txtMainImage.Text = myitem.Name;
            testR.Fill = null;
            testR.Visibility = System.Windows.Visibility.Collapsed;
            SearchResult window = null;
            foreach (Window wnd in System.Windows.Application.Current.Windows)
            {
                if (wnd.Title == "View/Order Station")
                {
                    window = (SearchResult)wnd;
                    break;
                }
            }
            mPreviewControl.SetControlledObject(window.gdMediaPlayer);
            window.lstImages.SelectedItem = myitem;
            if (myitem != null && !String.IsNullOrEmpty(myitem.Name))
                AuditLog.AddUserLog(Common.LoginUser.UserId, (int)FrameworkHelper.ActionType.PreviewPhoto, "Show Preview of " + ((DigiPhoto.LstMyItems)(myitem)).Name.ToString() + " image.", ((DigiPhoto.LstMyItems)(myitem)).PhotoId);
        }
        private void btnPrev_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (RobotImageLoader.GroupImages != null && RobotImageLoader.GroupImages.Count != 0)
                {
                    var myitem = RobotImageLoader.GroupImages.OrderByDescending(o => o.PhotoId).FirstOrDefault(o => o.PhotoId < _currentImageId);
                    if (myitem != null)
                    {
                        LoadImage(myitem);
                        var mainitem = RobotImageLoader.PrintImages.FirstOrDefault(t => t.PhotoId == _currentImageId);
                        if (mainitem == null)
                        {
                            imgprintgroup.Source = new BitmapImage(new Uri(@"/images/print-group.png", UriKind.Relative));
                        }
                        else
                        {
                            imgprintgroup.Source = new BitmapImage(new Uri(@"/images/print-accept.png", UriKind.Relative));
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }
        private BitmapImage GetImageFromPath(string path)
        {
            try
            {
                var bi = new BitmapImage();
                using (FileStream fileStream = File.OpenRead(path))
                {
                    var ms = new MemoryStream();
                    fileStream.CopyTo(ms);
                    ms.Seek(0, SeekOrigin.Begin);
                    fileStream.Close();
                    bi.CacheOption = BitmapCacheOption.OnLoad; // Memory 
                    bi.BeginInit();
                    bi.StreamSource = ms;
                    bi.EndInit();
                    bi.Freeze(); // Memory 
                    fileStream.Close();
                    ms.Flush();
                    ms.Close();
                    ms.Dispose();
                    return bi;
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
                return null;
            }
        }
        MFileSeeking mSeeking = new MFileSeeking();
        public MPreviewClass previewSecondary;
        public void PlayVideoOnClient(string Source, object mfile, int? VideoId = null)
        {
            try
            {
                gdMediaPlayer.Height = 440;
                gdMediaPlayer.Width = 500;
                if (Source == "VideoEditor")
                {
                    gdMediaPlayer.Margin = new Thickness(-100, 150, 0, -210);
                    gdMediaPlayer.Visibility = Visibility.Visible;
                }
                else
                {
                    gdMediaPlayer.Margin = new Thickness(-210, -50, 0, 0);
                    gdMediaPlayer.Visibility = Visibility.Visible;
                }
                if (Source == "FrameExtractionPreview")
                {
                    imgNext.Visibility = System.Windows.Visibility.Collapsed;
                    imgNext.Source = null;
                    txtMainImage.Visibility = System.Windows.Visibility.Visible;
                    stkPrint.Visibility = System.Windows.Visibility.Collapsed;
                    btnMinimize.Visibility = Visibility.Collapsed;
                    stkPrevNext.Visibility = Visibility.Collapsed;
                    testR.Visibility = System.Windows.Visibility.Collapsed;
                    gdMediaPlayer.Height = 550;
                    gdMediaPlayer.Width = 700;
                    gdMediaPlayer.Margin = new Thickness(0, 50, 0, 0);
                    txtMainImage.Text = VideoId.ToString();
                }
                if (previewSecondary == null)
                {
                    previewSecondary = new MPreviewClass();
                }
                previewSecondary.ObjectClose();
                previewSecondary.ObjectStart(mfile);
                previewSecondary.PreviewEnable("", 0, 1);
                mPreviewControl.SetControlledObject(previewSecondary);
            }
            catch
            { }
        }
        public void StopMediaPlay()
        {
            previewSecondary = null;
            gdMediaPlayer.Visibility = Visibility.Collapsed;
        }
        public void StopMediaPlay(bool? HideControl)
        {
            previewSecondary = null;
            gdMediaPlayer.Visibility = Visibility.Collapsed;
            if (HideControl == true)
            {
                testR.Visibility = System.Windows.Visibility.Visible;
                txtMainImage.Visibility = System.Windows.Visibility.Collapsed;
            }
        }
        private void Window_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            this.UpdateLayout();
            SearchResult window = System.Windows.Application.Current.Windows.Cast<Window>().Where(wnd => wnd.Title == "View/Order Station").Cast<SearchResult>().FirstOrDefault();

            if ((imgNext.Visibility == Visibility.Visible || testR.Fill != null) && window.isSingleScreenPreview)
            {
                var grouptestR = new TransformGroup();
                grouptestR.Children.Add(new RotateTransform(0));
                img12.RenderTransform = grouptestR;
                grouptestR.Children.Clear();
                grouptestR.Children.Add(new RotateTransform(180));
                grouptestR.Children.Add(new TranslateTransform(img12.ActualWidth, img12.ActualHeight));
                img12.RenderTransform = grouptestR;
            }
        }
        private void btnPrintGroup_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                LstMyItems curItem;
                ////////finding the image from the list for selection and preview
                var senderImage = (System.Windows.Controls.Image)btnPrintGroup.Content;
                curItem = RobotImageLoader.GroupImages.First(t => t.PhotoId == _currentImageId);
                SearchResult window = System.Windows.Application.Current.Windows.Cast<Window>().Where(wnd => wnd.Title == "View/Order Station").Cast<SearchResult>().FirstOrDefault();

                if (window == null) return;

                ////////Checking whether selected item already is in Print group or not               
                var mainitem = RobotImageLoader.PrintImages.FirstOrDefault(t => t.PhotoId == _currentImageId);
                if (mainitem == null)
                {
                    ////////if Item not exists adding item to printgroup collection
                    var myitem = new LstMyItems();
                    myitem = curItem;

                    var listItems = window.lstImages.Items;
                    foreach (LstMyItems item in listItems)
                    {
                        if (item.PhotoId == curItem.PhotoId)
                        {
                            window.lstImages.SelectedItem = item;
                            break;
                        }
                    }

                    var listBoxItem = (ListBoxItem)window.lstImages.ItemContainerGenerator.ContainerFromItem(window.lstImages.SelectedItem);
                    /////list image replace
                    if (listBoxItem != null)
                    {
                        myitem.FilePath = curItem.FilePath;
                        myitem.PrintGroup = new BitmapImage(new Uri(@"/images/print-accept.png", UriKind.Relative));
                        RobotImageLoader.PrintImages.Add(curItem);

                        senderImage.Source = new BitmapImage(new Uri(@"/images/print-accept.png", UriKind.Relative));
                        curItem.PrintGroup = new BitmapImage(new Uri(@"/images/print-accept.png", UriKind.Relative));

                        var myContentPresenter = FindVisualChild<ContentPresenter>(listBoxItem);
                        // Finding textBlock from the DataTemplate that is set on that ContentPresenter
                        DataTemplate myDataTemplate = myContentPresenter.ContentTemplate;

                        var myTextBlock = (Grid)myDataTemplate.FindName("grdMain", myContentPresenter);
                        ((System.Windows.Controls.Image)(((System.Windows.Controls.Button)(((System.Windows.Controls.Panel)((((Grid)(myTextBlock.FindName("Printbtns")))))).Children[0])).Content)).Source = null;
                        ((System.Windows.Controls.Image)(((System.Windows.Controls.Button)(((System.Windows.Controls.Panel)((((Grid)(myTextBlock.FindName("Printbtns")))))).Children[0])).Content)).Source = new BitmapImage(new Uri(@"/images/print-accept.png", UriKind.Relative));
                        ((System.Windows.Controls.Image)(((System.Windows.Controls.Button)(((System.Windows.Controls.Panel)((((Grid)(myTextBlock.FindName("Printbtns")))))).Children[0])).Content)).UpdateLayout();
                        window.imgprintgroup.Source = new BitmapImage(new Uri(@"/images/print-accept.png", UriKind.Relative));
                    }
                }
                else
                {
                    var listItems = window.lstImages.Items;
                    foreach (LstMyItems item in listItems)
                    {
                        if (item.PhotoId == curItem.PhotoId)
                        {
                            window.lstImages.SelectedItem = item;
                            break;
                        }
                    }

                    var listBoxItem = (ListBoxItem)window.lstImages.ItemContainerGenerator.ContainerFromItem(window.lstImages.SelectedItem);
                    /////list image replace
                    if (listBoxItem != null)
                    {
                        RobotImageLoader.PrintImages.Remove(mainitem);
                        senderImage.Source = new BitmapImage(new Uri(@"/images/print-group.png", UriKind.Relative));
                        curItem.PrintGroup = new BitmapImage(new Uri(@"/images/print-group.png", UriKind.Relative));
                        var myContentPresenter = FindVisualChild<ContentPresenter>(listBoxItem);
                        // Finding textBlock from the DataTemplate that is set on that ContentPresenter
                        DataTemplate myDataTemplate = myContentPresenter.ContentTemplate;
                        var myTextBlock = (Grid)myDataTemplate.FindName("grdMain", myContentPresenter);
                        ((System.Windows.Controls.Image)(((System.Windows.Controls.Button)(((System.Windows.Controls.Panel)((((Grid)(myTextBlock.FindName("Printbtns")))))).Children[0])).Content)).Source = null;
                        ((System.Windows.Controls.Image)(((System.Windows.Controls.Button)(((System.Windows.Controls.Panel)((((Grid)(myTextBlock.FindName("Printbtns")))))).Children[0])).Content)).Source = new BitmapImage(new Uri(@"/images/print-group.png", UriKind.Relative));
                        ((System.Windows.Controls.Image)(((System.Windows.Controls.Button)(((System.Windows.Controls.Panel)((((Grid)(myTextBlock.FindName("Printbtns")))))).Children[0])).Content)).UpdateLayout();
                        window.imgprintgroup.Source = new BitmapImage(new Uri(@"/images/print-group.png", UriKind.Relative));
                    }
                }
                if (window.vwGroup.Text == "View All")
                {
                    var count = (from groupImages in RobotImageLoader.GroupImages
                                 join printImages in RobotImageLoader.PrintImages
                                 on groupImages.PhotoId equals printImages.PhotoId
                                 select groupImages).Count();

                    window.txtSelectImages.Text = "Selected : " + count.ToString() + "/" + RobotImageLoader.GroupImages.Count.ToString();
                    if (RobotImageLoader.GroupImages.Count == RobotImageLoader.PrintImages.Count)
                        window.chkSelectAll.IsChecked = true;
                    else window.chkSelectAll.IsChecked = false;
                }
                else if (window.vwGroup.Text == "View Group")
                {
                    window.txtSelectImages.Visibility = Visibility.Collapsed;
                    window.txtSelectedImages.Visibility = Visibility.Visible;
                    window.txtSelectedImages.Text = "Grouped : " + RobotImageLoader.GroupImages.Count;
                }
            }
            catch (Exception ex)
            {
                ///////////////Entry in error log for scope error
                ErrorHandler.ErrorHandler.LogError(ex);
            }
        }

        private childItem FindVisualChild<childItem>(DependencyObject obj)
                                                where childItem : DependencyObject
        {
            for (int i = 0; i < VisualTreeHelper.GetChildrenCount(obj); i++)
            {
                DependencyObject child = VisualTreeHelper.GetChild(obj, i);
                if (child != null && child is childItem)
                    return (childItem)child;
                else
                {
                    childItem childOfChild = FindVisualChild<childItem>(child);
                    if (childOfChild != null)
                        return childOfChild;
                }
            }
            return null;
        }
        public int FilpFrom = 0; //1: VOS, 2: MainWindow
        private void btnMinimize_Click(object sender, RoutedEventArgs e)
        {
            this.WindowState = System.Windows.WindowState.Minimized;
            stkPrevNext.Visibility = Visibility.Collapsed;
            stkPrint.Visibility = Visibility.Collapsed;
            if (FilpFrom == 1)
            {
                SearchResult window = System.Windows.Application.Current.Windows.Cast<Window>().Where(wnd => wnd.Title == "View/Order Station").Cast<SearchResult>().FirstOrDefault();
                if (window == null) return;

                if ((imgNext.Visibility == Visibility.Visible || testR.Fill != null) && window.isSingleScreenPreview)
                {
                    var grouptestR = new TransformGroup();
                    grouptestR.Children.Add(new RotateTransform(0));
                    img12.LayoutTransform = grouptestR;

                    var group = new TransformGroup();
                    group.Children.Add(new RotateTransform(0));
                    window.ContentContainer.LayoutTransform = group;
                    window.btnchkpreview.IsChecked = false;
                    window.btnchkthumbpreview.IsChecked = false;
                    window.PreviewPhoto();
                }
            }
            else if (FilpFrom == 2)
            {
                MainWindow mainWin = null;
                foreach (Window wnd in System.Windows.Application.Current.Windows)
                {
                    if (wnd.Title == "DigiMain")
                    {
                        mainWin = (MainWindow)wnd;
                        break;
                    }
                }
                if (mainWin != null)
                {
                    if ((imgNext.Visibility == Visibility.Visible || testR.Fill != null) && mainWin.isSingleScreenPreview)
                    {
                        TransformGroup grouptestR = new TransformGroup();
                        grouptestR.Children.Add(new RotateTransform(0));
                        img12.LayoutTransform = grouptestR;
                        TransformGroup group = new TransformGroup();
                        group.Children.Add(new RotateTransform(0));
                        mainWin.ContentContainer.LayoutTransform = group;
                        mainWin.btnchkpreview.IsChecked = false;
                    }
                }
            }
        }
        private void Window_StateChanged(object sender, EventArgs e)
        {
            switch (this.WindowState)
            {
                case WindowState.Minimized:
                    btnMinimize_Click(sender, new RoutedEventArgs());
                    break;
            }
        }
    }
    public static class myPrinters
    {
        [DllImport("winspool.drv", CharSet = CharSet.Auto, SetLastError = true)]
        public static extern bool SetDefaultPrinter(string Name);
    }
}
