﻿using Baracoda.Cameleon.PC.Modularity.GuiFeatures;
using Baracoda.Cameleon.PC.Readers;
using DigiPhoto.IMIX.Business;
using DigiPhoto.IMIX.Model;
using FrameworkHelper.RfidLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Threading;

namespace DigiPhoto
{
    public partial class RfidManualDownload : Window
    {
        private List<DataContainer> _readerList = new List<DataContainer>();
        //List<DataContainer> rawDataToDump = new List<DataContainer>();
        private RfidBusiness _rfidBusiness = new RfidBusiness();
        private readonly MainModel _model;
        /// <summary>
        /// Creates new instance of this class
        /// </summary>
        public RfidManualDownload()
        {
            InitializeComponent();
            OnGui.Initialize(Dispatcher.CurrentDispatcher);
            _model = new MainModel();
            //model.registeredDevices = GetDeviceList();
            _model.ListRefreshNeeded += OnListRefreshNeeded;
            _model.DataReceived += OnDataReceived;
            ShowWait();
            //ReadRFIdFromFile();
        }
        private void ShowWait()
        {
            if (_readerList.Count() == 0)
            {
                DataContainer container = new DataContainer();
                container.Id = "No Device Found.";
                container.TagId = "";
                _readerList.Add(container);
            }
            DGUsbReader.ItemsSource = _readerList;
            //DGUsbReader.Items.Refresh();
        }
        void OnDataReceived(object sender, DataEventArgs e)
        {
            var device = (BaracodaReaderBase)sender;
            if (device == null)
                return;
            //string data = e.RfidData.Id + " :- " + e.RfidData.Content + ",";
            OnGui.Run(() =>
            {
                SaveIntoDatabase(e.RfidData);
                var itm = _readerList.FirstOrDefault(o => string.Compare(o.TagId, e.RfidData.Id, true) == 0);
                if (itm != null)
                    ++itm.NoOfIdReceived;

                DGUsbReader.ItemsSource = _readerList;
                DGUsbReader.Items.Refresh();
                //rawDataToDump.Add(e.RfidData);
            });
        }

        void OnListRefreshNeeded(object sender, EventArgs e)
        {
            OnGui.Run(() =>
             {
                 _readerList.Clear();
                 DataContainer container = null;
                 foreach (var reader in _model.Readers)
                 {
                     container = new DataContainer();
                     container.Id = reader.Id + "(" + reader.Retrieved.SerialNumber + ")";
                     container.TagId = reader.Retrieved.SerialNumber;
                     _readerList.Add(container);
                     //DGUsbReader.ItemsSource = readerList;
                 }
                 if (_readerList.Count() == 0)
                 {
                     container = new DataContainer();
                     container.Id = "No Device Found.";
                     container.TagId = "";
                     _readerList.Add(container);
                 }
                 DGUsbReader.ItemsSource = _readerList;
                 DGUsbReader.Items.Refresh();
             });
        }


        private void btnClose_Click(object sender, RoutedEventArgs e)
        {
            Home objHome = new Home();
            objHome.Show();
            this.Close();
        }

        private void Window_Closed(object sender, EventArgs e)
        {
            //WriteRFIdToFile();
            _model.ListRefreshNeeded -= OnListRefreshNeeded;
            _model.DataReceived -= OnDataReceived;
            _model.CloseForced();
            this.ClearValue(System.Windows.Controls.TextBox.TextProperty);
            this.ClearValue(System.Windows.Controls.Button.TagProperty);
            this.ClearValue(System.Windows.Controls.Grid.WidthProperty);
            this.ClearValue(System.Windows.Controls.Grid.HeightProperty);
            this.ClearValue(System.Windows.Controls.TextBlock.TextProperty);
            this.ClearValue(System.Windows.Controls.TextBlock.TextProperty);
        }

        //List<string> GetDeviceList()
        //{
        //    DeviceManager deviceManager = new DeviceManager();
        //   var devices= deviceManager.GetDeviceList().Select(o => o.SerialNo).ToList();
        //    devices.Add("USB_00189A31C88D");

        //    return devices;
        //}

        void SaveIntoDatabase(DataContainer tagData)
        {
            try
            {
                string tagId = tagData.Content != null ? tagData.Content.Split(']').LastOrDefault() : string.Empty;
                string time = tagData.Content != null ? tagData.Content.Split('[').FirstOrDefault() : string.Empty;
                DateTime? scannedTime = new DateTime();
                try
                {
                    System.Globalization.DateTimeFormatInfo objFormat = new System.Globalization.DateTimeFormatInfo();
                    objFormat.ShortDatePattern = @"dd/MM/yyyy HH:mm:ss";
                    if (!string.IsNullOrEmpty(time) && time.Length >= 12)
                        scannedTime = Convert.ToDateTime((time.Substring(4, 2) + "/" + time.Substring(2, 2) + "/" + time.Substring(0, 2) + " " + time.Substring(6, 2) + ":" + time.Substring(8, 2) + ":" + time.Substring(10, 2)), objFormat);
                    else
                        scannedTime = null;
                }
                catch (Exception ex)
                {
                    scannedTime = null;
                    string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                    ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                }
                if (tagId == null)
                    tagId = string.Empty;

                var rfidTag = new RFIDTagInfo
                {
                    SerialNo = tagData.Id,
                    TagId = tagId,
                    RawData = tagData.Content,
                    ScanningTime = scannedTime
                };
                _rfidBusiness.SaveRfidTag(rfidTag);
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
            }
        }


    }
}
