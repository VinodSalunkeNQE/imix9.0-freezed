﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.IO;
using DigiPhoto.DataLayer.Model;
using DigiPhoto.DataLayer;
using System.Collections.ObjectModel;
using DigiPhoto.Common;
using System.Diagnostics;
using FrameworkHelper;
using System.Collections;
using DigiPhoto.Interop;
using DigiPhoto.IMIX.Model;
using DigiPhoto.IMIX.Business;
using FrameworkHelper.Common;

namespace DigiPhoto
{
    /// <summary>
    /// Interaction logic for ImagePreviewer.xaml
    /// </summary>
    public partial class ImagePreviewer : Window
    {
        string _mktImgPath = string.Empty;
        int _mktImgTime = 0;

        /// <summary>
        /// The filepath
        /// </summary>
        string filepath;

        /// <summary>
        /// The count
        /// </summary>
        int count = 0;
        /// <summary>
        /// The processed count
        /// </summary>
        static int ProcessedCount = 0;
        /// <summary>
        /// The path
        /// </summary>
        string path = string.Empty;
        /// <summary>
        /// The thumbnailspath
        /// </summary>
        string thumbnailspath = string.Empty;
        /// <summary>
        /// The thumbnailsfolderpath
        /// </summary>
        string thumbnailsfolderpath = string.Empty;
        /// <summary>
        /// The tumbnailsfolderbigpath
        /// </summary>
        string tumbnailsfolderbigpath = string.Empty;
        /// <summary>
        /// The stopwatch
        /// </summary>
        Stopwatch stopwatch = new Stopwatch();
        /// <summary>
        /// The img
        /// </summary>
        //List<string> img = new List<string>();
        Dictionary<string, DateTime> img = new Dictionary<string, DateTime>();
        Hashtable htpath = new Hashtable();
        bool isVideoEditingEnabled = false;
        string[] mediaExtensions = new[] { ".jpg", ".avi", ".mp4", ".wmv", ".mov", ".3gp", ".3g2", ".m2v", ".m4v", ".flv", ".mpeg", ".ffmpeg", ".mkv", ".mts" };
        const string fileExtension = ".jpg";

        private string _filePath;
        private string _path = string.Empty;
        private string _thumbnailsPath = string.Empty;
        private string _tempVideoPath = string.Empty;
        private Dictionary<string, DownloadFileInfo> _img = new Dictionary<string, DownloadFileInfo>();
        public Hashtable htVidLength = new Hashtable();
        //private Dictionary<string, DateTime> _img = new Dictionary<string, DateTime>();
        private Dictionary<string, string> _imagePath = new Dictionary<string, string>();
        public ObservableCollection<MyImageClass> MyImages { get; set; }
        private BusyWindow _busyWindows = new BusyWindow();
        public static string imgCount = string.Empty;
        //{

        /// <summary>
        /// Initializes a new instance of the <see cref="ImageDownloader"/> class.
        /// </summary>
        public ImagePreviewer()
        {
            InitializeComponent();
            lstImages.Items.Clear();
            GetConfigurationInfo();
            filepath = LoginUser.DigiFolderPath;
            string root = Environment.CurrentDirectory;
            path = root + "\\";
            path = System.IO.Path.Combine(path, "Download\\");

            ConfigurationInfo config = (new ConfigBusiness()).GetConfigurationData(LoginUser.SubStoreId);
            _filePath = config.DG_Hot_Folder_Path;
            //string root = Environment.CurrentDirectory;
            _path = Environment.CurrentDirectory + "\\";
            _path = System.IO.Path.Combine(_path, "Download\\");
            _thumbnailsPath = System.IO.Path.Combine(_path, "Temp\\");
            _tempVideoPath = System.IO.Path.Combine(Environment.CurrentDirectory, "DownloadVideo");
            if (Directory.Exists(_path))
                DeletePath(_path);

            //thumbnailspath = System.IO.Path.Combine(path, "Temp\\");
            _busyWindows.Show();
            _busyWindows.BringIntoView();
            ClearImages();
            CreateImages();
            _busyWindows.Hide();
            btnBack.IsDefault = true;
            VideoPlayer.ExecuteParentMethod += new EventHandler(ShowClientView);

        }

        private void ShowClientView(object sender, EventArgs e)
        {
            ShowToClientView();
        }
        /// <summary>
        /// This method is for showing the images from the collection in the listbox
        /// </summary>
        #region Common Methods

        private void ShowImages()
        {
            try
            {
                lstImages.Items.Clear();

                MyImages = new ObservableCollection<MyImageClass>();
                MyImages.Clear();

                foreach (var item in _img)
                {
                    try
                    {
                        var objFileInfo = (DownloadFileInfo)item.Value;
                        string drivePath = string.Empty;
                        string fileName = System.IO.Path.GetFileNameWithoutExtension(objFileInfo.fileName);
                        string displayFileName = fileName;
                        if (!objFileInfo.isVideo)
                        {
                            drivePath = objFileInfo.filePath;

                        }
                        else
                        {
                            drivePath = objFileInfo.videoPath;
                            displayFileName = System.IO.Path.GetFileNameWithoutExtension(displayFileName);
                        }
                        MyImages.Add(new MyImageClass(displayFileName, GetImageFromResourceString(fileName, objFileInfo.filePath), false, objFileInfo.CreatedDate, drivePath, objFileInfo.fileExtension));
                    }
                    catch { }
                }

                lstImages.ItemsSource = MyImages;

            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
            }
        }
        private BitmapImage GetImageFromResourceString(string imageName)
        {

            BitmapImage image = new BitmapImage();
            image.BeginInit();
            image.DecodePixelWidth = 150;
            image.CacheOption = BitmapCacheOption.OnLoad;
            image.UriSource = new Uri(path + imageName + ".jpg");

            image.EndInit();

            return image;
        }

        private BitmapImage GetImageFromResourceString(string imageName, string imageDrivePath)
        {

            var image = new BitmapImage();
            image.BeginInit();
            image.DecodePixelWidth = 150;
            image.CacheOption = BitmapCacheOption.OnLoad;
            image.UriSource = new Uri(imageDrivePath + "\\" + imageName + ".jpg");
            image.EndInit();

            return image;
        }

        #endregion

        #region Events
        void btnBack_Click(object sender, RoutedEventArgs e)
        {
            GetMktImgInfo();
            ClientView window = null;

            foreach (Window wnd in Application.Current.Windows.Cast<Window>().Where(wnd => wnd.Title == "ClientView"))
            {
                window = (ClientView)wnd;
                window.tbkItemCount.Visibility = System.Windows.Visibility.Visible;
                window.tbkItemCount.Text = " ";
                window.watermark.Visibility = System.Windows.Visibility.Collapsed;
            }

            if (window == null)
            {
                window = new ClientView
                {
                    WindowStartupLocation = System.Windows.WindowStartupLocation.Manual
                };

            }

            window.GroupView = false;
            window.DefaultView = false;


            if (!(_mktImgPath == string.Empty || _mktImgTime == 0))
            {
                window.instructionVideo.Visibility = System.Windows.Visibility.Visible;
                window.instructionVideo.Play();
                window.instructionVideoBlur.Visibility = System.Windows.Visibility.Visible;
                window.instructionVideoBlur.Play();
            }
            else
            {
                window.imgDefault.Visibility = System.Windows.Visibility.Visible;
                window.imgDefaultBlur.Visibility = System.Windows.Visibility.Visible;  // Added for Blur Images
            }
            window.testR.Fill = null;
            window.DefaultView = true;
            if (window.instructionVideo.Visibility == System.Windows.Visibility.Visible)
                window.instructionVideo.Play();
            else
                window.instructionVideo.Pause();

            // Added for Blur Images
            if (window.instructionVideoBlur.Visibility == System.Windows.Visibility.Visible)
                window.instructionVideoBlur.Play();
            else
                window.instructionVideoBlur.Pause();
            // End Changes

            Home objhome = new Home();
            objhome.Show();
            this.Close();
            ClearImages();
        }
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            _busyWindows.Show();
            _busyWindows.BringIntoView();
            ShowImages();
            ShowToClientView();
            _busyWindows.Hide();
        }
        private void ClearImages()
        {
            try
            {
                if (MyImages != null)
                    MyImages.Clear();
                if (Directory.Exists(thumbnailspath))
                {
                    string[] filePaths2 = Directory.GetFiles(thumbnailspath);
                    foreach (var item in filePaths2)
                    {
                        if (File.Exists(item))
                        {
                            File.Delete(item);
                        }
                    }
                    Directory.Delete(thumbnailspath, true);
                }
                if (Directory.Exists(path))
                {
                    string[] filePaths = Directory.GetFiles(path);
                    foreach (var item in filePaths)
                    {
                        if (File.Exists(item))
                        {
                            File.Delete(item);
                        }
                    }
                    Directory.Delete(path, true);
                }
                //Temporary storage for videos
                if (Directory.Exists(_tempVideoPath))
                {
                    string[] filePaths = Directory.GetFiles(_tempVideoPath);
                    foreach (var item in filePaths)
                    {
                        if (File.Exists(item))
                        {
                            File.Delete(item);
                        }
                    }
                    Directory.Delete(_tempVideoPath, true);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }
        string DirPath = string.Empty;

        #endregion
        /// <summary>
        /// This method is for collecting the images from the USB into a collection
        /// </summary>

        private void CreateImages()
        {
            int imageCount = 0;
            int videoCount = 0;
            string strMessage = string.Empty;
            var drives = from drive in DriveInfo.GetDrives()
                         where drive.DriveType == DriveType.Removable && drive.IsReady == true
                         select drive;

            if (drives == null || drives.Count() == 0)
            {
                drives = from drive in DriveInfo.GetDrives()
                         where drive.DriveType == DriveType.CDRom && drive.IsReady == true
                         select drive;
                if (drives.Count() > 0)
                    strMessage = "CD";
            }
            else
                strMessage = "USB";
            if (drives.Count() > 0)
            {
                foreach (var drivesitem in drives)
                {
                    try
                    {
                        DirPath = drivesitem.Name;
                        DirectoryInfo dirInfo = new DirectoryInfo(drivesitem.Name);
                        //FileInfo[] fileinfo = dirInfo.EnumerateFiles("*.jpg", SearchOption.AllDirectories).OrderBy(f => f.CreationTime).ToArray();
                        FileInfo[] fileinfo = dirInfo.EnumerateFiles("*.*", SearchOption.AllDirectories).Where(f => mediaExtensions.Contains(f.Extension.ToLower())).ToArray();
                        if (fileinfo.Count() > 0)
                        {
                            #region Create the file directory if it does not exists
                            if (fileinfo != null && fileinfo.Count() > 0)
                            {
                                if (!Directory.Exists(_path))
                                    System.IO.Directory.CreateDirectory(_path);

                                if (!Directory.Exists(_thumbnailsPath))
                                    System.IO.Directory.CreateDirectory(_thumbnailsPath);
                            }

                            #endregion
                            foreach (var fileitem in fileinfo)
                            {
                                if (!fileitem.Name.Contains("Thumbs.db"))
                                {
                                    if (fileitem.Extension.ToLower().Equals(".jpg"))
                                    {
                                        if (!_img.ContainsKey(fileitem.Name))
                                        {
                                            _img.Add(fileitem.Name.ToLower(), new DownloadFileInfo
                                            {
                                                CreatedDate = fileitem.CreationTime,
                                                isVideo = false,
                                                fileName = fileitem.Name,
                                                filePath = fileitem.DirectoryName,
                                                fileExtension = fileExtension
                                            });

                                            imageCount++;
                                        }
                                    }
                                    else
                                    {
                                        string FilePath = string.Empty;
                                        //Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Input, new System.Windows.Threading.DispatcherOperationCallback
                                        //    (delegate
                                        //    {
                                        //        FilePath = MLHelpers.ExtractThumbnail(fileitem.FullName);
                                        //        if (!string.IsNullOrEmpty(FilePath))
                                        //            MLHelpers.ResizeImage(FilePath, 210, _path + fileitem.Name + fileExtension);
                                        //        return null;

                                        //    }),
                                        //null);
                                        FilePath = MLHelpers.ExtractThumbnail(fileitem.FullName);
                                        if (!string.IsNullOrEmpty(FilePath))
                                            MLHelpers.ResizeImage(FilePath, 210, _path + fileitem.Name + fileExtension);
                                        //Dispatcher.Invoke(new Action(() =>                                         
                                        //   ThumbnailExtractor.ExtractThumbnail(fileitem.FullName,_path + fileitem.Name + fileExtension)
                                        // // ThumbnailExtractor.ExtractThumbnailFromVideo(fileitem.FullName, 4, 4, _path + fileitem.Name + fileExtension, _VisioForgeLicenseKey, _VisioForgeUserName, _VisioForgeEmailID)
                                        //));

                                        long vLen = Convert.ToInt64(MLHelpers.VideoLength);
                                        if (!htVidLength.ContainsKey(fileitem.Name))
                                        {
                                            htVidLength.Add(fileitem.Name, vLen);
                                        }
                                        if (!_img.ContainsKey(fileitem.Name))
                                        {
                                            _img.Add(fileitem.Name.ToLower(), new DownloadFileInfo
                                            {
                                                CreatedDate = fileitem.CreationTime,
                                                isVideo = true,
                                                fileName = fileitem.Name + fileExtension,
                                                filePath = _path,
                                                videoPath = fileitem.DirectoryName,
                                                fileExtension = fileitem.Extension,
                                                drivePath = fileitem.DirectoryName
                                            });
                                            videoCount++;

                                        }
                                    }
                                }
                            }

                            MessageBox.Show(strMessage + "-" + fileinfo.Count() + " File(s) found.");
                            tbkItemCount.Text = fileinfo.Count().ToString() + " " + "Items";// "Images : " + "  " + imageCount + "  " + "videos :" + "  " + videoCount;
                        }
                        else
                        {
                            MessageBox.Show("No file(s) found.");
                            this.Close();

                        }

                    }
                    catch (Exception ex)
                    {
                        ErrorHandler.ErrorHandler.LogError(ex);
                    }
                }
                GC.Collect();
                GC.AddMemoryPressure(20000);
            }
            else
            {
                MessageBox.Show("No drive(s) found.");
                this.Close();

            }
        }

        /// <summary>
        /// Function to extract thumbnail from a video
        /// </summary>
        /// <param name="mediaFile">Input video</param>
        /// <param name="waitTime"></param>
        /// <param name="position"></param>
        /// <param name="acquiredPath"></param>
        void ExtractThumbnailFromVideo(string mediaFile, int waitTime, int position, string acquiredPath)
        {
            MediaPlayer player = new MediaPlayer { Volume = 0, ScrubbingEnabled = true };
            player.Open(new Uri(mediaFile));
            player.Pause();
            player.Position = TimeSpan.FromSeconds(position);
            System.Threading.Thread.Sleep(waitTime * 1000);
            RenderTargetBitmap rtb = new RenderTargetBitmap(120, 90, 96, 96, PixelFormats.Pbgra32);
            DrawingVisual dv = new DrawingVisual();
            using (DrawingContext dc = dv.RenderOpen())
            {
                dc.DrawVideo(player, new Rect(0, 0, 120, 90));
            }
            rtb.Render(dv);
            Duration duration = player.NaturalDuration;
            int videoLength = 0;
            if (duration.HasTimeSpan)
            {
                videoLength = (int)duration.TimeSpan.TotalSeconds;
            }
            BitmapFrame frame = BitmapFrame.Create(rtb).GetCurrentValueAsFrozen() as BitmapFrame;
            BitmapEncoder encoder = new JpegBitmapEncoder();
            encoder.Frames.Add(frame as BitmapFrame);
            MemoryStream memoryStream = new MemoryStream();
            encoder.Save(memoryStream);
            FileStream file = new FileStream(acquiredPath, FileMode.Create, FileAccess.Write);
            memoryStream.WriteTo(file);
            file.Close();
            memoryStream.Close();
            player.Close();
        }
        /*
        /// <summary>
        /// Determines whether [is showor hide].
        /// </summary>
        /// <returns></returns>
        public bool IsShoworHide()
        {
            return Show;
        }

        */
        /// <summary>
        /// Handles the Loaded event of the Window control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        public void ShowToClientView()
        {
            try
            {
                VisualBrush CB = new VisualBrush(ThumbPreview);
                SearchResult objSearchResult = new SearchResult();
                objSearchResult.CompileEffectChanged(CB, -1);
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }

        private void GetMktImgInfo()
        {

            try
            {
                List<long> objList = new List<long>();
                objList.Add((long)ConfigParams.MktImgPath);
                objList.Add((long)ConfigParams.MktImgTimeInSec);
                List<iMIXConfigurationInfo> ConfigValuesList = (new ConfigBusiness()).GetNewConfigValues(LoginUser.SubStoreId)
                  .Where(o => objList.Contains(o.IMIXConfigurationMasterId)).ToList();

                if (ConfigValuesList != null || ConfigValuesList.Count > 0)
                {
                    for (int i = 0; i < ConfigValuesList.Count; i++)
                    {
                        switch (ConfigValuesList[i].IMIXConfigurationMasterId)
                        {
                            case (int)ConfigParams.MktImgPath:
                                _mktImgPath = ConfigValuesList[i].ConfigurationValue;
                                break;
                            case (int)ConfigParams.MktImgTimeInSec:
                                _mktImgTime = (ConfigValuesList[i].ConfigurationValue != null ? Convert.ToInt32(ConfigValuesList[i].ConfigurationValue) : 10) * 1000;
                                break;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }

        private void vidPlay_Click(object sender, RoutedEventArgs e)
        {
            string vidFileName = ((System.Windows.Media.Imaging.BitmapImage)((((Image)((System.Windows.Controls.Grid)(((Button)e.Source).Parent)).FindName("thumbImage"))).Source)).UriSource.OriginalString.ToLower();
            vidFileName = Preview(vidFileName);
        }

        private string Preview(string vidFileName)
        {
            if (vidFileName.EndsWith("avi.jpg") || vidFileName.EndsWith("mp4.jpg") || vidFileName.EndsWith("wmv.jpg") || vidFileName.EndsWith("mov.jpg") || vidFileName.EndsWith("3gp.jpg") || vidFileName.EndsWith("3g2.jpg") || vidFileName.EndsWith("m2v.jpg") || vidFileName.EndsWith("m4v.jpg") || vidFileName.EndsWith("flv.jpg") || vidFileName.EndsWith("mpg.jpg") || vidFileName.EndsWith("ffmpeg.jpg") || vidFileName.EndsWith("mkv.jpg") || vidFileName.EndsWith("mts.jpg"))
            {
                vidFileName = vidFileName.Replace(".jpg", string.Empty);
                //var drives = from drive in DriveInfo.GetDrives()
                //             where drive.DriveType == DriveType.Removable && drive.IsReady == true &&( drive.DriveType == DriveType.CDRom|| drive.DriveFormat == "FAT32")
                //             select drive;
                //  if (drives.Count() > 0)
                {
                    //   foreach (var drivesitem in drives)
                    {
                        //  var videoitems = Directory.EnumerateFiles(drivesitem.Name, "*.*", SearchOption.AllDirectories).Where(s => s.ToLower().EndsWith(".wmv") || s.ToLower().EndsWith(".mp4") || s.ToLower().EndsWith(".avi") || s.ToLower().EndsWith(".mov") || s.ToLower().EndsWith(".3gp") || s.ToLower().EndsWith(".3g2") || s.ToLower().EndsWith(".m2v") || s.ToLower().EndsWith(".m4v") || s.ToLower().EndsWith(".flv") || s.ToLower().EndsWith(".mpg") || s.ToLower().EndsWith("ffmpeg")).ToList();
                        //  var tempvid = videoitems.Where(v => v.ToLower().Contains(System.IO.Path.GetFileName(vidFileName))).FirstOrDefault();
                        //  if (tempvid != null)
                        {
                            VideoPlayer.Visibility = Visibility.Visible;
                            vidFileName = System.IO.Path.GetFileName(vidFileName);
                            DownloadFileInfo file = _img[vidFileName];
                            VideoPlayer.vsMediaFileName = file.drivePath + "\\" + file.fileName.ToString().Replace(".jpg", "");
                            VideoPlayer.Title = System.IO.Path.GetFileName(vidFileName);
                            VideoPlayer.SetParent(this);
                        }
                    }
                }
            }
            else
            {
                VideoPlayer.Title = System.IO.Path.GetFileNameWithoutExtension(vidFileName);
                VideoPlayer.imagesource = GetImageFromPath(vidFileName);
                VideoPlayer.Visibility = Visibility.Visible;
                VideoPlayer.SetParent(this);
            }
            return vidFileName;
        }
        ///////////////Method to get bitmapimage from given path/////
        public BitmapImage GetImageFromPath(string path)
        {
            try
            {
                BitmapImage bi = new BitmapImage();
                using (FileStream fileStream = File.OpenRead(path))
                {
                    MemoryStream ms = new MemoryStream();
                    fileStream.CopyTo(ms);
                    ms.Seek(0, SeekOrigin.Begin);
                    fileStream.Close();
                    bi.CacheOption = BitmapCacheOption.OnLoad; // Memory 
                    bi.BeginInit();
                    bi.StreamSource = ms;
                    bi.EndInit();
                    bi.Freeze(); // Memory 
                    fileStream.Close();
                    return bi;
                }

            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                return null;
            }
        }
        private void Selectedimages_Click(object sender, RoutedEventArgs e)
        {
            string vidFileName = ((System.Windows.Media.Imaging.BitmapImage)((((Image)((System.Windows.Controls.Grid)(((CheckBox)e.Source).Parent)).FindName("thumbImage"))).Source)).UriSource.OriginalString.ToLower();
            vidFileName = Preview(vidFileName);
        }
        private void GetConfigurationInfo()
        {
            try
            {
                List<long> objList = new List<long>();
                objList.Add((long)ConfigParams.MktImgPath);
                objList.Add((long)ConfigParams.MktImgTimeInSec);
                objList.Add((long)ConfigParams.IsEnabledVideoEdit);
                objList.Add((int)ConfigParams.MediaPlayerLicenseKey);
                objList.Add((int)ConfigParams.VisioForgeUsername);
                objList.Add((int)ConfigParams.VisioForgeEmailId);
                List<iMIXConfigurationInfo> ConfigValuesList = (new ConfigBusiness()).GetNewConfigValues(LoginUser.SubStoreId)
                 .Where(o => objList.Contains(o.IMIXConfigurationMasterId)).ToList();
                // List<iMIXConfigurationValue> ConfigValuesList = objdbLayer.GetNewConfigValues(LoginUser.SubStoreId).Where(o => objList.Contains(o.IMIXConfigurationMasterId)).ToList();
                if (ConfigValuesList != null || ConfigValuesList.Count > 0)
                {
                    for (int i = 0; i < ConfigValuesList.Count; i++)
                    {
                        switch (ConfigValuesList[i].IMIXConfigurationMasterId)
                        {
                            case (int)ConfigParams.IsEnabledVideoEdit:
                                isVideoEditingEnabled = ConfigValuesList[i].ConfigurationValue != null ? Convert.ToBoolean(ConfigValuesList[i].ConfigurationValue) : false;
                                break;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }

        private void DeletePath(string path)
        {

            try
            {
                string[] filePaths = Directory.GetFiles(path);
                foreach (var item in filePaths)
                {
                    try
                    {
                        if (File.Exists(item))
                        {
                            File.Delete(item);
                        }
                    }
                    catch (Exception ex)
                    {
                        ErrorHandler.ErrorHandler.LogError(ex);
                    }
                }

                if (Directory.Exists(path))
                {
                    Directory.Delete(path, true);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
            finally
            {
                if (Directory.Exists(path))
                {
                    try
                    {
                        Directory.Delete(path, true);
                    }
                    catch (Exception ex)
                    {
                        ErrorHandler.ErrorHandler.LogError(ex);
                    }
                }
            }

        }

    }
}
