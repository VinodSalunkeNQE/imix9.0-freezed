﻿using log4net;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace DigiPhoto.ServiceHelper
{
    public class ServiceProxy<T>
    {
        public static ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        public static void Use(Action<T> action)
        {
            BasicHttpBinding binding = new BasicHttpBinding();

            string apiLocation = ConfigurationManager.AppSettings["API_URL"].ToString();
            bool useHttps = false;

            if (apiLocation.Contains("https:"))
                useHttps = true;

            if (typeof(T).Name.StartsWith("I"))
                apiLocation = apiLocation + "/" + typeof(T).Name.Substring(1) + ".svc";
            else
                apiLocation = apiLocation + "/" + typeof(T).Name + ".svc";

            binding.ReceiveTimeout = new TimeSpan(0, 5, 0);
            binding.SendTimeout = new TimeSpan(0, 5, 0);

            EndpointAddress ep = new EndpointAddress(apiLocation);
            string dataSize = string.Empty;
            if (ConfigurationManager.AppSettings["ServiceDataSize"] == null)
            {
                dataSize = Convert.ToString(1024 * 65535);
            }
            else
            {
                dataSize = ConfigurationManager.AppSettings["ServiceDataSize"];
            }

            binding.MaxReceivedMessageSize = Convert.ToInt64(dataSize);
            binding.ReaderQuotas.MaxArrayLength = Convert.ToInt32(dataSize);
            binding.ReaderQuotas.MaxStringContentLength = Convert.ToInt32(dataSize);
            binding.MaxBufferSize = Convert.ToInt32(dataSize);

            //Decide which binding needs to use
            if (useHttps)
            {
                binding.Security = new BasicHttpSecurity();
                binding.Security.Mode = BasicHttpSecurityMode.Transport;
                binding.Security.Transport.ClientCredentialType = HttpClientCredentialType.None;
            }

            ChannelFactory<T> factory = new ChannelFactory<T>(binding, ep);
            T client = factory.CreateChannel();

            bool sucess = false;
            try
            {
                if (useHttps)
                    SetCertificatePolicy();

                action(client);
                ((IClientChannel)client).Close();
                factory.Close();
                sucess = true;
            }
            finally
            {
                if (!sucess)
                {
                    //Abort the Channel if it didn't close sucessfully
                    ((IClientChannel)client).Abort();
                    factory.Abort();
                }
            }
            //try
            //{
            //    if (useHttps)
            //        SetCertificatePolicy();

            //    action(client);
            //    ((IClientChannel)client).Close();
            //    factory.Close();
            //    sucess = true;
            //}
            //catch (CommunicationException cex)
            //{
            //    log.Error(cex);
            //    throw;
            //}
            //catch (TimeoutException tex)
            //{

            //    log.Error(tex);
            //    throw;
            //}
            //catch (Exception ex)
            //{
            //    log.Error(ex);
            //    throw;
            //}
            //finally
            //{
            //    if (!sucess)
            //    {
            //        //Abort the Channel if it didn't close sucessfully
            //        ((IClientChannel)client).Abort();
            //        factory.Abort();
            //    }
            //}
        }

        public static void SetCertificatePolicy()
        {

            System.Net.ServicePointManager.ServerCertificateValidationCallback

                       += RemoteCertificateValidate;

        }



        /// <summary>

        /// Remotes the certificate validate.

        /// </summary>

        private static bool RemoteCertificateValidate(

           object sender, System.Security.Cryptography.X509Certificates.X509Certificate cert,

            System.Security.Cryptography.X509Certificates.X509Chain chain, System.Net.Security.SslPolicyErrors error)
        {

            // trust any certificate!!!

            // System.Console.WriteLine("Warning, trust any certificate");

            return true;

        }

    }
}
