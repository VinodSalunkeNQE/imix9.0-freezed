﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using DigiPhoto.DataLayer;
using DigiPhoto.DataLayer.Model;
using DigiPhoto.Common;
using System.IO;
using DigiPhoto.Shader;
using DigiAuditLogger;
using System.Drawing.Printing;
using Microsoft.Win32;
using System.Xml;
using System.Security.Cryptography;
using System.Configuration;
using System.Windows.Navigation;
//using System.Windows.Shapes;
using System.Windows.Media.Effects;
using System.Reflection;
using System.Collections;
using System.ComponentModel;
using System.ServiceProcess;
using System.Text.RegularExpressions;
using FrameworkHelper;
using DigiPhoto.IMIX.Business;
using DigiPhoto.IMIX.Model;
using DigiPhoto.Cache.MasterDataCache;
using DigiPhoto.Cache.Repository;
using DigiPhoto.Cache.DataCache;
using DigiPhoto.Utility.Repository.ValueType;
using System.Windows.Threading;
using DigiPhoto.Utility.Repository.Data;
using WMPLib;
using System.Globalization;
using System.Management;
using System.Collections.ObjectModel;
using System.Net.NetworkInformation;
using DigiPhoto.ViewModel;
using DigiPhoto.IMIX.Model.Base;

namespace DigiPhoto.Manage
{
    /// <summary>
    /// Interaction logic for Configuration.xaml
    /// </summary>
    public partial class Configuration : Window, INotifyPropertyChanged
    {
        enum IntervalType { Day = 1, Week = 2, Month = 3 };
        //Filter setting
        //"Red", "1" "Green", "2" "Blue", "3" "Magenta", "4" "Lime", "4" "Orange", "5" "Yellow", "6" "Underwater", "7" //"Warning", "8"
        System.ComponentModel.BackgroundWorker BackupWorker = new System.ComponentModel.BackgroundWorker();
        private delegate void LoadTabDelegate();
        BusyWindow bs = new BusyWindow();
        TaxBusiness taxBusiness;
        ConfigBusiness configBusiness;
        string _databasename = string.Empty;
        string _username = string.Empty;
        string _pass = string.Empty;
        string _server = string.Empty;
        string _destinationPath = string.Empty;
        string _imgSource = string.Empty;
        string _imgDestination = string.Empty;
        string _heavyTables = string.Empty;
        ///string DG_Product_Name = "";
        int _cleanupdaysBackup;
        int _FailedOnlineCleanupdayes;
        ///Stack EffectLog;
        string SpecFileName = string.Empty;
        bool _isDefaultColorCode = true;
        string _isDefaultColorSeletedValue = string.Empty;
        private bool _iseditStockImg = false;
        private bool _isNewFileSelected = false;
        StockShot stockShotImg = new StockShot();
        //long videoTemplateId = 0;
        string videoDisplayname = string.Empty;
        string audioName = string.Empty;
        string audioDisplayname = string.Empty;
        bool IsAudioEdited = false;
        long audioId = 0;
        string videoBGDisplayname = string.Empty;
        int videoBGMediaType = 0;
        bool IsVideoBGEdited = false;
        bool IsVideoOverlayEdited = false;
        string videoBGName = string.Empty;
        long videoBackgroundId = 0;
        string errorMsg = string.Empty;
        private Dictionary<string, Int32> _codeTypeList;
        //VideoSlotsTime CtrlVideoSlotsTime = new VideoSlotsTime();
        public Dictionary<string, Int32> CodeTypeList
        {
            get { return _codeTypeList; }
            set { _codeTypeList = value; }
        }
        private Dictionary<string, Int32> _scanTypeList;
        public Dictionary<string, Int32> ScanTypeList
        {
            get { return _scanTypeList; }
            set { _scanTypeList = value; }
        }
        List<ConfigurationInfo> lstConfigurationInfo = new List<ConfigurationInfo>();
        ObservableCollection<BackGroundInfo> backgroundList;
        public ObservableCollection<BackGroundInfo> BackgroundList { get { return backgroundList; } set { backgroundList = value; PropertyModified("BackgroundList"); } }
        BackGroundInfo editableBackground;
        public BackGroundInfo EditableBackground { get { return editableBackground; } set { editableBackground = value; PropertyModified("SelectedBackground"); } }
        AllGraphics editableGraphics;
        public AllGraphics EditableGraphics { get { return editableGraphics; } set { editableGraphics = value; PropertyModified("EditableGraphics"); } }
        int originalWidth;
        int originalHeight;
        int ProductTypeId = 0;
        string sourcepath = string.Empty;
        string savepath = string.Empty;
        string selectedFileName = string.Empty;
        string filenamebg = string.Empty;
        string saveFolderPath = string.Empty;

        System.ComponentModel.BackgroundWorker bw_CopySettings = new System.ComponentModel.BackgroundWorker();
        #region Constructor
        /// <summary>
        /// Initializes a new instance of the <see cref="Configuration"/> class.
        /// </summary>
        /// 

        private static Configuration instance;
        public static Configuration Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new Configuration();
                }
                return instance;
            }
            set { instance = value; }
        }

        public Configuration()
        {
            InitializeComponent();
            this.DataContext = this;
            txbUserName.Text = LoginUser.UserName;
            txbStoreName.Text = LoginUser.StoreName;
            ucAddEditSpecPrintProfile.SetParentConfig(this);
            this.Dispatcher.BeginInvoke(DispatcherPriority.Background, new LoadTabDelegate(LoadTabData));
            BackupWorker.DoWork += new System.ComponentModel.DoWorkEventHandler(BackupWorker_DoWork);
            BackupWorker.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(BackupWorker_RunWorkerCompleted);
            bw_CopySettings.DoWork += bw_CopySettings_DoWork;
            bw_CopySettings.RunWorkerCompleted += bw_CopySettings_RunWorkerCompleted;
            if (txtTables.Text == "")
            {
                txtTables.Text = "DG_Activity,ChangeTracking,CloudinaryDtl,DG_Photos,DG_Refund,DG_RefundDetails,DG_Orders_Details_Data,DG_Orders,DG_Orders_Details,DG_PrinterQueue,DG_PhotoGroup,DG_PhotosXml,RFIDTags,iMixImageAssociation,ArchivedPhotos,ArchivediMixImageAssociation,DG_Emails,DG_EmailDetail,ArchivedRFIDTags";
            }
            //ucDynamicImgCrop.SetParent(btn);
            GetStockImgDetails();
            MsgBox.SetParent(btn);


        }
        #endregion
        private void bw_CopySettings_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            bs.Hide();
            MessageBox.Show("Background saved Successfully");
            GetBackgrounds();
            txtSelectedBackground.Clear();
            lstBackgroundInfo.Clear();           
            ChkIsBackgroundPanoramic.IsChecked = false;

        }

        private void bw_CopySettings_DoWork(object sender, DoWorkEventArgs e)
        {
            SaveBackground();
        }


        #region Declaration
        private int _numValue = 0;
        public int NumValue
        {
            get { return _numValue; }
            set
            {
                _numValue = value;
            }
        }
        Dictionary<string, string> lstPrinterList;
        public int borderId;
        public Int64 _stockImgId;
        ///List<BGMaster> lstmainBGList;
        TextBox controlon;
        // List<vw_GetBorderDetails> lstBorderList;
        List<BorderInfo> lstBorderList;
        Dictionary<string, string> lstlogoList;
        Dictionary<string, string> lstBackgroundList;
        Dictionary<string, string> lstBGList;
        ///Dictionary<string, string> lstProductTypeList;
        Dictionary<string, string> lstFilterList;
        Dictionary<string, string> lstCurrencyList;
        //DigiPhotoDataServices _objDataLayer;
        Dictionary<string, string> lstThemeListSelect;
        MonochromeEffect _colorfiltereff = new MonochromeEffect();
        ///double cont = 1;
        ///double bright = 0;
        int pkey;
        //double rationwidth = 0, ratioheight = 0;
        ContrastAdjustEffect _brighteff = new ContrastAdjustEffect();
        ContrastAdjustEffect _conteff = new ContrastAdjustEffect();
        List<ThemeInfo> lstThemeList;
        public static string HorizontalCropCoordinates = string.Empty;
        public static string VerticalCropCoordinates = string.Empty;
        public static int SelectedLocationId = 0;


        #endregion
        #region Scene configuraion
        List<BGMaster> lstmainBGListScene = new List<BGMaster>();
        List<AllGraphics> _objgraphicsdetailScene = new List<AllGraphics>();
        List<BorderInfo> lstBorderListScene = new List<BorderInfo>();
        bool isEditScene = false;
        public int sceneId = 0;

        #endregion

        #region chracter configuration
        bool isEditCharacter = false;
        public int CharacterId = 0;
        #endregion

        #region Common Methods

        ///int selectedProductType = 1;

        /// <summary>
        /// Checks the validation.
        /// </summary>
        /// <returns></returns>
        private bool CheckValidation()
        {
            bool retvalue = true;
            //if (cmbCurrency.SelectedValue.ToString() == "0")
            //{
            //    MessageBox.Show("Please Select Default Currency.");
            //    retvalue = false;
            //}
            if (string.IsNullOrEmpty(txtHotFolder.Text))
            {
                txtHotFolder.BorderBrush = Brushes.Red;
                retvalue = false;
            }
            if (string.IsNullOrEmpty(txtPassword.Password))
            {
                txtPassword.BorderBrush = Brushes.Red;
                retvalue = false;
            }

            if (string.IsNullOrEmpty(txtPageSize.Text))
            {
                txtPageSize.BorderBrush = Brushes.Red;
                retvalue = false;
            }
            if (string.IsNullOrEmpty(txtConfigColorCode.Text))
            {
                txtConfigColorCode.BorderBrush = Brushes.Red;
                retvalue = false;
            }

            try
            {
                int intPageS;
                if (!Int32.TryParse(txtPageSize.Text, out intPageS))
                {
                    intPageS = 0;
                }

                if ((intPageS > 20) || (intPageS <= 0))
                {
                    MessageBox.Show("Page size should be between 1 to 20");
                    txtPageSize.BorderBrush = Brushes.Red;
                    retvalue = false;
                }
                //else
                //{
                //    retvalue = true;
                //}

                /*
                Convert.ToInt32(txtNumber.Text);

               int intPageS = Convert.ToInt32(txtPageSize.Text);
               if ((intPageS > 20) || (intPageS <= 0)) 
               {
                   MessageBox.Show("Page size should be between 1 to 20");
                   txtPageSize.BorderBrush = Brushes.Red;
                   retvalue = false;
               }
               else
               {
                   retvalue = true;
               }
                */


            }
            catch (Exception ew)
            {
                txtPageSize.BorderBrush = Brushes.Red;

                retvalue = false;
            }

            //if (string.IsNullOrEmpty(txtNumber.Text))
            //{
            //    txtNumber.BorderBrush = Brushes.Red;

            //    retvalue = false;
            //}
            //try
            //{
            //    Convert.ToInt32(txtNumber.Text);
            //}
            //catch (Exception ew)
            //{
            //    txtNumber.BorderBrush = Brushes.Red;

            //    retvalue = false;
            //}
            //Check for null or emtry 1
            if (string.IsNullOrEmpty(txtNoOfReceipt.Text.Trim()) || !System.Text.RegularExpressions.Regex.IsMatch(txtNoOfReceipt.Text.Trim(), "^[0-9]*$") || int.Parse(txtNoOfReceipt.Text.Trim()) < 1)
            {
                //MessageBox.Show("Enter No. of Billing Receipts.", "Error Message", MessageBoxButton.OK);
                txtNoOfReceipt.BorderBrush = Brushes.Red;
                retvalue = false;
            }
            //Check for empty, valid and non negative
            if (string.IsNullOrEmpty(txtNoOfPhotoIdSearch.Text.Trim()) || !System.Text.RegularExpressions.Regex.IsMatch(txtNoOfPhotoIdSearch.Text.Trim(), "^[0-9]*$") || int.Parse(txtNoOfPhotoIdSearch.Text.Trim()) < 1)
            {
                //MessageBox.Show("Enter No. of Billing Receipts.", "Error Message", MessageBoxButton.OK);
                txtNoOfPhotoIdSearch.BorderBrush = Brushes.Red;
                retvalue = false;
            }
            if (retvalue)
            {
                txtbgPath.BorderBrush = Brushes.Gray;
                txtBorderFolder.BorderBrush = Brushes.Gray;
                txtHotFolder.BorderBrush = Brushes.Gray;
                txtPassword.BorderBrush = Brushes.Gray;
                txtbgGraphics.BorderBrush = Brushes.Gray;
                txtNumber.BorderBrush = Brushes.Gray;
                txtNoOfReceipt.BorderBrush = Brushes.Gray;
                txtNoOfPhotoIdSearch.BorderBrush = Brushes.Gray;
            }
            return retvalue;
        }

        private bool CheckValidationsCurrency()
        {
            bool retvalue = true;
            if (cmbCurrency.SelectedValue.ToString() == "0")
            {
                MessageBox.Show("Please Select Default Currency.");
                retvalue = false;
            }
            return retvalue;
        }
        /// <summary>
        /// Fills the printers.
        /// </summary>
        private void FillPrinters()
        {
            try
            {
                lstPrinterList = new Dictionary<string, string>();
                lstPrinterList.Add("--Select--", "0");
                foreach (String printer in PrinterSettings.InstalledPrinters)
                {
                    lstPrinterList.Add(printer.ToString(), printer.ToString());
                }
                cmbReceiptPrinter.ItemsSource = lstPrinterList;
                cmbReceiptPrinter.SelectedValue = "0";
            }
            catch (Exception ec)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ec);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }
        /// <summary>
        /// Gets the configuration data.
        /// </summary>
        private void GetConfigData()
        {
            ConfigBusiness conBiz = new ConfigBusiness();
            ConfigurationInfo _objdata = conBiz.GetConfigurationData(LoginUser.SubStoreId);
            if (_objdata != null)
            {
                txtbgPath.Text = _objdata.DG_BG_Path;
                txtBorderFolder.Text = _objdata.DG_Frame_Path;
                txtHotFolder.Text = _objdata.DG_Hot_Folder_Path;
                txtPassword.Password = _objdata.DG_Mod_Password;
                txtbgGraphics.Text = _objdata.DG_Graphics_Path;
                txtNumber.Text = _objdata.DG_NoOfPhotos.ToString();
                chkwatermark.IsChecked = _objdata.DG_Watermark;
                chkSemiOrder.IsChecked = _objdata.DG_SemiOrder;
                chkHgihResolutionPreview.IsChecked = _objdata.DG_HighResolution;
                chkTotalDiscount.IsChecked = _objdata.DG_EnableDiscountOnTotal;
                chkLineItemDiscount.IsChecked = _objdata.DG_AllowDiscount;
                chkEnablePos.IsChecked = _objdata.PosOnOff;
                chkAutoRotate.IsChecked = _objdata.DG_IsAutoRotate;
                chkCompressImage.IsChecked = _objdata.DG_IsCompression;
                //chkClearUSBImageDown.IsChecked = _objdata.IsDeleteFromUSB;
                cmbCurrency.SelectedValue = Convert.ToString(_objdata.DefaultCurrency);
                chkSemiOrderMain.IsChecked = _objdata.DG_SemiOrderMain.ToBoolean();
                chkSaveReportToAnyDerive.IsChecked = _objdata.IsExportReportToAnyDrive.ToBoolean();
                chkEnableGrouping.IsChecked = _objdata.DG_IsEnableGroup;
                _isDefaultColorSeletedValue = Convert.ToString(_objdata.DG_ChromaColor);
                cmbChromaColor.SelectedValue = Convert.ToString(_objdata.DG_ChromaColor);

                txtChromaTolerance.Text = Convert.ToString(_objdata.DG_ChromaTolerance);
                if (!string.IsNullOrEmpty(_objdata.DG_ReceiptPrinter))
                {
                    cmbReceiptPrinter.SelectedValue = _objdata.DG_ReceiptPrinter;
                    Common.LoginUser.ReceiptPrinterPath = _objdata.DG_ReceiptPrinter;
                }
                else
                {
                    cmbReceiptPrinter.SelectedValue = "0";
                }

                //cmbReceiptPrinter.SelectedValue = LoginUser.ReceiptPrinterPath;
                //Change here for reciept printer settings.
                txtNoOfReceipt.Text = _objdata.DG_NoOfBillReceipt.ToString();
                txtNoOfPhotoIdSearch.Text = _objdata.DG_NoOfPhotoIdSearch.ToString();
                txtPageSize.Text = _objdata.DG_PageCountGrid.ToString();//Added by Adarsh
                hiddenHFPath.Text = _objdata.DG_Hot_Folder_Path;
            }
            else
            {
                txtbgPath.Text = "";
                txtBorderFolder.Text = "";

                txtPassword.Password = "";
                txtbgGraphics.Text = "";
                txtNumber.Text = "";
                chkwatermark.IsChecked = false;
                chkSemiOrder.IsChecked = false;
                chkSemiOrderMain.IsChecked = false;
                chkSaveReportToAnyDerive.IsChecked = false;
                chkHgihResolutionPreview.IsChecked = false;
                chkAutoRotate.IsChecked = false;
                chkCompressImage.IsChecked = false;
                //chkClearUSBImageDown.IsChecked = false;
                cmbCurrency.SelectedValue = "0";
                cmbReceiptPrinter.SelectedValue = "0";
                //vw_GetConfigdata Item2 = _objDataLayer.GetDeafultPathData();
                conBiz = new ConfigBusiness();
                ConfigurationInfo Item1 = conBiz.GetDeafultPathData();
                if (Item1 != null)
                {
                    txtHotFolder.Text = Item1.DG_Hot_Folder_Path;
                }
                txtNoOfReceipt.Text = "1";
                txtNoOfPhotoIdSearch.Text = "10";
            }
            txtHotFolder.Focus();
            txtbgPath.BorderBrush = Brushes.Gray;
            txtBorderFolder.BorderBrush = Brushes.Gray;
            txtHotFolder.BorderBrush = Brushes.Gray;
            txtPassword.BorderBrush = Brushes.Gray;
            txtbgGraphics.BorderBrush = Brushes.Gray;
            txtNumber.BorderBrush = Brushes.Gray;
            txtNoOfReceipt.BorderBrush = Brushes.Gray;
            txtNoOfPhotoIdSearch.BorderBrush = Brushes.Gray;


        }

        /// <summary>
        /// Saves the configuration data.
        /// </summary>
        private void SaveConfigData()
        {
            int PageSizeGrid = txtPageSize.Text.ToInt32();
            int PageSizePreview = 4; //string TempTables = "";
            if (txtTables.Text == "")
            {
                txtTables.Text = "DG_Activity,ChangeTracking,CloudinaryDtl,DG_Photos,DG_Refund,DG_RefundDetails,DG_Orders_Details_Data,DG_Orders,DG_Orders_Details,DG_PrinterQueue,DG_PhotoGroup,DG_PhotosXml,RFIDTags,iMixImageAssociation,ArchivedPhotos,ArchivediMixImageAssociation,DG_Emails,DG_EmailDetail,ArchivedRFIDTags";
            }
            //DigiPhotoDataServices _objDataLayer = new DigiPhotoDataServices();

            string SyncCode = CommonUtility.GetUniqueSynccode(Convert.ToInt32(ApplicationObjectEnum.Configuration).ToString().PadLeft(2, '0'), LoginUser.countrycode, LoginUser.Storecode, LoginUser.SubStoreId.ToString().PadLeft(2, '0'));
            //string SyncCode = CommonUtility.GetRandomString(8) + Convert.ToInt32(ApplicationObjectEnum.Product).ToString().PadLeft(2, '0');
            //bool IsSaved = _objDataLayer.SetConfigurationData(txtHotFolder.Text, txtHotFolder.Text + "Frames\\", txtHotFolder.Text + "BG\\", txtPassword.Password, txtHotFolder.Text + "Graphics\\", Convert.ToInt32(txtNumber.Text), chkwatermark.IsChecked, chkHgihResolutionPreview.IsChecked, chkSemiOrder.IsChecked, chkAutoRotate.IsChecked, chkLineItemDiscount.IsChecked, chkTotalDiscount.IsChecked, chkEnablePos.IsChecked, Convert.ToInt32(cmbCurrency.SelectedValue.ToString()), chkSemiOrderMain.IsChecked, lstPrinterList.Where(t => t.Value == cmbReceiptPrinter.SelectedValue.ToString()).FirstOrDefault().Key, LoginUser.SubStoreId, chkCompressImage.IsChecked, chkEnableGrouping.IsChecked, int.Parse(txtNoOfReceipt.Text.Trim()), cmbChromaColor.SelectedValue.ToString(), decimal.Parse(txtChromaTolerance.Text), chkClearUSBImageDown.IsChecked);

            //bool IsSaved = _objDataLayer.SetConfigurationData(txtHotFolder.Text, txtHotFolder.Text + "Frames\\", txtHotFolder.Text + "BG\\", txtPassword.Password, txtHotFolder.Text + "Graphics\\", Convert.ToInt32(txtNumber.Text), chkwatermark.IsChecked, chkHgihResolutionPreview.IsChecked, chkSemiOrder.IsChecked, chkAutoRotate.IsChecked, chkLineItemDiscount.IsChecked, chkTotalDiscount.IsChecked, chkEnablePos.IsChecked, Convert.ToInt32(cmbCurrency.SelectedValue.ToString()), chkSemiOrderMain.IsChecked, lstPrinterList.Where(t => t.Value == cmbReceiptPrinter.SelectedValue.ToString()).FirstOrDefault().Key, LoginUser.SubStoreId, chkCompressImage.IsChecked, chkEnableGrouping.IsChecked, int.Parse(txtNoOfReceipt.Text.Trim()), cmbChromaColor.SelectedValue.ToString(), decimal.Parse(txtChromaTolerance.Text), PageSizeGrid, PageSizePreview);

            //bool IsSaved1 = _objDataLayer.SetConfigurationData(SyncCode, txtHotFolder.Text, txtHotFolder.Text + "Frames\\", txtHotFolder.Text + "BG\\",
            //    txtPassword.Password, txtHotFolder.Text + "Graphics\\", 1000, chkwatermark.IsChecked, chkHgihResolutionPreview.IsChecked, chkSemiOrder.IsChecked,
            //    chkAutoRotate.IsChecked, chkLineItemDiscount.IsChecked, chkTotalDiscount.IsChecked, chkEnablePos.IsChecked,
            //    Convert.ToInt32(cmbCurrency.SelectedValue.ToString()), chkSemiOrderMain.IsChecked,
            //    LoginUser.SubStoreId, chkCompressImage.IsChecked, chkEnableGrouping.IsChecked, int.Parse(txtNoOfReceipt.Text.Trim()),
            //    cmbChromaColor.SelectedValue.ToString(), decimal.Parse(txtChromaTolerance.Text), PageSizeGrid, PageSizePreview, int.Parse(txtNoOfPhotoIdSearch.Text.Trim()));

            ConfigBusiness conBiz = new ConfigBusiness();

            bool IsSaved = conBiz.SetConfigurationData(SyncCode, txtHotFolder.Text, txtHotFolder.Text + "Frames\\", txtHotFolder.Text + "BG\\",
               txtPassword.Password, txtHotFolder.Text + "Graphics\\", 1000, chkwatermark.IsChecked, chkHgihResolutionPreview.IsChecked, chkSemiOrder.IsChecked,
               chkAutoRotate.IsChecked, chkLineItemDiscount.IsChecked, chkTotalDiscount.IsChecked, chkEnablePos.IsChecked, cmbReceiptPrinter.SelectedValue.ToString(),
               cmbCurrency.SelectedValue.ToInt32(), chkSemiOrderMain.IsChecked,
               LoginUser.SubStoreId, chkCompressImage.IsChecked, chkEnableGrouping.IsChecked, int.Parse(txtNoOfReceipt.Text.Trim()),
               cmbChromaColor.SelectedValue.ToString(), decimal.Parse(txtChromaTolerance.Text), PageSizeGrid, PageSizePreview, int.Parse(txtNoOfPhotoIdSearch.Text.Trim()), chkSaveReportToAnyDerive.IsChecked);


            /*
            bool IsSaved = _objDataLayer.SetConfigurationData(txtHotFolder.Text, txtHotFolder.Text + "Frames\\", txtHotFolder.Text + "BG\\", txtPassword.Password, txtHotFolder.Text + "Graphics\\", Convert.ToInt32(txtNumber.Text), chkwatermark.IsChecked, chkHgihResolutionPreview.IsChecked, chkSemiOrder.IsChecked, chkAutoRotate.IsChecked, chkLineItemDiscount.IsChecked, chkTotalDiscount.IsChecked, chkEnablePos.IsChecked, Convert.ToInt32(cmbCurrency.SelectedValue.ToString()), chkSemiOrderMain.IsChecked, lstPrinterList.Where(t => t.Value == cmbReceiptPrinter.SelectedValue.ToString()).FirstOrDefault().Key, LoginUser.SubStoreId, chkCompressImage.IsChecked, chkEnableGrouping.IsChecked, int.Parse(txtNoOfReceipt.Text.Trim()), cmbChromaColor.SelectedValue.ToString(), decimal.Parse(txtChromaTolerance.Text));

            //bool IsSaved = _objDataLayer.SetConfigurationData(txtHotFolder.Text, txtHotFolder.Text + "Frames\\", txtHotFolder.Text + "BG\\", txtPassword.Password, txtHotFolder.Text + "Graphics\\", Convert.ToInt32(txtNumber.Text), chkwatermark.IsChecked, chkHgihResolutionPreview.IsChecked, chkSemiOrder.IsChecked, chkAutoRotate.IsChecked, chkLineItemDiscount.IsChecked, chkTotalDiscount.IsChecked, chkEnablePos.IsChecked, Convert.ToInt32(cmbCurrency.SelectedValue.ToString()), chkSemiOrderMain.IsChecked, lstPrinterList.Where(t => t.Value == cmbReceiptPrinter.SelectedValue.ToString()).FirstOrDefault().Key, LoginUser.SubStoreId, chkCompressImage.IsChecked, chkEnableGrouping.IsChecked, int.Parse(txtNoOfReceipt.Text.Trim()), cmbChromaColor.SelectedValue.ToString(), decimal.Parse(txtChromaTolerance.Text), PageSizeGrid, PageSizePreview);
            bool IsSaved = _objDataLayer.SetConfigurationData(txtHotFolder.Text, txtHotFolder.Text + "Frames\\", txtHotFolder.Text + "BG\\",
                txtPassword.Password, txtHotFolder.Text + "Graphics\\", 1000, chkwatermark.IsChecked, chkHgihResolutionPreview.IsChecked, chkSemiOrder.IsChecked, 
                chkAutoRotate.IsChecked, chkLineItemDiscount.IsChecked, chkTotalDiscount.IsChecked, chkEnablePos.IsChecked, 
                Convert.ToInt32(cmbCurrency.SelectedValue.ToString()), chkSemiOrderMain.IsChecked, 
                lstPrinterList.Where(t => t.Value == cmbReceiptPrinter.SelectedValue.ToString()).FirstOrDefault().Key, 
                LoginUser.SubStoreId, chkCompressImage.IsChecked, chkEnableGrouping.IsChecked, int.Parse(txtNoOfReceipt.Text.Trim()), 
                cmbChromaColor.SelectedValue.ToString(), decimal.Parse(txtChromaTolerance.Text), PageSizeGrid, PageSizePreview);
            */
            List<iMIXConfigurationInfo> configList = new List<iMIXConfigurationInfo>();

            iMIXConfigurationInfo config = new iMIXConfigurationInfo();
            config.IMIXConfigurationMasterId = Convert.ToInt64(ConfigParams.SequencePrefix);
            if (string.IsNullOrEmpty(txtSeqPrefix.Text.Trim()))
            {
                config.ConfigurationValue = LoginUser.SubstoreName;
            }
            else
            {
                config.ConfigurationValue = txtSeqPrefix.Text;
            }
            config.SubstoreId = LoginUser.SubStoreId;
            configList.Add(config);

            configBusiness = new ConfigBusiness();
            bool result = configBusiness.SaveUpdateNewConfig(configList);
            //Add ImageBarCodeTextFontSize Config Value.
            iMIXConfigurationInfo ImageBarCodeTextFontSizeConfig = new iMIXConfigurationInfo();
            ImageBarCodeTextFontSizeConfig.IMIXConfigurationMasterId = Convert.ToInt64(ConfigParams.ImageBarCodeTextFontSize);
            if (string.IsNullOrEmpty(txtImageBarCodeTextFontSize.Text.Trim()))
            {
                ImageBarCodeTextFontSizeConfig.ConfigurationValue = "7";  //Default ImageBarCodeTextFontSize Value.
            }
            else
            {
                ImageBarCodeTextFontSizeConfig.ConfigurationValue = txtImageBarCodeTextFontSize.Text;
            }
            ImageBarCodeTextFontSizeConfig.SubstoreId = LoginUser.SubStoreId;
            configList.Add(ImageBarCodeTextFontSizeConfig);

            iMIXConfigurationInfo ImageColorCodeTextConfig = new iMIXConfigurationInfo();
            ImageColorCodeTextConfig.IMIXConfigurationMasterId = Convert.ToInt64(ConfigParams.ColorCode);
            ImageColorCodeTextConfig.ConfigurationValue = txtConfigColorCode.Text;
            ImageColorCodeTextConfig.SubstoreId = LoginUser.SubStoreId;
            configList.Add(ImageColorCodeTextConfig);

            configBusiness = new ConfigBusiness();
            result = configBusiness.SaveUpdateNewConfig(configList);
            if (IsSaved && result)
            {
                MessageBox.Show("Record saved successfully");
                //txtDefaultCurrency.Text = _objDataLayer.GetDefaultCurrencyName();
                //CurrencyBusiness curBiz = new CurrencyBusiness();
                //txtDefaultCurrency.Text = curBiz.GetDefaultCurrencyName().ToString();
                txtDefaultCurrency.Text = GetDefaultCurrency().ToString();
                AuditLog.AddUserLog(Common.LoginUser.UserId, (int)FrameworkHelper.ActionType.ADDEditConfigData, "Add/Edit configuration data at:- ");
                RobotImageLoader.GetConfigData();
                GetNewConfigData();
            }
        }
        private string GetDefaultCurrency()
        {

            //DigiPhotoDataServices _objDataLayer = new DigiPhotoDataServices();
            var currency = (from objcurrency in (new CurrencyBusiness()).GetCurrencyList()
                            where objcurrency.DG_Currency_Default == true
                            select objcurrency.DG_Currency_Name.ToString()).FirstOrDefault();
            return currency;

        }
        /// <summary>
        /// Retrieve data from DG_Configuration for backup settings
        /// </summary>
        /// 
        public class Action : INotifyPropertyChanged
        {
            private bool _isSelected;
            public bool IsSelected
            {
                get
                {
                    return _isSelected;
                }
                set
                {
                    _isSelected = value;
                    if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("IsSelected"));
                    }
                }
            }

            private string _name;
            public string Name
            {
                get
                {
                    return _name;
                }
                set
                {
                    _name = value;
                    if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("Name"));
                    }
                }
            }

            public event PropertyChangedEventHandler PropertyChanged;


        }
        /// <summary>
        /// Retrieve data from DG_Configuration for backup settings
        /// </summary>
        //private void GetBackupConfigData()
        //{
        //    _objDataLayer = new DigiPhotoDataServices();
        //    vw_GetConfigdata _objdata = _objDataLayer.GetConfigurationData(LoginUser.SubStoreId);
        //    if (_objdata != null)
        //    {
        //        txtDbBackupPath.Text = _objdata.DG_DbBackupPath;
        //        txtTables.Text = _objdata.DG_CleanupTables;
        //        txtCleanupdays.Text = _objdata.DG_CleanUpDaysBackUp.ToString();

        //        //Get selected table names
        //        string[] valuesArray = txtTables.Text.Split(',');
        //        List<string> names = valuesArray.ToList();

        //        //Get all table names
        //        List<vw_GetAllTable> _objTabledata = _objDataLayer.GetAllTable();
        //        List<Action> abc = new List<Action>();
        //        if (_objTabledata != null)
        //            foreach (var datas in _objTabledata)
        //            {
        //                Action data = new Action();
        //                data.IsSelected = false;
        //                data.Name = datas.name;

        //                abc.Add(data);
        //            }

        //        foreach (var datas in abc)
        //        {
        //            foreach (string s in names)
        //            {
        //                if (datas.Name.Trim() == s.Trim())
        //                {
        //                    datas.IsSelected = true;


        //                }
        //            }
        //        }
        //        listtables.ItemsSource = abc;

        //        txtHfBackupPath.Text = _objdata.DG_HfBackupPath;
        //        chkSchBackup.IsChecked = _objdata.DG_IsBackupScheduled;
        //        hiddenHFPath.Text = _objdata.DG_Hot_Folder_Path;
        //        if (chkSchBackup.IsChecked == true)
        //        {
        //            DateTime dtsch = DateTime.Parse(_objdata.DG_ScheduleBackup);
        //            ccBackupDateTime.Value = dtsch;
        //        }
        //        else
        //        {
        //            ccBackupDateTime.Value = DateTime.Now.Date.Add(new TimeSpan(6, 0, 0));
        //        }
        //    }
        //    else
        //    {
        //        string[] valuesArray = txtTables.Text.Split(',');
        //        List<string> names = valuesArray.ToList();

        //        //Get all table names
        //        List<vw_GetAllTable> _objTabledata = _objDataLayer.GetAllTable();
        //        List<Action> abc = new List<Action>();
        //        if (_objTabledata != null)
        //            foreach (var datas in _objTabledata)
        //            {
        //                Action data = new Action();
        //                data.IsSelected = false;
        //                data.Name = datas.name;

        //                abc.Add(data);
        //            }

        //        foreach (var datas in abc)
        //        {
        //            foreach (string s in names)
        //            {
        //                if (datas.Name.Trim() == s.Trim())
        //                {
        //                    datas.IsSelected = true;


        //                }
        //            }
        //        }
        //        listtables.ItemsSource = abc;
        //        txtDbBackupPath.Text = String.Empty;
        //        txtTables.Text = String.Empty;
        //        txtCleanupdays.Text = string.Empty;
        //        txtHfBackupPath.Text = String.Empty;
        //        chkSchBackup.IsChecked = false;
        //        ccBackupDateTime.Value = DateTime.Now.Date.Add(new TimeSpan(6, 0, 0));
        //    }
        //}
        //private void fillRecursiveOptions()
        //{
        //    cmbSelectRecursiveCount.Items.Clear();
        //    for (int d = 1; d < 30; d++)
        //    {
        //        cmbSelectRecursiveCount.Items.Add(d.ToString());
        //    }

        //    cmbSelectRecursiveTime.Items.Clear();
        //    cmbSelectRecursiveTime.Items.Add(IntervalType.Day);
        //    cmbSelectRecursiveTime.Items.Add(IntervalType.Week);
        //    cmbSelectRecursiveTime.Items.Add(IntervalType.Month);
        //}

        private void GetNewConfigData()
        {
            List<iMIXConfigurationInfo> _objdata = (new ConfigBusiness()).GetNewConfigValues(LoginUser.SubStoreId);
            if (_objdata != null)
            {
                bool isBarcodeActive = false;
                foreach (iMIXConfigurationInfo imixConfigValue in _objdata)
                {
                    switch (imixConfigValue.IMIXConfigurationMasterId)
                    {
                        case (int)ConfigParams.IsBarcodeActive://for QR Code
                            if (imixConfigValue.ConfigurationValue.ToLower() == "true")
                            {
                                chkIsBarcodeActive.IsChecked = true;
                                isBarcodeActive = true;
                            }
                            break;

                        case (int)ConfigParams.DefaultMappingCode://for QR Code
                            if (isBarcodeActive)
                                cmbMappingType.SelectedValue = imixConfigValue.ConfigurationValue;
                            break;

                        case (int)ConfigParams.ScanType://for QR Code
                            if (isBarcodeActive)
                                cmbScanType.SelectedValue = imixConfigValue.ConfigurationValue;
                            break;

                        case (int)ConfigParams.LostImgTimeGap://for QR Code
                            txtLostImgTimeGap.Text = imixConfigValue.ConfigurationValue;
                            break;

                        case (int)ConfigParams.OnlineImageResize://for QR Code and Social Media
                            txtOnlineResize.Text = imixConfigValue.ConfigurationValue;
                            break;

                        case (int)ConfigParams.StorageMediaImageResize://for CD and USB
                            txtSMResize.Text = imixConfigValue.ConfigurationValue;
                            break;

                        case (int)ConfigParams.Online://online
                            chkIsOnline.IsChecked = string.Compare(imixConfigValue.ConfigurationValue, "True", true) == 0 ? true : false;
                            break;

                        case (int)ConfigParams.DgServiceURL://online
                            txtDgServiceUrl.Text = imixConfigValue.ConfigurationValue;
                            break;

                        case (int)ConfigParams.DbBackupPath:// For Backup
                            txtDbBackupPath.Text = Convert.ToString(imixConfigValue.ConfigurationValue);
                            if (!String.IsNullOrWhiteSpace(txtDbBackupPath.Text))
                                btnBackupNow.IsEnabled = true;
                            break;

                        case (int)ConfigParams.CleanUpTable:// For Backup
                            txtTables.Text = Convert.ToString(imixConfigValue.ConfigurationValue);
                            string[] valuesArray = txtTables.Text.Split(',');
                            List<string> names = valuesArray.ToList();

                            //Get all table names

                            //List<vw_GetAllTable> _objTabledata1 = _objDataLayer.GetAllTable();
                            CommonBusiness comBiz = new CommonBusiness();
                            List<TableBaseInfo> _objTabledata = comBiz.GetAllTable();
                            List<Action> abc = new List<Action>();
                            if (_objTabledata != null)
                                foreach (var datas in _objTabledata)
                                {
                                    Action data = new Action();
                                    data.IsSelected = false;
                                    data.Name = datas.name;
                                    abc.Add(data);
                                }

                            foreach (var datas in abc)
                            {
                                foreach (string s in names)
                                {
                                    if (datas.Name.Trim() == s.Trim())
                                    {
                                        datas.IsSelected = true;
                                    }
                                }
                            }
                            listtables.ItemsSource = abc;
                            break;

                        case (int)ConfigParams.HfBackupPath:// For Backup
                            txtHfBackupPath.Text = Convert.ToString(imixConfigValue.ConfigurationValue);
                            break;

                        case (int)ConfigParams.ScheduleBackup:// For Backup
                            if (chkSchBackup.IsChecked == true)
                            {
                                ErrorHandler.ErrorHandler.LogFileWrite("Schedule bkp at :" + imixConfigValue.ConfigurationValue);
                                DateTime dtsch = DateTime.Parse(imixConfigValue.ConfigurationValue);
                                ccBackupDateTime.Value = dtsch;
                            }
                            else
                            {
                                ccBackupDateTime.Value = DateTime.Now.Date.Add(new TimeSpan(6, 0, 0));
                            }
                            break;

                        case (int)ConfigParams.IsBackupScheduled:// For Backup
                            chkSchBackup.IsChecked = imixConfigValue.ConfigurationValue.ToBoolean();
                            break;

                        case (int)ConfigParams.IsRecursive:// For Backup
                            chkRecursive.IsChecked = imixConfigValue.ConfigurationValue.ToBoolean();
                            break;

                        case (int)ConfigParams.IntervalCount:// For Backup
                            cmbSelectRecursiveCount.SelectedValue = Convert.ToString(imixConfigValue.ConfigurationValue);
                            break;

                        case (int)ConfigParams.IntervalType:// For Backup
                            cmbSelectRecursiveTime.SelectedValue = Convert.ToString(imixConfigValue.ConfigurationValue);
                            break;

                        case (int)ConfigParams.CleanUpDaysBackup:// For Backup
                            txtCleanupdays.Text = Convert.ToString(imixConfigValue.ConfigurationValue);
                            break;

                        case (int)ConfigParams.DigimagicBrightness:// For DigiMagic
                            brightVal.Value = imixConfigValue.ConfigurationValue.ToDouble();
                            txtbright.Text = Convert.ToString(imixConfigValue.ConfigurationValue);
                            break;

                        case (int)ConfigParams.DigimagicContrast:// For DigiMagic
                            contrastVal.Value = imixConfigValue.ConfigurationValue.ToDouble();
                            txtcontrasts.Text = Convert.ToString(imixConfigValue.ConfigurationValue);
                            break;

                        case (int)ConfigParams.DigimagicSharpen:// For DigiMagic
                            sharpVal.Value = imixConfigValue.ConfigurationValue.ToDouble();
                            txtsharp.Text = Convert.ToString(imixConfigValue.ConfigurationValue);
                            break;

                        case (int)ConfigParams.IsAnonymousQrCodeEnabled://IsAnonymousQrCode
                            chkAnonymousQrCode.IsChecked = imixConfigValue.ConfigurationValue.ToBoolean();
                            break;

                        case (int)ConfigParams.IsPrepaidAutoPurchaseActive://for QR Code
                            chkIsAutoPurchaseActive.IsChecked = imixConfigValue.ConfigurationValue.ToBoolean();
                            break;
                        case (int)ConfigParams.SequencePrefix://for QR Code
                            txtSeqPrefix.Text = imixConfigValue.ConfigurationValue;
                            break;
                        case (int)ConfigParams.ImageBarCodeTextFontSize:
                            txtImageBarCodeTextFontSize.Text = Convert.ToString(imixConfigValue.ConfigurationValue);
                            break;
                        case (int)ConfigParams.ColorCode:
                            txtConfigColorCode.Text = Convert.ToString(imixConfigValue.ConfigurationValue);

                            if (txtConfigColorCode.Text != "")
                            {
                                _isDefaultColorCode = false;
                            }
                            break;
                        case (int)ConfigParams.QRCodeLengthSetting:
                            txtQRCodeLen.Text = Convert.ToString(imixConfigValue.ConfigurationValue);
                            break;
                        case (int)ConfigParams.FailedOnlineOrderCleanupDays:
                            txtFailedOnlineOrderCleanupdays.Text = imixConfigValue.ConfigurationValue.ToString();
                            break;
                        default:
                            break;
                    }
                }
            }
        }
        private void GetBackupNewConfigData()
        {
            txtTables.Text = string.Empty;
            //var item1 = _objDataLayer.GetConfigurationData(LoginUser.SubStoreId);
            ConfigBusiness conBiz = new ConfigBusiness();
            var item = conBiz.GetConfigurationData(LoginUser.SubStoreId);
            if (item != null)
            {
                string hotfolder = item.DG_Hot_Folder_Path;
                hiddenHFPath.Text = hotfolder;
            }
            //_objDataLayer = new DigiPhotoDataServices();
            //List<iMIXConfigurationValue> _objdata = _objDataLayer.GetNewConfigValues(LoginUser.SubStoreId);

            List<iMIXConfigurationInfo> _objdata = conBiz.GetNewConfigValues(LoginUser.SubStoreId);
            if (_objdata != null)
            {
                foreach (iMIXConfigurationInfo imixConfigValue in _objdata)
                {
                    switch (imixConfigValue.IMIXConfigurationMasterId)
                    {
                        case (int)ConfigParams.DbBackupPath:// For Backup
                            txtDbBackupPath.Text = Convert.ToString(imixConfigValue.ConfigurationValue);
                            if (!String.IsNullOrWhiteSpace(txtDbBackupPath.Text))
                                btnBackupNow.IsEnabled = true;
                            break;

                        case (int)ConfigParams.CleanUpTable:// For Backup
                            txtTables.Text = Convert.ToString(imixConfigValue.ConfigurationValue);
                            string[] valuesArray = txtTables.Text.Split(',');
                            List<string> names = valuesArray.ToList();

                            //Get all table names
                            //List<vw_GetAllTable> _objTabledata = _objDataLayer.GetAllTable();
                            CommonBusiness comBiz = new CommonBusiness();
                            List<TableBaseInfo> _objTabledata = comBiz.GetAllTable();
                            List<Action> abc = new List<Action>();
                            if (_objTabledata != null)
                                foreach (var datas in _objTabledata)
                                {
                                    Action data = new Action();
                                    data.IsSelected = false;
                                    data.Name = datas.name;
                                    abc.Add(data);
                                }

                            foreach (var datas in abc)
                            {
                                foreach (string s in names)
                                {
                                    if (datas.Name.Trim() == s.Trim())
                                    {
                                        datas.IsSelected = true;
                                    }
                                }
                            }
                            listtables.ItemsSource = abc;
                            break;

                        case (int)ConfigParams.HfBackupPath:// For Backup
                            txtHfBackupPath.Text = Convert.ToString(imixConfigValue.ConfigurationValue);
                            break;

                        case (int)ConfigParams.ScheduleBackup:// For Backup
                            if (chkSchBackup.IsChecked == true)
                            {
                                ErrorHandler.ErrorHandler.LogFileWrite("Schedule bkp at :" + imixConfigValue.ConfigurationValue);
                                DateTime dtsch = DateTime.Parse(imixConfigValue.ConfigurationValue);
                                ccBackupDateTime.Value = dtsch;
                            }
                            else
                            {
                                ccBackupDateTime.Value = DateTime.Now.Date.Add(new TimeSpan(6, 0, 0));
                            }
                            break;

                        case (int)ConfigParams.IsBackupScheduled:// For Backup
                            chkSchBackup.IsChecked = imixConfigValue.ConfigurationValue.ToBoolean();
                            break;

                        case (int)ConfigParams.IsRecursive:// For Backup
                            chkRecursive.IsChecked = imixConfigValue.ConfigurationValue.ToBoolean();
                            break;

                        case (int)ConfigParams.IntervalCount:// For Backup
                            cmbSelectRecursiveCount.SelectedValue = Convert.ToString(imixConfigValue.ConfigurationValue);
                            break;

                        case (int)ConfigParams.IntervalType:// For Backup
                            cmbSelectRecursiveTime.SelectedValue = Convert.ToString(imixConfigValue.ConfigurationValue);
                            break;

                        case (int)ConfigParams.CleanUpDaysBackup:// For Backup
                            txtCleanupdays.Text = Convert.ToString(imixConfigValue.ConfigurationValue);
                            break;

                        default:
                            break;
                    }
                }



                //Get selected table names


            }
            else
            {
                string[] valuesArray = txtTables.Text.Split(',');
                List<string> names = valuesArray.ToList();

                //Get all table names
                //List<vw_GetAllTable> _objTabledata = _objDataLayer.GetAllTable();
                CommonBusiness comBiz = new CommonBusiness();
                List<TableBaseInfo> _objTabledata = comBiz.GetAllTable();
                List<Action> abc = new List<Action>();
                if (_objTabledata != null)
                    foreach (var datas in _objTabledata)
                    {
                        Action data = new Action();
                        data.IsSelected = false;
                        data.Name = datas.name;

                        abc.Add(data);
                    }

                foreach (var datas in abc)
                {
                    foreach (string s in names)
                    {
                        if (datas.Name.Trim() == s.Trim())
                        {
                            datas.IsSelected = true;


                        }
                    }
                }
                listtables.ItemsSource = abc;
                txtDbBackupPath.Text = String.Empty;
                txtTables.Text = String.Empty;
                txtCleanupdays.Text = string.Empty;
                txtHfBackupPath.Text = String.Empty;
                chkSchBackup.IsChecked = false;
                ccBackupDateTime.Value = DateTime.Now.Date.Add(new TimeSpan(6, 0, 0));
            }
        }
        private void fillRecursiveNewOptions()
        {
            cmbSelectRecursiveCount.Items.Clear();
            for (int d = 1; d < 30; d++)
            {
                cmbSelectRecursiveCount.Items.Add(d.ToString());
            }

            cmbSelectRecursiveTime.Items.Clear();
            cmbSelectRecursiveTime.Items.Add(IntervalType.Day.ToString());
            cmbSelectRecursiveTime.Items.Add(IntervalType.Week.ToString());
            cmbSelectRecursiveTime.Items.Add(IntervalType.Month.ToString());
        }

        private void btnBackupNow_Click(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrEmpty(txtDbBackupPath.Text.Trim()))
            {
                MessageBox.Show("Select database backup path.", "Digiphoto", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            if (string.IsNullOrEmpty(txtHfBackupPath.Text.Trim()))
            {
                MessageBox.Show("Select hot folder backup path.", "Digiphoto", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            if (string.IsNullOrEmpty(txtCleanupdays.Text.Trim()))
            {
                MessageBox.Show("Select No of days for Cleanup Backup.", "Digiphoto", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            if (string.IsNullOrEmpty(txtFailedOnlineOrderCleanupdays.Text.Trim()))
            {
                MessageBox.Show("Select No of days for failed online orders Backup.", "Digiphoto", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            else if (Convert.ToInt32(txtCleanupdays.Text.Trim()) > Convert.ToInt32(txtFailedOnlineOrderCleanupdays.Text.Trim()))
            {
                MessageBox.Show("Failed online orders cleanup days should be grater then backup cleanup days!", "Digiphoto", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            // bs.Show();
            string appConString = "";
            appConString = System.Configuration.ConfigurationManager.ConnectionStrings["DigiConnectionString"].ConnectionString;
            string[] strVar = appConString.Split(';');
            Hashtable htCon = new Hashtable();
            if (strVar.Length != 0)
            {
                for (int k = 0; k <= strVar.Length - 1; k++)
                {
                    string[] tempK = strVar[k].Split('=');
                    if (tempK.Length != 0)
                    {
                        if (!htCon.ContainsKey(tempK[0]))
                        {
                            htCon.Add(tempK[0], tempK[1]);
                        }
                    }
                }
            }

            string ConfigUsername = ConfigurationManager.AppSettings["UserName"];// Added by Suraj.
            ConfigUsername = DecryptConfigvalues.DecryptValue(ConfigUsername);// Added by Suraj.
            string ConfigPassword = ConfigurationManager.AppSettings["UserPass"];// Added by Suraj.
            ConfigPassword = DecryptConfigvalues.DecryptValue(ConfigPassword);// Added by Suraj.
            _databasename = htCon.ContainsKey("Initial Catalog") ? htCon["Initial Catalog"].ToString() : ConfigurationManager.AppSettings["DatabaseName"];
            //_username = htCon.ContainsKey("User ID") ? htCon["User ID"].ToString() : ConfigurationManager.AppSettings["UserName"];
            _username = htCon.ContainsKey("User ID") ? htCon["User ID"].ToString() : ConfigUsername;  // Added by Suraj.
            //_pass = htCon.ContainsKey("Password") ? htCon["Password"].ToString() : ConfigurationManager.AppSettings["UserPass"];
            _pass = htCon.ContainsKey("Password") ? htCon["Password"].ToString() : ConfigPassword;   // Added by Suraj.
            //_server = htCon.ContainsKey("Data Source") ? htCon["Data Source"].ToString() : ConfigurationManager.AppSettings["UserPass"];
            _server = htCon.ContainsKey("Data Source") ? htCon["Data Source"].ToString() : ConfigPassword;  // Added by Suraj.
            _destinationPath = txtDbBackupPath.Text;
            _imgSource = hiddenHFPath.Text;
            _imgDestination = txtHfBackupPath.Text;
            //List<vw_GetAllTable> _objTabledata = _objDataLayer.GetAllTable();

            CommonBusiness comBiz = new CommonBusiness();
            List<TableBaseInfo> _objTabledata = comBiz.GetAllTable();
            listtables.DataContext = _objTabledata;
            _heavyTables = txtTables.Text;
            _cleanupdaysBackup = txtCleanupdays.Text.ToInt32();
            _FailedOnlineCleanupdayes = txtFailedOnlineOrderCleanupdays.Text.ToInt32();
            bs.Show();
            BackupWorker.RunWorkerAsync();

        }
        private void BackupWorker_DoWork(object Sender, System.ComponentModel.DoWorkEventArgs e)
        {

            errorMsg = "Error Message: There is not enough space in the disk/Drive not found.";
            try
            {
                int errorId = 0;
                bool isBackupFailed = false;
                if (FrameworkHelper.Common.clsBackup.IsMemoryAvailable(_imgSource, _imgDestination, LoginUser.SubStoreId))
                {
                    e.Result = DoBackupNow(ref errorId);
                    if (errorId == (int)BackupErrorMessage.DestinationDiskFull)
                        isBackupFailed = true;
                }
                else
                {
                    isBackupFailed = true;
                }
                if (isBackupFailed)
                {
                    //MessageBox.Show(this, errorMsg, "Digiphoto", MessageBoxButton.OK, MessageBoxImage.Information);
                    ErrorHandler.ErrorHandler.LogFileWrite(errorMsg);
                    SaveEmailDetails(errorMsg);

                }
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogFileWrite(ex.Message + errorMsg);
            }
        }
        private void BackupWorker_RunWorkerCompleted(object Sender, System.ComponentModel.RunWorkerCompletedEventArgs e)
        {
            bs.Hide();
            //GetBackupConfigData();
            MessageBox.Show(this, e.Result == null ? errorMsg : e.Result.ToString(), "Digiphoto", MessageBoxButton.OK, MessageBoxImage.Information);
            GetBackupNewConfigData();
        }


        private object DoBackupNow(ref int errorId)
        {
            bool isBackupDBSuccess; bool isTableCleaned; bool isImagesCompressed; string strMsg = ""; object returnMsg;
            try
            {
                if (!String.IsNullOrEmpty(_destinationPath) && !String.IsNullOrWhiteSpace(_destinationPath))
                {
                    if (Directory.Exists(_destinationPath))
                    {
                        isBackupDBSuccess = FrameworkHelper.Common.clsBackup.BackupDatabase(_databasename, _username, _pass, _server, _destinationPath, ref errorId);
                        if (!isBackupDBSuccess && errorId == (int)BackupErrorMessage.DestinationDiskFull)
                        {
                            strMsg += "ERROR : There is not enough space in the disk.";
                            return returnMsg = strMsg;
                        }
                        if (isBackupDBSuccess)
                        {
                            //MessageBox.Show("Database backup complete!", "Digiphoto", MessageBoxButton.OK, MessageBoxImage.Information);
                            strMsg += "- Database backup complete! \n\r\n\r";
                            if (!String.IsNullOrEmpty(_heavyTables))
                            {
                                if (!_heavyTables.EndsWith(","))
                                {
                                    _heavyTables = _heavyTables + ",";
                                }
                                isTableCleaned = FrameworkHelper.Common.clsBackup.EmptyTables(_heavyTables, _cleanupdaysBackup, _cleanupdaysBackup, LoginUser.SubStoreId,_FailedOnlineCleanupdayes);
                                if (isTableCleaned)
                                {
                                    strMsg += "- Specified tables have been emptied! \n\r\n\r";
                                    //MessageBox.Show("Specified tables have been emptied!", "Digiphoto", MessageBoxButton.OK, MessageBoxImage.Information);
                                    if (!String.IsNullOrEmpty(_imgDestination) && !String.IsNullOrWhiteSpace(_imgDestination))
                                    {
                                        if (Directory.Exists(_imgDestination))
                                        {
                                            if (!String.IsNullOrEmpty(_imgSource))
                                            {
                                                isImagesCompressed = true;
                                                string destinationFld = FrameworkHelper.Common.clsBackup.CreateBackupFolderExits(_imgSource, _imgDestination);
                                                if (isImagesCompressed)
                                                {
                                                    int delImgCount = 0;

                                                    if (!String.IsNullOrEmpty(_imgSource))
                                                    {
                                                        int tableCount = 1;//for the first time
                                                        while (tableCount != 0)
                                                        {
                                                            tableCount = FrameworkHelper.Common.clsBackup.GetPhotoArchived();
                                                            if (tableCount > 0)
                                                            {
                                                                DoBackupNowDaily(LoginUser.SubStoreId, _imgSource);
                                                                delImgCount += FrameworkHelper.Common.clsBackup.CopyDirectoryFiles(destinationFld, _imgSource);
                                                                bool statusUpdated = FrameworkHelper.Common.clsBackup.UpdateArchivedPhotoDetails();
                                                            }
                                                        }
                                                        strMsg += "- " + delImgCount.ToString() + " images deleted! \n\r\n\r";
                                                    }
                                                }
                                                else
                                                {
                                                    throw new Exception("Images not Compressed");
                                                }
                                            }
                                            else
                                            {
                                                //MessageBox.Show("Hot folder path has not been set!", "Digiphoto", MessageBoxButton.OK, MessageBoxImage.Error);
                                                returnMsg = "Hot folder path has not been set!";
                                            }
                                        }
                                        else
                                        {
                                            //MessageBox.Show("Drive Not Exists");
                                            return returnMsg = "Drive Not Exists";

                                        }
                                    }
                                    else
                                    {

                                        //MessageBox.Show("Image Destination Path is Null");
                                        return returnMsg = "Image Destination Path is Null";
                                    }
                                }

                                else
                                {

                                    //MessageBox.Show("specified Tables are not Cleaned");
                                    return returnMsg = "Specified Tables are not Cleaned";
                                }
                            }
                            else
                            {
                                //MessageBox.Show("No tables specified!", "Digiphoto", MessageBoxButton.OK, MessageBoxImage.Error);
                                return returnMsg = "No tables specified!";
                            }
                        }
                        else
                        {
                            //MessageBox.Show("DB Backup operation could not be executed");
                            return returnMsg = "DB Backup operation could not be executed";

                        }
                    }

                    else
                    {
                        //MessageBox.Show("Drive Not Exists");
                        return returnMsg = "Drive Not Exists";

                    }
                }
                else
                {
                    //MessageBox.Show("Destination Path is not correct");
                    return returnMsg = "Destination Path is not correct";

                }
                returnMsg = "Backup Complete\n\r\n\r" + strMsg;
                //MessageBox.Show("Backup Complete\n\r\n\r" + strMsg, "Digiphoto", MessageBoxButton.OK, MessageBoxImage.Information);

            }
            catch (Exception ex)
            {
                //MessageBox.Show("There was some error in backup! Error: " + ex.Message, "Digiphoto", MessageBoxButton.OK, MessageBoxImage.Error);
                returnMsg = "There was some error in backup! Error: " + ex.Message;
            }
            return returnMsg;
        }

        private bool SaveEmailDetails(string exceptionMsg)
        {
            ConfigBusiness buss = new ConfigBusiness();
            Dictionary<long, string> iMIXConfigurations = new Dictionary<long, string>();
            iMIXConfigurations = buss.GetConfigurations(iMIXConfigurations, LoginUser.SubStoreId);
            string ToAddress = iMIXConfigurations.Where(x => x.Key == (int)ConfigParams.BackUpEmailAddress).FirstOrDefault().Value;
            if (ToAddress != null)
            {
                bool success = buss.SaveEmailDetails(ToAddress, string.Empty, string.Empty, exceptionMsg, "BACKUP FAILED", LoginUser.SubStoreId);
            }
            return true;
        }
        /// <summary>
        /// Fills all combo.
        /// </summary>
        public static void CopyAllFile(DirectoryInfo source, DirectoryInfo target)
        {
            // Copy each file into the new directory.
            foreach (FileInfo fi in source.GetFiles())
            {
                if (!File.Exists(target + @"\" + fi.Name))
                    fi.CopyTo(System.IO.Path.Combine(target.FullName, fi.Name));

            }
        }
        private static void DoBackupNowDaily(Int32 subStoreId, string _imgSource)
        {
            try
            {
                string _destinationPathDaily = string.Empty;
                string _filePathDate = string.Empty;
                string _thumbnailFilePathDate = string.Empty;
                string _bigThumbnailFilePathDate = string.Empty;
                string _destinationPathDailySite = string.Empty;
                string _filePathDateSource = string.Empty;
                string _thumbnailFilePathDateSource = string.Empty;
                string _bigThumbnailFilePathDateSource = string.Empty;
                iMIXConfigurationInfo _objdata = (new ConfigBusiness()).GetNewConfigValues(subStoreId)
                    .Where(o => o.IMIXConfigurationMasterId == 95).FirstOrDefault();
                if (_objdata != null && !string.IsNullOrEmpty(_objdata.ConfigurationValue))
                {
                    _destinationPathDaily = _objdata.ConfigurationValue.ToString();
                    if (!String.IsNullOrEmpty(_destinationPathDaily) && !String.IsNullOrWhiteSpace(_destinationPathDaily))
                    {
                        if (Directory.Exists(_destinationPathDaily))
                        {
                            if (!String.IsNullOrEmpty(_imgSource))
                            {
                                if (Directory.Exists(_imgSource))
                                {
                                    _destinationPathDailySite = System.IO.Path.Combine(_destinationPathDaily, "Site" + subStoreId.ToString());
                                    _filePathDate = System.IO.Path.Combine(_destinationPathDailySite, DateTime.Now.ToString("yyyyMMdd"));
                                    _thumbnailFilePathDate = System.IO.Path.Combine(_destinationPathDailySite, "Thumbnails", DateTime.Now.ToString("yyyyMMdd"));
                                    _bigThumbnailFilePathDate = System.IO.Path.Combine(_destinationPathDailySite, "Thumbnails_Big", DateTime.Now.ToString("yyyyMMdd"));

                                    _filePathDateSource = System.IO.Path.Combine(_imgSource, DateTime.Now.ToString("yyyyMMdd"));
                                    _thumbnailFilePathDateSource = System.IO.Path.Combine(_imgSource, "Thumbnails", DateTime.Now.ToString("yyyyMMdd"));
                                    _bigThumbnailFilePathDateSource = System.IO.Path.Combine(_imgSource, "Thumbnails_Big", DateTime.Now.ToString("yyyyMMdd"));


                                    if (Directory.Exists(_filePathDateSource))
                                    {
                                        if (!Directory.Exists(_filePathDate))
                                        {
                                            Directory.CreateDirectory(_filePathDate);
                                        }
                                        CopyAllFile(new DirectoryInfo(_filePathDateSource), new DirectoryInfo(_filePathDate));
                                    }
                                    if (Directory.Exists(_thumbnailFilePathDateSource))
                                    {
                                        if (!Directory.Exists(_thumbnailFilePathDate))
                                        {
                                            Directory.CreateDirectory(_thumbnailFilePathDate);
                                        }
                                        CopyAllFile(new DirectoryInfo(_thumbnailFilePathDateSource), new DirectoryInfo(_thumbnailFilePathDate));
                                    }
                                    if (Directory.Exists(_bigThumbnailFilePathDateSource))
                                    {
                                        if (!Directory.Exists(_bigThumbnailFilePathDate))
                                        {
                                            Directory.CreateDirectory(_bigThumbnailFilePathDate);
                                        }
                                        CopyAllFile(new DirectoryInfo(_bigThumbnailFilePathDateSource), new DirectoryInfo(_bigThumbnailFilePathDate));
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }
        private void FillAllCombo()
        {
            lstlogoList = new Dictionary<string, string>();
            lstBackgroundList = new Dictionary<string, string>();
            lstFilterList = new Dictionary<string, string>();
            lstCurrencyList = new Dictionary<string, string>();
            lstBGList = new Dictionary<string, string>();
            lstCurrencyList.Add("--Select--", "0");
            lstlogoList.Add("--Select--", "0");
            lstBackgroundList.Add("--Select--", "0");
            lstBGList.Add("--Select--", "0");

            BindBorders();
            BindTheme(); //Loading theme DropDown Value Added By Ajinkya.

        }

        /// <summary>
        /// Fills the product combo.
        /// </summary>
        private void FillProductCombo()
        {
            try
            {
                var productTypes = (new ProductBusiness()).GetProductType();
                var primaryProduct = productTypes.Where(t => t.DG_IsPrimary == true).ToList();
                CommonUtility.BindComboWithSelect<ProductTypeInfo>(CmbProductType, productTypes, "DG_Orders_ProductType_Name", "DG_Orders_ProductType_pkey", 0, ClientConstant.SelectString);
                CmbProductType.SelectedValue = "1";
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }

        private void FillLocationCombo()
        {
            try
            {
                var locSubstoreBased = (new StoreSubStoreDataBusniess()).GetLocationSubstoreWise(LoginUser.SubStoreId);
                if (locSubstoreBased != null && locSubstoreBased.Count > 0)
                {
                    CommonUtility.BindCombo<LocationInfo>(cmb_Location, locSubstoreBased, "DG_Location_Name", "DG_Location_pkey");
                    cmb_Location.SelectedIndex = 0;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }
        /// <summary>
        /// Fills the product list.
        /// </summary>


        private void FillProductList()
        {
            try
            {
                string productToRemove = "95,96,97";
                string[] productToRemoveArray = productToRemove.Split(',');
                var oList = (new ProductBusiness()).GetProductType().Where(x => !productToRemoveArray.Contains(x.DG_Orders_ProductType_pkey.ToString())).ToList();
                // CommonUtility.BindListBox(lstProducttype, oList);
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }
        private void GetBackgrounds()
        {
            var backgroundBusiness = new BackgroundBusiness();
            BackgroundList = new ObservableCollection<BackGroundInfo>();
            var backgrounds = backgroundBusiness.GetBackgoundDetails();

            if (backgrounds != null && backgrounds.Count > 0)
            {
                BackgroundList = new ObservableCollection<BackGroundInfo>(backgrounds);
                foreach (var background in BackgroundList)
                    if (background.IsPanoramicLabel == "Yes")
                    {
                        background.DG_BackgroundPath = LoginUser.DigiFolderBackGroundPath + @"\Thumbnails\Panorama\" + background.DG_BackGround_Image_Name;
                    }
                    else
                    {
                        background.DG_BackgroundPath = LoginUser.DigiFolderBackGroundPath + @"\Thumbnails\" + background.DG_BackGround_Image_Name;
                    }
            }
            #region Commented
            //BackGroundInfo  objBagckground=new BackGroundInfo();
            //var backgroundDetails = (new BackgroundBusiness()).GetBackgoundDetails();
            //List<BackGroundInfo> lst = new List<BackGroundInfo>();

            //foreach (var item in backgroundDetails)
            //{
            //    objBagckground = new BackGroundInfo();
            //    objBagckground.DG_BackGround_Image_Display_Name = item.DG_BackGround_Image_Display_Name;
            //    objBagckground.DG_BackgroundPath = LoginUser.DigiFolderBackGroundPath + "\\Thumbnails\\" + item.DG_BackGround_Image_Name;
            //    objBagckground.DG_Background_pkey = item.DG_Background_pkey;
            //    objBagckground.DG_BackGround_Image_Name = item.DG_BackGround_Image_Name;
            //    lst.Add(objBagckground);
            //}
            //DGManageBackground.ItemsSource = lst;
            #endregion
        }
        /// <summary>
        /// Gets the back ground details.
        /// </summary>
        //private void GetBackGroundDetails()
        //{
        //    try
        //    {
        //        string productToRemove = "95,96,97";
        //        string[] productToRemoveArray = productToRemove.Split(',');
        //        lstmainBGList = new List<BGMaster>();
        //        BGMaster _objnew;
        //        List<AllProductType> _objdetail;
        //        lstbg.Items.Clear();
        //        var backgroundDetails = (new BackgroundBusiness()).GetBackgoundDetails();

        //        ProductBusiness proBiz = new ProductBusiness();
        //        var productType = proBiz.GetProductType();

        //        foreach (var item in backgroundDetails)
        //        {
        //            _objnew = new BGMaster();
        //            _objnew.BgDisplayName = item.DG_BackGround_Image_Display_Name;
        //            _objnew.BgID = item.DG_Background_pkey;
        //            _objnew.BgName = item.DG_BackGround_Image_Name;
        //            _objnew.Filepath = LoginUser.DigiFolderBackGroundPath + "\\Thumbnails\\" + item.DG_BackGround_Image_Name;
        //            _objdetail = new List<AllProductType>();

        //            foreach (var items in productType)
        //            {

        //                AllProductType _objnewpro = new AllProductType();
        //                _objnewpro.ProductId1 = items.DG_Orders_ProductType_pkey;
        //                _objnewpro.BGId = item.DG_Background_pkey;
        //                _objnewpro.ProductName1 = items.DG_Orders_ProductType_Name;
        //                if (proBiz.CheckIsVisibleProductForBackGround(item.DG_Background_pkey, items.DG_Orders_ProductType_pkey))
        //                {
        //                    _objnewpro.Isvisible1 = Visibility.Visible;
        //                }
        //                else
        //                {
        //                    _objnewpro.Isvisible1 = Visibility.Collapsed;
        //                }
        //                _objdetail.Add(_objnewpro);
        //            }

        //                _objnew.LstProductType = _objdetail.Where(x => !productToRemoveArray.Contains(x.ProductId1.ToString())).ToList();
        //                lstbg.Items.Add(_objnew);
        //                lstbg.SelectedItem = _objnew;
        //            }
        //        }




        //    catch (Exception ex)
        //    {
        //        string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
        //        ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
        //    }

        //}

        /// <summary>
        /// Gets the graphics details.
        /// </summary>
        /// 
        private void GetGraphicsDetails()
        {
            List<AllGraphics> _objgraphicsdetail = new List<AllGraphics>();
            var objGraphicsDetails = (new GraphicsBusiness()).GetGraphicsDetails();
            try
            {
                foreach (var item in objGraphicsDetails)
                {
                    AllGraphics _objnew = new AllGraphics();
                    _objnew.GraphicsName1 = item.DG_Graphics_Name;
                    _objnew.GraphicsDisplayName1 = item.DG_Graphics_Displayname;
                    _objnew.IsActive1 = (bool)item.DG_Graphics_IsActive;
                    _objnew.Filepath1 = LoginUser.DigiFolderGraphicsPath + "\\" + item.DG_Graphics_Name;
                    _objnew.Pkey1 = item.DG_Graphics_pkey;
                    _objgraphicsdetail.Add(_objnew);
                }

                DGManageGraphics.ItemsSource = _objgraphicsdetail;
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
            }

        }
        /// <summary>
        /// Gets the permissions.
        /// </summary>
        private void GetPermissions()
        {
            RolePermissionsBusniess rolBiz = new RolePermissionsBusniess();
            List<PermissionRoleInfo> _objPList = rolBiz.GetPermissionData(LoginUser.RoleId);

            var Useritem = _objPList.Where(t => t.DG_Permission_Id == 26).FirstOrDefault();
            if (Useritem != null)
            {
                chkEnableGrouping.Visibility = Visibility.Visible;
                tbenablegroup.Visibility = Visibility.Visible;
            }
            else
            {
                chkEnableGrouping.Visibility = Visibility.Collapsed;
                tbenablegroup.Visibility = Visibility.Collapsed;
            }

            foreach (var Permissions in _objPList)
            {
                switch (Permissions.DG_Permission_Id)
                {
                    case 1:
                    case 3:
                    case 4:
                    case 6:
                        break;
                    case 2:
                        {
                            TabSpecPrinting.Visibility = Visibility.Visible;
                            break;
                        }
                    case 5:
                        {
                            TabConfiguration.Visibility = Visibility.Visible;
                            break;
                        }
                    case 7:
                        {
                            TabManageBorder.Visibility = Visibility.Visible;
                            break;
                        }
                    case 8:
                        {
                            TabManageBackground.Visibility = Visibility.Visible;
                            break;
                        }
                    case 9:
                        {
                            TabManageCurrency.Visibility = Visibility.Visible;
                            break;
                        }
                    case 13:
                        {
                            TabManageGraphics.Visibility = Visibility.Visible;
                            break;
                        }
                    case 14:
                        {
                            TabManageDiscount.Visibility = Visibility.Visible;
                            break;
                        }
                    //case 21:
                    //     {
                    //       TabSSConfiguration.Visibility = Visibility.Visible;
                    //         break;
                    //  }
                    case 23:
                        {
                            TabBackup.Visibility = Visibility.Visible;
                            break;
                        }
                    case 25:
                        {
                            Tabdigimagic.Visibility = Visibility.Visible;
                            break;
                        }
                    case 26:
                        {
                            TabManageAudio.Visibility = Visibility.Visible;
                            //SetDefaultButton(btnSaveSelectAudio);
                            break;
                        }

                    case 27:
                        {
                            TabManageVideo.Visibility = Visibility.Visible;
                            //SetDefaultButton(btnSaveSelectVideo);
                            break;
                        }

                    case 28:
                        {
                            TabManageVideoBG.Visibility = Visibility.Visible;
                            //SetDefaultButton(btnSaveVideoBG);
                            break;
                        }
                    case 29:
                        {
                            TabOnlineConfig.Visibility = Visibility.Visible;
                            break;
                        }
                    case 36:
                        {
                            TabManageScene.Visibility = Visibility.Visible;
                            break;
                        }
                    case 37:
                        {
                            TabSSConfiguration.Visibility = Visibility.Visible;
                            break;
                        }
                    case 38:
                        {
                            TabVenueConfiguration.Visibility = Visibility.Visible;
                            break;
                        }
                    case 40:
                        {
                            TabCharacterConfiguration.Visibility = Visibility.Visible;
                            break;
                        }


                }
            }

            for (int i = 0; i < 10; i++)
            {
                if (((TabItem)(tbmain.Items[i])).Visibility == Visibility.Visible)
                {
                    ((TabItem)(tbmain.Items[i])).IsSelected = true;
                    break;
                }
            }
        }

        //private void SetDefaultButton(Button btn)
        //{
        //    //Configuration Tab Save
        //    btnSaveChanges.IsDefault = false;
        //    //Spec Printing
        //    btnSaveAutoCorrect.IsDefault = false;
        //    //Manage Border
        //    btnSaveSelectBorder.IsDefault = false;
        //    //Manage Background
        //    btnSaveSelectBackground.IsDefault = false;
        //    //Manage Currency
        //    btnSaveCurrency.IsDefault = false;
        //    //Manage Graphics
        //    btnSaveSelectGraphics.IsDefault = true;
        //    //Manage Discount
        //    btnSaveDiscount.IsDefault = false;
        //    //Sub-store configuration
        //    btnSaveSubStore.IsDefault = false;
        //    //Back up
        //    btnBackupBack.IsDefault = false;
        //    //set Default
        //    btn.IsDefault = true;
        //    // digimagic
        //    btnSaveDigimagic.IsDefault = false;
        //}

        #endregion

        #region Events
        private void btnDefault_Click(object sender, RoutedEventArgs e)
        {

        }

        string graphicsname = string.Empty;
        string graphicsdisplayname = string.Empty;
        /// <summary>
        /// Handles the Click event of the btnSelectGraphics control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void btnSelectGraphics_Click(object sender, RoutedEventArgs e)
        {
            if (EditableGraphics != null)
                return;
            Stream checkstream = null;
            Microsoft.Win32.OpenFileDialog ofd = new Microsoft.Win32.OpenFileDialog();
            ofd.Multiselect = false;
            //ofd.Filter = "All Image Files | *.*";
            ofd.Filter = "Select Image|*.gif;*.png;|" + "GIF|*.gif;*.GIF|PNG|*.png;*.PNG";
            if ((bool)ofd.ShowDialog())
            {
                try
                {
                    if ((checkstream = ofd.OpenFile()) != null)
                    {
                        txtSelectedgraphics.Text = ofd.FileName.ToString();
                        graphicsname = System.IO.Path.GetFileNameWithoutExtension(ofd.FileName);
                        graphicsdisplayname = System.IO.Path.GetFileName(ofd.FileName);
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Error: Could not read file from disk. Original error: " + ex.Message);
                }
            }
        }

        /// <summary>
        /// Handles the Click event of the btnSaveSelectGraphics control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void btnSaveSelectGraphics_Click(object sender, RoutedEventArgs e)
        {
            if (CheckGraphicsValidations())
            {
                try
                {
                    if (EditableGraphics != null)
                    {
                        Guid _obj = Guid.NewGuid();
                        string destFile = string.Empty;
                        if (string.IsNullOrEmpty(graphicsname) && string.IsNullOrEmpty(graphicsdisplayname))
                        {
                            graphicsname = EditableGraphics.GraphicsName1;
                            graphicsdisplayname = EditableGraphics.GraphicsDisplayName1;
                        }

                        else
                        {
                            graphicsname = graphicsname + _obj.ToString() + ".png";
                            destFile = LoginUser.DigiFolderGraphicsPath + "\\" + graphicsname;
                            if (graphicsdisplayname != EditableGraphics.GraphicsDisplayName1)
                                System.IO.File.Copy(txtSelectedgraphics.Text, destFile, true);
                        }

                        //_objDataLayer = new DigiPhotoDataServices();
                        string SyncCode = CommonUtility.GetUniqueSynccode(Convert.ToInt32(ApplicationObjectEnum.Graphic).ToString().PadLeft(2, '0'), LoginUser.countrycode, LoginUser.Storecode, "00");
                        //string SyncCode = CommonUtility.GetRandomString(8) + Convert.ToInt32(ApplicationObjectEnum.Graphic).ToString().PadLeft(2, '0');
                        //_objDataLayer.SetGraphicsDetails(graphicsname, graphicsdisplayname, true, SyncCode);
                        GraphicsBusiness graBiz = new GraphicsBusiness();
                        bool isActive = (IsGraphicsActive.IsChecked.HasValue ? IsGraphicsActive.IsChecked.Value : false);
                        graBiz.UpdateGraphicsDetails(graphicsname, graphicsdisplayname, isActive, SyncCode, EditableGraphics.Pkey1, LoginUser.UserId);
                        //IsGraphicsActive.IsChecked = false;
                        if (!string.IsNullOrEmpty(destFile))
                        {
                            //Copy selected item to all substore
                            CopyToAllSubstore(lstConfigurationInfo, destFile, "", "Graphics");
                        }

                        txtSelectedgraphics.Text = "";
                        graphicsname = string.Empty;
                        graphicsdisplayname = string.Empty;
                        AuditLog.AddUserLog(Common.LoginUser.UserId, (int)FrameworkHelper.ActionType.AddGraphics, "Add/Edit Graphics at ");
                        GetGraphicsDetails();
                        MessageBox.Show("Graphic saved Successfully");
                    }
                    else
                    {
                        Guid _obj = Guid.NewGuid();
                        graphicsname = graphicsname + _obj.ToString() + ".png";
                        string destFile = LoginUser.DigiFolderGraphicsPath + "\\" + graphicsname;
                        System.IO.File.Copy(txtSelectedgraphics.Text, destFile, true);

                        //_objDataLayer = new DigiPhotoDataServices();
                        string SyncCode = CommonUtility.GetUniqueSynccode(Convert.ToInt32(ApplicationObjectEnum.Graphic).ToString().PadLeft(2, '0'), LoginUser.countrycode, LoginUser.Storecode, "00");
                        //string SyncCode = CommonUtility.GetRandomString(8) + Convert.ToInt32(ApplicationObjectEnum.Graphic).ToString().PadLeft(2, '0');
                        //_objDataLayer.SetGraphicsDetails(graphicsname, graphicsdisplayname, true, SyncCode);
                        GraphicsBusiness graBiz = new GraphicsBusiness();
                        bool isActive = (IsGraphicsActive.IsChecked.HasValue ? IsGraphicsActive.IsChecked.Value : false);
                        graBiz.SetGraphicsDetails(graphicsname, graphicsdisplayname, isActive, SyncCode, LoginUser.UserId);
                        //IsGraphicsActive.IsChecked = false;
                        //Copy selected item to all substore
                        CopyToAllSubstore(lstConfigurationInfo, destFile, "", "Graphics");

                        txtSelectedgraphics.Text = "";
                        AuditLog.AddUserLog(Common.LoginUser.UserId, (int)FrameworkHelper.ActionType.AddGraphics, "Add/Edit Graphics at ");
                        GetGraphicsDetails();
                        MessageBox.Show("Graphic saved Successfully");
                    }
                }
                catch (Exception ex)
                {
                    string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                    ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                }

            }
            graphicsname = graphicsdisplayname = string.Empty;
        }

        /// <summary>
        /// Handles the Click event of the btnDeletegraphics control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void btnDeletegraphics_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                AllGraphics selectedGraphic = null;
                Button btnSender = (Button)(sender);
                if (btnSender != null && btnSender.DataContext != null)
                {
                    try
                    {
                        var editedGraphics = (AllGraphics)btnSender.DataContext;


                        selectedGraphic = (AllGraphics)editedGraphics.Clone();
                    }
                    catch (Exception ex)
                    {
                        selectedGraphic = null;
                    }
                    if (selectedGraphic != null)
                    {

                        #region Check for dependency
                        string statusMessage = string.Empty;
                        bool foundDependency = FindDependencyOfGraphic(selectedGraphic.Pkey1, selectedGraphic.GraphicsName1, selectedGraphic.GraphicsDisplayName1, "delete", out statusMessage);
                        if (foundDependency)
                        {
                            MsgBox.ShowHandlerDialog(statusMessage, DigiMessageBox.DialogType.OK);
                            return;
                        }

                        #endregion
                    }

                    if (MessageBox.Show("Do you want to delete this graphic?", "Confirm delete", MessageBoxButton.YesNo) == MessageBoxResult.Yes)
                    {
                        GraphicsBusiness graBiz = new GraphicsBusiness();
                        bool isdelete = graBiz.DeleteGraphics(selectedGraphic.Pkey1);
                        if (isdelete)
                        {

                            if (System.IO.File.Exists(LoginUser.DigiFolderGraphicsPath + "\\" + selectedGraphic.GraphicsName1))
                            {
                                System.IO.File.Delete(LoginUser.DigiFolderGraphicsPath + "\\" + selectedGraphic.GraphicsName1);
                                DeleteFromAllSubstore(lstConfigurationInfo, selectedGraphic.GraphicsName1, "Graphics");
                            }
                            MessageBox.Show("Graphic deleted successfully");
                            AuditLog.AddUserLog(Common.LoginUser.UserId, (int)FrameworkHelper.ActionType.DeleteGraphics, "Graphics deleted at ");
                            GetGraphicsDetails();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                if (ex is IOException)
                {
                    MessageBox.Show("Graphic deleted successfully");
                    GetGraphicsDetails();
                }
                else
                {
                    MessageBox.Show("Can not delete graphic because " + ex.Message);
                    string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                    ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                }
            }
        }

        /// <summary>
        /// Handles the Click event of the btnSaveDiscount control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void btnSaveDiscount_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (CheckValidationDiscount())
                {
                    //_objDataLayer = new DigiPhotoDataServices();
                    //string SyncCode = CommonUtility.GetRandomString(8) + Convert.ToInt32(ApplicationObjectEnum.Discount).ToString().PadLeft(2, '0');
                    string SyncCode = CommonUtility.GetUniqueSynccode(Convert.ToInt32(ApplicationObjectEnum.Discount).ToString().PadLeft(2, '0'), LoginUser.countrycode, LoginUser.Storecode, "00");
                    //_objDataLayer.SetDiscountDetails(txtdiscountname.Text, txtdiscountdescription.Text, (bool)IsDiscountActive.IsChecked, (bool)IsDiscountSecure.IsChecked, (bool)IsDiscountItemLevel.IsChecked, (bool)IsDiscountasPercentage.IsChecked, txtDiscountCode.Text, SyncCode);
                    (new DiscountBusiness()).SetDiscountDetails(txtdiscountname.Text, txtdiscountdescription.Text, (bool)IsDiscountActive.IsChecked, (bool)IsDiscountSecure.IsChecked, (bool)IsDiscountItemLevel.IsChecked, (bool)IsDiscountasPercentage.IsChecked, txtDiscountCode.Text, SyncCode);
                    //DiscountBusiness diBiz = new DiscountBusiness();
                    //diBiz.SetDiscountDetails(txtdiscountname.Text, txtdiscountdescription.Text, (bool)IsDiscountActive.IsChecked, (bool)IsDiscountSecure.IsChecked, (bool)IsDiscountItemLevel.IsChecked, (bool)IsDiscountasPercentage.IsChecked, txtDiscountCode.Text, SyncCode);
                    MessageBox.Show("Discount save Successfully");
                    AuditLog.AddUserLog(Common.LoginUser.UserId, (int)FrameworkHelper.ActionType.AddDiscount, "Changed discount at ");
                    //DGManageDiscount.ItemsSource = _objDataLayer.GetDiscountDetails();
                    DGManageDiscount.ItemsSource = (new DiscountBusiness()).GetDiscountDetails();
                    //DGManageDiscount.ItemsSource = diBiz.GetDiscountDetails();
                    ClearControlDiscount();
                }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }
        /// <summary>
        /// Checks the validation discount.
        /// </summary>
        /// <returns></returns>
        private bool CheckValidationDiscount()
        {
            if (txtdiscountname.Text == "")
            {
                MessageBox.Show("Please enter the discount name");
                return false;
            }
            else if (txtdiscountdescription.Text == "")
            {
                MessageBox.Show("Please enter the discount description");
                return false;
            }
            else if (txtDiscountCode.Text == "")
            {
                MessageBox.Show("Please enter the discount code");
                return false;
            }

            else return true;
        }
        /// <summary>
        /// Handles the Click event of the btnDeleteDiscount control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void btnDeleteDiscount_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (MessageBox.Show("Do you want to delete this discount?", "Confirm delete", MessageBoxButton.YesNo) == MessageBoxResult.Yes)
                {

                    Button btnSender = (Button)sender;
                    //DigiPhotoDataServices _objdbLayer = new DigiPhotoDataServices();
                    //bool isdelete = _objdbLayer.DeleteDiscount((int)btnSender.Tag);

                    DiscountBusiness disBiz = new DiscountBusiness();
                    bool isdelete = disBiz.DeleteDiscount((int)btnSender.Tag);
                    if (isdelete)
                    {
                        MessageBox.Show("Discount deleted successfully");
                        AuditLog.AddUserLog(Common.LoginUser.UserId, (int)FrameworkHelper.ActionType.DeleteDiscount, "Deleted Discount at ");
                        //DGManageDiscount.ItemsSource = _objDataLayer.GetDiscountDetails();
                        //DiscountBusiness diBiz = new DiscountBusiness();
                        //DGManageDiscount.ItemsSource = diBiz.GetDiscountDetails();
                        DGManageDiscount.ItemsSource = (new DiscountBusiness()).GetDiscountDetails();
                    }

                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }

        /// <summary>
        /// Handles the Click event of the btnEditDiscount control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void btnEditDiscount_Click(object sender, RoutedEventArgs e)
        {
            IsDiscountActive.IsChecked = false;
            IsDiscountSecure.IsChecked = false;
            IsDiscountItemLevel.IsChecked = false;
            IsDiscountasPercentage.IsChecked = false;
            Button btnSender = (Button)sender;
            //DigiPhotoDataServices _objdbLayer = new DigiPhotoDataServices();
            //string discount = _objdbLayer.GetDiscountDetailsFromID((int)btnSender.Tag);

            //DiscountBusiness disBiz = new DiscountBusiness();
            string discount = (new DiscountBusiness()).GetDiscountDetailsFromID((int)btnSender.Tag);
            string[] discountdetails = discount.Split('#');
            txtdiscountname.Text = discountdetails[0].ToString(); ;
            txtdiscountdescription.Text = discountdetails[1].ToString();
            string discountactive = discountdetails[2].ToString();
            if (discountactive == "True")
            {
                IsDiscountActive.IsChecked = true;
            }
            string discountsecure = discountdetails[3].ToString();
            if (discountsecure == "True")
            {
                IsDiscountSecure.IsChecked = true;
            }
            string discountitemlevel = discountdetails[4].ToString();
            if (discountitemlevel == "True")
            {
                IsDiscountItemLevel.IsChecked = true;
            }
            string discountaspercentage = discountdetails[5].ToString();
            if (discountaspercentage == "True")
            {
                IsDiscountasPercentage.IsChecked = true;
            }
            txtDiscountCode.Text = discountdetails[6].ToString();
        }

        /// <summary>
        /// Handles the Click event of the btnCanceldiscount control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void btnCanceldiscount_Click(object sender, RoutedEventArgs e)
        {
            ClearControlDiscount();
        }
        /// <summary>
        /// Clears the control discount.
        /// </summary>
        private void ClearControlDiscount()
        {
            txtdiscountdescription.Text = "";
            txtdiscountname.Text = "";
            txtDiscountCode.Text = "";
            IsDiscountActive.IsChecked = true;
            IsDiscountSecure.IsChecked = false;
            IsDiscountItemLevel.IsChecked = false;
            IsDiscountasPercentage.IsChecked = false;
        }

        /// <summary>
        /// Handles the Click event of the btnCancelSelectgraphics control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void btnCancelSelectgraphics_Click(object sender, RoutedEventArgs e)
        {
            txtSelectedgraphics.Text = "";
            IsGraphicsActive.IsChecked = true;
            EditableGraphics = null;
        }

        /// <summary>
        /// Loads the type of the product.
        /// </summary>
        /// <param name="selectedPType">Type of the selected application.</param>
        private void loadProductType(int selectedPType)
        {

            lstBGList = new Dictionary<string, string>();
            try
            {
                lstBGList.Add("--Select--", "0");
                BackgroundBusiness bacBiz = new BackgroundBusiness();
                foreach (var item in bacBiz.GetBackgoundDetailsALL())
                {
                    lstBGList.Add(item.DG_BackGround_Image_Name.ToString(), item.DG_Background_pkey.ToString());
                }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }
        private void BindBorders()
        {
            //lstBorderList = (new DigiPhotoDataServices()).GetBorderDetails();
            lstBorderList = (new BorderBusiness()).GetBorderDetails();
            lstBorderList = lstBorderList.Where(o => o.DG_IsActive == true).ToList();
        }

        /// <summary>
        /// Load the Theme Added By Ajinkya Y.
        /// </summary>
        private void BindTheme()
        {
            try
            {
                var themeTypes = (new SceneBusiness()).GetAllTheme();
                var primaryProduct = themeTypes.Where(t => t.DG_IsActive == true).ToList();
                CommonUtility.BindComboWithSelect<ThemeInfo>(CmbThemeType, themeTypes, "DG_ThemeName", "DG_ThemeId", 0, ClientConstant.SelectString);
                CmbThemeType.SelectedValue = "0";
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }

        }


        /// <summary>
        /// Loads the graphics.
        /// </summary>

        private UIElement elementForContextMenu;

        /// <summary>
        /// Handles the Click event of the btn control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void btn_Click(object sender, RoutedEventArgs e)
        {
            Button _objbtn = new Button();
            _objbtn = (Button)sender;
            switch (_objbtn.Content.ToString())
            {
                case "ENTER":
                    {
                        KeyBorder.Visibility = Visibility.Collapsed;
                        break;
                    }
                case "SPACE":
                    {
                        controlon.Text = controlon.Text + " ";
                        break;
                    }
                case "CLOSE":
                    {
                        KeyBorder.Visibility = Visibility.Collapsed;
                        break;
                    }
                case "Back":
                    {
                        TextBox objtxt = (TextBox)(controlon);
                        if (controlon.Text.Length > 0)
                        {
                            controlon.Text = controlon.Text.Remove(controlon.Text.Length - 1, 1);
                        }
                        break;
                    }
                default:
                    {
                        controlon.Text = controlon.Text + _objbtn.Content;
                    }
                    break;
            }
        }
        /// <summary>
        /// Handles the Click event of the btnSaveChanges control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void btnSaveChanges_Click(object sender, RoutedEventArgs e)
        {

            try
            {
                int Groupid = 0;
                string printerName = string.Empty;
                string printerGroupConcat = string.Empty;
                if (cmbReceiptPrinter.SelectedIndex > 0)
                {
                    printerName = lstPrinterList.Where(t => t.Value == cmbReceiptPrinter.SelectedValue.ToString()).FirstOrDefault().Key;
                }
                //if (cmbGroupSelection.SelectedIndex > 0)
                //{
                //    Groupid = Convert.ToInt16(cmbGroupSelection.SelectedValue);
                //}
                printerGroupConcat = printerName;// +"," + Groupid;
                const string message = "Do you want to save changes.?";
                var result = MessageBox.Show(message, "Confirmation", MessageBoxButton.YesNo);
                if (result == MessageBoxResult.Yes)
                {
                    if (CheckValidationsCurrency())
                    {
                        if (CheckValidation())
                        {
                            SaveConfigData();
                            string pathtosave = Environment.CurrentDirectory;
                            if (File.Exists(pathtosave + "\\slipPrinter.dat"))
                            {
                                File.Delete(pathtosave + "\\slipPrinter.dat");
                            }

                            using (StreamWriter writer = new StreamWriter(File.Open(pathtosave + "\\slipPrinter.dat", FileMode.Create)))
                            {
                                writer.Write(printerGroupConcat, true);
                                writer.Close();
                                Common.LoginUser.ReceiptPrinterPath = lstPrinterList.Where(t => t.Value == cmbReceiptPrinter.SelectedValue.ToString()).FirstOrDefault().Key;
                                // Common.LoginUser.GroupValue = cmbGroupSelection.SelectedValue.ToString();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }
        /// <summary>
        /// Handles the Click event of the btnhotfolder control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void btnhotfolder_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                System.Windows.Forms.FolderBrowserDialog fbDialog = new System.Windows.Forms.FolderBrowserDialog();
                Button _objbtn = (Button)sender;
                switch (_objbtn.Name)
                {
                    case "btnhotfolder":
                        {

                            var result = fbDialog.ShowDialog();
                            if (result == System.Windows.Forms.DialogResult.OK)
                            {
                                txtHotFolder.Text = fbDialog.SelectedPath.Replace(@"\", @"\\") + "\\\\";
                            }
                            break;
                        }
                    case "btnFrmaes":
                        {
                            var result = fbDialog.ShowDialog();
                            if (result == System.Windows.Forms.DialogResult.OK)
                            {
                                txtBorderFolder.Text = fbDialog.SelectedPath + "\\";
                            }
                            break;
                        }
                    case "btnBg":
                        {
                            var result = fbDialog.ShowDialog();
                            if (result == System.Windows.Forms.DialogResult.OK)
                            {
                                txtbgPath.Text = fbDialog.SelectedPath + "\\";
                            }
                            break;
                        }
                    case "btnBrowseDBbackupPath":
                        {
                            var result = fbDialog.ShowDialog();
                            if (result == System.Windows.Forms.DialogResult.OK)
                            {
                                txtDbBackupPath.Text = fbDialog.SelectedPath + "\\";
                            }
                            break;
                        }
                    case "btnBrowseDfbackupPath":
                        {
                            var result = fbDialog.ShowDialog();
                            if (result == System.Windows.Forms.DialogResult.OK)
                            {
                                txtHfBackupPath.Text = fbDialog.SelectedPath + "\\";
                            }
                            break;
                        }
                    case "btnBrowseMktImgPath":
                        {
                            /*
                            var result = fbDialog.ShowDialog();
                            if (result == System.Windows.Forms.DialogResult.OK)
                            {
                                txtMktImgPath.Text = fbDialog.SelectedPath + "\\";
                            }
                            */
                            break;
                        }
                    case "btnBrowseEKSamplePath":
                        {
                            /*
                            var result = fbDialog.ShowDialog();
                            if (result == System.Windows.Forms.DialogResult.OK)
                            {
                                txtEKSamplePath.Text = fbDialog.SelectedPath + "\\";
                            }
                            */
                            break;
                        }
                    case "btnServerhotfolder":
                        {

                            var result = fbDialog.ShowDialog();
                            if (result == System.Windows.Forms.DialogResult.OK)
                            {
                                txtServerHotFolder.Text = fbDialog.SelectedPath.Replace(@"\", @"\\") + "\\\\";
                            }
                            break;
                        }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }
        /// <summary>
        /// Handles the Loaded event of the Window control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            GetPermissions();
            GetAllSubstoreConfigdata();
        }

        private void LoadTabData()
        {
#if DEBUG
            var watch = FrameworkHelper.CommonUtility.Watch();
            watch.Start();
#endif
            //bs.Show();
            try
            {
                FillSubstore();
                GetGraphicsDetails();
                FillAllCombo();
                FillProductCombo();
                FillOnlineTabInfo();
                FillPrinters();
                var borderDetails = new BorderBusiness().GetBorderDetails();
                if (borderDetails != null)
                {
                    foreach (var border in borderDetails)
                    {
                        if (!string.IsNullOrEmpty(LoginUser.DigiFolderFramePath) && !string.IsNullOrEmpty(border.DG_Border))
                            border.FilePath = System.IO.Path.Combine(LoginUser.DigiFolderFramePath, border.DG_Border);
                    }
                    DGManageBorder.ItemsSource = borderDetails;
                }
                CurrencyBusiness curBiz = new CurrencyBusiness();
                DGManageCurrency.ItemsSource = curBiz.GetCurrencyDetails();
                DGManageDiscount.ItemsSource = (new DiscountBusiness()).GetDiscountDetails();
                txtDefaultCurrency.Text = (new CurrencyBusiness()).GetDefaultCurrencyName();

                GetBackgrounds();
                // FillProductList();

                lstCurrencyList = new Dictionary<string, string>();
                lstBackgroundList = new Dictionary<string, string>();
                //lstBorderList = new Dictionary<string, string>();
                lstlogoList = new Dictionary<string, string>();
                lstBGList = new Dictionary<string, string>();
                RobotImageLoader.GetConfigData();
                GetNewConfigData();
                fillRecursiveNewOptions();
                getTaxConfigData();
                GetAudioDetails();

                GetVideoTemplateDetails();

                GetVideoBGDetails();
                GetVideoOverlayDetails();
                #region Scene
                // GetGraphicsDetailsScene();
                GetBorderDetailsScene();
                GetBackGroundDetailsScene();
                FillSceneProductCombo();
                DGManageScene.ItemsSource = (new SceneBusiness()).GetSceneDetails();
                BindCharacterGrid();
                // BindGroupGrid();
                FillLocationCombo();
                #endregion

                try
                {
                    List<CurrencyInfo> lstCurrency = curBiz.GetCurrencyListforconfig();
                    CommonUtility.BindComboWithSelect<CurrencyInfo>(cmbCurrency, lstCurrency, "DG_Currency_Name", "DG_Currency_pkey", 0, ClientConstant.SelectString);
                    cmbCurrency.SelectedValue = "0";

                    int i = 0;
                    GetConfigData();
                    if (LoginUser.DigiFolderBackGroundPath != null)
                    {
                        foreach (var item in new DirectoryInfo(LoginUser.DigiFolderBackGroundPath).GetFiles())
                        {
                            if (!item.Name.Contains("Thumb"))
                            {
                                lstBackgroundList.Add(item.Name, i.ToString());
                                i++;
                            }
                        }
                        i = 0;

                        foreach (var item in new DirectoryInfo(LoginUser.DigiFolderGraphicsPath).GetFiles())
                        {
                            if (!item.Name.Contains("Thumb"))
                            {
                                lstlogoList.Add(item.Name, i.ToString());
                            }
                            i++;
                        }
                    }
                    i = 0;
                }
                catch (Exception ex)
                {
                    string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                    ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                }

                lstBGList.Add("--Select--", "0");
                //foreach (var item in _objdbLayer.GetBackgoundDetailsALL())
                BackgroundBusiness bacBiz = new BackgroundBusiness();
                foreach (var item in bacBiz.GetBackgoundDetailsALL())
                {
                    lstBGList.Add(item.DG_BackGround_Image_Name.ToString(), item.DG_Background_pkey.ToString());
                }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
            finally
            {
                //bs.Hide();
            }
#if DEBUG
            if (watch != null)
                FrameworkHelper.CommonUtility.WatchStop("Login", watch);
#endif
        }
        /// <summary>
        /// Handles the Click event of the btnLogout control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void btnLogout_Click(object sender, RoutedEventArgs e)
        {
            //_objDataLayer = new DigiPhotoDataServices();
            AuditLog.AddUserLog(Common.LoginUser.UserId, (int)FrameworkHelper.ActionType.Logout, "Logged out at ");
            Login _objLogin = new Login();
            _objLogin.Show();
            this.Close();
        }

        /// <summary>
        /// Handles the KeyDown event of the txtNumber control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="KeyEventArgs"/> instance containing the event data.</param>
        private void txtNumber_KeyDown(object sender, KeyEventArgs e)
        {
        }

        string filename = string.Empty;
        /// <summary>
        /// Handles the Click event of the btnSelectBorder control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void btnSelectBorder_Click(object sender, RoutedEventArgs e)
        {
            if (iseditborder)
                return;
            Stream checkstream = null;
            Microsoft.Win32.OpenFileDialog ofd = new Microsoft.Win32.OpenFileDialog();
            ofd.Multiselect = false;
            ofd.Filter = "All Image Files | *.*";

            if ((bool)ofd.ShowDialog())
            {
                try
                {
                    if ((checkstream = ofd.OpenFile()) != null)
                    {
                        txtSelectedborder.Text = System.IO.Path.GetFileName(ofd.FileName);
                        txtSelectedborder.Tag = ofd.FileName;
                        //txtSelectedborder.Text = ofd.FileName.ToString();
                        filename = System.IO.Path.GetFileName(ofd.FileName);
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Error: Could not read file from disk. Original error: " + ex.Message);
                }
            }
            // checkstream.Close();
            //checkstream.Dispose();
            //GC.Collect();
        }
        /// <summary>
        /// Handles the Click event of the btnCancelSelectBorder control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void btnCancelSelectBorder_Click(object sender, RoutedEventArgs e)
        {
            txtSelectedborder.Text = "";
            IsBorderActive.IsChecked = true;
            DGManageBorder.SelectedItem = null;
            iseditborder = false;
            borderId = 0;
            filename = string.Empty;
        }

        /// <summary>
        /// Handles the Click event of the btnSaveSelectBorder control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void btnSaveSelectBorder_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (CheckBorderValidations())
                {

                    if (!iseditborder)
                    {
                        SaveBorderFile();
                    }
                    else
                    {
                        var selectedBorder = (BorderInfo)DGManageBorder.SelectedItem;
                        if (txtSelectedborder.Text != selectedBorder.DG_Border)
                            SaveBorderFile();
                    }

                    //DigiPhotoDataServices _objdbLayer = new DigiPhotoDataServices();
                    string producttypevalue = ((ProductTypeInfo)(CmbProductType.SelectedItem)).DG_Orders_ProductType_Name;
                    //int ProductTypeID = _objdbLayer.GetProductID(producttypevalue);
                    int ProductTypeID = (new ProductBusiness()).GetProductsynccodeName(producttypevalue);
                    if (ProductTypeID != 0)
                    {
                        string SyncCode = CommonUtility.GetUniqueSynccode(Convert.ToInt32(ApplicationObjectEnum.Border).ToString().PadLeft(2, '0'), LoginUser.countrycode, LoginUser.Storecode, "00");
                        //string SyncCode = CommonUtility.GetRandomString(8) + Convert.ToInt32(ApplicationObjectEnum.Border).ToString().PadLeft(2, '0');
                        //_objdbLayer.SetBorderDetails(filename, ProductTypeID, (bool)IsBorderActive.IsChecked, borderId, SyncCode);
                        (new BorderBusiness()).SetBorderDetails(filename, ProductTypeID, (bool)IsBorderActive.IsChecked, borderId, SyncCode, LoginUser.UserId);
                        MessageBox.Show("Record saved successfully");
                        AuditLog.AddUserLog(Common.LoginUser.UserId, (int)FrameworkHelper.ActionType.AddBorder, "Add/Edit Border at ");
                    }
                    DGManageBorder.ItemsSource = null;
                    //DGManageBorder.ItemsSource = _objdbLayer.GetBorderDetails();
                    BorderBusiness borBiz = new BorderBusiness();
                    var borderDetails = borBiz.GetBorderDetails();
                    borderDetails.ForEach(x => x.FilePath = System.IO.Path.Combine(LoginUser.DigiFolderFramePath, x.DG_Border));
                    DGManageBorder.ItemsSource = borderDetails;
                    borderId = 0;
                    txtBorderFolder.Text = "";
                    iseditborder = false;
                    btnCancelSelectBorder_Click(null, null);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }

        }
        void SaveBorderFile()
        {
            try
            {
                string destFile = LoginUser.DigiFolderFramePath + txtSelectedborder.Text;
                System.IO.File.Copy(txtSelectedborder.Tag.ToString(), destFile, true);
                string thumbnails = string.Empty;
                using (
                                FileStream fileStream =
                                    File.OpenRead(LoginUser.DigiFolderFramePath + txtSelectedborder.Text))
                {
                    MemoryStream ms = new MemoryStream();
                    fileStream.CopyTo(ms);
                    ms.Seek(0, SeekOrigin.Begin);
                    thumbnails = LoginUser.DigiFolderFramePath + "\\Thumbnails\\" + txtSelectedborder.Text;
                    ResizeAndSaveHighQualityImage(System.Drawing.Image.FromStream(ms), thumbnails, 100, 150);
                    fileStream.Close();
                    ms.Dispose();

                }
                //Copy selected item to all substore
                CopyToAllSubstore(lstConfigurationInfo, txtSelectedborder.Tag.ToString(), thumbnails, "Frames");
            }
            catch (Exception ex)
            {

            }
        }

        /// <summary>
        /// Resizes the and save high quality image.
        /// </summary>
        /// <param name="image">The image.</param>
        /// <param name="pathToSave">The path automatic save.</param>
        /// <param name="quality">The quality.</param>
        /// <param name="height">The height.</param>
        /// <exception cref="System.ArgumentOutOfRangeException"></exception>
        //private void ResizeAndSaveHighQualityImage(System.Drawing.Image image, string pathToSave, int quality, int height)
        //{
        //    try
        //    {
        //        //pathToSave += "\\Images";
        //        // the resized result bitmap
        //        decimal ratio = image.Width.ToDecimal() / image.Height.ToDecimal();

        //        int width = Convert.ToInt32(height * ratio);

        //        using (System.Drawing.Bitmap result = new System.Drawing.Bitmap(width, height))
        //        {
        //            // get the graphics and draw the passed image to the result bitmap
        //            using (System.Drawing.Graphics grphs = System.Drawing.Graphics.FromImage(result))
        //            {
        //                grphs.CompositingQuality = System.Drawing.Drawing2D.CompositingQuality.HighQuality;
        //                grphs.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
        //                grphs.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
        //                grphs.DrawImage(image, 0, 0, result.Width, result.Height);
        //            }

        //            // check the quality passed in
        //            if ((quality < 0) || (quality > 100))
        //            {
        //                string error = string.Format("quality must be 0, 100", quality);
        //                throw new ArgumentOutOfRangeException(error);
        //            }

        //            System.Drawing.Imaging.EncoderParameter qualityParam = new System.Drawing.Imaging.EncoderParameter(System.Drawing.Imaging.Encoder.Quality, quality);
        //            string lookupKey = "image/jpeg";
        //            var jpegCodec =
        //                System.Drawing.Imaging.ImageCodecInfo.GetImageEncoders().Where(i => i.MimeType.Equals(lookupKey)).FirstOrDefault();

        //            //create a collection of EncoderParameters and set the quality parameter
        //            var encoderParams = new System.Drawing.Imaging.EncoderParameters(1);
        //            encoderParams.Param[0] = qualityParam;
        //            //save the image using the codec and the encoder parameter
        //            result.Save(pathToSave, jpegCodec, encoderParams);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        ErrorHandler.ErrorHandler.LogError(ex);

        //    }
        //}
        private void ResizeAndSaveHighQualityImage(System.Drawing.Image image, string pathToSave, int quality, int height)
        {
            System.IO.MemoryStream memorySteamForfile = null;
            System.IO.FileStream fileStream = null;

            try
            {

                // the resized result bitmap
                decimal ratio = image.Width.ToDecimal() / image.Height.ToDecimal();

                int width = Convert.ToInt32(height * ratio);

                using (System.Drawing.Bitmap result = new System.Drawing.Bitmap(width, height))
                {
                    // get the graphics and draw the passed image to the result bitmap
                    using (System.Drawing.Graphics grphs = System.Drawing.Graphics.FromImage(result))
                    {
                        grphs.CompositingQuality = System.Drawing.Drawing2D.CompositingQuality.HighQuality;
                        grphs.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
                        grphs.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
                        grphs.DrawImage(image, 0, 0, result.Width, result.Height);
                    }

                    // check the quality passed in
                    if ((quality < 0) || (quality > 100))
                    {
                        string error = string.Format("quality must be 0, 100", quality);
                        throw new ArgumentOutOfRangeException(error);
                    }

                    System.Drawing.Imaging.EncoderParameter qualityParam = new System.Drawing.Imaging.EncoderParameter(System.Drawing.Imaging.Encoder.Quality, quality);
                    string lookupKey = "image/jpeg";
                    var jpegCodec =
                        System.Drawing.Imaging.ImageCodecInfo.GetImageEncoders().Where(i => i.MimeType.Equals(lookupKey)).FirstOrDefault();

                    //create a collection of EncoderParameters and set the quality parameter
                    var encoderParams = new System.Drawing.Imaging.EncoderParameters(1);
                    encoderParams.Param[0] = qualityParam;

                    memorySteamForfile = new System.IO.MemoryStream();
                    fileStream = new System.IO.FileStream(pathToSave, System.IO.FileMode.Create, System.IO.FileAccess.ReadWrite);


                    //save the image using the codec and the encoder parameter
                    //result.Save(pathToSave, jpegCodec, encoderParams);
                    result.Save(memorySteamForfile, jpegCodec, encoderParams);
                    byte[] fileBytes = memorySteamForfile.ToArray();
                    fileStream.Write(fileBytes, 0, fileBytes.Length);

                }
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);

            }
            finally
            {
                if (memorySteamForfile != null)
                {
                    memorySteamForfile.Close();
                    memorySteamForfile.Dispose();
                }
                if (fileStream != null)
                {
                    fileStream.Close();
                    fileStream.Dispose();
                }
            }
        }
        bool iseditborder = false;
        /// <summary>
        /// Handles the Click event of the btnEdit control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void btnEdit_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (sender != null)
                {
                    bool allowEdit = true;
                    if (iseditborder)
                    {
                        var messageResult = MessageBox.Show("It looks like you have been editing a border.If you leave before saving, your changes will be lost.Would you like to save your unsaved changes ?", "Alert", MessageBoxButton.YesNo);
                        if (messageResult == MessageBoxResult.Yes)
                            allowEdit = false;
                    }
                    if (allowEdit)
                    {


                        Button _objbtn = new Button();
                        _objbtn = (Button)(sender);
                        if (_objbtn != null)
                        {
                            BorderInfo borderInfo = (BorderInfo)_objbtn.DataContext;
                            if (borderInfo != null)
                            {
                                #region Check for dependency
                                string statusMessage = string.Empty;
                                bool foundDependency = FindDependencyOfBorder(borderInfo, "edit", out statusMessage);
                                if (foundDependency)
                                {
                                    MsgBox.ShowHandlerDialog(statusMessage, DigiMessageBox.DialogType.OK);
                                    return;
                                }
                                #endregion
                                borderId = _objbtn.Tag.ToInt32();
                                iseditborder = true;
                                IsBorderActive.IsChecked = false;
                                Button btnSender = (Button)sender;
                                //DigiPhotoDataServices _objdbLayer = new DigiPhotoDataServices();
                                //string Border = _objdbLayer.GetBorderNameFromID((int)btnSender.Tag);
                                var borderList = (new BorderBusiness()).GetBorderNameFromID((int)btnSender.Tag);
                                string border = borderList.DG_Border + "#" + borderList.DG_IsActive + "#" + borderList.DG_ProductTypeID;
                                string[] Borderdetails = border.Split('#');
                                string bodername = Borderdetails[0];
                                string borderactive = Borderdetails[1];
                                filename = bodername;
                                string product = Borderdetails[2];
                                //string productname = _objdbLayer.GetProductNameFromID(int.Parse(product));
                                txtSelectedborder.Text = bodername;
                                CmbProductType.SelectedValue = product;
                                if (borderactive == "True")
                                {
                                    IsBorderActive.IsChecked = true;
                                }
                            }
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                iseditborder = false;
            }
        }

        /// <summary>
        /// Handles the Click event of the btnSaveCurrency control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void btnSaveCurrency_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (CheckCurrencyValidations())
                {
                    string currencyname = TBCurrencyName.Text;
                    string currencySymbol = TBCurrencySymbol.Text;
                    float currencyrate = float.Parse(TBCurrencyRate.Text);
                    bool update = false;

                    if (currencyid > 0)
                    {
                        update = true;
                    }
                    string SyncCode = CommonUtility.GetUniqueSynccode(Convert.ToInt32(ApplicationObjectEnum.Currency).ToString().PadLeft(2, '0'), LoginUser.countrycode, LoginUser.Storecode, "00");
                    //DigiPhotoDataServices _objdbLayer = new DigiPhotoDataServices();
                    CustomBusineses cusBiz = new CustomBusineses();
                    if (update)
                    {
                        MessageBoxResult response = MessageBox.Show("Alert: Are you sure you want to update the currrency details?", "Temper Alert", MessageBoxButton.YesNo, MessageBoxImage.Warning);
                        if (response == MessageBoxResult.Yes)
                        {

                            string oldprice = string.Empty;
                            bool IsPricingChanged = IsRateUpdated(currencyid, currencyrate.ToDouble(), out oldprice);

                            //_objdbLayer.SetCurrencyDetails(currencyname, currencyrate, currencySymbol, currencyid, 4, false, cusBiz.ServerDateTime(), "Test", TBCurrencyCode.Text, SyncCode);
                            (new CurrencyBusiness()).SetCurrencyDetails(currencyname, currencyrate, currencySymbol, currencyid, 4, false, cusBiz.ServerDateTime(), "Test", TBCurrencyCode.Text, SyncCode);



                            if (IsPricingChanged)
                            {
                                AuditLog.AddUserLog(Common.LoginUser.UserId, (int)FrameworkHelper.ActionType.ChangeCurrencyRate, "Currency (" + currencyname + ") Rate has been changed from " + oldprice.ToDouble().ToString() + " to " + currencyrate.ToDouble().ToString() + " on ");
                            }
                            else
                            {
                                AuditLog.AddUserLog(Common.LoginUser.UserId, (int)FrameworkHelper.ActionType.EditCurrency, "Currency (" + currencyname + ") has been updated on ");
                            }
                            MessageBox.Show("[" + currencyname + "] Currency details has been updated successfully");

                        }
                    }
                    else
                    {
                        //_objdbLayer.SetCurrencyDetails(currencyname, currencyrate, currencySymbol, currencyid, (int)FrameworkHelper.ActionType.EditPhoto, false, cusBiz.ServerDateTime(), "Test", TBCurrencyCode.Text, SyncCode);
                        (new CurrencyBusiness()).SetCurrencyDetails(currencyname, currencyrate, currencySymbol, currencyid, (int)FrameworkHelper.ActionType.EditPhoto, false, cusBiz.ServerDateTime(), "Test", TBCurrencyCode.Text, SyncCode);
                        AuditLog.AddUserLog(Common.LoginUser.UserId, (int)FrameworkHelper.ActionType.AddCurrency, "Currency (" + currencyname + ") has been created on ");
                        MessageBox.Show("[" + currencyname + "] Currency details has been created successfully");
                    }





                    //  MessageBox.Show("Record saved successfully");
                    DGManageCurrency.ItemsSource = null;
                    //DGManageCurrency.ItemsSource = _objdbLayer.GetCurrencyDetails();
                    CurrencyBusiness curBiz = new CurrencyBusiness();
                    DGManageCurrency.ItemsSource = curBiz.GetCurrencyDetails();
                    currencyid = 0;
                    ClearControlCurrency();
                }
                else
                {
                    MessageBox.Show("Please enter all required fields to continue");
                }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }
        /// <summary>
        /// Determines whether [is rate updated] [the specified currency unique identifier].
        /// </summary>
        /// <param name="CurrencyID">The currency unique identifier.</param>
        /// <param name="newprice">The newprice.</param>
        /// <param name="OldPrice">The old price.</param>
        /// <returns></returns>
        private bool IsRateUpdated(int CurrencyID, double newprice, out string OldPrice)
        {
            //DigiPhotoDataServices _objDataService = new DigiPhotoDataServices();
            //var CurrencyDetails = _objDataService.GetCurrencyDetailFromID(CurrencyID);
            var CurrencyDetails = (new CurrencyBusiness()).GetCurrencyDetailFromID(CurrencyID);
            string[] currency = CurrencyDetails.Split('#');
            string currencyrate = currency[1];

            if (currencyrate.ToDouble() == newprice)
            {
                OldPrice = currencyrate.ToString();
                return false;

            }
            else
            {
                OldPrice = currencyrate.ToString();
                return true;
            }
        }
        int currencyid;
        /// <summary>
        /// Handles the Click event of the btnEditCurrency control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void btnEditCurrency_Click(object sender, RoutedEventArgs e)
        {
            try
            {

                Button btnSender = (Button)sender;
                currencyid = (int)btnSender.Tag;
                //DigiPhotoDataServices _objdbLayer = new DigiPhotoDataServices();
                //string CurrencyDetails = _objdbLayer.GetCurrencyDetailFromID((int)btnSender.Tag);
                string CurrencyDetails = (new CurrencyBusiness()).GetCurrencyDetailFromID((int)btnSender.Tag);
                string[] currency = CurrencyDetails.Split('#');
                string currencyname = currency[0];
                string currencyrate = currency[1];
                string currencysymbol = currency[2];
                TBCurrencyName.Text = currencyname;
                TBCurrencyRate.Text = currencyrate;
                TBCurrencySymbol.Text = currencysymbol;
                TBCurrencyCode.Text = currency[3];

            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }

        /// <summary>
        /// Handles the Click event of the btnDeleteCurrency control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void btnDeleteCurrency_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (MessageBox.Show("Do you want to delete this currency?", "Tempar Data", MessageBoxButton.YesNo, MessageBoxImage.Warning) == MessageBoxResult.Yes)
                {

                    //DigiPhotoDataServices _objdbLayer = new DigiPhotoDataServices();

                    Button btnSender = (Button)sender;
                    //string CurrencyDetails = _objdbLayer.GetCurrencyDetailFromID((int)btnSender.Tag);
                    string CurrencyDetails = (new CurrencyBusiness()).GetCurrencyDetailFromID((int)btnSender.Tag);
                    string[] currency = CurrencyDetails.Split('#');
                    string currencyname = currency[0];

                    //bool isdelete = _objdbLayer.DeleteCurrency((int)btnSender.Tag);
                    bool isdelete = (new CurrencyBusiness()).DeleteCurrency((int)btnSender.Tag);
                    if (isdelete)
                    {
                        AuditLog.AddUserLog(Common.LoginUser.UserId, (int)FrameworkHelper.ActionType.DeleteCurrency, "Currency (" + currencyname + ") has been deleted on ");
                        MessageBox.Show("Currency deleted successfully");
                        DGManageCurrency.ItemsSource = (new CurrencyBusiness()).GetCurrencyDetails();
                    }
                }

            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }

        /// <summary>
        /// Handles the Click event of the btnDelete control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void btnDelete_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (sender != null)
                {
                    Button btnSender = null;

                    btnSender = (Button)sender;
                    if (btnSender != null)
                    {


                        BorderInfo borderInfo = null;
                        borderInfo = (BorderInfo)btnSender.DataContext;
                        if (borderInfo != null)
                        {
                            #region Check for dependency
                            string statusMessage = string.Empty;
                            bool foundDependency = FindDependencyOfBorder(borderInfo, "edit", out statusMessage);
                            if (foundDependency)
                            {
                                MsgBox.ShowHandlerDialog(statusMessage, DigiMessageBox.DialogType.OK);
                                return;
                            }
                            #endregion
                            int boderId = (int)btnSender.Tag;
                            if (MessageBox.Show("Do you want to delete this border?", "Confirm delete", MessageBoxButton.YesNo) == MessageBoxResult.Yes)
                            {
                                //DigiPhotoDataServices _objdbLayer = new DigiPhotoDataServices();
                                //string filename = _objdbLayer.GetBorderFileName(boderId);
                                string filename = (new BorderBusiness()).GetBorderNameFromID(boderId).DG_Border;
                                //bool isdelete = _objdbLayer.DeleteBorder(boderId);
                                bool isdelete = (new BorderBusiness()).DeleteBorder(boderId);
                                if (System.IO.File.Exists(LoginUser.DigiFolderFramePath + "\\Thumbnails\\" + filename) && System.IO.File.Exists(LoginUser.DigiFolderFramePath + filename))
                                {
                                    System.IO.File.Delete(LoginUser.DigiFolderFramePath + "\\Thumbnails\\" + filename);
                                    System.IO.File.Delete(LoginUser.DigiFolderFramePath + filename);
                                    DeleteFromAllSubstore(lstConfigurationInfo, filename, "Frames");
                                    if (isdelete)
                                    {
                                        MessageBox.Show("Border deleted successfully");
                                        AuditLog.AddUserLog(Common.LoginUser.UserId, (int)FrameworkHelper.ActionType.DeleteBorder, "Border deleted at ");
                                        //DGManageBorder.ItemsSource = _objdbLayer.GetBorderDetails();
                                        //DGManageBorder.ItemsSource = (new BorderBusiness()).GetBorderDetails();
                                    }
                                }
                                var borderDetails = new BorderBusiness().GetBorderDetails();
                                borderDetails.ForEach(x => x.FilePath = System.IO.Path.Combine(LoginUser.DigiFolderFramePath, x.DG_Border));
                                DGManageBorder.ItemsSource = borderDetails;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                if (ex is IOException)
                {
                    MessageBox.Show("Border deleted successfully");
                    var borderDetails = new BorderBusiness().GetBorderDetails();
                    borderDetails.ForEach(x => x.FilePath = System.IO.Path.Combine(LoginUser.DigiFolderFramePath, x.DG_Border));
                    DGManageBorder.ItemsSource = borderDetails;
                }
                else
                {
                    MessageBox.Show("Can not delete file because " + ex.Message);
                    string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                    ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                }
            }
        }

        /// <summary>
        /// Handles the Click event of the btnclearCurrency control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void btnclearCurrency_Click(object sender, RoutedEventArgs e)
        {
            ClearControlCurrency();
        }
        /// <summary>
        /// Clears the control currency.
        /// </summary>
        private void ClearControlCurrency()
        {
            TBCurrencyName.Text = "";
            TBCurrencySymbol.Text = "";
            TBCurrencyRate.Text = "";
            TBCurrencyCode.Text = "";
        }

        /// <summary>
        /// Checks the border validations.
        /// </summary>
        /// <returns></returns>
        private bool CheckBorderValidations()
        {
            if (txtSelectedborder.Text == "")
            {
                return false;
            }
            else if (CmbProductType.SelectedValue.ToString() == "0")
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        /// <summary>
        /// Checks the currency validations.
        /// </summary>
        /// <returns></returns>
        private bool CheckCurrencyValidations()
        {
            if (TBCurrencyName.Text == "")
            {
                return false;
            }
            else if (TBCurrencyRate.Text == "")
            {
                return false;
            }
            else if (TBCurrencyCode.Text == "")
            {
                return false;
            }
            else return true;
        }

        string backgoundname = string.Empty;
        string backgounddisplayname = string.Empty;
        bool isPanorma = false;

        List<BackGroundInfo> lstBackgroundInfo = new List<BackGroundInfo>();
        /// <summary>
        /// Handles the Click event of the btnSelectBackground control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void btnSelectBackground_Click(object sender, RoutedEventArgs e)
        {
            if (EditableBackground != null)
                return;
            Stream checkstream = null;
            Microsoft.Win32.OpenFileDialog ofd = new Microsoft.Win32.OpenFileDialog();
            ofd.Filter = "Image files (*.jpg, *.jpeg, *.jpe, *.jfif, *.png) | *.jpg; *.jpeg; *.jpe; *.jfif; *.png";
            lstBackgroundInfo.Clear();
            ofd.Multiselect = (EditableBackground == null);
            if ((bool)ofd.ShowDialog())
            {
                try
                {
                    if ((checkstream = ofd.OpenFile()) != null)
                    {
                        BackGroundInfo bgInfo;
                        string bgText = string.Empty;
                        if (ofd.Multiselect)
                        {
                            foreach (String bgFiles in ofd.FileNames)
                            {
                                bgInfo = new BackGroundInfo();
                                bgText = System.IO.Path.GetFileName(bgFiles) + "," + bgText;
                                bgInfo.DG_BackGround_Image_Name = System.IO.Path.GetFileNameWithoutExtension(bgFiles);
                                bgInfo.DG_BackGround_Image_Display_Name = System.IO.Path.GetFileName(bgFiles);
                                bgInfo.DG_BackgroundPath = bgFiles;
                                bgInfo.DG_Background_IsActive = (IsBackgroundActive.IsChecked.HasValue) ? IsBackgroundActive.IsChecked.Value : false;
                                lstBackgroundInfo.Add(bgInfo);
                            }
                            txtSelectedBackground.Text = bgText.Remove(bgText.Length - 1);
                        }
                        else
                        {
                            txtSelectedBackground.Text = System.IO.Path.GetFileName(ofd.FileName);
                            txtSelectedBackground.Tag = ofd.FileName;
                        }

                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Error: Could not read file from disk. Original error: " + ex.Message);
                }
            }
        }

        /// <summary>
        /// Handles the Click event of the btnSaveSelectBackground control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void btnSaveSelectBackground_Click(object sender, RoutedEventArgs e)
        {
            if (lstBackgroundInfo != null && lstBackgroundInfo.Count > 0)
            {
                bs.Show();
                bw_CopySettings.RunWorkerAsync();
            }
            else if (EditableBackground != null)
            {

                bs.Show();
                bw_CopySettings.RunWorkerAsync();
            }
            else
            {
                MessageBox.Show("Please browse Background");
            }

        }
        //BusyWindow bs = new BusyWindow();

        private void SaveBackground()
        {
            try
            {
                int productId = 2;
                BackgroundBusiness bacBiz = new BackgroundBusiness();
                string _backgroundname = string.Empty;
                string SyncCode = CommonUtility.GetUniqueSynccode(Convert.ToInt32(ApplicationObjectEnum.Background).ToString().PadLeft(2, '0'), LoginUser.countrycode, LoginUser.Storecode, "00");
                string strThumbnails = string.Empty;

                if (lstBackgroundInfo != null && lstBackgroundInfo.Count > 0)
                {
                    DelegateWork bgActiveWork = delegate
                 {
                     //backgoundname = backgoundname + ".png";
                     foreach (BackGroundInfo bgInfo in lstBackgroundInfo)
                     {
                         _backgroundname = bgInfo.DG_BackGround_Image_Name + ".png";
                         backgounddisplayname = bgInfo.DG_BackGround_Image_Display_Name;
                         bool isActive = IsBackgroundActive.IsChecked.HasValue ? IsBackgroundActive.IsChecked.Value : false;
                         bool isBackgroundPanorama = ChkIsBackgroundPanoramic.IsChecked.HasValue ? ChkIsBackgroundPanoramic.IsChecked.Value : false;
                         BackGroundID = bacBiz.SetBackGroundDetails(productId, _backgroundname, backgounddisplayname, SyncCode, IsBackgroundActive.IsChecked.HasValue ? IsBackgroundActive.IsChecked.Value : false, LoginUser.UserId, isBackgroundPanorama);

                         backgoundname = System.IO.Path.GetFileNameWithoutExtension(_backgroundname) + "_" + BackGroundID.ToString() + ".png";

                         if (EditableBackground != null)
                         {
                             EditableBackground.DG_Background_pkey = BackGroundID;

                         }
                         else
                         {
                             EditableBackground = new BackGroundInfo();
                             EditableBackground.DG_Background_pkey = BackGroundID;

                         }
                         EditableBackground.DG_Background_IsPanorama = isBackgroundPanorama;
                         bacBiz.SetBackGroundDetails(productId, backgoundname, _backgroundname, EditableBackground, SyncCode, isActive, LoginUser.UserId);
                         string destFile = LoginUser.DigiFolderBackGroundPath + "\\8x10\\" + backgoundname;
                         // System.IO.File.Copy(bgInfo.DG_BackgroundPath, destFile, true);                          
                         BackgroundImageOtherFormats(bgInfo.DG_BackgroundPath);
                         using (FileStream fileStream =
                                             File.OpenRead(LoginUser.DigiFolderBackGroundPath + "\\8x10\\" + backgoundname))
                         {
                             MemoryStream ms = new MemoryStream();
                             fileStream.CopyTo(ms);
                             ms.Seek(0, SeekOrigin.Begin);
                             strThumbnails = LoginUser.DigiFolderBackGroundPath + "\\Thumbnails\\";
                             //check if the checkbox panorama is checked
                             if (ChkIsBackgroundPanoramic.IsChecked == true)
                             {
                                 if (!Directory.Exists(strThumbnails + "\\Panorama"))
                                     Directory.CreateDirectory(strThumbnails + "\\Panorama");
                                 ResizeAndSaveHighQualityImage(System.Drawing.Image.FromStream(ms), strThumbnails + "\\Panorama\\" + backgoundname, 100, 150);
                             }
                             else
                             {
                                 ResizeAndSaveHighQualityImage(System.Drawing.Image.FromStream(ms), strThumbnails + backgoundname, 100, 150);
                             }
                             fileStream.Close();
                             ms.Dispose();
                             EditableBackground = null;
                         }

                         //Copy selected item to all substore
                         CopyToAllSubstore(lstConfigurationInfo, destFile, strThumbnails, "BG");
                     }
                     //GetBackgrounds();
                     AuditLog.AddUserLog(Common.LoginUser.UserId, 49, "Add/Edit Background at ");
                 };
                    this.Dispatcher.BeginInvoke(DispatcherPriority.Normal, bgActiveWork);
                }
                else if (EditableBackground != null)
                {
                    DelegateWork work = delegate
                   {
                       if (txtSelectedBackground.Text.Equals(EditableBackground.DG_BackGround_Image_Display_Name))
                       {

                           backgounddisplayname = EditableBackground.DG_BackGround_Image_Display_Name;
                           backgoundname = EditableBackground.DG_BackGround_Image_Name;
                       }
                       else
                       {
                           _backgroundname = System.IO.Path.GetFileNameWithoutExtension(txtSelectedBackground.Tag.ToString());
                           backgounddisplayname = _backgroundname + ".png";
                           backgoundname = _backgroundname + "_" + BackGroundID.ToString() + ".png";
                       }
                       bool isBackgroundPanorama = ChkIsBackgroundPanoramic.IsChecked.HasValue ? ChkIsBackgroundPanoramic.IsChecked.Value : false;
                       EditableBackground.DG_Background_IsPanorama = isBackgroundPanorama;
                       bacBiz.SetBackGroundDetails(productId, backgoundname, backgounddisplayname, EditableBackground, SyncCode, IsBackgroundActive.IsChecked, LoginUser.UserId);
                       if (!txtSelectedBackground.Text.Equals(EditableBackground.DG_BackGround_Image_Display_Name))
                       {
                           string destFile = LoginUser.DigiFolderBackGroundPath + backgoundname;
                           // System.IO.File.Copy(txtSelectedBackground.Tag.ToString(), destFile, true);

                           BackgroundImageOtherFormats(txtSelectedBackground.Tag.ToString());
                           using (FileStream fileStream = File.OpenRead(LoginUser.DigiFolderBackGroundPath + "\\8x10\\" + backgoundname))
                           {
                               MemoryStream ms = new MemoryStream();
                               fileStream.CopyTo(ms);
                               ms.Seek(0, SeekOrigin.Begin);
                               strThumbnails = LoginUser.DigiFolderBackGroundPath + "\\Thumbnails\\" + backgoundname;
                               ResizeAndSaveHighQualityImage(System.Drawing.Image.FromStream(ms), strThumbnails, 100, 150);

                               fileStream.Close();
                               ms.Dispose();
                           }
                           //Copy selected item to all substore
                           CopyToAllSubstore(lstConfigurationInfo, destFile, strThumbnails, "BG");
                       }
                       using (FileStream fileStream =
                                             File.OpenRead(LoginUser.DigiFolderBackGroundPath + "\\8x10\\" + backgoundname))
                       {
                           MemoryStream ms = new MemoryStream();
                           fileStream.CopyTo(ms);
                           ms.Seek(0, SeekOrigin.Begin);
                           strThumbnails = LoginUser.DigiFolderBackGroundPath + "\\Thumbnails\\";
                           //check if the checkbox panorama is checked
                           if (ChkIsBackgroundPanoramic.IsChecked == true)
                           {
                               if (!Directory.Exists(strThumbnails + "\\Panorama"))
                                   Directory.CreateDirectory(strThumbnails + "\\Panorama");
                               ResizeAndSaveHighQualityImage(System.Drawing.Image.FromStream(ms), strThumbnails + "\\Panorama\\" + backgoundname, 100, 150);
                           }
                           else
                           {
                               ResizeAndSaveHighQualityImage(System.Drawing.Image.FromStream(ms), strThumbnails + backgoundname, 100, 150);
                           }
                       }
                       EditableBackground = null;
                       _backgroundname = backgoundname = backgounddisplayname = string.Empty;
                       //GetBackgrounds(); // Commented by Manoj on 19July18
                       btnCancelSelectBackgound_Click(null, null);
                       AuditLog.AddUserLog(Common.LoginUser.UserId, 49, "Add/Edit Background at ");
                   };
                    this.Dispatcher.BeginInvoke(DispatcherPriority.Normal, work);
                }
                else
                {
                    MessageBox.Show("Please browse Background");
                }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }
        delegate void DelegateWork();
        private void CopyToAllSubstore(List<ConfigurationInfo> _objConfigInfo, string Sourcefile, string sourceThumbnails, string ItemType)
        {
            try
            {
                foreach (var item in _objConfigInfo)
                {
                    if (item.DG_Substore_Id != LoginUser.SubStoreId)
                    {
                        string strPath = string.Empty;
                        if (ItemType == "BG")
                        {
                            strPath = item.DG_BG_Path;
                        }
                        else if (ItemType == "Frames")
                        {
                            strPath = item.DG_Frame_Path;
                        }
                        else if (ItemType == "Graphics")
                        {
                            strPath = item.DG_Graphics_Path;
                        }
                        else if (ItemType == "StockShot")
                        {
                            strPath = item.DG_Hot_Folder_Path + "StockShot\\";
                        }
                        else if (ItemType == "Audio")
                        {
                            strPath = item.DG_Hot_Folder_Path + "Audio\\";
                        }
                        else if (ItemType == "Video")
                        {
                            strPath = item.DG_Hot_Folder_Path + "VideoTemplate\\";
                        }
                        else if (ItemType == "VideoBG")
                        {
                            strPath = item.DG_Hot_Folder_Path + "VideoBackGround\\";
                        }
                        else if (ItemType == "VideoOverlay")
                        {
                            strPath = item.DG_Hot_Folder_Path + "VideoOverlay\\";
                        }

                        string destFile = strPath + System.IO.Path.GetFileName(Sourcefile);
                        if (System.IO.Directory.Exists(System.IO.Path.GetDirectoryName(destFile)))
                        {
                            if (!strPath.Contains("BG"))
                                System.IO.File.Copy(Sourcefile, destFile, true);
                            if (!string.IsNullOrEmpty(sourceThumbnails))
                            {
                                destFile = strPath + "Thumbnails\\" + System.IO.Path.GetFileName(sourceThumbnails);
                                if (System.IO.File.Exists(sourceThumbnails))
                                    System.IO.File.Copy(sourceThumbnails, destFile, true);
                                if (ItemType == "BG")
                                {
                                    string src3x3 = LoginUser.DigiFolderBackGroundPath + "\\3x3\\" + backgoundname;
                                    string src4x6 = LoginUser.DigiFolderBackGroundPath + "\\4x6\\" + backgoundname;
                                    string src5x7 = LoginUser.DigiFolderBackGroundPath + "\\5x7\\" + backgoundname;
                                    string src6x8 = LoginUser.DigiFolderBackGroundPath + "\\6x8\\" + backgoundname;
                                    string src8x10 = LoginUser.DigiFolderBackGroundPath + "\\8x10\\" + backgoundname;
                                    string src4x6QR = LoginUser.DigiFolderBackGroundPath + "\\(4x6)&QR\\" + backgoundname;

                                    string dest3x3 = item.DG_BG_Path + "\\3x3\\";
                                    string dest4x6 = item.DG_BG_Path + "\\4x6\\";
                                    string dest5x7 = item.DG_BG_Path + "\\5x7\\";
                                    string dest6x8 = item.DG_BG_Path + "\\6x8\\";
                                    string dest8x10 = item.DG_BG_Path + "\\8x10\\";
                                    string dest4x6QR = item.DG_BG_Path + "\\(4x6)&QR\\";


                                    if (!Directory.Exists(dest3x3))
                                        Directory.CreateDirectory(dest3x3);
                                    if(!sourceThumbnails.Contains(".png"))
                                    {
                                        sourceThumbnails = sourceThumbnails + backgoundname;
                                    }
                                    destFile = dest3x3 + System.IO.Path.GetFileName(sourceThumbnails);
                                    if (System.IO.File.Exists(src3x3))
                                        System.IO.File.Copy(src3x3, destFile, true);


                                    if (!Directory.Exists(dest4x6))
                                        Directory.CreateDirectory(dest4x6);
                                    destFile = dest4x6 + System.IO.Path.GetFileName(sourceThumbnails);
                                    if (System.IO.File.Exists(src4x6))
                                        System.IO.File.Copy(src4x6, destFile, true);

                                    if (!Directory.Exists(dest5x7))
                                        Directory.CreateDirectory(dest5x7);
                                    destFile = dest5x7 + System.IO.Path.GetFileName(sourceThumbnails);
                                    if (System.IO.File.Exists(src5x7))
                                        System.IO.File.Copy(src5x7, destFile, true);

                                    if (!Directory.Exists(dest6x8))
                                        Directory.CreateDirectory(dest6x8);
                                    destFile = dest6x8 + System.IO.Path.GetFileName(sourceThumbnails);
                                    if (System.IO.File.Exists(src6x8))
                                        System.IO.File.Copy(src6x8, destFile, true);

                                    if (!Directory.Exists(dest8x10))
                                        Directory.CreateDirectory(dest8x10);
                                    destFile = dest8x10 + System.IO.Path.GetFileName(sourceThumbnails);
                                    if (System.IO.File.Exists(src8x10))
                                        System.IO.File.Copy(src8x10, destFile, true);

                                    if (!Directory.Exists(dest4x6QR))
                                        Directory.CreateDirectory(dest4x6QR);
                                    destFile = dest4x6QR + System.IO.Path.GetFileName(sourceThumbnails);
                                    if (System.IO.File.Exists(src4x6QR))
                                        System.IO.File.Copy(src4x6QR, destFile, true);

                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }
        private void DeleteFromAllSubstore(List<ConfigurationInfo> _objConfigInfo, string Sourcefile, string ItemType)
        {
            try
            {
                foreach (var item in _objConfigInfo)
                {
                    if (item.DG_Substore_Id != LoginUser.SubStoreId)
                    {
                        string strPath = string.Empty;
                        if (ItemType == "BG")
                        {
                            strPath = item.DG_BG_Path;
                        }
                        else if (ItemType == "Frames")
                        {
                            strPath = item.DG_Frame_Path;
                        }
                        else if (ItemType == "Graphics")
                        {
                            strPath = item.DG_Graphics_Path;
                        }
                        else if (ItemType == "StockShot")
                        {
                            strPath = item.DG_Hot_Folder_Path + "StockShot\\";
                        }

                        else if (ItemType == "Audio")
                        {
                            strPath = item.DG_Hot_Folder_Path + "Audio\\";
                        }

                        else if (ItemType == "Video")
                        {
                            strPath = item.DG_Hot_Folder_Path + "VideoTemplate\\";
                        }

                        else if (ItemType == "VideoBG")
                        {
                            strPath = item.DG_Hot_Folder_Path + "VideoBackGround\\";
                        }

                        else if (ItemType == "VideoOverlay")
                        {
                            strPath = item.DG_Hot_Folder_Path + "VideoOverlay\\";
                        }

                        string destFile = strPath + System.IO.Path.GetFileName(Sourcefile);
                        if (System.IO.Directory.Exists(System.IO.Path.GetDirectoryName(destFile)))
                        {
                            if (System.IO.File.Exists(destFile))
                                System.IO.File.Delete(destFile);

                            string thumbnailPath = strPath + "Thumbnails\\" + System.IO.Path.GetFileName(destFile);
                            if (System.IO.File.Exists(thumbnailPath))
                                System.IO.File.Delete(thumbnailPath);
                            if (ItemType == "BG")
                            {
                                string threebythree = strPath + "3x3\\" + System.IO.Path.GetFileName(destFile);
                                if (System.IO.File.Exists(threebythree))
                                    System.IO.File.Delete(threebythree);

                                string fourbysix = strPath + "4x6\\" + System.IO.Path.GetFileName(destFile);
                                if (System.IO.File.Exists(fourbysix))
                                    System.IO.File.Delete(fourbysix);

                                string fivebyseven = strPath + "5x7\\" + System.IO.Path.GetFileName(destFile);
                                if (System.IO.File.Exists(fivebyseven))
                                    System.IO.File.Delete(fivebyseven);

                                string sixbyeight = strPath + "6x8\\" + System.IO.Path.GetFileName(destFile);
                                if (System.IO.File.Exists(sixbyeight))
                                    System.IO.File.Delete(sixbyeight);

                                string eightbyten = strPath + "8x10\\" + System.IO.Path.GetFileName(destFile);
                                if (System.IO.File.Exists(eightbyten))
                                    System.IO.File.Delete(eightbyten);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }

        /// <summary>
        /// Checks the back ground validations.
        /// </summary>
        /// <returns></returns>
        private bool CheckBackGroundValidations()
        {
            if (txtSelectedBackground.Text == "")
            {
                MessageBox.Show("Please Select the background File");

                return false;
            }
            else if (!IsProductTypeSelected)
            {
                MessageBox.Show("Please Select the product type");

                return false;
            }
            else return true;
        }
        private bool CheckGraphicsValidations()
        {
            if (txtSelectedgraphics.Text == "")
            {
                MessageBox.Show("Please select the graphics file");
                return false;
            }
            else return true;

        }
        /// <summary>
        /// Handles the Click event of the btnDeleteBackground control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void btnDeleteBackground_Click(object sender, RoutedEventArgs e)
        {
            int bgID = 0;
            try
            {
                btnCancelSelectBackgound_Click(sender, e);
                if (sender != null)
                {
                    Button btnSender = (Button)sender;
                    if (btnSender != null)
                    {
                        //int bgID = (int)btnSender.Tag;
                        BackGroundInfo selectedBackground = null;
                        try
                        {
                            selectedBackground = (BackGroundInfo)btnSender.Tag;
                        }
                        catch (Exception ex)
                        {
                            selectedBackground = null;
                        }
                        if (selectedBackground != null)
                        {
                            #region Check for dependency
                            string statusMessage = string.Empty;
                            bool foundDependency = FindDependencyOfBackground(selectedBackground, "delete", out statusMessage);
                            if (foundDependency)
                            {
                                MsgBox.ShowHandlerDialog(statusMessage, DigiMessageBox.DialogType.OK);
                                return;
                            }

                            #endregion
                            bgID = selectedBackground.DG_Background_pkey;
                            string bgFileName = btnSender.CommandParameter.ToString();
                            if (MessageBox.Show("Do you want to delete this background?", "Confirm delete", MessageBoxButton.YesNo) == MessageBoxResult.Yes)
                            {
                                BackgroundBusiness bacBiz = new BackgroundBusiness();
                                try
                                {

                                    //string filename = bacBiz.GetBackGroundFileName(productID, BackGroundGroupId);
                                    if (
                                        System.IO.File.Exists(LoginUser.DigiFolderBackGroundPath + "\\Thumbnails\\" + bgFileName)
                                        // && System.IO.File.Exists(LoginUser.DigiFolderBackGroundPath + bgFileName)
                                        && System.IO.File.Exists(LoginUser.DigiFolderBackGroundPath + "\\4x6\\" + bgFileName)
                                        && System.IO.File.Exists(LoginUser.DigiFolderBackGroundPath + "\\5x7\\" + bgFileName)
                                        && System.IO.File.Exists(LoginUser.DigiFolderBackGroundPath + "\\6x8\\" + bgFileName)
                                        && System.IO.File.Exists(LoginUser.DigiFolderBackGroundPath + "\\8x10\\" + bgFileName)
                                        && System.IO.File.Exists(LoginUser.DigiFolderBackGroundPath + "\\3x3\\" + bgFileName)
                                        )
                                    {
                                        System.IO.File.Delete(LoginUser.DigiFolderBackGroundPath + "\\Thumbnails\\" + bgFileName);
                                        //  System.IO.File.Delete(LoginUser.DigiFolderBackGroundPath + bgFileName);
                                        System.IO.File.Delete(LoginUser.DigiFolderBackGroundPath + "\\4x6\\" + bgFileName);
                                        System.IO.File.Delete(LoginUser.DigiFolderBackGroundPath + "\\5x7\\" + bgFileName);
                                        System.IO.File.Delete(LoginUser.DigiFolderBackGroundPath + "\\3x3\\" + bgFileName);
                                        System.IO.File.Delete(LoginUser.DigiFolderBackGroundPath + "\\6x8\\" + bgFileName);
                                        System.IO.File.Delete(LoginUser.DigiFolderBackGroundPath + "\\8x10\\" + bgFileName);
                                        DeleteFromAllSubstore(lstConfigurationInfo, bgFileName, "BG");
                                        //if (_objDataLayer.DeleteBackGround(productID, BackGroundGroupId))
                                    }
                                }
                                catch (Exception ex)
                                {
                                    string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                                    ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                                }

                                if (bacBiz.DeleteBackGround(bgID))
                                {
                                    MessageBox.Show("Background deleted successfully");
                                    AuditLog.AddUserLog(Common.LoginUser.UserId, (int)FrameworkHelper.ActionType.DeleteBackground, "Background deleted at ");
                                    GetBackgrounds();
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                if (ex is IOException)
                {
                    MessageBox.Show("Background deleted successfully");
                    GetBackgrounds();
                }
                else
                {
                    MessageBox.Show("Can not delete file because " + ex.Message);
                    string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                    ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                }

            }
        }

        /// <summary>
        /// Handles the Click event of the btnCancelSelectBackgound control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void btnCancelSelectBackgound_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                txtSelectedBackground.Text = string.Empty;
                lstBackgroundInfo.Clear();
                DGManageBackground.SelectedItem = null;
                IsBackgroundActive.IsChecked = true;
                EditableBackground = null;
            }
            catch (Exception ex)
            {

            }
        }


        int productId;
        bool IsProductTypeSelected;
        /// <summary>
        /// Handles the Checked event of the RadioButton control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void RadioButton_Checked(object sender, RoutedEventArgs e)
        {
            RadioButton btnsender = (RadioButton)sender;
            productId = btnsender.CommandParameter.ToInt32();
            if (productId != null)
            {
                IsProductTypeSelected = true;
            }
        }

        public int BackGroundID;
        /// <summary>
        /// Finds the visual child.
        /// </summary>
        /// <typeparam name="childItem">The type of the hild item.</typeparam>
        /// <param name="obj">The object.</param>
        /// <returns></returns>
        private childItem FindVisualChild<childItem>(DependencyObject obj) where childItem : DependencyObject
        {
            for (int i = 0; i < VisualTreeHelper.GetChildrenCount(obj); i++)
            {
                DependencyObject child = VisualTreeHelper.GetChild(obj, i);
                if (child != null && child is childItem)
                    return (childItem)child;
                else
                {
                    childItem childOfChild = FindVisualChild<childItem>(child);
                    if (childOfChild != null)
                        return childOfChild;
                }
            }
            return null;
        }
        #endregion

        #region BackGround
        private void BackgroundImageOtherFormats(string selectedBGFileName)
        {
            BitmapImage bitmap = new BitmapImage();
            bitmap.BeginInit();
            bitmap.UriSource = new Uri(selectedBGFileName);
            bitmap.EndInit();
            originalWidth = bitmap.PixelWidth;
            originalHeight = bitmap.PixelHeight;
            sourcepath = selectedBGFileName;
            savepath = LoginUser.DigiFolderPath;
            int index = savepath.IndexOf("DigiImages");
            if (index != -1)
            {
                savepath = savepath.Remove(index - 1);
            }
            CreateThumbnailsFolder();
            Convert6by8();
            Convert4by6();
            Convert5by7();
            Convert3by3();
            Convert8by10();
            ConvertUnique4by6andQR();//Added by VINS 17 Mar 2020
            Convert8by8();//Added by VINS 15 Sep 2021

            #region ajay
            ////6x14
            ////6x20
            ////8x18
            ////8x24
            ////8x26

            Convert6by14();
            Convert6by20();
            Convert8by18();
            Convert8by16();//Add Storybook product size background
            Convert8by24();
            Convert8by26();
            #endregion

        }

        // 6*8 = 2400*1800
        private void Convert6by8()
        {
            ProductTypeId = 1;
            // saveFolderPath = savepath + "\\6x8";
            saveFolderPath = System.IO.Path.Combine(savepath, "DigiImages", "BG", "6x8");
            if (!Directory.Exists(saveFolderPath))
            {
                Directory.CreateDirectory(saveFolderPath);
            }
            ResizeWPFImage(sourcepath, saveFolderPath);
            saveFolderPath = null;
        }

        // 4*6 = 1800*1200
        private void Convert4by6()
        {
            ProductTypeId = 2;
            // saveFolderPath = savepath + "\\4x6";
            saveFolderPath = System.IO.Path.Combine(savepath, "DigiImages", "BG", "4x6");
            if (!Directory.Exists(saveFolderPath))
            {
                Directory.CreateDirectory(saveFolderPath);
            }
            ResizeWPFImage(sourcepath, saveFolderPath);
            saveFolderPath = null;
        }

        // 5*7 = 2100*1500
        private void Convert5by7()
        {
            ProductTypeId = 3;
            // saveFolderPath = savepath + "\\5x7";
            saveFolderPath = System.IO.Path.Combine(savepath, "DigiImages", "BG", "5x7");
            if (!Directory.Exists(saveFolderPath))
            {
                Directory.CreateDirectory(saveFolderPath);
            }
            ResizeWPFImage(sourcepath, saveFolderPath);
            saveFolderPath = null;
        }

        // 3*3 = 900*900
        private void Convert3by3()
        {
            ProductTypeId = 4;
            // saveFolderPath = savepath + "\\3x3";
            saveFolderPath = System.IO.Path.Combine(savepath, "DigiImages", "BG", "3x3");
            if (!Directory.Exists(saveFolderPath))
            {
                Directory.CreateDirectory(saveFolderPath);
            }
            ResizeWPFImage(sourcepath, saveFolderPath);
            saveFolderPath = null;
        }

        private void Convert8by10()
        {
            ProductTypeId = 5;
            saveFolderPath = System.IO.Path.Combine(savepath, "DigiImages", "BG", "8x10");
            if (!Directory.Exists(saveFolderPath))
            {
                Directory.CreateDirectory(saveFolderPath);
            }
            ResizeWPFImage(sourcepath, saveFolderPath);
            saveFolderPath = null;
        }

        private void Convert8by8()
        {
            ProductTypeId = 130;
            saveFolderPath = System.IO.Path.Combine(savepath, "DigiImages", "BG", "8x8");
            if (!Directory.Exists(saveFolderPath))
            {
                Directory.CreateDirectory(saveFolderPath);
            }
            ResizeWPFImage(sourcepath, saveFolderPath);
            saveFolderPath = null;
        }

        private void ConvertUnique4by6andQR()
        {
            ProductTypeId = 124;
            saveFolderPath = System.IO.Path.Combine(savepath, "DigiImages", "BG", "Unique(4x6)&QR");
            if (!Directory.Exists(saveFolderPath))
            {
                Directory.CreateDirectory(saveFolderPath);
            }
            ResizeWPFImage(sourcepath, saveFolderPath);
            saveFolderPath = null;
        }


        #region Ajay create background for the panoramic sizes
        private void Convert6by14()
        {
            ProductTypeId = 6;
            // saveFolderPath = savepath + "\\6x8";
            saveFolderPath = System.IO.Path.Combine(savepath, "DigiImages", "BG", "6x14");
            if (!Directory.Exists(saveFolderPath))
            {
                Directory.CreateDirectory(saveFolderPath);
            }
            ResizeWPFImage(sourcepath, saveFolderPath);
            saveFolderPath = null;
        }
        private void Convert6by20()
        {
            ProductTypeId = 7;
            // saveFolderPath = savepath + "\\6x8";
            saveFolderPath = System.IO.Path.Combine(savepath, "DigiImages", "BG", "6x20");
            if (!Directory.Exists(saveFolderPath))
            {
                Directory.CreateDirectory(saveFolderPath);
            }
            ResizeWPFImage(sourcepath, saveFolderPath);
            saveFolderPath = null;
        }
        private void Convert8by18()
        {
            ProductTypeId = 8;
            // saveFolderPath = savepath + "\\6x8";
            saveFolderPath = System.IO.Path.Combine(savepath, "DigiImages", "BG", "8x18");
            if (!Directory.Exists(saveFolderPath))
            {
                Directory.CreateDirectory(saveFolderPath);
            }
            ResizeWPFImage(sourcepath, saveFolderPath);
            saveFolderPath = null;
        }
        private void Convert8by24()
        {
            ProductTypeId = 9;
            // saveFolderPath = savepath + "\\6x8";
            saveFolderPath = System.IO.Path.Combine(savepath, "DigiImages", "BG", "8x24");
            if (!Directory.Exists(saveFolderPath))
            {
                Directory.CreateDirectory(saveFolderPath);
            }
            ResizeWPFImage(sourcepath, saveFolderPath);
            saveFolderPath = null;
        }
        private void Convert8by26()
        {
            ProductTypeId = 10;
            // saveFolderPath = savepath + "\\6x8";
            saveFolderPath = System.IO.Path.Combine(savepath, "DigiImages", "BG", "8x26");
            if (!Directory.Exists(saveFolderPath))
            {
                Directory.CreateDirectory(saveFolderPath);
            }
            ResizeWPFImage(sourcepath, saveFolderPath);
            saveFolderPath = null;
        }

        /// <summary>
        /// Method added by Vins_26Aug2021
        /// To save background of size 8*16 size
        /// </summary>
        private void Convert8by16()
        {
            ProductTypeId = 11;           
            saveFolderPath = System.IO.Path.Combine(savepath, "DigiImages", "BG", "8x16");
            if (!Directory.Exists(saveFolderPath))
            {
                Directory.CreateDirectory(saveFolderPath);
            }
            ResizeWPFImage(sourcepath, saveFolderPath);
            saveFolderPath = null;
        }
        #endregion

        private void CreateThumbnailsFolder()
        {
            saveFolderPath = System.IO.Path.Combine(savepath, "DigiImages", "BG", "Thumbnails");
            if (!Directory.Exists(saveFolderPath))
            {
                Directory.CreateDirectory(saveFolderPath);
            }
            saveFolderPath = null;
        }
        private void ResizeWPFImage(string sourceImage, string saveToPath)
        {
            BitmapImage bi = new BitmapImage();
            BitmapImage bitmapImage = new BitmapImage();
            double ratio = 1;
            try
            {
                using (FileStream fileStream = File.OpenRead(sourceImage.ToString()))
                {
                    MemoryStream ms = new MemoryStream();
                    fileStream.CopyTo(ms);
                    ms.Seek(0, SeekOrigin.Begin);
                    fileStream.Close();
                    bi.BeginInit();
                    bi.StreamSource = ms;
                    bi.EndInit();
                    bi.Freeze();

                    if (ProductTypeId == 1 || ProductTypeId == 124)//6by8 or (Unique 4*6 + QR)
                    {
                        if (originalWidth >= originalHeight)
                        {
                            ms.Seek(0, SeekOrigin.Begin);
                            bitmapImage.BeginInit();
                            bitmapImage.StreamSource = ms;
                            bitmapImage.DecodePixelWidth = 2400;
                            bitmapImage.DecodePixelHeight = 1800;

                            bitmapImage.EndInit();
                            bitmapImage.Freeze();
                            ratio = (double)((double)bitmapImage.PixelWidth / (double)bitmapImage.PixelHeight);
                        }
                        else
                        {
                            ms.Seek(0, SeekOrigin.Begin);
                            bitmapImage.BeginInit();
                            bitmapImage.StreamSource = ms;
                            bitmapImage.DecodePixelWidth = 1800;
                            bitmapImage.DecodePixelHeight = 2400;
                            bitmapImage.EndInit();
                            bitmapImage.Freeze();
                            ratio = (double)((double)bitmapImage.PixelHeight / (double)bitmapImage.PixelWidth);
                        }
                    }
                    else if (ProductTypeId == 2)
                    {
                        if (originalWidth >= originalHeight)
                        {
                            ms.Seek(0, SeekOrigin.Begin);
                            bitmapImage.BeginInit();
                            bitmapImage.StreamSource = ms;
                            bitmapImage.DecodePixelWidth = 1800;
                            bitmapImage.DecodePixelHeight = 1200;

                            bitmapImage.EndInit();
                            bitmapImage.Freeze();
                            ratio = (double)((double)bitmapImage.PixelWidth / (double)bitmapImage.PixelHeight);
                        }
                        else
                        {
                            ms.Seek(0, SeekOrigin.Begin);
                            bitmapImage.BeginInit();
                            bitmapImage.StreamSource = ms;
                            bitmapImage.DecodePixelWidth = 1200;
                            bitmapImage.DecodePixelHeight = 1800;

                            bitmapImage.EndInit();
                            bitmapImage.Freeze();
                            ratio = (double)((double)bitmapImage.PixelHeight / (double)bitmapImage.PixelWidth);
                        }
                    }

                    else if (ProductTypeId == 3)
                    {
                        if (originalWidth >= originalHeight)
                        {
                            ms.Seek(0, SeekOrigin.Begin);
                            bitmapImage.BeginInit();
                            bitmapImage.StreamSource = ms;
                            bitmapImage.DecodePixelWidth = 2100;
                            bitmapImage.DecodePixelHeight = 1500;

                            bitmapImage.EndInit();
                            bitmapImage.Freeze();
                            ratio = (double)((double)bitmapImage.PixelWidth / (double)bitmapImage.PixelHeight);
                        }
                        else
                        {
                            ms.Seek(0, SeekOrigin.Begin);
                            bitmapImage.BeginInit();
                            bitmapImage.StreamSource = ms;
                            bitmapImage.DecodePixelWidth = 1500;
                            bitmapImage.DecodePixelHeight = 2100;

                            bitmapImage.EndInit();
                            bitmapImage.Freeze();
                            ratio = (double)((double)bitmapImage.PixelWidth / (double)bitmapImage.PixelHeight);
                        }
                    }
                    else if (ProductTypeId == 4)
                    {
                        ms.Seek(0, SeekOrigin.Begin);
                        bitmapImage.BeginInit();
                        bitmapImage.StreamSource = ms;
                        bitmapImage.DecodePixelWidth = 900;
                        bitmapImage.DecodePixelHeight = 900;

                        bitmapImage.EndInit();
                        bitmapImage.Freeze();
                        ratio = (double)((double)bitmapImage.PixelWidth / (double)bitmapImage.PixelHeight);
                    }
                    else if (ProductTypeId == 5)
                    {
                        if (originalWidth >= originalHeight)
                        {
                            ms.Seek(0, SeekOrigin.Begin);
                            bitmapImage.BeginInit();
                            bitmapImage.StreamSource = ms;
                            bitmapImage.DecodePixelWidth = 3000;
                            bitmapImage.DecodePixelHeight = 2400;

                            bitmapImage.EndInit();
                            bitmapImage.Freeze();
                            ratio = (double)((double)bitmapImage.PixelWidth / (double)bitmapImage.PixelHeight);
                        }
                        else
                        {
                            ms.Seek(0, SeekOrigin.Begin);
                            bitmapImage.BeginInit();
                            bitmapImage.StreamSource = ms;
                            bitmapImage.DecodePixelWidth = 2400;
                            bitmapImage.DecodePixelHeight = 3000;

                            bitmapImage.EndInit();
                            bitmapImage.Freeze();
                            ratio = (double)((double)bitmapImage.PixelHeight / (double)bitmapImage.PixelWidth);
                        }
                    }

                    else if (ProductTypeId == 130)//8*8 size
                    {
                        if (originalWidth >= originalHeight)
                        {
                            ms.Seek(0, SeekOrigin.Begin);
                            bitmapImage.BeginInit();
                            bitmapImage.StreamSource = ms;
                            bitmapImage.DecodePixelWidth = 2400;
                            bitmapImage.DecodePixelHeight = 2400;

                            bitmapImage.EndInit();
                            bitmapImage.Freeze();
                            ratio = (double)((double)bitmapImage.PixelWidth / (double)bitmapImage.PixelHeight);
                        }
                        else
                        {
                            ms.Seek(0, SeekOrigin.Begin);
                            bitmapImage.BeginInit();
                            bitmapImage.StreamSource = ms;
                            bitmapImage.DecodePixelWidth = 2400;
                            bitmapImage.DecodePixelHeight = 2400;

                            bitmapImage.EndInit();
                            bitmapImage.Freeze();
                            ratio = (double)((double)bitmapImage.PixelHeight / (double)bitmapImage.PixelWidth);
                        }
                    }

                    #region ajay
                    if (ProductTypeId == 6)
                    {
                        if (originalWidth >= originalHeight)
                        {
                            ms.Seek(0, SeekOrigin.Begin);
                            bitmapImage.BeginInit();
                            bitmapImage.StreamSource = ms;
                            bitmapImage.DecodePixelWidth = 4200;
                            bitmapImage.DecodePixelHeight = 1800;

                            bitmapImage.EndInit();
                            bitmapImage.Freeze();
                            ratio = (double)((double)bitmapImage.PixelWidth / (double)bitmapImage.PixelHeight);
                        }
                        else
                        {
                            ms.Seek(0, SeekOrigin.Begin);
                            bitmapImage.BeginInit();
                            bitmapImage.StreamSource = ms;
                            bitmapImage.DecodePixelWidth = 1800;
                            bitmapImage.DecodePixelHeight = 4200;
                            bitmapImage.EndInit();
                            bitmapImage.Freeze();
                            ratio = (double)((double)bitmapImage.PixelHeight / (double)bitmapImage.PixelWidth);
                        }
                    }
                    else if (ProductTypeId == 7)
                    {
                        if (originalWidth >= originalHeight)
                        {
                            ms.Seek(0, SeekOrigin.Begin);
                            bitmapImage.BeginInit();
                            bitmapImage.StreamSource = ms;
                            bitmapImage.DecodePixelWidth = 6000;
                            bitmapImage.DecodePixelHeight = 1800;

                            bitmapImage.EndInit();
                            bitmapImage.Freeze();
                            ratio = (double)((double)bitmapImage.PixelWidth / (double)bitmapImage.PixelHeight);
                        }
                        else
                        {
                            ms.Seek(0, SeekOrigin.Begin);
                            bitmapImage.BeginInit();
                            bitmapImage.StreamSource = ms;
                            bitmapImage.DecodePixelWidth = 1800;
                            bitmapImage.DecodePixelHeight = 6000;

                            bitmapImage.EndInit();
                            bitmapImage.Freeze();
                            ratio = (double)((double)bitmapImage.PixelHeight / (double)bitmapImage.PixelWidth);
                        }
                    }

                    else if (ProductTypeId == 8) //8 * 18
                    {
                        if (originalWidth >= originalHeight)
                        {
                            ms.Seek(0, SeekOrigin.Begin);
                            bitmapImage.BeginInit();
                            bitmapImage.StreamSource = ms;
                            bitmapImage.DecodePixelWidth = 5400;
                            bitmapImage.DecodePixelHeight = 2400;

                            bitmapImage.EndInit();
                            bitmapImage.Freeze();
                            ratio = (double)((double)bitmapImage.PixelWidth / (double)bitmapImage.PixelHeight);
                        }
                        else
                        {
                            ms.Seek(0, SeekOrigin.Begin);
                            bitmapImage.BeginInit();
                            bitmapImage.StreamSource = ms;
                            bitmapImage.DecodePixelWidth = 2400;
                            bitmapImage.DecodePixelHeight = 5400;

                            bitmapImage.EndInit();
                            bitmapImage.Freeze();
                            ratio = (double)((double)bitmapImage.PixelWidth / (double)bitmapImage.PixelHeight);
                        }
                    }
                    else if (ProductTypeId == 9)//8x24
                    {
                        if (originalWidth >= originalHeight)
                        {
                            ms.Seek(0, SeekOrigin.Begin);
                            bitmapImage.BeginInit();
                            bitmapImage.StreamSource = ms;
                            bitmapImage.DecodePixelWidth = 14400;// 14400;//600 dpi  7200 px
                            bitmapImage.DecodePixelHeight = 4800;//4800;//600 dpi   2400 px

                            bitmapImage.EndInit();
                            bitmapImage.Freeze();
                            ratio = (double)((double)bitmapImage.PixelWidth / (double)bitmapImage.PixelHeight);
                        }
                        else
                        {

                            ms.Seek(0, SeekOrigin.Begin);
                            bitmapImage.BeginInit();
                            bitmapImage.StreamSource = ms;
                            bitmapImage.DecodePixelWidth = 2400;
                            bitmapImage.DecodePixelHeight = 7200;

                            bitmapImage.EndInit();
                            bitmapImage.Freeze();
                            ratio = (double)((double)bitmapImage.PixelWidth / (double)bitmapImage.PixelHeight);
                        }
                    }
                    else if (ProductTypeId == 10)//8by26
                    {
                        if (originalWidth >= originalHeight)
                        {
                            ms.Seek(0, SeekOrigin.Begin);
                            bitmapImage.BeginInit();
                            bitmapImage.StreamSource = ms;
                            bitmapImage.DecodePixelWidth = 2400;
                            bitmapImage.DecodePixelHeight = 7800;

                            bitmapImage.EndInit();
                            bitmapImage.Freeze();
                            ratio = (double)((double)bitmapImage.PixelWidth / (double)bitmapImage.PixelHeight);
                        }
                        else
                        {
                            ms.Seek(0, SeekOrigin.Begin);
                            bitmapImage.BeginInit();
                            bitmapImage.StreamSource = ms;
                            bitmapImage.DecodePixelWidth = 7800;
                            bitmapImage.DecodePixelHeight = 2400;

                            bitmapImage.EndInit();
                            bitmapImage.Freeze();
                            ratio = (double)((double)bitmapImage.PixelHeight / (double)bitmapImage.PixelWidth);
                        }
                    }
                    else if (ProductTypeId == 11) //8 * 16
                    {
                        if (originalWidth >= originalHeight)
                        {
                            //Horizontal 
                            //1cm=300 pixels
                            ms.Seek(0, SeekOrigin.Begin);
                            bitmapImage.BeginInit();
                            bitmapImage.StreamSource = ms;
                            bitmapImage.DecodePixelWidth = 4800;
                            bitmapImage.DecodePixelHeight = 2400;

                            bitmapImage.EndInit();
                            bitmapImage.Freeze();
                            ratio = (double)((double)bitmapImage.PixelWidth / (double)bitmapImage.PixelHeight);
                        }
                        else
                        {
                            //Vertical image
                            ms.Seek(0, SeekOrigin.Begin);
                            bitmapImage.BeginInit();
                            bitmapImage.StreamSource = ms;
                            bitmapImage.DecodePixelWidth = 2400;
                            bitmapImage.DecodePixelHeight = 4800;

                            bitmapImage.EndInit();
                            bitmapImage.Freeze();
                            ratio = (double)((double)bitmapImage.PixelWidth / (double)bitmapImage.PixelHeight);
                        }
                    }
                    #endregion


                    bitmapImage.Freeze();
                    fileStream.Close();
                }

                using (var fileStreamForSave = new FileStream(saveToPath + "\\" + backgoundname, FileMode.Create, FileAccess.ReadWrite))
                {
                    //string savepath1 = savepath + "6x8";
                    //if (!Directory.Exists(savepath1))
                    //{
                    //    Directory.CreateDirectory(savepath1);
                    //}
                    //savepath1 = null;
                    JpegBitmapEncoder encoder = new JpegBitmapEncoder();
                    encoder.QualityLevel = 94;
                    encoder.Frames.Add(BitmapFrame.Create(bitmapImage));
                    encoder.Save(fileStreamForSave);
                    fileStreamForSave.Close();
                }
                bi = null;
                bitmapImage = null;
            }
            catch (Exception ex)
            {
                //_logger.LogException(ex, DigiPhoto.LogEnvelop.Data.LoggingLevel.Error);
                //  ErrorHandler.ErrorHandler.LogFileWrite(ex.Message);
            }
            finally
            {
                bi = null;
                bitmapImage = null;
            }
        }

        #endregion BacGround


        /// <summary>
        /// Handles the Click event of the NewBackground control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        //private void NewBackground_Click(object sender, RoutedEventArgs e)
        //{
        //    string productToRemove = "95,96,97";
        //    string[] productToRemoveArray = productToRemove.Split(',');
        //    BGMaster _objnew = new BGMaster();
        //    List<AllProductType> _objdetail;
        //    _objdetail = new List<AllProductType>();
        //    //foreach (var items in _objDataLayer.GetProductType())
        //    ProductBusiness proBiz = new ProductBusiness();
        //    //foreach (var items in proBiz.GetProductType())
        //    List<ProductTypeInfo> objPro = (new ProductBusiness()).GetProductType();
        //    foreach (var items in objPro)
        //    {
        //        AllProductType _objnewpro = new AllProductType();
        //        _objnewpro.ProductId1 = items.DG_Orders_ProductType_pkey;
        //        _objnewpro.ProductName1 = items.DG_Orders_ProductType_Name;
        //        _objnewpro.Isvisible1 = Visibility.Collapsed;
        //        _objdetail.Add(_objnewpro);
        //    }
        //   _objnew.LstProductType = _objdetail.Where(x => !productToRemoveArray.Contains(x.ProductId1.ToString())).ToList();
        //    lstbg.Items.Add(_objnew);
        //    lstbg.SelectedItem = _objnew;
        //    lstbg.ScrollIntoView(lstbg.SelectedItem);
        //}

        // public int BackGroundID;
        /// <summary>
        /// Handles the SelectionChanged event of the lstbg control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="SelectionChangedEventArgs"/> instance containing the event data.</param>
        //private void lstbg_SelectionChanged(object sender, SelectionChangedEventArgs e)
        //{
        //    try
        //    {
        //        if (lstbg.SelectedItem != null)
        //        {
        //            BackGroundID = ((BGMaster)lstbg.SelectedItem).BgID.ToString().ToInt32();
        //            // ListBoxItem listBoxItem = (ListBoxItem)lstbg.ItemContainerGenerator.ContainerFromItem(lstbg.SelectedItem);
        //            // listBoxItem.Focus();
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
        //        ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
        //    }

        //}

        /// <summary>
        /// Finds the visual child.
        /// </summary>
        /// <typeparam name="childItem">The type of the hild item.</typeparam>
        /// <param name="obj">The object.</param>
        /// <returns></returns>
        //private childItem FindVisualChild<childItem>(DependencyObject obj)
        //where childItem : DependencyObject
        //{
        //    for (int i = 0; i < VisualTreeHelper.GetChildrenCount(obj); i++)
        //    {
        //        DependencyObject child = VisualTreeHelper.GetChild(obj, i);
        //        if (child != null && child is childItem)
        //            return (childItem)child;
        //        else
        //        {
        //            childItem childOfChild = FindVisualChild<childItem>(child);
        //            if (childOfChild != null)
        //                return childOfChild;
        //        }
        //    }
        //    return null;
        //}


        #region Code for Saving the SubStore
        List<SelectedSubStores> lstSelectedSubstore;

        /// <summary>
        /// Handles the Click event of the btnSaveSubStore control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void btnSaveSubStore_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (cmbSelectSubstore.SelectedValue == null)
                {
                    MessageBox.Show("Select site name");
                    return;
                }
                string pathtosave = Environment.CurrentDirectory;
                if (File.Exists(pathtosave + "\\ss.dat"))
                {
                    File.Delete(pathtosave + "\\ss.dat");
                }
                using (StreamWriter b = new StreamWriter(File.Open(pathtosave + "\\ss.dat", FileMode.Create)))
                {
                    string substoresearch = "";
                    foreach (var item in lstSelectedSubstore.Where(t => t.Isselected == true))
                    {
                        substoresearch += "," + item.SubStoreId.ToString();
                    }

                    LoginUser.DefaultSubstores = cmbSelectSubstore.SelectedValue.ToString() + substoresearch;
                    LoginUser.SubStoreId = cmbSelectSubstore.SelectedValue.ToInt32();
                    //LoginUser.SubstoreName = (new DigiPhotoDataServices()).GetSubstoreNameById(LoginUser.SubStoreId);
                    LoginUser.SubstoreName = (new StoreSubStoreDataBusniess()).GetSubstoreNameById(LoginUser.SubStoreId);
                    b.Write(CryptorEngine.Encrypt(cmbSelectSubstore.SelectedValue.ToString() + substoresearch, true));
                    b.Close();
                    //////changed by latika for visible in view store setting 15Nov2019
                    string seleSubstoreVisible = cmbSelectSubstore.SelectedValue.ToString();

                    foreach (SelectedSubStores row in dgrdLocations.Items)
                    {
                        if (row.VisiblVOrderStation)
                        {
                            seleSubstoreVisible += "," + row.SubStoreId.ToString(); 

                        }
                    }
                   bool RetunVal = (new StoreSubStoreDataBusniess()).SaveSubstoreConfigVisbleInOrdStation(seleSubstoreVisible,Convert.ToInt32(cmbSelectSubstore.SelectedValue));
                    ///end by latika
                    MessageBox.Show("Substore info saved successfully");
                    string strprint = Common.LoginUser.ReceiptPrinterPath;
                    if (!string.IsNullOrEmpty(strprint))
                    {
                        LoginUser.ReceiptPrinterPath = strprint;
                    }
                    else
                    {
                        LoginUser.ReceiptPrinterPath = "0";
                    }
                    cmbReceiptPrinter.SelectedValue = LoginUser.ReceiptPrinterPath;
                    string strGroupPath = Common.LoginUser.GroupValue;
                    if (!string.IsNullOrEmpty(strGroupPath))
                    {
                        LoginUser.GroupValue = strGroupPath;
                    }
                    else
                    {
                        LoginUser.GroupValue = "0";
                    }
                    // cmbGroupSelection.SelectedValue = LoginUser.GroupValue;


                    GetConfigData();
                    FillOnlineTabInfo();
                    GetNewConfigData();
                    FillLocationCombo();
                    RobotImageLoader.GroupImages = new List<LstMyItems>();
                    RobotImageLoader.PrintImages = new List<LstMyItems>();
                    //Get DataSyncServiceURL
                    App.DataSyncServiceURl = string.Empty;

                    var searchWindow = GetSearchWindow();
                    if (searchWindow != null)
                        searchWindow.LoadSubStore();

                    #region "Update POS Mapping"
                    //update pos mapping
                    try
                    {
                        ServicePosInfoBusiness servicePosInfoBusiness = new ServicePosInfoBusiness();
                        ImixPOSDetail imixPosDettail = new ImixPOSDetail();
                        imixPosDettail.CreatedBy = "webusers";
                        imixPosDettail.IsStart = true;
                        imixPosDettail.SyncCode = "";
                        imixPosDettail.SubStoreID = cmbSelectSubstore.SelectedValue.ToInt32();
                        imixPosDettail.ImixPOSDetailID = 0;
                        imixPosDettail.MacAddress = getMAcAddress();
                        servicePosInfoBusiness.InsertImixPosBusiness(imixPosDettail);
                    }
                    catch (Exception ex)
                    {
                        string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                        ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                    }

                    #endregion
                }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                MessageBox.Show(ex.Message);
            }
        }

        public string getMAcAddress()
        {
            ManagementObjectSearcher search = new ManagementObjectSearcher("SELECT * FROM Win32_NetworkAdapterConfiguration where IPEnabled=true");
            IEnumerable<ManagementObject> objects = search.Get().Cast<ManagementObject>();
            string mac = (from o in objects orderby o["IPConnectionMetric"] select o["MACAddress"].ToString()).FirstOrDefault();
            return mac;
        }
        private static SearchByPhoto GetSearchWindow()
        {
            SearchByPhoto window = System.Windows.Application.Current.Windows.Cast<Window>().Where(wnd => wnd.Title == "Search").Cast<SearchByPhoto>().FirstOrDefault();
            return window;
        }
        /// <summary>
        /// Fills the substore.
        /// </summary>
        private void FillSubstore()
        {
            try
            {

                lstSelectedSubstore = new List<SelectedSubStores>();
                var lstSubStore = (new StoreSubStoreDataBusniess()).GetSubstoreData();

                foreach (var item in lstSubStore)
                {
                    SelectedSubStores objitem = new SelectedSubStores();
                    objitem.SubStoreId = item.DG_SubStore_pkey;
                    objitem.SubStoreName = item.DG_SubStore_Name;
                    objitem.Isselected = false;
objitem.VisiblVOrderStation = false;//////changed by latika for visible in view store setting 15Nov2019
                    lstSelectedSubstore.Add(objitem);
                }
                CommonUtility.BindCombo<SubStoresInfo>(cmbSelectSubstore, lstSubStore, "DG_SubStore_Name", "DG_SubStore_pkey");

                cmbSelectSubstore.SelectedValue = lstSubStore.FirstOrDefault().DG_SubStore_pkey.ToString();
                dgrdLocations.ItemsSource = lstSelectedSubstore;

            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
            ReadSubStore();
        }
        /// <summary>
        /// Reads the sub store.
        /// </summary>
        private void ReadSubStore()
        {
            try
            {
                string pathtosave = Environment.CurrentDirectory;
                if (File.Exists(pathtosave + "\\ss.dat"))
                {
                    string line;
                    string SubStoreId;
                    using (StreamReader reader = new StreamReader(pathtosave + "\\ss.dat"))
                    {
                        line = reader.ReadLine();
                        SubStoreId = CryptorEngine.Decrypt(line, true);
                        LoginUser.SubStoreId = SubStoreId.Split(',')[0].ToInt32();
                        LoginUser.DefaultSubstores = SubStoreId;
                        List<string> _objsubstoresID = new List<string>();
                        _objsubstoresID = SubStoreId.Split(',').ToList();
                        cmbSelectSubstore.SelectedValue = SubStoreId.Split(',')[0];
                        var lstSubStore2 = (new StoreSubStoreDataBusniess()).GetSubstoreVisibleOrdStion(Convert.ToInt32(cmbSelectSubstore.SelectedValue));//////changed by latika for visible in view store setting 15Nov2019
                        string VisibleSubstore = lstSubStore2.VisiblVOrderStationlst;
                        string []ArrSub = VisibleSubstore.Split(',');
                        foreach (var item in lstSelectedSubstore)
                        {
                            var itellist = _objsubstoresID.Where(t => t.ToString() == item.SubStoreId.ToString());
                            if (item.SubStoreId.ToString() == cmbSelectSubstore.SelectedValue.ToString())
                            { item.Isselected = true;
                              //item.isSelectEnable = false;
                              //  item.isVisiblEnable = false;
                            }
                            else if (itellist.Count() > 0)
                            {
                                item.Isselected = true;
                            }
                            item.VisiblVOrderStation = false;
                            if (ArrSub.Length > 0)
                            {
                                for (int sublist = 0; sublist < ArrSub.Length; sublist++)
                                {
                                    if (ArrSub[sublist].ToString() == item.SubStoreId.ToString())
                                    {
                                        item.VisiblVOrderStation = true;
                                    }
                                }
                            }
                        }
                    }

                }
                 var lstSubStore = (new StoreSubStoreDataBusniess()).GetSubstoreVisibleOrdStion(Convert.ToInt32(cmbSelectSubstore.SelectedValue));//////changed by latika for visible in view store setting 15Nov2019
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                //MessageBox.Show(ex.Message);
            }
        }
        #endregion

        private void txtChromaTolerance_KeyUp(object sender, KeyEventArgs e)
        {
            {
                if (char.IsLetter((char)KeyInterop.VirtualKeyFromKey(e.Key)) ||
                    char.IsSymbol((char)KeyInterop.VirtualKeyFromKey(e.Key)) ||
                    char.IsWhiteSpace((char)KeyInterop.VirtualKeyFromKey(e.Key)) ||
                    char.IsPunctuation((char)KeyInterop.VirtualKeyFromKey(e.Key)))
                    e.Handled = true;
            }

            {
                //allows only numbers between 1 and 2
                string value = txtChromaTolerance.Text;


                if (txtChromaTolerance.Text != "")
                {
                    if (value[0] == '.')
                    {
                        value = "0.";
                        txtChromaTolerance.Text = "0.";
                    }
                    if (Decimal.Parse(value) < 0)
                    {
                        MessageBox.Show("This Value should be a decimal number between 0 and 2");
                        txtChromaTolerance.Text = "";
                    }
                    else if (Decimal.Parse(value) > 2)
                    {
                        MessageBox.Show("This Value should be a decimal number between 0 and 2");
                        txtChromaTolerance.Text = "";
                    }

                }
            }
        }

        private void cmbChromaColor_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            string colorCode = string.Empty;
            if (_isDefaultColorSeletedValue != ((ComboBoxItem)cmbChromaColor.SelectedItem).Content.ToString())
                _isDefaultColorCode = true;
            if (cmbChromaColor.SelectedIndex == 0)
            {
                txtChromaTolerance.Text = "1.0";
                colorCode = "#00FF00";
            }
            else if (cmbChromaColor.SelectedIndex == 1)
            {
                txtChromaTolerance.Text = "0.4";
                colorCode = "#FF0000";
            }
            else if (cmbChromaColor.SelectedIndex == 2)
            {
                txtChromaTolerance.Text = "0.5";
                colorCode = "#0080FF";
            }
            else if (cmbChromaColor.SelectedIndex == 3)
            {
                txtChromaTolerance.Text = "0.15";
                colorCode = "#A9A9A9";
            }
            if (_isDefaultColorCode)
            {
                txtConfigColorCode.Text = colorCode;

            }
            _isDefaultColorCode = true;
        }
        /*
        private void btnResetBriCon_Click(object sender, RoutedEventArgs e)
        {
            cVal.Value = 1;
            bVal.Value = 0;
        }
        */

        //private void TabConfiguration_GotFocus(object sender, RoutedEventArgs e)
        //{
        //    SetDefaultButton(btnSaveChanges);
        //}

        //private void tbmain_SelectionChanged(object sender, SelectionChangedEventArgs e)
        //{
        //    if (((System.Windows.Controls.Primitives.Selector)(sender)).SelectedItem != null)
        //    {
        //        string TabName = ((TabItem)((System.Windows.Controls.Primitives.Selector)(sender)).SelectedItem).Name;
        //        //Configuration Tab 
        //        if (string.Compare(TabName, "TabConfiguration", true) == 0)
        //        {
        //            SetDefaultButton(btnSaveChanges);
        //        }
        //        //Spec Printing
        //        else if (string.Compare(TabName, "TabSemiOrder", true) == 0)
        //        {
        //            SetDefaultButton(btnSaveAutoCorrect);
        //        }
        //        //Manage Border
        //        else if (string.Compare(TabName, "TabManageBorder", true) == 0)
        //        {
        //            SetDefaultButton(btnSaveSelectBorder);
        //        }
        //        //Manage Background
        //        else if (string.Compare(TabName, "TabManageBackground", true) == 0)
        //        {
        //            SetDefaultButton(btnSaveSelectBackground);
        //        }
        //        //Manage Currency
        //        else if (string.Compare(TabName, "TabManageCurrency", true) == 0)
        //        {
        //            SetDefaultButton(btnSaveCurrency);
        //        }
        //        //Manage Graphics
        //        else if (string.Compare(TabName, "TabManageGraphics", true) == 0)
        //        {
        //            SetDefaultButton(btnSaveSelectGraphics);
        //        }
        //        //Manage Discount
        //        else if (string.Compare(TabName, "TabManageDiscount", true) == 0)
        //        {
        //            SetDefaultButton(btnSaveDiscount);
        //        }
        //        //Sub-store configuration
        //        else if (string.Compare(TabName, "TabSSConfiguration", true) == 0)
        //        {
        //            SetDefaultButton(btnSaveSubStore);
        //        }
        //        //Back up
        //        else if (string.Compare(TabName, "TabBackup", true) == 0)
        //        {
        //            SetDefaultButton(btnBackupBack);
        //        }
        //        //digimagic 
        //        else if (string.Compare(TabName, "Tabdigimagic", true) == 0)
        //        {
        //            SetDefaultButton(btnSaveDigimagic);
        //        }
        //    }
        //}

        private void btndefault_Click(object sender, RoutedEventArgs e)
        {

            GrdSharpens.Visibility = Visibility.Collapsed;
            brightVal.Focus();
            Flower.Visibility = Visibility.Collapsed;
            brightVal.Value = -0.05;
            contrastVal.Value = 1.3;
            sharpVal.Value = 0.05;
            txtbright.Text = "-0.05";
            txtcontrasts.Text = "1.3";
            txtsharp.Text = "0.05";

        }
        private void btnSaveDigimagic_click(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrEmpty(txtbright.Text))
            {
                MessageBox.Show("Please enter the value of brightness");
                return;
            }
            if (string.IsNullOrEmpty(txtcontrasts.Text))
            {
                MessageBox.Show("Please enter the value of contrast");
                return;
            }
            if (string.IsNullOrEmpty(txtsharp.Text))
            {
                MessageBox.Show("Please enter the value of sharpness");
                return;
            }
            setDigimagicbrightness();

        }
        private void txtslider_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = !IsTextAllowed(e.Text);
        }

        private void PastingHandler(object sender, DataObjectPastingEventArgs e)
        {
            if (e.DataObject.GetDataPresent(typeof(String)))
            {
                String text = (String)e.DataObject.GetData(typeof(String));
                if (!IsTextAllowed(text)) e.CancelCommand();
            }
            else e.CancelCommand();
        }
        /// <summary>
        /// Determines whether [is text allowed] [the specified text].
        /// </summary>
        /// <param name="text">The text.</param>
        /// <returns></returns>
        private static bool IsTextAllowed(string text)
        {
            Regex regex = new Regex("[^0-9.-]+"); //regex that matches disallowed text
            //Regex regex = new Regex("[0-9]+(\.[0-9][0-9]?)?");


            return !regex.IsMatch(text);
        }

        private void setDigimagicbrightness()
        {
            double Amount = 0;
            double configBrightness = 0;
            double configContrast = 0;
            double Sharpness = 0;
            int substoreId = LoginUser.SubStoreId;
            if (Double.TryParse(txtbright.Text, out Amount))
            {
                configBrightness = double.Parse(txtbright.Text);
            }
            else
            {

                MessageBox.Show("Please enter correct value");
                return;
            }
            if (Double.TryParse(txtcontrasts.Text, out Amount))
            {
                configContrast = double.Parse(txtcontrasts.Text);
            }
            else
            {

                MessageBox.Show("Please enter correct value");
                return;
            }
            if (Double.TryParse(txtsharp.Text, out Amount))
            {
                Sharpness = double.Parse(txtsharp.Text);
            }
            else
            {

                MessageBox.Show("Please enter correct value");
                return;
            }
            // double configBrightness = double.Parse(txtbright.Text);
            // double configContrast = double.Parse(txtcontrasts.Text);
            //double Sharpness = double.Parse(txtsharp.Text);
            //List<iMIXConfigurationValue> configEntityList = new List<iMIXConfigurationValue>();

            //iMIXConfigurationValue configEntity = new iMIXConfigurationValue();
            //configEntity.SubstoreId = LoginUser.SubStoreId;
            //configEntity.IMIXConfigurationMasterId = ConfigParams.DigimagicBrightness.ToInt32();
            //configEntity.ConfigurationValue = Convert.ToString(configBrightness);
            //configEntityList.Add(configEntity);

            //configEntity = new iMIXConfigurationValue();
            //configEntity.SubstoreId = LoginUser.SubStoreId;
            //configEntity.IMIXConfigurationMasterId = ConfigParams.DigimagicContrast.ToInt32();
            //configEntity.ConfigurationValue = Convert.ToString(configContrast);
            //configEntityList.Add(configEntity);

            //configEntity = new iMIXConfigurationValue();
            //configEntity.SubstoreId = LoginUser.SubStoreId;
            //configEntity.IMIXConfigurationMasterId = ConfigParams.DigimagicSharpen.ToInt32();
            //configEntity.ConfigurationValue = Convert.ToString(Sharpness);
            //configEntityList.Add(configEntity);

            //_objDataLayer = new DigiPhotoDataServices();

            //Boolean IsSaved1 = _objDataLayer.SaveUpdateNewConfig(configEntityList);

            List<iMIXConfigurationInfo> configEntityList = new List<iMIXConfigurationInfo>();

            iMIXConfigurationInfo configEntity = new iMIXConfigurationInfo();
            configEntity.SubstoreId = LoginUser.SubStoreId;
            configEntity.IMIXConfigurationMasterId = Convert.ToInt32(ConfigParams.DigimagicBrightness);
            configEntity.ConfigurationValue = Convert.ToString(configBrightness);
            configEntityList.Add(configEntity);

            configEntity = new iMIXConfigurationInfo();
            configEntity.SubstoreId = LoginUser.SubStoreId;
            configEntity.IMIXConfigurationMasterId = Convert.ToInt32(ConfigParams.DigimagicContrast);
            configEntity.ConfigurationValue = Convert.ToString(configContrast);
            configEntityList.Add(configEntity);

            configEntity = new iMIXConfigurationInfo();
            configEntity.SubstoreId = LoginUser.SubStoreId;
            configEntity.IMIXConfigurationMasterId = Convert.ToInt32(ConfigParams.DigimagicSharpen);
            configEntity.ConfigurationValue = Convert.ToString(Sharpness);
            configEntityList.Add(configEntity);



            ConfigBusiness conBiz = new ConfigBusiness();
            Boolean IsSaved = conBiz.SaveUpdateNewConfig(configEntityList);

            if (IsSaved)
            {
                MessageBox.Show("Digimagic effects updated successfully!", "Digiphoto", MessageBoxButton.OK, MessageBoxImage.Information);
            }
            else
            {
                MessageBox.Show("There was some error updating the data!", "Digiphoto", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void sharpVal_GotFocus(object sender, RoutedEventArgs e)
        {
            GrdSharpens.Visibility = Visibility.Visible;
            Flower.Visibility = Visibility.Visible;
        }

        private void contrastVal_GotFocus(object sender, RoutedEventArgs e)
        {
            GrdSharpens.Visibility = Visibility.Collapsed;
        }

        private void brightVal_GotFocus(object sender, RoutedEventArgs e)
        {
            GrdSharpens.Visibility = Visibility.Collapsed;

        }

        private void sharpVal_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            GrdSharpens.Visibility = Visibility.Visible;
            ShEffect _sharpeff = new ShEffect();
            double sharpen = 0;
            GrdSharpens.Visibility = Visibility.Visible;
            _sharpeff.PixelWidth = 0.0015;
            _sharpeff.PixelHeight = 0.0015;
            _sharpeff.Strength = sharpVal.Value;
            Flower.Effect = _sharpeff;
            sharpen = sharpVal.Value;
        }

        #region AudioTemplate

        private void GetAudioDetails()
        {
            try
            {
                ConfigBusiness objAudioBuss = new ConfigBusiness();
                List<AudioTemplateInfo> objAudioList = objAudioBuss.GetAudioTemplateList();
                DGManageAudio.ItemsSource = objAudioList;
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }


        /// <summary>
        /// Handles the Click event of the btnSelectAudio control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void btnSelectAudio_Click(object sender, RoutedEventArgs e)
        {
            Stream checkstream = null;
            Microsoft.Win32.OpenFileDialog ofd = new Microsoft.Win32.OpenFileDialog();
            ofd.Multiselect = false;
            ofd.Filter = "Select Audio|*.mp3;*.wav;*.wma;|" + "MP3|*.mp3|WAV|*.wav|WMA|*.wma;";
            if ((bool)ofd.ShowDialog())
            {
                try
                {
                    if ((checkstream = ofd.OpenFile()) != null)
                    {
                        if (System.IO.Path.GetFileName(ofd.FileName.ToString()).Count() <= 50)
                        {
                            txtSelectedAudio.Text = ofd.FileName.ToString();
                            audioDisplayname = System.IO.Path.GetFileName(ofd.FileName);
                        }
                        else
                        {
                            MessageBox.Show("File name should not excced 50 characters.", "DigiPhoto", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                        }
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Error: Could not read file from disk. Original error: " + ex.Message);
                }
            }

        }
        /// <summary>
        /// Handles the Click event of the btnSaveSelectAudio control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void btnSaveSelectAudio_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (CheckAudioValidations())
                {
                    var player = new WindowsMediaPlayer();
                    var clip = player.newMedia(txtSelectedAudio.Text);
                    long audioLength = string.IsNullOrEmpty(TimeSpan.FromSeconds(clip.duration).ToString()) ? 0 : (long)(TimeSpan.FromSeconds(clip.duration).TotalSeconds);
                    if (audioLength <= 1800)//30 min 
                    {
                        bool isSaved = false;
                        Guid _obj = Guid.NewGuid();
                        audioName = System.IO.Path.GetFileNameWithoutExtension(audioDisplayname) + _obj.ToString() + System.IO.Path.GetExtension(audioDisplayname);
                        string destFile = LoginUser.DigiFolderAudioPath + audioName;

                        string strDescription = txtAudioDescription.Text;
                        bool? IsActive = IsAudioActive.IsChecked;

                        if (!IsAudioEdited)
                        {

                            if (!Directory.Exists(LoginUser.DigiFolderAudioPath))
                            {
                                Directory.CreateDirectory(LoginUser.DigiFolderAudioPath);
                            }
                            System.IO.File.Copy(txtSelectedAudio.Text, destFile, true);
                            isSaved = SaveAudioDetails(audioDisplayname, audioName, strDescription, IsActive, IsAudioEdited, audioLength);
                        }
                        else
                        {
                            string strEditedFile = txtSelectedAudio.Text;
                            if (System.IO.File.Exists(strEditedFile))
                            {
                                System.IO.File.Copy(strEditedFile, destFile, true);
                                if (strEditedFile.Contains(LoginUser.DigiFolderAudioPath))
                                {
                                    System.IO.File.Delete(strEditedFile);
                                }
                                isSaved = SaveAudioDetails(audioDisplayname, audioName, strDescription, IsActive, IsAudioEdited, audioLength);
                            }
                            else
                            {
                                MessageBox.Show("File not found.", "DigiPhoto", MessageBoxButton.OK, MessageBoxImage.Information);
                            }
                            IsAudioEdited = false;
                        }
                        if (isSaved)
                        {
                            CopyToAllSubstore(lstConfigurationInfo, destFile, "", "Audio");
                            MessageBox.Show("Audio saved successfully.", "DigiPhoto", MessageBoxButton.OK, MessageBoxImage.Information);

                            AuditLog.AddUserLog(Common.LoginUser.UserId, 49, "Add/Edit Audio at ");
                            GetAudioDetails();
                        }
                        ResetAudioTemplateControls();
                    }
                    else
                    {
                        MessageBox.Show("Audio length should not exceed 30 minutes.", "DigiPhoto", MessageBoxButton.OK, MessageBoxImage.Information);
                    }
                }
                else
                {
                    MessageBox.Show("Select Audio File.", "DigiPhoto", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }

        /// <summary>
        /// Handles the Click event of the btnCancelSelectAudio control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void btnCancelSelectAudio_Click(object sender, RoutedEventArgs e)
        {
            ResetAudioTemplateControls();
        }
        /// <summary>
        /// Handles the Click event of the btnDeleteAudio control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void btnDeleteAudio_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Button btnSender = (Button)sender;
                audioId = Convert.ToInt32(btnSender.Tag);
                MessageBoxResult response = MessageBox.Show("Do you want to delete record?", "Digiphoto", MessageBoxButton.YesNo, MessageBoxImage.Question);

                if (response == MessageBoxResult.Yes)
                {
                    ConfigBusiness objAudioBuss = new ConfigBusiness();
                    AudioTemplateInfo obj = objAudioBuss.GetAudioTemplateList(audioId);
                    if (obj != null)
                    {
                        string filepath = LoginUser.DigiFolderAudioPath + obj.Name;
                        bool isDeleted = objAudioBuss.DeleteAudio(audioId);
                        if (isDeleted)
                        {
                            if (System.IO.File.Exists(filepath))
                            {
                                System.IO.File.Delete(filepath);
                            }
                            DeleteFromAllSubstore(lstConfigurationInfo, obj.Name, "Audio");
                            GetAudioDetails();
                            MessageBox.Show("Audio record deleted successfully.", "DigiPhoto", MessageBoxButton.OK, MessageBoxImage.Information);
                            AuditLog.AddUserLog(Common.LoginUser.UserId, 49, "Audio deleted at ");
                        }
                    }
                    ResetAudioTemplateControls();

                }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }
        /// <summary>
        /// Handles the Click event of the btnEditAudio control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void btnEditAudio_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Button btnSender = (Button)sender;
                audioId = Convert.ToInt32(btnSender.Tag);
                IsAudioEdited = true;
                ConfigBusiness objAudioBuss = new ConfigBusiness();
                AudioTemplateInfo obj = objAudioBuss.GetAudioTemplateList(audioId);
                audioName = obj.Name;
                txtSelectedAudio.Text = LoginUser.DigiFolderAudioPath + obj.Name;
                txtAudioDescription.Text = obj.Description;
                IsAudioActive.IsChecked = obj.IsActive;
                audioDisplayname = System.IO.Path.GetFileName(obj.DisplayName);
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }
        /// <summary>
        /// Checks the audio validations.
        /// </summary>
        /// <returns></returns>
        private bool CheckAudioValidations()
        {
            if (txtSelectedAudio.Text == "")
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        /// <summary>
        /// Function to Add/Edit audio record
        /// </summary>
        /// <param name="audioDisplayname">Audio disply name</param>
        /// <param name="audioName">Audio origional name with appended GUID </param>
        /// <param name="strDescription">input description</param>
        /// <param name="IsActive">input true/false</param>
        /// <param name="IsEdited">new record or edited(false/true)</param>
        /// <returns></returns>
        private bool SaveAudioDetails(string audioDisplayname, string audioName, string strDescription, bool? IsActive, bool IsEdited, long AudioLength)
        {
            try
            {
                IsActive = IsActive == null ? false : IsActive;
                AudioTemplateInfo objAudio = new AudioTemplateInfo();
                objAudio.DisplayName = audioDisplayname;
                objAudio.Name = audioName;
                objAudio.IsActive = Convert.ToBoolean(IsActive);
                objAudio.Description = strDescription;
                objAudio.AudioLength = AudioLength;
                if (!IsEdited)
                {
                    objAudio.CreatedBy = LoginUser.UserId;
                    objAudio.CreatedOn = System.DateTime.Now;
                    objAudio.ModifiedBy = 0;
                    objAudio.ModifiedOn = System.DateTime.Now;
                }
                else
                {
                    objAudio.CreatedBy = LoginUser.UserId;
                    objAudio.CreatedOn = System.DateTime.Now;
                    objAudio.AudioTemplateId = audioId;
                    objAudio.ModifiedBy = LoginUser.UserId;
                    objAudio.ModifiedOn = System.DateTime.Now;
                }
                ConfigBusiness obj = new ConfigBusiness();
                return obj.SaveUpdateAudioTemplate(objAudio);

            }
            catch
            {
                return false;
            }
        }
        private void ResetAudioTemplateControls()
        {
            txtSelectedAudio.Text = "";
            txtAudioDescription.Text = "";
            IsAudioActive.IsChecked = true;
            audioId = 0;
        }
        #endregion AudioTemplate

        #region Video Template

        private void GetVideoTemplateDetails()
        {
            try
            {
                ConfigBusiness configBusiness = new ConfigBusiness();
                List<VideoTemplateInfo> videoTemplateList = configBusiness.GetVideoTemplate();
                DGManageVideo.ItemsSource = videoTemplateList;

            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }

        }
        /// <summary>
        /// Handles the Click event of the btnSelectBorder control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void btnSelectVideo_Click(object sender, RoutedEventArgs e)
        {
            Stream checkstream = null;

            Microsoft.Win32.OpenFileDialog ofd = new Microsoft.Win32.OpenFileDialog();
            ofd.Multiselect = false;
            ofd.Filter = "Select Video|*.mp4;*.wmv;*.avi;*.flv;*.mpg;*.m4a;*.3gpp;*.3gpp2;*.m2v;*.mov;|" + "MP4|*.mp4|WMV|*.wmv|AVI|*.avi|FLV|*.flv|MPG|*.mpg|M4A|*.m4a|3GPP|*.3gpp|3GPP2|*.3gpp2|M2V|*.m2v|MOV|*.mov;";

            if ((bool)ofd.ShowDialog())
            {
                try
                {
                    if ((checkstream = ofd.OpenFile()) != null)
                    {
                        if (System.IO.Path.GetFileName(ofd.FileName.ToString()).ToCharArray().Count() <= 50)
                        {
                            txtSelectedVideo.Text = ofd.FileName.ToString();
                            videoDisplayname = System.IO.Path.GetFileName(ofd.FileName);
                        }
                        else
                        {
                            MessageBox.Show("File name should not excced 50 characters.", "DigiPhoto", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                        }
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Error: Could not read file from disk. Original error: " + ex.Message);
                }
            }

        }

        private void btnSaveSelectVideo_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                long videoLength = 0;
                string videoName = string.Empty;
                long videoTemplateId = 0;
                if (CheckVideoValidations())
                {
                    var player = new WindowsMediaPlayer();
                    var clip = player.newMedia(txtSelectedVideo.Text);

                    var extension = (System.IO.Path.GetExtension(txtSelectedVideo.Text)).ToLower();
                    if (extension.Equals(".flv"))
                    {
                        FlvMetaInfo metaInfo = FlvMetaDataReader.GetFlvMetaInfo(txtSelectedVideo.Text);
                        videoLength = Convert.ToInt64(metaInfo.Duration);

                    }
                    else
                    {
                        videoLength = string.IsNullOrEmpty(TimeSpan.FromSeconds(clip.duration).ToString()) ? 0 : (long)(TimeSpan.FromSeconds(clip.duration).TotalSeconds);
                    }
                    if (videoLength <= 1800)//30 min 
                    {
                        if (!string.IsNullOrEmpty(btnSaveSelectVideo.Tag.ToString()))
                            videoTemplateId = long.Parse(btnSaveSelectVideo.Tag.ToString());

                        Guid _obj = Guid.NewGuid();
                        videoName = System.IO.Path.GetFileNameWithoutExtension(videoDisplayname) + _obj.ToString() + System.IO.Path.GetExtension(videoDisplayname);
                        string destFile = LoginUser.DigiFolderVideoTemplatePath + videoName;
                        if (!Directory.Exists(LoginUser.DigiFolderVideoTemplatePath))
                        {
                            Directory.CreateDirectory(LoginUser.DigiFolderVideoTemplatePath);
                        }
                        System.IO.File.Copy(txtSelectedVideo.Text, destFile, true);
                        //Get video duration/length
                        // var player = new WindowsMediaPlayer();
                        // clip = player.newMedia(destFile);
                        //  videoLength = string.IsNullOrEmpty(TimeSpan.FromSeconds(clip.duration).ToString()) ? 0 : (long)(TimeSpan.FromSeconds(clip.duration).TotalSeconds);

                        bool isSaved = SaveVideoDetails(videoTemplateId, videoDisplayname, videoName, txtVideoDescription.Text.Trim(), IsVideoActive.IsChecked, videoLength);
                        if (isSaved)
                        {
                            CopyToAllSubstore(lstConfigurationInfo, destFile, "", "Video");
                            MessageBox.Show("Video saved successfully", "DigiPhoto", MessageBoxButton.OK, MessageBoxImage.Information);
                            AuditLog.AddUserLog(Common.LoginUser.UserId, 49, "Add/Edit Audio at ");
                            GetVideoTemplateDetails();
                            ResetVideoTemplateControls();
                        }
                    }
                    else
                    {
                        MessageBox.Show("Video length should not exceed 30 minutes.", "DigiPhoto", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    }
                }
                else
                {
                    MessageBox.Show("Select video file ", "DigiPhoto", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }
        private void btnCancelSelectVideo_Click(object sender, RoutedEventArgs e)
        {
            ResetVideoTemplateControls();
        }

        private void ResetVideoTemplateControls()
        {
            txtVideoDescription.Text = string.Empty;
            txtSelectedVideo.Text = string.Empty;
            IsVideoActive.IsChecked = true;
            btnSaveSelectVideo.Tag = string.Empty;
        }

        private void btnDeleteVideo_Click(object sender, RoutedEventArgs e)
        {
            Button btnSender = (Button)sender;
            long videoTemplateId = long.Parse(btnSender.Tag.ToString());
            MessageBoxResult response = MessageBox.Show("Do you want to delete record?", "Digiphoto", MessageBoxButton.YesNo, MessageBoxImage.Question);
            if (response == MessageBoxResult.Yes)
            {
                ConfigBusiness configBusiness = new ConfigBusiness();
                VideoTemplateInfo obj = configBusiness.GetVideoTemplate(videoTemplateId);
                string filepath = LoginUser.DigiFolderVideoTemplatePath + obj.Name;
                bool isDeleted = configBusiness.DeleteVideoTemplate(videoTemplateId);
                if (isDeleted)
                {
                    if (System.IO.File.Exists(filepath))
                    {
                        System.IO.File.Delete(filepath);
                    }
                    DeleteFromAllSubstore(lstConfigurationInfo, obj.Name, "Video");

                    GetVideoTemplateDetails();
                    MessageBox.Show("Video record deleted Successfully", "DigiPhoto", MessageBoxButton.OK, MessageBoxImage.Information);
                    AuditLog.AddUserLog(Common.LoginUser.UserId, 49, "Delete video at ");

                }
            }
            ResetVideoTemplateControls();
        }

        //  long 
        private void btnEditVideo_Click(object sender, RoutedEventArgs e)
        {
            long videoTemplateId = 0;
            Button btnSender = (Button)sender;
            videoTemplateId = long.Parse(btnSender.Tag.ToString());
            btnSaveSelectVideo.Tag = videoTemplateId;
            ConfigBusiness configBusiness = new ConfigBusiness();
            VideoTemplateInfo videoTemplate = configBusiness.GetVideoTemplate(videoTemplateId);
            if (videoTemplate != null)
            {
                //sourcepath = LoginUser.DigiFolderVideoTemplatePath + videoTemplate.Name;
                txtSelectedVideo.Text = LoginUser.DigiFolderVideoTemplatePath + videoTemplate.Name;
                txtVideoDescription.Text = videoTemplate.Description;
                IsVideoActive.IsChecked = videoTemplate.IsActive;
                videoDisplayname = System.IO.Path.GetFileName(videoTemplate.DisplayName);
            }
        }

        private void btnSlotTimeFrame_Click(object sender, RoutedEventArgs e)
        {
            
            long videoTemplateId = 0;
            Button btnSender = (Button)sender;
            videoTemplateId = long.Parse(btnSender.Tag.ToString());
            CtrlVideoSlotsTime.VideoTemplateId = videoTemplateId;
            DataGridRow row = FindParent<DataGridRow>(btnSender);
            CtrlVideoSlotsTime.VideoLength = ((DigiPhoto.IMIX.Model.VideoTemplateInfo)(row.Item)).VideoLength;
            CtrlVideoSlotsTime.SetParent(tbmain);
            CtrlVideoSlotsTime.Visibility = Visibility.Visible;
            tbmain.IsEnabled = false;
        }

        private T FindParent<T>(DependencyObject child) where T : DependencyObject
        {
            //get parent item
            DependencyObject parentObject = VisualTreeHelper.GetParent(child);    //we’ve reached the end of the tree
            if (parentObject == null) return null;
            //check if the parent matches the type we’re looking for
            T parent = parentObject as T;
            if (parent != null)
                return parent;
            else
                return FindParent<T>(parentObject);
        }

        /// <summary>
        /// Checks the border validations.
        /// </summary>
        /// <returns></returns>
        private bool CheckVideoValidations()
        {
            if (string.IsNullOrEmpty(txtSelectedVideo.Text.Trim()))
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        private bool SaveVideoDetails(long videoTemplateId, string videoDisplayname, string videoName, string strDescription, bool? IsActive, long videoLength)
        {
            try
            {
                ConfigBusiness configBusiness = new ConfigBusiness();
                VideoTemplateInfo videoTemplate = new VideoTemplateInfo();
                IsActive = IsActive == null ? false : IsActive;
                videoTemplate.DisplayName = videoDisplayname;
                videoTemplate.Name = videoName;
                videoTemplate.IsActive = Convert.ToBoolean(IsActive);
                videoTemplate.Description = strDescription;
                videoTemplate.CreatedBy = LoginUser.UserId;
                videoTemplate.CreatedOn = System.DateTime.Now;
                videoTemplate.VideoTemplateId = videoTemplateId;
                videoTemplate.ModifiedBy = LoginUser.UserId;
                videoTemplate.ModifiedOn = System.DateTime.Now;
                videoTemplate.videoSlots = null;
                videoTemplate.VideoLength = videoLength;
                return configBusiness.SaveVideoTemplate(videoTemplate);
            }
            catch
            {
                return false;
            }
        }

        #endregion Video Template

        #region Video Background

        private void GetVideoBGDetails()
        {
            try
            {
                ConfigBusiness objConfigBus = new ConfigBusiness();
                List<VideoBackgroundInfo> objVideoBGList = objConfigBus.GetVideoBackgrounds();
                DGManageVideoBG.ItemsSource = objVideoBGList;
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }

        }

        /// <summary>
        /// Handles the Click event of the btnSelectVideoBG control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void btnSelectVideoBG_Click(object sender, RoutedEventArgs e)
        {
            Stream checkstream = null;
            Microsoft.Win32.OpenFileDialog ofd = new Microsoft.Win32.OpenFileDialog();
            ofd.Multiselect = false;
            ofd.Filter = "Select Media|*.jpg;*.png;*.mp4;*.wmv;*.avi;|" + "JPG|*.jpg;*.JPG|PNG|*.png;*.PNG|WMV|*.wmv;*.WMV|MP4|*.mp4;*.MP4|AVI|*.avi;*.AVI;";
            if ((bool)ofd.ShowDialog())
            {
                try
                {
                    if ((checkstream = ofd.OpenFile()) != null)
                    {
                        if (System.IO.Path.GetFileName(ofd.FileName.ToString()).ToCharArray().Count() <= 50)
                        {
                            txtSelectedVideoBG.Text = ofd.FileName.ToString();
                            videoBGDisplayname = System.IO.Path.GetFileName(ofd.FileName);
                            if (System.IO.Path.GetExtension(ofd.FileName) == ".jpg" || System.IO.Path.GetExtension(ofd.FileName) == ".JPEG")
                            {
                                videoBGMediaType = 1;
                            }
                            else
                            {
                                videoBGMediaType = 2;
                            }
                        }
                        else
                        {
                            MessageBox.Show("File name should not excced 50 characters.", "DigiPhoto", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                        }
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Error: Could not read file from disk. Original error: " + ex.Message);
                }
            }
        }

        private void SelectVideoOrImage(TextBox txtselect, string vDisplayname, int mediaId, string selectItem)
        {
            Stream checkstream = null;
            Microsoft.Win32.OpenFileDialog ofd = new Microsoft.Win32.OpenFileDialog();
            ofd.Multiselect = false;
            ofd.Filter = "Select Media|*.jpg;*.png;*.mp4;*.wmv;*.avi;|" + "JPG|*.jpg;*.JPG|PNG|*.png;*.PNG|WMV|*.wmv;*.WMV|MP4|*.mp4;*.MP4|AVI|*.avi;*.AVI;";
            if ((bool)ofd.ShowDialog())
            {
                try
                {
                    if ((checkstream = ofd.OpenFile()) != null)
                    {
                        if (System.IO.Path.GetFileName(ofd.FileName.ToString()).ToCharArray().Count() <= 50)
                        {
                            txtselect.Text = ofd.FileName.ToString();
                            selectItem = System.IO.Path.GetFileName(ofd.FileName);
                            if (System.IO.Path.GetExtension(ofd.FileName) == ".jpg" || System.IO.Path.GetExtension(ofd.FileName) == ".JPEG" || System.IO.Path.GetExtension(ofd.FileName) == ".png" || System.IO.Path.GetExtension(ofd.FileName) == ".PNG")
                            {
                                mediaId = 1;
                            }
                            else
                            {
                                mediaId = 2;
                            }
                        }
                        else
                        {
                            MessageBox.Show("File name should not excced 50 characters.", "DigiPhoto", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                        }
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Error: Could not read file from disk. Original error: " + ex.Message);
                }
            }
        }
        /// <summary>
        /// Handles the Click event of the btnSaveVideoBG control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/>instance containing the event data.</param>
        private void btnSaveVideoBG_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                bool validationSuccess = false;
                var player = new WindowsMediaPlayer();
                if (CheckVideoBGValidations(txtSelectedVideoBG))
                {
                    if (System.IO.Path.GetExtension(videoBGDisplayname) == ".mp4" || System.IO.Path.GetExtension(videoBGDisplayname) == ".wmv" || System.IO.Path.GetExtension(videoBGDisplayname) == ".avi")
                    {
                        var clip = player.newMedia(txtSelectedVideo.Text);
                        long videoLength = string.IsNullOrEmpty(TimeSpan.FromSeconds(clip.duration).ToString()) ? 0 : (long)(TimeSpan.FromSeconds(clip.duration).TotalSeconds);
                        if (videoLength <= 1800)//30 min 
                        {
                            validationSuccess = true;
                        }
                        else
                        {
                            validationSuccess = false;
                        }
                    }
                    else
                    {
                        validationSuccess = true;
                    }
                    if (validationSuccess)
                    {
                        bool isSaved = false;
                        Guid _obj = Guid.NewGuid();
                        videoBGName = System.IO.Path.GetFileNameWithoutExtension(videoBGDisplayname) + _obj.ToString() + System.IO.Path.GetExtension(videoBGDisplayname);
                        string destFile = LoginUser.DigiFolderVideoBackGroundPath + videoBGName;
                        string strDescription = txtVideoBGDescription.Text;
                        bool? IsActive = IsVideoBGActive.IsChecked;
                        if (!IsVideoBGEdited)
                        {
                            if (!Directory.Exists(LoginUser.DigiFolderVideoBackGroundPath))
                            {
                                Directory.CreateDirectory(LoginUser.DigiFolderVideoBackGroundPath);
                            }
                            System.IO.File.Copy(txtSelectedVideoBG.Text, destFile, true);
                            isSaved = SaveVideoBGDetails(videoBGDisplayname, videoBGName, strDescription, IsActive, IsVideoBGEdited, videoBGMediaType);
                        }
                        else
                        {
                            string strSourceFile = txtSelectedVideoBG.Text;
                            if (System.IO.File.Exists(strSourceFile))
                            {
                                System.IO.File.Copy(strSourceFile, destFile, true);
                                if (strSourceFile.Contains(LoginUser.DigiFolderVideoBackGroundPath))
                                {
                                    System.IO.File.Delete(strSourceFile);
                                }
                                isSaved = SaveVideoBGDetails(videoBGDisplayname, videoBGName, strDescription, IsActive, IsVideoBGEdited, videoBGMediaType);
                            }
                            else
                            {
                                MessageBox.Show("File not found.", "DigiPhoto", MessageBoxButton.OK, MessageBoxImage.Error);
                            }
                            IsVideoBGEdited = false;
                        }
                        if (isSaved)
                        {
                            CopyToAllSubstore(lstConfigurationInfo, destFile, "", "VideoBG");
                            MessageBox.Show("Video background saved successfully.", "DigiPhoto", MessageBoxButton.OK, MessageBoxImage.Information);
                            AuditLog.AddUserLog(Common.LoginUser.UserId, 49, "Add/Edit Video Background at ");
                            GetVideoBGDetails();
                        }
                        ResetVideoBGControls();
                    }
                    else
                    {
                        MessageBox.Show("Video background length should not exceed 30 minutes.", "DigiPhoto", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    }
                }
                else
                {
                    MessageBox.Show("Select video background file.", "DigiPhoto", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }

        /// <summary>
        /// Handles the Click event of the btnCancelVideoBG control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void btnCancelVideoBG_Click(object sender, RoutedEventArgs e)
        {
            ResetVideoBGControls();
        }
        /// <summary>
        /// Handles the Click event of the btnDeleteVideoBG control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void btnDeleteVideoBG_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Button btnSender = (Button)sender;
                videoBackgroundId = Convert.ToInt32(btnSender.Tag);
                MessageBoxResult response = MessageBox.Show("Do you want to delete record?", "Digiphoto", MessageBoxButton.YesNo, MessageBoxImage.Question);

                if (response == MessageBoxResult.Yes)
                {
                    ConfigBusiness objConfig = new ConfigBusiness();
                    VideoBackgroundInfo obj = objConfig.GetVideoBackgrounds(videoBackgroundId);
                    if (obj != null)
                    {
                        string filepath = LoginUser.DigiFolderVideoBackGroundPath + obj.Name;
                        bool isDeleted = objConfig.DeleteVideoBackground(videoBackgroundId);
                        if (isDeleted)
                        {
                            if (System.IO.File.Exists(filepath))
                            {
                                System.IO.File.Delete(filepath);
                            }
                            DeleteFromAllSubstore(lstConfigurationInfo, obj.Name, "VideoBG");
                            GetVideoBGDetails();
                            MessageBox.Show("Video background record deleted successfully.", "DigiPhoto", MessageBoxButton.OK, MessageBoxImage.Information);
                            AuditLog.AddUserLog(Common.LoginUser.UserId, 49, "Video background deleted at ");
                        }
                    }
                    ResetVideoBGControls();

                }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }
        /// <summary>
        /// Handles the Click event of the btnEditVideoBG control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void btnEditVideoBG_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Button btnSender = (Button)sender;
                videoBackgroundId = Convert.ToInt32(btnSender.Tag);
                IsVideoBGEdited = true;
                ConfigBusiness objConfigBus = new ConfigBusiness();
                VideoBackgroundInfo obj = objConfigBus.GetVideoBackgrounds(videoBackgroundId);
                videoBGName = obj.Name;
                txtSelectedVideoBG.Text = LoginUser.DigiFolderVideoBackGroundPath + obj.Name;
                txtVideoBGDescription.Text = obj.Description;
                IsVideoBGActive.IsChecked = obj.IsActive;
                videoBGDisplayname = System.IO.Path.GetFileName(obj.DisplayName);
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }
        /// <summary>
        /// Checks the video background validations.
        /// </summary>
        /// <returns></returns>
        private bool CheckVideoBGValidations(TextBox txtSelected)
        {
            if (txtSelected.Text == "")
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        /// <summary>
        /// Function to Add/Edit video background record
        /// </summary>
        /// <param name="videoBGDisplayName">video background disply name</param>
        /// <param name="videoBGName">video background origional name with appended GUID </param>
        /// <param name="strDescription">input description</param>
        /// <param name="IsActive">input true/false</param>
        /// <param name="IsEdited">new record or edited(false/true)</param>
        /// <returns></returns>
        private bool SaveVideoBGDetails(string videoBGDisplayName, string videoBGName, string strDescription, bool? IsActive, bool IsEdited, int videoBGMediaType)
        {
            try
            {
                IsActive = IsActive == null ? false : IsActive;
                VideoBackgroundInfo objVBG = new VideoBackgroundInfo();
                objVBG.DisplayName = videoBGDisplayName;
                objVBG.Name = videoBGName;
                objVBG.IsActive = Convert.ToBoolean(IsActive);
                objVBG.MediaType = videoBGMediaType;
                objVBG.Description = strDescription;
                if (!IsEdited)
                {
                    objVBG.CreatedBy = LoginUser.UserId;
                    objVBG.CreatedOn = System.DateTime.Now;
                    objVBG.ModifiedBy = 0;
                    objVBG.ModifiedOn = System.DateTime.Now;
                }
                else
                {
                    objVBG.CreatedBy = LoginUser.UserId;
                    objVBG.CreatedOn = System.DateTime.Now;
                    objVBG.VideoBackgroundId = videoBackgroundId;
                    objVBG.ModifiedBy = LoginUser.UserId;
                    objVBG.ModifiedOn = System.DateTime.Now;
                }
                ConfigBusiness obj = new ConfigBusiness();
                int id = obj.SaveUpdateVideoBackground(objVBG);
                if (id > 0)
                    return true;
                else return false;

            }
            catch
            {
                return false;
            }
        }

        private void ResetVideoBGControls()
        {
            txtVideoBGDescription.Text = string.Empty;
            txtSelectedVideoBG.Text = string.Empty;
            IsVideoBGActive.IsChecked = true;
            videoBackgroundId = 0;
        }
        #endregion Video Background

        #region Online Tab Code
        private void chkIsBarcodeActive_Checked(object sender, RoutedEventArgs e)
        {
            //FillOnlineTabInfo();
            cmbMappingType.Visibility = System.Windows.Visibility.Visible;
            cmbScanType.Visibility = System.Windows.Visibility.Visible;
            txtblMappingType.Visibility = System.Windows.Visibility.Visible;
            txtblScanType.Visibility = System.Windows.Visibility.Visible;
            spLostImgTimeGap.Visibility = System.Windows.Visibility.Visible;
        }
        private void chkIsOnline_Checked(object sender, RoutedEventArgs e)
        {
            DgService.Visibility = System.Windows.Visibility.Visible;
        }

        private void chkIsOnline_UnChecked(object sender, RoutedEventArgs e)
        {
            DgService.Visibility = System.Windows.Visibility.Collapsed;
        }
        private void chkIsBarcodeActive_Unchecked(object sender, RoutedEventArgs e)
        {
            //cmbMappingType.ItemsSource = null;
            cmbMappingType.Visibility = System.Windows.Visibility.Collapsed;
            cmbScanType.Visibility = System.Windows.Visibility.Collapsed;
            txtblMappingType.Visibility = System.Windows.Visibility.Collapsed;
            txtblScanType.Visibility = System.Windows.Visibility.Collapsed;
            spLostImgTimeGap.Visibility = System.Windows.Visibility.Collapsed;
        }

        private void btnSaveOnlineConfig_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                string sPattern1 = "^[1-9]{1}[0-9]{0,4}$";
                string data = txtLostImgTimeGap.Text.Trim();
                data = data.TrimStart('0');
                string numericValidation = "^[0-9]*$";
                if (chkIsBarcodeActive.IsChecked == true && cmbMappingType.SelectedValue.ToString() == "400")
                {
                    MessageBox.Show("Please select Mapping Type");
                    return;
                }
                if (chkIsBarcodeActive.IsChecked == true && cmbScanType.SelectedValue.ToString() == "500")
                {
                    MessageBox.Show("Please Select Scan Type");
                    return;
                }
                if (chkIsBarcodeActive.IsChecked == true && (!System.Text.RegularExpressions.Regex.IsMatch(data, sPattern1)))
                {
                    MessageBox.Show("Please select Lost Image Time Gap( integer value between 1-99999)");
                    return;
                }
                if (chkIsOnline.IsChecked == true && string.IsNullOrWhiteSpace(txtDgServiceUrl.Text))
                {
                    MessageBox.Show("Please enter service URL.");
                    return;
                }
                if (string.IsNullOrWhiteSpace(txtQRCodeLen.Text) && string.IsNullOrEmpty(txtQRCodeLen.Text))
                {
                    MessageBox.Show("Please Enter QR Code Length.");
                    return;
                }
                if (!System.Text.RegularExpressions.Regex.IsMatch(txtQRCodeLen.Text.ToString().Trim(), numericValidation))
                {
                    MessageBox.Show("Please Enter Valid QRCode Length.");
                    return;
                }
                if (txtQRCodeLen.Text.ToString().Length > 2)
                {
                    MessageBox.Show("Please Enter Valid QRCode Length.");
                    return;
                }

                if ((Convert.ToInt16(txtQRCodeLen.Text) > 30) || (Convert.ToInt16(txtQRCodeLen.Text) < 5))
                {
                    MessageBox.Show("Please Enter QRCode between 5-30.");
                    return;
                }
                if ((chkIsBarcodeActive.IsChecked == true && !(cmbMappingType.SelectedValue.ToString() == "400") && !(cmbScanType.SelectedValue.ToString() == "500") && System.Text.RegularExpressions.Regex.IsMatch(data, sPattern1)) || chkIsBarcodeActive.IsChecked == false)
                {
                    //List<iMIXConfigurationValue> configEntityList = new List<iMIXConfigurationValue>();

                    //iMIXConfigurationValue configEntity = new iMIXConfigurationValue();

                    List<iMIXConfigurationInfo> configEntityList = new List<iMIXConfigurationInfo>();
                    iMIXConfigurationInfo configEntity = new iMIXConfigurationInfo();

                    configEntity.SubstoreId = LoginUser.SubStoreId;
                    configEntity.IMIXConfigurationMasterId = Convert.ToInt32(ConfigParams.IsBarcodeActive);
                    configEntity.ConfigurationValue = Convert.ToString(chkIsBarcodeActive.IsChecked);
                    configEntityList.Add(configEntity);

                    # region Image Resize Settings start
                    if (!string.IsNullOrEmpty(txtOnlineResize.Text.Trim()) && !string.IsNullOrEmpty(txtSMResize.Text.Trim()))
                    {

                        if (ValidateImageResizePercent(txtOnlineResize.Text.Trim()) && ValidateImageResizePercent(txtSMResize.Text.Trim()))
                        {
                            configEntity = new iMIXConfigurationInfo();
                            configEntity.SubstoreId = LoginUser.SubStoreId;
                            configEntity.IMIXConfigurationMasterId = Convert.ToInt32(ConfigParams.OnlineImageResize); //For QR Code & Social Media
                            configEntity.ConfigurationValue = txtOnlineResize.Text.Trim();
                            configEntityList.Add(configEntity);

                            configEntity = new iMIXConfigurationInfo();
                            configEntity.SubstoreId = LoginUser.SubStoreId;
                            configEntity.IMIXConfigurationMasterId = Convert.ToInt32(ConfigParams.StorageMediaImageResize); //For Compact Disc & USB(Pen Drive) Media
                            configEntity.ConfigurationValue = txtSMResize.Text.Trim();
                            configEntityList.Add(configEntity);
                        }
                        else
                        {
                            MessageBox.Show("Resize value should be between 1 to 100", "iMIX", MessageBoxButton.OK, MessageBoxImage.Error);
                            return;
                        }
                    }
                    else
                    {
                        MessageBox.Show("Please provide a valid resize value", "iMIX", MessageBoxButton.OK, MessageBoxImage.Error);
                        return;
                    }
                    #endregion

                    # region Image Association Settings
                    if (chkIsBarcodeActive.IsChecked == true)
                    {
                        configEntity = new iMIXConfigurationInfo();
                        configEntity.SubstoreId = LoginUser.SubStoreId;
                        configEntity.IMIXConfigurationMasterId = Convert.ToInt32(ConfigParams.DefaultMappingCode);
                        configEntity.ConfigurationValue = Convert.ToString(cmbMappingType.SelectedValue);
                        configEntityList.Add(configEntity);
                        configEntity = new iMIXConfigurationInfo();
                        configEntity.SubstoreId = LoginUser.SubStoreId;
                        configEntity.IMIXConfigurationMasterId = Convert.ToInt32(ConfigParams.ScanType);
                        configEntity.ConfigurationValue = Convert.ToString(cmbScanType.SelectedValue);
                        configEntityList.Add(configEntity);
                        configEntity = new iMIXConfigurationInfo();
                        configEntity.SubstoreId = LoginUser.SubStoreId;
                        configEntity.IMIXConfigurationMasterId = Convert.ToInt32(ConfigParams.LostImgTimeGap);
                        configEntity.ConfigurationValue = data;
                        configEntityList.Add(configEntity);
                    }
                    else
                    {
                        configEntity = new iMIXConfigurationInfo();
                        configEntity.SubstoreId = LoginUser.SubStoreId;
                        configEntity.IMIXConfigurationMasterId = Convert.ToInt32(ConfigParams.DefaultMappingCode);
                        configEntity.ConfigurationValue = "0";
                        configEntityList.Add(configEntity);
                        configEntity = new iMIXConfigurationInfo();
                        configEntity.SubstoreId = LoginUser.SubStoreId;
                        configEntity.IMIXConfigurationMasterId = Convert.ToInt32(ConfigParams.ScanType);
                        configEntity.ConfigurationValue = "0";
                        configEntityList.Add(configEntity);
                        configEntity = new iMIXConfigurationInfo();
                        configEntity.SubstoreId = LoginUser.SubStoreId;
                        configEntity.IMIXConfigurationMasterId = Convert.ToInt32(ConfigParams.LostImgTimeGap);
                        configEntity.ConfigurationValue = "0";
                        configEntityList.Add(configEntity);
                    }
                    #endregion

                    #region online
                    //configEntity = new iMIXConfigurationValue();
                    //configEntity.SubstoreId = LoginUser.SubStoreId;
                    //configEntity.IMIXConfigurationMasterId = Convert.ToInt32(ConfigParams.Online);
                    //configEntity.ConfigurationValue = chkIsOnline.IsChecked == true ? "True" : "False";
                    //configEntityList.Add(configEntity);
                    bool isOnline = chkIsOnline.IsChecked.HasValue ? chkIsOnline.IsChecked.Value : false;
                    SaveSystemOnlineStatus(isOnline);

                    configEntity = new iMIXConfigurationInfo();
                    configEntity.SubstoreId = LoginUser.SubStoreId;
                    #region IsOnline check saved in DB- Ashirwad
                    configEntity.IMIXConfigurationMasterId = Convert.ToInt32(ConfigParams.IsSyncedEnabled);
                    configEntity.ConfigurationValue = Convert.ToString(isOnline);
                    configEntityList.Add(configEntity);

                    configEntity = new iMIXConfigurationInfo();
                    configEntity.SubstoreId = LoginUser.SubStoreId;
                    #endregion
                    configEntity.IMIXConfigurationMasterId = Convert.ToInt32(ConfigParams.DgServiceURL);
                    configEntity.ConfigurationValue = txtDgServiceUrl.Text;
                    configEntityList.Add(configEntity);

                    #endregion
                    #region AnonymousQrCode
                    configEntity = new iMIXConfigurationInfo();
                    configEntity.SubstoreId = LoginUser.SubStoreId;
                    configEntity.IMIXConfigurationMasterId = Convert.ToInt32(ConfigParams.IsAnonymousQrCodeEnabled);
                    configEntity.ConfigurationValue = chkAnonymousQrCode.IsChecked.ToString();
                    configEntityList.Add(configEntity);
                    #endregion

                    //#region IsPrepaidAutoPurchaseActive
                    //configEntity = new iMIXConfigurationInfo();
                    //configEntity.SubstoreId = LoginUser.SubStoreId;
                    //configEntity.IMIXConfigurationMasterId = Convert.ToInt32(ConfigParams.IsPrepaidAutoPurchaseActive);
                    //configEntity.ConfigurationValue = chkIsAutoPurchaseActive.IsChecked.ToString();
                    //configEntityList.Add(configEntity);
                    //#endregion

                    #region QRCode length Settings
                    configEntity = new iMIXConfigurationInfo();
                    configEntity.SubstoreId = LoginUser.SubStoreId;
                    configEntity.IMIXConfigurationMasterId = Convert.ToInt32(ConfigParams.QRCodeLengthSetting);
                    configEntity.ConfigurationValue = txtQRCodeLen.Text.ToString();
                    configEntityList.Add(configEntity);

                    #endregion

                    //_objDataLayer = new DigiPhotoDataServices();
                    //_objDataLayer.SaveUpdateNewConfig(configEntityList);
                    //save for all sub stores Sync enhancement
                    (new ConfigBusiness()).SaveUpdateNewConfigForSync(configEntityList);
                    StoreSubStoreDataBusniess stoBiz = new StoreSubStoreDataBusniess();
                    //bool x = _objDataLayer.UpdateQRCodeWebUrl(txtQRCodeWebUrl.Text.Trim());
                    bool x = stoBiz.UpdateQRCodeWebUrl(txtQRCodeWebUrl.Text.Trim());
                    if (x)
                        MessageBox.Show("All details saved successfully");
                    //Update status properties
                    Login objLogin = new Login();
                    objLogin.SetAnonymousQrCodeEnableStatus(LoginUser.SubStoreId);

                    try
                    {
                        Ping myPing = new Ping();
                        String host = "google.com";
                        byte[] buffer = new byte[32];
                        int timeout = 1000;
                        PingOptions pingOptions = new PingOptions();
                        PingReply reply = myPing.Send(host, timeout, buffer, pingOptions);
                    }
                    catch (Exception ex)
                    {
                        SaveSystemOnlineStatus(false);
                    }
                }
            }

            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }

        private bool ReadSystemOnlineStatus()
        {
            bool status = false;
            try
            {
                string pathtosave = System.IO.Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
                if (File.Exists(pathtosave + "\\Config.dat"))
                {
                    string OnlineStatus;
                    using (StreamReader reader = new StreamReader(pathtosave + "\\Config.dat"))
                    {
                        OnlineStatus = reader.ReadLine();
                        status = OnlineStatus.Split(',')[1].ToBoolean();
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
            return status;
        }
        private void SaveSystemOnlineStatus(bool onlineStatus)
        {
            try
            {
                string pathtosave = System.IO.Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
                if (File.Exists(pathtosave + "\\Config.dat"))
                {
                    File.Delete(pathtosave + "\\Config.dat");
                }
                using (StreamWriter b = new StreamWriter(File.Open(pathtosave + "\\Config.dat", FileMode.Create)))
                {
                    string machineStatus = "";
                    machineStatus += "," + onlineStatus.ToString();
                    b.Write("IsMachineOnline" + machineStatus);
                    b.Close();
                }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                MessageBox.Show(ex.Message);
            }
        }


        /// <summary>
        /// For validating image resize.
        /// </summary>
        /// <param name="val">Value should be between 0 - 100</param>
        /// <returns></returns>
        private bool ValidateImageResizePercent(string val)
        {
            bool result = false;
            try
            {
                int resize = val.ToInt32();
                if (resize != null)
                {
                    if (resize > 0 && resize <= 100)
                    {
                        result = true;
                    }
                    else
                    {
                        result = false;
                    }
                }
            }
            catch
            {
                result = false;
            }
            return result;
        }

        private void FillOnlineTabInfo()
        {
            txtQRCodeWebUrl.Text = (new StoreSubStoreDataBusniess()).GetQRCodeWebUrl();

            var typeList = (new CardBusiness()).GetCardTypes();
            CommonUtility.BindComboWithSelect(cmbMappingType, typeList, 400, ClientConstant.SelectString);

            var ScanTypeList = (new ValueTypeBusiness()).GetScanTypes();
            CommonUtility.BindComboWithSelect(cmbScanType, ScanTypeList, 500, ClientConstant.SelectString);

            //Show default values
            cmbMappingType.SelectedValue = "400";
            cmbScanType.SelectedValue = "500";
            txtOnlineResize.Text = "70";
            txtSMResize.Text = "70";
            chkIsOnline.IsChecked = ReadSystemOnlineStatus();
        }
        #endregion

        private void chkIsAutoPurchaseActive_Checked(object sender, RoutedEventArgs e)
        {
            chkAnonymousQrCode.IsChecked = false;
        }

        /// <summary>
        /// commented by latika
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        //private void chkAnonymousQrCode_Checked(object sender, RoutedEventArgs e)
        //{
        //    chkIsAutoPurchaseActive.IsChecked = false;
        //}

        private void btnSaveVenueChanges_Click(object sender, RoutedEventArgs e)
        {
            StoreInfo store = new StoreInfo();
            long minSeqNo = 0;
            long maxSeqNo = 0;

            if (string.IsNullOrEmpty(txtBillReceiptTitle.Text.Trim()))
            {
                MessageBox.Show("Please enter receipt titile");
                return;
            }

            if ((bool)chkSequenceNoReqd.IsChecked)
            {
                if (string.IsNullOrEmpty(txtMinSeqNo.Text.Trim()) || string.IsNullOrEmpty(txtMaxSeq.Text.Trim()))
                {
                    MessageBox.Show("Please enter min and max invoice number");
                    return;
                }
                else if (!Int64.TryParse(txtMinSeqNo.Text, out minSeqNo) || !Int64.TryParse(txtMaxSeq.Text, out maxSeqNo))
                {
                    minSeqNo = 0;
                    maxSeqNo = 0;
                    MessageBox.Show("Invoice number should be numeric");
                    return;
                }
                if (txtMinSeqNo.Text.Trim().ToInt64() >= txtMaxSeq.Text.Trim().ToInt64())
                {
                    MessageBox.Show("Max invoice number should be greater than min invoice number");
                    return;
                }
            }
            store.IsTaxEnabled = (bool)chkTaxEnabled.IsChecked;
            store.RunVideoProcessingEngineLocationWise = (bool)chkRunvideoProcessingEngineLocationWise.IsChecked;
            store.BillReceiptTitle = txtBillReceiptTitle.Text;
            store.Address = txtAddress.Text;
            store.PhoneNo = txtPhoneNo.Text;
            store.TaxRegistrationNumber = txtTaxRegistrationNo.Text;
            store.TaxRegistrationText = txtTaxRegistrationText.Text;
            store.IsSequenceNoRequired = chkSequenceNoReqd.IsChecked;
            store.TaxMinSequenceNo = minSeqNo;
            store.TaxMaxSequenceNo = maxSeqNo;
            store.EmailID = txtEmail.Text;
            store.WebsiteURL = txtWebsiteURL.Text;
            store.ServerHotFolderPath = txtServerHotFolder.Text;
            store.IsActiveStockShot = (bool)chkStockShot.IsChecked;
            store.RunImageProcessingEngineLocationWise = (bool)chkImageProssingEngineLocWise.IsChecked;
            store.RunManualDownloadLocationWise = (bool)chkManualDownloadLocWise.IsChecked;
            store.IsTaxIncluded = (bool)chkIsTaxIncluded.IsChecked;
            store.PlaceSpecOrderAcrossSites = (bool)chkSpecPrintOrderAcrossSites.IsChecked;
            bool result = UpdateReceiptData(store);
            if (result)
            {
                MessageBox.Show("Venue configuration saved successfully");
            }
            getTaxConfigData();
        }
        private bool UpdateReceiptData(StoreInfo store)
        {
            taxBusiness = new TaxBusiness();
            bool result = taxBusiness.UpdateStoreTaxData(store);
            return result;
        }
        private void getTaxConfigData()
        {
            StoreInfo store = new StoreInfo();
            taxBusiness = new TaxBusiness();
            store = taxBusiness.getTaxConfigData();
            txtBillReceiptTitle.Text = store.BillReceiptTitle;
            txtAddress.Text = store.Address;
            txtPhoneNo.Text = store.PhoneNo;
            txtTaxRegistrationNo.Text = store.TaxRegistrationNumber;
            txtTaxRegistrationText.Text = store.TaxRegistrationText;
            chkTaxEnabled.IsChecked = store.IsTaxEnabled;
            chkSequenceNoReqd.IsChecked = store.IsSequenceNoRequired;
            txtMaxSeq.Text = store.TaxMaxSequenceNo.ToString();
            txtMinSeqNo.Text = store.TaxMinSequenceNo.ToString();
            txtWebsiteURL.Text = store.WebsiteURL;
            txtEmail.Text = store.EmailID;
            txtServerHotFolder.Text = store.ServerHotFolderPath;
            chkStockShot.IsChecked = store.IsActiveStockShot;
            chkImageProssingEngineLocWise.IsChecked = store.RunImageProcessingEngineLocationWise;
            chkManualDownloadLocWise.IsChecked = store.RunManualDownloadLocationWise;
            chkRunvideoProcessingEngineLocationWise.IsChecked = store.RunVideoProcessingEngineLocationWise;
            chkIsTaxIncluded.IsChecked = store.IsTaxIncluded;
            chkSpecPrintOrderAcrossSites.IsChecked = store.PlaceSpecOrderAcrossSites == null ? false : store.PlaceSpecOrderAcrossSites;
            if (chkTaxEnabled.IsChecked.Equals(true))
            {
                chkIsTaxIncluded.IsEnabled = true;
            }
            else
            {
                chkIsTaxIncluded.IsEnabled = false;
                chkIsTaxIncluded.IsChecked = false;
            }
        }

        private void chkSequenceNoReqd_Click(object sender, RoutedEventArgs e)
        {
            //if ((bool)chkSequenceNoReqd.IsChecked)
            //    stkSeqNumber.Visibility = Visibility.Visible;
            //else
            //    stkSeqNumber.Visibility = Visibility.Collapsed;
        }



        #region Scene

        private void FillSceneProductCombo()
        {
            try
            {
                var productTypes = (new ProductBusiness()).GetProductType();
                //var primaryProduct = productTypes.Where(t => t.DG_IsPrimary == true && (t.DG_Orders_ProductType_pkey == 1 || t.DG_Orders_ProductType_pkey == 2)).ToList();
                CommonUtility.BindComboWithSelect<ProductTypeInfo>(CmbSceneProductType, productTypes, "DG_Orders_ProductType_Name", "DG_Orders_ProductType_pkey", 0, ClientConstant.SelectString);
                CmbSceneProductType.SelectedIndex = 0;
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }

        //private void GetGraphicsDetailsScene()
        //{

        //    var objGraphicsDetails = (new GraphicsBusiness()).GetGraphicsDetails();
        //    try
        //    {
        //        foreach (var item in objGraphicsDetails)
        //        {
        //            AllGraphics _objnew = new AllGraphics();
        //            _objnew.GraphicsName1 = item.DG_Graphics_Name;
        //            _objnew.GraphicsDisplayName1 = item.DG_Graphics_Displayname;
        //            _objnew.IsActive1 = (bool)item.DG_Graphics_IsActive;
        //            // _objnew.Filepath1 = LoginUser.DigiFolderGraphicsPath + "\\" + item.DG_Graphics_Name;
        //            _objnew.Filepath1 = System.IO.Path.Combine(LoginUser.DigiFolderGraphicsPath, item.DG_Graphics_Name);
        //            // "D:\\DigiImages\\Graphics" + "\\" + item.DG_Graphics_Name;
        //            //                string backgroundThumbnailPath = System.IO.Path.Combine(LoginUser.DigiFolderBackGroundPath, "Thumbnails", bgvalue);
        //            _objnew.Pkey1 = item.DG_Graphics_pkey;
        //            //_objnew.FilepathCombine1 = _objnew.Filepath1 + _objnew.GraphicsName1;
        //            _objgraphicsdetailScene.Add(_objnew);
        //        }

        //        //CmbGraphicsType.ItemsSource = _objgraphicsdetailScene;
        //        //CommonUtility.BindComboWithSelect<AllGraphics>(CmbGraphicsType, _objgraphicsdetailScene, "GraphicsDisplayName1", "Pkey1", 0, ClientConstant.SelectString);
        //        CommonUtility.BindCombo<AllGraphics>(CmbGraphicsType, _objgraphicsdetailScene, "GraphicsDisplayName1", "Pkey1");


        //    }
        //    catch (Exception ex)
        //    {
        //        ErrorHandler.ErrorHandler.LogError(ex);
        //    }

        //}

        private void GetBorderDetailsScene()
        {
            try
            {
                ICacheRepository imixBorderCache = DataCacheFactory.GetFactory<ICacheRepository>(typeof(BorderCaches).FullName);
                var dataBorder = (List<BorderInfo>)imixBorderCache.GetData();

                if (dataBorder == null)
                {
                    var lstBorderListSceneFetched = (new BorderBusiness()).GetBorderDetails();
                    lstBorderListScene = lstBorderListSceneFetched;
                    lstBorderListScene = lstBorderListScene.Where(o => o.DG_IsActive == true).ToList();
                }
                else
                {
                    lstBorderListScene = dataBorder.Where(o => o.DG_IsActive == true).ToList();
                }

                lstBorderListScene.ForEach(x => x.FilePath = System.IO.Path.Combine(LoginUser.DigiFolderFramePath, x.DG_Border));
                CommonUtility.BindComboWithSelect<BorderInfo>(CmbBorderType, lstBorderListScene, "DG_Border", "DG_Borders_pkey", 0, ClientConstant.SelectString);
                CmbBorderType.SelectedIndex = 0;
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }
        //private void CmbGraphicsType_SelectionChanged(object sender, SelectionChangedEventArgs e)
        //{

        //    if (CmbGraphicsType.SelectedValue != null && Convert.ToInt16(CmbGraphicsType.SelectedValue) > 0)
        //    {
        //        Int32 selectedval = CmbGraphicsType.SelectedValue.ToInt32();
        //        var imageName1 = _objgraphicsdetailScene.AsQueryable().Where(x => x.Pkey1 == selectedval).SingleOrDefault().Filepath1;
        //        if (System.IO.File.Exists(imageName1))
        //            ImgGraphics.Source = new BitmapImage(new Uri(imageName1, UriKind.Absolute));// GetImage(imageName1);
        //    }
        //}
        private void CmbBorderType_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (CmbBorderType.SelectedValue != null && Convert.ToInt16(CmbBorderType.SelectedValue) > 0)
            {
                ImgBorder.Source = null;
                Int32 selectedval = CmbBorderType.SelectedValue.ToInt32();

                var imageName1 = lstBorderListScene.AsQueryable().Where(x => x.DG_Borders_pkey == selectedval).SingleOrDefault().FilePath;
                if (System.IO.File.Exists(imageName1))
                    ImgBorder.Source = new BitmapImage(new Uri(imageName1, UriKind.Absolute));// GetImage(imageName1);
            }
        }

        private void CmbBackgroundType_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (CmbBackgroundType.SelectedValue != null && Convert.ToInt16(CmbBackgroundType.SelectedValue) > 0)
            {
                Int32 selectedval = CmbBackgroundType.SelectedValue.ToInt32();
                var imageName1 = lstmainBGListScene.AsQueryable().Where(x => x.BgID == selectedval).SingleOrDefault().Filepath;
                if (System.IO.File.Exists(imageName1))
                    ImgBacground.Source = new BitmapImage(new Uri(imageName1, UriKind.Absolute)); //GetImage(imageName1);
            }

        }

        private void GetBackGroundDetailsScene()
        {
            try
            {
                BGMaster _objnew;

                ICacheRepository imixBackGroundCache = DataCacheFactory.GetFactory<ICacheRepository>(typeof(BackgroundCache).FullName);
                var dataBackground = (List<BackGroundInfo>)imixBackGroundCache.GetData();
                if (dataBackground == null)
                {

                    var backgroundDetails = (new BackgroundBusiness()).GetBackgoundDetails();
                    foreach (var item in backgroundDetails)
                    {
                        if (item.DG_Background_IsActive.HasValue && item.DG_Background_IsActive.Value)
                        {
                            _objnew = new BGMaster();
                            _objnew.BgDisplayName = item.DG_BackGround_Image_Display_Name;
                            _objnew.BgID = item.DG_Background_pkey;
                            _objnew.BgName = item.DG_BackGround_Image_Name;
                            _objnew.Filepath = System.IO.Path.Combine(LoginUser.DigiFolderBackGroundPath, "Thumbnails", item.DG_BackGround_Image_Name);// "D:\\DigiImages\\BG\\" + item.DG_BackGround_Image_Name;
                            lstmainBGListScene.Add(_objnew);
                        }
                    }
                }
                else
                {
                    foreach (var item in dataBackground)
                    {
                        if (item.DG_Background_IsActive.HasValue && item.DG_Background_IsActive.Value)
                        {
                            _objnew = new BGMaster();
                            _objnew.BgDisplayName = item.DG_BackGround_Image_Display_Name;
                            _objnew.BgID = item.DG_Background_pkey;
                            _objnew.BgName = item.DG_BackGround_Image_Name;
                            _objnew.Filepath = System.IO.Path.Combine(LoginUser.DigiFolderBackGroundPath, "Thumbnails", item.DG_BackGround_Image_Name);// "D:\\DigiImages\\BG\\" + item.DG_BackGround_Image_Name;
                            lstmainBGListScene.Add(_objnew);
                        }
                    }
                }
                CommonUtility.BindComboWithSelect<BGMaster>(CmbBackgroundType, lstmainBGListScene, "BgDisplayName", "BgID", 0, ClientConstant.SelectString);
                CmbBackgroundType.SelectedIndex = 0;
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }

        private static BitmapImage GetImage(string imageUri)
        {
            var tunedImageUrl = imageUri.Replace(" ", "");
            var bitmapImage = new BitmapImage();
            bitmapImage.BeginInit();
            bitmapImage.UriSource = new Uri("pack://siteoforigin:,,,/" + imageUri, UriKind.RelativeOrAbsolute);
            bitmapImage.EndInit();
            return bitmapImage;
        }

        private void CmbSceneProductType_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (CmbSceneProductType.SelectedValue != null && Convert.ToInt16(CmbSceneProductType.SelectedValue) > 0)
            {
                Int32 selectedval = CmbSceneProductType.SelectedValue.ToInt32();
            }
        }

        private bool CheckSceneValidations()
        {
            if (string.IsNullOrEmpty(txtSceneName.Text))
            {
                MessageBox.Show("Please Enter Scene Name");
                return false;
            }
            if (CmbSceneProductType.SelectedValue.ToString() == "0")
            {
                MessageBox.Show("Please Select Product Type");
                return false;
            }

            if (CmbBackgroundType.SelectedValue.ToString() == "0" &&
                CmbBorderType.SelectedValue.ToString() == "0"
              )
            {
                MessageBox.Show("Please Select Either Background or Border");
                return false;
            }
            else
            {
                return true;
            }
        }

        private void btnCancelSelectScene_Click(object sender, RoutedEventArgs e)
        {
            CmbSceneProductType.SelectedValue = "0";
            CmbBackgroundType.SelectedValue = "0";
            CmbBorderType.SelectedValue = "0";
            IsSceneActive.IsChecked = true;
            IsBlankPage.IsChecked = false;
            ImgBorder.Source = null;
            CmbThemeType.SelectedValue = "0";
            CmbpageNo.SelectedIndex = 0;
            txtChineseFont.Text = string.Empty;
            txtEnglishFont.Text = string.Empty;
            ImgBacground.Source = null;
            txtSceneName.Text = string.Empty;
            sceneId = 0;
        }

        private void btnEditScene_Click(object sender, RoutedEventArgs e)
        {
            isEditScene = true;
            try
            {
                Button _objbtn = new Button();
                _objbtn = (Button)(sender);
                sceneId = _objbtn.Tag.ToInt32();
                isEditScene = true;
                IsSceneActive.IsChecked = false;
                IsBlankPage.IsChecked = false;
                Button btnSender = (Button)sender;
                var sceneList = (new SceneBusiness()).GetSceneNameFromID((int)btnSender.Tag);
                CmbSceneProductType.SelectedValue = sceneList.ProductId;
                CmbBackgroundType.SelectedValue = sceneList.BackGroundId;
                CmbBorderType.SelectedValue = sceneList.BorderId;
                CmbThemeType.SelectedValue = sceneList.ThemeId;  // Theme Type DropDown Added by Ajinkya
                CmbpageNo.SelectedIndex = Convert.ToInt32(sceneList.PageNo); // PageNo DropDown Added by Ajinkya           


                txtSceneName.Text = sceneList.SceneName;
                txtChineseFont.Text = sceneList.ChineseFont;
                txtEnglishFont.Text = sceneList.EnglishFont;
                // var graphicsPath = _objgraphicsdetailScene.AsQueryable().Where(x => x.Pkey1 == sceneList.GraphicsId).SingleOrDefault().Filepath1;
                var bgPath = lstmainBGListScene.AsQueryable().Where(x => x.BgID == sceneList.BackGroundId).SingleOrDefault().Filepath;
                var borderPath = lstBorderListScene.AsQueryable().Where(x => x.DG_Borders_pkey == sceneList.BorderId).SingleOrDefault().FilePath;

                if (System.IO.File.Exists(borderPath))
                    ImgBorder.Source = new BitmapImage(new Uri(borderPath, UriKind.Absolute));
                if (System.IO.File.Exists(bgPath))
                    ImgBacground.Source = new BitmapImage(new Uri(bgPath, UriKind.Absolute));
                if (sceneList.IsActive)
                {
                    IsSceneActive.IsChecked = true;
                }
                if (sceneList.IsBlankPage)
                {
                    IsBlankPage.IsChecked = true;
                }
            }
            catch (Exception ex)
            {
                isEditScene = false;
            }
        }

        private void btnDeleteScene_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (MessageBox.Show("Do you want to delete this Scene?", "Confirm delete", MessageBoxButton.YesNo) == MessageBoxResult.Yes)
                {
                    Button btnSender = (Button)sender;
                    sceneId = (int)btnSender.Tag;
                    bool isdelete = (new SceneBusiness()).DeleteScene(sceneId);
                    if (isdelete)
                    {
                        MessageBox.Show("Scene deleted successfully");
                        //AuditLog.AddUserLog(Common.LoginUser.UserId, (int)FrameworkHelper.ActionType.DeleteBorder, "Border deleted at ");
                    }
                    DGManageScene.ItemsSource = (new SceneBusiness()).GetSceneDetails();
                    CmbSceneProductType.SelectedValue = "0";
                    CmbBackgroundType.SelectedValue = "0";
                    CmbBorderType.SelectedValue = "0";
                    CmbThemeType.SelectedValue = "0";
                    CmbpageNo.SelectedIndex = 0;
                    txtChineseFont.Text = string.Empty;
                    txtEnglishFont.Text = string.Empty;

                    IsSceneActive.IsChecked = true;
                    IsBlankPage.IsChecked = false;
                    ImgBorder.Source = null;
                    ImgBacground.Source = null;
                    txtSceneName.Text = string.Empty;
                    sceneId = 0;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }

        }

        private void btnSaveSelectScene_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (CheckSceneValidations())
                {
                    string pageNo = Convert.ToString(CmbpageNo.SelectedIndex); //Get Page No Value Added by Ajinkya
                    string chineseFont = txtChineseFont.Text; //Get Page No Value Added by Ajinkya
                    string englishFont = txtEnglishFont.Text; //Get Page No Value Added by Ajinkya
                    int themeId = Convert.ToInt32(CmbThemeType.SelectedValue); //Get Theme Value Added by Ajinkya
                    int productTypeId = Convert.ToInt32(CmbSceneProductType.SelectedValue);
                    int backGroundId = Convert.ToInt32(CmbBackgroundType.SelectedValue);
                    int borderId = Convert.ToInt32((CmbBorderType.SelectedValue));
                    int graphicsId = 0;
                    string sceneName = txtSceneName.Text;

                    string SyncCode = CommonUtility.GetUniqueSynccode(Convert.ToInt32(ApplicationObjectEnum.Scene).ToString().PadLeft(2, '0'), "1", "1", "00");
                    SceneInfo _objnew = new SceneInfo();
                    _objnew.ProductId = productTypeId;
                    _objnew.BackGroundId = backGroundId;
                    _objnew.BorderId = borderId;
                    _objnew.GraphicsId = graphicsId;
                    _objnew.SyncCode = SyncCode;
                    _objnew.IsSynced = false;
                    _objnew.IsActive = (bool)IsSceneActive.IsChecked;
                    _objnew.IsBlankPage = (bool)IsBlankPage.IsChecked;
                    _objnew.SceneName = sceneName;
                    _objnew.SceneId = sceneId;
                    _objnew.PageNo = pageNo;  //Added by Ajinkya
                    _objnew.ThemeId = themeId; //Added by Ajinkya
                    _objnew.ChineseFont = chineseFont; //Added by Ajinkya
                    _objnew.EnglishFont = englishFont; //Added by Ajinkya
                    // For Removing the 0 Index position to stored in Database Added By Ajinkya
                    if (pageNo == "0")
                    {
                        _objnew.PageNo = "";
                    }
                    else
                    {
                        _objnew.PageNo = pageNo;
                    }
                    (new SceneBusiness()).SetSceneDetails(_objnew);
                    MessageBox.Show("Record saved successfully");

                    DGManageScene.ItemsSource = null;
                    SceneBusiness sceneBiz = new SceneBusiness();
                    DGManageScene.ItemsSource = sceneBiz.GetSceneDetails();

                    CmbSceneProductType.SelectedValue = "0";
                    CmbBackgroundType.SelectedValue = "0";
                    CmbBorderType.SelectedValue = "0";
                    CmbThemeType.SelectedValue = "0";
                    CmbpageNo.SelectedIndex = 0;
                    txtChineseFont.Text = string.Empty;
                    txtEnglishFont.Text = string.Empty;
                    IsSceneActive.IsChecked = true; ;
                    IsBlankPage.IsChecked = false; ;
                    ImgBorder.Source = null;
                    ImgBacground.Source = null;
                    txtSceneName.Text = string.Empty;
                    sceneId = 0;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }

        private void btnBackScene_Click(object sender, RoutedEventArgs e)
        {

        }

        #endregion

        #region
        private void btnResetCharacter_Click(object sender, RoutedEventArgs e)
        {
            clearCharacterData();
        }
        private void btnBackCharacter_Click(object sender, RoutedEventArgs e)
        {

        }

        private void btnEditCharacter_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                isEditCharacter = true;

                Button _objbtn = new Button();
                _objbtn = (Button)(sender);
                CharacterId = _objbtn.Tag.ToInt32();
                isEditCharacter = true;
                IsSceneActive.IsChecked = false;

                var objCharacterInfo = new CharacterInfo
                {
                    DG_Character_Pkey = CharacterId,
                    DG_Character_OperationType = 2
                };
                var characterList = (new CharacterBusiness()).GetCharacter(objCharacterInfo);

                txtCharacterName.Text = characterList.FirstOrDefault().DG_Character_Name;
                if (characterList.FirstOrDefault().DG_Character_IsActive == 1)
                    IsCharacterActive.IsChecked = true;
                else if (characterList.FirstOrDefault().DG_Character_IsActive == 0)
                    IsCharacterActive.IsChecked = false;
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }

        }
        private void btnDeleteCharacter_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Button _objbtn = new Button();
                _objbtn = (Button)(sender);
                CharacterId = _objbtn.Tag.ToInt32();
                var objCharacterInfo = new CharacterInfo
                {
                    DG_Character_Pkey = CharacterId,
                    DG_Character_OperationType = 3
                };
                var characterList = (new CharacterBusiness()).GetCharacter(objCharacterInfo);
                BindCharacterGrid();
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }
        private void btnSaveCharacter_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if ((string.IsNullOrEmpty(txtCharacterName.Text)) && (string.IsNullOrWhiteSpace(txtCharacterName.Text)))
                {
                    MessageBox.Show("Please Enter Character Name");
                    return;
                }

                if (txtCharacterName.Text.Length > 25)
                {
                    MessageBox.Show("Character length should be less than 25");
                    return;
                }
                if (!ValidateCharacter(txtCharacterName.Text.Trim()))
                {
                    MessageBox.Show("Please Enter Valid Character Name.");
                    return;
                }

                var objCharacter = new CharacterInfo
                {
                    DG_Character_Name = txtCharacterName.Text.ToUpper().Trim(),
                    DG_Character_IsActive = Convert.ToInt16(IsCharacterActive.IsChecked)
                };

                var result = (new CharacterBusiness().InsertCharacter(objCharacter));
                if (result)
                {
                    MessageBox.Show("Character Details Saved Successfully");

                }
                else
                {
                    MessageBox.Show("Error Occured");
                }
                BindCharacterGrid();
                clearCharacterData();
            }

            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }

        private void clearCharacterData()
        {
            txtCharacterName.Text = string.Empty;
            IsCharacterActive.IsChecked = true;
        }
        private void BindCharacterGrid()
        {
            try
            {
                var objCharacter = new CharacterInfo
                {
                    DG_Character_Pkey = 0,
                    DG_Character_OperationType = 1
                };
                var resultSet = (new CharacterBusiness().GetCharacter(objCharacter));
                DGCharacter.ItemsSource = resultSet;
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }

        }
        #endregion



        private bool ValidateCharacter(string inputStr)
        {
            // var regex = new Regex("[0-9a-zA-Z' ']");
            var regex = new Regex(@"^[a-zA-Z0-9 ]+$");
            //  var regex = new Regex(@"^\d*[a-zA-Z][a-zA-Z0-9]*$");
            //^\d*[a-zA-Z][a-zA-Z0-9]*$
            if (!regex.IsMatch(inputStr))
            {
                return false;
            }
            return true;
        }

        //private void BindGroupGrid()
        //{
        //    try
        //    {
        //        var groupBiz = new GroupBusiness();
        //        var result1 = groupBiz.GetGroupDetails(0);
        //        CommonUtility.BindComboWithSelect<GroupDetails>(cmbGroupSelection, result1, "DG_Group_Name", "DG_Group_pkey", 0, DigiPhoto.Utility.Repository.Data.ClientConstant.SelectString);
        //        if (!String.IsNullOrWhiteSpace(LoginUser.GroupValue))
        //            cmbGroupSelection.SelectedValue = LoginUser.GroupValue;
        //        else
        //            cmbGroupSelection.SelectedValue = "0";
        //    }
        //    catch (Exception ex)
        //    {
        //        ErrorHandler.ErrorHandler.LogError(ex);
        //    }
        //}

        private void btnSelectedStockImage_Click(object sender, RoutedEventArgs e)
        {
            Stream checkstream = null;
            Microsoft.Win32.OpenFileDialog ofd = new Microsoft.Win32.OpenFileDialog();
            ofd.Multiselect = false;
            ofd.Filter = "Image Files | *.jpg;*.png;*.gif; | Video Files | *.mp4;*.mov;*.flv;*.wmv;*.avi;*.mpeg;*.3gp;*.";

            if ((bool)ofd.ShowDialog())
            {
                try
                {
                    if ((checkstream = ofd.OpenFile()) != null)
                    {
                        txtSelectedStockImage.Text = ofd.FileName.ToString();
                        stockshotname = System.IO.Path.GetFileName(ofd.FileName);
                        _isNewFileSelected = true;
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Error: Could not read file from disk. Original error: " + ex.Message);
                }
            }
        }

        string stockshotname = string.Empty;

        private void btnSaveStockShotImage_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                string sourceFilePath = string.Empty;
                sourceFilePath = txtSelectedStockImage.Text.Trim();
                if (string.IsNullOrEmpty(sourceFilePath))
                {
                    MessageBox.Show("Please select image");
                    return;
                }
                configBusiness = new ConfigBusiness();
                if (!_iseditStockImg)
                {
                    stockShotImg = new StockShot();
                    stockShotImg.CreatedBy = LoginUser.UserId;
                    stockShotImg.ImageName = stockshotname;
                }
                else
                {
                    stockShotImg.ModifiedBy = LoginUser.UserId;
                    stockShotImg.ImageId = _stockImgId;
                }
                stockShotImg.IsActive = (bool)chkIsStockImageActive.IsChecked;
                string destPath = System.IO.Path.Combine(LoginUser.DigiFolderPath, "StockShot");
                if (_isNewFileSelected)
                {
                    Int64 Id = configBusiness.SaveUpdateStockShotImage(stockShotImg);
                    if (Id > 0)
                    {
                        stockshotname = System.IO.Path.GetFileNameWithoutExtension(stockshotname) + "_" + Id.ToString() + System.IO.Path.GetExtension(stockshotname);
                        string filepath = System.IO.Path.Combine(destPath, stockshotname);
                        if (!Directory.Exists(destPath))
                            Directory.CreateDirectory(destPath);
                        if (!Directory.Exists(System.IO.Path.Combine(destPath, "Thumbnails")))
                            Directory.CreateDirectory(System.IO.Path.Combine(destPath, "Thumbnails"));
                        if (!File.Exists(destPath + "\\" + stockshotname))
                            System.IO.File.Copy(sourceFilePath, destPath + "\\" + stockshotname, true);
                        if (System.IO.Path.GetExtension(sourceFilePath).ToLower().Equals(".jpg") || System.IO.Path.GetExtension(sourceFilePath).ToLower().Equals(".png") || System.IO.Path.GetExtension(sourceFilePath).ToLower().Equals(".gif"))
                        {
                            using (FileStream fileStream = File.OpenRead(destPath + "\\" + stockshotname))
                            {
                                MemoryStream ms = new MemoryStream();
                                fileStream.CopyTo(ms);
                                ms.Seek(0, SeekOrigin.Begin);
                                ResizeAndSaveHighQualityImage(System.Drawing.Image.FromStream(ms),
                                                                   System.IO.Path.Combine(LoginUser.DigiFolderPath, "StockShot", "Thumbnails", stockshotname), 100, 150);
                                fileStream.Close();
                                ms.Dispose();

                            }
                        }
                        CopyToAllSubstore(lstConfigurationInfo, destPath + "\\" + stockshotname, System.IO.Path.Combine(LoginUser.DigiFolderPath, "StockShot", "Thumbnails", stockshotname), "StockShot");
                        stockShotImg.ImageName = stockshotname;
                        stockShotImg.ImageId = Id;
                        configBusiness.SaveUpdateStockShotImage(stockShotImg);
                    }
                }
                else
                {
                    stockshotname = stockShotImg.ImageName;
                    configBusiness.SaveUpdateStockShotImage(stockShotImg);
                    if (!File.Exists(destPath + "\\" + stockShotImg.ImageName))
                    {
                        System.IO.File.Copy(sourceFilePath, destPath + "\\" + stockshotname, true);
                        if (System.IO.Path.GetExtension(sourceFilePath).ToLower().Equals(".jpg") || System.IO.Path.GetExtension(sourceFilePath).ToLower().Equals(".png") || System.IO.Path.GetExtension(sourceFilePath).ToLower().Equals(".gif"))
                        {
                            using (FileStream fileStream = File.OpenRead(destPath + "\\" + stockshotname))
                            {
                                MemoryStream ms = new MemoryStream();
                                fileStream.CopyTo(ms);
                                ms.Seek(0, SeekOrigin.Begin);
                                ResizeAndSaveHighQualityImage(System.Drawing.Image.FromStream(ms),
                                                           System.IO.Path.Combine(LoginUser.DigiFolderPath, "StockShot", "Thumbnails", stockshotname), 100, 150);
                                fileStream.Close();
                                ms.Dispose();
                            }
                        }
                        CopyToAllSubstore(lstConfigurationInfo, destPath + "\\" + stockshotname, System.IO.Path.Combine(LoginUser.DigiFolderPath, "StockShot", "Thumbnails", stockshotname), "StockShot");
                    }
                }
                MessageBox.Show("Item saved successfully.");
                stockshotname = string.Empty;
                GetStockImgDetails();
                _iseditStockImg = false;
                _isNewFileSelected = false;
                txtSelectedStockImage.Text = "";
                chkIsStockImageActive.IsChecked = true;
                btnSelectedStockImage.IsEnabled = true;
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
            }
        }
        private void GetStockImgDetails()
        {
            List<StockShot> _objImagedetail = new List<StockShot>();
            var objImgDetails = (new ConfigBusiness()).GetStockShotImagesList(0);
            try
            {
                foreach (var item in objImgDetails)
                {
                    StockShot _objnew = new StockShot();
                    _objnew.ImageId = item.ImageId;
                    _objnew.ImageName = item.ImageName;
                    _objnew.IsActive = item.IsActive;
                    var v = System.IO.Path.GetExtension(item.ImageName);
                    if (System.IO.Path.GetExtension(item.ImageName).ToLower().Equals(".jpg") || System.IO.Path.GetExtension(item.ImageName).ToLower().Equals(".png") || System.IO.Path.GetExtension(item.ImageName).ToLower().Equals(".gif"))
                    {
                        _objnew.ImageThumbnailPath = System.IO.Path.Combine(LoginUser.DigiFolderPath, "StockShot", "Thumbnails", item.ImageName);
                    }
                    else
                    {
                        _objnew.ImageThumbnailPath = @"/DigiPhoto;component/images/vidico.png";
                    }
                    _objImagedetail.Add(_objnew);
                }
                grdManagestockShotImages.ItemsSource = _objImagedetail;
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
            }

        }
        private void GetAllSubstoreConfigdata()
        {
            try
            {

                lstConfigurationInfo = (new ConfigBusiness()).GetAllSubstoreConfigdata();
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
            }
        }

        private void btnDeleteStockShotImage_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (MessageBox.Show("Do you want to delete this Image?", "Confirm delete", MessageBoxButton.YesNo) == MessageBoxResult.Yes)
                {
                    Button btnSender = (Button)sender;
                    var Id = btnSender.Tag;
                    configBusiness = new ConfigBusiness();
                    string fileName = (new ConfigBusiness()).GetStockShotImagesList(Convert.ToInt64(Id)).FirstOrDefault().ImageName;
                    string filePath = System.IO.Path.Combine(LoginUser.DigiFolderPath, "StockShot", fileName);
                    string thumbnailPath = System.IO.Path.Combine(LoginUser.DigiFolderPath, "StockShot", "Thumbnails", fileName);
                    grdManagestockShotImages.SelectedItem = null;
                    grdManagestockShotImages.ItemsSource = null;
                    btnSender = null;
                    sender = null;
                    grdManagestockShotImages.UpdateLayout();
                    bool isdelete = configBusiness.DeleteStockShotImg(Convert.ToInt64(Id));
                   
                    if (isdelete)
                    {
                        txtSelectedStockImage.Text = "";
                       // chkIsStockImageActive.IsChecked = false;
                        if (File.Exists(filePath))
                            File.Delete(filePath);
                        DeleteFromAllSubstore(lstConfigurationInfo, filePath, "StockShot");
                        if (File.Exists(thumbnailPath))
                            File.Delete(thumbnailPath);
                        MessageBox.Show("Image deleted successfully");
                        stockshotname = string.Empty;
                        _iseditStockImg = false;
                        _isNewFileSelected = false;
                        txtSelectedStockImage.Text = "";
                        chkIsStockImageActive.IsChecked = true;
                    }
                    btnSelectedStockImage.IsEnabled = true;
                }
            }
            catch(IOException eio)
            {
                stockshotname = string.Empty;
                GetStockImgDetails();
                _iseditStockImg = false;
                _isNewFileSelected = false;
                txtSelectedStockImage.Text = "";
                chkIsStockImageActive.IsChecked = true;
                btnSelectedStockImage.IsEnabled = true;
                MessageBox.Show("Image deleted successfully");
            }
            catch (Exception ex)
            {
                MessageBox.Show("Can not delete Image because " + ex.Message);
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
            finally
            {
                GetStockImgDetails();
            }
        }

        private void btnEditStockShotImage_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Button _objbtn = new Button();
                _objbtn = (Button)(sender);
                _stockImgId = _objbtn.Tag.ToInt32();
                _iseditStockImg = true;
                stockShotImg = (new ConfigBusiness()).GetStockShotImagesList(_stockImgId).FirstOrDefault();
                if (stockShotImg != null)
                {
                    txtSelectedStockImage.Text = stockShotImg.ImageName;
                    chkIsStockImageActive.IsChecked = stockShotImg.IsActive;
                    btnSelectedStockImage.IsEnabled = false;
                }
            }
            catch (Exception ex)
            {
                _iseditStockImg = false;
            }
        }

        private void btnCancelStockShot_Click(object sender, RoutedEventArgs e)
        {
            txtSelectedStockImage.Text = "";
            chkIsStockImageActive.IsChecked = true;
            btnSelectedStockImage.IsEnabled = true;
        }
        List<SemiOrderSettings> objDG_SemiOrder_Settings = null;
        public void loadSpecGrid(int SelectedLocationId)
        {
            objDG_SemiOrder_Settings = (new SemiOrderBusiness()).GetSemiOrderSettings(null, SelectedLocationId);
            if (objDG_SemiOrder_Settings != null && objDG_SemiOrder_Settings.Count > 0)
            {
                for (int f = 0; f < objDG_SemiOrder_Settings.Count; f++)
                {
                    string id = objDG_SemiOrder_Settings[f].DG_SemiOrder_ProductTypeId;
                }
                grdSpecSetting.ItemsSource = null;
                grdSpecSetting.ItemsSource = objDG_SemiOrder_Settings;
            }
            else
            {
                grdSpecSetting.ItemsSource = null;
            }
        }

        private void cmb_Location_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            SelectedLocationId = cmb_Location.SelectedValue.ToInt32();
            loadSpecGrid(SelectedLocationId);
        }

        private void btnBack_Click(object sender, RoutedEventArgs e)
        {
            ManageHome _objhome = new ManageHome();
            _objhome.Show();
            // this.Close();
            this.Hide();
        }

        private void btnEditSpecSetting_Click(object sender, RoutedEventArgs e)
        {
            ///Below 3 lines added to get recent data in dropdown for Borders,Frames and background
            //Added by Vinod Salunke 1 Aug 2018
              ucAddEditSpecPrintProfile.BindBorders();
            ucAddEditSpecPrintProfile.loadProductType();
            ucAddEditSpecPrintProfile.LoadGraphics();

            Button btnSender = (Button)sender;
            int id = btnSender.Tag.ToInt32();
            btn.IsEnabled = false;
            btn.Background = Brushes.Black;
            btn.Opacity = 0.4;
            ucAddEditSpecPrintProfile.LocationId = cmb_Location.SelectedValue.ToInt32();
            ucAddEditSpecPrintProfile.semiOrderSettings = objDG_SemiOrder_Settings.Where(t => t.DG_SemiOrder_Settings_Pkey == id).FirstOrDefault();
            ucAddEditSpecPrintProfile.SemiOrderPrimaryKey = id;
            ucAddEditSpecPrintProfile.GetSemiOrderSettings();
            ucAddEditSpecPrintProfile.SetOrientatin();
           
            ucAddEditSpecPrintProfile.Visibility = Visibility.Visible;
        }

        private void btnDeleteSpecSetting_Click(object sender, RoutedEventArgs e)
        {
            Button btnSender = (Button)sender;
            int id = btnSender.Tag.ToInt32();

            try
            {
                if (id != null)
                {
                    if (MessageBox.Show("Do you want to delete the Spec settings?", "Tempar Data", MessageBoxButton.YesNo, MessageBoxImage.Warning) == MessageBoxResult.Yes)
                    {

                        SemiOrderBusiness objSemiOrderBusiness = new SemiOrderBusiness();
                        bool isdelete = objSemiOrderBusiness.DeleteSpecSetting(id);
                        if (isdelete)
                        {
                            MessageBox.Show("Spec settings deleted successfully");
                            SelectedLocationId = cmb_Location.SelectedValue.ToInt32();
                            loadSpecGrid(SelectedLocationId);

                        }
                    }
                }

            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }

        }

        private void btnAddNew_Click(object sender, RoutedEventArgs e)
        {
            if (cmb_Location.SelectedValue.ToInt32() > 0)
            {
                btn.IsEnabled = false;
                btn.Background = Brushes.Black;
                btn.Opacity = 0.4;
                Configuration.VerticalCropCoordinates = string.Empty;
                Configuration.HorizontalCropCoordinates = string.Empty;
                ucAddEditSpecPrintProfile.LocationId = cmb_Location.SelectedValue.ToInt32();
                ucAddEditSpecPrintProfile.SemiOrderPrimaryKey = 0;
                ucAddEditSpecPrintProfile.ResetSpecPrintEffects();
                ucAddEditSpecPrintProfile.ResetSpecControlValues();
                ucAddEditSpecPrintProfile.ResetVariables();
                ///Below 3 lines added to get recent data in dropdown for Borders,Frames and background
                //Added by Vinod Salunke 1 Aug 2018
                ucAddEditSpecPrintProfile.BindBorders();
                ucAddEditSpecPrintProfile.loadProductType();
                ucAddEditSpecPrintProfile.LoadGraphics();
                ucAddEditSpecPrintProfile.Visibility = Visibility.Visible;
            }
            else
            {
                MessageBox.Show("Please select the location for which new profile has to be added");
            }
        }

        private void chkTaxEnabled_Checked(object sender, RoutedEventArgs e)
        {
            if (chkTaxEnabled.IsChecked.Equals(true))
            {
                chkIsTaxIncluded.IsEnabled = true;
            }
            else
            {
                chkIsTaxIncluded.IsEnabled = false;
                chkIsTaxIncluded.IsChecked = false;
            }
        }
        private void EditBackground(object sender, RoutedEventArgs e)
        {
            try
            {
                bool allowEdit = true;
                if (EditableBackground != null)
                {
                    var messageResult = MessageBox.Show("It looks like you have been editing a background.If you leave before saving, your changes will be lost.Would you like to save your unsaved changes ?", "Alert", MessageBoxButton.YesNo);
                    if (messageResult == MessageBoxResult.Yes)
                        allowEdit = false;
                }
                if (allowEdit)
                {
                    btnCancelSelectBackgound_Click(sender, e);
                    Button editButton = (Button)(sender);
                    if (editButton != null && editButton.DataContext != null)
                    {
                        try
                        {
                            var editedBackground = (BackGroundInfo)editButton.DataContext;
                            #region Check for dependency
                            if (editedBackground != null)
                            {
                                string statusMessage = string.Empty;
                                bool foundDependency = FindDependencyOfBackground(editedBackground, "edit", out statusMessage);
                                if (foundDependency)
                                {
                                    MsgBox.ShowHandlerDialog(statusMessage, DigiMessageBox.DialogType.OK);
                                    return;
                                }
                            }
                            #endregion
                            EditableBackground = (BackGroundInfo)editedBackground.Clone();
                        }
                        catch (Exception ex)
                        {
                            EditableBackground = null;
                        }
                        if (EditableBackground != null)
                        {
                            txtSelectedBackground.Text = EditableBackground.DG_BackGround_Image_Display_Name;
                            IsBackgroundActive.IsChecked = EditableBackground.DG_Background_IsActive;
                            ChkIsBackgroundPanoramic.IsChecked = EditableBackground.DG_Background_IsPanorama;
                        }
                    }
                }

            }
            catch (Exception ex)
            {
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        protected virtual void PropertyModified(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }

        private void EditGraphics(object sender, RoutedEventArgs e)
        {
            try
            {
                bool allowEdit = true;
                if (EditableGraphics != null)
                {
                    var messageResult = MessageBox.Show("It looks like you have been editing a graphics.If you leave before saving, your changes will be lost.Would you like to save your unsaved changes ?", "Alert", MessageBoxButton.YesNo);
                    if (messageResult == MessageBoxResult.Yes)
                        allowEdit = false;
                }
                if (allowEdit)
                {
                    Button editButton = (Button)(sender);
                    if (editButton != null && editButton.DataContext != null)
                    {
                        try
                        {
                            var editedGraphics = (AllGraphics)editButton.DataContext;


                            EditableGraphics = (AllGraphics)editedGraphics.Clone();
                        }
                        catch (Exception ex)
                        {
                            EditableGraphics = null;
                        }
                        if (EditableGraphics != null)
                        {
                            string statusMessage = string.Empty;
                            if (!FindDependencyOfGraphic(EditableGraphics.Pkey1, EditableGraphics.GraphicsName1, EditableGraphics.GraphicsDisplayName1, "edit", out statusMessage))
                            {
                                txtSelectedgraphics.Text = EditableGraphics.GraphicsDisplayName1;
                                IsGraphicsActive.IsChecked = EditableGraphics.IsActive1;
                            }
                            else
                            {
                                MsgBox.ShowHandlerDialog(statusMessage, DigiMessageBox.DialogType.OK);
                                return;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
            }
        }

        bool FindDependencyOfBorder(BorderInfo border, string mode, out string message)
        {
            message = string.Empty;
            if (border != null)
            {

                #region find dependency in scene
                var sceneBusiness = new SceneBusiness();
                var sceneList = sceneBusiness.GetSceneDetails();
                if (sceneList != null && sceneList.Count > 0)
                {
                    var backGroundinScene = sceneList.FirstOrDefault(sc => sc.BorderId == border.DG_Borders_pkey);
                    if (backGroundinScene != null && !string.IsNullOrEmpty(backGroundinScene.SceneName))
                    {
                        message = string.Format("You can't {0} the border {1}, because this has been associated in a Scene.", mode, backGroundinScene.BorderName);
                        return true;
                    }
                }
                #endregion
                #region Find dependency in Spec Printing
                var semiOrderBusiness = new SemiOrderBusiness();

                var orderSettings = semiOrderBusiness.GetSemiOrderSettings(null, 0);
                if (orderSettings != null && orderSettings.Count > 0)
                {
                    var orderSettting = orderSettings.FirstOrDefault(os => (os.DG_SemiOrder_Settings_IsImageFrame.HasValue && os.DG_SemiOrder_Settings_IsImageFrame.Value)); 
                         
                    if (orderSettting != null)
                    {
                        var HorizontalSetting = orderSettings.FirstOrDefault(hs => hs.ImageFrame_Horizontal.Trim().ToLower() == border.DG_Border.Trim().ToLower());
                        if(HorizontalSetting !=null && HorizontalSetting.ImageFrame_Horizontal != null)
                        {
                            message = string.Format("You can't {0} the border {1}, because this has been associated in a Spec Printing.", mode, orderSettting.ImageFrame_Horizontal);
                            return true;
                        }
                        var VerticalSetting = orderSettings.FirstOrDefault(vs => vs.ImageFrame_Vertical.Trim().ToLower() == border.DG_Border.Trim().ToLower());
                        if(VerticalSetting !=null && VerticalSetting.ImageFrame_Vertical != null)
                        {
                            message = string.Format("You can't {0} the border {1}, because this has been associated in a Spec Printing.", mode, orderSettting.ImageFrame_Vertical);
                            return true;
                        }
                    }
                }
                #endregion



            }

            return false;


        }

        bool FindDependencyOfBackground(BackGroundInfo background, string mode, out string message)
        {
            message = string.Empty;
            if (background != null)
            {

                #region find dependency in scene
                var sceneBusiness = new SceneBusiness();
                var sceneList = sceneBusiness.GetSceneDetails();
                if (sceneList != null && sceneList.Count > 0)
                {
                    var backGroundinScene = sceneList.FirstOrDefault(sc => sc.BackGroundId == background.DG_Background_pkey);
                    if (backGroundinScene != null && !string.IsNullOrEmpty(backGroundinScene.SceneName))
                    {
                        message = string.Format("You can't {0} the background {1}, because this has been associated in a Scene.", mode, backGroundinScene.BackgroundName);
                        return true;
                    }
                }
                #endregion
                #region Find dependency in Spec Printing
                var semiOrderBusiness = new SemiOrderBusiness();

                var orderSettings = semiOrderBusiness.GetSemiOrderSettings(null, 0);
                if (orderSettings != null && orderSettings.Count > 0)
                {
                    var orderSettting = orderSettings.FirstOrDefault(os => os.Background_Horizontal.Trim().ToLower() == background.DG_BackGround_Image_Name.Trim().ToLower() || os.Background_Vertical.Trim().ToLower() == background.DG_BackGround_Image_Name.Trim().ToLower());
                    if (orderSettting != null)
                    {
                        if(!string.IsNullOrEmpty(orderSettting.Background_Horizontal))
                        {
                            message = string.Format("You can't {0} the background {1}, because this has been associated in a Spec Printing.", mode, orderSettting.Background_Horizontal);
                            return true;
                        }
                        else if (!string.IsNullOrEmpty(orderSettting.Background_Vertical))
                        {
                            message = string.Format("You can't {0} the background {1}, because this has been associated in a Spec Printing.", mode, orderSettting.Background_Vertical);
                            return true;
                        }
                    }
                }
                #endregion

                #region Find dependency in Default Background
                ConfigBusiness conBiz = new ConfigBusiness();
                List<iMIXConfigurationInfo> ConfigValuesList = conBiz.GetNewConfigValues(LoginUser.SubStoreId);
                if (ConfigValuesList != null && ConfigValuesList.Count > 0)
                {

                    var defaultBackground = ConfigValuesList.FirstOrDefault(cvl => cvl.IMIXConfigurationMasterId == (long)ConfigParams.DefaultBackImagePath);
                    if (defaultBackground != null && !string.IsNullOrEmpty(defaultBackground.ConfigurationValue))
                    {
                        if (System.IO.File.Exists(defaultBackground.ConfigurationValue))
                        {
                            string backgroundFileName = System.IO.Path.GetFileName(defaultBackground.ConfigurationValue);
                            if (!string.IsNullOrEmpty(backgroundFileName))
                            {
                                if (backgroundFileName.Trim().ToLower() == background.DG_BackGround_Image_Name.Trim().ToLower())
                                {
                                    message = string.Format("You can't {0} the background {1}, because this has been defined as default background", mode, background.DG_BackGround_Image_Name);
                                    return true;
                                }
                            }
                        }
                    }
                }
                #endregion

            }

            return false;


        }

        bool FindDependencyOfGraphic(int graphicId, string graphicName, string displayName, string mode, out string message)
        {
            message = string.Empty;
            if (graphicName != null)
            {
                #region Find dependency in Spec Printing
                var semiOrderBusiness = new SemiOrderBusiness();

                var orderSettings = semiOrderBusiness.GetSemiOrderSettings(null, 0);
                if (orderSettings != null && orderSettings.Count > 0)
                {
                    var orderSettting = orderSettings.FirstOrDefault(os => os.Graphics_layer_Horizontal.ToLower().Contains(graphicName.Trim().ToLower()) || os.Graphics_layer_Vertical.ToLower().Contains(graphicName.Trim().ToLower()));
                    if (orderSettting != null && (!string.IsNullOrEmpty(orderSettting.Graphics_layer_Horizontal) && !string.IsNullOrEmpty(orderSettting.Graphics_layer_Vertical)))
                    {
                        message = string.Format("You can't {0} the graphic {1}, because this has been associated in a Spec Printing.", mode, displayName);
                        return true;
                    }
                }
                #endregion
            }

            return false;
        }
        string OverlayDisplyname = string.Empty; int overlayMediaId = 0; int OverlayId = 0;long VideoOverlayId=0;
        //    int videoBGMediaType = 0;
        //   bool IsVideoBGEdited = false;
        string OverlayName = string.Empty;
        long videoOverlayId = 0;
        private void GetVideoOverlayDetails()
        {
            try
            {
                ConfigBusiness configBusiness = new ConfigBusiness();
                List<VideoOverlay> videoOverlayList = configBusiness.GetVideoOverlays();
                DGManageOverlay.ItemsSource = videoOverlayList;

            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }

        }
        private void btnSelectOverlay_Click(object sender, RoutedEventArgs e)
        {

            Stream checkstream = null;
            Microsoft.Win32.OpenFileDialog ofd = new Microsoft.Win32.OpenFileDialog();
            ofd.Multiselect = false;
            ofd.Filter = "Select Media|*.jpg;*.png;*.mp4;*.wmv;*.avi;|" + "JPG|*.jpg;*.JPG|PNG|*.png;*.PNG|WMV|*.wmv;*.WMV|MP4|*.mp4;*.MP4|AVI|*.avi;*.AVI;";
            if ((bool)ofd.ShowDialog())
            {
                try
                {
                    if ((checkstream = ofd.OpenFile()) != null)
                    {
                        if (System.IO.Path.GetFileName(ofd.FileName.ToString()).ToCharArray().Count() <= 50)
                        {
                            txtSelectedOverlay.Text = ofd.FileName.ToString();
                            OverlayDisplyname = System.IO.Path.GetFileName(ofd.FileName);
                            if (System.IO.Path.GetExtension(ofd.FileName) == ".jpg" || System.IO.Path.GetExtension(ofd.FileName) == ".JPEG")
                            {
                                overlayMediaId = 1;
                            }
                            else
                            {
                                overlayMediaId = 2;
                            }
                        }
                        else
                        {
                            MessageBox.Show("File name should not excced 50 characters.", "DigiPhoto", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                        }
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Error: Could not read file from disk. Original error: " + ex.Message);
                }
            }
        }

        private void btnSaveSelectOverlay_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                long videoLength = 0;
                bool validationSuccess = false;
                var player = new WindowsMediaPlayer();
                if (CheckVideoBGValidations(txtSelectedOverlay))
                {
                    if (System.IO.Path.GetExtension(OverlayDisplyname) == ".mp4" || System.IO.Path.GetExtension(OverlayDisplyname) == ".wmv" || System.IO.Path.GetExtension(OverlayDisplyname) == ".avi" || System.IO.Path.GetExtension(OverlayDisplyname) == ".mov")
                    {
                        var clip = player.newMedia(txtSelectedOverlay.Text);
                        videoLength = string.IsNullOrEmpty(TimeSpan.FromSeconds(clip.duration).ToString()) ? 0 : (long)(TimeSpan.FromSeconds(clip.duration).TotalSeconds);
                        if (videoLength <= 1800)//30 min 
                        {
                            validationSuccess = true;
                        }
                        else
                        {
                            validationSuccess = false;
                        }
                    }
                    else
                    {
                        validationSuccess = true;
                    }
                    if (validationSuccess)
                    {
                        bool isSaved = false;
                        Guid _obj = Guid.NewGuid();
                        OverlayName = System.IO.Path.GetFileNameWithoutExtension(OverlayDisplyname) + _obj.ToString() + System.IO.Path.GetExtension(OverlayDisplyname);
                        string destFile = LoginUser.DigiFolderVideoOverlayPath + OverlayName;
                        string strDescription = txtOverlayDescription.Text;
                        bool? IsActive = IsOverlayActive.IsChecked;
                        if (!IsVideoOverlayEdited)
                        {
                            if (!Directory.Exists(LoginUser.DigiFolderVideoOverlayPath))
                            {
                                Directory.CreateDirectory(LoginUser.DigiFolderVideoOverlayPath);
                            }

                            System.IO.File.Copy(txtSelectedOverlay.Text, destFile, true);
                            isSaved = SaveVideoOverlay(OverlayDisplyname, OverlayName, strDescription, IsActive, IsVideoOverlayEdited, overlayMediaId, (int)videoLength);
                        }
                        else
                        {
                            string strSourceFile = txtSelectedOverlay.Text;
                            if (System.IO.File.Exists(strSourceFile))
                            {
                                System.IO.File.Copy(strSourceFile, destFile, true);
                                if (strSourceFile.Contains(LoginUser.DigiFolderVideoOverlayPath))
                                {
                                    System.IO.File.Delete(strSourceFile);
                                   // DeleteFromAllSubstore(lstConfigurationInfo, obj.Name, "VideoOverlay");
                                }
                                isSaved = SaveVideoOverlay(OverlayDisplyname, OverlayName, strDescription, IsActive, IsVideoOverlayEdited, overlayMediaId, (int)videoLength);
                            }
                            else
                            {
                                MessageBox.Show("File not found.", "DigiPhoto", MessageBoxButton.OK, MessageBoxImage.Error);
                            }
                            IsVideoOverlayEdited = false;
                        }
                        if (isSaved)
                        {
                            CopyToAllSubstore(lstConfigurationInfo, destFile, "", "VideoOverlay");
                            MessageBox.Show("Video overlay saved successfully.", "DigiPhoto", MessageBoxButton.OK, MessageBoxImage.Information);
                            AuditLog.AddUserLog(Common.LoginUser.UserId, 49, "Add/Edit Video overlay at ");
                            GetVideoOverlayDetails();
                        }
                        ResetOverlayControls();
                    }
                    else
                    {
                        MessageBox.Show("Video overlay length should not exceed 30 minutes.", "DigiPhoto", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    }
                }
                else
                {
                    MessageBox.Show("Select video overlay file.", "DigiPhoto", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }

        }

        private void ResetOverlayControls()
        {
            txtOverlayDescription.Text = string.Empty;
            txtSelectedOverlay.Text = string.Empty;
            IsOverlayActive.IsChecked = true;
            VideoOverlayId = 0;
            IsVideoOverlayEdited = false;
        }
        private bool SaveVideoOverlay(string OverlayDisplyname, string OverlayName, string strDescription, bool? IsActive, bool IsEdited, int overlayMediaId, int VidLength)
        {
            try
            {
                IsActive = IsActive == null ? false : IsActive;
                VideoOverlay objVBG = new VideoOverlay();
                objVBG.DisplayName = OverlayDisplyname;
                objVBG.Name = OverlayName;
                objVBG.IsActive = Convert.ToBoolean(IsActive);
                objVBG.MediaType = overlayMediaId;
                objVBG.Description = strDescription;
                objVBG.VideoLength = VidLength;
                if (!IsEdited)
                {
                    objVBG.CreatedBy = LoginUser.UserId;
                    objVBG.CreatedOn = System.DateTime.Now;
                    objVBG.ModifiedBy = 0;
                    objVBG.ModifiedOn = System.DateTime.Now;
                }
                else
                {
                    objVBG.CreatedBy = LoginUser.UserId;
                    objVBG.CreatedOn = System.DateTime.Now;
                    objVBG.VideoOverlayId = videoBackgroundId;
                    objVBG.ModifiedBy = LoginUser.UserId;
                    objVBG.ModifiedOn = System.DateTime.Now;
                    objVBG.VideoOverlayId = VideoOverlayId;
                }
                ConfigBusiness obj = new ConfigBusiness();
                int id = obj.SaveUpdateVideoOverlay(objVBG);
                if (id > 0)
                    return true;
                else return false;

            }
            catch
            {
                return false;
            }
        }

        private void btnCancelSelectOverlay_Click(object sender, RoutedEventArgs e)
        {
            ResetOverlayControls();
        }
        private void btnDeleteOverlay_Click(object sender, RoutedEventArgs e)
        {
            Button btnSender = (Button)sender;
            long videoOverlayId = long.Parse(btnSender.Tag.ToString());
            MessageBoxResult response = MessageBox.Show("Do you want to delete record?", "Digiphoto", MessageBoxButton.YesNo, MessageBoxImage.Question);
            if (response == MessageBoxResult.Yes)
            {
                ConfigBusiness configBusiness = new ConfigBusiness();
                VideoOverlay obj = configBusiness.GetVideoOverlays(VideoOverlayId);
                string filepath = LoginUser.DigiFolderVideoOverlayPath + obj.Name;
                bool isDeleted = configBusiness.DeleteVideoOverlay(videoOverlayId);
                if (isDeleted)
                {
                    if (System.IO.File.Exists(filepath))
                    {
                        System.IO.File.Delete(filepath);
                    }
                    DeleteFromAllSubstore(lstConfigurationInfo, obj.Name, "VideoOverlay");

                    GetVideoOverlayDetails();
                    MessageBox.Show("Video overlay record deleted Successfully", "DigiPhoto", MessageBoxButton.OK, MessageBoxImage.Information);
                    AuditLog.AddUserLog(Common.LoginUser.UserId, 49, "Delete video overlay at ");
                }
                ResetOverlayControls();
            }
           
        }

        //  long 
        private void btnEditOverlay_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Button btnSender = (Button)sender;
                VideoOverlayId = Convert.ToInt32(btnSender.Tag);
                IsVideoOverlayEdited = true;
                ConfigBusiness objConfigBus = new ConfigBusiness();
                VideoOverlay obj = objConfigBus.GetVideoOverlays(VideoOverlayId);
                videoBGName = obj.Name;
                txtSelectedOverlay.Text = LoginUser.DigiFolderVideoOverlayPath + obj.Name;
                txtOverlayDescription.Text = obj.Description;
                IsOverlayActive.IsChecked = obj.IsActive;
                videoBGDisplayname = System.IO.Path.GetFileName(obj.DisplayName);
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }

        private void CmbThemeType_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (CmbThemeType.SelectedValue != null && Convert.ToInt16(CmbThemeType.SelectedValue) > 0)
            {
                Int32 selectedval = CmbThemeType.SelectedValue.ToInt32();
            }
        }

        private static bool IsFontvalueAllowed(string text)
        {
            Regex regex = new Regex(@"[^0-9.]+");//regex that matches disallowed text
            return !regex.IsMatch(text);
        }
        private void txtChineseFont_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = !IsFontvalueAllowed(e.Text);
        }

        private void txtEnglishFont_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = !IsFontvalueAllowed(e.Text);
        }

    }
}



namespace CustomPixelRender
{
    public class BrightContrastEffect : ShaderEffect
    {
        private static string executableLocation = System.IO.Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
        private static string xslLocation = System.IO.Path.Combine(executableLocation, "Shader\\bricon.ps");
        private static PixelShader m_shader =
            new PixelShader()
            {
                UriSource = new Uri(xslLocation)
            };

        public BrightContrastEffect()
        {
            PixelShader = m_shader;
            UpdateShaderValue(InputProperty);
            UpdateShaderValue(BrightnessProperty);
            UpdateShaderValue(ContrastProperty);
        }

        public Brush Input
        {
            get { return (Brush)GetValue(InputProperty); }
            set { SetValue(InputProperty, value); }
        }

        public static readonly DependencyProperty InputProperty = ShaderEffect.RegisterPixelShaderSamplerProperty("Input", typeof(BrightContrastEffect), 0);

        public float Brightness
        {
            get { return (float)GetValue(BrightnessProperty); }
            set { SetValue(BrightnessProperty, value); }
        }

        public static readonly DependencyProperty BrightnessProperty = DependencyProperty.Register("Brightness", typeof(double), typeof(BrightContrastEffect), new UIPropertyMetadata(0.0, PixelShaderConstantCallback(0)));

        public float Contrast
        {
            get { return (float)GetValue(ContrastProperty); }
            set { SetValue(ContrastProperty, value); }
        }

        public static readonly DependencyProperty ContrastProperty = DependencyProperty.Register("Contrast", typeof(double), typeof(BrightContrastEffect), new UIPropertyMetadata(0.0, PixelShaderConstantCallback(1)));



    }

}
