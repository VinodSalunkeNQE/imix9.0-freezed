﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using DigiPhoto.Common;
using DigiPhoto.IMIX.Business;
using DigiPhoto.IMIX.Model;
using DigiAuditLogger;
using DigiPhoto.Utility.Repository.ValueType;
using FrameworkHelper;
using DigiPhoto.Utility.Repository.Data;
namespace DigiPhoto.Manage
{
    /// <summary>
    /// Interaction logic for EmailStatus.xaml
    /// </summary>
    public partial class EmailStatus : Window
    {
        public int type = 1;
        List<EmailStatusInfo> _lstEmailStatus;
        public EmailStatus()
        {
            InitializeComponent();
            txbUserName.Text = LoginUser.UserName;
            txbStoreName.Text = LoginUser.StoreName;
            DateTime dt = (new CustomBusineses()).ServerDateTime().Date;
            dtFrom.DefaultValue = dt;
            dtFrom.Value = dt;
            dtTo.Value = (new CustomBusineses()).ServerDateTime();
            FillEmailTypeDropdown();
            GetPendingEmails();
            AllowForPhotoSend();
            chkALL.IsEnabled = true;
            chkALL.IsChecked = false;

        }

        private void btnBack_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                ManageHome _objHome = new ManageHome();
                _objHome.Show();
                this.Close();
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
            }
        }
        private void btnLogout_Click(object sender, RoutedEventArgs e)
        {
            try
            {

                AuditLog.AddUserLog(Common.LoginUser.UserId, (int)FrameworkHelper.ActionType.Logout, "Logged out at ");

                Login _objLogin = new Login();
                _objLogin.Show();
                this.Close();
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
            }
            finally
            {
                MemoryManagement.FlushMemory();
            }
        }
        private void btnSearch_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (BindGrid() == 0)
                {
                    //btnEmailSend.IsEnabled = false;
                    //btnEmailSendFailed.IsEnabled = false;
                    MessageBox.Show("There is no Record !!");
                }
                EnableDisableSendMail();
                //chkALL.IsEnabled = true;
                //chkALL.IsChecked = false;
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
            }
        }

        int BindGrid()
        {
                chkALL.IsChecked = false;
                DateTime dateFrom = dtFrom.Value != null ? dtFrom.Value.ToDateTime() : DateTime.Now.Date;
                DateTime dateTo = dtTo.Value != null ? dtTo.Value.ToDateTime() : DateTime.Now;
                EmailStatusInfo rfidEmailObj = new EmailStatusInfo()
                {
                    StartDate = dateFrom,
                    EndDate = dateTo,
                    Status = Convert.ToInt16(cmbEmailType.SelectedValue)
                };
                _lstEmailStatus = (new EmailBusniess()).GetEmailStatus(rfidEmailObj);
                _lstEmailStatus.ForEach(x => x.OrderId = x.OrderId.ToString().Substring(x.OrderId.LastIndexOf("\\") + 1).ToString());
                Dg_EmailStatus.ItemsSource = _lstEmailStatus;
            EnableDisableSendMail();
            return _lstEmailStatus.Count;
        }

        private void EnableDisableSendMail()
                {
            if (Dg_EmailStatus.Items.Count > 0)
            {
                btnEmailSend.IsEnabled = true;
                btnEmailSendFailed.IsEnabled = true;
                chkALL.IsEnabled = true;
                }
            else
            {
                btnEmailSend.IsEnabled = false;
                btnEmailSendFailed.IsEnabled = false;
                chkALL.IsEnabled = false;
            }
        }

        private void GetPendingEmails()
        {
            chkALL.IsChecked = false;
            DateTime dateFrom = dtFrom.Value != null ? dtFrom.Value.ToDateTime() : DateTime.Now.Date;
            DateTime dateTo = dtTo.Value != null ? dtTo.Value.ToDateTime() : DateTime.Now;
            EmailStatusInfo rfidEmailObj = new EmailStatusInfo()
            {
                StartDate = dateFrom,
                EndDate = dateTo,
                Status = Convert.ToInt16(cmbEmailType.SelectedValue)
            };
            _lstEmailStatus = (new EmailBusniess()).GetEmailStatus(rfidEmailObj);
            _lstEmailStatus.ForEach (x=>x.OrderId = x.OrderId.ToString().Substring(x.OrderId.LastIndexOf("\\") + 1).ToString());


           
            Dg_EmailStatus.ItemsSource = _lstEmailStatus;

        }
        private void cmbEmailType_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                Dg_EmailStatus.ItemsSource = null;
                Dg_EmailStatus.Items.Refresh();
                //chkALL.IsEnabled = false;
                //chkALL.IsChecked = false;
                AllowForPhotoSend();
                if (type != 1)
                    EnableDisableSendMail();
                type += 1;
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
            }
        }
        private void btnEmailSend_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                //SendType 1=All , 0=Failed
                int SendType = 0;
                Button _objButton = (Button)(sender);
                switch (_objButton.Name)
                {
                    case "btnEmailSend":
                        {
                            SendType = 1;
                            break;
                        }
                    case "btnEmailSendFailed":
                        {
                            SendType = 0;
                            break;
                        }
                }
                Boolean flag = true;
                StringBuilder EmailId=new StringBuilder();
                StringBuilder OrderId = new StringBuilder();
                //if (Dg_EmailStatus.Items.Count > 0)
                //{
                EmailId.Clear();
                foreach (var item in _lstEmailStatus)
                {
                    if (item.IsAvailable == true)
                    {
                        EmailId.Append(item.DG_Email_pkey + ",");
                        OrderId.Append(item.OrderId + ",");
                    }
                }
                if (EmailId.Length > 0)
                {
                    flag = (new EmailBusniess()).ResendEmail(EmailId.ToString().Substring(0, EmailId.Length - 1), SendType);
                    if (flag == true)
                    {
                        AuditLog.AddUserLog(Common.LoginUser.UserId, (int)FrameworkHelper.ActionType.ResendEmail, "Resend email for the order  " + OrderId.ToString().Substring(0, OrderId.Length - 1) + " .");
                        MessageBox.Show("Email Re-Initiated Successfully");
                        BindGrid();
                    }
                }
                else
                {
                    MessageBox.Show("No record selected");
                }

            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
            }
        }
        private void btnDetails_Click(object sender, RoutedEventArgs e)
        {
            try
            {

            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
            }
        }
        private void chkALL_Checked(object sender, RoutedEventArgs e)
        {
            try
            {
                _lstEmailStatus.ForEach(z => z.IsAvailable = true);
                Dg_EmailStatus.ItemsSource = _lstEmailStatus;
                Dg_EmailStatus.Items.Refresh();
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
            }
        }
        private void chkALL_Unchecked(object sender, RoutedEventArgs e)
        {
            try
            {
                _lstEmailStatus.ForEach(z => z.IsAvailable = false);
                Dg_EmailStatus.ItemsSource = _lstEmailStatus;
                Dg_EmailStatus.Items.Refresh();
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
            }
        }
        private void FillEmailTypeDropdown()
        {
            cmbEmailType.Items.Clear();
            cmbEmailType.ItemsSource = RobotImageLoader.GetEmailType();
            cmbEmailType.SelectedValue = "10";
        }
        private void AllowForPhotoSend()
        {
            if (cmbEmailType.SelectedValue == "2")
            {
                btnEmailSend.Width = 265;
                btnEmailSend.Content = "Send Email for all photos";
                btnEmailSendFailed.Content = "Send Email for failed photos";
                btnEmailSendFailed.Visibility = Visibility.Visible;
            }
            else
            {
                btnEmailSend.Width = 127;
                btnEmailSend.Content = "Send Email";
                btnEmailSendFailed.Visibility = Visibility.Collapsed;
            }
        }
    }
}
