﻿using DigiAuditLogger;
using DigiPhoto.Common;
using DigiPhoto.DataLayer;
using DigiPhoto.DataLayer.Model;
using DigiPhoto.IMIX.Business;
using DigiPhoto.IMIX.Model;
using DigiPhoto.Utility.Repository.ValueType;
using FrameworkHelper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Media;

namespace DigiPhoto.Manage
{
    /// <summary>
    /// Interaction logic for UserPermission.xaml
    /// </summary>
    public partial class UserPermission : Window
    {
        #region Declaration
        // DigiPhotoDataServices _objDataLayer = new DigiPhotoDataServices();
        private List<RoleInfo> _objRoleList;
        private List<PermisiionList> _objPerList;
        private int _roleId = 0;
        #endregion

        #region constructor
        public UserPermission()
        {
            InitializeComponent();
            GetRoledata();
            GetDropdownData();
            txtUserRole.Text = string.Empty;
            _roleId = 0;
            txbUserName.Text = LoginUser.UserName;
            txbStoreName.Text = LoginUser.StoreName;
        }
        #endregion

        #region Events
        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                string syncCode = CommonUtility.GetUniqueSynccode(Convert.ToInt32(ApplicationObjectEnum.Role).ToString().PadLeft(2, '0'), LoginUser.countrycode, LoginUser.Storecode, "00");
                //string SyncCode = CommonUtility.GetRandomString(8) + Convert.ToInt32(ApplicationObjectEnum.Role).ToString().PadLeft(2, '0');
                if (!string.IsNullOrEmpty(txtUserRole.Text))
                {
                    RolePermissionsBusniess rolBiz = new RolePermissionsBusniess();
                    new System.Windows.Media.SolidColorBrush(Colors.Gray);
                    string strmsg = rolBiz.AddUpdateRoleData(_roleId, txtUserRole.Text, syncCode,LoginUser.RoleId);
                    if (strmsg == "Saved")
                    {
                        GetRoledata();
                        GetDropdownData();
                        txtUserRole.Text = string.Empty;
                        _roleId = 0;
                        MessageBox.Show("Record saved successfully!");
                        AuditLog.AddUserLog(Common.LoginUser.UserId, (int)FrameworkHelper.ActionType.CreateRole, "Role Add/Edit at ");
                    }
                    else if (strmsg == "Duplicate")
                    {
                        MessageBox.Show("Record already exists!");
                    }
                }
                else
                {
                    MessageBox.Show("Please enter the Role.");
                    new System.Windows.Media.SolidColorBrush(Colors.Red);
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
            }

        }
        private void btnRoleEdit_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Button _objbtn = (Button)(sender);
                _roleId = _objbtn.CommandParameter.ToInt32();
                txtUserRole.Text = _objRoleList.Where(t => t.DG_User_Roles_pkey == _roleId).FirstOrDefault().DG_User_Role;
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
            }

        }

        private void btnRoleDelete_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (MessageBox.Show("Do you want to delete this role?", "Confirm delete", MessageBoxButton.YesNo) == MessageBoxResult.Yes)
                {
                    Button _objbtn = (Button)(sender);
                    _roleId = _objbtn.CommandParameter.ToInt32();
                    RolePermissionsBusniess rolBiz = new RolePermissionsBusniess();
                    AuditLog.AddUserLog(Common.LoginUser.UserId, (int)FrameworkHelper.ActionType.DeleteRole, "Deleted Role-" + rolBiz.GetRoleName(_roleId));
                    if (rolBiz.DeleteRoleData(_roleId))
                    {
                        GetRoledata();
                        GetDropdownData();
                        txtUserRole.Text = string.Empty;
                        _roleId = 0;
                    }
                    else
                    {
                        MessageBox.Show("Reocrd is in use");
                    }
                }

            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
            }
        }


        #endregion

        #region Common Methods
        private void GetRoledata()
        {
            //List<DG_User_Roles> _objlst = new List<DG_User_Roles>();
            RolePermissionsBusniess rolBiz = new RolePermissionsBusniess();
            //var _objRoleList = rolBiz.GetRoleNames(LoginUser.RoleId, string.Empty);
            _objRoleList = (new RolePermissionsBusniess()).GetChildUserData(LoginUser.RoleId).Where(o => o.DG_User_Roles_pkey != LoginUser.RoleId).ToList() ;
            grdUserRoles.ItemsSource = _objRoleList;
            

        }
        private void GetDropdownData()
        {
            var objRoles = new List<RoleInfo>();
            objRoles.AddRange(_objRoleList);
            CommonUtility.BindComboWithSelect<RoleInfo>(cmbUserRole, objRoles, "DG_User_Role", "DG_User_Roles_pkey", 0, DigiPhoto.Utility.Repository.Data.ClientConstant.SelectString);
            cmbUserRole.SelectedValue = "0";
        }
        //private void GetPermissionDataforGrid()
        //{
        //    RolePermissionsBusniess rolBiz = new RolePermissionsBusniess();
        //    _objPerList = new List<PermisiionList>();
        //    var itemList = rolBiz.GetPermissionData(cmbUserRole.SelectedValue.ToInt32());
        //    if (itemList.Count == 0)
        //    {
        //        var PermissionNameList = rolBiz.GetPermissionNames(0, string.Empty);
        //        foreach (var item in PermissionNameList)
        //        {
        //            PermisiionList _objPLP = new PermisiionList();
        //            _objPLP.IsAvailable = false;
        //            _objPLP.PermissionName = item.DG_Permission_Name;
        //            _objPLP.PermissionId = item.DG_Permission_pkey;
        //            _objPerList.Add(_objPLP);
        //        }
        //    }
        //    else
        //    {
        //        var PermissonList = rolBiz.GetPermissionNames(0, string.Empty);
        //        List<int> _objPLst = itemList.Select(t => t.DG_Permission_Id).ToList();
        //        foreach (var item in PermissonList)
        //        {
        //            PermisiionList _objPLP = new PermisiionList();
        //            if (_objPLst.Contains(item.DG_Permission_pkey))
        //            {
        //                _objPLP.PermissionId = item.DG_Permission_pkey;
        //                _objPLP.IsAvailable = true;
        //                _objPLP.PermissionName = item.DG_Permission_Name;
        //            }
        //            else
        //            {
        //                _objPLP.PermissionId = item.DG_Permission_pkey;
        //                _objPLP.IsAvailable = false;
        //                _objPLP.PermissionName = item.DG_Permission_Name;
        //            }
        //            _objPerList.Add(_objPLP);
        //        }
        //    }
        //    grdPermission.ItemsSource = _objPerList;
        //}
        #endregion

        /// <summary>
        /// Handles the Click event of the btnPermissionSave control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void btnPermissionSave_Click(object sender, RoutedEventArgs e)
        {
            RolePermissionsBusniess rolBiz = new RolePermissionsBusniess();
            string itemsadded = string.Empty;
            string itemsremoved = string.Empty;
            try
            {
                if (cmbUserRole.SelectedValue.ToString() == "0")
                {
                    MessageBox.Show("Please select the Role");
                }
                else
                {
                    DataTable udt_Permission = new DataTable();
                    udt_Permission.Columns.Add("RoleId", typeof(string));
                    udt_Permission.Columns.Add("PermissonId", typeof(string));
                    udt_Permission.Columns.Add("IsAvailable", typeof(bool));
                    foreach (var item in _objPerList)
                    {
                        DataRow dr = udt_Permission.NewRow();
                        dr["RoleId"] = cmbUserRole.SelectedValue.ToInt32();
                        dr["PermissonId"] = item.PermissionId;
                        dr["IsAvailable"] = item.IsAvailable;
                        udt_Permission.Rows.Add(dr);
                    }
                    itemsadded = string.Join(",", _objPerList.Where(a => a.IsAvailable == true).Select(c => c.PermissionName));
                    itemsremoved = string.Join(",", _objPerList.Where(a => a.IsAvailable == false).Select(c => c.PermissionName));

                        //if (item.IsAvailable == true)
                        //{
                        //    ////Add to RolePermission Table
                        //    DataRow dr = udt_Permission.NewRow();
                        //    dr["RoleId"] = cmbUserRole.SelectedValue.ToInt32();
                        //    dr["PermissonId"] = item.PermissionId;
                        //    dr["IsAvailable"] = true;
                        //    if (itemsadded == string.Empty)
                        //        itemsadded = item.PermissionName;
                        //    else
                        //        itemsadded += ", " + item.PermissionName;
                        //    udt_Permission.Rows.Add(dr);
                        //}
                        //else
                        //{
                        //    ////remove from  RolePermission Table
                        //    DataRow dr = udt_Permission.NewRow();
                        //    dr["RoleId"] = cmbUserRole.SelectedValue.ToInt32();
                        //    dr["PermissonId"] = item.PermissionId;
                        //    dr["IsAvailable"] = false;
                        //    if (itemsremoved == string.Empty)
                        //        itemsremoved = item.PermissionName;
                        //    else
                        //        itemsremoved += ", " + item.PermissionName;
                        //    udt_Permission.Rows.Add(dr);
                        //}
                    //}

                    //foreach (var item in _objPerList)
                    //{
                    //    if (item.IsAvailable == true)
                    //    {
                    //        ////Add to RolePermission Table
                    //        if (rolBiz.SetPermissionData(cmbUserRole.SelectedValue.ToInt32(), item.PermissionId))
                    //        {
                    //            if (itemsadded == string.Empty)
                    //            {
                    //                itemsadded = item.PermissionName;
                    //            }
                    //            else
                    //            {
                    //                itemsadded += ", " + item.PermissionName;
                    //            }
                    //        }
                    //    }
                    //    else
                    //    {
                    //        ////remove from  RolePermission Table

                    //        if (rolBiz.RemovePermissionData(cmbUserRole.SelectedValue.ToInt32(), item.PermissionId))
                    //        {
                    //            if (itemsremoved == string.Empty)
                    //            {
                    //                itemsremoved = item.PermissionName;
                    //            }
                    //            else
                    //            {
                    //                itemsremoved += ", " + item.PermissionName;
                    //            }
                    //        }
                    //    }
                    //}
                    if (new RolePermissionsBusniess().SetremovePermissionData(udt_Permission))
                    {
                        if (!(itemsadded == string.Empty))
                            AuditLog.AddUserLog(Common.LoginUser.UserId, (int)FrameworkHelper.ActionType.AddPermission, "Added Permission- " + itemsadded + " to " + rolBiz.GetRoleName(cmbUserRole.SelectedValue.ToInt32()));
                        if (!(itemsremoved == string.Empty))
                            AuditLog.AddUserLog(Common.LoginUser.UserId, (int)FrameworkHelper.ActionType.DeletePermission, "Removed Permission- " + itemsremoved + " from " + rolBiz.GetRoleName(cmbUserRole.SelectedValue.ToInt32()));
                        MessageBox.Show("Records updated successfully!");
                        //GetPermissionDataforGrid();
                        GetPermissionDataforGridIsChecked(false, false);
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
            }
        }
        /// <summary>
        /// Handles the SelectionChanged event of the cmbUserRole control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="SelectionChangedEventArgs"/> instance containing the event data.</param>
        private void cmbUserRole_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                //GetPermissionDataforGrid();
                GetPermissionDataforGridIsChecked(false, false);
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
            }

        }

        /// <summary>
        /// Handles the Click event of the btnLogout control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void btnLogout_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                AuditLog.AddUserLog(Common.LoginUser.UserId, (int)FrameworkHelper.ActionType.Logout, "Logged out at ");
                Login _objLogin = new Login();
                _objLogin.Show();
                this.Close();
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
            }
        }

        /// <summary>
        /// Handles the Click event of the btnback control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void btnback_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                AddEditUsers _objaddeditusers = new AddEditUsers();
                _objaddeditusers.Show();
                this.Close();
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
            }
        }

        /// <summary>
        /// Handles the Checked event of the chkALL control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void chkALL_Checked(object sender, RoutedEventArgs e)
        {
            try
            {
                GetPermissionDataforGridIsChecked(true, true);
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
            }
        }

        /// <summary>
        /// Handles the Unchecked event of the chkALL control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void chkALL_Unchecked(object sender, RoutedEventArgs e)
        {
            try
            {
                GetPermissionDataforGridIsChecked(false, true);
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
            }

        }
        /// <summary>
        /// Gets the permission datafor grid is checked.
        /// </summary>
        /// <param name="isAllcheecked">if set to <c>true</c> [is allcheecked].</param>
        private void GetPermissionDataforGridIsChecked(bool isAllcheecked, bool isChecked)
        {
            RolePermissionsBusniess rolBiz = new RolePermissionsBusniess();
            _objPerList = new List<PermisiionList>();
            var itemList = rolBiz.GetPermissionData(cmbUserRole.SelectedValue.ToInt32());
            string totalRole = LoginUser.RoleId.ToString() + "," + cmbUserRole.SelectedValue;
            if (itemList.Count == 0)
            {
                //var PermissionNameList = rolBiz.GetPermissionNames(0, string.Empty);
                var PermissionNameList = rolBiz.GetPermissionNames(totalRole, string.Empty);
                foreach (var item in PermissionNameList)
                {
                    PermisiionList _objPLP = new PermisiionList();
                    if (isChecked)
                        _objPLP.IsAvailable = isAllcheecked;
                    else
                        _objPLP.IsAvailable = false;
                    _objPLP.PermissionName = item.DG_Permission_Name;
                    _objPLP.PermissionId = item.DG_Permission_pkey;
                    _objPerList.Add(_objPLP);
                }
            }
            else
            {
                //var PermissonList = rolBiz.GetPermissionNames(0, string.Empty);
                var PermissonList = rolBiz.GetPermissionNames(totalRole, string.Empty);
                List<int> _objPLst = itemList.Select(t => t.DG_Permission_Id).ToList();
                foreach (var item in PermissonList)
                {
                    PermisiionList _objPLP = new PermisiionList();
                    if (_objPLst.Contains(item.DG_Permission_pkey))
                    {
                        _objPLP.PermissionId = item.DG_Permission_pkey;
                        if (isChecked)
                            _objPLP.IsAvailable = isAllcheecked;
                        else
                            _objPLP.IsAvailable = true;
                        _objPLP.PermissionName = item.DG_Permission_Name;
                    }
                    else
                    {
                        _objPLP.PermissionId = item.DG_Permission_pkey;
                        if (isChecked)
                            _objPLP.IsAvailable = isAllcheecked;
                        else
                            _objPLP.IsAvailable = false;
                        _objPLP.PermissionName = item.DG_Permission_Name;
                    }
                    _objPerList.Add(_objPLP);
                }
            }
            grdPermission.ItemsSource = _objPerList;
        }


    }
    #region Custom class
    //class PermisiionList
    //{
    //    public bool? IsAvailable { get; set; }
    //    public string PermissionName { get; set; }
    //    public int PermissionId { get; set; }
    //}
    #endregion
}
