﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Collections.ObjectModel;
using System.Printing;
using DigiPhoto.Common;
using DigiAuditLogger;
using System.Timers;
using System.Windows.Threading;
using System.Globalization;
using DigiPhoto.DataLayer.Model;
using DigiPhoto.DataLayer;
using System.Management;
using DigiPhoto.IMIX.Business;
using DigiPhoto.IMIX.Model;
using DigiPhoto.Utility.Repository.ValueType;
using System.Data;
using FrameworkHelper;

namespace DigiPhoto.Manage
{
    /// <summary>
    /// Interaction logic for SystemPrinterQueuee.xaml
    /// </summary>
    public partial class SystemPrinterQueuee : Window
    {
        ObservableCollection<PrinterDetails> myprinterdetails { get; set; }
        DispatcherTimer _objmytimer;
        public SystemPrinterQueuee()
        {
            InitializeComponent();
            txbUserName.Text = LoginUser.UserName;
            txbStoreName.Text = LoginUser.StoreName;
        }

        /// <summary>
        /// Handles the Tick event of the _objmytimer control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void _objmytimer_Tick(object sender, EventArgs e)
        {
            try
            {
                _objmytimer.Stop();
                GetPrintersName();
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
            }
            finally
            {
                _objmytimer.Start();
            }
        }

        /// <summary>
        /// Handles the Click event of the btnLogout control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void btnLogout_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                AuditLog.AddUserLog(Common.LoginUser.UserId, (int)FrameworkHelper.ActionType.Logout, "Logged out at ");
                Login _objLogin = new Login();
                _objLogin.Show();
                this.Close();
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
            }
        }

        /// <summary>
        /// Gets the name of the printers.
        /// </summary>
        public void GetPrintersName()
        {
            var server = new PrintServer();
            var queues = server.GetPrintQueues(new[] { EnumeratedPrintQueueTypes.Local, EnumeratedPrintQueueTypes.Connections });
            myprinterdetails = new ObservableCollection<PrinterDetails>();

            List<AssociatedPrintersInfo> objlist = (new PrinterBusniess()).GetAssociatedPrintersName(LoginUser.SubStoreId);
            //List<AssociatedPrintersInfo> _objlist = new PrinterBusniess().GetAssociatedPrintersName(1);
            IEnumerable<string> _objprname = objlist.Select(t => t.DG_AssociatedPrinters_Name).Distinct();
            for (int i = 0; i < _objprname.Count(); i++)
            {
                var item = queues.Where(t => t.FullName == (_objprname.ToArray())[i].ToString()).FirstOrDefault();
                if (item != null)
                {
                    myprinterdetails.Add(new PrinterDetails(item.FullName, GetPrintersJobs(item.FullName), getPrinterStatus(item.FullName)));
                }
            }
            mylist.ItemsSource = myprinterdetails;
        }

        /// <summary>
        /// Spots the trouble using queue attributes.
        /// </summary>
        /// <param name="pq">The pq.</param>
        /// <returns></returns>
        //public string SpotTroubleUsingQueueAttributes(PrintQueue pq)
        //{
        //    string statusReport = "";
        //    if (pq.HasPaperProblem)
        //    {
        //        statusReport = statusReport + "Has a paper problem. ";
        //    }
        //    if (!(pq.HasToner))
        //    {
        //        statusReport = statusReport + "Is out of toner. ";
        //    }
        //    if (pq.IsDoorOpened)
        //    {
        //        statusReport = statusReport + "Has an open door. ";
        //    }
        //    if (pq.IsInError)
        //    {
        //        statusReport = statusReport + "Is in an error state. ";
        //    }
        //    if (pq.IsNotAvailable)
        //    {
        //        statusReport = statusReport + "Is not available. ";
        //    }
        //    if (pq.IsOffline)
        //    {
        //        statusReport = statusReport + "Is off line. ";
        //    }
        //    if (pq.IsOutOfMemory)
        //    {
        //        statusReport = statusReport + "Is out of memory. ";
        //    }
        //    if (pq.IsOutOfPaper)
        //    {
        //        statusReport = statusReport + "Is out of paper. ";
        //    }
        //    if (pq.IsOutputBinFull)
        //    {
        //        statusReport = statusReport + "Has a full output bin. ";
        //    }
        //    if (pq.IsPaperJammed)
        //    {
        //        statusReport = statusReport + "Has a paper jam. ";
        //    }
        //    if (pq.IsPaused)
        //    {
        //        statusReport = statusReport + "Is paused. ";
        //    }
        //    if (pq.IsTonerLow)
        //    {
        //        statusReport = statusReport + "Is low on toner. ";
        //    }
        //    if (pq.NeedUserIntervention)
        //    {
        //        statusReport = statusReport + "Needs user intervention. ";
        //    }


        //    // Check if queue is even available at this time of day
        //    // The method below is defined in the complete example.
        //    return statusReport;
        //}


        public string getPrinterStatus(string PrinterName)
        {
            // Set management scope
            string retvalue = "";
            ManagementScope scope = new ManagementScope("\\root\\cimv2");
            scope.Connect();

            // Select Printers from WMI Object Collections
            ManagementObjectSearcher searcher = new ManagementObjectSearcher("SELECT * FROM Win32_Printer");
            string printerName = "";
            var results = searcher.Get();
            string[] PrinterStatuses = 
            {
                "Other", "Unknown", "Idle", "Printing", "WarmUp", "Stopped Printing", "Offline"
            };

            foreach (ManagementObject printer1 in results)
            {
                printerName = printer1["Name"].ToString().ToLower();
                if (printerName.Equals(PrinterName.ToLower()))
                {
                    Console.WriteLine("Printer = " + printer1["Name"]);
                    if (printer1["WorkOffline"].ToString().ToLower().Equals("true"))
                    {
                        // printer is offline by user
                        retvalue = "Offline";
                    }
                    else
                    {
                        // printer is not offline
                        Int32 status = printer1["PrinterStatus"].ToInt32();
                        string printerStatus = PrinterStatuses[status];
                        if (status == Convert.ToInt32(PrinterStatus.Other) || status == Convert.ToInt32(PrinterStatus.Offline))
                        {
                            retvalue = "Error";
                        }
                        else
                        {
                            retvalue = "Online";
                        }
                    }
                }
            }
            return retvalue;
        }

       public class PrinterDetails
        {
            /// <summary>
            /// Initializes a new instance of the <see cref="PrinterDetails"/> class.
            /// </summary>
            /// <param name="printername">The printername.</param>
            /// <param name="printerjOb">The printerj object.</param>
            /// <param name="questatus">The questatus.</param>
            public PrinterDetails(string printername, ObservableCollection<PrinterJobInfo> printerjOb, string questatus)
            {
                this.PrinterName = printername + " (" + questatus + ")";
                this.PrinterJOb = printerjOb;
                this.PrinterStatus = questatus;
            }
            public string PrinterName { get; set; }
            public ObservableCollection<PrinterJobInfo> PrinterJOb { get; set; }
            public string PrinterStatus { get; set; }


        }
        public ObservableCollection<PrinterJobInfo> GetPrintersJobs(string printerName)
        {
            ObservableCollection<PrinterJobInfo> lstPrinterjob = new ObservableCollection<PrinterJobInfo>();
            var server = new PrintServer();
            var queues = server.GetPrintQueues(new[] { EnumeratedPrintQueueTypes.Local, EnumeratedPrintQueueTypes.Connections });
            var printitem = queues.Where(t => t.FullName == printerName).FirstOrDefault();
            printitem.Refresh();
            //var printerJobTable = CommonUtility.ToDataTable<PrintSystemJobInfo>(printitem.GetPrintJobInfoCollection());
            PrintJobInfoCollection jobscol = printitem.GetPrintJobInfoCollection();
           
            DataTable udt_PrintJobInfo = new DataTable();
            udt_PrintJobInfo.Columns.Add("JobName", typeof(string));
            udt_PrintJobInfo.Columns.Add("JobStatus", typeof(string));
            udt_PrintJobInfo.Columns.Add("JobIdentifier", typeof(Int32));
            foreach (var jobs in jobscol)
            {
                DataRow dr = udt_PrintJobInfo.NewRow();
                dr["JobName"] = jobs.Name;
                dr["JobStatus"] = jobs.JobStatus.ToString();
                dr["JobIdentifier"] = jobs.JobIdentifier;
                udt_PrintJobInfo.Rows.Add(dr);
            }
            lstPrinterjob = (new PrinterBusniess()).GetPrinterJobInfo(udt_PrintJobInfo, printerName,LoginUser.DigiFolderThumbnailPath);
            printitem = null;
            queues = null;
            return lstPrinterjob;
        }


        /// <summary>
        /// Handles the Loaded event of the Window control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                GetPrintersName();
                _objmytimer = new DispatcherTimer();
                _objmytimer.Interval = new TimeSpan(0, 0, 2);
                _objmytimer.Tick += new EventHandler(_objmytimer_Tick);
                _objmytimer.Start();
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
            }
        }

        private void btnBack_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                _objmytimer.Stop();
                ManageHome _objhome = new ManageHome();
                _objhome.Show();
                this.Close();
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
            }
        }

        //private void btnDelete_Click(object sender, RoutedEventArgs e)
        //{

        //}

        //private void btnDelete_Click_1(object sender, RoutedEventArgs e)
        //{

        //}

        /// <summary>
        /// Handles the CanExecute event of the CommandBinding control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="CanExecuteRoutedEventArgs"/> instance containing the event data.</param>
        private void CommandBinding_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            try
            {
                e.CanExecute = true;
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
            }
        }

        /// <summary>
        /// Handles the Executed event of the CommandBinding control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="ExecutedRoutedEventArgs"/> instance containing the event data.</param>
        private void CommandBinding_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            try
            {
                var server = new PrintServer();
                var queues = server.GetPrintQueues(new[] { EnumeratedPrintQueueTypes.Local, EnumeratedPrintQueueTypes.Connections });
                var printitem = queues.Where(t => t.FullName == ((FindCommandParameters)e.Parameter).Text).FirstOrDefault();
                printitem.Refresh();
                PrintJobInfoCollection jobscol = printitem.GetPrintJobInfoCollection();
                var jobitem = printitem.GetPrintJobInfoCollection().Where(t => t.JobIdentifier == (((FindCommandParameters)e.Parameter).IgnoreCase).ToInt32()).FirstOrDefault();
                jobitem.Cancel();
                printitem.Refresh();
                var QDetails = (new PrinterBusniess()).GetQueueDetail(jobitem.Name.ToInt32());
                (new PrinterBusniess()).SetPrinterQueueForReprint(jobitem.Name.ToInt32());
                (new OrderBusiness()).SetOrderDetailsForReprint(QDetails.DG_Orders_LineItems_pkey, LoginUser.SubStoreId);
                AuditLog.AddUserLog(Common.LoginUser.UserId, (int)FrameworkHelper.ActionType.MovePrint, "Move Print" + QDetails.DG_Orders_ProductType_Name.ToString() + " ( image no:" + QDetails.DG_Photos_RFID.ToString() + " of Order No " + QDetails.DG_Orders_Number.ToString() + " from substore " + LoginUser.SubstoreName);
                GetPrintersName();

            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
            }
        }

        /// <summary>
        /// Handles the Click event of the btnReprint control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void btnReprint_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                ReprintCtrl.SetParent(this);
                ReprintCtrl.dgReprint.Visibility = Visibility.Collapsed;
                var res = ReprintCtrl.ShowHandlerDialog();
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
            }
        }

    }
    public class Commands
    {
        public static RoutedCommand EditURI = new RoutedCommand("EditURI", typeof(Commands));
    }
    public class FindCommandParametersConverter : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            FindCommandParameters parameters = new FindCommandParameters();
            parameters.Text = values[0].ToString();
            parameters.IgnoreCase = values[1].ToString();
            return parameters;
        }
        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
    public class FindCommandParameters
    {
        public string Text { get; set; }
        public string IgnoreCase { get; set; }
    }
}
