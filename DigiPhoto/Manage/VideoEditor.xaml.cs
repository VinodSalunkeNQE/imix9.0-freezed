﻿using DigiPhoto.DataLayer;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Drawing.Imaging;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.IO;
using DigiPhoto.Common;
using FrameworkHelper;
using DigiPhoto.IMIX.Model;
using DigiPhoto.IMIX.Business;
using System.Windows.Threading;
using System.Windows.Controls.Primitives;
using System.Xml;
using System.Xml.Linq;
using WMPLib;
using DigiPhoto.Interop;
using System.Data;
using System.Threading;
using System.Diagnostics;
using System.Runtime.InteropServices;
using Application = System.Windows.Application;
using Color = System.Windows.Media.Color;
using HorizontalAlignment = System.Windows.HorizontalAlignment;
using MessageBox = System.Windows.MessageBox;
using System.Windows.Media.Animation;
using DigiPhoto.Shader;
using MPLATFORMLib;
using MCHROMAKEYLib;
using MLCHARGENLib;
using MControls;
using CGEditor.CGItemWrappers;
using System.Collections;
//using MCOLORSLib;
using FrameworkHelper.Common;
using MCOLORSLib;
//using MCOLORSLib;
//using System.Drawing;

namespace DigiPhoto.Manage
{
    /// <summary>
    /// Interaction logic for VideoEditor.xaml
    /// </summary>
    public partial class VideoEditor : Window
    {
        #region LocalVariable
        public int DropPhotoId = 0;
        #region TemplateList

        public List<TemplateListItems> objTemplateList = new List<TemplateListItems>();
        #endregion
        List<VideoElements> lstVideoElements = new List<VideoElements>();
        List<VideoFrames> lstVideoFrames = new List<VideoFrames>();
        ProcessedVideoInfo objProcVideoInfo = new ProcessedVideoInfo();
        public string IsGoupped;
        string processVideoTemp = Environment.CurrentDirectory + "\\DigiProcessVideoTemp\\";
        Dictionary<string, int> listValueType;
        //Template Type
        string templateType = "chroma";
        List<VideoProducts> lstVideoProduct = new List<VideoProducts>();
        ProcessedVideoInfo lstProcessedVideo = new ProcessedVideoInfo();
        bool IsProcessedVideoSaved = true;
        bool IsProcessedVideoEditing = false;
        int ProcessedVideoID = 0;
        bool isCreateNew = false;
        List<VideoPage> lstVideoPage;
        int productId = 0;
        public List<VideoTemplateInfo.VideoSlot> slotList = new List<VideoTemplateInfo.VideoSlot>();
        BusyWindow bs = new BusyWindow();
        string outputFormat = "mp4";
        private FileStream memoryFileStream;
        public static string vsMediaFileName = "";
        //timer
        private readonly DispatcherTimer timer = new DispatcherTimer();
        public int defaultProcessListCount = 10;
        bool isVideoTempSelected = false;
        int activePage = 0;
        //Group Flag
        bool isGroup = false;
        DateTime lastmemoryUpdateTime = DateTime.Now;
        int folderCount = 0;
        bool AutomatedVideoEditWorkFlow = false;
        BackgroundWorker bwSaveVideos = new BackgroundWorker();
        string tempFile; string path_tempThumbnail;
        int Vidlength = 0;
        #endregion


        string m_strItemID = "";
        public MLCHARGENLib.CoMLCharGen m_objCharGen = new MLCHARGENLib.CoMLCharGen();
        CGEditor_WinForms.MainWindow cgEditor = new CGEditor_WinForms.MainWindow();
        ConfigBusiness objConfigBL;
        CGConfigSettings objCGConfig;
        Dictionary<int, string> dicConfig;
        private UIElement elementForContextMenu;
        double numericTimeForChange = 0.0;
        private CoMColorsClass m_objColors;


        #region Constructor
        public VideoEditor()
        {
            try
            {
                InitializeComponent();
                isGroup = RobotImageLoader.isGroup;
                slotList.Clear();
                PrintOrderPageList.Clear();
                lstVideoElements.Clear();
                lstDragImages.Items.Clear();
                objTemplateList.Clear();
                listValueType = LoadValueTypeList();
                mplayer = new MLMediaPlayer(vsMediaFileName, "VideoEditor");
                LoadGuestImageList();
                // ChangeGuestElement = false;
                BindPageStrips();
                // FillProductCombo();
                ShowHideControls("video");
                LoadTemplateList();
                BindCGConfigCombo();
                templateType = "chroma";
                MsgBox.SetParent(OuterBorder);
                CommonUtility.CleanFolder(System.Environment.CurrentDirectory, new string[] { "CroppedImage" });
                ConfigManager.SubStoreId = LoginUser.SubStoreId;
                viewControl.SetParent(OuterBorder);
                FrameBox.ExecuteMethod += new EventHandler(FrameBox_ExecuteMethod);
                if (!System.IO.Directory.Exists(processVideoTemp))
                {
                    System.IO.Directory.CreateDirectory(processVideoTemp);
                }
                m_objColors = new CoMColorsClass();


                #region MediaLooks variables
                HighlightSelectedButton("btnVideo");
                btnSave.IsEnabled = false;
                m_objMixer = new MMixerClass();
                mPreviewControlMixer1.SetControlledObject(m_objMixer);
                mMixerList1.SetControlledObject(m_objMixer);
                mMixerList1.OnMixerSelChanged += new EventHandler(mMixerList1_OnMixerSelChanged);
                m_objMixer.ObjectStart(null);
                VidTimer = new DispatcherTimer();
                VidTimer.Tick += new EventHandler(VidTimer_Tick);
                m_objWriter = new MWriterClass();
                mFormatControl1.SetControlledObject(m_objMixer);
                mElementsTree1.SetControlledObject(m_objMixer);
                mFormatControl1.comboBoxVideo.SelectedIndex = 0;
                mFormatControl1.comboBoxAudio.SelectedIndex = 0;
                mConfigList1.SetControlledObject(m_objWriter);
                mConfigList1.OnConfigChanged += new EventHandler(mConfigList1_OnConfigChanged);
                bwSaveVideos.DoWork += bwSaveVideos_DoWork;
                bwSaveVideos.RunWorkerCompleted += bwSaveVideos_RunWorkerCompleted;
                if (mPreviewControlMixer1.m_pPreview != null)
                    mPreviewControlMixer1.m_pPreview.PreviewEnable("", 0, 1);
                mConfigList1_OnConfigChanged(null, EventArgs.Empty);
                m_objMixer.PluginsAdd(m_objColors, 0);

                //cgEditor.SetSourceObject(m_objMixer, m_objCharGen);
                #endregion MediaLooks
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }

        private void LoadDefaultSceneAndFile()
        {
            VideoEditPlayerPanel.Visibility = Visibility.Collapsed;
            loadScene(0);
            LoadFirstItemOnMixer();
        }
        string strGuestStreamID = string.Empty;
        bool IsLoadChroma = false;
        private void LoadFirstItemOnMixer()
        {
            VideoEditPlayerPanel.Visibility = Visibility.Visible; ;
            var firstImgDetail = lstVideoElements.FirstOrDefault();
            string inputfile = string.Empty;
            if (AutomatedVideoEditWorkFlow)
            {
                if (firstImgDetail.MediaType == 1 && File.Exists(firstImgDetail.VideoFilePath.Replace("jpg", "png")))
                {
                    firstImgDetail.VideoFilePath = firstImgDetail.VideoFilePath.Replace("jpg", "png");
                    IsLoadChroma = false;
                }
                else
                    IsLoadChroma = true;
            }
            SetFirstGuestStream(firstImgDetail.VideoFilePath);
        }

        private void SetFirstGuestStream(string filepath)
        {
            MItem pFile;
            if (m_objMixer != null)
            {
                m_pMixerStreams = (IMStreams)m_objMixer;
                int count = 0; string streamID = string.Empty;
                if (string.IsNullOrEmpty(strGuestStreamID))
                {
                    m_pMixerStreams.StreamsAdd("", null, filepath, "loop=false", out pFile, 0);
                    m_pMixerStreams.StreamsGetCount(out count);
                    m_pMixerStreams.StreamsGetByIndex(count - 1, out streamID, out pFile);
                    strGuestStreamID = streamID;
                }
                else
                {
                    m_pMixerStreams.StreamsAdd(strGuestStreamID, null, filepath, "loop=false", out pFile, 0);
                }

                GuestElement.ElementMultipleSet("stream_id=" + strGuestStreamID + " audio_gain=-100 show=true", 0.0);
                try
                {
                    if (pFile != null)
                        mMixerList1.SelectFile(pFile);
                }
                catch { }

                mMixerList1.UpdateList(true, 1);
                InitializeStream(strGuestStreamID, false);
            }
        }
        private void BindGuestNodeProps()
        {
            if (GuestElement != null)
            {
                string strType, strXML;
                GuestElement.ElementGet(out strType, out strXML);
                GetElementInformation(strXML);
            }
        }
        #endregion

        #region PrintOrderPageList
        public ObservableCollection<VideoPage> PrintOrderPageList
        {
            get
            {
                return (ObservableCollection<VideoPage>)
                GetValue(PrintOrderPageProperty);
            }
            set { SetValue(PrintOrderPageProperty, value); }
        }

        // Using a DependencyProperty as the backing store for MyProperty.This enables animation, styling, binding, etc...
        public static readonly DependencyProperty PrintOrderPageProperty =
        DependencyProperty.Register("PrintOrderPageList",
        typeof(ObservableCollection<VideoPage>), typeof(VideoEditor),
            new PropertyMetadata(new ObservableCollection<VideoPage>()));
        #endregion

        #region Template List
        //Load Template List
        private void LoadTemplateList()
        {
            try
            {
                ConfigBusiness configBusiness = new ConfigBusiness();
                TemplateListItems tmpList;
                /// <summary>
                /// Loads the AudioTemplate.
                /// </summary>
                List<AudioTemplateInfo> objAudioTemplatesList = configBusiness.GetAudioTemplateList();
                for (int i = 0; i < objAudioTemplatesList.Count; i++)
                {
                    if (objAudioTemplatesList[i].IsActive == true)
                    {
                        tmpList = new TemplateListItems();
                        tmpList.isActive = true;
                        tmpList.FilePath = @"/DigiPhoto;component/images/audioico.png";
                        tmpList.Item_ID = objAudioTemplatesList[i].AudioTemplateId;
                        tmpList.IsChecked = false;
                        tmpList.DisplayName = objAudioTemplatesList[i].DisplayName;
                        tmpList.MediaType = 604;
                        tmpList.Length = objAudioTemplatesList[i].AudioLength;
                        tmpList.StartTime = 0;
                        tmpList.EndTime = (int)tmpList.Length;
                        tmpList.InsertTime = 0;
                        tmpList.Name = objAudioTemplatesList[i].Name;
                        tmpList.Tooltip = "Audio Template\n" + "Name : " + tmpList.DisplayName + "\n" + "Length: " + tmpList.Length.ToString() + " sec";
                        objTemplateList.Add(tmpList);
                    }
                }
                /// <summary>
                /// Loads the VideoTemplate.
                /// </summary>
                List<VideoTemplateInfo> objVideoTemplatesList = configBusiness.GetVideoTemplate();
                for (int i = 0; i < objVideoTemplatesList.Count; i++)
                {
                    if (objVideoTemplatesList[i].IsActive == true)
                    {
                        tmpList = new TemplateListItems();
                        tmpList.isActive = true;
                        tmpList.FilePath = @"/DigiPhoto;component/images/vidico.png";
                        tmpList.Item_ID = objVideoTemplatesList[i].VideoTemplateId;
                        tmpList.IsChecked = false;
                        tmpList.DisplayName = objVideoTemplatesList[i].DisplayName;
                        tmpList.MediaType = 603;
                        tmpList.Name = objVideoTemplatesList[i].Name;
                        tmpList.Length = objVideoTemplatesList[i].VideoLength;
                        List<VideoTemplateInfo.VideoSlot> slotListTemp = configBusiness.GetVideoTemplate(objVideoTemplatesList[i].VideoTemplateId).videoSlots;
                        string slotData = "";
                        for (int j = 0; j < slotListTemp.Count; j++)
                        {
                            slotData += "\n(" + (j + 1) + ") Slot:" + slotListTemp[j].FrameTimeIn + " sec," + "Photo Display Time:" + slotListTemp[j].PhotoDisplayTime + " sec.";
                        }
                        tmpList.Tooltip = "Video Template\n" + "Name : " + objVideoTemplatesList[i].DisplayName + "\n" + "Length : " + objVideoTemplatesList[i].VideoLength + " sec" + "\n" + "Slots" + slotData;
                        objTemplateList.Add(tmpList);
                    }
                }


                /// <summary>
                /// Loads the VideoBackground. line 387
                /// </summary>
                List<VideoBackgroundInfo> objVideoBackgroundsTemplatesList = configBusiness.GetVideoBackgrounds();
                for (int i = 0; i < objVideoBackgroundsTemplatesList.Count; i++)
                {
                    if (objVideoBackgroundsTemplatesList[i].IsActive == true)
                    {
                        tmpList = new TemplateListItems();
                        tmpList.isActive = true;
                        tmpList.Item_ID = objVideoBackgroundsTemplatesList[i].VideoBackgroundId;
                        tmpList.IsChecked = false;
                        tmpList.DisplayName = objVideoBackgroundsTemplatesList[i].DisplayName;
                        tmpList.MediaType = 605;
                        tmpList.Name = objVideoBackgroundsTemplatesList[i].Name;
                        if (!tmpList.Name.Contains(".jpg") && !(tmpList.Name.Contains(".png")))
                            tmpList.FilePath = @"/DigiPhoto;component/images/vidico.png";
                        else
                            tmpList.FilePath = LoginUser.DigiFolderPath + "VideoBackGround" + "\\" + objVideoBackgroundsTemplatesList[i].Name;
                        tmpList.Tooltip = "Video BackGround";
                        objTemplateList.Add(tmpList);
                    }

                }

                /// <summary>
                /// Loads the VideoOverlays.
                /// </summary>
                List<VideoOverlay> objVideoOverlayTemplatesList = configBusiness.GetVideoOverlays();
                for (int i = 0; i < objVideoOverlayTemplatesList.Count; i++)
                {
                    if (objVideoOverlayTemplatesList[i].IsActive == true)
                    {
                        tmpList = new TemplateListItems();
                        tmpList.isActive = true;
                        tmpList.Item_ID = objVideoOverlayTemplatesList[i].VideoOverlayId;
                        tmpList.IsChecked = false;
                        tmpList.DisplayName = objVideoOverlayTemplatesList[i].DisplayName;
                        tmpList.MediaType = 609;
                        tmpList.Name = objVideoOverlayTemplatesList[i].Name;
                        tmpList.FilePath = @"/DigiPhoto;component/images/vidico.png";
                        tmpList.Tooltip = "Video Overlay";
                        objTemplateList.Add(tmpList);
                    }

                }

                /// <summary>
                /// Loads the graphics.
                /// </summary>
                //DigiPhotoDataServices _objdataLayer = new DigiPhotoDataServices();
                var graphics = (new GraphicsBusiness()).GetGraphicsDetails().Where(t => t.DG_Graphics_IsActive == true);
                foreach (var item in graphics)
                {
                    tmpList = new TemplateListItems();
                    tmpList.isActive = true;
                    tmpList.FilePath = LoginUser.DigiFolderGraphicsPath + item.DG_Graphics_Name;
                    tmpList.Item_ID = item.DG_Graphics_pkey;
                    tmpList.IsChecked = false;
                    tmpList.DisplayName = item.DG_Graphics_Displayname;
                    tmpList.MediaType = 606;
                    tmpList.Name = item.DG_Graphics_Name;
                    tmpList.Tooltip = "Graphics";
                    objTemplateList.Add(tmpList);
                }

                /// <summary>
                /// Loads the Borders.
                /// </summary>

                var borders = (new BorderBusiness()).GetBorderDetails().Where(t => t.DG_IsActive == true && t.DG_ProductTypeID == 95);
                foreach (var item in borders)
                {
                    tmpList = new TemplateListItems();
                    tmpList.isActive = true;
                    tmpList.FilePath = LoginUser.DigiFolderFramePath + "\\Thumbnails\\" + item.DG_Border;
                    tmpList.Item_ID = item.DG_Borders_pkey;
                    tmpList.IsChecked = false;
                    tmpList.DisplayName = item.DG_Border;
                    tmpList.MediaType = 608;
                    tmpList.Name = item.DG_Border;
                    tmpList.Tooltip = "Borders";
                    objTemplateList.Add(tmpList);
                }
                List<TemplateListItems> TLI = objTemplateList.Where(o => o.MediaType == 605 && o.isActive == true).ToList();
                lstTemplates.ItemsSource = TLI;

            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }
        private void btnCloseTempList_Click(object sender, RoutedEventArgs e)
        {

            //chkAddLogo.IsChecked = false;
            if (lstTemplates.Visibility == System.Windows.Visibility.Collapsed)
            {
                SetTemplateListVisibility(true);

            }
            else
            {
                SetTemplateListVisibility(false);
            }
        }

        private void SetTemplateListVisibility(bool IsVisible)
        {
            if (IsVisible)
            {
                lstTemplates.Visibility = System.Windows.Visibility.Visible;
                btnCloseTempList.Visibility = System.Windows.Visibility.Visible;
                txtTemplateRotate.Visibility = Visibility.Visible;
            }
            else
            {
                lstTemplates.Visibility = System.Windows.Visibility.Collapsed;
                btnCloseTempList.Visibility = System.Windows.Visibility.Collapsed;
                txtTemplateRotate.Visibility = Visibility.Collapsed;
            }
        }
        private void btnPlayAudio_Click(object sender, RoutedEventArgs e)
        {
            int audioId = Convert.ToInt32(txtAudioId.Text);
            TemplateListItems lstMyItem = objTemplateList.Where(o => o.Item_ID == audioId && o.MediaType == 604).FirstOrDefault();
            string filePath = LoginUser.DigiFolderAudioPath + System.IO.Path.GetFileName(lstMyItem.Name);
            vsMediaFileName = filePath;
            MediaPlay();
        }
        #endregion Template List

        #region GuestImages
        private void LoadGuestImageList()
        {
            try
            {
                int count = RobotImageLoader.PrintImages.Count();
                for (int i = 0; i < count; i++)
                {
                    LstMyItems _objnew = new LstMyItems();
                    _objnew = RobotImageLoader.PrintImages[i];
                    VideoElements ve = new VideoElements();
                    VideoProcessingClass vpc = new VideoProcessingClass();
                    ve.GuestImagePath = _objnew.FilePath.Replace(vpc.SupportedVideoFormats["avi"].ToString(), ".jpg").Replace(vpc.SupportedVideoFormats["mp4"].ToString(), ".jpg").Replace(vpc.SupportedVideoFormats["wmv"].ToString(), ".jpg").Replace(vpc.SupportedVideoFormats["mov"].ToString(), ".jpg").Replace(vpc.SupportedVideoFormats["3gp"].ToString(), ".jpg").Replace(vpc.SupportedVideoFormats["3g2"].ToString(), ".jpg").Replace(vpc.SupportedVideoFormats["m2v"].ToString(), ".jpg").Replace(vpc.SupportedVideoFormats["m4v"].ToString(), ".jpg").Replace(vpc.SupportedVideoFormats["flv"].ToString(), ".jpg").Replace(vpc.SupportedVideoFormats["mpeg"].ToString(), ".jpg").Replace(vpc.SupportedVideoFormats["ffmpeg"].ToString(), ".jpg");

                    if (_objnew.MediaType == 2)
                    {
                        using (FileStream fileStream = File.OpenRead(System.IO.Path.Combine(_objnew.HotFolderPath, "Videos", _objnew.CreatedOn.ToString("yyyyMMdd"), _objnew.FileName)))
                        {
                            ve.VideoFilePath = fileStream.Name;
                        }
                    }
                    else if (_objnew.MediaType == 3)
                    {
                        using (FileStream fileStream = File.OpenRead(System.IO.Path.Combine(_objnew.HotFolderPath, "ProcessedVideos", _objnew.CreatedOn.ToString("yyyyMMdd"), _objnew.FileName)))
                        {
                            ve.VideoFilePath = fileStream.Name;
                        }
                    }
                    else if (_objnew.MediaType == 1)//Due to folder stucture changed for images also
                    {
                        if (count == 1)
                        {
                            ve.VideoFilePath = System.IO.Path.Combine(_objnew.HotFolderPath, _objnew.CreatedOn.ToString("yyyyMMdd"), _objnew.FileName);
                        }
                        else
                        {
                            if (File.Exists(System.IO.Path.Combine(_objnew.HotFolderPath, "EditedImages", _objnew.FileName)))
                            {
                                ve.VideoFilePath = System.IO.Path.Combine(_objnew.HotFolderPath, "EditedImages", _objnew.FileName);
                            }
                            else if (File.Exists(System.IO.Path.Combine(_objnew.HotFolderPath, _objnew.FileName)))
                            {
                                ve.VideoFilePath = System.IO.Path.Combine(_objnew.HotFolderPath, _objnew.FileName);
                            }
                            else
                            {
                                ve.VideoFilePath = System.IO.Path.Combine(_objnew.HotFolderPath, _objnew.CreatedOn.ToString("yyyyMMdd"), _objnew.FileName);
                            }
                        }

                    }
                    ve.PhotoId = _objnew.PhotoId;
                    ve.Name = _objnew.Name;
                    ve.MediaType = _objnew.MediaType;
                    ve.videoLength = _objnew.VideoLength == null ? 0 : (long)_objnew.VideoLength;
                    ve.PageNo = i + 1;
                    ve.CreatedDate = _objnew.CreatedOn;
                    lstVideoElements.Add(ve);

                }
                if (lstVideoElements.Count > 0)
                {
                    lstGuestImages.ItemsSource = lstVideoElements;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }
        private void Image_PreviewMouseDown(object sender, EventArgs e)
        {
            try
            {
                VideoElements lstMyItems = (VideoElements)(sender as Image).DataContext;
                this.DropPhotoId = lstMyItems.PhotoId;
                this.activePage = lstMyItems.PageNo;
                if (lstMyItems.MediaType == 2 || lstMyItems.MediaType == 3)
                {
                    //  StackGuestVideoTemplate.Visibility = Visibility.Visible;
                    //StackGuestImageAsGraphic.Visibility = Visibility.Collapsed;
                    StackImageDisplayTime.Visibility = Visibility.Collapsed;
                    StackVideoStartEndTime.Visibility = Visibility.Collapsed;
                    StackVideoTemplatePlay.Visibility = Visibility.Collapsed;
                    StackGraphicSettingPanel1.Visibility = Visibility.Collapsed;
                    StackGraphicSettingPanel2.Visibility = Visibility.Collapsed;
                    if (lstMyItems.MediaType == 3)
                    {
                        // stkZommControl.Visibility = Visibility.Collapsed;
                        //StackGuestImageAsGraphic.Visibility = Visibility.Visible;
                    }
                }
                else
                {
                    // StackGuestImageAsGraphic.Visibility = Visibility.Visible;
                    // StackGuestVideoTemplate.Visibility = Visibility.Collapsed;
                    StackImageDisplayTime.Visibility = Visibility.Collapsed;
                    StackVideoStartEndTime.Visibility = Visibility.Collapsed;
                    StackVideoTemplatePlay.Visibility = Visibility.Collapsed;
                    StackGraphicSettingPanel1.Visibility = Visibility.Collapsed;
                    StackGraphicSettingPanel2.Visibility = Visibility.Collapsed;
                    // stkZommControl.Visibility = Visibility.Collapsed;
                }
                DragDrop.DoDragDrop((DependencyObject)sender, ((Image)sender).Source, DragDropEffects.Copy);
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }
        private void btnImgPlay_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                VideoElements lstMyItems = (VideoElements)(sender as Button).DataContext;
                if (lstMyItems.MediaType == 2 || lstMyItems.MediaType == 3)
                {
                    //MediaStop();
                    vsMediaFileName = lstMyItems.VideoFilePath;
                    MediaPlay();
                }
                else
                {
                    MediaStop();
                    vsMediaFileName = lstMyItems.GuestImagePath.Replace("Thumbnails", "Thumbnails_Big");
                    //ImageSource imageSource = new BitmapImage(new Uri(vsMediaFileName, UriKind.RelativeOrAbsolute));
                    MediaPlay();
                    //mpc = new MediaPlayerControl(vsMediaFileName, "VideoEditor", "", false, true);
                    //gdMediaPlayer.Children.Add(mpc);
                    //VisioMediaPlayer.BackgroundImage_Source = (BitmapSource)imageSource;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }
        #endregion GuestImages

        #region Video processing events and functions

        #region Video Effects
        ////Scroll Changed Event        
        ////Lightness
        //private void tbLightness_Scroll(object sender, RoutedPropertyChangedEventArgs<double> e)
        //{
        //    VisioEditPlayer.Video_Effect_Ex(11, 0, 0, true, VFVideoEffectType.Lightness, GeneralEffectsBox.tbLightness.Value);
        //    objProcVideoInfo.lightness = GeneralEffectsBox.tbLightness.Value.ToString();
        //}
        //// Saturation
        //private void tbSaturation_Scroll(object sender, RoutedPropertyChangedEventArgs<double> e)
        //{
        //    if (VisioEditPlayer != null)
        //    {
        //        VisioEditPlayer.Video_Effect_Ex(12, 0, 0, true, VFVideoEffectType.Saturation, GeneralEffectsBox.tbSaturation.Value);
        //        objProcVideoInfo.saturation = GeneralEffectsBox.tbSaturation.Value.ToString();
        //    }
        //}
        ////Contrast
        //private void tbContrast_Scroll(object sender, RoutedPropertyChangedEventArgs<double> e)
        //{
        //    VisioEditPlayer.Video_Effect_Ex(13, 0, 0, true, VFVideoEffectType.Contrast, GeneralEffectsBox.tbContrast.Value);
        //    objProcVideoInfo.contrast = GeneralEffectsBox.tbContrast.Value.ToString();
        //}
        ////Darkness
        //private void tbDarkness_Scroll(object sender, RoutedPropertyChangedEventArgs<double> e)
        //{
        //    VisioEditPlayer.Video_Effect_Ex(14, 0, 0, true, VFVideoEffectType.Darkness, GeneralEffectsBox.tbDarkness.Value);
        //    objProcVideoInfo.darkness = GeneralEffectsBox.tbDarkness.Value.ToString();
        //}
        //Add Logo
        //private void chkAddLogo_Click(object sender, RoutedEventArgs e)
        //{
        //    if (chkAddLogo.IsChecked == true)
        //    {
        //        List<TemplateListItems> TLI = objTemplateList.Where(o => o.MediaType == 606).ToList();
        //        lstTemplates.ItemsSource = TLI;
        //        //stackLogo.Visibility = Visibility.Visible;
        //        templateType = "graphics";
        //        txtTemplateRotate.Text = "Graphics Templates";
        //        StackImageDisplayTime.Visibility = Visibility.Collapsed;
        //        StackVideoStartEndTime.Visibility = Visibility.Collapsed;
        //        StackVideoTemplatePlay.Visibility = Visibility.Collapsed;
        //        StackGuestVideoTemplate.Visibility = Visibility.Collapsed;
        //        LogoControlBox.ShowPanHandlerDialog();
        //    }
        //    else
        //    {
        //        //stackLogo.Visibility = Visibility.Collapsed;
        //        List<TemplateListItems> TLI = objTemplateList.Where(o => o.MediaType == 603 && o.isActive == true).ToList();
        //        lstTemplates.ItemsSource = TLI;
        //        templateType = "video";
        //        txtTemplateRotate.Text = "Video Templates";
        //        VisioMediaPlayer.BackgroundImage_Source = null;
        //    }
        //}
        //Text Logo
        //private void chkTextLogo_Click(object sender, RoutedEventArgs e)
        //{
        //    try
        //    {
        //        VFTextRotationMode rotate;
        //        VFTextFlipMode flip;
        //        VisioEditPlayer.Video_Effects_Text_Logo(17, 0, 0, false, LogoControlBox.txtTextLogo.Text, Convert.ToInt32(LogoControlBox.txtTextLogoLeft.Text), Convert.ToInt32(LogoControlBox.txtTextLogoTop.Text),
        //            LogoControlBox.fontDialog.Font, LogoControlBox.fontDialog.Color);

        //        System.Drawing.StringFormat stringFormat = new System.Drawing.StringFormat();
        //        stringFormat.Alignment = (System.Drawing.StringAlignment)0;
        //        VisioEditPlayer.Video_Effects_Text_Logo_Parameters_Text(17, true, Colors.Black, stringFormat, (System.Drawing.Text.TextRenderingHint)0,
        //            (System.Drawing.Drawing2D.InterpolationMode)0);

        //        rotate = VFTextRotationMode.RmNone;
        //        flip = VFTextFlipMode.None;
        //        VisioEditPlayer.Video_Effects_Text_Logo_Parameters_Rotate(17, rotate, flip);
        //        VisioEditPlayer.Video_Effects_Text_Logo_Parameters_Effect(17, (VFTextEffectMode)0, Colors.Black, Colors.Black, 1, 1);
        //        VisioEditPlayer.Video_Effects_Text_Logo_Parameters_Transparency(17, 0);
        //        VisioEditPlayer.Video_Effects_Text_Logo_Parameters_Update(17, LogoControlBox.chkTextLogo.IsChecked == true);
        //    }
        //    catch (Exception ex)
        //    {
        //        string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
        //        ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
        //    }

        //}
        ////Grpahic Logo
        //private void chkGraphicLogo_Click(object sender, RoutedEventArgs e)
        //{
        //    try
        //    {
        //        if (objTemplateList.Where(o => o.MediaType == 606 && o.IsChecked == true).ToList().Count > 0 && LogoControlBox.chkGraphicLogo.IsChecked == true)
        //        {
        //            LogoControlBox.chkGraphicLogo.IsChecked = true;
        //            LogoControlBox.GraphicLogoPanel.Visibility = Visibility.Visible;
        //            List<TemplateListItems> TLI = objTemplateList.Where(o => o.MediaType == 606).ToList();
        //            lstTemplates.ItemsSource = TLI;
        //            if (VisioEditPlayer != null)
        //            {
        //                VisioEditPlayer.Video_Effects_Picture_Logo_Ex(graphicImage, 18, 0, 0, LogoControlBox.chkGraphicLogo.IsChecked == true, Convert.ToUInt32(LogoControlBox.txtGraphicLogoLeft.Text),
        //                    Convert.ToUInt32(LogoControlBox.txtGraphicLogoTop.Text), null, 0, Colors.Black, false);// (100) left and top margin to be fatch from configurations
        //                //VisioEditPlayer.Video_Effects_Picture_Logo_Ex(graphicImage, 19, 0, 0, rdbGraphicLogo.IsChecked == true, 200,
        //                //200, null, 0, Colors.Black, false);
        //            }
        //        }
        //        else
        //        {
        //            objTemplateList.Where(o => o.IsChecked == true && o.MediaType == 606).ToList().ForEach(t => t.IsChecked = false);
        //            List<TemplateListItems> TLI = objTemplateList.Where(o => o.MediaType == 606).ToList();
        //            lstTemplates.ItemsSource = null;
        //            lstTemplates.ItemsSource = TLI;
        //            dragCanvas.Children.Clear();
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
        //        ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
        //    }
        //}
        //Font Button Click
        //private void btFont_Click(object sender, RoutedEventArgs e)
        //{

        //    if (LogoControlBox.fontDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
        //    {
        //        chkTextLogo_Click(null, null);
        //    }
        //}
        ////Invert
        //private void chkInvert_Click(object sender, RoutedEventArgs e)
        //{
        //    VisioEditPlayer.Video_Effect_Ex(16, 0, 0, GeneralEffectsBox.chkInvert.IsChecked == true, VFVideoEffectType.Invert, 0);
        //}
        ////GrayScale
        //private void chkGrayScale_Click(object sender, RoutedEventArgs e)
        //{
        //    VisioEditPlayer.Video_Effect_Ex(15, 0, 0, GeneralEffectsBox.chkGrayScale.IsChecked == true, VFVideoEffectType.Greyscale, 0);
        //}
        //Image Display Button
        bool ChangeGuestElement = true;
        private void btnSetDisplayTime_Click(object sender, RoutedEventArgs e)
        {
            VideoPage lstMyItem = PrintOrderPageList.Where(o => o.PhotoId == this.DropPhotoId && o.PageNo == activePage).FirstOrDefault();
            if (lstMyItem != null)
            {
                int imageDT = Convert.ToInt32(txtImageDisplayTime.Text);
                if (chkImgDTSetToAll.IsChecked == true)
                {
                    foreach (VideoPage ve in PrintOrderPageList.Where(o => o.MediaType == 601))
                    {
                        ve.ImageDisplayTime = imageDT;
                        ve.tooltip = "Image display time: " + ve.ImageDisplayTime;
                    }
                }
                else
                {
                    lstMyItem.ImageDisplayTime = imageDT;
                    lstMyItem.tooltip = "Image display time: " + lstMyItem.ImageDisplayTime;
                }
                chkImgDTSetToAll.IsChecked = false;
                // ChangeGuestElement = false;
                BindPageStrips();
            }
            StackImageDisplayTime.Visibility = Visibility.Collapsed;
        }

        //Pan Effect
        ////protected void PanBox_ExecuteMethod(object sender, EventArgs e)
        ////{
        ////    PanPropertyLst = PanBox.ppLst;

        ////}

        ////private void btnPanEffect_Click(object sender, RoutedEventArgs e)
        ////{

        ////    PanBox.slotList = slotList;
        ////    PanBox.videoExpLen = CalculateProcessedVideoLength();
        ////    PanBox.ShowPanHandlerDialog();
        ////}
        //////Transition Effect
        ////void TransitionBox_ExecuteMethod(object sender, EventArgs e)
        ////{
        ////    TransitionPropertyLst = TransitionBox.tpLst;
        ////}
        ////private void btnTransitionEffect_Click(object sender, RoutedEventArgs e)
        ////{
        ////    TransitionBox.ShowPanHandlerDialog();
        ////}

        ////void BorderControl_ExecuteMethod(object sender, EventArgs e)
        ////{
        ////    // TransitionPropertyLst = (List<TransitionProperty>)TransitionBox.DGManageTransition.ItemsSource;
        ////}
        #endregion Video Effects

        //#region Audio Effects
        //#region Amplify
        //private void sldAmplify_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        //{
        //    VisioEditPlayer.Audio_Effects_Amplify(-1, 0, (int)sldAmplify.Value * 10, false);
        //}
        //#endregion

        //#region Equalizer

        //private void sldEqual1_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        //{
        //    VisioEditPlayer.Audio_Effects_Equalizer_Band_Set(-1, 1, 0, (sbyte)sldEqual1.Value);
        //}

        //private void sldEqual2_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        //{
        //    VisioEditPlayer.Audio_Effects_Equalizer_Band_Set(-1, 1, 0, (sbyte)sldEqual2.Value);
        //}

        //private void sldEqual3_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        //{
        //    VisioEditPlayer.Audio_Effects_Equalizer_Band_Set(-1, 1, 0, (sbyte)sldEqual3.Value);
        //}

        //private void sldEqual4_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        //{
        //    VisioEditPlayer.Audio_Effects_Equalizer_Band_Set(-1, 1, 0, (sbyte)sldEqual4.Value);
        //}

        //private void sldEqual5_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        //{
        //    VisioEditPlayer.Audio_Effects_Equalizer_Band_Set(-1, 1, 0, (sbyte)sldEqual5.Value);
        //}

        //private void sldEqual6_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        //{
        //    VisioEditPlayer.Audio_Effects_Equalizer_Band_Set(-1, 1, 0, (sbyte)sldEqual6.Value);
        //}
        //#endregion

        //#endregion Audio Effects

        //#region Chroma Effects
        //private void chkApplyChroma_Click(object sender, RoutedEventArgs e)
        //{
        //    // Chromakey

        //    // VisioEditPlayer.ChromaKey_Enabled = chkApplyChroma.IsChecked == true;
        //    VisioEditPlayer.ChromaKey_ContrastHigh = (int)sldContHigh.Value;
        //    VisioEditPlayer.ChromaKey_ContrastLow = (int)sldContLow.Value;
        //    VisioEditPlayer.ChromaKey_ImageFilename = backgroundChromaImage;
        //    if (rdbGreen.IsChecked == true)
        //    {
        //        VisioEditPlayer.ChromaKey_Color = VFChromaColor.Green;
        //        objProcVideoInfo.chromaKeyColor = "Green";
        //    }
        //    else if (rdbBlue.IsChecked == true)
        //    {
        //        VisioEditPlayer.ChromaKey_Color = VFChromaColor.Blue;
        //        objProcVideoInfo.chromaKeyColor = "Blue";
        //    }
        //    else
        //    {
        //        VisioEditPlayer.ChromaKey_Color = VFChromaColor.Red;
        //        objProcVideoInfo.chromaKeyColor = "Red";
        //    }
        //}
        //private void sldContHigh_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        //{
        //    if (VisioEditPlayer != null)
        //    {
        //        VisioEditPlayer.ChromaKey_ContrastHigh = (int)sldContHigh.Value;
        //    }
        //}
        //private void sldContLow_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        //{
        //    if (VisioEditPlayer != null)
        //    {
        //        VisioEditPlayer.ChromaKey_ContrastLow = (int)sldContLow.Value;
        //    }
        //}
        //#endregion Chroma Effects
        //private void VisioEditPlayer_OnProgress(object sender, ProgressEventArgs e)
        //{
        //    pbProgress.Value = e.Progress;
        //    if (pbProgress.Value == 8)
        //    {
        //        string path_temp = processVideoTemp + "Output.jpg";
        //        if (File.Exists(path_temp))
        //        {
        //            File.Delete(path_temp);
        //        }
        //        VisioEditPlayer.Frame_Save(path_temp, VFImageFormat.JPEG, 85);
        //    }
        //}
        //private void VisioEditPlayer_OnStart(object sender, EventArgs e)
        //{

        //}
        //private void VisioEditPlayer_OnError(object sender, ErrorsEventArgs e)
        //{
        //    ErrorHandler.ErrorHandler.LogFileWrite("VisioEditPlayer_OnError:" + e.Message);
        //    if (e.Message == "Input files not found.")
        //    {
        //        EnableControls();
        //    }
        //}
        //private void VisioEditPlayer_OnStop(object sender, EventArgs e)
        //{
        //    ClearImageSourceControl();
        //    transitionFlag = false;
        //    pbProgress.Value = 0;
        //    File.Copy(processVideoTemp + "Output." + outputFormat, processVideoTemp + "PlayerOutput." + outputFormat, true);
        //    MessageBox.Show("The video has been processed successfully!", "DigiPhoto", MessageBoxButton.OK, MessageBoxImage.Information);
        //    ClearVideoEditPlayer();
        //    VideoEditPlayerPanel.Visibility = Visibility.Collapsed;
        //    vidoriginal.Visibility = Visibility.Visible;
        //    backgroundChromaImage = "";
        //    EnableControls();
        //    grdVideoCaptureControls.IsEnabled = true;
        //    btnStopProcess.IsEnabled = false;
        //    vsMediaFileName = processVideoTemp + "PlayerOutput." + outputFormat;
        //    // vsMediaFileName = processVideoTemp + "Output." + outputFormat;
        //    replayFilePath = vsMediaFileName;
        //    isReplay = true;
        //    if (AutomatedVideoEditWorkFlow)
        //    {
        //        btnSave_Click(sender, null);
        //    }
        //    else
        //    {
        //        MediaPlay();
        //    }
        //    //MediaReplay();
        //}
        bool isImageAsGraphic = false;
        //private void StartProcess(string outputFilePath, string outputFileName)
        //{
        //    try
        //    {
        //        // isImageAsGraphic = false;
        //        GetGraphicPosition();
        //        if (isImageAsGraphic)
        //        {
        //            string ext = System.IO.Path.GetExtension(specFileName);
        //            if (string.IsNullOrEmpty(ext))
        //                ext = ".png";
        //            specFileName = Environment.CurrentDirectory + "\\CroppedImage\\" + System.IO.Path.GetFileNameWithoutExtension(specFileName) + "1" + ext;
        //            ExtractPng(specFileName);
        //            ClearImageSourceControl();
        //        }
        //        VisioEditPlayer.OnStart += VisioEditPlayer_OnStart;
        //        VisioEditPlayer.OnProgress += VisioEditPlayer_OnProgress;
        //        VisioEditPlayer.OnStop += VisioEditPlayer_OnStop;
        //        VisioEditPlayer.OnError += VisioEditPlayer_OnError;
        //        //VisioEditPlayer.SetLicenseKey("1DDA-B0F7-39EA-A12D-3C62-AD55", "Adarsh", "adarsh.srivastava@commdel.net");
        //        VisioEditPlayer.SetLicenseKey(LicenseKey, UserName, EmailId);
        //        VisioEditPlayer.WMV_Custom_Audio_Mode = VFWMVStreamMode.CBR;
        //        dragCanvas.Visibility = Visibility.Collapsed;
        //        grdVideoCaptureControls.IsEnabled = false;
        //        VisioEditPlayer.Video_Effects_Clear();
        //        VisioEditPlayer.Audio_Preview_Enabled = true;
        //        VisioEditPlayer.Video_Preview_Enabled = true;
        //        VisioEditPlayer.Mode = VFVideoEditMode.Convert;
        //        VisioEditPlayer.Video_Resize = ConfigManager.IMIXConfigurations.ContainsKey((int)ConfigParams.ResizeImage) ? Convert.ToBoolean(ConfigManager.IMIXConfigurations[(int)ConfigParams.ResizeImage]) : true;
        //        if (!IsVideoInput)
        //        {
        //            VisioEditPlayer.Video_FrameRate = ConfigManager.IMIXConfigurations.ContainsKey((int)ConfigParams.FrameRate) ? Convert.ToInt32(ConfigManager.IMIXConfigurations[(int)ConfigParams.FrameRate]) : 25;
        //        }
        //        else
        //        {
        //            VisioEditPlayer.Video_FrameRate = 0;
        //        }
        //        VisioEditPlayer.Video_Renderer = VFVideoRendererWPF.WPF;
        //        VisioEditPlayer.Video_Renderer_StretchMode = VFVideoRendererStretchMode.Letterbox;
        //        VisioEditPlayer.Video_Renderer_RotationAngle = Convert.ToInt32(0);
        //        VisioEditPlayer.Input_Images_EnableMultiImageEngine = true;

        //        // Encryption
        //        VisioEditPlayer.Encryption_Key = "100";
        //        VisioEditPlayer.Encryption_Format = VFEncryptionFormat.MP4_H264_SW_AAC;
        //        VisioEditPlayer.Encryption_Mode = VFEncryptionMode.v9_AES256;
        //        VisioEditPlayer.Output_Filename = outputFilePath + "\\" + outputFileName;
        //        string file = outputFilePath + outputFileName;


        //        if (outputFormat == "mp4")
        //        {
        //            bool isMp4CudaApply = Convert.ToBoolean(ConfigManager.IMIXConfigurations.ContainsKey((int)ConfigParams.ApplyMP4CUDA) ? ConfigManager.IMIXConfigurations[(int)ConfigParams.ApplyMP4CUDA] : "false");// "MJPEG Compressor";               
        //            if (isMp4CudaApply)
        //            {
        //                VisioEditPlayer.Output_Format = VFVideoEditOutputFormat.MP4_CUDA;
        //            }
        //            else
        //            {
        //                VisioEditPlayer.Output_Format = VFVideoEditOutputFormat.MP4;
        //            }
        //            //VisioEditPlayer.MP4_LegacyCodecs = true;
        //            //VisioEditPlayer.Audio_Codec_Channels = 2;
        //            //VisioEditPlayer.MP4_Video_Profile = VFH264Profile.ProfileMain;
        //            //VisioEditPlayer.MP4_Video_Level = VFH264Level.LevelAuto;
        //            //VisioEditPlayer.MP4_Video_Target_Usage = VFH264TargetUsage.Auto;
        //            //VisioEditPlayer.MP4_Video_PictureType = VFH264PictureType.Auto;
        //            //VisioEditPlayer.MP4_Video_RateControl = VFH264RateControl.VBR;
        //            //VisioEditPlayer.MP4_Video_MBEncoding = VFH264MBEncoding.CAVLC;
        //            //VisioEditPlayer.MP4_Video_GOP = true;
        //            VisioEditPlayer.MP4_LegacyCodecs = true;
        //            VisioEditPlayer.MP4_Video_H264_Profile = VFH264Profile.ProfileAuto;
        //            VisioEditPlayer.MP4_Video_H264_Level = VFH264Level.LevelAuto;
        //            VisioEditPlayer.MP4_Video_H264_Target_Usage = VFH264TargetUsage.Auto;
        //            VisioEditPlayer.MP4_Video_H264_PictureType = VFH264PictureType.Auto;
        //            VisioEditPlayer.MP4_Video_H264_RateControl = VFH264RateControl.CBR;
        //            VisioEditPlayer.MP4_Video_H264_MBEncoding = VFH264MBEncoding.CAVLC;
        //            VisioEditPlayer.MP4_Video_H264_GOP = false;

        //            VisioEditPlayer.MP4_Video_H264_IDR_Period = 15;
        //            VisioEditPlayer.MP4_Video_H264_P_Period = 3;
        //            VisioEditPlayer.MP4_Video_H264_Bitrate = 2000;
        //            VisioEditPlayer.MP4_Video_H264_MaxBitrate = 2000 * 2;
        //            VisioEditPlayer.MP4_Video_H264_MinBitrate = 2000 / 2;
        //            VisioEditPlayer.MP4_Video_H264_BitrateAuto = true;
        //            VisioEditPlayer.Video_Codec = ConfigManager.IMIXConfigurations.ContainsKey((int)ConfigParams.VideoCodecs) ? ConfigManager.IMIXConfigurations[(int)ConfigParams.VideoCodecs] : "MJPEG Compressor";// "MJPEG Compressor";               

        //            //CUDA
        //            //VisioEditPlayer.MP4_CUDA_Video_H264_BitrateAuto = false;
        //            //VisioEditPlayer.MP4_CUDA_Video_H264_Deblocking = true;
        //            VisioEditPlayer.MP4_CUDA_MultiGPU = true;

        //            VisioEditPlayer.MP4_CUDA_ForceGPU_ID = -1;
        //            VisioEditPlayer.MP4_CUDA_Video_H264_Profile = VFCUDAH264ProfileIDC.ProfileMain;
        //            VisioEditPlayer.MP4_CUDA_Video_H264_Level = VFCUDAH264LevelIDC.LevelAuto;
        //            VisioEditPlayer.MP4_CUDA_Video_H264_Bitrate = 2000;
        //            VisioEditPlayer.MP4_CUDA_Video_H264_BitrateAuto = true;
        //            VisioEditPlayer.MP4_CUDA_Video_H264_Peak_Bitrate = 6000;
        //            VisioEditPlayer.MP4_CUDA_Video_H264_QPLevel_InterB = 28;
        //            VisioEditPlayer.MP4_CUDA_Video_H264_QPLevel_InterP = 28;
        //            VisioEditPlayer.MP4_CUDA_Video_H264_QPLevel_Intra = 28;
        //            VisioEditPlayer.MP4_CUDA_Video_H264_MBEncoding = VFCUDAH264MBEncoding.CAVLC;
        //            VisioEditPlayer.MP4_CUDA_Video_H264_Deblocking = true;
        //            VisioEditPlayer.MP4_CUDA_Video_H264_GOP = false;
        //            VisioEditPlayer.MP4_CUDA_Video_H264_IDR_Period = 15;
        //            VisioEditPlayer.MP4_CUDA_Video_H264_P_Period = 3;
        //            VisioEditPlayer.MP4_CUDA_Video_H264_ForceIDR = false;
        //            VisioEditPlayer.MP4_CUDA_Video_H264_ForceIntra = false;
        //            VisioEditPlayer.MP4_CUDA_Video_H264_Deinterlace = true;
        //            VisioEditPlayer.MP4_CUDA_Video_H264_FieldEncoding = VFCUDAH264FieldEncoding.Auto;
        //            string CUDACodec = ConfigManager.IMIXConfigurations.ContainsKey((int)ConfigParams.CudaCodec) ? ConfigManager.IMIXConfigurations[(int)ConfigParams.CudaCodec].ToString() : string.Empty;
        //            if (CUDACodec == "Modern")
        //            {
        //                VisioEditPlayer.MP4_CUDA_Video_Encoder = VFCUDAVideoEncoder.Modern;
        //            }
        //            else
        //            {
        //                VisioEditPlayer.MP4_CUDA_Video_Encoder = VFCUDAVideoEncoder.Legacy;
        //            }
        //            VisioEditPlayer.MP4_CUDA_Audio_AAC_Version = VFAACVersion.MPEG4;
        //            VisioEditPlayer.MP4_CUDA_Audio_AAC_Object = VFAACObject.Low;
        //            VisioEditPlayer.MP4_CUDA_Audio_AAC_Bitrate = 128;
        //            VisioEditPlayer.MP4_CUDA_Audio_AAC_Output = VFAACOutput.RAW;
        //            //2
        //            VisioEditPlayer.MP4_CUDA_Video_H264_Profile = VFCUDAH264ProfileIDC.ProfileMain;
        //            VisioEditPlayer.MP4_CUDA_Video_H264_Level = VFCUDAH264LevelIDC.LevelAuto;

        //        }
        //        else if (outputFormat == "avi")
        //        {
        //            VisioEditPlayer.Audio_Codec_Channels = 1;
        //            VisioEditPlayer.Output_Format = VFVideoEditOutputFormat.AVI;
        //            VisioEditPlayer.Video_Codec = ConfigManager.IMIXConfigurations.ContainsKey((int)ConfigParams.VideoCodecs) ? ConfigManager.IMIXConfigurations[(int)ConfigParams.VideoCodecs] : "MJPEG Compressor";// "MJPEG Compressor";               
        //        }
        //        else if (outputFormat == "wma")
        //        {
        //            VisioEditPlayer.Audio_Codec_Channels = 1;
        //            VisioEditPlayer.Output_Format = VFVideoEditOutputFormat.WMA;
        //            VisioEditPlayer.WMV_Mode = VFWMVMode.InternalProfile;
        //            VisioEditPlayer.WMV_Internal_Profile_Name = VisioEditPlayer.WMV_Internal_Profiles()[0];
        //            VisioEditPlayer.Video_Codec = ConfigManager.IMIXConfigurations.ContainsKey((int)ConfigParams.VideoCodecs) ? ConfigManager.IMIXConfigurations[(int)ConfigParams.VideoCodecs] : "MJPEG Compressor";// "MJPEG Compressor";               
        //        }
        //        else if (outputFormat == "wmv")
        //        {
        //            VisioEditPlayer.Audio_Codec_Channels = 1;
        //            VisioEditPlayer.Output_Format = VFVideoEditOutputFormat.WMV;
        //            VisioEditPlayer.WMV_Mode = VFWMVMode.InternalProfile;
        //            VisioEditPlayer.WMV_Internal_Profile_Name = VisioEditPlayer.WMV_Internal_Profiles()[0];
        //            VisioEditPlayer.Video_Codec = ConfigManager.IMIXConfigurations.ContainsKey((int)ConfigParams.VideoCodecs) ? ConfigManager.IMIXConfigurations[(int)ConfigParams.VideoCodecs] : "MJPEG Compressor";// "MJPEG Compressor";               
        //        }
        //        else if (outputFormat == "mkv")
        //        {
        //            VisioEditPlayer.Audio_Codec_Channels = 1;
        //            VisioEditPlayer.Output_Format = VFVideoEditOutputFormat.MKV;
        //            VisioEditPlayer.Video_Codec = ConfigManager.IMIXConfigurations.ContainsKey((int)ConfigParams.VideoCodecs) ? ConfigManager.IMIXConfigurations[(int)ConfigParams.VideoCodecs] : "MJPEG Compressor";// "MJPEG Compressor";               
        //        }
        //        else if (outputFormat == "ffmpeg" || outputFormat == "mpg" || outputFormat == "flv")
        //        {
        //            VisioEditPlayer.Audio_Codec_Channels = 1;
        //            VisioEditPlayer.Output_Format = VFVideoEditOutputFormat.FFMPEG;
        //            VisioEditPlayer.FFMPEG_DLL_Video_AspectRatioW = 4;
        //            VisioEditPlayer.FFMPEG_DLL_Video_AspectRatioH = 3;
        //            VisioEditPlayer.FFMPEG_DLL_Video_Width = ConfigManager.IMIXConfigurations.ContainsKey((int)ConfigParams.ResizeWidth) ? Convert.ToInt32(ConfigManager.IMIXConfigurations[(int)ConfigParams.ResizeWidth]) : 1080; //720;
        //            VisioEditPlayer.FFMPEG_DLL_Video_Height = ConfigManager.IMIXConfigurations.ContainsKey((int)ConfigParams.ResizeHeight) ? Convert.ToInt32(ConfigManager.IMIXConfigurations[(int)ConfigParams.ResizeHeight]) : 1920;//576;
        //            VisioEditPlayer.FFMPEG_DLL_Video_Bitrate = 2000;
        //            VisioEditPlayer.FFMPEG_DLL_Video_MaxBitrate = 2000 * 2;
        //            VisioEditPlayer.FFMPEG_DLL_Video_MinBitrate = 2000 / 2;
        //            VisioEditPlayer.FFMPEG_DLL_Video_TVSystem = VFFFMPEGDLLTVSystem.PAL;
        //            VisioEditPlayer.FFMPEG_DLL_Audio_SampleRate = 48000;
        //            switch (outputFormat)
        //            {
        //                case "mpg":
        //                    this.VisioEditPlayer.FFMPEG_DLL_OutputFormat = VFFFMPEGDLLOutputFormat.MPEG1;
        //                    break;
        //                case "flv":
        //                    this.VisioEditPlayer.FFMPEG_DLL_OutputFormat = VFFFMPEGDLLOutputFormat.FLV;
        //                    break;
        //            }
        //            VisioEditPlayer.Video_Codec = ConfigManager.IMIXConfigurations.ContainsKey((int)ConfigParams.VideoCodecs) ? ConfigManager.IMIXConfigurations[(int)ConfigParams.VideoCodecs] : "MJPEG Compressor";// "MJPEG Compressor";               
        //        }
        //        else if (outputFormat == "mov" || outputFormat == "3GP" || outputFormat == "3G2" || outputFormat == "M2V" || outputFormat == "M4V")
        //        {
        //            EnableControls();
        //            VisioEditPlayer.Stop();
        //            MsgBox.ShowHandlerDialog("Format Not Supported.", DigiMessageBox.DialogType.OK);
        //            return;
        //        }
        //        else
        //        {
        //            EnableControls();
        //            VisioEditPlayer.Stop();
        //            MsgBox.ShowHandlerDialog("Format Not Supported.", DigiMessageBox.DialogType.OK);
        //            return;
        //        }


        //        this.VisioEditPlayer.Audio_Codec_BPS = 16;
        //        this.VisioEditPlayer.Audio_Codec_SampleRate = 44100;
        //        this.VisioEditPlayer.Audio_Codec_Name = ConfigManager.IMIXConfigurations.ContainsKey((int)ConfigParams.AudioCodecs) ? ConfigManager.IMIXConfigurations[(int)ConfigParams.AudioCodecs] : "PCM";//"PCM";   
        //        this.VisioEditPlayer.Video_Resize_Width = ConfigManager.IMIXConfigurations.ContainsKey((int)ConfigParams.ResizeWidth) ? Convert.ToInt32(ConfigManager.IMIXConfigurations[(int)ConfigParams.ResizeWidth]) : 1920;
        //        this.VisioEditPlayer.Video_Resize_Height = ConfigManager.IMIXConfigurations.ContainsKey((int)ConfigParams.ResizeHeight) ? Convert.ToInt32(ConfigManager.IMIXConfigurations[(int)ConfigParams.ResizeHeight]) : 1080;

        //        // Virtual camera output
        //        VisioEditPlayer.Virtual_Camera_Output_Enabled = false;
        //        VisioEditPlayer.Video_Effects_Clear();
        //        VisioEditPlayer.Video_Effects_Enabled = true;
        //        VisioEditPlayer.Video_Effects_Deinterlace_CAVT(1, 0, 0, true, Convert.ToByte("20"));
        //        // AForge.Net 
        //        VisioEditPlayer.AForge_Motion_Detection_Enabled = false;
        //        VisioEditPlayer.AForge_Motion_Detection_ProcessorType = AFMotionProcessorType.None;
        //        VisioEditPlayer.Network_Streaming_Audio_Enabled = false;
        //        // Barcode detection
        //        VisioEditPlayer.Barcode_Reader_Enabled = false;
        //        VisioEditPlayer.Barcode_Reader_Type = VFBarcodeType.Auto;
        //        // Decklink output
        //        VisioEditPlayer.Decklink_Output_Enabled = false;
        //        VisioEditPlayer.Decklink_Output_DV_Encoding = false;
        //        VisioEditPlayer.Decklink_Output_AnalogOutputIREUSA = true;
        //        VisioEditPlayer.Decklink_Output_AnalogOutputSMPTE = true;
        //        VisioEditPlayer.Decklink_Output_BlackToDeckInCapture = DecklinkBlackToDeckInCapture.Default;
        //        VisioEditPlayer.Decklink_Output_DualLinkOutputMode = (DecklinkDualLinkMode.Default);
        //        VisioEditPlayer.Decklink_Output_HDTVPulldownOnOutput = (DecklinkHDTVPulldownOnOutput.Default);
        //        VisioEditPlayer.Decklink_Output_SingleFieldOutputForSynchronousFrames =
        //            (DecklinkSingleFieldOutputForSynchronousFrames.Default);
        //        VisioEditPlayer.Decklink_Output_VideoOutputDownConversionMode = (DecklinkVideoOutputDownConversionMode.Auto);
        //        VisioEditPlayer.Decklink_Output_VideoOutputDownConversionModeAnalogUsed = false;
        //        VisioEditPlayer.Decklink_Output_AnalogOutput = (DecklinkAnalogOutput.Auto);
        //        VisioEditPlayer.Video_Rotation = VFRotateMode.RotateNone;
        //        // VisioEditPlayer.Video_Rotation = VFRotateMode.Rotate90;


        //        //Effects
        //        //Lightness
        //        if (GeneralEffectsBox.tbLightness.Value > 0)
        //        {
        //            VisioEditPlayer.Video_Effect_Ex(11, 0, 0, true, VFVideoEffectType.Lightness, GeneralEffectsBox.tbLightness.Value);
        //        }
        //        //Saturation
        //        if (GeneralEffectsBox.tbSaturation.Value < 255)
        //        {
        //            VisioEditPlayer.Video_Effect_Ex(12, 0, 0, true, VFVideoEffectType.Saturation, GeneralEffectsBox.tbSaturation.Value);
        //        }
        //        //Contrast
        //        if (GeneralEffectsBox.tbContrast.Value > 0)
        //        {
        //            VisioEditPlayer.Video_Effect_Ex(13, 0, 0, true, VFVideoEffectType.Contrast, GeneralEffectsBox.tbContrast.Value);
        //        }
        //        //Darkness
        //        if (GeneralEffectsBox.tbDarkness.Value > 0)
        //        {
        //            VisioEditPlayer.Video_Effect_Ex(14, 0, 0, true, VFVideoEffectType.Darkness, GeneralEffectsBox.tbDarkness.Value);
        //        }

        //        //For Invert
        //        if (GeneralEffectsBox.chkInvert.IsChecked == true)
        //        {
        //            VisioEditPlayer.Video_Effect_Ex(16, 0, 0, GeneralEffectsBox.chkInvert.IsChecked == true, VFVideoEffectType.Invert, 0);
        //        }
        //        //For Gray scale

        //        if (GeneralEffectsBox.chkGrayScale.IsChecked == true)
        //        {
        //            VisioEditPlayer.Video_Effect_Ex(15, 0, 0, GeneralEffectsBox.chkGrayScale.IsChecked == true, VFVideoEffectType.Greyscale, 0);
        //        }

        //        // Chromakey

        //        if (objTemplateList.Where(o => o.MediaType == 605 && o.IsChecked == true).ToList().Count > 0)
        //        {
        //            VisioEditPlayer.ChromaKey_Enabled = true;
        //        }
        //        else
        //        {
        //            VisioEditPlayer.ChromaKey_Enabled = false;
        //        }
        //        VisioEditPlayer.ChromaKey_ContrastHigh = (int)sldContHigh.Value;
        //        VisioEditPlayer.ChromaKey_ContrastLow = (int)sldContLow.Value;
        //        VisioEditPlayer.ChromaKey_ImageFilename = backgroundChromaImage;
        //        if (rdbGreen.IsChecked == true)
        //        {
        //            VisioEditPlayer.ChromaKey_Color = VFChromaColor.Green;
        //            objProcVideoInfo.chromaKeyColor = "Green";
        //        }
        //        else if (rdbBlue.IsChecked == true)
        //        {
        //            VisioEditPlayer.ChromaKey_Color = VFChromaColor.Blue;
        //            objProcVideoInfo.chromaKeyColor = "Blue";
        //        }
        //        else
        //        {
        //            VisioEditPlayer.ChromaKey_Color = VFChromaColor.Red;
        //            objProcVideoInfo.chromaKeyColor = "Red";
        //        }
        //        //Audio effects
        //        VisioEditPlayer.Audio_Preview_Enabled = true;
        //        VisioEditPlayer.Audio_Effects_Clear(-1);
        //        VisioEditPlayer.Audio_Effects_Enabled = true;
        //        if (VisioEditPlayer.Audio_Effects_Enabled)
        //        {
        //            VisioEditPlayer.Audio_Effects_Add(-1, VFAudioEffectType.Amplify, true, -1, -1);
        //            VisioEditPlayer.Audio_Effects_Add(-1, VFAudioEffectType.Equalizer, true, -1, -1);
        //            sldAmplify_ValueChanged(null, null);
        //            sldEqual1_ValueChanged(null, null);
        //            sldEqual2_ValueChanged(null, null);
        //            sldEqual3_ValueChanged(null, null);
        //            sldEqual4_ValueChanged(null, null);
        //            sldEqual5_ValueChanged(null, null);
        //            sldEqual6_ValueChanged(null, null);
        //        }


        //        if (transitionFlag)
        //        {
        //            //Transition effect
        //            if (TransitionBox.cbTransition.IsChecked == true && TransitionPropertyLst != null && TransitionPropertyLst.Count > 0)
        //            {
        //                foreach (TransitionProperty tp in TransitionPropertyLst)
        //                {
        //                    VisioEditPlayer.Video_Transition_Add(tp.TransitionStartTime * 1000, tp.TransitionStopTime * 1000, tp.TransitionID);
        //                }
        //            }
        //        }
        //        //Pan effect
        //        if (PanBox.cbPan.IsChecked == true && PanPropertyLst != null && PanPropertyLst.Count > 0)
        //        {
        //            foreach (PanProperty pp in PanPropertyLst)
        //            {
        //                VisioEditPlayer.Video_Effects_Pan(
        //                    pp.PanID,
        //                    pp.PanStartTime * 1000,
        //                    pp.PanStopTime * 1000,
        //                    PanBox.cbPan.IsChecked == true,
        //                    pp.PanSourceLeft,
        //                    pp.PanSourceTop,
        //                    pp.PanSourceWidth,
        //                    pp.PanSourceHeight,
        //                    pp.PanDestLeft,
        //                    pp.PanDestTop,
        //                    pp.PanDestWidth,
        //                    pp.PanDestHeight);
        //            }
        //        }
        //        //Logos

        //        if (LogoControlBox.chkTextLogo.IsChecked == true)
        //        {
        //            chkTextLogo_Click(null, null);
        //        }
        //        //Graphical Logo
        //        if (isImageAsGraphic)
        //        {
        //            int length = CalculateProcessedVideoLength();
        //            VisioEditPlayer.Video_Effects_Picture_Logo_Ex(specFileName, 1000, 0, length * 1000, true, 0, 0, null, 0, Colors.Transparent, false);
        //        }
        //        if (LogoControlBox.chkGraphicLogo.IsChecked == true && LogoControlBox.LogoSlotList.Count > 0)
        //        {
        //            foreach (SlotProperty lsp in LogoControlBox.LogoSlotList)
        //            {
        //                if (System.IO.Path.GetExtension(lsp.FilePath).ToLower() == ".gif")
        //                {
        //                    TemplateListItems item = objTemplateList.Where(o => o.Item_ID == lsp.ItemID && o.MediaType == 606).FirstOrDefault();
        //                    string imagepath = lsp.FilePath.Replace("Thumbnails\\", "");
        //                    VisioEditPlayer.Video_Effects_Picture_Logo_Ex(lsp.FilePath, lsp.ID, lsp.StartTime * 1000, lsp.StopTime * 1000, true, Convert.ToUInt32(lsp.Left), Convert.ToUInt32(lsp.Top), null, 0, Colors.Transparent, false);
        //                }
        //            }
        //        }
        //        if (BorderControl.chkApplyBorders.IsChecked == true && BorderControl.borderslotlist != null && BorderControl.borderslotlist.Count > 0)
        //        {
        //            foreach (SlotProperty bsp in BorderControl.borderslotlist)
        //            {
        //                string imagepath = bsp.FilePath.Replace("Thumbnails\\", "");
        //                VisioEditPlayer.Video_Effects_Picture_Logo_Ex(imagepath, bsp.ID, bsp.StartTime * 1000, bsp.StopTime * 1000, true, 0, 0, null, 0, Colors.Black, false);
        //            }
        //        }
        //        videoEditorGrid.Children.Add(VisioEditPlayer);
        //        string path = ConfigManager.IMIXConfigurations.ContainsKey((int)ConfigParams.LogFolderPath) ? ConfigManager.IMIXConfigurations[(int)ConfigParams.LogFolderPath].ToString() : string.Empty;
        //        bool isDebugActive = ConfigManager.IMIXConfigurations.ContainsKey((int)ConfigParams.DebugVisioForge) ? Convert.ToBoolean(ConfigManager.IMIXConfigurations[(int)ConfigParams.DebugVisioForge]) : false;
        //        if (!string.IsNullOrEmpty(path) && isDebugActive)
        //        {
        //            VisioEditPlayer.Debug_Mode = true;
        //            VisioEditPlayer.Debug_Dir = path;
        //        }
        //        //Start Processing   
        //        VisioEditPlayer.Start();
        //        //  isImageAsGraphic = false;
        //    }
        //    catch (Exception ex)
        //    {
        //        string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
        //        ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
        //    }
        //}
        private int CalculateProcessedVideoLength()
        {
            int length = 0;
            try
            {
                // For Product-III  (Template video + guest images -> video)
                List<TemplateListItems> objTempList = objTemplateList.Where(o => (o.MediaType == 603 || o.MediaType == 602 || o.MediaType == 607 || (o.MediaType == 601)) && o.IsChecked == true).ToList();//VideoTemplate
                if (objTempList.Count > 0)
                {
                    foreach (TemplateListItems item in objTempList)
                    {
                        if (item.MediaType != 601)// && !item.FilePath.ToLower().EndsWith("png"))
                            length += (int)(item.Length);
                        else
                        {
                            if (objTemplateList.Where(x => x.MediaType == 603 && x.IsChecked == true).ToList().Count == 0)
                                length = Convert.ToInt32(txtImageDisplayTime.Text);
                        }
                    }
                }
                // For Product-I(multiple images -> video), Product-II (guest video + effects -> video) & Product-IV (guest video + guest images + effects -> video)
                else
                {
                    foreach (VideoPage item in PrintOrderPageList.Where(o => o.MediaType != 0 || o.MediaType != null))
                    {
                        if (item.MediaType == 602 || item.MediaType == 607)
                        {
                            length += item.videoEndTime - item.videoStartTime;
                        }
                        else
                        {
                            length += item.ImageDisplayTime;
                        }
                    }
                }
                return length;
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                return 0;
            }
        }

        private void ClearVideoEditPlayer()
        {
            try
            {
                videoEditorGrid.Children.Clear();
                vsMediaFileName = "";
                MediaStop();
                //  objChromaKey("", 1000);
                RemoveColorPlugins();
                RemoveChromaPlugins();
                RemovePlugins();
                if (m_objMixer != null)
                    m_objMixer.ObjectClose();

                if (m_objWriter != null) m_objWriter.ObjectClose();//              
                mMixerList1.ClearList();
                //  Marshal.FinalReleaseComObject(m_objMixer);
                Marshal.FinalReleaseComObject(m_objWriter);
            }
            catch (Exception ex)
            {
                //string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                //ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }

        }

        private void RemoveChromaPlugins()
        {
            m_objMixer.PluginsRemove(objChromaKey);
            Marshal.ReleaseComObject(objChromaKey);
            objChromaKey = null;
            GC.Collect();
        }
        #endregion Processed video events and functions

        #region Right panel event and functions
        private void btnVideo_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                HighlightSelectedButton("btnVideo");
                //chkAddLogo.IsChecked = false;
                //LogoControlBox.stackLogo.Visibility = Visibility.Collapsed;
                txtTemplateRotate.Visibility = Visibility.Visible;
                templateType = "chroma";
                ShowHideControls("video");
                List<TemplateListItems> TLI = objTemplateList.Where(o => o.MediaType == 605 && o.isActive == true).ToList();
                if (TLI.Count == 0)
                    lstTemplates.ItemsSource = null;
                else
                {
                    lstTemplates.ItemsSource = TLI;
                }
                if (objTemplateList.Where(o => (o.MediaType == 602 || o.MediaType == 607) && o.isActive == false).ToList().Count > 0)
                {
                    lstTemplates.IsEnabled = false;
                }
                else
                {
                    lstTemplates.IsEnabled = true;
                }
                txtTemplateRotate.Text = "Chroma Backgrounds";
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }

        private void btnAudio_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                SetTemplateListVisibility(true);
                StackGraphicSettingPanel1.Visibility = Visibility.Collapsed;
                StackGraphicSettingPanel2.Visibility = Visibility.Collapsed;
                //HighlightSelectedButton("btnAudio");
                //chkAddLogo.IsChecked = false;
                //LogoControlBox.stackLogo.Visibility = Visibility.Collapsed;
                txtTemplateRotate.Visibility = Visibility.Visible;
                templateType = "audio";
                //ShowHideControls("audio");
                lstTemplates.IsEnabled = true;
                List<TemplateListItems> TLI = objTemplateList.Where(o => o.MediaType == 604).ToList();
                if (TLI.Count == 0)
                    lstTemplates.ItemsSource = null;
                else
                {
                    lstTemplates.ItemsSource = TLI;
                }
                txtTemplateRotate.Text = "Audio Templates";
                int videoLen = CalculateProcessedVideoLength();
                //txtVideoLength.Text = "Expected video length : " + videoLen.ToString() + " sec";
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }

        }

        private void btnChroma_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                HighlightSelectedButton("btnChroma");
                //chkAddLogo.IsChecked = false;
                //LogoControlBox.stackLogo.Visibility = Visibility.Collapsed;
                txtTemplateRotate.Visibility = Visibility.Visible;
                templateType = "chroma";
                ShowHideControls("chroma");
                lstTemplates.IsEnabled = true;
                List<TemplateListItems> TLI = objTemplateList.Where(o => o.MediaType == 605).ToList();
                if (TLI.Count == 0)
                    lstTemplates.ItemsSource = null;
                else
                {
                    lstTemplates.ItemsSource = TLI;
                }
                txtTemplateRotate.Text = "Chroma Backgrounds";
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }

        private void ShowHideControls(string ctrl)
        {
            try
            {
                lstTemplates.Visibility = System.Windows.Visibility.Visible;
                btnCloseTempList.Visibility = System.Windows.Visibility.Visible;
                if (ctrl == "video")
                {
                    //grpControls.Content = "Video Controls";
                    grdVideoControls.Visibility = Visibility.Visible;
                    //grdAudioControls.Visibility = Visibility.Collapsed;
                    grdChromaControls.Visibility = Visibility.Collapsed;
                    grdVideoCaptureControls.Visibility = Visibility.Collapsed;
                    grdColorEffects.Visibility = Visibility.Collapsed;
                }
                else if (ctrl == "audio")
                {
                    //grpControls.Content = "Audio Controls";
                    grdVideoControls.Visibility = Visibility.Collapsed;
                    //grdAudioControls.Visibility = Visibility.Visible;
                    grdChromaControls.Visibility = Visibility.Collapsed;
                    grdColorEffects.Visibility = Visibility.Collapsed;
                    grdVideoCaptureControls.Visibility = Visibility.Collapsed;
                }
                else if (ctrl == "chroma")
                {
                    //grpControls.Content = "Chroma Controls";
                    grdVideoControls.Visibility = Visibility.Collapsed;
                    //grdAudioControls.Visibility = Visibility.Collapsed;
                    grdChromaControls.Visibility = Visibility.Visible;
                    grdColorEffects.Visibility = Visibility.Collapsed;
                    grdVideoCaptureControls.Visibility = Visibility.Collapsed;
                }
                else if (ctrl == "Video Capture")
                {
                    grdVideoControls.Visibility = Visibility.Collapsed;
                    //grdAudioControls.Visibility = Visibility.Collapsed;
                    grdChromaControls.Visibility = Visibility.Collapsed;
                    grdColorEffects.Visibility = Visibility.Collapsed;
                    grdVideoCaptureControls.Visibility = Visibility.Visible;
                }

                else if (ctrl == "ColorEffects")
                {
                    grdVideoControls.Visibility = Visibility.Collapsed;
                    //grdAudioControls.Visibility = Visibility.Collapsed;
                    grdChromaControls.Visibility = Visibility.Collapsed;
                    grdVideoCaptureControls.Visibility = Visibility.Collapsed;
                    grdColorEffects.Visibility = Visibility.Visible;

                }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }
        private void btnCollapse_Click(object sender, RoutedEventArgs e)
        {
            if (RightPanel.Visibility != System.Windows.Visibility.Collapsed)
            {
                RightPanel.Width = 0;
                LeftPanel.Width = 965;
                RightPanel.Visibility = System.Windows.Visibility.Collapsed;
                //BitmapImage bitMapImg = new BitmapImage(new Uri(@"/DigiPhoto;component/images/LeftArrow.png", UriKind.Relative));
                //System.Drawing.Bitmap be = BitmapImage2Bitmap(bitMapImg);
                //IntPtr ip = be.GetHbitmap();
                //DeleteObject(ip);
                imgCollapse.Source = new BitmapImage(new Uri(@"/images/LeftArrow.png", UriKind.Relative));//bitMapImg; 
                playerMainGrid.HorizontalAlignment = HorizontalAlignment.Right;
            }
            else
            {
                playerMainGrid.HorizontalAlignment = HorizontalAlignment.Left;
                LeftPanel.Width = 700;
                RightPanel.Width = 300;
                RightPanel.Visibility = System.Windows.Visibility.Visible;
                imgCollapse.Source = new BitmapImage(new Uri(@"/images/RightArrow.png", UriKind.Relative));
                //BitmapImage bitMapImg = new BitmapImage(new Uri(@"/DigiPhoto;component/images/RightArrow.png", UriKind.Relative));
                //System.Drawing.Bitmap be = BitmapImage2Bitmap(bitMapImg);
                //IntPtr ip = be.GetHbitmap();
                //DeleteObject(ip);
                //imgCollapse.Source = bitMapImg; 
            }
        }


        #endregion Right panel event and functions

        #region Drop List event and functions
        [System.Runtime.InteropServices.DllImport("gdi32.dll")]
        public static extern bool DeleteObject(IntPtr hObject);
        private System.Drawing.Bitmap BitmapImage2Bitmap(BitmapImage bitmapImage)
        {
            using (MemoryStream outStream = new MemoryStream())
            {
                BitmapEncoder enc = new BmpBitmapEncoder();
                enc.Frames.Add(BitmapFrame.Create(bitmapImage));
                enc.Save(outStream);
                System.Drawing.Bitmap bitmap = new System.Drawing.Bitmap(outStream);

                return new System.Drawing.Bitmap(bitmap);
            }
        }
        private void Image_Drop(object sender, DragEventArgs e)
        {
            try
            {
                bool iscontinue = false;
                if (!IsProcessedVideoSaved)
                {
                    iscontinue = MsgBox.ShowHandlerDialog("Do you wish to save changes?", DigiMessageBox.DialogType.Confirm);
                }
                if ((!IsProcessedVideoSaved && iscontinue) || IsProcessedVideoSaved)
                {
                    IsProcessedVideoSaved = true;
                    btnSave.IsEnabled = false;
                    VideoElements lstMyItem = lstVideoElements.Where(o => o.PhotoId == this.DropPhotoId).FirstOrDefault();
                    BitmapImage bitMapImg = new BitmapImage();
                    bitMapImg.BeginInit();
                    bitMapImg.UriSource = new Uri(lstMyItem.GuestImagePath, UriKind.Relative);
                    bitMapImg.DecodePixelWidth = 50;
                    bitMapImg.EndInit();
                    System.Drawing.Bitmap be = BitmapImage2Bitmap(bitMapImg);
                    IntPtr ip = be.GetHbitmap();
                    DeleteObject(ip);
                    string recName = ((Rectangle)sender).Name;
                    object objectName = ((Rectangle)sender).FindName("ViewBox1");
                    object item2 = ((Rectangle)sender).FindName("img1");
                    if (item2 is Image)
                    {
                        Image img2 = (Image)item2;
                        Rectangle objrect = (Rectangle)sender;
                        int activePage = int.Parse(objrect.Uid.ToString());
                        VideoPage objCurrentPage = PrintOrderPageList.Where(o => o.PageNo == activePage).FirstOrDefault();
                        var chkProVideoExists = PrintOrderPageList.Where(o => o.PhotoId == this.DropPhotoId).FirstOrDefault();
                        if (chkProVideoExists != null)
                        {
                            if (chkProVideoExists.MediaType == 607)
                            {
                                MessageBox.Show("Same processed video cannot be added more than once!!\n", "Digiphoto", MessageBoxButton.OK, MessageBoxImage.Information, MessageBoxResult.OK);
                                //  MsgBox.ShowHandlerDialog("Same processed video cannot be added more than once!", DigiMessageBox.DialogType.OK);
                                return;

                            }
                        }
                        if (objCurrentPage.PhotoId == 0)
                        {
                            int blankContainer = PrintOrderPageList.Where(o => o.PhotoId == 0).Count();
                            objCurrentPage = PrintOrderPageList.Where(o => o.PhotoId == 0).FirstOrDefault();
                            img2.Source = bitMapImg;
                            objCurrentPage.FilePath = lstMyItem.GuestImagePath;
                            objCurrentPage.PhotoId = lstMyItem.PhotoId;
                            objCurrentPage.Name = lstMyItem.Name;
                            objCurrentPage.DropVideoPath = lstMyItem.VideoFilePath;
                            objCurrentPage.IsGuestImage = true;
                            if (lstMyItem.MediaType == 2)
                            {
                                objCurrentPage.MediaType = 602; // Guest video
                                objCurrentPage.videoLength = lstMyItem.videoLength;
                                objCurrentPage.videoStartTime = 0;
                                objCurrentPage.videoEndTime = (int)objCurrentPage.videoLength;
                                objCurrentPage.tooltip = "Video length: " + objCurrentPage.videoLength + " sec.";
                            }
                            else if (lstMyItem.MediaType == 3)
                            {
                                objCurrentPage.MediaType = 607; // Processed video
                                objCurrentPage.videoLength = lstMyItem.videoLength;
                                objCurrentPage.videoStartTime = 0;
                                objCurrentPage.videoEndTime = (int)objCurrentPage.videoLength;
                                objCurrentPage.tooltip = "Video length: " + objCurrentPage.videoLength + " sec.";
                            }
                            else
                            {
                                objCurrentPage.MediaType = 601; // Guest image
                                if (objCurrentPage.ImageDisplayTime == 0 || objCurrentPage.ImageDisplayTime == null)
                                {
                                    objCurrentPage.ImageDisplayTime = 10;
                                }
                                objCurrentPage.tooltip = "Image display time: " + objCurrentPage.ImageDisplayTime + " sec.";
                            }
                            blankContainer = PrintOrderPageList.Where(o => o.PhotoId == 0).Count();
                            if (blankContainer < 1 && !isVideoTempSelected)
                            {
                                objCurrentPage = new VideoPage();
                                objCurrentPage.PageNo = PrintOrderPageList.Count + 1;
                                objCurrentPage.slotTime = "Drop here!";
                                PrintOrderPageList.Add(objCurrentPage);
                            }
                        }
                        else
                        {
                            int pageNum = objCurrentPage.PageNo;
                            ShuffleList(pageNum, false);
                            objCurrentPage = new VideoPage();
                            objCurrentPage.PageNo = pageNum;
                            img2.Source = bitMapImg;
                            objCurrentPage.FilePath = lstMyItem.GuestImagePath;
                            objCurrentPage.PhotoId = lstMyItem.PhotoId;

                            objCurrentPage.Name = lstMyItem.Name;
                            objCurrentPage.IsGuestImage = true;
                            objCurrentPage.DropVideoPath = lstMyItem.VideoFilePath;
                            objCurrentPage.slotTime = "Drop here!";
                            if (lstMyItem.MediaType == 2)
                            {
                                objCurrentPage.MediaType = 602; // Guest video
                                objCurrentPage.videoLength = lstMyItem.videoLength;
                                objCurrentPage.videoStartTime = 0;
                                objCurrentPage.videoEndTime = (int)objCurrentPage.videoLength;
                                objCurrentPage.tooltip = "Video length: " + objCurrentPage.videoLength + " sec.";
                            }
                            else if (lstMyItem.MediaType == 3)
                            {
                                objCurrentPage.MediaType = 607; // Processed video
                                objCurrentPage.videoLength = lstMyItem.videoLength;
                                objCurrentPage.videoStartTime = 0;
                                objCurrentPage.videoEndTime = (int)objCurrentPage.videoLength;
                                objCurrentPage.tooltip = "Video length: " + objCurrentPage.videoLength + " sec.";
                            }
                            else
                            {
                                objCurrentPage.MediaType = 601; // Guest image
                                if (objCurrentPage.ImageDisplayTime == 0 || objCurrentPage.ImageDisplayTime == null)
                                {
                                    objCurrentPage.ImageDisplayTime = 10;
                                }
                                objCurrentPage.tooltip = "Image display time: " + objCurrentPage.ImageDisplayTime + " sec.";
                            }
                            PrintOrderPageList.Add(objCurrentPage);
                            int blankContainer = PrintOrderPageList.Where(o => o.PhotoId == 0).Count();
                            if (blankContainer > 1)
                            {
                                objCurrentPage = PrintOrderPageList.Where(o => o.PhotoId == 0).LastOrDefault();
                                if (objCurrentPage != null)
                                    PrintOrderPageList.Remove(objCurrentPage);
                            }
                            else if (blankContainer < 1 && !isVideoTempSelected)
                            {
                                objCurrentPage = new VideoPage();
                                objCurrentPage.PageNo = PrintOrderPageList.Count + 1;
                                objCurrentPage.slotTime = "Drop here!";
                                PrintOrderPageList.Add(objCurrentPage);
                            }
                            if (isVideoTempSelected)
                            {
                                if (PrintOrderPageList.Count > defaultProcessListCount)
                                {
                                    List<VideoPage> temp = PrintOrderPageList.Where(o => o.PageNo > defaultProcessListCount).OrderBy(o => o.PageNo).ToList();
                                    foreach (VideoPage vp in temp)
                                    {
                                        PrintOrderPageList.Remove(vp);
                                    }
                                }
                            }
                        }
                        BindPageStrips();
                        int videoLen = CalculateProcessedVideoLength();
                        UpdateGuestImage();
                        //txtVideoLength.Text = "Expected video length : " + videoLen.ToString() + " sec";
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }

        }
        private void ShuffleList(int pagenum, bool isDeleted)
        {
            if (!isDeleted)
            {
                foreach (VideoPage objpage in PrintOrderPageList)
                {
                    if (objpage.PageNo >= pagenum)
                    {
                        objpage.PageNo = objpage.PageNo + 1;
                    }
                }
            }
            else
            {
                foreach (VideoPage objpage in PrintOrderPageList)
                {
                    if (objpage.PageNo >= pagenum)
                    {
                        objpage.PageNo = objpage.PageNo - 1;
                    }
                }
            }
        }
        public void BindPageStrips()
        {
            try
            {
                if (PrintOrderPageList.Count() == 0)
                {
                    VideoPage objPrintOrderPage = null;
                    for (int i = 1; i <= defaultProcessListCount; i++)
                    {
                        objPrintOrderPage = new VideoPage();
                        objPrintOrderPage.PageNo = i;
                        if (isVideoTempSelected)
                        {
                            objPrintOrderPage.slotTime = "Slot - " + i.ToString() + "\n" + slotList[i - 1].FrameTimeIn.ToString() + " sec";
                            objPrintOrderPage.ImageDisplayTime = slotList[i - 1].PhotoDisplayTime;
                            objPrintOrderPage.tooltip = "Image display time: " + objPrintOrderPage.ImageDisplayTime + " sec.";
                        }
                        else objPrintOrderPage.slotTime = "Drop here!";
                        if (objPrintOrderPage != null)
                            PrintOrderPageList.Add(objPrintOrderPage);
                    }
                    PrintOrderPageList.Reverse();
                }
                else
                {
                    for (int i = 1; i <= defaultProcessListCount; i++)
                    {
                        VideoPage objPrintOrderPage = PrintOrderPageList[i - 1];
                        if (isVideoTempSelected)
                        {
                            objPrintOrderPage.slotTime = "Slot - " + i.ToString() + "\n" + slotList[i - 1].FrameTimeIn.ToString() + " sec";
                            objPrintOrderPage.ImageDisplayTime = slotList[i - 1].PhotoDisplayTime;
                            objPrintOrderPage.tooltip = "Image display time: " + objPrintOrderPage.ImageDisplayTime + " sec.";
                        }
                        else
                        {
                            objPrintOrderPage.slotTime = "Drop here!";
                        }
                    }
                }
                lstDragImages.ItemsSource = PrintOrderPageList.OrderBy(o => o.PageNo).ToList();// (o => o.PageNo);     

            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }

        private void UpdateGuestImage()
        {
            if (ChangeGuestElement)
            {
                try
                {
                    if (PrintOrderPageList.Where(o => o.PageNo == 1 && o.FilePath != "" && o.FilePath != null).FirstOrDefault() != null)
                    {
                        var item = PrintOrderPageList.Where(o => o.PageNo == 1 && o.FilePath != "" && o.FilePath != null).FirstOrDefault();
                        SetFirstGuestStream(item.DropVideoPath);
                    }
                    else
                    {
                        LoadFirstItemOnMixer();
                    }
                }
                catch { }
            }
            ChangeGuestElement = true;
        }
        private void ContentControl_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            VideoPage lstMyItems = new VideoPage();
            lstMyItems = PrintOrderPageList.Where(o => o.PageNo == activePage && o.PhotoId == DropPhotoId).FirstOrDefault();
            RemoveItem(lstMyItems);
        }

        private void RemoveItem(VideoPage item)
        {
            try
            {
                if (this.DropPhotoId != 0)
                {
                    string name = item.Name;
                    bool iscontinue = false;
                    if (!IsProcessedVideoSaved)
                    {
                        MediaStop();
                        iscontinue = MsgBox.ShowHandlerDialog("Do you wish to save changes?", DigiMessageBox.DialogType.Confirm);
                    }
                    else
                    {
                        iscontinue = (MessageBox.Show("Do you want to remove the selected item- '" + name + "' from processing list?", "Digiphoto", MessageBoxButton.YesNo, MessageBoxImage.Question, MessageBoxResult.No) == MessageBoxResult.Yes);
                        //  iscontinue = MsgBox.ShowHandlerDialog("Do you want to remove the selected item- '" + name + "' from processing list?", DigiMessageBox.DialogType.Confirm);

                    }
                    if ((!IsProcessedVideoSaved && iscontinue) || iscontinue)
                    {
                        IsProcessedVideoSaved = true;
                        btnSave.IsEnabled = false;
                        PrintOrderPageList.Remove(item);
                        ShuffleList(item.PageNo + 1, true);
                        if (PrintOrderPageList.Count < defaultProcessListCount)
                        {
                            item = new VideoPage();
                            item.PageNo = PrintOrderPageList.Count + 1;
                            PrintOrderPageList.Add(item);
                        }
                        BindPageStrips();
                        //btReplay.Visibility = Visibility.Collapsed;
                        vsMediaFileName = "";
                        //VisioMediaPlayer.FilenamesOrURL.Clear();
                        //VisioMediaPlayer.BackgroundImage_Source = null;
                        //txtVideoLength.Text = "Expected video length : " + CalculateProcessedVideoLength() + " sec";
                        UpdateGuestImage();
                    }
                    else
                    {
                        //MediaStop();
                        vsMediaFileName = processVideoTemp + @"\" + "Output." + outputFormat;
                        MediaPlay();
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }

        //Lst DragImages Stack Panel Click Event
        private void lstDragImagesStackPanel_MouseDown(object sender, MouseButtonEventArgs e)
        {
            SelectDragImageOrVideo(sender);
        }
        private void lstDragImagesStackPanel_TouchDown(object sender, TouchEventArgs e)
        {
            SelectDragImageOrVideo(sender);
            if (IsDoubleTap(e))
            {
                VideoPage lstMyItems = new VideoPage();
                lstMyItems = PrintOrderPageList.Where(o => o.PageNo == activePage && o.PhotoId == DropPhotoId).FirstOrDefault();
                if (lstMyItems != null)
                    RemoveItem(lstMyItems);
            }
        }
        string extractVideoPath = "";
        long extractVideoEndTime = 0;
        private void SelectDragImageOrVideo(object sender)
        {
            try
            {
                VideoPage lstMyItems = new VideoPage();
                if (sender is StackPanel)
                {
                    StackPanel ob = sender as StackPanel;
                    DropPhotoId = Convert.ToInt32(ob.Tag.ToString());
                    int page = Convert.ToInt32(ob.Uid);
                    lstMyItems = PrintOrderPageList.Where(o => o.PhotoId == this.DropPhotoId && o.PageNo == page).FirstOrDefault();
                    extractVideoPath = lstMyItems.DropVideoPath;
                    extractVideoEndTime = lstMyItems.videoLength;
                }
                this.activePage = lstMyItems.PageNo;
                if (!string.IsNullOrEmpty(lstMyItems.Name))
                {
                    //chkAddLogo.IsChecked = false;
                    //LogoControlBox.stackLogo.Visibility = Visibility.Collapsed;
                    if (lstMyItems.MediaType == 602 || lstMyItems.MediaType == 607)
                    {
                        StackVideoTemplatePlay.Visibility = Visibility.Collapsed;
                        StackImageDisplayTime.Visibility = Visibility.Collapsed;
                        // StackGuestVideoTemplate.Visibility = Visibility.Collapsed;
                        StackVideoStartEndTime.Visibility = Visibility.Visible;
                        StackGraphicSettingPanel1.Visibility = Visibility.Collapsed;
                        StackGraphicSettingPanel2.Visibility = Visibility.Collapsed;
                        txtvideoStart.Text = lstMyItems.videoStartTime.ToString();
                        txtVideoEnd.Text = lstMyItems.videoEndTime.ToString();
                        RangeSldGuestVideo.LowerValue = lstMyItems.videoStartTime;
                        RangeSldGuestVideo.UpperValue = lstMyItems.videoEndTime;
                        RangeSldGuestVideo.Maximum = lstMyItems.videoLength;
                    }
                    else
                    {
                        StackVideoTemplatePlay.Visibility = Visibility.Collapsed;
                        txtImageDisplayTime.Text = lstMyItems.ImageDisplayTime.ToString();
                        StackImageDisplayTime.Visibility = Visibility.Visible;
                        StackVideoStartEndTime.Visibility = Visibility.Collapsed;
                        StackGraphicSettingPanel1.Visibility = Visibility.Collapsed;
                        StackGraphicSettingPanel2.Visibility = Visibility.Collapsed;
                        //  StackGuestVideoTemplate.Visibility = Visibility.Collapsed;
                        vsMediaFileName = lstMyItems.FilePath.Replace("Thumbnails", "Thumbnails_Big");
                    }
                }
                else
                {
                    StackImageDisplayTime.Visibility = Visibility.Collapsed;
                    StackVideoStartEndTime.Visibility = Visibility.Collapsed;
                    StackVideoTemplatePlay.Visibility = Visibility.Collapsed;
                    StackGraphicSettingPanel1.Visibility = Visibility.Collapsed;
                    StackGraphicSettingPanel2.Visibility = Visibility.Collapsed;
                    //  StackGuestVideoTemplate.Visibility = Visibility.Collapsed;

                }
                if (isVideoTempSelected)
                {
                    btnSetDisplayTime.IsEnabled = false;
                    chkImgDTSetToAll.IsEnabled = false;
                }
                else
                {
                    btnSetDisplayTime.IsEnabled = true;
                    chkImgDTSetToAll.IsEnabled = true;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }
        private void btnDropImgPlay_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                VideoPage lstMyItems = new VideoPage();
                Button ob = sender as Button;
                DropPhotoId = Convert.ToInt32(ob.Tag.ToString());
                lstMyItems = PrintOrderPageList.Where(o => o.PhotoId == this.DropPhotoId).FirstOrDefault();
                if (!string.IsNullOrEmpty(lstMyItems.Name))
                {
                    if (lstMyItems.MediaType == 602 || lstMyItems.MediaType == 607)
                    {
                        //MediaStop();
                        vsMediaFileName = lstMyItems.DropVideoPath;
                        MediaPlay();
                    }
                    else
                    {
                        MediaStop();
                        vsMediaFileName = lstMyItems.FilePath.Replace("Thumbnails", "Thumbnails_Big");
                        //ImageSource imageSource = new BitmapImage(new Uri(vsMediaFileName, UriKind.RelativeOrAbsolute));
                        //VisioMediaPlayer.BackgroundImage_Source = (BitmapSource)imageSource;
                        MediaPlay();
                        //mpc = new MediaPlayerControl(vsMediaFileName, "VideoEditor", "", false, true);
                        //gdMediaPlayer.Children.Add(mpc);
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }
        #endregion Drop List event and functions

        #region Lower strip functions end events
        /// <summary>
        /// Fills the product combo.
        /// </summary>
        private void FillProductCombo()
        {
            Dictionary<string, string> lstProductList = new Dictionary<string, string>();
            lstProductList.Add("Select Product", "0");
            try
            {
                ProcessedVideoBusiness objPV = new ProcessedVideoBusiness();
                lstVideoProduct = objPV.GetVideoPackages();
                foreach (VideoProducts item in lstVideoProduct.OrderBy(o => o.ProductID))
                {
                    if (!(lstProductList.Where(o => o.Key == item.ProductName).Count() > 0))
                        lstProductList.Add(item.ProductName, item.ProductID.ToString());

                }
                CmbProductType.ItemsSource = lstProductList;
                if (lstProductList.Count > 1)
                {
                    CmbProductType.SelectedIndex = 1;
                }
                else
                    CmbProductType.SelectedValue = "0";
            }
            catch (Exception ex)
            {
                CmbProductType.ItemsSource = lstProductList;
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }
        private static string GetFileExt(string filename)
        {
            int k = filename.LastIndexOf('.');
            return filename.Substring(k, filename.Length - k);
        }
        //Button Select All
        private void btnSelectAll_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                bool iscontinue = false;
                MediaStop();
                //To remove guest image graphic
                objTemplateList.RemoveAll(o => o.MediaType == 601 && o.IsChecked == true);

                if (!IsProcessedVideoSaved)
                {
                    iscontinue = MsgBox.ShowHandlerDialog("Do you wish to save changes?", DigiMessageBox.DialogType.Confirm);
                }
                if ((!IsProcessedVideoSaved && iscontinue) || IsProcessedVideoSaved)
                {
                    IsProcessedVideoSaved = true;
                    btnSave.IsEnabled = false;
                    int a = 1;
                    VideoPage objCurrentPage;
                    PrintOrderPageList.Clear();
                    foreach (VideoElements ve in lstVideoElements)
                    {
                        if (isVideoTempSelected && PrintOrderPageList.Count == slotList.Count)
                        {
                            break;
                        }
                        objCurrentPage = new VideoPage();
                        objCurrentPage.FilePath = ve.GuestImagePath;
                        objCurrentPage.PhotoId = ve.PhotoId;

                        objCurrentPage.Name = ve.Name;
                        objCurrentPage.DropVideoPath = ve.VideoFilePath;
                        objCurrentPage.PageNo = a;
                        objCurrentPage.IsGuestImage = true;

                        if (ve.MediaType == 2)
                        {
                            objCurrentPage.MediaType = 602; // Guest video
                            objCurrentPage.videoLength = ve.videoLength;
                            objCurrentPage.videoStartTime = 0;
                            objCurrentPage.videoEndTime = (int)objCurrentPage.videoLength;
                            objCurrentPage.tooltip = "Video length: " + objCurrentPage.videoLength + " sec.";
                        }
                        else if (ve.MediaType == 3)
                        {
                            objCurrentPage.MediaType = 607; // Processed video
                            objCurrentPage.videoLength = ve.videoLength;
                            objCurrentPage.videoStartTime = 0;
                            objCurrentPage.videoEndTime = (int)objCurrentPage.videoLength;
                            objCurrentPage.tooltip = "Video length: " + objCurrentPage.videoLength + " sec.";
                        }
                        else
                        {
                            objCurrentPage.MediaType = 601; // Guest image
                            if (isVideoTempSelected && slotList.Count <= a)
                            {
                                objCurrentPage.ImageDisplayTime = slotList[a - 1].PhotoDisplayTime;
                            }
                            else
                            {
                                objCurrentPage.ImageDisplayTime = 10;
                            }
                            if (objCurrentPage.ImageDisplayTime == 0 || objCurrentPage.ImageDisplayTime == null)
                            {
                                objCurrentPage.ImageDisplayTime = 10;
                            }
                            objCurrentPage.tooltip = "Image display time: " + objCurrentPage.ImageDisplayTime + " sec.";
                        }
                        PrintOrderPageList.Add(objCurrentPage);
                        a++;
                    }
                    if (PrintOrderPageList.Count < defaultProcessListCount)
                    {
                        for (int count = PrintOrderPageList.Count; count < defaultProcessListCount; count++)
                        {
                            objCurrentPage = new VideoPage();
                            objCurrentPage.PageNo = count;
                            objCurrentPage.slotTime = "Drop here!";
                            PrintOrderPageList.Add(objCurrentPage);
                        }
                    }
                    int blankContainer = PrintOrderPageList.Where(o => o.PhotoId == 0).Count();
                    if (blankContainer < 1 && !isVideoTempSelected)
                    {
                        objCurrentPage = new VideoPage();
                        objCurrentPage.PageNo = PrintOrderPageList.Count + 1;
                        objCurrentPage.slotTime = "Drop here!";
                        PrintOrderPageList.Add(objCurrentPage);
                    }
                    BindPageStrips();
                    int videoLen = CalculateProcessedVideoLength();
                    UpdateGuestImage();
                    //txtVideoLength.Text = "Expected video length : " + videoLen.ToString() + " sec";
                }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }

        }
        private void btnReset_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                bool iscontinue = false;
                if (!IsProcessedVideoSaved)
                {
                    iscontinue = MsgBox.ShowHandlerDialog("Do you wish to save changes?", DigiMessageBox.DialogType.Confirm);
                }
                if ((!IsProcessedVideoSaved && iscontinue) || IsProcessedVideoSaved)
                {
                    IsProcessedVideoSaved = true;
                    btnSave.IsEnabled = false;
                    PrintOrderPageList.Clear();
                    BindPageStrips();
                    // VisioEditPlayer.Stop();
                    MediaStop();
                    // VideoEditPlayerPanel.Visibility = Visibility.Collapsed;
                    //  vidoriginal.Visibility = Visibility.Visible;
                    int videoLen = CalculateProcessedVideoLength();
                    //txtVideoLength.Text = "Expected video length : " + videoLen.ToString() + " sec";
                    string root = Environment.CurrentDirectory;
                    string capturedFramePath = root + "\\";
                    capturedFramePath = System.IO.Path.Combine(capturedFramePath, "CapturedFrames\\");
                    if (Directory.Exists(capturedFramePath))
                    {
                        Directory.Delete(capturedFramePath);
                    }
                    //    lblShotCount.Text = "(" + "0" + ")";

                    UpdateGuestImage();
                }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }
        private static volatile object _lockObject = new object();
        public static bool isVideoEditorStopped = true;
        private delegate void EditorStopDelegate();
        //private void btnStopProcess_Click(object sender, RoutedEventArgs e)
        //{
        //    try
        //    {
        //        lock (_lockObject)
        //        {
        //            DispatcherOperation opEditor = VisioEditPlayer.Dispatcher.BeginInvoke(new EditorStopDelegate(this.EditorStopDelegateMethod));
        //            opEditor.Completed += opEditor_Completed;
        //        }
        //    }
        //    catch (Exception ex)
        //    {

        //    }

        //    // VisioEditPlayer.Stop();
        //    // pbProgress.Value = 0;           

        //}
        //private void EditorStopDelegateMethod()
        //{
        //    VisioEditPlayer.Stop();
        //    pbProgress.Value = 0;
        //}
        //void opEditor_Completed(object sender, EventArgs e)
        //{
        //    isVideoEditorStopped = true;
        //    var cur = new BusyWindow();
        //    if (VisioEditPlayer.Status == VFVideoEditStatus.Free)
        //    {
        //        try
        //        {
        //            cur.Dispatcher.BeginInvoke(new Action(() => { cur.Show(); }));
        //            isImageAsGraphic = false;
        //            Thread.Sleep(1000);
        //            VisioEditPlayer.BeginInit();
        //            VideoEditPlayerPanel.Visibility = Visibility.Collapsed;
        //            vidoriginal.Visibility = Visibility.Visible;
        //            IsProcessedVideoSaved = true;
        //            btnSave.IsEnabled = false;
        //            grdVideoCaptureControls.IsEnabled = true;
        //            vsMediaFileName = string.Empty;
        //            isReplay = false;
        //            ClearVideoEditPlayer();
        //            ClearImageSourceControl();
        //            VisioEditPlayer.EndInit();
        //            EnableControls();
        //            cur.Dispatcher.BeginInvoke(new Action(() => { cur.Hide(); }));
        //        }
        //        catch
        //        {
        //            cur.Hide();
        //        }
        //        finally { }
        //    }

        //    isVideoEditorStopped = false;
        //}
        #region Old Save
        ////private void btnSave_Click(object sender, RoutedEventArgs e)
        ////{
        ////    try
        ////    {
        ////        MediaStop();
        ////        ClearImageSourceControl();
        ////        int lenth = CalculateProcessedVideoLength();
        ////        objProcVideoInfo.VideoLength = lenth;
        ////        ClearVideoEditPlayer();
        ////        lstVideoPage = PrintOrderPageList.ToList();
        ////        productId = Convert.ToInt32(CmbProductType.SelectedValue);
        ////        if (IsProcessedVideoEditing)
        ////        {
        ////            if (!MsgBox.ShowHandlerDialog("Click 'Yes' to overwrite or click 'No' to save a new video.", DigiMessageBox.DialogType.Confirm))
        ////            {
        ////                isCreateNew = true;
        ////            }
        ////            else
        ////                lstGuestImages.ItemsSource = null;
        ////        }
        ////        bs.Show();
        ////        bwVideoEditing.RunWorkerAsync();
        ////        //lstGuestImages.ItemsSource = lstVideoElements;
        ////    }
        ////    catch (Exception ex)
        ////    {
        ////        string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
        ////        ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
        ////    }
        ////}
        //private void SaveProcessedVideoDetails(string effects, string firstRFID, string outputFile, int processedVideoId)
        //{
        //    try
        //    {
        //        objProcVideoInfo.Effects = effects;
        //        objProcVideoInfo.CreatedBy = LoginUser.UserId;
        //        objProcVideoInfo.CreatedOn = System.DateTime.Now;
        //        objProcVideoInfo.OutputVideoFileName = outputFile;
        //        objProcVideoInfo.ProcessedVideoId = 0;
        //        objProcVideoInfo.ProductId = productId;
        //        objProcVideoInfo.FirstMediaRFID = firstRFID;
        //        objProcVideoInfo.SubStoreId = ConfigManager.SubStoreId;
        //        objProcVideoInfo.VideoId = processedVideoId;


        //        ProcessedVideoBusiness objPVBuss = new ProcessedVideoBusiness();
        //        List<ProcessedVideoDetailsInfo> lstPVDetails = new List<ProcessedVideoDetailsInfo>();// PrintOrderPageList.ToList();

        //        //For DropList Items             
        //        int a = 0;
        //        foreach (VideoPage item in lstVideoPage.Where(o => o.PhotoId != null && o.PhotoId != 0).OrderBy(o => o.PageNo))
        //        {
        //            ProcessedVideoDetailsInfo objinfo = new ProcessedVideoDetailsInfo();
        //            objinfo.MediaType = item.MediaType;
        //            objinfo.MediaId = item.PhotoId;
        //            objinfo.JoiningOrder = a + 1;
        //            objinfo.FrameTime = 0;
        //            objinfo.DisplayTime = item.ImageDisplayTime;
        //            objinfo.StartTime = item.videoStartTime;
        //            objinfo.EndTime = item.videoEndTime;

        //            lstPVDetails.Add(objinfo);
        //            a++;

        //        }
        //        //For Template Items
        //        List<TemplateListItems> lstCheckedtemplates = objTemplateList.Where(o => o.IsChecked == true).ToList();
        //        foreach (TemplateListItems objchecked in lstCheckedtemplates)
        //        {
        //            ProcessedVideoDetailsInfo objinfo = new ProcessedVideoDetailsInfo();
        //            objinfo.MediaType = objchecked.MediaType;
        //            objinfo.MediaId = objchecked.Item_ID;
        //            objinfo.JoiningOrder = 0;
        //            objinfo.FrameTime = objchecked.InsertTime;
        //            objinfo.StartTime = objchecked.StartTime;
        //            objinfo.EndTime = objchecked.EndTime;
        //            lstPVDetails.Add(objinfo);
        //        }
        //        objPVBuss.SaveProcessedVideoAndDetails(objProcVideoInfo, lstPVDetails);
        //    }
        //    catch (Exception ex)
        //    {
        //        string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
        //        ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
        //    }
        //}
        //private void bwVideoEditing_DoWork(object Sender, System.ComponentModel.DoWorkEventArgs e)
        //{
        //    try
        //    {
        //        VideoProcessingClass obj = new VideoProcessingClass();
        //        string outputFile = string.Empty;
        //        string firstRFID = string.Empty;
        //        string videoFileTosave = string.Empty;
        //        string videoThumbnil = string.Empty;
        //        int processedVideoId = 0;
        //        string thumbnailPath = LoginUser.DigiFolderThumbnailPath + DateTime.Now.ToString("yyyyMMdd");
        //        if (!Directory.Exists(thumbnailPath))
        //        {
        //            Directory.CreateDirectory(thumbnailPath);
        //        }
        //        if ((IsProcessedVideoEditing && isCreateNew) || !IsProcessedVideoEditing)
        //        {
        //            string pathToSave = LoginUser.DigiFolderProcessedVideosPath + DateTime.Now.ToString("yyyyMMdd");
        //            if (!Directory.Exists(pathToSave))
        //            {
        //                Directory.CreateDirectory(pathToSave);
        //            }
        //            VideoPage objpage = lstVideoPage.Where(o => o.MediaType != 0).OrderBy(o => o.PageNo).FirstOrDefault();
        //            if (objpage != null)
        //            {

        //                outputFile = objpage.Name + "_" + DateTime.Now.ToString("HH:mm").Replace(":", "") + "." + outputFormat;
        //                //TO get guest video Id                          
        //                firstRFID = objpage.Name;
        //                videoFileTosave = pathToSave + "\\" + outputFile;
        //                File.Copy(processVideoTemp + "Output." + outputFormat, videoFileTosave);
        //                // CopyVideoToDisplayFolder(videoFileTosave);
        //                if (isGroup)
        //                {
        //                    //Save Processed Videos In group
        //                    LstMyItems _myitem = new LstMyItems();
        //                    _myitem.MediaType = 3;
        //                    _myitem.VideoLength = objProcVideoInfo.VideoLength;
        //                    _myitem.FilePath = thumbnailPath + "\\" + outputFile.Replace(".avi", ".jpg").Replace(".mp4", ".jpg").Replace(".wmv", ".jpg");
        //                    _myitem.Name = objpage.Name;
        //                    _myitem.IsPassKeyVisible = Visibility.Collapsed;
        //                    _myitem.PhotoId = objpage.PhotoId;
        //                    _myitem.FileName = outputFile;
        //                    _myitem.CreatedOn = DateTime.Now;
        //                    _myitem.IsLocked = Visibility.Visible;
        //                    _myitem.HotFolderPath = LoginUser.DigiFolderPath;
        //                    if (RobotImageLoader.GroupImages.Count > 0)
        //                        _myitem.BmpImageGroup = RobotImageLoader.GroupImages[0].BmpImageGroup;
        //                    //_myitem.BmpImageGroup = new BitmapImage(new Uri(@"/images/view-accept.png", UriKind.Relative));
        //                    //_myitem.FrameBrdr = ((LstMyItems)(lstImages.SelectedItem)).FrameBrdr;
        //                    RobotImageLoader.GroupImages.Add(_myitem);
        //                    RobotImageLoader.ViewGroupedImagesCount.Add(_myitem.Name);
        //                }
        //            }
        //            else if (isImageAsGraphic) // case of live video capture
        //            {
        //                VideoElements ve = lstVideoElements[0];
        //                outputFile = ve.Name + "_" + DateTime.Now.ToString("HH:mm").Replace(":", "") + "." + outputFormat;
        //                firstRFID = ve.Name;
        //                videoFileTosave = pathToSave + "\\" + outputFile;
        //                File.Copy(processVideoTemp + "Output." + outputFormat, videoFileTosave, true);
        //            }// case of live video capture
        //            else
        //            {
        //                VideoElements ve = lstVideoElements[0];
        //                outputFile = ve.Name + "_" + DateTime.Now.ToString("HH:mm").Replace(":", "") + "." + "avi";
        //                firstRFID = ve.Name;
        //                videoFileTosave = pathToSave + "\\" + outputFile;
        //                File.Copy(processVideoTemp + "Output.avi", videoFileTosave, true);
        //                //  obj.ExtractThumbnailFromVideo(videoFileTosave, 4, 4, processVideoTemp + "Output.jpg");
        //                Dispatcher.Invoke(new Action(() =>
        //                                  ThumbnailExtractor.ExtractThumbnailFromVideo(videoFileTosave, 4, 4, processVideoTemp + "Output.jpg", MediaPlayerKey, UserName, EmailId)
        //                                ));
        //            }
        //            videoThumbnil = thumbnailPath + "\\" + System.IO.Path.GetFileNameWithoutExtension(outputFile) + ".jpg";
        //            ResizeWPFImage(processVideoTemp + "Output.jpg", 210, videoThumbnil);
        //        }
        //        else
        //        {
        //            videoFileTosave = LoginUser.DigiFolderProcessedVideosPath + "\\" + Convert.ToDateTime(lstProcessedVideo.CreatedOn).ToString("yyyyMMdd") + "\\" + lstProcessedVideo.OutputVideoFileName;
        //            outputFile = lstProcessedVideo.OutputVideoFileName;
        //            //if (File.Exists(videoFileTosave))
        //            //{
        //            //    File.Delete(videoFileTosave);
        //            //}
        //            File.Copy(processVideoTemp + "Output." + outputFormat, videoFileTosave, true);
        //            //Dispatcher.Invoke(new Action(() =>
        //            //                     ThumbnailExtractor.ExtractThumbnailFromVideo(videoFileTosave, 4, 4, processVideoTemp + "Output.jpg", MediaPlayerKey, UserName, EmailId)
        //            //                   ));
        //            videoThumbnil = LoginUser.DigiFolderThumbnailPath + "\\" + Convert.ToDateTime(lstProcessedVideo.CreatedOn).ToString("yyyyMMdd") + "\\" + System.IO.Path.GetFileNameWithoutExtension(outputFile) + ".jpg";
        //            System.GC.Collect();
        //            System.GC.WaitForPendingFinalizers();
        //            File.Delete(videoThumbnil);
        //            ResizeWPFImage(processVideoTemp + "Output.jpg", 210, videoThumbnil);
        //            processedVideoId = ProcessedVideoID;
        //        }
        //        VideoEffects ob = new VideoEffects();

        //        string effects = ob.GetVideoEffectsXML(objProcVideoInfo, PanPropertyLst, TransitionPropertyLst, BorderControl.borderslotlist, LogoControlBox.LogoSlotList);
        //        SaveProcessedVideoDetails(effects, firstRFID, outputFile, processedVideoId);
        //        IsProcessedVideoSaved = true;
        //    }

        //    catch (Exception ex)
        //    {
        //        string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
        //        ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
        //    }
        //}
        //private void bwVideoEditing_RunWorkerCompleted(object Sender, System.ComponentModel.RunWorkerCompletedEventArgs e)
        //{
        //    try
        //    {
        //        bs.Hide();
        //        if (!AutomatedVideoEditWorkFlow)
        //            MsgBox.ShowHandlerDialog("\nProcessed video has been saved successfully!", DigiMessageBox.DialogType.OK);
        //        btnSave.IsEnabled = false;
        //        lstVideoElements.Clear();
        //        CmbProductType.Visibility = Visibility.Collapsed;
        //        RobotImageLoader.IsZeroSearchNeeded = true;
        //        RobotImageLoader.currentCount = 0;
        //        //////creating Instance of Search result window//////
        //        SearchResult _objSearchResult = new SearchResult();
        //        //if (RobotImageLoader.GroupImages.Count == 0 && IsGoupped == "View All")
        //        //{
        //        //    IsGoupped = "View Group";
        //        //}

        //        ////////Calling logic of searching 
        //        _objSearchResult.LoadWindow();
        //        ////////Closing current window
        //        if (isGroup)
        //        {
        //            _objSearchResult.ViewGroup();
        //        }
        //        _objSearchResult.Show();
        //        this.Close();
        //    }
        //    catch (Exception ex)
        //    {
        //        string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
        //        ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
        //    }
        //}
        #endregion Old Save
        /// <summary>
        /// Resizes the WPF image.
        /// </summary>
        /// <param name="sourceImage">The source image.</param>
        /// <param name="maxHeight">The maximum height.</param>
        /// <param name="saveToPath">The save automatic path.</param>
        private void ResizeWPFImage(string sourceImage, int maxHeight, string saveToPath)
        {
            try
            {
                BitmapImage bi = new BitmapImage();
                BitmapImage bitmapImage = new BitmapImage();
                using (FileStream fileStream = File.OpenRead(sourceImage.ToString()))
                {
                    MemoryStream ms = new MemoryStream();
                    fileStream.CopyTo(ms);
                    ms.Seek(0, SeekOrigin.Begin);
                    fileStream.Close();
                    bi.BeginInit();
                    bi.StreamSource = ms;
                    bi.EndInit();
                    bi.Freeze();
                    decimal ratio = Convert.ToDecimal(bi.Width) / Convert.ToDecimal(bi.Height);
                    int newWidth = Convert.ToInt32(maxHeight * ratio);
                    int newHeight = maxHeight;
                    ms.Seek(0, SeekOrigin.Begin);
                    bitmapImage.BeginInit();
                    bitmapImage.StreamSource = ms;
                    bitmapImage.DecodePixelWidth = newWidth;
                    bitmapImage.DecodePixelHeight = newHeight;
                    bitmapImage.EndInit();
                    bitmapImage.Freeze();
                }
                using (var fileStreamForSave = new FileStream(saveToPath, FileMode.Create, FileAccess.ReadWrite, FileShare.ReadWrite))
                {
                    JpegBitmapEncoder encoder = new JpegBitmapEncoder();
                    encoder.QualityLevel = 94;
                    encoder.Frames.Add(BitmapFrame.Create(bitmapImage));
                    encoder.Save(fileStreamForSave);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
            finally
            {
                if (DateTime.Now.Subtract(lastmemoryUpdateTime).Seconds > 30)
                {
                    MemoryManagement.FlushMemory();
                    lastmemoryUpdateTime = DateTime.Now;
                }
            }
        }

        private void CopyVideoToDisplayFolder(string videofile)
        {
            if (ConfigManager.IMIXConfigurations.ContainsKey((int)ConfigParams.AutoVidDisplayEnabled) && Convert.ToBoolean(ConfigManager.IMIXConfigurations[(int)ConfigParams.AutoVidDisplayEnabled]) == true)
            {
                string dispPath = ConfigManager.IMIXConfigurations.ContainsKey((int)ConfigParams.AutoVidDisplayFolder) ? ConfigManager.IMIXConfigurations[(int)ConfigParams.AutoVidDisplayFolder] : null;
                int NoOfScreen = ConfigManager.IMIXConfigurations.ContainsKey((int)ConfigParams.NoOfDisplayScreens) ? Convert.ToInt32(ConfigManager.IMIXConfigurations[(int)ConfigParams.NoOfDisplayScreens]) : 0;
                folderCount++;
                bool IsCyclic = ConfigManager.IMIXConfigurations.ContainsKey((int)ConfigParams.IsCyclicDisplay) ? Convert.ToBoolean(ConfigManager.IMIXConfigurations[(int)ConfigParams.IsCyclicDisplay]) : false;
                if (IsCyclic)
                {
                    if (folderCount <= NoOfScreen)
                    {
                        string Screenfolder = dispPath + "\\Display\\Display" + folderCount;
                        if (!string.IsNullOrEmpty(Screenfolder))
                        {
                            if (!Directory.Exists(Screenfolder))
                            {
                                Directory.CreateDirectory(Screenfolder);
                            }
                            string pathToCopy = Screenfolder + "\\" + System.IO.Path.GetFileName(videofile);
                            File.Copy(videofile, pathToCopy);
                        }
                        if (folderCount == NoOfScreen)
                        {
                            folderCount = 0;
                        }
                    }
                }
                else
                {
                    for (int i = 1; i <= NoOfScreen; i++)
                    {
                        string Screenfolder = dispPath + "\\Display\\Display" + i.ToString();
                        if (!Directory.Exists(Screenfolder))
                        {
                            Directory.CreateDirectory(Screenfolder);
                        }
                        File.Copy(videofile, Screenfolder + "\\" + System.IO.Path.GetFileName(videofile));
                    }
                }
            }
        }

        private void btnExit_Click(object sender, RoutedEventArgs e)
        {
            try
            {

                bool isContinue = false;
                if (!IsProcessedVideoSaved)
                {
                    isContinue = (MessageBox.Show("Would you like to continue exit? Click “Yes” to exit or click “No” to save processed video.", "Digiphoto", MessageBoxButton.YesNo, MessageBoxImage.Question, MessageBoxResult.No) == MessageBoxResult.Yes);
                    //  isContinue = MsgBox.ShowHandlerDialog("Would you like to continue exit? Click “Yes” to exit or click “No” to save processed video.", DigiMessageBox.DialogType.Confirm);
                }
                if ((!IsProcessedVideoSaved && isContinue) || IsProcessedVideoSaved)
                {
                    MediaStop();
                    ClearVideoEditPlayer();
                    ShowVOS();
                    //lstVideoElements.Clear();
                    //CmbProductType.Visibility = Visibility.Collapsed;
                    ////////creating Instance of Search result window//////

                    //RobotImageLoader.RFID = "0";
                    //RobotImageLoader.SearchCriteria = "PhotoId";
                    //SearchResult window = null;

                    //foreach (Window wnd in Application.Current.Windows)
                    //{
                    //    if (wnd.Title == "View/Order Station")
                    //    {
                    //        window = (SearchResult)wnd;
                    //        break;
                    //    }
                    //}
                    ////  ClearImageSourceControl();
                    //if (window == null)
                    //    window = new SearchResult();

                    //if (RobotImageLoader.GroupImages.Count == 0 && IsGoupped == "View All")
                    //    IsGoupped = "View Group";


                    ////window.Savebackpid = Convert.ToString(photoId);
                    //window.Savebackpid = Convert.ToString(RobotImageLoader.PrintImages[RobotImageLoader.PrintImages.Count - 1].PhotoId);
                    //window.Show();
                    //window.LoadWindow();
                    //this.Close();
                }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }

        #endregion Lower strip functions end events

        #region MediaPlayer

        void MediaStop()
        {
            if (mplayer != null)
            {
                mplayer.MediaStop();
                mplayer = null;
            }
            gdMediaPlayer.Children.Remove(mplayer);
            gdMediaPlayer.Children.Clear();
            if (clientWin != null)
                clientWin.StopMediaPlay();
        }
        public MLMediaPlayer mplayer;
        ClientView clientWin = null;

        void MediaPlay()
        {
            stkbtnClosePreview.Visibility = Visibility.Visible;
            VideoEditPlayerPanel.Visibility = Visibility.Collapsed;
            vidoriginal.Visibility = Visibility.Visible;
            MediaStop();
            if (mplayer != null)
            {
                mplayer.Dispose();
            }

            //gdMediaPlayer.Dispatcher.BeginInvoke(new Action(
            //    () =>
            //    {
            mplayer = new MLMediaPlayer(vsMediaFileName, "VideoEditor", btnchkpreview.IsChecked == true ? true : false);
            gdMediaPlayer.BeginInit();
            gdMediaPlayer.Children.Add(mplayer);
            gdMediaPlayer.EndInit();
            //}));

            if (btnchkpreview.IsChecked == true)
            {
                if (clientWin == null)
                {
                    foreach (Window wnd in Application.Current.Windows)
                    {
                        if (wnd.Title == "ClientView")
                        {
                            clientWin = (ClientView)wnd;
                            break;
                        }
                    }
                }
                // clientWin.PlayVideoOnClient("VideoEditor",);
                //MediaStop();
                //if (isReplay)
                //{       
                //    mpc = new MediaPlayerControl(vsMediaFileName, "VideoEditor", replayFilePath, true, false);
                //}
                //else
                //{
                //    mpc = new MediaPlayerControl(vsMediaFileName, "VideoEditor");
                //}
                //gdMediaPlayer.Children.Add(mpc);

            }
        }
        #endregion

        #region Processed Video Details
        //Parse Effect Xml
        //private void parseEffectXml(string effects)
        //{
        //    try
        //    {
        //        XmlDocument xmlDoc = new XmlDocument();
        //        xmlDoc.LoadXml(effects);
        //        #region Video Effect
        //        //Lightness
        //        GeneralEffectsBox.tbLightness.Value = Convert.ToDouble(xmlDoc.GetElementsByTagName("lightness")[0].InnerText);
        //        //Saturation
        //        GeneralEffectsBox.tbSaturation.Value = Convert.ToDouble(xmlDoc.GetElementsByTagName("saturation")[0].InnerText);
        //        //Contrast
        //        GeneralEffectsBox.tbContrast.Value = Convert.ToDouble(xmlDoc.GetElementsByTagName("contrast")[0].InnerText);
        //        //Darkness
        //        GeneralEffectsBox.tbDarkness.Value = Convert.ToDouble(xmlDoc.GetElementsByTagName("darkness")[0].InnerText);
        //        //Gray Scale
        //        GeneralEffectsBox.chkGrayScale.IsChecked = Convert.ToBoolean(xmlDoc.GetElementsByTagName("greyScale")[0].InnerText);
        //        //Invert
        //        GeneralEffectsBox.chkInvert.IsChecked = Convert.ToBoolean(xmlDoc.GetElementsByTagName("invert")[0].InnerText);
        //        //Graphic Logo
        //        //if (!string.IsNullOrEmpty(xmlDoc.GetElementsByTagName("graphicLogo")[0].Attributes["graphicLogoPosition"].InnerText))
        //        //{
        //        //    string logoPosition = xmlDoc.GetElementsByTagName("graphicLogo")[0].Attributes["graphicLogoPosition"].InnerText;
        //        //    LogoControlBox.txtTextLogo.Text = xmlDoc.GetElementsByTagName("textLogo")[0].InnerText;
        //        //    LogoControlBox.chkGraphicLogo.IsChecked = true;
        //        //    //chkAddLogo.IsChecked = true;
        //        //    //LogoControlBox.stackLogo.Visibility = Visibility.Visible;
        //        //    if (logoPosition.Contains(','))
        //        //    {
        //        //        LogoControlBox.txtGraphicLogoTop.Text = logoPosition.Split(',')[0];
        //        //        LogoControlBox.txtGraphicLogoLeft.Text = logoPosition.Split(',')[1];

        //        //    }
        //        //}
        //        //else
        //        //{
        //        //    LogoControlBox.chkGraphicLogo.IsChecked = false;
        //        //}
        //        //Text Logo
        //        if (!string.IsNullOrEmpty(xmlDoc.GetElementsByTagName("textLogo")[0].Attributes["textLogoPosition"].InnerText))
        //        {
        //            string logoPosition = xmlDoc.GetElementsByTagName("textLogo")[0].Attributes["textLogoPosition"].InnerText;
        //            LogoControlBox.txtTextLogo.Text = xmlDoc.GetElementsByTagName("textLogo")[0].InnerText;
        //            LogoControlBox.chkTextLogo.IsChecked = true;
        //            //chkAddLogo.IsChecked = true;
        //            //LogoControlBox.stackLogo.Visibility = Visibility.Visible;
        //            if (logoPosition.Contains(','))
        //            {
        //                LogoControlBox.txtTextLogoTop.Text = logoPosition.Split(',')[0];
        //                LogoControlBox.txtTextLogoLeft.Text = logoPosition.Split(',')[1];
        //            }
        //            //textfontName
        //            if (!string.IsNullOrEmpty(xmlDoc.GetElementsByTagName("textLogo")[0].Attributes["textfontName"].InnerText))
        //            {
        //                string fontName = xmlDoc.GetElementsByTagName("textLogo")[0].Attributes["textfontName"].InnerText;
        //                float fontSize = 20;
        //                string fontStyle = "";
        //                string fontColor = "";
        //                //textfontSize
        //                if (!string.IsNullOrEmpty(xmlDoc.GetElementsByTagName("textLogo")[0].Attributes["textfontSize"].InnerText))
        //                {
        //                    fontSize = float.Parse(xmlDoc.GetElementsByTagName("textLogo")[0].Attributes["textfontSize"].InnerText);
        //                }
        //                //textfontStyle
        //                if (!string.IsNullOrEmpty(xmlDoc.GetElementsByTagName("textLogo")[0].Attributes["textfontStyle"].InnerText))
        //                {
        //                    fontStyle = xmlDoc.GetElementsByTagName("textLogo")[0].Attributes["textfontStyle"].InnerText;
        //                }
        //                if (!string.IsNullOrEmpty(xmlDoc.GetElementsByTagName("textLogo")[0].Attributes["textfontColor"].InnerText))
        //                {
        //                    fontColor = xmlDoc.GetElementsByTagName("textLogo")[0].Attributes["textfontColor"].InnerText;
        //                    //LogoControlBox.fontDialog.Color.Name = fontColor;
        //                }
        //                if (fontStyle.Contains(','))
        //                {
        //                    fontStyle = fontStyle.Split(',')[0];
        //                }

        //                switch (fontStyle)
        //                {
        //                    case "Bold":
        //                        LogoControlBox.fontDialog.Font = new System.Drawing.Font(fontName, fontSize, System.Drawing.FontStyle.Bold);
        //                        break;
        //                    case "Italic":
        //                        LogoControlBox.fontDialog.Font = new System.Drawing.Font(fontName, fontSize, System.Drawing.FontStyle.Italic);
        //                        break;
        //                    case "Underline":
        //                        LogoControlBox.fontDialog.Font = new System.Drawing.Font(fontName, fontSize, System.Drawing.FontStyle.Underline);
        //                        break;
        //                    default:
        //                        LogoControlBox.fontDialog.Font = new System.Drawing.Font(fontName, fontSize, System.Drawing.FontStyle.Regular);
        //                        break;
        //                }

        //            }
        //            //textfontColor
        //            if (!string.IsNullOrEmpty(xmlDoc.GetElementsByTagName("textLogo")[0].Attributes["textfontColor"].InnerText))
        //            {
        //                string fontColor = xmlDoc.GetElementsByTagName("textLogo")[0].Attributes["textfontColor"].InnerText;
        //            }
        //        }
        //        else
        //        {
        //            LogoControlBox.chkTextLogo.IsChecked = false;
        //        }
        //        //Pan Effect
        //        XmlNodeList panList = xmlDoc.GetElementsByTagName("Pans");
        //        if (panList.Count > 0)
        //        {
        //            XmlNodeList xmlnode = xmlDoc.GetElementsByTagName("Pan");
        //            for (int i = 0; i <= xmlnode.Count - 1; i++)
        //            {
        //                PanProperty pp = new PanProperty();
        //                pp.PanID = Convert.ToInt32(xmlnode[i].ChildNodes.Item(0).InnerText);
        //                pp.PanStartTime = Convert.ToInt32(xmlnode[i].ChildNodes.Item(1).InnerText);
        //                pp.PanStopTime = Convert.ToInt32(xmlnode[i].ChildNodes.Item(2).InnerText);
        //                pp.PanSourceLeft = Convert.ToInt32(xmlnode[i].ChildNodes.Item(3).InnerText);
        //                pp.PanSourceTop = Convert.ToInt32(xmlnode[i].ChildNodes.Item(4).InnerText);
        //                pp.PanSourceWidth = Convert.ToInt32(xmlnode[i].ChildNodes.Item(5).InnerText);
        //                pp.PanSourceHeight = Convert.ToInt32(xmlnode[i].ChildNodes.Item(6).InnerText);
        //                pp.PanDestLeft = Convert.ToInt32(xmlnode[i].ChildNodes.Item(7).InnerText);
        //                pp.PanDestTop = Convert.ToInt32(xmlnode[i].ChildNodes.Item(8).InnerText);
        //                pp.PanDestWidth = Convert.ToInt32(xmlnode[i].ChildNodes.Item(9).InnerText);
        //                pp.PanDestHeight = Convert.ToInt32(xmlnode[i].ChildNodes.Item(10).InnerText);
        //                PanPropertyLst.Add(pp);
        //            }
        //            PanBox.PanControlListAutoFill(PanPropertyLst);
        //        }
        //        else
        //        {
        //            //set blank
        //        }
        //        //Transition Effect
        //        XmlNodeList TransitionList = xmlDoc.GetElementsByTagName("Transitions");
        //        if (TransitionList.Count > 0)
        //        {
        //            XmlNodeList xmlnode = xmlDoc.GetElementsByTagName("Transition");
        //            for (int i = 0; i <= xmlnode.Count - 1; i++)
        //            {
        //                TransitionProperty tp = new TransitionProperty();
        //                tp.ID = Convert.ToInt32(xmlnode[i].ChildNodes.Item(0).InnerText);
        //                tp.TransitionID = Convert.ToInt32(xmlnode[i].ChildNodes.Item(1).InnerText);
        //                tp.TransitionStartTime = Convert.ToInt32(xmlnode[i].ChildNodes.Item(2).InnerText);
        //                tp.TransitionStopTime = Convert.ToInt32(xmlnode[i].ChildNodes.Item(3).InnerText);
        //                tp.TransitionName = xmlnode[i].ChildNodes.Item(4).InnerText.ToString();
        //                TransitionPropertyLst.Add(tp);
        //            }
        //            TransitionBox.TransitionControlListAutoFill(TransitionPropertyLst);
        //        }
        //        else
        //        {
        //            //set blank
        //        }

        //        //Add borders
        //        XmlNodeList BordersList = xmlDoc.GetElementsByTagName("Borders");
        //        if (TransitionList.Count > 0)
        //        {
        //            int id = 151;
        //            XmlNodeList xmlnode = xmlDoc.GetElementsByTagName("Border");
        //            for (int i = 0; i <= xmlnode.Count - 1; i++)
        //            {
        //                SlotProperty borderlist = new SlotProperty();
        //                borderlist.ID = id++;
        //                borderlist.ItemID = Convert.ToInt32(xmlnode[i].ChildNodes.Item(0).InnerText);
        //                borderlist.FilePath = LoginUser.DigiFolderFramePath + "\\Thumbnails\\" + xmlnode[i].ChildNodes.Item(1).InnerText;
        //                borderlist.StartTime = Convert.ToInt32(xmlnode[i].ChildNodes.Item(2).InnerText);
        //                borderlist.StopTime = Convert.ToInt32(xmlnode[i].ChildNodes.Item(3).InnerText);
        //                BorderControl.borderslotlist.Add(borderlist);
        //            }
        //            BorderControl.ControlListAutoFill();
        //        }
        //        else
        //        {
        //            //set blank
        //        }
        //        //Add Graphic
        //        XmlNodeList graphicList = xmlDoc.GetElementsByTagName("graphics");
        //        if (TransitionList.Count > 0)
        //        {
        //            int uniqueID = 51;
        //            XmlNodeList xmlnode = xmlDoc.GetElementsByTagName("graphic");
        //            for (int i = 0; i <= xmlnode.Count - 1; i++)
        //            {
        //                SlotProperty logoprop = new SlotProperty();
        //                logoprop.ID = uniqueID++;
        //                logoprop.ItemID = Convert.ToInt32(xmlnode[i].ChildNodes.Item(0).InnerText);
        //                logoprop.FilePath = LoginUser.DigiFolderGraphicsPath + xmlnode[i].ChildNodes.Item(1).InnerText;
        //                logoprop.StartTime = Convert.ToInt32(xmlnode[i].ChildNodes.Item(2).InnerText);
        //                logoprop.StopTime = Convert.ToInt32(xmlnode[i].ChildNodes.Item(3).InnerText);
        //                logoprop.Top = Convert.ToInt32(xmlnode[i].ChildNodes.Item(4).InnerText);
        //                logoprop.Left = Convert.ToInt32(xmlnode[i].ChildNodes.Item(5).InnerText);
        //                LogoControlBox.LogoSlotList.Add(logoprop);

        //                TemplateListItems itemTemplateList = objTemplateList.Where(o => o.Item_ID == logoprop.ItemID && o.MediaType == 606).FirstOrDefault();
        //                itemTemplateList.TopPositon = logoprop.Top;
        //                itemTemplateList.LeftPositon = logoprop.Left;
        //                itemTemplateList.IsChecked = true;
        //                //  if (logoprop.FilePath.EndsWith(".gif"))
        //                //  DragGraphics(logoprop.FilePath, true, logoprop.ItemID);
        //            }
        //            //if (LogoControlBox.LogoSlotList.Count>0)
        //            //loadGraphic(LogoControlBox.LogoSlotList);
        //            LogoControlBox.ControlListAutoFill();
        //        }
        //        else
        //        {
        //            //set blank
        //        }
        //        #endregion
        //        #region Audio Effect
        //        if (xmlDoc.GetElementsByTagName("audio")[0].InnerText == "True")
        //        {
        //            //Amplifier
        //            sldAmplify.Value = Convert.ToDouble(xmlDoc.GetElementsByTagName("audio")[0].Attributes["amplify"].InnerText);
        //            //Equalizer
        //            sldEqual1.Value = Convert.ToDouble(xmlDoc.GetElementsByTagName("audio")[0].Attributes["equal1"].InnerText);
        //            sldEqual2.Value = Convert.ToDouble(xmlDoc.GetElementsByTagName("audio")[0].Attributes["equal2"].InnerText);
        //            sldEqual3.Value = Convert.ToDouble(xmlDoc.GetElementsByTagName("audio")[0].Attributes["equal3"].InnerText);
        //            sldEqual4.Value = Convert.ToDouble(xmlDoc.GetElementsByTagName("audio")[0].Attributes["equal4"].InnerText);
        //            sldEqual5.Value = Convert.ToDouble(xmlDoc.GetElementsByTagName("audio")[0].Attributes["equal5"].InnerText);
        //            sldEqual6.Value = Convert.ToDouble(xmlDoc.GetElementsByTagName("audio")[0].Attributes["equal6"].InnerText);
        //        }

        //        #endregion
        //        #region ChromaEffect
        //        if (xmlDoc.GetElementsByTagName("chroma")[0].InnerText == "True")
        //        {
        //            string chromaColor = xmlDoc.GetElementsByTagName("chroma")[0].Attributes["chromaKeyColor"].InnerText;
        //            string chromaKeyBG = xmlDoc.GetElementsByTagName("chroma")[0].Attributes["chromaKeyBG"].InnerText;
        //            switch (chromaColor)
        //            {
        //                case "Red":
        //                    rdbRed.IsChecked = true;
        //                    break;
        //                case "Green":
        //                    rdbGreen.IsChecked = true;
        //                    break;
        //                case "Blue":
        //                    rdbBlue.IsChecked = true;
        //                    break;
        //            }
        //        }
        //        #endregion
        //        #region AddBorder
        //        #endregion
        //    }
        //    catch (Exception ex)
        //    {
        //        string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
        //        ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
        //    }
        //}
        //Bind Drop List
        FilePhotoInfo processImageItem = null;
        bool IsVideoInput = false;
        //private void bindDropList()
        //{
        //    int a = 1;
        //    bool imageAsGraphics = false;
        //    VideoPage objCurrentPage;
        //    PrintOrderPageList.Clear();
        //    ConfigBusiness configBusiness = new ConfigBusiness();
        //    foreach (ProcessedVideoDetailsInfo pvd in lstProcessedVideo.processedVideoItemsDetails.ToList())
        //    {
        //        var processedVideoItem = lstProcessedVideo.processedVideoItemsDetails.Where(x => x.MediaType == 603).FirstOrDefault();
        //        int countImage = lstProcessedVideo.processedVideoItemsDetails.Where(x => x.MediaType == 601).ToList().Count();
        //        int CountVB = lstProcessedVideo.processedVideoItemsDetails.Where(x => x.MediaType == 605).ToList().Count();
        //        if (processedVideoItem != null)
        //        {
        //            List<VideoTemplateInfo.VideoSlot> slotListTemp = configBusiness.GetVideoTemplate(processedVideoItem.MediaId).videoSlots;
        //            if (slotListTemp.Count() == 0)
        //            {
        //                imageAsGraphics = true;
        //            }
        //        }
        //        else if (countImage == 1 && CountVB == 1)
        //        {
        //            imageAsGraphics = true;
        //        }
        //        if (pvd.MediaType == 601 || pvd.MediaType == 602 || pvd.MediaType == 607)
        //        {
        //            objCurrentPage = new VideoPage();
        //            objCurrentPage.PhotoId = Convert.ToInt32(pvd.MediaId);
        //            objCurrentPage.ImageDisplayTime = pvd.DisplayTime;
        //            objCurrentPage.MediaType = pvd.MediaType;
        //            objCurrentPage.IsGuestImage = true;
        //            ProcessedVideoBusiness objPV = new ProcessedVideoBusiness();
        //            FilePhotoInfo fInfo = objPV.GetFileInfo(Convert.ToInt32(pvd.MediaId));
        //            objCurrentPage.Name = fInfo.Photo_RFID;
        //            if (pvd.MediaType == 602)
        //            {
        //                IsVideoInput = true;
        //                VideoProcessingClass vpc = new VideoProcessingClass();
        //                objCurrentPage.FilePath = fInfo.HotFolderPath + "Thumbnails\\" + fInfo.FileName.Replace(vpc.SupportedVideoFormats["avi"].ToString(), ".jpg").Replace(vpc.SupportedVideoFormats["mp4"].ToString(), ".jpg").Replace(vpc.SupportedVideoFormats["wmv"].ToString(), ".jpg").Replace(vpc.SupportedVideoFormats["mov"].ToString(), ".jpg").Replace(vpc.SupportedVideoFormats["3gp"].ToString(), ".jpg").Replace(vpc.SupportedVideoFormats["3g2"].ToString(), ".jpg").Replace(vpc.SupportedVideoFormats["m2v"].ToString(), ".jpg").Replace(vpc.SupportedVideoFormats["m4v"].ToString(), ".jpg").Replace(vpc.SupportedVideoFormats["flv"].ToString(), ".jpg").Replace(vpc.SupportedVideoFormats["mpeg"].ToString(), ".jpg").Replace(vpc.SupportedVideoFormats["ffmpeg"].ToString(), ".jpg");
        //                // Guest video
        //                objCurrentPage.DropVideoPath = fInfo.HotFolderPath + "Videos\\" + fInfo.CreatedOn.ToString("yyyyMMdd") + "\\" + fInfo.FileName;

        //            }
        //            else if (pvd.MediaType == 607)
        //            {
        //                IsVideoInput = true;
        //                VideoProcessingClass vpc = new VideoProcessingClass();
        //                objCurrentPage.FilePath = fInfo.HotFolderPath + "Thumbnails\\" + fInfo.FileName.Replace(vpc.SupportedVideoFormats["avi"].ToString(), ".jpg").Replace(vpc.SupportedVideoFormats["mp4"].ToString(), ".jpg").Replace(vpc.SupportedVideoFormats["wmv"].ToString(), ".jpg").Replace(vpc.SupportedVideoFormats["mov"].ToString(), ".jpg").Replace(vpc.SupportedVideoFormats["3gp"].ToString(), ".jpg").Replace(vpc.SupportedVideoFormats["3g2"].ToString(), ".jpg").Replace(vpc.SupportedVideoFormats["m2v"].ToString(), ".jpg").Replace(vpc.SupportedVideoFormats["m4v"].ToString(), ".jpg").Replace(vpc.SupportedVideoFormats["flv"].ToString(), ".jpg").Replace(vpc.SupportedVideoFormats["mpeg"].ToString(), ".jpg").Replace(vpc.SupportedVideoFormats["ffmpeg"].ToString(), ".jpg");
        //                objCurrentPage.DropVideoPath = fInfo.HotFolderPath + "ProcessedVideos\\" + fInfo.CreatedOn.ToString("yyyyMMdd") + "\\" + fInfo.FileName; ;

        //            }
        //            else if (pvd.MediaType == 601)
        //            {
        //                IsVideoInput = true;
        //                if (File.Exists(System.IO.Path.Combine(fInfo.HotFolderPath, fInfo.CreatedOn.ToString("yyyyMMdd"), fInfo.FileName.Replace(".jpg", ".png"))))
        //                {
        //                    objCurrentPage.DropVideoPath = System.IO.Path.Combine(fInfo.HotFolderPath, fInfo.CreatedOn.ToString("yyyyMMdd"), fInfo.FileName);
        //                }
        //                else if (File.Exists(System.IO.Path.Combine(fInfo.HotFolderPath, "EditedImages", fInfo.FileName)))
        //                {
        //                    objCurrentPage.DropVideoPath = System.IO.Path.Combine(fInfo.HotFolderPath, "EditedImages", fInfo.FileName);
        //                }
        //                //else if (File.Exists(System.IO.Path.Combine(fInfo.HotFolderPath, fInfo.FileName)))
        //                //{
        //                //    objCurrentPage.DropVideoPath = System.IO.Path.Combine(fInfo.HotFolderPath, fInfo.FileName);
        //                //}
        //                else
        //                {
        //                    objCurrentPage.DropVideoPath = System.IO.Path.Combine(fInfo.HotFolderPath, fInfo.CreatedOn.ToString("yyyyMMdd"), fInfo.FileName);
        //                }
        //                objCurrentPage.FilePath = objCurrentPage.DropVideoPath;

        //                if (imageAsGraphics)
        //                {
        //                    CreateGraphicsFromImage(objCurrentPage.FilePath, fInfo.CreatedOn, objCurrentPage.PhotoId);
        //                    //stkZommControl.Visibility = Visibility.Collapsed;
        //                    //StackGuestImageAsGraphic.Visibility = Visibility.Visible;
        //                    StackVideoTemplatePlay.Visibility = Visibility.Visible;
        //                    txtSelVideoTemp.Text = lstProcessedVideo.processedVideoItemsDetails.Where(x => x.MediaType == 603).FirstOrDefault().MediaId.ToString();
        //                    processImageItem = fInfo;
        //                }
        //            }
        //            if (!imageAsGraphics || pvd.MediaType != 601)
        //            {
        //                objCurrentPage.PageNo = a;
        //                PrintOrderPageList.Add(objCurrentPage);
        //                a++;
        //            }


        //        }
        //    }


        //    if (PrintOrderPageList.Count < defaultProcessListCount)
        //    {
        //        for (int count = PrintOrderPageList.Count; count < defaultProcessListCount; count++)
        //        {
        //            objCurrentPage = new VideoPage();
        //            objCurrentPage.PageNo = count;
        //            PrintOrderPageList.Add(objCurrentPage);
        //        }
        //    }
        //    lstDragImages.ItemsSource = PrintOrderPageList.OrderBy(o => o.PageNo);

        //}
        //Update Template List
        //private void updateTemplateList()
        //{
        //    for (int i = 0; i < lstProcessedVideo.processedVideoItemsDetails.ToList().Count; i++)
        //    {
        //        for (int j = 0; j < objTemplateList.Count; j++)
        //        {
        //            if (objTemplateList[j].MediaType == lstProcessedVideo.processedVideoItemsDetails.ToList()[i].MediaType && objTemplateList[j].Item_ID == lstProcessedVideo.processedVideoItemsDetails.ToList()[i].MediaId)
        //            {
        //                objTemplateList[j].IsChecked = true;
        //                objTemplateList[j].StartTime = lstProcessedVideo.processedVideoItemsDetails.ToList()[i].StartTime;
        //                objTemplateList[j].EndTime = lstProcessedVideo.processedVideoItemsDetails.ToList()[i].EndTime;
        //                objTemplateList[j].InsertTime = lstProcessedVideo.processedVideoItemsDetails.ToList()[i].FrameTime;
        //                if (objTemplateList[j].MediaType == 603)
        //                {
        //                    isVideoTempSelected = true;
        //                    defaultProcessListCount = slotList.Count;
        //                    vsMediaFileName = LoginUser.DigiFolderVideoTemplatePath + System.IO.Path.GetFileName(objTemplateList[j].Name);
        //                    MediaPlay();
        //                }
        //                else if (objTemplateList[j].MediaType == 606)
        //                {
        //                    string imagepath = objTemplateList[j].FilePath;
        //                    dragCanvas.Visibility = Visibility.Visible;
        //                    SlotProperty lsp = LogoControlBox.LogoSlotList.Where(o => o.ItemID == objTemplateList[j].Item_ID).FirstOrDefault();

        //                    objTemplateList[j].TopPositon = lsp.Top;
        //                    objTemplateList[j].LeftPositon = lsp.Left;

        //                    DragGraphics(imagepath, true, objTemplateList[j].Item_ID);
        //                }
        //            }
        //        }
        //    }
        //}
        // Get Processed Video Details
        //private void GetProcessedDetails(int VideoId)
        //{
        //    try
        //    {
        //        //dragCanvas.Children.Clear();
        //        ProcessedVideoBusiness objPV = new ProcessedVideoBusiness();
        //        lstProcessedVideo = objPV.GetProcessedVideoDetails(VideoId);
        //        if (lstProcessedVideo == null)
        //            return;

        //        //parseEffectXml(lstProcessedVideo.Effects);
        //        //Update Template List
        //        updateTemplateList();
        //        //Bind Drop List
        //        bindDropList();
        //        string ProcessedVideoFilePath = lstProcessedVideo.HotFolderPath + Convert.ToDateTime(lstProcessedVideo.CreatedOn).ToString("yyyyMMdd") + "\\" + lstProcessedVideo.OutputVideoFileName;
        //        vsMediaFileName = ProcessedVideoFilePath;
        //    }
        //    catch (Exception ex)
        //    {
        //        string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
        //        ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
        //    }
        //}
        #endregion


        private void btnCapture_Click(object sender, RoutedEventArgs e)
        {
            HighlightSelectedButton("btnCapture");
            //chkAddLogo.IsChecked = false;
            //LogoControlBox.stackLogo.Visibility = Visibility.Collapsed;
            txtTemplateRotate.Visibility = Visibility.Visible;
            lstTemplates.IsEnabled = true;
            ShowHideControls("Video Capture");
            btnStopProcess.IsEnabled = false;
        }


        #region Other Event & functions
        private Dictionary<string, int> LoadValueTypeList()
        {
            listValueType = new Dictionary<string, int>();
            listValueType.Add("Image", 601);
            listValueType.Add("Video", 602);
            listValueType.Add("VideoTemplate", 603);
            listValueType.Add("AudioTemplate", 604);
            listValueType.Add("VideoBackground", 605);
            listValueType.Add("Graphics", 606);
            listValueType.Add("ProcessedVideo", 607);
            return listValueType;
        }
        // Template Item Check Event
        int graphicId = 51;

        private void ChkSelected_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                bool iscontinue = false;
                if (!IsProcessedVideoSaved)
                {
                    iscontinue = MsgBox.ShowHandlerDialog("Do you wish to save changes?", DigiMessageBox.DialogType.Confirm);
                }
                if ((!IsProcessedVideoSaved && iscontinue) || IsProcessedVideoSaved)
                {
                    IsProcessedVideoSaved = true;
                    btnSave.IsEnabled = false;
                    {
                        CheckBox chk = sender as CheckBox;
                        int id = Convert.ToInt32(chk.Uid);
                        if (templateType == "chroma")
                        {
                            for (int i = 0; i < objTemplateList.Count; i++)
                            {
                                if (objTemplateList[i].MediaType == 605 && objTemplateList[i].Item_ID == id && chk.IsChecked == true)
                                {
                                    objTemplateList[i].IsChecked = true;
                                    objTemplateList[i].CheckedBoxVisible = Visibility.Visible;
                                    vsMediaFileName = System.IO.Path.Combine(LoginUser.DigiFolderVideoBackGroundPath, System.IO.Path.GetFileName(objTemplateList[i].Name));
                                    MItem myBackground;
                                    m_objMixer.StreamsBackgroundSet(null, vsMediaFileName, "loop=false", out myBackground, 0.0);
                                    Thread.Sleep(1000);
                                    // ((MPLATFORMLib.IMProps)myBackground).PropsSet("object::audio_gain","-100"); //to mute the background video
                                }
                                else if (objTemplateList[i].MediaType == 605)
                                {
                                    objTemplateList[i].IsChecked = false;
                                }
                            }
                            if (objTemplateList.Where(o => o.MediaType == 605 && o.IsChecked == true).ToList().Count == 0)
                            {
                                MItem myBackground;
                                //if (myBackground!=null)
                                m_objMixer.StreamsBackgroundSet(null, null, "", out myBackground, 0.0);
                                Thread.Sleep(1000);
                            }
                            List<TemplateListItems> TLI = objTemplateList.Where(o => o.MediaType == 605).ToList();
                            lstTemplates.ItemsSource = TLI;
                        }
                        else if (templateType == "video")
                        {
                            for (int i = 0; i < objTemplateList.Count; i++)
                            {
                                if (objTemplateList[i].MediaType == 603 && objTemplateList[i].Item_ID == id && chk.IsChecked == true)
                                {
                                    objTemplateList[i].IsChecked = true;
                                    objTemplateList[i].CheckedBoxVisible = Visibility.Visible;
                                    ConfigBusiness configBusiness = new ConfigBusiness();
                                    slotList = configBusiness.GetVideoTemplate(id).videoSlots;
                                    if (slotList == null)
                                        slotList = new List<VideoTemplateInfo.VideoSlot>();

                                    string filepath = LoginUser.DigiFolderVideoTemplatePath + System.IO.Path.GetFileName(objTemplateList[i].Name);
                                    txtSelVideoTemp.Text = id.ToString();
                                    //  MediaStop();
                                }
                                else if (objTemplateList[i].MediaType == 603)
                                {
                                    MediaStop();
                                    objTemplateList[i].IsChecked = false;
                                    isVideoTempSelected = false;
                                }
                            }
                            if (objTemplateList.Where(o => o.MediaType == 603 && o.IsChecked == true).ToList().Count() > 0)
                            {
                                isVideoTempSelected = true;
                                defaultProcessListCount = slotList.Count;
                                PrintOrderPageList.Clear();
                                BindPageStrips();
                                StackImageDisplayTime.Visibility = Visibility.Collapsed;
                                StackVideoStartEndTime.Visibility = Visibility.Collapsed;
                                // StackGuestVideoTemplate.Visibility = Visibility.Collapsed;
                                StackVideoTemplatePlay.Visibility = Visibility.Visible;


                            }
                            else
                            {
                                isVideoTempSelected = false;
                                defaultProcessListCount = 10;
                                PrintOrderPageList.Clear();
                                BindPageStrips();
                                StackImageDisplayTime.Visibility = Visibility.Collapsed;
                                StackVideoStartEndTime.Visibility = Visibility.Collapsed;
                                StackVideoTemplatePlay.Visibility = Visibility.Collapsed;
                                // StackGuestVideoTemplate.Visibility = Visibility.Collapsed;
                                slotList.Clear();
                            }
                            List<TemplateListItems> TLI = objTemplateList.Where(o => o.MediaType == 603 && IsActive == true).ToList();
                            lstTemplates.ItemsSource = TLI;
                        }
                        else if (templateType == "audio")
                        {
                            int videoLen = CalculateProcessedVideoLength();
                            txtVideoLength.Text = "Expected video length : " + videoLen.ToString() + " sec";
                            TemplateListItems objtemp = objTemplateList.Where(o => o.MediaType == 604 && o.Item_ID == id).FirstOrDefault();
                            if (objtemp != null && chk.IsChecked == true)
                            {
                                objtemp.IsChecked = true;
                                objtemp.CheckedBoxVisible = Visibility.Visible;
                                StackAudioStartEndTime.Visibility = Visibility.Visible;
                                StackGraphicSettingPanel1.Visibility = Visibility.Collapsed;
                                StackGraphicSettingPanel2.Visibility = Visibility.Collapsed;
                                txtAudioStart.Text = objtemp.StartTime.ToString();
                                txtAudioEnd.Text = objtemp.EndTime.ToString();
                                txtAudioInsert.Text = objtemp.InsertTime.ToString();
                                txtAudioId.Text = id.ToString();
                                lstTemplates.SelectedItem = objtemp;
                                RangeSliderAudio.LowerValue = objtemp.StartTime;
                                RangeSliderAudio.UpperValue = objtemp.EndTime;
                                RangeSliderAudio.Maximum = objtemp.Length;
                            }
                            else if (objtemp.MediaType == 604)
                            {
                                objtemp.IsChecked = false;
                                StackAudioStartEndTime.Visibility = Visibility.Collapsed;
                                lstTemplates.SelectedItem = null;
                            }
                            // }
                            List<TemplateListItems> TLI = objTemplateList.Where(o => o.MediaType == 604).ToList();
                            lstTemplates.ItemsSource = TLI;
                        }
                        else if (templateType == "graphics")
                        {
                            TemplateListItems objtemp = objTemplateList.Where(o => o.MediaType == 606 && o.Item_ID == id).FirstOrDefault();
                            if (objtemp != null && chk.IsChecked == true)
                            {
                                objtemp.IsChecked = true;
                                objtemp.CheckedBoxVisible = Visibility.Visible;

                            }
                            else if (objtemp.MediaType == 606)
                            {
                                objtemp.IsChecked = false;
                            }

                            List<TemplateListItems> TLI = objTemplateList.Where(o => o.MediaType == 606).ToList();
                            lstTemplates.ItemsSource = TLI;
                        }
                        else if (templateType == "overlay")
                        {
                            for (int i = 0; i < objTemplateList.Count; i++)
                            {
                                if (objTemplateList[i].MediaType == 609 && objTemplateList[i].Item_ID == id && chk.IsChecked == true)
                                {
                                    StackOverlayChroma.Visibility = Visibility.Visible;
                                    objTemplateList[i].IsChecked = true;
                                    objTemplateList[i].CheckedBoxVisible = Visibility.Visible;
                                    MItem pFile;
                                    string file = LoginUser.DigiFolderVideoOverlayPath + System.IO.Path.GetFileName(objTemplateList[i].Name);
                                    m_pMixerStreams.StreamsAdd(strOverlayStream, null, file, "loop=false", out pFile, 0);
                                    int count; string streamName = string.Empty;
                                    if (string.IsNullOrEmpty(strOverlayStream))
                                    {
                                        m_pMixerStreams.StreamsGetCount(out count);
                                        m_pMixerStreams.StreamsGetByIndex(count - 1, out streamName, out pFile);
                                        strOverlayStream = streamName;
                                    }
                                    OverlayElement.ElementMultipleSet("stream_id=" + strOverlayStream + " audio_gain=-100 show=true h=1 w=1 x=0 y=0", 0.0);
                                    OverlayElement.ElementReorder(500);
                                    //  VideoEditPlayerPanel.IsEnabled = false;
                                }
                                else if (objTemplateList[i].MediaType == 609)
                                {
                                    objTemplateList[i].IsChecked = false;
                                }

                            }
                            if (objTemplateList.Where(o => o.MediaType == 609 && o.IsChecked == true).ToList().Count == 0)
                            {
                                OverlayElement.ElementMultipleSet("show=false", 0.0);
                                //  overlayChromaSetting = string.Empty;
                                StackOverlayChroma.Visibility = Visibility.Collapsed;
                                //  VideoEditPlayerPanel.IsEnabled = true;
                            }
                            List<TemplateListItems> TLI = objTemplateList.Where(o => o.MediaType == 609).ToList();
                            lstTemplates.ItemsSource = TLI;
                        }
                        else
                        {
                            //Border
                            for (int i = 0; i < objTemplateList.Count; i++)
                            {
                                if (objTemplateList[i].MediaType == 608 && objTemplateList[i].Item_ID == id && chk.IsChecked == true)
                                {
                                    objTemplateList[i].IsChecked = true;
                                    objTemplateList[i].CheckedBoxVisible = Visibility.Visible;
                                    MItem pFile;
                                    string file = LoginUser.DigiFolderFramePath + System.IO.Path.GetFileName(objTemplateList[i].Name);
                                    m_pMixerStreams.StreamsAdd(strBorderStream, null, file, "loop=false", out pFile, 0);
                                    int count; string streamName = string.Empty;
                                    if (string.IsNullOrEmpty(strBorderStream))
                                    {
                                        m_pMixerStreams.StreamsGetCount(out count);
                                        m_pMixerStreams.StreamsGetByIndex(count - 1, out streamName, out pFile);
                                        strBorderStream = streamName;
                                    }
                                    BorderElement.ElementMultipleSet("stream_id=" + strBorderStream + " audio_gain=-100 show=true h=1 w=1 x=0 y=0", 0.0);
                                    BorderElement.ElementReorder(1000);
                                    //  VideoEditPlayerPanel.IsEnabled = false;
                                }
                                else if (objTemplateList[i].MediaType == 608)
                                {
                                    objTemplateList[i].IsChecked = false;
                                }

                            }
                            if (objTemplateList.Where(o => o.MediaType == 608 && o.IsChecked == true).ToList().Count == 0)
                            {
                                BorderElement.ElementMultipleSet("show=false", 0.0);
                                //  VideoEditPlayerPanel.IsEnabled = true;
                            }
                            List<TemplateListItems> TLI = objTemplateList.Where(o => o.MediaType == 608).ToList();
                            lstTemplates.ItemsSource = TLI;

                        }
                    }
                }
                else
                {
                    CheckBox chk = sender as CheckBox;
                    if (chk.IsChecked == true)
                    {
                        chk.IsChecked = false;
                    }
                    else
                    {
                        chk.IsChecked = true;
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }

        string strBorderStream = string.Empty;
        string strOverlayStream = string.Empty;//overlay

        private void DisableControls()
        {
            //btnCapture.IsEnabled = false;
            btnVideo.IsEnabled = false;
            // btnAudio.IsEnabled = false;
            btnChroma.IsEnabled = false;
            btnSave.IsEnabled = false;
            btnIncludeAll.IsEnabled = false;
            btnProcess.IsEnabled = false;
            btnReset.IsEnabled = false;
            grdChromaControls.IsEnabled = false;
            //grdAudioControls.IsEnabled = false;
            grdVideoControls.IsEnabled = false;
            lstGuestImages.IsEnabled = false;
            lstDragImages.IsEnabled = false;
            lstTemplates.IsEnabled = false;
            //   CmbProductType.IsEnabled = false;
            btnExit.IsEnabled = false;
            //cbVideoInputDevice.IsEnabled = false;
            //cbAudioInputDevice.IsEnabled = false;
            btnStopProcess.IsEnabled = true;
            btnDefaultSettings.IsEnabled = false;
            //stkZommControl.IsEnabled = false;
        }
        //Enable Button
        private void EnableControls()
        {

            // btnCapture.IsEnabled = true;
            btnVideo.IsEnabled = true;
            //btnAudio.IsEnabled = true;
            btnChroma.IsEnabled = true;
            btnSave.IsEnabled = true;
            btnIncludeAll.IsEnabled = true;
            btnProcess.IsEnabled = true;
            btnReset.IsEnabled = true;
            grdChromaControls.IsEnabled = true;
            //grdAudioControls.IsEnabled = true;
            grdVideoControls.IsEnabled = true;
            lstGuestImages.IsEnabled = true;
            lstDragImages.IsEnabled = true;
            lstTemplates.IsEnabled = true;
            if (objTemplateList.Where(o => (o.MediaType == 602 || o.MediaType == 607) && o.isActive == false).ToList().Count > 0)
            {
                lstTemplates.IsEnabled = false;
            }
            else
            {
                lstTemplates.IsEnabled = true;
            }
            // CmbProductType.IsEnabled = true;
            btnExit.IsEnabled = true;
            //cbVideoInputDevice.IsEnabled = true;
            //cbAudioInputDevice.IsEnabled = true;
            btnDefaultSettings.IsEnabled = true;
            //stkZommControl.IsEnabled = true;
            btnStopProcess.IsEnabled = false;
            VideoEditPlayerPanel.IsEnabled = true;
        }
        private void btnSetVideoTime_Click(object sender, RoutedEventArgs e)
        {
            VideoPage lstMyItem = PrintOrderPageList.Where(o => o.PhotoId == this.DropPhotoId && o.PageNo == activePage).FirstOrDefault();
            if (lstMyItem != null)
            {
                if (((!string.IsNullOrEmpty(txtvideoStart.Text)) && !string.IsNullOrEmpty(txtVideoEnd.Text)) && !((txtvideoStart.Text.Contains(".") || txtVideoEnd.Text.Contains("."))))
                {

                    if (!(Convert.ToInt32(txtvideoStart.Text) >= lstMyItem.videoLength || Convert.ToInt32(txtVideoEnd.Text) > lstMyItem.videoLength))
                    {
                        if (Convert.ToInt32(txtvideoStart.Text) < Convert.ToInt32(txtVideoEnd.Text))
                        {
                            lstMyItem.videoStartTime = Convert.ToInt32(txtvideoStart.Text);
                            lstMyItem.videoEndTime = Convert.ToInt32(txtVideoEnd.Text);
                            StackVideoStartEndTime.Visibility = Visibility.Collapsed;
                        }
                        else
                        {
                            // MsgBox.ShowHandlerDialog("Video start time should be less than video end time!", DigiMessageBox.DialogType.OK);
                            MessageBox.Show("Video start time should be less than video end time!", "Digiphoto", MessageBoxButton.OK, MessageBoxImage.Exclamation, MessageBoxResult.OK);
                        }

                    }
                    else
                    {
                        // MsgBox.ShowHandlerDialog("Video Start/End time cannot be greater than video length.", DigiMessageBox.DialogType.OK);
                        MessageBox.Show("Video Start/End time cannot be greater than video length.", "Digiphoto", MessageBoxButton.OK, MessageBoxImage.Exclamation, MessageBoxResult.OK);
                    }
                }
                else
                {
                    // MsgBox.ShowHandlerDialog("Video Start/End time has some invalid data!.", DigiMessageBox.DialogType.OK);
                    MessageBox.Show("Video Start/End time has some invalid data!.", "Digiphoto", MessageBoxButton.OK, MessageBoxImage.Exclamation, MessageBoxResult.OK);
                }
            }
        }
        private bool validateAudio()
        {
            //int videoLen = CalculateProcessedVideoLength();
            //if (string.IsNullOrEmpty(txtAudioStart.Text) || string.IsNullOrEmpty(txtAudioEnd.Text) || string.IsNullOrEmpty(txtAudioInsert.Text))
            //{
            //    MsgBox.ShowHandlerDialog("Audio Start/End/Insert time can not be blank!", DigiMessageBox.DialogType.OK);
            //    return false;
            //}
            //if (txtAudioStart.Text.Contains(".") || txtAudioEnd.Text.Contains(".") || txtAudioInsert.Text.Contains("."))
            //{
            //    MsgBox.ShowHandlerDialog("Audio Start/End/Insert time has some invalid data!", DigiMessageBox.DialogType.OK);
            //    return false;
            //}
            //if (Convert.ToInt32(txtAudioInsert.Text) >= videoLen)
            //{
            //    MsgBox.ShowHandlerDialog("Audio Insert time should be less than expected video length.", DigiMessageBox.DialogType.OK);
            //    return false;
            //}
            return true;

        }

        private void btnchkpreview_Click(object sender, RoutedEventArgs e)
        {
            PreviewVideo();
            if (gdMediaPlayer.Visibility == Visibility.Visible)
                MediaPlay();
        }
        private void PreviewVideo()
        {
            if ((bool)btnchkpreview.IsChecked)
            {
                VisualBrush CB = new VisualBrush();
                //  if (grdVideoCaptureControls.Visibility != Visibility.Visible)
                {
                    CB = new VisualBrush(grdMain);
                }
                //else
                //{
                //    CB = new VisualBrush(VideoCapturePlayer);
                //}
                CompileEffectChanged(CB, -1);
            }
            else
            {
                CompileEffectChanged(null, -1);
            }
        }
        public void CompileEffectChanged(VisualBrush compiledBitmapImage, int ProductType)
        {
            try
            {
                ClientView window = null;


                foreach (Window wnd in Application.Current.Windows)
                {
                    if (wnd.Title == "ClientView")
                    {
                        window = (ClientView)wnd;
                    }
                }

                if (window == null)
                {

                    window = new ClientView();
                    window.WindowStartupLocation = System.Windows.WindowStartupLocation.Manual;

                }
                window.GroupView = false;
                window.DefaultView = false;
                if (compiledBitmapImage != null)
                {
                    window.testR.Fill = null;
                    compiledBitmapImage.Stretch = Stretch.Uniform;
                    window.imgDefaultBlur.Visibility = System.Windows.Visibility.Collapsed;
                    window.testR.Fill = compiledBitmapImage;
                }
                else
                {
                    window.DefaultView = true;

                    window.imgDefaultBlur.Visibility = System.Windows.Visibility.Visible;
                    window.testR.Fill = null;
                }

                System.Windows.Forms.Screen[] screens = System.Windows.Forms.Screen.AllScreens;
                if (screens.Length > 1)
                {
                    if (screens[0].Primary)
                    {
                        System.Windows.Forms.Screen s2 = System.Windows.Forms.Screen.AllScreens[1];
                        System.Drawing.Rectangle r2 = s2.WorkingArea;

                        window.Top = r2.Top;
                        window.Left = r2.Left;
                        //
                        window.Show();
                    }
                    else
                    {
                        System.Drawing.Rectangle r2 = screens[0].WorkingArea;
                        window.Top = r2.Top;
                        window.Left = r2.Left;
                        window.Show();
                    }

                }
                else
                {
                    window.Show();
                }

            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
            finally
            {
                MemoryManagement.FlushMemory();
            }
        }
        private void btnSetAudioTime_Click(object sender, RoutedEventArgs e)
        {
            int audioId = Convert.ToInt32(txtAudioId.Text);
            TemplateListItems lstMyItem = objTemplateList.Where(o => o.Item_ID == audioId && o.MediaType == 604).FirstOrDefault();
            if (lstMyItem != null)
            {
                if (validateAudio())
                {
                    if (!(Convert.ToInt32(txtAudioStart.Text) >= lstMyItem.Length || Convert.ToInt32(txtAudioEnd.Text) > lstMyItem.Length))
                    {
                        if (Convert.ToInt32(txtAudioStart.Text) < Convert.ToInt32(txtAudioEnd.Text))
                        {
                            lstMyItem.StartTime = Convert.ToInt32(txtAudioStart.Text);
                            lstMyItem.EndTime = Convert.ToInt32(txtAudioEnd.Text);
                            lstMyItem.InsertTime = Convert.ToInt32(txtAudioInsert.Text);
                            StackAudioStartEndTime.Visibility = Visibility.Collapsed;
                        }
                        else
                        {
                            MessageBox.Show("Audio start time must be less then audio end time!", "Digiphoto", MessageBoxButton.OK, MessageBoxImage.Exclamation, MessageBoxResult.OK);
                            //MsgBox.ShowHandlerDialog("Audio start time must be less then audio end time!", DigiMessageBox.DialogType.OK);
                        }

                    }
                    else
                    {
                        MessageBox.Show("Audio Start/End time can not exceed the audio length.", "Digiphoto", MessageBoxButton.OK, MessageBoxImage.Exclamation, MessageBoxResult.OK);
                        // MsgBox.ShowHandlerDialog("Audio Start/End time can not exceed the audio length.", DigiMessageBox.DialogType.OK);
                    }
                }
            }
        }
        #region Double Touch
        //Code for get double touch event
        private readonly Stopwatch _doubleTapStopwatch = new Stopwatch();
        private Point _lastTapLocation;
        private bool IsDoubleTap(TouchEventArgs e)
        {
            Point currentTapPosition = e.GetTouchPoint(this).Position;
            bool tapsAreCloseInDistance = GetDistanceBetweenPoints(currentTapPosition, _lastTapLocation) < 40;
            _lastTapLocation = currentTapPosition;
            TimeSpan elapsed = _doubleTapStopwatch.Elapsed;
            _doubleTapStopwatch.Restart();
            bool tapsAreCloseInTime = (elapsed != TimeSpan.Zero && elapsed < TimeSpan.FromSeconds(0.7));
            return tapsAreCloseInDistance && tapsAreCloseInTime;
        }
        public static double GetDistanceBetweenPoints(Point p, Point q)
        {
            double a = p.X - q.X;
            double b = p.Y - q.Y;
            double distance = Math.Sqrt(a * a + b * b);
            return distance;
        }
        #endregion Double Touch
        private void btnPlayVideoTemp_Click(object sender, RoutedEventArgs e)
        {
            int videoid = 0;
            if (!string.IsNullOrEmpty(txtSelVideoTemp.Text.Trim()))
                videoid = Convert.ToInt32(txtSelVideoTemp.Text);
            TemplateListItems lstMyItem = objTemplateList.Where(o => o.Item_ID == videoid && o.MediaType == 603).FirstOrDefault();
            //MediaStop();
            if (lstMyItem != null)
            {
                string filepath = LoginUser.DigiFolderVideoTemplatePath + System.IO.Path.GetFileName(lstMyItem.Name);
                vsMediaFileName = filepath;
                // MediaStop();
                MediaPlay();
            }
        }
        private void btnPlayAudio_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            e.Handled = true;
        }
        private void btnPlayVideoTemp_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            e.Handled = true;
        }
        private void btnImgPlay_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            e.Handled = true;
        }
        private void btnDropImgPlay_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            e.Handled = true;
        }
        private void btnDefaultSettings_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                btnClosePreview_Click(sender, e);
                LoadDefaultSceneAndFile();
                // FillPositionDropDown();
                BindGuestNodeProps();
                applyDefaultSettings();
                CurrentchromaSetting = string.Empty;
                overlayChromaSetting = string.Empty;
                MItem myBackground;
                IsLoadChroma = false;
                IsLoadOverlayChroma = false;
                m_objMixer.StreamsBackgroundSet(null, null, "", out myBackground, 0.0);
                OverlayElement.ElementMultipleSet("show=false", 0.0);
                Thread.Sleep(1000);
                rbNone.IsChecked = true;
                rbColorEffect_Click(rbNone, null);
            }
            catch
            { }
        }
        private void applyDefaultSettings()
        {
            objTemplateList.Where(o => o.IsChecked == true).ToList().ForEach(t => t.IsChecked = false);
            if (templateType.Equals("graphics"))
            {
                List<TemplateListItems> TLI = objTemplateList.Where(o => o.MediaType == 606).ToList();
                lstTemplates.ItemsSource = null;
                lstTemplates.ItemsSource = TLI;
            }
            if (templateType.Equals("audio"))
            {
                List<TemplateListItems> TLI = objTemplateList.Where(o => o.MediaType == 604).ToList();
                lstTemplates.ItemsSource = null;
                lstTemplates.ItemsSource = TLI;
            }
            if (templateType.Equals("video"))
            {
                List<TemplateListItems> TLI = objTemplateList.Where(o => o.MediaType == 603).ToList();
                lstTemplates.ItemsSource = null;
                lstTemplates.ItemsSource = TLI;
            }
            if (templateType.Equals("chroma"))
            {
                List<TemplateListItems> TLI = objTemplateList.Where(o => o.MediaType == 605).ToList();
                lstTemplates.ItemsSource = null;
                lstTemplates.ItemsSource = TLI;
            }
            if (templateType.Equals("border"))
            {
                List<TemplateListItems> TLI = objTemplateList.Where(o => o.MediaType == 608).ToList();
                lstTemplates.ItemsSource = null;
                lstTemplates.ItemsSource = TLI;
            }
            if (templateType.Equals("overlay"))
            {
                List<TemplateListItems> TLI = objTemplateList.Where(o => o.MediaType == 609).ToList();
                lstTemplates.ItemsSource = null;
                lstTemplates.ItemsSource = TLI;
            }


        }
        private void BindGuestTemplateList(VideoElements lstMyItem, bool addTemplate)
        {
            if (addTemplate)
            {
                TemplateListItems tmpList = new TemplateListItems();
                tmpList.isActive = false;
                tmpList.FilePath = @"/DigiPhoto;component/images/vidico.png";
                tmpList.Item_ID = lstMyItem.PhotoId;
                tmpList.IsChecked = true;
                tmpList.DisplayName = lstMyItem.Name;
                tmpList.MediaType = lstMyItem.MediaType == 2 ? 602 : 607;
                tmpList.Name = lstMyItem.Name;
                tmpList.Length = lstMyItem.videoLength;
                tmpList.GuestVideoPath = lstMyItem.VideoFilePath;
                objTemplateList.Add(tmpList);
                lstTemplates.IsEnabled = false;
            }
            else
            {
                TemplateListItems tmpList = objTemplateList.Where(o => (o.MediaType == 602 || o.MediaType == 607) && o.isActive == false).FirstOrDefault();
                objTemplateList.Remove(tmpList);
            }
        }
        public void LoadGuestVideoSlots()
        {
            //  StackGuestVideoTemplate.Visibility = Visibility.Collapsed;
            if (slotList.Count > 0)
            {

                markImage.Source = new BitmapImage(new Uri(@"/DigiPhoto;component/images/UnMarkT1.png", UriKind.Relative)); ;
                btnGuestVideoTemplate.Tag = 0;
                isVideoTempSelected = true;
                PrintOrderPageList.Clear();
                defaultProcessListCount = slotList.Count;
                BindPageStrips();
                VideoElements lstMyItem = lstVideoElements.Where(o => o.PhotoId == this.DropPhotoId && o.PageNo == this.activePage).FirstOrDefault();
                lstMyItem.isVideoTemplate = Visibility.Visible;
                lstGuestImages.ItemsSource = null;
                lstGuestImages.ItemsSource = lstVideoElements;
                BindGuestTemplateList(lstMyItem, true);
            }
            else
            {

                markImage.Source = new BitmapImage(new Uri(@"/DigiPhoto;component/images/MarkT1.png", UriKind.Relative)); //bitMapImg;
                //markImage.Source = new BitmapImage(new Uri(@"/images/MarkT1.png", UriKind.Relative));
                btnGuestVideoTemplate.Tag = 1;
                //chkGuestVideoTemplate.IsChecked = false;
            }
        }
        private void btnHide_Click(object sender, RoutedEventArgs e)
        {
            //////creating Instance of Search result window//////
            SearchResult _objSearchResult = new SearchResult();
            //_objSearchResult.pagename = "";
            if (RobotImageLoader.GroupImages.Count == 0 && IsGoupped == "View All")
            {
                IsGoupped = "View Group";
            }
            _objSearchResult.pagename = "Saveback";
            if (RobotImageLoader.PrintImages.Count > 0)
                _objSearchResult.Savebackpid = Convert.ToString(RobotImageLoader.PrintImages[RobotImageLoader.PrintImages.Count - 1].PhotoId);
            else
            {
                _objSearchResult.Savebackpid = "0";
            }
            _objSearchResult.Show();
            ////////Calling logic of searching 
            _objSearchResult.LoadWindow();
            ////////Closing current window
            this.Hide();
            //SearchResult.
        }
        private void extractBtn_Click(object sender, RoutedEventArgs e)
        {
            //MediaStop();
            //FrameBox.ShowPanHandlerDialog(extractVideoPath, extractVideoEndTime);
            //FrameBox.LicenseKey = ConfigManager.IMIXConfigurations.ContainsKey((int)ConfigParams.MediaPlayerLicenseKey) ? ConfigManager.IMIXConfigurations[(int)ConfigParams.MediaPlayerLicenseKey] : string.Empty;
            //FrameBox.UserName = ConfigManager.IMIXConfigurations.ContainsKey((int)ConfigParams.VisioForgeUsername) ? ConfigManager.IMIXConfigurations[(int)ConfigParams.VisioForgeUsername] : string.Empty;
            //FrameBox.EmailID = ConfigManager.IMIXConfigurations.ContainsKey((int)ConfigParams.VisioForgeEmailId) ? ConfigManager.IMIXConfigurations[(int)ConfigParams.VisioForgeEmailId] : string.Empty;
        }
        protected void FrameBox_ExecuteMethod(object sender, EventArgs e)
        {
            txtvideoStart.Text = FrameBox.txtvideoStart.Text;
            txtVideoEnd.Text = FrameBox.txtVideoEnd.Text;
            VideoPage lstMyItem = PrintOrderPageList.Where(o => o.PhotoId == this.DropPhotoId && o.PageNo == activePage).FirstOrDefault();
            if (lstMyItem != null)
            {
                if (((!string.IsNullOrEmpty(txtvideoStart.Text)) && !string.IsNullOrEmpty(txtVideoEnd.Text)) && !((txtvideoStart.Text.Contains(".") || txtVideoEnd.Text.Contains("."))))
                {

                    if (!(Convert.ToInt32(txtvideoStart.Text) >= lstMyItem.videoLength || Convert.ToInt32(txtVideoEnd.Text) > lstMyItem.videoLength))
                    {
                        if (Convert.ToInt32(txtvideoStart.Text) < Convert.ToInt32(txtVideoEnd.Text))
                        {
                            lstMyItem.videoStartTime = Convert.ToInt32(txtvideoStart.Text);
                            lstMyItem.videoEndTime = Convert.ToInt32(txtVideoEnd.Text);
                            //StackVideoStartEndTime.Visibility = Visibility.Collapsed;
                        }
                        else
                        {
                            MsgBox.ShowHandlerDialog("Video start time should be less than video end time!", DigiMessageBox.DialogType.OK);
                        }

                    }
                    else
                    {
                        MsgBox.ShowHandlerDialog("Video Start/End time cannot be greater than video length.", DigiMessageBox.DialogType.OK);
                    }
                }
                else
                {
                    MsgBox.ShowHandlerDialog("Video Start/End time has some invalid data!.", DigiMessageBox.DialogType.OK);
                }
            }
        }
        protected void LogoControlBox_ExecuteMethod(object sender, EventArgs e)
        {
            //chkTextLogo_Click(null, null);
        }
        //private void generalEffectBtn_Click(object sender, RoutedEventArgs e)
        //{
        //    GeneralEffectsBox.ShowPanHandlerDialog();
        //    GeneralEffectsBox.LicenseKey = ConfigManager.IMIXConfigurations.ContainsKey((int)ConfigParams.MediaPlayerLicenseKey) ? ConfigManager.IMIXConfigurations[(int)ConfigParams.MediaPlayerLicenseKey] : string.Empty;
        //    GeneralEffectsBox.UserName = ConfigManager.IMIXConfigurations.ContainsKey((int)ConfigParams.VisioForgeUsername) ? ConfigManager.IMIXConfigurations[(int)ConfigParams.VisioForgeUsername] : string.Empty;
        //    GeneralEffectsBox.EmailID = ConfigManager.IMIXConfigurations.ContainsKey((int)ConfigParams.VisioForgeEmailId) ? ConfigManager.IMIXConfigurations[(int)ConfigParams.VisioForgeEmailId] : string.Empty;

        //}
        //private void btnLogo_Click(object sender, RoutedEventArgs e)
        //{
        //    Button btn = sender as Button;
        //    LogoControlBox.Top = ConfigManager.IMIXConfigurations.ContainsKey((int)ConfigParams.ResizeHeight) ? Convert.ToInt32(ConfigManager.IMIXConfigurations[(int)ConfigParams.ResizeHeight]) : 1072;
        //    LogoControlBox.Left = ConfigManager.IMIXConfigurations.ContainsKey((int)ConfigParams.ResizeWidth) ? Convert.ToInt32(ConfigManager.IMIXConfigurations[(int)ConfigParams.ResizeWidth]) : 1920;
        //    if (Convert.ToInt32(btn.Tag) == 1)
        //    {
        //        LoadGraphicsList();
        //        //templateType = "graphics";
        //        //txtTemplateRotate.Text = "Graphics Templates";
        //        LogoControlBox.videoExpLen = CalculateProcessedVideoLength();
        //        LogoControlBox.ShowPanHandlerDialog();
        //        lstTemplates.Visibility = Visibility.Visible;
        //        txtTemplateRotate.Visibility = Visibility.Visible;
        //        btnCloseTempList.Visibility = Visibility.Visible;
        //        StackImageDisplayTime.Visibility = Visibility.Collapsed;
        //        StackVideoStartEndTime.Visibility = Visibility.Collapsed;
        //        StackVideoTemplatePlay.Visibility = Visibility.Collapsed;
        //        StackGuestVideoTemplate.Visibility = Visibility.Collapsed;

        //    }
        //    else
        //    {
        //        //stackLogo.Visibility = Visibility.Collapsed;
        //        List<TemplateListItems> TLI = objTemplateList.Where(o => o.MediaType == 603 && o.isActive == true).ToList();
        //        lstTemplates.ItemsSource = TLI;
        //        templateType = "video";
        //        txtTemplateRotate.Text = "Video Templates";
        //        //VisioMediaPlayer.BackgroundImage_Source = null;
        //    }
        //}
        private void btnGuestVideoTemplate_Click(object sender, RoutedEventArgs e)
        {
            Button btn = sender as Button;
            if (Convert.ToInt32(btn.Tag) == 1)
            {
                objTemplateList.Where(o => o.IsChecked == true && o.MediaType == 603).ToList().ForEach(t => t.IsChecked = false);
                List<TemplateListItems> TLI = objTemplateList.Where(o => o.MediaType == 603).ToList();
                lstTemplates.ItemsSource = null;
                lstTemplates.ItemsSource = TLI;

                slotList.Clear();
                VideoElements lstMyItem = lstVideoElements.Where(o => o.PhotoId == this.DropPhotoId && o.PageNo == this.activePage).FirstOrDefault();
                VideoSlotsTime.VideoLength = lstMyItem.videoLength;
                VideoSlotsTime.VideoName = lstMyItem.Name;
                VideoSlotsTime.isGuestVideoTemplate = true;
                VideoSlotsTime.SetParent(this);
                VideoSlotsTime.Visibility = Visibility.Visible;
                markImage.Source = new BitmapImage(new Uri(@"/images/UnMarkT1.png", UriKind.Relative));
                btn.Tag = 0;

            }
            else
            {
                VideoElements lstMyItem = lstVideoElements.Where(o => o.PhotoId == this.DropPhotoId && o.PageNo == this.activePage).FirstOrDefault();
                if (isVideoTempSelected)
                {
                    btn.Tag = 1;
                    markImage.Source = new BitmapImage(new Uri(@"/DigiPhoto;component/images/MarkT1.png", UriKind.Relative));//bitMapImg;
                    isVideoTempSelected = false;
                    defaultProcessListCount = 10;
                    PrintOrderPageList.Clear();
                    BindGuestTemplateList(lstMyItem, false);
                    BindPageStrips();
                    lstMyItem.isVideoTemplate = Visibility.Collapsed;
                    lstGuestImages.ItemsSource = null;
                    lstGuestImages.ItemsSource = lstVideoElements;
                    lstTemplates.IsEnabled = true;
                    slotList.Clear();
                }
            }
        }
        private void Window_Closing(object sender, CancelEventArgs e)
        {
            UnsubscribeEvents();
        }
        private void UnsubscribeEvents()
        {
            btnCloseTempList.Click -= new RoutedEventHandler(btnCloseTempList_Click);
            btnCollapse.Click -= new RoutedEventHandler(btnCollapse_Click);
            btnProcess.Click -= new RoutedEventHandler(btnProcess_Click);
            btnStopProcess.Click -= new RoutedEventHandler(btnStopProcess_Click);
            btnchkpreview.Click -= new RoutedEventHandler(btnchkpreview_Click);
            btnIncludeAll.Click -= new RoutedEventHandler(btnSelectAll_Click);
            btnReset.Click -= new RoutedEventHandler(btnReset_Click);
            btnSave.Click -= new RoutedEventHandler(btnSave_Click);
            btnExit.Click -= new RoutedEventHandler(btnExit_Click);
            btnDefaultSettings.Click -= new RoutedEventHandler(btnDefaultSettings_Click);
            btnHide.Click -= new RoutedEventHandler(btnHide_Click);
            btnVideo.Click -= new RoutedEventHandler(btnVideo_Click);
            btnAudioEffects.Click -= new RoutedEventHandler(btnAudio_Click);
            btnChroma.Click -= new RoutedEventHandler(btnChroma_Click);
            mMixerList1.OnMixerSelChanged -= new EventHandler(mMixerList1_OnMixerSelChanged);
            VidTimer.Tick -= new EventHandler(VidTimer_Tick);
            mConfigList1.OnConfigChanged -= new EventHandler(mConfigList1_OnConfigChanged);
            bwSaveVideos.DoWork -= new System.ComponentModel.DoWorkEventHandler(bwSaveVideos_DoWork);
            bwSaveVideos.RunWorkerCompleted -= new System.ComponentModel.RunWorkerCompletedEventHandler(bwSaveVideos_RunWorkerCompleted);

            ///Timers
            //timer.Tick -= new EventHandler(delegate(object s, EventArgs a) { timer_Tick(); });
            //timer.Stop();

            //bwVideoEditing.DoWork -= new System.ComponentModel.DoWorkEventHandler(bwVideoEditing_DoWork);
            //bwVideoEditing.RunWorkerCompleted -= new System.ComponentModel.RunWorkerCompletedEventHandler(bwVideoEditing_RunWorkerCompleted);

        }
        private void txtvideoStart_TextChanged(object sender, TextChangedEventArgs e)
        {
            //VideoPage lstMyItem = PrintOrderPageList.Where(o => o.PhotoId == this.DropPhotoId && o.PageNo == activePage).FirstOrDefault();
            //if (lstMyItem != null)
            //{
            //    if (((!string.IsNullOrEmpty(txtvideoStart.Text)) && !string.IsNullOrEmpty(txtVideoEnd.Text)) && !((txtvideoStart.Text.Contains(".") || txtVideoEnd.Text.Contains("."))))
            //    {

            //        if (!(Convert.ToInt32(txtvideoStart.Text) >= lstMyItem.videoLength || Convert.ToInt32(txtVideoEnd.Text) > lstMyItem.videoLength))
            //        {
            //            if (Convert.ToInt32(txtvideoStart.Text) < Convert.ToInt32(txtVideoEnd.Text))
            //            {
            //                lstMyItem.videoStartTime = Convert.ToInt32(txtvideoStart.Text);
            //                lstMyItem.videoEndTime = Convert.ToInt32(txtVideoEnd.Text);
            //                //StackVideoStartEndTime.Visibility = Visibility.Collapsed;
            //            }
            //            else
            //            {
            //                MsgBox.ShowHandlerDialog("Video start time should be less than video end time!", DigiMessageBox.DialogType.OK);
            //            }

            //        }
            //        else
            //        {
            //            MsgBox.ShowHandlerDialog("Video Start/End time cannot be greater than video length.", DigiMessageBox.DialogType.OK);
            //        }
            //    }
            //    else
            //    {
            //        MsgBox.ShowHandlerDialog("Video Start/End time has some invalid data!.", DigiMessageBox.DialogType.OK);
            //    }
            //}
        }

        #endregion
        private void btnBorder_Click(object sender, RoutedEventArgs e)
        {
            StackGraphicSettingPanel1.Visibility = Visibility.Collapsed;
            StackGraphicSettingPanel2.Visibility = Visibility.Collapsed;
            SetTemplateListVisibility(true);
            // StackGraphicSettingPanel1.Visibility = Visibility.Visible;
            //Button btn = sender as Button;
            //if (Convert.ToInt32(btn.Tag) == 1)
            //{
            //    BorderControl.videoExpLen = CalculateProcessedVideoLength();
            //    BorderControl.slotList = slotList;
            //    BorderControl.ShowHandlerDialog();
            //}
            try
            {
                //  HighlightSelectedButton("btnChroma");
                //chkAddLogo.IsChecked = false;
                //LogoControlBox.stackLogo.Visibility = Visibility.Collapsed;
                txtTemplateRotate.Visibility = Visibility.Visible;
                templateType = "border";
                // ShowHideControls("chroma");
                lstTemplates.IsEnabled = true;
                List<TemplateListItems> TLI = objTemplateList.Where(o => o.MediaType == 608).ToList();
                if (TLI.Count == 0)
                    lstTemplates.ItemsSource = null;
                else
                {
                    lstTemplates.ItemsSource = TLI;
                }
                txtTemplateRotate.Text = "Borders";
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }

        private void btnLoadGraphic_Click(object sender, RoutedEventArgs e)
        {
            LoadGraphicsList();
            SetTemplateListVisibility(true);
            StackGraphicSettingPanel1.Visibility = Visibility.Visible;
            StackGraphicSettingPanel2.Visibility = Visibility.Visible;
        }

        private void LoadGraphicsList()
        {
            List<TemplateListItems> TLI = objTemplateList.Where(o => o.MediaType == 606).ToList();
            foreach (TemplateListItems item in TLI)
            {
                item.IsChecked = false;
            }
            lstTemplates.ItemsSource = TLI;
            lstTemplates.IsEnabled = true;
            templateType = "graphics";
            txtTemplateRotate.Text = "Graphics Templates";
        }

        private void Window_Unloaded(object sender, RoutedEventArgs e)
        {
            //UnsubscribeEvents();
        }

        private void btAcquiredFrame_Click(object sender, RoutedEventArgs e)
        {
            //CaptureFrameDownloader _objmdlnd = new CaptureFrameDownloader();
            //if (_objmdlnd.IsShoworHide())
            //{
            //    _objmdlnd.Show();
            //}

        }


        public static int tempGraphicId = 100;
        private void btnGuestImageAsGraphic_Click(object sender, RoutedEventArgs e)
        {
            // CommonUtility.CleanFolder(System.Environment.CurrentDirectory,new string[]{"CroppedImage"});
            VideoElements lstMyItem = lstVideoElements.Where(o => o.PhotoId == this.DropPhotoId && o.PageNo == this.activePage && o.MediaType == 1).FirstOrDefault();
            if (lstMyItem != null)
            {
                string FilePath = lstMyItem.GuestImagePath.Replace("\\Thumbnails", "");
                CreateGraphicsFromImage(FilePath, lstMyItem.CreatedDate, lstMyItem.PhotoId);
            }
            else
            {
                if (processImageItem != null)
                {
                    string FilePath = processImageItem.HotFolderPath + processImageItem.CreatedOn.ToString("yyyyMMdd") + "\\" + processImageItem.FileName; //image path  
                    CreateGraphicsFromImage(FilePath, (DateTime)processImageItem.CreatedOn, Convert.ToInt32(processImageItem.Photo_RFID));
                }
            }
        }

        private void CreateGraphicsFromImage(string filePath, DateTime createdDate, int photoId)
        {
            try
            {
                ClearEffects();
                isImageAsGraphic = true;
                if (!Directory.Exists(Environment.CurrentDirectory + "\\CroppedImage"))
                    Directory.CreateDirectory(Environment.CurrentDirectory + "\\CroppedImage");
                string imagePath = filePath;
                string fileName = System.IO.Path.GetFileName(imagePath);
                if (File.Exists(imagePath.Replace(".jpg", ".png")))
                {
                    imagePath = filePath.Replace(".jpg", ".png");
                }

                string tempPath = Environment.CurrentDirectory + "\\CroppedImage\\" + tempGraphicId + System.IO.Path.GetExtension(imagePath); //".png";
                if (File.Exists(tempPath))
                {
                    File.Delete(tempPath);
                }

                tempPath = CropImageAsPerAspectRatio(imagePath, tempPath);
                dragCanvas.Visibility = Visibility.Visible;
                TemplateListItems tmpList = new TemplateListItems();
                tmpList.isActive = true;
                tmpList.FilePath = tempPath;
                tmpList.Item_ID = photoId;
                tmpList.IsChecked = true;
                tmpList.DisplayName = fileName;
                tmpList.MediaType = 601;
                tmpList.Name = fileName;
                //tmpList.Tooltip = "Graphics";
                tempGraphicId++;
                objTemplateList.Add(tmpList);
            }
            catch (Exception ex)
            {
                ClearImageSourceControl();
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }
        #region CropImage
        private string CropImageAsPerAspectRatio(string inputImage, string CroppedImagePath)
        {
            try
            {
                ClearImageSourceControl();
                VideoProcessingClass objVideoPrcessing = new VideoProcessingClass();
                int width = ConfigManager.IMIXConfigurations.ContainsKey((int)ConfigParams.ResizeWidth) ? Convert.ToInt32(ConfigManager.IMIXConfigurations[(int)ConfigParams.ResizeWidth]) : 1920;
                int height = ConfigManager.IMIXConfigurations.ContainsKey((int)ConfigParams.ResizeHeight) ? Convert.ToInt32(ConfigManager.IMIXConfigurations[(int)ConfigParams.ResizeHeight]) : 1072;

                objVideoPrcessing.CropeImageAsperAspectRatio(inputImage, CroppedImagePath, width, height);
                specFileName = CroppedImagePath;
                Uri uri = new Uri(CroppedImagePath);
                ClearImageSourceControl();
                mainImage.Source = new BitmapImage(uri);
                mainImage_Size.Source = new BitmapImage(uri);
                mainImagesize.Source = new BitmapImage(uri);
                mainImage.UpdateLayout();
                mainImagesize.UpdateLayout();
                mainImage_Size.UpdateLayout();
                canbackground.UpdateLayout();
                if (System.IO.Path.GetExtension(inputImage).ToLower() != ".png") //2 - Aquarium
                {
                    ExtractPng(specFileName);
                }
                //    if (objPhotoLayer != null && AutomatedVideoEditWorkFlow && !IsProcessedVideoEditing)
                //        ApplyZoomAndPosition(objPhotoLayer);
            }
            catch (Exception ex)
            {
                ClearImageSourceControl();
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                return null;
            }
            finally
            {
            }
            return CroppedImagePath;
        }

        private string LoadChromacolor()
        {
            string chromaColor = string.Empty;
            //if (rdbGreen.IsChecked == true)
            //{
            //    chromaColor = "Green";
            //}
            //else if (rdbBlue.IsChecked == true)
            //{
            //    chromaColor = "Blue";
            //}
            //else
            //{
            //    chromaColor = "Red";
            //}
            return chromaColor;
        }
        #endregion CropImage

        #region image Editing
        private double _ZoomFactor = 1;
        private double _GraphicsZoomFactor = 1;
        private TransformGroup transformGroup;
        private ScaleTransform zoomTransform = new ScaleTransform();
        private double _maxZoomFactor = 4;
        string specFileName = string.Empty;
        /// <summary>
        /// Handles the Click event of the ZoomInButton control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void ZoomInButton_Click(object sender, RoutedEventArgs e)
        {
            ZoomIn();
        }

        private void ZoomIn()
        {
            RenderOptions.SetEdgeMode(mainImage, EdgeMode.Aliased);
            try
            {
                if (this.elementForContextMenu is Button)
                {

                    Button b = (Button)this.elementForContextMenu;
                    string source = ((System.Windows.Controls.Image)(b.Content)).Source.ToString();
                    if (System.IO.Path.GetExtension(source) != ".gif")
                    {
                        if (b.Tag != null)
                        {
                            //_GraphicsZoomFactor = b.Tag.ToDouble();
                            _GraphicsZoomFactor += .025;
                        }
                        else
                        {
                            _GraphicsZoomFactor = 1;
                        }
                        TransformGroup grp = new TransformGroup();
                        TransformGroup tg = this.elementForContextMenu.GetValue(Canvas.RenderTransformProperty) as TransformGroup;
                        RotateTransform rotation = new RotateTransform();
                        b.RenderTransformOrigin = new System.Windows.Point(0.5, 0.5);
                        rotation.CenterX = 0;
                        rotation.CenterY = 0;
                        ScaleTransform scale = new ScaleTransform();

                        if (tg != null)
                        {
                            if (tg.Children.Count > 0)
                            {
                                if (tg.Children[0] is ScaleTransform)
                                {
                                    scale = (ScaleTransform)tg.Children[0];
                                }
                                else if (tg.Children[0] is RotateTransform)
                                {
                                    rotation = (RotateTransform)tg.Children[0];

                                }
                            }
                            if (tg.Children.Count > 1)
                            {
                                if (tg.Children[1] is ScaleTransform)
                                {
                                    scale = (ScaleTransform)tg.Children[1];
                                }
                                else if (tg.Children[1] is RotateTransform)
                                {
                                    rotation = (RotateTransform)tg.Children[1];

                                }
                            }
                        }
                        if (scale == null)
                        {
                            scale = new ScaleTransform();
                            scale.ScaleX = _GraphicsZoomFactor;
                            scale.ScaleY = _GraphicsZoomFactor;
                            scale.CenterX = 0;
                            scale.CenterY = 0;
                        }
                        else
                        {
                            scale.ScaleX = _GraphicsZoomFactor;
                            scale.ScaleY = _GraphicsZoomFactor;
                            scale.CenterX = 0;
                            scale.CenterY = 0;
                        }

                        grp.Children.Add(scale);
                        if (rotation != null)
                        {
                            grp.Children.Add(rotation);


                        }
                        b.RenderTransformOrigin = new System.Windows.Point(0.5, 0.5);
                        b.Tag = _GraphicsZoomFactor.ToString();
                        b.RenderTransform = grp;
                        this.elementForContextMenu = b;
                    }
                }

                else if (this.elementForContextMenu == null)
                {
                    if (_ZoomFactor >= _maxZoomFactor)
                    {
                        _ZoomFactor = 4;

                        return;
                    }

                    _ZoomFactor += .025;


                    if (zoomTransform != null)
                    {

                        zoomTransform.CenterX = mainImage.ActualWidth / 2;
                        zoomTransform.CenterY = mainImage.ActualHeight / 2;

                        zoomTransform.ScaleX = _ZoomFactor;
                        zoomTransform.ScaleY = _ZoomFactor;

                        transformGroup = new TransformGroup();
                        transformGroup.Children.Add(zoomTransform);

                        GrdBrightness.RenderTransform = transformGroup;
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                MessageBox.Show(ex.Message);
            }
            finally
            {
                //  DigiPhoto.MemoryManagement.FlushMemory();
            }
        }

        /// <summary>
        /// Handles the Click event of the ZoomOutButton control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void ZoomOutButton_Click(object sender, RoutedEventArgs e)
        {
            RenderOptions.SetEdgeMode(mainImage, EdgeMode.Aliased);
            try
            {
                if (this.elementForContextMenu is Button)
                {
                    Button b = (Button)this.elementForContextMenu;
                    string source = ((System.Windows.Controls.Image)(b.Content)).Source.ToString();
                    if (System.IO.Path.GetExtension(source) != ".gif")
                    {
                        if (b.Tag != null)
                        {
                            // _GraphicsZoomFactor = b.Tag.ToDouble();

                        }

                        if (_GraphicsZoomFactor >= .625)
                            _GraphicsZoomFactor -= .025;
                        else
                        {
                            //  _GraphicsZoomFactor = 1;
                            return;
                        }


                        TransformGroup grp = new TransformGroup();
                        TransformGroup tg = this.elementForContextMenu.GetValue(Canvas.RenderTransformProperty) as TransformGroup;
                        RotateTransform rotation = new RotateTransform();
                        ScaleTransform scale = new ScaleTransform();

                        if (tg != null)
                        {
                            if (tg.Children.Count > 0)
                            {
                                if (tg.Children[0] is ScaleTransform)
                                {
                                    scale = (ScaleTransform)tg.Children[0];
                                }
                                else if (tg.Children[0] is RotateTransform)
                                {
                                    rotation = (RotateTransform)tg.Children[0];
                                }
                            }
                            if (tg.Children.Count > 1)
                            {
                                if (tg.Children[1] is ScaleTransform)
                                {
                                    scale = (ScaleTransform)tg.Children[1];
                                }
                                else if (tg.Children[1] is RotateTransform)
                                {
                                    rotation = (RotateTransform)tg.Children[1];
                                }
                            }
                        }
                        if (scale == null)
                        {
                            return;
                        }
                        else
                        {
                            scale.ScaleX = scale.ScaleX - 0.025;
                            scale.ScaleY = scale.ScaleY - 0.025;
                            scale.CenterX = 0;
                            scale.CenterY = 0;

                            grp.Children.Add(scale);
                            if (rotation != null)
                            {
                                grp.Children.Add(rotation);
                            }

                            b.RenderTransformOrigin = new System.Windows.Point(0.5, 0.5);
                            b.Tag = _GraphicsZoomFactor;
                            b.RenderTransform = grp;
                            this.elementForContextMenu = b;
                        }
                    }
                }
                else if (this.elementForContextMenu == null)
                {
                    if (_ZoomFactor >= .525)
                        _ZoomFactor -= .025;
                    else
                    {
                        _ZoomFactor = .5;
                        return;
                    }

                    if (zoomTransform != null)
                    {
                        zoomTransform.CenterX = mainImage.ActualWidth / 2;
                        zoomTransform.CenterY = mainImage.ActualHeight / 2;
                        zoomTransform.ScaleX = _ZoomFactor;
                        zoomTransform.ScaleY = _ZoomFactor;
                        transformGroup = new TransformGroup();
                        transformGroup.Children.Add(zoomTransform);
                        GrdBrightness.RenderTransform = transformGroup;
                    }
                }

            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }

        }
        /// <summary>
        /// Handles the MouseLeftButtonUp event of the mainImage control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="MouseButtonEventArgs"/> instance containing the event data.</param>
        private void mainImage_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            if (e.LeftButton == MouseButtonState.Released)
            {
                this.elementForContextMenu = null;
            }
        }

        /// <summary>
        /// Handles the MouseLeftButtonUp event of the mainImage_Size control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="MouseButtonEventArgs"/> instance containing the event data.</param>
        private void mainImage_Size_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            if (e.LeftButton == MouseButtonState.Released)
            {
                this.elementForContextMenu = null;
            }
        }
        private void RepeatButton_MouseWheel(object sender, MouseWheelEventArgs e)
        {
            if (e.Delta > 0)
            {
                ZoomInButton_Click(sender, new RoutedEventArgs());
            }
            else
            {

                ZoomOutButton_Click(sender, new RoutedEventArgs());
            }
        }
        private void GrdBrightn_MouseWheel(object sender, MouseWheelEventArgs e)
        {
            if (dragCanvas.IsMouseOver == true)
            {
                if (e.Delta > 0)
                {
                    ZoomInButton_Click(sender, new RoutedEventArgs());
                }
                else
                {

                    ZoomOutButton_Click(sender, new RoutedEventArgs());
                }
            }
        }
        private void ExtractPng(string imagename)
        {
            try
            {
                imagename = imagename.Replace("jpg", "png").Replace("JPG", "PNG");
                specFileName = imagename;
                string chromaColor = LoadChromacolor();
                ChromaEffectAllColor _colorscreendefault = new ChromaEffectAllColor();
                switch (chromaColor)
                {

                    case "Green":
                        ChromaKeyHSVEffect _greenscreendefault3 = new ChromaKeyHSVEffect();
                        _greenscreendefault3.HueMin = 0.2;
                        _greenscreendefault3.HueMax = 0.5;
                        _greenscreendefault3.LightnessShift = 0.15;
                        _greenscreendefault3.SaturationShift = 0.38;
                        Grdcartoonize.Effect = _greenscreendefault3;
                        break;

                    case "Blue":
                        _colorscreendefault.ColorKey = (System.Windows.Media.Color)System.Windows.Media.ColorConverter.ConvertFromString("#4D8AEC"); ;//(Color)ColorConverter.ConvertFromString(txtColorCode.Text);
                        _colorscreendefault.Tolerance = float.Parse(".6");
                        Grdcartoonize.Effect = _colorscreendefault;
                        break;

                    case "Red":
                        _colorscreendefault.ColorKey = (System.Windows.Media.Color)System.Windows.Media.ColorConverter.ConvertFromString("#FF0000");
                        _colorscreendefault.Tolerance = 0.4f;
                        // _colorscreendefault.Tolerance = ChromaTolerance;
                        Grdcartoonize.Effect = _colorscreendefault;
                        break;

                    default:
                        break;
                }
                foreach (var ctrl in dragCanvas.Children)
                {
                    if (ctrl is Button)
                    {
                        Button cid = (Button)ctrl;
                        string source = ((System.Windows.Controls.Image)(cid.Content)).Source.ToString();
                        if (System.IO.Path.GetExtension(source) == ".gif")
                        {
                            cid.Visibility = Visibility.Hidden;
                        }
                    }
                }

                canbackground.Width = mainImage.Source.Width;
                canbackground.Height = mainImage.Source.Height;
                canbackground.UpdateLayout();
                RenderTargetBitmap _objeditedImage = jCaptureScreen(canbackground);// jCaptureScreen(Grdcartoonize);
                using (var fileStream = new FileStream(imagename, FileMode.Create, FileAccess.ReadWrite))
                {
                    PngBitmapEncoder encoder = new PngBitmapEncoder();
                    encoder.Interlace = PngInterlaceOption.On;
                    encoder.Frames.Add(BitmapFrame.Create(_objeditedImage));
                    encoder.Save(fileStream);
                }

                ClearImageSourceControl();
                canbackground.UpdateLayout();
                Uri uri1 = new Uri(imagename);
                canbackground.Width = 500;
                canbackground.Height = 290;
                canbackground.UpdateLayout();
                mainImage.Source = new BitmapImage(uri1);
                mainImage_Size.Source = new BitmapImage(uri1);
                mainImagesize.Source = new BitmapImage(uri1);

                canbackground.UpdateLayout();
            }
            catch (Exception ex)
            {
                ClearImageSourceControl();
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }
        public RenderTargetBitmap jCaptureScreen(Grid forWdht)
        {
            RenderTargetBitmap renderBitmap = null;
            try
            {
                this.InvalidateVisual();
                BitmapImage bi = mainImage.Source as BitmapImage;
                if (bi == null)
                {
                    ClearImageSourceControl();
                    bi = mainImage.Source as BitmapImage;
                }
                double dpiX = bi.DpiX;
                double dpiY = bi.DpiY;
                RenderOptions.SetBitmapScalingMode(forWdht, BitmapScalingMode.HighQuality);
                RenderOptions.SetEdgeMode(forWdht, EdgeMode.Aliased);
                try
                {
                    //if (_ZoomFactor > 1.4)
                    //{
                    //    Size size = new Size(forWdht.ActualWidth, forWdht.ActualHeight);
                    //    renderBitmap = new RenderTargetBitmap((int)(size.Width * dpiX / 96.0 * (1 / _ZoomFactor)), (int)(size.Height * dpiY / 96.0 * (1 / _ZoomFactor)),
                    //        dpiX, dpiY, PixelFormats.Default);
                    //    RenderOptions.SetEdgeMode(forWdht, EdgeMode.Aliased);
                    //    forWdht.SnapsToDevicePixels = true;
                    //    forWdht.RenderTransform = new ScaleTransform(1 / _ZoomFactor, 1 / _ZoomFactor, 0.5, 0.5);
                    //    forWdht.Measure(size);
                    //    forWdht.Arrange(new Rect(size));
                    //    renderBitmap.Render(forWdht);
                    //    forWdht.RenderTransform = null;
                    //}
                    //else
                    //{
                    Size size = new Size(forWdht.Width, forWdht.Height);
                    renderBitmap = new RenderTargetBitmap((int)(size.Width * dpiY / 96.0),
                        (int)(size.Height * dpiY / 96.0), dpiX, dpiY, PixelFormats.Default);
                    RenderOptions.SetEdgeMode(forWdht, EdgeMode.Aliased);
                    forWdht.SnapsToDevicePixels = true;
                    forWdht.RenderTransform = new ScaleTransform(1, 1, 0.5, 0.5);
                    forWdht.Measure(size);
                    forWdht.Arrange(new Rect(size));
                    renderBitmap.Render(forWdht);
                    forWdht.RenderTransform = null;
                    //}
                    return renderBitmap;
                }
                catch (Exception ex)
                {
                    Rect bounds = VisualTreeHelper.GetDescendantBounds(GrdBrightness);
                    DrawingVisual dv = new DrawingVisual();
                    renderBitmap = new RenderTargetBitmap((int)(bounds.Width * dpiX / 96.0),
                                                                  (int)(bounds.Height * dpiY / 96.0),
                                                                  dpiX,
                                                                  dpiY,
                                                                  PixelFormats.Default);

                    using (DrawingContext ctx = dv.RenderOpen())
                    {
                        VisualBrush vb = new VisualBrush(forWdht);
                        ctx.DrawRectangle(vb, null, new Rect(new System.Windows.Point(), bounds.Size));
                    }

                    renderBitmap.Render(dv);
                    return renderBitmap;
                }
            }
            catch (Exception ex)
            {
                ClearImageSourceControl();
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                return renderBitmap;
            }
        }
        private void ClearImageSourceControl()
        {
            mainImage.Source = null;
            mainImage_Size.Source = null;
            mainImagesize.Source = null;
            Uri ImagePathUri = new Uri(@"/DigiPhoto;component/images/blank.png", UriKind.RelativeOrAbsolute);
            mainImage.Source = new BitmapImage(ImagePathUri);
            mainImage_Size.Source = new BitmapImage(ImagePathUri);
            mainImagesize.Source = new BitmapImage(ImagePathUri);
        }
        private void ClearEffects()
        {
            mainImage.Source = null;
            mainImage_Size.Source = null;
            _ZoomFactor = 1;
            VideoEditPlayerPanel.Visibility = Visibility.Hidden;
            gdMediaPlayer.Visibility = Visibility.Visible;
            grdZoom.UpdateLayout();
            grdsize.UpdateLayout();

            mainImage.SnapsToDevicePixels = true;
            RenderOptions.SetEdgeMode(mainImage, EdgeMode.Aliased);
            canbackground.RenderTransform = new RotateTransform();

            dragCanvas.AllowDragOutOfView = true;
            GrdBrightness.Margin = new Thickness(0, 0, 0, 0);
            canbackground.HorizontalAlignment = System.Windows.HorizontalAlignment.Center;
            canbackground.VerticalAlignment = System.Windows.VerticalAlignment.Center;
            canbackground.RenderTransform = null;
            Canvas.SetLeft(GrdBrightness, 0);
            Canvas.SetTop(GrdBrightness, 0);
            GrdBrightness.RenderTransform = null;
            grdZoom.RenderTransform = null;
        }
        private void GetGraphicPosition()
        {
            foreach (var ctrl in dragCanvas.Children)
            {
                if (ctrl is Button)
                {
                    Button cid = (Button)ctrl;
                    double top1 = Canvas.GetTop(cid);
                    double left1 = Canvas.GetLeft(cid);
                    string source = ((System.Windows.Controls.Image)(cid.Content)).Source.ToString();
                    int segment = source.Split('/').Count();
                    source = source.Split('/')[segment - 1].ToString();

                    foreach (var logo in LogoControlBox.LogoSlotList)
                    {
                        if (logo.FilePath.Contains(source))
                        {
                            logo.Top = (int)top1;
                            logo.Left = (int)left1;
                        }
                    }
                }
            }
        }
        #endregion

        private void btnRemoveMarkGraphic_Click(object sender, RoutedEventArgs e)
        {
            TemplateListItems tmpList = objTemplateList.Where(o => o.MediaType == 601).FirstOrDefault();
            if (tmpList != null)
            {
                ClearImageSourceControl();
                isImageAsGraphic = false;
                objTemplateList.Remove(tmpList);
            }
        }

        private void HighlightSelectedButton(string btName)
        {
            if (btName == "btnVideo")
                btnVideo.IsEnabled = false;
            else
                btnVideo.IsEnabled = true;
            //if (btName == "btnAudio")
            //    btnAudio.IsEnabled = false;
            //else
            //    btnAudio.IsEnabled = true;
            if (btName == "btnChroma")
                btnChroma.IsEnabled = false;
            else
                btnChroma.IsEnabled = true;
            if (btName == "btnCapture")
                btnCapture.IsEnabled = false;
            else
                btnCapture.IsEnabled = true;
            if (btName == "btnColorEffects")
                btnColorEffects.IsEnabled = false;
            else
                btnColorEffects.IsEnabled = true;
        }

        VideoColorEffects colorEffects;
        private void LoadSceneSettingsBasedOnImage(LstMyItems ImgDetail)
        {
            ConfigBusiness objConfigBL = new ConfigBusiness();
            VideoSceneViewModel VideoScene = new VideoSceneViewModel();
            VideoScene = objConfigBL.GetVideoSceneBasedOnPhotoId(ImgDetail.PhotoId);
            if (VideoScene != null)
            {
                try
                {
                    string sceneFilePath = System.IO.Path.Combine(ConfigManager.DigiFolderPath, VideoScene.VideoScene.ScenePath);
                    Vidlength = VideoScene.VideoScene.VideoLength;
                    IsVerticalVideo = VideoScene.VideoScene.IsVerticalVideo;
                    m_pMPersist = (MPLATFORMLib.IMPersist)m_objMixer;
                    m_pMPersist.PersistLoad("", sceneFilePath, "");
                    mMixerList1.UpdateList(true, 1);
                    mElementsTree1.UpdateTree(false);
                    GetGuestElement();
                    LoadFirstItemOnMixer();
                    LoadChromaAndCGConfig(VideoScene.VideoScene.Name, VideoScene.VideoScene.CG_ConfigID);
                    GetAudioListToBeApplied(VideoScene.VideoScene.Name);
                    CheckTemplateItems();
                    IsSceneLoaded = true;
                    colorEffects = FrameworkHelper.Common.MLHelpers.ReadColorXml(System.IO.Path.Combine(ConfigManager.DigiFolderPath, "Profiles", VideoScene.VideoScene.Name, "Color.xml"));
                    if (colorEffects != null)
                    {
                        LoadValuestoColorControls(colorEffects);
                        // Thread.Sleep(500);
                    }
                    else
                        MessageBox.Show("File does not Exists!");
                }
                catch (Exception ex)
                {
                    string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                    ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                }
            }
            else
            {
                MessageBox.Show("No scene settings found for the selected image/video!\nDefault scene settings will be loaded.", "DigiPhoto", MessageBoxButton.OK, MessageBoxImage.Exclamation);
            }
        }


        private void CheckTemplateItems()
        {
            try
            {
                //Background
                MItem myItem;
                string name = string.Empty;
                m_objMixer.StreamsBackgroundGet(out name, out myItem);
                if (!string.IsNullOrEmpty(name))
                {
                    string name1 = System.IO.Path.GetFileName(name);
                    TemplateListItems item = objTemplateList.Where(o => o.MediaType == 605 && o.Name.Contains(name1)).FirstOrDefault();
                    if (item != null)
                    {
                        item.IsChecked = true;
                    }
                }


                //Overlay element
                OverlayElement.AttributesStringGet("stream_id", out name);
                if (!string.IsNullOrEmpty(name))
                {
                    int indOveray; myItem = null;
                    m_objMixer.StreamsGet(name, out indOveray, out myItem);
                    if (myItem != null)
                    {
                        string videOverlayName = string.Empty;
                        myItem.FileNameGet(out videOverlayName);
                        videOverlayName = System.IO.Path.GetFileName(videOverlayName);
                        TemplateListItems item = objTemplateList.Where(o => o.MediaType == 609 && o.Name.Contains(videOverlayName)).FirstOrDefault();
                        if (item != null)
                        {
                            int have = 0;
                            string value;
                            OverlayElement.AttributesHave("show", out have, out value);
                            if (value == "true")
                            {
                                item.IsChecked = true;
                                OverlayElement.ElementReorder(500);
                                StackOverlayChroma.Visibility = Visibility.Visible;
                            }
                        }
                    }
                }
                //Border element
                BorderElement.AttributesStringGet("stream_id", out name);
                int ind; myItem = null;
                m_objMixer.StreamsGet(name, out ind, out myItem);
                if (myItem != null)
                {
                    string bordername = string.Empty;
                    myItem.FileNameGet(out bordername);
                    bordername = System.IO.Path.GetFileName(bordername);
                    TemplateListItems item = objTemplateList.Where(o => o.MediaType == 608 && o.FilePath.Contains(bordername)).FirstOrDefault();
                    if (item != null)
                    {
                        int have = 0;
                        string value;
                        BorderElement.AttributesHave("show", out have, out value);
                        if (value == "true")
                        {
                            item.IsChecked = true;
                            BorderElement.ElementReorder(1000);
                        }
                    }
                }

                lstTemplates.ItemsSource = null;

                List<TemplateListItems> TLI = objTemplateList.Where(o => o.MediaType == 605 && o.isActive == true).ToList();
                lstTemplates.ItemsSource = TLI;
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }

        #region Commented code
        //private void LoadVideoEffects(string effects)
        //{
        //    try
        //    {
        //        XmlDocument xmlDoc = new XmlDocument();
        //        xmlDoc.LoadXml(effects);
        //        #region General Effect
        //        //Lightness
        //        if (!string.IsNullOrEmpty(xmlDoc.GetElementsByTagName("lightness")[0].InnerText))
        //        {
        //            GeneralEffectsBox.tbLightness.Value = Convert.ToDouble(xmlDoc.GetElementsByTagName("lightness")[0].InnerText);
        //        }
        //        //Saturation
        //        if (!string.IsNullOrEmpty(xmlDoc.GetElementsByTagName("saturation")[0].InnerText))
        //        {
        //            GeneralEffectsBox.tbSaturation.Value = Convert.ToDouble(xmlDoc.GetElementsByTagName("saturation")[0].InnerText);
        //        }
        //        //Contrast
        //        if (!string.IsNullOrEmpty(xmlDoc.GetElementsByTagName("contrast")[0].InnerText))
        //        {
        //            GeneralEffectsBox.tbContrast.Value = Convert.ToDouble(xmlDoc.GetElementsByTagName("contrast")[0].InnerText);
        //        }
        //        //Darkness
        //        if (!string.IsNullOrEmpty(xmlDoc.GetElementsByTagName("darkness")[0].InnerText))
        //        {
        //            GeneralEffectsBox.tbDarkness.Value = Convert.ToDouble(xmlDoc.GetElementsByTagName("darkness")[0].InnerText);
        //        }
        //        //Gray Scale
        //        if (!string.IsNullOrEmpty(xmlDoc.GetElementsByTagName("greyScale")[0].InnerText))
        //        {
        //            GeneralEffectsBox.chkGrayScale.IsChecked = Convert.ToBoolean(xmlDoc.GetElementsByTagName("greyScale")[0].InnerText);
        //        }
        //        //Invert
        //        if (!string.IsNullOrEmpty(xmlDoc.GetElementsByTagName("invert")[0].InnerText))
        //        {
        //            GeneralEffectsBox.chkInvert.IsChecked = Convert.ToBoolean(xmlDoc.GetElementsByTagName("invert")[0].InnerText);
        //        }

        //        #endregion General Effect
        //        #region ChromaEffect

        //        if (xmlDoc.GetElementsByTagName("chroma")[0].InnerText == "True")
        //        {
        //            string chromaColor = xmlDoc.GetElementsByTagName("chroma")[0].Attributes["chromaKeyColor"].InnerText;
        //            int chromaBGId = Convert.ToInt32(xmlDoc.GetElementsByTagName("chroma")[0].Attributes["chromaBackgroundId"].InnerText);
        //            //switch (chromaColor)
        //            //{
        //            //    //case "Red":
        //            //    //    rdbRed.IsChecked = true;
        //            //    //    break;
        //            //    //case "Green":
        //            //    //    rdbGreen.IsChecked = true;
        //            //    //    break;
        //            //    //case "Blue":
        //            //    //    rdbBlue.IsChecked = true;
        //            //    //    break;
        //            //}
        //            TemplateListItems tempItem = objTemplateList.Where(o => o.MediaType == 605 && o.Item_ID == chromaBGId).FirstOrDefault();
        //            if (tempItem != null)
        //            {
        //                tempItem.IsChecked = true;
        //            }

        //        }
        //        #endregion
        //        #region Text Logo
        //        ////TextLogo objTextLogo = new TextLogo();
        //        ////if (!string.IsNullOrEmpty(xmlDoc.GetElementsByTagName("textLogo")[0].Attributes["textLogoPosition"].InnerText))
        //        ////{
        //        ////    string logoPosition = xmlDoc.GetElementsByTagName("textLogo")[0].Attributes["textLogoPosition"].InnerText;
        //        ////    objTextLogo.TextLogoText = xmlDoc.GetElementsByTagName("textLogo")[0].InnerText;
        //        ////    System.Windows.Forms.FontDialog fontDialog = new System.Windows.Forms.FontDialog();

        //        ////    if (logoPosition.Contains(','))
        //        ////    {
        //        ////        objTextLogo.TextLogoTop = logoPosition.Split(',')[0];
        //        ////        objTextLogo.TextLogoleft = logoPosition.Split(',')[1];
        //        ////    }
        //        ////    //textfontName
        //        ////    if (!string.IsNullOrEmpty(xmlDoc.GetElementsByTagName("textLogo")[0].Attributes["textfontName"].InnerText))
        //        ////    {
        //        ////        string fontName = xmlDoc.GetElementsByTagName("textLogo")[0].Attributes["textfontName"].InnerText;
        //        ////        float fontSize = 20;
        //        ////        string fontStyle = "";
        //        ////        //textfontSize
        //        ////        if (!string.IsNullOrEmpty(xmlDoc.GetElementsByTagName("textLogo")[0].Attributes["textfontSize"].InnerText))
        //        ////        {
        //        ////            fontSize = float.Parse(xmlDoc.GetElementsByTagName("textLogo")[0].Attributes["textfontSize"].InnerText);
        //        ////        }
        //        ////        //textfontStyle
        //        ////        if (!string.IsNullOrEmpty(xmlDoc.GetElementsByTagName("textLogo")[0].Attributes["textfontStyle"].InnerText))
        //        ////        {
        //        ////            fontStyle = xmlDoc.GetElementsByTagName("textLogo")[0].Attributes["textfontStyle"].InnerText;
        //        ////        }
        //        ////        if (!string.IsNullOrEmpty(xmlDoc.GetElementsByTagName("textLogo")[0].Attributes["applyTextLogo"].InnerText))
        //        ////        {
        //        ////            objTextLogo.IsTextLogoEnabled = Convert.ToBoolean(xmlDoc.GetElementsByTagName("textLogo")[0].Attributes["applyTextLogo"].InnerText);
        //        ////        }
        //        ////        //applyTextLogo
        //        ////        if (fontStyle.Contains(','))
        //        ////        {
        //        ////            fontStyle = fontStyle.Split(',')[0];
        //        ////        }
        //        ////        switch (fontStyle)
        //        ////        {
        //        ////            case "Bold":
        //        ////                fontDialog.Font = new System.Drawing.Font(fontName, fontSize, System.Drawing.FontStyle.Bold);
        //        ////                break;
        //        ////            case "Italic":
        //        ////                fontDialog.Font = new System.Drawing.Font(fontName, fontSize, System.Drawing.FontStyle.Italic);
        //        ////                break;
        //        ////            case "Underline":
        //        ////                fontDialog.Font = new System.Drawing.Font(fontName, fontSize, System.Drawing.FontStyle.Underline);
        //        ////                break;
        //        ////            default:
        //        ////                fontDialog.Font = new System.Drawing.Font(fontName, fontSize, System.Drawing.FontStyle.Regular);
        //        ////                break;
        //        ////        }
        //        ////    }
        //        ////    //textfontColor
        //        ////    if (!string.IsNullOrEmpty(xmlDoc.GetElementsByTagName("textLogo")[0].Attributes["textfontColor"].InnerText))
        //        ////    {
        //        ////        string fontColor = xmlDoc.GetElementsByTagName("textLogo")[0].Attributes["textfontColor"].InnerText;
        //        ////    }
        //        ////    objTextLogo.TextFontName = fontDialog.Font;
        //        ////    objTextLogo.TextFontColor = fontDialog.Color;
        //        ////}
        //        #endregion Text Logo

        //        #region Add Borders
        //        //Add borders
        //        XmlNodeList BordersList = xmlDoc.GetElementsByTagName("Borders");
        //        if (BordersList.Count > 0)
        //        {
        //            // borderSlotPropLst.Clear();
        //            int id = 151;
        //            XmlNodeList xmlnode = xmlDoc.GetElementsByTagName("Border");
        //            for (int i = 0; i <= xmlnode.Count - 1; i++)
        //            {
        //                SlotProperty borderlist = new SlotProperty();
        //                borderlist.ID = id++;
        //                borderlist.ItemID = Convert.ToInt32(xmlnode[i].ChildNodes.Item(0).InnerText);
        //                borderlist.FilePath = ConfigManager.DigiFolderPath + "Frames" + "\\Thumbnails\\" + xmlnode[i].ChildNodes.Item(1).InnerText;
        //                borderlist.StartTime = Convert.ToInt32(xmlnode[i].ChildNodes.Item(2).InnerText);
        //                borderlist.StopTime = Convert.ToInt32(xmlnode[i].ChildNodes.Item(3).InnerText);
        //                TemplateListItems tempBorder = objTemplateList.Where(o => o.MediaType == 608 && o.Item_ID == borderlist.ItemID).FirstOrDefault();
        //                if (tempBorder != null)
        //                {
        //                    tempBorder.IsChecked = true;
        //                    tempBorder.StartTime = borderlist.StartTime;
        //                    tempBorder.EndTime = borderlist.StopTime;
        //                    tempBorder.InsertTime = 0;
        //                }
        //                BorderControl.borderslotlist.Add(borderlist);
        //                //  borderSlotPropLst.Add(borderlist);
        //            }
        //            BorderControl.ControlListAutoFill();
        //        }

        //        #endregion Add Borders
        //        #region Add Graphic
        //        //Add Graphic
        //        XmlNodeList graphicList = xmlDoc.GetElementsByTagName("graphics");
        //        if (graphicList.Count > 0)
        //        {
        //            //logoSlotPropLst.Clear();
        //            int uniqueID = 51;
        //            XmlNodeList xmlnode = xmlDoc.GetElementsByTagName("graphic");
        //            for (int i = 0; i <= xmlnode.Count - 1; i++)
        //            {
        //                SlotProperty logoprop = new SlotProperty();
        //                logoprop.ID = uniqueID++;
        //                logoprop.ItemID = Convert.ToInt32(xmlnode[i].ChildNodes.Item(0).InnerText);
        //                logoprop.FilePath = ConfigManager.DigiFolderPath + "Graphics\\" + xmlnode[i].ChildNodes.Item(1).InnerText;
        //                logoprop.StartTime = Convert.ToInt32(xmlnode[i].ChildNodes.Item(2).InnerText);
        //                logoprop.StopTime = Convert.ToInt32(xmlnode[i].ChildNodes.Item(3).InnerText);
        //                logoprop.Top = Convert.ToInt32(xmlnode[i].ChildNodes.Item(4).InnerText);
        //                logoprop.Left = Convert.ToInt32(xmlnode[i].ChildNodes.Item(5).InnerText);
        //                if (xmlnode[i].ChildNodes.Count > 6)
        //                {
        //                    logoprop.settings = xmlnode[i].ChildNodes.Item(6).OuterXml;
        //                }
        //                //  logoSlotPropLst.Add(logoprop);
        //                LogoControlBox.LogoSlotList.Add(logoprop);


        //                TemplateListItems tempItem = objTemplateList.Where(o => o.MediaType == 606 && o.Item_ID == logoprop.ItemID).FirstOrDefault();
        //                if (tempItem != null)
        //                {
        //                    tempItem.IsChecked = true;
        //                    tempItem.StartTime = logoprop.StartTime;
        //                    tempItem.EndTime = logoprop.StopTime;
        //                    tempItem.TopPositon = logoprop.Top;
        //                    tempItem.LeftPositon = logoprop.Left;
        //                    tempItem.InsertTime = 0;
        //                }
        //            }
        //            //LogoControlBox
        //        }

        //        #endregion Add Graphic

        //        #region Add Audio
        //        XmlNodeList audioList = xmlDoc.GetElementsByTagName("audios");
        //        if (audioList.Count > 0)
        //        {
        //            //  audioSlotPropLst.Clear();
        //            int uniqueID = 1;
        //            XmlNodeList xmlnode = xmlDoc.GetElementsByTagName("audio");
        //            for (int i = 0; i <= xmlnode.Count - 1; i++)
        //            {
        //                SlotProperty audioprop = new SlotProperty();
        //                audioprop.ID = uniqueID++;
        //                int ItemID = Convert.ToInt32(xmlnode[i].ChildNodes.Item(0).InnerText);
        //                TemplateListItems tempItem = objTemplateList.Where(o => o.MediaType == 604 && o.Item_ID == ItemID).FirstOrDefault();
        //                if (tempItem != null)
        //                {
        //                    tempItem.IsChecked = true;
        //                    tempItem.StartTime = Convert.ToInt32(xmlnode[i].ChildNodes.Item(3).InnerText);
        //                    tempItem.EndTime = Convert.ToInt32(xmlnode[i].ChildNodes.Item(4).InnerText);
        //                    tempItem.InsertTime = Convert.ToInt32(xmlnode[i].ChildNodes.Item(5).InnerText);
        //                }
        //            }
        //        }
        //        #endregion Add Audio

        //        #region Video Template
        //        XmlNodeList videoTempList = xmlDoc.GetElementsByTagName("videoTemplates");
        //        if (videoTempList.Count > 0)
        //        {
        //            XmlNodeList xmlnode = xmlDoc.GetElementsByTagName("videoTemplate");
        //            for (int i = 0; i <= xmlnode.Count - 1; i++)
        //            {
        //                int ItemID = Convert.ToInt32(xmlnode[i].ChildNodes.Item(0).InnerText);
        //                TemplateListItems tempItem = objTemplateList.Where(o => o.MediaType == 603 && o.Item_ID == ItemID).FirstOrDefault();
        //                if (tempItem != null)
        //                {
        //                    tempItem.IsChecked = true;
        //                    tempItem.StartTime = Convert.ToInt32(xmlnode[i].ChildNodes.Item(3).InnerText);
        //                    tempItem.EndTime = Convert.ToInt32(xmlnode[i].ChildNodes.Item(4).InnerText);
        //                }
        //                MediaStop();
        //                string filePath = LoginUser.DigiFolderVideoTemplatePath + System.IO.Path.GetFileName(tempItem.Name);
        //                vsMediaFileName = filePath;
        //                MediaPlay();
        //            }
        //        }
        //        #endregion Video Template
        //        #region ProductId and VidLength
        //       

        //        XmlNodeList VideoLengthList = xmlDoc.GetElementsByTagName("VideoLength");
        //        if (VideoLengthList.Count > 0)
        //        {
        //            Vidlength = Convert.ToInt32(VideoLengthList[0].InnerText);
        //        }
        //        #endregion ProductId and VidLength

        //        #region LayeringEffect

        //        XmlNodeList leringXML = xmlDoc.GetElementsByTagName("photo");
        //        if (leringXML.Count > 0)
        //        {
        //            objPhotoLayer.canvasLeft = Convert.ToDouble(xmlDoc.GetElementsByTagName("photo")[0].Attributes["canvasleft"].InnerText);
        //            objPhotoLayer.canvasTop = Convert.ToDouble(xmlDoc.GetElementsByTagName("photo")[0].Attributes["canvastop"].InnerText);
        //            objPhotoLayer.zoomfactor = Convert.ToDouble(xmlDoc.GetElementsByTagName("photo")[0].Attributes["zoomfactor"].InnerText);
        //            objPhotoLayer.scaleCenterX = xmlDoc.GetElementsByTagName("photo")[0].Attributes["scalecenterx"].InnerText;
        //            objPhotoLayer.scaleCenterY = xmlDoc.GetElementsByTagName("photo")[0].Attributes["scalecentery"].InnerText;
        //        }
        //        #endregion LayeringEffect

        //    }
        //    catch (Exception ex)
        //    {
        //        ErrorHandler.ErrorHandler.LogFileWrite(DateTime.Now.ToString() + "Load video effects: '" + ex.Message);

        //    }
        //}

        //private void ApplyZoomAndPosition(PhotoLayering objlayer)
        //{
        //    Canvas.SetLeft(GrdBrightness, 0);
        //    Canvas.SetTop(GrdBrightness, 0);
        //    GrdBrightness.RenderTransform = null;
        //    //  grdZoomCanvas.RenderTransform = null;
        //    double translateX = 0;
        //    double translateY = 0;
        //    TransformGroup transformGroup = new TransformGroup();

        //    Canvas.SetLeft(GrdBrightness, objlayer.canvasLeft);
        //    Canvas.SetTop(GrdBrightness, objlayer.canvasTop);
        //    string _centerX = objlayer.scaleCenterX;
        //    string _centerY = objlayer.scaleCenterY;
        //    double _ZoomFactor = objlayer.zoomfactor;
        //    if (_centerX != "-1")
        //    {
        //        ScaleTransform st = new ScaleTransform();
        //        st.CenterX = Convert.ToDouble(_centerX);
        //        st.CenterY = Convert.ToDouble(_centerY);
        //        if (_ZoomFactor != -1)
        //        {
        //            st.ScaleX = _ZoomFactor;
        //            st.ScaleY = _ZoomFactor;
        //        }
        //        zoomTransform = st;
        //        transformGroup.Children.Add(st);
        //    }
        //    if (translateX != 0)
        //    {
        //        TranslateTransform st = new TranslateTransform();
        //        st.X = Convert.ToDouble(translateX);
        //        st.Y = Convert.ToDouble(translateY);
        //        transformGroup.Children.Add(st);
        //    }
        //    // loadGraphic(graphicList);
        //    if (GrdBrightness.RenderTransform == null)
        //        GrdBrightness.RenderTransform = transformGroup;
        //    // Zomout(true);
        //}
        //public void Zomout(bool orignal)
        //{
        //    try
        //    {
        //        // first = false;
        //        grdsize.UpdateLayout();
        //        double currentwidth = mainImagesize.Source.Width;
        //        double currentheight = mainImagesize.Source.Height;

        //        double ratiowidth = currentwidth / 600;
        //        double ratioheight = currentheight / 600;
        //        ratiowidth = 100 / ratiowidth / 100;
        //        ratioheight = 100 / ratioheight / 100;

        //        if (frm.Children.Count == 1)
        //        {
        //            currentwidth = GrdBrightness.Width;
        //            currentheight = GrdBrightness.Height;
        //            ratiowidth = currentwidth / 600;
        //            ratioheight = currentheight / 600;
        //            ratiowidth = 100 / ratiowidth / 100;
        //            ratioheight = 100 / ratioheight / 100;
        //        }

        //        ScaleTransform zoomTransform = new ScaleTransform(); ;
        //        TransformGroup transformGroup = new TransformGroup();
        //        if (currentheight > currentwidth)
        //        {
        //            zoomTransform.ScaleX = ratioheight - .01;
        //            zoomTransform.ScaleY = ratioheight - .01;
        //        }

        //        if (currentheight < currentwidth)
        //        {
        //            zoomTransform.ScaleX = ratiowidth - .01;
        //            zoomTransform.ScaleY = ratiowidth - .01;
        //        }
        //        zoomTransform.CenterX = GrdBrightness.ActualWidth / 2;
        //        zoomTransform.CenterY = GrdBrightness.ActualHeight / 2;
        //        transformGroup.Children.Add(zoomTransform);
        //        if (orignal)
        //        {
        //            MyInkCanvas.RenderTransform = null;

        //            canbackground.LayoutTransform = transformGroup;
        //        }
        //        else
        //        {

        //            Grdcartoonize.RenderTransform = null;
        //            MyInkCanvas.RenderTransform = transformGroup;

        //            MyInkCanvas.DefaultDrawingAttributes.StylusTipTransform = new Matrix(zoomTransform.ScaleX, 0, 0, zoomTransform.ScaleY, 0, 0);

        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
        //        ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
        //    }
        //    finally
        //    {
        //        //DigiPhoto.MemoryManagement.FlushMemory();
        //    }
        //}
        //private void loadGraphic(List<SlotProperty> graphicList)
        //{
        //    try
        //    {
        //        foreach (var item in graphicList)
        //        {
        //            if (System.IO.Path.GetExtension(item.FilePath) != ".gif")
        //            {
        //                XmlDocument xmlDoc = new XmlDocument();
        //                xmlDoc.LoadXml(item.settings);

        //                double zoomfactor = Convert.ToDouble(xmlDoc.GetElementsByTagName("settings")[0].Attributes["zoomfactor"].InnerText);
        //                double wthsource = Convert.ToDouble(xmlDoc.GetElementsByTagName("settings")[0].Attributes["wthsource"].InnerText);
        //                double scalex = Convert.ToDouble(xmlDoc.GetElementsByTagName("settings")[0].Attributes["scalex"].InnerText);
        //                double scaley = Convert.ToDouble(xmlDoc.GetElementsByTagName("settings")[0].Attributes["scaley"].InnerText);
        //                double angle = Convert.ToDouble(xmlDoc.GetElementsByTagName("settings")[0].Attributes["angle"].InnerText);

        //                Button btngrph = new Button();
        //                btngrph.HorizontalAlignment = System.Windows.HorizontalAlignment.Center;
        //                btngrph.VerticalAlignment = System.Windows.VerticalAlignment.Center;
        //                Style defaultStyle = (Style)FindResource("ButtonStyleGraphic");
        //                btngrph.Style = defaultStyle;
        //                System.Windows.Controls.Image imgctrl = new System.Windows.Controls.Image();
        //                BitmapImage img = new BitmapImage(new Uri(item.FilePath));
        //                imgctrl.Name = "A" + Guid.NewGuid().ToString().Split('-')[0].ToString();
        //                btngrph.Name = "btn" + Guid.NewGuid().ToString().Split('-')[0].ToString();
        //                btngrph.Uid = "uid" + Guid.NewGuid().ToString().Split('-')[0].ToString();
        //                imgctrl.Source = img;
        //                btngrph.Width = 90;
        //                btngrph.Height = 90;

        //                btngrph.Content = imgctrl;
        //                dragCanvas.Children.Add(btngrph);
        //                var left = String.Format("{0:0.00}", Convert.ToDouble(item.Left));
        //                var top = String.Format("{0:0.00}", Convert.ToDouble(item.Top));
        //                Canvas.SetLeft(btngrph, Convert.ToDouble(left));
        //                Canvas.SetTop(btngrph, Convert.ToDouble(top));

        //                Double ZoomFactor = zoomfactor;
        //                btngrph.Width = wthsource != 0 ? wthsource : 90;
        //                btngrph.Height = wthsource != 0 ? wthsource : 90;
        //                btngrph.Tag = ZoomFactor.ToString();
        //                TransformGroup tg = new TransformGroup();
        //                //RotateTransform rotation = new RotateTransform();
        //                ScaleTransform scale = new ScaleTransform();
        //                scale.CenterX = 0;
        //                scale.CenterY = 0;
        //                if (scalex != 0)
        //                {
        //                    scale.ScaleX = scalex;
        //                }
        //                if (scaley != 0)
        //                {
        //                    scale.ScaleY = scaley;
        //                }

        //                btngrph.RenderTransformOrigin = new System.Windows.Point(0.5, 0.5);
        //                tg.Children.Add(scale);
        //                //rotation.CenterX = 0;
        //                //rotation.CenterY = 0;
        //                //rotation.Angle = angle;
        //                //btngrph.RenderTransformOrigin = new System.Windows.Point(0.5, 0.5);
        //                //tg.Children.Add(rotation);
        //                btngrph.RenderTransform = tg;
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
        //        ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
        //    }
        //}
        //



        //===================MediaLooks code====================
        #endregion Commented code

        MMixerClass m_objMixer;
        private MPLATFORMLib.IMPersist m_pMPersist;
        string _guestVideoObject;
        IMStreams m_pMixerStreams;
        DispatcherTimer VidTimer;
        public MWriterClass m_objWriter;
        //////////////////////////////////////////////////////////////////////////
        // For update seeking control (all files / selected file)

        void mMixerList1_OnMixerSelChanged(object sender, EventArgs e)
        {
            try
            {
                System.Windows.Forms.ListView listView = (System.Windows.Forms.ListView)sender;
                if (listView.SelectedItems.Count > 0)
                {
                    MCHROMAKEYLib.MChromaKey objChromaKey = GetChromakeyFilter(listView.SelectedItems[0]);
                }
            }
            catch { }
        }
        private MCHROMAKEYLib.MChromaKey GetChromakeyFilter(object source)
        {
            MCHROMAKEYLib.MChromaKey pChromaKey = null;
            try
            {
                int nCount = 0;
                IMPlugins pPlugins = (IMPlugins)source;
                pPlugins.PluginsGetCount(out nCount);
                for (int i = 0; i < nCount; i++)
                {
                    object pPlugin;
                    long nCBCookie;
                    pPlugins.PluginsGetByIndex(i, out pPlugin, out nCBCookie);
                    try
                    {
                        pChromaKey = (MCHROMAKEYLib.MChromaKey)pPlugin;
                        break;
                    }
                    catch { }
                }
            }
            catch { }
            return pChromaKey;
        }
        void mPersistControl1_OnLoad(object sender, EventArgs e)
        {
            mMixerList1.UpdateList(false);
            mElementsTree1.UpdateTree(false);
        }
        string overlayChromaSetting = string.Empty;
        private void btnProcess_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                MediaStop();
                thumCaptured = false;
                if (PrintOrderPageList.Where(o => o.Name != null && o.Name.Length > 0).OrderBy(o => o.PageNo).ToList().Count() > 0)
                {
                    stkbtnClosePreview.Visibility = Visibility.Collapsed;
                    //if (IsLoadChroma)
                    //{
                    //    MItem mitem; string stid = "";
                    //    m_objMixer.StreamsGetByIndex(0, out stid, out mitem);
                    //    CurrentchromaSetting = SaveChromaSetting(mitem);
                    //    Marshal.ReleaseComObject(mitem);
                    //    GC.Collect();
                    //}
                    //if (!string.IsNullOrEmpty(strOverlayStream) && IsLoadOverlayChroma)
                    //{
                    //    int id; MItem mitemOverlay = null;
                    //    m_objMixer.StreamsGet(strOverlayStream, out id, out mitemOverlay);
                    //    overlayChromaSetting = SaveChromaSetting(mitemOverlay);
                    //    Marshal.ReleaseComObject(mitemOverlay);
                    //    GC.Collect();
                    //}
                    IsExternalAudio = false;
                    btnStopProcess.IsEnabled = true;
                    NextAudioStartTime = 0;
                    NextItemStartTime = 0;
                    ClearAllControl();
                    UpdatePageListTime();

                    if (OverlayElement != null)
                    {
                        if (OverlayElement != null)
                            OverlayElement.ElementReorder(500);
                    }

                    if (BorderElement != null)
                        BorderElement.ElementReorder(1000);

                    AddInputFiles();
                    Thread.Sleep(500);
                    StartWriter();
                }
                else
                {
                    MessageBox.Show("Include guest images or video in processing list", "DigiPhoto", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    //MsgBox.ShowHandlerDialog("Include guest images or video in processing list.", DigiMessageBox.DialogType.OK);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }
        void UpdatePageListTime()
        {
            int Intime = 0;
            foreach (var item in PrintOrderPageList.Where(o => o.Name != null && o.Name.Length > 0).OrderBy(o => o.PageNo))
            {
                item.InsertTime = Intime;
                if (item.MediaType == 601)
                {
                    item.videoStartTime = Intime;
                    item.videoEndTime = Intime + item.ImageDisplayTime;
                    Intime = Intime + item.ImageDisplayTime;
                }
                else
                {
                    Intime = Intime + (item.videoEndTime - item.videoStartTime);
                }
            }
            pbProgress.Maximum = CalculateProcessedVideoLength();
            vidLength = (decimal)pbProgress.Maximum;
        }
        private void ClearAllControl()
        {
            try
            {
                lstStreamDetails.Clear();
                int streamCount;
                m_objMixer.StreamsGetCount(out streamCount);//
                try
                {
                    for (int i = 0; i < streamCount; i++)
                        m_objMixer.StreamsRemoveByIndex(0, 0, 0);
                }
                catch { }
                m_objWriter.ObjectClose();
                VideoEditPlayerPanel.Visibility = Visibility.Visible;
                vidoriginal.Visibility = Visibility.Collapsed;
                VideoEditPlayerPanel.IsEnabled = false;
                DisableControls();
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }
        void loadScene(int sceneId)
        {
            try
            {
                m_pMPersist = (MPLATFORMLib.IMPersist)m_objMixer;
                string filePath = ConfigManager.DigiFolderPath;
                if (AutomatedVideoEditWorkFlow)
                {
                    filePath = filePath + "Profiles\\DefaultProfiles\\StandardScene_overlay.xml";
                }
                else
                {
                    filePath = filePath + "Profiles\\DefaultProfiles\\StandardScene_concatenate.xml";
                }
                if (System.IO.File.Exists(filePath))
                {
                    m_pMPersist.PersistLoad("", filePath, "");
                    mMixerList1.UpdateList(true, 1);
                    mElementsTree1.UpdateTree(false);
                    m_objMixer.StreamsRemoveByIndex(0, 0, 0);
                    GetGuestElement();
                }
                else
                {
                    //btnStartProcess.IsEnabled = false;
                    MessageBox.Show("No video scene settings found!\n", "Digiphoto", MessageBoxButton.OK, MessageBoxImage.Information, MessageBoxResult.OK);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }

        private void AddInputFiles()
        {
            foreach (VideoPage objVP in PrintOrderPageList.Where(o => o.FilePath != "" && o.FilePath != null).OrderBy(o => o.InsertTime))
            {
                AddFile(objVP.DropVideoPath, objVP.videoStartTime, objVP.videoEndTime, objVP.InsertTime, objVP.MediaType);
            }

            foreach (TemplateListItems item in objTemplateList.Where(o => o.MediaType == 604 && o.IsChecked == true).OrderBy(o => o.InsertTime))
            {
                IsExternalAudio = true;
                MItem myBackground; string ss = string.Empty;
                m_objMixer.StreamsBackgroundGet(out ss, out myBackground);
                if (myBackground != null)
                    ((MPLATFORMLib.IMProps)myBackground).PropsSet("object::audio_gain", "-100"); //to mute the background video

                string audiopath = LoginUser.DigiFolderAudioPath + System.IO.Path.GetFileName(item.Name.ToString());
                AddFile(audiopath, item.StartTime, item.EndTime, item.InsertTime, 604);
            }
            foreach (TemplateListItems item in objTemplateList.Where(o => o.MediaType == 608 && o.IsChecked == true))//Border element
            {
                string audiopath = LoginUser.DigiFolderFramePath + System.IO.Path.GetFileName(item.Name.ToString());
                AddFile(audiopath, item.StartTime, item.EndTime, item.InsertTime, 608);
            }
            foreach (TemplateListItems item in objTemplateList.Where(o => o.MediaType == 609 && o.IsChecked == true))//Overlay Element
            {
                string path = LoginUser.DigiFolderVideoOverlayPath + System.IO.Path.GetFileName(item.Name.ToString());
                AddFile(path, item.StartTime, item.EndTime, item.InsertTime, 609);
            }
        }
        private string strTransition = "Pixelate";

        void AddFile(string filePath, double InTime, double OutTime, double InsertTime, int MediaType)
        {
            try
            {
                m_pMixerStreams = (IMStreams)m_objMixer;
                MItem pFile = null;
                string streamID = string.Empty;

                try
                {
                    //transition='iris'
                    // string strTrans = strTransition != "" ? " transition='" + strTransition + "'" : "";
                    m_pMixerStreams.StreamsAdd(streamID, null, filePath, "loop=false", out pFile, numericTimeForChange);
                    int count;
                    m_pMixerStreams.StreamsGetCount(out count);
                    m_pMixerStreams.StreamsGetByIndex(count - 1, out streamID, out pFile);
                    if (MediaType != 601)
                        InitializeStream(streamID, true);
                    StreamList st = new StreamList();
                    st.Index = count;
                    st.streamName = streamID;
                    st.Isprocessed = false;
                    st.In = InTime;
                    st.Out = OutTime;
                    st.InsertTime = InsertTime;
                    st.MediaType = MediaType;
                    lstStreamDetails.Add(st);
                    if (count == 1)
                    {
                        strGuestStreamID = streamID;
                    }
                    // Get new In/Out
                    double dblIn = MHelpers.ParsePos(InTime.ToString());
                    double dblOut = MHelpers.ParsePos(OutTime.ToString());
                    // Set new in-out
                    pFile.FileInOutSet(dblIn, dblOut);
                    if (IsLoadChroma || IsLoadOverlayChroma)
                    {
                        if ((!string.IsNullOrEmpty(CurrentchromaSetting) && (MediaType == 601 || MediaType == 602 || MediaType == 607)) || !string.IsNullOrEmpty(overlayChromaSetting) && (MediaType == 609))
                        {
                            //Applying Chroma 
                            if (MediaType == 609)
                                LoadChromaString(pFile, overlayChromaSetting);
                            else LoadChromaString(pFile, CurrentchromaSetting);
                        }
                    }
                }
                catch (Exception)
                {
                    MessageBox.Show("File " + filePath + " can't be added as stream");
                }
                try
                {
                    if (pFile != null)
                        mMixerList1.SelectFile(pFile);
                }
                catch { }
                mMixerList1.UpdateList(true, 1);
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }

        private void LoadChromaString(MItem pFile, string ChromaString)
        {
            MCHROMAKEYLib.MChromaKey objChromaKey = LoadChromaPlugin(true, pFile, 1);
            try
            {
                string GPUModeActive = string.Empty;
                ((MPLATFORMLib.IMProps)objChromaKey).PropsGet("gpu_mode", out GPUModeActive);
                if (!string.IsNullOrEmpty(GPUModeActive))
                {
                    if (GPUModeActive.ToLower().Equals("0") && !GPUModeActive.ToLower().Equals("true"))
                        ((MPLATFORMLib.IMProps)objChromaKey).PropsSet("gpu_mode", "true");
                }
            }
            catch (Exception ex) { }
            (objChromaKey as MPLATFORMLib.IMPersist).PersistLoad("", ChromaString, "");
            Marshal.ReleaseComObject(objChromaKey);
            Marshal.ReleaseComObject(pFile);
            GC.Collect();
        }
        private string GetStreamName(int index)
        {
            string stream = "stream-000";
            if (index > 0)
            {
                stream = "stream-00" + (index - 1).ToString();
            }
            return stream;
        }
        private MCHROMAKEYLib.MChromaKey LoadChromaPlugin(bool onloadChroma, object source, int a)
        {
            MCHROMAKEYLib.MChromaKey pChromaKey = null;
            try
            {
                //  if (mMixerList1.SelectedItem != null)
                {
                    IMPlugins pPlugins = (IMPlugins)source;
                    int nCount;
                    long nCBCookie;
                    object pPlugin = null;
                    bool isCK = false;
                    pPlugins.PluginsGetCount(out nCount);
                    for (int i = 0; i < nCount; i++)
                    {
                        pPlugins.PluginsGetByIndex(i, out pPlugin, out nCBCookie);

                        if (pPlugin.GetType().Name == "CoMChromaKeyClass" || pPlugin.GetType().Name == "MChromaKeyClass")
                        {
                            isCK = true;
                            break;
                        }
                    }
                    if ((isCK == false) || (onloadChroma && isCK == false))
                    {
                        pChromaKey = new MCHROMAKEYLib.MChromaKey();
                        pPlugins.PluginsAdd(pChromaKey, 0);
                    }
                    else if (isCK == true)
                    {
                        pPlugins.PluginsRemove(pPlugin);
                    }
                    //buttonChromaProps.IsEnabled = true;
                    //  Marshal.ReleaseComObject(pPlugins);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
            return pChromaKey;
        }
        #region writer settings
        string VideoSettings = string.Empty;
        private void LoadWriterSettings(string settings)
        {
            try
            {
                XmlDocument xdoc = new XmlDocument();
                xdoc.LoadXml(settings);
                XmlNodeList nodes = xdoc.GetElementsByTagName("Settings");
                if (nodes.Count > 0)
                {
                    foreach (XmlNode node in nodes)
                    {
                        int Videoid = Convert.ToInt32(node.ChildNodes[1].InnerText);
                        int Audioid = Convert.ToInt32(node.ChildNodes[3].InnerText);
                        string OpFormat = node.ChildNodes[4].InnerText;
                        string Videocodec = node.ChildNodes[6].InnerText.Trim();
                        string Audiocodec = node.ChildNodes[5].InnerText.Trim();
                        mFormatControl1.comboBoxVideo.SelectedIndex = Videoid;
                        mFormatControl1.comboBoxAudio.SelectedIndex = Audioid;

                        if (node.ChildNodes.Count >= 6)
                            VideoSettings = node.ChildNodes[7].InnerText.Trim();

                        (((MControls.ListViewEx)(mConfigList1)).Columns[1].ListView).Items[0].SubItems[1].Text = OpFormat;//o/p format
                        (((MControls.ListViewEx)(mConfigList1)).Columns[1].ListView).Items[1].SubItems[1].Text = Audiocodec;//audiocodec

                        if (Videocodec != "")
                        { (((MControls.ListViewEx)(mConfigList1)).Columns[1].ListView).Items[2].SubItems[1].Text = Videocodec; }//videocodec   }

                        IMAttributes pConfigProps;
                        m_objWriter.ConfigSet("format", OpFormat, out pConfigProps);
                        int bHave = 0; string strExtensions = string.Empty;
                        pConfigProps.AttributesHave("extensions", out bHave, out strExtensions);

                        string[] arr = strExtensions.Split(',');
                        outputFormat = arr[0]; //set output format
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }
        #endregion
        decimal vidLength = 0;
        string CurrentITemStream = string.Empty;
        double NextItemStartTime = 0;
        double NextAudioStartTime = 0; double CurrentAudioStoptTime = 0; string CurrentAudioStream = string.Empty;
        List<StreamList> lstStreamDetails = new List<StreamList>();
        bool IsExternalAudio = false;
        // decimal thumbnailTime = 0.50M;
        bool thumCaptured = false;
        private void VidTimer_Tick(object sender, EventArgs e)
        {
            decimal vidLengthNow = 0;
            UpdateStatus(m_objWriter, out vidLengthNow);
            eMState eState;
            m_objWriter.ObjectStateGet(out eState);
            pbProgress.Value = Convert.ToDouble(vidLengthNow);
            if (!thumCaptured && vidLengthNow > 0.50M)
            {
                ExtractThumbnail();
                thumCaptured = true;
            }
            if (vidLengthNow >= vidLength)
            {
                StopWriter(false);
            }
            else
            {
                if ((double)vidLengthNow >= NextItemStartTime)
                {
                    int inx = lstStreamDetails.Where(o => o.streamName == CurrentITemStream).FirstOrDefault().Index;
                    StreamList sl = lstStreamDetails.Where(o => o.Index == inx + 1 && o.MediaType != 604).FirstOrDefault();
                    if (sl != null)
                    {
                        string stname = sl.streamName;

                        if (sl.MediaType != 601)
                            InitializeStream(stname, false);

                        if (IsExternalAudio)
                            GuestElement.ElementMultipleSet("stream_id=" + stname + " audio_gain=-100 show=true", numericTimeForChange);
                        else
                            GuestElement.ElementMultipleSet("stream_id=" + stname + " audio_gain=1 show=true", numericTimeForChange);
                        CurrentITemStream = stname;
                        NextItemStartTime = sl.InsertTime + (sl.Out - sl.In);
                    }
                }
                if (NextAudioStartTime != 0 && (double)vidLengthNow >= NextAudioStartTime)
                {
                    if (!string.IsNullOrEmpty(CurrentAudioStream))
                    {
                        int inx = lstStreamDetails.Where(o => o.streamName == CurrentAudioStream).FirstOrDefault().Index;
                        StreamList sl = lstStreamDetails.Where(o => o.Index == inx + 1 && o.MediaType == 604).FirstOrDefault();
                        if (sl != null)
                        {
                            string stname = sl.streamName;
                            InitializeStream(stname, false);
                            AudioElement.ElementMultipleSet("stream_id=" + stname + " audio_gain=1", 0.0);
                            StreamList slAudioNext = lstStreamDetails.Where(o => o.MediaType == 604 && o.Index == sl.Index + 1).FirstOrDefault();
                            if (slAudioNext != null)
                                NextAudioStartTime = slAudioNext.InsertTime;
                            else NextAudioStartTime = 0;
                            CurrentAudioStream = stname;
                            CurrentAudioStoptTime = sl.InsertTime + (sl.Out - sl.In);
                        }
                    }
                }
                if (CurrentAudioStoptTime != 0 && (double)vidLengthNow >= CurrentAudioStoptTime)
                {
                    InitializeStream(CurrentAudioStream, true);
                }
            }
        }
        private string GetElementInformation(string strAttributes, string PropRequired)
        {
            string name, value = string.Empty;
            try
            {
                XmlDocument sdoc = new XmlDocument();
                sdoc.LoadXml(strAttributes);
                XmlNode xnode = sdoc.FirstChild;
                for (int i = 0; i < xnode.Attributes.Count; i++)
                {
                    name = Convert.ToString(xnode.Attributes[i].Name);
                    value = Convert.ToString(xnode.Attributes[i].Value);
                    if (name == PropRequired)
                        break;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
            return value;
        }
        private void GetElementInformation(string strAttributes)
        {
            try
            {
                Hashtable htAttrib = new Hashtable();
                XmlDocument sdoc = new XmlDocument();
                sdoc.LoadXml(strAttributes);
                XmlNode xnode = sdoc.FirstChild;
                for (int i = 0; i < xnode.Attributes.Count; i++)
                {
                    string name = Convert.ToString(xnode.Attributes[i].Name);
                    string value = Convert.ToString(xnode.Attributes[i].Value);
                    if (!htAttrib.ContainsKey(name))
                    {
                        htAttrib.Add(name, value);
                    }
                }
                if (htAttrib.Count > 0)
                {
                    PopulateProperties(htAttrib);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }
        private void InitializeStream(string streamId, bool isStop)
        {
            try
            {
                MItem m_pFileItem;
                int nCount = 0;
                m_objMixer.StreamsGet(streamId, out nCount, out m_pFileItem);
                if (m_pFileItem != null)
                {
                    m_pFileItem.FilePosSet(MHelpers.ParsePos("00:00:00"), 1.0);
                    if (isStop)
                        m_pFileItem.FilePlayStop(0);
                    else
                        m_pFileItem.FilePlayStart();
                }
            }
            catch (Exception ex)
            {
                //string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                //ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }
        MElement GuestElement;
        MElement AudioElement;
        MElement BorderElement;
        MElement OverlayElement;
        private void GetGuestElement()
        {
            MElement myElementRoute1 = null;
            try
            {
                string entry = string.Empty;
                m_objMixer.ElementsGetByIndex(0, out myElementRoute1);
                ((IMElements)myElementRoute1).ElementsGetByID("Guest_Video", out GuestElement);
                ((IMElements)myElementRoute1).ElementsGetByID("Audio_Video", out AudioElement);
                ((IMElements)myElementRoute1).ElementsGetByID("Border_Video", out BorderElement);
                ((IMElements)myElementRoute1).ElementsGetByID("Overlay_Video", out OverlayElement);

            }
            catch (Exception ex)
            {
            }
        }
        string UpdateStatus(MWriterClass pCapture, out decimal VidLength)
        {
            VidLength = 0; string strVidlen;
            string strRes = "";
            try
            {
                eMState eState;
                pCapture.ObjectStateGet(out eState);
                string strPid;
                m_objWriter.PropsGet("stat::last::server_pid", out strPid);
                string strSkip;
                pCapture.PropsGet("stat::skip", out strSkip);
                strRes = " State: " + eState + (strSkip != null ? " Skip rest:" + DblStrTrim(strSkip, 3) : "") + " Server PID:" + strPid + "\r\n";

                string sFile;
                pCapture.WriterNameGet(out sFile);
                strRes += " Path: " + sFile + "\r\n";


                {
                    strRes += "TOTAL:\r\n";
                    string strBuffer;
                    pCapture.PropsGet("stat::buffered", out strBuffer);
                    string strFrames;
                    pCapture.PropsGet("stat::frames", out strFrames);

                    string strFPS;
                    pCapture.PropsGet("stat::fps", out strFPS);

                    string strTime;
                    pCapture.PropsGet("stat::video_time", out strTime);

                    string strBreaks = "0";
                    pCapture.PropsGet("stat::breaks", out strBreaks);
                    string strDropped = "0";
                    pCapture.PropsGet("stat::frames_dropped", out strDropped);

                    m_objWriter.PropsGet("stat::video_len", out strVidlen);
                    VidLength = Convert.ToDecimal(strVidlen);
                    string strAVtime;
                    m_objWriter.PropsGet("stat::av_sync_time", out strAVtime);
                    string strAVlen;
                    m_objWriter.PropsGet("stat::av_sync_len", out strAVlen);
                    strRes += "  Buffers:" + strBuffer + " Frames:" + strFrames + " Fps:" + DblStrTrim(strFPS, 3) + " Dropped:" + strDropped +
                    " Breaks:" + strBreaks + "  Video time:" + DblStrTrim(strTime, 3) + "\r\n" + " Video Len:" + strVidlen + " AV Sync time:" +
                    strAVtime + " AV sync len:" + strAVlen + "\r\n";

                    string strSamples;
                    pCapture.PropsGet("stat::samples", out strSamples);
                    pCapture.PropsGet("stat::audio_time", out strTime);

                    strRes += "  Samples:" + strSamples + " Audio time:" + DblStrTrim(strTime, 3) + "\r\n";
                }
                {
                    strRes += "LAST:\r\n";

                    string strFrames;
                    pCapture.PropsGet("stat::last::frames", out strFrames);

                    string strFPS = "";
                    pCapture.PropsGet("stat::last::fps", out strFPS);

                    string strTime = "";
                    pCapture.PropsGet("stat::last::video_time", out strTime);

                    string strBreaks;
                    pCapture.PropsGet("stat::breaks", out strBreaks);

                    strRes += "  Frames:" + strFrames + " Breaks:" + strBreaks +
                        " Video time:" + DblStrTrim(strTime, 3) + "\r\n";

                    string strSamples;
                    pCapture.PropsGet("stat::last::samples", out strSamples);
                    pCapture.PropsGet("stat::last::audio_time", out strTime);

                    strRes += "  Samples:" + strSamples + " Audio time:" + DblStrTrim(strTime, 3);
                }
                return strRes;
            }
            catch (Exception ex)
            {
                return strRes;
            }
        }
        private void StopWriter(bool isbtnStop)
        {
            try
            {
                if (!isbtnStop)
                    StopAllInputFiles();
                VidTimer.Stop();
                m_objWriter.ObjectClose();
                pbProgress.Value = 0;
                EnableControls();
                btnStopProcess.IsEnabled = false;
                vsMediaFileName = tempFile;
                if (!isbtnStop)
                {
                    File.Copy(tempFile, processVideoTemp + "PlayerOutput." + outputFormat, true);
                    vsMediaFileName = processVideoTemp + "Output." + outputFormat;
                    MessageBox.Show("The video has been processed successfully!", "DigiPhoto", MessageBoxButton.OK, MessageBoxImage.Information);
                    if (AutomatedVideoEditWorkFlow)
                    {
                        btnSave_Click(null, null);
                    }
                    else
                    {
                        MediaPlay();
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }
        private void StopAllInputFiles()
        {
            MItem m_pFile; string streamId = string.Empty;
            int nCount = 0;
            m_objMixer.StreamsGetCount(out nCount);
            for (int i = 0; i < nCount; i++)
            {
                m_objMixer.StreamsGetByIndex(i, out streamId, out m_pFile);
                try
                {
                    if (m_pFile != null)
                    {
                        m_pFile.FilePlayStop(1.0);
                    }
                }
                catch
                {

                }
            }
        }
        string DblStrTrim(string str, int nPeriod)
        {
            if (str == null)
                return str = "0";
            int nDot = str.LastIndexOf('.');
            int nPeriodHave = nDot >= 0 ? str.Length - nDot - 1 : 0;
            if (nPeriodHave > nPeriod)
            {
                return str.Substring(0, nDot + nPeriod + 1);
            }
            // Add zeroes
            if (nDot < 0)
                str += ".";

            for (int i = nPeriodHave; i < nPeriod; i++)
            {
                str += "0";
            }
            return str;
        }
        MItem thisBackground;
        private void Initilizebackground()
        {
             string ss = string.Empty;
             m_objMixer.StreamsBackgroundGet(out ss, out thisBackground);
            if (thisBackground != null)
            {
                //myBackground.FilePosSet(MHelpers.ParsePos("00:00:00"), 1.0);
                thisBackground.FilePlayPause(0);
                thisBackground.FilePosSet(0, 0);
                thisBackground.FilePlayStart();
            }
        }
        private void StartWriter()
        {
            VidTimer.Start();
            mFormatControl1.SetControlledObjectForMWriter(m_objMixer);

            eMState eState;
            m_objWriter.ObjectStateGet(out eState);
            if (eState == eMState.eMS_Paused)
            {
                // Continue capture
                m_objWriter.ObjectStart(m_objMixer);
            }
            if (eState == eMState.eMS_Running)
            {
                m_objWriter.WriterNameSet("", "");
            }
            else
            {
                //First element in the list
                CurrentITemStream = "stream-000";
                StreamList sl = lstStreamDetails.Where(o => o.streamName == "stream-000").FirstOrDefault();
                NextItemStartTime = sl.InsertTime + (sl.Out - sl.In);
                InitializeStream(CurrentITemStream, false);
                if (IsExternalAudio)
                    GuestElement.ElementMultipleSet("stream_id=" + CurrentITemStream + " audio_gain=-100 show=true", numericTimeForChange);
                else
                    GuestElement.ElementMultipleSet("stream_id=" + CurrentITemStream + " audio_gain=1 show=true", numericTimeForChange);

                //First Audio element in the list
                StreamList slAudio = lstStreamDetails.Where(o => o.MediaType == 604).FirstOrDefault();
                if (slAudio != null)
                {
                    StreamList slAudioNext = lstStreamDetails.Where(o => o.MediaType == 604 && o.Index == slAudio.Index + 1).FirstOrDefault();
                    if (slAudioNext != null)
                        NextAudioStartTime = slAudioNext.InsertTime;
                    else NextAudioStartTime = 0;
                    CurrentAudioStream = slAudio.streamName;
                    CurrentAudioStoptTime = slAudio.InsertTime + (slAudio.Out - slAudio.In);
                    if (slAudio.InsertTime == 0)
                    {
                        AudioElement.ElementMultipleSet("stream_id=" + CurrentAudioStream + " audio_gain=1", 0.0);
                        InitializeStream(CurrentAudioStream, false);
                    }
                }
                //Border element in the list
                StreamList slBorder = lstStreamDetails.Where(o => o.MediaType == 608).FirstOrDefault();
                if (slBorder != null)
                {
                    BorderElement.ElementMultipleSet("stream_id=" + slBorder.streamName + " audio_gain=-100 show=true h=1 w=1 x=0 y=0", 0.0);
                    InitializeStream(slBorder.streamName, false);
                }
                //Overlay element in the list
                StreamList slOverlay = lstStreamDetails.Where(o => o.MediaType == 609).FirstOrDefault();
                if (slOverlay != null)
                {
                    OverlayElement.ElementMultipleSet("stream_id=" + slOverlay.streamName + " audio_gain=-100 show=true h=1 w=1 x=0 y=0", 0.0);
                    InitializeStream(slOverlay.streamName, false);
                    strOverlayStream = slOverlay.streamName;
                }
                
                Thread.Sleep(500);
                string strFormat;
                IMAttributes pFormatConfig;
                m_objWriter.ConfigGet("format", out strFormat, out pFormatConfig);
                tempFile = processVideoTemp + "Output" + "." + outputFormat;
                try
                {
                    //m_objWriter.WriterNameSet(tempFile, comboBoxProps.Text != "" ? comboBoxProps.Text : "video::bitrate=1M audio::bitrate=64K");
                    Initilizebackground();
                    InitializeAllInputFilesMixer();
                    m_objWriter.WriterNameSet(tempFile, VideoSettings != "" ? VideoSettings : "audio::bitrate=256K video::bitrate=30M video::codec=mpeg4 gop_size=1 qmin=1 qscale=1 v422=true");
                    m_objWriter.ObjectStart(m_objMixer);
                    m_objMixer.FilePlayStart();

                }
                catch (System.Exception ex)
                {
                    string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                    ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                    return;
                }
            }
        }
        private void InitializeAllInputFilesMixer()
        {
            MItem m_pFile; string streamId = string.Empty;
            int nCount = 0;
            m_objMixer.StreamsGetCount(out nCount);
            for (int i = 0; i < nCount; i++)
            {
                m_objMixer.StreamsGetByIndex(i, out streamId, out m_pFile);
                try
                {
                    if (m_pFile != null)
                    {
                        m_objMixer.FilePlayPause(0);
                        m_objMixer.FilePosSet(0, 0);
                    }
                }
                catch (Exception ex)
                {
                    string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                    ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                }
            }
        }
        private void ExtractThumbnail()
        {
            try
            {
                MFrame mf1;
                m_objMixer.ObjectFrameGet(out mf1, "");
                path_tempThumbnail = processVideoTemp + "Output.jpg";
                if (File.Exists(path_tempThumbnail))
                {
                    File.Delete(path_tempThumbnail);
                }
                mf1.FrameVideoSaveToFile(path_tempThumbnail);
                Marshal.ReleaseComObject(mf1);
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }

        }
        static MCHROMAKEYLib.MChromaKey objChromaKey;
        //  FormChromaKey formCK = new FormChromaKey();
        private void buttonChromaProps_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                MItem pFile; int ind = 0;
                string name;
                m_objMixer.StreamsGet(strGuestStreamID, out ind, out pFile);
                pFile.FileNameGet(out name);
                if (System.IO.Path.GetExtension(name).ToLower() != ".jpg" && System.IO.Path.GetExtension(name).ToLower() != ".png")
                    InitializeStream(strGuestStreamID, false);
                if (pFile != null)
                {

                    FrameworkHelper.Common.MLHelpers mh = new MLHelpers();
                    objChromaKey = mh.LoadChromaScreen(pFile, strGuestStreamID);

                    if (objChromaKey != null)
                    {
                        IsLoadChroma = true;
                        CurrentchromaSetting = SaveChromaSetting(pFile);
                    }
                }
                //string name;
                //pFile.FileNameGet(out name);
                //if (System.IO.Path.GetExtension(name).ToLower() != ".jpg" && System.IO.Path.GetExtension(name).ToLower() != ".png")
                //    InitializeStream(strGuestStreamID, false);
                //if (pFile != null)
                //{
                //    LoadChromaPlugin(true, pFile);
                //    Thread.Sleep(500);
                //    objChromaKey = GetChromakeyFilter(pFile);
                //    try
                //    {
                //        string GPUModeActive = string.Empty;
                //        ((MPLATFORMLib.IMProps)objChromaKey).PropsGet("gpu_mode", out GPUModeActive);
                //        if (!string.IsNullOrEmpty(GPUModeActive))
                //        {
                //            if (GPUModeActive.ToLower().Equals("0") && !GPUModeActive.ToLower().Equals("true"))
                //                ((MPLATFORMLib.IMProps)objChromaKey).PropsSet("gpu_mode", "true");
                //        }
                //    }
                //    catch { }
                //    Thread.Sleep(500);
                //    if (objChromaKey != null)
                //    {
                //        IsLoadChroma = true;
                //        FormChromaKey formCK = new FormChromaKey(objChromaKey);
                //        formCK.ShowDialog();
                //    }
                //}
                //Marshal.ReleaseComObject(pFile);
                //GC.Collect();
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }

        }
        //Load chroma 
        private void LoadChromaPlugin(bool onloadChroma, object source)
        {
            try
            {
                if (source != null)
                {
                    IMPlugins pPlugins = source as IMPlugins;
                    int nCount;
                    long nCBCookie;
                    object pPlugin = null;
                    bool isCK = false;
                    pPlugins.PluginsGetCount(out nCount);
                    for (int i = 0; i < nCount; i++)
                    {
                        pPlugins.PluginsGetByIndex(i, out pPlugin, out nCBCookie);

                        if (pPlugin.GetType().Name == "CoMChromaKeyClass" || pPlugin.GetType().Name == "MChromaKeyClass")
                        {
                            isCK = true;
                            break;
                        }
                    }
                    if ((isCK == false) || (onloadChroma && isCK == false))
                    {
                        pPlugins.PluginsAdd(new MCHROMAKEYLib.MChromaKey(), 0);
                    }
                    buttonChromaProps.IsEnabled = true;
                    //btnChromaSettings.IsEnabled = true;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }
        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                MediaStop();
                //  ClearImageSourceControl();
                //   int lenth = CalculateProcessedVideoLength();
                //  objProcVideoInfo.VideoLength = lenth;
                //  ClearVideoEditPlayer();
                lstVideoPage = PrintOrderPageList.ToList();
                bs.Show();
                bwSaveVideos.RunWorkerAsync();
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }
        private void bwSaveVideos_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            bs.Hide();
            ClearVideoEditPlayer();
            ShowVOS();
        }
        private void bwSaveVideos_DoWork(object sender, DoWorkEventArgs e)
        {
            if (File.Exists(tempFile))
            {
                saveOutputVideo();

            }
            else
                MessageBox.Show("Video not found!\n", "Video Editor", MessageBoxButton.OK, MessageBoxImage.Information, MessageBoxResult.OK);
        }
        private void saveOutputVideo()
        {
            try
            {

                VideoPage objpage = lstVideoPage.Where(o => o.MediaType != 0).OrderBy(o => o.PageNo).FirstOrDefault();
                if (objpage != null)
                {
                    PhotoBusiness photoBusiness = new PhotoBusiness();
                    string pathToSave = LoginUser.DigiFolderPath + "ProcessedVideos\\" + DateTime.Now.ToString("yyyyMMdd");
                    string outputfile = pathToSave + "\\" + objpage.Name + "_" + DateTime.Now.ToString("HH:mm").Replace(":", "") + "." + outputFormat;
                    string fileName = objpage.Name + "_" + DateTime.Now.ToString("HH:mm").Replace(":", "") + "." + outputFormat;
                    string thumbPath = System.IO.Path.Combine(LoginUser.DigiFolderPath, "Thumbnails", DateTime.Now.ToString("yyyyMMdd"));
                    if (!Directory.Exists(pathToSave))
                        Directory.CreateDirectory(pathToSave);
                    if (!Directory.Exists(thumbPath))
                        Directory.CreateDirectory(thumbPath);
                    if (File.Exists(tempFile))
                        File.Copy(tempFile, outputfile, true);
                    if (File.Exists(path_tempThumbnail))
                        ResizeWPFImage(path_tempThumbnail, 210, System.IO.Path.Combine(thumbPath, System.IO.Path.GetFileNameWithoutExtension(fileName) + ".jpg"));
                    int VideoId = 0;
                    PhotoCaptureInfo photoCaptureInfo = (new PhotoBusiness()).GetphotoCapturedetails(objpage.PhotoId).FirstOrDefault();
                    int locId = photoCaptureInfo.LocationId;
                    VideoId = photoBusiness.SetPhotoDetails(ConfigManager.SubStoreId, objpage.Name, fileName, DateTime.Now, LoginUser.UserId.ToString(), "", locId, string.Empty, string.Empty, null, null, 1, null, (long)vidLength, true, 3, 0, null, 0);
                    if (VideoId > 0)
                    {
                        VideoAssociation(objpage.PhotoId, VideoId);
                        MessageBox.Show("Video saved successfully!\n", "Video Editor", MessageBoxButton.OK, MessageBoxImage.Information, MessageBoxResult.OK);
                    }
                    //  _isVideoSaved = true;


                    if (isGroup)
                    {
                        //Save Processed Videos In group
                        LstMyItems _myitem = new LstMyItems();
                        _myitem.MediaType = 3;
                        _myitem.VideoLength = objProcVideoInfo.VideoLength;
                        _myitem.FilePath = thumbPath + "\\" + fileName.Replace(".avi", ".jpg").Replace(".mp4", ".jpg").Replace(".wmv", ".jpg");
                        _myitem.Name = objpage.Name;
                        _myitem.IsPassKeyVisible = Visibility.Collapsed;
                        _myitem.PhotoId = objpage.PhotoId;
                        _myitem.FileName = fileName;
                        _myitem.CreatedOn = DateTime.Now;
                        _myitem.IsLocked = Visibility.Visible;
                        _myitem.HotFolderPath = LoginUser.DigiFolderPath;
                        //if (RobotImageLoader.PrintImages.Count == 1)
                        //    RobotImageLoader.PrintImages.Clear();
                        if (RobotImageLoader.GroupImages.Count > 0)
                            _myitem.BmpImageGroup = RobotImageLoader.GroupImages[0].BmpImageGroup;
                        //_myitem.BmpImageGroup = new BitmapImage(new Uri(@"/images/view-accept.png", UriKind.Relative));
                        //_myitem.FrameBrdr = ((LstMyItems)(lstImages.SelectedItem)).FrameBrdr;
                        RobotImageLoader.GroupImages.Add(_myitem);
                        RobotImageLoader.ViewGroupedImagesCount.Add(_myitem.Name);
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }
        void VideoAssociation(int photoId, int videoId)
        {
            try
            {
                new AssociateImageBusiness().AssociateVideos(photoId, videoId);
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }
        //private void HideHandlerDialog()
        //{
        //    try
        //    {
        //        if (m_objMixer != null)
        //            m_objMixer.ObjectClose();
        //        if (m_objWriter != null) m_objWriter.ObjectClose();//              
        //        mMixerList1.ClearList();
        //        //  Marshal.FinalReleaseComObject(m_objComposer);
        //        Marshal.FinalReleaseComObject(m_objWriter);

        //    }
        //    catch (Exception ex)
        //    {
        //        string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
        //        ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
        //    }
        //}
        private void ShowVOS()
        {
            //bool isContinue = false;
            RobotImageLoader.RFID = "0";
            RobotImageLoader.SearchCriteria = "PhotoId";
            SearchResult window = null;
            foreach (Window wnd in Application.Current.Windows)
            {
                if (wnd.Title == "View/Order Station")
                {
                    window = (SearchResult)wnd;
                    break;
                }
            }
            if (window == null)
                window = new SearchResult();

            if (RobotImageLoader.GroupImages.Count == 0 && IsGoupped == "View All")
                IsGoupped = "View Group";
            window.pagename = "Saveback";
            // window.Savebackpid = Convert.ToString(photoId);
            window.Savebackpid = Convert.ToString(RobotImageLoader.PrintImages[RobotImageLoader.PrintImages.Count - 1].PhotoId);
            if (AutomatedVideoEditWorkFlow)
            {
                if (RobotImageLoader.PrintImages.Count == 1)
                {
                    RobotImageLoader.curItem = RobotImageLoader.robotImages.Where(t => t.PhotoId == RobotImageLoader.PrintImages.FirstOrDefault().PhotoId).First();
                    if (RobotImageLoader.curItem != null)
                        RobotImageLoader.curItem.PrintGroup = new BitmapImage(new Uri(@"/images/print-group.png", UriKind.Relative));
                    RobotImageLoader.PrintImages.Clear();
                    //window.pagename = string.Empty;
                }
            }
            window.Show();
            window.LoadWindow();
            this.Close();
        }
        private void btnStopProcess_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                StopWriter(true);
                EnableControls();
                GuestElement.ElementMultipleSet("stream_id=" + strGuestStreamID + " audio_gain=-100 show=true", 0.0);
                InitializeStream(strGuestStreamID, false);
                // GuestElement.ElementMultipleSet("show=true", 0.0);
            }
            catch (Exception ex)
            {

            }
        }

        #region Editor Events
        private void btnCGEditor_Click(object sender, RoutedEventArgs e)
        {
            RemovePlugins();
            LoadGraphicEditor();
        }
        private void RemovePlugins()
        {
            try
            {
                m_objCharGen.RemoveItem("", 1000);
                m_objMixer.PluginsRemove(m_objCharGen);
                Marshal.ReleaseComObject(m_objCharGen);

                m_strItemID = "";
                m_objCharGen = null;
            }
            catch (System.Exception ex)
            { }

        }
        private void RemoveColorPlugins()
        {
            try
            {
                m_objMixer.PluginsRemove(m_objColors);
            }
            catch (System.Exception ex)
            { }
        }
        private void cgEditor_FormClosed(object sender, System.Windows.Forms.FormClosedEventArgs e)
        {
            cgEditor.CGConfig -= SaveCGConfig;
            cgEditor.FormClosed -= cgEditor_FormClosed;
        }

        private void btnDelCGConfig_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                objConfigBL = new ConfigBusiness();
                if (cmbGraphicsSettings.SelectedIndex > 0)
                {
                    int configID = Convert.ToInt32(cmbGraphicsSettings.SelectedValue);
                    CGConfigSettings objCGConfig = GetCGConfigSettings(configID);
                    if (objCGConfig != null)
                    {
                        File.Delete(System.IO.Path.Combine(ConfigManager.DigiFolderPath, "CGSettings", objCGConfig.ConfigFileName + objCGConfig.Extension));
                        bool isDeleted = objConfigBL.DeleteCGConfigSettngs(configID);
                        if (isDeleted)
                            MessageBox.Show("Graphics profile deleted successfully");
                        BindCGConfigCombo();
                        _configId = 0;
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }

        private void txtStreamId_LostFocus(object sender, RoutedEventArgs e)
        {
            InvalidatePropertyChange((TextBox)sender, 0.0);
        }
        private void PopulateProperties(Hashtable htAttrib)
        {
            txtXAxis.Text = htAttrib.ContainsKey("x") ? htAttrib["x"].ToString() : "0.0";
            txtYAxis.Text = htAttrib.ContainsKey("y") ? htAttrib["y"].ToString() : "0.0";
            txtHeight.Text = htAttrib.ContainsKey("h") ? htAttrib["h"].ToString() : "0.0";
            txtWidth.Text = htAttrib.ContainsKey("w") ? htAttrib["w"].ToString() : "0.0";
            txtRotation.Text = htAttrib.ContainsKey("r") ? htAttrib["r"].ToString() : "0.0";
            txtCropX.Text = htAttrib.ContainsKey("sx") ? htAttrib["sx"].ToString() : "0.0";
            txtCropY.Text = htAttrib.ContainsKey("sy") ? htAttrib["sy"].ToString() : "0.0";
            txtCropH.Text = htAttrib.ContainsKey("sh") ? htAttrib["sh"].ToString() : "1.0";
            txtCropW.Text = htAttrib.ContainsKey("sw") ? htAttrib["sw"].ToString() : "1.0";
            txtAudioGain.Text = htAttrib.ContainsKey("audio_gain") ? htAttrib["audio_gain"].ToString() : "+0.0";
            txtStreamId.Text = htAttrib.ContainsKey("stream_id") ? htAttrib["stream_id"].ToString() : "";

            drpPosition.SelectedValue = htAttrib.ContainsKey("pos") ? htAttrib["pos"].ToString() : "center";
            drpCropP.SelectedValue = htAttrib.ContainsKey("spos") ? htAttrib["spos"].ToString() : "center";

        }
        private List<string> FillPositionList()
        {
            List<string> Positions = new List<string>();
            Positions.Add("center");
            Positions.Add("right");
            Positions.Add("left");
            Positions.Add("top");
            Positions.Add("bottom");
            Positions.Add("bottom-right");
            Positions.Add("bottom-left");
            Positions.Add("top-right");
            Positions.Add("top-left");
            return Positions;
        }
        private void FillTransitionCombo()
        {
            List<string> Transition = new List<string>();
            Transition.Add("--Select--");
            Transition.Add("fade");
            Transition.Add("pixelate");
            Transition.Add("blur");
            Transition.Add("slide-h");
            Transition.Add("slide-v");
            Transition.Add("slide-rep-h");
            Transition.Add("slide-rep-v");
            Transition.Add("slide-rep-h-2");
            Transition.Add("slide-rep-v-2");
            //return Positions;
            cmbTransitions.ItemsSource = Transition;
            cmbTransitions.SelectedIndex = 0;
        }
        private void FillPositionDropDown()
        {
            try
            {

                List<string> Positions = FillPositionList();
                drpPosition.Items.Clear();
                drpPosition.ItemsSource = Positions;
                drpCropP.Items.Clear();
                drpCropP.ItemsSource = Positions;
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }
        private void sldXAxis_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            InvalidatePropertyChange((Slider)sender, 0.0);
        }

        private void sldYAxis_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            InvalidatePropertyChange((Slider)sender, 0.0);
        }

        private void sldHeight_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            InvalidatePropertyChange((Slider)sender, 0.0);
        }

        private void sldWidth_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            InvalidatePropertyChange((Slider)sender, 0.0);
        }

        private void sldRotation_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            InvalidatePropertyChange((Slider)sender, 0.0);
        }

        private void sldCropX_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            InvalidatePropertyChange((Slider)sender, 0.0);
        }

        private void sldCropY_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            InvalidatePropertyChange((Slider)sender, 0.0);
        }

        private void sldCropW_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            InvalidatePropertyChange((Slider)sender, 0.0);
            //  sldWidth.Value = sldCropW.Value;
        }

        private void sldCropH_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            InvalidatePropertyChange((Slider)sender, 0.0);
            //  sldHeight.Value = sldCropH.Value;
        }

        private void txtAudioGain_LostFocus(object sender, RoutedEventArgs e)
        {
            InvalidatePropertyChange((TextBox)sender, 0.0);
        }

        //private void txtStreamId_LostFocus(object sender, RoutedEventArgs e)
        //{
        //    InvalidatePropertyChange((TextBox)sender, 0.0);
        //}

        private void drpPosition_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            InvalidatePropertyChange((ComboBox)sender, 0.0);
        }
        private void drpCropP_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            InvalidatePropertyChange((ComboBox)sender, 0.0);
        }
        //MElement GuestElement;
        private void InvalidatePropertyChange(TextBox textB, double timeForChange)
        {
            if (GuestElement != null)
            {
                if (textB == txtAudioGain)
                    ApplyPropertyChange(GuestElement, "audio_gain", Convert.ToString(txtAudioGain.Text), timeForChange);
                if (textB == txtStreamId)
                    ApplyPropertyChange(GuestElement, "stream_id", Convert.ToString(txtStreamId.Text), timeForChange);
            }
        }
        private void InvalidatePropertyChange(ComboBox cmb, double timeForChange)
        {
            if (GuestElement != null)
            {
                if (cmb == drpPosition)
                    ApplyPropertyChange(GuestElement, "pos", Convert.ToString(drpPosition.SelectedValue), timeForChange);
                if (cmb == drpCropP)
                    ApplyPropertyChange(GuestElement, "spos", Convert.ToString(drpCropP.SelectedValue), timeForChange);
            }
        }
        private void ApplyPropertyChange(MElement SelectedElem, string attribute, string value, double timeForChange)
        {
            SelectedElem.ElementMultipleSet(attribute + "=" + value, timeForChange);
        }
        private void InvalidatePropertyChange(Slider sdr, double timeForChange)
        {
            if (GuestElement != null)
            {
                if (sdr == sldXAxis)
                    ApplyPropertyChange(GuestElement, "x", Convert.ToString(sdr.Value), timeForChange);
                if (sdr == sldYAxis)
                    ApplyPropertyChange(GuestElement, "y", Convert.ToString(sdr.Value), timeForChange);
                if (sdr == sldHeight)
                    ApplyPropertyChange(GuestElement, "h", Convert.ToString(sdr.Value), timeForChange);
                if (sdr == sldWidth)
                    ApplyPropertyChange(GuestElement, "w", Convert.ToString(sdr.Value), timeForChange);
                if (sdr == sldRotation)
                    ApplyPropertyChange(GuestElement, "r", Convert.ToString(sdr.Value), timeForChange);
                if (sdr == sldCropX)
                    ApplyPropertyChange(GuestElement, "sx", Convert.ToString(sdr.Value), timeForChange);
                if (sdr == sldCropY)
                    ApplyPropertyChange(GuestElement, "sy", Convert.ToString(sdr.Value), timeForChange);
                if (sdr == sldCropH)
                    ApplyPropertyChange(GuestElement, "sh", Convert.ToString(sdr.Value), timeForChange);
                if (sdr == sldCropW)
                    ApplyPropertyChange(GuestElement, "sw", Convert.ToString(sdr.Value), timeForChange);
            }
        }

        private void btnAudios_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                //if (mPreviewComposerControl.m_pPreview != null)
                //    mPreviewComposerControl.m_pPreview.PreviewEnable("", (Boolean)btnAudio.IsChecked ? 0 : 1, (Boolean)btnVideoPreview.IsChecked ? 0 : 1);
            }
            catch
            {

            }
        }

        private void sldVolume_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            // Volume in dB
            // 0 - full volume, -100 silence
            double dblPos = (double)sldVolume.Value / sldVolume.Maximum;
            //if (mPreviewComposerControl1.m_pPreview != null)
            //mPreviewComposerControl1.m_pPreview.PreviewAudioVolumeSet("", -1, -30 * (1 - dblPos));

        }

        private void btnVideoPreview_Click(object sender, RoutedEventArgs e)
        {
            if (mPreviewControlMixer1.m_pPreview != null)
                mPreviewControlMixer1.m_pPreview.PreviewEnable("", (Boolean)btnAudios.IsChecked ? 0 : 1, (Boolean)btnVideoPreview.IsChecked ? 0 : 1);
        }
        private void btnFullScreen_Checked(object sender, RoutedEventArgs e)
        {
            ///This code works fine
            if (mPreviewControlMixer1.m_pPreview != null)
            {
                // Enable full screen (use -1 for auto-select monitor)
                if ((Boolean)btnFullScreen.IsChecked)
                {
                    mPreviewControlMixer1.m_pPreview.PreviewFullScreen("", 1, 1);
                    btnFullScreen.IsChecked = false;
                }
                else
                    mPreviewControlMixer1.m_pPreview.PreviewFullScreen("", 0, 1);
            }
        }
        private void btnAspectRatio_Click(object sender, RoutedEventArgs e)
        {
            if (mPreviewControlMixer1.m_pPreview != null)
                ((MPLATFORMLib.IMProps)mPreviewControlMixer1.m_pPreview).PropsSet("maintain_ar", (Boolean)btnAspectRatio.IsChecked ? "none" : "letter-box");
        }
        private void cmbGraphicsSettings_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            RemovePlugins();
            LoadCGConfigSettingFromProfile();
        }
        private void LoadCGConfigSettingFromProfile()
        {
            try
            {
                if (m_objMixer != null)
                {
                    m_objCharGen = new MLCHARGENLib.CoMLCharGen();
                    //m_objMixer.PluginsRemove(m_objCharGen);
                    m_objMixer.PluginsAdd(m_objCharGen, 0);
                    //cgEditor = new CGEditor_WinForms.MainWindow();
                    cgEditor.SetSourceObject(m_objMixer, m_objCharGen);
                    Thread.Sleep(1000);
                    if (cmbGraphicsSettings.SelectedIndex > 0)
                    {
                        CGConfigSettings objCGConfig = GetCGConfigSettings(Convert.ToInt32(cmbGraphicsSettings.SelectedValue));
                        if (objCGConfig == null)
                            return;
                        cgEditor.UpdateFileName(objCGConfig.DisplayName);
                        cgEditor.ConfigFileName = objCGConfig.DisplayName;
                        MLCHARGENLib.IMLXMLPersist pXMLPersist = (MLCHARGENLib.IMLXMLPersist)m_objCharGen;
                        pXMLPersist.LoadFromXML(System.IO.Path.Combine(ConfigManager.DigiFolderPath, "CGSettings", objCGConfig.ConfigFileName + objCGConfig.Extension), -1);
                        cgEditor.Editor.UpdateItemsList();
                    }
                }

            }
            catch (Exception ex)
            {
                //LogConfigurator.log.Error("Error Message: " + ex.Message + " Error Stack Trace: " + ex.StackTrace);
            }
        }
        #endregion

        #region Editor Functions
        private void LoadGraphicEditor()
        {
            try
            {
                List<TemplateListItems> lstGraphics = objTemplateList.Where(o => o.MediaType == 606 && o.IsChecked == true).ToList();
                if (lstGraphics.Count > 0 || (chkTextLogo.IsChecked == true && !string.IsNullOrEmpty(txtTextLogo.Text)) || cmbGraphicsSettings.SelectedIndex > 0)
                {
                    string _logoText = string.Empty;
                    m_objCharGen = new MLCHARGENLib.CoMLCharGen();
                    m_objMixer.PluginsAdd(m_objCharGen, 0);
                    cgEditor = new CGEditor_WinForms.MainWindow();
                    cgEditor.CGConfig += SaveCGConfig;
                    cgEditor.FormClosed += cgEditor_FormClosed;
                    cgEditor.SetSourceObject(m_objMixer, m_objCharGen);

                    if (cmbGraphicsSettings.SelectedIndex > 0)
                    {
                        CGConfigSettings objCGConfig = GetCGConfigSettings(Convert.ToInt32(cmbGraphicsSettings.SelectedValue));
                        if (objCGConfig == null)
                            return;
                        cgEditor.UpdateFileName(objCGConfig.DisplayName);
                        cgEditor.ConfigFileName = objCGConfig.DisplayName;
                        IMLXMLPersist pXMLPersist = (IMLXMLPersist)m_objCharGen;
                        pXMLPersist.LoadFromXML(System.IO.Path.Combine(ConfigManager.DigiFolderPath, "CGSettings", objCGConfig.ConfigFileName + objCGConfig.Extension), -1);
                        //cgEditor.Editor.UpdateItemsList();
                        //List<CGBaseItem> items = cgEditor.Editor.CGItems;
                        //cgEditor.Editor.UpdateItemsList();
                    }
                    else
                    {
                        if (chkTextLogo.IsChecked == true && !string.IsNullOrEmpty(txtTextLogo.Text))
                        {
                            _logoText = txtTextLogo.Text.Trim();
                            m_objCharGen.AddNewItem(_logoText, 0.1, 0.1, 1, 1, ref m_strItemID);
                            m_objCharGen.SetItemProperties(m_strItemID, "", "", "", 0);
                            m_objCharGen.ShowItem(m_strItemID, 1, 1000);
                            m_strItemID = string.Empty;
                        }
                        foreach (var item in lstGraphics)
                        {
                            m_objCharGen.AddNewItem(item.FilePath, 0, 0, 0, 1, ref m_strItemID);
                            m_strItemID = string.Empty;
                        }
                    }
                    cgEditor.Editor.UpdateItemsList();

                    cgEditor.ShowDialog();
                }
                else
                {
                    MessageBox.Show("Please select graphics profile or graphics.", "Digiphoto", MessageBoxButton.OK, MessageBoxImage.Information);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }
        private void SaveCGConfig(object sender, EventArgs e)
        {
            SaveCGConfigDb();
            BindCGConfigCombo();
            LoadGraphicsList();
            chkTextLogo.IsChecked = false;
            txtTextLogo.Text = string.Empty;
            if (!cgEditor.IsFleNameExists)
            {
                cgEditor.Close();
            }
        }
        int _configId = 0;
        void SaveCGConfigDb()
        {
            objConfigBL = new ConfigBusiness();
            objCGConfig = new CGConfigSettings();
            if (dicConfig.ContainsValue(cgEditor.ConfigFileName) && cgEditor.IsUpdateConfig == false)
            {
                cgEditor.IsFleNameExists = true;
                return;
            }
            else
                cgEditor.IsFleNameExists = false;
            string extension = ".ml-cgc";

            objCGConfig.Extension = extension;
            objCGConfig.ConfigFileName = cgEditor.ConfigFileName;
            objCGConfig.IsActive = true;
            try
            {
                if (cgEditor.IsUpdateConfig)
                    _configId = Convert.ToInt32(cmbGraphicsSettings.SelectedValue);
                if (!cgEditor.IsUpdateConfig)
                    _configId = objConfigBL.SaveCGConfigSetting(objCGConfig);
                if (_configId > 0)
                    cgEditor.IsSaved = true;
                IMLXMLPersist pXMLPersist = (IMLXMLPersist)cgEditor.Editor.CGObject;
                string fileName = cgEditor.ConfigFileName + "_" + _configId + extension;
                string directoryPath = System.IO.Path.Combine(ConfigManager.DigiFolderPath, "CGSettings");
                if (!Directory.Exists(directoryPath))
                    Directory.CreateDirectory(directoryPath);
                pXMLPersist.SaveToXMLFile(System.IO.Path.Combine(directoryPath, fileName), 1);
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }

        }
        void BindCGConfigCombo()
        {
            try
            {
                List<CGConfigSettings> objCGConfigList = new List<CGConfigSettings>();
                objConfigBL = new ConfigBusiness();
                objCGConfigList = objConfigBL.GetCGConfigSettngs(0);
                dicConfig = new Dictionary<int, string>();
                dicConfig.Add(0, "-Select Graphics Profile-");
                foreach (var item in objCGConfigList)
                {
                    dicConfig.Add(item.ID, item.DisplayName);
                }
                cmbGraphicsSettings.ItemsSource = dicConfig;
                var selectedItem = dicConfig.Where(x => x.Key == _configId).FirstOrDefault();
                if (_configId > 0)
                    cmbGraphicsSettings.SelectedItem = selectedItem;
                else
                    cmbGraphicsSettings.SelectedIndex = 0;
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }

        private CGConfigSettings GetCGConfigSettings(int configID)
        {
            try
            {
                objConfigBL = new ConfigBusiness();
                objCGConfig = new CGConfigSettings();
                objCGConfig = objConfigBL.GetCGConfigSettngs(Convert.ToInt32(cmbGraphicsSettings.SelectedValue)).FirstOrDefault();
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
            return objCGConfig;
        }
        #endregion
        string CurrentchromaSetting = string.Empty;
        private string SaveChromaSetting(MItem mitem)
        {
            string strchromaSetting = string.Empty;
            try
            {
                GetChromakeyFilter(mitem);
                MPLATFORMLib.IMPersist m_persist;
                MCHROMAKEYLib.MKey m_pKey;
                objChromaKey.KeyGet(null, out m_pKey, "");

                m_persist = (MPLATFORMLib.IMPersist)objChromaKey;
                if (m_pKey != null && objChromaKey != null)
                {
                    {
                        m_persist.PersistSaveToString("", out strchromaSetting, "");
                    }
                }
                Marshal.ReleaseComObject(m_persist);
                Marshal.ReleaseComObject(mitem);
                GC.Collect();
                return strchromaSetting;
            }
            catch
            { return strchromaSetting; }
        }
        bool IsSceneLoaded = false;
        bool IsVerticalVideo = false;
        private void CreateVerticalVideo()
        {

        }
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            MediaStop();
            IsLoadChroma = false;
            CurrentchromaSetting = string.Empty;
            overlayChromaSetting = string.Empty;
            try
            {
                if (ConfigManager.IMIXConfigurations.ContainsKey((int)ConfigParams.VideoWriterSettings) && !string.IsNullOrEmpty(ConfigManager.IMIXConfigurations[(int)ConfigParams.VideoWriterSettings]))
                {
                    string writerSettings = ConfigManager.IMIXConfigurations[(int)ConfigParams.VideoWriterSettings];
                    LoadWriterSettings(writerSettings);

                }
                if (mPreviewControlMixer1.m_pPreview != null)
                    mPreviewControlMixer1.m_pPreview.PreviewEnable("", (Boolean)btnAudios.IsChecked ? 0 : 1, (Boolean)btnVideoPreview.IsChecked ? 0 : 1);

                if (ConfigManager.IMIXConfigurations.ContainsKey((int)ConfigParams.AutomatedVideoEditWorkFlow) && Convert.ToBoolean(ConfigManager.IMIXConfigurations[(int)ConfigParams.AutomatedVideoEditWorkFlow]) == true)
                {
                    AutomatedVideoEditWorkFlow = true;
                }
                //Aquarium Flow
                if (RobotImageLoader.PrintImages.Where(o => o.MediaType == 1 || o.MediaType == 2).ToList().Count == 1 && RobotImageLoader.PrintImages.Count == 1 && AutomatedVideoEditWorkFlow)
                {
                    bool isAutoVideoProcessingEnabled = false;
                    if (ConfigManager.IMIXConfigurations.ContainsKey((int)ConfigParams.IsEnableAutoVidProcessing))
                        isAutoVideoProcessingEnabled = Convert.ToBoolean(ConfigManager.IMIXConfigurations[(int)ConfigParams.IsEnableAutoVidProcessing]);
                    var ImgDetail = RobotImageLoader.PrintImages.Where(x => x.MediaType == 1 || x.MediaType == 2).FirstOrDefault();

                    if (ImgDetail != null && isAutoVideoProcessingEnabled)
                    {
                        this.DropPhotoId = ImgDetail.PhotoId;
                        this.activePage = 1;
                        VideoEditPlayerPanel.Visibility = Visibility.Collapsed;
                        btnSelectAll_Click(sender, e);
                        LoadSceneSettingsBasedOnImage(ImgDetail);
                        //Implement vertical design changes here
                        if (IsVerticalVideo)
                        {
                            btnResolution.IsChecked = false;
                            txbResolution.Text = "Horizontal";
                            MPLATFORMLib.M_VID_PROPS props = new MPLATFORMLib.M_VID_PROPS();
                            props.eVideoFormat = MPLATFORMLib.eMVideoFormat.eMVF_Custom;
                            props.dblRate = 29;
                            props.nAspectX = 9;
                            props.nAspectY = 16;
                            props.nHeight = 1920;
                            props.nWidth = 1080;
                            m_objMixer.FormatVideoSet(eMFormatType.eMFT_Convert, ref props);
                        }
                        else
                        {
                            btnResolution.IsChecked = true;
                            txbResolution.Text = "Vertical";
                        }
                        // ChangeGuestElement = false;
                        if (IsSceneLoaded)
                        {
                            VideoPage lstMyItem = PrintOrderPageList.Where(o => o.PhotoId == this.DropPhotoId && o.PageNo == 1).FirstOrDefault();
                            lstMyItem.ImageDisplayTime = Vidlength;
                            var firstImgDetail = lstVideoElements.FirstOrDefault();
                            lstMyItem.DropVideoPath = firstImgDetail.VideoFilePath;


                            StackImageDisplayTime.Visibility = Visibility.Visible;
                        }
                    }
                }
                if (!IsSceneLoaded)//Normal flow
                {
                    AutomatedVideoEditWorkFlow = false;
                    LoadDefaultSceneAndFile();
                    IsSceneLoaded = true;
                }
                if (IsSceneLoaded)
                    BindGuestNodeProps();

                FillPositionDropDown();
                FillTransitionCombo();
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }
        private void LoadChromaAndCGConfig(string sceneName, int CG_ConfigID)
        {
            try
            {
                string streamID = strGuestStreamID;
                m_objCharGen = new MLCHARGENLib.CoMLCharGen();
                m_objMixer.PluginsAdd(m_objCharGen, 0);
                //   ConfigBusiness objConfigBL = new ConfigBusiness();
                CGConfigSettings objCGConfig = null;
                Thread.Sleep(1000);
                if (CG_ConfigID != 0)
                {
                    objCGConfig = objConfigBL.GetCGConfigSettngs(CG_ConfigID).FirstOrDefault();
                    if (objCGConfig != null)
                    {
                        MLCHARGENLib.IMLXMLPersist pXMLPersist = (MLCHARGENLib.IMLXMLPersist)m_objCharGen;
                        string filePath = System.IO.Path.Combine(ConfigManager.DigiFolderPath, "CGSettings", objCGConfig.ConfigFileName + objCGConfig.Extension);
                        if (File.Exists(filePath))
                            pXMLPersist.LoadFromXML(filePath, -1);

                        var selectedItem = dicConfig.Where(x => x.Key == CG_ConfigID).FirstOrDefault();
                        if (CG_ConfigID > 0)
                            cmbGraphicsSettings.SelectedItem = selectedItem;
                    }
                }
                if (IsLoadChroma)
                {
                    string chromaPath = System.IO.Path.Combine(ConfigManager.DigiFolderPath, "Profiles", sceneName, sceneName + "_Chroma.xml");
                    CurrentchromaSetting = LoadChromaFromFile(streamID, chromaPath);

                }
                string overlayPath = System.IO.Path.Combine(ConfigManager.DigiFolderPath, "Profiles", sceneName, sceneName + "_OverlayChroma.xml"); ;
                if (OverlayElement != null && System.IO.File.Exists(overlayPath))
                {
                    string value; int have;
                    OverlayElement.AttributesHave("show", out have, out value);
                    if (value == "true")
                    {
                        string OverlayStream = string.Empty;
                        OverlayElement.AttributesStringGet("stream_id", out OverlayStream);
                        overlayChromaSetting = LoadChromaFromFile(OverlayStream, overlayPath);
                        IsLoadOverlayChroma = true;
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }
        private string LoadChromaFromFile(string streamID, string chromaPath)
        {
            string strChromaToString = string.Empty;
            MItem pItem; int myIndex;
            m_objMixer.StreamsGet(streamID, out myIndex, out pItem);
            //
            //Load ChromaPlugin For MItem
            LoadChromaPlugin(true, pItem);
            Thread.Sleep(500);
            MCHROMAKEYLib.MChromaKey objChromaKey = GetChromakeyFilter(pItem);
            try
            {
                ((MPLATFORMLib.IMProps)objChromaKey).PropsSet("gpu_mode", "true");
            }
            catch { }
            Thread.Sleep(500);
            //Apply chroma
            if (File.Exists(chromaPath))
                (objChromaKey as MPLATFORMLib.IMPersist).PersistLoad("", chromaPath, "");
            Thread.Sleep(500);
            //Apply chroma
            if (System.IO.File.Exists(chromaPath))
            {
                (objChromaKey as MPLATFORMLib.IMPersist).PersistLoad("", chromaPath, "");
                (objChromaKey as MPLATFORMLib.IMPersist).PersistSaveToString("", out strChromaToString, "");
            }
            return strChromaToString;
        }
        private void btnClosePreview_Click(object sender, RoutedEventArgs e)
        {
            MediaStop();
            VideoEditPlayerPanel.Visibility = Visibility.Visible;
            vidoriginal.Visibility = Visibility.Collapsed;
            stkbtnClosePreview.Visibility = Visibility.Collapsed;
            GuestElement.ElementMultipleSet("stream_id=" + strGuestStreamID + " audio_gain=-100 show=true", 0.0);
            InitializeStream(strGuestStreamID, false);
        }
        void mConfigList1_OnConfigChanged(object sender, EventArgs e)
        {
            // Update config string 
            string strConfig;
            m_objWriter.ConfigGetAll(1, out strConfig);
            VideoSettings = strConfig;
            // Check if format support network streaming
            string strFormat;
            IMAttributes pFormatConfig;
            m_objWriter.ConfigGet("format", out strFormat, out pFormatConfig);
            int bNetwork = 0;
            try
            {
                pFormatConfig.AttributesBoolGet("network", out bNetwork);
            }
            catch (System.Exception) { }
        }

        private void cmbTransitions_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            numericTimeForChange = 1.0;
        }
        private void GetAudioListToBeApplied(string sceneName)
        {
            XmlDocument xmlDoc = new XmlDocument();
            string audioXmlPath = System.IO.Path.Combine(ConfigManager.DigiFolderPath, "Profiles", sceneName, sceneName + "_Audio.xml");
            if (File.Exists(audioXmlPath))
            {
                xmlDoc.Load(audioXmlPath);
                XmlNodeList nodeList = xmlDoc.GetElementsByTagName("audio");
                foreach (XmlNode node in nodeList)
                {
                    int id = Convert.ToInt32(node["ItemID"].InnerText);
                    TemplateListItems obj = objTemplateList.Where(o => o.Item_ID == id).FirstOrDefault();
                    if (obj != null)
                    {
                        obj.StartTime = Convert.ToInt32(node["StartTime"].InnerText);
                        obj.EndTime = Convert.ToInt32(node["StopTime"].InnerText);
                        obj.InsertTime = Convert.ToInt32(node["InsertTime"].InnerText);
                        obj.IsChecked = true;
                    }
                }
            }
        }

        private void btnBackgrounds_Click(object sender, RoutedEventArgs e)
        {
            SetTemplateListVisibility(true);
            templateType = "chroma";
            lstTemplates.IsEnabled = true;
            List<TemplateListItems> TLI = objTemplateList.Where(o => o.MediaType == 605).ToList();
            // if (TLI.Count == 0)
            lstTemplates.ItemsSource = null;
            // else
            {
                lstTemplates.ItemsSource = TLI;
            }
            txtTemplateRotate.Text = "Chroma Backgrounds";
        }


        /// <summary>
        /// Function to zoom in and out the guest image
        /// </summary>
        /// <param name="blGrow"></param>
        private void ZoomInOut(bool blGrow)
        {
            double h;
            double w;
            double zoom = 0.05;

            GuestElement.AttributesDoubleGet("w", out w);
            GuestElement.AttributesDoubleGet("h", out h);
            if (w == 0 & h == 0)
            {
                string _strw;
                string _strh;
                GuestElement.AttributesInfoGet("w", MPLATFORMLib.eMInfoType.eMIT_Default, out _strw);
                GuestElement.AttributesInfoGet("h", MPLATFORMLib.eMInfoType.eMIT_Default, out _strh);
                w = Convert.ToDouble(_strw);
                h = Convert.ToDouble(_strh);
            }
            w = blGrow ? w + zoom : w - zoom;
            h = blGrow ? h + zoom : h - zoom;
            string strZoomValue = "w=" + w + " " + "h=" + h;
            if (!(h <= 0.01 || w <= 0.01))
            {
                if ((blGrow && (h <= 4.0 || w <= 4.0)) || !blGrow && (h >= 0.25 || w >= 0.25))
                {
                    GuestElement.AttributesMultipleSet(strZoomValue, eMUpdateType.eMUT_Update);
                    if ((w <= 1 && w >= 0) && (h <= 1 && h >= 0))
                    {
                        txtHeight.Text = Convert.ToString(h);
                        txtWidth.Text = Convert.ToString(w);
                    }
                }
            }
        }
        private void buttonZoomIn_Click(object sender, RoutedEventArgs e)
        {
            ZoomInOut(true);
        }
        private void buttonZoomOut_Click(object sender, RoutedEventArgs e)
        {
            ZoomInOut(false);
        }

        private void btnLoadOverlay_Click(object sender, RoutedEventArgs e)
        {
            StackGraphicSettingPanel1.Visibility = Visibility.Collapsed;
            StackGraphicSettingPanel2.Visibility = Visibility.Collapsed;
            SetTemplateListVisibility(true);
            templateType = "overlay";
            lstTemplates.IsEnabled = true;
            List<TemplateListItems> TLI = objTemplateList.Where(o => o.MediaType == 609).ToList();
            // if (TLI.Count == 0)
            lstTemplates.ItemsSource = null;
            // else
            {
                lstTemplates.ItemsSource = TLI;
            }
            txtTemplateRotate.Text = "Video Overlays";
        }

        #region Color effects
        /// <summary>
        /// Color effects
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnColorEffects_Click(object sender, RoutedEventArgs e)
        {
            HighlightSelectedButton("btnColorEffects");
            txtTemplateRotate.Visibility = Visibility.Visible;
            // templateType = "ColorEffects";
            ShowHideControls("ColorEffects");
            lstTemplates.IsEnabled = true;
        }
        string PresetNode = "None";
        private void rbColorEffect_Click(object sender, RoutedEventArgs e)
        {
            RadioButton rb = sender as RadioButton;
            VideoColorEffects colorEffects = new VideoColorEffects();
            if (rb.IsChecked == true && (rb.Name == "rbGrayScale"))
            {
                PresetNode = "GrayScale";
                colorEffects = FrameworkHelper.Common.MLHelpers.VideoPresetColorEffects(1);
            }
            else if (rb.IsChecked == true && (rb.Name == "rbSepia"))
            {
                PresetNode = "Sepia";
                colorEffects = FrameworkHelper.Common.MLHelpers.VideoPresetColorEffects(2);
            }
            else if (rb.IsChecked == true && (rb.Name == "rbNone"))
            {
                PresetNode = "None";
                colorEffects = FrameworkHelper.Common.MLHelpers.VideoPresetColorEffects(3);
            }
            else if (rb.IsChecked == true && (rb.Name == "rbInvert"))
            {
                PresetNode = "Invert";
                colorEffects = FrameworkHelper.Common.MLHelpers.VideoPresetColorEffects(4);
            }
            UpdateColorSliderValues(colorEffects);
        }
        private void UpdateColorSliderValues(VideoColorEffects colorEffects)
        {
            sldULevelSlider.Value = colorEffects.ULevel;
            sldUVGainSlider.Value = colorEffects.UVGain;
            sldVLevelSlider.Value = colorEffects.VLevel;
            sldYGainSlider.Value = colorEffects.YGain;
            sldYLevelSlider.Value = colorEffects.YLevel;
            sldBlackSlider.Value = colorEffects.BlackLevel;
            sldBrightnessSlider.Value = colorEffects.Brightness;
            sldColorSlider.Value = colorEffects.ColorGain;
            sldContrastSlider.Value = colorEffects.Contrast;
            sldWhiteSlider.Value = colorEffects.WhiteLevel;
        }

        private void btnRefresh_Click(object sender, RoutedEventArgs e)
        {
            VideoColorEffects colorEffects = new VideoColorEffects();
            colorEffects = FrameworkHelper.Common.MLHelpers.VideoPresetColorEffects(3);
            rbNone.IsChecked = true;
            UpdateColorSliderValues(colorEffects);
        }
        private void SetColorConversion(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            SetColorConversion();
        }
        private void SetBrightnessContrast(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            SetBrightnessContrast();
        }
        private void SetColorConversion()
        {
            if (m_objColors != null)
            {
                MG_COLOR_PARAM colorParam = new MG_COLOR_PARAM
                {
                    dblUlevel = (double)sldULevelSlider.Value / 100,
                    dblUVGain = (double)sldUVGainSlider.Value / 100,
                    dblVlevel = (double)sldVLevelSlider.Value / 100,
                    dblYGain = (double)sldYGainSlider.Value / 100,
                    dblYlevel = (double)sldYLevelSlider.Value / 100
                };
                m_objColors.SetColorParam(ref colorParam, 1, 0);
            }
        }
        private void SetBrightnessContrast()
        {
            if (m_objColors != null)
            {
                MG_BRIGHT_CONT_PARAM brightContParam = new MG_BRIGHT_CONT_PARAM
                {
                    dblWhiteLevel = (double)sldWhiteSlider.Value / 100,
                    dblContrast = (double)sldContrastSlider.Value / 100,
                    dblColorGain = (double)sldColorSlider.Value / 100,
                    dblBrightness = (double)sldBrightnessSlider.Value / 100,
                    dblBlackLevel = (double)sldBlackSlider.Value / 100
                };
                m_objColors.SetBrightContParam(ref brightContParam, 1, 0);
            }
        }

        private void LoadValuestoColorControls(VideoColorEffects colorEffects)
        {
            string presetEffects = colorEffects.PresetEffects;
            if (presetEffects == "Sepia")
                rbSepia.IsChecked = true;
            else if (presetEffects == "GrayScale")
                rbGrayScale.IsChecked = true;
            else
                rbNone.IsChecked = true;

            UpdateColorSliderValues(colorEffects);
            SetColorConversion();
        }
        bool IsLoadOverlayChroma = false;
        private void btnOverlayChroma_Click(object sender, RoutedEventArgs e)
        {
            StackGraphicSettingPanel1.Visibility = Visibility.Collapsed;
            StackGraphicSettingPanel2.Visibility = Visibility.Collapsed;
            MItem pFile; int ind = 0;
            if (!string.IsNullOrEmpty(strOverlayStream))
            {
                m_objMixer.StreamsGet(strOverlayStream, out ind, out pFile);
                FrameworkHelper.Common.MLHelpers mh = new MLHelpers();
                objChromaKey = mh.LoadChromaScreen(pFile, strOverlayStream);
                if (objChromaKey != null)
                {
                    IsLoadOverlayChroma = true;
                    overlayChromaSetting = SaveChromaSetting(pFile);
                }
            }
        }

        private void btnResolution_Click(object sender, RoutedEventArgs e)
        {
            if (btnResolution.IsChecked == false)
            {
                txbResolution.Text = "Horizontal";
                MPLATFORMLib.M_VID_PROPS props = new MPLATFORMLib.M_VID_PROPS();
                props.eVideoFormat = MPLATFORMLib.eMVideoFormat.eMVF_Custom;
                props.dblRate = 29;
                props.nAspectX = 9;
                props.nAspectY = 16;
                props.nHeight = 1920;
                props.nWidth = 1080;
                m_objMixer.FormatVideoSet(eMFormatType.eMFT_Convert, ref props);
            }
            else
            {
                txbResolution.Text = "Vertical";
                MPLATFORMLib.M_VID_PROPS props = new MPLATFORMLib.M_VID_PROPS();
                props.eVideoFormat = MPLATFORMLib.eMVideoFormat.eMVF_Custom;
                props.dblRate = 29;
                props.nAspectX = 16;
                props.nAspectY = 9;
                props.nHeight = 1080;
                props.nWidth = 1920;
                m_objMixer.FormatVideoSet(eMFormatType.eMFT_Convert, ref props);
            }
        }
    }
        #endregion Color effects

    class StreamList
    {
        public int Index { get; set; }
        public string streamName { get; set; }
        public double In { get; set; }
        public double Out { get; set; }
        public bool Isprocessed { get; set; }
        public string FilePath { get; set; }
        public double InsertTime { get; set; }
        public int MediaType { get; set; }
    }
}

