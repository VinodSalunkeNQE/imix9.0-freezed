﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using DigiPhoto.DataLayer;
using DigiPhoto.DataLayer.Model;
using System.Drawing.Printing;
using DigiAuditLogger;
using DigiPhoto.Common;
using DigiPhoto.IMIX.Business;
using DigiPhoto.IMIX.Model;
using System.Management;
using DigiPhoto.Utility.Repository.ValueType;
using FrameworkHelper;
namespace DigiPhoto.Manage
{
    /// <summary>
    /// Interaction logic for ManagePrinter.xaml
    /// </summary>
    public partial class ManagePrinter : Window
    {
        #region Declaration
        Dictionary<string, string> lstProductList;
        Dictionary<string, string> lstPrinterList;
        Dictionary<string, string> lstPaperSizeList;
        #endregion
        #region Constructor
        /// <summary>
        /// Initializes a new instance of the <see cref="ManagePrinter"/> class.
        /// </summary>
        public ManagePrinter()
        {
            InitializeComponent();
            FillProductCombo();
            FillPrinters();
            LoadPrinterDetails();
            //  CmbPaperSize.IsEnabled = false;
            FillPaperSizeCombo();
            btnSavePrinter.IsDefault = true;
            GetPrinterTypeDetails();
        }

        private void LoadPrinterDetails()
        {
            PrinterBusniess priBiz = new PrinterBusniess();
            DGManagePrinter.ItemsSource = priBiz.GetPrinterDetails(LoginUser.SubStoreId);
        }

        #endregion
        #region Common Methods
        /// <summary>
        /// Fills the product combo.
        /// </summary>
        private void FillProductCombo()
        {

            try
            {
                ProductBusiness proBiz = new ProductBusiness();
                var list = proBiz.GetProductType().Where(t => t.DG_IsAccessory == false).ToList();

                CommonUtility.BindComboWithSelect<ProductTypeInfo>(CmbProductType, list, "DG_Orders_ProductType_Name", "DG_Orders_ProductType_pkey", 0, DigiPhoto.Utility.Repository.Data.ClientConstant.SelectString);              
                CmbProductType.SelectedValue = "0";

               // CommonUtility.BindComboWithSelect<ProductTypeInfo>(CmbProductType1, list, "DG_Orders_ProductType_Name", "DG_Orders_ProductType_pkey", 0, DigiPhoto.Utility.Repository.Data.ClientConstant.SelectString);
                CommonUtility.BindCombo<ProductTypeInfo>(CmbProductType1, list, "DG_Orders_ProductType_Name", "DG_Orders_ProductType_pkey");
                CmbProductType1.SelectedValue = "0";

            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
            }
        }
        /// <summary>
        /// Fills the printers.
        /// </summary>
        private void FillPrinters()
        {
            lstPrinterList = new Dictionary<string, string>();
            lstPrinterList.Add("--Select--", "0");
            int i = 1;
            foreach (String printer in PrinterSettings.InstalledPrinters)
            {

                lstPrinterList.Add(printer.ToString(), i.ToString());
                i++;

            }
            CmbPrinterType.ItemsSource = lstPrinterList;
            CmbPrinterType.SelectedValue = "0";
        }

        /// <summary>
        /// Fills the paper size combo.
        /// </summary>
        private void FillPaperSizeCombo()
        {
            lstPaperSizeList = new Dictionary<string, string>();
            lstPaperSizeList.Add("--Select--", "0");

            PrinterSettings printer = new PrinterSettings();
            printer.PrinterName = ((System.Collections.Generic.KeyValuePair<string, string>)(CmbPrinterType.SelectedItem)).Key;
            int i = 1;
            foreach (PaperSize size in printer.PaperSizes)
            {

                lstPaperSizeList.Add(size.ToString(), i.ToString());
                i++;

            }
            //if (lstPaperSizeList.Count > 0)
            //    CmbPaperSize.IsEnabled = true;
            //else
            //    CmbPaperSize.IsEnabled = false;

            //CmbPaperSize.ItemsSource = lstPaperSizeList;
            //CmbPaperSize.SelectedValue = "0";
        }
        /// <summary>
        /// Checks the printer validations.
        /// </summary>
        /// <returns></returns>
        private bool CheckPrinterValidations()
        {
            if (CmbPrinterType.SelectedValue.ToString() == "0")
            {
                MessageBox.Show(UIConstant.SelectPrinterType);
                return false;
            }
            else if (CmbProductType.SelectedValue.ToString() == "0")
            {
                MessageBox.Show(UIConstant.SelectProductType);
                return false;
            }
            //else if (CmbPaperSize.SelectedValue == "0")
            //{
            //    return false;
            //}
            else
            {
                return true;
            }
        }
        #endregion
        #region Events
        /// <summary>
        /// Handles the Click event of the btnSavePrinter control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void btnSavePrinter_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (CheckPrinterValidations())
                {
                    string producttypevalue = ((ProductTypeInfo)(CmbProductType.SelectedItem)).DG_Orders_ProductType_Name;
                    string printertypevalue = ((System.Collections.Generic.KeyValuePair<string, string>)(CmbPrinterType.SelectedItem)).Key;
                    ProductBusiness proBiz = new ProductBusiness();
                    int ProductTypeID = proBiz.GetProductID(producttypevalue);
                    string papersize = "";
                    if (ProductTypeID != 0)
                    {
                        PrinterBusniess priBiz = new PrinterBusniess();
                        //DigiPhotoDataServices _objdbLayer = new DigiPhotoDataServices();
                        //_objdbLayer.SetPrinterDetails(printertypevalue, ProductTypeID, (bool)IsPrinterActive.IsChecked, papersize, LoginUser.SubStoreId);

                        new PrinterBusniess().SetPrinterDetails(printertypevalue, ProductTypeID, (bool)IsPrinterActive.IsChecked, papersize, LoginUser.SubStoreId);

                        //priBiz.SetPrinterDetails(printertypevalue, ProductTypeID, (bool)IsPrinterActive.IsChecked, papersize, LoginUser.SubStoreId);
                        MessageBox.Show(UIConstant.RecordSavedSuccessfully);
                        CustomBusineses cusBiz = new CustomBusineses();
                        AuditLog.AddUserLog(Common.LoginUser.UserId, (int)FrameworkHelper.ActionType.PrinterAdded, "Printer Add/Edit at ");
                        DGManagePrinter.ItemsSource = priBiz.GetPrinterDetails(LoginUser.SubStoreId);
                    }

                }
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
            }
        }



        /// <summary>
        /// Handles the Click event of the btnDeletePrinter control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void btnDeletePrinter_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (MessageBox.Show(UIConstant.DoYouWantToDeleteThisPrinter, "Confirm delete", MessageBoxButton.YesNo) == MessageBoxResult.Yes)
                {

                    Button btnSender = (Button)sender;
                    PrinterBusniess priBiz = new PrinterBusniess();
                    bool isdelete = priBiz.DeletePrinter((int)btnSender.Tag);
                    if (isdelete)
                    {
                        MessageBox.Show(UIConstant.PrinterDeletedSuccessfully);
                        CustomBusineses cusBiz = new CustomBusineses();
                        AuditLog.AddUserLog(Common.LoginUser.UserId, (int)FrameworkHelper.ActionType.DeletePrinter, "Printer deleted at ");
                        DGManagePrinter.ItemsSource = priBiz.GetPrinterDetails(LoginUser.SubStoreId);
                    }

                }

            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);

            }
        }

        //bool iseditprinter = false;
        /// <summary>
        /// Handles the Click event of the btnEditPrinter control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void btnEditPrinter_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                IsPrinterActive.IsChecked = false;
                Button btnSender = (Button)sender;
                PrinterBusniess priBiz = new PrinterBusniess();
                string Printer = priBiz.GetPrinterNameFromID((int)btnSender.Tag);
                string[] Printerdetails = Printer.Split('#');
                string printername = Printerdetails[0];
                string printeractive = Printerdetails[1];
                string papersize = Printerdetails[3];
                string product = Printerdetails[2]; string value;
                try
                {
                    value = lstPrinterList[printername].ToString();
                }
                catch (Exception ex)
                {
                    value = "0";
                }
                CmbPrinterType.SelectedValue = value;
                CmbProductType.SelectedValue = product;
                //CmbPaperSize.SelectedItem = papersize;
                if (printeractive == "True")
                {
                    IsPrinterActive.IsChecked = true;
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
               
            }
        }

        /// <summary>
        /// Handles the Click event of the btnClearPrinter control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void btnClearPrinter_Click(object sender, RoutedEventArgs e)
        {
            CmbPrinterType.SelectedValue = "0";
            CmbProductType.SelectedValue = "0";
            //  CmbPaperSize.SelectedValue = "0";
            IsPrinterActive.IsChecked = true;
            FillPaperSizeCombo();
        }

        /// <summary>
        /// Handles the Click event of the btnBackPrinter control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void btnBackPrinter_Click(object sender, RoutedEventArgs e)
        {
            ManageHome _objhome = new ManageHome();
            _objhome.Show();
            this.Close();
        }

        /// <summary>
        /// Handles the DropDownClosed event of the CmbPrinterType control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void CmbPrinterType_DropDownClosed(object sender, EventArgs e)
        {
            FillPaperSizeCombo();
        }
        /// <summary>
        /// Refreshes the printer list
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnRefreshPrinters_Click(object sender, RoutedEventArgs e)
        {
            if (DeleteAllPrinters())
            {
                AddAvailablePrinters();
                AuditLog.AddUserLog(Common.LoginUser.UserId, (int)FrameworkHelper.ActionType.RemapPrinter, "Printer Remapped on :- ");
                MessageBox.Show("Printer list refreshed successfully.", "DigiPhoto", MessageBoxButton.OK, MessageBoxImage.Information);
            }
            else
                MessageBox.Show("Printer list could not be cleared!", "DigiPhoto", MessageBoxButton.OK, MessageBoxImage.Error);
        }
        #endregion
        #region Manage Printer Type
        bool isPrinterTypeEdited = false;
        int pTypeId = 0;
        private void GetPrinterTypeDetails()
        {
            try
            {
                PrinterTypeBusiness objPrinterTypeBusiness = new PrinterTypeBusiness();
                List<PrinterTypeInfo> objPrintertypeList = objPrinterTypeBusiness.GetPrinterTypeList(0);
                DGManagePrinterType.ItemsSource = objPrintertypeList;
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);

            }
        }

        private void btnEditPrinterType_Click(object sender, RoutedEventArgs e)
        {
            isPrinterTypeEdited = true;
            Button btnSender = (Button)sender;
            pTypeId = btnSender.Tag.ToInt32();
            PrinterTypeBusiness pTypeBusiness = new PrinterTypeBusiness();
            PrinterTypeInfo pInfo = pTypeBusiness.GetPrinterTypeList(pTypeId).FirstOrDefault();
            txtPrinterType.Text = pInfo.PrinterType;
            CmbProductType1.SelectedValue = pInfo.ProductTypeID;
            IsPrinterActiveType.IsChecked = pInfo.IsActive;
        }

        private void btnDeletePrinterType_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Button btnSender = (Button)sender;
                pTypeId = btnSender.Tag.ToInt32();
                MessageBoxResult response = MessageBox.Show("Do you want to delete the record?", "Digiphoto", MessageBoxButton.YesNo, MessageBoxImage.Question);
                if (response == MessageBoxResult.Yes)
                {
                    PrinterTypeBusiness pTypeBusiness = new PrinterTypeBusiness();
                    PrinterTypeInfo pInfo = pTypeBusiness.GetPrinterTypeList(pTypeId).FirstOrDefault();
                    if (pInfo != null)
                    {
                        bool isDeleted = pTypeBusiness.DeletePrinterType(pTypeId);
                        if (isDeleted)
                        {
                            GetPrinterTypeDetails();
                            MessageBox.Show("Printer type deleted successfully.", "DigiPhoto", MessageBoxButton.OK, MessageBoxImage.Information);
                        }
                        else
                            MessageBox.Show(UIConstant.PrinterTypeCouldNotBeDeleted, "DigiPhoto", MessageBoxButton.OK, MessageBoxImage.Error);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(UIConstant.PrinterTypeCouldNotBeDeleted, "DigiPhoto", MessageBoxButton.OK, MessageBoxImage.Error);
                ErrorHandler.ErrorHandler.LogError(ex);

            }
        }

        private void btnClearPrinterType_Click(object sender, RoutedEventArgs e)
        {
            txtPrinterType.Text = string.Empty;
            CmbProductType1.SelectedIndex = 0;
            IsPrinterActiveType.IsChecked = true;
        }

        private void btnSavePrinterType_Click(object sender, RoutedEventArgs e)
        {
            string printerTypeKeyword = txtPrinterType.Text;
            if (CheckPrinterTypeValidations(printerTypeKeyword))
            {
                bool IsPrintTypeActive = (bool)IsPrinterActiveType.IsChecked;
                int prodType = CmbProductType1.SelectedValue.ToInt16();
                bool isSaved = SavePrinterTypeDetails(pTypeId, printerTypeKeyword, prodType, IsPrintTypeActive);
                GetPrinterTypeDetails();
                if (isSaved)
                {
                    MessageBox.Show(UIConstant.PrinterTypeSavedSuccessfull, "DigiPhoto", MessageBoxButton.OK, MessageBoxImage.Information);
                    isPrinterTypeEdited = false;
                    pTypeId = 0;
                }
                else
                    MessageBox.Show(UIConstant.PrinterTypeCouldNotBeSavedDueToSomeError, "DigiPhoto", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            else
            {
                MessageBox.Show(UIConstant.PrinterTypeKeywordCannotBeEmpty, "DigiPhoto", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
        private bool SavePrinterTypeDetails(int PrinterTypeId, string PrinterTypeKeyword, int ProductTypeId, bool isActive)
        {
            try
            {
                PrinterTypeInfo pInfo = new PrinterTypeInfo();
                pInfo.PrinterTypeID = PrinterTypeId;
                pInfo.PrinterType = PrinterTypeKeyword;
                pInfo.ProductTypeID = ProductTypeId;
                pInfo.IsActive = isActive;

                PrinterTypeBusiness pBussiness = new PrinterTypeBusiness();
                return pBussiness.SavePrinterType(pInfo);
            }
            catch
            {
                return false;
            }
        }
        private bool CheckPrinterTypeValidations(string keyword)
        {
            if (String.IsNullOrEmpty(keyword))
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        private void AddAvailablePrinters()
        {
            PrinterTypeBusiness AssociatedPrinterBusiness = new PrinterTypeBusiness();
            foreach (var printer in lstPrinterList)
            {
                if (printer.Value != "0")
                {
                    string printerName = printer.Key;
                    bool isPrinterActive = isPrinterReady(printerName);
                    bool result = AssociatedPrinterBusiness.SaveOrActivateNewPrinter(printerName, LoginUser.SubStoreId, isPrinterActive);
                }
            }
            LoadPrinterDetails();
            AssociatedPrinterBusiness = null;
        }

        private bool DeleteAllPrinters()
        {
            bool result = false;
            try
            {
                PrinterTypeBusiness AssociatedPrinterBusiness = new PrinterTypeBusiness();
                result = AssociatedPrinterBusiness.DeleteAssociatedPrinters(LoginUser.SubStoreId);
            }
            catch (Exception ex)
            {
                result = false;
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
            return result;
        }

        private bool isPrinterReady(string PrinterName)
        {
            // Set management scope
            bool retvalue = false;
            ManagementScope scope = new ManagementScope("\\root\\cimv2");
            scope.Connect();

            // Select Printers from WMI Object Collections
            ManagementObjectSearcher searcher = new ManagementObjectSearcher("SELECT * FROM Win32_Printer");
            string printerName = "";
            var results = searcher.Get();
            string[] PrinterStatuses = 
            {
                "Other", "Unknown", "Idle", "Printing", "WarmUp", "Stopped Printing", "Offline"
            };

            foreach (ManagementObject printer1 in results)
            {
                printerName = printer1["Name"].ToString().ToLower();
                if (printerName.Equals(PrinterName.ToLower()))
                {
                    if (printer1["WorkOffline"].ToString().ToLower().Equals("true"))
                    {
                        // printer is offline 
                        ErrorHandler.ErrorHandler.LogFileWrite(printerName + " is Offline.");
                        retvalue = false;
                    }
                    else
                    {
                        Int32 status = printer1["PrinterStatus"].ToInt32();
                        string printerStatus = PrinterStatuses[status];
                        if (status == Convert.ToInt32(PrinterStatus.Other) || status == Convert.ToInt32(PrinterStatus.Offline))
                        {
                            retvalue = false;
                            string errorMessage = printerName + " is in error (" + printerStatus + ") status : ";
                            ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                        }
                        else
                        {
                            retvalue = true;
                        }
                    }
                }
            }
            return retvalue;
        }
        #endregion Manage Printer Type
    }
}
