﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using DigiPhoto.DataLayer;
using DigiPhoto.Common;
using FrameworkHelper;
using DigiPhoto.Utility.Repository.ValueType;
using DigiPhoto.IMIX.Business;
using DigiPhoto.IMIX.Model;
namespace DigiPhoto.Manage
{
    /// <summary>
    /// Interaction logic for ManageLocations.xaml
    /// </summary>
    public partial class ManageLocations : Window
    {

        #region Declaration
        public int _locationId = 0;
        #endregion

        #region Constructor
        /// <summary>
        /// Initializes a new instance of the <see cref="ManageLocations"/> class.
        /// </summary>
        public ManageLocations()
        {
            try
            {
                InitializeComponent();
                GetLocations(false);
                txbUserName.Text = LoginUser.UserName;
                txbStoreName.Text = LoginUser.StoreName;
                txtStoreName.Text = LoginUser.StoreName;
                btnSave.IsDefault = true;

            }
            catch (Exception ex)
            {
            }
        }
        #endregion

        #region Events
        /// <summary>
        /// Handles the Click event of the btnEdit control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void btnEdit_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                //Button _objbtn = (Button)sender;
                _locationId = ((Button)sender).Tag.ToInt32();
                LocationBusniess locBiz = new LocationBusniess();
                var item = locBiz.GetLocationsbyId(_locationId);
                if (item != null)
                {
                    txtLocationName.Text = item.DG_Location_Name;
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
            }
        }
        /// <summary>
        /// Handles the Click event of the btnClear control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void btnClear_Click(object sender, RoutedEventArgs e)
        {
            txtLocationName.Text = string.Empty;
            _locationId = 0;
        }
        /// <summary>
        /// Handles the Click event of the btnBack control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void btnBack_Click(object sender, RoutedEventArgs e)
        {
            ManageHome _objmngHome = new ManageHome();
            _objmngHome.Show();
            this.Close();
        }
        /// <summary>
        /// Handles the Click event of the btnDelete control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void btnDelete_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                //Button _objbtn = (Button)sender;
                _locationId = ((Button)sender).Tag.ToInt32();
                LocationBusniess locBiz = new LocationBusniess();

                if (locBiz.IsLocationAssociatedToSite(_locationId))
                {
                    string msg = UIConstant.LocationAssociation;
                    MessageBox.Show(msg);
                    return;
                }
                if (MessageBox.Show("Do you want to delete this location?", "Confirm delete", MessageBoxButton.YesNo) == MessageBoxResult.Yes)
                {
                    if (locBiz.DeleteLocations(_locationId))
                    {
                        GetLocations(true);
                        MessageBox.Show(UIConstant.LocationSuccessfullyDeleted);
                        txtLocationName.Text = string.Empty;
                        _locationId = 0;
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
            }
        }
        /// <summary>
        /// Handles the Click event of the btnLogout control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void btnLogout_Click(object sender, RoutedEventArgs e)
        {
            Login _objLogin = new Login();
            _objLogin.Show();
            this.Close();
        }
        /// <summary>
        /// Handles the Click event of the btnSave control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (!string.IsNullOrEmpty(txtLocationName.Text))
                {
                    string SyncCode = CommonUtility.GetUniqueSynccode("00", LoginUser.countrycode, LoginUser.Storecode, "00");
                    //string SyncCode = CommonUtility.GetRandomString(8) + "00";
                    LocationBusniess locBiz = new LocationBusniess();
                    if (locBiz.SetLocations(_locationId, txtLocationName.Text, LoginUser.StoreId, SyncCode))
                    {
                        MessageBox.Show(UIConstant.LocationSavedSuccessfully);
                        txtLocationName.Text = string.Empty;
                        _locationId = 0;
                    }
                    else
                    {
                        MessageBox.Show(UIConstant.Locationalreadyexist);
                        txtLocationName.Text = string.Empty;
                        _locationId = 0;
                    }
                    GetLocations(true);
                }
                else
                {
                    MessageBox.Show(UIConstant.LocationNameCannotBeBlank);
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
            }
        }
        #endregion

        #region Common Methods
        /// <summary>
        /// Gets the locations.
        /// </summary>
        void GetLocations(bool reloadLoc)
        {
            LocationBusniess locBiz = new LocationBusniess();
            var itemlist = locBiz.GetLocations(LoginUser.StoreId);
            if (itemlist.Count > 0)
            {
                DGManageLocation.ItemsSource = itemlist;
            }
            if (reloadLoc)
                LoadSearchByPhotoLocation(itemlist);
        }

        private void LoadSearchByPhotoLocation(List<LocationInfo> itemlist)
        {
            SearchByPhoto window = System.Windows.Application.Current.Windows.Cast<Window>().Where(wnd => wnd.Title == "Search").Cast<SearchByPhoto>().FirstOrDefault();
            if (window != null)
            {
                var location = new List<LocationInfo>();
                location.AddRange(itemlist);
                window.LoadLocation(location);
            }
        }

        #endregion

    }
}
