﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using DigiPhoto.Common;
using DigiAuditLogger;
using DigiPhoto.DataLayer;
using DigiPhoto.IMIX.Business;


namespace DigiPhoto.Manage
{
    /// <summary>
    /// Interaction logic for ManageReport.xaml
    /// </summary>
    public partial class ManageReport : Window
    {
        public ManageReport()
        {
            InitializeComponent();
            txbUserName.Text = LoginUser.UserName;
            txbStoreName.Text = LoginUser.StoreName;
        }

        private void btnPrntLog_Click(object sender, RoutedEventArgs e)
        {
            PrintLog _objprintlog = new PrintLog();
            _objprintlog.Show();
            this.Hide();
        }

        private void btnBack_Click(object sender, RoutedEventArgs e)
        {
            ManageHome _objmanagehome = new ManageHome();
            _objmanagehome.Show();
            this.Close();
        }

        private void btnLogout_Click(object sender, RoutedEventArgs e)
        {
            AuditLog.AddUserLog(Common.LoginUser.UserId, (int)FrameworkHelper.ActionType.Logout, "Logged out at "); 
            Login _objLogin = new Login();
            _objLogin.Show();
            this.Close();
        }

       
        private void btnUserLog_Click(object sender, RoutedEventArgs e)
        {
            Reports.ActivityReport AR = new Reports.ActivityReport();
            this.Hide();
            AR.Show();
        }
    }
}
