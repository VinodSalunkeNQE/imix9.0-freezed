﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using DigiPhoto.DataLayer;
using DigiPhoto.DataLayer.Model;
using DigiPhoto.Common;
using System.IO;
using System.Threading;
using DigiAuditLogger;
using System.Windows.Threading;
using DigiPhoto.Utility.Repository.ValueType;
using DigiPhoto.IMIX.Business;
namespace DigiPhoto
{
    /// <summary>
    /// Interaction logic for ManagePrinting.xaml
    /// </summary>
    public partial class ManagePrinting : Window
    {

        /// <summary>
        /// /////Need to add Send Print Timer(Rajeev is working on that render screen once he will finish .I will add timer in the code
        /// </summary>

        #region Declaration 
        DispatcherTimer _timer;        
        public static System.Timers.Timer _printingService;
        public static int _gridSelectedIndex = 0;
        public bool PrintJob
        {
            get;
            set;
        }

        #endregion

        #region Constructor

        public ManagePrinting()
        {
            InitializeComponent();
            RobotImageLoader.GetConfigData();
            txbUserName.Text = LoginUser.UserName;
            txbStoreName.Text = LoginUser.StoreName;
            _timer = new DispatcherTimer();
            _timer.Interval = TimeSpan.FromSeconds(6);
            _timer.Tick += new EventHandler(timer_Tick);
            DGPrintingqueue.SelectedCellsChanged += new SelectedCellsChangedEventHandler(DGPrintingqueue_SelectedCellsChanged);
            _timer.Start();
            _gridSelectedIndex = 0;
        }

        void DGPrintingqueue_SelectedCellsChanged(object sender, SelectedCellsChangedEventArgs e)
        {
            if (DGPrintingqueue.SelectedIndex != -1)
                _gridSelectedIndex = DGPrintingqueue.SelectedIndex;
        }

        void timer_Tick(object sender, EventArgs e)
        {
            try
            {
                _timer.Stop();
                GetPrinterQueue();
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
            finally
            {
                _timer.Start();
            }
        }

       #endregion

        #region Common Methods
        void GetPrinterQueue()
        {            
            DGPrintingqueue.ItemsSource = null;
            PrinterBusniess prBiz = new PrinterBusniess();
            DGPrintingqueue.ItemsSource = prBiz.GetPrinterQueueForUpdown(LoginUser.SubStoreId);

            if (_gridSelectedIndex <= DGPrintingqueue.Items.Count - 1)
            {
                DGPrintingqueue.SelectedIndex = _gridSelectedIndex;
            }
            else
            {
                _gridSelectedIndex = 0;
                DGPrintingqueue.SelectedIndex = _gridSelectedIndex;
            }
        }

        #endregion
       
        #region Events
        private void btnLogout_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                AuditLog.AddUserLog(Common.LoginUser.UserId, (int)FrameworkHelper.ActionType.Logout, "Logged out at ");
                Login _objLogin = new Login();
                _objLogin.Show();
                this.Hide();
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }
        private void btnUp_Click(object sender, RoutedEventArgs e)
        {
            try
            {

                _timer.Stop();
                _gridSelectedIndex = DGPrintingqueue.SelectedIndex;
                if (_gridSelectedIndex >= 1)
                {
                    _gridSelectedIndex -= 1;
                   
                }

                Button _objbtn = new Button();
                _objbtn = (Button)(sender);               
                PrinterBusniess priBiz = new PrinterBusniess();
                priBiz.SetPrintQueueIndex(_objbtn.CommandParameter.ToInt32(), "Up");

            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                _gridSelectedIndex = 0;
            }
            finally
            {
                GetPrinterQueue();
                DGPrintingqueue.SelectedIndex = _gridSelectedIndex;
                _timer.Start();
            }
        }
        private void btnDown_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                _timer.Stop();
                _gridSelectedIndex = DGPrintingqueue.SelectedIndex;
                if (_gridSelectedIndex >= 0 && DGPrintingqueue.Items.Count > 1 && _gridSelectedIndex != DGPrintingqueue.Items.Count - 1)
                {
                    _gridSelectedIndex += 1;
                }

                Button _objbtn = new Button();
                _objbtn = (Button)(sender);              
                PrinterBusniess priBiz = new PrinterBusniess();
                priBiz.SetPrintQueueIndex(_objbtn.CommandParameter.ToInt32(), "Down");
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
            finally
            {
                GetPrinterQueue();
                DGPrintingqueue.SelectedIndex = _gridSelectedIndex;
                _timer.Start();
            }
          
        }
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Manage.ManageHome _objmanage = new Manage.ManageHome();
            _objmanage.Show();
            this.Close();
        }
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            GetPrinterQueue();
            DGPrintingqueue.SelectedIndex = _gridSelectedIndex;
        }
        #endregion

        private void btnReprint_Click(object sender, RoutedEventArgs e)
        {
            ReprintCtrl.SetParent(this);
            ReprintCtrl.dgReprint.Visibility = Visibility.Collapsed;
          var res=  ReprintCtrl.ShowHandlerDialog();
        }

     
    }

}
