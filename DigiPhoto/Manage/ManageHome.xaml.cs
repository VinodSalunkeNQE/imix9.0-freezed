﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Management;
using DigiPhoto.Common;
using DigiPhoto.DataLayer;
//using DigiPhoto.DataLayer.Model;
using System.Diagnostics;
using System.Runtime.InteropServices;
using DigiAuditLogger;
using DigiPhoto.Orders;
using Microsoft.Win32;
using System.Configuration;
using DigiPhoto.IMIX.Business;
using System.Drawing.Printing;
using DigiPhoto.IMIX.Model;
using DigiPhoto.Utility.Repository.ValueType;
using FrameworkHelper;
using System.Net;

namespace DigiPhoto.Manage
{
    /// <summary>
    /// Interaction logic for ManageHome.xaml
    /// </summary>
    public partial class ManageHome : Window
    {


        #region Declaration
        //  DigiPhotoDataServices _objdblayer = new DigiPhotoDataServices();
        private delegate int LowLevelKeyboardProcDelegate(int nCode, int wParam, ref KBDLLHOOKSTRUCT lParam);
        string _MktImgPath = string.Empty;
        int _mktImgTime = 0;
        private struct KBDLLHOOKSTRUCT
        {
            public int vkCode;
            int scanCode;
            public int flags;
            int time;
            int dwExtraInfo;
        }
        [DllImport("kernel32.dll")]
        private static extern IntPtr GetModuleHandle(IntPtr path);
        [DllImport("user32.dll", EntryPoint = "SetWindowsHookExA", CharSet = CharSet.Ansi)]
        private static extern IntPtr SetWindowsHookEx(
            int idHook,
            LowLevelKeyboardProcDelegate lpfn,
            IntPtr hMod,
            int dwThreadId);
        private IntPtr intLLKey;
        const int WH_KEYBOARD_LL = 13;
        [DllImport("user32.dll", EntryPoint = "CallNextHookEx", CharSet = CharSet.Ansi)]
        private static extern int CallNextHookEx(
            int hHook, int nCode,
            int wParam, ref KBDLLHOOKSTRUCT lParam);
        [DllImport("user32.dll")]
        private static extern IntPtr UnhookWindowsHookEx(IntPtr hHook);
        #endregion

        #region Constructor
        /// <summary>
        /// Initializes a new instance of the <see cref="ManageHome"/> class.
        /// </summary>
        public ManageHome()
        {
            InitializeComponent();
            txbUserName.Text = LoginUser.UserName;
            txbStoreName.Text = LoginUser.StoreName;

            //1	Manage Product Pricing
            //2	Semi Order
            //3	Change Currency Rate
            //4	Add/Edit User
            //5	Change System Configuration
            //6	Change Printer Setting
            CtrlOpeningForm.SetParent(modelparent);
            CtrlInventoryConsumables.SetParent(modelparent);
            CtrlOperationalStatistics.SetParent(modelparent);
            CtrlOpenCloseForm.SetParent(modelparent);
            CtrlOfflineClosingForm.SetParent(modelparent);
        }
        #endregion

        #region Variables
        Int64 TotalCount8810 = 0;
        Int64 TotalCount6850 = 0;
        Int64 TotalCount6900 = 0;
        List<ImixPOSDetail> lstPosDetail = new List<ImixPOSDetail>();
        List<Printer6850> lst6850Printer = new List<Printer6850>();
        List<Printer8810> lst8810Printer = new List<Printer8810>();
        private bool btnBackWasClicked = false;
        private bool btnReceiptReprintClicked = false;
        private bool btnManageDeviceClicked = false;
        private bool btnManageSubstoresClicked = false;
        private bool btnPrntMgrClicked = false;
        private bool btnConfigClicked = false;
        private bool btnUsrMgmtClicked = false;
        private bool btncashboxClicked = false;
        private bool btnRemapPrintersClicked = false;
        private bool btnHelpClicked = false;
        private bool btnHealthChkupClicked = false;
        private bool btnFinancialReportClicked = false;
        private bool btnCleargroupClicked = false;
        private bool btnManageStoresClicked = false;
        private bool btnMinimizeAppClicked = false;
        private bool btnShutDownClicked = false;
        private bool btnLogoutClicked = false;
        private bool btnManageCameraClicked = false;
        private bool btnManageProductClicked = false;
        private bool btnPrinterQueueClicked = false;
        private bool btnEnableTaskbarClicked = false;
        private bool btnLocationsClicked = false;
        private bool btnRefundClicked = false;
        private bool btnSystemprinterqueueClicked = false;
        private bool btnBurnOrdersClicked = false;
        private bool btnCardTypeClicked = false;
        private bool btnsynstatusClicked = false;
        private bool btnRFIDMatchClicked = false;
        private bool btnEmailStatusClicked = false;
        private bool btnCurrencyExchangeClicked = false;
        private bool btnUpldMasterDataClicked = false;
        private bool btnDownloadClosingFormClicked = false;
        #endregion

        #region Events
        /// <summary>
        /// Handles the Click event of the Button control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        /// 


        public void AutoCount8810()
        {
            TotalCount8810 = 0;
            foreach (ImixPOSDetail objPosDetail in lstPosDetail)
            {
                try
                {
                    string url = "http://" + objPosDetail.SystemName + ":8000/GEt8810PrintersInfo";
                    var client = new WebClient();
                    string str8810Printer = client.DownloadString(url);

                    lst8810Printer = serialize.DeserializeXML<List<Printer8810>>(str8810Printer);
                    if (lst8810Printer.Count == 0)
                    {
                        ErrorHandler.ErrorHandler.LogFileWrite("No Printer Found by Kodak SDK");
                    }
                    else
                    {
                        foreach (Printer8810 objPrinter in lst8810Printer)
                        {
                            if (objPrinter.ErrorMessage != "")
                            {
                                ErrorHandler.ErrorHandler.LogFileWrite(objPrinter.ErrorMessage);
                            }
                            else
                            {
                                TotalCount8810 = TotalCount8810 + objPrinter.ImageCount;
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    ErrorHandler.ErrorHandler.LogError(ex);
                }
            }
        }

        public void AutoCount6850()
        {
            TotalCount6850 = 0;
            foreach (ImixPOSDetail objPosDetail in lstPosDetail)
            {
                try
                {
                    //here i will put the loop for printer server means i will get all the details from the  machine 
                    //here i will pass the logical substore id and will get all the name of the physical associated printservernames

                    string url = "http://" + objPosDetail.SystemName + ":8000/Get6850PrintersInfo";
                    var client = new WebClient();
                    string str6850Printer = client.DownloadString(url);

                    lst6850Printer = serialize.DeserializeXML<List<Printer6850>>(str6850Printer);

                    if (lst6850Printer.Count == 0)
                    {
                        ErrorHandler.ErrorHandler.LogFileWrite("No Printer Found by Kodak SDK");
                    }
                    else
                    {
                        foreach (Printer6850 objPrinter in lst6850Printer)
                        {
                            if (objPrinter.ErrorMessage != "")
                            {
                                ErrorHandler.ErrorHandler.LogFileWrite(objPrinter.ErrorMessage);
                            }
                            else
                            {
                                TotalCount6850 = objPrinter.ImageCount;
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    ErrorHandler.ErrorHandler.LogError(ex);
                }
            }
        }

        public void GetPrintServerDetails()
        {
            lstPosDetail = (new SageBusiness()).GetPrintServerDetails(LoginUser.SubStoreId);
        }


        string correctHour(string time)
        {
            var ampmLen = 2;
            var ampm = time.Substring(time.Length - ampmLen, ampmLen);
            var hourIndex = 0;
            var hour = time.Split(' ')[hourIndex];
            var h = hour;
            if (ampm.Equals("PM"))
            {
                h = (int.Parse(hour) + 12).ToString();
            }
            if (hour.Equals("12") || hour.Equals("24"))
            {
                if (ampm.Equals("AM"))
                {
                    h = "00";
                }
                else if (ampm.Equals("PM"))
                {
                    h = "12";
                }
            }
            return h;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            CustomBusineses cusBiz = new CustomBusineses();
            try
            {
                Button _objButton = (Button)(sender);
                RolePermissionsBusniess rolBiz = new RolePermissionsBusniess();
                List<PermissionRoleInfo> _objPList = rolBiz.GetPermissionData(LoginUser.RoleId);
                switch (_objButton.Name)
                {
                    // Adding By Bhagyashree Dhobale 
                    // To Restrict Receipt/Reprint Image Icon if Opening/Closing form is not filled at site 
                    // On 21/07/2017.
                    case "btnReceiptReprint":
                        {
                            btnReceiptReprintClicked = true;
                            bool open = false;
                            DateTime ServerTime;
                            DateTime ClosingFormTime = new DateTime();
                            TimeSpan ts = new TimeSpan(4, 0, 0);
                            try
                            {
                                List<iMIXConfigurationInfo> lstConfig = (new ConfigBusiness()).GetNewConfigValues(LoginUser.SubStoreId);
                                var ClosingProcTime = lstConfig.Where(o => o.IMIXConfigurationMasterId == Convert.ToInt32(ConfigParams.ClosingProcTime)).FirstOrDefault();
                                //ASHU
                                if (ClosingProcTime != null)
                                {
                                    string[] arr = ClosingProcTime.ConfigurationValue.ToString().Split(' ');
                                    ts = new TimeSpan(Convert.ToInt32(correctHour(ClosingProcTime.ConfigurationValue)), Convert.ToInt32(arr[1]), 0);
                                    if (Convert.ToInt32(correctHour(ClosingProcTime.ConfigurationValue)) > 11)
                                    {
                                        ClosingFormTime = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, Convert.ToInt32(correctHour(ClosingProcTime.ConfigurationValue)), Convert.ToInt32(arr[1]), 0);
                                    }
                                    else
                                    {
                                        //ClosingFormTime = new DateTime(DateTime.Now.Year, DateTime.Now.Month, (DateTime.Now.Day + 1), Convert.ToInt32(correctHour(ClosingProcTime.ConfigurationValue)), Convert.ToInt32(arr[1]), 0);
                                        ClosingFormTime = DateTime.Now.AddDays(1).Date.AddHours(Convert.ToInt32(correctHour(ClosingProcTime.ConfigurationValue))).AddMinutes(Convert.ToInt32(arr[1]));
                                    }

                                }

                                SageInfo _sageInfo = (new SageBusiness()).GetOpenCloseProcDetail(LoginUser.SubStoreId);
                                if (_sageInfo == null)
                                    _sageInfo = new SageInfo();
                                ServerTime = _sageInfo.ServerTime;
                                string OpeningSyncCode = CommonUtility.GetUniqueSynccode(Convert.ToInt32(ApplicationObjectEnum.OpeningFormDetail).ToString().PadLeft(2, '0'), LoginUser.countrycode, LoginUser.Storecode, LoginUser.SubStoreId.ToString().PadLeft(2, '0'));

                                //if opening is  mendatory then we will push him for opening
                                if (_sageInfo.FormTypeID != 1 && _sageInfo.TransDate.Value.Date < ServerTime.Date)
                                {
                                    var Useritem = _objPList.Where(t => t.DG_Permission_Id == 34).FirstOrDefault();
                                    if (Useritem == null)
                                    {
                                        MessageBox.Show("You are not an authorized user to fill opening form, Please contact administrator");
                                        return;
                                    }

                                    //AutoCount6850();
                                    //AutoCount8810();
                                    CtrlOpeningForm.FromDate = ServerTime.Date;

                                    CtrlOpeningForm.printAutoStart6850 = TotalCount6850;
                                    CtrlOpeningForm.printAutoStart8810 = TotalCount8810;
                                    // BY KCB ON 08 AUG 2019 FOR storing panorama printer inventory
                                    K6900Printer.K6900Driver k6900Driver = new K6900Printer.K6900Driver();
                                    string[] serialNumbers = new string[1];
                                    if (k6900Driver.GetSerialNumbers(out serialNumbers) == true)
                                    {

                                        K6900Printer.Entity.PrintCountInfo printCountInfo;
                                        if (k6900Driver.GetPrintCountInfo(serialNumbers[0], out printCountInfo))
                                        {
                                            if (printCountInfo != null)
                                            {
                                                TotalCount6900 = printCountInfo.Cutter;
                                                CtrlOpeningForm.printAutoStart6900 = TotalCount6900;
                                            }
                                        }
                                    }
                                    //END  
                                    var res = CtrlOpeningForm.ShowHandlerDialog("Opening Form");
                                    if (res != "")
                                    {
                                        open = true;
                                        //here save the data to db after operation and move ahead
                                        string[] split = new string[] { "%##%" };
                                        string[] data = res.Split(split, StringSplitOptions.None);
                                        _sageInfo = new SageInfo();
                                        _sageInfo.sixEightStartingNumber = Convert.ToInt64(data[0]);
                                        _sageInfo.eightTenStartingNumber = Convert.ToInt64(data[1]);
                                        _sageInfo.sixTwentyStartingNumber = Convert.ToInt64(data[2]);
                                        _sageInfo.eightTwentyStartingNumber = Convert.ToInt64(data[3]);
                                        _sageInfo.PosterStartingNumber = Convert.ToInt64(data[4]);
                                        _sageInfo.CashFloatAmount = Convert.ToDecimal(data[5]);
                                        _sageInfo.BusinessDate = Convert.ToDateTime(data[6]);
                                        _sageInfo.K6900StartingNumber = Convert.ToInt64(data[7]); // BY KCB ON 08 AUG 2019 FOR storing panorama printer inventory

                                        _sageInfo.SubStoreID = LoginUser.SubStoreId;
                                        _sageInfo.OpeningDate = ServerTime;
                                        _sageInfo.FilledBy = LoginUser.UserId;
                                        _sageInfo.FormTypeID = 1;
                                        _sageInfo.SyncCode = OpeningSyncCode;
                                        _sageInfo.eightTenAutoStartingNumber = TotalCount8810;
                                        _sageInfo.sixEightAutoStartingNumber = TotalCount6850;
                                        _sageInfo.K6900AutoStartingNumber = TotalCount6900;// BY KCB ON 08 AUG 2019 FOR storing panorama printer inventory
                                        _sageInfo.K6900StartingNumber = TotalCount6850;
                                        if (_sageInfo.BusinessDate.Value.Date > ServerTime.Date)
                                        {
                                            if (MessageBox.Show("Do you want to fill the opening form for future date " + string.Format("{0:dd-MMM-yyyy}", _sageInfo.BusinessDate.Value) + " ?", "Confirm opening form", MessageBoxButton.YesNo) == MessageBoxResult.Yes)
                                            {
                                                string retval = (new SageBusiness()).SaveOpeningForm(_sageInfo, lst6850Printer, lst8810Printer);
                                                string[] splitRet = new string[] { "%##%" };
                                                string[] RetData = retval.Split(splitRet, StringSplitOptions.None);
                                                if (RetData[0] == "Success")
                                                {
                                                    string strmsg = "Opening form filled successfully for " + RetData[1] + " for business date " + string.Format("{0:dd MMM yyyy}", _sageInfo.BusinessDate.Value);
                                                    AuditLog.AddUserLog(Common.LoginUser.UserId, (int)FrameworkHelper.ActionType.OpeningForm, strmsg);
                                                    MessageBox.Show("Opening form filled successfully for business date " + string.Format("{0:dd-MMM-yyyy}", _sageInfo.BusinessDate.Value));
                                                }
                                                else if (retval == "Already Exist")
                                                {
                                                    MessageBox.Show("You have already filled the opening form for selected business date, change the date and try again.");
                                                }
                                                else
                                                {
                                                    MessageBox.Show("There is some error in opening form. Please try again.");
                                                }
                                            }
                                        }
                                        else
                                        {
                                            //here i will put a confirmation box based on the condition
                                            string retval = (new SageBusiness()).SaveOpeningForm(_sageInfo, lst6850Printer, lst8810Printer);
                                            string[] splitRet = new string[] { "%##%" };
                                            string[] RetData = retval.Split(splitRet, StringSplitOptions.None);
                                            if (RetData[0] == "Success")
                                            {
                                                string strmsg = "Opening form filled successfully for " + RetData[1] + " for business date " + string.Format("{0:dd MMM yyyy}", _sageInfo.BusinessDate.Value);
                                                AuditLog.AddUserLog(Common.LoginUser.UserId, (int)FrameworkHelper.ActionType.OpeningForm, strmsg);
                                                MessageBox.Show("Opening form filled successfully for business date " + string.Format("{0:dd-MMM-yyyy}", _sageInfo.BusinessDate.Value));
                                            }
                                            else if (retval == "Already Exist")
                                            {
                                                MessageBox.Show("You have already filled the opening form for selected business date, change the date and try again.");
                                            }
                                            else
                                            {
                                                MessageBox.Show("There is some error in opening form. Please try again.");
                                            }
                                        }
                                    }

                                    //here will save the file to db and move ahead
                                } //if opening is  mendatory and user has filled the opening form then we will push him for closing
                                /* Closing form was not coming as per config utility- code chnages done by Manoj*/
                                /* Closing form was not coming as per config utility- code chnages done by Manoj*/
                                //else if (_sageInfo.FormTypeID == 1 && _sageInfo.TransDate.Value.Date < ServerTime.Date && ((ServerTime.Date - _sageInfo.TransDate.Value.Date).TotalDays > 1 || ServerTime > ClosingFormTime)) //By Vinod
                                else if (_sageInfo.FormTypeID == 1 && _sageInfo.TransDate.Value.Date < ServerTime.Date || (_sageInfo.FormTypeID == 1 && ServerTime > ClosingFormTime)) // Code Added by Anis
                                //else if (_sageInfo.FormTypeID == 1 && _sageInfo.TransDate < ServerTime && ((ServerTime.Date - _sageInfo.TransDate.Value.Date).TotalDays > 1 || ServerTime.TimeOfDay > ts)) //By Manoj
                                //else if (_sageInfo.FormTypeID == 1 && _sageInfo.TransDate < ServerTime && ((ServerTime.Date - _sageInfo.TransDate.Value.Date).TotalDays > 1 || ServerTime.TimeOfDay > ts))
                                {
                                    var Useritem = _objPList.Where(t => t.DG_Permission_Id == 34).FirstOrDefault();
                                    if (Useritem == null)
                                    {
                                        MessageBox.Show("You are not an authorized user to fill closing form, please contact administrator");
                                        return;
                                    }
                                    //AutoCount6850();
                                    //AutoCount8810();
                                    CtrlInventoryConsumables.FromDate = _sageInfo.FilledOn.Value;
                                    CtrlInventoryConsumables.BusinessDate = _sageInfo.TransDate.Value;
                                    CtrlInventoryConsumables.ToDate = ServerTime;
                                    CtrlInventoryConsumables.SubStoreID = LoginUser.SubStoreId;
                                    //this starting number and end number will come from the opening form if opening mendatory otherwise from closing form
                                    CtrlInventoryConsumables.SixEightStartingNumber = _sageInfo.sixEightStartingNumber;
                                    CtrlInventoryConsumables.EightTenStartingNumber = _sageInfo.eightTenStartingNumber;

                                    // BY KCB ON 26 MARCH 2018 FOR storing panorama printer inventry
                                    CtrlInventoryConsumables.SixTwentyStartingNumber = _sageInfo.sixTwentyStartingNumber;
                                    CtrlInventoryConsumables.EightTwentyStartingNumber = _sageInfo.eightTwentyStartingNumber;
                                    //END
                                    CtrlInventoryConsumables.PosterStartingNumber = _sageInfo.PosterStartingNumber;

                                    CtrlInventoryConsumables.EightTenAutoStartingNumber = _sageInfo.eightTenAutoStartingNumber;
                                    CtrlInventoryConsumables.SixEightAutoStartingNumber = _sageInfo.sixEightAutoStartingNumber;
                                    // BY KCB ON 26 MARCH 2018 FOR storing panorama printer inventry
                                    CtrlInventoryConsumables.EightTwemtyAutoStartingNumber = _sageInfo.eightTwentyAutoStartingNumber;
                                    CtrlInventoryConsumables.SixTwentyAutoStartingNumber = _sageInfo.sixTwentyAutoStartingNumber;
                                    //END
                                    CtrlInventoryConsumables.EightTenAutoClosingNumber = TotalCount8810;
                                    CtrlInventoryConsumables.SixEightAutoClosingNumber = TotalCount6850;

                                    var Res1 = CtrlInventoryConsumables.ShowHandlerDialog("Closing Form");
                                    string[] split = new string[] { "%##%" };
                                    string[] data = Res1.Split(split, StringSplitOptions.None);
                                    SageInfoClosing _sageInfoClose = new SageInfoClosing();

                                    List<InventoryConsumables> lstinventoryItem = new List<InventoryConsumables>();
                                    if (Res1 != "")
                                    {
                                        lstinventoryItem = CtrlInventoryConsumables.lstinventoryItem;
                                        _sageInfoClose.sixEightClosingNumber = Convert.ToInt64(data[0]);
                                        _sageInfoClose.eightTenClosingNumber = Convert.ToInt64(data[1]);
                                        _sageInfoClose.PosterClosingNumber = Convert.ToInt64(data[2]);
                                        _sageInfoClose.SixEightWestage = Convert.ToInt64(data[3]);
                                        _sageInfoClose.EightTenWestage = Convert.ToInt64(data[4]);
                                        _sageInfoClose.PosterWestage = Convert.ToInt64(data[5]);
                                        _sageInfoClose.objSubStore = new SubStoresInfo();
                                        _sageInfoClose.objSubStore.DG_SubStore_pkey = LoginUser.SubStoreId;
                                        _sageInfoClose.ClosingDate = ServerTime;
                                        _sageInfoClose.FilledBy = LoginUser.UserId;
                                        _sageInfoClose.FormTypeID = 2;
                                        _sageInfoClose.SixEightPrintCount = CtrlInventoryConsumables.SixEightPrintCount;
                                        _sageInfoClose.EightTenPrintCount = CtrlInventoryConsumables.EightTenPrintCount;
                                        _sageInfoClose.PosterPrintCount = CtrlInventoryConsumables.PosterPrintCount;
                                        _sageInfoClose.TransDate = CtrlInventoryConsumables.BusinessDate;
                                        _sageInfoClose.OpeningDate = _sageInfo.FilledOn.Value;

                                        CtrlOperationalStatistics.BusinessDate = (_sageInfo.TransDate != null && _sageInfo.TransDate != DateTime.MinValue) ? _sageInfo.TransDate.Value : Convert.ToDateTime("1/1/1753");
                                        CtrlOperationalStatistics.FromDate = _sageInfo.FilledOn.Value;

                                        CtrlOperationalStatistics.ToDate = ServerTime;
                                        CtrlOperationalStatistics.SubStoreID = LoginUser.SubStoreId;

                                        var Res2 = CtrlOperationalStatistics.ShowHandlerDialog("Closing Form");
                                        if (Res2 != "")
                                        {
                                            //open = true;
                                            string[] split1 = new string[] { "%##%" };
                                            string[] data1 = Res2.Split(split1, StringSplitOptions.None);
                                            _sageInfoClose.Attendance = Convert.ToInt32(data1[0]);
                                            _sageInfoClose.LaborHour = Convert.ToDecimal(data1[1]);
                                            _sageInfoClose.NoOfCapture = Convert.ToInt64(data1[2]);
                                            _sageInfoClose.NoOfPreview = Convert.ToInt64(data1[3]);
                                            _sageInfoClose.NoOfImageSold = Convert.ToInt64(data1[4]);
                                            _sageInfoClose.Comments = Convert.ToString(data1[5]);
                                            _sageInfoClose.NoOfTransactions = Convert.ToInt32(data1[6]);
                                            //_sageInfoClose.TotalRevenue = Convert.ToDecimal(data1[7]);
                                            string ClosingSyncCode = CommonUtility.GetUniqueSynccode(Convert.ToInt32(ApplicationObjectEnum.ClosingFormDetail).ToString().PadLeft(2, '0'), LoginUser.countrycode, LoginUser.Storecode, LoginUser.SubStoreId.ToString().PadLeft(2, '0'));
                                            _sageInfoClose.SyncCode = ClosingSyncCode;
                                            _sageInfoClose.InventoryConsumable = lstinventoryItem;

                                            SageOpenClose objOpenClose = new SageOpenClose();
                                            objOpenClose.objClose = _sageInfoClose;
                                            bool isSaved = false;
                                            try
                                            {
                                                isSaved = (new SageBusiness()).SaveClosingForm(objOpenClose, lst6850Printer, lst8810Printer);
                                            }
                                            catch (Exception en)
                                            {
                                                MessageBox.Show(en.Message);
                                                ErrorHandler.ErrorHandler.LogError(en);
                                                return;
                                            }
                                            if (isSaved)
                                            {
                                                string filename = "Closing Form" + "_" + string.Format("{0:dd-MMM-yyyy}", _sageInfo.TransDate.Value) + "_" + _sageInfoClose.objSubStore.DG_SubStore_Name;

                                                StoreSubStoreDataBusniess StoreData = new StoreSubStoreDataBusniess();
                                                var store = StoreData.GetStore();
                                                //bool isonline = store.IsOnline;
                                                #region Sync online check - Ashirwad
                                                bool isonline = StoreData.GetSyncIsOnline(LoginUser.SubStoreId);
                                                #endregion
                                                if (!isonline)
                                                {
                                                    string dataXML = string.Empty;
                                                    dataXML = serialize.SerializeObject<SageOpenClose>(objOpenClose);
                                                    string strenc = CryptorEngine.Encrypt(dataXML, true);
                                                    CtrlOfflineClosingForm.encData = strenc;
                                                    CtrlOfflineClosingForm.filename = filename;
                                                    CtrlOfflineClosingForm.ShowHandlerDialog("Offline Closing Form");
                                                }
                                                string strmsg = "Closing form saved successfully for " + _sageInfoClose.objSubStore.DG_SubStore_Name + " for business date " + string.Format("{0:dd MMM yyyy}", _sageInfo.TransDate.Value);
                                                AuditLog.AddUserLog(Common.LoginUser.UserId, (int)FrameworkHelper.ActionType.OpeningForm, strmsg);
                                                MessageBox.Show("Closing form saved successfully for business date " + string.Format("{0:dd-MMM-yyyy}", _sageInfo.TransDate.Value));
                                            }
                                            else
                                            {
                                                MessageBox.Show("There is some error in closing form. Please try again.");
                                            }
                                        }
                                        else if (CtrlOperationalStatistics.Back == 1)
                                        {
                                            Button_Click(sender, e);
                                        }
                                    }
                                }   //if opening is not mendatory and user has filled the opening form then we will push him for closing
                                else if (_sageInfo.BusinessDate.Value.Date == ServerTime.Date && _sageInfo.FormTypeID == 2)
                                {
                                    MessageBox.Show("You have already filled the closing form for business date " + string.Format("{0:dd-MMM-yyyy}" + ", if you want to fill the opening form for next business date please go to open/close procedure section.", _sageInfo.BusinessDate.Value));
                                }
                                else
                                {
                                    open = true;
                                    var Useritem = _objPList.Where(t => t.DG_Permission_Id == 34).FirstOrDefault();
                                    if (Useritem != null)
                                    {
                                        (new ReceiptReprint()).Show();
                                        this.Close();
                                    }
                                    else
                                    {
                                        MessageBox.Show("You are not authorized! please contact admin");
                                        AuditLog.AddUserLog(Common.LoginUser.UserId, 66, "Tried to access Receipt Re-Print");
                                    }
                                }
                            }
                            catch (Exception ex)
                            {
                                ErrorHandler.ErrorHandler.LogError(ex);
                            }

                            break;
                        }

                    case "btnManageDevice":
                        {
                            btnManageDeviceClicked = true;
                            var Useritem = _objPList.Where(t => t.DG_Permission_Id == 33).FirstOrDefault();
                            if (Useritem != null)
                            {
                                ManageDevice _objsubStores = new ManageDevice();
                                _objsubStores.Show();
                                this.Close();
                            }
                            else
                            {
                                MessageBox.Show(UIConstant.NotAuthorized);
                                AuditLog.AddUserLog(Common.LoginUser.UserId, (int)FrameworkHelper.ActionType.TemperAlert, UIConstant.TriedToAccessDeviceManager);
                            }
                            break;
                        }
                    case "btnManageSubstores":
                        {
                            btnManageSubstoresClicked = true;
                            var Useritem = _objPList.Where(t => t.DG_Permission_Id == 22).FirstOrDefault();
                            if (Useritem != null)
                            {
                                ManageSubstores _objsubStores = new ManageSubstores();
                                _objsubStores.Show();
                                this.Close();
                            }
                            else
                            {
                                MessageBox.Show(UIConstant.NotAuthorized);
                                AuditLog.AddUserLog(Common.LoginUser.UserId, (int)FrameworkHelper.ActionType.TemperAlert, UIConstant.TriedToAccessPrintManager);
                            }
                            break;
                        }
                    case "btnPrntMgr":
                        {
                            btnPrntMgrClicked = true;
                            var Useritem = _objPList.Where(t => t.DG_Permission_Id == 6).FirstOrDefault();
                            if (Useritem != null)
                            {
                                ManagePrinter _objprinter = new ManagePrinter();
                                _objprinter.Show();
                                this.Close();
                            }
                            else
                            {
                                MessageBox.Show(UIConstant.NotAuthorized);
                                AuditLog.AddUserLog(Common.LoginUser.UserId, (int)FrameworkHelper.ActionType.TemperAlert, UIConstant.TriedToAccessPrintManager);
                            }
                            break;
                        }
                    case "btnConfig":
                        {
                            btnConfigClicked = true;
                            var Useritem = _objPList.Where(t => t.DG_Permission_Id == 2).FirstOrDefault();
                            var Useritem2 = _objPList.Where(t => t.DG_Permission_Id == 5).FirstOrDefault();
                            var Useritem3 = _objPList.Where(t => t.DG_Permission_Id == 7).FirstOrDefault();
                            var Useritem4 = _objPList.Where(t => t.DG_Permission_Id == 8).FirstOrDefault();
                            var Useritem5 = _objPList.Where(t => t.DG_Permission_Id == 9).FirstOrDefault();
                            var Useritem6 = _objPList.Where(t => t.DG_Permission_Id == 13).FirstOrDefault();
                            var Useritem7 = _objPList.Where(t => t.DG_Permission_Id == 14).FirstOrDefault();
                            if (Useritem != null || Useritem2 != null || Useritem3 != null || Useritem4 != null || Useritem5 != null || Useritem6 != null || Useritem7 != null)
                            {
                                Configuration _objconfig = Configuration.Instance;// new Configuration();
                                _objconfig.tbmain.SelectedIndex = 0;
                                _objconfig.Show();
                                this.Close();
                            }

                            else
                            {
                                MessageBox.Show(UIConstant.NotAuthorized);
                                AuditLog.AddUserLog(Common.LoginUser.UserId, (int)FrameworkHelper.ActionType.TemperAlert, UIConstant.TriedToAccessAonfiguration);
                            }
                            break;
                        }
                    case "btnUsrMgmt":
                        {
                            btnUsrMgmtClicked = true;
                            //var Useritem = (new RolePermissionsBusniess()).GetChildUserData(LoginUser.RoleId);
                            var Useritem = _objPList.Where(t => t.DG_Permission_Id == 4).FirstOrDefault();
                            if (Useritem != null)
                            {
                                AddEditUsers _objLogin = new AddEditUsers();
                                _objLogin.Show();
                                this.Close();
                            }
                            else
                            {
                                MessageBox.Show(UIConstant.NotAuthorized);
                                AuditLog.AddUserLog(Common.LoginUser.UserId, (int)FrameworkHelper.ActionType.TemperAlert, UIConstant.TriedToAccessUserManagement);
                            }
                            break;
                        }

                    case "btncashbox":
                        {
                            btncashboxClicked = true;
                            var Useritem = _objPList.Where(t => t.DG_Permission_Id == 35).FirstOrDefault();
                            if (Useritem != null)
                            {
                                try
                                {
                                    mycontrol.Visibility = Visibility.Visible;
                                    brdMain.IsEnabled = false;
                                }
                                catch (Exception ex)
                                {
                                    string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                                    ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                                }
                            }
                            else
                            {
                                MessageBox.Show(UIConstant.NotAuthorized);
                                AuditLog.AddUserLog(Common.LoginUser.UserId, (int)FrameworkHelper.ActionType.TemperAlert, UIConstant.TriedToAccessCashBox);
                            }
                            break;
                        }
                    case "btnRemapPrinters":
                        {
                            btnRemapPrintersClicked = true;
                            if (DeleteAllPrinters())
                            {
                                AddAvailablePrinters();
                                AuditLog.AddUserLog(Common.LoginUser.UserId, (int)FrameworkHelper.ActionType.RemapPrinter, "Printer Remapped on :- ");
                                MessageBox.Show("Printer list refreshed successfully.", "DigiPhoto", MessageBoxButton.OK, MessageBoxImage.Information);
                            }
                            else
                                MessageBox.Show("Printer list could not be cleared!", "DigiPhoto", MessageBoxButton.OK, MessageBoxImage.Error);

                            break;
                        }
                    case "btnHelp":
                        btnHelpClicked = true;
                        string currentPath = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
                        string filePath = currentPath + "\\Help\\i-MixUserManual.chm";
                        System.Diagnostics.Process.Start(filePath);
                        // System.Windows.Forms.Help.ShowHelp(this, @"\Help\i-MixUserManual.chm","Digiphoto");
                        break;
                    case "btnHealthChkup":
                        {
                            btnHealthChkupClicked = true;
                            SoftwareHealthCheckup _objSHC = new SoftwareHealthCheckup();
                            _objSHC.Show();
                            this.Close();
                            break;
                        }
                    case "btnFinancialReport":
                        {
                            btnFinancialReportClicked = true;
                            var Useritem = _objPList.Where(t => t.DG_Permission_Id == 10).FirstOrDefault();
                            if (Useritem != null)
                            {
                                //ManageReport _objManageReport = new ManageReport();
                                //_objManageReport.Show();
                                //this.Close();
                                Reports.ActivityReport AR = new Reports.ActivityReport();
                                this.Hide();
                                AR.Show();
                            }
                            else
                            {
                                MessageBox.Show("You are not authorized! please contact admin");
                                AuditLog.AddUserLog(Common.LoginUser.UserId, (int)FrameworkHelper.ActionType.TemperAlert, "Tried to access Financial Report");
                            }
                            break;
                        }
                    case "btnCleargroup":
                        {
                            btnCleargroupClicked = true;
                            var Useritem = _objPList.Where(t => t.DG_Permission_Id == 25).FirstOrDefault();
                            //int olderthandays = Convert.ToInt32(ConfigurationManager.AppSettings["DeletedOldImages"]);
                            int deletedOldImagesValue = (new ConfigBusiness()).GetDeletedOldImagesConfigurationData("DeletedOldImages"); //Abhishek
                            int olderthandays = deletedOldImagesValue;
                            Console.WriteLine("DeletedOldImages =" + olderthandays);
                            if (Useritem != null)
                            {
                                string value = MessageBox.Show("Do you want to clear all groups?", "Confirm Clear Group", MessageBoxButton.YesNo, MessageBoxImage.Question).ToString();
                                if (value == "Yes")
                                {
                                    PhotoBusiness phoBiz = new PhotoBusiness();
                                    if (phoBiz.TruncatePhotoGroupTable(olderthandays, LoginUser.SubStoreId))
                                    {
                                        MessageBox.Show("All groups cleared succesfully!");
                                    }
                                    else
                                    {
                                        MessageBox.Show("Error in clearing groups!");
                                    }
                                }

                            }
                            else
                            {
                                MessageBox.Show("You are not authorized! please contact admin");
                                AuditLog.AddUserLog(Common.LoginUser.UserId, (int)FrameworkHelper.ActionType.TemperAlert, "Tried to access Clear Group");
                            }
                            break;
                        }
                    case "btnManageStores":
                        {
                            btnManageStoresClicked = true;
                            break;
                        }
                    case "btnMinimizeApp":
                        {
                            btnMinimizeAppClicked = true;
                            var Useritem = _objPList.Where(t => t.DG_Permission_Id == 24).FirstOrDefault();
                            if (Useritem != null)
                            {
                                string value = MessageBox.Show("Do you want to minimize the application?", "Confirm Minimize", MessageBoxButton.YesNo, MessageBoxImage.Question).ToString();
                                if (value == "Yes")
                                {
                                    try
                                    {
                                        ReleaseKeyboardHook();
                                        EnableDisableTaskManager(true); //Modified by Nirjhar
                                        AuditLog.AddUserLog(Common.LoginUser.UserId, (int)FrameworkHelper.ActionType.MinimizeToWindows, "Minimized to Windows on :- ");
                                        Taskbar.Show();
                                        this.WindowState = WindowState.Minimized;

                                    }
                                    catch (Exception ex)
                                    {
                                        string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                                        ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                                    }

                                }

                            }
                            else
                            {
                                //if (MessageBox.Show("Are you sure, you want to shutdown the system?", "Confirm shutdown", MessageBoxButton.YesNo,MessageBoxImage.Question) == MessageBoxResult.Yes)
                                //{
                                //    Shutdown();
                                //    //ForceShutDown();
                                //}
                                MessageBox.Show("You are not authorized! please contact admin");

                                AuditLog.AddUserLog(Common.LoginUser.UserId, (int)FrameworkHelper.ActionType.TemperAlert, "Tried to access Shut down at :- ");
                            }

                            break;
                        }

                    case "btnShutDown":
                        {
                            btnShutDownClicked = true;
                            var Useritem = _objPList.Where(t => t.DG_Permission_Id == 11).FirstOrDefault();
                            if (Useritem != null)
                            {
                                string value = MessageBox.Show("Do you want to shutdown?", "Confirm shutdown", MessageBoxButton.YesNo, MessageBoxImage.Question).ToString();
                                if (value == "Yes")
                                {
                                    if (System.Diagnostics.Process.GetProcessesByName("PhotoPrinting").Count() > 0)
                                    {
                                        foreach (System.Diagnostics.Process p in System.Diagnostics.Process.GetProcessesByName("PhotoPrinting"))
                                        {
                                            p.Kill();
                                        }
                                    }
                                    Shutdown();

                                    //ForceShutDown();
                                }

                            }
                            else
                            {
                                //if (MessageBox.Show("Are you sure, you want to shutdown the system?", "Confirm shutdown", MessageBoxButton.YesNo,MessageBoxImage.Question) == MessageBoxResult.Yes)
                                //{
                                //    Shutdown();
                                //    //ForceShutDown();
                                //}
                                MessageBox.Show("You are not authorized! please contact admin");
                                AuditLog.AddUserLog(Common.LoginUser.UserId, (int)FrameworkHelper.ActionType.TemperAlert, "Tried to access Shut down at :- ");
                            }

                            break;
                        }
                    case "btnLogout":
                        {
                            btnLogoutClicked = true;
                            AuditLog.AddUserLog(Common.LoginUser.UserId, (int)FrameworkHelper.ActionType.Logout, "Logged out at ");
                            Login _objLogin = new Login();
                            _objLogin.Show();
                            this.Close();
                            break;
                        }
                    case "btnBack":
                        {
                            btnBackWasClicked = true;
                            GetMktImgInfo();
                            ClientView window = null;

                            foreach (Window wnd in Application.Current.Windows)
                            {
                                if (wnd.Title == "ClientView")
                                {
                                    window = (ClientView)wnd;
                                }
                            }

                            if (window == null)
                            {

                                window = new ClientView();
                                window.WindowStartupLocation = System.Windows.WindowStartupLocation.Manual;

                            }


                            window.GroupView = false;
                            window.DefaultView = false;


                            if (!(_MktImgPath == "" || _mktImgTime == 0))
                            {
                                window.instructionVideo.Visibility = System.Windows.Visibility.Visible;
                                window.instructionVideo.Play();
                                window.instructionVideoBlur.Visibility = System.Windows.Visibility.Visible;
                                window.instructionVideoBlur.Play();
                            }
                            else
                            {
                                window.imgDefault.Visibility = System.Windows.Visibility.Visible;
                                window.imgDefaultBlur.Visibility = System.Windows.Visibility.Visible; // Added for Blur Iamges
                            }
                            window.testR.Fill = null;
                            window.DefaultView = true;
                            if (window.instructionVideo.Visibility == System.Windows.Visibility.Visible)
                                window.instructionVideo.Play();
                            else
                                window.instructionVideo.Pause();

                            if (window.instructionVideoBlur.Visibility == System.Windows.Visibility.Visible)
                                window.instructionVideoBlur.Play();
                            else
                                window.instructionVideoBlur.Pause();

                            Home _objHome = new Home();
                            if (_objHome.Visibility == Visibility.Collapsed)
                            {
                                _objHome.Show();
                            }
                            this.Close();
                            break;
                        }
                    case "btnManageCamera":
                        {

                            btnManageCameraClicked = true;
                            CameraBusiness camBiz = new CameraBusiness();

                            var Useritem = _objPList.Where(t => t.DG_Permission_Id == 12 || t.DG_Permission_Id == camBiz.CheckIsCamDeleteAccess(Convert.ToInt32(LoginUser.RoleId.ToString()))).FirstOrDefault();
                            if (Useritem != null)
                            {
                                ManageCamera _obj = new ManageCamera();
                                _obj.Show();
                                this.Close();
                            }
                            else
                            {
                                MessageBox.Show("You are not authorized! please contact admin");
                                AuditLog.AddUserLog(Common.LoginUser.UserId, (int)FrameworkHelper.ActionType.TemperAlert, "Tried to access Manage Camera at :- ");
                            }
                            break;
                        }
                    case "btnManageProduct":
                        {
                            btnManageProductClicked = true;
                            var Useritem = _objPList.Where(t => t.DG_Permission_Id == 1).FirstOrDefault();
                            if (Useritem != null)
                            {
                                ManageProduct _obj = new ManageProduct();
                                _obj.Show();
                                this.Close();
                            }
                            else
                            {
                                MessageBox.Show("You are not authorized! please contact admin");
                                AuditLog.AddUserLog(Common.LoginUser.UserId, (int)FrameworkHelper.ActionType.TemperAlert, "Tried to access Manage Product at :- ");
                            }
                            break;
                        }
                    case "btnPrinterQueue":
                        {
                            btnPrinterQueueClicked = true;
                            var Useritem = _objPList.Where(t => t.DG_Permission_Id == 15).FirstOrDefault();
                            if (Useritem != null)
                            {
                                ManagePrinting window = null;

                                foreach (Window wnd in Application.Current.Windows)
                                {
                                    if (wnd.Title == "ManagePrinting")
                                    {
                                        window = (ManagePrinting)wnd;
                                    }
                                }

                                if (window == null)
                                {
                                    window = new ManagePrinting();
                                }
                                window.PrintJob = true;
                                window.Show();
                                this.Close();
                            }
                            else
                            {
                                MessageBox.Show("You are not authorized! please contact admin");
                                AuditLog.AddUserLog(Common.LoginUser.UserId, (int)FrameworkHelper.ActionType.TemperAlert, "Tried to access Printer queue at :- ");
                            }
                            break;

                        }
                    case "btnEnableTaskbar":
                        {

                            btnEnableTaskbarClicked = true;
                            break;
                        }


                    case "btnLocations":
                        {
                            btnLocationsClicked = true;
                            var Useritem = _objPList.Where(t => t.DG_Permission_Id == 18).FirstOrDefault();
                            if (Useritem != null)
                            {
                                ManageLocations _objmanageLocation = new ManageLocations();
                                _objmanageLocation.Show();
                                this.Close();
                            }
                            else
                            {
                                MessageBox.Show("You are not authorized! please contact admin");
                                AuditLog.AddUserLog(Common.LoginUser.UserId, (int)FrameworkHelper.ActionType.TemperAlert, "Tried to access Manage Location at :- ");
                            }

                            break;
                        }
                    case "btnRefund":
                        {
                            btnRefundClicked = true;
                            // Adding By Bhagyashree Dhobale 
                            // To Restrict Refund Image Icon if Opening/Closing form is not filled at site 
                            // On 21/07/2017.
                            bool open = false;
                            DateTime ServerTime;
                            TimeSpan ts = new TimeSpan(4, 0, 0);
                            DateTime ClosingFormTime = new DateTime();
                            List<iMIXConfigurationInfo> lstConfig = (new ConfigBusiness()).GetNewConfigValues(LoginUser.SubStoreId);
                            var ClosingProcTime = lstConfig.Where(o => o.IMIXConfigurationMasterId == Convert.ToInt32(ConfigParams.ClosingProcTime)).FirstOrDefault();
                            // ASHU
                            if (ClosingProcTime != null)
                            {
                                string[] arr = ClosingProcTime.ConfigurationValue.ToString().Split(' ');
                                ts = new TimeSpan(Convert.ToInt32(correctHour(ClosingProcTime.ConfigurationValue)), Convert.ToInt32(arr[1]), 0);

                                if (Convert.ToInt32(correctHour(ClosingProcTime.ConfigurationValue)) > 11)
                                {
                                    ClosingFormTime = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, Convert.ToInt32(correctHour(ClosingProcTime.ConfigurationValue)), Convert.ToInt32(arr[1]), 0);
                                }
                                else
                                {
                                    //ClosingFormTime = new DateTime(DateTime.Now.Year, DateTime.Now.Month, (DateTime.Now.Day + 1), Convert.ToInt32(correctHour(ClosingProcTime.ConfigurationValue)), Convert.ToInt32(arr[1]), 0);
                                    ClosingFormTime = DateTime.Now.AddDays(1).Date.AddHours(Convert.ToInt32(correctHour(ClosingProcTime.ConfigurationValue))).AddMinutes(Convert.ToInt32(arr[1]));
                                }

                            }

                            SageInfo _sageInfo = (new SageBusiness()).GetOpenCloseProcDetail(LoginUser.SubStoreId);
                            if (_sageInfo == null)
                                _sageInfo = new SageInfo();
                            ServerTime = _sageInfo.ServerTime;
                            string OpeningSyncCode = CommonUtility.GetUniqueSynccode(Convert.ToInt32(ApplicationObjectEnum.OpeningFormDetail).ToString().PadLeft(2, '0'), LoginUser.countrycode, LoginUser.Storecode, LoginUser.SubStoreId.ToString().PadLeft(2, '0'));

                            //if opening is  mendatory then we will push him for opening
                            if (_sageInfo.FormTypeID != 1 && _sageInfo.TransDate.Value.Date < ServerTime.Date)
                            {
                                var Useritem = _objPList.Where(t => t.DG_Permission_Id == 16).FirstOrDefault();
                                if (Useritem == null)
                                {
                                    MessageBox.Show("You are not an authorized user to fill opening form, Please contact administrator");
                                    return;
                                }

                                //AutoCount6850();
                                //AutoCount8810();
                                CtrlOpeningForm.FromDate = ServerTime.Date;

                                CtrlOpeningForm.printAutoStart6850 = TotalCount6850;
                                CtrlOpeningForm.printAutoStart8810 = TotalCount8810;
                                // BY KCB ON 08 AUG 2019 FOR storing panorama printer inventory
                                K6900Printer.K6900Driver k6900Driver = new K6900Printer.K6900Driver();
                                string[] serialNumbers = new string[1];
                                if (k6900Driver.GetSerialNumbers(out serialNumbers) == true)
                                {

                                    K6900Printer.Entity.PrintCountInfo printCountInfo;
                                    if (k6900Driver.GetPrintCountInfo(serialNumbers[0], out printCountInfo))
                                    {
                                        if (printCountInfo != null)
                                        {
                                            TotalCount6900 = printCountInfo.Cutter;
                                            CtrlOpeningForm.printAutoStart6900 = TotalCount6900;
                                        }
                                    }
                                }
                                //END 
                                var res = CtrlOpeningForm.ShowHandlerDialog("Opening Form");
                                if (res != "")
                                {
                                    open = true;
                                    //here save the data to db after operation and move ahead
                                    string[] split = new string[] { "%##%" };
                                    string[] data = res.Split(split, StringSplitOptions.None);
                                    _sageInfo = new SageInfo();
                                    _sageInfo.sixEightStartingNumber = Convert.ToInt64(data[0]);
                                    _sageInfo.eightTenStartingNumber = Convert.ToInt64(data[1]);
                                    _sageInfo.sixTwentyStartingNumber = Convert.ToInt64(data[2]);
                                    _sageInfo.eightTwentyStartingNumber = Convert.ToInt64(data[3]);
                                    _sageInfo.PosterStartingNumber = Convert.ToInt64(data[4]);
                                    _sageInfo.CashFloatAmount = Convert.ToDecimal(data[5]);
                                    _sageInfo.BusinessDate = Convert.ToDateTime(data[6]);
                                    _sageInfo.K6900StartingNumber = Convert.ToInt64(data[7]); // BY KCB ON 08 AUG 2019 FOR storing panorama printer inventory

                                    _sageInfo.SubStoreID = LoginUser.SubStoreId;
                                    _sageInfo.OpeningDate = ServerTime;
                                    _sageInfo.FilledBy = LoginUser.UserId;
                                    _sageInfo.FormTypeID = 1;
                                    _sageInfo.SyncCode = OpeningSyncCode;
                                    _sageInfo.eightTenAutoStartingNumber = TotalCount8810;
                                    _sageInfo.sixEightAutoStartingNumber = TotalCount6850;
                                    _sageInfo.K6900AutoStartingNumber = TotalCount6900;// BY KCB ON 08 AUG 2019 FOR storing panorama printer inventory

                                    if (_sageInfo.BusinessDate.Value.Date > ServerTime.Date)
                                    {
                                        if (MessageBox.Show("Do you want to fill the opening form for future date " + string.Format("{0:dd-MMM-yyyy}", _sageInfo.BusinessDate.Value) + " ?", "Confirm opening form", MessageBoxButton.YesNo) == MessageBoxResult.Yes)
                                        {
                                            string retval = (new SageBusiness()).SaveOpeningForm(_sageInfo, lst6850Printer, lst8810Printer);
                                            string[] splitRet = new string[] { "%##%" };
                                            string[] RetData = retval.Split(splitRet, StringSplitOptions.None);
                                            if (RetData[0] == "Success")
                                            {
                                                string strmsg = "Opening form filled successfully for " + RetData[1] + " for business date " + string.Format("{0:dd MMM yyyy}", _sageInfo.BusinessDate.Value);
                                                AuditLog.AddUserLog(Common.LoginUser.UserId, (int)FrameworkHelper.ActionType.OpeningForm, strmsg);
                                                MessageBox.Show("Opening form filled successfully for business date " + string.Format("{0:dd-MMM-yyyy}", _sageInfo.BusinessDate.Value));
                                            }
                                            else if (retval == "Already Exist")
                                            {
                                                MessageBox.Show("You have already filled the opening form for selected business date, change the date and try again.");
                                            }
                                            else
                                            {
                                                MessageBox.Show("There is some error in opening form. Please try again.");
                                            }
                                        }
                                    }
                                    else
                                    {
                                        //here i will put a confirmation box based on the condition
                                        string retval = (new SageBusiness()).SaveOpeningForm(_sageInfo, lst6850Printer, lst8810Printer);
                                        string[] splitRet = new string[] { "%##%" };
                                        string[] RetData = retval.Split(splitRet, StringSplitOptions.None);
                                        if (RetData[0] == "Success")
                                        {
                                            string strmsg = "Opening form filled successfully for " + RetData[1] + " for business date " + string.Format("{0:dd MMM yyyy}", _sageInfo.BusinessDate.Value);
                                            AuditLog.AddUserLog(Common.LoginUser.UserId, (int)FrameworkHelper.ActionType.OpeningForm, strmsg);
                                            MessageBox.Show("Opening form filled successfully for business date " + string.Format("{0:dd-MMM-yyyy}", _sageInfo.BusinessDate.Value));
                                        }
                                        else if (retval == "Already Exist")
                                        {
                                            MessageBox.Show("You have already filled the opening form for selected business date, change the date and try again.");
                                        }
                                        else
                                        {
                                            MessageBox.Show("There is some error in opening form. Please try again.");
                                        }
                                    }
                                }

                                //here will save the file to db and move ahead
                            } //if opening is  mendatory and user has filled the opening form then we will push him for closing

                            //By KCB ON 28 JUL 2018 For showing closing form as per ClosingProcTime
                            //else if (_sageInfo.FormTypeID == 1 && _sageInfo.TransDate.Value.Date < ServerTime.Date && ((ServerTime.Date - _sageInfo.TransDate.Value.Date).TotalDays > 1 || ServerTime.TimeOfDay > ts))
                            else if (_sageInfo.FormTypeID == 1 && _sageInfo.TransDate.Value.Date < ServerTime.Date || (_sageInfo.FormTypeID == 1 && ServerTime > ClosingFormTime))
                            //else if (_sageInfo.FormTypeID == 1 && _sageInfo.TransDate.Value.Date < ServerTime.Date && ((ServerTime.Date - _sageInfo.TransDate.Value.Date).TotalDays > 1 || ServerTime.TimeOfDay > ts))                            
                            {
                                var Useritem = _objPList.Where(t => t.DG_Permission_Id == 16).FirstOrDefault();
                                if (Useritem == null)
                                {
                                    MessageBox.Show("You are not an authorized user to fill closing form, please contact administrator");
                                    return;
                                }
                                //AutoCount6850();
                                //AutoCount8810();
                                CtrlInventoryConsumables.FromDate = _sageInfo.FilledOn.Value;
                                CtrlInventoryConsumables.BusinessDate = _sageInfo.TransDate.Value;
                                CtrlInventoryConsumables.ToDate = ServerTime;
                                CtrlInventoryConsumables.SubStoreID = LoginUser.SubStoreId;
                                //this starting number and end number will come from the opening form if opening mendatory otherwise from closing form
                                CtrlInventoryConsumables.SixEightStartingNumber = _sageInfo.sixEightStartingNumber;
                                CtrlInventoryConsumables.EightTenStartingNumber = _sageInfo.eightTenStartingNumber;
                                CtrlInventoryConsumables.PosterStartingNumber = _sageInfo.PosterStartingNumber;

                                CtrlInventoryConsumables.EightTenAutoStartingNumber = _sageInfo.eightTenAutoStartingNumber;
                                CtrlInventoryConsumables.SixEightAutoStartingNumber = _sageInfo.sixEightAutoStartingNumber;
                                CtrlInventoryConsumables.EightTenAutoClosingNumber = TotalCount8810;
                                CtrlInventoryConsumables.SixEightAutoClosingNumber = TotalCount6850;

                                // BY KCB ON 08 AUG 2019 FOR storing panorama printer inventory
                                K6900Printer.K6900Driver k6900Driver = new K6900Printer.K6900Driver();
                                string[] serialNumbers = new string[1];
                                if (k6900Driver.GetSerialNumbers(out serialNumbers) == true)
                                {

                                    K6900Printer.Entity.PrintCountInfo printCountInfo;
                                    if (k6900Driver.GetPrintCountInfo(serialNumbers[0], out printCountInfo))
                                    {
                                        if (printCountInfo != null)
                                        {
                                            TotalCount6900 = printCountInfo.Cutter;
                                            CtrlOpeningForm.printAutoStart6900 = TotalCount6900;
                                        }
                                    }
                                }
                                //END  

                                var Res1 = CtrlInventoryConsumables.ShowHandlerDialog("Closing Form");
                                string[] split = new string[] { "%##%" };
                                string[] data = Res1.Split(split, StringSplitOptions.None);
                                SageInfoClosing _sageInfoClose = new SageInfoClosing();

                                List<InventoryConsumables> lstinventoryItem = new List<InventoryConsumables>();
                                if (Res1 != "")
                                {
                                    lstinventoryItem = CtrlInventoryConsumables.lstinventoryItem;
                                    _sageInfoClose.sixEightClosingNumber = Convert.ToInt64(data[0]);
                                    _sageInfoClose.eightTenClosingNumber = Convert.ToInt64(data[1]);
                                    _sageInfoClose.PosterClosingNumber = Convert.ToInt64(data[2]);
                                    _sageInfoClose.SixEightWestage = Convert.ToInt64(data[3]);
                                    _sageInfoClose.EightTenWestage = Convert.ToInt64(data[4]);
                                    _sageInfoClose.PosterWestage = Convert.ToInt64(data[5]);
                                    _sageInfoClose.objSubStore = new SubStoresInfo();
                                    _sageInfoClose.objSubStore.DG_SubStore_pkey = LoginUser.SubStoreId;
                                    _sageInfoClose.ClosingDate = ServerTime;
                                    _sageInfoClose.FilledBy = LoginUser.UserId;
                                    _sageInfoClose.FormTypeID = 2;
                                    _sageInfoClose.SixEightPrintCount = CtrlInventoryConsumables.SixEightPrintCount;
                                    _sageInfoClose.EightTenPrintCount = CtrlInventoryConsumables.EightTenPrintCount;
                                    _sageInfoClose.PosterPrintCount = CtrlInventoryConsumables.PosterPrintCount;

                                    _sageInfoClose.TransDate = CtrlInventoryConsumables.BusinessDate;
                                    _sageInfoClose.OpeningDate = _sageInfo.FilledOn.Value;

                                    CtrlOperationalStatistics.BusinessDate = (_sageInfo.TransDate != null && _sageInfo.TransDate != DateTime.MinValue) ? _sageInfo.TransDate.Value : Convert.ToDateTime("1/1/1753");
                                    CtrlOperationalStatistics.FromDate = _sageInfo.FilledOn.Value;

                                    CtrlOperationalStatistics.ToDate = ServerTime;
                                    CtrlOperationalStatistics.SubStoreID = LoginUser.SubStoreId;

                                    var Res2 = CtrlOperationalStatistics.ShowHandlerDialog("Closing Form");
                                    if (Res2 != "")
                                    {
                                        //open = true;
                                        string[] split1 = new string[] { "%##%" };
                                        string[] data1 = Res2.Split(split1, StringSplitOptions.None);
                                        _sageInfoClose.Attendance = Convert.ToInt32(data1[0]);
                                        _sageInfoClose.LaborHour = Convert.ToDecimal(data1[1]);
                                        _sageInfoClose.NoOfCapture = Convert.ToInt64(data1[2]);
                                        _sageInfoClose.NoOfPreview = Convert.ToInt64(data1[3]);
                                        _sageInfoClose.NoOfImageSold = Convert.ToInt64(data1[4]);
                                        _sageInfoClose.Comments = Convert.ToString(data1[5]);
                                        _sageInfoClose.NoOfTransactions = Convert.ToInt32(data1[6]);
                                        //_sageInfoClose.TotalRevenue = Convert.ToDecimal(data1[7]);
                                        string ClosingSyncCode = CommonUtility.GetUniqueSynccode(Convert.ToInt32(ApplicationObjectEnum.ClosingFormDetail).ToString().PadLeft(2, '0'), LoginUser.countrycode, LoginUser.Storecode, LoginUser.SubStoreId.ToString().PadLeft(2, '0'));
                                        _sageInfoClose.SyncCode = ClosingSyncCode;
                                        _sageInfoClose.InventoryConsumable = lstinventoryItem;

                                        SageOpenClose objOpenClose = new SageOpenClose();
                                        objOpenClose.objClose = _sageInfoClose;
                                        bool isSaved = false;
                                        try
                                        {
                                            isSaved = (new SageBusiness()).SaveClosingForm(objOpenClose, lst6850Printer, lst8810Printer);
                                        }
                                        catch (Exception en)
                                        {
                                            MessageBox.Show(en.Message);
                                            ErrorHandler.ErrorHandler.LogError(en);
                                            return;
                                        }
                                        if (isSaved)
                                        {
                                            string filename = "Closing Form" + "_" + string.Format("{0:dd-MMM-yyyy}", _sageInfo.TransDate.Value) + "_" + _sageInfoClose.objSubStore.DG_SubStore_Name;

                                            StoreSubStoreDataBusniess StoreData = new StoreSubStoreDataBusniess();
                                            var store = StoreData.GetStore();
                                            //bool isonline = store.IsOnline;
                                            bool isonline = StoreData.GetSyncIsOnline(LoginUser.SubStoreId);

                                            if (!isonline)
                                            {
                                                string dataXML = string.Empty;
                                                dataXML = serialize.SerializeObject<SageOpenClose>(objOpenClose);
                                                string strenc = CryptorEngine.Encrypt(dataXML, true);
                                                CtrlOfflineClosingForm.encData = strenc;
                                                CtrlOfflineClosingForm.filename = filename;
                                                CtrlOfflineClosingForm.ShowHandlerDialog("Offline Closing Form");
                                            }
                                            string strmsg = "Closing form saved successfully for " + _sageInfoClose.objSubStore.DG_SubStore_Name + " for business date " + string.Format("{0:dd MMM yyyy}", _sageInfo.TransDate.Value);
                                            AuditLog.AddUserLog(Common.LoginUser.UserId, (int)FrameworkHelper.ActionType.OpeningForm, strmsg);
                                            MessageBox.Show("Closing form saved successfully for business date " + string.Format("{0:dd-MMM-yyyy}", _sageInfo.TransDate.Value));
                                        }
                                        else
                                        {
                                            MessageBox.Show("There is some error in closing form. Please try again.");
                                        }
                                    }
                                    else if (CtrlOperationalStatistics.Back == 1)
                                    {
                                        // btnImageDownLoad_Click(sender, e);
                                    }
                                }
                            }   //if opening is not mendatory and user has filled the opening form then we will push him for closing
                            else if (_sageInfo.BusinessDate.Value.Date == ServerTime.Date && _sageInfo.FormTypeID == 2)
                            {
                                MessageBox.Show("You have already filled the closing form for business date " + string.Format("{0:dd-MMM-yyyy}" + ", if you want to fill the opening form for next business date please go to open/close procedure section.", _sageInfo.BusinessDate.Value));
                            }
                            //else
                            //{
                            //    open = true;
                            //   // ImageDownload();
                            //}

                            else
                            //if (CtrlOperationalStatistics.Back == 1)
                            {
                                // By default code
                                var Useritem = _objPList.Where(t => t.DG_Permission_Id == 16).FirstOrDefault();
                                if (Useritem != null)
                                {
                                    OrderRefund _objnew = new OrderRefund();
                                    _objnew.Show();
                                    this.Close();
                                }
                                else
                                {
                                    MessageBox.Show("You are not authorized! please contact admin");
                                    AuditLog.AddUserLog(Common.LoginUser.UserId, (int)FrameworkHelper.ActionType.TemperAlert, "Tried to access Refund Module at :- ");
                                }
                                // break;

                            }
                            break;
                        }
                    case "btnSystemprinterqueue":
                        {
                            btnSystemprinterqueueClicked = true;
                            var Useritem = _objPList.Where(t => t.DG_Permission_Id == 23).FirstOrDefault();
                            if (Useritem != null)
                            {
                                SystemPrinterQueuee _objnew = new SystemPrinterQueuee();
                                _objnew.Show();
                                this.Close();
                            }
                            else
                            {
                                MessageBox.Show("You are not authorized! please contact admin");
                                AuditLog.AddUserLog(Common.LoginUser.UserId, (int)FrameworkHelper.ActionType.TemperAlert, "Tried to access System Printer queue Module at :- ");
                            }
                            break;
                        }
                    case "btnBurnOrders":
                        {
                            btnBurnOrdersClicked = true;
                            RestoreMinimizeBurnOrders();
                            break;
                        }

                    case "btnCardType":
                        {
                            btnCardTypeClicked = true;
                            //var Useritem = _objPList.Where(t => t.DG_Permission_Id == 23).FirstOrDefault();
                            //if (Useritem != null)
                            //{
                            CardType _objnew = new CardType();
                            _objnew.Show();
                            this.Close();
                            //}
                            //else
                            //{
                            //    MessageBox.Show("You are not authorized! please contact admin");
                            //    AuditLog.AddUserLog(Common.LoginUser.UserId, 45, "Tried to access System Printer queue Module at :- " + _objdblayer.ServerDateTime().ToString());
                            //}
                            break;
                        }


                    case "btnsynstatus":
                        {
                            btnsynstatusClicked = true;
                            var Useritem = _objPList.Where(t => t.DG_Permission_Id == 32).FirstOrDefault();
                            if (Useritem != null)
                            {
                                SyncStatus _objnew = new SyncStatus();
                                _objnew.Show();
                                this.Close();
                            }
                            else
                            {
                                MessageBox.Show("You are not authorized! please contact admin");
                                AuditLog.AddUserLog(Common.LoginUser.UserId, (int)FrameworkHelper.ActionType.TemperAlert, "Tried to access SyncStatus Module at :- ");
                            }
                            break;
                        }
                    case "btnRFIDMatch":
                        {
                            btnRFIDMatchClicked = true;
                            RFIDMapping _obj = new RFIDMapping();
                            _obj.Show();
                            this.Close();
                            break;
                        }
                    //bikram
                    case "btnEmailStatus":
                        {
                            btnEmailStatusClicked = true;
                            var Useritem = _objPList.Where(t => t.DG_Permission_Id == 39).FirstOrDefault();
                            if (Useritem != null)
                            {
                                EmailStatus _objnew = new EmailStatus();
                                _objnew.Show();
                                this.Close();
                            }
                            else
                            {
                                MessageBox.Show("You are not authorized! please contact admin");
                                AuditLog.AddUserLog(Common.LoginUser.UserId, (int)FrameworkHelper.ActionType.TemperAlert, "Tried to access SyncStatus Module at :- ");
                            }
                            break;
                        }
                    case "btnCurrencyExchange":
                        {
                            btnCurrencyExchangeClicked = true;
                            var Useritem = _objPList.Where(t => t.DG_Permission_Id == 43).FirstOrDefault();
                            if (Useritem != null)
                            {
                                ManageCurrencyProfile _objCurrencyProfile = new ManageCurrencyProfile();
                                _objCurrencyProfile.Show();
                                this.Close();
                            }
                            else
                            {
                                MessageBox.Show(UIConstant.NotAuthorized);
                                AuditLog.AddUserLog(Common.LoginUser.UserId, (int)FrameworkHelper.ActionType.TemperAlert, "Tried to access Manage Currency Module at :- ");
                            }
                            break;
                        }
                    case "btnUpldMasterData":
                        {
                            btnUpldMasterDataClicked = true;
                            var Useritem = _objPList.Where(t => t.DG_Permission_Id == 44).FirstOrDefault();
                            if (Useritem != null)
                            {
                                ManageMasterData _objMasterData = new ManageMasterData();
                                _objMasterData.Show();
                                this.Close();
                            }
                            else
                            {
                                MessageBox.Show(UIConstant.NotAuthorized);
                                AuditLog.AddUserLog(Common.LoginUser.UserId, (int)FrameworkHelper.ActionType.TemperAlert, "Tried to access Manage Currency Module at :- ");
                            }
                            break;
                        }
                    case "btnDownloadClosingForm":
                        {
                            btnDownloadClosingFormClicked = true;
                            var Useritem = _objPList.Where(t => t.DG_Permission_Id == 45).FirstOrDefault();
                            if (Useritem != null)
                            {
                                DownloadClosingForm _objDownloadClosingForm = new DownloadClosingForm();
                                _objDownloadClosingForm.Show();
                                this.Close();
                            }
                            else
                            {
                                MessageBox.Show(UIConstant.NotAuthorized);
                                AuditLog.AddUserLog(Common.LoginUser.UserId, (int)FrameworkHelper.ActionType.TemperAlert, "Tried to access Manage Currency Module at :- ");
                            }
                            break;
                        }

                }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }
        #endregion
        /// <summary>
        /// Function to show the Burn Orders in the window.
        /// This function restores the width and height of the window if it has been set to 0.
        /// </summary>
        private void RestoreMinimizeBurnOrders()
        {
            Window window = null;
            foreach (Window wnd in Application.Current.Windows)
            {
                if (wnd.Title == "BurnMedia")
                {
                    window = (Orders.BurnMediaOrderList)wnd;
                    wnd.Height = 730;
                    wnd.Width = 900;
                    wnd.WindowState = System.Windows.WindowState.Maximized;
                    wnd.Show();
                    break;
                }
            }
            if (window == null)
            {
                Orders.BurnMediaOrderList bm = new Orders.BurnMediaOrderList();
                bm.Height = 730;
                bm.Width = 900;
                bm.WindowState = System.Windows.WindowState.Maximized;
                bm.Show();
            }
        }

        #region Common Methods
        /// <summary>
        /// Shutdowns this instance.
        /// </summary>
        void Shutdown()
        {
            try
            {
                //int count = 0;
                object SystemName = "";
                string hostName = Dns.GetHostName(); // Retrive the Name of HOST
                string myIP = Dns.GetHostByName(hostName).AddressList[0].ToString();//ip              
                ManagementObjectSearcher searcher =
                    new ManagementObjectSearcher("root\\CIMV2",
                    "SELECT Name FROM Win32_ComputerSystem");

                foreach (ManagementObject queryObj in searcher.Get())
                {
                    SystemName = queryObj["Name"];
                }
                ManagementObjectSearcher search = new ManagementObjectSearcher("SELECT * FROM Win32_NetworkAdapterConfiguration where IPEnabled=true");
                IEnumerable<ManagementObject> objects = search.Get().Cast<ManagementObject>();
                string mac = (from o in objects orderby o["IPConnectionMetric"] select o["MACAddress"].ToString()).FirstOrDefault();

                ServicePosInfoBusiness servicePosInfoBusiness = new ServicePosInfoBusiness();
                string systemname = SystemName.ToString();
                servicePosInfoBusiness.StartStopSystemBusiness(mac, myIP, systemname, 0);
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
            ManagementBaseObject mboShutdown = null;
            ManagementClass mcWin32 = new ManagementClass("Win32_OperatingSystem");
            mcWin32.Get();          // You can't shutdown without security privileges         
            mcWin32.Scope.Options.EnablePrivileges = true;
            ManagementBaseObject mboShutdownParams = mcWin32.GetMethodParameters("Win32Shutdown");
            // Flag 1 means we want to shut down the system. Use "2" to reboot.         
            mboShutdownParams["Flags"] = "1";
            mboShutdownParams["Reserved"] = "0";
            foreach (ManagementObject manObj in mcWin32.GetInstances())
            {
                mboShutdown = manObj.InvokeMethod("Win32Shutdown", mboShutdownParams, null);
            }


        }
        /// <summary>
        /// Forces the shut down.
        /// </summary>
        private void ForceShutDown()
        {
            Process[] runningProcesses = Process.GetProcesses();
            foreach (Process process in runningProcesses)
            {
                process.Kill();
            }
            Process.Start("shutdown", "/s /t 0");
        }

        #endregion

        #region Common Methods For Locking
        /// <summary>
        /// Enables the disable task manager.
        /// </summary>
        /// <param name="enable">if set to <c>true</c> [enable].</param>
        private static void EnableDisableTaskManager(bool enable)
        {
            Microsoft.Win32.RegistryKey HKCU = Microsoft.Win32.Registry.CurrentUser;
            Microsoft.Win32.RegistryKey key = HKCU.CreateSubKey(@"Software\Microsoft\Windows\CurrentVersion\Policies\System");
            key.SetValue("DisableTaskMgr", enable ? 0 : 1, Microsoft.Win32.RegistryValueKind.DWord);
        }

        /// <summary>
        /// Lows the level keyboard proc.
        /// </summary>
        /// <param name="nCode">The asynchronous code.</param>
        /// <param name="wParam">The forward parameter.</param>
        /// <param name="lParam">The calculate parameter.</param>
        /// <returns></returns>
        private int LowLevelKeyboardProc(int nCode, int wParam, ref KBDLLHOOKSTRUCT lParam)
        {
            bool blnEat = false;
            switch (wParam)
            {
                case 256:
                case 257:
                case 260:
                case 261:
                    //Alt+Tab, Alt+Esc, Ctrl+Esc, Windows Key
                    if (((lParam.vkCode == 9) && (lParam.flags == 32)) ||
                    ((lParam.vkCode == 27) && (lParam.flags == 32)) || ((lParam.vkCode ==
                    27) && (lParam.flags == 0)) || ((lParam.vkCode == 91) && (lParam.flags
                    == 1)) || ((lParam.vkCode == 92) && (lParam.flags == 1)) || ((true) &&
                    (lParam.flags == 32)))
                    {
                        blnEat = true;
                    }
                    break;
            }
            try
            {
                if (blnEat)
                    return 1;
                else
                {
                    try
                    {
                        int Hook = CallNextHookEx(0, nCode, wParam, ref lParam);
                        return Hook;
                    }
                    catch (Exception)
                    {
                        return 1;
                    }

                }
            }
            catch (Exception ex)
            {
                return 1;
            }

        }
        static LowLevelKeyboardProcDelegate mHookProc;
        private void KeyboardHook(object sender, EventArgs e)
        {
            // NOTE: ensure delegate can't be garbage collected
            mHookProc = new LowLevelKeyboardProcDelegate(LowLevelKeyboardProc);
            // Get handle to main .exe
            IntPtr hModule = GetModuleHandle(IntPtr.Zero);
            // Hook
            intLLKey = SetWindowsHookEx(WH_KEYBOARD_LL, mHookProc, hModule, 0);
            if (intLLKey == IntPtr.Zero)
            {
                MessageBox.Show("Failed to set hook,error = " + Marshal.GetLastWin32Error().ToString());

            }

        }

        /// <summary>
        /// Releases the keyboard hook.
        /// </summary>
        private void ReleaseKeyboardHook()
        {
            intLLKey = UnhookWindowsHookEx(intLLKey);
        }
        #endregion

        /// <summary>
        /// Handles the Click event of the btnPrntMgr control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void btnPrntMgr_Click(object sender, RoutedEventArgs e)
        {

        }

        private void GetMktImgInfo()
        {
            try
            {
                ConfigBusiness conBiz = new ConfigBusiness();
                List<long> objList = new List<long>();
                objList.Add((long)ConfigParams.MktImgPath);
                objList.Add((long)ConfigParams.MktImgTimeInSec);

                List<iMIXConfigurationInfo> ConfigValuesList = conBiz.GetNewConfigValues(LoginUser.SubStoreId).Where(o => objList.Contains(o.IMIXConfigurationMasterId)).ToList();
                if (ConfigValuesList != null || ConfigValuesList.Count > 0)
                {
                    for (int i = 0; i < ConfigValuesList.Count; i++)
                    {
                        switch (ConfigValuesList[i].IMIXConfigurationMasterId)
                        {
                            case (int)ConfigParams.MktImgPath:
                                _MktImgPath = ConfigValuesList[i].ConfigurationValue;
                                break;
                            case (int)ConfigParams.MktImgTimeInSec:
                                _mktImgTime = (ConfigValuesList[i].ConfigurationValue != null ? ConfigValuesList[i].ConfigurationValue.ToInt32() : 10) * 1000;
                                break;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }

        private void AddAvailablePrinters()
        {
            PrinterTypeBusiness AssociatedPrinterBusiness = new PrinterTypeBusiness(); ;
            foreach (string printer in PrinterSettings.InstalledPrinters)
            {
                if (!string.IsNullOrEmpty(printer))
                {
                    bool isPrinterActive = IsPrinterReady(printer);
                    bool result = AssociatedPrinterBusiness.SaveOrActivateNewPrinter(printer, LoginUser.SubStoreId, isPrinterActive);
                }
            }
            AssociatedPrinterBusiness = null;
        }

        private bool DeleteAllPrinters()
        {
            bool result = false;
            try
            {
                PrinterTypeBusiness AssociatedPrinterBusiness = new PrinterTypeBusiness();
                result = AssociatedPrinterBusiness.DeleteAssociatedPrinters(LoginUser.SubStoreId);
                AssociatedPrinterBusiness = null;
            }
            catch (Exception ex)
            {
                result = false;
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
            return result;
        }

        private bool IsPrinterReady(string PrinterName)
        {
            // Set management scope
            bool retvalue = false;
            ManagementScope scope = new ManagementScope("\\root\\cimv2");
            scope.Connect();

            // Select Printers from WMI Object Collections
            ManagementObjectSearcher searcher = new ManagementObjectSearcher("SELECT * FROM Win32_Printer");
            string printerName = "";
            var results = searcher.Get();
            string[] PrinterStatuses =
            {
                "Other", "Unknown", "Idle", "Printing", "WarmUp", "Stopped Printing", "Offline"
            };

            foreach (ManagementObject printer1 in results)
            {
                printerName = printer1["Name"].ToString().ToLower();
                if (printerName.Equals(PrinterName.ToLower()))
                {
                    if (printer1["WorkOffline"].ToString().ToLower().Equals("true"))
                    {
                        // printer is offline 
                        ErrorHandler.ErrorHandler.LogFileWrite(printerName + " is Offline.");
                        retvalue = false;
                    }
                    else
                    {
                        Int32 status = printer1["PrinterStatus"].ToInt32();
                        string printerStatus = PrinterStatuses[status];
                        if (status == Convert.ToInt32(PrinterStatus.Other) || status == Convert.ToInt32(PrinterStatus.Offline))
                        {
                            retvalue = false;
                            string errorMessage = printerName + " is in error (" + printerStatus + ") status : ";
                            ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                        }
                        else
                        {
                            retvalue = true;
                        }
                    }
                }
            }
            return retvalue;
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            try
            {
                if (btnBackWasClicked || btnReceiptReprintClicked || btnManageDeviceClicked || btnManageSubstoresClicked || btnPrntMgrClicked
                    || btnConfigClicked || btnUsrMgmtClicked || btncashboxClicked || btnRemapPrintersClicked || btnHelpClicked
                    || btnHealthChkupClicked || btnFinancialReportClicked || btnCleargroupClicked || btnManageStoresClicked || btnShutDownClicked
                    || btnLogoutClicked || btnManageCameraClicked || btnManageProductClicked || btnPrinterQueueClicked || btnEnableTaskbarClicked
                    || btnLocationsClicked || btnRefundClicked || btnSystemprinterqueueClicked || btnBurnOrdersClicked || btnCardTypeClicked
                    || btnsynstatusClicked || btnRFIDMatchClicked || btnEmailStatusClicked || btnCurrencyExchangeClicked || btnUpldMasterDataClicked || btnDownloadClosingFormClicked)
                {
                    e.Cancel = false;
                }
                else
                {
                    e.Cancel = true;
                }

            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }

        private void CtrlInventoryConsumables_Loaded(object sender, RoutedEventArgs e)
        {

        }

        private void CtrlInventoryConsumables_Loaded_1(object sender, RoutedEventArgs e)
        {

        }
    }

    #region Class TO disable Taskbar
    public class Taskbar
    {
        [DllImport("user32.dll")]
        private static extern int GetWindowText(IntPtr hWnd, StringBuilder text, int count);
        [DllImport("user32.dll", CharSet = CharSet.Auto)]
        private static extern bool EnumThreadWindows(int threadId, EnumThreadProc pfnEnum, IntPtr lParam);
        [DllImport("user32.dll", SetLastError = true)]
        private static extern System.IntPtr FindWindow(string lpClassName, string lpWindowName);
        [DllImport("user32.dll", SetLastError = true)]
        private static extern IntPtr FindWindowEx(IntPtr parentHandle, IntPtr childAfter, string className, string windowTitle);
        [DllImport("user32.dll")]
        private static extern IntPtr FindWindowEx(IntPtr parentHwnd, IntPtr childAfterHwnd, IntPtr className, string windowText);
        [DllImport("user32.dll")]
        private static extern int ShowWindow(IntPtr hwnd, int nCmdShow);
        [DllImport("user32.dll")]
        private static extern uint GetWindowThreadProcessId(IntPtr hwnd, out int lpdwProcessId);

        private const int SW_HIDE = 0;
        private const int SW_SHOW = 5;

        private const string VistaStartMenuCaption = "Start";
        private static IntPtr vistaStartMenuWnd = IntPtr.Zero;
        private delegate bool EnumThreadProc(IntPtr hwnd, IntPtr lParam);

        /// <summary>
        /// Show the taskbar.
        /// </summary>
        public static void Show()
        {
            SetVisibility(true);
        }

        /// <summary>
        /// Hide the taskbar.
        /// </summary>
        public static void Hide()
        {
            SetVisibility(false);
        }

        /// <summary>
        /// Sets the visibility of the taskbar.
        /// </summary>
        public static bool Visible
        {
            set { SetVisibility(value); }
        }

        /// <summary>
        /// Hide or show the Windows taskbar and startmenu.
        /// </summary>
        /// <param name="show">true to show, false to hide</param>
        private static void SetVisibility(bool show)
        {
            // get taskbar window
            IntPtr taskBarWnd = FindWindow("Shell_TrayWnd", null);

            // try it the WinXP way first...
            IntPtr startWnd = FindWindowEx(taskBarWnd, IntPtr.Zero, "Button", "Start");

            if (startWnd == IntPtr.Zero)
            {
                // try an alternate way, as mentioned on CodeProject by Earl Waylon Flinn
                startWnd = FindWindowEx(IntPtr.Zero, IntPtr.Zero, (IntPtr)0xC017, "Start");
            }

            if (startWnd == IntPtr.Zero)
            {
                // ok, let's try the Vista easy way...
                startWnd = FindWindow("Button", null);

                if (startWnd == IntPtr.Zero)
                {
                    // no chance, we need to to it the hard way...
                    startWnd = GetVistaStartMenuWnd(taskBarWnd);
                }
            }

            ShowWindow(taskBarWnd, show ? SW_SHOW : SW_HIDE);
            ShowWindow(startWnd, show ? SW_SHOW : SW_HIDE);
        }

        /// <summary>
        /// Returns the window handle of the Vista start menu orb.
        /// </summary>
        /// <param name="taskBarWnd">windo handle of taskbar</param>
        /// <returns>window handle of start menu</returns>
        private static IntPtr GetVistaStartMenuWnd(IntPtr taskBarWnd)
        {
            // get process that owns the taskbar window
            int procId;
            GetWindowThreadProcessId(taskBarWnd, out procId);

            Process p = Process.GetProcessById(procId);
            if (p != null)
            {
                // enumerate all threads of that process...
                foreach (ProcessThread t in p.Threads)
                {
                    EnumThreadWindows(t.Id, MyEnumThreadWindowsProc, IntPtr.Zero);
                }
            }
            return vistaStartMenuWnd;
        }

        /// <summary>
        /// Callback method that is called from 'EnumThreadWindows' in 'GetVistaStartMenuWnd'.
        /// </summary>
        /// <param name="hWnd">window handle</param>
        /// <param name="lParam">parameter</param>
        /// <returns>true to continue enumeration, false to stop it</returns>
        private static bool MyEnumThreadWindowsProc(IntPtr hWnd, IntPtr lParam)
        {
            StringBuilder buffer = new StringBuilder(256);
            if (GetWindowText(hWnd, buffer, buffer.Capacity) > 0)
            {
                Console.WriteLine(buffer);
                if (buffer.ToString() == VistaStartMenuCaption)
                {
                    vistaStartMenuWnd = hWnd;
                    return false;
                }
            }
            return true;
        }
    }
    #endregion
}
