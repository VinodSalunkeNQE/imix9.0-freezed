﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.SqlServer.Management.Smo;
using Microsoft.SqlServer.Management.Common;
using Microsoft.SqlServer.Management.Utility;
using System.Data.SqlClient;
using System.IO;
using Ionic.Zip;
using System.Data;
using DigiPhoto.DataLayer.Model;
using DigiPhoto.DataLayer;
using System.Windows.Documents;
using System.Collections;
using DigiPhoto.IMIX.Model;
using DigiPhoto.IMIX.Business;


namespace DigiPhoto.Manage
{
    class clsBackup
    {
        static Hashtable htPhotoname = new Hashtable();
        static Dictionary<int, DateTime> dicPhotoCreated = new Dictionary<int, DateTime>();
        static DataTable dtPhotos = new DataTable();
        public static bool BackupDatabase(string databaseName, string userName, string password, string serverName, string destinationPath)
        {
            bool IsBackupSuccess;
            try
            {
                
                Console.WriteLine("Backup is being executed...");

                //Define a Backup object variable.
                Backup sqlBackup = new Backup();

                //Specify the type of backup, the description, the name, and the database to be backed up.
                sqlBackup.Action = BackupActionType.Database;
                sqlBackup.BackupSetDescription = "BackUp of:" + databaseName + "on" + DateTime.Now.ToShortDateString();
                sqlBackup.BackupSetName = "FullBackUp";
                sqlBackup.Database = databaseName;
                string bakfilename = databaseName + "_" + DateTime.Now.ToString("dd-MMM-yyyy-HH-mm-ss") + ".bak";
                //Declare a BackupDeviceItem
                //BackupDeviceItem deviceItem = new BackupDeviceItem(destinationPath + "FullBackUp.bak", DeviceType.File);
                BackupDeviceItem deviceItem = new BackupDeviceItem(destinationPath + bakfilename, DeviceType.File);
                //Define Server connection
                ServerConnection connection = new ServerConnection(serverName, userName, password);
                //To Avoid TimeOut Exception
                Server sqlServer = new Server(connection);
                sqlServer.ConnectionContext.StatementTimeout = 60 * 60;
                Database db = sqlServer.Databases[databaseName];

                sqlBackup.Initialize = true;
                sqlBackup.Checksum = true;
                sqlBackup.ContinueAfterError = true;

                //Add the device to the Backup object.
                sqlBackup.Devices.Add(deviceItem);
                //Set the Incremental property to False to specify that this is a full database backup.
                sqlBackup.Incremental = false;

                sqlBackup.ExpirationDate = DateTime.Now.AddDays(3);
                //Specify that the log must be truncated after the backup is complete.
                sqlBackup.LogTruncation = BackupTruncateLogType.Truncate;

                sqlBackup.FormatMedia = false;
                //Run SqlBackup to perform the full database backup on the instance of SQL Server.
                sqlBackup.SqlBackup(sqlServer);
                //Remove the backup device from the Backup object.
                sqlBackup.Devices.Remove(deviceItem);

                IsBackupSuccess = true;
                Console.WriteLine("Backup successfully taken at: " + destinationPath + bakfilename);
                Console.WriteLine("Press any key to exit...");
                //Console.ReadLine();
                

            }
            catch (Exception ex)
            {
                Console.WriteLine("Backup operation could not be executed. Error: " + ex.Message);
                Console.ReadLine();
                IsBackupSuccess = false;
            }
            return IsBackupSuccess;
        }

        public static bool EmptyTables(string heavytables, int _cleanupdaysBackup, int SubStoreId)
        {
            bool IsTableEmpty;
            try
            {
                string _connstr = System.Configuration.ConfigurationManager.ConnectionStrings["DigiConnectionString"].ConnectionString;
                if (!String.IsNullOrEmpty(heavytables))
                {
                    using (SqlConnection con = new SqlConnection(_connstr))
                    {
                        using (SqlCommand cmd = new SqlCommand("DG_TableCleanup", con))
                        {
                            cmd.CommandType = CommandType.StoredProcedure;

                            cmd.Parameters.Add("@tables", SqlDbType.VarChar).Value = heavytables; 
                            cmd.Parameters.Add("@BackUpDays", SqlDbType.Int).Value = _cleanupdaysBackup;
                            cmd.Parameters.Add("@SubStoreId", SqlDbType.Int).Value = SubStoreId;
                            con.Open();
                            cmd.ExecuteNonQuery(); //uncomment
                        }
                    }
                }
                IsTableEmpty = true;
            }
            catch 
            {

                IsTableEmpty = false;
            }
            return IsTableEmpty;

        }

        public static bool BackupCompress(string strSource, string strDestination)
        {
            bool isBackupCompressed;
            try
            {
                string currDir = "";
                currDir = Path.GetFileNameWithoutExtension(strSource);
                //string newpath = strDestination + currDir + "_" + DateTime.Now.ToString("dd-MMM-yyyy-HH-mm-ss") + ".zip";
                currDir = !String.IsNullOrEmpty(currDir) ? currDir : "HotFolderBackup";
                string newpath = strDestination + currDir + "_" + DateTime.Now.ToString("dd-MMM-yyyy-HH-mm-ss") + ".zip";
                using (ZipFile zip = new ZipFile())
                {
                    zip.AddDirectory(strSource, currDir);
                    zip.UseZip64WhenSaving = Zip64Option.Always;
                    zip.CompressionLevel = Ionic.Zlib.CompressionLevel.BestCompression;
                    zip.BufferSize = 65536 * 8;
                    zip.Save(newpath);
                }
                isBackupCompressed = true;
            }
            catch
            {

                isBackupCompressed = false;
            }
            return isBackupCompressed;

        }

        public static int CleanDirectory(string clrDirectoryPath)
        {
            //DigiPhotoDataServices _objDataLayer1 = new DigiPhotoDataServices();DigiPhotoDataServices _objDataLayer1 = new DigiPhotoDataServices();
            //List<vw_GetPhotoList> _objdata = _objDataLayer1.GetPhoto();
            List<PhotoInfo> _objdata = (new PhotoBusiness()).GetSearchedPhoto(null, null, null, null, string.Empty);
            int retCount = 0;
            try
            {
                if (Directory.Exists(clrDirectoryPath))
                {
                    System.IO.DirectoryInfo myDirInfo = new DirectoryInfo(clrDirectoryPath);

                    foreach (FileInfo file in myDirInfo.GetFiles())
                    {
                        var tempFile = _objdata.Where(te => te.DG_Photos_FileName == file.Name).FirstOrDefault();
                        if (tempFile == null)
                        {
                            if (file.Extension.ToUpper() == ".JPG")
                            {
                                try
                                {
                                    file.Delete(); //uncomment
                                    retCount++;
                                }
                                catch
                                { }
                            }
                        }
                    }
                }
            }
            catch
            {
                retCount = -1;
            }
            return retCount;
        }
        public static int CopyDirectoryFiles(string destinationFld, string clrDirectoryPath)
        {
            int retCount = 0;
            Hashtable htCleanDir = new Hashtable();
            try
            {
                //Add all the directories(directory names) to be cleared
                htCleanDir.Add(1, "Croped");
                htCleanDir.Add(2, "Download");
               // htCleanDir.Add(3, "GreenImage");
                htCleanDir.Add(3, "PendingItems");
                htCleanDir.Add(4, "PrintImages");
                htCleanDir.Add(5, "Thumbnails");
                htCleanDir.Add(6, "Thumbnails//Temp");
                htCleanDir.Add(7, "Thumbnails_Big");
                htCleanDir.Add(8, "");//this points to _imgSource directory itself i.e. D:\\DigiImages
                foreach (string strPhotoId in htPhotoname.Keys)
                {
                    bool fileCopied = false;
                    bool fileDeleted = false;
                    string directoryVal = string.Empty;
                    string strPhoto = htPhotoname[strPhotoId].ToString();
                    foreach (string dirval in htCleanDir.Values)
                    {
                        if (dirval == "")
                        {
                            directoryVal = dicPhotoCreated[Convert.ToInt32(strPhotoId)].ToString("yyyyMMdd");
                        }
                        else
                        {
                            directoryVal = dirval;
                        }
                        if (!Directory.Exists(destinationFld + "\\" + directoryVal))
                        {
                            Directory.CreateDirectory(destinationFld + "\\" + directoryVal);
                        }
                        if (Directory.Exists(destinationFld + "\\" + directoryVal))
                        {
                            if (File.Exists(clrDirectoryPath + directoryVal + "\\" + strPhoto))
                            {
                                File.Copy(clrDirectoryPath + directoryVal + "\\" + strPhoto, destinationFld + "\\" + directoryVal + "\\" + strPhoto, true);
                                fileCopied = true;
                            }
                            if (File.Exists(clrDirectoryPath + directoryVal + "\\" + strPhoto.Replace(".jpg", ".png")))
                            {
                                File.Copy(clrDirectoryPath + directoryVal + "\\" + strPhoto.Replace(".jpg", ".png"), destinationFld + "\\" + directoryVal + "\\" + strPhoto.Replace(".jpg", ".png"), true);
                                fileCopied = true;
                            }
                        }
                        if (fileCopied)
                        {
                            try
                            {
                                if (File.Exists(clrDirectoryPath + directoryVal + "\\" + strPhoto))
                                {
                                    File.Delete(clrDirectoryPath + directoryVal + "\\" + strPhoto);
                                    fileDeleted = true;
                                }
                                if (File.Exists(clrDirectoryPath + directoryVal + "\\" + strPhoto.Replace(".jpg", ".png")))
                                {
                                    File.Delete(clrDirectoryPath + directoryVal + "\\" + strPhoto.Replace(".jpg", ".png"));
                                    fileDeleted = true;
                                }
                                try
                                {
                                    if (dirval == "")
                                    {
                                        string[] files = System.IO.Directory.GetFiles(clrDirectoryPath + directoryVal + "\\");
                                        if (files.Length == 0)
                                            Directory.Delete(clrDirectoryPath + directoryVal + "\\");
                                    }
                                }
                                catch (Exception ex1)
                                {
                                    string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex1);
                                    ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                                    string[] files = System.IO.Directory.GetFiles(clrDirectoryPath + directoryVal + "\\");
                                    if (files.Length == 0)
                                        Directory.Delete(clrDirectoryPath + directoryVal + "\\");
                                }
                            }
                            catch (Exception ex1)
                            {
                                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex1);
                                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                            }
                        }
                    }
                    if (fileDeleted)
                    {
                        try
                        {
                            DataRow[] result = dtPhotos.Select("ArchivedPhotoId =" + Convert.ToInt32(strPhotoId) + "");
                            foreach (DataRow dr in result)
                            {
                                dr["FileDeleted"] = 1;
                                dr["FileDeletedOn"] = DateTime.Now.ToString();
                            }
                        }
                        catch (Exception ex1)
                        {
                            string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex1);
                            ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                        }
                    }
                    else
                    {
                        DataRow[] result = dtPhotos.Select("ArchivedPhotoId =" + Convert.ToInt32(strPhotoId) + "");
                        foreach (DataRow dr in result)
                        {
                            dr["FileDeleted"] = 1;
                        }
                    }
                    retCount++;
                }
                return retCount;
            }
            catch (Exception ex)
            {

                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                return retCount;
            }
        }

        public static int GetPhotoArchived()
        {
            int tablecount = 0;
            try
            {

                if (!dtPhotos.Columns.Contains("ArchivedPhotoId"))
                    dtPhotos.Columns.Add("ArchivedPhotoId", typeof(long));
                if (!dtPhotos.Columns.Contains("FileDeleted"))
                    dtPhotos.Columns.Add("FileDeleted", typeof(bool));
                if (!dtPhotos.Columns.Contains("FileDeletedOn"))
                    dtPhotos.Columns.Add("FileDeletedOn", typeof(DateTime));

                htPhotoname.Clear();
                dtPhotos.Clear();
                dicPhotoCreated.Clear();
                string _connstr = System.Configuration.ConfigurationManager.ConnectionStrings["DigiConnectionString"].ConnectionString;
                using (SqlConnection con = new SqlConnection(_connstr))
                {
                    using (SqlCommand cmd = new SqlCommand("dbo.GetArchivedPhotoName", con))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        con.Open();
                        using (SqlDataReader rdr = cmd.ExecuteReader(CommandBehavior.CloseConnection))
                        {
                            if (rdr.HasRows)
                            {
                                while (rdr.Read())
                                {
                                    string PhotofileName = rdr["DG_Photos_FileName"].ToString();
                                    string archivedPhotoId = rdr["ArchivedPhotoId"].ToString();
                                    DateTime photoCreatedDate = Convert.ToDateTime(rdr["DG_Photos_CreatedOn"]);
                                    bool fileDeleted = Convert.ToBoolean(rdr["FileDeleted"]);
                                    if (!htPhotoname.ContainsKey(archivedPhotoId))
                                    {
                                        htPhotoname.Add(archivedPhotoId, PhotofileName);
                                        dicPhotoCreated.Add(Convert.ToInt32(archivedPhotoId), photoCreatedDate);
                                        DataRow dr = dtPhotos.NewRow();
                                        dr["ArchivedPhotoId"] = archivedPhotoId;
                                        dr["FileDeleted"] = fileDeleted;
                                        dtPhotos.Rows.Add(dr);
                                        tablecount++;
                                    }
                                }
                            }
                        }
                    }
                }
                return tablecount;
            }
            catch (Exception ex)
            {

                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                return tablecount;

            }
        }

        public static string CreateBackupFolderExits(string strSource, string strDestination)
        {
            try
            {
                string currDir = "";
                currDir = Path.GetFileNameWithoutExtension(strSource);
                //string newpath = strDestination + currDir + "_" + DateTime.Now.ToString("dd-MMM-yyyy-HH-mm-ss") + ".zip";
                currDir = !String.IsNullOrEmpty(currDir) ? currDir : "HotFolderBackup";
                // string newpath = strDestination + currDir + "_" + DateTime.Now.ToString("dd-MMM-yyyy-HH-mm-ss")+"\\HotFolderBackup";
                // string newpath = strDestination + currDir + "_" + DateTime.Now.ToString("dd-MMM-yyyy") + "\\HotFolderBackup";
                string newpath = strDestination + currDir + "_" + DateTime.Now.ToString("dd-MMM-yyyy");
                if (!Directory.Exists(newpath))
                {
                    Directory.CreateDirectory(newpath);
                }
                return newpath;
            }
            catch
            {
                return null;
            }
        }
        public static bool UpdateArchivedPhotoDetails()
        {
            bool isTableEmpty;
            try
            {
                string _connstr = System.Configuration.ConfigurationManager.ConnectionStrings["DigiConnectionString"].ConnectionString;
                if (dtPhotos.Rows.Count > 0)
                {
                    using (SqlConnection con = new SqlConnection(_connstr))
                    {
                        using (SqlCommand cmd = new SqlCommand("UpdateArchivedPhotoDetails", con))
                        {
                            cmd.CommandType = CommandType.StoredProcedure;

                            cmd.Parameters.Add("@ArchivedPhotoDetails", dtPhotos);

                            con.Open();
                            cmd.ExecuteNonQuery(); //uncomment
                        }
                    }
                }
                isTableEmpty = true;
            }
            catch
            {

                isTableEmpty = false;
            }
            return isTableEmpty;
        }

    }
}
