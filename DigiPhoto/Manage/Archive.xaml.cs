﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using DigiPhoto.Common;
using DigiAuditLogger;
using System.IO;
using DigiPhoto.DataLayer;
using Ionic.Zip;
using System.Windows.Media.Animation;
using System.Threading;
using System.Windows.Threading;
using System.ComponentModel;
using DigiPhoto.IMIX.Business;
using FrameworkHelper;

namespace DigiPhoto.Manage
{
    /// <summary>
    /// Interaction logic for Archive.xaml
    /// </summary>
    public partial class Archive : Window
    {
        
        string root = string.Empty;
        string ArchiveFilePath = string.Empty;
        bool isAcquired = false;
        bool IsrequireArchive = true;
        /// <summary>
        /// Initializes a new instance of the <see cref="Archive"/> class.
        /// </summary>
        public Archive()
        {
            InitializeComponent();
            root = Environment.CurrentDirectory;
            ArchiveFilePath = root + "\\";
            ArchiveFilePath = System.IO.Path.Combine(ArchiveFilePath, "DigiArchive");
            #region Create the Archive file directory if it does not exists
            if (!Directory.Exists(ArchiveFilePath))
            {
                System.IO.Directory.CreateDirectory(ArchiveFilePath);
            }
            #endregion Create the Archive file directory if it does not exists
            txbUserName.Text = LoginUser.UserName;
            txbStoreName.Text = LoginUser.StoreName;
            ArchiveProgress.Minimum = 0;
            
        }

        /// <summary>
        /// Handles the Click event of the btnback control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void btnback_Click(object sender, RoutedEventArgs e)
        {
            ManageHome _objhome = new ManageHome();
            _objhome.Show();
            this.Close();
        }


        /// <summary>
        /// Handles the Click event of the btnLogout control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void btnLogout_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                AuditLog.AddUserLog(Common.LoginUser.UserId, (int)FrameworkHelper.ActionType.Logout, "Logged out at ");
                Login _objLogin = new Login();
                _objLogin.Show();
                this.Close();
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }

        /// <summary>
        /// Handles the Click event of the btnSelectdFolder control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void btnSelectdFolder_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                System.Windows.Forms.FolderBrowserDialog fbDialog = new System.Windows.Forms.FolderBrowserDialog();
                var result = fbDialog.ShowDialog();
                if (result == System.Windows.Forms.DialogResult.OK)
                {
                    txtSelectedfolder.Text = fbDialog.SelectedPath + "\\";
                }

            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }        
        /// <summary>
        /// Handles the Click event of the btnArchive control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void btnArchive_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (txtSelectedfolder.Text != "")
                {
                    //txtstatus.Visibility = Visibility.Visible;
                    //txtstatus.Text = "Archiving.....";
                    Go(txtSelectedfolder.Text);
                    
                }
                else
                {
                    MessageBox.Show(UIConstant.SelectFolderFirst);
                }
                
            }
            catch (Exception ex)
            {
                // MessageBox.Show("Error in Archive " + ex.Message);
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }
        /// <summary>
        /// Goes the specified movearchive.
        /// </summary>
        /// <param name="movearchive">The movearchive.</param>
        void Go(string movearchive)
        {
            int getitem = 0;
            ThreadStart start = delegate()
            {
                //this is taking place on the background thread
                PhotoBusiness phoBiz = new PhotoBusiness();
                var items = phoBiz.GetArchiveImages();
                    if (items.Count > 0)
                    {
                        int i = 0;
                        foreach (var item in items)
                        {
                            i++;
                            getitem = items.Count;
                            phoBiz.SetArchiveDetails(item.DG_Photos_FileName);
                            try
                            {
                                string Filepathhotfolder = LoginUser.DigiFolderPath + item.DG_Photos_FileName;
                                string filepaththumnail = System.IO.Path.Combine(LoginUser.DigiFolderThumbnailPath, item.DG_Photos_CreatedOn.ToString("yyyyMMdd"), item.DG_Photos_FileName);
                                string filepaththumnailbig = System.IO.Path.Combine(LoginUser.DigiFolderBigThumbnailPath, item.DG_Photos_CreatedOn.ToString("yyyyMMdd"), item.DG_Photos_FileName);
                                string filepathcropped = LoginUser.DigiFolderCropedPath + item.DG_Photos_FileName;
                                string filepathgreenimage = LoginUser.DigiFolderPath + "\\GreenImage\\" + item.DG_Photos_FileName;
                                string destFile = ArchiveFilePath + "\\" + item.DG_Photos_FileName;
                                //System.IO.File.Copy(Filepathhotfolder, destFile, true);
                                File.Move(Filepathhotfolder, destFile);
                                if (File.Exists(filepaththumnail))
                                {
                                    File.Delete(filepaththumnail);
                                }
                                if (File.Exists(filepathcropped))
                                {
                                    File.Delete(filepathcropped);
                                }
                                if (File.Exists(filepaththumnailbig))
                                {
                                    File.Delete(filepaththumnailbig);
                                }
                                if(File.Exists(filepathgreenimage))
                                {
                                    File.Delete(filepathgreenimage);
                                }

                            }
                            catch (Exception ex)
                            {
                                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                            }
                            //Thread.Sleep(20);
                            object[] obj = new object[]{i,getitem};
                            //this will marshal us back to the UI thread
                            Dispatcher.Invoke(DispatcherPriority.Normal, new Action<object[]>(Update), obj);
                        }

                    }
                    else
                    {
                        MessageBox.Show(UIConstant.AlredyArchivedImages);
                       
                        IsrequireArchive = false;
                    }
                    if (IsrequireArchive)
                    {
                        ZipPhotos(ArchiveFilePath, root + "\\", movearchive);

                        if (isAcquired)
                        {
                            MessageBox.Show(UIConstant.ImagesArchivedSuccessfully);
                            isAcquired = false;
                        }
                    }

            };
            
            new Thread(start).Start();
            
        }

        /// <summary>
        /// Updates the specified object.
        /// </summary>
        /// <param name="obj">The object.</param>
        void Update(object[] obj)
        {
            int value = (int)obj[0];
            int maxvalue = (int)obj[1];

            //this is taking place on the UI thread
            btnArchive.IsEnabled = false;
            ArchiveProgress.Maximum = maxvalue;
            ArchiveProgress.Value = value;
            txtfilescopied.Text = maxvalue + " File(s) found.";

        }

        /// <summary>
        /// Zips the photos.
        /// </summary>
        /// <param name="Folderpathtoarchive">The folderpathtoarchive.</param>
        /// <param name="FoldertosaveArchived">The foldertosave archived.</param>
        /// <param name="MoveArchiveFolder">The move archive folder.</param>
        private void ZipPhotos(String Folderpathtoarchive,string FoldertosaveArchived,string MoveArchiveFolder)
        {
            try
            {
                using (ZipFile zip = new ZipFile())
                {
                    zip.UseZip64WhenSaving = Zip64Option.AsNecessary;// utf-8
                    zip.AddDirectory(Folderpathtoarchive);
                    zip.Comment = "This zip was created at " + new CustomBusineses().ServerDateTime().ToString();
                    zip.Save(FoldertosaveArchived + "DigiArchive" + DateTime.Today.ToString("yyyyMMdd") + ".zip");
                }
               
                string sourceFile = System.IO.Path.Combine(FoldertosaveArchived, "DigiArchive" + DateTime.Today.ToString("yyyyMMdd") + ".zip");
                string destFile = System.IO.Path.Combine(MoveArchiveFolder, "DigiArchive" + DateTime.Today.ToString("yyyyMMdd") + ".zip");
                File.Move(sourceFile, destFile);
               // DeleteDirectory(Folderpathtoarchive);
                if (Directory.Exists(Folderpathtoarchive))
                {
                    Directory.Delete(Folderpathtoarchive, true);
                }
                //System.IO.File.Copy(sourceFile, destFile, true);
                isAcquired = true;

            }
            catch (Exception ex)
            {
                MessageBox.Show(UIConstant.ErrorinArchive + ex.Message);
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }

        /// <summary>
        /// Deletes the directory.
        /// </summary>
        /// <param name="target_dir">The target_dir.</param>
        public static void DeleteDirectory(string target_dir)
        {
            string[] files = Directory.GetFiles(target_dir);
            string[] dirs = Directory.GetDirectories(target_dir);

            foreach (string file in files)
            {
                File.SetAttributes(file, FileAttributes.Normal);
                File.Delete(file);
            }

            foreach (string dir in dirs)
            {
                DeleteDirectory(dir);
            }

            Directory.Delete(target_dir, false);
        }
    }
}
