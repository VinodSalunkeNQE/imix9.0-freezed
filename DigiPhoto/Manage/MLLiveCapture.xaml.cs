﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using MControls;
using MPLATFORMLib;
using FrameworkHelper;
using DigiPhoto.Common;
using System.IO;
using System.Runtime.InteropServices;
using DigiPhoto.IMIX.Model;
using DigiPhoto.IMIX.Business;
using System.Windows.Threading;
using FrameworkHelper.Common;
using System.Xml;
using System.Collections;
using System.Threading;
using System.ComponentModel;
using DigiPhoto.DataLayer;

namespace DigiPhoto.Manage
{
    /// <summary>
    /// Interaction logic for MLLiveCapture.xaml
    /// </summary>
    public partial class MLLiveCapture
    {
        #region Declaration
        public UIElement _parent;
        private bool _result = false;
        MComposerClass m_objComposer;
        /// /Writer
       
        public MWriterClass m_objWriter;       
        IMConfig m_pConfigRoot;
        // For extern object    
        private IMPersist m_pMPersist;
        // Called if user change playlist selection
        public event EventHandler OnLoad;    
        IMStreams m_pMixerStreams;
        MPreviewControl mPreviewControl = new MPreviewControl();
       
        string outputFormat = "mp4";
        BackgroundWorker bwRoute = new BackgroundWorker();
        BackgroundWorker bwSaveVideos = new BackgroundWorker();
        BusyWindow bs = new BusyWindow();
        string processVideoTemp = Environment.CurrentDirectory + "\\DigiProcessVideoTemp\\";
        List<VideoScene> lstVideoScene;
        DispatcherTimer VidTimer;
        VideoSceneBusiness business;
        bool CancelbwRoute = false;
        int VideoLength = 0;
        string tempFile;
        string path_tempThumbnail;
        LstMyItems objLast;
        public string IsGoupped;
        int frameCount = 0;
        string _guestVideoObject;
        bool _isVideoSaved = true;
        int FlipMode = 0;
        List<VideoSceneObject> lstVideoSceneObject = new List<VideoSceneObject>();
        #endregion
        public MLLiveCapture()
        {
            InitializeComponent();
            VidTimer = new DispatcherTimer();
            VidTimer.Tick += new EventHandler(VidTimer_Tick);
            bwSaveVideos.DoWork += bwSaveVideos_DoWork;
            bwSaveVideos.RunWorkerCompleted += bwSaveVideos_RunWorkerCompleted;
            this.Loaded += new RoutedEventHandler(MainWindow_Loaded);
           
            if (!Directory.Exists(processVideoTemp))                                  
            {
                Directory.CreateDirectory(processVideoTemp);
            }
            try
            {
                m_objComposer = new MComposerClass();
                m_objWriter = new MWriterClass();
            }
            catch (Exception exception)
            {
                return;
            }
            mMixerList1.SetControlledObject(m_objComposer);
            mElementsTree1.SetControlledObject(m_objComposer);
            mPreviewComposerControl.SetControlledObject(m_objComposer);

            //mPreviewControl1.SetControlledObject(m_objComposer);
            //mPreviewControl1.m_pPreview.PreviewFullScreen("", 1, 1);

            mFormatControl1.SetControlledObject(m_objComposer);
            mFormatControl1.SetControlledObject(m_objWriter);
            mPersistControl1.SetControlledObject(m_objComposer);
            mScenesCombo1.SetControlledObject(m_objComposer);
            SetWriterControlledObject(m_objWriter);     
            mConfigList1.SetControlledObject(m_objWriter);    
            mConfigList1.OnConfigChanged += new EventHandler(mConfigList1_OnConfigChanged);
            mScenesCombo1.OnActiveSceneChange += new EventHandler(mScenesCombo1_OnActiveSceneChange);
            mMixerList1.OnMixerSelChanged += new EventHandler(mMixerList1_OnMixerSelChanged);
            mPersistControl1.OnLoad += new EventHandler(mPersistControl1_OnLoad);

            //// Fill Senders
            FillSenders((IMSenders)m_objWriter);
            // Update config and enable/disable URL field
            mConfigList1_OnConfigChanged(null, EventArgs.Empty);
            mFormatControl1.comboBoxVideo.SelectedIndex = 0;
            mFormatControl1.comboBoxAudio.SelectedIndex = 0;

            mAttributesList1.ElementDescriptors = MHelpers.MComposerElementDescriptors;

            mElementsTree1.ElementDescriptors = MHelpers.MComposerElementDescriptors;
            m_objComposer.ObjectStart(null);
            MPersistSetControlledObject(m_objComposer);

            //bwRoute.DoWork += new System.ComponentModel.DoWorkEventHandler(bwRoute_DoWork);
            //bwRoute.WorkerSupportsCancellation = true;
            if (mPreviewComposerControl.m_pPreview != null)
                mPreviewComposerControl.m_pPreview.PreviewEnable("", 0, 1);
            ShowHideSettings(ShowHideSet);
            SetFormatControlSize();
            FillPositionDropDown();

        }
        private void bwSaveVideos_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            bs.Hide();
            if (GoToVOS)
            {
                HideHandlerDialog();
                ShowVOS();
            }
        }
        bool GoToVOS = false;
        private void bwSaveVideos_DoWork(object sender, DoWorkEventArgs e)
        {
            if (File.Exists(tempFile))
            {
                saveOutputVideo();
                GoToVOS = true;
            }
            else
            {
                GoToVOS = false;
                MessageBox.Show("Video not found!\n", "Video Editor", MessageBoxButton.OK, MessageBoxImage.Information, MessageBoxResult.OK);

            }
        }

        private void SetFormatControlSize()
        {
            mConfigList1.Width = 250;
            mConfigList1.Columns[0].Width = 95;
            mConfigList1.Columns[1].Width = 150;
            mConfigList1.Height = 100;
        }

        void m_objComposer_OnFrame(string bsChannelID, object pMFrame)
        {
            if (pMFrame != null)
            {
                frameCount++;
                foreach (var item in htVideoObjects.Keys)
                {
                    MElement elemRoute = GetMElement(item.ToString());
                    ApplyVideoObjectRoute(elemRoute, (Hashtable)htVideoObjects[item], frameCount);
                    Marshal.ReleaseComObject(elemRoute);
                }
                if(frameCount==5)
                { StartWriter(); 
                }
            }
            Marshal.ReleaseComObject(pMFrame);
        }
        void MainWindow_Loaded(object sender, RoutedEventArgs e)
        {
            loadSceneCombo();
            if (cmbScene.SelectedValue != null)
                loadScene((int)cmbScene.SelectedValue);
            else
            {
                enableDisableControls(false);
                MessageBox.Show("No video scene found!\n", "Video Editor", MessageBoxButton.OK, MessageBoxImage.Information, MessageBoxResult.OK);

            }
        }

        private void enableDisableControls(bool value)
        {
            btnStartProcess.IsEnabled = value;
            btnPreviewVideo.IsEnabled = value;
            btnPauseProcess.IsEnabled = value;
            btnSaveFile.IsEnabled = value;
            btnStopProcess.IsEnabled = value;
            if (value == false)
                stkQuickSettings.Visibility = Visibility.Collapsed;
            else
                stkQuickSettings.Visibility = Visibility.Visible;
        }
        void mPersistControl1_OnLoad(object sender, EventArgs e)
        {
            mMixerList1.UpdateList(true, 1);
            mElementsTree1.UpdateTree(false);
        }
        public Object MPersistSetControlledObject(Object pObject)
        {
            Object pOld = (Object)m_pMPersist;
            try
            {
                m_pMPersist = (IMPersist)pObject;
            }
            catch (System.Exception) { }

            return pOld;
        }
        public bool ShowPanHandlerDialog()
        {
            Visibility = Visibility.Visible;
            _parent.IsEnabled = false;

            return _result;
        }
        private void HideHandlerDialog()
        {
            try
            {
                if (m_objComposer != null)
                    m_objComposer.ObjectClose();               
                if (m_objWriter != null) m_objWriter.ObjectClose();//              
                mMixerList1.ClearList();            
                //  Marshal.FinalReleaseComObject(m_objComposer);
                Marshal.FinalReleaseComObject(m_objWriter);
               
            }
            catch (Exception ex)
            {

            }
        }
        public void SetParent(UIElement parent)
        {
            _parent = (UIElement)parent;
        }
        private void btnClose_Click(object sender, RoutedEventArgs e)
        {
            if (!_isVideoSaved)
            {
                MessageBoxResult result = MessageBox.Show("Video not saved, would you still like to exit?", "Video Editor", MessageBoxButton.YesNo, MessageBoxImage.Information);
                if (result == MessageBoxResult.Yes)
                {
                    HideHandlerDialog();
                    ShowVOS();
                }
            }
            else
            {
                HideHandlerDialog();
                ShowVOS();
            }
        }

        private void ShowVOS()
        {
            //bool isContinue = false;
            RobotImageLoader.RFID = "0";
            RobotImageLoader.SearchCriteria = "PhotoId";
            SearchResult window = null;
            foreach (Window wnd in Application.Current.Windows)
            {
                if (wnd.Title == "View/Order Station")
                {
                    window = (SearchResult)wnd;
                    break;
                }
            }
            if (window == null)
                window = new SearchResult();

            if (RobotImageLoader.GroupImages.Count == 0 && IsGoupped == "View All")
                IsGoupped = "View Group";
            window.pagename = "Saveback";
            // window.Savebackpid = Convert.ToString(photoId);
            window.Savebackpid = Convert.ToString(RobotImageLoader.PrintImages[RobotImageLoader.PrintImages.Count - 1].PhotoId);
            window.Show();
            window.LoadWindow();
            this.Close();
        }
        public Object SetWriterControlledObject(Object pObject)
        {
            Object pOld = (Object)m_pConfigRoot;
            try
            {
                m_pConfigRoot = (IMConfig)pObject;
            }
            catch (System.Exception) { }
            return pOld;
        }
        void mScenesCombo1_OnActiveSceneChange(object sender, EventArgs e)
        {
            mElementsTree1.UpdateTree(false);
        }
        MElement SelectedTreeElement;
        string strDefaultSceneSettings = string.Empty;
        private void mElementsTree1_AfterSelect(object sender, System.Windows.Forms.TreeViewEventArgs e)
        {
            try
            {
                MElement pElement = (MElement)e.Node.Tag;
                SelectedTreeElement = pElement;
                mAttributesList1.SetControlledObject(pElement);
                string strType, strXML;
                pElement.ElementGet(out strType, out strXML);
                if (string.IsNullOrEmpty(strDefaultSceneSettings))
                    strDefaultSceneSettings = strXML;
                GetElementInformation(strXML);
                bool bDefElement = false;
                foreach (string strDefElement in MHelpers.strDefaultElements)
                {
                    if (strType.Contains(strDefElement))
                    {
                        mPreviewComposerControl.SetEditElement(pElement);
                        bDefElement = true;
                        break;
                    }
                }
                if (!bDefElement)
                {
                    mPreviewComposerControl.SetEditElement(null);
                }

            }
            catch (System.Exception) { }
        }
        void mConfigList1_OnConfigChanged(object sender, EventArgs e)
        {
            // Update config string 
            string strConfig;
            m_objWriter.ConfigGetAll(1, out strConfig);
            comboBoxProps.Text = strConfig;
            // Check if format support network streaming
            string strFormat;
            IMAttributes pFormatConfig;
            m_objWriter.ConfigGet("format", out strFormat, out pFormatConfig);
            int bNetwork = 0;
            try
            {
                pFormatConfig.AttributesBoolGet("network", out bNetwork);
            }
            catch (System.Exception) { }
        }
        private void btnStartProcess_Click(object sender, RoutedEventArgs e)
        {
            EnableORDisableControls(false);
            thumCaptured = false;
            m_objComposer.OnFrame += new IMEvents_OnFrameEventHandler(m_objComposer_OnFrame);
            CancelbwRoute = false;
            //Initilizing of  input files
            InitializeAllInputFiles();
            //Apply Route
            //  bwRoute.RunWorkerAsync();
          //  StartWriter();
        }
        private void InitializeAllInputFiles()
        {
            try
            {
                MItem m_pFile; string streamId = string.Empty;
                int nCount = 0;
                m_objComposer.StreamsGetCount(out nCount);
                m_objComposer.FilePlayPause(0);
                for (int i = 0; i < nCount; i++)
                {
                    m_objComposer.StreamsGetByIndex(i, out streamId, out m_pFile);
                    try
                    {
                        //string name = string.Empty;
                        //m_pFile.FileNameGet(out name);
                       // if (m_pFile != null)
                       // {
                        m_pFile.FilePosSet(0, 0);
                        //}
                    }
                    catch { }
                }
                m_objComposer.FilePlayStart();
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }
        string strCapturePath_or_URL = string.Empty;

        private void StartWriter()
        {
            
            //string uniqueName = GetVideoName();
            VidTimer.Start();
           // ExtractThumbnail();
           // mFormatControl1.SetControlledObjectForMWriter(m_objComposer);

            eMState eState;
            m_objWriter.ObjectStateGet(out eState);
            if (eState == eMState.eMS_Paused)
            {
                // Continue capture
                m_objWriter.ObjectStart(m_objComposer);
            }
            if (eState == eMState.eMS_Running)
            {
                m_objWriter.WriterNameSet("", "");
            }
            else
            {
                string strFormat;
                IMAttributes pFormatConfig;
                m_objWriter.ConfigGet("format", out strFormat, out pFormatConfig);
                tempFile = processVideoTemp + "Output" + "." + outputFormat;
                strCapturePath_or_URL = tempFile;
                try
                {
                    m_objWriter.WriterNameSet(strCapturePath_or_URL, comboBoxProps.Text != "" ? comboBoxProps.Text : "video::bitrate=1M audio::bitrate=64K");
                    m_objWriter.ObjectStart(m_objComposer);
                    ////  m_objWriter.WriterNameSet(strCapturePath_or_URL, comboBoxProps.Text != "" ? comboBoxProps.Text : "video::bitrate=1M audio::bitrate=64K");
                    //m_objWriter.WriterNameSet(strCapturePath_or_URL, "audio::bitrate=256K video::bitrate=30M video::codec=mpeg4 gop_size=1 qmin=1 qscale=1 v422=true");
                    ////  m_objWriter.ObjectStart(pSource);
                    //m_objWriter.ObjectStart(m_objComposer);
                }
                catch (System.Exception ex)
                {
                    string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                    ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                    return;
                }
            }
        }

        private void btnStopProcess_Click(object sender, RoutedEventArgs e)
        {
            StopWriter(true);
            if (!string.IsNullOrEmpty(strCapturePath_or_URL) && File.Exists(strCapturePath_or_URL))
            {
                File.Delete(strCapturePath_or_URL);
            }
            EnableORDisableControls(true);
            txbPause.Text = "Pause";
        }
        private void buttonChromaProps_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (mMixerList1.SelectedItem != null)
                {
                    MItem mitem = null;
                    LoadChromaPlugin(true, mMixerList1.SelectedItem);
                    MCHROMAKEYLib.MChromaKey objChromaKey = GetChromakeyFilter(mMixerList1.SelectedItem);
                    try
                    {
                        ((IMProps)objChromaKey).PropsSet("gpu_mode", "true");
                        mitem = (MItem)mMixerList1.SelectedItem;
                    }
                    catch { }
                    if (objChromaKey != null)
                    {
                        FormChromaKey formCK = new FormChromaKey(objChromaKey);
                        formCK.ShowDialog();
                        string streamId = string.Empty;
                        mitem.FileNameGet(out streamId);
                        ((IMProps)mitem).PropsGet("stream_id", out streamId);
                        string chpath = lstVideoSceneObject.Where(x => x.VideoObjectId == streamId).FirstOrDefault().ObjectFileMapping.ChromaPath;
                        if (!string.IsNullOrEmpty(chpath))
                        {
                            if (File.Exists(System.IO.Path.Combine(ConfigManager.DigiFolderPath, chpath)))
                            {
                                //  File.Delete(System.IO.Path.Combine(ConfigManager.DigiFolderPath, chpath));
                                File.Copy(System.IO.Path.Combine(System.Environment.CurrentDirectory, "ChromaSettings.xml"), System.IO.Path.Combine(ConfigManager.DigiFolderPath, chpath), true);
                            }
                        }
                    }
                }
            }
            catch (Exception Exception) { }
        }
        private MCHROMAKEYLib.MChromaKey GetChromakeyFilter(object source)
        {
            MCHROMAKEYLib.MChromaKey pChromaKey = null;
            try
            {
                int nCount = 0;
                IMPlugins pPlugins = (IMPlugins)source;
                pPlugins.PluginsGetCount(out nCount);
                for (int i = 0; i < nCount; i++)
                {
                    object pPlugin;
                    long nCBCookie;
                    pPlugins.PluginsGetByIndex(i, out pPlugin, out nCBCookie);
                    try
                    {
                        pChromaKey = (MCHROMAKEYLib.MChromaKey)pPlugin;
                        break;
                    }
                    catch { }
                }
            }
            catch { }
            return pChromaKey;
        }
        private void mMixerList1_OnMixerSelChanged(object sender, EventArgs e)
        {
            System.Windows.Forms.ListView listView = (System.Windows.Forms.ListView)sender;
            if (listView.SelectedItems.Count > 0)
            {
                MCHROMAKEYLib.MChromaKey objChromaKey = GetChromakeyFilter(listView.SelectedItems[0].ToString());
                //checkBoxCK.IsChecked = objChromaKey == null ? false : true;
                buttonChromaProps.IsEnabled = true; ;
                //btnChromaSettings.IsEnabled = true;
            }
            else
            {
                //checkBoxCK.IsChecked = false;
                buttonChromaProps.IsEnabled = false;
            }


        }
        private void LoadChromaPlugin(bool onloadChroma, object source)
        {
            if (mMixerList1.SelectedItem != null)
            {
                IMPlugins pPlugins = source as IMPlugins;
                int nCount;
                long nCBCookie;
                object pPlugin = null;
                bool isCK = false;
                pPlugins.PluginsGetCount(out nCount);
                for (int i = 0; i < nCount; i++)
                {
                    pPlugins.PluginsGetByIndex(i, out pPlugin, out nCBCookie);

                    if (pPlugin.GetType().Name == "CoMChromaKeyClass" || pPlugin.GetType().Name == "MChromaKeyClass")
                    {
                        isCK = true;
                        break;
                    }
                }
                if ((isCK == false) || (onloadChroma && isCK == false))
                {
                    pPlugins.PluginsAdd(new MCHROMAKEYLib.MChromaKey(), 0);
                }
                buttonChromaProps.IsEnabled = true;
                //btnChromaSettings.IsEnabled = true;
            }
        }
        //private void checkBoxCK_Checked(object sender, RoutedEventArgs e)
        //{
        //    if (checkBoxCK.IsChecked == true)
        //    {
        //        LoadChromaPlugin(false, mMixerList1.SelectedItem);
        //    }
        //}
        private void FillSenders(IMSenders pSenders)
        {
            comboBoxExtSources.Items.Clear();
            comboBoxExtSources.Items.Add("<Live Source>");

            int nCount = 0;
            pSenders.SendersGetCount(out nCount);
            for (int i = 0; i < nCount; i++)
            {
                string strName;
                M_VID_PROPS vidProps;
                M_AUD_PROPS audProps;
                pSenders.SendersGetByIndex(i, out strName, out vidProps, out audProps);

                comboBoxExtSources.Items.Add(strName);
            }

            comboBoxExtSources.SelectedIndex = 0;
        }

        private void ExtractThumbnail()
        {
            MFrame mf1;
            m_objComposer.ObjectFrameGet(out mf1, "");
            path_tempThumbnail = processVideoTemp + "Output.jpg";
            if (File.Exists(path_tempThumbnail))
            {
                File.Delete(path_tempThumbnail);
            }
            mf1.FrameVideoSaveToFile(path_tempThumbnail);
            Marshal.ReleaseComObject(mf1);

        }
      
       
        bool thumCaptured = false;
        private void VidTimer_Tick(object sender, EventArgs e)
        {
            decimal vidLengthNow = 0;
            decimal vidLength = VideoLength + 0.043M;
            txtStatus.Text = UpdateStatus(m_objWriter, out vidLengthNow);

            eMState eState;
            m_objWriter.ObjectStateGet(out eState);

            if (!thumCaptured && vidLengthNow > 0.50M)
            {
                ExtractThumbnail();
                thumCaptured = true;
            }
            pbProgress.Value = Convert.ToDouble(vidLengthNow);
            decimal percent = ((vidLengthNow / vidLength) * 100);
            txtPercentage.Text = System.Math.Round(percent, 0).ToString() + "%";
            txtTimer.Text = vidLengthNow.ToString("0") + " sec";
            if (vidLengthNow >= vidLength)
            {
                StopWriter(false);
            }
        }
        string UpdateStatus(MWriterClass pCapture, out decimal VidLength)
        {
            VidLength = 0; string strVidlen;
            string strRes = "";
            try
            {
                eMState eState;
                pCapture.ObjectStateGet(out eState);
                string strPid;
                m_objWriter.PropsGet("stat::last::server_pid", out strPid);
                string strSkip;
                pCapture.PropsGet("stat::skip", out strSkip);
                strRes = " State: " + eState + (strSkip != null ? " Skip rest:" + DblStrTrim(strSkip, 3) : "") + " Server PID:" + strPid + "\r\n";

                string sFile;
                pCapture.WriterNameGet(out sFile);
                strRes += " Path: " + sFile + "\r\n";

                {
                    strRes += "TOTAL:\r\n";
                    string strBuffer;
                    pCapture.PropsGet("stat::buffered", out strBuffer);
                    string strFrames;
                    pCapture.PropsGet("stat::frames", out strFrames);

                    string strFPS;
                    pCapture.PropsGet("stat::fps", out strFPS);

                    string strTime;
                    pCapture.PropsGet("stat::video_time", out strTime);

                    string strBreaks = "0";
                    pCapture.PropsGet("stat::breaks", out strBreaks);
                    string strDropped = "0";
                    pCapture.PropsGet("stat::frames_dropped", out strDropped);

                    m_objWriter.PropsGet("stat::video_len", out strVidlen);
                    VidLength = Convert.ToDecimal(strVidlen);
                    string strAVtime;
                    m_objWriter.PropsGet("stat::av_sync_time", out strAVtime);
                    string strAVlen;
                    m_objWriter.PropsGet("stat::av_sync_len", out strAVlen);
                    strRes += "  Buffers:" + strBuffer + " Frames:" + strFrames + " Fps:" + DblStrTrim(strFPS, 3) + " Dropped:" + strDropped +
                    " Breaks:" + strBreaks + "  Video time:" + DblStrTrim(strTime, 3) + "\r\n" + " Video Len:" + strVidlen + " AV Sync time:" +
                    strAVtime + " AV sync len:" + strAVlen + "\r\n";

                    string strSamples;
                    pCapture.PropsGet("stat::samples", out strSamples);
                    pCapture.PropsGet("stat::audio_time", out strTime);

                    strRes += "  Samples:" + strSamples + " Audio time:" + DblStrTrim(strTime, 3) + "\r\n";
                }
                {
                    strRes += "LAST:\r\n";

                    string strFrames;
                    pCapture.PropsGet("stat::last::frames", out strFrames);

                    string strFPS = "";
                    pCapture.PropsGet("stat::last::fps", out strFPS);

                    string strTime = "";
                    pCapture.PropsGet("stat::last::video_time", out strTime);

                    string strBreaks;
                    pCapture.PropsGet("stat::breaks", out strBreaks);

                    strRes += "  Frames:" + strFrames + " Breaks:" + strBreaks +
                        " Video time:" + DblStrTrim(strTime, 3) + "\r\n";

                    string strSamples;
                    pCapture.PropsGet("stat::last::samples", out strSamples);
                    pCapture.PropsGet("stat::last::audio_time", out strTime);

                    strRes += "  Samples:" + strSamples + " Audio time:" + DblStrTrim(strTime, 3);
                }
                return strRes;
            }
            catch (Exception ex)
            {
                return strRes;
            }
        }
        string DblStrTrim(string str, int nPeriod)
        {
            if (str == null)
                return str = "0";
            int nDot = str.LastIndexOf('.');
            int nPeriodHave = nDot >= 0 ? str.Length - nDot - 1 : 0;
            if (nPeriodHave > nPeriod)
            {
                return str.Substring(0, nDot + nPeriod + 1);
            }
            // Add zeroes
            if (nDot < 0)
                str += ".";

            for (int i = nPeriodHave; i < nPeriod; i++)
            {
                str += "0";
            }
            return str;
        }
        private void btnLoadScene_Click(object sender, RoutedEventArgs e)
        {
            strDefaultSceneSettings = string.Empty;
            txtPercentage.Text = "0%";
            txtTimer.Text = "0 sec";
            if (htVideoObjects != null)
            {
                htVideoObjects.Clear();
            }
            if (cmbScene.SelectedIndex > 0)
                loadScene((int)cmbScene.SelectedValue);
        }
        private void loadSceneCombo()
        {
            try
            {
                subStoreID = ConfigManager.SubStoreId;
                business = new VideoSceneBusiness();
                lstVideoScene = new List<VideoScene>();
                lstVideoScene = business.GetVideoScene(0,subStoreID);
                Dictionary<int, string> scenes = new Dictionary<int, string>();
                scenes.Clear();
                scenes.Add(0, "--Select--");
                foreach (VideoScene item in lstVideoScene.Where(o => o.IsActive == true))
                {
                    scenes.Add(item.SceneId, item.Name);
                }
                cmbScene.ItemsSource = scenes;
                if (scenes.Count > 0)
                    cmbScene.SelectedIndex = 1;
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }
        private void LoadVideoObjectList(List<VideoSceneObject> lstVideoObjects)
        {
            foreach (var item in lstVideoObjects)
            {
                MItem mitem; int indx = 0;
                m_objComposer.StreamsGet(item.VideoObjectId, out indx, out mitem);
                string name;
                mitem.FileNameGet(out name);
                item.FileName = System.IO.Path.GetFileName(name);
            }
            datagrdAudioSettings.ItemsSource = lstVideoObjects;
        }
        private void cmbScene_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (cmbScene.SelectedIndex > 0)
                loadScene((int)cmbScene.SelectedValue);
        }

        int subStoreID = 0;
        int locationId;
        void loadScene(int sceneId)
        {
            try
            {
               mFormatControl1.SetControlledObject(m_objComposer);//reload the format control settings
                lstVideoScene = new List<VideoScene>();
                subStoreID = ConfigManager.SubStoreId;
                lstVideoScene = business.GetVideoScene(sceneId, subStoreID);
                locationId = lstVideoScene.FirstOrDefault().LocationId;
                GetConfigLocationData();
                if (lstVideoScene != null)
                {
                    string filePath = System.IO.Path.Combine(ConfigManager.DigiFolderPath, lstVideoScene.FirstOrDefault().ScenePath);
                   
                  //  List<VideoSceneObject> lstVideoSceneObject = new List<VideoSceneObject>();
                    business = new VideoSceneBusiness();
                    lstVideoSceneObject = business.GuestVideoObjectBySceneID(sceneId);
                    if (lstVideoSceneObject.Count > 0)
                    {
                        m_pMPersist = (IMPersist)m_objComposer;
                        m_pMPersist.PersistLoad("", filePath, "");
                        mMixerList1.UpdateList(true, 1);
                        mElementsTree1.UpdateTree(false);

                        VideoSceneObject obj = lstVideoSceneObject.Where(x => x.GuestVideoObject == true).FirstOrDefault();
                        _guestVideoObject = obj.VideoObjectId;
                        objLast = new LstMyItems();
                        objLast = RobotImageLoader.PrintImages.LastOrDefault();
                        if (!string.IsNullOrEmpty(_guestVideoObject))
                        {
                            if (objLast.MediaType == 2)
                                addFile(System.IO.Path.Combine(objLast.HotFolderPath, "Videos", objLast.CreatedOn.ToString("yyyyMMdd"), objLast.FileName), _guestVideoObject);
                            else if (objLast.MediaType == 3)
                                addFile(System.IO.Path.Combine(objLast.HotFolderPath, "ProcessedVideos", objLast.CreatedOn.ToString("yyyyMMdd"), objLast.FileName), _guestVideoObject);
                            else
                                addFile(System.IO.Path.Combine(objLast.HotFolderPath, objLast.CreatedOn.ToString("yyyyMMdd"), objLast.FileName), _guestVideoObject);
                        }
                        LoadChromaAndRouteSettings(lstVideoSceneObject);
                        VideoLength = lstVideoScene.FirstOrDefault().VideoLength;
                        LoadWriterSettings(lstVideoScene.FirstOrDefault().Settings);
                        pbProgress.Maximum = Convert.ToDouble(VideoLength);
                        
                        //Implement vertical design changes here
                        if (lstVideoScene.FirstOrDefault().IsVerticalVideo)
                        {
                            MPLATFORMLib.M_VID_PROPS props = new MPLATFORMLib.M_VID_PROPS();
                            props.eVideoFormat = MPLATFORMLib.eMVideoFormat.eMVF_Custom;
                            props.dblRate = 29;
                            props.nAspectX = 9;
                            props.nAspectY = 16;
                            props.nHeight = 1920;
                            props.nWidth = 1080;
                            m_objComposer.FormatVideoSet(eMFormatType.eMFT_Convert, ref props);
                            mFormatControl1.comboBoxVideo.Enabled = false;
                        }
                        else
                            mFormatControl1.comboBoxVideo.Enabled = true;
                        LoadVideoObjectList(lstVideoSceneObject);
//                        mFormatControl1.SetControlledObject(m_objComposer);//reload the format control setting
                        if (FlipMode > 0)
                        {
                            MElement me = GetGuestVideoElement(obj.VideoObjectId);
                            ApplyFlip(me, FlipMode);
                            Marshal.ReleaseComObject(me);
                        }
                    }
                    else
                    {
                        btnStartProcess.IsEnabled = false;
                        MessageBox.Show("No video scene objects found!\n", "Digiphoto", MessageBoxButton.OK, MessageBoxImage.Information, MessageBoxResult.OK);
                    }
                }
                else
                {
                    btnStartProcess.IsEnabled = false;
                    MessageBox.Show("No video scene settings found!\n", "Digiphoto", MessageBoxButton.OK, MessageBoxImage.Information, MessageBoxResult.OK);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }
        void addFile(string filePath, string streamId)
        {
            // If some streams selected -> change this stream
            //string strItemID = listViewFiles.SelectedItems.Count > 0 ? listViewFiles.SelectedItems[0].Text : "";
            //string strTrans = listViewFiles.SelectedItems.Count > 0 && strTransition != "" ? " transition='" + strTransition + "'" : "";
            //double dblTimeForChange = listViewFiles.SelectedItems.Count > 0 ? (double)numericChangeTime.Value : 0;
            m_pMixerStreams = (IMStreams)m_objComposer;

            MItem pFile = null;
            Cursor prev = this.Cursor;
            //this.Cursor = Cursors.WaitCursor;

            try
            {
                m_pMixerStreams.StreamsAdd(streamId, null, filePath, null, out pFile, 0);
            }
            catch (Exception)
            {
                MessageBox.Show("File " + filePath + " can't be added as stream");
            }

            this.Cursor = prev;


            if (pFile != null)
                mMixerList1.SelectFile(pFile);
            mMixerList1.UpdateList(true, 1);

        }
        //Load chroma 
        private void LoadChromaAndRouteSettings(List<VideoSceneObject> lstVideoSceneObject)
        {
            try
            {
                foreach (var item in lstVideoSceneObject.Where(o => o.ObjectFileMapping.ChromaPath != null || o.ObjectFileMapping.RoutePath != null))
                {
                    if (item.ObjectFileMapping.ChromaPath != "")
                    {
                        MItem pItem; int myIndex;
                        m_objComposer.StreamsGet(item.VideoObjectId, out myIndex, out pItem);
                        string chromaPath = System.IO.Path.Combine(ConfigManager.DigiFolderPath, item.ObjectFileMapping.ChromaPath);
                        //Load ChromaPlugin For MItem
                        LoadChromaPlugin(true, pItem);
                        MCHROMAKEYLib.MChromaKey objChromaKey = GetChromakeyFilter(pItem);
                        try
                        {
                            ((IMProps)objChromaKey).PropsSet("gpu_mode", "true");
                        }
                        catch { }
                        //Apply chroma
                        if (System.IO.File.Exists(chromaPath))
                            (objChromaKey as IMPersist).PersistLoad("", chromaPath, "");
                    }
                    if (item.ObjectFileMapping.RoutePath != "")
                    {
                        string vidId = "";
                        string rootpath = System.IO.Path.Combine(ConfigManager.DigiFolderPath, item.ObjectFileMapping.RoutePath);
                        vidId = item.ObjectFileMapping.RoutePath.Substring(item.ObjectFileMapping.RoutePath.LastIndexOf("\\") + 1).Replace("Route-", "");
                        vidId = vidId.Substring(0, vidId.Length - 4);

                        ReadVideoObjectRouteFile(rootpath, vidId);
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }
        Hashtable htVideoObjects = new Hashtable();
        #region Apply Route



        //Read Route File
        private void ReadVideoObjectRouteFile(string filePath, string vidId)
        {
            int counter = 0; Hashtable htRouteList = new Hashtable();
            string line;
            string x = string.Empty;
            string y = string.Empty;
            string h = string.Empty;
            string w = string.Empty;
            string r = string.Empty;
            string d = string.Empty;
            System.IO.StreamReader file = new System.IO.StreamReader(filePath);
            while ((line = file.ReadLine()) != null)
            {
                string[] coordinates = line.Split(';');
                if (coordinates.Length > 1)
                {
                    Hashtable htListVar = new Hashtable();

                    x = coordinates[0];
                    y = coordinates[1];
                    h = coordinates[2];
                    w = coordinates[3];
                    r = coordinates[4];
                    d = coordinates[5];

                    htListVar.Add("x", x);
                    htListVar.Add("y", y);
                    htListVar.Add("h", h);
                    htListVar.Add("w", w);
                    htListVar.Add("r", r);
                    htListVar.Add("tc", d);
                    htRouteList.Add(counter, htListVar);
                    counter++;
                }
            }
            file.Close();
            if (!htVideoObjects.ContainsKey(vidId))
                htVideoObjects.Add(vidId, htRouteList);
        }
        private void ApplyVideoObjectRoute(MElement elemRoute, Hashtable htRouteList)
        {
            for (int i = 0; i <= htRouteList.Count - 1; i++)
            {
                string varRoute = GetUpdateElementString((Hashtable)htRouteList[i]);
                elemRoute.ElementMultipleSet(varRoute, 0.0);
                Thread.Sleep(50);
                if (CancelbwRoute)
                {
                    KillBackgroundWorker(bwRoute);
                    break;
                }
            }
        }

        private void ApplyVideoObjectRoute(MElement elemRoute, Hashtable htRouteList, int count)
        {
            if (htRouteList.Count > count)
            {
                string varRoute = GetUpdateElementString((Hashtable)htRouteList[count]);
                elemRoute.ElementMultipleSet(varRoute, 0.0);
            }
        }

        //To get the level of the tree element
        private MElement GetMElement(string vidObject)
        {

            MElement elemRoute = null; MElement myElementRoute = null;
            try
            {
                string entry = string.Empty;
                m_objComposer.ElementsGetByIndex(3, out myElementRoute);
                ((IMElements)myElementRoute).ElementsGetByID(vidObject, out elemRoute);
                return elemRoute;
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                return elemRoute = null;
            }
            finally
            {
                //Marshal.ReleaseComObject(elemRoute);
                //Marshal.ReleaseComObject(myElementRoute);
            }
        }
        private string GetUpdateElementString(Hashtable htSingleRoute)
        {
            string strVar = string.Empty;
            foreach (string key in htSingleRoute.Keys)
            {
                strVar += htSingleRoute[key].ToString() + " ";
            }
            return strVar;
        }
        public void KillBackgroundWorker(System.ComponentModel.BackgroundWorker bw)
        {
            bw.CancelAsync();
            bw.Dispose();
            bw = null;
            GC.Collect();
        }
        //private void bwRoute_DoWork(object Sender, System.ComponentModel.DoWorkEventArgs e)
        //{
        //try
        //{
        //        if (bwRoute.CancellationPending)
        //        {
        //            Thread.Sleep(1200);
        //            e.Cancel = true;
        //}
        //        else
        //        {
        //            foreach (var item in htVideoObjects.Keys)
        //            {
        //                MElement elemRoute = GetMElement(item.ToString());
        //                ApplyVideoObjectRoute(elemRoute, (Hashtable)htVideoObjects[item]);
        //            }
        //        }
        //    }

        //catch (Exception ex)
        //{
        //    string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
        //    ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
        //}
        //}

        #endregion
        #region writer settings
        private void LoadWriterSettings(string settings)
        {
            try
            {
                XmlDocument xdoc = new XmlDocument();
                xdoc.LoadXml(settings);
                XmlNodeList nodes = xdoc.GetElementsByTagName("Settings");
                if (nodes.Count > 0)
                {
                    foreach (XmlNode node in nodes)
                    {
                        int Videoid = Convert.ToInt32(node.ChildNodes[1].InnerText);
                        int Audioid = Convert.ToInt32(node.ChildNodes[3].InnerText);
                        string OpFormat = node.ChildNodes[4].InnerText;
                        string Videocodec = node.ChildNodes[6].InnerText.Trim();
                        string Audiocodec = node.ChildNodes[5].InnerText.Trim();
                        mFormatControl1.comboBoxVideo.SelectedIndex = Videoid;
                        mFormatControl1.comboBoxAudio.SelectedIndex = Audioid;
                        string VideoSettings = string.Empty;
                        if (node.ChildNodes.Count >= 6)
                            VideoSettings = node.ChildNodes[7].InnerText.Trim();

                        (((MControls.ListViewEx)(mConfigList1)).Columns[1].ListView).Items[0].SubItems[1].Text = OpFormat;//o/p format
                        (((MControls.ListViewEx)(mConfigList1)).Columns[1].ListView).Items[1].SubItems[1].Text = Audiocodec;//audiocodec

                        if (Videocodec != "")
                        { (((MControls.ListViewEx)(mConfigList1)).Columns[1].ListView).Items[2].SubItems[1].Text = Videocodec; }//videocodec   }

                        IMAttributes pConfigProps;
                        m_objWriter.ConfigSet("format", OpFormat, out pConfigProps);
                        int bHave = 0; string strExtensions = string.Empty;
                        pConfigProps.AttributesHave("extensions", out bHave, out strExtensions);

                        string[] arr = strExtensions.Split(',');
                        outputFormat = arr[0]; //set output format
                        comboBoxProps.Text = VideoSettings;
                    }
                }
            }
            catch
            {

            }
        }
        #endregion
        private void StopWriter(bool isbtnStop)
        {
            try
            {
                frameCount = 0;
                EnableORDisableControls(true);
                m_objComposer.OnFrame -= new IMEvents_OnFrameEventHandler(m_objComposer_OnFrame);
                CancelbwRoute = true;
                VidTimer.Stop();
                m_objWriter.ObjectClose();
                if (!isbtnStop)
                {
                    if (MessageBox.Show("Video created successfully!\n\rWould you like to save it now?", "Advance Video Editor", MessageBoxButton.YesNo, MessageBoxImage.Question, MessageBoxResult.No) == MessageBoxResult.Yes)
                    {
                        btnSaveFile.IsEnabled = false;
                        btnSaveFile_Click(null, null);
                    }
                }
                 //   MessageBox.Show("Video created successfully!\n", "Video Editor", MessageBoxButton.OK, MessageBoxImage.Information, MessageBoxResult.OK);
                pbProgress.Value = 0;
                _isVideoSaved = false;
                txtPercentage.Text = "0%";
                txtTimer.Text = "0 sec";
                pbProgress.Value = 0;
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }

        private void btnSaveFile_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                bs.Show();
                bwSaveVideos.RunWorkerAsync();
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }

        #region HideShow Settings Panel
        Boolean ShowHideSet = false;
        private void btnShowHideSettings_Click(object sender, RoutedEventArgs e)
        {
            if (!ShowHideSet)
                ShowHideSet = true;
            else
                ShowHideSet = false;

            ShowHideSettings(ShowHideSet);
        }

        private void ShowHideSettings(bool flag)
        {
            if (flag)
            {
                stkSettingsRightPanel.Visibility = Visibility.Visible;
                stkSettingsBottomPanel.Visibility = Visibility.Visible;
            }
            else
            {
                stkSettingsRightPanel.Visibility = Visibility.Collapsed;
                stkSettingsBottomPanel.Visibility = Visibility.Collapsed;
            }
        }
        #endregion HideShow Settings Panel

        #region Expanders
        private void CollapseOthers(Expander exp)
        {
            if (exp != ExpSceneSettings)
            {
                ExpSceneSettings.IsExpanded = false;
            }
            if (exp != ExpRecordSettings)
            {
                ExpRecordSettings.IsExpanded = false;
            }
            if (exp != ExpRecordProgress)
            {
                ExpRecordProgress.IsExpanded = false;
            }
            if (exp != ExpPreview)
            {
                ExpPreview.IsExpanded = false;
            }
            if (exp != ExpEnableAudio)
            {
                ExpEnableAudio.IsExpanded = false;
            }
            if (exp != ExpQuickSettings)
            {
                ExpQuickSettings.IsExpanded = false;
            }

        }
        private void ExpRecordSettings_Expanded(object sender, RoutedEventArgs e)
        {
            Expander exp = (Expander)sender;
            CollapseOthers(exp);
        }
        private void ExpSceneSettings_Expanded(object sender, RoutedEventArgs e)
        {
            Expander exp = (Expander)sender;
            CollapseOthers(exp);
        }

        private void ExpPreview_Expanded(object sender, RoutedEventArgs e)
        {
            Expander exp = (Expander)sender;
            CollapseOthers(exp);
        }

        private void ExpRecordProgress_Expanded(object sender, RoutedEventArgs e)
        {
            Expander exp = (Expander)sender;
            CollapseOthers(exp);
        }
        #endregion Expanders
        private void btnPauseProcess_Click(object sender, RoutedEventArgs e)
        {
            eMState eState;
            m_objWriter.ObjectStateGet(out eState);
            if (eState == eMState.eMS_Paused)
            {
                m_objComposer.OnFrame += new IMEvents_OnFrameEventHandler(m_objComposer_OnFrame);
                m_objComposer.FilePlayStart();
                m_objWriter.ObjectStart(null);
                m_objWriter.ObjectStateGet(out eState);              
                txbPause.Text = "Pause";
            }
            else
            {
                m_objComposer.OnFrame -= new IMEvents_OnFrameEventHandler(m_objComposer_OnFrame);
                m_objComposer.FilePlayPause(0);
                m_objWriter.WriterSkip(0);
                txbPause.Text = "Continue";
            }
        }
        private string GetVideoName()
        {
            const string chars = "0123456789";
            var random = new Random();
            return "live" + new string(Enumerable.Repeat(chars, 5).Select(s => s[random.Next(s.Length)]).ToArray());
            //byte[] buffer = Guid.NewGuid().ToByteArray();
            //return "live-" + BitConverter.ToUInt32(buffer, 6).ToString();
        }

        private void InvalidatePropertyChange(Slider sdr, double timeForChange)
        {
            if (SelectedTreeElement != null)
            {
                if (sdr == sldXAxis)
                    ApplyPropertyChange(SelectedTreeElement, "x", Convert.ToString(sdr.Value), timeForChange);
                if (sdr == sldYAxis)
                    ApplyPropertyChange(SelectedTreeElement, "y", Convert.ToString(sdr.Value), timeForChange);
                if (sdr == sldHeight)
                    ApplyPropertyChange(SelectedTreeElement, "h", Convert.ToString(sdr.Value), timeForChange);
                if (sdr == sldWidth)
                    ApplyPropertyChange(SelectedTreeElement, "w", Convert.ToString(sdr.Value), timeForChange);
                if (sdr == sldRotation)
                    ApplyPropertyChange(SelectedTreeElement, "r", Convert.ToString(sdr.Value), timeForChange);
                if (sdr == sldCropX)
                    ApplyPropertyChange(SelectedTreeElement, "sx", Convert.ToString(sdr.Value), timeForChange);
                if (sdr == sldCropY)
                    ApplyPropertyChange(SelectedTreeElement, "sy", Convert.ToString(sdr.Value), timeForChange);
                if (sdr == sldCropH)
                    ApplyPropertyChange(SelectedTreeElement, "sh", Convert.ToString(sdr.Value), timeForChange);
                if (sdr == sldCropW)
                    ApplyPropertyChange(SelectedTreeElement, "sw", Convert.ToString(sdr.Value), timeForChange);
            }
        }
        private void InvalidatePropertyChange(TextBox textB, double timeForChange)
        {
            if (SelectedTreeElement != null)
            {
                if (textB == txtAudioGain)
                    ApplyPropertyChange(SelectedTreeElement, "audio_gain", Convert.ToString(txtAudioGain.Text), timeForChange);
                if (textB == txtStreamId)
                    ApplyPropertyChange(SelectedTreeElement, "stream_id", Convert.ToString(txtStreamId.Text), timeForChange);
            }
        }
        private void InvalidatePropertyChange(ComboBox cmb, double timeForChange)
        {
            if (SelectedTreeElement != null)
            {
                if (cmb == drpPosition)
                    ApplyPropertyChange(SelectedTreeElement, "pos", Convert.ToString(drpPosition.SelectedValue), timeForChange);
                if (cmb == drpCropP)
                    ApplyPropertyChange(SelectedTreeElement, "spos", Convert.ToString(drpCropP.SelectedValue), timeForChange);
            }
        }
        private void ApplyPropertyChange(MElement SelectedElem, string attribute, string value, double timeForChange)
        {
            SelectedElem.ElementMultipleSet(attribute + "=" + value, timeForChange);
        }


        private void btnPreviewVideo_Click(object sender, RoutedEventArgs e)
        {

            if (!string.IsNullOrEmpty(strCapturePath_or_URL) && File.Exists(strCapturePath_or_URL))
            {
                SetVisibility(false);
                popVideoPlayer.Visibility = Visibility.Visible;
                popVideoPlayer.vsMediaFileName = strCapturePath_or_URL;
                popVideoPlayer.SetParent(this);
                popVideoPlayer.Visibility = Visibility.Visible;
            }
            else
            {
                MessageBox.Show("No video available to preview!", "Video Editor", MessageBoxButton.OK, MessageBoxImage.Information, MessageBoxResult.OK);
            }
        }
        bool isRightPnlVisible = false;
        public void SetVisibility(bool IsVisible)
        {
            if (IsVisible)
            {
                winMPreviewComposer.Visibility = Visibility.Visible;
                winMStreamsList.Visibility = Visibility.Visible;
                winMConfigListl.Visibility = Visibility.Visible;
                winMFormatControl.Visibility = Visibility.Visible;
                winMPreviewGreenScreen.Visibility = Visibility.Visible;
                if (isRightPnlVisible == true)
                {
                    stkSettingsRightPanel.Visibility = Visibility.Visible;
                    isRightPnlVisible = false;
                }
            }
            else
            {
                winMPreviewComposer.Visibility = Visibility.Hidden;
                winMStreamsList.Visibility = Visibility.Hidden;
                winMConfigListl.Visibility = Visibility.Hidden;
                winMFormatControl.Visibility = Visibility.Hidden;
                winMPreviewGreenScreen.Visibility = Visibility.Hidden;
                if (stkSettingsRightPanel.Visibility == Visibility.Visible)
                {
                    isRightPnlVisible = true;
                    stkSettingsRightPanel.Visibility = Visibility.Hidden;
                }
            }
        }
        public void EnableORDisableControls(bool IsEnabled)
        {
            winMPreviewComposer.IsEnabled = IsEnabled;
            winMStreamsList.IsEnabled = IsEnabled;
            winMConfigListl.IsEnabled = IsEnabled;
            winMFormatControl.IsEnabled = IsEnabled;
            winMPreviewGreenScreen.IsEnabled = IsEnabled;
            btnStartProcess.IsEnabled = IsEnabled;
            btnPreviewVideo.IsEnabled = IsEnabled;
            cmbScene.IsEnabled = IsEnabled;
            btnLoadScene.IsEnabled = IsEnabled;
            ExpSceneSettings.IsEnabled = IsEnabled;
            btnClose.IsEnabled = IsEnabled;
            buttonChromaProps.IsEnabled = IsEnabled;
            btnSaveFile.IsEnabled = IsEnabled;
            btnPauseProcess.IsEnabled = !IsEnabled;
            btnStopProcess.IsEnabled = !IsEnabled;

        }
        private void GetElementInformation(string strAttributes)
        {
            try
            {
                Hashtable htAttrib = new Hashtable();
                XmlDocument sdoc = new XmlDocument();
                sdoc.LoadXml(strAttributes);
                XmlNode xnode = sdoc.FirstChild;
                for (int i = 0; i < xnode.Attributes.Count; i++)
                {
                    string name = Convert.ToString(xnode.Attributes[i].Name);
                    string value = Convert.ToString(xnode.Attributes[i].Value);
                    if (!htAttrib.ContainsKey(name))
                    {
                        htAttrib.Add(name, value);
                    }
                }
                if (htAttrib.Count > 0)
                {
                    PopulateProperties(htAttrib);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }
        private void PopulateProperties(Hashtable htAttrib)
        {
            txtXAxis.Text = htAttrib.ContainsKey("x") ? htAttrib["x"].ToString() : "0.0";
            txtYAxis.Text = htAttrib.ContainsKey("y") ? htAttrib["y"].ToString() : "0.0";
            txtHeight.Text = htAttrib.ContainsKey("h") ? htAttrib["h"].ToString() : "0.0";
            txtWidth.Text = htAttrib.ContainsKey("w") ? htAttrib["w"].ToString() : "0.0";
            txtRotation.Text = htAttrib.ContainsKey("r") ? htAttrib["r"].ToString() : "0.0";
            txtCropX.Text = htAttrib.ContainsKey("sx") ? htAttrib["sx"].ToString() : "0.0";
            txtCropY.Text = htAttrib.ContainsKey("sy") ? htAttrib["sy"].ToString() : "0.0";
            txtCropH.Text = htAttrib.ContainsKey("sh") ? htAttrib["sh"].ToString() : "0.0";
            txtCropW.Text = htAttrib.ContainsKey("sw") ? htAttrib["sw"].ToString() : "0.0";
            txtAudioGain.Text = htAttrib.ContainsKey("audio_gain") ? htAttrib["audio_gain"].ToString() : "+0.0";
            txtStreamId.Text = htAttrib.ContainsKey("stream_id") ? htAttrib["stream_id"].ToString() : "";

            drpPosition.SelectedValue = htAttrib.ContainsKey("pos") ? htAttrib["pos"].ToString() : "bottom-left";
            drpCropP.SelectedValue = htAttrib.ContainsKey("spos") ? htAttrib["spos"].ToString() : "bottom-left";

        }
        private List<string> FillPositionList()
        {
            List<string> Positions = new List<string>();
            Positions.Add("center");
            Positions.Add("right");
            Positions.Add("left");
            Positions.Add("top");
            Positions.Add("bottom");
            Positions.Add("bottom-right");
            Positions.Add("bottom-left");
            Positions.Add("top-right");
            Positions.Add("top-left");
            return Positions;
        }
        private void FillPositionDropDown()
        {
            try
            {
                List<string> Positions = FillPositionList();
                drpPosition.Items.Clear();
                drpPosition.ItemsSource = Positions;
                drpCropP.Items.Clear();
                drpCropP.ItemsSource = Positions;
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }
        private void sldXAxis_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            InvalidatePropertyChange((Slider)sender, 0.0);
        }

        private void sldYAxis_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            InvalidatePropertyChange((Slider)sender, 0.0);
        }

        private void sldHeight_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            InvalidatePropertyChange((Slider)sender, 0.0);
        }

        private void sldWidth_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            InvalidatePropertyChange((Slider)sender, 0.0);
        }

        private void sldRotation_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            InvalidatePropertyChange((Slider)sender, 0.0);
        }

        private void sldCropX_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            InvalidatePropertyChange((Slider)sender, 0.0);
        }

        private void sldCropY_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            InvalidatePropertyChange((Slider)sender, 0.0);
        }

        private void sldCropW_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            InvalidatePropertyChange((Slider)sender, 0.0);
        }

        private void sldCropH_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            InvalidatePropertyChange((Slider)sender, 0.0);
        }

        private void txtAudioGain_LostFocus(object sender, RoutedEventArgs e)
        {
            InvalidatePropertyChange((TextBox)sender, 0.0);
        }

        private void txtStreamId_LostFocus(object sender, RoutedEventArgs e)
        {
            InvalidatePropertyChange((TextBox)sender, 0.0);
        }

        private void drpPosition_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            InvalidatePropertyChange((ComboBox)sender, 0.0);
        }
        private void drpCropP_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            InvalidatePropertyChange((ComboBox)sender, 0.0);
        }

        #region Quick Settings
        private void btnFullScreen_Checked(object sender, RoutedEventArgs e)
        {
            //MPreviewClass previewSecondary = new MPreviewClass();

            //previewSecondary.PreviewWindowSet("", mPreviewComposerControl.panelPreview.Handle.ToInt32());
            //previewSecondary.PreviewFullScreen("", 1, 1);
            ////mPreviewComposerControl.SetControlledObject(m_objComposer);
            ////((IMObject)previewSecondary).ObjectStart(m_objComposer);
            //previewSecondary.ObjectStart(m_objComposer);
            //////mPreviewComposerControl.SetControlledObject(null);
            ////m_objComposer.PreviewWindowSet("", mPreviewComposerControl.Handle.ToInt32());
            ////m_objComposer.PreviewFullScreen("", 0, 1);

            ////((IMObject)previewSecondary).ObjectStart(m_objComposer);

            //m_objComposer.FilePlayStart();


            ///This code works fine
            if (mPreviewComposerControl.m_pPreview != null)
            {
                // Enable full screen (use -1 for auto-select monitor)
                if ((Boolean)btnFullScreen.IsChecked)
                    mPreviewComposerControl.m_pPreview.PreviewFullScreen("", 1, 1);
                else
                    mPreviewComposerControl.m_pPreview.PreviewFullScreen("", 0, 1);
            }
        }
        private void sldVolume_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            // Volume in dB
            // 0 - full volume, -100 silence
            double dblPos = (double)sldVolume.Value / sldVolume.Maximum;
            if (mPreviewComposerControl.m_pPreview != null)
                mPreviewComposerControl.m_pPreview.PreviewAudioVolumeSet("", -1, -30 * (1 - dblPos));

        }
        private void btnAspectRatio_Click(object sender, RoutedEventArgs e)
        {
            if (mPreviewComposerControl.m_pPreview != null)
                ((IMProps)mPreviewComposerControl.m_pPreview).PropsSet("maintain_ar", (Boolean)btnAspectRatio.IsChecked ? "none" : "letter-box");
        }
        private void btnAudio_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (mPreviewComposerControl.m_pPreview != null)
                    mPreviewComposerControl.m_pPreview.PreviewEnable("", (Boolean)btnAudio.IsChecked ? 0 : 1, (Boolean)btnVideoPreview.IsChecked ? 0 : 1);
            }
            catch
            {

            }
        }
        private void btnVideoPreview_Click(object sender, RoutedEventArgs e)
        {
            if (mPreviewComposerControl.m_pPreview != null)
                mPreviewComposerControl.m_pPreview.PreviewEnable("", (Boolean)btnAudio.IsChecked ? 0 : 1, (Boolean)btnVideoPreview.IsChecked ? 0 : 1);
        }
        #endregion Quick Settings
        private void ExpEnableAudio_Expanded(object sender, RoutedEventArgs e)
        {
            Expander exp = (Expander)sender;
            CollapseOthers(exp);
        }

        private void chkEnableAudio_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                CheckBox chkBox = (CheckBox)sender;
                int pitem = 0;
                MItem mitem;
                IMElements element = (IMElements)m_objComposer;
                m_objComposer.StreamsGet(chkBox.Tag.ToString(), out pitem, out mitem);
                IMProps m_pProps = (IMProps)mitem;
                if (mitem != null)
                {
                    if (chkBox.IsChecked == true)
                    {
                        m_pProps.PropsSet("object::audio_gain", "0");
                    }
                    else
                    {
                        m_pProps.PropsSet("object::audio_gain", "-100");
                    }
                }
            }
            catch (Exception ex) { }
        }

        private void ExpQuickSettings_Expanded(object sender, RoutedEventArgs e)
        {
            Expander exp = (Expander)sender;
            CollapseOthers(exp);
        }
        private void saveOutputVideo()
        {
            try
            {
                if (objLast != null)
                {
                    PhotoBusiness photoBusiness = new PhotoBusiness();
                    string pathToSave = objLast.HotFolderPath + "ProcessedVideos\\" + DateTime.Now.ToString("yyyyMMdd");
                    string outputfile = pathToSave + "\\" + objLast.Name + "_" + DateTime.Now.ToString("HH:mm").Replace(":", "") + "." + outputFormat;
                    string fileName = objLast.Name + "_" + DateTime.Now.ToString("HH:mm").Replace(":", "") + "." + outputFormat;
                    string thumbPath = System.IO.Path.Combine(objLast.HotFolderPath, "Thumbnails", DateTime.Now.ToString("yyyyMMdd"));
                    if (!Directory.Exists(pathToSave))
                        Directory.CreateDirectory(pathToSave);
                    if (!Directory.Exists(thumbPath))
                        Directory.CreateDirectory(thumbPath);
                    if (File.Exists(tempFile))
                        File.Copy(tempFile, outputfile, true);
                    if (File.Exists(path_tempThumbnail))
                        ResizeWPFImage(path_tempThumbnail, 210, System.IO.Path.Combine(thumbPath, System.IO.Path.GetFileNameWithoutExtension(fileName) + ".jpg"));
                    int photoId = 0;
                    photoId = photoBusiness.SetPhotoDetails(ConfigManager.SubStoreId, objLast.Name, fileName, DateTime.Now, LoginUser.UserId.ToString(), "", 0, string.Empty, string.Empty, null, null, 1, null, VideoLength, true, 3);
                    if (photoId > 0)
                        MessageBox.Show("Video saved successfully!\n", "Video Editor", MessageBoxButton.OK, MessageBoxImage.Information, MessageBoxResult.OK);
                   // _isVideoSaved = true;

                }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }
        private void ResizeWPFImage(string sourceImage, int maxHeight, string saveToPath)
        {
            try
            {
                BitmapImage bi = new BitmapImage();
                BitmapImage bitmapImage = new BitmapImage();
                using (FileStream fileStream = File.OpenRead(sourceImage.ToString()))
                {
                    MemoryStream ms = new MemoryStream();
                    fileStream.CopyTo(ms);
                    ms.Seek(0, SeekOrigin.Begin);
                    fileStream.Close();
                    bi.BeginInit();
                    bi.StreamSource = ms;
                    bi.EndInit();
                    bi.Freeze();
                    decimal ratio = Convert.ToDecimal(bi.Width) / Convert.ToDecimal(bi.Height);

                    int newWidth = Convert.ToInt32(maxHeight * ratio);
                    int newHeight = maxHeight;

                    ms.Seek(0, SeekOrigin.Begin);
                    bitmapImage.BeginInit();
                    bitmapImage.StreamSource = ms;
                    bitmapImage.DecodePixelWidth = newWidth;
                    bitmapImage.DecodePixelHeight = newHeight;
                    bitmapImage.EndInit();
                    bitmapImage.Freeze();
                }

                using (var fileStreamForSave = new FileStream(saveToPath, FileMode.Create, FileAccess.ReadWrite))
                {
                    JpegBitmapEncoder encoder = new JpegBitmapEncoder();
                    encoder.QualityLevel = 94;
                    encoder.Frames.Add(BitmapFrame.Create(bitmapImage));
                    encoder.Save(fileStreamForSave);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
            finally
            {

            }
        }

        private void btnChromaSettings_Click(object sender, RoutedEventArgs e)
        {
            if (mMixerList1.SelectedItem != null)
            {
                LoadChromaPlugin(true, mMixerList1.SelectedItem);
                MCHROMAKEYLib.MChromaKey objChromaKey = GetChromakeyFilter(mMixerList1.SelectedItem);
                if (objChromaKey != null)
                {
                    FormChromaKey formCK = new FormChromaKey(objChromaKey);
                    formCK.ShowDialog();
                }
            }
        }
        public void GetConfigLocationData()
        {
            try
            {
                ConfigBusiness configBusiness = new ConfigBusiness();
                List<long> filterValues = new List<long>();
                //filterValues.Add((long)ConfigParams.IsAdvancedVideoEditActive);
                filterValues.Add((long)ConfigParams.VideoFlipMode);
                List<iMixConfigurationLocationInfo> ConfigValuesList = configBusiness.GetConfigLocation(locationId, ConfigManager.SubStoreId).Where(o => filterValues.Contains(o.IMIXConfigurationMasterId)).ToList();
                if (ConfigValuesList != null || ConfigValuesList.Count > 0)
                {
                    for (int i = 0; i < ConfigValuesList.Count; i++)
                    {
                        switch (ConfigValuesList[i].IMIXConfigurationMasterId)
                        {
                            case (int)ConfigParams.VideoFlipMode:
                                if (!string.IsNullOrEmpty(ConfigValuesList[i].ConfigurationValue))
                                    FlipMode = Convert.ToInt32(ConfigValuesList[i].ConfigurationValue);
                                else FlipMode = 0;
                                break;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }
        private MElement GetGuestVideoElement(string comparedID)
        {
            MElement GuestElement = null;
            int tempCount = 0;
            m_objComposer.ElementsGetCount(out tempCount);

            for (int i = 0; i < tempCount; i++)
            {
                MElement tmpElem;
                m_objComposer.ElementsGetByIndex(i, out tmpElem);
                if (tmpElem != null)
                {
                    int tempCount2 = 0;
                    ((IMElements)tmpElem).ElementsGetCount(out tempCount2);
                    for (int k = 0; k < tempCount2; k++)
                    {
                        MElement tmpChildElem;
                        ((IMElements)tmpElem).ElementsGetByIndex(k, out tmpChildElem);

                        if (tmpChildElem != null)
                        {
                            string strID; int have = 0;
                            tmpChildElem.AttributesHave("stream_id", out have, out strID);
                            ///  tmpChildElem.("stream_id", out strID);
                            if (strID == comparedID)
                            {
                                GuestElement = tmpChildElem;
                                break;
                            }
                        }
                    }

                }
            }
            return GuestElement;
        }

        private void ApplyFlip(MElement pElement, int FlipMode)
        {
            //MElement elemRoute = GetMElement(LiveVideoObjectID);
            if (FlipMode == 1)
                pElement.ElementMultipleSet("rh=180", 0.0);
            else if (FlipMode == 2)
                pElement.ElementMultipleSet("rv=180", 0.0);
        }

        private void btnReset_Click(object sender, RoutedEventArgs e)
        {
            GetElementInformation(strDefaultSceneSettings);
        }
    }


}
