﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using DigiPhoto.Common;
using DigiPhoto.IMIX.Business;
using DigiPhoto.IMIX.Model;
//using DigiPhoto.DataLayer;
using DigiAuditLogger;
using DigiPhoto.Utility.Repository.ValueType;
using FrameworkHelper;
using DigiPhoto.Utility.Repository.Data;

namespace DigiPhoto.Manage
{
    /// <summary>
    /// Interaction logic for RFIDMapping.xaml
    /// </summary>
    public partial class RFIDMapping : Window
    {
        //Dictionary<int, string> _lstPhotographer;
        //Dictionary<int, string> _lstDevices;
        private List<PhotoDetail> _lstPhotoDetails = null;
        public RFIDMapping()
        {
            InitializeComponent();
            GetDummyRFIDTagDetails();
            txbUserName.Text = LoginUser.UserName;
            txbStoreName.Text = LoginUser.StoreName;
            DateTime dt = (new CustomBusineses()).ServerDateTime().Date;
            dtFrom.DefaultValue = dt;
            dtFrom.Value = dt;
            FromDate.Value = dt;
            FromDate.DefaultValue = dt;
            dtTo.Value = (new CustomBusineses()).ServerDateTime();
            ToDate.Value = dtTo.Value;
            FillPhotoGraphersList();
            FillLocationDropdown();
            GetSeperatorRFIDTagDetails();
            dtFromRpt.Value = dt;
            dtToRpt.Value = (new CustomBusineses()).ServerDateTime().Date.AddHours(23);
            FillPhotoGraphersListForreport();
        }

        private void FillPhotoGraphersList()
        {
            List<UserInfo> lstUser = (new UserBusiness()).GetPhotoGraphersList();
            CommonUtility.BindComboWithSelect<UserInfo>(cmbPhotographer, lstUser, "Photographer", "UserId", 0, ClientConstant.SelectString);
            cmbPhotographer.SelectedIndex = 0;
            CommonUtility.BindCombo<UserInfo>(cmbPhotographerNon, lstUser, "Photographer", "UserId");
            cmbPhotographerNon.SelectedIndex = 0;
        }

        private void btnBack_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                ManageHome _objHome = new ManageHome();
                _objHome.Show();
                this.Close();
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
            }
        }

        private void btnLogout_Click(object sender, RoutedEventArgs e)
        {
            try
            {

                AuditLog.AddUserLog(Common.LoginUser.UserId, (int)FrameworkHelper.ActionType.Logout, "Logged out at ");

                Login _objLogin = new Login();
                _objLogin.Show();
                this.Close();
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
            }
            finally
            {
                MemoryManagement.FlushMemory();
            }
        }

        private void cmbPhotographer_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                List<DeviceInfo> lstDevice = (new DeviceManager()).GetPhotoGrapherDeviceList(cmbPhotographer.SelectedValue.ToInt32());
                CommonUtility.BindComboWithSelect<DeviceInfo>(cmbDevice, lstDevice, "Name", "DeviceId", 0, ClientConstant.SelectString);
                cmbDevice.SelectedIndex = 0;
            }
            catch (Exception ex)
            {

                ErrorHandler.ErrorHandler.LogError(ex);
            }
        }

        private void btnSearch_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                int photographerId = cmbPhotographer.SelectedValue.ToInt32();
                int deviceid = cmbDevice.SelectedValue.ToInt32();
                DateTime dateFrom = dtFrom.Value != null ? dtFrom.Value.ToDateTime() : DateTime.Now.Date;
                DateTime dateTo = dtTo.Value != null ? dtTo.Value.ToDateTime() : DateTime.Now;
                Dg_MappingStatus.ItemsSource = (new RfidBusiness()).GetRFIDAssociationSearch(photographerId, deviceid, dateFrom, dateTo);
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
            }
        }

        private void btnDetails_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Button _objbtn = (Button)sender;
                string photoIds = Convert.ToString(_objbtn.CommandParameter);

                //List<PhotoDetail> photoDetail = (new PhotoBusiness()).GetPhotoDetailsByPhotoIds(photoIds);
                //List<PhotoDetail> lstPhotoDetails = new List<PhotoDetail>();
                //foreach (var item in photoDetail)
                //{
                //    PhotoDetail photo = new PhotoDetail();
                //    photo = item;
                //    photo.FileName = System.IO.Path.Combine(LoginUser.DigiFolderThumbnailPath, item.FileName);
                //    photo.IsEnabled = false;
                //    lstPhotoDetails.Add(photo);
                //    photo = null;
                //}
                //lstImages.Items.Clear();
                //foreach (var photo in lstPhotoDetails)
                //{
                //    lstImages.Items.Add(photo);
                //}

                PhotoDetail photoDetailObj = new PhotoDetail()
                {
                    PhotoRFID = photoIds,
                    FileName = LoginUser.DigiFolderThumbnailPath,
                    IsEnabled = false
                };

                List<PhotoDetail> photoDetail = (new PhotoBusiness()).GetPhotoDetailsByPhotoIds(photoDetailObj);
                lstImages.ItemsSource = photoDetail;

                stkAssociation.Visibility = Visibility.Collapsed;
                GrdImageDetail.Visibility = Visibility.Visible;
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
            }
        }

        private void btnImageClose_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                txtRFIDAssociate.Text = string.Empty;
                lstImages.ItemsSource = string.Empty;
                GrdImageDetail.Visibility = Visibility.Collapsed;
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
            }
        }

        private void btnSearchNonAssociated_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                _lstPhotoDetails = new List<PhotoDetail>();
                int photographerId = cmbPhotographerNon.SelectedValue.ToInt32();
                DateTime dateFrom = FromDate.Value != null ? FromDate.Value.ToDateTime() : DateTime.Now.Date;
                DateTime dateTo = ToDate.Value != null ? ToDate.Value.ToDateTime() : DateTime.Now;
                //List<PhotoDetail> photoDetail = (new RfidBusiness()).GetRFIDNotAssociatedPhotos(photographerId, dateFrom, dateTo);
                //List<PhotoDetail> lstPhotoDetails = new List<PhotoDetail>();

                //foreach (var item in photoDetail)
                //{
                //    PhotoDetail photo = new PhotoDetail();
                //    photo.DG_Photos_CreatedOn = item.DG_Photos_CreatedOn;
                //    photo.Effects = item.Effects;
                //    photo.IsCropped = item.IsCropped;
                //    photo.IsGreen = item.IsGreen;
                //    photo.Layering = item.Layering;
                //    photo.LocationId = item.LocationId;
                //    photo.PhotoId = item.PhotoId;
                //    photo.PhotoRFID = item.PhotoRFID;
                //    photo.SubstoreId = item.SubstoreId;
                //    photo.FileName = System.IO.Path.Combine(LoginUser.DigiFolderThumbnailPath, item.FileName);
                //    _lstPhotoDetails.Add(photo);
                //    photo = null;
                //}
                //lstImages.Items.Clear();
                //foreach (var photo in _lstPhotoDetails)
                //{
                //    lstImages.Items.Add(photo);
                //}
                //  lstImages.ItemsSource = null;
                //  lstImages.ItemsSource = lstPhotoDetails;

                RFIDPhotoDetails rfidPhotoObj = new RFIDPhotoDetails()
                {
                    PhotoGrapherId = photographerId,
                    StartDate = dateFrom,
                    EndDate = dateTo,
                    FileName = LoginUser.DigiFolderThumbnailPath
                };
                _lstPhotoDetails = (new RfidBusiness()).GetRFIDNotAssociatedPhotos(rfidPhotoObj);
                lstImages.ItemsSource = _lstPhotoDetails;



                //photoDetail = null;
                stkAssociation.Visibility = Visibility.Visible;
                GrdImageDetail.Visibility = Visibility.Visible;
            }
            catch (Exception ex)
            {

                ErrorHandler.ErrorHandler.LogError(ex);
            }
        }

        private void cmbPhotographerNon_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                if (cmbPhotographerNon.SelectedIndex > 0)
                    btnSearchNonAssociated.IsEnabled = true;
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
            }
        }
        private void GetDummyRFIDTagDetails()
        {
            try
            {
                List<RFIDTagInfo> objRFIDTagInfoList = new RfidBusiness().GetDummyRFIDTags(0);
                DGDummyRFIDTags.ItemsSource = objRFIDTagInfoList;
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
            }
        }
        private bool CheckDummyTagValidations(string keyword)
        {
            //if (String.IsNullOrEmpty(keyword))
            //{
            //    return false;
            //}
            //else
            //{
            //    return true;
            //}

            return !String.IsNullOrEmpty(keyword);
        }
        private bool SaveDummyRFIDTagDetails(int DummyTagID, string TagId, bool isActive)
        {
            try
            {
                RFIDTagInfo rInfo = new RFIDTagInfo()
                {
                    DummyRFIDTagID = DummyTagID,
                    TagId = TagId,
                    IsActive = isActive
                };
                return (new RfidBusiness()).SaveUpdateDummyRFIDTags(rInfo);
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
                return false;
            }
        }
        private void btnDeleteTag_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Button btnSender = (Button)sender;
                int DummyTagId = btnSender.Tag.ToInt32();
                MessageBoxResult response = MessageBox.Show("Do you want to delete the RFID tag?", "Digiphoto", MessageBoxButton.YesNo, MessageBoxImage.Question);
                if (response == MessageBoxResult.Yes)
                {
                    RFIDTagInfo rInfo = (new RfidBusiness()).GetDummyRFIDTags(DummyTagId).FirstOrDefault();
                    if (rInfo != null)
                    {
                        bool isDeleted = (new RfidBusiness()).DeleteDummyRFIDTags(DummyTagId);
                        if (isDeleted)
                        {
                            GetDummyRFIDTagDetails();
                            MessageBox.Show("RFID tag deleted successfully.", "DigiPhoto", MessageBoxButton.OK, MessageBoxImage.Information);
                        }
                        else
                            MessageBox.Show("RFID tag could not be deleted!", "DigiPhoto", MessageBoxButton.OK, MessageBoxImage.Error);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("RFID tag could not be deleted!", "DigiPhoto", MessageBoxButton.OK, MessageBoxImage.Error);
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }

        private void btnSaveTag_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                string DummyRFIDTagKeyword = txtTag.Text;
                if (CheckDummyTagValidations(DummyRFIDTagKeyword))
                {
                    bool IsTagActive = (bool)chkIsTagActive.IsChecked;
                    bool isSaved = SaveDummyRFIDTagDetails(0, DummyRFIDTagKeyword, IsTagActive);
                    GetDummyRFIDTagDetails();
                    if (isSaved)
                    {
                        txtTag.Text = string.Empty;
                        MessageBox.Show("RFID tag saved successfully.", "DigiPhoto", MessageBoxButton.OK, MessageBoxImage.Information);
                    }
                    else
                        MessageBox.Show("RFID tag could not be saved due to some error!", "DigiPhoto", MessageBoxButton.OK, MessageBoxImage.Error);
                }
                else
                {
                    MessageBox.Show("RFID tag keyword cannot be empty!", "DigiPhoto", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
            }
        }
        private void btnAssociate_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                //StringBuilder photoIDs = new StringBuilder();
                if (string.IsNullOrEmpty(txtRFIDAssociate.Text))
                {
                    MessageBox.Show("Please enter RFID for association", "DigiPhoto", MessageBoxButton.OK, MessageBoxImage.Information);
                }
                else
                {
                    //List<PhotoDetail> photoDetailsInfo = _lstPhotoDetails.Where(x => x.IsChecked == true).ToList();
                    //foreach (PhotoDetail photo in photoDetailsInfo)
                    //{
                    //    photoIDs.Append(photo.PhotoId);
                    //    photoIDs.Append(",");
                    //}

                    string photoIDs = string.Join(", ", _lstPhotoDetails.Where(x => x.IsChecked == true).ToList().Select(i => i.PhotoId.ToString()).ToArray());

                    if (photoIDs.Length > 1)
                    {
                        //photoIDs.Remove((photoIDs.Length - 1), 1);
                        MapNonAssociatedImages(txtRFIDAssociate.Text, photoIDs.ToString());
                        MessageBox.Show("Images associated successfully", "DigiPhoto", MessageBoxButton.OK, MessageBoxImage.Information);
                        btnSearchNonAssociated_Click(null, new RoutedEventArgs());
                    }
                    else
                        MessageBox.Show("Please select Images to associate", "DigiPhoto", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
            }
        }

        private void chkSelectedimages_Click(object sender, RoutedEventArgs e)
        {

        }
        private bool MapNonAssociatedImages(string cardUniqueIdentifier, string photoIDS)
        {
            try
            {
                return (new RfidBusiness()).MapNonAssociatedImages(cardUniqueIdentifier, photoIDS);
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
                return false;
            }
        }
        private void btnSaveSeperatorTag_Click(object sender, RoutedEventArgs e)
        {

            if (string.IsNullOrWhiteSpace(txtSeperatortxtTag.Text))
            {
                MessageBox.Show("Separator RFID tag can not be empty");
                return;
            }
            bool isSaved = SaveSeperatorRFIDTagDetails(0, txtSeperatortxtTag.Text, Convert.ToInt32(cmbLocation.SelectedValue), (bool)chkIsSeperatorTagActive.IsChecked);
            if (isSaved)
                MessageBox.Show("Separator RFID tag saved successfully");
            else
                MessageBox.Show("Some error occured");

            clearControls();
            GetSeperatorRFIDTagDetails();
        }
        private void FillLocationDropdown()
        {
            Dictionary<string, string> lstLocations = null;
            lstLocations = new Dictionary<string, string>();
            lstLocations.Add("--Select--", "0");
            RfidBusiness RFIDBusiness = new RfidBusiness();
            foreach (LocationInfo location in RFIDBusiness.GetAllLocations())
            {
                lstLocations.Add(location.DG_Location_Name, location.DG_Location_ID.ToString());
            }
            cmbLocation.ItemsSource = lstLocations;
            cmbLocation.SelectedValue = "0";
        }
        private bool SaveSeperatorRFIDTagDetails(int SeperatorTagID, string TagId, int locationId, bool isActive)
        {
            try
            {
                SeperatorTagInfo seperatorTagInfo = new SeperatorTagInfo()
                {
                    SeparatorRFIDTagID = SeperatorTagID,
                    TagID = TagId,
                    LocationId = locationId,
                    IsActive = isActive
                };
                return (new RfidBusiness()).SaveSeperatorRFIDTagsInfo(seperatorTagInfo);
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
                return false;
            }
        }
        private void GetSeperatorRFIDTagDetails()
        {
            try
            {
                List<SeperatorTagInfo> seperatorTagInfoList = new RfidBusiness().GetSeperatorRFIDTags(0);
                DGSeperatorRFIDTags.ItemsSource = seperatorTagInfoList;
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
            }
        }
        private void btnDeleteSeperatorTag_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Button btnsender = (Button)sender;
                int seperatorRFIDTagID = btnsender.Tag.ToInt32();
                RfidBusiness RfidBusiness = new RfidBusiness();
                MessageBoxResult response = MessageBox.Show("Do you want to delete the separator RFID tag?", "Digiphoto", MessageBoxButton.YesNo, MessageBoxImage.Question);
                if (response == MessageBoxResult.Yes)
                {
                    bool isDeleted = RfidBusiness.DeleteSeperatorTag(seperatorRFIDTagID);
                    if (isDeleted)
                        MessageBox.Show("Separator RFID tag deleted successfully");
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
            }
            GetSeperatorRFIDTagDetails();
        }
        private void clearControls()
        {
            txtSeperatortxtTag.Text = string.Empty;
            cmbLocation.SelectedIndex = 0;
        }

        private void btnReportearch_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (dtFromRpt.Value == null)
                {
                    MessageBox.Show("Please enter from date", "DEI");
                    return;
                }
                if (dtToRpt.Value == null)
                {
                    MessageBox.Show("Please enter to date", "DEI");
                    return;
                }
                if (dtFromRpt.Value.ToDateTime() > dtToRpt.Value.ToDateTime())
                {
                    MessageBox.Show("From date is not greater than to date", "DEI");
                    return;
                }

                int? photographerId = cmbPhotographerRpt.SelectedValue.ToInt32();
                if (photographerId == 0)
                    photographerId = null;
                DateTime dateFrom = dtFromRpt.Value.ToDateTime();
                DateTime dateTo = dtToRpt.Value.ToDateTime();
                Dg_PhotographerReport.ItemsSource = (new RfidBusiness()).GetPhotographerRFIDAssociation(photographerId, dateFrom, dateTo);
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
            }
        }
        private void FillPhotoGraphersListForreport()
        {
            List<UserInfo> lstPhotographer = (new UserBusiness()).GetPhotoGraphersList();
            CommonUtility.BindComboWithSelect<UserInfo>(cmbPhotographerRpt, lstPhotographer, "Photographer", "UserId", 0, "Select All");
            cmbPhotographerRpt.SelectedIndex = 0;
        }
    }
}
