﻿using DigiAuditLogger;
using DigiPhoto.Common;
using DigiPhoto.IMIX.Business;
using DigiPhoto.IMIX.Model;
using DigiPhoto.Utility.Repository.ValueType;
using FrameworkHelper.Common;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Management;
using System.Security.Principal;
using System.ServiceProcess;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;

namespace DigiPhoto.Manage
{
    /// <summary>
    /// Interaction logic for SoftwareHealthCheckup.xaml
    /// </summary>
    public partial class SoftwareHealthCheckup : Window
    {
        #region Constructor
        public SoftwareHealthCheckup()
        {
            try
            {
                InitializeComponent();
                GetAllrunningServices();
                txbUserName.Text = LoginUser.UserName;
                txbStoreName.Text = LoginUser.StoreName;
                cpuCounter = new PerformanceCounter();
                cpuCounter.CategoryName = "Processor";
                cpuCounter.CounterName = "% Processor Time";
                cpuCounter.InstanceName = "_Total";
                ramCounter = new PerformanceCounter("Memory", "Available MBytes");

                DigiDate.Text = "Digi " + CurrentRegistryVersion();
                SystemMetaData();
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
            }
        }
        #endregion

        #region Declaration

        protected PerformanceCounter cpuCounter;
        protected PerformanceCounter ramCounter;
        #endregion

        #region Common Methods
        /// <summary>
        /// Gets the current version of Digiphoto Assembly
        /// </summary>
        /// <returns></returns>
        private string CurrentRegistryVersion()
        {
            ModifyRegistry mr = new ModifyRegistry();
            string currRegistry = mr.Read("InstallVersion");
            return currRegistry;
        }
        public string getCurrentCpuUsage()
        {
            PerformanceCounter cpuCounter;
            cpuCounter = new PerformanceCounter();
            cpuCounter.CategoryName = "Processor";
            cpuCounter.CounterName = "% Processor Time";
            cpuCounter.InstanceName = "_Total";
            return cpuCounter.NextValue() + "%";
        }

        /* 
        Call this method every time you need to get 
        the amount of the available RAM in Mb 
        */
        /// <summary>
        /// Gets the available parameter.
        /// </summary>
        /// <returns></returns>
        public string getAvailableRAM()
        {
            return ramCounter.NextValue().ToString();
        }

        /// <summary>
        /// Determines whether this instance is administrator.
        /// </summary>
        /// <returns></returns>
        public static bool IsAdministrator()
        {
            WindowsIdentity identity = WindowsIdentity.GetCurrent();

            if (null != identity)
            {
                WindowsPrincipal principal = new WindowsPrincipal(identity);
                return principal.IsInRole(WindowsBuiltInRole.Administrator);
            }

            return false;
        }
        /// <summary>
        /// Loads the pie chart data.
        /// </summary>
        /// <param name="Available">The available.</param>
        /// <param name="Used">The used.</param>
        /// <param name="Application">The application.</param>
        /// <param name="Total">The total.</param>
        private void LoadPieChartData(double available, double used, double application, double total)
        {
            ((System.Windows.Controls.DataVisualization.Charting.PieSeries)mcChart.Series[0]).ItemsSource =
                new KeyValuePair<string, double>[]{
                new KeyValuePair<string, double>("Available Memory (in MB)", available),
                new KeyValuePair<string, double>("System Memory Consumption(in MB)",used)};

            ((System.Windows.Controls.DataVisualization.Charting.PieSeries)mcChart1.Series[0]).ItemsSource =
              new KeyValuePair<string, double>[]{
                new KeyValuePair<string, double>("Total Memory", total),
                new KeyValuePair<string, double>("DigiPhoto Consumption",application)};

        }

        /// <summary>
        /// Systems the meta data.
        /// </summary>
        public void SystemMetaData()
        {

            ObjectQuery objectQuery = new ObjectQuery("select Capacity from Win32_PhysicalMemory");
            ManagementObjectSearcher searcher = new
            ManagementObjectSearcher(objectQuery);
            ManagementObjectCollection vals = searcher.Get();
            Double totalCapacity = 0;
            Double available = 0;
            Double used = 0;
            foreach (ManagementObject val in vals)
            {
                totalCapacity += val.GetPropertyValue("Capacity").ToDouble();
            }

            TotalMemory.Text = "Installed Memory(RAM): " + (totalCapacity / 1073741824) + " GB";
            used = ((totalCapacity / 1024f) / 1024f) - getAvailableRAM().ToDouble();

            available = getAvailableRAM().ToDouble(); // MB
            double TotalCap = ((totalCapacity / 1024f) / 1024f);
            used = TotalCap - available;

            var counter = new PerformanceCounter("Process", "Working Set - Private", System.Diagnostics.Process.GetCurrentProcess().ProcessName);
            LoadPieChartData(available, used, (counter.RawValue / 1024f / 1024f), TotalCap);


            ManagementObjectSearcher query1 = new ManagementObjectSearcher("SELECT Name FROM Win32_Processor");
            ManagementObjectCollection queryCollection1 = query1.Get();
            string processor = string.Empty;
            foreach (ManagementObject mo in queryCollection1)
            {
                processor = mo["Name"].ToString();
            }

            // Code Added by Anis on 28Jan2019
            String subKey = @"SOFTWARE\Wow6432Node\Microsoft\Windows NT\CurrentVersion";
            RegistryKey key = Registry.LocalMachine;
            RegistryKey skey = key.OpenSubKey(subKey);
            Processor.Text = "Processor: " + processor;
            //OS.Text = "Operating System: " + System.Environment.OSVersion.ToString();
            OS.Text = "Operating System: " + skey.GetValue("ProductName").ToString();
            // End here
            MachineName.Text = "Computer Name: " + System.Environment.MachineName.ToString();

            if (System.Environment.Is64BitOperatingSystem)
            {
                SystemType.Text = "System Type : 64-bit Operating System";
            }
            else
            {
                SystemType.Text = "System Type : 32-bit Operating System";
            }

            // Code Added by Anis on 28Jan2019
            float currentCpuUsage = cpuCounter.NextValue(); 
            System.Threading.Thread.Sleep(1000);
            currentCpuUsage = cpuCounter.NextValue(); 

            CPUUsage.Text = "CPU Usage: " + currentCpuUsage + "%";
            // End here
        }
        /// <summary>
        /// Gets the allrunning services.
        /// </summary>
        private void GetAllrunningServices()
        {
            ServicesStatus _objnew = null;

            List<ServicesInfo> services = (new SoftwareHealthCheckBusiness()).GetRunningServices();
            lstServices.Items.Clear();
            foreach (var item in services)
            {
                _objnew = new ServicesStatus();
                Process[] processlist;
                if (item.DG_Sevice_Name == "DigiEmailService")
                    processlist = Process.GetProcessesByName("EmailService");
                else if (item.DG_Sevice_Name == "DigiphotoDataSyncService")
                    processlist = Process.GetProcessesByName("DataSyncWinService");//project exe name is different than service name
                else if (item.DG_Sevice_Name == "DigiPreSoldService")
                    processlist = Process.GetProcessesByName("PreSoldService");
                else if (item.DG_Sevice_Name == "DigiReportExportService")
                    processlist = Process.GetProcessesByName("DGReportExportService");
                else if (item.DG_Sevice_Name == "DigiWatermarkService")
                    processlist = Process.GetProcessesByName("DigiWatermarkService");
                //Start Author Bhavin Udani 05 March 2021 Changes to make DigiWatchManualProcess 
                //and DigiphotoOnlineOrdersStatus start and stoppable from healthcheckup
                else if (item.DG_Sevice_Name == "DigiWatchManualProcess")
                    processlist = Process.GetProcessesByName("ManualDownloadProcess");
                else if (item.DG_Sevice_Name == "DigiphotoOnlineOrdersStatus")
                    processlist = Process.GetProcessesByName("DigiOnlineSyncServiceStatus");
                //End Author Bhavin Udani 05 March 2021 Changes to make DigiWatchManualProcess 
                //and DigiphotoOnlineOrdersStatus start and stoppable from healthcheckup
                else
                    processlist = Process.GetProcessesByName(item.DG_Sevice_Name);

                var searchItem = processlist.FirstOrDefault();
                if (searchItem != null)
                {
                    _objnew.ButtonText = "Stop";
                    _objnew.RunningStatus = "Running";
                    _objnew.ServiceName = item.DG_Service_Display_Name;
                    _objnew.Originalservicename = item.DG_Sevice_Name;
                    _objnew.ServicePath = item.DG_Service_Path;
                    _objnew.IsInterface = item.IsInterface;
                    _objnew.BackColor = "#9AFF9A";
                    _objnew.BackOffsetColor = "#006400";
                }
                else
                {
                    _objnew.ButtonText = "Start";
                    _objnew.RunningStatus = "Not running";
                    _objnew.ServiceName = item.DG_Service_Display_Name;
                    _objnew.Originalservicename = item.DG_Sevice_Name;
                    _objnew.ServicePath = item.DG_Service_Path;
                    _objnew.IsInterface = item.IsInterface;
                    _objnew.BackColor = "#FFE46B6B";
                    _objnew.BackOffsetColor = "#FFB60000";
                }
                lstServices.Items.Add(_objnew);
            }
        }
        #endregion

        #region Events
        /// <summary>
        /// Handles the Click event of the btnAction control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void btnAction_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Process _objprocess = new Process();
                Button btn = new Button();
                btn = (Button)sender;
                string servicename = btn.Tag.ToString();
                if (btn.Content.ToString() == "Start")
                {
                    if (btn.ToolTip.ToBoolean())
                    {
                        ProcessStartInfo info = new ProcessStartInfo
                        {
                            UseShellExecute = true,
                            FileName = btn.CommandParameter.ToString()
                        };
                        if (!IsAdministrator())
                        {
                            info.Verb = "runas";
                        }
                        try
                        {
                            Process.Start(info);
                        }
                        catch (Win32Exception ex)
                        {
                            ErrorHandler.ErrorHandler.LogError(ex);
                        }
                        btnback.Focus();
                    }
                    else
                    {
                        ServiceController controller = new ServiceController(servicename);
                        controller.Start();
                    }
                }
                else
                {
                    if (btn.ToolTip.ToBoolean())
                    {
                        Process[] processes = Process.GetProcesses();
                        foreach (Process process in processes)
                        {
                            if (process.ProcessName == servicename)
                            {
                                try
                                {
                                    process.Kill();
                                    Thread.Sleep(2000);
                                }
                                catch (Exception ex)
                                {
                                    ErrorHandler.ErrorHandler.LogError(ex);
                                }
                            }
                        }
                    }
                    else
                    {
                        ServiceController controller = new ServiceController(servicename);
                         string status=controller.Status.ToString();
                         if (status != "Stopped")
                         {
                        controller.Stop();
                        controller.WaitForStatus(ServiceControllerStatus.Stopped);
                    }
                        
                    }
                }
                GetAllrunningServices();
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
            }
        }
        /// <summary>
        /// Handles the Click event of the btnLogout control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void btnLogout_Click(object sender, RoutedEventArgs e)
        {
            try
            {

                AuditLog.AddUserLog(Common.LoginUser.UserId, 39, "Logged out at ");

                Login _objLogin = new Login();
                _objLogin.Show();
                this.Close();
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
            }
            finally
            {
                MemoryManagement.FlushMemory();
            }
        }
        /// <summary>
        /// Handles the Click event of the btnback control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void btnback_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                ManageHome _objHome = new ManageHome();
                _objHome.Show();
                this.Close();
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
            }
        }
        /// <summary>
        /// Handles the Click event of the btnexplore control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void btnexplore_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Datetimelist.Items.Clear();
                versionlist.Items.Clear();
                List<VersionHistoryInfo> odjVersion = (new SoftwareHealthCheckBusiness()).GetVersionDetails(System.Environment.MachineName);
                foreach (var item in odjVersion)
                {
                    DateTime dt = (DateTime)item.DG_Version_Date;
                    DateTime mydatetime = new DateTime(dt.Ticks);
                    versionlist.Items.Add(item.DG_Version_Number);
                    Datetimelist.Items.Add(mydatetime.ToString("MMMM dd yyyy, hh:mm:ss"));
                }
                GrdPopup.Visibility = Visibility.Visible;
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
            }
        }

        /// <summary>
        /// Handles the Click event of the btncloseversion control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void btncloseversion_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                GrdPopup.Visibility = Visibility.Collapsed;
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
            }
        }

        #endregion
    }
    #region Custom Classes

    #endregion
}
