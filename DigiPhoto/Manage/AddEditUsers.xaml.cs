﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using DigiPhoto.DataLayer;
using DigiPhoto.DataLayer.Model;
using System.Collections;
using DigiPhoto.Common;
using System.Text.RegularExpressions;
using DigiAuditLogger;
using FrameworkHelper;
using DigiPhoto.IMIX.Business;
using DigiPhoto.IMIX.Model;
using DigiPhoto.Utility.Repository.ValueType;
using DigiPhoto.Utility.Repository.Data;

namespace DigiPhoto.Manage
{
    public partial class AddEditUsers : Window
    {
        #region Declaration & Properties
        TextBox controlon;
        public string _email;
        public string _password;
        public string _roleName;
        public string _firstname;
        public string _username;
        public string _lastname;
        public string _storename;
        public string _location;
        public string _status;
        public string _Temparmsg;

        //DigiPhotoDataServices _objDataLayer = new DigiPhotoDataServices();

        Dictionary<string, string> lstStatus = new Dictionary<string, string>();
        int userId = 0;
        private Dictionary<string, Int32> _locationList;
        public Dictionary<string, Int32> LocationList
        {
            get { return _locationList; }
            set { _locationList = value; }
        }
        #endregion

        #region Constructor
        /// <summary>
        /// Initializes a new instance of the <see cref="AddEditUsers"/> class.
        /// </summary>
        public AddEditUsers()
        {
            InitializeComponent();
            BindGrid();
            FillDropDown();
            ClearControls();
            Add_EditUser.Visibility = Visibility.Collapsed;
            grdUsers.Visibility = Visibility.Visible;
            txbUserName.Text = LoginUser.UserName;
            txbStoreName.Text = LoginUser.StoreName;

        }
        #endregion

        #region Common Methods
        /// <summary>
        /// Binds the user datagrid.
        /// </summary>
        public void BindGrid()
        {
            UserBusiness useBiz = new UserBusiness();
            //List<vw_GetUserDetails> _objUserData = _objDataLayer.GetUserDetailsByUserId(0);
            grdUsers.ItemsSource = useBiz.GetChildUserDetailByUserId(LoginUser.RoleId);

            //grdUsers.ItemsSource = useBiz.GetUserDetailByUserId(0, null, LoginUser.RoleId);

            //if (LoginUser.RoleId != 7)
            //    grdUsers.ItemsSource = _objUserData.Where(t => t.DG_Store_ID == LoginUser.StoreId && t.DG_User_Roles_Id != 7);
            //else
            //    grdUsers.ItemsSource = _objUserData.Where(t => t.DG_Store_ID == LoginUser.StoreId);
        }

        /// <summary>
        /// Checks the tempar data for logging in audit log.
        /// </summary>
        /// <returns></returns>
        public string checkTempar()
        {
            _Temparmsg = _username + " details updated,";
            bool iSAnd = false;

            if (_email != txtEmail.Text)
            {
                _Temparmsg += " email '" + _email + "' has been changed to '" + txtEmail.Text + "'";
                iSAnd = true;
            }
            if (_password != txtPassword.Password)
            {
                if (iSAnd)
                {
                    _Temparmsg += " and password has been changed";
                }
                else
                {
                    _Temparmsg += " password has been changed";
                }
                iSAnd = true;
            }
            if (_location != cmbLocation.Text)
            {
                if (iSAnd)
                {
                    _Temparmsg += " and location' " + _location + " ' has been changed to '" + cmbLocation.Text + "'";
                }
                else
                {
                    _Temparmsg += " location'" + _location + "' has been changed to '" + cmbLocation.Text + "'";
                }
                iSAnd = true;
            }

            if (_roleName != cmbRoleName.Text)
            {
                if (iSAnd)
                {
                    _Temparmsg += " and role '" + _roleName + "' has been changed to '" + cmbRoleName.Text + "'";
                }
                else
                {
                    _Temparmsg += "role '" + _roleName + "' has been changed to '" + cmbRoleName.Text + "'"; ;
                }
                iSAnd = true;
            }

            if (_status != cmbStatus.Text)
            {
                if (iSAnd)
                {
                    _Temparmsg += " and user status '" + _status + "' has been changed to '" + cmbStatus.Text + "'";
                }
                else
                {
                    _Temparmsg += "user status '" + _status + "' has been changed to '" + cmbStatus.Text + "'";
                }
                iSAnd = true;
            }
            if (_firstname != txtFName.Text)
            {
                if (iSAnd)
                {
                    _Temparmsg += " and first name '" + _firstname + "' has been changed to '" + txtFName.Text + "'";
                }
                else
                {
                    _Temparmsg += " first name '" + _firstname + "' has been changed to '" + txtFName.Text + "'";
                }
                iSAnd = true;


            }

            if (_lastname != txtLName.Text)
            {
                if (iSAnd)
                {
                    _Temparmsg += " and last name '" + _lastname + "' has been changed to '" + txtLName.Text + "'";
                }
                else
                {
                    _Temparmsg += " last name '" + _lastname + "' has been changed to '" + txtLName.Text + "'";
                }
                iSAnd = true;


            }
            return _Temparmsg;

        }
        /// <summary>
        /// Fills the drop down with required data.
        /// </summary>
        public void FillDropDown()
        {
            ////////////////////////Status DropDown////////////////////////

            lstStatus.Add("--Select--", "0");
            lstStatus.Add("Active", "1");
            lstStatus.Add("InActive", "2");
            cmbStatus.ItemsSource = lstStatus;
            cmbStatus.SelectedValue = "0";

            /////////////////////////Role Dropdown//////////////////////////
            ////////////////////////Store Dropdown//////////////////////////

            //List<DG_Store> _lstStores = new List<DG_Store>();
            //DG_Store _objitem = new DG_Store();
            //_objitem.DG_Store_Name = "--Select--";
            //_objitem.DG_Store_pkey = 0;
            //_lstStores.Add(_objitem);
            //foreach (var item in _objDataLayer.GetStoreName())
            //{
            //    _lstStores.Add(item);
            //}
            //cmbStoreName.ItemsSource = _lstStores;
            //StoreSubStoreDataBusniess stoBiz = new StoreSubStoreDataBusniess();
            cmbStoreName.ItemsSource = (new StoreSubStoreDataBusniess()).SelStoreDataDir();



            ////////////////////////Store Dropdown//////////////////////////

            //List<RoleInfo> _lstRoles = new List<RoleInfo>();



            //RoleInfo _objRoleitem = new RoleInfo();
            //_objRoleitem.DG_User_Role = "--Select--";
            //_objRoleitem.DG_User_Roles_pkey = 0;
            //_lstRoles.Add(_objRoleitem);


            RolePermissionsBusniess rolBiz = new RolePermissionsBusniess();
            //foreach (var item in rolBiz.GetRoleNames())
            //{
            //    if (LoginUser.RoleId == 7)
            //    {
            //        _lstRoles.Add(item);
            //    }
            //    else
            //    {
            //        if (item.DG_User_Roles_pkey != 7)
            //            _lstRoles.Add(item);
            //    }


            //}


            //List<RoleInfo> roles = new List<RoleInfo>();
            ////roles = rolBiz.GetRoleNames(LoginUser.RoleId, string.Empty);
            //roles = (new RolePermissionsBusniess()).GetChildUserData(LoginUser.RoleId).Where(o => o.DG_User_Roles_pkey != LoginUser.RoleId).ToList();
            //CommonUtility.BindComboWithSelect<RoleInfo>(cmbRoleName, roles, "DG_User_Role", "DG_User_Roles_pkey", 0, ClientConstant.SelectString);
            //cmbLocation.SelectedValue = "0";

            //cmbRoleName.ItemsSource = rolBiz.GetRoleNames(LoginUser.RoleId, string.Empty);
            //cmbLocation.ItemsSource = null;

            LocationList = new Dictionary<string, int>();
            //List<DG_Location> lstLocations = new List<DG_Location>();
            cmbStoreName.SelectedValue = LoginUser.StoreId;
            cmbStoreName.IsEnabled = false;
            int storeId = cmbStoreName.SelectedValue.ToInt32();
            LocationBusniess locBiz = new LocationBusniess();
            cmbLocation.ItemsSource = locBiz.GetLocationNameDir(storeId);
            cmbLocation.SelectedValue = "0";
            //if (lstLocations != null)
            //{
            //    LocationList = new Dictionary<string, int>();
            //    LocationList.Add("--Select--", 0);
            //    foreach (var item in lstLocations)
            //    {
            //        LocationList.Add(item.DG_Location_Name, item.DG_Location_pkey);
            //    }
            //}
            //else
            //{
            //    LocationList = new Dictionary<string, int>();
            //    LocationList.Add("--Select--", 0);
            //}
            //cmbLocation.ItemsSource = LocationList;
            //cmbLocation.SelectedValue = "0";

        }
        /// <summary>
        /// Clears the controls.
        /// </summary>
        public void ClearControls()
        {
            txtEmail.Text = "";
            txtEmail.BorderBrush = new System.Windows.Media.SolidColorBrush(Colors.Gray);
            txtFName.Text = "";
            txtFName.BorderBrush = new System.Windows.Media.SolidColorBrush(Colors.Gray);
            txtLName.Text = "";
            txtLName.BorderBrush = new System.Windows.Media.SolidColorBrush(Colors.Gray);
            txtMobileNumber.Text = "";
            txtMobileNumber.BorderBrush = new System.Windows.Media.SolidColorBrush(Colors.Gray);
            cmbRoleName.SelectedValue = "0";
            cmbRoleName.BorderBrush = new System.Windows.Media.SolidColorBrush(Colors.Gray);
            cmbStatus.SelectedValue = "0";
            cmbStatus.BorderBrush = new System.Windows.Media.SolidColorBrush(Colors.Gray);
            cmbStoreName.SelectedValue = LoginUser.StoreId;
            cmbStoreName.BorderBrush = new System.Windows.Media.SolidColorBrush(Colors.Gray);
            txtFName.Focus();
            cmbLocation.SelectedValue = "0";
            cmbLocation.BorderBrush = new System.Windows.Media.SolidColorBrush(Colors.Gray);
            txtUserName.Text = "";
            txtPassword.Password = "";
            txtPassword.BorderBrush = new System.Windows.Media.SolidColorBrush(Colors.Gray);
            txtEmpId.Text = "";
            txtEmpId.BorderBrush = new System.Windows.Media.SolidColorBrush(Colors.Gray);
        }
        /// <summary>
        /// put validation on controls 
        /// </summary>
        /// <returns></returns>
        public bool ControlValidation()
        {
            bool retvalue = true;
            if (string.IsNullOrEmpty(txtFName.Text))
            {
                txtFName.BorderBrush = new System.Windows.Media.SolidColorBrush(Colors.Red);
                retvalue = false;
            }
            else
            {
                txtFName.BorderBrush = new System.Windows.Media.SolidColorBrush(Colors.Gray);
            }
            if (string.IsNullOrEmpty(txtUserName.Text))
            {
                txtUserName.BorderBrush = new System.Windows.Media.SolidColorBrush(Colors.Red);
                retvalue = false;
            }
            else
            {
                txtUserName.BorderBrush = new System.Windows.Media.SolidColorBrush(Colors.Gray);
            }
            if (string.IsNullOrEmpty(txtEmail.Text))
            {
                txtEmail.BorderBrush = new System.Windows.Media.SolidColorBrush(Colors.Red);
                retvalue = false;
            }
            else
            {
                txtEmail.BorderBrush = new System.Windows.Media.SolidColorBrush(Colors.Gray);
            }

            if (string.IsNullOrEmpty(txtMobileNumber.Text))
            {
                txtMobileNumber.BorderBrush = new System.Windows.Media.SolidColorBrush(Colors.Red);
                retvalue = false;
            }
            else
            {
                txtMobileNumber.BorderBrush = new System.Windows.Media.SolidColorBrush(Colors.Gray);
            }
            if (cmbRoleName.SelectedValue.ToString() == "0")
            {
                cmbRoleName.BorderBrush = new System.Windows.Media.SolidColorBrush(Colors.Red);
                retvalue = false;
            }
            else
            {
                cmbRoleName.BorderBrush = new System.Windows.Media.SolidColorBrush(Colors.Gray);
            }
            if (cmbStatus.SelectedValue.ToString() == "0")
            {
                cmbStatus.BorderBrush = new System.Windows.Media.SolidColorBrush(Colors.Red);
                retvalue = false;
            }
            else
            {
                cmbStatus.BorderBrush = new System.Windows.Media.SolidColorBrush(Colors.Gray);
            }
            if (cmbStoreName.SelectedValue.ToString() == "0")
            {
                cmbStoreName.BorderBrush = new System.Windows.Media.SolidColorBrush(Colors.Red);
                retvalue = false;
            }
            else
            {
                cmbStoreName.BorderBrush = new System.Windows.Media.SolidColorBrush(Colors.Gray);
            }



            return retvalue;
        }

        private bool ControlValueValidate(bool retvalue)
        {

            return retvalue;
        }
        #endregion

        #region Events
        /// <summary>
        /// Handles the SelectionChanged event of the cmbStoreName control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="SelectionChangedEventArgs"/> instance containing the event data.</param>
        private void cmbStoreName_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                cmbLocation.ItemsSource = null;
                LocationList = new Dictionary<string, int>();
                //List<DG_Location> lstLocations = new List<DG_Location>();
                int storeId = cmbStoreName.SelectedValue.ToInt32();
                //lstLocations = _objDataLayer.GetLocationName(storeId);
                //if (lstLocations != null)
                //{
                //    LocationList = new Dictionary<string, int>();
                //    LocationList.Add("--Select--", 0);
                //    foreach (var item in lstLocations)
                //    {
                //        LocationList.Add(item.DG_Location_Name, item.DG_Location_pkey);
                //    }
                //}
                //else
                //{
                //    LocationList = new Dictionary<string, int>();
                //    LocationList.Add("--Select--", 0);
                //}
                //cmbLocation.ItemsSource = LocationList;
                //cmbLocation.SelectedValue = "0";
                LocationBusniess locBiz = new LocationBusniess();
                cmbLocation.ItemsSource = locBiz.GetLocationNameDir(storeId);
                cmbLocation.SelectedValue = "0";
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }

        /// <summary>
        /// Handles the Click event of the btnManageRole control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs" /> instance containing the event data.</param>
        private void btnManageRole_Click(object sender, RoutedEventArgs e)
        {
            try
            {

                UserPermission _objPwindow = new UserPermission();
                //RolePermissionsBusniess rolBiz = new RolePermissionsBusniess();
                //List<PermissionRoleInfo> _objPList = rolBiz.GetPermissionData(LoginUser.RoleId);
                //var Useritem = _objPList.Where(t => t.DG_Permission_Id == 19).FirstOrDefault();
                //if (Useritem != null)
                //{
                //    _objPwindow.Show();
                //    this.Close();
                //}

                List<RoleInfo> _objRoleList = (new RolePermissionsBusniess()).GetChildUserData(LoginUser.RoleId).Where(o => o.DG_User_Roles_pkey != LoginUser.RoleId).ToList() ;;
                if (_objRoleList != null)
                {
                    _objPwindow.Show();
                    this.Close();
                }
                else
                {
                    MessageBox.Show(UIConstant.NotAuthorized);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }

        }

        /// <summary>
        /// Handles the LostFocus event of the txtUserName control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void txtUserName_LostFocus(object sender, RoutedEventArgs e)
        {
            if (!String.IsNullOrEmpty(txtUserName.Text.Trim()) && SPSearchCancel.Visibility == Visibility.Collapsed)
            {
                UserBusiness useBiz = new UserBusiness();
                var item2 = useBiz.GetUserDetailByUserId(0, txtUserName.Text, LoginUser.RoleId);
                // var item2 = _objDataLayer.GetUserDetailsByUserName(txtUserName.Text);
                if (item2.Count != 0)
                {
                    MessageBox.Show(UIConstant.UsernameAlreadyExists);
                }
            }
        }
        /// <summary>
        /// Handles the Click event of the btn control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void btn_Click(object sender, RoutedEventArgs e)
        {

            Button _objbtn = new Button();
            _objbtn = (Button)sender;
            switch (_objbtn.Content.ToString())
            {
                case "ENTER":
                    {
                        KeyBorder.Visibility = Visibility.Collapsed;
                        controlon.Focus();
                        break;
                    }
                case "SPACE":
                    {
                        if (controlon.SelectionStart >= 0)
                        {
                            int index = controlon.SelectionStart;
                            controlon.Text = controlon.Text.Insert(controlon.SelectionStart, " ");
                            controlon.Select(index + 1, 0);
                        }
                        controlon.Focus();
                        break;
                    }
                case "CLOSE":
                    {
                        KeyBorder.Visibility = Visibility.Collapsed;
                        break;
                    }
                case "Back":
                    {
                        if (controlon.Text.Length > 0 && controlon.SelectionStart > 0)
                        {
                            int index = controlon.SelectionStart;
                            controlon.Text = controlon.Text.Remove(controlon.SelectionStart - 1, 1);
                            controlon.Select(index - 1, 0);
                        }
                        controlon.Focus();
                        break;
                    }
                default:
                    {
                        if (controlon.SelectionStart >= 0)
                        {
                            int index = controlon.SelectionStart;
                            controlon.Text = controlon.Text.Insert(controlon.SelectionStart, _objbtn.Content.ToString());
                            controlon.Select(index + 1, 0);
                        }
                        controlon.Focus();
                    }
                    break;
            }
        }
        /// <summary>
        /// Handles the Loaded event of the Window control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {

            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }

        }

        /// <summary>
        /// Handles the Click event of the btnEdit control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void btnEdit_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                List<RoleInfo> roles = new List<RoleInfo>();
                //roles = rolBiz.GetRoleNames(LoginUser.RoleId, string.Empty);
                
                //cmbRoleName.SelectedValue = LoginUser.RoleId;

                Button btnSender = (Button)sender;
                userId = btnSender.Tag.ToInt32();
                UserBusiness useBiz = new UserBusiness();
                List<UsersInfo> _objUserDetails = useBiz.GetUserDetailByUserId(btnSender.Tag.ToInt32(), null, LoginUser.RoleId);
                if (_objUserDetails.Count > 0)
                {
                    //txtEmail.Text = _objUserDetails.First().;
                    txtFName.Text = _objUserDetails.First().DG_User_First_Name;
                    txtLName.Text = _objUserDetails.First().DG_User_Last_Name;
                    txtEmail.Text = _objUserDetails.First().DG_User_Email;
                    txtUserName.Text = _objUserDetails.First().DG_User_Name;
                    txtEmpId.Text=_objUserDetails.First().DG_Emp_Id;//BY KCB ON 20 JUN 2020 For display user' employee id
                    txtPassword.Password = CryptorEngine.Decrypt(_objUserDetails.First().DG_User_Password, true);
                    txtMobileNumber.Text = _objUserDetails.First().DG_User_PhoneNo;
                    if (_objUserDetails.First().DG_User_Roles_Id == LoginUser.RoleId)
                    {
                        roles = (new RolePermissionsBusniess()).GetChildUserData(LoginUser.RoleId);
                        CommonUtility.BindComboWithSelect<RoleInfo>(cmbRoleName, roles, "DG_User_Role", "DG_User_Roles_pkey", 0, ClientConstant.SelectString);
                        cmbRoleName.SelectedValue = LoginUser.RoleId;
                    }
                    else
                    {
                        roles = (new RolePermissionsBusniess()).GetChildUserData(LoginUser.RoleId).Where(o => o.DG_User_Roles_pkey != LoginUser.RoleId).ToList();
                        CommonUtility.BindComboWithSelect<RoleInfo>(cmbRoleName, roles, "DG_User_Role", "DG_User_Roles_pkey", 0, ClientConstant.SelectString);
                    cmbRoleName.SelectedValue = _objUserDetails.First().DG_User_Roles_Id.ToString();
                    }
                    cmbStatus.SelectedValue = _objUserDetails.First().DG_User_Status == true ? "1" : "2";
                    cmbStoreName.SelectedValue = _objUserDetails.First().DG_Store_ID.ToString();
                    _email = _objUserDetails.First().DG_User_Email;
                    _password = CryptorEngine.Decrypt(_objUserDetails.First().DG_User_Password, true);
                    _roleName = cmbRoleName.Text;
                    _firstname = _objUserDetails.First().DG_User_First_Name;
                    _username = _objUserDetails.First().DG_User_Name;
                    _lastname = _objUserDetails.First().DG_User_Last_Name;
                    _storename = cmbStoreName.Text;
                    _location = cmbLocation.Text;
                    _status = cmbStatus.Text;
                    txtUserName.IsEnabled = false;
                    txtPassword.IsEnabled = true;
                    cmbLocation.ItemsSource = null;
                    LocationList = new Dictionary<string, int>();
                    //List<DG_Location> lstLocatServerDateTimeions = new List<DG_Location>();
                    int storeId = cmbStoreName.SelectedValue.ToInt32();
                    //lstLocations = _objDataLayer.GetLocationName(storeId);
                    //if (lstLocations != null)
                    //{
                    //    LocationList = new Dictionary<string, int>();
                    //    LocationList.Add("--Select--", 0);
                    //    foreach (var item in lstLocations)
                    //    {
                    //        LocationList.Add(item.DG_Location_Name, item.DG_Location_pkey);
                    //    }
                    //}
                    //else
                    //{
                    //    LocationList = new Dictionary<string, int>();
                    //    LocationList.Add("--Select--", 0);
                    //}
                    //cmbLocation.ItemsSource = LocationList;
                    //cmbLocation.SelectedValue = "0";
                    LocationBusniess locBiz = new LocationBusniess();
                    cmbLocation.ItemsSource = locBiz.GetLocationNameDir(storeId);
                    // cmbLocation.SelectedValue = "0";

                    cmbLocation.SelectedValue = _objUserDetails.First().DG_Location_pkey.ToString();
                    _location = cmbLocation.Text;
                    Add_EditUser.Visibility = Visibility.Visible;
                    SPSearchCancel.Visibility = Visibility.Collapsed;
                    SPSubmitCancel.Visibility = Visibility.Visible;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }
        /// <summary>
        /// Handles the Click event of the btnDelete control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void btnDelete_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Button btnSender = (Button)sender;
                MessageBox.Show(btnSender.Tag.ToString());

                UserBusiness useBiz = new UserBusiness();
                if (useBiz.DeleteUsers(btnSender.Tag.ToInt32()))
                {
                    MessageBox.Show(UIConstant.UserDeleted);
                    CustomBusineses cusBiz = new CustomBusineses();
                    AuditLog.AddUserLog(Common.LoginUser.UserId, (int)FrameworkHelper.ActionType.DeleteUser, "Delete user at:- ");
                }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }
        /// <summary>
        /// Handles the Click event of the btnSearchUser control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void btnSearchUser_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                List<RoleInfo> roles = new List<RoleInfo>();
                roles = (new RolePermissionsBusniess()).GetChildUserData(LoginUser.RoleId);
                CommonUtility.BindComboWithSelect<RoleInfo>(cmbRoleName, roles, "DG_User_Role", "DG_User_Roles_pkey", 0, ClientConstant.SelectString);
                cmbRoleName.SelectedValue = "0";
                SPSearchCancel.Visibility = Visibility.Visible;
                SPSubmitCancel.Visibility = Visibility.Collapsed;
                grdUsers.Visibility = Visibility.Visible;
                Add_EditUser.Visibility = Visibility.Visible;
                txtUserName.IsEnabled = true;
                txtPassword.IsEnabled = false;
                ClearControls();
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }
        /// <summary>
        /// Handles the Click event of the btnAddUsers control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void btnAddUsers_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                List<RoleInfo> roles = new List<RoleInfo>();
                roles = (new RolePermissionsBusniess()).GetChildUserData(LoginUser.RoleId).Where(o => o.DG_User_Roles_pkey != LoginUser.RoleId).ToList();
                CommonUtility.BindComboWithSelect<RoleInfo>(cmbRoleName, roles, "DG_User_Role", "DG_User_Roles_pkey", 0, ClientConstant.SelectString);
                cmbRoleName.SelectedValue = "0";
                userId = 0;
                txtUserName.IsEnabled = true;
                txtPassword.IsEnabled = true;
                SPSearchCancel.Visibility = Visibility.Collapsed;
                SPSubmitCancel.Visibility = Visibility.Visible;
                Add_EditUser.Visibility = Visibility.Visible;
                ClearControls();
                btnSubmit.IsDefault = true;
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }
        /// <summary>
        /// Handles the Click event of the btnSubmit control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void btnSubmit_Click(object sender, RoutedEventArgs e)
        {
            try
            {

                UserBusiness useBiz = new UserBusiness();
                string SyncCode = CommonUtility.GetUniqueSynccode(Convert.ToInt32(ApplicationObjectEnum.User).ToString().PadLeft(2, '0'), LoginUser.countrycode, LoginUser.Storecode, "00");
                //string SyncCode = CommonUtility.GetRandomString(8) + Convert.ToInt32(ApplicationObjectEnum.User).ToString().PadLeft(2, '0');
                bool retValue = ControlValidation();
                if (retValue)
                {
                    if (ValidateUserInfo())
                    {
                        UserSaveAndUpdate(useBiz, SyncCode);
                    }
                }
                else
                {
                    MessageBox.Show(UIConstant.PleaseCheckAllEnteredInformation);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }

        private bool ValidateUserInfo()
        {

            bool retValue = true;
            Regex regmobille = new Regex("^[0-9]+$");

            if ((!Regex.IsMatch(txtEmail.Text, @"^[a-zA-Z][\w\.-]*[a-zA-Z0-9]@[a-zA-Z0-9][\w\.-]*[a-zA-Z0-9]\.[a-zA-Z][a-zA-Z\.]*[a-zA-Z]$")))
            {
                MessageBox.Show(UIConstant.ValidEmail);
                txtEmail.Focus();
                retValue = false;
            }
            else if (!regmobille.IsMatch(txtMobileNumber.Text))
            {
                MessageBox.Show(UIConstant.MobileNumberNumeric);
                txtMobileNumber.Focus();
                retValue = false;
            }
            else if (txtPassword.Password == "")
            {
                MessageBox.Show(UIConstant.EmptyPassword);
                txtPassword.Focus();
                retValue = false;
            }
            else if (txtPassword.Password.Length < 6)
            {
                MessageBox.Show(UIConstant.MinimunPassword);
                txtPassword.Focus();
                retValue = false;
            }
            else if (txtPassword.Password.Length > 12)
            {
                MessageBox.Show(UIConstant.MaximumPassword);
                txtPassword.Focus();
                retValue = false;
            }


            else if (!Regex.IsMatch(txtPassword.Password, @"
    # Match string having one letter and one digit (min).
    \A                        # Anchor to start of string.
      (?=[^0-9]*[0-9])        # at least one number and
      (?=[^A-Za-z]*[A-Za-z])  # at least one letter.
      \w+                     # Match string of alphanums.
    \Z                        # Anchor to end of string.
    ",
RegexOptions.IgnorePatternWhitespace))
            {
                MessageBox.Show(UIConstant.AlphanumericPassword);
                txtPassword.Focus();
                retValue = false;
            }
            return retValue;
        }

        private void UserSaveAndUpdate(UserBusiness useBiz, string SyncCode)
        {
            if (userId == 0)
            {

                var item = useBiz.GetUserDetailByUserId(0, txtUserName.Text, LoginUser.RoleId);
                if (item.Count != 0)
                {
                    MessageBox.Show(UIConstant.UsernameAlreadyExists);
                }
                else
                {
                    bool? userstatus;
                    if (cmbStatus.SelectedValue.ToString() == "1")
                        userstatus = true;
                    else
                        userstatus = false;
                    //bool issaved = useBiz.AddUpdateUserDetails(userId, txtUserName.Text, txtFName.Text, txtLName.Text, CryptorEngine.Encrypt(txtPassword.Password, true), cmbRoleName.SelectedValue.ToInt32(), cmbLocation.SelectedValue.ToInt32(), userstatus, txtMobileNumber.Text, txtEmail.Text, SyncCode);
                    bool issaved = useBiz.AddUpdateUserDetails(userId, txtUserName.Text, txtFName.Text, txtLName.Text, CryptorEngine.Encrypt(txtPassword.Password, true), cmbRoleName.SelectedValue.ToInt32(), cmbLocation.SelectedValue.ToInt32(), userstatus, txtMobileNumber.Text, txtEmail.Text, SyncCode, txtEmpId.Text.Trim());
                    if (issaved)
                    {
                        string msg = checkTempar();
                        ClearControls();
                        BindGrid();
                        Add_EditUser.Visibility = Visibility.Collapsed;
                        MessageBox.Show(UIConstant.RecordSuccessfullyUpdated);
                        CustomBusineses cusBiz = new CustomBusineses();
                        AuditLog.AddUserLog(Common.LoginUser.UserId, (int)FrameworkHelper.ActionType.EditUser, "Add user data at:- ");
                        btnSubmit.IsDefault = false;
                        LoadSearchPhotographer();
                    }
                    else
                    {
                        MessageBox.Show(UIConstant.Error);
                    }
                }
            }

            else
            {
                var item = useBiz.GetUserDetailByUserId(userId, null, LoginUser.RoleId).FirstOrDefault();

                if (item.DG_User_Name != txtUserName.Text.Trim())
                {
                    var item2 = useBiz.GetUserDetailByUserId(0, txtUserName.Text, LoginUser.RoleId);
                    if (item2 != null)
                    {
                        MessageBox.Show(UIConstant.UsernameAlreadyExists);
                    }
                    else
                    {
                        bool? userstatus;
                        if (cmbStatus.SelectedValue.ToString() == "1")
                            userstatus = true;
                        else
                            userstatus = false;
                        //bool issaved = useBiz.AddUpdateUserDetails(userId, txtUserName.Text, txtFName.Text, txtLName.Text, CryptorEngine.Encrypt(txtPassword.Password, true), cmbRoleName.SelectedValue.ToInt32(), cmbLocation.SelectedValue.ToInt32(), userstatus, txtMobileNumber.Text, txtEmail.Text, SyncCode);
                        bool issaved = useBiz.AddUpdateUserDetails(userId, txtUserName.Text, txtFName.Text, txtLName.Text, CryptorEngine.Encrypt(txtPassword.Password, true), cmbRoleName.SelectedValue.ToInt32(), cmbLocation.SelectedValue.ToInt32(), userstatus, txtMobileNumber.Text, txtEmail.Text, SyncCode, txtEmpId.Text.Trim());
                        if (issaved)
                        {
                            string msg = checkTempar();
                            ClearControls();
                            BindGrid();
                            Add_EditUser.Visibility = Visibility.Collapsed;

                            MessageBox.Show(UIConstant.RecordSuccessfullyUpdated);
                            AuditLog.AddUserLog(Common.LoginUser.UserId, (int)FrameworkHelper.ActionType.EditUser, msg);
                            btnSubmit.IsDefault = false;
                            LoadSearchPhotographer();
                        }
                        else
                        {
                            MessageBox.Show(UIConstant.Error);
                        }
                    }
                }
                else
                {

                    bool? userstatus;
                    if (cmbStatus.SelectedValue.ToString() == "1")
                        userstatus = true;
                    else
                        userstatus = false;
                    //bool issaved = useBiz.AddUpdateUserDetails(userId, txtUserName.Text, txtFName.Text, txtLName.Text, CryptorEngine.Encrypt(txtPassword.Password, true), cmbRoleName.SelectedValue.ToInt32(), cmbLocation.SelectedValue.ToInt32(), userstatus, txtMobileNumber.Text, txtEmail.Text, SyncCode);
                    bool issaved = useBiz.AddUpdateUserDetails(userId, txtUserName.Text, txtFName.Text, txtLName.Text, CryptorEngine.Encrypt(txtPassword.Password, true), cmbRoleName.SelectedValue.ToInt32(), cmbLocation.SelectedValue.ToInt32(), userstatus, txtMobileNumber.Text, txtEmail.Text, SyncCode, txtEmpId.Text.Trim());
                    if (issaved)
                    {
                        string msg = checkTempar();
                        ClearControls();
                        BindGrid();
                        Add_EditUser.Visibility = Visibility.Collapsed;
                        MessageBox.Show(UIConstant.RecordSuccessfullyUpdated);

                        AuditLog.AddUserLog(Common.LoginUser.UserId, (int)FrameworkHelper.ActionType.EditUser, msg);
                        btnSubmit.IsDefault = false;
                        LoadSearchPhotographer();
                    }
                    else
                    {
                        MessageBox.Show("Error");
                    }

                }
            }
        }
        /// <summary>
        /// Handles the Click event of the btnSubmitCancel control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void btnSubmitCancel_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                ClearControls();
                Add_EditUser.Visibility = Visibility.Collapsed;
                grdUsers.Visibility = Visibility.Visible;

            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }
        /// <summary>
        /// Handles the Click event of the btnSearch control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void btnSearch_Click(object sender, RoutedEventArgs e)
        {
            UserBusiness useBiz = new UserBusiness();
            try
            {
                if (string.IsNullOrEmpty(txtFName.Text) && string.IsNullOrEmpty(txtLName.Text) && string.IsNullOrEmpty(txtEmail.Text)
                    && string.IsNullOrEmpty(txtMobileNumber.Text) && cmbRoleName.SelectedValue.ToString() == "0" && cmbStatus.SelectedValue.ToString() == "0" && cmbStoreName.SelectedValue.ToString() == "0")
                {
                    grdUsers.ItemsSource = useBiz.GetUserDetailByUserId(0, null, LoginUser.RoleId);
                }
                else
                {

                    if (LoginUser.RoleId == 7)
                    {
                        grdUsers.ItemsSource = useBiz.SearchUserDetails(txtFName.Text, txtLName.Text, cmbStoreName.SelectedValue.ToInt32(), Convert.ToString(cmbStatus.SelectedValue), cmbRoleName.SelectedValue.ToInt32(), txtMobileNumber.Text, txtEmail.Text, cmbLocation.SelectedValue.ToInt32(), txtUserName.Text,txtEmpId.Text.Trim());
                    }
                    else
                    {
                        grdUsers.ItemsSource = useBiz.SearchUserDetails(txtFName.Text, txtLName.Text, cmbStoreName.SelectedValue.ToInt32(), Convert.ToString(cmbStatus.SelectedValue), cmbRoleName.SelectedValue.ToInt32(), txtMobileNumber.Text, txtEmail.Text, cmbLocation.SelectedValue.ToInt32(), txtUserName.Text, txtEmpId.Text.Trim()).Where(t => t.DG_User_Roles_Id != 7);
                    }
                }

                if (grdUsers.Items.Count <= 0)
                {
                    grdUsers.ItemsSource = null;
                    MessageBox.Show(UIConstant.NoResultFound);
                    grdUsers.ItemsSource = useBiz.GetUserDetailByUserId(0, null, LoginUser.RoleId);
                }
                userId = 0;
                SPSearchCancel.Visibility = Visibility.Visible;
                SPSubmitCancel.Visibility = Visibility.Collapsed;
                Add_EditUser.Visibility = Visibility.Collapsed;
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }

        }
        /// <summary>
        /// Handles the Click event of the btnSearchCancel control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void btnSearchCancel_Click(object sender, RoutedEventArgs e)
        {
            Add_EditUser.Visibility = Visibility.Collapsed;
            grdUsers.Visibility = Visibility.Visible;
        }
        /// <summary>
        /// Handles the GotFocus event of the txtFName control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void txtFName_GotFocus(object sender, RoutedEventArgs e)
        {
            try
            {
                TextBox objtxt = (TextBox)sender;

                controlon = objtxt;

                if (KeyBorder.Visibility == Visibility.Collapsed)
                {
                    KeyBorder.Visibility = Visibility.Visible;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }
        /// <summary>
        /// Handles the GotFocus event of the cmbStoreName control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void cmbStoreName_GotFocus(object sender, RoutedEventArgs e)
        {

            KeyBorder.Visibility = Visibility.Collapsed;
        }
        /// <summary>
        /// Handles the Click event of the btnSearchPhoto_Copy control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void btnSearchPhoto_Copy_Click(object sender, RoutedEventArgs e)
        {
            ManageHome _objmain = new ManageHome();
            _objmain.Show();
            this.Close();
        }
        /// <summary>
        /// Handles the Click event of the btnLogout control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void btnLogout_Click(object sender, RoutedEventArgs e)
        {
            CustomBusineses cusBiz = new CustomBusineses();
            AuditLog.AddUserLog(Common.LoginUser.UserId, (int)FrameworkHelper.ActionType.Logout, "Logged out at ");
            Login _objLogin = new Login();
            _objLogin.Show();
            this.Close();
        }

        private void LoadSearchPhotographer()
        {
            SearchByPhoto window = System.Windows.Application.Current.Windows.Cast<Window>().Where(wnd => wnd.Title == "Search").Cast<SearchByPhoto>().FirstOrDefault();
            if (window != null)
            {
                window.LoadPhotoGrapher();
            }
        }
        #endregion

    }
}
