﻿using AVT.VmbAPINET;
using DigiAuditLogger;
using DigiPhoto.Common;
using DigiPhoto.DataLayer;
using DigiPhoto.IMIX.Business;
using DigiPhoto.IMIX.Model;
using DigiPhoto.Utility.Repository.Data;
using DigiPhoto.Utility.Repository.ValueType;
using FrameworkHelper;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Media;

namespace DigiPhoto.Manage
{
    /// <summary>
    /// Interaction logic for ManageCamera.xaml
    /// </summary>
    public partial class ManageCamera : Window
    {
        #region Declaration
        DeviceManager objDeviceMgr = null;
        Dictionary<string, string> lstPhotographerList;
        public int CameraId = 0;
        public int _CameraId = 0;
        private Dictionary<string, Int32> _locationList;
        List<DeviceInfo> deviceInforList = null;
        Dictionary<string, string> lstSubStore;
        public Dictionary<string, Int32> LocationList
        {
            get { return _locationList; }
            set { _locationList = value; }
        }
        Vimba system = new Vimba();
        CameraCollection cameras;

        #endregion

        #region constructor
        /// <summary>
        /// Initializes a new instance of the <see cref="ManageCamera"/> class.
        /// </summary>
        public ManageCamera()
        {
            try
            {
                InitializeComponent();
                chkIsTripCam.IsEnabled = false;
                btnSave.Tag = "0";
                GetCameraData();
                FillPhotographerCombo();
                FillSubstore();
                FillSubstoreSearch();
                cmbSubStore.SelectedValue = LoginUser.SubStoreId.ToString();
                cmbSubstoreSearch.SelectedValue = LoginUser.SubStoreId.ToString();
                FillLocationBysubstore();
                FillLocationBysubstoreSearch();
                FillCharacter();
                txbUserName.Text = LoginUser.UserName;
                txbStoreName.Text = LoginUser.StoreName;
                btnSave.IsDefault = true;
                lstDevice.Visibility = Visibility.Visible;
                tbDevice.Visibility = Visibility.Visible;
                GetDevice();
                MsgBox.SetParent(myBorder);
                GetCameras();
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
            }
        }
        #endregion

        #region Events
        /// <summary>
        /// Handles the Click event of the btnLogout control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void btnLogout_Click(object sender, RoutedEventArgs e)
        {
            AuditLog.AddUserLog(Common.LoginUser.UserId, (int)FrameworkHelper.ActionType.Logout, "Logged out at");
            Login _objLogin = new Login();
            _objLogin.Show();
            this.Close();
        }
        /// <summary>
        /// Handles the Click event of the btnEdit control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void btnEdit_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                int cameraId = ((Button)sender).Tag.ToInt32();
                CameraBusiness camBiz = new CameraBusiness();
                CameraInfo _objEditData = camBiz.GetCameraByID(((Button)sender).Tag.ToInt32());
                btnSave.Tag = (int)_objEditData.DG_Camera_ID;
                CameraId = _objEditData.DG_Camera_pkey;
                _CameraId = (int)_objEditData.DG_Camera_ID;


                txtCameraName.Text = _objEditData.DG_Camera_Name;
                txtCameraMake.Text = _objEditData.DG_Camera_Make;
                txtCameraModel.Text = _objEditData.DG_Camera_Model;
                txtPhotoSeries.Text = _objEditData.DG_Camera_Start_Series.ToString();
                cmbPhotoGraphers.SelectedValue = _objEditData.DG_AssignTo.ToString();
                chkIsChromaColor.IsChecked = _objEditData.IsChromaColor;
                chkIsTripCam.IsChecked = _objEditData.IsTripCam;
                txtCameraSerialNumber.Text = _objEditData.SerialNo;
                if ((bool)_objEditData.IsTripCam)
                {
                    //GetCameras();
                    if (cmbTripCamModels.Items.Contains(_objEditData.DG_Camera_Model))
                        cmbTripCamModels.Text = _objEditData.DG_Camera_Model.Trim();

                    else
                        cmbTripCamModels.SelectedIndex = 0;
                    cmbTripCamModels.IsEnabled = true;
                }
                chkIsLiveStream.IsChecked = _objEditData.IsLiveStream;
                cmbCharacter.SelectedValue = _objEditData.CharacterId == null ? "0" : _objEditData.CharacterId.ToString();
                UserBusiness userBiz = new UserBusiness();
                List<UsersInfo> _objEditLocation = userBiz.GetUserDetailsByUserIddetail(cmbPhotoGraphers.SelectedValue.ToInt32());

                cmbSubStore.SelectedValue = _objEditLocation.FirstOrDefault().DG_Substore_ID;
                cmbStore_SelectionChanged(null, null);
                cmbLocation.SelectedValue = _objEditLocation.FirstOrDefault().DG_Location_pkey;

                txtPhotoSeries.Focus();
                //used to set the associated chechboxes
                BindCameraDeviceAssociation(cameraId);
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
            }
        }

        private void BindCameraDeviceAssociation(int cameraId)
        {
            objDeviceMgr = new DeviceManager();
            List<CameraDeviceAssociationInfo> devices = objDeviceMgr.GetCameraDeviceList(_CameraId);
            for (int i = 0; i < lstDevice.Items.Count; i++)
            {
               //
                DependencyObject obj = lstDevice.ItemContainerGenerator.ContainerFromIndex(i);
                if (obj != null)
                {
                    //  CheckBox rdo = FindVisualChild<CheckBox>(obj);// 
                    RadioButton rdo = FindVisualChild<RadioButton>(obj);
                    int ctr = 0;
                    while (rdo == null)
                    {
                        lstDevice.ScrollIntoView(lstDevice.Items[ctr]);
                        rdo = FindVisualChild<RadioButton>(obj);
                        ctr++;
                        if (ctr == lstDevice.Items.Count || rdo != null)
                            break;
                    }
                    if (rdo != null && devices.Select(o => o.DeviceId).Contains(Convert.ToInt16(rdo.Tag)))
                    {
                        rdo.IsChecked = true;
                        rdo.Foreground = Brushes.DarkRed;
                        rdo.BorderBrush = Brushes.Red;

                        lstDevice.ScrollIntoView(lstDevice.Items[i]);
                    }
                    else
                    {
                       // lstDevice.ScrollIntoView(lstDevice.Items[0]);
                        rdo.IsChecked = false;
                        rdo.Foreground = Brushes.Black;
                        rdo.BorderBrush = Brushes.Black;
                    }
                }
            }

            //lstDevice.Items.Clear();
            //if (devices != null && deviceInforList != null)
            //{
            //    deviceInforList.ForEach(o => o.IsChecked = devices.Select(s => s.DeviceId).Contains(o.DeviceId));
            //}
            //lstDevice.ItemsSource = deviceInforList;
        }

        /// <summary>
        /// Handles the Click event of the btnDelete control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void btnDelete_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (MessageBox.Show("Do you want to delete this camera?", "Confirm delete", MessageBoxButton.YesNo) == MessageBoxResult.Yes)
                {

                    //bool IsSaved = _objDataLayer.DeleteCameraDetails(Convert.ToInt32(((Button)sender).Tag.ToString()));
                    CameraBusiness camBiz = new CameraBusiness();
                    int CamerID =Convert.ToInt32(((Button)sender).Tag);
                    string hotFolderPath = camBiz.DeleteCameraDetails(CamerID);
                    if (!String.IsNullOrWhiteSpace(hotFolderPath))
                    {
                        DirectoryInfo dinfo = new DirectoryInfo(hotFolderPath);
                        if (dinfo.Exists)
                            dinfo.Delete(true);
                    }
                    MessageBox.Show("Camera deleted successfully");
                    GetCameraData();
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
            }
        }
        /// <summary>
        /// Handles the Click event of the btnSave control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                objDeviceMgr = new DeviceManager();
                string deviceIds = GetSelectedDevices();
                if (Isvalidated())
                {
                    ConfigBusiness configBusiness = new ConfigBusiness();
                    FolderStructureInfo FolderInfo = new FolderStructureInfo();
                    FolderInfo = configBusiness.GetFolderStructureInfo(Convert.ToInt32(cmbSubStore.SelectedValue));
                    
                    try
                    {
                        DirectoryInfo _objnew = new DirectoryInfo(System.IO.Path.Combine(FolderInfo.HotFolderPath,"Camera"));
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("Please check the permission on folder.There is some problem while creating folder.");
                        return;
                    }
                    //int Camera_ID = btnSave.Tag.ToInt32();
                     if (deviceIds != "")
                    {
                        if (!CheckSingleDevice(deviceIds))
                        {
                            MsgBox.ShowHandlerDialog("Please select a single device", DigiMessageBox.DialogType.OK);
                            return;
                        }

                    }
                    if (objDeviceMgr.IsDeviceAssociatedToOthers(_CameraId, deviceIds))
                    {
                        string msg = "The device(s) you have selected are already associated with other camera. Do you want to re-associate with this camera?";
                        bool res = MsgBox.ShowHandlerDialog(msg, DigiMessageBox.DialogType.Confirm);
                        if (res == false)
                            return;
                    }

                    string SyncCode = CommonUtility.GetUniqueSynccode(Convert.ToInt32(ApplicationObjectEnum.Camera).ToString().PadLeft(2, '0'), LoginUser.countrycode, LoginUser.Storecode, "00");
                    CameraBusiness camBiz = new CameraBusiness();
                    CameraInfo _objCameraInfo = new CameraInfo();
                    _objCameraInfo.DG_Camera_Name = txtCameraName.Text;
                    _objCameraInfo.DG_Camera_Make = txtCameraMake.Text;
                    _objCameraInfo.DG_Camera_Model = txtCameraModel.Text;
                    _objCameraInfo.DG_Camera_Start_Series = txtPhotoSeries.Text;
                    _objCameraInfo.DG_Camera_ID = CameraId;
                    _objCameraInfo.DG_AssignTo = cmbPhotoGraphers.SelectedValue.ToInt32();
                    _objCameraInfo.DG_Updatedby = LoginUser.UserId;
                    _objCameraInfo.DG_Location_ID = cmbLocation.SelectedValue.ToInt32();
                    _objCameraInfo.SyncCode = SyncCode;
                    _objCameraInfo.IsSynced = false;
                    _objCameraInfo.IsChromaColor = chkIsChromaColor.IsChecked;
                    _objCameraInfo.IsLiveStream = chkIsLiveStream.IsChecked;
                    _objCameraInfo.IsTripCam = chkIsTripCam.IsChecked;
                    _objCameraInfo.SubStoreId = cmbSubStore.SelectedValue.ToInt32();
                    _objCameraInfo.SerialNo = txtCameraSerialNumber.Text;
                    CustomBusineses cusBiz = new CustomBusineses();
                    _objCameraInfo.DG_UpdatedDate = DateTime.Now;

                    int cmraId = 0;
                    int Camerapkey = 0;
                    camBiz.SetCameraDetails(_objCameraInfo);
                    //Camera folder Id is cmraId
                    cmraId = _objCameraInfo.CameraId;
                    if (_CameraId == 0)
                    {
                    Camerapkey = _objCameraInfo.DG_Camera_pkey;
                    }
                    else
                    {
                        Camerapkey = _CameraId;
                    }
                
                    if (_objCameraInfo.IsTripCam == true && CameraId > 0)
                    {
                        int oldCamId = CameraId;
                        int newCamId = Camerapkey;
                        TripCamBusiness tripCamBusiness = new TripCamBusiness();
                        tripCamBusiness.UpdCamIdForTripCamPOSMapping(oldCamId, newCamId);
                    }
                    bool isdeviceAssociated = objDeviceMgr.SaveCameraDeviceAssociation(Camerapkey, deviceIds);
                    int _result = 0;
                    if (deviceIds != "")
                    {
                        _result = objDeviceMgr.DeleteCameraDeviceSyncDetails(Camerapkey, deviceIds);
                    }
                    if (cmbCharacter.SelectedIndex>0)
                        objDeviceMgr.SaveCameraCharacterAssociation(CameraId, _objCameraInfo.DG_Camera_pkey, cmbCharacter.SelectedValue.ToInt32());

                    if (cmraId > 0)
                    {
                        if (_result > 0)
                        {
                            MessageBox.Show("Camera details saved successfully.\n Please Sync your camera and mobile device.");
                        }
                        else
                        {
                            MessageBox.Show("Camera details saved successfully");
                        }
                        GetCameraData();
                        ClearControls();
                        DirectoryInfo _objnew = new DirectoryInfo(System.IO.Path.Combine(FolderInfo.HotFolderPath, "Camera", ("C" + cmraId)));
                        if (!_objnew.Exists)
                            _objnew.Create();
                        if (chkIsTripCam.IsChecked == true)
                        {
                            DirectoryInfo _objnew2 = new DirectoryInfo(System.IO.Path.Combine(FolderInfo.HotFolderPath, "Camera", ("RideC" + cmraId)));
                            if (!_objnew2.Exists)
                                _objnew2.Create();

                            DirectoryInfo _objnew5 = new DirectoryInfo(System.IO.Path.Combine(FolderInfo.HotFolderPath, "Camera", ("RideC" + cmraId), "marketing"));
                            if (!_objnew5.Exists)
                                _objnew5.Create();

                            DirectoryInfo _objnew3 = new DirectoryInfo(System.IO.Path.Combine(FolderInfo.HotFolderPath, "Camera", "RideCamera"));
                            if (!_objnew3.Exists)
                                _objnew3.Create();

                            DirectoryInfo _objnew4 = new DirectoryInfo(System.IO.Path.Combine(FolderInfo.HotFolderPath, "Camera", "RideCamera", "marketing"));
                            if (!_objnew4.Exists)
                                _objnew4.Create();

                        }
                      
                    }
                    else if (cmraId == 0)
                    {
                        MessageBox.Show("This PhotoGrapher is  already assigned to other location, kindly select the other PhotoGrapher and try again .");
                        
                    }
                }
                else
                {
                    MessageBox.Show("Please check all the mandatory fields .");
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
            }
        }

        private bool CheckSingleDevice(string DeviceID)
        {
            string[] listSplitDeviceId = DeviceID.Split(',');
            if (listSplitDeviceId.Length > 1)
            {
                return false;
            }
            return true;
        }
        string GetSelectedDevices()
        {
            try
            {
                string DeviceIds = string.Empty;
                for (int i = 0; i < lstDevice.Items.Count; i++)
                {
                    DependencyObject obj = lstDevice.ItemContainerGenerator.ContainerFromIndex(i);
                    if (obj != null)
                    {
                        // CheckBox rdo = FindVisualChild<CheckBox>(obj);// FindByName("rdoSelect", row) as RadioButton;
                        RadioButton rdo = FindVisualChild<RadioButton>(obj);
                        if (rdo != null && rdo.IsChecked == true)
                        {
                            DeviceIds += "," + rdo.Tag.ToString();
                        }
                    }
                }
                if (DeviceIds.Length > 0)
                    DeviceIds = DeviceIds.Remove(0, 1);
                return DeviceIds;
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
                return string.Empty;
            }
        }

        public static childItem FindVisualChild<childItem>(DependencyObject obj)
  where childItem : DependencyObject
        {
            // Search immediate children
            for (int i = 0; i < VisualTreeHelper.GetChildrenCount(obj); i++)
            {
                DependencyObject child = VisualTreeHelper.GetChild(obj, i);
                if (child is childItem)
                    return (childItem)child;
                else
                {
                    childItem childOfChild = FindVisualChild<childItem>(child);
                    if (childOfChild != null)
                        return childOfChild;
                }
            }
            return null;
        }

        /// <summary>
        /// Handles the Click event of the btnClear control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void btnClear_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                ClearControls();
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
            }
        }
        /// <summary>
        /// Handles the Click event of the btnBack control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void btnBack_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                ManageHome _obj = new ManageHome();
                _obj.Show();
                system.Shutdown();
                this.Close();
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
            }

        }

        #endregion

        #region Common Methods
        void GetDevice()
        {
            lstDevice.Items.Clear();
            deviceInforList = new List<DeviceInfo>();
            DeviceManager objDevice = new DeviceManager();
            deviceInforList = objDevice.GetDeviceList().Where(o => o.IsActive).ToList();
            deviceInforList.ForEach(o => o.Name += "(" + o.SerialNo + ")");
            lstDevice.ItemsSource = deviceInforList;
            for (int j = 0; j < lstDevice.Items.Count; j++)
            {
                lstDevice.ScrollIntoView(lstDevice.Items[j]);
            }
        }
        /// <summary>
        /// Gets the camera data.
        /// </summary>
        void GetCameraData()
        {
            CameraBusiness camBiz = new CameraBusiness();
            DGManageCamrea.ItemsSource = camBiz.GetCameraList();
            string totalRole = LoginUser.RoleId.ToString();
            DGManageCamrea.Columns[8].Visibility = Visibility.Hidden;
            if (camBiz.CheckIsCamDeleteAccess(Convert.ToInt32(totalRole))>0)
            {
                DGManageCamrea.Columns[8].Visibility = Visibility.Visible;
            }
            //DGManageCamrea.ItemsSource = _objDataLayer.GetCameraList();
        }
        /// <summary>
        /// Fills the photographer combo.
        /// </summary>
        private void FillPhotographerCombo()
        {
            lstPhotographerList = new Dictionary<string, string>();
            lstPhotographerList.Add("0", "--Select--");
            try
            {
                //DigiPhotoDataServices _objdbLayer = new DigiPhotoDataServices();
                //foreach (var item in _objdbLayer.GetPhotoGrapher())
                //{
                //    lstPhotographerList.Add(item.DG_User_pkey.ToString(), item.Photograper + " (" + item.DG_User_Name + ")");
                //}
                UserBusiness userBiz = new UserBusiness();
                foreach (var item in userBiz.GetPhotoGrapher())
                {
                    lstPhotographerList.Add(item.UserId.ToString(), item.Photographer + " (" + item.UserName + ")");
                }
                cmbPhotoGraphers.ItemsSource = lstPhotographerList;
                cmbPhotoGraphers.SelectedValue = "0";
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
            }
        }
        ///
        /// Fills the location combo
        ///
        private void FillPhotographerLocation()
        {
            cmbLocation.ItemsSource = null;
            LocationList = new Dictionary<string, int>();
            //List<DG_Location> lstLocations = new List<DG_Location>();
            List<LocationInfo> lstLocations = new List<LocationInfo>();
            LocationBusniess locBiz = new LocationBusniess();
            int storeId = LoginUser.StoreId;
            //lstLocations = _objDataLayer.GetLocationName(storeId);
            lstLocations = locBiz.GetLocationName(storeId);
            if (lstLocations != null)
            {
                LocationList = new Dictionary<string, int>();
                LocationList.Add("--Select--", 0);
                foreach (var item in lstLocations)
                {
                    LocationList.Add(item.DG_Location_Name, item.DG_Location_pkey);
                }
            }
            else
            {
                LocationList = new Dictionary<string, int>();
                LocationList.Add("--Select--", 0);
            }
            cmbLocation.ItemsSource = LocationList;
            cmbLocation.SelectedValue = "0";

        }
        private void FillLocationBysubstore()
        {
            int subStoreId = Convert.ToInt32(cmbSubStore.SelectedValue);
            List<LocationInfo> locations = new List<LocationInfo>();
            locations = (new StoreSubStoreDataBusniess()).GetSelectedLocationsSubstore(subStoreId).ToList();
            CommonUtility.BindComboWithSelect<LocationInfo>(cmbLocation, locations, "DG_Location_Name", "DG_Location_pkey", 0, ClientConstant.SelectString);
            cmbLocation.SelectedValue = "0";
        }
        private void FillLocationBysubstoreSearch()
        {
            int subStoreId = Convert.ToInt32(cmbSubstoreSearch.SelectedValue);
            List<LocationInfo> locations = new List<LocationInfo>();
            locations = (new StoreSubStoreDataBusniess()).GetSelectedLocationsSubstore(subStoreId).ToList();
            CommonUtility.BindComboWithSelect<LocationInfo>(cmbLocationSearch, locations, "DG_Location_Name", "DG_Location_pkey", 0, ClientConstant.SelectString);
            cmbLocationSearch.SelectedValue = "0";
        }
        private void FillCharacter()
        {
            List<CharacterInfo> characters = new List<CharacterInfo>();
            characters = (new CharacterBusiness()).GetCharacter().ToList();
            CommonUtility.BindComboWithSelect<CharacterInfo>(cmbCharacter, characters, "DG_Character_Name", "DG_Character_Pkey", 0, ClientConstant.SelectString);
            cmbCharacter.SelectedValue = "0";
        }
        /// <summary>
        /// Clears the controls.
        /// </summary>
        private void ClearControls()
        {
            txtCameraSerialNumber.Text = string.Empty;
            txtCameraMake.Text = string.Empty;
            txtCameraModel.Text = string.Empty;
            txtCameraName.Text = string.Empty;
            txtPhotoSeries.Text = string.Empty;
            cmbPhotoGraphers.SelectedValue = "0";
            cmbSubStore.SelectedValue = "0";
            cmbLocation.SelectedValue = "0";
            cmbCharacter.SelectedValue = "0";
            CameraId = 0;
            chkIsChromaColor.IsChecked = false;
            chkIsTripCam.IsChecked = false;
            cmbTripCamModels.SelectedIndex = 0;
            chkIsLiveStream.IsChecked = false;
            //uncheck checkboxes
            for (int i = 0; i < lstDevice.Items.Count; i++)
            {
                DependencyObject obj = lstDevice.ItemContainerGenerator.ContainerFromIndex(i);
                if (obj != null)
                {
                    RadioButton rdo = FindVisualChild<RadioButton>(obj);
                    if (rdo != null)
                        rdo.IsChecked = false;
                    rdo.Foreground = Brushes.Black;
                    rdo.BorderBrush = Brushes.Black;
                }
            }
            cmbSubstoreSearch.SelectedValue = "0";
        }
        /// <summary>
        /// Isvalidateds this instance.
        /// </summary>
        /// <returns></returns>
        private bool Isvalidated()
        {
            bool retvalue = true;
            if (txtCameraName.Text.Trim() == "")
            {
                retvalue = false;
            }
            if (txtCameraModel.Text.Trim() == "")
            {
                retvalue = false;
            }
            if (txtPhotoSeries.Text.Trim() == "")
            {
                retvalue = false;
            }
            if (txtCameraMake.Text.Trim() == "")
            {
                retvalue = false;
            }
            if (cmbPhotoGraphers.SelectedValue.ToString() == "0")
            {
                retvalue = false;
            }
            if (cmbLocation.SelectedValue.ToString() == "0")
            {
                retvalue = false;
            }
            try
            {
                txtPhotoSeries.Text.ToInt64();
            }
            catch (Exception)
            {
                retvalue = false;
            }
            return retvalue;
        }
        private void FillSubstore()
        {
            try
            {
                List<SubStoresInfo> lstStoreSubStore = (new StoreSubStoreDataBusniess()).GetAllSubstoreName();
                CommonUtility.BindComboWithSelect<SubStoresInfo>(cmbSubStore, lstStoreSubStore, "DG_SubStore_Name", "DG_SubStore_pkey", 0, ClientConstant.SelectString);
                cmbSubStore.SelectedValue = LoginUser.SubStoreId.ToString();
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
            }
        }
        private void FillSubstoreSearch()
        {
            try
            {
                List<SubStoresInfo> lstStoreSubStore = (new StoreSubStoreDataBusniess()).GetAllSubstoreName();
                CommonUtility.BindComboWithSelect<SubStoresInfo>(cmbSubstoreSearch, lstStoreSubStore, "DG_SubStore_Name", "DG_SubStore_pkey", 0, ClientConstant.SelectString);
                cmbSubstoreSearch.SelectedValue = LoginUser.SubStoreId.ToString();
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
            }
        }
        #endregion

        private void cmbStore_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            FillLocationBysubstore();
        }
        private void txtnumber_TextChanged(object sender, TextChangedEventArgs e)
        {
            string str = string.Empty;
            if (string.IsNullOrEmpty(((TextBox)sender).Text))

                str = string.Empty;
            else
            {
                double num = 0;
                bool success = double.TryParse(((TextBox)sender).Text, out num);
                if (success & num >= 0)
                {
                    ((TextBox)sender).Text.Trim();
                    str = ((TextBox)sender).Text;
                }
                else
                {
                    ((TextBox)sender).Text = str;
                    ((TextBox)sender).SelectionStart = ((TextBox)sender).Text.Length;
                }
            }
        }
        private void NumericOnly(System.Object sender, System.Windows.Input.TextCompositionEventArgs e)
        {
            e.Handled = IsTextNumeric(e.Text);

        }
        private static bool IsTextNumeric(string str)
        {
            System.Text.RegularExpressions.Regex reg = new System.Text.RegularExpressions.Regex("[^0-9]");
            return reg.IsMatch(str);

        }
        private void chkIsTripCam_Click(object sender, RoutedEventArgs e)
        {
            CheckBox chkBox = (CheckBox)sender;
            if (chkBox.IsChecked == true)
            {
                txtCameraModel.Text = "";
                cmbTripCamModels.IsEnabled = true;
                txtCameraModel.IsEnabled = false;
            }
            else
            {
                cmbTripCamModels.SelectedIndex = 0;
                txtCameraModel.Text = "";
                cmbTripCamModels.IsEnabled = false;
                txtCameraModel.IsEnabled = true;
            }
        }
        private void GetCameras()
        {
            try
            {
                cmbTripCamModels.Items.Add("--Select--");
                system.Startup();
                cameras = system.Cameras;
                if (cameras != null && cameras.Count > 0)
                {
                    foreach (Camera camera in cameras)
                    {
                        cmbTripCamModels.Items.Add(camera.Id);
                    }
                }
                if (cmbTripCamModels.Items.Count > 1)
                {
                    chkIsTripCam.IsEnabled = true;
                    cmbTripCamModels.IsEnabled = true;
                }
            }
            catch (Exception ex)
            {
                if (cmbTripCamModels.Items.Count <= 1)
                {
                    chkIsTripCam.IsEnabled = false;
                    cmbTripCamModels.IsEnabled = false;
                }
                    if (!(((AVT.VmbAPINET.VimbaException)(ex)).ReturnCode == -16))
                    {
                        string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                        ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                    }
            }
        }

        private void cmbTripCamModels_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (cmbTripCamModels.SelectedIndex > 0)
                txtCameraModel.Text = cmbTripCamModels.SelectedValue.ToString();
            else
                txtCameraModel.Text = "";
        }

        private void DetectCameraSerial(object sender, RoutedEventArgs e)
        {
            try
            {
                var app = (App)(Application.Current);
                if (app.IsCameraConnected)
                {
                    if (!string.IsNullOrEmpty(app.CameraSerialNumber))
                    {
                        txtCameraSerialNumber.Text = app.CameraSerialNumber;
                    }
                    else
                    {
                        MessageBox.Show("Please either connect a Camera to fetch it's serial number or manually enter the serial number.");
                        ErrorHandler.ErrorHandler.LogFileWrite("Unable to fetch Camera serial number");
                    }
                }
                else
                {
                    MessageBox.Show("Please either connect a Camera to fetch it's serial number or manually enter the serial number.");
                   
                    ErrorHandler.ErrorHandler.LogFileWrite("Camera is not connected");
                }
            }
            catch(Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }

        private void cmbSubstoreSearch_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            FillLocationBysubstoreSearch();
        }

        private void btnSearch_Click(object sender, RoutedEventArgs e)
        {
            GetCameraDataSearch();
        }
       public void GetCameraDataSearch()
        {
            CameraBusiness camBiz = new CameraBusiness();
            if (Convert.ToInt32(cmbSubstoreSearch.SelectedValue) > 0)
            {

                
                DGManageCamrea.ItemsSource = camBiz.GetCameraListSearch(Convert.ToInt32(cmbSubstoreSearch.SelectedValue), Convert.ToInt32(cmbLocationSearch.SelectedValue));
                if (DGManageCamrea.Items.Count < 1){ MessageBox.Show("No Records found."); }
            }
            else { MessageBox.Show("Kindly select the Sub store."); }

            string totalRole = LoginUser.RoleId.ToString();
            DGManageCamrea.Columns[8].Visibility = Visibility.Hidden;
            if (camBiz.CheckIsCamDeleteAccess(Convert.ToInt32(totalRole))>0)
            {
                DGManageCamrea.Columns[8].Visibility = Visibility.Visible;
            }

        }
       
        
    }
}
