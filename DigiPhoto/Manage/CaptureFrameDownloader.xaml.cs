﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.IO;
using DigiPhoto.DataLayer.Model;
using DigiPhoto.DataLayer;
using System.Collections.ObjectModel;
using DigiPhoto.Common;
using System.Diagnostics;
using FrameworkHelper;
using System.Windows.Threading;
using System.Windows.Controls.Primitives;
using WMPLib;
using System.Threading;
using DigiPhoto.Interop;
using System.Collections;
using DigiPhoto.IMIX.Business;
using DigiPhoto.IMIX.Model;


namespace DigiPhoto
{
    public partial class CaptureFrameDownloader : Window
    {
        #region Variables

        /// <summary>
        /// Process videos or not
        /// </summary>
        bool processVideos;
        /// <summary>
        /// The filepath
        /// </summary>
        string filepath;
        /// <summary>
        /// The image name
        /// </summary>
        private List<string> ImageName;
        /// <summary>
        /// The count
        /// </summary>
        static int count = 0;
        /// <summary>
        /// The processed count
        /// </summary>
        static int ProcessedCount = 0;
        /// <summary>
        /// The path
        /// </summary>
        string path = string.Empty;
        string thumbnailspath = string.Empty;
        string thumbnailsfolderpath = string.Empty;
        string tumbnailsfolderbigpath = string.Empty;
        Stopwatch stopwatch = new Stopwatch();
        bool IsBarcodeActive = false;
        bool IsUSBDelete = false;
        bool Show = false;

        Dictionary<string, DateTime> img = new Dictionary<string, DateTime>();
        Dictionary<string, string> imagePath = new Dictionary<string, string>();

        readonly DispatcherTimer plyTimer = new DispatcherTimer();
        //DispatcherTimer MediaTimer = new DispatcherTimer();
        private bool userIsDraggingSlider = false;
        private bool mediaPlayerIsPlaying = false;
        bool CheckBoxState = true;
        string selectedImage = string.Empty;
        System.ComponentModel.BackgroundWorker ManualDownloadworker = new System.ComponentModel.BackgroundWorker();
        BusyWindow bs = new BusyWindow();
        private FileStream memoryFileStream;
        static string vsMediaFileName = "";
        bool isMuted = false;
        Hashtable htFileSize = new Hashtable();
        public Hashtable htVidLength = new Hashtable();
        bool isVideoEditingEnabled = false;
       // MediaPlayerControl mpc = new MediaPlayerControl(vsMediaFileName, "ImageDownloader");
        DirectoryInfo dInfo = null;
        public ObservableCollection<MyImageClass> MyImages { get; set; }
        #endregion

        #region Constructor
        public CaptureFrameDownloader()
        {
            InitializeComponent();
            GetConfigurationInfo();
            lstImages.Items.Clear();
            filepath = LoginUser.DigiFolderPath;
            string root = Environment.CurrentDirectory;
            path = root + "\\";
            path = System.IO.Path.Combine(path, "Download\\");
            thumbnailspath = System.IO.Path.Combine(path, "Temp\\");
            string capturedFramePath = root + "\\";
            capturedFramePath = System.IO.Path.Combine(capturedFramePath, "CapturedFrames\\");
            dInfo = new DirectoryInfo(capturedFramePath);
            if (Directory.Exists(path))
                DeletePath(path);
            ManualDownloadworker.DoWork += new System.ComponentModel.DoWorkEventHandler(ManualDownloadworker_DoWork);
            ManualDownloadworker.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(ManualDownloadworker_RunWorkerCompleted);
            if (CheckDriveForImages())
            {
                Show = true;

            }

            btnacquire.IsDefault = true;
            //IsBarCodeActive();
        }
        #endregion
        private void ShowImages()
        {
            try
            {
                lstImages.Items.Clear();

                //MyImages = new ObservableCollection<MyImageClass>();
                //MyImages.Clear();

                //foreach (var item in img)
                //{
                //    string imgname = System.IO.Path.GetFileNameWithoutExtension(item.Key);
                //    string drivePath = string.Empty;
                //    bool isSuccess = imagePath.TryGetValue(item.Key, out drivePath);
                //    MyImages.Add(new MyImageClass(imgname, GetImageFromResourceString(imgname, drivePath), true, item.Value, drivePath));
                //}

                lstImages.ItemsSource = MyImages;

            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }
        private BitmapImage GetImageFromResourceString(string imageName)
        {

            BitmapImage image = new BitmapImage();
            image.BeginInit();
            image.DecodePixelWidth = 150;
            image.CacheOption = BitmapCacheOption.OnLoad;
            //image.UriSource = new Uri(filepath + imageName + ".jpg");
            image.UriSource = new Uri(imageName);
            image.EndInit();

            return image;
        }
        #region Events
        void btnBack_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                MediaStop();
                Home _objhome = new Home();
                _objhome.Show();
                this.Close();
                MyImages.Clear();
                if (Directory.Exists(path))
                {
                    if (Directory.Exists(thumbnailspath))
                    {
                        string[] filePaths1 = Directory.GetFiles(thumbnailspath);
                        foreach (var item in filePaths1)
                        {
                            if (File.Exists(item))
                            {
                                File.Delete(item);
                            }
                        }
                        Directory.Delete(thumbnailspath, true);
                    }
                    string[] filePaths = Directory.GetFiles(path);
                    foreach (var item in filePaths)
                    {
                        if (File.Exists(item))
                        {
                            File.Delete(item);
                        }
                    }
                    Directory.Delete(path, true);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }
        private void btnacquire_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                MediaStop();
                var Allimages = (from item in MyImages
                                 select item).ToList();
                ManualCtrl.SetParent(this);
                ManualCtrl.Visibility = Visibility.Collapsed;

                ManualCtrl.imagelist = Allimages;
                btnacquire.IsDefault = false;
                ManualCtrl.btnDownload.IsDefault = true;
                ManualCtrl.htVidL = this.htVidLength;
                var res = ManualCtrl.ShowHandlerDialog();
                btnacquire.IsDefault = true;
                ManualCtrl.btnDownload.IsDefault = false;
                if (res != null)
                {
                    Home _objhome = new Home();
                    _objhome.Show();
                    this.Close();
                    MyImages.Clear();
                    if (Directory.Exists(path))
                    {
                        if (Directory.Exists(thumbnailspath))
                        {
                            string[] filePaths1 = Directory.GetFiles(thumbnailspath);
                            foreach (var item in filePaths1)
                            {
                                try
                                {
                                    if (File.Exists(item))
                                    {
                                        File.Delete(item);
                                    }
                                }
                                catch (Exception ex)
                                {
                                    string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                                    ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                                }
                            }
                            Directory.Delete(thumbnailspath, true);
                        }
                        string[] filePaths = Directory.GetFiles(path);
                        foreach (var item in filePaths)
                        {
                            try
                            {
                                if (File.Exists(item))
                                {
                                    File.Delete(item);
                                }
                            }
                            catch (Exception ex)
                            {
                                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                            }
                        }
                        Directory.Delete(path, true);
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }
        private void DeletePath(string path)
        {
            try
            {
                string[] filePaths = Directory.GetFiles(path);
                foreach (var item in filePaths)
                {
                    try
                    {
                        if (File.Exists(item))
                        {
                            File.Delete(item);
                        }
                    }
                    catch (Exception ex)
                    {
                        string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                        ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                    }
                }

                if (Directory.Exists(path))
                {
                    Directory.Delete(path, true);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
            finally
            {
                if (Directory.Exists(path))
                {
                    try
                    {
                        Directory.Delete(path, true);
                    }
                    catch (Exception ex)
                    {
                        string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                        ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                    }
                }
            }
        }
        private bool CheckDriveForImages()
        {

            bool imgFound = false;
            Show = true;
            var items = Directory.EnumerateFiles(dInfo.FullName, "*.*", SearchOption.AllDirectories).Where(s => s.ToLower().EndsWith(".jpg")).ToList();
            if (isVideoEditingEnabled)
            {
                if (items.Count > 0)
                {
                    string msg = "Found\n\rImages: " + items.Count + "\n\rProcessing starting....";
                    MessageBox.Show(msg, "iMIX", MessageBoxButton.OK, MessageBoxImage.Information);
                    imgFound = true;
                }
                else
                {
                    MessageBox.Show("No file(s) found.");
                    this.Close();
                    Show = false;
                }
            }

            return imgFound;
        }
        public void CreateImages(bool sub = true)
        {
            #region Create the  file directory if it does not exists
            if (!Directory.Exists(path))
            {
                System.IO.Directory.CreateDirectory(path);
            }
            if (!Directory.Exists(thumbnailspath))
            {
                System.IO.Directory.CreateDirectory(thumbnailspath);
            }
            #endregion
            MyImages = new ObservableCollection<MyImageClass>();

            try
            {
                if (isVideoEditingEnabled)
                {
                    var videoitems = Directory.EnumerateFiles(dInfo.FullName, "*.*", SearchOption.AllDirectories).Where(s => s.ToLower().EndsWith(".wmv") || s.ToLower().EndsWith(".mp4") || s.ToLower().EndsWith(".avi") || s.ToLower().EndsWith(".mov") || s.ToLower().EndsWith(".3gp") || s.ToLower().EndsWith(".3g2") || s.ToLower().EndsWith(".m2v") || s.ToLower().EndsWith(".m4v") || s.ToLower().EndsWith(".flv") || s.ToLower().EndsWith(".mpeg") || s.ToLower().EndsWith(".mpg") || s.ToLower().EndsWith(".jpg")).ToList();
                    foreach (var vidItems in videoitems)
                    {
                        FileInfo _objVid = new FileInfo(vidItems.ToString());
                        if (_objVid.Extension.ToLower() == ".jpg")
                        {
                            Dispatcher.Invoke(new Action(() =>
                               MyImages.Add(new MyImageClass(_objVid.Name, GetImageFromResourceString(_objVid.FullName), true, _objVid.CreationTime, _objVid.FullName))
                             ));
                        }
                        else
                        {
                            //Dispatcher.Invoke(new Action(() =>
                            //   ThumbnailExtractor.ExtractThumbnailFromVideo(_objVid.FullName, 4, 4, path + _objVid.Name + ".jpg",_VisioForgeLicenseKey,_VisioForgeUserName,_VisioForgeEmailID)
                            // ));
                            long vLen = 0;// ThumbnailExtractor.VideoLength;
                            if (!htVidLength.ContainsKey(_objVid.Name))
                            {
                                htVidLength.Add(_objVid.Name, vLen);
                            }
                            Dispatcher.Invoke(new Action(() =>
                               MyImages.Add(new MyImageClass(_objVid.Name, GetImageFromResourceString(path + _objVid.Name + ".jpg"), true, _objVid.CreationTime, _objVid.FullName))
                             ));
                        }

                        count++;
                        ProcessedCount++;
                        string mbsize = ConvertBytesToMegabytes((long)_objVid.Length).ToString("0.00");
                        if (!htFileSize.ContainsKey(_objVid.Name))
                        {
                            htFileSize.Add(_objVid.Name, mbsize);
                        }
                        //}
                    }
                }
                else
                {
                    var pictures = Directory.EnumerateFiles(dInfo.FullName, "*.*", SearchOption.AllDirectories).Where(s => s.ToLower().EndsWith(".jpg")).ToList();
                    foreach (var picItems in pictures)
                    {
                        if (!picItems.Contains("Thumbs.db"))
                        {
                            FileInfo _objPic = new FileInfo(picItems.ToString());
                            _objPic.CopyTo(path + _objPic.Name, true);
                            ResizeWPFImage(path + _objPic.Name, 900, thumbnailspath + _objPic.Name);
                            string mbsize = ConvertBytesToMegabytes((long)_objPic.Length).ToString("0.00");
                            if (!htFileSize.ContainsKey(_objPic.Name))
                            {
                                htFileSize.Add(_objPic.Name, mbsize);
                            }
                            count++;
                            ProcessedCount++;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }

        }

        private double ConvertBytesToMegabytes(long bytes)
        {
            return (bytes / 1024f) / 1024f;
        }
        #region Public Menthod
        public bool IsShoworHide()
        {
            return Show;
        }
        #endregion

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            if (Show == true)
            {
                bs.Show();
                bs.BringIntoView();
                ManualDownloadworker.RunWorkerAsync();
            }
        }
        private BitmapImage GetImageFromPath(string path)
        {
            try
            {
                BitmapImage bi = new BitmapImage();
                using (FileStream fileStream = File.OpenRead(path))
                {
                    MemoryStream ms = new MemoryStream();
                    fileStream.CopyTo(ms);
                    ms.Seek(0, SeekOrigin.Begin);
                    fileStream.Close();
                    bi.CacheOption = BitmapCacheOption.OnLoad; // Memory 
                    bi.BeginInit();
                    bi.StreamSource = ms;
                    bi.EndInit();
                    bi.Freeze(); // Memory 
                    fileStream.Close();
                    return bi;
                }

            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                return null;
            }
        }

        private string EstimatedAcquisitionTime()
        {
            string time = string.Empty;
            double tempSeconds = 0.0; double filecopylag = 0.0;
            foreach (var item1 in lstImages.Items)
            {
                var tempItem1 = (MyImageClass)item1;
                if (tempItem1.IsChecked)
                {
                    if (htFileSize.ContainsKey(tempItem1.Title))
                    {
                        //file of 1mb takes 0.25 sec to acquire
                        filecopylag = Convert.ToDouble(htFileSize[tempItem1.Title].ToString()) * 0.25;
                        if (!tempItem1.Title.ToLower().EndsWith(".jpg"))
                        {
                            tempSeconds += filecopylag + 2;//2 for video thumbnail
                        }
                        else
                        {
                            tempSeconds += filecopylag; //for images
                        }
                    }
                }
            }
            TimeSpan t = TimeSpan.FromSeconds(tempSeconds);
            time = "[Minimum acquisition time: " + string.Format("{0:D2}h:{1:D2}m:{2:D2}s", t.Hours, t.Minutes, t.Seconds) + "]";
            return time;
        }

        /// <summary>
        /// Used to select all images
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void chkSelectAll_Click(object sender, RoutedEventArgs e)
        {

            foreach (FrameworkHelper.MyImageClass MyImageClass in lstImages.Items)
            {
                MyImageClass.IsChecked = (bool)((CheckBox)sender).IsChecked;
            }
            txbSelectedImages.Text = "Selected:" + (((CheckBox)sender).IsChecked == true ? lstImages.Items.Count.ToString() : "0");
            lblEstimate.Text = EstimatedAcquisitionTime();
            lstImages.Items.Refresh();
        }
        private void Selectedimages_Click(object sender, RoutedEventArgs e)
        {
            if (IMGFrame.Visibility == Visibility.Visible)
            {
                if (((CheckBox)sender).IsChecked == true)
                {
                    txbSelectedImages.Text = "Selected :" + (int.Parse(txbSelectedImages.Text.Split(':').Last().Trim()) + 1).ToString();
                }
                if (((CheckBox)sender).IsChecked == false)
                {
                    txbSelectedImages.Text = "Selected :" + (int.Parse(txbSelectedImages.Text.Split(':').Last().Trim()) - 1).ToString();
                }
                if (int.Parse(txbSelectedImages.Text.Split(':').Last().Trim()) == lstImages.Items.Count)
                    chkSelectAll.IsChecked = true;
                else
                    chkSelectAll.IsChecked = false;

                string vidFileName = ((System.Windows.Media.Imaging.BitmapImage)((((Image)((System.Windows.Controls.Grid)(((CheckBox)e.Source).Parent)).FindName("thumbImage"))).Source)).UriSource.OriginalString.ToLower();
                if (vidFileName.EndsWith("avi.jpg") || vidFileName.EndsWith("mp4.jpg") || vidFileName.EndsWith("wmv.jpg") || vidFileName.EndsWith("mov.jpg") || vidFileName.EndsWith("3gp.jpg") || vidFileName.EndsWith("3g2.jpg") || vidFileName.EndsWith("m2v.jpg") || vidFileName.EndsWith("m4v.jpg") || vidFileName.EndsWith("flv.jpg") || vidFileName.EndsWith("mpg.jpg") || vidFileName.EndsWith("ffmpeg.jpg"))
                {
                }
                else
                {
                    MediaStop();
                    imgmain.Visibility = Visibility.Visible;
                    vidoriginal.Visibility = Visibility.Collapsed;
                    imgmain.Visibility = Visibility.Visible;
                    vidoriginal.Visibility = Visibility.Collapsed;
                    imgmain.Source = GetImageFromPath(((System.Windows.Media.Imaging.BitmapImage)((((Image)((System.Windows.Controls.Grid)(((CheckBox)e.Source).Parent)).FindName("thumbImage"))).Source)).UriSource.OriginalString);
                }
            }
            else
            {
                selectedImage = ((System.Windows.Media.Imaging.BitmapImage)((((Image)((System.Windows.Controls.Grid)(((CheckBox)e.Source).Parent)).FindName("thumbImage"))).Source)).UriSource.OriginalString; ;
                if (((CheckBox)sender).IsChecked == true)
                {
                    txbSelectedImages.Text = "Selected :" + (int.Parse(txbSelectedImages.Text.Split(':').Last().Trim()) + 1).ToString();
                }
                if (((CheckBox)sender).IsChecked == false)
                {
                    txbSelectedImages.Text = "Selected :" + (int.Parse(txbSelectedImages.Text.Split(':').Last().Trim()) - 1).ToString();
                }
                if (int.Parse(txbSelectedImages.Text.Split(':').Last().Trim()) == lstImages.Items.Count)
                    chkSelectAll.IsChecked = true;
                else
                    chkSelectAll.IsChecked = false;

            }
            lblEstimate.Text = EstimatedAcquisitionTime();
        }
        private void btnWithPreviewActive_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (lstImages.Items.Count > 0)
                {
                    chkSelectAll.IsEnabled = true;//saurabh
                    thumbPreview.HorizontalAlignment = HorizontalAlignment.Right;
                    lstImages.Width = 250;
                    IMGFrame.Visibility = Visibility.Visible;

                    imgwithPreview.Source = new BitmapImage(new Uri(@"/images/thumbnailview1_active.png", UriKind.Relative));
                    imgwithoutPreview.Source = new BitmapImage(new Uri(@"images/thumbnailview2.png", UriKind.Relative));
                    string vidFileName = selectedImage;
                    if (string.IsNullOrEmpty(vidFileName))
                    {
                        lstImages.SelectedItem = lstImages.Items[0];
                        MediaStop();
                        vidFileName = ((System.Windows.Media.Imaging.BitmapImage)(((FrameworkHelper.MyImageClass)(lstImages.SelectedItem)).Image)).UriSource.OriginalString.ToLower();
                    }
                    if (vidFileName.EndsWith("avi.jpg") || vidFileName.EndsWith("mp4.jpg") || vidFileName.EndsWith("wmv.jpg") || vidFileName.EndsWith("mov.jpg") || vidFileName.EndsWith("3gp.jpg") || vidFileName.EndsWith("3g2.jpg") || vidFileName.EndsWith("m2v.jpg") || vidFileName.EndsWith("m4v.jpg") || vidFileName.EndsWith("flv.jpg") || vidFileName.EndsWith("mpg.jpg") || vidFileName.EndsWith("ffmpeg.jpg"))
                    {
                        vidFileName = vidFileName.Replace(".jpg", string.Empty);
                        var videoitems = Directory.EnumerateFiles(dInfo.FullName, "*.*", SearchOption.AllDirectories).Where(s => s.ToLower().EndsWith(".wmv") || s.ToLower().EndsWith(".mp4") || s.ToLower().EndsWith(".avi") || s.ToLower().EndsWith(".mov") || s.ToLower().EndsWith(".3gp") || s.ToLower().EndsWith(".3g2") || s.ToLower().EndsWith(".m2v") || s.ToLower().EndsWith(".m4v") || s.ToLower().EndsWith(".flv") || s.ToLower().EndsWith(".mpg") || s.ToLower().EndsWith("ffmpeg")).ToList();
                        var tempvid = videoitems.Where(v => v.ToLower().Contains(System.IO.Path.GetFileName(vidFileName))).FirstOrDefault();
                        if (tempvid != null)
                        {
                            imgmain.Visibility = Visibility.Collapsed;
                            vidoriginal.Visibility = Visibility.Visible;
                            MediaStop();
                            vsMediaFileName = tempvid.ToString();
                            MediaPlay();
                        }
                    }
                    else
                    {
                        imgmain.Visibility = Visibility.Visible;
                        vidoriginal.Visibility = Visibility.Collapsed;
                        imgmain.Source = GetImageFromPath(vidFileName);
                    }
                }
                else
                    IMGFrame.Visibility = Visibility.Collapsed;
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
            finally
            {
                MemoryManagement.FlushMemory();
            }
        }
        private void btnWithoutPreview_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                MediaStop();
                if (lstImages.Items.Count > 0)
                {
                    selectedImage = string.Empty;
                    chkSelectAll.IsEnabled = true;
                    thumbPreview.HorizontalAlignment = HorizontalAlignment.Stretch;
                    lstImages.Width = double.NaN;
                    IMGFrame.Visibility = Visibility.Collapsed;
                    imgwithPreview.Source = new BitmapImage(new Uri(@"images/thumbnailview1.png", UriKind.Relative));
                    imgwithoutPreview.Source = new BitmapImage(new Uri(@"/images/thumbnailview2_active.png", UriKind.Relative));
                }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
            finally
            {
                MemoryManagement.FlushMemory();
            }
        }
        private void ResizeWPFImage(string sourceImage, int maxHeight, string saveToPath)
        {
            try
            {
                BitmapImage bi = new BitmapImage();
                BitmapImage bitmapImage = new BitmapImage();
                using (FileStream fileStream = File.OpenRead(sourceImage.ToString()))
                {
                    MemoryStream ms = new MemoryStream();
                    fileStream.CopyTo(ms);
                    ms.Seek(0, SeekOrigin.Begin);
                    fileStream.Close();
                    bi.BeginInit();
                    bi.StreamSource = ms;
                    bi.EndInit();
                    bi.Freeze();

                    decimal ratio = 0;
                    int newWidth = 0;
                    int newHeight = 0;

                    if (bi.Width >= bi.Height)
                    {
                        ratio = Convert.ToDecimal(bi.Width) / Convert.ToDecimal(bi.Height);
                        newWidth = maxHeight;
                        newHeight = Convert.ToInt32(maxHeight / ratio);
                    }
                    else
                    {
                        ratio = Convert.ToDecimal(bi.Height) / Convert.ToDecimal(bi.Width);
                        newHeight = maxHeight;
                        newWidth = Convert.ToInt32(maxHeight / ratio);
                    }
                    ms.Seek(0, SeekOrigin.Begin);
                    bitmapImage.BeginInit();
                    bitmapImage.StreamSource = ms;
                    bitmapImage.DecodePixelWidth = newWidth;
                    bitmapImage.DecodePixelHeight = newHeight;
                    bitmapImage.EndInit();
                    bitmapImage.Freeze();
                    fileStream.Close();
                }

                using (var fileStreamForSave = new FileStream(saveToPath, FileMode.Create, FileAccess.ReadWrite))
                {
                    JpegBitmapEncoder encoder = new JpegBitmapEncoder();
                    encoder.QualityLevel = 94;
                    encoder.Frames.Add(BitmapFrame.Create(bitmapImage));
                    encoder.Save(fileStreamForSave);
                    fileStreamForSave.Close();
                }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }

        private void ManualDownloadworker_DoWork(object Sender, System.ComponentModel.DoWorkEventArgs e)
        {
            CreateImages(true);
        }
        private void ManualDownloadworker_RunWorkerCompleted(object Sender, System.ComponentModel.RunWorkerCompletedEventArgs e)
        {
            txbCount.Text = "/" + MyImages.Count().ToString();// img.Count.ToString();
            txbSelectedImages.Text = "Selected :" + MyImages.Count().ToString();
            ShowImages();
            bs.Hide();
            lblEstimate.Text = EstimatedAcquisitionTime();
        }

        private void vidPlay_Click(object sender, RoutedEventArgs e)
        {
            string vidFileName = ((System.Windows.Media.Imaging.BitmapImage)((((Image)((System.Windows.Controls.Grid)(((Button)e.Source).Parent)).FindName("thumbImage"))).Source)).UriSource.OriginalString.ToLower();
            selectedImage = vidFileName;
            btnWithPreviewActive_Click(sender, e);
        }

        private static DependencyObject RecursiveVisualChildFinder<T>(DependencyObject rootObject)
        {
            var child = VisualTreeHelper.GetChild(rootObject, 0);
            if (child == null) return null;

            return child.GetType() == typeof(T) ? child : RecursiveVisualChildFinder<T>(child);
        }

        #region MediaPlayer
        void MediaStop()
        {
            //mpc.VisioMediaPlayer.Stop();
            //gdMediaPlayer.Children.Clear();
        }
        void MediaPlay()
        {
            //mpc = new MediaPlayerControl(vsMediaFileName, "ImageDownloader");
            //gdMediaPlayer.Children.Add(mpc);
        }

        #endregion

        private void btnWithPreviewActive_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            e.Handled = true;
        }

        private void vidPlay_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            e.Handled = true;
        }

        private void GetConfigurationInfo()
        {
            try
            {
                //DigiPhotoDataServices objdbLayer = new DigiPhotoDataServices();
                List<long> objList = new List<long>();
                objList.Add((long)ConfigParams.MktImgPath);
                objList.Add((long)ConfigParams.MktImgTimeInSec);
                objList.Add((long)ConfigParams.IsEnabledVideoEdit);

                List<iMIXConfigurationInfo> ConfigValuesList = (new ConfigBusiness()).GetNewConfigValues(LoginUser.SubStoreId).Where(o => objList.Contains(o.IMIXConfigurationMasterId)).ToList();
                if (ConfigValuesList != null || ConfigValuesList.Count > 0)
                {
                    for (int i = 0; i < ConfigValuesList.Count; i++)
                    {
                        switch (ConfigValuesList[i].IMIXConfigurationMasterId)
                        {
                            case (int)ConfigParams.IsEnabledVideoEdit:
                                isVideoEditingEnabled = ConfigValuesList[i].ConfigurationValue != null ? Convert.ToBoolean(ConfigValuesList[i].ConfigurationValue) : false;
                                break;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }

    }
}

        #endregion