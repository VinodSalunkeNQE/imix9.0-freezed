﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Threading;
using System.Windows;
using DigiPhoto.Common;
using DigiPhoto.DataLayer;
using DigiPhoto.DataLayer.Model;
using FrameworkHelper;
using System.Configuration;
using Microsoft.Win32;
using System.Windows.Media.Animation;
using System.ServiceProcess;
using FrameworkHelper.Common;
using DigiPhoto.iMix.ViewModel;
using System.Collections;
using System.Net;
using System.Net.NetworkInformation;
using DigiPhoto.IMIX.Model;
using DigiPhoto.IMIX.Business;
using System.ComponentModel;
using DigiPhoto.Utility.Repository.ValueType;
using DigiPhoto.Cache.DataCache;
using System.Management;
using System.Data.SqlClient;
using System.Data;
using Nikon;
using System.Text;
using System.Web.Script.Serialization;
using Newtonsoft.Json;
using System.Windows.Documents;
using Newtonsoft.Json.Linq;

namespace DigiPhoto
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
#if DEBUG
        Stopwatch watch1 = CommonUtility.Watch();
#endif
        #region Declaration
        BackgroundWorker worker;
        public static int ViewOrderSearchIndex = 0;
        public static int SelectedCodeType = 405;
        public static string QRCodeWebUrl = string.Empty;
        public static string DataSyncServiceURl = string.Empty;
        public static bool IsAnonymousQrCodeEnabled = false;
        public static bool IsRFIDEnabled = false;
        public static int? RfidScanType = null;
        public static int tripCamCleanUpDays, iMIXCleanUpDays = 3;
        private delegate int LowLevelKeyboardProcDelegate(int nCode, int wParam, ref KBDLLHOOKSTRUCT lParam);

        #region ADDED bY AJAY
        private List jsonAsList = new List();
        #endregion
        private struct KBDLLHOOKSTRUCT
        {
            public System.Windows.Forms.Keys key;
            public int vkCode;
            int scanCode;
            public int flags;
            int time;
            int dwExtraInfo;
        }
        [DllImport("kernel32.dll")]
        private static extern IntPtr GetModuleHandle(IntPtr path);
        [DllImport("user32.dll", EntryPoint = "SetWindowsHookExA", CharSet = CharSet.Ansi)]
        private static extern IntPtr SetWindowsHookEx(
            int idHook,
            LowLevelKeyboardProcDelegate lpfn,
            IntPtr hMod,
            int dwThreadId);
        private IntPtr intLLKey;
        const int WH_KEYBOARD_LL = 13;
        [DllImport("user32.dll", EntryPoint = "CallNextHookEx", CharSet = CharSet.Ansi)]
        private static extern int CallNextHookEx(
            int hHook, int nCode,
            int wParam, ref KBDLLHOOKSTRUCT lParam);
        [DllImport("user32.dll")]
        private static extern IntPtr UnhookWindowsHookEx(IntPtr hHook);


        [DllImport("user32", EntryPoint = "SystemParametersInfo", CharSet = CharSet.Auto, SetLastError = true)]
        private static extern int SystemParametersInfo(int uiAction, int uiParam, int pvParam, int fWinIni);

        private const Int32 SPI_SETSCREENSAVETIMEOUT = 15;


        #endregion
        /// <summary>
        /// This event is for cleaning/deleting  the images older than 3 days
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Application_Startup(object sender, StartupEventArgs e)
        {
            AppDomain.CurrentDomain.UnhandledException += CurrentDomain_UnhandledException;
            LoadMD3Files();//It should be optimized in Manage Camera ....By Vins for preventing Nikon DLL use_18Oct2019//Fixed SmartShooter issue
            DataCacheFactory.Register();
#if DEBUG
            var watch = CommonUtility.Watch();
            watch.Start();
#endif

            try
            {
                object SystemName = "";
                string hostName = Dns.GetHostName(); // Retrive the Name of HOST
                string myIP = Dns.GetHostByName(hostName).AddressList[0].ToString();//ip              
                ManagementObjectSearcher searcher =
                    new ManagementObjectSearcher("root\\CIMV2",
                    "SELECT Name FROM Win32_ComputerSystem");

                foreach (ManagementObject queryObj in searcher.Get())
                {
                    SystemName = queryObj["Name"];
                }
                ManagementObjectSearcher search = new ManagementObjectSearcher("SELECT * FROM Win32_NetworkAdapterConfiguration where IPEnabled=true");
                IEnumerable<ManagementObject> objects = search.Get().Cast<ManagementObject>();
                string mac = string.Empty;
                try
                {
                    mac = (from o in objects orderby o["IPConnectionMetric"] select o["MACAddress"].ToString()).FirstOrDefault();
                }
                catch
                {

                }
                ServicePosInfoBusiness servicePosInfoBusiness = new ServicePosInfoBusiness();
                string systemname = SystemName.ToString();
                servicePosInfoBusiness.StartStopSystemBusiness(mac, myIP, systemname, 1);

                worker = new BackgroundWorker();
                worker.DoWork += new DoWorkEventHandler(worker_DoWork_AppDataCleanUp);
                worker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(worker_RunWorkerCompleted_AppDataCleanUp);

                Timeline.DesiredFrameRateProperty.OverrideMetadata(typeof(Timeline),
                                    new FrameworkPropertyMetadata { DefaultValue = 5 });

                int x = ConfigurationManager.AppSettings["NeverSleepTimeInMinutes"].ToInt32();
                SystemParametersInfo(SPI_SETSCREENSAVETIMEOUT, x, 0, 0);
                bool isSplashActive = false;
                try
                {                    
                    if (ConfigurationManager.AppSettings.AllKeys.Contains("IsSplashScreenActive") == true)
                    {
                        if (ConfigurationManager.AppSettings["IsSplashScreenActive"] == "1")
                        {
                            isSplashActive = true;
                            Splasher.Splash = new SplashScreen();
                            Splasher.ShowSplash();
                        }
                    }
                    else
                    {
                        //Key not added first time
                        isSplashActive = true;
                        Splasher.Splash = new SplashScreen();
                        Splasher.ShowSplash();
                    }
                    //MessageListener.Instance.ReceiveMessage(string.Format("Initializing...."));
                    //DigiPhotoDataServices objdbLayer = new DigiPhotoDataServices();

                    string line;
                    string SubStoreId;
                    using (StreamReader reader = new StreamReader(Environment.CurrentDirectory + "\\ss.dat"))
                    {
                        line = reader.ReadLine();
                        SubStoreId = CryptorEngine.Decrypt(line, true);
                        LoginUser.SubStoreId = (SubStoreId.Split(',')[0]).ToInt32();
                    }
                    //TBD
                    // vw_GetConfigdata _objdata = objdbLayer.GetConfigurationData(LoginUser.SubStoreId);

                    ConfigBusiness configBusiness = new ConfigBusiness();
                    ConfigurationInfo _objdata = configBusiness.GetConfigurationData(LoginUser.SubStoreId);

                    ErrorHandler.ErrorHandler.LogFileWrite("Substore ID :- " + LoginUser.SubStoreId.ToString());

                    List<string> mylistCam = new List<string>();

                    CameraBusiness CameraBusiness = new CameraBusiness();
                    foreach (var mycameras in CameraBusiness.GetAvailableRideCameras())
                    {
                        mylistCam.Add(mycameras.RideCameras);
                    }
                    int deletedOldImagesValue = (new ConfigBusiness()).GetDeletedOldImagesConfigurationData("DeletedOldImages");
                    // int olderthandays = ConfigurationManager.AppSettings["DeletedOldImages"].ToInt32();
                    int olderthandays = deletedOldImagesValue;
                    Console.WriteLine("DeletedOldImages =" + olderthandays);
                    object configData = _objdata;
                    object camList = mylistCam;
                    object daysOld = olderthandays;

                    object[] workerParams = new object[] { configData, camList, daysOld };

                    //stopWatch.Start();
                    worker.RunWorkerAsync(workerParams);

                }
                catch (Exception ex)
                {
                    string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                    ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                }
                DigiStartup(this, e);
                if (isSplashActive)
                {
                    Splasher.Splash.Hide();
                }
                CheckUpdate();
                RegisterSystem();
                ChromaKeypluginLic.IntializeProtection();
                DecoderlibLic.IntializeProtection();
                EncoderlibLic.IntializeProtection();
                MComposerlibLic.IntializeProtection();
                MPlatformSDKLic.IntializeProtection();


                #region Add Keys Dynamically by AJay on  1 June
                //string ConfigPath = @"C:\Program Files (x86)\iMix";
                //var directory = ConfigPath + "\\ConfiDetails\\ConfigDetailsJSON-Digiphoto.json";
                //string jrf = System.IO.File.ReadAllText(directory);
                //JavaScriptSerializer serializer = new JavaScriptSerializer();
                //jsonAsList = serializer.Deserialize<List>(jrf.ToString());
                //var jsonObj = JsonConvert.DeserializeObject<JObject>(jrf).First.First;
                //System.Configuration.Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);

                ////READ THE CONFIG FILE.
                //if (ConfigurationManager.AppSettings.AllKeys.Contains("jpgFileExtension") == false)
                //{
                //    string jpgFileExtension = Convert.ToString(jsonObj["jpgFileExtension"]);
                //    config.AppSettings.Settings.Add("jpgFileExtension", jpgFileExtension);
                //}
                //if (ConfigurationManager.AppSettings.AllKeys.Contains("pngFileExtension") == false)
                //{
                //    string pngFileExtension = Convert.ToString(jsonObj["pngFileExtension"]);
                //    config.AppSettings.Settings.Add("pngFileExtension", pngFileExtension);
                //}
                //if (ConfigurationManager.AppSettings.AllKeys.Contains("IsGSTEnabled") == false)
                //{
                //    string IsGSTEnabled = Convert.ToString(jsonObj["IsGSTEnabled"]);
                //    config.AppSettings.Settings.Add("IsGSTEnabled", IsGSTEnabled);
                //}

                //// For We chat, whats app 

                //if (ConfigurationManager.AppSettings.AllKeys.Contains("CloudName") == false)
                //{
                //    string IsGSTEnabled = Convert.ToString(jsonObj["CloudName"]);
                //    config.AppSettings.Settings.Add("CloudName", IsGSTEnabled);
                //}

                //if (ConfigurationManager.AppSettings.AllKeys.Contains("ApiKey") == false)
                //{
                //    string IsGSTEnabled = Convert.ToString(jsonObj["ApiKey"]);
                //    config.AppSettings.Settings.Add("ApiKey", IsGSTEnabled);
                //}

                //if (ConfigurationManager.AppSettings.AllKeys.Contains("ApiSecret") == false)
                //{
                //    string IsGSTEnabled = Convert.ToString(jsonObj["ApiSecret"]);
                //    config.AppSettings.Settings.Add("ApiSecret", IsGSTEnabled);
                //}

                //if (ConfigurationManager.AppSettings.AllKeys.Contains("ApiBaseAddress") == false)
                //{
                //    string IsGSTEnabled = Convert.ToString(jsonObj["ApiBaseAddress"]);
                //    config.AppSettings.Settings.Add("ApiBaseAddress", IsGSTEnabled);
                //}

                //if (ConfigurationManager.AppSettings.AllKeys.Contains("LinkAddress") == false)
                //{
                //    string IsGSTEnabled = Convert.ToString(jsonObj["LinkAddress"]);
                //    config.AppSettings.Settings.Add("LinkAddress", IsGSTEnabled);
                //}

                //if (ConfigurationManager.AppSettings.AllKeys.Contains("DestinationSiteDirectoryPath_1") == false)
                //{
                //    string IsGSTEnabled = Convert.ToString(jsonObj["DestinationSiteDirectoryPath_1"]);
                //    config.AppSettings.Settings.Add("DestinationSiteDirectoryPath_1", IsGSTEnabled);
                //}

                //if (ConfigurationManager.AppSettings.AllKeys.Contains("DefaultCanvasWidth") == false)
                //{
                //    string IsGSTEnabled = Convert.ToString(jsonObj["DefaultCanvasWidth"]);
                //    config.AppSettings.Settings.Add("DefaultCanvasWidth", IsGSTEnabled);
                //}

                //if (ConfigurationManager.AppSettings.AllKeys.Contains("DefaultCanvasHeight") == false)
                //{
                //    string IsGSTEnabled = Convert.ToString(jsonObj["DefaultCanvasHeight"]);
                //    config.AppSettings.Settings.Add("DefaultCanvasHeight", IsGSTEnabled);
                //}

                //if (ConfigurationManager.AppSettings.AllKeys.Contains("CloudinaryOrderImagesPath") == false)
                //{
                //    string IsGSTEnabled = Convert.ToString(jsonObj["CloudinaryOrderImagesPath"]);
                //    config.AppSettings.Settings.Add("CloudinaryOrderImagesPath", IsGSTEnabled);
                //}

                //if (ConfigurationManager.AppSettings.AllKeys.Contains("CloudinaryImagesBufferSize") == false)
                //{
                //    string IsGSTEnabled = Convert.ToString(jsonObj["CloudinaryImagesBufferSize"]);
                //    config.AppSettings.Settings.Add("CloudinaryImagesBufferSize", IsGSTEnabled);
                //}

                //if (ConfigurationManager.AppSettings.AllKeys.Contains("CloudinaryVideosBufferSize") == false)
                //{
                //    string IsGSTEnabled = Convert.ToString(jsonObj["CloudinaryVideosBufferSize"]);
                //    config.AppSettings.Settings.Add("CloudinaryVideosBufferSize", IsGSTEnabled);
                //}

                //if (ConfigurationManager.AppSettings.AllKeys.Contains("WeChatPortal") == false)
                //{
                //    string IsGSTEnabled = Convert.ToString(jsonObj["WeChatPortal"]);
                //    config.AppSettings.Settings.Add("WeChatPortal", IsGSTEnabled);
                //}

                //if (ConfigurationManager.AppSettings.AllKeys.Contains("isWhatsAppCloudUpload") == false)
                //{
                //    string IsGSTEnabled = Convert.ToString(jsonObj["isWhatsAppCloudUpload"]);
                //    config.AppSettings.Settings.Add("isWhatsAppCloudUpload", IsGSTEnabled);
                //}

                ////END For We chat, whats app 

                //if (ConfigurationManager.AppSettings.AllKeys.Contains("ProductExtentionCode") == false)
                //{
                //    string isProductExtentionCode = Convert.ToString(jsonObj["ProductExtentionCode"]);
                //    config.AppSettings.Settings.Add("ProductExtentionCode", isProductExtentionCode);
                //}

                //if (ConfigurationManager.AppSettings.AllKeys.Contains("IsProductCodeWithBarCode") == false)
                //{
                //    string isPCodeWithBarCode = "false";//Convert.ToString(jsonObj["IsProductCodeWithBarCode"]);
                //    config.AppSettings.Settings.Add("IsProductCodeWithBarCode", isPCodeWithBarCode);
                //}

                //if (ConfigurationManager.AppSettings.AllKeys.Contains("ISSGSTCGST") == false)
                //{
                //    string iSSGSTCGST = "false";//Convert.ToString(jsonObj["ISSGSTCGST"]);
                //    config.AppSettings.Settings.Add("ISSGSTCGST", iSSGSTCGST);
                //}

                //if (ConfigurationManager.AppSettings.AllKeys.Contains("Arabic") == true)
                //{
                //    config.AppSettings.Settings.Remove("Arabic");
                //}

                //if (ConfigurationManager.AppSettings.AllKeys.Contains("IsArabic") == false)
                //{
                //    string isArabicVal = "0";
                //    config.AppSettings.Settings.Add("IsArabic", isArabicVal);
                //}

                //if (ConfigurationManager.AppSettings.AllKeys.Contains("QualityLevel") == false)
                //{
                //    string QualityLevel = "100";
                //    config.AppSettings.Settings.Add("QualityLevel", QualityLevel);
                //}

                //if (ConfigurationManager.AppSettings.AllKeys.Contains("ServiceEmail") == false)
                //{
                //    string ServiceEmail = Convert.ToString(jsonObj["ServiceEmail"]);
                //    config.AppSettings.Settings.Add("ServiceEmail", ServiceEmail);

                //}
                //// Save the changes in App.config file.
                //config.Save(ConfigurationSaveMode.Full);
                //ConfigurationManager.RefreshSection("appSettings");
                //if (ConfigurationManager.AppSettings.AllKeys.Contains("LicenseExpireInMonths") == false)
                //{
                //    string LicenseExpireInMonths = Convert.ToString(jsonObj["LicenseExpireInMonths"]);
                //    config.AppSettings.Settings.Add("LicenseExpireInMonths", LicenseExpireInMonths);

                //    // Save the changes in App.config file.
                //    config.Save(ConfigurationSaveMode.Full);
                //    ConfigurationManager.RefreshSection("appSettings");
                //}
                #endregion

            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }

#if DEBUG
            if (watch != null)
                CommonUtility.WatchStop("Start Up", watch);
#endif
        }

        private void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            var exception = e.ExceptionObject as Exception;
            ErrorHandler.ErrorHandler.LogError(exception);
            if (e.IsTerminating)
            {
                //Now is a good time to write that critical error file! 
                ErrorHandler.ErrorHandler.LogFileWrite("App terminated at time:" + System.DateTime.Now.ToString());
            }
        }

        private void worker_DoWork_AppDataCleanUp(object sender, DoWorkEventArgs e)
        {
#if DEBUG
            if (watch1 != null)
                watch1.Start();
#endif

            object[] parameters = e.Argument as object[];
            try
            {
                // do something.
                ConfigurationInfo configData = (ConfigurationInfo)parameters[0];
                List<string> camList = (List<string>)parameters[1];
                int daysOld = (int)parameters[2];
                GetStoreConfigData();
                AppDataCleanUp(configData, camList, daysOld);
                e.Result = true;
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogFileWrite("AppDataCleanup error:" + ex.Message);
            }
        }

        private void worker_RunWorkerCompleted_AppDataCleanUp(object sender, RunWorkerCompletedEventArgs e)
        {
#if DEBUG
            if (watch1 != null)
                CommonUtility.WatchStop("Clean Up", watch1);
#endif
            //stopWatch.Stop();
            //object result = e.Result;
            //// Handle what to do when complete.  
            //TimeSpan ts = stopWatch.Elapsed;
            //string elapsedTime = String.Format("{0:00}:{1:00}:{2:00}.{3:00}", ts.Hours, ts.Minutes, ts.Seconds, ts.Milliseconds / 10);

            //FileStream fileStream = new FileStream(@"D:\pl.txt", FileMode.Append);
            //StreamWriter streamWriter;
            //streamWriter = new StreamWriter(fileStream);
            //streamWriter.WriteLine(elapsedTime);
            //streamWriter.WriteLine(" ");
            //if (streamWriter != null) streamWriter.Close();
            //if (fileStream != null) fileStream.Close();          
        }
        private void AppDataCleanUp(ConfigurationInfo _objdata, List<string> mylistCam, int olderthandays)
        {
            #region Add Keys Dynamically by Vinod Salunke in background
            string ConfigPath = @"C:\Program Files (x86)\iMix";
            var jsonDirectory = ConfigPath + "\\ConfiDetails\\ConfigDetailsJSON-Digiphoto.json";
            string jrf = System.IO.File.ReadAllText(jsonDirectory);
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            jsonAsList = serializer.Deserialize<List>(jrf.ToString());
            var jsonObj = JsonConvert.DeserializeObject<JObject>(jrf).First.First;
            System.Configuration.Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);

            //READ THE CONFIG FILE.
            if (ConfigurationManager.AppSettings.AllKeys.Contains("jpgFileExtension") == false)
            {
                string jpgFileExtension = Convert.ToString(jsonObj["jpgFileExtension"]);
                config.AppSettings.Settings.Add("jpgFileExtension", jpgFileExtension);
            }
            if (ConfigurationManager.AppSettings.AllKeys.Contains("pngFileExtension") == false)
            {
                string pngFileExtension = Convert.ToString(jsonObj["pngFileExtension"]);
                config.AppSettings.Settings.Add("pngFileExtension", pngFileExtension);
            }
            if (ConfigurationManager.AppSettings.AllKeys.Contains("IsGSTEnabled") == false)
            {
                string IsGSTEnabled = Convert.ToString(jsonObj["IsGSTEnabled"]);
                config.AppSettings.Settings.Add("IsGSTEnabled", IsGSTEnabled);
            }

            // For We chat, whats app 

            if (ConfigurationManager.AppSettings.AllKeys.Contains("CloudName") == false)
            {
                string IsGSTEnabled = Convert.ToString(jsonObj["CloudName"]);
                config.AppSettings.Settings.Add("CloudName", IsGSTEnabled);
            }

            if (ConfigurationManager.AppSettings.AllKeys.Contains("ApiKey") == false)
            {
                string IsGSTEnabled = Convert.ToString(jsonObj["ApiKey"]);
                config.AppSettings.Settings.Add("ApiKey", IsGSTEnabled);
            }

            if (ConfigurationManager.AppSettings.AllKeys.Contains("ApiSecret") == false)
            {
                string IsGSTEnabled = Convert.ToString(jsonObj["ApiSecret"]);
                config.AppSettings.Settings.Add("ApiSecret", IsGSTEnabled);
            }

            if (ConfigurationManager.AppSettings.AllKeys.Contains("ApiBaseAddress") == false)
            {
                string IsGSTEnabled = Convert.ToString(jsonObj["ApiBaseAddress"]);
                config.AppSettings.Settings.Add("ApiBaseAddress", IsGSTEnabled);
            }

            if (ConfigurationManager.AppSettings.AllKeys.Contains("LinkAddress") == false)
            {
                string IsGSTEnabled = Convert.ToString(jsonObj["LinkAddress"]);
                config.AppSettings.Settings.Add("LinkAddress", IsGSTEnabled);
            }

            if (ConfigurationManager.AppSettings.AllKeys.Contains("DestinationSiteDirectoryPath_1") == false)
            {
                string IsGSTEnabled = Convert.ToString(jsonObj["DestinationSiteDirectoryPath_1"]);
                config.AppSettings.Settings.Add("DestinationSiteDirectoryPath_1", IsGSTEnabled);
            }

            if (ConfigurationManager.AppSettings.AllKeys.Contains("DefaultCanvasWidth") == false)
            {
                string defaultCanvasWidth = Convert.ToString(jsonObj["DefaultCanvasWidth"]);
                config.AppSettings.Settings.Add("DefaultCanvasWidth", defaultCanvasWidth);
            }

            if (ConfigurationManager.AppSettings.AllKeys.Contains("DefaultCanvasHeight") == false)
            {
                string defaultCanvasHeight = Convert.ToString(jsonObj["DefaultCanvasHeight"]);
                config.AppSettings.Settings.Add("DefaultCanvasHeight", defaultCanvasHeight);
            }

            if (ConfigurationManager.AppSettings.AllKeys.Contains("CloudinaryOrderImagesPath") == false)
            {
                string IsGSTEnabled = Convert.ToString(jsonObj["CloudinaryOrderImagesPath"]);
                config.AppSettings.Settings.Add("CloudinaryOrderImagesPath", IsGSTEnabled);
            }

            if (ConfigurationManager.AppSettings.AllKeys.Contains("CloudinaryImagesBufferSize") == false)
            {
                string IsGSTEnabled = Convert.ToString(jsonObj["CloudinaryImagesBufferSize"]);
                config.AppSettings.Settings.Add("CloudinaryImagesBufferSize", IsGSTEnabled);
            }

            if (ConfigurationManager.AppSettings.AllKeys.Contains("CloudinaryVideosBufferSize") == false)
            {
                string IsGSTEnabled = Convert.ToString(jsonObj["CloudinaryVideosBufferSize"]);
                config.AppSettings.Settings.Add("CloudinaryVideosBufferSize", IsGSTEnabled);
            }

            if (ConfigurationManager.AppSettings.AllKeys.Contains("WeChatPortal") == false)
            {
                string IsGSTEnabled = Convert.ToString(jsonObj["WeChatPortal"]);
                config.AppSettings.Settings.Add("WeChatPortal", IsGSTEnabled);
            }

            if (ConfigurationManager.AppSettings.AllKeys.Contains("isWhatsAppCloudUpload") == false)
            {
                string IsGSTEnabled = Convert.ToString(jsonObj["isWhatsAppCloudUpload"]);
                config.AppSettings.Settings.Add("isWhatsAppCloudUpload", IsGSTEnabled);
            }

            //END For We chat, whats app 

            if (ConfigurationManager.AppSettings.AllKeys.Contains("ProductExtentionCode") == false)
            {
                string isProductExtentionCode = Convert.ToString(jsonObj["ProductExtentionCode"]);
                config.AppSettings.Settings.Add("ProductExtentionCode", isProductExtentionCode);
            }

            if (ConfigurationManager.AppSettings.AllKeys.Contains("IsProductCodeWithBarCode") == false)
            {
                string isPCodeWithBarCode = "false";//Convert.ToString(jsonObj["IsProductCodeWithBarCode"]);
                config.AppSettings.Settings.Add("IsProductCodeWithBarCode", isPCodeWithBarCode);
            }

            if (ConfigurationManager.AppSettings.AllKeys.Contains("ISSGSTCGST") == false)
            {
                string iSSGSTCGST = "false";//Convert.ToString(jsonObj["ISSGSTCGST"]);
                config.AppSettings.Settings.Add("ISSGSTCGST", iSSGSTCGST);
            }

            if (ConfigurationManager.AppSettings.AllKeys.Contains("Arabic") == true)
            {
                config.AppSettings.Settings.Remove("Arabic");
            }

            if (ConfigurationManager.AppSettings.AllKeys.Contains("IsArabic") == false)
            {
                string isArabicVal = "0";
                config.AppSettings.Settings.Add("IsArabic", isArabicVal);
            }

            if (ConfigurationManager.AppSettings.AllKeys.Contains("QualityLevel") == false)
            {
                string QualityLevel = "100";
                config.AppSettings.Settings.Add("QualityLevel", QualityLevel);
            }

            if (ConfigurationManager.AppSettings.AllKeys.Contains("ServiceEmail") == false)
            {
                string ServiceEmail = Convert.ToString(jsonObj["ServiceEmail"]);
                config.AppSettings.Settings.Add("ServiceEmail", ServiceEmail);

            }

            if (ConfigurationManager.AppSettings.AllKeys.Contains("ScaleTransformValue") == false)
            {
                config.AppSettings.Settings.Add("ScaleTransformValue", "1.6");
            }
            if (ConfigurationManager.AppSettings.AllKeys.Contains("isSBPreviewEnabledWithGuest") == false)
            {
                config.AppSettings.Settings.Add("isSBPreviewEnabledWithGuest", "true");
            }
            if (ConfigurationManager.AppSettings.AllKeys.Contains("China") == false)
            {
                config.AppSettings.Settings.Add("China", "China");
            }
            if (ConfigurationManager.AppSettings.AllKeys.Contains("IsChina") == false)
            {
                config.AppSettings.Settings.Add("IsChina", "1");
            }
            if (ConfigurationManager.AppSettings.AllKeys.Contains("LicenseExpireInMonths") == false)
            {
                string LicenseExpireInMonths = Convert.ToString(jsonObj["LicenseExpireInMonths"]);
                config.AppSettings.Settings.Add("LicenseExpireInMonths", LicenseExpireInMonths);
            }
            if (ConfigurationManager.AppSettings.AllKeys.Contains("IsSplashScreenActive") == false)
            {
                string LicenseExpireInMonths = Convert.ToString(jsonObj["IsSplashScreenActive"]);
                config.AppSettings.Settings.Add("IsSplashScreenActive", "1");
            }

            // Save the changes in App.config file.
            config.Save(ConfigurationSaveMode.Full);
            ConfigurationManager.RefreshSection("appSettings");


            #endregion

            if (_objdata != null)
            {
                string HotFolderPathWithDate = _objdata.DG_Hot_Folder_Path + "\\" + DateTime.Now.ToString("yyyyMMdd");
                if (!Directory.Exists(HotFolderPathWithDate))
                    Directory.CreateDirectory(HotFolderPathWithDate);

                string printimagespath = _objdata.DG_Hot_Folder_Path + "\\PrintImages";
                string emailimages = _objdata.DG_Hot_Folder_Path + "\\PrintImages" + "\\Email";
                string CDUSBimages = Environment.CurrentDirectory + "\\DigiOrderdImages";
                string WIFIdownload = _objdata.DG_Hot_Folder_Path + "\\Archived";
                //string WIFIdownload = _objdata.DG_Hot_Folder_Path + "\\Download";
                string RideCameraMain = _objdata.DG_Hot_Folder_Path + "\\Camera\\RideCamera";
                string Mainhotfolder = _objdata.DG_Hot_Folder_Path;
                string thumbnails = _objdata.DG_Hot_Folder_Path + "\\Thumbnails\\";
                string thumbnailsbig = _objdata.DG_Hot_Folder_Path + "\\Thumbnails_Big\\";
                //string greenimg = _objdata.DG_Hot_Folder_Path + "\\GreenImage\\";
                string cropimg = _objdata.DG_Hot_Folder_Path + "\\Croped\\";
                string editedImages = Path.Combine(_objdata.DG_Hot_Folder_Path, "EditedImages");
                string thumbnailsTemp = _objdata.DG_Hot_Folder_Path + "\\Thumbnails\\Temp\\";
                string processedMobileTags = _objdata.DG_Hot_Folder_Path + "\\MobileTags\\Processed\\";

                int i = 1;
                ErrorHandler.ErrorHandler.LogFileWrite("No of days  :- " + olderthandays.ToString());
                List<string> hotfolderimages = new List<string>();
                hotfolderimages = GetImagesToDelete(Mainhotfolder, olderthandays);
                ErrorHandler.ErrorHandler.LogFileWrite("Hot Folder Count :- " + hotfolderimages.Count().ToString());
                if (hotfolderimages.Count > 0)
                {

                    foreach (var img in hotfolderimages)
                    {
                        // ErrorHandler.ErrorHandler.LogFileWrite("Hot Folder Count :- " + hotfolderimages.Count().ToString());
                        FileInfo fi = new FileInfo(img);
                        try
                        {
                            PhotoBusiness phBusiness = new PhotoBusiness();
                            phBusiness.SetArchiveDetails(fi.Name);
                            ErrorHandler.ErrorHandler.LogFileWrite("f1 name :- " + fi.Name.ToString());
                            fi.Delete();
                            fi = new FileInfo(thumbnails + img);
                            fi.Delete();
                            fi = new FileInfo(thumbnailsbig + img);
                            fi.Delete();
                            //fi = new FileInfo(greenimg + img);
                            //fi.Delete();
                            fi = new FileInfo(cropimg + img);
                            fi.Delete();
                            //MessageListener.Instance.ReceiveMessage(string.Format("Cleaning Hotfolder: {0}", i));
                            Thread.Sleep(1);
                        }
                        catch
                        {
                            ErrorHandler.ErrorHandler.LogFileWrite("In use : -" + fi.Name.ToString());
                            continue;
                        }
                        i++;
                    }

                }

                if (Directory.Exists(editedImages))
                {
                    DirectoryInfo directory = new DirectoryInfo(editedImages);
                    //ErrorHandler.ErrorHandler.LogFileWrite("CleanUpdaysEditedImg :- " + iMIXCleanUpDays.ToString());
                    //delete files:
                    directory.GetFiles().ToList().Where(t => t.CreationTime < DateTime.Now.AddDays(-iMIXCleanUpDays)).ToList().ForEach(f => f.Delete());
                }
                //Delete processed mobile tags
                if (Directory.Exists(processedMobileTags))
                {
                    int deleteddayleft = ConfigurationManager.AppSettings["DeletedProcessedMobileTags"].ToInt32();
                    DirectoryInfo directory = new DirectoryInfo(processedMobileTags);

                    //delete files:
                    //   directory.GetFiles().ToList().Where(t => t.CreationTime < DateTime.Now.AddDays(-2)).ToList().ForEach(f => f.Delete());
                    directory.GetFiles().ToList().Where(t => t.CreationTime < DateTime.Now.AddDays(-deleteddayleft)).ToList().ForEach(f => f.Delete());
                }
                i = 1;
                if (Directory.Exists(printimagespath))
                {
                    string[] filesprintimages = Directory.GetFiles(printimagespath);
                    ErrorHandler.ErrorHandler.LogFileWrite("PrintImages Count :- " + filesprintimages.Count().ToString());
                    if (filesprintimages.Count() > 0)
                    {
                        foreach (string file in filesprintimages)
                        {
                            FileInfo fi = new FileInfo(file);
                            //On request of Faraz changed from 3 days to 1 day. It will reduce folder read and write time. date:1 Nov 2014
                            if (fi.CreationTime < DateTime.Now.AddDays(-iMIXCleanUpDays))
                            {
                                try
                                {
                                    fi.Delete();
                                }
                                catch (Exception ex)
                                {

                                }

                                //MessageListener.Instance.ReceiveMessage(string.Format("Cleaning PrintImages: {0}", i));
                                Thread.Sleep(1);
                            }
                            i++;
                        }
                    }
                    string[] foldersinPrintimages = Directory.GetDirectories(printimagespath);
                    foreach (string dir in foldersinPrintimages)
                    {
                        DirectoryInfo di = new DirectoryInfo(dir);

                        if (di.CreationTime < DateTime.Now.AddDays(-iMIXCleanUpDays))
                        {
                            try
                            {
                                if (di.Name != "Email")
                                {
                                    Directory.Delete(dir, true);
                                }
                            }
                            catch (Exception ex)
                            {

                            }
                            //MessageListener.Instance.ReceiveMessage(string.Format("Cleaning PrintImages: {0}", i));
                            Thread.Sleep(1);
                        }
                        i++;
                    }
                }
                i = 1;
                if (Directory.Exists(emailimages))
                {
                    string[] filesinEmail = Directory.GetDirectories(emailimages);
                    foreach (string dir in filesinEmail)
                    {
                        DirectoryInfo di = new DirectoryInfo(dir);
                        if (di.CreationTime < DateTime.Now.AddDays(-iMIXCleanUpDays))
                        {
                            try
                            {
                                Directory.Delete(dir, true);
                            }
                            catch (Exception ex)
                            {

                            }
                            //MessageListener.Instance.ReceiveMessage(string.Format("Cleaning Email: {0}", i));
                            Thread.Sleep(1);
                        }
                        i++;
                    }
                }
                i = 1;
                if (Directory.Exists(CDUSBimages))
                {
                    string[] filesinCDUSB = Directory.GetFiles(CDUSBimages);
                    foreach (string file in filesinCDUSB)
                    {
                        FileInfo fi = new FileInfo(file);
                        if (fi.CreationTime < DateTime.Now.AddDays(-iMIXCleanUpDays))
                        {
                            try
                            {
                                fi.Delete();
                            }
                            catch (Exception ex)
                            {

                            }
                            //MessageListener.Instance.ReceiveMessage(string.Format("Cleaning CD/USB: {0}", i));
                            Thread.Sleep(1);
                        }
                        i++;
                    }
                }
                i = 1;


                if (Directory.Exists(RideCameraMain))
                {
                    DirectoryInfo _objdir = new DirectoryInfo(RideCameraMain);

                    if (_objdir.Exists)
                    {
                        IEnumerable<FileInfo> files = _objdir.EnumerateFiles("*.jpg", SearchOption.TopDirectoryOnly);
                        files = files.Where(t => t.CreationTime < DateTime.Now.AddDays(-tripCamCleanUpDays));
                        if (files.Count() > 36)
                        {
                            foreach (var file in files.OrderBy(t => t.CreationTime).Take(files.Count() - 36))
                            {
                                try
                                {
                                    file.Delete();
                                }
                                catch (Exception ex)
                                {
                                }
                                //MessageListener.Instance.ReceiveMessage(string.Format("Cleaning RideCameraMain: {0}", i));
                                Thread.Sleep(1);
                                i++;
                            }
                        }
                    }
                }
                i = 1;
                if (mylistCam != null)
                {
                    foreach (var dir in mylistCam)
                    {
                        DirectoryInfo _objdir = new DirectoryInfo(_objdata.DG_Hot_Folder_Path + "\\Camera\\" + dir);
                        if (_objdir.Exists)
                        {
                            IEnumerable<FileInfo> files = _objdir.EnumerateFiles("*.jpg", SearchOption.TopDirectoryOnly);
                            files = files.Where(t => t.CreationTime < DateTime.Now.AddDays(-tripCamCleanUpDays));
                            if (files.Count() > 36)
                            {
                                foreach (var file in files.OrderBy(t => t.CreationTime).Take(files.Count() - 36))
                                {
                                    try
                                    {
                                        file.Delete();
                                    }
                                    catch (Exception ex)
                                    {

                                    }
                                    //MessageListener.Instance.ReceiveMessage(string.Format("Cleaning" + dir + " " + i));
                                    Thread.Sleep(1);
                                    i++;
                                }
                            }
                        }
                    }
                }

                i = 1;
                if (Directory.Exists(thumbnailsTemp))
                {
                    string[] filesinthumbnailsTemp = Directory.GetFiles(thumbnailsTemp);
                    foreach (string file in filesinthumbnailsTemp)
                    {
                        FileInfo fi = new FileInfo(file);
                        if (fi.CreationTime < DateTime.Now.AddDays(-iMIXCleanUpDays))
                        {
                            try
                            {
                                fi.Delete();
                            }
                            catch (Exception ex)
                            {

                            }
                            Thread.Sleep(1);
                        }
                        i++;
                    }
                }

                //int olderthandays2 = ConfigurationManager.AppSettings["DeletedOldImages"].ToInt32();
                int deletedOldImagesValue = (new ConfigBusiness()).GetDeletedOldImagesConfigurationData("DeletedOldImages"); //Abhishek
                int olderthandays2 = deletedOldImagesValue;
                //Console.WriteLine("DeletedOldImages =" + olderthandays2);
                PhotoBusiness photoObj = new PhotoBusiness();
                photoObj.TruncatePhotoGroupTablefordate(olderthandays, LoginUser.SubStoreId);
                //objdbLayer.TruncatePhotoGroupTablefordate();
            }
            StartWindowService("FontCache3.0.0.0");
        }




        private void GetStoreConfigData()
        {
            ConfigBusiness conBiz = new ConfigBusiness();
            List<iMIXStoreConfigurationInfo> ConfigValuesList = conBiz.GetStoreConfigData();
            for (int i = 0; i < ConfigValuesList.Count; i++)
            {
                switch (ConfigValuesList[i].IMIXConfigurationMasterId)
                {
                    case (int)ConfigParams.TripCamCleanupDays:
                        tripCamCleanUpDays = ConfigValuesList[i].ConfigurationValue != null ? ConfigValuesList[i].ConfigurationValue.ToInt16() : 0;
                        break;
                    case (int)ConfigParams.ImixCleanUpDays:
                        iMIXCleanUpDays = ConfigValuesList[i].ConfigurationValue != null ? ConfigValuesList[i].ConfigurationValue.ToInt16() : 0;
                        break;
                }
            }
        }
        public List<string> GetImagesToDelete(string hotfolderpath, int days)
        {
            List<string> filesname = new List<string>();
            try
            {
                string DateFolderPath = hotfolderpath + "\\" + DateTime.Now.AddDays(-days).ToString("yyyyMMdd");
                // ErrorHandler.ErrorHandler.LogFileWrite("DateFolderPAth  :- " + DateFolderPath.ToString());
                int NoOfOldDays = days;
                // ErrorHandler.ErrorHandler.LogFileWrite("NoOfOldDays  :- " + NoOfOldDays.ToString());
                //while (Directory.Exists(DateFolderPath))
                while (true)
                {
                    if (DateFolderPath == DateTime.Now.AddDays(-NoOfOldDays - 30).ToString("yyyyMMdd"))//
                    {
                        return filesname;
                    }
                    if (Directory.Exists(DateFolderPath))
                    {
                        string[] files = Directory.GetFiles(DateFolderPath);
                        foreach (string file in files)
                        {
                            FileInfo fi = new FileInfo(file);
                            if (fi.CreationTime < DateTime.Now.AddDays(-days))
                            {
                                if (fi.Name != "Locked.png")
                                {
                                    filesname.Add(fi.FullName);
                                    ErrorHandler.ErrorHandler.LogFileWrite("File to be deleted:" + fi.Name + " File creation date:" + fi.CreationTime);
                                }
                            }
                        }
                        NoOfOldDays = NoOfOldDays + 1;
                        DateFolderPath = hotfolderpath + "\\" + DateTime.Now.AddDays(-NoOfOldDays).ToString("yyyyMMdd");
                        //ErrorHandler.ErrorHandler.LogFileWrite("DateFolderPAth1  :- " + DateFolderPath.ToString());
                    }
                    else
                    {
                        NoOfOldDays = NoOfOldDays + 1;
                        DateFolderPath = hotfolderpath + "\\" + DateTime.Now.AddDays(-NoOfOldDays).ToString("yyyyMMdd");
                        // ErrorHandler.ErrorHandler.LogFileWrite("DateFolderPAth1  :- " + DateFolderPath.ToString());
                    }
                    if (NoOfOldDays == 180)//exit the loop on reaching max 180 days
                    {
                        break;
                    }
                }
                return filesname;
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                return filesname;
            }
        }
        private void Application_Exit(object sender, ExitEventArgs e)
        {
            try
            {
                //int count = 0;
                object SystemName = "";
                string hostName = Dns.GetHostName(); // Retrive the Name of HOST
                string myIP = Dns.GetHostByName(hostName).AddressList[0].ToString();//ip              
                ManagementObjectSearcher searcher =
                    new ManagementObjectSearcher("root\\CIMV2",
                    "SELECT Name FROM Win32_ComputerSystem");

                foreach (ManagementObject queryObj in searcher.Get())
                {
                    SystemName = queryObj["Name"];
                }
                ManagementObjectSearcher search = new ManagementObjectSearcher("SELECT * FROM Win32_NetworkAdapterConfiguration where IPEnabled=true");
                IEnumerable<ManagementObject> objects = search.Get().Cast<ManagementObject>();
                string mac = (from o in objects orderby o["IPConnectionMetric"] select o["MACAddress"].ToString()).FirstOrDefault();
                string systemname = SystemName.ToString();
                ServicePosInfoBusiness servicePosInfoBusiness = new ServicePosInfoBusiness();
                servicePosInfoBusiness.StartStopSystemBusiness(mac, myIP, systemname, 0);
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }

        }

        #region Ctrl+Alt+Tab Disable code_31Jan2018 by Ajay Sinha


        private delegate IntPtr LowLevelKeyboardProcCAT(int nCode, IntPtr wParam, IntPtr lParam);
        [DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        private static extern IntPtr SetWindowsHookEx(int id, LowLevelKeyboardProcCAT callback, IntPtr hMod, uint dwThreadId);
        [DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        private static extern bool UnhookWindowsHookExCAT(IntPtr hook);
        [DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        private static extern IntPtr CallNextHookEx(IntPtr hook, int nCode, IntPtr wp, IntPtr lp);
        [DllImport("kernel32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        private static extern IntPtr GetModuleHandle(string name);
        [DllImport("user32.dll", CharSet = CharSet.Auto)]
        private static extern short GetAsyncKeyState(System.Windows.Forms.Keys key);
        private IntPtr ptrHook;
        private LowLevelKeyboardProcCAT objKeyboardProcess;

        bool HasAltModifier(int flags)
        {
            return (flags & 0x20) == 0x20;
        }
        private IntPtr captureKey(int nCode, IntPtr wp, IntPtr lp)
        {
            if (nCode >= 0)
            {
                KBDLLHOOKSTRUCT objKeyInfo = (KBDLLHOOKSTRUCT)Marshal.PtrToStructure(lp, typeof(KBDLLHOOKSTRUCT));
                if (objKeyInfo.key == System.Windows.Forms.Keys.LWin ||
                    objKeyInfo.key == System.Windows.Forms.Keys.Tab && (System.Windows.Forms.Control.ModifierKeys & System.Windows.Forms.Keys.Alt) == System.Windows.Forms.Keys.Alt ||
                    objKeyInfo.key == System.Windows.Forms.Keys.Tab && (System.Windows.Forms.Control.ModifierKeys & System.Windows.Forms.Keys.Alt) == System.Windows.Forms.Keys.Alt && (System.Windows.Forms.Control.ModifierKeys & System.Windows.Forms.Keys.Control) == System.Windows.Forms.Keys.Control || // Disabling Windows keys||
                    objKeyInfo.key == System.Windows.Forms.Keys.Delete && (System.Windows.Forms.Control.ModifierKeys & System.Windows.Forms.Keys.Alt) == System.Windows.Forms.Keys.Alt && (System.Windows.Forms.Control.ModifierKeys & System.Windows.Forms.Keys.Control) == System.Windows.Forms.Keys.Control ||
                    objKeyInfo.key == System.Windows.Forms.Keys.Space && (System.Windows.Forms.Control.ModifierKeys & System.Windows.Forms.Keys.Alt) == System.Windows.Forms.Keys.Alt ||
                    objKeyInfo.key == System.Windows.Forms.Keys.Space && (System.Windows.Forms.Control.ModifierKeys & System.Windows.Forms.Keys.Alt) == System.Windows.Forms.Keys.Alt && (System.Windows.Forms.Control.ModifierKeys & System.Windows.Forms.Keys.Control) == System.Windows.Forms.Keys.Control ||
                    objKeyInfo.key == System.Windows.Forms.Keys.LWin && (System.Windows.Forms.Control.ModifierKeys & System.Windows.Forms.Keys.Space) == System.Windows.Forms.Keys.Space) // Disabling Windows keys
                {
                    //  MessageBox.Show("45"+objKeyInfo.key.ToString());
                    return (IntPtr)1;
                }
            }
            return CallNextHookEx(ptrHook, nCode, wp, lp);
        }


        #endregion

        //private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        //{


        //}/// <summary>
        /// This method is for locking the application.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void DigiStartup(object sender, EventArgs e)
        {
            try
            {
                #region Ctrl+Alt+Tab Disable code_31Jan2018 by Ajay Sinha

                //ProcessModule objCurrentModule = Process.GetCurrentProcess().MainModule;
                //objKeyboardProcess = new LowLevelKeyboardProcCAT(captureKey);
                //ptrHook = SetWindowsHookEx(13, objKeyboardProcess, GetModuleHandle(objCurrentModule.ModuleName), 0);

                #endregion

                // Get Reference to the current Process
                Process thisProc = Process.GetCurrentProcess();
                // Check how many total processes have the same name as the current one
                if (Process.GetProcessesByName(thisProc.ProcessName).Length > 1)
                {
                    // If there is more than one, then it is already running.
                    MessageBox.Show("Digiphoto is already running.");
                    Application.Current.Shutdown();
                    return;
                }
                //#endif
                ErrorHandler.ErrorHandler.DeleteLog();

                //TBD
                CustomBusineses cusObj = new CustomBusineses();
                cusObj.setServicePathForApplication(System.IO.Directory.GetCurrentDirectory());

                //DigiPhotoDataServices _objdblayer = new DigiPhotoDataServices();
                //_objdblayer.setServicePathForApplication(System.IO.Directory.GetCurrentDirectory());

                ////Modified by Nirjhar on 25 May 2014
#if !DEBUG
                Taskbar.Hide();
                EnableDisableTaskManager(false);
                KeyboardHook(this, e);
#endif
                StartupUri = new System.Uri("Login.xaml", UriKind.Relative);
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }

        }
        private static bool RegisterSystem()
        {
            bool output = false;
            try
            {
                /// string connnectionName = ConfigurationManager.ConnectionStrings["DigiConnectionString"].ConnectionString;

                int Count = 0;
                object SystemName = "";
                string hostName = Dns.GetHostName(); // Retrive the Name of HOST
                string myIP = Dns.GetHostByName(hostName).AddressList[0].ToString();//ip              
                ManagementObjectSearcher searcher =
                    new ManagementObjectSearcher("root\\CIMV2",
                    "SELECT Name FROM Win32_ComputerSystem");

                foreach (ManagementObject queryObj in searcher.Get())
                {
                    try
                    {
                        SystemName = queryObj["Name"];
                    }
                    catch { }
                }
                ManagementObjectSearcher search = new ManagementObjectSearcher("SELECT * FROM Win32_NetworkAdapterConfiguration where IPEnabled=true");
                string mac = string.Empty;
                IEnumerable<ManagementObject> objects = search.Get().Cast<ManagementObject>();
                try
                {
                    mac = (from o in objects orderby o["IPConnectionMetric"] select o["MACAddress"].ToString()).FirstOrDefault();
                }
                catch { }
                string SyncCode = CommonUtility.GetUniqueSynccode(Convert.ToInt32(ApplicationObjectEnum.PosDetail).ToString().PadLeft(2, '0'), LoginUser.countrycode, LoginUser.Storecode, "00");
                ImixPOSDetail imixposdetail = new ImixPOSDetail();
                imixposdetail.SystemName = SystemName.ToString();
                imixposdetail.IPAddress = myIP;
                imixposdetail.MacAddress = mac;
                imixposdetail.SubStoreID = LoginUser.SubStoreId;
                imixposdetail.IsActive = true;
                imixposdetail.CreatedBy = "webusers";
                imixposdetail.ImixPOSDetailID = 0;
                imixposdetail.IsStart = false;
                imixposdetail.SyncCode = SyncCode;
                output = InsertImixPosDetail(imixposdetail);
                //ServicePosInfoBusiness svcPosInfoBusiness = new ServicePosInfoBusiness();
                //Count = svcPosInfoBusiness.InsertImixPosBusiness(imixposdetail,conn);
                return output;
            }
            catch (Exception ex)
            {
                output = false;
                return output;
            }
        }
        public static bool InsertImixPosDetail(ImixPOSDetail imixposdetail)
        {
            bool isDBUpdated;
            try
            {
                string _connstr = System.Configuration.ConfigurationManager.ConnectionStrings["DigiConnectionString"].ConnectionString;
                using (SqlConnection cn = new SqlConnection(_connstr))
                {
                    SqlCommand cmd = new SqlCommand("USP_INSERTUPDATEIMIXPOSDETAIL", cn);

                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@iMixPOSDetailID", imixposdetail.ImixPOSDetailID);
                    cmd.Parameters.AddWithValue("@SystemName", imixposdetail.SystemName);
                    cmd.Parameters.AddWithValue("@IPAddress", imixposdetail.IPAddress);
                    cmd.Parameters.AddWithValue("@MacAddress", imixposdetail.MacAddress);
                    cmd.Parameters.AddWithValue("@SubStoreID", imixposdetail.SubStoreID);
                    cmd.Parameters.AddWithValue("@IsActive", imixposdetail.IsActive);
                    cmd.Parameters.AddWithValue("@CreatedBy", imixposdetail.CreatedBy);
                    cmd.Parameters.AddWithValue("@IsStart", imixposdetail.IsStart);
                    cmd.Parameters.AddWithValue("@SyncCode", imixposdetail.SyncCode);
                    cn.Open();
                    cmd.ExecuteNonQuery();
                    cn.Close();
                }
                isDBUpdated = true;
            }
            catch (Exception exp)
            {
                //log.Error("DBAction.UpdateStatusLocal: ", exp);
                isDBUpdated = false;
            }
            return isDBUpdated;
        }

        #region Update Checking Code
        public static string ExecutablePath
        {
            get
            {
                return System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
            }
        }
        /// <summary>
        /// Verify if the current build supports automatic version update
        /// </summary>
        /// <returns></returns>
        private bool verifyAutoUpdateEnabled()
        {
            bool isAutoUpdateEnabled = false;
            try
            {
                ConfigBusiness conObj = new ConfigBusiness();
                var isEnabled = conObj.GetNewConfigValues(LoginUser.SubStoreId).Where(o => o.IMIXConfigurationMasterId == Convert.ToInt32(ConfigParams.IsVersionUpdateEnabled)).FirstOrDefault();

                //DigiPhotoDataServices objDataLayer = new DataLayer.DigiPhotoDataServices();
                //var isEnabled = objDataLayer.GetNewConfigValues(LoginUser.SubStoreId).Where(o => o.IMIXConfigurationMasterId == Convert.ToInt32(ConfigParams.IsVersionUpdateEnabled)).FirstOrDefault();
                if (isEnabled != null)
                {
                    isAutoUpdateEnabled = Convert.ToBoolean(isEnabled.ConfigurationValue);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
            return isAutoUpdateEnabled;
        }
        /// <summary>
        /// Function to fetch the update service path from configuration section
        /// </summary>
        /// <returns>Update Service WCF path</returns>
        private string GetUpdateServicePath()
        {
            string ePath = string.Empty;
            //DigiPhotoDataServices objDataLayer = new DataLayer.DigiPhotoDataServices();
            //var address = objDataLayer.GetNewConfigValues(LoginUser.SubStoreId).Where(o => o.IMIXConfigurationMasterId == Convert.ToInt32(ConfigParams.UpdateServicePath)).FirstOrDefault();
            var address = (new ConfigBusiness()).GetNewConfigValues(LoginUser.SubStoreId).Where(o => o.IMIXConfigurationMasterId == Convert.ToInt32(ConfigParams.UpdateServicePath)).FirstOrDefault();
            if (address != null)
            {
                ePath = Convert.ToString(address.ConfigurationValue);
            }
            return ePath;
        }
        /// <summary>
        /// Parent Method for checking software updates on the application launch
        /// </summary>
        private void CheckUpdate()
        {
            string avUpd = string.Empty; string intrMesag = string.Empty; string avLocalUpdate = string.Empty;
            if (verifyAutoUpdateEnabled())
            {
                avLocalUpdate = CheckForLocalUpdates();
                string exePath = ExecutablePath + "\\DigiUpdates.exe";//Orginal path
                //string exePath = @"D:\Workstation\DigiPhoto\Releases\V2.0.0.3_Versioning\DigiPhotoEnhancedCode\DigiUpdates\bin\Debug\DigiUpdates.exe";//Testing path
                if (!string.IsNullOrEmpty(avLocalUpdate))
                {
                    //close current application and launch the download/Update exe
                    if (File.Exists(exePath))
                    {
                        avUpd = "New version of i-Mix " + avLocalUpdate + " is available! Would you like to download it now?";
                        MessageBoxResult messageBoxResult = System.Windows.MessageBox.Show(avUpd, "i-Mix (Local Update)", System.Windows.MessageBoxButton.YesNo, MessageBoxImage.Question);
                        if (messageBoxResult == MessageBoxResult.Yes)
                        {
                            Process.Start(exePath, "1");
                            //System.Windows.Application.Current.Shutdown();
                        }
                    }
                }
                else
                {
                    VersonViewModel[] lstTemp = VerifyUpdatesAtServer();
                    if (lstTemp != null && lstTemp.Count() > 0)
                    {
                        foreach (var item in lstTemp)
                        {
                            intrMesag = item.Major + "." + item.Minor + "." + item.Revision + "." + item.BuildNumber + ",";
                        }
                        if (intrMesag.EndsWith(","))
                        {
                            intrMesag = intrMesag.Substring(0, intrMesag.Length - 1);
                        }
                        avUpd = "New version of i-Mix " + intrMesag + " is available! Would you like to download it now?";
                    }
                    if (!String.IsNullOrEmpty(avUpd))
                    {
                        //launch the exe
                        MessageBoxResult messageBoxResult = System.Windows.MessageBox.Show(avUpd, "i-Mix (Server Update)", System.Windows.MessageBoxButton.YesNo, MessageBoxImage.Question);
                        if (messageBoxResult == MessageBoxResult.Yes)
                        {
                            if (File.Exists(exePath))
                            {

                                Process.Start(exePath, "2");
                                //System.Windows.Application.Current.Shutdown();
                            }
                        }
                    }
                }
            }
        }

        //for single store
        private VersonViewModel[] VerifyUpdatesAtServer()
        {

            StoreSubStoreDataBusniess stoObj = new StoreSubStoreDataBusniess();
            // DigiPhotoDataServices objDataLayer = new DataLayer.DigiPhotoDataServices();
            Hashtable htStoreInfo = stoObj.GetStoreDetails();
            string ipaddr = GetComputer_LanIP();
            string macaddr = GetMACAddress();
            string curReg = CurrentRegistryVersion();
            VersonViewModel viewModel = new VersonViewModel();
            viewModel.Country = htStoreInfo.ContainsKey("Country") ? htStoreInfo["Country"].ToString() : "";
            viewModel.Store = htStoreInfo.ContainsKey("Store") ? htStoreInfo["Store"].ToString() : "";
            viewModel.Description = "Test description";
            viewModel.MACAddress = macaddr;
            viewModel.IPAddress = ipaddr;
            viewModel.Location = "";
            viewModel.SubStore = "";
            viewModel.User = "admin";//as per the wcf service web.config
            viewModel.Password = "admin";//as per the wcf service web.config
            viewModel.Version = curReg;
            VersonViewModel[] lstTemp = VerifyUpdateService(viewModel);
            return lstTemp;
        }
        /// <summary>
        /// Get all the updates available for the given store
        /// </summary>
        /// <param name="viewModel"></param>
        /// <returns></returns>
        private VersonViewModel[] VerifyUpdateService(VersonViewModel viewModel)
        {
            string avUpd = ""; string intrMesag = "";
            try
            {
                string epAdd = GetUpdateServicePath();
                VersionServiceClient client = new VersionServiceClient("BasicHttpBinding_IVersionService", epAdd);
                VersonViewModel[] lstTemp = client.CheckAvailableUpdates(viewModel);
                return lstTemp;
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                return null;
            }

        }


        /// <summary>
        /// Get computer LAN address like 192.168.1.3
        /// </summary>
        /// <returns></returns>
        private string GetComputer_LanIP()
        {
            string strHostName = System.Net.Dns.GetHostName();

            IPHostEntry ipEntry = System.Net.Dns.GetHostEntry(strHostName);

            foreach (IPAddress ipAddress in ipEntry.AddressList)
            {
                if (ipAddress.AddressFamily.ToString() == "InterNetwork")
                {
                    return ipAddress.ToString();
                }
            }
            return "-";
        }
        /// <summary>
        /// Get local system machine address (MAC address)
        /// </summary>
        /// <returns></returns>
        public string GetMACAddress()
        {
            NetworkInterface[] nics = NetworkInterface.GetAllNetworkInterfaces();
            String sMacAddress = string.Empty;
            foreach (NetworkInterface adapter in nics)
            {
                if (sMacAddress == String.Empty)// only return MAC Address from first card  
                {
                    IPInterfaceProperties properties = adapter.GetIPProperties();
                    sMacAddress = adapter.GetPhysicalAddress().ToString();
                }
            }
            return sMacAddress;
        }
        /// <summary>
        /// If an update is available locally
        /// </summary>
        /// <returns></returns>
        private string CheckForLocalUpdates()
        {
            string avUpdate = string.Empty;
            string currRegistry = CurrentRegistryVersion();
            //TBD:Sanchit
            //DigiPhotoDataServices objDataLayer = new DataLayer.DigiPhotoDataServices();
            //avUpdate = objDataLayer.IsUpdateAvailable(currRegistry);

            avUpdate = (new AppStartUpBusiness()).IsUpdateAvailable(currRegistry);
            return avUpdate;
        }
        /// <summary>
        /// Gets the current version of Digiphoto Assembly
        /// </summary>
        /// <returns></returns>
        private string CurrentRegistryVersion()
        {
            ModifyRegistry mr = new ModifyRegistry();
            string currRegistry = mr.Read("InstallVersion");
            return currRegistry;
        }
        #endregion Update Checking Code

        #region Start Window Service
        /// <summary>
        /// Start the given window service if current status is stopped
        /// </summary>
        /// <param name="servicename"></param>
        private void StartWindowService(string servicename)
        {
            try
            {
                ServiceController myService = new ServiceController();
                myService.ServiceName = servicename;
                myService.Refresh();
                string svcStatus = myService.Status.ToString();
                if (svcStatus == "Stopped")
                {
                    myService.Start();
                }

            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }
        #endregion Start Window Service

        #region Common Methods For Locking
        private static void EnableDisableTaskManager(bool enable)
        {
            Microsoft.Win32.RegistryKey HKCU = Microsoft.Win32.Registry.CurrentUser;
            Microsoft.Win32.RegistryKey key = HKCU.CreateSubKey(@"Software\Microsoft\Windows\CurrentVersion\Policies\System");
            key.SetValue("DisableTaskMgr", enable ? 0 : 1, Microsoft.Win32.RegistryValueKind.DWord);
        }

        private int LowLevelKeyboardProc(int nCode, int wParam, ref KBDLLHOOKSTRUCT lParam)
        {
            bool blnEat = false;
            switch (wParam)
            {
                case 256:
                case 257:
                case 260:
                case 261:
                    //Alt+Tab, Alt+Esc, Ctrl+Esc, Windows Key
                    if (((lParam.vkCode == 9) && (lParam.flags == 32)) ||
                    ((lParam.vkCode == 27) && (lParam.flags == 32)) || ((lParam.vkCode ==
                    27) && (lParam.flags == 0)) || ((lParam.vkCode == 91) && (lParam.flags
                    == 1)) || ((lParam.vkCode == 92) && (lParam.flags == 1)) || ((true) &&
                    (lParam.flags == 32)))
                    {
                        blnEat = true;
                    }
                    break;
            }
            try
            {
                if (blnEat)
                    return 1;
                else
                {
                    try
                    {
                        int Hook = CallNextHookEx(0, nCode, wParam, ref lParam);
                        return Hook;
                    }
                    catch (Exception ex)
                    {
                        string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                        ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                        return 1;
                    }
                    return 1;
                }
            }
            catch (Exception ex)
            {
                return 1;
            }

        }
        static LowLevelKeyboardProcDelegate mHookProc;
        private void KeyboardHook(object sender, EventArgs e)
        {
            // NOTE: ensure delegate can't be garbage collected
            mHookProc = new LowLevelKeyboardProcDelegate(LowLevelKeyboardProc);
            // Get handle to main .exe
            IntPtr hModule = GetModuleHandle(IntPtr.Zero);
            // Hook
            intLLKey = SetWindowsHookEx(WH_KEYBOARD_LL, mHookProc, hModule, 0);
            if (intLLKey == IntPtr.Zero)
            {
                MessageBox.Show("Failed to set hook,error = " + Marshal.GetLastWin32Error().ToString());

            }

        }

        private void ReleaseKeyboardHook()
        {
            intLLKey = UnhookWindowsHookEx(intLLKey);
        }
        #endregion



        private void LoadMD3Files()
        {
            var _managers = new List<NikonManager>();
            string[] md3s = Directory.GetFiles(Directory.GetCurrentDirectory(), "*.md3", SearchOption.AllDirectories);

            //string[] md3s = Directory.GetFiles(@"C:\Program Files (x86)\iMix", "*.md3", SearchOption.AllDirectories);

            //WriteToFile("LoadMD3Files Started Line 1110 -------" + md3s);
            if (md3s.Length == 0)
            {
                //WriteToFile("LoadMD3Files md3s.Length == 0 Line 1107 -------" + md3s.Length);
                ErrorHandler.ErrorHandler.LogFileWrite("Couldn't find any MD3 files in " + Directory.GetCurrentDirectory());
                ErrorHandler.ErrorHandler.LogFileWrite("Download MD3 files from Nikons SDK website: https://sdk.nikonimaging.com/apply/");
            }

            foreach (string md3 in md3s)
            {
                const string requiredDllFile = "NkdPTP.dll";

                string requiredDllPath = System.IO.Path.Combine(System.IO.Path.GetDirectoryName(md3), requiredDllFile);
                //WriteToFile("LoadMD3Files requiredDllPath Line 1123 -------" + requiredDllPath + "(md3): " + md3);
                if (!File.Exists(requiredDllPath))
                {
                    //WriteToFile("LoadMD3Files requiredDllPath Line 1120 -------" + requiredDllPath);
                    ErrorHandler.ErrorHandler.LogFileWrite("Warning: Couldn't find " + requiredDllFile + " in " + System.IO.Path.GetDirectoryName(md3) + ". The library will not work properly without it!");
                }
                ErrorHandler.ErrorHandler.LogFileWrite("Opening " + md3);
                //WriteToFile("LoadMD3Files Opening Line 1124 -------" + md3);
                NikonManager manager = null;
                try
                {
                    manager = new NikonManager(md3);

                }
                catch (Exception ex)
                {
                    manager = null;

                }
                if (manager != null)
                {
                    manager.DeviceAdded += new DeviceAddedDelegate(_manager_DeviceAdded);
                    manager.DeviceRemoved += new DeviceRemovedDelegate(_manager_DeviceRemoved);
                }

            }
        }

        void _manager_DeviceRemoved(NikonManager sender, NikonDevice device)
        {
            IsCameraConnected = false;
            CameraSerialNumber = string.Empty;
            ModelNumber = string.Empty;
        }
        void _manager_DeviceAdded(NikonManager sender, NikonDevice device)
        {
            try
            {
                IsCameraConnected = true;
                ModelNumber = device.GetString(eNkMAIDCapability.kNkMAIDCapability_Name);
                FetchSerialNo();
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);



            }
        }
        void FetchSerialNo()
        {
            StringBuilder sb = new StringBuilder();
            try
            {
                ManagementObjectSearcher searcher =
                    new ManagementObjectSearcher("root\\CIMV2",
                    "SELECT * FROM Win32_PnPEntity");
                foreach (ManagementObject queryObj in searcher.Get())
                {
                    var caption = queryObj["Caption"];
                    if (caption != null && caption.ToString().Trim().ToUpper().Contains(ModelNumber.ToUpper()))
                    {
                        var deviceDetails = queryObj["PNPDeviceID"].ToString();
                        string[] arr = new string[] { @"\" };
                        var arrDetails = deviceDetails.Split(arr, StringSplitOptions.None);
                        CameraSerialNumber = arrDetails[2];
                        return;
                    }
                }
            }
            catch (ManagementException e)
            {
                MessageBox.Show("An error occurred while querying for WMI data: " + e.Message);
            }

        }
        public bool IsCameraConnected
        {
            get;
            set;
        }
        public string CameraSerialNumber
        {
            get; set;
        }
        public string ModelNumber
        {
            get;
            set;
        }
    }
}
