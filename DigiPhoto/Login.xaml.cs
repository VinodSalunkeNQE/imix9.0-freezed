﻿using System;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using DigiPhoto.Common;
using DigiPhoto.DataLayer;
using DigiAuditLogger;
using FrameworkHelper.Common;
using DigiPhoto.IMIX.Business;
using DigiPhoto.IMIX.Model;
using System.Windows.Threading;
using Digiphoto.Cache.Cache;
using DigiPhoto.Manage;
using System.Runtime.InteropServices;

namespace DigiPhoto
{
    /// <summary>
    /// Interaction logic for Login.xaml
    /// </summary>
    public partial class Login : Window
    {

        /// <summary>
        /// The count attempt
        /// </summary>
        private int _countAttempt = 0;
        private delegate void LoadTabDelegate();
        private delegate void ClearCacheDelegate();
        private bool IsCapsOn = false;

        #region Constructor
        /// <summary>
        /// The _obj data services
        /// </summary>
        //DigiPhotoDataServices _objDataServices;
        private ConfigBusiness _configBusiness;
        private UserBusiness _userBusiness;
        private string _controlOn;
        /// <summary>
        /// Initializes a new instance of the <see cref="Login"/> class.
        /// </summary>
        public Login()
        {
            InitializeComponent();
            /////////////////Reset Controls on window////////////////////
            ClearControls();

            //reset all cache
            this.Dispatcher.BeginInvoke(DispatcherPriority.Background, new ClearCacheDelegate(clearCache));
            ///////////////////Get configuration data from Database for Common Configuration////
            RobotImageLoader.GetConfigData();
            RobotImageLoader.Is9ImgViewActive = true;
            tbDigiVersion.Text = "Version: Digi " + CurrentRegistryVersion();

        }

        private void clearCache()
        {
            CacheHelper.ClearAllCache();
        }
        #endregion

        #region Common Methods
        /// <summary>
        /// Gets the current version of Digiphoto Assembly
        /// </summary>
        /// <returns></returns>
        private string CurrentRegistryVersion()
        {
            var mr = new ModifyRegistry();
            //string currRegistry = mr.Read("InstallVersion");
            return mr.Read("InstallVersion");
        }
        ///////////////////Reset all the controls//////////////
        /// <summary>
        /// Clears the controls.
        /// </summary>
        private void ClearControls()
        {
            txtPassword.Password = string.Empty;
            txtUserName.Text = string.Empty;
            txtUserName.Focus();
            txbErrorMessage.Text = string.Empty;
        }

        ///////////////////Check for required fields in window//////////////
        /// <summary>
        /// Isvalidates this instance.
        /// </summary>
        /// <returns></returns>
        private bool Isvalidate()
        {
            if (string.IsNullOrWhiteSpace(txtUserName.Text))
            {
                ShowErrorMessage("Please enter username");
                txtUserName.Focus();
                return false;
            }
            if (string.IsNullOrWhiteSpace(txtPassword.Password))
            {
                ShowErrorMessage("Please enter password");
                txtPassword.Focus();
                return false;
            }
            return true;
        }

        //////////////////Logic for Login Method///////////////////// 
        /// <summary>
        /// Users the login.
        /// </summary>
		////////////change made by latika for set server date to all machine////////
        public struct SYSTEMTIME
        {
            public short wYear;
            public short wMonth;
            public short wDayOfWeek;
            public short wDay;
            public short wHour;
            public short wMinute;
            public short wSecond;
            public short wMilliseconds;
        }

        [DllImport("kernel32.dll", SetLastError = true)]
        public static extern bool SetSystemTime([In] ref SYSTEMTIME st);

//////end by latika

        private void UserLogin()
        {
#if DEBUG
            var watch = FrameworkHelper.CommonUtility.Watch();
            watch.Start();
#endif
            /////////////CALLING VALIDATION
            if (Isvalidate())
            {
                try
                {
                    //_objDataServices = new DigiPhotoDataServices();
                    _userBusiness = new UserBusiness();
                    /////////////////////////GET DATA FOR ENTERD USERNAME AND PASSWORD/////////
                    var objUser = _userBusiness.GetUserDetails(txtUserName.Text, CryptorEngine.Encrypt(txtPassword.Password, true));
                    if (objUser != null)
                    {/////////////////////////change made by latika for set server date to all machine
                        ConfigBusiness configBiz = new ConfigBusiness();
                        string sqlDBdatetime = configBiz.GetDBDatetime();
                        if (!string.IsNullOrEmpty(sqlDBdatetime))
                        {
                            string[] strdate = sqlDBdatetime.Split(' ');
                            string[] strDates = strdate[0].Split('-');
                            string[] strTime = strdate[1].Split(':');
                            string dates = strDates[0] + "-" + strDates[1] + "-" + strDates[2] + " " + strdate[1];
                            //DBdatetime = Convert.ToDateTime(dates);
                           

                            SYSTEMTIME st = new SYSTEMTIME();
                            st.wYear = Convert.ToInt16(strDates[2]); // must be short 
                            st.wMonth = Convert.ToInt16(strDates[0]);
                            st.wDay = Convert.ToInt16(strDates[1]);
                            st.wHour = Convert.ToInt16(strTime[0]); 
                            st.wMinute = Convert.ToInt16(strTime[1]); 
                            st.wSecond = Convert.ToInt16(strTime[2]); 

                            SetSystemTime(ref st);
                            ///end
                        }
                        ////////////ASSIGNING VALUES TO COMMON VARIABLES(FOR WHOLE APPLICATION//////////
                        LoginUser.StoreName = objUser.DG_Store_Name;
                        LoginUser.UserId = objUser.DG_User_pkey;
                        LoginUser.RoleId = objUser.DG_User_Roles_Id;
                        LoginUser.StoreId = objUser.DG_Store_ID;
                        LoginUser.UserName = (objUser.DG_User_First_Name + " " + objUser.DG_User_Last_Name).Trim();
                        LoginUser.Storecode = objUser.StoreCode;
                        LoginUser.countrycode = objUser.CountryCode;
                        LoginUser.ServerHotFolderPath = objUser.ServerHotFolderPath;
                        ReadSubStore();
                        //SetAnonymousQrCodeEnableStatus(LoginUser.SubStoreId);
                        /////////////CREATING OBJECT FOR HOME SCREEN///////////
                        Home objHome = new Home();
                        objHome.Show();
                        /////////////ENTRY FOR ACTIVITY LOG////////////
                        AuditLog.AddUserLog(objUser.DG_User_pkey, (int)FrameworkHelper.ActionType.Login, "Logged in ");
                        ////////CLOSING CURRENT WINDOW//////////
                        try
                        {
                            SetAnonymousQrCodeEnableStatus(LoginUser.SubStoreId);
                            this.Dispatcher.BeginInvoke(DispatcherPriority.Background, new LoadTabDelegate(LoadTabData));
                        }
                        catch { }

                        this.Close();
                        objHome.Focus();
                        objHome.Activate();
                        SetCurrencyProfile();

                    }
                    else
                    {
                        ////////LOGIN FAILED//////////
                        ShowErrorMessage("Incorrect username or password");
                        txtUserName.Focus();

                        ///////CALCULATING lOGIN FAILED ATTEMPTS
                        _countAttempt++;
                    }

                    if (_countAttempt == 3)
                    {
                        ///////ENTRY IN ACTIVITY LOG FOR UNAUTHORIZED ATTEMPTS 
                        AuditLog.AddUserLog(Common.LoginUser.UserId, (int)FrameworkHelper.ActionType.TemperAlert, "Unauthorized access attempted from " + System.Environment.MachineName);
                    }
                }
                catch (Exception ex)
                {
                    ///////ENTRY IN ERROR LOG FOR APPLICATION ERROR 
                    string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                    ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                    MessageBox.Show("Could not connect to database! Please check error log for more details.");

                }
            }
#if DEBUG
            if (watch != null)
                FrameworkHelper.CommonUtility.WatchStop("Login", watch);
#endif
        }
        private void LoadTabData()
        {
            Configuration.Instance = null;
            Configuration _objConfiguration = Configuration.Instance;

        }

        private void ShowErrorMessage(string strMessage)
        {
            txbErrorMessage.Text = strMessage;
        }
        #endregion

        #region Events

        /// <summary>
        /// Handles the Click event of the btnLogin control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void btnLogin_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                //////Calling user login logic
#if DEBUG
                var watch = FrameworkHelper.CommonUtility.Watch();
                watch.Start();
#endif
                UserLogin();
#if DEBUG
                if (watch != null)
                    FrameworkHelper.CommonUtility.WatchStop("Login", watch);
#endif
            }
            catch (Exception ex)
            {
                ///////ENTRY IN ERROR LOG FOR APPLICATION ERROR 
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }

        }

        /// <summary>
        /// Handles the KeyDown event of the txtUserName control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="KeyEventArgs"/> instance containing the event data.</param>
        private void txtUserName_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                //////////////////calling login logic on enter press
                if (e.Key == Key.Enter)
                {
                    UserLogin();
                }
            }
            catch (Exception ex)
            {
                ///////ENTRY IN ERROR LOG FOR APPLICATION ERROR 
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }

        /// <summary>
        /// Handles the KeyDown event of the txtPassword control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="KeyEventArgs"/> instance containing the event data.</param>
        private void txtPassword_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                //////////////////calling login logic on enter press
                if (e.Key == Key.Enter)
                {
                    UserLogin();
                }
            }
            catch (Exception ex)
            {
                ///////ENTRY IN ERROR LOG FOR APPLICATION ERROR 
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }
        ///////Single click event for all buttons of touch keyboard
        /// <summary>
        /// Handles the Click event of the btn control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void btn_Click(object sender, RoutedEventArgs e)
        {
            Button _objbtn = new Button();
            _objbtn = (Button)sender;
            switch (_objbtn.Content.ToString())
            {
                case "ENTER":
                    {
                        UserLogin();
                        break;
                    }
                case "SPACE":
                    {
                        txtUserName.Text = txtUserName.Text + " ";
                        break;
                    }
                case "CLOSE":
                    {
                        KeyBorder.Visibility = Visibility.Collapsed;
                        break;
                    }
                case "Back":
                    {
                        if (_controlOn == "UserName")
                        {
                            if (txtUserName.Text.Length > 0)
                            {
                                txtUserName.Text = txtUserName.Text.Remove(txtUserName.Text.Length - 1, 1);
                            }
                        }
                        else if (_controlOn == "Password")
                        {
                            if (txtPassword.Password.Length > 0)
                            {
                                txtPassword.Password = txtPassword.Password.Remove(txtPassword.Password.Length - 1, 1);
                            }
                        }
                        break;
                    }
                default:
                    {
                        if (_controlOn == "UserName")
                        {
                            txtUserName.Text = txtUserName.Text + _objbtn.Content;
                        }
                        else if (_controlOn == "Password")
                        {
                            txtPassword.Password = txtPassword.Password + _objbtn.Content;
                        }
                    }
                    break;
            }
        }

        /////////////// Login window  loaded event
        /// <summary>
        /// Handles the Loaded event of the Window control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {

        }

        /// <summary>
        /// Handles the GotFocus event of the txtUserName control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void txtUserName_GotFocus(object sender, RoutedEventArgs e)
        {
            KeyBorder.Visibility = Visibility.Visible;
            _controlOn = "UserName";
        }

        /// <summary>
        /// Handles the GotFocus event of the txtPassword control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void txtPassword_GotFocus(object sender, RoutedEventArgs e)
        {
            KeyBorder.Visibility = Visibility.Visible;
            _controlOn = "Password";
        }

        #endregion

        /// <summary>
        /// Handles the Closed event of the Window control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void Window_Closed(object sender, EventArgs e)
        {
            /////Window closing events/////////////////
            btnLogin.Click -= new RoutedEventHandler(btnLogin_Click);
            txtPassword.GotFocus -= new RoutedEventHandler(txtPassword_GotFocus);
            txtUserName.GotFocus -= new RoutedEventHandler(txtUserName_GotFocus);
            txtPassword.KeyDown -= new KeyEventHandler(txtPassword_KeyDown);
            txtUserName.KeyDown -= new KeyEventHandler(txtUserName_KeyDown);

        }

        public void SetAnonymousQrCodeEnableStatus(int substoreId)
        {
            try
            {
                //_objDataServices = new DigiPhotoDataServices();
                _configBusiness = new ConfigBusiness();

                //TBD:Cache
                var configList = _configBusiness.GetNewConfigValues(substoreId);
                if (configList != null && configList.Count > 0)
                {
                    iMIXConfigurationInfo configValue = configList.FirstOrDefault(o => o.IMIXConfigurationMasterId == (long)ConfigParams.IsAnonymousQrCodeEnabled);
                    if (configValue != null && !string.IsNullOrEmpty(configValue.ConfigurationValue))
                        App.IsAnonymousQrCodeEnabled = Convert.ToBoolean(configValue.ConfigurationValue);
                    iMIXConfigurationInfo configValue2 = configList.FirstOrDefault(o => o.IMIXConfigurationMasterId == (long)ConfigParams.IsEnabledRFID);
                    if (configValue2 != null && !string.IsNullOrEmpty(configValue2.ConfigurationValue))
                    {
                        App.IsRFIDEnabled = Convert.ToBoolean(configValue2.ConfigurationValue);
                    }
                    else
                    {
                        App.IsRFIDEnabled = false;
                    }

                    iMIXConfigurationInfo configValue3 = configList.FirstOrDefault(o => o.IMIXConfigurationMasterId == (long)ConfigParams.RFIDScanType);
                    if (App.IsRFIDEnabled == true && configValue3 != null && !string.IsNullOrEmpty(configValue3.ConfigurationValue))
                    {
                        App.RfidScanType = Convert.ToInt32(configValue3.ConfigurationValue);
                    }
                    else
                        App.RfidScanType = null;
                }
                else
                {
                    App.IsRFIDEnabled = false;
                    App.IsAnonymousQrCodeEnabled = false;
                    App.RfidScanType = null;
                }

            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
            }

        }
        private void ReadSubStore()
        {
            try
            {
                string pathtosave = Environment.CurrentDirectory;

                if (!System.IO.File.Exists(pathtosave + "\\ss.dat")) return;

                using (var reader = new System.IO.StreamReader(pathtosave + "\\ss.dat"))
                {
                    string line = reader.ReadLine();
                    string subStoreId = CryptorEngine.Decrypt(line, true);
                    LoginUser.SubStoreId = Convert.ToInt32(subStoreId.Split(',')[0]);
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
            }
        }

        private void btnCapsLock_Click(object sender, RoutedEventArgs e)
        {
            IsCapsOn = !IsCapsOn;
            ToggleKey();
        }
        private void ToggleKey()
        {
            if(IsCapsOn)
            {
                btnA.Content = "A";
                btnB.Content = "B";
                btnC.Content = "C";
                btnD.Content = "D";
                btnE.Content = "E";
                btnF.Content = "F";
                btnG.Content = "G";
                btnH.Content = "H";
                btnI.Content = "I";
                btnJ.Content = "J";
                btnK.Content = "K";
                btnL.Content = "L";
                btnM.Content = "M";
                btnN.Content = "N";
                btnO.Content = "O";
                btnP.Content = "P";
                btnQ.Content = "Q";
                btnR.Content = "R";
                btnS.Content = "S";
                btnT.Content = "T";
                btnU.Content = "U";
                btnV.Content = "V";
                btnW.Content = "W";
                btnX.Content = "X";
                btnY.Content = "Y";
                btnZ.Content = "Z";
            }
            else
            {
                btnA.Content = "a";
                btnB.Content = "b";
                btnC.Content = "c";
                btnD.Content = "d";
                btnE.Content = "e";
                btnF.Content = "f";
                btnG.Content = "g";
                btnH.Content = "h";
                btnI.Content = "i";
                btnJ.Content = "j";
                btnK.Content = "k";
                btnL.Content = "l";
                btnM.Content = "m";
                btnN.Content = "n";
                btnO.Content = "o";
                btnP.Content = "p";
                btnQ.Content = "q";
                btnR.Content = "r";
                btnS.Content = "s";
                btnT.Content = "t";
                btnU.Content = "u";
                btnV.Content = "v";
                btnW.Content = "w";
                btnX.Content = "x";
                btnY.Content = "y";
                btnZ.Content = "z";
            }
        }
        public void SetCurrencyProfile()
        {
            try
            {
                bool result = (new CurrencyExchangeBussiness()).UpdateInsertProfile(LoginUser.UserId);
            }
            catch (Exception ex)
            {
                ///////ENTRY IN ERROR LOG FOR APPLICATION ERROR 
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                // MessageBox.Show("Could not connect to database! Please check error log for more details.");

            }
        }
    }
}
