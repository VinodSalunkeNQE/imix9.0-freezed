//---------------------------------------------------------------------------
// MLProtect_MComposerlib.cs : Personal protection code for the MediaLooks License system
//---------------------------------------------------------------------------
// Copyright (c) 2012, MediaLooks Soft
// www.medialooks.com (dev@medialooks.com)
//
// Authors: MediaLooks Team
// Version: 3.0.0.0
//
//---------------------------------------------------------------------------
// CONFIDENTIAL INFORMATION
//
// This file is Intellectual Property (IP) of MediaLooks Soft and is
// strictly confidential. You can gain access to this file only if you
// sign a License Agreement and a Non-Disclosure Agreement (NDA) with
// MediaLooks Soft If you had not signed any of these documents, please
// contact <dev@medialooks.com> immediately.
//
//---------------------------------------------------------------------------
// Usage:
//
// 1. Copy MLProxy.dll from the license package to any folder and register it
//    in the system registry by using regsvr32.exe utility
//
// 2. Add the reference to MLProxy.dll in the C# project
//
// 3. Add this file to the project
//
// 4. Call MComposerlibLic.IntializeProtection() method before creating any Medialooks 
//    objects (for e.g. in Form_Load event handler)
//
// 5. Compile the project
//
// IMPORTANT: If your application is running for a period longer than 24 
//            hours, the IntializeProtection() call should be repeated 
//            periodically  (every 24 hours or less).
//
// IMPORTANT: If your have several Medialooks products, don't forget to initialize
//            protection for all of them. For e.g.
//
//             MPlatformLic.IntializeProtection();
//             MDecodersLic.IntializeProtection();
//             etc.

using System;

    public class MComposerlibLic
    {        
        private static MLPROXYLib.CoMLProxyClass m_objMLProxy;
        private static string strLicInfo = @"[MediaLooks]
License.ProductName=MComposer lib
License.IssuedTo=Digiphoto Entertainment Imaging (DIGI PHOTO STUDIO)
License.CompanyID=9935
License.UUID={87FCE46A-486B-44FA-9A87-2949C8014D95}
License.Key={186756CF-E445-AB2F-F945-E9F8C1422D9F}
License.Name=MComposer Module
License.UpdateExpirationDate=June 22, 2017
License.Edition=Professional MDecoders
License.AllowedModule=*.*
License.Signature=91CB55D1501CAAC10A414E3B58D8526E7A64E22CB21223DB666C3EEBC86DAF4D14D24EE48D65EFB7EE2AD08B81796B8844F3EB338410A76BC144347C80DD84333624FFC797C5A1629657AA106385D983EA9BC2F88F9A60AAFA72342DD03D5CF5FE7945FA3DE74F380D57E59D74D20F3052A2F9361C0135C98E7FC821976FC628

";

		//License initialization
        public static void IntializeProtection()
        {
            if (m_objMLProxy == null)
            {
                // Create MLProxy object 
                m_objMLProxy = new MLPROXYLib.CoMLProxyClass();
                m_objMLProxy.PutString(strLicInfo);                
            }
           UpdatePersonalProtection();
        }

        private static void UpdatePersonalProtection()
        {
            ////////////////////////////////////////////////////////////////////////
            // MediaLooks License secret key
            // Issued to: Digiphoto Entertainment Imaging (DIGI PHOTO STUDIO)
            const long _Q1_ = 62370083;
            const long _P1_ = 53855743;
            const long _Q2_ = 52173661;
            const long _P2_ = 55443887;

            try
            {

                int nFirst = 0;
                int nSecond = 0;
                m_objMLProxy.GetData(out nFirst, out  nSecond);

                // Calculate First * Q1 mod P1
                long llFirst = (long)nFirst * _Q1_ % _P1_;
                // Calculate Second * Q2 mod P2
                long llSecond = (long)nSecond * _Q2_ % _P2_;

                uint uRes = SummBits((uint)(llFirst + llSecond));

                // Calculate check value
                long llCheck = (long)(nFirst - 29) * (nFirst - 23) % nSecond;
                // Calculate return value
                int nRand = new Random().Next(0x7FFF);
                int nValue = (int)llCheck + (int)nRand * (uRes > 0 ? 1 : -1);

                m_objMLProxy.SetData(nFirst, nSecond, (int)llCheck, nValue);

            }
            catch (System.Exception) { }

        }

        private static uint SummBits(uint _nValue)
        {
            uint nRes = 0;
            while (_nValue > 0)
            {
                nRes += (_nValue & 1);
                _nValue >>= 1;
            }

            return nRes % 2;
        }
    }