﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.IO;
using System.Collections.ObjectModel;
using DigiPhoto.Common;
using DigiPhoto.DataLayer.Model;
using DigiPhoto.DataLayer;
using System.Windows.Media.Animation;
using Microsoft.Win32;
using System.Runtime.InteropServices;
using System.Windows.Threading;
using System.ComponentModel;
using System.Threading;
using DigiPhoto.Shader;
using System.Xml.Linq;
using System.Diagnostics;
using DigiAuditLogger;
using System.Windows.Controls.Primitives;
using System.Text.RegularExpressions;
using DigiPhoto.IMIX.Model;
using DigiPhoto.IMIX.Business;
using FrameworkHelper;
using DigiPhoto.Utility.Repository.ValueType;
using DigiPhoto.Manage;
using WMPLib;
using FrameworkHelper;
using Path = System.IO.Path;
using MPLATFORMLib;
using static DigiPhoto.IMIX.Model.Product;
using System.Web.Script.Serialization;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Configuration;
using DigiPhoto.FR;

namespace DigiPhoto
{
    public partial class SearchResult : Window
    {
        #region Declaration
        /////////////// Public variables for Window and Application 
        public static List<LstMyItems> itemsNotPrinted = new List<LstMyItems>();
        public bool flgViewScrl = false;
        private int num = 0;
        public string pagename;
        public string isStorybookBack;
        public delegate void NextPrimeDelegate();
        private bool continueCalculating = false;
        private bool IsEnableGrouping = false;
        int count;
        string MktImgPath = string.Empty;
        int mktImgTime = 0;
        private double dbContr = 0.0;
        private double dbBrit = 0.0;
        /// <summary>
        /// The current process
        /// </summary>
        Process currentProcess = Process.GetCurrentProcess();
        public string Photoname = "";
        public Int64 unlockImageId;
        public bool ismod;
        public BitmapImage unlockImage;
        BitmapImage _objBitmap;
        string _currentImage;
        int _currentImageId;
        int _currentMediaType;
        bool flgLoadNext = false;
        private bool isBarcodeActive = false;
        ShEffect _sharpeff = new ShEffect();

        bool flgGridWithoutPreview = true;
        int scrollIndexWithoutPreview;// = 20;
        int scrollIndexWithPreview;// = 4;
        public string Savebackpid = "";
        public AssociateImage uctlAssociateImage = null;
        public ModalDialog ModalDialog = null;
        public SaveGroup savegroupusercontrol = null;
        public bool isSingleScreenPreview = false;
        ClientView clientWin = null;
        FullClientPreview fullClientWin = null;
        public bool returntoHome = true;
        SearchDetailInfo searchDetails = new SearchDetailInfo();
        public int serachType = 0;

        private FileStream memoryFileStream;
        static string vsMediaFileName = "";

        public long MaxPhotoIdCriteria = 0;
        public long MinPhotoIdCriteria = 0;
        public int NewReord = 0;
        public Int32 AngleValue = 0;
        public MLMediaPlayer mplayer;
        public int isMarketingImgShow = 0;//////changed by latika for showing marketing images while editing.
        PhotographerBusiness PhotBiz = new PhotographerBusiness(); /////changed by Monika 
        #endregion

        #region Properties
        public BitmapImage CurrentBitmapImage
        {
            get;
            set;
        }

        #endregion

        #region Constructor
        public SearchResult()
        {
            try
            {
                InitializeComponent();
                if (RobotImageLoader.NotPrintedImages == null)
                    RobotImageLoader.NotPrintedImages = new List<LstMyItems>();

                RobotImageLoader._objnewincrement = new List<LstMyItems>();
                grdSelectAll.Visibility = Visibility.Visible; // Change this
                GetConfigPageSize();
                RobotImageLoader.ViewGroupedImagesCount = new List<string>();
                LoadPendingBurnOrders();
                MsgBox.SetParent(ModalDialogParent);
                imageInfo.SetParent(imageInfoParent);
                imageInfo.ExecuteParentMethod += new EventHandler(ShowMediaPlayer);

            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
            }
        }

        private void savegroupusercontrol_ExecuteParentMethod(object sender, EventArgs e)
        {
            gdMediaPlayer.Visibility = Visibility.Visible;
            savegroupusercontrol.ExecuteParentMethod -= savegroupusercontrol_ExecuteParentMethod;
        }
        void AddUserControlAssociateImage()
        {
            uctlAssociateImage = new AssociateImage();
            gdMediaPlayer.Visibility = Visibility.Collapsed;
            uctlAssociateImage.ExecuteParentMethod += new EventHandler(ShowMediaPlayer);
            grdCotrol.Children.Add(uctlAssociateImage);
            uctlAssociateImage.SetParent(ModalDialogParent);
        }
        void AddUserControlModalDialog()
        {
            ModalDialog = new ModalDialog();
            grdCotrol.Children.Add(ModalDialog);
            ModalDialog.SetParent(ModalDialogParent);
        }

        void AddUserControlSaveGroup()
        {
            savegroupusercontrol = new SaveGroup();
            savegroupusercontrol.ExecuteParentMethod += savegroupusercontrol_ExecuteParentMethod;
            grdCotrol.Children.Add(savegroupusercontrol);
            savegroupusercontrol.ExecuteMethod += new EventHandler(ClearGroup);
            savegroupusercontrol.ExecuteGroupMethod += new EventHandler(savegroupusercontrol_ExecuteGroupMethod);
        }

        //Constructor added to reduce the main constructor call(to avoid loading all objects) 
        public SearchResult(bool isShowClientView)
        {

        }
        private void GetConfigPageSize()
        {
            ConfigBusiness configBiz = new ConfigBusiness();
            List<long> objConfigMasterIds = new List<long>
            {
                (long)ConfigParams.Contrast,
                (long)ConfigParams.Brightness,
                (long)ConfigParams.IsPhotographerSerailSearchActive
            };

            List<iMIXConfigurationInfo> objdata = configBiz.GetNewConfigValues(LoginUser.SubStoreId).
                            Where(o => objConfigMasterIds.Contains(o.IMIXConfigurationMasterId)).ToList();

            if (objdata != null)
            {
                foreach (var imixConfigValue in objdata)
                {
                    switch (imixConfigValue.IMIXConfigurationMasterId)
                    {
                        case (int)ConfigParams.Contrast:
                            dbContr = string.IsNullOrEmpty(imixConfigValue.ConfigurationValue) ? 0 : imixConfigValue.ConfigurationValue.ToDouble();
                            break;

                        case (int)ConfigParams.Brightness:
                            dbBrit = string.IsNullOrEmpty(imixConfigValue.ConfigurationValue) ? 0 : imixConfigValue.ConfigurationValue.ToDouble();
                            break;
                        case (int)ConfigParams.IsBarcodeActive:
                            isBarcodeActive = string.IsNullOrEmpty(imixConfigValue.ConfigurationValue) ? false : imixConfigValue.ConfigurationValue.ToBoolean();
                            break;
                        case (int)ConfigParams.IsPhotographerSerailSearchActive:
                            LoginUser.IsPhotographerSerailSearchActive = imixConfigValue.ConfigurationValue != null ? imixConfigValue.ConfigurationValue.ToBoolean() : false;
                            break;
                    }
                }
            }
            ConfigurationInfo pgobjdata = configBiz.GetConfigurationData(LoginUser.SubStoreId);
            if (pgobjdata != null)
            {
                searchDetails.PageSize = 9;
                searchDetails.PageNumber = 1;
            }
            if (!RobotImageLoader.Is9ImgViewActive)
            {
                if (pgobjdata != null)
                {
                    scrollIndexWithoutPreview = pgobjdata.DG_PageCountGrid.ToInt32();
                    scrollIndexWithPreview = pgobjdata.DG_PageCountGrid.ToInt32();
                }
            }
            else
            {
                scrollIndexWithoutPreview = 9;
                scrollIndexWithPreview = scrollIndexWithoutPreview;
            }
        }
        void savegroupusercontrol_ExecuteGroupMethod(object sender, EventArgs e)
        {
            vwGroup.Text = "View Group";
            grdSelectAll.Visibility = Visibility.Visible;
            ViewGroup();
        }
        #endregion

        #region Private Methods
        private void LoadNext()
        {
            continueCalculating = false;
            flgLoadNext = true;
            RobotImageLoader.LoadImages();
            LoadImages();
        }
        private void LoadImages()
        {
            try
            {
                txtimageofphotographer.Visibility = Visibility.Collapsed;
                txtcountspace.Visibility = Visibility.Collapsed;
                if (RobotImageLoader.robotImages.Count != 0)
                {
                    if (RobotImageLoader.UserId > 0 && (RobotImageLoader.SearchCriteria == "Time" || RobotImageLoader.SearchCriteria == "TimeWithQrcode"))
                    {
                        txtimageofphotographer.Visibility = Visibility.Visible;
                        txtcountspace.Visibility = Visibility.Visible;
                        txtimageofphotographer.Text = "PhotoGrapherImage:" + " " + RobotImageLoader.ImgCount;
                    }
                    else
                    {
                        txtimageofphotographer.Visibility = Visibility.Collapsed;
                        txtcountspace.Visibility = Visibility.Collapsed;
                    }
                    RobotImageLoader.IsLastPage = false;
                    //added by ajay sinha on 6Aug19 to handle exception.
                    if (lstImages.Items.Count > 0 && lstImages != null)
                    {
                        lstImages.Items.Clear();
                    }
                    num = 0;
                    if (continueCalculating)
                    {
                        continueCalculating = false;
                    }
                    else
                    {
                        continueCalculating = true;
                        if (RobotImageLoader.robotImages == null)
                        {
                            RobotImageLoader.LoadImages();
                        }

                        LoadImagestoList();
                        //Check if select all remains true
                        CheckSelectAllGroup();
                        SPSelectAll.Visibility = System.Windows.Visibility.Visible;
                    }
                }
                else
                {
                    //added by ajay sinha on 6Aug19 to handle exception.
                    if (lstImages.Items.Count > 0 && lstImages != null)
                    {
                        lstImages.Items.Clear();
                    }
                    RobotImageLoader.IsLastPage = true;
                }
                //Added by Manoj, if product is instamobile then shareit icon will display other wise collapsed at 4-Apr-2019- Start
                if (!string.IsNullOrEmpty(InstaMobileProd.InstaMobileProductName) && InstaMobileProd.InstaMobileProductName.ToLower().Contains("instamobile"))
                {
                    btnShare.Visibility = System.Windows.Visibility.Visible;
                }
                else
                {
                    btnShare.Visibility = System.Windows.Visibility.Collapsed;
                }
                SetMessageText("Grouped");

            }
            catch (Exception ex)
            {
                //Entry in error log for scope error
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
            finally
            {
                //MemoryManagement.FlushMemory();
            }
        }
        private void CheckSelectAllGroup()
        {
            if (RobotImageLoader.IsLastPage == true)
                return;
            if (RobotImageLoader.ViewGroupedImagesCount != null && (RobotImageLoader.robotImages.Count() == RobotImageLoader.ViewGroupedImagesCount.Count))
            {
                chkSelectAll.IsChecked = true;
            }
            else
                chkSelectAll.IsChecked = false;

            chkSelectAll.Visibility = Visibility.Visible;
        }
        private void FillImageList()
        {
            RobotImageLoader.ViewGroupedImagesCount = new List<string>();

            if (RobotImageLoader.robotImages.Count > 0)
            {
                foreach (LstMyItems itm in RobotImageLoader.robotImages)
                {
                    if (itm.BmpImageGroup.UriSource.ToString() == "/images/view-accept.png")
                        RobotImageLoader.ViewGroupedImagesCount.Add(itm.Name);
                }
            }
        }

        private void LoadImagestoList()
        {
            int irobot = RobotImageLoader.robotImages.Count;
            LstMyItems _objnew = new LstMyItems();
            txtSelectedImages.Foreground = new SolidColorBrush(Colors.Black);

            /////adding images to listview as per criteria and selecting the main images 
            if (num < irobot)
            {
                _objnew = RobotImageLoader.robotImages[num];
                if (continueCalculating)
                {

                    if (_objnew.PhotoId == Savebackpid.ToInt32())
                    {
                        if (_objnew.IsVosDisplay && _objnew.StoryBookId > 0)
                        {
                            _objnew.BmpImageUplaoad = new BitmapImage(new Uri(@"/images/flipbook.png", UriKind.Relative));
                        }
                    }


                    lstImages.Items.Add(_objnew);
                    if (pagename == "Saveback" || pagename == "Placeback")
                    {
                        if (_objnew.PhotoId == Savebackpid.ToInt32())
                        {
                            lstImages.SelectedIndex = lstImages.Items.Count - 1;
                            lstImages.ScrollIntoView(lstImages.SelectedIndex);
                            ListBoxItem listBoxItem = (ListBoxItem)lstImages.ItemContainerGenerator.ContainerFromItem(lstImages.SelectedItem);
                            listBoxItem.Focus();
                            pagename = "";
                        }
                    }

                    if (_objnew.Name == RobotImageLoader.RFID)//For photo id search other than 0.
                    {
                        if (lstImages.SelectedItem == null)
                        {
                            lstImages.SelectedItem = _objnew;
                            lstImages.ScrollIntoView(lstImages.SelectedIndex);
                            ListBoxItem listBoxItem = (ListBoxItem)lstImages.ItemContainerGenerator.ContainerFromItem(lstImages.SelectedItem);
                            listBoxItem.Focus();
                        }
                    }
                    if ((num == irobot - 1) && (lstImages.SelectedIndex == -1))
                    {
                        try///////////////////by latika to handle the error while clearing group
                        {
                            lstImages.SelectedIndex = 0;
                            lstImages.ScrollIntoView(lstImages.SelectedIndex);
                            ListBoxItem listBoxItem = (ListBoxItem)lstImages.ItemContainerGenerator.ContainerFromItem(lstImages.SelectedItem);
                            listBoxItem.Focus();
                        }
                        catch { }////////////////////end/////////////////

                    }

                    ///adding images into queue
                    this.Dispatcher.BeginInvoke(
                        System.Windows.Threading.DispatcherPriority.Background,
                        new NextPrimeDelegate(this.LoadImagestoList));
                }
                num++;
            }
            else
            {
                continueCalculating = false;
                num = 0;
            }
        }

        private childItem FindVisualChild<childItem>(DependencyObject obj)
        where childItem : DependencyObject
        {
            for (int i = 0; i < VisualTreeHelper.GetChildrenCount(obj); i++)
            {
                DependencyObject child = VisualTreeHelper.GetChild(obj, i);
                if (child != null && child is childItem)
                    return (childItem)child;
                else
                {
                    childItem childOfChild = FindVisualChild<childItem>(child);
                    if (childOfChild != null)
                        return childOfChild;
                }
            }
            return null;
        }
        private BitmapImage GetImageFromPath(string path)
        {
            try
            {
                BitmapImage bi = new BitmapImage();
                using (FileStream fileStream = File.OpenRead(path))
                {
                    MemoryStream ms = new MemoryStream();
                    fileStream.CopyTo(ms);
                    ms.Seek(0, SeekOrigin.Begin);
                    fileStream.Close();
                    bi.CacheOption = BitmapCacheOption.OnLoad; // Memory 
                    bi.BeginInit();
                    bi.StreamSource = ms;
                    bi.EndInit();
                    bi.Freeze(); // Memory 
                    fileStream.Close();
                    return bi;
                }

            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                return null;
            }
        }
        private void CheckForAllImgSelectToPrint()
        {
            if (RobotImageLoader.GroupImages != null && RobotImageLoader.PrintImages != null)
            {
                var s = (from p in RobotImageLoader.PrintImages join g in RobotImageLoader.GroupImages on p.PhotoId equals g.PhotoId select p.PhotoId).Count();

                if (RobotImageLoader.PrintImages.Count > 0 && RobotImageLoader.GroupImages.Count > 0 && s == RobotImageLoader.GroupImages.Count)
                    chkSelectAll.IsChecked = true;
                else
                    chkSelectAll.IsChecked = false;
            }
            else
            {
                chkSelectAll.IsChecked = false;
            }

        }

        public void GetNewMaxId(out long maxPhotoId, int mediaType)
        {// for watcher if watcher is running then it will update maxPhotoId
            long maxid = 0;
            PhotoBusiness phBiz = new PhotoBusiness();
            string ssInfo = RobotImageLoader.GetShowAllSubstorePhotos() ? String.Empty : LoginUser.DefaultSubstores;
            phBiz.GetMaxId(ssInfo, out maxid, mediaType);
            maxPhotoId = maxid;

        }

        private void LoadImageGroupPrev()
        {
            if (lstImages.Items.Count == 0)
            {
                RobotImageLoader.IsNextPage = true;
                if (RobotImageLoader.ViewGroupedImagesCount != null && RobotImageLoader.ViewGroupedImagesCount.Count > 0)
                {
                    RobotImageLoader.ViewGroupedImagesCount.Clear();
                }
                RobotImageLoader.thumbSet = scrollIndexWithoutPreview;
                flgLoadNext = true;
                LoadNext();
                FillImageList();
                CheckSelectAllGroup();
            }
        }
        private void LoadImageGroupNext()
        {
            if (lstImages.Items.Count == 0)
            {
                if (RobotImageLoader.ViewGroupedImagesCount != null && RobotImageLoader.ViewGroupedImagesCount.Count > 0)
                {
                    RobotImageLoader.ViewGroupedImagesCount.Clear();
                }
                int recCount = 0;
                recCount = scrollIndexWithoutPreview;
                RobotImageLoader.IsNextPage = false;
                RobotImageLoader.thumbSet = recCount;
                flgLoadNext = true;
                LoadNext();
                FillImageList();
                CheckSelectAllGroup();
            }
        }

        private void ClearResources()
        {
            ////disposing all resources that are used in the Window for memory management///////
            DataObject.RemovePastingHandler(txtImageId, PastingHandler);
            btnEdit.Click -= new RoutedEventHandler(btnEdit_Click);
            btnHome.Click -= new RoutedEventHandler(btnHome_Click);
            btnLogout.Click -= new RoutedEventHandler(btnLogout_Click);
            btnPlaceOrder.Click -= new RoutedEventHandler(btnPlaceOrder_Click);
            btnPrintGroup.Click -= new RoutedEventHandler(btnPrintGroup_Click);
            btnSearchPhoto.Click -= new RoutedEventHandler(btnSearchPhoto_Click);
            btnWithoutPreview.Click -= new RoutedEventHandler(btnWithoutPreview_Click);
            btnWithPreviewActive.Click -= new RoutedEventHandler(btnWithPreviewActive_Click);
            btnWithoutPreview9.Click -= new RoutedEventHandler(btnWithoutPreview9_Click);
            btnViewGroup.Click -= new RoutedEventHandler(btnWithPreviewActive_Click);
            btnViewGroup.Click -= new RoutedEventHandler(btnViewGroup_Click);
            chkSelectAll.Click -= new RoutedEventHandler(chkSelectAll_Click);
            btnSearchPhoto.Click -= new RoutedEventHandler(chkSelectAll_Click);
            if (savegroupusercontrol != null)
            {
                savegroupusercontrol.ExecuteMethod -= new EventHandler(ClearGroup);
                savegroupusercontrol.ExecuteGroupMethod -= new EventHandler(savegroupusercontrol_ExecuteGroupMethod);
            }
            this.KeyUp -= new KeyEventHandler(Window_KeyUp);
            RobotImageLoader.ViewGroupedImagesCount = null;
            savegroupusercontrol = null;
            uctlAssociateImage = null;
            ModalDialog = null;
            RobotImageLoader._objnewincrement = null;
            MemoryManagement.DisposeImage(CurrentBitmapImage);
            CurrentBitmapImage = null;
            ModalDialog = null;
            //added by ajay sinha on 6Aug19 to handle exception.
            if (lstImages.Items.Count > 0 && lstImages != null)
            {
                lstImages.Items.Clear();
            }
            _objBitmap = null;
            unlockImage = null;
            this.ClearValue(TextBox.TextProperty);
            this.ClearValue(Button.TagProperty);
            this.ClearValue(Grid.WidthProperty);
            this.ClearValue(Grid.HeightProperty);
            this.ClearValue(TextBlock.TextProperty);
            this.ClearValue(TextBlock.TextProperty);
            if (lstImages.Items.Count > 0 && lstImages != null)////changed by latika 
            {
                lstImages.Items.Clear();
            }
            RobotImageLoader.currentCount = 0;
            this.Closed -= new EventHandler(Window_Closed);
            this.Loaded -= new RoutedEventHandler(Window_Loaded);
            this.Unloaded -= new RoutedEventHandler(Window_Unloaded);
            btnPlaceOrder.Click -= new RoutedEventHandler(btnPlaceOrder_Click);
            btnImageAddToGroup.Click -= new RoutedEventHandler(btnImageAddToGroup_Click);
            btnRemoveGroup.Click -= new RoutedEventHandler(btnRemoveGroup_Click);
            btnUnloack.Click -= new RoutedEventHandler(btnUnlock_Click);
            btnViewGroup.Click -= new RoutedEventHandler(btnViewGroup_Click);
            lstImages.SelectionChanged -= new SelectionChangedEventHandler(lstImages_SelectionChanged);
            chkSelectAll.Click -= new RoutedEventHandler(chkSelectAll_Click);
            txtImageId.KeyDown -= new KeyEventHandler(bttnLogin_Enter);
            Ok.Click -= new RoutedEventHandler(btnSearchImage_Click);
            btnQRSearch.Click -= new RoutedEventHandler(btnQRSearch_Click);
            btnPendingOrders.Click -= new RoutedEventHandler(btnPendingOrders_Click);
            btnchkpreview.Click -= new RoutedEventHandler(btnpreview_Click);
            btnNextButton.Click -= new RoutedEventHandler(btnScrollDown_Click);
            btnPrevButton.Click -= new RoutedEventHandler(btnScrollUp_Click);
            txtImageId.PreviewTextInput -= new TextCompositionEventHandler(txtAmountEntered_PreviewTextInput);
            BindingOperations.ClearBinding(lstImages, DataGrid.ItemsSourceProperty);
            ModalDialogParent = null;
        }
        #endregion

        #region Events
        private void Window_KeyUp(object sender, KeyEventArgs e)
        {

        }
        private void btnPrintGroup_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                LstMyItems curItem;
                ////////finding the image from the list for selection and preview
                System.Windows.Controls.Image _senderImage = (System.Windows.Controls.Image)btnPrintGroup.Content;
                if (vwGroup.Text == "View Group" && pagename != "Saveback")
                {
                    curItem = RobotImageLoader.robotImages.Where(t => t.PhotoId == _currentImageId).First();
                }
                else
                {
                    curItem = RobotImageLoader.GroupImages.Where(t => t.PhotoId == _currentImageId).First();
                }

                lstImages.SelectedItem = curItem;
                ListBoxItem listBoxItem = (ListBoxItem)lstImages.ItemContainerGenerator.ContainerFromItem(lstImages.SelectedItem);
                ///////setting focus to current image////////////////////////
                listBoxItem.Focus();

                ////////Checking whether selected item already is in Print group or not
                var mainitem = RobotImageLoader.PrintImages.Where(t => t.PhotoId == _currentImageId).FirstOrDefault();

                if (mainitem == null)
                {
                    ////////if Item not exists adding item to printgroup collection
                    LstMyItems _myitem = new LstMyItems();
                    _myitem = curItem;
                    _myitem.Name = _currentImage;
                    _myitem.PhotoId = curItem.PhotoId;
                    _myitem.MediaType = curItem.MediaType;
                    _myitem.VideoLength = curItem.VideoLength;
                    _myitem.FileName = curItem.FileName;
                    _myitem.FilePath = curItem.FilePath;
                    RobotImageLoader.PrintImages.Add(_myitem);
                    _senderImage.Source = new BitmapImage(new Uri(@"/images/print-accept.png", UriKind.Relative));
                    curItem.PrintGroup = new BitmapImage(new Uri(@"/images/print-accept.png", UriKind.Relative));
                    /////list image replace
                    ContentPresenter myContentPresenter = FindVisualChild<ContentPresenter>(listBoxItem);
                    // Finding textBlock from the DataTemplate that is set on that ContentPresenter
                    DataTemplate myDataTemplate = myContentPresenter.ContentTemplate;
                    Grid myTextBlock = (Grid)myDataTemplate.FindName("grdMain", myContentPresenter);
                    ((System.Windows.Controls.Image)(((Button)(((System.Windows.Controls.Panel)((((Grid)(myTextBlock.FindName("Printbtns")))))).Children[0])).Content)).Source = null;
                    ((System.Windows.Controls.Image)(((Button)(((System.Windows.Controls.Panel)((((Grid)(myTextBlock.FindName("Printbtns")))))).Children[0])).Content)).Source = new BitmapImage(new Uri(@"/images/print-accept.png", UriKind.Relative));

                }
                else
                {
                    RobotImageLoader.PrintImages.Remove(mainitem);
                    _senderImage.Source = new BitmapImage(new Uri(@"/images/print-group.png", UriKind.Relative));
                    curItem.PrintGroup = new BitmapImage(new Uri(@"/images/print-group.png", UriKind.Relative));

                    if (pagename == "Saveback")
                    {
                        //Added towards clearing the group items which are not to be printed
                        if (vwGroup.Text == "View Group" && pagename == "Saveback")
                        {
                            foreach (LstMyItems itx in RobotImageLoader.GroupImages)
                            {
                                var teItem = RobotImageLoader.PrintImages.Where(xs => xs.PhotoId == itx.PhotoId).FirstOrDefault();
                                if (teItem == null)
                                {
                                    RobotImageLoader.NotPrintedImages.Add(itx);
                                }
                            }
                            if (RobotImageLoader.NotPrintedImages.Count != 0)
                            {
                                foreach (LstMyItems ity in RobotImageLoader.NotPrintedImages)
                                {
                                    if (lstImages.Items.Count > 0)////changed by latika for resolve minimise issue
                                    {
                                        lstImages.Items.Remove(ity);
                                    }
                                    var tempG = RobotImageLoader.GroupImages.Where(tg => tg.PhotoId == ity.PhotoId).FirstOrDefault();
                                    tempG.PrintGroup = new BitmapImage(new Uri(@"/images/print-group.png", UriKind.Relative));
                                }
                            }
                            SPSelectAll.Visibility = System.Windows.Visibility.Hidden;

                            SetMessageText("EditPrintGrouped");

                            imgprintgroup.Source = new BitmapImage(new Uri(@"/images/print-accept.png", UriKind.Relative));
                        }
                        else
                        {
                            SPSelectAll.Visibility = System.Windows.Visibility.Visible;
                        }

                        //Added for clearing the View Group/Print Group if all items are removed//
                        if (lstImages.Items.Count == 0)
                        {
                            IMGFrame.Visibility = Visibility.Collapsed;
                            Grid.SetColumnSpan(thumbPreview, 2);
                            Grid.SetColumn(thumbPreview, 0);
                            thumbPreview.Margin = new Thickness(0);
                            pagename = "";
                            SPSelectAll.Visibility = System.Windows.Visibility.Visible;
                            vwGroup.Text = "View Group";
                            flgGridWithoutPreview = true;
                            ViewGroup();
                            return;
                        }
                        else
                        {
                            lstImages.SelectedIndex = 0;
                            if (lstImages.Items.Count > 0)
                            {

                                ListBoxItem listBoxItem1 = (ListBoxItem)lstImages.ItemContainerGenerator.ContainerFromItem(lstImages.Items[0]);
                                listBoxItem1.Focus();
                                lstImages.ScrollIntoView(lstImages.SelectedItem);
                                if (((DigiPhoto.LstMyItems)(lstImages.SelectedItem)).MediaType == 1)
                                    imgmain.Source = GetImageFromPath(LoginUser.DigiFolderBigThumbnailPath + Convert.ToString(((DigiPhoto.LstMyItems)(lstImages.SelectedItem)).FileName));

                                imgmain.UpdateLayout();
                            }
                        }

                    }

                    /////list image replace
                    ContentPresenter myContentPresenter = FindVisualChild<ContentPresenter>(listBoxItem);
                    // Finding textBlock from the DataTemplate that is set on that ContentPresenter
                    DataTemplate myDataTemplate = myContentPresenter.ContentTemplate;
                    Grid myTextBlock = (Grid)myDataTemplate.FindName("grdMain", myContentPresenter);
                    ((System.Windows.Controls.Image)(((Button)(((System.Windows.Controls.Panel)((((Grid)(myTextBlock.FindName("Printbtns")))))).Children[0])).Content)).Source = null;
                    ((System.Windows.Controls.Image)(((Button)(((System.Windows.Controls.Panel)((((Grid)(myTextBlock.FindName("Printbtns")))))).Children[0])).Content)).Source = new BitmapImage(new Uri(@"/images/print-group.png", UriKind.Relative));
                    /////list image replace

                }
                if (vwGroup.Text == "View Group")
                {
                    SetMessageText("Grouped");
                }
                else
                {
                    SetMessageText("PrintGrouped");
                }

                //Added towards clearing the group items which are not to be printed
                if (vwGroup.Text == "View Group" && pagename == "Saveback")
                {
                    foreach (LstMyItems itx in RobotImageLoader.GroupImages)
                    {
                        var teItem = RobotImageLoader.PrintImages.Where(xs => xs.PhotoId == itx.PhotoId).FirstOrDefault();
                        if (teItem == null)
                        {
                            RobotImageLoader.NotPrintedImages.Add(itx);
                        }
                    }
                    if (RobotImageLoader.NotPrintedImages.Count != 0)
                    {
                        foreach (LstMyItems ity in RobotImageLoader.NotPrintedImages)
                        {
                            if (lstImages.Items.Count > 0)////changed by latika for resolve minimise issue
                            {
                                lstImages.Items.Remove(ity);
                            }
                            var tempG = RobotImageLoader.GroupImages.Where(tg => tg.PhotoId == ity.PhotoId).FirstOrDefault();
                            tempG.PrintGroup = new BitmapImage(new Uri(@"/images/print-group.png", UriKind.Relative));
                        }
                    }
                    SetMessageText("EditPrintGrouped");

                    SPSelectAll.Visibility = System.Windows.Visibility.Hidden;
                }
                else
                {
                    SPSelectAll.Visibility = System.Windows.Visibility.Visible;
                }
                //////////////
                if (lstImages.Items.Count > 0 && lstImages.SelectedIndex == -1)
                {
                    lstImages.SelectedIndex = 0;
                    ListBoxItem listBoxItem1 = (ListBoxItem)lstImages.ItemContainerGenerator.ContainerFromItem(lstImages.Items[0]);
                    listBoxItem1.Focus();
                    lstImages.ScrollIntoView(lstImages.SelectedItem);
                }
                if (vwGroup.Text == "View All")
                {
                    CheckForAllImgSelectToPrint();
                }
                else
                {
                    FillImageList();
                    CheckSelectAllGroup();
                }
            }
            catch (Exception ex)
            {
                ///////////////Entry in error log for scope error
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }
        private void btnHome_Click(object sender, RoutedEventArgs e)
        {
            if (RobotImageLoader.GroupImages != null)
            {
                RobotImageLoader.GroupImages.RemoveAll(t => t.StoryBookId > 0 && t.IsVosDisplay == false);//Remove storybook child images                
            }
            if (RobotImageLoader.robotImages != null)
            {
                RobotImageLoader.robotImages.RemoveAll(t => t.StoryBookId > 0 && t.IsVosDisplay == false);//Remove storybook child images
            }
            if (returntoHome)
            {
                MediaStop();
                vsMediaFileName = string.Empty;
                RobotImageLoader.IsZeroSearchNeeded = true;
                RobotImageLoader.StartIndex = 0;
                vwGroup.Text = "View Group";
                GetMktImgInfo();
                ClientView window = null;

                foreach (Window wnd in Application.Current.Windows)
                {
                    if (wnd.Title == "ClientView")
                    {
                        window = (ClientView)wnd;
                    }
                }

                if (window == null)
                {
                    window = new ClientView();
                    window.WindowStartupLocation = System.Windows.WindowStartupLocation.Manual;
                }

                window.GroupView = false;
                window.DefaultView = false;

                if (!(MktImgPath == "" || mktImgTime == 0))
                {
                    window.instructionVideo.Visibility = System.Windows.Visibility.Visible;
                    window.instructionVideo.Play();
                    // Added for Blur Iamges
                    window.instructionVideoBlur.Visibility = System.Windows.Visibility.Visible;
                    window.instructionVideoBlur.Play();
                    // End Changes
                }
                else
                {
                    window.imgDefault.Visibility = System.Windows.Visibility.Visible;
                    window.imgDefaultBlur.Visibility = System.Windows.Visibility.Visible; // Added for Blur Iamges
                }
                window.testR.Fill = null;
                window.DefaultView = true;
                if (window.instructionVideo.Visibility == System.Windows.Visibility.Visible)
                    window.instructionVideo.Play();
                else
                    window.instructionVideo.Pause();
                // Added for Blur Iamges
                if (window.instructionVideoBlur.Visibility == System.Windows.Visibility.Visible)
                    window.instructionVideoBlur.Play();
                else
                    window.instructionVideoBlur.Pause();
                // End Changes 
                CompileEffectChanged(null, -2);
                Home _objhome = new Home();
                //Chek if Home window is opened or not
                if (_objhome.Visibility == Visibility.Collapsed)
                {
                    _objhome.Show();
                }

                this.Hide();
                //////Closing current window and moving the cursor to home page////
            }
            else
            {
                returntoHome = true;
            }
        }
        private void Window_Closed(object sender, EventArgs e)
        {
            try
            {
                MediaStop();
                ClearResources();
            }
            catch (Exception en)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(en);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }
        private void btnPrint_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                ///////addming images to print group when clicked from with preview screen  
                Button _btnSender = (Button)sender;
                System.Windows.Controls.Image _senderImage = (System.Windows.Controls.Image)_btnSender.Content;
                if (vwGroup.Text == "View Group" && pagename != "Saveback")
                    RobotImageLoader.curItem = RobotImageLoader.robotImages.Where(t => t.PhotoId == (int)((Button)sender).CommandParameter).First();
                else
                    RobotImageLoader.curItem = RobotImageLoader.GroupImages.Where(t => t.PhotoId == (int)((Button)sender).CommandParameter).First();

                var printitem = RobotImageLoader.PrintImages.Where(t => t.PhotoId == (int)((Button)(sender)).CommandParameter).FirstOrDefault();
                int currIndex = 0;
                if (printitem == null)
                {
                    LstMyItems _myitem = new LstMyItems();
                    _myitem = RobotImageLoader.curItem;
                    _myitem.Name = (String)(((Button)(sender)).Tag);
                    _myitem.PhotoId = RobotImageLoader.curItem.PhotoId;
                    _myitem.MediaType = RobotImageLoader.curItem.MediaType;
                    _myitem.VideoLength = RobotImageLoader.curItem.VideoLength;
                    _myitem.FileName = RobotImageLoader.curItem.FileName;
                    _myitem.CreatedOn = RobotImageLoader.curItem.CreatedOn;
                    _myitem.OnlineQRCode = RobotImageLoader.curItem.OnlineQRCode;
                    _myitem.FilePath = RobotImageLoader.curItem.FilePath;
                    RobotImageLoader.PrintImages.Add(_myitem);
                    _senderImage.Source = new BitmapImage(new Uri(@"/images/print-accept.png", UriKind.Relative));
                    RobotImageLoader.curItem.PrintGroup = new BitmapImage(new Uri(@"/images/print-accept.png", UriKind.Relative));
                    imgprintgroup.Source = new BitmapImage(new Uri(@"/images/print-accept.png", UriKind.Relative));
                }
                else
                {
                    currIndex = NextGroupSelectedIndex(lstImages.Items.IndexOf(printitem));
                    RobotImageLoader.PrintImages.Remove(printitem);
                    _senderImage.Source = new BitmapImage(new Uri(@"/images/print-group.png", UriKind.Relative));
                    RobotImageLoader.curItem.PrintGroup = new BitmapImage(new Uri(@"/images/print-group.png", UriKind.Relative));
                    imgprintgroup.Source = new BitmapImage(new Uri(@"/images/print-group.png", UriKind.Relative));
                    //Added for clearing the View Group/Print Group if all items are removed//
                    if (lstImages.Items.Count == 0) //reverting if the print group gets empty
                    {
                        pagename = "";
                    }
                    if (lstImages.Items.Count > 0)
                    {
                        if (currIndex < 0)
                            currIndex = 0;
                        lstImages.SelectedItem = lstImages.Items[currIndex];
                        ListBoxItem listBoxItem = (ListBoxItem)lstImages.ItemContainerGenerator.ContainerFromItem(lstImages.Items[currIndex]);
                        listBoxItem.Focus();
                    }
                    else
                    {
                        continueCalculating = false;
                        num = 0;
                        RobotImageLoader.currentCount = 0;
                        RobotImageLoader.IsZeroSearchNeeded = true;
                        RobotImageLoader.RFID = "0";
                        RobotImageLoader.LoadImages();
                        LoadImages();
                        IMGFrame.Visibility = Visibility.Collapsed;
                        Grid.SetColumnSpan(thumbPreview, 2);
                        Grid.SetColumn(thumbPreview, 0);
                        thumbPreview.Margin = new Thickness(0);
                        vwGroup.Text = "View Group";
                        flgGridWithoutPreview = true;
                        ////For showing the paginate buttons and hiding the scrollbars
                        btnprev.Visibility = System.Windows.Visibility.Visible;
                        btnnext.Visibility = System.Windows.Visibility.Visible;
                        ScrollViewer.SetVerticalScrollBarVisibility(lstImages, ScrollBarVisibility.Hidden);

                    }
                    //Added for clearing the View Group/Print Group if all items are removed//

                }
                //To check mark the image
                lstImages.SelectedItem = RobotImageLoader.curItem;

                if (vwGroup.Text == "View All")
                    CheckForAllImgSelectToPrint();

                if (vwGroup.Text == "View Group")
                {
                    SetMessageText("Grouped");
                }
                else
                {
                    SetMessageText("PrintGrouped");
                }

                //Added towards clearing the group items which are not to be printed
                if (vwGroup.Text == "View Group" && pagename == "Saveback")
                {
                    foreach (LstMyItems itx in RobotImageLoader.GroupImages)
                    {
                        var teItem = RobotImageLoader.PrintImages.Where(xs => xs.PhotoId == itx.PhotoId).FirstOrDefault();
                        if (teItem == null)
                        {
                            RobotImageLoader.NotPrintedImages.Add(itx);
                        }
                    }
                    if (RobotImageLoader.NotPrintedImages.Count != 0)
                    {
                        foreach (LstMyItems ity in RobotImageLoader.NotPrintedImages)
                        {
                            if (lstImages.Items.Count > 0)/////changed by latika for minimise issue
                            {
                                lstImages.Items.Remove(ity);
                            }
                            var tempG = RobotImageLoader.GroupImages.Where(tg => tg.PhotoId == ity.PhotoId).FirstOrDefault();
                            tempG.PrintGroup = new BitmapImage(new Uri(@"/images/print-group.png", UriKind.Relative));
                        }
                    }
                    SetMessageText("EditPrintGrouped");
                    SPSelectAll.Visibility = System.Windows.Visibility.Hidden;
                }
                else
                {
                    SPSelectAll.Visibility = System.Windows.Visibility.Visible;
                }
                //////////////
                //Added for clearing the View Group/Print Group if all items are removed//
                if (lstImages.Items.Count == 0)
                {
                    IMGFrame.Visibility = Visibility.Collapsed;
                    Grid.SetColumnSpan(thumbPreview, 2);
                    Grid.SetColumn(thumbPreview, 0);
                    thumbPreview.Margin = new Thickness(0);
                    pagename = "";
                    SPSelectAll.Visibility = System.Windows.Visibility.Visible;
                    vwGroup.Text = "View Group";
                    ViewGroup();
                    return;
                }

                ListBoxItem listBoxItem1 = (ListBoxItem)lstImages.ItemContainerGenerator.ContainerFromItem(lstImages.SelectedItem);
                if (listBoxItem1 != null)
                {
                    listBoxItem1.Focus();
                }
                else
                {
                    if (lstImages.Items.Count > 0 && lstImages.SelectedIndex == -1)
                    {
                        lstImages.SelectedIndex = currIndex;
                        ListBoxItem listBoxItem = (ListBoxItem)lstImages.ItemContainerGenerator.ContainerFromItem(lstImages.Items[currIndex]);
                        listBoxItem.Focus();
                        lstImages.ScrollIntoView(lstImages.SelectedItem);
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
            finally
            {
                var itemsb = RobotImageLoader.GroupImages.Where(o => o.MediaType == 1 && o.StoryBookId != 0).Count();
                var itembs = RobotImageLoader.GroupImages.Where(o => o.MediaType == 1 && o.StoryBookId == 0).Count();
                var itemps = RobotImageLoader.PrintImages.Where(o => o.MediaType == 1 && o.StoryBookId != 0).Count();
                var itemsp = RobotImageLoader.PrintImages.Where(o => o.MediaType == 1 && o.StoryBookId == 0).Count();
                if (itemsb != 0 && itembs == 0)
                {
                    btnEdit.IsEnabled = false;
                }
                else if (itemsb != 0 && itembs != 0)
                {
                    btnEdit.IsEnabled = false;
                }
                else if (itemps != 0 && itemsp == 0)
                {
                    btnEdit.IsEnabled = false;
                }
                else if (itemps != 0 && itemsp != 0)
                {
                    btnEdit.IsEnabled = false;
                }
                else
                {
                    btnEdit.IsEnabled = true;
                }

            }
        }
        int NextGroupSelectedIndex(int currentIndex)
        {
            int index = -1;
            int totalCount = lstImages.Items.Count;
            if (totalCount > 0)
            {
                index = totalCount > (currentIndex + 1) ? currentIndex : --currentIndex;
            }
            return index;
        }
        private void MainImage_Click(object sender, RoutedEventArgs e)
        {
            //Add selected/grouped storybook child and edited images in lstImages
            var SBitm = RobotImageLoader.GroupImages.Where(tg => tg.StoryBookId > 0).FirstOrDefault();
            if (SBitm != null)
            {
                var StoryBookPhotos = (new PhotoBusiness()).GetAllStoryBookPhotoBYSTID(SBitm.StoryBookId);
                if (ConfigurationManager.AppSettings.AllKeys.Contains("isSBPreviewEnabled") == true)
                {
                    bool isSBPreviewEnabled = Convert.ToBoolean(ConfigurationManager.AppSettings["isSBPreviewEnabledWithGuest"]);
                    if (StoryBookPhotos != null && isSBPreviewEnabled)
                    {
                        lstImages.Items.Clear();
                        RobotImageLoader.robotImages.Clear();
                    }
                }
                foreach (var storyitem in StoryBookPhotos)
                {
                    if (RobotImageLoader.GroupImages.Where(o => o.PhotoId == storyitem.DG_Photos_pkey).Count() == 0)
                    {
                        LstMyItems _mySBitem = new LstMyItems();
                        _mySBitem.MediaType = storyitem.DG_MediaType;
                        _mySBitem.VideoLength = storyitem.DG_VideoLength;
                        _mySBitem.Name = storyitem.DG_Photos_RFID;
                        _mySBitem.StoryBookId = storyitem.StoryBookID;
                        _mySBitem.IsVosDisplay = storyitem.IsVosDisplay;
                        _mySBitem.IsPassKeyVisible = Visibility.Collapsed;
                        _mySBitem.PhotoId = storyitem.DG_Photos_pkey;
                        _mySBitem.FileName = storyitem.DG_Photos_FileName;
                        _mySBitem.IsVosDisplay = storyitem.IsVosDisplay;
                        _mySBitem.CreatedOn = storyitem.DG_Photos_CreatedOn;
                        var date = _mySBitem.CreatedOn.ToString("yyyyMMdd");
                        string FilepathforStoryBook = storyitem.HotFolderPath + "Thumbnails" + "\\" + date + "\\" + storyitem.DG_Photos_FileName;
                        _mySBitem.OnlineQRCode = "";
                        //_mySBitem.IsLocked = Visibility.Visible;
                        //_mySBitem.BmpImageGroup = new BitmapImage(new Uri(@"/images/view-accept.png", UriKind.Relative));
                        //_mySBitem.FilePath = FilepathforStoryBook;
                        _mySBitem.HotFolderPath = storyitem.HotFolderPath;
                        _mySBitem.PrintGroup = SBitm.PrintGroup;
                        //_mySBitem.BigDBThumnailPath = Path.Combine(storyitem.HotFolderPath, _mySBitem.BigDBThumnailPath);
                        _mySBitem.BigThumbnailPath = Path.Combine(storyitem.HotFolderPath, _mySBitem.BigDBThumnailPath, storyitem.DG_Photos_CreatedOn.ToString("yyyyMMdd"), storyitem.DG_Photos_FileName);
                        _mySBitem.FilePath = Path.Combine(storyitem.HotFolderPath, _mySBitem.ThumnailPath, storyitem.DG_Photos_CreatedOn.ToString("yyyyMMdd"), storyitem.DG_Photos_FileName);
                        //_mySBitem.ThumnailPath = FilepathforStoryBook;
                        //_mySBitem.BigDBThumnailPath = storyitem.HotFolderPath + "Thumbnails_Big" + "\\" + date + "\\" + storyitem.DG_Photos_FileName;                        
                        _mySBitem.GridMainHeight = 190;
                        _mySBitem.GridMainWidth = 226;
                        _mySBitem.GridMainRowHeight1 = 140;
                        _mySBitem.GridMainRowHeight2 = 50;
                        //Check if item present in list..if not then add

                        bool ifExists = lstImages.Items.Cast<LstMyItems>().Any(x => x.PhotoId == _mySBitem.PhotoId);

                        if (!ifExists)
                        {
                            lstImages.Items.Add(_mySBitem);//Add child image to listImages control on side strip 
                        }
                        var curItem = RobotImageLoader.robotImages.Where(t => t.PhotoId.ToString() == _mySBitem.PhotoId.ToString()).FirstOrDefault();
                        if (curItem == null)
                        {
                            RobotImageLoader.robotImages.Add(_mySBitem);
                        }
                    }
                }
            }

            //Sort all items in listbox guest images
            lstImages.Items.SortDescriptions.Add(new SortDescription("Name", ListSortDirection.Ascending));
            lstImages.Items.Refresh();

            MediaStop();
            //////added by latika for marketing images date:2019-07-19
            isMarketingImgShow = 0;
            //VisualBrush CB = new VisualBrush(ContentContainer);
            //CompileEffectChanged(CB, -1);
            //////end////
            Grid.SetColumnSpan(thumbPreview, 1);
            Grid.SetColumn(thumbPreview, 1);
            thumbPreview.Margin = new Thickness(0, 0, -77.5, 8);
            ScrollViewer.SetVerticalScrollBarVisibility(lstImages, ScrollBarVisibility.Visible);
            imgwithPreview.Source = new BitmapImage(new Uri(@"/images/thumbnailview1_active.png", UriKind.Relative));
            imgwithoutPreview.Source = new BitmapImage(new Uri(@"images/16blocks_black.png", UriKind.Relative));
            imgwithoutPreview9.Source = new BitmapImage(new Uri(@"/images/9Blocks_black.png", UriKind.Relative));

            try
            {
                IMGFrame.Visibility = Visibility.Visible;

                if (vwGroup.Text == "View Group" && pagename != "Saveback")
                {

                    #region firstif
                    var curItem = RobotImageLoader.robotImages.Where(t => t.PhotoId.ToString() == ((Button)sender).CommandParameter.ToString()).FirstOrDefault();


                    if (curItem != null)
                    {
                        if (curItem.MediaType == 2 || curItem.MediaType == 3)
                        {
                            btnEdit.IsEnabled = false;
                            img.Visibility = Visibility.Hidden;
                            vidoriginal.Visibility = Visibility.Visible;
                            string vidPath = curItem.FileName;
                            if (!String.IsNullOrEmpty(vidPath))
                            {
                                if (curItem.MediaType == 2)
                                {
                                    using (FileStream fileStream = File.OpenRead(System.IO.Path.Combine(curItem.HotFolderPath, "Videos", curItem.CreatedOn.ToString("yyyyMMdd"), vidPath)))
                                    {
                                        vsMediaFileName = fileStream.Name;
                                    }
                                }
                                else
                                {
                                    using (FileStream fileStream = File.OpenRead(System.IO.Path.Combine(curItem.HotFolderPath, "ProcessedVideos", curItem.CreatedOn.ToString("yyyyMMdd"), vidPath)))
                                    {
                                        vsMediaFileName = fileStream.Name;
                                    }
                                }
                            }
                            //MediaStop();
                            MediaPlay();
                            // Dispatcher.Invoke(new Action(() => MediaPlay()));
                            txtMainVideo.Text = curItem.Name;
                        }
                        else
                        {
                            btnEdit.IsEnabled = true;
                            img.Visibility = Visibility.Visible;
                            vidoriginal.Visibility = Visibility.Hidden;
                            MediaStop();
                        }
                        if (lstImages.SelectedItem != curItem)
                        {
                            if (curItem != null)
                            {
                                lstImages.SelectedItem = curItem;
                                RobotImageLoader.PhotoId = curItem.Name;
                                ListBoxItem listBoxItem = (ListBoxItem)lstImages.ItemContainerGenerator.ContainerFromItem(lstImages.SelectedItem);
                                listBoxItem.Focus();
                                lstImages.ScrollIntoView(lstImages.SelectedItem);
                            }
                        }
                        if (lstImages.SelectedItem != null)
                        {
                            _currentImageId = ((DigiPhoto.LstMyItems)(lstImages.SelectedItem)).PhotoId;
                            txtMainImage.Text = ((DigiPhoto.LstMyItems)(lstImages.SelectedItem)).Name;
                        }
                        //var searItem = RobotImageLoader.GroupImages.Where(t => t.Name == curItem.Name).FirstOrDefault();
                        var searItem = RobotImageLoader.GroupImages.Where(t => t.PhotoId == curItem.PhotoId).FirstOrDefault();
                        if (searItem != null)
                        {
                            ImgAddToGroup.Source = new BitmapImage(new Uri(@"/images/view-accept.png", UriKind.Relative));
                            btnImageAddToGroup.ToolTip = "Remove from group";
                            searItem = null;
                        }
                        else
                        {
                            ImgAddToGroup.Source = new BitmapImage(new Uri(@"/images/view-GROUP.png", UriKind.Relative));
                            btnImageAddToGroup.ToolTip = "Add to group";
                        }

                        //var psearItem = RobotImageLoader.PrintImages.Where(t => t.Name == curItem.Name).FirstOrDefault();
                        var psearItem = RobotImageLoader.PrintImages.Where(t => t.PhotoId == curItem.PhotoId).FirstOrDefault();
                        if (psearItem != null)
                        {
                            imgprintgroup.Source = new BitmapImage(new Uri(@"/images/print-accept.png", UriKind.Relative));
                            //btnImageAddToGroup.ToolTip = "Remove from group";
                            psearItem = null;
                        }
                        else
                        {
                            imgprintgroup.Source = new BitmapImage(new Uri(@"/images/print-group.png", UriKind.Relative));
                            //btnImageAddToGroup.ToolTip = "Add to group";
                        }

                    }
                    else
                    {
                        lstImages.ScrollIntoView(lstImages.SelectedItem);
                        txtMainImage.Text = ((DigiPhoto.LstMyItems)(lstImages.SelectedItem)).Name;
                        _currentImage = ((DigiPhoto.LstMyItems)(lstImages.SelectedItem)).Name;
                        _currentImageId = ((DigiPhoto.LstMyItems)(lstImages.SelectedItem)).PhotoId;
                        var searItem = RobotImageLoader.GroupImages.Where(t => t.PhotoId == _currentImageId).FirstOrDefault();
                        if (searItem != null)
                        {
                            ImgAddToGroup.Source = new BitmapImage(new Uri(@"/images/view-accept.png", UriKind.Relative));
                            btnImageAddToGroup.ToolTip = "Remove from group";
                            searItem = null;
                        }
                        else
                        {
                            ImgAddToGroup.Source = new BitmapImage(new Uri(@"/images/view-GROUP.png", UriKind.Relative));
                            btnImageAddToGroup.ToolTip = "Add to group";
                        }
                        //var psearItem = RobotImageLoader.PrintImages.Where(t => t.Name == curItem.Name).FirstOrDefault();
                        var psearItem = RobotImageLoader.PrintImages.Where(t => t.PhotoId == curItem.PhotoId).FirstOrDefault();
                        if (psearItem != null)
                        {
                            imgprintgroup.Source = new BitmapImage(new Uri(@"/images/print-accept.png", UriKind.Relative));
                            //btnImageAddToGroup.ToolTip = "Remove from group";
                            psearItem = null;
                        }
                        else
                        {
                            imgprintgroup.Source = new BitmapImage(new Uri(@"/images/print-group.png", UriKind.Relative));
                            //btnImageAddToGroup.ToolTip = "Add to group";
                        }
                        Int32 Photo_PKey = ((DigiPhoto.LstMyItems)(lstImages.SelectedItem)).PhotoId;
                        unlockImageId = Photo_PKey;
                    }
                    #endregion firstif
                }
                else
                {
                    var curItem = RobotImageLoader.GroupImages.Where(t => t.FilePath == ((Button)sender).Tag).FirstOrDefault();

                    if (curItem.MediaType != 1)
                    {
                        if (curItem.MediaType == 2)
                        {
                            using (FileStream fileStream = File.OpenRead(System.IO.Path.Combine(curItem.HotFolderPath, "Videos", curItem.CreatedOn.ToString("yyyyMMdd"), curItem.FileName)))
                            {
                                vsMediaFileName = fileStream.Name;
                            }
                        }
                        else
                        {
                            using (FileStream fileStream = File.OpenRead(System.IO.Path.Combine(curItem.HotFolderPath, "ProcessedVideos", curItem.CreatedOn.ToString("yyyyMMdd"), curItem.FileName)))
                            {
                                vsMediaFileName = fileStream.Name;
                            }
                        }
                        MediaPlay();
                    }
                    if (lstImages.SelectedItem != curItem)
                    {
                        lstImages.SelectedItem = curItem;
                        ListBoxItem listBoxItem = (ListBoxItem)lstImages.ItemContainerGenerator.ContainerFromItem(lstImages.SelectedItem);
                        listBoxItem.Focus();
                        RobotImageLoader.UniquePhotoId = curItem.PhotoId;
                        RobotImageLoader.PhotoId = curItem.Name.ToString();
                    }
                    else
                    {
                        lstImages.ScrollIntoView(lstImages.SelectedItem);
                        txtMainImage.Text = ((DigiPhoto.LstMyItems)(lstImages.SelectedItem)).Name;
                        _currentImage = ((DigiPhoto.LstMyItems)(lstImages.SelectedItem)).Name;
                        _currentImageId = ((DigiPhoto.LstMyItems)(lstImages.SelectedItem)).PhotoId;
                        var searItem = RobotImageLoader.GroupImages.Where(t => t.PhotoId == _currentImageId).FirstOrDefault();
                        if (searItem != null)
                        {
                            ImgAddToGroup.Source = new BitmapImage(new Uri(@"/images/view-accept.png", UriKind.Relative));
                            btnImageAddToGroup.ToolTip = "Remove from group";
                            searItem = null;
                        }
                        else
                        {
                            ImgAddToGroup.Source = new BitmapImage(new Uri(@"/images/view-GROUP.png", UriKind.Relative));
                            btnImageAddToGroup.ToolTip = "Add to group";
                        }
                        var psearItem = RobotImageLoader.PrintImages.Where(t => t.PhotoId == curItem.PhotoId).FirstOrDefault();
                        if (psearItem != null)
                        {
                            imgprintgroup.Source = new BitmapImage(new Uri(@"/images/print-accept.png", UriKind.Relative));
                            psearItem = null;
                        }
                        else
                        {
                            imgprintgroup.Source = new BitmapImage(new Uri(@"/images/print-group.png", UriKind.Relative));
                        }
                        Int32 Photo_PKey = ((DigiPhoto.LstMyItems)(lstImages.SelectedItem)).PhotoId;
                        unlockImageId = Photo_PKey;
                    }
                }
                if (lstImages.Items.Count > 0 && lstImages.SelectedIndex == -1)
                {
                    lstImages.SelectedIndex = 0;
                    ListBoxItem listBoxItem = (ListBoxItem)lstImages.ItemContainerGenerator.ContainerFromItem(lstImages.Items[0]);
                    lstImages.SelectedItem = listBoxItem;
                    lstImages.ScrollIntoView(lstImages.SelectedItem);
                    listBoxItem.Focus();
                }
                flgGridWithoutPreview = false;
                #region Creating dynamic grid as per the vertical / horizontal
                if (ExampleStackPanel.Visibility != Visibility.Collapsed)
                {
                    mainFrame.UpdateLayout();

                    DynamicGrid.Children.Clear();
                    if (mainFrame.Height > mainFrame.Width)
                    {
                        CreateDynamicGrid(objWatermarkModel, 1);
                    }
                    else
                        CreateDynamicGrid(objWatermarkModel, 2);
                }
                #endregion
                if (btnchkpreview.IsChecked == true) // Added by Suraj. For update blur Image 30102021
                {
                    PreviewPhoto();
                }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
            finally
            {
            }
        }

        private void setPreview(BitmapImage objBitmap, bool GroupView, bool defaultview)
        {
            try
            {
                ClientView window = null;

                foreach (Window wnd in Application.Current.Windows)
                {
                    if (wnd.Title == "ClientView")
                    {
                        window = (ClientView)wnd;
                    }
                }

                if (window != null)
                {
                    window.WindowStartupLocation = System.Windows.WindowStartupLocation.Manual;
                }
                else
                {
                    window = new ClientView();
                    window.WindowStartupLocation = System.Windows.WindowStartupLocation.Manual;
                }

                if (RobotImageLoader.GroupImages.Count < 0)
                {
                    window.DefaultView = true;
                    window.GroupView = false;
                }
                else if (RobotImageLoader.GroupImages.Count > 0)
                {
                    window.DefaultView = false;
                    window.GroupView = true;
                }
                else
                {
                    window.GroupView = false;
                    window.DefaultView = false;
                    window.ChildImage = objBitmap;
                    window.Photoname = txtMainImage.Text;
                }

                window.SetEffect();

                System.Windows.Forms.Screen[] screens = System.Windows.Forms.Screen.AllScreens;
                if (screens.Length > 1)
                {
                    if (screens[0].Primary)
                    {
                        System.Windows.Forms.Screen s2 = System.Windows.Forms.Screen.AllScreens[1];
                        System.Drawing.Rectangle r2 = s2.WorkingArea;
                        window.Top = r2.Top;
                        window.Left = r2.Left;
                        window.Show();
                    }
                    else
                    {
                        System.Drawing.Rectangle r2 = screens[0].WorkingArea;
                        window.Top = r2.Top;
                        window.Left = r2.Left;
                        window.Show();
                    }
                }
                else
                {
                    window.Show();
                }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
            finally
            {
                MemoryManagement.FlushMemory();
            }
        }

        //btnStorybook_Click
        /// <summary>
        /// Added by Vins_Storybook_26Aug2021
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnStorybook_Click(object sender, RoutedEventArgs e)
        {
            ////changed by latika 
            ///
            ConfigBusiness conBiz = new ConfigBusiness();//////changed by latika for Screen saver (Marketing Image) should  run on guest view when operator is editing the image. The edited image should be shown when final edited image is ready

            if (conBiz.GETStopEditingImageClntvwBySubID(LoginUser.SubStoreId) == true)
            {
                isMarketingImgShow = 1;
                VisualBrush CB = new VisualBrush(ContentContainer);
                CompileEffectChanged(CB, -1);
            }

            ///end
            MediaStop();
            if (lstImages.SelectedItem != null)
            {
                try
                {
                    //Added towards clearing the group items which are not to be printed
                    if (vwGroup.Text == "View All" || pagename == "Saveback")
                    {
                        if (RobotImageLoader.NotPrintedImages != null && RobotImageLoader.NotPrintedImages.Count > 0)
                        {
                            RobotImageLoader.NotPrintedImages.Clear();
                        }
                        //RobotImageLoader.NotPrintedImages.Clear();
                        foreach (LstMyItems itx in RobotImageLoader.GroupImages)
                        {
                            var teItem = RobotImageLoader.PrintImages.Where(xs => xs.PhotoId == itx.PhotoId).FirstOrDefault();
                            if (teItem == null)
                            {
                                RobotImageLoader.NotPrintedImages.Add(itx);
                            }
                        }
                    }

                    //Check if item selected but not grouped..only grouped items need to be edited_VINS
                    //if (RobotImageLoader.GroupImages.Count > 0 && !RobotImageLoader.GroupImages.Contains((LstMyItems)lstImages.SelectedItem))
                    //{
                    //    ErrorHandler.ErrorHandler.LogFileWrite(RobotImageLoader.GroupImages.ToList().First().Name);
                    //    ListBoxItem listBoxItem = (ListBoxItem)lstImages.ItemContainerGenerator.ContainerFromItem(RobotImageLoader.GroupImages.ToList().First());
                    //    if (listBoxItem != null)
                    //    {
                    //        listBoxItem.Focus();
                    //        lstImages.SelectedItem = RobotImageLoader.GroupImages.ToList().First();
                    //    }
                    //}

                    LstMyItems selectedItem = (LstMyItems)lstImages.SelectedItem;
                    //Check if item selected but not grouped..only grouped items need to be edited_VINS
                    if (RobotImageLoader.GroupImages.Count > 0)
                    {
                        LstMyItems SBItemsExists = RobotImageLoader.GroupImages.Where(t => t.PhotoId == selectedItem.PhotoId).ToList().FirstOrDefault();
                        LstMyItems SBSelectedItem = RobotImageLoader.GroupImages.ToList().First();

                        if (SBItemsExists == null)
                        {
                            selectedItem = SBSelectedItem;
                        }
                    }

                    continueCalculating = false;
                    num = 0;
                    SBSingleEditModule _objMainWindow;

                    if (SBSingleEditModule.Instance != null)
                        _objMainWindow = SBSingleEditModule.Instance;
                    else
                        _objMainWindow = SBSingleEditModule.Instance;

                    if (pagename == "Saveback")
                        _objMainWindow.IsGoupped = "View All";
                    else
                        _objMainWindow.IsGoupped = vwGroup.Text;

                    FrameworkHelper.Common.ContantValueForMainWindow.RedEyeSize = .0105;
                    _objMainWindow.WindowState = System.Windows.WindowState.Maximized;
                    _objMainWindow.WindowStyle = System.Windows.WindowStyle.None;

                    //int photoId = ((DigiPhoto.LstMyItems)(lstImages.SelectedItem)).PhotoId;
                    //_objMainWindow.PhotoName = ((DigiPhoto.LstMyItems)(lstImages.SelectedItem)).Name;
                    //_objMainWindow.PhotoId = photoId;
                    //_objMainWindow.PhotoLocationId = ((DigiPhoto.LstMyItems)(lstImages.SelectedItem)).PhotoLocation;
                    int photoId = selectedItem.PhotoId;
                    _objMainWindow.PhotoName = selectedItem.Name;
                    _objMainWindow.PhotoId = photoId;
                    _objMainWindow.PhotoLocationId = selectedItem.PhotoLocation;
                    _objMainWindow.dbBrit = _objMainWindow._brighteff.Brightness = dbBrit;
                    _objMainWindow.dbContr = _objMainWindow._brighteff.Contrast = dbContr;
                    _objMainWindow.SetEffect();

                    PhotoBusiness photoBiz = new PhotoBusiness();
                    //PhotoInfo _objphoto = photoBiz.GetPhotoDetailsbyPhotoId(photoId);
                    PhotoInfo _objphoto = photoBiz.GetPhotoStoryBookDetailsbyPhotoId(photoId);
                    _objMainWindow.HotFolderPath = _objphoto.HotFolderPath;
                    _objMainWindow.BigThumnailFolderPath = Path.Combine(_objphoto.HotFolderPath, "Thumbnails_Big");
                    _objMainWindow.ThumnailFolderPath = Path.Combine(_objphoto.HotFolderPath, "Thumbnails");
                    _objMainWindow.CropFolderPath = Path.Combine(_objphoto.HotFolderPath, "Croped");
                    _objMainWindow.tempfilename = _objphoto.DG_Photos_FileName;
                    _objMainWindow.semiOrderProfileId = _objphoto.SemiOrderProfileId;


                    var SBItems = RobotImageLoader.GroupImages.Where(t => t.StoryBookId > 0).ToList().FirstOrDefault();
                    if (SBItems != null && _objphoto.StoryBookID == 0)
                    {
                        _objMainWindow.selectedStoryBookId = SBItems.StoryBookId;
                        _objMainWindow.selectedThemeId = 1;
                        _objMainWindow.selectedPageno = 1;
                    }
                    else
                    {
                        _objMainWindow.selectedStoryBookId = _objphoto.StoryBookID;
                        _objMainWindow.selectedThemeId = _objphoto.ThemeID;
                        _objMainWindow.selectedPageno = _objphoto.PageNo;
                    }

                    if (_objphoto != null)
                    {
                        if (!string.IsNullOrEmpty(_objphoto.DG_Photos_Effects))
                        {
                            _objMainWindow.ImageEffect = _objphoto.DG_Photos_Effects;
                        }
                        else
                        {
                            _objMainWindow.ImageEffect = "<image brightness = '" + dbBrit.ToString() + "' contrast = '" + dbContr.ToString() + "' Crop='##' colourvalue = '##' rotatewidth='##' rotateheight='##' rotate='##' flipMode='0' flipModeY='0' _centerx ='0' _centery='0'><effects sharpen='##' greyscale='0' digimagic='0' sepia='0' defog='0' underwater='0' emboss='0' invert = '0' granite='0' hue ='##' cartoon = '0' firstredeye= 'false' firstradius='.0105' firstcenterx='.5' firstcentery='.5' secondredeye= 'false' secondradius='.0105' secondcenterx='.5' secondcentery='.5'></effects></image>";
                        }

                        bool redeye, iscrop, isgreen = false;
                        if (_objphoto.DG_Photos_IsGreen != null)
                            isgreen = (bool)_objphoto.DG_Photos_IsGreen;
                        else
                            isgreen = false;

                        if (_objphoto.DG_Photos_IsCroped != null)
                        {
                            iscrop = (bool)_objphoto.DG_Photos_IsCroped;
                        }
                        else
                            iscrop = false;

                        if (_objphoto.DG_Photos_IsRedEye != null)
                        {
                            redeye = (bool)_objphoto.DG_Photos_IsRedEye;
                        }
                        else
                            redeye = false;


                        if (_objphoto.DG_Photos_CreatedOn != null)
                            _objMainWindow.DateFolder = _objphoto.DG_Photos_CreatedOn.ToString("yyyyMMdd");

                        try
                        {
                            _objMainWindow.ShowStripImages();
                            //if (_objMainWindow.selectedGuestPhotos != null)
                            //{
                            //    //_objMainWindow.selectedGuestPhotos = new ObservableCollection<LstMyItems>();
                            //    DigiPhoto.LstMyItems objItem = (DigiPhoto.LstMyItems)(RobotImageLoader.robotImages.Where(o => o.PhotoId == photoId).FirstOrDefault());
                            //    objItem.IsChecked = true;
                            //    if (!_objMainWindow.selectedGuestPhotos.Contains(objItem))
                            //    {
                            //        _objMainWindow.selectedGuestPhotos.Add(objItem);
                            //    }
                            //}
                            //else
                            //{
                            //    _objMainWindow.selectedGuestPhotos = new ObservableCollection<LstMyItems>();
                            //    DigiPhoto.LstMyItems objItem = (DigiPhoto.LstMyItems)(RobotImageLoader.robotImages.Where(o => o.PhotoId == photoId).FirstOrDefault());
                            //    objItem.IsChecked = true;
                            //    _objMainWindow.selectedGuestPhotos.Add(objItem);
                            //}
                            _objMainWindow.Onload(redeye, iscrop, isgreen, _objphoto.DG_Photos_Layering, _objphoto.IsGumRideShow);
                            _objMainWindow.ShowStripImages();
                            _objMainWindow.searchDetails = this.searchDetails;
                            _objMainWindow.Show();
                        }
                        catch (Exception ex)
                        {
                            string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                            ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                        }

                        //if (!LicenseDetails.IsLicenseDetailsRemovedFromCanvas(_objphoto.DG_Photos_pkey))
                        //{
                        if (_objphoto.DG_Photos_Layering.Contains("isLicenseProduct")) //This condition is applied to check if licence applied to selected photo and handling first time photo edit part//VinS
                        {
                            LicenseDetails.AddControlsToCanvasIfLicenseInfoInDB(_objMainWindow.dragCanvas, 6, _objphoto.DG_Photos_pkey);
                        }

                    }
                    this.Hide();

                    #region Added by Ajay on 4 July check if background applied to the Photo 
                    //------------Added condition to edit orgininal image for rolling image------22nd Oct 2018-------------
                    if (Convert.ToBoolean(_objphoto.DG_Photos_IsGreen))
                    {
                        string photoproductName = GetOrderProductName(photoId);
                        string[] PanoramicSizes = (new CommonBusiness()).GetPanoramicSizes();
                        string[] strFound = Array.FindAll(PanoramicSizes, str => str.ToLower().Contains(photoproductName));
                        if (strFound.Length != 0)
                        {
                            //Method to handle
                            _objMainWindow.EditAppliedBackgroundImage(photoproductName);


                        }
                    }
                    this.Hide();
                    #endregion
                }
                catch (Exception ex)
                {
                    string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                    ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                }
                finally
                {
                    // MemoryManagement.FlushMemory();
                }
            }
        }

        private void btnEdit_Click(object sender, RoutedEventArgs e)
        {
            ////changed by latika 
            ///
            ConfigBusiness conBiz = new ConfigBusiness();//////changed by latika for Screen saver (Marketing Image) should  run on guest view when operator is editing the image. The edited image should be shown when final edited image is ready

            if (conBiz.GETStopEditingImageClntvwBySubID(LoginUser.SubStoreId) == true)
            {
                isMarketingImgShow = 1;
                VisualBrush CB = new VisualBrush(ContentContainer);
                CompileEffectChanged(CB, -1);
            }

            List<LstMyItems> tempLstStripImages = new List<LstMyItems>();
            ///end
            MediaStop();
            if (lstImages.SelectedItem != null)
            {
                foreach (var item in RobotImageLoader.GroupImages)
                {
                    if (item.StoryBookId == 0)
                    {
                        tempLstStripImages.Add(item);
                    }
                }

                if (tempLstStripImages != null)
                {
                    RobotImageLoader.GroupImages.Clear();
                    RobotImageLoader.GroupImages = tempLstStripImages;
                }
                try
                {
                    //Added towards clearing the group items which are not to be printed
                    if (vwGroup.Text == "View All" || pagename == "Saveback")
                    {
                        if (RobotImageLoader.NotPrintedImages != null && RobotImageLoader.NotPrintedImages.Count > 0)
                        {
                            RobotImageLoader.NotPrintedImages.Clear();
                        }
                        //RobotImageLoader.NotPrintedImages.Clear();
                        foreach (LstMyItems itx in RobotImageLoader.GroupImages)
                        {
                            var teItem = RobotImageLoader.PrintImages.Where(xs => xs.PhotoId == itx.PhotoId).FirstOrDefault();
                            if (teItem == null)
                            {
                                RobotImageLoader.NotPrintedImages.Add(itx);
                            }
                        }
                    }

                    continueCalculating = false;
                    num = 0;
                    MainWindow _objMainWindow;

                    if (MainWindow.Instance != null)
                        _objMainWindow = MainWindow.Instance;
                    else
                        _objMainWindow = MainWindow.Instance;

                    if (pagename == "Saveback")
                        _objMainWindow.IsGoupped = "View All";
                    else
                        _objMainWindow.IsGoupped = vwGroup.Text;

                    FrameworkHelper.Common.ContantValueForMainWindow.RedEyeSize = .0105;
                    _objMainWindow.WindowState = System.Windows.WindowState.Maximized;
                    _objMainWindow.WindowStyle = System.Windows.WindowStyle.None;

                    int photoId = ((DigiPhoto.LstMyItems)(lstImages.SelectedItem)).PhotoId;
                    _objMainWindow.PhotoName = ((DigiPhoto.LstMyItems)(lstImages.SelectedItem)).Name;
                    _objMainWindow.PhotoId = photoId;
                    _objMainWindow.PhotoLocationId = ((DigiPhoto.LstMyItems)(lstImages.SelectedItem)).PhotoLocation;
                    _objMainWindow.dbBrit = _objMainWindow._brighteff.Brightness = dbBrit;
                    _objMainWindow.dbContr = _objMainWindow._brighteff.Contrast = dbContr;
                    _objMainWindow.SetEffect();

                    PhotoBusiness photoBiz = new PhotoBusiness();
                    PhotoInfo _objphoto = photoBiz.GetPhotoDetailsbyPhotoId(photoId);
                    _objMainWindow.HotFolderPath = _objphoto.HotFolderPath;
                    _objMainWindow.BigThumnailFolderPath = Path.Combine(_objphoto.HotFolderPath, "Thumbnails_Big");
                    _objMainWindow.ThumnailFolderPath = Path.Combine(_objphoto.HotFolderPath, "Thumbnails");
                    _objMainWindow.CropFolderPath = Path.Combine(_objphoto.HotFolderPath, "Croped");
                    _objMainWindow.tempfilename = _objphoto.DG_Photos_FileName;
                    _objMainWindow.semiOrderProfileId = _objphoto.SemiOrderProfileId;




                    if (_objphoto != null)
                    {
                        if (!string.IsNullOrEmpty(_objphoto.DG_Photos_Effects))
                        {
                            _objMainWindow.ImageEffect = _objphoto.DG_Photos_Effects;
                        }
                        else
                        {
                            _objMainWindow.ImageEffect = "<image brightness = '" + dbBrit.ToString() + "' contrast = '" + dbContr.ToString() + "' Crop='##' colourvalue = '##' rotatewidth='##' rotateheight='##' rotate='##' flipMode='0' flipModeY='0' _centerx ='0' _centery='0'><effects sharpen='##' greyscale='0' digimagic='0' sepia='0' defog='0' underwater='0' emboss='0' invert = '0' granite='0' hue ='##' cartoon = '0' firstredeye= 'false' firstradius='.0105' firstcenterx='.5' firstcentery='.5' secondredeye= 'false' secondradius='.0105' secondcenterx='.5' secondcentery='.5'></effects></image>";
                        }

                        bool redeye, iscrop, isgreen = false;
                        if (_objphoto.DG_Photos_IsGreen != null)
                            isgreen = (bool)_objphoto.DG_Photos_IsGreen;
                        else
                            isgreen = false;

                        if (_objphoto.DG_Photos_IsCroped != null)
                        {
                            iscrop = (bool)_objphoto.DG_Photos_IsCroped;
                        }
                        else
                            iscrop = false;

                        if (_objphoto.DG_Photos_IsRedEye != null)
                        {
                            redeye = (bool)_objphoto.DG_Photos_IsRedEye;
                        }
                        else
                            redeye = false;
                        if (_objphoto.DG_Photos_CreatedOn != null)
                            _objMainWindow.DateFolder = _objphoto.DG_Photos_CreatedOn.ToString("yyyyMMdd");

                        try
                        {
                            _objMainWindow.ShowStripImages();
                            _objMainWindow.Onload(redeye, iscrop, isgreen, _objphoto.DG_Photos_Layering, _objphoto.IsGumRideShow);
                            _objMainWindow.ShowStripImages();
                            _objMainWindow.searchDetails = this.searchDetails;
                            _objMainWindow.Show();
                        }
                        catch (Exception ex)
                        {
                            string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                            ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                        }

                        //if (!LicenseDetails.IsLicenseDetailsRemovedFromCanvas(_objphoto.DG_Photos_pkey))
                        //{
                        if (_objphoto.DG_Photos_Layering.Contains("isLicenseProduct")) //This condition is applied to check if licence applied to selected photo and handling first time photo edit part//VinS
                        {
                            LicenseDetails.AddControlsToCanvasIfLicenseInfoInDB(_objMainWindow.dragCanvas, 6, _objphoto.DG_Photos_pkey);
                        }

                    }
                    this.Hide();

                    #region Added by Ajay on 4 July check if background applied to the Photo 
                    //------------Added condition to edit orgininal image for rolling image------22nd Oct 2018-------------
                    if (Convert.ToBoolean(_objphoto.DG_Photos_IsGreen))
                    {
                        string photoproductName = GetOrderProductName(photoId);
                        string[] PanoramicSizes = (new CommonBusiness()).GetPanoramicSizes();
                        string[] strFound = Array.FindAll(PanoramicSizes, str => str.ToLower().Contains(photoproductName));
                        if (strFound.Length != 0)
                        {
                            //Method to handle
                            _objMainWindow.EditAppliedBackgroundImage(photoproductName);


                        }
                    }
                    this.Hide();
                    #endregion
                }
                catch (Exception ex)
                {
                    string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                    ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                }
                finally
                {
                    // MemoryManagement.FlushMemory();
                }
            }
        }
        private void btnLogout_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                AuditLog.AddUserLog(Common.LoginUser.UserId, (int)FrameworkHelper.ActionType.Logout, "Logged out at ");
                RobotImageLoader.IsZeroSearchNeeded = true;
                Login _objLogin = new Login();
                _objLogin.Show();
                CompileEffectChanged(null, -2);
                this.Close();
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
            finally
            {
                MemoryManagement.FlushMemory();
            }
        }
        private void btnSearchPhoto_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (returntoHome)
                {
                    MediaStop();
                    RobotImageLoader.IsZeroSearchNeeded = true;
                    vwGroup.Text = "View Group";
                    GetMktImgInfo();
                    ClientView window = null;
                    foreach (Window wnd in Application.Current.Windows)
                    {
                        if (wnd.Title == "ClientView")
                        {
                            window = (ClientView)wnd;
                        }
                    }
                    if (window == null)
                    {
                        window = new ClientView();
                        window.WindowStartupLocation = System.Windows.WindowStartupLocation.Manual;
                    }
                    window.GroupView = false;
                    window.DefaultView = false;
                    if (!(MktImgPath == "" || mktImgTime == 0))
                    {
                        window.instructionVideo.Visibility = System.Windows.Visibility.Visible;
                        window.instructionVideo.Play();
                        //window.instructionVideoBlur.Visibility = System.Windows.Visibility.Visible;
                        //window.instructionVideoBlur.Play();
                    }
                    else
                    {
                        window.imgDefault.Visibility = System.Windows.Visibility.Visible;
                        //window.imgDefaultBlur.Visibility = System.Windows.Visibility.Visible;
                    }
                    window.testR.Fill = null;
                    window.DefaultView = true;
                    if (window.instructionVideo.Visibility == System.Windows.Visibility.Visible)
                        window.instructionVideo.Play();
                    else
                        window.instructionVideo.Pause();

                    //if (window.instructionVideoBlur.Visibility == System.Windows.Visibility.Visible)
                    //    window.instructionVideoBlur.Play();
                    //else
                    //    window.instructionVideoBlur.Pause();

                    SearchByPhoto _objsearchPhoto;
                    Window win = null;
                    foreach (Window wnd in Application.Current.Windows)
                    {
                        if (wnd.Title == "Search")
                        {
                            win = wnd as SearchByPhoto;
                            break;
                        }
                    }
                    if (win != null)
                    {
                        _objsearchPhoto = (SearchByPhoto)win;
                    }
                    else
                    {
                        _objsearchPhoto = new SearchByPhoto();
                    }
                    RobotImageLoader.PageName = "SearchResult";
                    _objsearchPhoto.Show();
                    continueCalculating = false;
                    _objsearchPhoto.LoadData();
                    this.Hide();
                }

                else
                {
                    returntoHome = true;
                }
            }

            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }
        private void btnWithoutPreview_Click(object sender, RoutedEventArgs e)
        {
            MediaStop();
            continueCalculating = false;
            if (scrollIndexWithoutPreview == 9)
            {
                ConfigBusiness configBiz = new ConfigBusiness();
                ConfigurationInfo _PGobjdata = configBiz.GetConfigurationData(LoginUser.SubStoreId);
                if (_PGobjdata != null)
                {
                    scrollIndexWithoutPreview = Convert.ToInt32(_PGobjdata.DG_PageCountGrid);
                    scrollIndexWithPreview = scrollIndexWithoutPreview;
                    RobotImageLoader.thumbSet = scrollIndexWithPreview;
                }
            }
            imgwithPreview.Source = new BitmapImage(new Uri(@"/images/thumbnailview1.png", UriKind.Relative));
            imgwithoutPreview.Source = new BitmapImage(new Uri(@"images/16blocks_red.png", UriKind.Relative));
            imgwithoutPreview9.Source = new BitmapImage(new Uri(@"/images/9Blocks_black.png", UriKind.Relative));
            RobotImageLoader.Is9ImgViewActive = false;
            RobotImageLoader.Is16ImgViewActive = true;
            RobotImageLoader.IsPreview9or16active = true;
            RobotImageLoader.IsPreviewModeActive = false;

            if (RobotImageLoader.robotImages == null || RobotImageLoader.robotImages.Count <= 0)
                return;


            //RobotImageLoader.StartIndex = RobotImageLoader.robotImages[0].PhotoId;
            RobotImageLoader.StartIndex = (int)RobotImageLoader.robotImages[0].DisplayOrder;
            if (RobotImageLoader.GroupImages != null && RobotImageLoader.GroupImages.Count > 0)
                RobotImageLoader.GroupImages.ForEach(t => { t.GridMainHeight = 140; t.GridMainWidth = 170; t.GridMainRowHeight1 = 90; t.GridMainRowHeight2 = 60; });
            if (RobotImageLoader.PrintImages != null && RobotImageLoader.PrintImages.Count > 0)
                RobotImageLoader.PrintImages.ForEach(t => { t.GridMainHeight = 140; t.GridMainWidth = 170; t.GridMainRowHeight1 = 90; t.GridMainRowHeight2 = 60; });

            if ((RobotImageLoader.SearchCriteria == "TimeWithQrcode"))
            {
                ConfigBusiness configBiz = new ConfigBusiness();
                ConfigurationInfo _PGobjdata = configBiz.GetConfigurationData(LoginUser.SubStoreId);
                if (_PGobjdata != null)
                {
                    searchDetails.PageSize = _PGobjdata.DG_PageCountGrid.ToInt32();
                }
                else
                {
                    searchDetails.PageSize = 16;
                }
                RobotImageLoader.LoadImages(searchDetails);
            }
            else
            {
                RobotImageLoader.LoadImages();
            }
            if (vwGroup.Text == "View Group")
            {
                try
                {
                    if (txtSelectImages.Text.Contains("Print Grouped") && txtSelectImages.Visibility == Visibility.Visible)
                    {
                        ScrollViewer.SetVerticalScrollBarVisibility(lstImages, ScrollBarVisibility.Visible);
                        SetMessageText("EditPrintGroupedView");
                        SPSelectAll.Visibility = System.Windows.Visibility.Hidden;
                        if (lstImages.Items.Count > 0 && lstImages != null)
                        {
                            lstImages.Items.Clear();
                        }
                        ////For showing the scrollbars and hiding the paginate buttons
                        btnprev.Visibility = System.Windows.Visibility.Collapsed;
                        btnnext.Visibility = System.Windows.Visibility.Collapsed;
                        ScrollViewer.SetVerticalScrollBarVisibility(lstImages, ScrollBarVisibility.Visible);
                        ///////////////
                        var listPrintedGrouped = (from p in RobotImageLoader.PrintImages
                                                  join g in RobotImageLoader.GroupImages on p.PhotoId equals g.PhotoId into ALLCOLUMNS
                                                  from entry in ALLCOLUMNS
                                                  select entry);

                        foreach (var photoItem in listPrintedGrouped)
                        {
                            lstImages.Items.Add(photoItem);

                            if (photoItem.PhotoId == RobotImageLoader.UniquePhotoId)
                            {
                                try
                                {
                                    lstImages.SelectedItem = photoItem;
                                    ListBoxItem listBoxItem = (ListBoxItem)lstImages.ItemContainerGenerator.ContainerFromItem(lstImages.SelectedItem);
                                    listBoxItem.Focus();
                                    lstImages.ScrollIntoView(lstImages.SelectedItem);
                                }
                                catch (Exception ex)
                                {
                                    string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                                    ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                                }
                            }
                        }
                    }
                    else
                    {
                        LoadImages();
                        if (string.Compare(RobotImageLoader.SearchCriteria, "Time", true) == 0 || string.Compare(RobotImageLoader.SearchCriteria, "QRCODE", true) == 0 || string.Compare(RobotImageLoader.SearchCriteria, "TimeWithQrcode", true) == 0)
                            ScrollViewer.SetVerticalScrollBarVisibility(lstImages, ScrollBarVisibility.Hidden);
                        else if (Convert.ToInt64(RobotImageLoader.RFID) > 0)
                            ScrollViewer.SetVerticalScrollBarVisibility(lstImages, ScrollBarVisibility.Hidden);
                        else if (RobotImageLoader.RFID != "0")
                            ScrollViewer.SetVerticalScrollBarVisibility(lstImages, ScrollBarVisibility.Visible);
                        else
                            ScrollViewer.SetVerticalScrollBarVisibility(lstImages, ScrollBarVisibility.Hidden);
                        SetMessageText("Grouped");
                        FillImageList();
                        CheckSelectAllGroup();
                    }
                    IMGFrame.Visibility = Visibility.Collapsed;
                    Grid.SetColumnSpan(thumbPreview, 2);
                    Grid.SetColumn(thumbPreview, 0);
                    thumbPreview.Margin = new Thickness(0);
                }
                catch (Exception ex)
                {
                    string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                    ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                }
                finally
                {
                    MemoryManagement.FlushMemory();
                }
            }
            else
            {
                LoadImages();
                SetMessageText("PrintGrouped");
                flgGridWithoutPreview = true;
                Int64 Photo_PKey = 0;
                continueCalculating = false;
                if (RobotImageLoader.GroupImages.Count > 0)
                {

                    if (lstImages.Items.Count > 0 && lstImages != null)
                    {
                        lstImages.Items.Clear();
                    }
                    ////For showing the scrollbars and hiding the paginate buttons
                    btnprev.Visibility = System.Windows.Visibility.Collapsed;
                    btnnext.Visibility = System.Windows.Visibility.Collapsed;
                    ScrollViewer.SetVerticalScrollBarVisibility(lstImages, ScrollBarVisibility.Visible);
                    ///////////////
                    foreach (var photoItem in RobotImageLoader.GroupImages)
                    {
                        var existItem = RobotImageLoader.PrintImages.Where(t => t.PhotoId == photoItem.PhotoId).FirstOrDefault();
                        if (existItem != null)
                        {
                            photoItem.PrintGroup = new BitmapImage(new Uri(@"/images/print-accept.png", UriKind.Relative));
                        }
                        else
                        {
                            photoItem.PrintGroup = new BitmapImage(new Uri(@"/images/print-group.png", UriKind.Relative));
                        }
                        PhotoBusiness phBiz = new PhotoBusiness();
                        if (phBiz.GetModeratePhotos(photoItem.PhotoId))
                        {
                            splockedImages.Visibility = Visibility.Collapsed;
                            spaddtoalbumnunlockedImages.Visibility = Visibility.Visible;
                            spunlockedImages.Visibility = Visibility.Visible;
                            if (RobotImageLoader.GroupImages.First().MediaType == 1)
                                unlockImage = new BitmapImage(new Uri(RobotImageLoader.GroupImages.First().BigThumbnailPath, UriKind.Absolute));
                            btnEdit.IsEnabled = false;
                            photoItem.IsPassKeyVisible = Visibility.Visible;
                            photoItem.BmpImageGroup = null;
                            photoItem.FilePath = photoItem.HotFolderPath + "/Locked.png";
                            photoItem.IsLocked = Visibility.Collapsed;
                            photoItem.IsPassKeyVisible = Visibility.Visible;
                        }
                        photoItem.GridMainHeight = 140;
                        photoItem.GridMainWidth = 170;
                        photoItem.GridMainRowHeight1 = 90;
                        photoItem.GridMainRowHeight2 = 60;

                        lstImages.Items.Add(photoItem);

                        if (photoItem.PhotoId == RobotImageLoader.UniquePhotoId)
                        {
                            try
                            {
                                lstImages.SelectedItem = photoItem;
                                ListBoxItem listBoxItem = (ListBoxItem)lstImages.ItemContainerGenerator.ContainerFromItem(lstImages.SelectedItem);
                                listBoxItem.Focus();
                                lstImages.ScrollIntoView(lstImages.SelectedItem);
                            }
                            catch (Exception ex)
                            {
                                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                            }
                        }
                    }
                    if (vwGroup.Text == "View All")
                    {
                        if (lstImages != null && lstImages.SelectedItem == null)
                        {
                            lstImages.SelectedItem = lstImages.Items[0];
                            ListBoxItem listBoxItem = (ListBoxItem)lstImages.ItemContainerGenerator.ContainerFromItem(lstImages.SelectedItem);
                            listBoxItem.Focus();
                            lstImages.ScrollIntoView(lstImages.SelectedItem);
                        }
                    }

                    txtMainImage.Text = Convert.ToString(RobotImageLoader.GroupImages.First().Name);
                    CurrentBitmapImage = new BitmapImage(new Uri(RobotImageLoader.GroupImages.First().FilePath));
                    CurrentBitmapImage.Freeze();
                    _currentImage = RobotImageLoader.GroupImages.First().Name;
                    _objBitmap = new BitmapImage();
                    _objBitmap = (System.Windows.Media.Imaging.BitmapImage)(new BitmapImage(new Uri(RobotImageLoader.GroupImages.First().FilePath)));
                    vwGroup.Text = "View All";
                    isStorybookBack = "";
                    grdSelectAll.Visibility = Visibility.Visible;
                    ImgAddToGroup.Source = new BitmapImage(new Uri(@"/images/view-accept.png", UriKind.Relative));
                    ImgAddToGroup.ToolTip = "Remove from group";
                    /////////Visibility false of image and group should be shown to user.
                    PhotoBusiness photoBiz = new PhotoBusiness();
                    Photo_PKey = RobotImageLoader.GroupImages.First().PhotoId;//objdbLayer.GetPhotoIDByRFFID();
                    unlockImageId = Photo_PKey;
                    if (photoBiz.GetModeratePhotos(Photo_PKey))
                    {
                        splockedImages.Visibility = Visibility.Collapsed;
                        spaddtoalbumnunlockedImages.Visibility = Visibility.Visible;
                        spunlockedImages.Visibility = Visibility.Visible;
                        if (RobotImageLoader.GroupImages.First().MediaType == 1)
                            unlockImage = new BitmapImage(new Uri(RobotImageLoader.GroupImages.First().BigThumbnailPath, UriKind.Absolute));
                        btnEdit.IsEnabled = false;
                    }
                    else
                    {
                        splockedImages.Visibility = Visibility.Visible;
                        spaddtoalbumnunlockedImages.Visibility = Visibility.Collapsed;
                        spunlockedImages.Visibility = Visibility.Collapsed;
                        if (RobotImageLoader.GroupImages.First().MediaType == 1)
                            unlockImage = new BitmapImage(new Uri(RobotImageLoader.GroupImages.First().BigThumbnailPath, UriKind.Absolute));
                        btnEdit.IsEnabled = true;
                    }
                }
                //to check mark the img
                CheckForAllImgSelectToPrint();

                IMGFrame.Visibility = Visibility.Collapsed;
                Grid.SetColumnSpan(thumbPreview, 2);
                Grid.SetColumn(thumbPreview, 0);
                thumbPreview.Margin = new Thickness(0);
                FillImageList();
            }
        }
        private void btnWithoutPreview9_Click(object sender, RoutedEventArgs e)
        {
            MediaStop();
            continueCalculating = false;
            scrollIndexWithoutPreview = 9;
            scrollIndexWithPreview = 9;
            RobotImageLoader.thumbSet = 9;
            //RobotImageLoader.StartIndex = RobotImageLoader.robotImages.Count() > 0 ? RobotImageLoader.robotImages[0].PhotoId : 0;
            RobotImageLoader.StartIndex = RobotImageLoader.robotImages.Count() > 0 ? (int)RobotImageLoader.robotImages[0].DisplayOrder : 0;
            RobotImageLoader.Is9ImgViewActive = true;
            RobotImageLoader.IsPreview9or16active = true;
            RobotImageLoader.Is16ImgViewActive = false;
            RobotImageLoader.IsPreviewModeActive = false;
            imgwithPreview.Source = new BitmapImage(new Uri(@"/images/thumbnailview1.png", UriKind.Relative));
            imgwithoutPreview.Source = new BitmapImage(new Uri(@"images/16blocks_black.png", UriKind.Relative));
            imgwithoutPreview9.Source = new BitmapImage(new Uri(@"/images/9Blocks_red.png", UriKind.Relative));

            if (RobotImageLoader.robotImages == null || RobotImageLoader.robotImages.Count <= 0)
                return;
            //RobotImageLoader.StartIndex = RobotImageLoader.robotImages.Count() > 0 ? RobotImageLoader.robotImages[0].PhotoId : 0;
            RobotImageLoader.StartIndex = RobotImageLoader.robotImages.Count() > 0 ? (int)RobotImageLoader.robotImages[0].DisplayOrder : 0;
            if (RobotImageLoader.GroupImages != null && RobotImageLoader.GroupImages.Count > 0)
                RobotImageLoader.GroupImages.ForEach(t => { t.GridMainHeight = 190; t.GridMainWidth = 226; t.GridMainRowHeight1 = 140; t.GridMainRowHeight2 = 50; });
            if (RobotImageLoader.PrintImages != null && RobotImageLoader.PrintImages.Count > 0)
                RobotImageLoader.PrintImages.ForEach(t => { t.GridMainHeight = 190; t.GridMainWidth = 226; t.GridMainRowHeight1 = 140; t.GridMainRowHeight2 = 50; });
            if ((RobotImageLoader.SearchCriteria == "TimeWithQrcode"))
            {
                searchDetails.PageSize = 9;
                RobotImageLoader.LoadImages(searchDetails);
            }
            else
            {
                RobotImageLoader.LoadImages();
            }

            if (vwGroup.Text == "View Group")
            {
                try
                {
                    FillImageList();
                    if (txtSelectImages.Text.Contains("Print Grouped") && txtSelectImages.Visibility == Visibility.Visible)
                    {
                        ScrollViewer.SetVerticalScrollBarVisibility(lstImages, ScrollBarVisibility.Visible);
                        SetMessageText("EditPrintGroupedView");
                        SPSelectAll.Visibility = System.Windows.Visibility.Hidden;

                        if (lstImages.Items.Count > 0 && lstImages != null)
                        {
                            lstImages.Items.Clear();
                        }
                        ////For showing the scrollbars and hiding the paginate buttons
                        btnprev.Visibility = System.Windows.Visibility.Collapsed;
                        btnnext.Visibility = System.Windows.Visibility.Collapsed;
                        ScrollViewer.SetVerticalScrollBarVisibility(lstImages, ScrollBarVisibility.Visible);

                        var listPrintedGrouped = (from p in RobotImageLoader.PrintImages
                                                  join g in RobotImageLoader.GroupImages on p.PhotoId equals g.PhotoId into ALLCOLUMNS
                                                  from entry in ALLCOLUMNS
                                                  select entry);

                        foreach (var photoItem in listPrintedGrouped)
                        {
                            lstImages.Items.Add(photoItem);

                            if (photoItem.PhotoId == RobotImageLoader.UniquePhotoId)
                            {
                                try
                                {
                                    lstImages.SelectedItem = photoItem;
                                    ListBoxItem listBoxItem = (ListBoxItem)lstImages.ItemContainerGenerator.ContainerFromItem(lstImages.SelectedItem);
                                    listBoxItem.Focus();
                                    lstImages.ScrollIntoView(lstImages.SelectedItem);
                                }
                                catch (Exception ex)
                                {
                                    string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                                    ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                                }
                            }
                        }
                    }
                    else
                    {
                        LoadImages();
                        if (string.Compare(RobotImageLoader.SearchCriteria, "Time", true) == 0 || string.Compare(RobotImageLoader.SearchCriteria, "QRCODE", true) == 0 || string.Compare(RobotImageLoader.SearchCriteria, "TimeWithQrcode", true) == 0)
                            ScrollViewer.SetVerticalScrollBarVisibility(lstImages, ScrollBarVisibility.Hidden);
                        else if (Convert.ToInt64(RobotImageLoader.RFID) > 0)
                            ScrollViewer.SetVerticalScrollBarVisibility(lstImages, ScrollBarVisibility.Hidden);
                        else if (RobotImageLoader.RFID != "0")
                            ScrollViewer.SetVerticalScrollBarVisibility(lstImages, ScrollBarVisibility.Visible);
                        else
                            ScrollViewer.SetVerticalScrollBarVisibility(lstImages, ScrollBarVisibility.Hidden);
                        SetMessageText("Grouped");
                        CheckSelectAllGroup();
                    }
                    IMGFrame.Visibility = Visibility.Collapsed;
                    Grid.SetColumnSpan(thumbPreview, 2);
                    Grid.SetColumn(thumbPreview, 0);
                    thumbPreview.Margin = new Thickness(0);
                }
                catch (Exception ex)
                {
                    string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                    ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                }
                finally
                {
                    MemoryManagement.FlushMemory();
                }
            }
            else
            {
                LoadImages();
                IMGFrame.Visibility = Visibility.Collapsed;
                SetMessageText("PrintGrouped");
                flgGridWithoutPreview = true;
                Int64 Photo_PKey = 0;
                continueCalculating = false;
                if (RobotImageLoader.GroupImages.Count > 0)
                {

                    if (lstImages.Items.Count > 0 && lstImages != null)
                    {
                        lstImages.Items.Clear();
                    }
                    ////For showing the scrollbars and hiding the paginate buttons
                    btnprev.Visibility = System.Windows.Visibility.Collapsed;
                    btnnext.Visibility = System.Windows.Visibility.Collapsed;
                    ScrollViewer.SetVerticalScrollBarVisibility(lstImages, ScrollBarVisibility.Visible);
                    List<ModratePhotoInfo> moderatePhotosList = new List<ModratePhotoInfo>();
                    PhotoBusiness photoBiz = new PhotoBusiness();
                    moderatePhotosList = photoBiz.GetModeratePhotos();

                    foreach (var photoItem in RobotImageLoader.GroupImages)
                    {
                        var existItem = RobotImageLoader.PrintImages.Where(t => t.PhotoId == photoItem.PhotoId).FirstOrDefault();
                        if (existItem != null)
                        {
                            photoItem.PrintGroup = new BitmapImage(new Uri(@"/images/print-accept.png", UriKind.Relative));
                        }
                        else
                        {
                            photoItem.PrintGroup = new BitmapImage(new Uri(@"/images/print-group.png", UriKind.Relative));
                        }
                        var moderateItem = moderatePhotosList.AsEnumerable().Where(x => x.DG_Mod_Photo_ID == photoItem.PhotoId);
                        if (moderateItem != null && moderateItem.Count() > 0)
                        {
                            splockedImages.Visibility = Visibility.Collapsed;
                            spaddtoalbumnunlockedImages.Visibility = Visibility.Visible;
                            spunlockedImages.Visibility = Visibility.Visible;
                            if (RobotImageLoader.GroupImages.First().MediaType == 1)
                                unlockImage = new BitmapImage(new Uri(RobotImageLoader.GroupImages.First().BigThumbnailPath, UriKind.Absolute));
                            btnEdit.IsEnabled = false;
                            photoItem.IsPassKeyVisible = Visibility.Visible;
                            photoItem.BmpImageGroup = null;
                            photoItem.FilePath = photoItem.HotFolderPath + "/Locked.png";
                            photoItem.IsLocked = Visibility.Collapsed;
                            photoItem.IsPassKeyVisible = Visibility.Visible;
                        }

                        lstImages.Items.Add(photoItem);

                        if (photoItem.PhotoId == RobotImageLoader.UniquePhotoId)
                        {
                            try
                            {
                                lstImages.SelectedItem = photoItem;
                                ListBoxItem listBoxItem = (ListBoxItem)lstImages.ItemContainerGenerator.ContainerFromItem(lstImages.SelectedItem);
                                listBoxItem.Focus();
                                lstImages.ScrollIntoView(lstImages.SelectedItem);
                            }
                            catch (Exception ex)
                            {
                                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                            }
                        }
                    }
                    if (vwGroup.Text == "View All")
                    {
                        if (lstImages != null && lstImages.SelectedItem == null)
                        {
                            lstImages.SelectedItem = lstImages.Items[0];
                            ListBoxItem listBoxItem = (ListBoxItem)lstImages.ItemContainerGenerator.ContainerFromItem(lstImages.SelectedItem);
                            listBoxItem.Focus();
                            lstImages.ScrollIntoView(lstImages.SelectedItem);
                        }
                    }
                    //--
                    txtMainImage.Text = Convert.ToString(RobotImageLoader.GroupImages.First().Name);
                    CurrentBitmapImage = new BitmapImage(new Uri(RobotImageLoader.GroupImages.First().FilePath));
                    CurrentBitmapImage.Freeze();
                    _currentImage = RobotImageLoader.GroupImages.First().Name;
                    _objBitmap = new BitmapImage();
                    _objBitmap = (System.Windows.Media.Imaging.BitmapImage)(new BitmapImage(new Uri(RobotImageLoader.GroupImages.First().FilePath)));
                    vwGroup.Text = "View All";
                    isStorybookBack = "";
                    grdSelectAll.Visibility = Visibility.Visible;
                    ImgAddToGroup.Source = new BitmapImage(new Uri(@"/images/view-accept.png", UriKind.Relative));
                    ImgAddToGroup.ToolTip = "Remove from group";
                    Photo_PKey = RobotImageLoader.GroupImages.First().PhotoId;
                    unlockImageId = Photo_PKey;
                    if (photoBiz.GetModeratePhotos(Photo_PKey))
                    {
                        splockedImages.Visibility = Visibility.Collapsed;
                        spaddtoalbumnunlockedImages.Visibility = Visibility.Visible;
                        spunlockedImages.Visibility = Visibility.Visible;
                        if (RobotImageLoader.GroupImages.First().MediaType == 1)
                            unlockImage = new BitmapImage(new Uri(RobotImageLoader.GroupImages.First().BigThumbnailPath, UriKind.Absolute));
                        btnEdit.IsEnabled = false;
                    }
                    else
                    {
                        splockedImages.Visibility = Visibility.Visible;
                        spaddtoalbumnunlockedImages.Visibility = Visibility.Collapsed;
                        spunlockedImages.Visibility = Visibility.Collapsed;
                        if (RobotImageLoader.GroupImages.First().MediaType == 1)
                            unlockImage = new BitmapImage(new Uri(RobotImageLoader.GroupImages.First().BigThumbnailPath, UriKind.Absolute));
                        btnEdit.IsEnabled = true;
                    }
                }
                //to check mark the img
                CheckForAllImgSelectToPrint();

                Grid.SetColumnSpan(thumbPreview, 2);
                Grid.SetColumn(thumbPreview, 0);
                thumbPreview.Margin = new Thickness(0);
                FillImageList();
            }
        }
        private void btnWithPreviewActive_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (RobotImageLoader.robotImages.Count <= 0)
                    return;

                RobotImageLoader.IsPreview9or16active = false;
                if (scrollIndexWithoutPreview == 9)
                {
                    ConfigBusiness configBiz = new ConfigBusiness();
                    ConfigurationInfo _PGobjdata = configBiz.GetConfigurationData(LoginUser.SubStoreId);
                    if (_PGobjdata != null)
                    {
                        scrollIndexWithoutPreview = Convert.ToInt32(_PGobjdata.DG_PageCountGrid);
                        scrollIndexWithPreview = scrollIndexWithoutPreview;
                        RobotImageLoader.thumbSet = scrollIndexWithPreview;
                    }
                }

                foreach (LstMyItems o in lstImages.Items)
                {
                    o.GridMainHeight = 140;
                    o.GridMainWidth = 170;
                    o.GridMainRowHeight1 = 90;
                    o.GridMainRowHeight2 = 60;
                }
                if (RobotImageLoader.GroupImages != null && RobotImageLoader.GroupImages.Count > 0)
                    RobotImageLoader.GroupImages.ForEach(t => { t.GridMainHeight = 140; t.GridMainWidth = 170; t.GridMainRowHeight1 = 90; t.GridMainRowHeight2 = 60; });
                if (RobotImageLoader.PrintImages != null && RobotImageLoader.PrintImages.Count > 0)
                    RobotImageLoader.PrintImages.ForEach(t => { t.GridMainHeight = 140; t.GridMainWidth = 170; t.GridMainRowHeight1 = 90; t.GridMainRowHeight2 = 60; });
                if (RobotImageLoader.robotImages != null && RobotImageLoader.robotImages.Count > 0)
                    RobotImageLoader.robotImages.ForEach(t => { t.GridMainHeight = 140; t.GridMainWidth = 170; t.GridMainRowHeight1 = 90; t.GridMainRowHeight2 = 60; });

                ScrollViewer.SetVerticalScrollBarVisibility(lstImages, ScrollBarVisibility.Visible);
                flgGridWithoutPreview = false;
                RobotImageLoader.IsPreviewModeActive = true;

                if (RobotImageLoader.robotImages.Count > 0)
                    IMGFrame.Visibility = Visibility.Visible;
                else
                    IMGFrame.Visibility = Visibility.Collapsed;
                Grid.SetColumnSpan(thumbPreview, 1);
                Grid.SetColumn(thumbPreview, 1);
                thumbPreview.Margin = new Thickness(0, 0, -77.5, 8);
                imgwithPreview.Source = new BitmapImage(new Uri(@"/images/thumbnailview1_active.png", UriKind.Relative));
                imgwithoutPreview.Source = new BitmapImage(new Uri(@"images/16blocks_black.png", UriKind.Relative));
                imgwithoutPreview9.Source = new BitmapImage(new Uri(@"/images/9Blocks_black.png", UriKind.Relative));
                if (lstImages.SelectedItem != null)
                {
                    ListBoxItem listBoxItem = (ListBoxItem)lstImages.ItemContainerGenerator.ContainerFromItem(lstImages.SelectedItem);
                    listBoxItem.Focus();
                    lstImages.ScrollIntoView(lstImages.SelectedItem);
                    LstMyItems TempItem = (LstMyItems)lstImages.SelectedItem;
                    if (TempItem.MediaType == 2 || TempItem.MediaType == 3)
                    {
                        img.Visibility = Visibility.Hidden;
                        vidoriginal.Visibility = Visibility.Visible;
                        string vidPath = TempItem.FileName;
                        if (!String.IsNullOrEmpty(vidPath))
                        {
                            if (TempItem.MediaType == 2)
                            {
                                using (FileStream fileStream = File.OpenRead(System.IO.Path.Combine(TempItem.HotFolderPath, "Videos", TempItem.CreatedOn.ToString("yyyyMMdd"), TempItem.FileName)))
                                {
                                    vsMediaFileName = fileStream.Name;
                                }
                            }
                            else
                            {
                                using (FileStream fileStream = File.OpenRead(System.IO.Path.Combine(TempItem.HotFolderPath, "ProcessedVideos", TempItem.CreatedOn.ToString("yyyyMMdd"), TempItem.FileName)))
                                {
                                    vsMediaFileName = fileStream.Name;
                                }
                            }
                        }
                        Dispatcher.Invoke(new Action(() => MediaPlay()));
                        txtMainVideo.Text = TempItem.Name;
                    }
                    else
                    {
                        img.Visibility = Visibility.Visible;
                        vidoriginal.Visibility = Visibility.Hidden;
                        MediaStop();
                    }
                    txtMainImage.Text = ((DigiPhoto.LstMyItems)(lstImages.SelectedItem)).Name;
                    RobotImageLoader.PhotoId = ((DigiPhoto.LstMyItems)(lstImages.SelectedItem)).Name;
                    RobotImageLoader.UniquePhotoId = ((DigiPhoto.LstMyItems)(lstImages.SelectedItem)).PhotoId;
                    _currentImage = ((DigiPhoto.LstMyItems)(lstImages.SelectedItem)).Name;
                    _currentImageId = ((DigiPhoto.LstMyItems)(lstImages.SelectedItem)).PhotoId;
                    var searItem = RobotImageLoader.GroupImages.Where(t => t.PhotoId == _currentImageId).FirstOrDefault();
                    if (searItem != null)
                    {
                        ImgAddToGroup.Source = new BitmapImage(new Uri(@"/images/view-accept.png", UriKind.Relative));
                        btnImageAddToGroup.ToolTip = "Remove from group";
                        btnImageAddToGroup.CommandParameter = "Remove from group";
                        searItem = null;
                    }
                    else
                    {
                        ImgAddToGroup.Source = new BitmapImage(new Uri(@"/images/view-GROUP.png", UriKind.Relative));
                        btnImageAddToGroup.ToolTip = "Add to group";
                        btnImageAddToGroup.CommandParameter = "Add to group";
                    }
                    var psearItem = RobotImageLoader.PrintImages.Where(t => t.PhotoId == _currentImageId).FirstOrDefault();
                    if (psearItem != null)
                    {
                        imgprintgroup.Source = new BitmapImage(new Uri(@"/images/print-accept.png", UriKind.Relative));
                        psearItem = null;
                    }
                    else
                    {
                        imgprintgroup.Source = new BitmapImage(new Uri(@"/images/print-group.png", UriKind.Relative));
                    }
                    Int32 Photo_PKey = ((DigiPhoto.LstMyItems)(lstImages.SelectedItem)).PhotoId;
                    unlockImageId = Photo_PKey;
                    var result = RobotImageLoader.LstUnlocknames.Where(x => x.Equals(Photo_PKey.ToString()));
                    PhotoBusiness phBiz = new PhotoBusiness();
                    if (phBiz.GetModeratePhotos(Photo_PKey) && (result.Count() == 0))
                    {
                        btnEdit.IsEnabled = false;
                        splockedImages.Visibility = Visibility.Visible;

                        spaddtoalbumnunlockedImages.Visibility = Visibility.Collapsed;
                        spunlockedImages.Visibility = Visibility.Collapsed;
                        mainFrame.Source = null;
                        imgmain.Source = new BitmapImage(new Uri(LoginUser.DigiFolderPath + "/Locked.png"));
                        CurrentBitmapImage = new BitmapImage(new Uri(LoginUser.DigiFolderPath + "/Locked.png"));
                        imgmain.UpdateLayout();
                    }
                    else
                    {
                        splockedImages.Visibility = Visibility.Collapsed;

                        spaddtoalbumnunlockedImages.Visibility = Visibility.Visible;
                        spunlockedImages.Visibility = Visibility.Visible;
                        unlockImage = CommonUtility.GetImageFromPath(Convert.ToString(((DigiPhoto.LstMyItems)(lstImages.SelectedItem)).BigThumbnailPath));
                        btnEdit.IsEnabled = true;
                        imgmain.Source = CommonUtility.GetImageFromPath(Convert.ToString(((DigiPhoto.LstMyItems)(lstImages.SelectedItem)).BigThumbnailPath)); ;
                        //if (((DigiPhoto.LstMyItems)(lstImages.SelectedItem)).FrameBrdr != null)
                        //    mainFrame.Source = new BitmapImage(new Uri(((DigiPhoto.LstMyItems)(lstImages.SelectedItem)).FrameBrdr));
                        //else
                        mainFrame.Source = null;
                        CurrentBitmapImage = CommonUtility.GetImageFromPath(Convert.ToString(((DigiPhoto.LstMyItems)(lstImages.SelectedItem)).BigThumbnailPath));
                        mainFrame.UpdateLayout();
                        imgmain.UpdateLayout();
                    }
                }
                else
                {
                    if (vwGroup.Text == "View Group")
                    {
                        lstImages.SelectedItem = lstImages.Items[0];
                        lstImages.Focus();
                        ListBoxItem listBoxItem = (ListBoxItem)lstImages.ItemContainerGenerator.ContainerFromItem(lstImages.SelectedItem);
                        listBoxItem.Focus();
                    }
                    else
                    {
                        lstImages.SelectedItem = RobotImageLoader.GroupImages.First();
                        ListBoxItem listBoxItem = (ListBoxItem)lstImages.ItemContainerGenerator.ContainerFromItem(lstImages.SelectedItem);
                        listBoxItem.Focus();
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
            finally
            {
                //MemoryManagement.FlushMemory();
            }
        }
        private void btnAddToGroup_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                //var item = RobotImageLoader.GroupImages;
                RobotImageLoader.curItem = null;
                Button _btnSender = (Button)sender;
                System.Windows.Controls.Image _senderImage = (System.Windows.Controls.Image)_btnSender.Content;
                var groupitem = RobotImageLoader.GroupImages.Where(t => t.PhotoId == (int)((Button)(sender)).CommandParameter).ToList();
                if (groupitem.Count == 0)
                {
                    //i.e. Photo is selected and adding into group images
                    var curItem = RobotImageLoader.robotImages.Where(t => t.PhotoId == (int)((Button)sender).CommandParameter).First();

                    //Check if selected photo having storybook ID
                    if (curItem.StoryBookId != 0 && curItem.StoryBookId > 0)
                    {
                        var SBItems = RobotImageLoader.GroupImages.Where(t => t.StoryBookId > 0).ToList().FirstOrDefault();

                        if (SBItems != null)
                        {
                            RobotImageLoader.GroupImages.Remove(SBItems);
                            if (RobotImageLoader.ViewGroupedImagesCount != null && RobotImageLoader.ViewGroupedImagesCount.Count > 0)
                                RobotImageLoader.ViewGroupedImagesCount.Remove(SBItems.Name);

                            lstImages.SelectedItem = SBItems;
                            ListBoxItem listBoxItemSB = (ListBoxItem)lstImages.ItemContainerGenerator.ContainerFromItem(lstImages.SelectedItem);
                            ContentPresenter myContentPresenter = FindVisualChild<ContentPresenter>(listBoxItemSB);
                            DataTemplate myDataTemplate = myContentPresenter.ContentTemplate;
                            Grid myTextBlock = (Grid)myDataTemplate.FindName("grdMain", myContentPresenter);
                            ((System.Windows.Controls.Image)(((Button)(((System.Windows.Controls.Panel)((((Grid)(myTextBlock.FindName("Printbtns")))))).Children[2])).Content)).Source = null;
                            ((System.Windows.Controls.Image)(((Button)(((System.Windows.Controls.Panel)((((Grid)(myTextBlock.FindName("Printbtns")))))).Children[2])).Content)).Source = new BitmapImage(new Uri(@"/images/view-group.png", UriKind.Relative));

                        }
                    }

                    lstImages.SelectedItem = curItem;
                    ListBoxItem listBoxItem = (ListBoxItem)lstImages.ItemContainerGenerator.ContainerFromItem(lstImages.SelectedItem);
                    listBoxItem.Focus();

                    LstMyItems _myitem = new LstMyItems();
                    _myitem = curItem;
                    _myitem.MediaType = curItem.MediaType;
                    _myitem.VideoLength = curItem.VideoLength;
                    _myitem.Name = curItem.Name;
                    _myitem.IsPassKeyVisible = Visibility.Collapsed;
                    _myitem.PhotoId = curItem.PhotoId;
                    _myitem.FileName = curItem.FileName;
                    _myitem.CreatedOn = curItem.CreatedOn;
                    _myitem.OnlineQRCode = curItem.OnlineQRCode;
                    _myitem.IsLocked = Visibility.Visible;
                    _myitem.BmpImageGroup = new BitmapImage(new Uri(@"/images/view-accept.png", UriKind.Relative));
                    _myitem.FrameBrdr = ((LstMyItems)(lstImages.SelectedItem)).FrameBrdr;
                    _myitem.FilePath = curItem.FilePath;
                    _myitem.StoryBookId = curItem.StoryBookId;
                    _myitem.IsVosDisplay = curItem.IsVosDisplay;
                    _myitem.PageNo = curItem.PageNo;

                    RobotImageLoader.GroupImages.Add(_myitem);

                    if (RobotImageLoader.ViewGroupedImagesCount == null)
                        RobotImageLoader.ViewGroupedImagesCount = new List<string>();
                    RobotImageLoader.ViewGroupedImagesCount.Add(_myitem.Name);
                    if (_currentImage == (String)((Button)(sender)).Tag)
                    {
                        ImgAddToGroup.Source = new BitmapImage(new Uri(@"/images/view-accept.png", UriKind.Relative));
                        btnImageAddToGroup.ToolTip = "Remove from group";
                    }
                    var mainitem = RobotImageLoader.robotImages.Where(t => t.PhotoId == (int)((Button)(sender)).CommandParameter).First();
                    if (mainitem != null)
                    {
                        mainitem.BmpImageGroup = new BitmapImage(new Uri(@"/images/view-accept.png", UriKind.Relative));
                    }
                    _senderImage.Source = new BitmapImage(new Uri(@"/images/view-accept.png", UriKind.Relative));
                }
                else
                {
                    //i.e. photo is de-selected and removing from group images
                    int sname = (int)_btnSender.CommandParameter;
                    var searchitem = RobotImageLoader.GroupImages.Where(t => t.PhotoId == sname).ToList();
                    if (searchitem.Count != 0)
                    {
                        RobotImageLoader.GroupImages.Remove(searchitem.First());
                        if (RobotImageLoader.ViewGroupedImagesCount != null && RobotImageLoader.ViewGroupedImagesCount.Count > 0)
                            RobotImageLoader.ViewGroupedImagesCount.Remove(searchitem.First().Name);
                        if (vwGroup.Text == "View All" || pagename == "Saveback")
                        {
                            int currIndex = NextGroupSelectedIndex(lstImages.Items.IndexOf(searchitem.First()));
                            if (isStorybookBack == "")
                            {
                                lstImages.Items.Remove(searchitem.First());
                            }
                            if (lstImages.Items.Count == 0) //reverting if the print group gets empty
                            {
                                pagename = "";
                            }
                            if (lstImages.Items.Count > 0)
                            {

                                lstImages.Focus();
                                lstImages.SelectedItem = lstImages.Items[currIndex];

                                ListBoxItem listBoxItem = (ListBoxItem)lstImages.ItemContainerGenerator.ContainerFromItem(lstImages.Items[currIndex]);
                                listBoxItem.Focus();
                            }
                            else
                            {
                                continueCalculating = false;
                                num = 0;
                                RobotImageLoader.IsZeroSearchNeeded = true;
                                RobotImageLoader.currentCount = 0;
                                RobotImageLoader.RFID = "0";
                                RobotImageLoader.SearchCriteria = "";
                                RobotImageLoader.LoadImages();
                                LoadImages();
                                IMGFrame.Visibility = Visibility.Collapsed;
                                Grid.SetColumnSpan(thumbPreview, 2);
                                Grid.SetColumn(thumbPreview, 0);
                                thumbPreview.Margin = new Thickness(0);
                                vwGroup.Text = "View Group";
                                flgGridWithoutPreview = true;
                                ////For showing the paginate buttons and hiding the scrollbars
                                if ((string.Compare(RobotImageLoader.SearchCriteria, "PhotoId", true) == 0 || string.IsNullOrEmpty(RobotImageLoader.SearchCriteria)) && RobotImageLoader.RFID == "0")
                                {
                                    btnprev.Visibility = System.Windows.Visibility.Visible;
                                    btnnext.Visibility = System.Windows.Visibility.Visible;
                                    ScrollViewer.SetVerticalScrollBarVisibility(lstImages, ScrollBarVisibility.Hidden);
                                }
                                else
                                {
                                    if (RobotImageLoader.RFID == "")
                                    {
                                        btnprev.Visibility = System.Windows.Visibility.Hidden;
                                        btnnext.Visibility = System.Windows.Visibility.Hidden;
                                        ScrollViewer.SetVerticalScrollBarVisibility(lstImages, ScrollBarVisibility.Visible);
                                    }
                                    else
                                    {
                                        btnprev.Visibility = System.Windows.Visibility.Visible;
                                        btnnext.Visibility = System.Windows.Visibility.Visible;
                                        ScrollViewer.SetVerticalScrollBarVisibility(lstImages, ScrollBarVisibility.Hidden);
                                        vwGroup.Text = "View Group";
                                    }
                                }
                            }
                        }
                        else
                        {
                            var curItem = RobotImageLoader.robotImages.Where(t => t.PhotoId == (int)((Button)sender).CommandParameter).First();
                            lstImages.SelectedItem = curItem;
                            ListBoxItem listBoxItem = (ListBoxItem)lstImages.ItemContainerGenerator.ContainerFromItem(lstImages.SelectedItem);
                            listBoxItem.Focus();
                        }
                    }
                    if (RobotImageLoader.robotImages != null && RobotImageLoader.robotImages.Count > 0)
                    {
                        var searchitemmain = RobotImageLoader.robotImages.Where(t => t.PhotoId == sname).ToList();
                        if (searchitemmain.Count != 0)
                        {
                            searchitemmain.First().BmpImageGroup = new BitmapImage(new Uri(@"/images/view-group.png", UriKind.Relative));
                            if (_currentImageId == sname)
                            {
                                ImgAddToGroup.Source = null;
                                ImgAddToGroup.Source = new BitmapImage(new Uri(@"/images/view-group.png", UriKind.Relative));
                                btnImageAddToGroup.ToolTip = "Add to group";
                                if (searchitemmain.First().MediaType == 2 || searchitemmain.First().MediaType == 3) //Guest Videos and Processed Videos
                                {
                                    btnEdit.IsEnabled = false;
                                    img.Visibility = Visibility.Hidden;
                                    vidoriginal.Visibility = Visibility.Visible;
                                    string vidPath = searchitemmain.First().FileName;
                                    if (!String.IsNullOrEmpty(vidPath))
                                    {
                                        if (searchitemmain.First().MediaType == 2)
                                        {
                                            using (FileStream fileStream = File.OpenRead(System.IO.Path.Combine(searchitemmain.First().HotFolderPath, "Videos", searchitemmain.First().CreatedOn.ToString("yyyyMMdd"), vidPath)))
                                            {
                                                vsMediaFileName = fileStream.Name;
                                            }
                                        }
                                        else
                                        {
                                            using (FileStream fileStream = File.OpenRead(System.IO.Path.Combine(searchitemmain.First().HotFolderPath, "ProcessedVideos", searchitemmain.First().CreatedOn.ToString("yyyyMMdd"), vidPath)))
                                            {
                                                vsMediaFileName = fileStream.Name;
                                            }
                                        }
                                        Dispatcher.Invoke(new Action(() => MediaPlay()));
                                        txtMainVideo.Text = searchitemmain.First().Name;
                                    }
                                }
                                else
                                {
                                    MediaStop();
                                }
                            }
                            _senderImage.Source = new BitmapImage(new Uri(@"/images/view-group.png", UriKind.Relative));
                        }
                    }
                }

                if (vwGroup.Text == "View Group") // to do
                {
                    SetMessageText("Grouped");
                }
                else
                {
                    SetMessageText("PrintGrouped");
                }

                if (pagename == "Saveback" && RobotImageLoader.PrintImages.Count > 0 && SPSelectAll.Visibility == Visibility.Hidden)
                {
                    SetMessageText("EditPrintGrouped");
                    SPSelectAll.Visibility = System.Windows.Visibility.Hidden;
                }
                else
                {
                    SPSelectAll.Visibility = System.Windows.Visibility.Visible;
                }

                if (lstImages.Items.Count > 0 && lstImages.SelectedIndex == -1)
                {
                    lstImages.SelectedIndex = 0;
                    ListBoxItem listBoxItem = (ListBoxItem)lstImages.ItemContainerGenerator.ContainerFromItem(lstImages.Items[0]);
                    listBoxItem.Focus();
                    lstImages.ScrollIntoView(lstImages.SelectedItem);
                }
                if (vwGroup.Text == "View All")
                {
                    CheckForAllImgSelectToPrint();
                }
                else
                {
                    FillImageList();
                    CheckSelectAllGroup();
                }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
            finally
            {
                var itemsb = RobotImageLoader.GroupImages.Where(o => o.MediaType == 1 && o.StoryBookId != 0).Count();
                var itembs = RobotImageLoader.GroupImages.Where(o => o.MediaType == 1 && o.StoryBookId == 0).Count();
                var itemps = RobotImageLoader.PrintImages.Where(o => o.MediaType == 1 && o.StoryBookId != 0).Count();
                var itemsp = RobotImageLoader.PrintImages.Where(o => o.MediaType == 1 && o.StoryBookId == 0).Count();
                if (itemsb != 0 && itembs == 0)
                {
                    btnEdit.IsEnabled = false;
                }
                else if (itemsb != 0 && itembs != 0)
                {
                    btnEdit.IsEnabled = false;
                }
                else if (itemps != 0 && itemsp == 0)
                {
                    btnEdit.IsEnabled = false;
                }
                else if (itemps != 0 && itemsp != 0)
                {
                    btnEdit.IsEnabled = false;
                }
                else
                {
                    btnEdit.IsEnabled = true;
                }
            }
        }
        public void ViewGroup()
        {
            try
            {
                MediaStop();
                btnEdit.IsEnabled = false;
                Int64 Photo_PKey = 0;
                bool flgProceed = false;
                int imageCount = 0;
                int printCount = 0;
                if (RobotImageLoader.GroupImages != null)
                {
                    imageCount = RobotImageLoader.GroupImages.Count;
                }
                if (RobotImageLoader.PrintImages != null)
                {
                    printCount = RobotImageLoader.PrintImages.Count;
                }

                SetMessageText("Grouped");

                if (vwGroup.Text == "View Group" || pagename == "Saveback")
                {
                    rdbtnAll.IsChecked = true;
                    continueCalculating = false;
                    if (imageCount > 0)
                    {
                        SetMessageText("Grouped");
                        if (lstImages.Items.Count > 0 && lstImages != null)
                        {
                            lstImages.Items.Clear();
                        }
                        ////For showing the scrollbars and hiding the paginate buttons
                        btnprev.Visibility = System.Windows.Visibility.Collapsed;
                        btnnext.Visibility = System.Windows.Visibility.Collapsed;
                        ScrollViewer.SetVerticalScrollBarVisibility(lstImages, ScrollBarVisibility.Visible);
                        ///////////////
                        List<ModratePhotoInfo> moderatePhotosList = new List<ModratePhotoInfo>();
                        PhotoBusiness phototBiz = new PhotoBusiness();
                        moderatePhotosList = phototBiz.GetModeratePhotos();

                        foreach (var photoItem in RobotImageLoader.GroupImages)
                        {
                            var existItem = RobotImageLoader.PrintImages.Where(t => t.PhotoId == photoItem.PhotoId).FirstOrDefault();
                            if (existItem != null)
                                photoItem.PrintGroup = new BitmapImage(new Uri(@"/images/print-accept.png", UriKind.Relative));
                            else
                                photoItem.PrintGroup = new BitmapImage(new Uri(@"/images/print-group.png", UriKind.Relative));

                            var tesItem = RobotImageLoader.NotPrintedImages.Where(xs => xs.FileName == photoItem.FileName).FirstOrDefault();
                            if (tesItem == null && !flgProceed)
                                flgProceed = true;

                            var moderateItem = moderatePhotosList.AsEnumerable().Where(x => x.DG_Mod_Photo_ID == photoItem.PhotoId);
                            if (moderateItem != null && moderateItem.Count() > 0)
                            {
                                splockedImages.Visibility = Visibility.Collapsed;
                                spaddtoalbumnunlockedImages.Visibility = Visibility.Visible;
                                spunlockedImages.Visibility = Visibility.Visible;
                                unlockImage = new BitmapImage(new Uri(RobotImageLoader.GroupImages.First().BigThumbnailPath, UriKind.Absolute));
                                btnEdit.IsEnabled = false;
                                photoItem.IsPassKeyVisible = Visibility.Visible;
                                photoItem.BmpImageGroup = null;
                                photoItem.FilePath = photoItem.HotFolderPath + "/Locked.png";
                                photoItem.IsLocked = Visibility.Collapsed;
                                photoItem.IsPassKeyVisible = Visibility.Visible;
                            }
                            lstImages.Items.Add(photoItem);

                            if (photoItem.PhotoId == RobotImageLoader.UniquePhotoId)
                            {
                                try
                                {
                                    lstImages.SelectedItem = photoItem;
                                    ListBoxItem listBoxItem = (ListBoxItem)lstImages.ItemContainerGenerator.ContainerFromItem(lstImages.SelectedItem);
                                    listBoxItem.Focus();
                                    lstImages.ScrollIntoView(lstImages.SelectedItem);
                                }
                                catch (Exception ex)
                                {
                                    string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                                    ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                                }
                            }
                        }

                        SetMessageText("PrintGrouped");

                        var groupItem = RobotImageLoader.GroupImages.First();
                        txtMainImage.Text = groupItem.Name.ToString();
                        _currentImage = groupItem.Name;
                        _objBitmap = new BitmapImage();
                        _objBitmap = CurrentBitmapImage;

                        vwGroup.Text = "View All";
                        isStorybookBack = "";//Reset the value to clear the group

                        ImgAddToGroup.Source = new BitmapImage(new Uri(@"/images/view-accept.png", UriKind.Relative));
                        ImgAddToGroup.ToolTip = "Remove from group";
                        /////////Visibility false of image and group should be shown to user.
                        Photo_PKey = groupItem.PhotoId;//objdbLayer.GetPhotoIDByRFFID();
                        unlockImageId = Photo_PKey;
                        var moderateItemValue = moderatePhotosList.AsEnumerable().Where(x => x.DG_Mod_Photo_ID == Photo_PKey);
                        if (moderateItemValue != null && moderateItemValue.Count() > 0)
                        {
                            splockedImages.Visibility = Visibility.Collapsed;
                            spaddtoalbumnunlockedImages.Visibility = Visibility.Visible;
                            spunlockedImages.Visibility = Visibility.Visible;
                            unlockImage = new BitmapImage(new Uri(groupItem.FilePath, UriKind.Absolute));
                            btnEdit.IsEnabled = false;
                        }

                        if (flgProceed)
                        {
                            MainWindow _objMainWindow = MainWindow.Instance;

                            if (_objMainWindow.IsGoupped == "View All" && pagename == "Saveback")
                            {
                                vwGroup.Text = "View Group";

                                foreach (LstMyItems itx in RobotImageLoader.GroupImages)
                                {
                                    var teItem = RobotImageLoader.PrintImages.Where(xs => xs.PhotoId == itx.PhotoId).FirstOrDefault();
                                    if (teItem == null)
                                    {
                                        RobotImageLoader.NotPrintedImages.Add(itx);
                                    }
                                }

                                if (RobotImageLoader.NotPrintedImages.Count != 0)
                                {
                                    foreach (LstMyItems ity in RobotImageLoader.NotPrintedImages)
                                    {
                                        lstImages.Items.Remove(ity);
                                        var tempG = RobotImageLoader.GroupImages.Where(tg => tg.PhotoId == ity.PhotoId).FirstOrDefault();
                                        if (tempG != null)
                                            tempG.PrintGroup =
                                                new BitmapImage(new Uri(@"/images/print-group.png", UriKind.Relative));
                                    }
                                }
                                SetMessageText("EditPrintGrouped");
                                SPSelectAll.Visibility = System.Windows.Visibility.Hidden;
                            }
                            else
                            {
                                AddGroupCount(PhotBiz);
                                SPSelectAll.Visibility = System.Windows.Visibility.Visible;
                            }
                        }

                    }
                    else
                    {
                        if (lstImages.SelectedIndex > -1)
                        {
                            ListBoxItem listBoxItem = (ListBoxItem)lstImages.ItemContainerGenerator.ContainerFromItem(lstImages.SelectedItem);
                            listBoxItem.Focus();
                            lstImages.ScrollIntoView(lstImages.SelectedItem);
                        }
                        MessageBox.Show("No images are grouped!", "DigiPhoto i-Mix", MessageBoxButton.OK, MessageBoxImage.Information);
                    }
                    //to check mark the img
                    CheckForAllImgSelectToPrint();
                }

                else
                {
                    if (lstImages.Items.Count > 0 && lstImages != null)
                    {
                        lstImages.Items.Clear();
                    }
                    ////For showing the paginate buttons and hiding the scrollbars
                    btnprev.Visibility = System.Windows.Visibility.Visible;
                    btnnext.Visibility = System.Windows.Visibility.Visible;
                    ScrollViewer.SetVerticalScrollBarVisibility(lstImages, ScrollBarVisibility.Hidden);
                    RobotImageLoader.GroupId = "";
                    RobotImageLoader.SearchCriteria = "";
                    RobotImageLoader.RFID = "0";
                    continueCalculating = false;
                    num = 0;

                    if (RobotImageLoader.RFID == "0")
                    {
                        if (flgGridWithoutPreview)
                        {
                            RobotImageLoader.thumbSet = scrollIndexWithoutPreview;
                        }
                        else
                        {
                            RobotImageLoader.thumbSet = scrollIndexWithPreview;
                        }
                        RobotImageLoader.currentCount = 0;
                        RobotImageLoader.IsZeroSearchNeeded = true;
                        flgLoadNext = false;
                        LoadNext();
                        lstImages.SelectedIndex = 0;
                    }

                    var item = RobotImageLoader.robotImages.First();
                    if (item.MediaType == 1)
                        CurrentBitmapImage = new BitmapImage(new Uri(item.BigThumbnailPath, UriKind.Absolute));
                    imgmain.Source = CurrentBitmapImage;
                    imgmain.UpdateLayout();
                    txtMainImage.Text = item.Name.ToString();
                    _currentImage = item.Name;
                    _objBitmap = new BitmapImage();
                    _objBitmap = (System.Windows.Media.Imaging.BitmapImage)(new BitmapImage(new Uri(item.FilePath)));
                    vwGroup.Text = "View Group";

                    Photo_PKey = item.PhotoId;
                    unlockImageId = Photo_PKey;
                    PhotoBusiness photoBiz = new PhotoBusiness();
                    if (!photoBiz.GetModeratePhotos(Photo_PKey))
                    {
                        splockedImages.Visibility = Visibility.Collapsed;
                        spaddtoalbumnunlockedImages.Visibility = Visibility.Visible;
                        spunlockedImages.Visibility = Visibility.Visible;
                        btnEdit.IsEnabled = false;
                    }
                    else
                    {
                        splockedImages.Visibility = Visibility.Visible;
                        spaddtoalbumnunlockedImages.Visibility = Visibility.Collapsed;
                        spunlockedImages.Visibility = Visibility.Collapsed;
                        unlockImage = new BitmapImage(new Uri(item.BigThumbnailPath, UriKind.Absolute));
                        btnEdit.IsEnabled = true;
                    }
                    ErrorHandler.ErrorHandler.LogFileWrite("else 2670 " + System.DateTime.Now);
                    if (lstImages.Items.Count > 0)
                    {
                        ListBoxItem listBoxItem = (ListBoxItem)lstImages.ItemContainerGenerator.ContainerFromItem(lstImages.Items[0]);
                        listBoxItem.Focus();
                        lstImages.ScrollIntoView(lstImages.SelectedItem);
                    }
                    RobotImageLoader.ViewGroupedImagesCount = new List<string>();
                    FillImageList();
                    CheckSelectAllGroup();
                }
                if (RobotImageLoader.curItem != null && lstImages.Items.Count > 0)
                {
                    ErrorHandler.ErrorHandler.LogFileWrite("else 2684 " + System.DateTime.Now);
                    lstImages.SelectedItem = RobotImageLoader.curItem;
                    lstImages.ScrollIntoView(lstImages.SelectedItem);
                    ((ListBoxItem)lstImages.ItemContainerGenerator.ContainerFromItem(lstImages.SelectedItem)).Focus();
                }
                else if (lstImages.Items.Count > 0 && lstImages.SelectedIndex == -1 && lstImages != null)
                {
                    lstImages.SelectedIndex = 0;
                    ListBoxItem listBoxItem = (ListBoxItem)lstImages.ItemContainerGenerator.ContainerFromItem(lstImages.Items[0]);
                    lstImages.SelectedItem = listBoxItem;
                    lstImages.ScrollIntoView(lstImages.SelectedItem);
                    listBoxItem.Focus();
                }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
            finally
            {
                MemoryManagement.FlushMemory();
            }
        }
        public void btnViewGroup_Click(object sender, RoutedEventArgs e)
        {
            MediaStop();
            pagename = "";
            SPSelectAll.Visibility = System.Windows.Visibility.Visible;
            ViewGroup();
        }
        private void btnImageAddToGroup_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                btnEdit.IsEnabled = false;
                Button _objbtn = (Button)sender; LstMyItems mainitem = new LstMyItems();
                if (_currentImageId == 0)
                    _currentImageId = ((DigiPhoto.LstMyItems)(lstImages.SelectedItem)).PhotoId;
                if (vwGroup.Text == "View Group" && pagename != "Saveback")
                    mainitem = RobotImageLoader.robotImages.Where(t => t.PhotoId == _currentImageId).FirstOrDefault();
                else
                    mainitem = RobotImageLoader.GroupImages.Where(t => t.PhotoId == _currentImageId).FirstOrDefault();

                if (_objbtn.CommandParameter.ToString() == "Remove from group")
                {
                    btnImageAddToGroup.ToolTip = "Add to group";
                    btnImageAddToGroup.CommandParameter = "Add to group";
                    ImgAddToGroup.Source = new BitmapImage(new Uri(@"/images/view-group.png", UriKind.Relative));
                    if (mainitem != null)
                    {
                        mainitem.BmpImageGroup = new BitmapImage(new Uri(@"/images/view-group.png", UriKind.Relative));
                        lstImages.SelectedItem = mainitem;
                        ListBoxItem listBoxItem = (ListBoxItem)lstImages.ItemContainerGenerator.ContainerFromItem(lstImages.SelectedItem);
                        ContentPresenter myContentPresenter = FindVisualChild<ContentPresenter>(listBoxItem);
                        DataTemplate myDataTemplate = myContentPresenter.ContentTemplate;
                        Grid myTextBlock = (Grid)myDataTemplate.FindName("grdMain", myContentPresenter);
                        ((System.Windows.Controls.Image)(((Button)(((System.Windows.Controls.Panel)((((Grid)(myTextBlock.FindName("Printbtns")))))).Children[2])).Content)).Source = null;
                        ((System.Windows.Controls.Image)(((Button)(((System.Windows.Controls.Panel)((((Grid)(myTextBlock.FindName("Printbtns")))))).Children[2])).Content)).Source = new BitmapImage(new Uri(@"/images/view-group.png", UriKind.Relative));
                        var groupitem = RobotImageLoader.GroupImages.Where(t => t.PhotoId == _currentImageId).FirstOrDefault();
                        RobotImageLoader.GroupImages.Remove(groupitem);
                        if (vwGroup.Text == "View All" || pagename == "Saveback")
                        {
                            if (lstImages.Items.Count > 0)
                            {
                                lstImages.Items.Remove(groupitem); // Remove from list only if in view group section
                            }
                        }
                    }

                }
                else
                {
                    btnImageAddToGroup.ToolTip = "Remove from group";
                    btnImageAddToGroup.CommandParameter = "Remove from group";
                    ImgAddToGroup.Source = new BitmapImage(new Uri(@"/images/view-accept.png", UriKind.Relative));
                    if (mainitem != null)
                    {
                        var itemExists = RobotImageLoader.GroupImages.Where(gi => gi.FileName == mainitem.FileName);
                        if (itemExists.Count() == 0) //Verify if item already exists then dont add it in the group image list
                        {
                            RobotImageLoader.GroupImages.Add(mainitem);
                        }
                        mainitem.BmpImageGroup = new BitmapImage(new Uri(@"/images/view-accept.png", UriKind.Relative));
                        lstImages.SelectedItem = mainitem;
                        ListBoxItem listBoxItem = (ListBoxItem)lstImages.ItemContainerGenerator.ContainerFromItem(lstImages.SelectedItem);
                        ContentPresenter myContentPresenter = FindVisualChild<ContentPresenter>(listBoxItem);
                        // Finding textBlock from the DataTemplate that is set on that ContentPresenter
                        DataTemplate myDataTemplate = myContentPresenter.ContentTemplate;
                        Grid myTextBlock = (Grid)myDataTemplate.FindName("grdMain", myContentPresenter);
                        ((System.Windows.Controls.Image)(((Button)(((System.Windows.Controls.Panel)((((Grid)(myTextBlock.FindName("Printbtns")))))).Children[2])).Content)).Source = null;
                        ((System.Windows.Controls.Image)(((Button)(((System.Windows.Controls.Panel)((((Grid)(myTextBlock.FindName("Printbtns")))))).Children[2])).Content)).Source = new BitmapImage(new Uri(@"/images/view-accept.png", UriKind.Relative));
                    }
                }

                if (vwGroup.Text == "View All")
                    CheckForAllImgSelectToPrint();
                else
                {
                    FillImageList();
                    CheckSelectAllGroup();
                }
                if (vwGroup.Text == "View All" || pagename == "Saveback")
                {
                    if (lstImages.Items.Count == 0)
                    {
                        if (pagename == "Saveback")
                        {
                            pagename = "";
                        }
                        continueCalculating = false;
                        num = 0;
                        RobotImageLoader.currentCount = 0;
                        RobotImageLoader.IsZeroSearchNeeded = true;
                        RobotImageLoader.RFID = "0";
                        RobotImageLoader.LoadImages();
                        LoadImages();
                        IMGFrame.Visibility = Visibility.Collapsed;
                        Grid.SetColumnSpan(thumbPreview, 2);
                        Grid.SetColumn(thumbPreview, 0);
                        thumbPreview.Margin = new Thickness(0);
                        vwGroup.Text = "View Group";
                        flgGridWithoutPreview = true;
                        ////For showing the paginate buttons and hiding the scrollbars
                        btnprev.Visibility = System.Windows.Visibility.Visible;
                        btnnext.Visibility = System.Windows.Visibility.Visible;
                        ScrollViewer.SetVerticalScrollBarVisibility(lstImages, ScrollBarVisibility.Hidden);
                        SPSelectAll.Visibility = System.Windows.Visibility.Visible;

                        FillImageList();
                        CheckSelectAllGroup();
                    }
                    else
                    {
                        if (pagename == "Saveback")
                            lstImages.SelectedIndex = 0;
                        else
                            lstImages.SelectedItem = RobotImageLoader.GroupImages.First();

                        LstMyItems selectedItem = ((DigiPhoto.LstMyItems)(lstImages.SelectedItem));

                        if (selectedItem.IsLocked == Visibility.Collapsed && selectedItem.IsPassKeyVisible == Visibility.Visible)
                            imgmain.Source = CommonUtility.GetImageFromPath(selectedItem.FilePath);
                        else
                        {
                            if (selectedItem.MediaType == 1)
                                imgmain.Source = CommonUtility.GetImageFromPath(Convert.ToString(selectedItem.BigThumbnailPath));
                        }
                        ListBoxItem listBoxItem1 = (ListBoxItem)lstImages.ItemContainerGenerator.ContainerFromItem(lstImages.SelectedItem);
                        if (lstImages.SelectedItem != null)
                        {
                            listBoxItem1.Focus();
                        }
                        else
                        {
                            if (lstImages.Items.Count > 0 && lstImages.SelectedIndex == -1)
                            {
                                lstImages.SelectedIndex = 0;
                                ListBoxItem listBoxItem = (ListBoxItem)lstImages.ItemContainerGenerator.ContainerFromItem(lstImages.Items[0]);
                                listBoxItem.Focus();
                                lstImages.ScrollIntoView(lstImages.SelectedItem);
                            }
                        }
                    }
                }

                if (vwGroup.Text == "View Group")
                    SetMessageText("Grouped");
                else
                    SetMessageText("PrintGrouped");

                //Added towards clearing the group items which are not to be printed
                if (vwGroup.Text == "View Group" && pagename == "Saveback")
                {
                    foreach (LstMyItems itx in RobotImageLoader.GroupImages)
                    {
                        var teItem = RobotImageLoader.PrintImages.Where(xs => xs.PhotoId == itx.PhotoId).FirstOrDefault();
                        if (teItem == null)
                        {
                            RobotImageLoader.NotPrintedImages.Add(itx);
                        }
                    }
                    if (RobotImageLoader.NotPrintedImages.Count != 0)
                    {
                        foreach (LstMyItems ity in RobotImageLoader.NotPrintedImages)
                        {
                            if (lstImages.Items.Count > 0)////changed by latika for minimise issue
                            {
                                lstImages.Items.Remove(ity);
                            }
                        }
                    }
                    SetMessageText("EditPrintGrouped");
                    SPSelectAll.Visibility = System.Windows.Visibility.Hidden;
                }
                else
                {
                    SPSelectAll.Visibility = System.Windows.Visibility.Visible;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }
        public void LoadWindow()
        {
#if DEBUG
            var watch = FrameworkHelper.CommonUtility.Watch();
            watch.Start();
#endif
            try
            {
                txbUserName.Text = LoginUser.UserName;
                txbStoreName.Text = LoginUser.StoreName;
                returntoHome = true;
                RobotImageLoader.GetConfigData();
                RobotImageLoader.LstUnlocknames = new List<string>();
                RobotImageLoader.IsPreviewModeActive = false;
                if (RobotImageLoader.Is9ImgViewActive)
                {
                    scrollIndexWithoutPreview = 9;
                    scrollIndexWithPreview = 9;
                }

                RobotImageLoader.thumbSet = scrollIndexWithoutPreview;

                if (pagename != "Saveback")
                {
                    if (serachType == 1)
                    {
                        searchDetails.NewRecord = 0;
                        searchDetails.StartIndex = 0;
                        RobotImageLoader.Is16ImgViewActive = false;
                        RobotImageLoader.Is9ImgViewActive = true;
                        searchDetails.PageSize = 9;
                    }
                    serachType = 0;
                    searchDetails.PageNumber = 1;
                    SetMediaType();
                    RobotImageLoader.LoadImages(searchDetails);
                    if ((RobotImageLoader.SearchCriteria != "TimeWithQrcode"))
                    {
                        btnprev.IsEnabled = true;
                        btnnext.IsEnabled = true;
                    }
                }

                if (RobotImageLoader.robotImages != null)
                    count = RobotImageLoader.robotImages.Count;

                if (RobotImageLoader.GroupImages == null)
                    RobotImageLoader.GroupImages = new List<LstMyItems>();

                if (RobotImageLoader.PrintImages == null)
                    RobotImageLoader.PrintImages = new List<LstMyItems>();

                if (scrollIndexWithoutPreview == 9)
                {
                    if (RobotImageLoader.GroupImages != null && RobotImageLoader.GroupImages.Count > 0)
                        RobotImageLoader.GroupImages.ForEach(t => { t.GridMainHeight = 190; t.GridMainWidth = 226; t.GridMainRowHeight1 = 140; t.GridMainRowHeight2 = 50; });
                    if (RobotImageLoader.PrintImages != null && RobotImageLoader.PrintImages.Count > 0)
                        RobotImageLoader.PrintImages.ForEach(t => { t.GridMainHeight = 190; t.GridMainWidth = 226; t.GridMainRowHeight1 = 140; t.GridMainRowHeight2 = 50; });
                    if (RobotImageLoader.robotImages != null && RobotImageLoader.robotImages.Count > 0)
                        RobotImageLoader.robotImages.ForEach(t => { t.GridMainHeight = 190; t.GridMainWidth = 226; t.GridMainRowHeight1 = 140; t.GridMainRowHeight2 = 50; });
                }

                _sharpeff.Strength = (3 * 0.025);
                _sharpeff.PixelWidth = 0.0015;
                _sharpeff.PixelHeight = 0.0015;
                GrdSharpen.Effect = _sharpeff;
                PreviewPhoto();

                if ((RobotImageLoader.SearchCriteria == "Time") || (RobotImageLoader.SearchCriteria == "QRCODE") || (RobotImageLoader.SearchCriteria == "TimeWithQrcode"))
                {
                    btnprev.Visibility = System.Windows.Visibility.Visible;
                    btnnext.Visibility = System.Windows.Visibility.Visible;
                    ScrollViewer.SetVerticalScrollBarVisibility(lstImages, ScrollBarVisibility.Hidden);
                }
                else
                {
                    if (RobotImageLoader.RFID == "")
                    {
                        btnprev.Visibility = System.Windows.Visibility.Hidden;
                        btnnext.Visibility = System.Windows.Visibility.Hidden;
                        ScrollViewer.SetVerticalScrollBarVisibility(lstImages, ScrollBarVisibility.Visible);
                    }
                    else
                    {
                        btnprev.Visibility = System.Windows.Visibility.Visible;
                        btnnext.Visibility = System.Windows.Visibility.Visible;
                        ScrollViewer.SetVerticalScrollBarVisibility(lstImages, ScrollBarVisibility.Hidden);
                        vwGroup.Text = "View Group";
                    }
                }

                if (pagename == "MainGroup")
                {
                    MainWindow _objMainWindow;

                    if (MainWindow.Instance != null)
                        _objMainWindow = MainWindow.Instance;
                    else
                        _objMainWindow = MainWindow.Instance;

                    if (_objMainWindow.IsGoupped == "View Group")
                        vwGroup.Text = "View Group";
                    ViewGroup();
                }
                else if (pagename == "Saveback")
                {
                    try
                    {
                        MainWindow _objMainWindow;

                        if (MainWindow.Instance != null)
                        {
                            _objMainWindow = MainWindow.Instance;
                        }
                        else
                        {
                            _objMainWindow = MainWindow.Instance;
                        }

                        if (_objMainWindow.IsGoupped == "View All")
                        {
                            ViewGroup();
                        }
                        else
                        {
                            _objMainWindow.IsGoupped = "View Group";
                            vwGroup.Text = "View Group";
                            if (lstImages.Items.Count > 0 && lstImages != null)
                            {
                                lstImages.Items.Clear();
                            }
                            continueCalculating = true;
                            SetMessageText("Grouped");

                            if (RobotImageLoader._objnewincrement != null && RobotImageLoader._objnewincrement.Count == 0)
                                RobotImageLoader._objnewincrement = RobotImageLoader.robotImages;
                            LoadImagestoList();
                            FillImageList();
                            CheckSelectAllGroup();
                        }
                    }
                    catch (Exception ex)
                    {
                        string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                        ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                    }
                }
                else if (pagename == "Placeback")
                {
                    try
                    {
                        if (RobotImageLoader.robotImages.Count == 0)
                            RobotImageLoader.LoadImages();

                        if (RobotImageLoader.robotImages.Count != 0)
                        {
                            bool flgPicFound = false;
                            while (flgPicFound == false)
                            {
                                LstMyItems xBack = new LstMyItems();
                                xBack = RobotImageLoader.robotImages.Where(xb => xb.PhotoId == Savebackpid.ToInt32()).FirstOrDefault();
                                if (xBack != null)
                                {
                                    flgPicFound = true;
                                    continueCalculating = true;
                                    if (lstImages.Items.Count > 0)
                                        lstImages.Items.Clear();

                                    LoadImagestoList();
                                    break;
                                }
                                else
                                {
                                    if (RobotImageLoader.robotImages.Count != 0)
                                    {
                                        RobotImageLoader.LoadImages();
                                    }
                                    else
                                    {
                                        flgPicFound = true;
                                        break;
                                    }
                                }
                            }
                            SetMessageText("Grouped");
                        }
                    }
                    catch (Exception ex)
                    {
                        string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                        ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                    }
                }
                else
                {
                    LoadImages();
                }
                if (pagename == "Placeback")
                {
                    CheckForAllImgSelectToPrint();
                }
                else if (pagename != "MainGroup" && pagename != "Saveback")
                {
                    FillImageList();
                    CheckSelectAllGroup();
                }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
            finally
            {
                MemoryManagement.FlushMemory(); //Commented 3 Jan 2015 based on ANTS profiler recommendation
            }

            IMGFrame.Visibility = Visibility.Collapsed;
            Grid.SetColumnSpan(thumbPreview, 2);
            Grid.SetColumn(thumbPreview, 0);
            thumbPreview.Margin = new Thickness(0);

            if (RobotImageLoader.robotImages != null)
            {
                if (RobotImageLoader.Is16ImgViewActive == true && RobotImageLoader.Is9ImgViewActive == false)
                {
                    imgwithPreview.Source = new BitmapImage(new Uri(@"/images/thumbnailview1.png", UriKind.Relative));
                    imgwithoutPreview.Source = new BitmapImage(new Uri(@"images/16blocks_red.png", UriKind.Relative));
                    imgwithoutPreview9.Source = new BitmapImage(new Uri(@"/images/9Blocks_black.png", UriKind.Relative));
                    ImageSize(16);
                }
                else if (RobotImageLoader.Is16ImgViewActive == false && RobotImageLoader.Is9ImgViewActive == true)
                {
                    imgwithPreview.Source = new BitmapImage(new Uri(@"/images/thumbnailview1.png", UriKind.Relative));
                    imgwithoutPreview.Source = new BitmapImage(new Uri(@"images/16blocks_black.png", UriKind.Relative));
                    imgwithoutPreview9.Source = new BitmapImage(new Uri(@"/images/9Blocks_red.png", UriKind.Relative));
                    ImageSize(9);
                }
                else
                {
                    imgwithPreview.Source = new BitmapImage(new Uri(@"/images/thumbnailview1.png", UriKind.Relative));
                    imgwithoutPreview.Source = new BitmapImage(new Uri(@"images/16blocks_black.png", UriKind.Relative));
                    imgwithoutPreview9.Source = new BitmapImage(new Uri(@"/images/9Blocks_black.png", UriKind.Relative));
                }
            }
#if DEBUG
            if (watch != null)
                FrameworkHelper.CommonUtility.WatchStop("Search Image Load", watch);
#endif
        }
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                //txtImageId.Focus();
                //if (objdbLayer == null)
                //    objdbLayer = new DigiPhotoDataServices();
                ConfigBusiness configBiz = new ConfigBusiness();
                IsEnableGrouping = Convert.ToBoolean(configBiz.GetConfigEnableGroup(LoginUser.SubStoreId));
                if (IsEnableGrouping)
                {
                    tbcleargroup.Text = "Group";
                }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }
        #region Public Methods
        public void CompileEffectChanged(VisualBrush compiledBitmapImage, int ProductType)
        {
            try
            {
                img.UpdateLayout();
                imgmain.UpdateLayout();

                GetMktImgInfo();
                ClientView window = null;

                foreach (Window wnd in Application.Current.Windows)
                {
                    if (wnd.Title == "ClientView")
                    {
                        window = (ClientView)wnd;
                    }
                }

                if (window == null)
                {
                    window = new ClientView();
                    window.WindowStartupLocation = System.Windows.WindowStartupLocation.Manual;
                }

                window.imgNext.Visibility = System.Windows.Visibility.Collapsed;
                window.imgNext.Source = null;
                window.txtMainImage.Visibility = System.Windows.Visibility.Collapsed;
                window.stkPrint.Visibility = System.Windows.Visibility.Collapsed;
                window.btnMinimize.Visibility = Visibility.Collapsed;
                window.stkPrevNext.Visibility = Visibility.Collapsed;
                window.testR.Visibility = System.Windows.Visibility.Visible;
                window.GroupView = false;
                window.DefaultView = false;
                //////changed by latika for showing marketing images while editing.
                if (compiledBitmapImage != null && isMarketingImgShow != 1)////end
                {
                    window.testR.Fill = null;
                    window.imgDefaultBlur.Source = null;
                    compiledBitmapImage.Stretch = Stretch.Uniform;
                    window.imgDefault.Visibility = Visibility.Collapsed;
                    window.imgDefaultBlur.Visibility = System.Windows.Visibility.Collapsed;
                    window.instructionVideoBlur.Visibility = System.Windows.Visibility.Collapsed;
                    window.instructionVideoBlur.Pause();
                    window.instructionVideo.Visibility = System.Windows.Visibility.Collapsed;
                    window.instructionVideo.Pause();
                    window.testR.Fill = compiledBitmapImage;
                    window.testR.UpdateLayout();//Vins
                    if (btnchkpreview.IsChecked == true)
                    {
                        window.IMGFrame.Margin = new Thickness(0);
                        window.testR.Margin = new Thickness(0, 20, 0, 0);
                        window.imgDefaultBlur.Visibility = Visibility.Visible;
                        window.imgDefaultBlur.Source = null;
                        if (window.testR.ActualHeight > window.testR.ActualWidth)
                        {
                            if (imgmain.ActualWidth > 700)
                            {
                                imgmain.UpdateLayout();//Vins
                                RenderTargetBitmap bitmap = new RenderTargetBitmap((int)window.testR.ActualWidth, (int)window.testR.ActualHeight, 145, 145, PixelFormats.Default);
                                bitmap.Render(imgmain);
                                //bitmap.Render(compiledBitmapImage.Visual);
                                window.imgDefaultBlur.Source = bitmap;
                                window.imgDefaultBlur.Stretch = Stretch.Fill;
                                //window.imgDefaultBlur.HorizontalAlignment = HorizontalAlignment.Left;
                                window.imgDefaultBlur.HorizontalAlignment = HorizontalAlignment.Stretch;
                                window.imgDefaultBlur.VerticalAlignment = VerticalAlignment.Stretch;
                                double screenwidth = SystemParameters.FullPrimaryScreenWidth;
                                if ((int)screenwidth < 1100)
                                {
                                    window.imgDefaultBlur.Margin = new Thickness(0, 0, 0, -10);
                                }
                                else
                                {
                                    window.imgDefaultBlur.Margin = new Thickness(0, 0, 0, 0);
                                }
                                window.imgDefaultBlur.UpdateLayout();//Vins
                            }
                            else
                            {
                                imgmain.UpdateLayout();//Vins
                                RenderTargetBitmap bitmap = new RenderTargetBitmap((int)window.testR.ActualWidth, (int)window.testR.ActualHeight, 185, 145, PixelFormats.Default);
                                bitmap.Render(imgmain);
                                //bitmap.Render(compiledBitmapImage.Visual);
                                window.imgDefaultBlur.Source = bitmap;
                                window.imgDefaultBlur.Stretch = Stretch.Fill;
                                //window.imgDefaultBlur.HorizontalAlignment = HorizontalAlignment.Left;
                                window.imgDefaultBlur.HorizontalAlignment = HorizontalAlignment.Stretch;
                                window.imgDefaultBlur.VerticalAlignment = VerticalAlignment.Stretch;
                                double screenwidth = SystemParameters.FullPrimaryScreenWidth;
                                if ((int)screenwidth < 1100)
                                {
                                    window.imgDefaultBlur.Margin = new Thickness(0, 0, 0, -10);
                                }
                                else
                                {
                                    window.imgDefaultBlur.Margin = new Thickness(0, 0, 0, 0);
                                }
                                window.imgDefaultBlur.UpdateLayout();//Vins
                            }
                        }
                        else
                        {

                            if (imgmain.ActualWidth > 900)
                            {
                                if (imgmain.ActualHeight > 650)
                                {
                                    imgmain.UpdateLayout();//Vins..Ensure all child elements are updated
                                    //RenderTargetBitmap bitmap = new RenderTargetBitmap((int)window.testR.ActualWidth, (int)window.testR.ActualHeight, 155, 145, PixelFormats.Default);
                                    RenderTargetBitmap bitmap = new RenderTargetBitmap((int)window.testR.ActualWidth, (int)window.testR.ActualHeight, 155, 130, PixelFormats.Default);
                                    bitmap.Render(imgmain);
                                    //bitmap.Render(compiledBitmapImage.Visual);

                                    window.imgDefaultBlur.Source = bitmap;
                                    window.imgDefaultBlur.Stretch = Stretch.Fill;
                                    //window.imgDefaultBlur.HorizontalAlignment = HorizontalAlignment.Left;
                                    window.imgDefaultBlur.HorizontalAlignment = HorizontalAlignment.Stretch;
                                    window.imgDefaultBlur.VerticalAlignment = VerticalAlignment.Stretch;
                                    double screenwidth = SystemParameters.FullPrimaryScreenWidth;
                                    if ((int)screenwidth < 1100)
                                    {
                                        window.imgDefaultBlur.Margin = new Thickness(0, 0, 0, -10);
                                    }
                                    else
                                    {
                                        window.imgDefaultBlur.Margin = new Thickness(0, 0, 0, 0);
                                    }
                                    window.imgDefaultBlur.UpdateLayout();//Vins
                                }
                                else
                                {
                                    imgmain.UpdateLayout();//Vins
                                    RenderTargetBitmap bitmap = new RenderTargetBitmap((int)window.testR.ActualWidth, (int)window.testR.ActualHeight, 155, 150, PixelFormats.Default);
                                    bitmap.Render(imgmain);
                                    //bitmap.Render(compiledBitmapImage.Visual);

                                    window.imgDefaultBlur.Source = bitmap;
                                    window.imgDefaultBlur.Stretch = Stretch.Fill;
                                    //window.imgDefaultBlur.HorizontalAlignment = HorizontalAlignment.Left;
                                    window.imgDefaultBlur.HorizontalAlignment = HorizontalAlignment.Stretch;
                                    window.imgDefaultBlur.VerticalAlignment = VerticalAlignment.Stretch;
                                    double screenwidth = SystemParameters.FullPrimaryScreenWidth;
                                    if ((int)screenwidth < 1100)
                                    {
                                        window.imgDefaultBlur.Margin = new Thickness(0, 0, 0, -10);
                                    }
                                    else
                                    {
                                        window.imgDefaultBlur.Margin = new Thickness(0, 0, 0, 0);
                                    }
                                    window.imgDefaultBlur.UpdateLayout();//Vins
                                }
                            }
                            else
                            {
                                if (imgmain.ActualHeight > 650)
                                {
                                    imgmain.UpdateLayout();//Vins
                                    //RenderTargetBitmap bitmap = new RenderTargetBitmap((int)window.testR.ActualWidth, (int)window.testR.ActualHeight, 155, 130, PixelFormats.Default);
                                    RenderTargetBitmap bitmap = new RenderTargetBitmap((int)window.testR.ActualWidth, (int)window.testR.ActualHeight, 180, 130, PixelFormats.Default);
                                    bitmap.Render(imgmain);
                                    //bitmap.Render(compiledBitmapImage.Visual);

                                    window.imgDefaultBlur.Source = bitmap;
                                    window.imgDefaultBlur.Stretch = Stretch.Fill;
                                    //window.imgDefaultBlur.HorizontalAlignment = HorizontalAlignment.Left;
                                    window.imgDefaultBlur.HorizontalAlignment = HorizontalAlignment.Stretch;
                                    window.imgDefaultBlur.VerticalAlignment = VerticalAlignment.Stretch;
                                    double screenwidth = SystemParameters.FullPrimaryScreenWidth;
                                    //if ((int)screenwidth < 1100)
                                    //{
                                    //    window.imgDefaultBlur.Margin = new Thickness(0, 0, 0, -10);
                                    //}
                                    //else
                                    //{
                                    //    window.imgDefaultBlur.Margin = new Thickness(0, 0, 0, 0);
                                    //}
                                    window.imgDefaultBlur.UpdateLayout();//Vins
                                }
                                else
                                {
                                    imgmain.UpdateLayout();
                                    //RenderTargetBitmap bitmap = new RenderTargetBitmap((int)window.testR.ActualWidth, (int)window.testR.ActualHeight, 155, 130, PixelFormats.Default);
                                    RenderTargetBitmap bitmap = new RenderTargetBitmap((int)window.testR.ActualWidth, (int)window.testR.ActualHeight, 180, 180, PixelFormats.Default);
                                    bitmap.Render(imgmain);
                                    //bitmap.Render(compiledBitmapImage.Visual);

                                    window.imgDefaultBlur.Source = bitmap;
                                    window.imgDefaultBlur.Stretch = Stretch.Fill;
                                    //window.imgDefaultBlur.HorizontalAlignment = HorizontalAlignment.Left;
                                    window.imgDefaultBlur.HorizontalAlignment = HorizontalAlignment.Stretch;
                                    window.imgDefaultBlur.VerticalAlignment = VerticalAlignment.Stretch;
                                    double screenwidth = SystemParameters.FullPrimaryScreenWidth;
                                    if ((int)screenwidth < 1100)
                                    {
                                        window.imgDefaultBlur.Margin = new Thickness(0, 0, 0, -10);
                                    }
                                    else
                                    {
                                        window.imgDefaultBlur.Margin = new Thickness(0, 0, 0, 0);
                                    }
                                    window.imgDefaultBlur.UpdateLayout();//Vins

                                }
                            }
                        }
                    }
                    else
                    {
                        window.IMGFrame.Margin = new Thickness(0, -20, 0, 0);
                    }
                    if (window.mPreviewControl.m_pPreview != null && vidoriginal.Visibility == Visibility.Visible)
                    {
                        window.imgDefaultBlur.Source = null;
                        window.imgDefaultBlur.UpdateLayout();
                        window.mPreviewControl.m_pPreview.PreviewEnable("", 1, 1);
                    }
                }
                else
                {
                    LstMyItems myItems = ((DigiPhoto.LstMyItems)(lstImages.SelectedItem));

                    if (btnchkpreview.IsChecked == true && myItems != null)
                    {
                        //if (((DigiPhoto.LstMyItems)(lstImages.SelectedItem)).MediaType == 2)
                        //{
                        //    CurrentBitmapImage = CommonUtility.GetImageFromPath(myItems.FilePath);
                        //}

                        window.DefaultView = false;
                        window.imgDefaultBlur.Source = null;
                        window.imgDefaultBlur.UpdateLayout();
                    }
                    else
                    {
                        window.DefaultView = true;
                    }

                    //window.DefaultView = true;                  
                    if (!(MktImgPath == "" || mktImgTime == 0))
                    {
                        window.instructionVideo.Visibility = System.Windows.Visibility.Visible;
                        window.instructionVideo.Play();
                        // Added for Blur Images
                        window.instructionVideoBlur.Visibility = System.Windows.Visibility.Visible;
                        window.instructionVideoBlur.Play();
                        // End Changes
                    }
                    else
                    {
                        window.imgDefault.Visibility = System.Windows.Visibility.Visible;
                        window.imgDefaultBlur.Visibility = Visibility.Visible; // Added for Blur Images
                    }
                    window.testR.Fill = null;
                    window.testR.UpdateLayout();//Vins
                }
                if (window.instructionVideo.Visibility == System.Windows.Visibility.Visible)
                    window.instructionVideo.Play();
                else
                    window.instructionVideo.Pause();
                // Added for Blur Images
                if (window.instructionVideoBlur.Visibility == System.Windows.Visibility.Visible)
                    window.instructionVideoBlur.Play();
                else
                    window.instructionVideoBlur.Pause();
                // End Changes
                System.Windows.Forms.Screen[] screens = System.Windows.Forms.Screen.AllScreens;
                if (screens.Length > 1)
                {
                    if (screens[0].Primary)
                    {
                        System.Windows.Forms.Screen s2 = System.Windows.Forms.Screen.AllScreens[1];
                        System.Drawing.Rectangle r2 = s2.WorkingArea;

                        window.Top = r2.Top;
                        window.Left = r2.Left;
                        window.Show();
                    }
                    else
                    {
                        System.Drawing.Rectangle r2 = screens[0].WorkingArea;
                        window.Top = r2.Top;
                        window.Left = r2.Left;
                        window.Show();
                    }
                }
                else
                {
                    window.Show();
                }
            } //////end by latika
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
            finally
            {
                // MemoryManagement.FlushMemory();
            }
        }

        public void CompileEffectChangedWithBG(VisualBrush compiledBitmapImage, int ProductType, Grid img)
        {
            try
            {
                GetMktImgInfo();
                ClientView window = null;

                foreach (Window wnd in Application.Current.Windows)
                {
                    if (wnd.Title == "ClientView")
                    {
                        window = (ClientView)wnd;
                    }
                }

                if (window == null)
                {
                    window = new ClientView();
                    window.WindowStartupLocation = System.Windows.WindowStartupLocation.Manual;
                }

                window.imgNext.Visibility = System.Windows.Visibility.Collapsed;
                window.imgNext.Source = null;
                window.txtMainImage.Visibility = System.Windows.Visibility.Collapsed;
                window.stkPrint.Visibility = System.Windows.Visibility.Collapsed;
                window.btnMinimize.Visibility = Visibility.Collapsed;
                window.stkPrevNext.Visibility = Visibility.Collapsed;
                window.testR.Visibility = System.Windows.Visibility.Visible;
                window.GroupView = false;
                window.DefaultView = false;

                //////changed by latika for showing marketing images while editing.
                if (compiledBitmapImage != null && isMarketingImgShow != 1)////end
                {
                    window.testR.Fill = null;
                    window.imgDefaultBlur.Source = null;
                    compiledBitmapImage.Stretch = Stretch.Uniform;
                    window.imgDefault.Visibility = Visibility.Collapsed;
                    window.imgDefaultBlur.Visibility = System.Windows.Visibility.Collapsed;// Added for Blur Images
                    window.instructionVideoBlur.Visibility = System.Windows.Visibility.Collapsed;
                    window.instructionVideoBlur.Pause();
                    window.instructionVideo.Visibility = System.Windows.Visibility.Collapsed;
                    window.instructionVideo.Pause();
                    window.testR.Fill = compiledBitmapImage;
                    if (btnchkpreview.IsChecked == true)
                    {
                        window.IMGFrame.Margin = new Thickness(0);
                        window.testR.Margin = new Thickness(0, 20, 0, 0);
                        window.imgDefaultBlur.Visibility = Visibility.Visible;
                        window.imgDefaultBlur.Source = null;
                        if (img.ActualHeight > img.ActualWidth)
                        {
                            RenderTargetBitmap bitmap = new RenderTargetBitmap((int)window.testR.ActualWidth, (int)window.testR.ActualHeight, 145, 145, PixelFormats.Default);
                            bitmap.Render(img);
                            window.imgDefaultBlur.Source = bitmap;
                            window.imgDefaultBlur.Stretch = Stretch.Fill;
                            window.imgDefaultBlur.HorizontalAlignment = HorizontalAlignment.Left;
                            window.imgDefaultBlur.HorizontalAlignment = HorizontalAlignment.Stretch;
                            window.imgDefaultBlur.VerticalAlignment = VerticalAlignment.Center;
                            window.imgDefaultBlur.Margin = new Thickness(-150, 0, 0, 0);
                        }
                        else
                        {
                            RenderTargetBitmap bitmap = new RenderTargetBitmap((int)window.testR.ActualWidth, (int)window.testR.ActualHeight, 145, 145, PixelFormats.Default);
                            bitmap.Render(img);
                            window.imgDefaultBlur.Source = bitmap;
                            window.imgDefaultBlur.Stretch = Stretch.Fill;
                            window.imgDefaultBlur.HorizontalAlignment = HorizontalAlignment.Left;
                            window.imgDefaultBlur.HorizontalAlignment = HorizontalAlignment.Stretch;
                            window.imgDefaultBlur.VerticalAlignment = VerticalAlignment.Center;
                            window.imgDefaultBlur.Margin = new Thickness(-150, 0, 0, 0);
                        }
                    }
                    else
                    {
                        window.IMGFrame.Margin = new Thickness(0, -20, 0, 0);
                    }
                    if (window.mPreviewControl.m_pPreview != null)
                    {
                        window.mPreviewControl.m_pPreview.PreviewEnable("", 1, 1);
                    }
                }
                else
                {
                    window.DefaultView = true;
                    if (!(MktImgPath == "" || mktImgTime == 0))
                    {
                        window.instructionVideo.Visibility = System.Windows.Visibility.Visible;
                        window.instructionVideo.Play();

                        window.instructionVideoBlur.Visibility = System.Windows.Visibility.Visible;
                        window.instructionVideoBlur.Play();
                    }
                    else
                    {
                        window.imgDefault.Visibility = System.Windows.Visibility.Visible;
                        window.imgDefaultBlur.Visibility = Visibility.Visible;
                    }
                    window.testR.Fill = null;
                }
                if (window.instructionVideo.Visibility == System.Windows.Visibility.Visible)
                    window.instructionVideo.Play();
                else
                    window.instructionVideo.Pause();

                if (window.instructionVideoBlur.Visibility == System.Windows.Visibility.Visible)
                    window.instructionVideoBlur.Play();
                else
                    window.instructionVideoBlur.Pause();

                System.Windows.Forms.Screen[] screens = System.Windows.Forms.Screen.AllScreens;
                if (screens.Length > 1)
                {
                    if (screens[0].Primary)
                    {
                        System.Windows.Forms.Screen s2 = System.Windows.Forms.Screen.AllScreens[1];
                        System.Drawing.Rectangle r2 = s2.WorkingArea;

                        window.Top = r2.Top;
                        window.Left = r2.Left;
                        window.Show();
                    }
                    else
                    {
                        System.Drawing.Rectangle r2 = screens[0].WorkingArea;
                        window.Top = r2.Top;
                        window.Left = r2.Left;
                        window.Show();
                    }
                }
                else
                {
                    window.Show();
                }
            } //////end by latika
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
            finally
            {
                // MemoryManagement.FlushMemory();
            }
        }
        private void ImageSize(int size)
        {
            if (size == 16)
            {
                if (RobotImageLoader.GroupImages != null && RobotImageLoader.GroupImages.Count > 0)
                    RobotImageLoader.GroupImages.ForEach(t => { t.GridMainHeight = 140; t.GridMainWidth = 170; t.GridMainRowHeight1 = 90; t.GridMainRowHeight2 = 60; });
                if (RobotImageLoader.PrintImages != null && RobotImageLoader.PrintImages.Count > 0)
                    RobotImageLoader.PrintImages.ForEach(t => { t.GridMainHeight = 140; t.GridMainWidth = 170; t.GridMainRowHeight1 = 90; t.GridMainRowHeight2 = 60; });
                if (RobotImageLoader.robotImages != null && RobotImageLoader.robotImages.Count > 0)
                    RobotImageLoader.robotImages.ForEach(t => { t.GridMainHeight = 140; t.GridMainWidth = 170; t.GridMainRowHeight1 = 90; t.GridMainRowHeight2 = 60; });
            }
            else
            {
                if (RobotImageLoader.GroupImages != null && RobotImageLoader.GroupImages.Count > 0)
                    RobotImageLoader.GroupImages.ForEach(t => { t.GridMainHeight = 190; t.GridMainWidth = 226; t.GridMainRowHeight1 = 140; t.GridMainRowHeight2 = 50; });
                if (RobotImageLoader.PrintImages != null && RobotImageLoader.PrintImages.Count > 0)
                    RobotImageLoader.PrintImages.ForEach(t => { t.GridMainHeight = 190; t.GridMainWidth = 226; t.GridMainRowHeight1 = 140; t.GridMainRowHeight2 = 50; });
                if (RobotImageLoader.robotImages != null && RobotImageLoader.robotImages.Count > 0)
                    RobotImageLoader.robotImages.ForEach(t => { t.GridMainHeight = 190; t.GridMainWidth = 226; t.GridMainRowHeight1 = 140; t.GridMainRowHeight2 = 50; });
            }
        }
        public void PreviewPhoto()
        {
            try
            {
                Visual vis;
                if (!(bool)btnchkpreview.IsChecked)
                {
                    if (RobotImageLoader.GroupImages.Count != 0)
                    {
                        if (IMGFrame.Visibility == Visibility.Visible)
                        {
                            VisualBrush CB = new VisualBrush(ContentContainer);
                            CompileEffectChanged(CB, -1);
                        }
                        else
                        {
                            VisualBrush CB = new VisualBrush(ContentContainer);
                            CompileEffectChanged(CB, -1);
                        }
                    }
                    else
                    {
                        VisualBrush CB = new VisualBrush(ContentContainer);
                        CompileEffectChanged(CB, -1);
                    }
                }
                else
                {
                    if (vidoriginal.Visibility == Visibility.Visible)
                        vis = vidoriginal;
                    else
                    {
                        vis = img;
                    }


                    if (RobotImageLoader.GroupImages.Count != 0)
                    {
                        if (IMGFrame.Visibility == Visibility.Visible)
                        {
                            VisualBrush CB = new VisualBrush(vis);
                            CompileEffectChanged(CB, -1);
                            continueCalculating = false;
                        }
                        else
                        {
                            VisualBrush CB = new VisualBrush(vis);
                            CompileEffectChanged(CB, -1);
                            continueCalculating = false;
                        }
                    }
                    else
                    {
                        VisualBrush CB = new VisualBrush(vis);
                        CompileEffectChanged(CB, -1);
                        //CompileEffectChangedWithBG(CB, -1,img);
                        continueCalculating = false;
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                MessageBox.Show(ex.Message);
            }
        }
        #endregion

        #region Preview & Flip
        private void RotateThumbPreview()
        {
            if (isSingleScreenPreview)
            {
                clientWin.FilpFrom = 1;
                if (btnchkthumbpreview.IsChecked == true)
                {
                    LstMyItems selectedItem = ((DigiPhoto.LstMyItems)(lstImages.SelectedItem));
                    LstMyItems myitem = null;
                    if (selectedItem != null)
                    {
                        myitem = RobotImageLoader.GroupImages.OrderByDescending(o => o.PhotoId).Where(o => o.PhotoId == selectedItem.PhotoId).FirstOrDefault();
                    }
                    if (myitem == null)
                    {
                        selectedItem = RobotImageLoader.GroupImages.FirstOrDefault();
                    }
                    if (selectedItem != null)
                    {

                        txtMainImage.Text = selectedItem.Name;
                        lstImages.SelectedItem = selectedItem;
                        //clientWin._currentImageId = selectedItem.PhotoId;
                    }
                    else
                    {
                        //clientWin.imgprintgroup.Source = new BitmapImage(new Uri(@"/images/print-group.png", UriKind.Relative));
                    }

                    AngleValue += 720;
                    AngleValue %= 360;
                    TransformGroup group = new TransformGroup();
                    group.Children.Add(new RotateTransform(AngleValue));
                    //group.Children.Add(new TranslateTransform(this.ContentContainer.ActualWidth, this.ContentContainer.ActualHeight));

                    //Thickness tx = new Thickness(0, -50, 0, 0);
                    //ContentContainer.Margin = tx;
                    row0.MinHeight = 31;
                    ContentContainer.LayoutTransform = group;
                }
                else
                {

                    TransformGroup group = new TransformGroup();
                    group.Children.Add(new RotateTransform(0));
                    ContentContainer.LayoutTransform = group;
                    //ContentContainer.UpdateLayout();
                }
            }
        }
        private void FlipPreviewNew()
        {
            if (isSingleScreenPreview)
            {
                if (fullClientWin == null)
                {
                    foreach (Window wnd in Application.Current.Windows)
                    {
                        if (wnd.Title == "FullClientPreview")
                        {
                            fullClientWin = (FullClientPreview)wnd;
                            break;
                        }
                    }
                }
                OpenFullScreenPreview();
            }
            else
            {
                if (btnchkpreview.IsChecked == true)
                {
                    if (IMGFrame.Visibility == Visibility.Visible)
                    {
                        AuditLog.AddUserLog(Common.LoginUser.UserId, (int)FrameworkHelper.ActionType.PreviewPhoto, "Show Preview of " + ((DigiPhoto.LstMyItems)(lstImages.SelectedItem)).Name.ToString() + " image.", ((DigiPhoto.LstMyItems)(lstImages.SelectedItem)).PhotoId);
                    }
                }
                PreviewPhoto();
            }
        }
        private void OpenFullScreenPreview()
        {
            this.WindowState = WindowState.Minimized;
            FullClientPreview fullClientPreview = new FullClientPreview();
            System.Windows.Forms.Screen[] screens = System.Windows.Forms.Screen.AllScreens;

            LstMyItems myitem = RobotImageLoader.GroupImages.OrderByDescending(o => o.PhotoId).Where(o => o.PhotoId == _currentImageId).FirstOrDefault();
            LstMyItems selectedItem = new LstMyItems();
            if (myitem == null)
            {
                selectedItem = RobotImageLoader.GroupImages.FirstOrDefault();
            }
            else
                selectedItem = myitem;
            if (selectedItem != null)
            {
                fullClientPreview.LoadImage(selectedItem);
                if (RobotImageLoader.PrintImages.Where(o => o.PhotoId == selectedItem.PhotoId).FirstOrDefault() != null)
                {
                    fullClientPreview.imgprintgroup.Source = new BitmapImage(new Uri(@"/images/print-accept.png", UriKind.Relative));

                }
                else
                {
                    fullClientPreview.imgprintgroup.Source = new BitmapImage(new Uri(@"/images/print-group.png", UriKind.Relative));
                }
                txtMainImage.Text = selectedItem.Name;
                lstImages.SelectedItem = selectedItem;
                fullClientPreview._currentImageId = selectedItem.PhotoId;
                fullClientPreview.txtblkImageId.Text = selectedItem.Name;
            }
            else
            {
                fullClientPreview.imgprintgroup.Source = new BitmapImage(new Uri(@"/images/print-group.png", UriKind.Relative));
            }
            if (true)
            {
                TransformGroup grouptestR = new TransformGroup();
                grouptestR.Children.Add(new RotateTransform(AngleValue));
                fullClientPreview.grdContent.LayoutTransform = grouptestR;
            }
            fullClientPreview.FilpFrom = 1;
            fullClientPreview.Show();

            if (screens.Length > 1)
            {
                if (screens[0].Primary)
                {
                    fullClientPreview.WindowState = WindowState.Normal;
                    System.Drawing.Rectangle workingArea = System.Windows.Forms.Screen.AllScreens[1].WorkingArea;
                    fullClientPreview.Left = workingArea.Left;
                    fullClientPreview.Top = workingArea.Top;
                    fullClientPreview.Width = workingArea.Width;
                    fullClientPreview.Height = workingArea.Height;
                    fullClientPreview.WindowStartupLocation = System.Windows.WindowStartupLocation.Manual;
                    fullClientPreview.WindowState = WindowState.Maximized;
                    //fullClientPreview.Topmost = true;
                }
            }
        }
        private void btnpreview_Click(object sender, RoutedEventArgs e)
        {
            FlipPreviewNew();
        }
        private void btnthumbpreview_Click(object sender, RoutedEventArgs e)
        {
            RotateThumbPreview();
        }

        #endregion
        private void btnUnlock_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                AddUserControlModalDialog();
                ModalDialog.OkButton.IsDefault = true;
                var res = ModalDialog.ShowHandlerDialog("Digi");
                ModalDialog.OkButton.IsDefault = false;
                if (res)
                {
                    PhotoBusiness photoBiz = new PhotoBusiness();
                    var item = photoBiz.GetPhotoDetailsbyPhotoId(Convert.ToInt32(unlockImageId));

                    RobotImageLoader.LstUnlocknames.Add(unlockImageId.ToString());
                    if (item.DG_MediaType == 1)
                        imgmain.Source = CommonUtility.GetImageFromPath(System.IO.Path.Combine(item.HotFolderPath, item.DG_Photos_CreatedOn.ToString("yyyyMMdd"), item.DG_Photos_FileName));
                    txtMainImage.Text = Convert.ToString(Photoname);
                    splockedImages.Visibility = Visibility.Collapsed;
                    spaddtoalbumnunlockedImages.Visibility = Visibility.Visible;
                    spunlockedImages.Visibility = Visibility.Visible;
                    if (Convert.ToBoolean(ModalDialog.rdbPermanantly.IsChecked))
                        photoBiz.immoderate(unlockImageId);
                    else
                        RobotImageLoader.LstUnlocknames.Add(unlockImageId.ToString());
                    var unlockedimage = RobotImageLoader.robotImages.Where(t => t.PhotoId == unlockImageId).FirstOrDefault();
                    unlockedimage.FilePath = System.IO.Path.Combine(item.HotFolderPath, "Thumbnails_Big", item.DG_Photos_CreatedOn.ToString("yyyyMMdd"), item.DG_Photos_FileName);
                    unlockedimage.BigThumbnailPath = System.IO.Path.Combine(item.HotFolderPath, "Thumbnails_Big", item.DG_Photos_CreatedOn.ToString("yyyyMMdd"), item.DG_Photos_FileName);
                    var LstItem = RobotImageLoader.robotImages.Where(t => t.PhotoId == unlockImageId).FirstOrDefault();
                    if (LstItem != null)
                    {
                        LstItem.BigThumbnailPath = unlockedimage.BigThumbnailPath;
                    }
                    unlockedimage.IsLocked = Visibility.Visible;
                    unlockedimage.IsPassKeyVisible = Visibility.Collapsed;
                    //unlockedimage.BmpImageGroup = new BitmapImage(new Uri(@"/images/view-group.png", UriKind.Relative));
                    var GRPunlockedimage = RobotImageLoader.GroupImages.Where(t => t.PhotoId == unlockImageId).FirstOrDefault();
                    if (GRPunlockedimage != null)
                    {

                        unlockedimage.BmpImageGroup = new BitmapImage(new Uri(@"/images/view-accept.png", UriKind.Relative));
                        GRPunlockedimage.FilePath = System.IO.Path.Combine(item.HotFolderPath, "Thumbnails_Big", item.DG_Photos_CreatedOn.ToString("yyyyMMdd"), item.DG_Photos_FileName);
                        GRPunlockedimage.IsLocked = Visibility.Visible;
                        GRPunlockedimage.IsPassKeyVisible = Visibility.Collapsed;
                        GRPunlockedimage.BmpImageGroup = new BitmapImage(new Uri(@"/images/view-accept.png", UriKind.Relative));
                    }
                    else
                    {
                        unlockedimage.BmpImageGroup = new BitmapImage(new Uri(@"/images/view-group.png", UriKind.Relative));
                    }
                    var PRNunlockedimage = RobotImageLoader.PrintImages.Where(t => t.PhotoId == unlockImageId).FirstOrDefault();
                    if (PRNunlockedimage != null)
                    {
                        unlockedimage.PrintGroup = new BitmapImage(new Uri(@"/images/print-accept.png", UriKind.Relative));
                        PRNunlockedimage.FilePath = System.IO.Path.Combine(item.HotFolderPath, "Thumbnails_Big", item.DG_Photos_CreatedOn.ToString("yyyyMMdd"), item.DG_Photos_FileName);
                        PRNunlockedimage.IsLocked = Visibility.Visible;
                        PRNunlockedimage.IsPassKeyVisible = Visibility.Collapsed;
                        PRNunlockedimage.PrintGroup = new BitmapImage(new Uri(@"/images/print-accept.png", UriKind.Relative));
                    }
                    else
                    {
                        unlockedimage.PrintGroup = new BitmapImage(new Uri(@"/images/print-group.png", UriKind.Relative));
                    }


                    splockedImages.Visibility = Visibility.Collapsed;
                    spaddtoalbumnunlockedImages.Visibility = Visibility.Visible;
                    spunlockedImages.Visibility = Visibility.Visible;
                    lstImages.SelectedItem = unlockedimage;
                    int index = lstImages.SelectedIndex;
                    if (lstImages.Items.Count > 0)
                    {
                        lstImages.Items.RemoveAt(index);
                    }

                    if (pagename == "Saveback" && vwGroup.Text == "View Group")
                    {
                        lstImages.Items.Insert(index, GRPunlockedimage);
                        lstImages.SelectedItem = GRPunlockedimage;
                    }
                    else if (pagename == "Saveback" && vwGroup.Text == "View All")
                    {
                        lstImages.Items.Insert(index, GRPunlockedimage);
                        lstImages.SelectedItem = GRPunlockedimage;
                    }
                    else
                    {
                        lstImages.Items.Insert(index, unlockedimage);
                        lstImages.SelectedItem = unlockedimage;
                    }
                    //ListBoxItem listBoxItem = (ListBoxItem)lstImages.ItemContainerGenerator.ContainerFromItem(lstImages.SelectedItem);
                    // listBoxItem.Focus();
                    btnEdit.IsEnabled = true;
                    AuditLog.AddUserLog(Common.LoginUser.UserId, (int)FrameworkHelper.ActionType.UnlockPhoto, "Unlock " + Photoname + " image.");
                }
                ModalDialog.Dispose();
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
            finally
            {
                MemoryManagement.FlushMemory();
            }
        }

        private void lstImages_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                MediaStop();
                if (clientWin == null)
                {
                    foreach (Window wnd in Application.Current.Windows)
                    {
                        if (wnd.Title == "ClientView")
                        {
                            clientWin = (ClientView)wnd;
                            break;
                        }
                    }
                }
                if (lstImages.Items.Count > 0 && lstImages.SelectedItem != null)
                {
                    if (IMGFrame.Visibility == Visibility.Visible)
                    {
                        if (!RobotImageLoader.Is9ImgViewActive)
                        {
                            foreach (LstMyItems o in lstImages.Items)
                            {
                                o.GridMainHeight = 140;
                                o.GridMainWidth = 170;
                                o.GridMainRowHeight1 = 90;
                                o.GridMainRowHeight2 = 60;
                            }
                        }

                        lstImages.ScrollIntoView(lstImages.SelectedItem);
                        txtMainImage.Text = ((DigiPhoto.LstMyItems)(lstImages.SelectedItem)).Name;
                        RobotImageLoader.PhotoId = ((DigiPhoto.LstMyItems)(lstImages.SelectedItem)).Name;
                        RobotImageLoader.UniquePhotoId = ((DigiPhoto.LstMyItems)(lstImages.SelectedItem)).PhotoId;
                        LstMyItems TempItem = (LstMyItems)lstImages.SelectedItem;
                        _currentImage = ((DigiPhoto.LstMyItems)(lstImages.SelectedItem)).Name;
                        _currentImageId = ((DigiPhoto.LstMyItems)(lstImages.SelectedItem)).PhotoId;
                        var searItem = RobotImageLoader.GroupImages.Where(t => t.PhotoId == _currentImageId).FirstOrDefault();
                        if (searItem != null)
                        {
                            ImgAddToGroup.Source = new BitmapImage(new Uri(@"/images/view-accept.png", UriKind.Relative));
                            btnImageAddToGroup.ToolTip = "Remove from group";
                            btnImageAddToGroup.CommandParameter = "Remove from group";
                            searItem = null;

                        }
                        else
                        {
                            ImgAddToGroup.Source = new BitmapImage(new Uri(@"/images/view-group.png", UriKind.Relative));
                            btnImageAddToGroup.ToolTip = "Add to group";
                            btnImageAddToGroup.CommandParameter = "Add to group";
                        }
                        var searpItem = RobotImageLoader.PrintImages.Where(t => t.PhotoId == _currentImageId).FirstOrDefault();
                        if (searpItem != null)
                        {
                            imgprintgroup.Source = new BitmapImage(new Uri(@"/images/print-accept.png", UriKind.Relative));
                            if (isSingleScreenPreview)
                                clientWin.imgprintgroup.Source = new BitmapImage(new Uri(@"/images/print-accept.png", UriKind.Relative));
                        }
                        else
                        {
                            imgprintgroup.Source = new BitmapImage(new Uri(@"/images/print-group.png", UriKind.Relative));
                            if (isSingleScreenPreview)
                                clientWin.imgprintgroup.Source = new BitmapImage(new Uri(@"/images/print-group.png", UriKind.Relative));
                        }
                        //// Below code added by Anis for full client preview images instead of thumbnail images
                        //if (btnchkpreview.IsChecked == true)
                        //{
                        //    FlipPreviewNew();
                        //}
                        Int32 Photo_PKey = _currentImageId;//objdbLayer.GetPhotoIDByRFFID(((DigiPhoto.LstMyItems)(lstImages.SelectedItem)).Name);
                        unlockImageId = Photo_PKey;
                        LstMyItems myItems = ((DigiPhoto.LstMyItems)(lstImages.SelectedItem));
                        var result = RobotImageLoader.LstUnlocknames.Where(x => x.Equals(myItems.PhotoId.ToString()));
                        PhotoBusiness phBiz = new PhotoBusiness();
                        if (phBiz.GetModeratePhotos(Photo_PKey) && (result.Count() == 0))
                        {
                            btnEdit.IsEnabled = false;
                            splockedImages.Visibility = Visibility.Visible;

                            spaddtoalbumnunlockedImages.Visibility = Visibility.Collapsed;
                            spunlockedImages.Visibility = Visibility.Collapsed;
                            mainFrame.Source = null;
                            imgmain.Source = new BitmapImage(new Uri(LoginUser.DigiFolderPath + "/Locked.png"));
                            CurrentBitmapImage = new BitmapImage(new Uri(LoginUser.DigiFolderPath + "/Locked.png"));
                        }
                        else
                        {
                            splockedImages.Visibility = Visibility.Collapsed;

                            spaddtoalbumnunlockedImages.Visibility = Visibility.Visible;
                            spunlockedImages.Visibility = Visibility.Visible;

                            btnEdit.IsEnabled = true;

                            if (myItems.MediaType == 1)
                            {
                                CurrentBitmapImage = CommonUtility.GetImageFromPath(myItems.BigThumbnailPath);
                                imgmain.Source = CurrentBitmapImage;

                            }
                            //if (((DigiPhoto.LstMyItems)(lstImages.SelectedItem)).FrameBrdr != null)
                            //    mainFrame.Source = new BitmapImage(new Uri(((DigiPhoto.LstMyItems)(lstImages.SelectedItem)).FrameBrdr));
                            //else
                            mainFrame.Source = null;
                            imgmain.UpdateLayout();
                            mainFrame.UpdateLayout();

                        }
                        if (TempItem.MediaType == 2 || TempItem.MediaType == 3) //Guest Videos and Processed Videos
                        {
                            btnEdit.IsEnabled = false;
                            img.Visibility = Visibility.Hidden;
                            vidoriginal.Visibility = Visibility.Visible;
                            string vidPath = TempItem.FileName;
                            if (!String.IsNullOrEmpty(vidPath))
                            {
                                //MediaStop();
                                if (TempItem.MediaType == 2)
                                {
                                    using (FileStream fileStream = File.OpenRead(System.IO.Path.Combine(TempItem.HotFolderPath, "Videos", TempItem.CreatedOn.ToString("yyyyMMdd"), vidPath)))
                                    {
                                        vsMediaFileName = fileStream.Name;
                                    }
                                }
                                else
                                {
                                    using (FileStream fileStream = File.OpenRead(System.IO.Path.Combine(TempItem.HotFolderPath, "ProcessedVideos", TempItem.CreatedOn.ToString("yyyyMMdd"), vidPath)))
                                    {
                                        vsMediaFileName = fileStream.Name;
                                    }
                                }
                                Dispatcher.Invoke(new Action(() => MediaPlay()));
                                txtMainVideo.Text = TempItem.Name;
                            }
                        }
                        else
                        {
                            btnEdit.IsEnabled = true;
                            img.Visibility = Visibility.Visible;
                            vidoriginal.Visibility = Visibility.Hidden;
                            // MediaStop();
                        }

                        // Below code added by Anis for full client preview images instead of thumbnail images
                        //if (btnchkpreview.IsChecked == true)
                        //{
                        //    FlipPreviewNew();
                        //    AuditLog.AddUserLog(Common.LoginUser.UserId, (int)FrameworkHelper.ActionType.PreviewPhoto, "Show Preview of " + ((DigiPhoto.LstMyItems)(lstImages.SelectedItem)).Name.ToString() + " image.", ((DigiPhoto.LstMyItems)(lstImages.SelectedItem)).PhotoId);
                        //}

                        //if (btnchkpreview.IsChecked == true)
                        //{
                        //    AuditLog.AddUserLog(Common.LoginUser.UserId, (int)FrameworkHelper.ActionType.PreviewPhoto, "Show Preview of " + ((DigiPhoto.LstMyItems)(lstImages.SelectedItem)).Name.ToString() + " image.", ((DigiPhoto.LstMyItems)(lstImages.SelectedItem)).PhotoId);
                        //}

                    }
                    else
                    {
                        LstMyItems myItems = ((DigiPhoto.LstMyItems)(lstImages.SelectedItem));
                        Int32 Photo_PKey = ((DigiPhoto.LstMyItems)(lstImages.SelectedItem)).PhotoId;
                        var result = RobotImageLoader.LstUnlocknames.Where(x => x.Equals(Photo_PKey.ToString()));

                        //Added by VIns 06 Dec 2021
                        txtMainImage.Text = ((DigiPhoto.LstMyItems)(lstImages.SelectedItem)).Name;
                        _currentImage = ((DigiPhoto.LstMyItems)(lstImages.SelectedItem)).Name;
                        _currentImageId = ((DigiPhoto.LstMyItems)(lstImages.SelectedItem)).PhotoId;
                        //
                        //RobotImageLoader.curItem = myItems;
                        PhotoBusiness phBiz = new PhotoBusiness();
                        if (phBiz.GetModeratePhotos(Photo_PKey) && (result.Count() == 0))
                        {
                            btnEdit.IsEnabled = false;
                            splockedImages.Visibility = Visibility.Visible;

                            spaddtoalbumnunlockedImages.Visibility = Visibility.Collapsed;
                            spunlockedImages.Visibility = Visibility.Collapsed;
                            mainFrame.Source = null;
                            imgmain.Source = new BitmapImage(new Uri(LoginUser.DigiFolderPath + "/Locked.png"));
                            CurrentBitmapImage = new BitmapImage(new Uri(LoginUser.DigiFolderPath + "/Locked.png"));
                        }
                        else
                        {
                            splockedImages.Visibility = Visibility.Collapsed;

                            spaddtoalbumnunlockedImages.Visibility = Visibility.Visible;
                            spunlockedImages.Visibility = Visibility.Visible;
                            btnEdit.IsEnabled = true;
                            if (myItems.MediaType == 1)
                                unlockImage = CommonUtility.GetImageFromPath(myItems.BigThumbnailPath);
                            imgmain.Source = unlockImage;

                            //if (((DigiPhoto.LstMyItems)(lstImages.SelectedItem)).FrameBrdr != null)
                            //{
                            //    try
                            //    {
                            //        mainFrame.Source = new BitmapImage(new Uri(((DigiPhoto.LstMyItems)(lstImages.SelectedItem)).FrameBrdr));
                            //    }
                            //    catch (Exception en)
                            //    {
                            //        string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(en);
                            //        ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                            //    }
                            //}
                            //else
                            mainFrame.Source = null;
                            if (((DigiPhoto.LstMyItems)(lstImages.SelectedItem)).MediaType == 1)
                                CurrentBitmapImage = CommonUtility.GetImageFromPath(myItems.BigThumbnailPath);

                        }
                        if (myItems.MediaType == 2 || myItems.MediaType == 3)
                            btnEdit.IsEnabled = false;
                        else
                            btnEdit.IsEnabled = true;
                    }
                    //Added by Vins on 6Dec2021
                    if (btnchkpreview.IsChecked == true)
                    {
                        FlipPreviewNew();
                        AuditLog.AddUserLog(Common.LoginUser.UserId, (int)FrameworkHelper.ActionType.PreviewPhoto, "Show Preview of " + ((DigiPhoto.LstMyItems)(lstImages.SelectedItem)).Name.ToString() + " image.", ((DigiPhoto.LstMyItems)(lstImages.SelectedItem)).PhotoId);
                    }
                    //if (btnchkpreview.IsChecked == true)
                    //{
                    //    AuditLog.AddUserLog(Common.LoginUser.UserId, (int)FrameworkHelper.ActionType.PreviewPhoto, "Show Preview of " + ((DigiPhoto.LstMyItems)(lstImages.SelectedItem)).Name.ToString() + " image.", ((DigiPhoto.LstMyItems)(lstImages.SelectedItem)).PhotoId);
                    //}
                }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
            finally
            {
                //MemoryManagement.FlushMemory();
            }
        }
        private void Window_Unloaded(object sender, RoutedEventArgs e)
        {

        }
        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e);
            continueCalculating = false;
            if (lstImages.Items.Count > 0 && lstImages != null)
            {
                lstImages.Items.Clear();
            }

            MemoryManagement.FlushMemory();
        }
        private void btnRemoveGroup_Click(object sender, RoutedEventArgs e)
        {
            if (IsEnableGrouping)
            {
                if (RobotImageLoader.GroupImages.Count > 0)
                {
                    AddUserControlSaveGroup();
                    savegroupusercontrol.txtGroupName.Focus();
                    FocusManager.SetFocusedElement(this, savegroupusercontrol.txtGroupName);
                    Keyboard.Focus(savegroupusercontrol.txtGroupName);
                    savegroupusercontrol.SaveButton.IsDefault = true;
                    LstMyItems myItems = ((DigiPhoto.LstMyItems)(lstImages.SelectedItem));
                    if (myItems != null)
                    {
                        if (myItems.MediaType != 1)
                        {
                            imageInfo.IsVideo = true;
                            gdMediaPlayer.Visibility = Visibility.Collapsed;
                        }
                        else
                            imageInfo.IsVideo = false;
                        savegroupusercontrol.Visibility = Visibility.Visible;
                        savegroupusercontrol.ClearControls();
                    }

                }
                else
                {
                    if (RobotImageLoader.PrintImages.Count > 0)
                    {
                        ClearGroupforPrint();
                    }
                    else
                    {
                        MessageBox.Show("Please add images to group!");
                    }
                }

            }
            else
            {
                ClearGroup(this, e);
            }

        }
        public void ClearGroup(object sender, EventArgs e)
        {
            try
            {
                try
                {
                    RobotImageLoader.GroupImages.Clear();
                }
                catch { }

                MediaStop();
                RobotImageLoader.currentCount = 0;
                RobotImageLoader.IsZeroSearchNeeded = true;
                flgGridWithoutPreview = true;
                if (RobotImageLoader.ViewGroupedImagesCount != null)
                    RobotImageLoader.ViewGroupedImagesCount.Clear();

                RobotImageLoader.thumbSet = scrollIndexWithoutPreview;
                ////For showing the paginate buttons and hiding the scrollbars
                btnprev.Visibility = System.Windows.Visibility.Visible;
                btnnext.Visibility = System.Windows.Visibility.Visible;
                ScrollViewer.SetVerticalScrollBarVisibility(lstImages, ScrollBarVisibility.Hidden);
                if (RobotImageLoader.GroupImages != null)
                {
                    IMGFrame.Visibility = Visibility.Collapsed;
                    Grid.SetColumnSpan(thumbPreview, 2);
                    Grid.SetColumn(thumbPreview, 0);
                    thumbPreview.Margin = new Thickness(0);
                    btnEdit.IsEnabled = false;
                    /////////////////////////Logic to save group photos for future use of Groups//////////////
                    ///

                    if (RobotImageLoader.GroupImages != null && RobotImageLoader.GroupImages.Count > 0)
                    {
                        if (lstImages.Items.Count > 0 && lstImages != null)
                        {
                            lstImages.Items.Clear();
                        }
                        RobotImageLoader.GroupImages.Clear();
                        if (RobotImageLoader.PrintImages == null)
                            RobotImageLoader.PrintImages = new List<LstMyItems>();

                        if (RobotImageLoader.PrintImages != null)
                        {
                            RobotImageLoader.PrintImages.Clear();
                        }

                        RobotImageLoader.RFID = "0";
                        RobotImageLoader.SearchCriteria = "";
                        RobotImageLoader.LoadImages();
                        continueCalculating = false;
                        num = 0;
                        vwGroup.Text = "View Group";


                        grdSelectAll.Visibility = Visibility.Visible;
                        LoadImages();
                        SPSelectAll.Visibility = Visibility.Visible;
                    }
                    else
                    {
                        if (RobotImageLoader.PrintImages != null)
                        {
                            //RobotImageLoader.PrintImages = new List<LstMyItems>();
                            RobotImageLoader.PrintImages.Clear();
                            if (lstImages.Items.Count > 0 && lstImages != null)
                            {
                                lstImages.Items.Clear();
                            }

                            if (RobotImageLoader.GroupImages == null)
                                RobotImageLoader.GroupImages = new List<LstMyItems>();

                            RobotImageLoader.GroupImages.Clear();
                            //RobotImageLoader.GroupImages = new List<LstMyItems>();
                            RobotImageLoader.RFID = "0";
                            RobotImageLoader.SearchCriteria = "";
                            RobotImageLoader.LoadImages();
                            continueCalculating = false;
                            num = 0;
                            vwGroup.Text = "View Group";

                            LoadImages();
                            SetMessageText("Grouped");
                            grdSelectAll.Visibility = System.Windows.Visibility.Visible;
                        }

                    }
                }
                else
                {
                    if (RobotImageLoader.PrintImages != null)
                    {
                        RobotImageLoader.PrintImages.Clear();// = new List<LstMyItems>();
                        if (lstImages.Items.Count > 0 && lstImages != null)
                        {
                            lstImages.Items.Clear();
                        }
                        if (RobotImageLoader.GroupImages == null)
                            RobotImageLoader.GroupImages = new List<LstMyItems>();

                        RobotImageLoader.GroupImages.Clear();

                        RobotImageLoader.currentCount = 0;
                        RobotImageLoader.IsZeroSearchNeeded = true;
                        RobotImageLoader.RFID = "0";
                        RobotImageLoader.SearchCriteria = "";
                        RobotImageLoader.LoadImages();
                        continueCalculating = false;
                        num = 0;
                        vwGroup.Text = "View Group";
                        SetMessageText("Grouped");
                        grdSelectAll.Visibility = Visibility.Visible;
                        LoadImages();
                    }
                }

                FillImageList();
                CheckSelectAllGroup();
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }
        private void ClearGroupforPrint()
        {
            try
            {
                if (lstImages.Items.Count > 0 && lstImages != null)
                    RobotImageLoader.GroupImages = new List<LstMyItems>();
                RobotImageLoader.PrintImages = new List<LstMyItems>();
                RobotImageLoader.RFID = "0";
                RobotImageLoader.SearchCriteria = "";
                RobotImageLoader.LoadImages();
                continueCalculating = false;
                RobotImageLoader.currentCount = 0;
                RobotImageLoader.IsZeroSearchNeeded = true;
                num = 0;
                vwGroup.Text = "View Group";
                grdSelectAll.Visibility = Visibility.Visible;
                LoadImages();
                txtSelectedImages.Visibility = System.Windows.Visibility.Visible;
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }
        private void btnPlaceOrder_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                vsMediaFileName = string.Empty;
                MediaStop();
                DigiPhoto.Orders.PlaceOrder Pc = new Orders.PlaceOrder();
                Pc.CtrlSelectedAlbumlist.PrintOrderPageList.Clear();
                RobotImageLoader.PrintImages.ForEach(o => o.PhotoPrintPositionList.Clear());
                Pc.Show();
                Pc.LoadProductType(sender);
                this.Close();
            }
            catch (Exception en)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(en);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }
        private void chkSelectAll_Click(object sender, RoutedEventArgs e)
        {
            LstMyItems selectedItem = (LstMyItems)lstImages.SelectedItem;
            if (vwGroup.Text != "View Group")
            {
                string printImgName = chkSelectAll.IsChecked == true ? @"/images/print-accept.png" : @"/images/print-group.png";

                foreach (LstMyItems curItem in lstImages.Items)
                {
                    try
                    {
                        var printitem = RobotImageLoader.PrintImages.Where(t => t.PhotoId == curItem.PhotoId).FirstOrDefault();
                        ListBoxItem listBoxItem = (ListBoxItem)lstImages.ItemContainerGenerator.ContainerFromItem(curItem);
                        if (chkSelectAll.IsChecked == true && printitem == null)
                        {
                            LstMyItems _myitem = new LstMyItems();
                            _myitem = curItem;
                            //_myitem.FilePath = LoginUser.DigiFolderThumbnailPath + curItem.FileName;
                            _myitem.FilePath = curItem.FilePath;
                            _myitem.Name = curItem.Name;
                            _myitem.PhotoId = curItem.PhotoId;
                            _myitem.MediaType = curItem.MediaType;
                            _myitem.VideoLength = curItem.VideoLength;
                            _myitem.FileName = curItem.FileName;
                            _myitem.CreatedOn = curItem.CreatedOn;
                            RobotImageLoader.PrintImages.Add(_myitem);
                        }
                        else if (chkSelectAll.IsChecked == false && printitem != null)
                        {
                            RobotImageLoader.PrintImages.Remove(printitem);
                            //Update the BmpImageGroup image in main list i.e. robotImages
                            var rImage = RobotImageLoader.robotImages.Where(r => r.PhotoId == curItem.PhotoId).FirstOrDefault();
                            if (rImage != null)
                            {
                                rImage.PrintGroup = new BitmapImage(new Uri(@"/images/print-group.png", UriKind.Relative));
                            }
                        }
                        imgprintgroup.Source = new BitmapImage(new Uri(printImgName, UriKind.Relative));

                        /////list image replace
                        ContentPresenter myContentPresenter = FindVisualChild<ContentPresenter>(listBoxItem);
                        // Finding textBlock from the DataTemplate that is set on that ContentPresenter
                        DataTemplate myDataTemplate = myContentPresenter.ContentTemplate;
                        Grid myTextBlock = (Grid)myDataTemplate.FindName("grdMain", myContentPresenter);
                        ((System.Windows.Controls.Image)(((Button)(((System.Windows.Controls.Panel)((((Grid)(myTextBlock.FindName("Printbtns")))))).Children[0])).Content)).Source = new BitmapImage(new Uri(printImgName, UriKind.Relative));

                    }
                    catch (Exception ex)
                    {
                        string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                        ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                    }
                    finally
                    {

                    }
                }

                SetMessageText("PrintGrouped");
            }
            else
            {
                //string printImgName = chkSelectAll.IsChecked == true ? @"/images/print-accept.png" : @"/images/print-group.png";
                string grpImgName = chkSelectAll.IsChecked == true ? @"/images/view-accept.png" : @"/images/view-group.png";
                foreach (LstMyItems curItem in lstImages.Items)
                {
                    try
                    {
                        var grpitem = RobotImageLoader.GroupImages.Where(t => t.PhotoId == curItem.PhotoId).FirstOrDefault();

                        ListBoxItem listBoxItem = (ListBoxItem)lstImages.ItemContainerGenerator.ContainerFromItem(curItem);

                        if (chkSelectAll.IsChecked == true && grpitem == null)
                        {
                            LstMyItems _myitem = new LstMyItems();
                            _myitem = curItem;
                            _myitem.PageNo = curItem.PageNo;
                            _myitem.StoryBookId = curItem.StoryBookId;
                            _myitem.IsVosDisplay = curItem.IsVosDisplay;

                            _myitem.BmpImageGroup = new BitmapImage(new Uri(@"/images/view-accept.png", UriKind.Relative));

                            RobotImageLoader.GroupImages.Add(_myitem);
                            if (RobotImageLoader.ViewGroupedImagesCount == null)
                                RobotImageLoader.ViewGroupedImagesCount = new List<string>();
                            RobotImageLoader.ViewGroupedImagesCount.Add(_myitem.Name);
                        }
                        else if (chkSelectAll.IsChecked == false && grpitem != null)
                        {
                            RobotImageLoader.GroupImages.Remove(grpitem);
                            if (RobotImageLoader.ViewGroupedImagesCount != null && RobotImageLoader.ViewGroupedImagesCount.Count > 0)
                                RobotImageLoader.ViewGroupedImagesCount.Remove(grpitem.Name);
                            //Update the BmpImageGroup image in main list i.e. robotImages
                            var rImage = RobotImageLoader.robotImages.Where(r => r.PhotoId == grpitem.PhotoId).FirstOrDefault();
                            if (rImage != null)
                            {
                                rImage.BmpImageGroup = new BitmapImage(new Uri(@"/images/view-group.png", UriKind.Relative));
                            }
                        }

                        //                      ShowHideFullScreenBtn();

                        //imgprintgroup.Source = new BitmapImage(new Uri(printImgName, UriKind.Relative));
                        /////list image replace
                        ContentPresenter myContentPresenter = FindVisualChild<ContentPresenter>(listBoxItem);
                        // Finding textBlock from the DataTemplate that is set on that ContentPresenter
                        DataTemplate myDataTemplate = myContentPresenter.ContentTemplate;
                        Grid myTextBlock = (Grid)myDataTemplate.FindName("grdMain", myContentPresenter);
                        ((System.Windows.Controls.Image)(((Button)(((System.Windows.Controls.Panel)((((Grid)(myTextBlock.FindName("Printbtns")))))).Children[2])).Content)).
                            Source = new BitmapImage(new Uri(grpImgName, UriKind.Relative));

                        ImgAddToGroup.Source = new BitmapImage(new Uri(grpImgName, UriKind.Relative));
                    }
                    catch (Exception ex)
                    {
                        string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                        ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                    }
                    finally
                    {

                    }
                }
                SetMessageText("Grouped");
            }

            if (selectedItem != null)
            {
                lstImages.SelectedItem = selectedItem;
                lstImages.Focus();

                var psearItem = RobotImageLoader.PrintImages.Where(t => t.PhotoId == selectedItem.PhotoId).FirstOrDefault();
                if (psearItem != null)
                {
                    imgprintgroup.Source = new BitmapImage(new Uri(@"/images/print-accept.png", UriKind.Relative));
                    //btnImageAddToGroup.ToolTip = "Remove from group";
                    psearItem = null;
                }
                else
                {
                    imgprintgroup.Source = new BitmapImage(new Uri(@"/images/print-group.png", UriKind.Relative));
                    //btnImageAddToGroup.ToolTip = "Add to group";
                }

            }

        }
        private void btnScrollUp_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if ((RobotImageLoader.SearchCriteria == "TimeWithQrcode"))
                {
                    //searchDetails.PageNumber = searchDetails.PageNumber + 1;
                    if (RobotImageLoader.robotImages != null && RobotImageLoader.robotImages.Count > 0)
                    {
                        searchDetails.StartIndex = RobotImageLoader.robotImages.Min(o => o.PhotoId);
                        searchDetails.NewRecord = 1;
                        MovePage(searchDetails);
                    }
                }
                else if ((RobotImageLoader.SearchCriteria == "PhotoId") && RobotImageLoader.RFID != "0")
                {
                    if (RobotImageLoader.robotImages != null && RobotImageLoader.robotImages.Count > 0)
                    {
                        RobotImageLoader.StartIndexRFID = RobotImageLoader.robotImages.Min(o => o.PhotoId);
                        RobotImageLoader.NewRecord = 1;
                        MovePagePhotoId(searchDetails);
                    }
                }
                else
                {
                    NextRec();//reversed
                }
            }
            finally
            {
                MemoryManagement.FlushMemory();
            }
        }
        private void MovePagePhotoId(SearchDetailInfo searchDetails)
        {
            if (RobotImageLoader.robotImages != null && RobotImageLoader.Is16ImgViewActive == true && RobotImageLoader.Is9ImgViewActive == false)
            {
                ImageSize(16);
            }
            else
            {
                ImageSize(9);
            }
            if (RobotImageLoader.robotImages != null && RobotImageLoader.robotImages.Count > 0)
            {

                //if (RobotImageLoader.robotImages.Max(o => o.PhotoId) == searchDetails.MaxPhotoId || RobotImageLoader.robotImages.Min(o => o.PhotoId) == searchDetails.MinPhotoId)
                if (RobotImageLoader.NewRecord == 0)
                {

                    var lstMaxSubStore = RobotImageLoader.robotImages.FirstOrDefault(o => o.PhotoId == RobotImageLoader.MaxPhotoIdCriteria);

                    // if (RobotImageLoader.robotImages[RobotImageLoader.robotImages.Count - 1].PhotoId == RobotImageLoader.MaxPhotoIdCriteria)//|| RobotImageLoader.robotImages.Min(o => o.PhotoId) == 481)
                    if (lstMaxSubStore != null)
                    {
                        RobotImageLoader.NewRecord = 1;
                        RobotImageLoader.StartIndexRFID = RobotImageLoader.MaxPhotoIdCriteria + 1;
                        MessageBox.Show("No more records found!", "DigiPhoto i-Mix", MessageBoxButton.OK, MessageBoxImage.Information);
                        LoadImagePhotoId();
                        return;
                    }
                }
                else
                {
                    var lstMinSubStore = RobotImageLoader.robotImages.FirstOrDefault(o => o.PhotoId == RobotImageLoader.MinPhotoIdCriteria);
                    //if (RobotImageLoader.robotImages[0].PhotoId == RobotImageLoader.MinPhotoIdCriteria)//|| RobotImageLoader.robotImages.Min(o => o.PhotoId) == 481)
                    if (lstMinSubStore != null)
                    {
                        RobotImageLoader.NewRecord = 0;
                        RobotImageLoader.StartIndexRFID = RobotImageLoader.MinPhotoIdCriteria - 1;
                        MessageBox.Show("No more records found!", "DigiPhoto i-Mix", MessageBoxButton.OK, MessageBoxImage.Information);
                        LoadImagePhotoId();
                        return;
                    }
                }
            }
            RobotImageLoader._rfidSearch = 1;
            //FillImageList();
            //CheckSelectAllGroup();
            //RobotImageLoader.ViewGroupedImagesCount.Clear();
            RobotImageLoader.LoadImages(searchDetails);
            LoadImages();
            FillImageList();
            CheckSelectAllGroup();
            if (RobotImageLoader.ViewGroupedImagesCount != null && RobotImageLoader.ViewGroupedImagesCount.Count > 0)
            {
                RobotImageLoader.ViewGroupedImagesCount.Clear();
            }
            ///RobotImageLoader.ViewGroupedImagesCount.Clear();
            //TogglePageButton(searchDetails);
        }
        private void PrevRec()
        {
            long maxPhotoId = 0;
            GetNewMaxId(out maxPhotoId, RobotImageLoader.MediaTypes);
            //if (RobotImageLoader._objnewincrement != null && RobotImageLoader._objnewincrement.Count > 0 && RobotImageLoader.MaxPhotoId == RobotImageLoader._objnewincrement[0].PhotoId)
            if (RobotImageLoader._objnewincrement != null && RobotImageLoader._objnewincrement.Count > 0 && (RobotImageLoader._objnewincrement.Count == 0 || maxPhotoId == RobotImageLoader._objnewincrement[0].DisplayOrder))
            {
                RobotImageLoader.currentCount = 0;
                MessageBox.Show("No more records found!", "DigiPhoto i-Mix", MessageBoxButton.OK, MessageBoxImage.Information);
                LoadImageGroupPrev();
                return;
            }
            RobotImageLoader.IsNextPage = true;
            if (RobotImageLoader.ViewGroupedImagesCount != null && RobotImageLoader.ViewGroupedImagesCount.Count > 0)
            {
                RobotImageLoader.ViewGroupedImagesCount.Clear();
            }
            //RobotImageLoader.ViewGroupedImagesCount.Clear();
            RobotImageLoader.thumbSet = scrollIndexWithoutPreview;

            //int multiplier = 2;
            //if (lstImages.Items.Count < scrollIndexWithoutPreview)
            //    RobotImageLoader.currentCount = RobotImageLoader.currentCount - (scrollIndexWithoutPreview + lstImages.Items.Count);
            //else if (RobotImageLoader.currentCount != 0)
            //    RobotImageLoader.currentCount = RobotImageLoader.currentCount - (scrollIndexWithoutPreview * multiplier);



            flgLoadNext = true;
            LoadNext();
            FillImageList();
            CheckSelectAllGroup();
        }
        private void NextRec()
        {
            if (RobotImageLoader._objnewincrement != null && RobotImageLoader._objnewincrement.Count > 0 && RobotImageLoader.MinPhotoId == RobotImageLoader._objnewincrement[RobotImageLoader._objnewincrement.Count - 1].DisplayOrder)
            {
                RobotImageLoader.currentCount = 0;
                MessageBox.Show("No more records found!", "DigiPhoto i-Mix", MessageBoxButton.OK, MessageBoxImage.Information);
                LoadImageGroupNext();
                return;
            }

            if (RobotImageLoader.ViewGroupedImagesCount != null)
                RobotImageLoader.ViewGroupedImagesCount.Clear();
            int recCount = 0;

            recCount = scrollIndexWithoutPreview;
            RobotImageLoader.IsNextPage = false;
            RobotImageLoader.thumbSet = recCount;

            flgLoadNext = true;
            LoadNext();
            FillImageList();
            CheckSelectAllGroup();
        }

        private void btnScrollDown_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if ((RobotImageLoader.SearchCriteria == "TimeWithQrcode"))
                {
                    //searchDetails.PageNumber = searchDetails.PageNumber - 1;
                    if (RobotImageLoader.robotImages != null && RobotImageLoader.robotImages.Count > 0)
                    {
                        searchDetails.StartIndex = RobotImageLoader.robotImages.Max(o => o.PhotoId);
                        searchDetails.NewRecord = 0;
                        MovePage(searchDetails);
                    }
                }
                else if ((RobotImageLoader.SearchCriteria == "PhotoId") && RobotImageLoader.RFID != "0")
                {
                    //searchDetails.PageNumber = searchDetails.PageNumber - 1;
                    if (RobotImageLoader.robotImages != null && RobotImageLoader.robotImages.Count > 0)
                    {
                        RobotImageLoader.StartIndexRFID = RobotImageLoader.robotImages.Max(o => o.PhotoId);
                        RobotImageLoader.NewRecord = 0;
                        MovePagePhotoId(searchDetails);
                    }
                }
                else
                {
                    PrevRec();//reversed
                }
            }
            finally
            {
                MemoryManagement.FlushMemory();
            }
        }
        private void GetMktImgInfo()
        {
            try
            {
                //if (objdbLayer == null)
                //    objdbLayer = new DigiPhotoDataServices();
                ConfigBusiness configBiz = new ConfigBusiness();
                List<long> objList = new List<long>();
                objList.Add((long)ConfigParams.MktImgPath);
                objList.Add((long)ConfigParams.MktImgTimeInSec);
                objList.Add((long)ConfigParams.IsEnabledVideoEdit);
                objList.Add((long)ConfigParams.IsEnableSingleScreenPreview);
                objList.Add((long)ConfigParams.SingleScreenPreviewAngle);
                objList.Add((long)ConfigParams.IsClientViewWatermark);
                List<iMIXConfigurationInfo> ConfigValuesList = configBiz.GetNewConfigValues(LoginUser.SubStoreId).Where(o => objList.Contains(o.IMIXConfigurationMasterId)).ToList();

                //if (ConfigValuesList != null || ConfigValuesList.Count > 0)
                if (ConfigValuesList != null && ConfigValuesList.Count > 0)
                {
                    for (int i = 0; i < ConfigValuesList.Count; i++)
                    {
                        switch (ConfigValuesList[i].IMIXConfigurationMasterId)
                        {
                            case (int)ConfigParams.MktImgPath:
                                MktImgPath = ConfigValuesList[i].ConfigurationValue;
                                break;
                            case (int)ConfigParams.MktImgTimeInSec:
                                mktImgTime = (ConfigValuesList[i].ConfigurationValue != null ? ConfigValuesList[i].ConfigurationValue.ToInt32() : 10) * 1000;
                                break;
                            case (int)ConfigParams.IsEnableSingleScreenPreview:
                                isSingleScreenPreview = ConfigValuesList[i].ConfigurationValue != null ? ConfigValuesList[i].ConfigurationValue.ToBoolean() : false;
                                break;
                            case (int)ConfigParams.IsEnabledVideoEdit:
                                bool isTrue = ConfigValuesList[i].ConfigurationValue != null ? Convert.ToBoolean(ConfigValuesList[i].ConfigurationValue) : false;
                                stkEditVideo.Visibility = isTrue ? Visibility.Visible : Visibility.Collapsed;
                                stkAdvVideoEdit.Visibility = isTrue ? Visibility.Visible : Visibility.Collapsed;
                                stackVideoSearchTypes.Visibility = isTrue ? Visibility.Visible : Visibility.Collapsed;
                                stkVideoOverlay.Visibility = isTrue ? Visibility.Visible : Visibility.Collapsed;
                                break;
                            #region added by ajay on 23dec20 for cliew view watermark
                            case (int)ConfigParams.IsClientViewWatermark:
                                bool IsClientViewWatermark = ConfigValuesList[i].ConfigurationValue != null ? Convert.ToBoolean(ConfigValuesList[i].ConfigurationValue) : false;
                                ExampleStackPanel.Visibility = IsClientViewWatermark ? Visibility.Visible : Visibility.Collapsed;
                                #region Added by AJay on 12Feb20 Client view watermark.
                                if (ExampleStackPanel.Visibility != Visibility.Collapsed)
                                {
                                    int SubStoreId = LoginUser.SubStoreId;
                                    GetClientWatermarkConfig(SubStoreId);
                                }
                                #endregion
                                break;
                            #endregion
                            case (int)ConfigParams.SingleScreenPreviewAngle:
                                AngleValue = ConfigValuesList[i].ConfigurationValue != null ? Convert.ToInt32(ConfigValuesList[i].ConfigurationValue) : 0;
                                break;

                        }
                    }
                }

                if (isSingleScreenPreview)
                {
                    txtpreview.Text = "Preview & Flip";
                    grdThumbPreview.Visibility = Visibility.Visible;
                    txtflippreview.Visibility = Visibility.Visible; /// Commented Part
                }
                else txtpreview.Text = "Preview Photo";

            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }
        private void PastingHandler(object sender, DataObjectPastingEventArgs e)
        {
            if (e.DataObject.GetDataPresent(typeof(String)))
            {
                String text = (String)e.DataObject.GetData(typeof(String));
                if (!IsTextAllowed(text)) e.CancelCommand();
            }
            else e.CancelCommand();
        }
        private static bool IsTextAllowed(string text)
        {
            Regex regex = new Regex("[^0-9.-]+"); //regex that matches disallowed text
            return !regex.IsMatch(text);
        }
        private void bttnLogin_Enter(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
                SearchImages();
        }
        public void SearchImages()
        {
            int _rfidSearch = RobotImageLoader._rfidSearch;
            RobotImageLoader._rfidSearch = 0;
            long StartIndexRFID = RobotImageLoader.StartIndexRFID;
            RobotImageLoader.StartIndexRFID = 0;
            if (txtImageId.Text.Trim() == "")
                txtImageId.Text = "0";


            if (txtImageId.Text.Trim() == "")
                txtImageId.Text = "0";
            SetMediaType();
            if (!string.IsNullOrEmpty(txtImageId.Text.Trim()))
            {
                string RFID = RobotImageLoader.RFID;
                RobotImageLoader.RFID = txtImageId.Text;
                string SearchCriteria = RobotImageLoader.SearchCriteria;
                RobotImageLoader.SearchCriteria = "PhotoId";
                pagename = "";
                if (txtImageId.Text != "0")
                {
                    string PhotoId = RobotImageLoader.PhotoId;
                    RobotImageLoader.PhotoId = txtImageId.Text;
                    PhotoBusiness phototBiz = new PhotoBusiness();
                    List<PhotoInfo> lstAllLatest = phototBiz.GetAllPhotosforSearch(LoginUser.DefaultSubstores, txtImageId.Text.Trim().ToInt64(), LoginUser.PageCountGrid, LoginUser.IsPhotographerSerailSearchActive
                        , RobotImageLoader.StartIndexRFID, RobotImageLoader._rfidSearch, NewReord, out MaxPhotoIdCriteria, out MinPhotoIdCriteria, RobotImageLoader.MediaTypes
                        ).OrderByDescending(t => t.DisplayOrder).ToList(); // Suraj..
                    List<PhotoInfo> tempSearchItem = new List<PhotoInfo>();
                    if (LoginUser.IsPhotographerSerailSearchActive == false)
                    {
                        if (RobotImageLoader._rfidSearch == 0)
                            tempSearchItem = lstAllLatest.Where(T => T.DG_Photos_RFID == txtImageId.Text).ToList();
                        else
                            tempSearchItem = lstAllLatest.ToList();
                    }
                    else
                    {
                        tempSearchItem = lstAllLatest.ToList();
                    }
                    if (tempSearchItem.Count == 0)
                    {
                        string msg = "Unable to find the Item with ID '" + txtImageId.Text + "'.\nDo you wish to see the images that were found?";
                        bool res = MsgBox.ShowHandlerDialog(msg, DigiMessageBox.DialogType.Confirm);
                        if (res == false)
                        {
                            RobotImageLoader._rfidSearch = _rfidSearch;
                            RobotImageLoader.StartIndexRFID = StartIndexRFID;
                            RobotImageLoader.RFID = RFID;
                            RobotImageLoader.SearchCriteria = SearchCriteria;
                            RobotImageLoader.PhotoId = PhotoId;
                            txtImageId.Text = "";
                            return;
                        }

                        txtImageId.Text = "0";
                        RobotImageLoader.RFID = txtImageId.Text;
                        RobotImageLoader.IsZeroSearchNeeded = true;
                        RobotImageLoader.currentCount = 0;
                    }
                    if (RobotImageLoader._objnewincrement != null)
                    {
                        RobotImageLoader._objnewincrement.Clear();
                    }
                }
                else
                {
                    RobotImageLoader.IsZeroSearchNeeded = true;
                    RobotImageLoader.currentCount = 0;
                    grdSelectAll.Visibility = Visibility.Visible;
                    SPSelectAll.Visibility = Visibility.Visible;
                }
                LoadWindow();
                txtImageId.Clear();
            }
        }
        /// <summary>
        /// BBy KCB ON 31 AUG 2019 for enable face scan
        /// </summary>
        /// <param name="PhotoIds">List of photo id</param>
        public void SearchImages(List<string> PhotoIds)
        {
            lstImages.Items.Clear();
            int _rfidSearch = RobotImageLoader._rfidSearch;
            RobotImageLoader._rfidSearch = 0;
            long StartIndexRFID = RobotImageLoader.StartIndexRFID;
            RobotImageLoader.StartIndexRFID = 0;
            if (txtImageId.Text.Trim() == "")
                txtImageId.Text = "0";


            if (txtImageId.Text.Trim() == "")
                txtImageId.Text = "0";
            SetMediaType();
            if (!string.IsNullOrEmpty(txtImageId.Text.Trim()))
            {
                string RFID = RobotImageLoader.RFID;
                RobotImageLoader.RFID = txtImageId.Text;
                string SearchCriteria = RobotImageLoader.SearchCriteria;
                RobotImageLoader.SearchCriteria = "FaceScan";
                pagename = "";
                //if (txtImageId.Text != "0")
                //{
                string PhotoId = RobotImageLoader.PhotoId;
                RobotImageLoader.PhotoId = txtImageId.Text;
                PhotoBusiness phototBiz = new PhotoBusiness();
                List<PhotoInfo> lstAllLatest;
                List<PhotoInfo> tempSearchItem = new List<PhotoInfo>();
                ErrorHandler.ErrorHandler.LogFileWrite("FR | " + DateTime.Now.ToString() + " : Fetching photo metadata from database");

                //lstAllLatest = new List<PhotoInfo>();

                if (PhotoIds.Count > 0 && PhotoIds[0] != "0")
                {
                    lstAllLatest = phototBiz.GetPhotoDetailsByPhotoIds(PhotoIds).ToList();
                    //lstAllLatest = phototBiz.GetPhotoDetailsByPhotoIds(PhotoIds).OrderByDescending(t => t.DG_Photos_pkey).ToList();


                }
                else
                {
                    lstAllLatest = new List<PhotoInfo>();
                }

                ErrorHandler.ErrorHandler.LogFileWrite("FR | " + DateTime.Now.ToString() + " :Completed Fetching photo metadata from database");
                //if (LoginUser.IsPhotographerSerailSearchActive == false)
                //{
                //    tempSearchItem = lstAllLatest.ToList();
                //}
                //else
                //{
                //    tempSearchItem = lstAllLatest.ToList();
                //}
                //tempSearchItem = lstAllLatest;
                if (lstAllLatest.Count == 0)
                {
                    //string msg = "Unable to find the Item with ID '" + txtImageId.Text + "'.\nDo you wish to see the images that were found?";
                    string msg = "Unable to find the exact match, showing the default images";
                    bool res = MsgBox.ShowHandlerDialog(msg, DigiMessageBox.DialogType.OK);
                    if (res == false)
                    {
                        RobotImageLoader._rfidSearch = _rfidSearch;
                        RobotImageLoader.StartIndexRFID = StartIndexRFID;
                        RobotImageLoader.RFID = RFID;
                        RobotImageLoader.SearchCriteria = SearchCriteria;
                        RobotImageLoader.PhotoId = PhotoId;
                        txtImageId.Text = "";
                        tempSearchItem = new List<PhotoInfo>();
                        return;
                    }
                    else
                    {

                        txtImageId.Text = "0";
                        RobotImageLoader.RFID = txtImageId.Text;
                        RobotImageLoader.IsZeroSearchNeeded = true;
                        RobotImageLoader.currentCount = 0;
                        RobotImageLoader.SearchCriteria = "PhotoId";
                        lstAllLatest = phototBiz.GetAllPhotosforSearch(LoginUser.DefaultSubstores, txtImageId.Text.Trim().ToInt64(), LoginUser.PageCountGrid, LoginUser.IsPhotographerSerailSearchActive
                          , RobotImageLoader.StartIndexRFID, RobotImageLoader._rfidSearch, NewReord, out MaxPhotoIdCriteria, out MinPhotoIdCriteria, RobotImageLoader.MediaTypes
                          ).OrderByDescending(t => t.DG_Photos_pkey).ToList();
                    }
                }
                if (RobotImageLoader._objnewincrement != null)
                {
                    RobotImageLoader._objnewincrement.Clear();
                }
                //}
                //else
                //{
                //    RobotImageLoader.IsZeroSearchNeeded = true;
                //    RobotImageLoader.currentCount = 0;
                //    grdSelectAll.Visibility = Visibility.Visible;
                //    SPSelectAll.Visibility = Visibility.Visible;
                //}

                tempSearchItem = lstAllLatest;
                //int ind = 0;
                //foreach (string pid in PhotoIds)
                //{
                //    ErrorHandler.ErrorHandler.LogFileWrite("pid :" + pid);
                //    PhotoInfo photoInfo = lstAllLatest.FirstOrDefault(p => Convert.ToString(p.DG_Photos_pkey) == pid);
                //    if (photoInfo != null)
                //    {

                //        tempSearchItem.Insert(ind,photoInfo);
                //    }
                //}
                RobotImageLoader._rfidSearch = lstAllLatest.Count();
                ErrorHandler.ErrorHandler.LogFileWrite("FR | " + DateTime.Now.ToString() + " : Loading Images");
                LoadWindow();
                ErrorHandler.ErrorHandler.LogFileWrite("FR | " + DateTime.Now.ToString() + " : Completed Loading Images");
                txtImageId.Clear();
            }
        }
        private void SetMediaType()
        {
            #region Search Criteria
            if (rdbtnPhoto.IsChecked == true)
                RobotImageLoader.MediaTypes = 1;
            else if (rdbtnVideo.IsChecked == true)
                RobotImageLoader.MediaTypes = 2;
            else
                RobotImageLoader.MediaTypes = 0;
            #endregion
        }
        private void SetMessageText(string caseValue)
        {
            switch (caseValue)
            {
                case "Grouped":
                    txtSelectImages.Visibility = Visibility.Collapsed;
                    txtSelectedImages.Visibility = Visibility.Visible;
                    txtSelectedImages.Text = "Grouped : " + RobotImageLoader.GroupImages.Count;
                    break;
                case "PrintGrouped":
                    txtSelectImages.Visibility = Visibility.Visible;
                    txtSelectedImages.Visibility = Visibility.Collapsed;

                    var count = (from groupImages in RobotImageLoader.GroupImages
                                 join printImages in RobotImageLoader.PrintImages
                                 on groupImages.PhotoId equals printImages.PhotoId
                                 select groupImages).Count();

                    txtSelectImages.Text = "Selected : " + count.ToString() + "/" + RobotImageLoader.GroupImages.Count.ToString();
                    break;
                case "EditPrintGrouped":
                    txtSelectImages.Visibility = Visibility.Visible;
                    txtSelectedImages.Visibility = Visibility.Collapsed;
                    txtSelectImages.Text = "Print Grouped : " + lstImages.Items.Count.ToString();
                    break;
                case "EditPrintGroupedView":
                    txtSelectImages.Visibility = Visibility.Visible;
                    txtSelectedImages.Visibility = Visibility.Collapsed;
                    var s = (from p in RobotImageLoader.PrintImages join g in RobotImageLoader.GroupImages on p.PhotoId equals g.PhotoId select p.PhotoId).Count();
                    txtSelectImages.Text = "Print Grouped : " + s.ToString();
                    break;
            }
        }
        private void btnSearchImage_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                //var item= RobotImageLoader.GroupImages;
                // if (item.Count!=0)
                // {
                //     RobotImageLoader.GroupImages.Clear();
                // }
                Ok.IsEnabled = false;
                MediaStop();
                SearchImages();
                Ok.IsEnabled = true;
            }
            catch (Exception en)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(en);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }
        private void OpenAssociateWindow()
        {
            AddUserControlAssociateImage();
            ModalDialogParent.IsEnabled = false;
            uctlAssociateImage.Visibility = Visibility.Visible;
            uctlAssociateImage.txtQRCode.Focus();
            FocusManager.SetFocusedElement(this, uctlAssociateImage.txtQRCode);
            Keyboard.Focus(uctlAssociateImage.txtQRCode);
        }
        private void btnQRSearch_Click(object sender, RoutedEventArgs e)
        {
            vwGroup.Text = "View Group";
            OpenAssociateWindow();
        }
        private void LoadPendingBurnOrders()
        {
            lblPendingOrders.Text = GetPendingBurnOrders();
        }
        private void btnPendingOrders_Click(object sender, RoutedEventArgs e)
        {
            InitiateBurnOrders();
        }
        private void InitiateBurnOrders()
        {
            if (BurnOrderElements.isExecuting == false)
            {
                closeExistingBurnOrderWindows();
                Orders.BurnMediaOrderList bm = new Orders.BurnMediaOrderList();
                string FurtherMessage = bm.FetchNextOrderDetails();
                if (FurtherMessage == "\r\nNo active orders found!")
                {
                    MessageBox.Show(FurtherMessage, "iMIX", MessageBoxButton.OK, MessageBoxImage.Information);
                }
                else
                {
                    MessageBoxResult messageBoxResult = System.Windows.MessageBox.Show(FurtherMessage, "iMIX", System.Windows.MessageBoxButton.YesNo, MessageBoxImage.Question);
                    if (messageBoxResult == MessageBoxResult.Yes)
                    {
                        bm.Width = 0;
                        bm.Height = 0;
                        bm.WindowState = System.Windows.WindowState.Minimized;
                        //bm.LoadBurnOrders();
                        bm.Show();
                        bm.Hide();
                        bm._isAutoStart = true;
                        bm.AutoExecuteOrders();
                    }
                }
            }
            else
            {
                MessageBox.Show("Orders are already being processed!", "iMIX", MessageBoxButton.OK, MessageBoxImage.Stop);
            }
        }

        private string GetPendingBurnOrders()
        {
            int ProdType;
            int USBcount = 0; int CDcount = 0;
            //if (objdbLayer == null)
            //    objdbLayer = new DigiPhotoDataServices();
            OrderBusiness orderBiz = new OrderBusiness();

            var ListOfItems = orderBiz.GetPendingBurnOrders(true);
            if (ListOfItems != null)
            {
                foreach (var IndividualItem in ListOfItems)
                {
                    if (IndividualItem.Status != 1)
                    {
                        if (isLocalOrder(IndividualItem.OrderNumber, IndividualItem.ProductType))
                        {
                            ProdType = IndividualItem.ProductType;
                            if (ProdType == 36 || ProdType == 97)
                                USBcount++;
                            else
                                CDcount++;
                        }
                    }
                }
            }
            return "USB(" + USBcount.ToString() + ") CD(" + CDcount.ToString() + ")";
        }
        private bool isLocalOrder(string OrderNumber, int ProdType)
        {
            string OrdPath = "";
            if (ProdType == 35)
                OrdPath = Environment.CurrentDirectory + "\\DigiOrderdImages\\CDOrders\\" + OrderNumber + "\\CD\\";
            else if (ProdType == 36)
                OrdPath = Environment.CurrentDirectory + "\\DigiOrderdImages\\USBOrders\\" + OrderNumber + "\\USB\\";
            else if (ProdType == 96)
            {
                OrdPath = Environment.CurrentDirectory + "\\DigiOrderdImages\\CDOrders\\" + OrderNumber + "\\VideoCD\\";
            }
            else if (ProdType == 97)
            {
                OrdPath = Environment.CurrentDirectory + "\\DigiOrderdImages\\USBOrders\\" + OrderNumber + "\\VideoUSB\\";
            }
            if (Directory.Exists(OrdPath))
                return true;
            else
                return false;
        }
        private void closeExistingBurnOrderWindows()
        {
            foreach (Window wnd in Application.Current.Windows)
            {
                if (wnd.Title == "BurnMedia")
                {
                    wnd.Close();
                }
            }
        }
        private void txtImageId_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (txtImageId.Text.Contains('.'))
            {
                txtImageId.Text = txtImageId.Text.Replace(".", "");
            }
            if (txtImageId.Text.Contains('-'))
            {
                txtImageId.Text = txtImageId.Text.Replace("-", "");
            }
            txtImageId.CaretIndex = txtImageId.Text.Length;
        }
        #region MediaPlayer
        void MediaStop()
        {
            if (mplayer != null)
            {
                mplayer.MediaStop();
                //  mplayer.UnloadMediaPlayer();
                mplayer = null;
            }
            gdMediaPlayer.Children.Clear();
            // gdMediaPlayer.Children.Remove(mplayer);
            if (clientWin != null)
            {
                clientWin.StopMediaPlay();
            }
        }
        void MediaPlay()
        {

            if (mplayer != null)
            {
                MediaStop();
                //  mplayer.Dispose();
            }
            //gdMediaPlayer.Dispatcher.BeginInvoke(new Action(
            //    () =>
            //    {
            mplayer = new MLMediaPlayer(vsMediaFileName, "Search", true);
            gdMediaPlayer.BeginInit();
            //   gdMediaPlayer.Children.Clear();
            gdMediaPlayer.Children.Add(mplayer);
            gdMediaPlayer.EndInit();
            //}));

            // clientWin.PlayVideoOnClient(vsMediaFileName,"");

        }

        #endregion

        private void btnEditVideo_Click(object sender, RoutedEventArgs e)
        {
            //VideoEditor ve = new VideoEditor();
            //ve.Show();
            if (vwGroup.Text == "View All")
            {
                RobotImageLoader.isGroup = true;
            }
            else
            {
                RobotImageLoader.isGroup = false;
            }

            vsMediaFileName = string.Empty;
            try
            {
                bool isvideoEditorHide = false;
                VideoEditor objVideoEditor;
                Window ve = null;
                foreach (Window wnd in Application.Current.Windows)
                {
                    if (wnd.Title == "VideoEditor")
                    {
                        ve = wnd as VideoEditor;
                        isvideoEditorHide = true;
                    }
                }

                if ((lstImages.SelectedItem != null && RobotImageLoader.PrintImages.Count > 0) || isvideoEditorHide)
                {
                    MediaStop();
                    if (ve != null)
                    {
                        objVideoEditor = (VideoEditor)ve;
                    }
                    else
                    {
                        objVideoEditor = new VideoEditor();
                    }
                    //Added towards clearing the group items which are not to be printed
                    if (vwGroup.Text == "View All" || pagename == "Saveback")
                    {

                        itemsNotPrinted = new List<LstMyItems>();
                        foreach (LstMyItems itx in RobotImageLoader.GroupImages)
                        {
                            var teItem = RobotImageLoader.PrintImages.Where(xs => xs.PhotoId == itx.PhotoId).FirstOrDefault();
                            if (teItem == null)
                            {
                                itemsNotPrinted.Add(itx);

                            }
                        }
                    }

                    if (pagename == "Saveback")
                        objVideoEditor.IsGoupped = "View All";
                    else
                        objVideoEditor.IsGoupped = vwGroup.Text;

                    //if (LoadFirstTimeOnly)
                    FrameworkHelper.Common.ContantValueForMainWindow.RedEyeSize = .0105;
                    objVideoEditor.WindowState = System.Windows.WindowState.Maximized;
                    objVideoEditor.WindowStyle = System.Windows.WindowStyle.None;
                    objVideoEditor.Show();
                    this.Hide();
                }
                else
                {
                    MessageBox.Show("No images or videos selected!", "DigiPhoto", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                }
                // this.Hide()
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
            finally
            {
                // MemoryManagement.FlushMemory();
            }
        }
        private void btnWithPreviewActive_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            e.Handled = true;
        }
        private void MovePage(SearchDetailInfo searchDetails)
        {
            if (RobotImageLoader.robotImages != null && RobotImageLoader.Is16ImgViewActive == true && RobotImageLoader.Is9ImgViewActive == false)
            {
                ImageSize(16);
            }
            else
            {
                ImageSize(9);
            }
            if (RobotImageLoader.robotImages != null && RobotImageLoader.robotImages.Count > 0)
            {

                //if (RobotImageLoader.robotImages.Max(o => o.PhotoId) == searchDetails.MaxPhotoId || RobotImageLoader.robotImages.Min(o => o.PhotoId) == searchDetails.MinPhotoId)
                if (searchDetails.NewRecord == 0)
                {

                    var lstMaxSubStore = RobotImageLoader.robotImages.FirstOrDefault(o => o.PhotoId == RobotImageLoader.MaxPhotoIdCriteria);

                    // if (RobotImageLoader.robotImages[RobotImageLoader.robotImages.Count - 1].PhotoId == RobotImageLoader.MaxPhotoIdCriteria)//|| RobotImageLoader.robotImages.Min(o => o.PhotoId) == 481)
                    if (lstMaxSubStore != null)
                    {
                        searchDetails.NewRecord = 1;
                        searchDetails.StartIndex = RobotImageLoader.MaxPhotoIdCriteria + 1;
                        MessageBox.Show("No more records found!", "DigiPhoto i-Mix", MessageBoxButton.OK, MessageBoxImage.Information);
                        LoadImageCriteria();
                        return;
                    }
                }
                else
                {
                    var lstMinSubStore = RobotImageLoader.robotImages.FirstOrDefault(o => o.PhotoId == RobotImageLoader.MinPhotoIdCriteria);
                    //if (RobotImageLoader.robotImages[0].PhotoId == RobotImageLoader.MinPhotoIdCriteria)//|| RobotImageLoader.robotImages.Min(o => o.PhotoId) == 481)
                    if (lstMinSubStore != null)
                    {
                        searchDetails.NewRecord = 0;
                        searchDetails.StartIndex = RobotImageLoader.MinPhotoIdCriteria - 1;
                        MessageBox.Show("No more records found!", "DigiPhoto i-Mix", MessageBoxButton.OK, MessageBoxImage.Information);
                        LoadImageCriteria();
                        return;
                    }
                }
            }

            //FillImageList();
            //CheckSelectAllGroup();
            //RobotImageLoader.ViewGroupedImagesCount.Clear();
            RobotImageLoader.LoadImages(searchDetails);
            LoadImages();
            FillImageList();
            CheckSelectAllGroup();
            if (RobotImageLoader.ViewGroupedImagesCount != null && RobotImageLoader.ViewGroupedImagesCount.Count > 0)
            {
                RobotImageLoader.ViewGroupedImagesCount.Clear();
            }
            ///RobotImageLoader.ViewGroupedImagesCount.Clear();
            //TogglePageButton(searchDetails);
        }
        private void LoadImageCriteria()
        {
            if (lstImages.Items.Count == 0)
            {
                FillImageList();
                CheckSelectAllGroup();
                if (RobotImageLoader.ViewGroupedImagesCount != null && RobotImageLoader.ViewGroupedImagesCount.Count > 0)
                {
                    RobotImageLoader.ViewGroupedImagesCount.Clear();
                }
                ///RobotImageLoader.ViewGroupedImagesCount.Clear();
                RobotImageLoader.LoadImages(searchDetails);
                LoadImages();
            }
        }
        private void LoadImagePhotoId()
        {
            if (lstImages.Items.Count == 0)
            {
                FillImageList();
                CheckSelectAllGroup();
                if (RobotImageLoader.ViewGroupedImagesCount != null && RobotImageLoader.ViewGroupedImagesCount.Count > 0)
                {
                    RobotImageLoader.ViewGroupedImagesCount.Clear();
                }
                ///RobotImageLoader.ViewGroupedImagesCount.Clear();
                RobotImageLoader.LoadImages(searchDetails);
                LoadImages();
            }
        }
        //private void Move(SearchDetailInfo searchDetails)
        //{
        //    FillImageList();
        //    CheckSelectAllGroup();
        //    RobotImageLoader.ViewGroupedImagesCount.Clear();
        //    RobotImageLoader.LoadImages(searchDetails);
        //    LoadImages();
        //    TogglePageButton(searchDetails);
        //}
        //void TogglePageButton(SearchDetailInfo searchDetails)
        //{
        //    //btnprev.IsEnabled = searchDetails.PageNumber < (searchDetails.TotalPage);
        //    //btnnext.IsEnabled = searchDetails.PageNumber > 1;
        //}
        private void txtAmountEntered_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = !IsTextAllowed(e.Text);
        }

        #endregion

        private void btnCaptureInfo_Click(object sender, RoutedEventArgs e)
        {
            if (lstImages.Items != null && lstImages.Items.Count > 0)
            {

                LstMyItems myItems = ((DigiPhoto.LstMyItems)(lstImages.SelectedItem));
                if (myItems != null)
                {
                    if (myItems.MediaType != 1)
                    {
                        imageInfo.IsVideo = true;
                        gdMediaPlayer.Visibility = Visibility.Collapsed;
                    }
                    else
                        imageInfo.IsVideo = false;

                    RobotImageLoader.curItem = myItems;
                    var currentImageId = myItems.PhotoId;
                    List<PhotoCaptureInfo> captureDetails = new List<PhotoCaptureInfo>();
                    captureDetails = (new PhotoBusiness()).GetphotoCapturedetails(currentImageId);
                    if (captureDetails != null && captureDetails.Count > 0)
                        imageInfo.ShowHandlerDialog(captureDetails.FirstOrDefault());
                    lstImages.SelectedItem = myItems;
                    lstImages.ScrollIntoView(lstImages.SelectedItem);
                    ((ListBoxItem)lstImages.ItemContainerGenerator.ContainerFromItem(lstImages.SelectedItem)).Focus();
                }

            }
        }


        #region  search implemented for video
        int searchType = 3;
        SearchTypeConfig searchConfig = new SearchTypeConfig();

        private void rdbtPVBoth_Checked(object sender, RoutedEventArgs e)
        {

            searchConfig = SearchTypeConfig.ImageVideo;
            RobotImageLoader.MediaTypes = 3;
            if (vwGroup.Text == "View All")
            {
                if (RobotImageLoader.robotImages != null && RobotImageLoader.robotImages.Count > 0)
                {
                    RobotImageLoader.robotImages.Clear();
                }

                ///RobotImageLoader.robotImages.Clear();
                foreach (var item in RobotImageLoader.GroupImages)
                {
                    var existItem = RobotImageLoader.PrintImages.Where(t => t.PhotoId == item.PhotoId).FirstOrDefault();
                    if (existItem != null)
                        item.PrintGroup = new BitmapImage(new Uri(@"/images/print-accept.png", UriKind.Relative));
                    else
                        item.PrintGroup = new BitmapImage(new Uri(@"/images/print-group.png", UriKind.Relative));
                    RobotImageLoader.robotImages.Add(item);
                }
                LoadImages();
                FillImageList();
                //CheckSelectAllGroup();
                CheckForAllImgSelectToPrint();
                SetMessageText("PrintGrouped");

            }
            else
            {
                RobotImageLoader.MinPhotoIdCriteria = -1;
                RobotImageLoader.IsZeroSearchNeeded = true;
                RobotImageLoader.currentCount = 0;
                RobotImageLoader.LoadImages();
                LoadImages();
                FillImageList();
                CheckSelectAllGroup();
            }


        }

        private void rdbtnVideo_Checked(object sender, RoutedEventArgs e)
        {
            searchConfig = SearchTypeConfig.Video;
            RobotImageLoader.MediaTypes = 2;
            if (vwGroup.Text == "View All")
            {
                if (RobotImageLoader.robotImages != null && RobotImageLoader.robotImages.Count > 0)
                {
                    RobotImageLoader.robotImages.Clear();
                }
                ///RobotImageLoader.robotImages.Clear();
                foreach (var item in RobotImageLoader.GroupImages)
                {
                    var existItem = RobotImageLoader.PrintImages.Where(t => t.PhotoId == item.PhotoId).FirstOrDefault();
                    if (existItem != null)
                        item.PrintGroup = new BitmapImage(new Uri(@"/images/print-accept.png", UriKind.Relative));
                    else
                        item.PrintGroup = new BitmapImage(new Uri(@"/images/print-group.png", UriKind.Relative));
                    if (item.MediaType == 2 || item.MediaType == 3)
                        RobotImageLoader.robotImages.Add(item);
                }
                LoadImages();
                FillImageList();
                //CheckSelectAllGroup();
                CheckForAllImgSelectToPrint();
                SetMessageText("PrintGrouped");
            }
            else
            {
                RobotImageLoader.MinPhotoIdCriteria = -1;
                RobotImageLoader.IsZeroSearchNeeded = true;
                RobotImageLoader.currentCount = 0;
                RobotImageLoader.LoadImages();
                LoadImages();
                FillImageList();
                CheckSelectAllGroup();
            }

        }

        private void rdbtnPhoto_checked(object sender, RoutedEventArgs e)
        {
            searchConfig = SearchTypeConfig.Image;
            RobotImageLoader.MediaTypes = 1;

            if (vwGroup.Text == "View All")
            {
                if (RobotImageLoader.robotImages != null && RobotImageLoader.robotImages.Count > 0)
                {
                    RobotImageLoader.robotImages.Clear();
                }

                ///RobotImageLoader.robotImages.Clear();
                foreach (var item in RobotImageLoader.GroupImages)
                {
                    var existItem = RobotImageLoader.PrintImages.Where(t => t.PhotoId == item.PhotoId).FirstOrDefault();
                    if (existItem != null)
                        item.PrintGroup = new BitmapImage(new Uri(@"/images/print-accept.png", UriKind.Relative));
                    else
                        item.PrintGroup = new BitmapImage(new Uri(@"/images/print-group.png", UriKind.Relative));
                    if (item.MediaType == RobotImageLoader.MediaTypes)
                        RobotImageLoader.robotImages.Add(item);
                }
                LoadImages();
                FillImageList();
                //CheckSelectAllGroup();
                CheckForAllImgSelectToPrint();
                SetMessageText("PrintGrouped");
            }
            else
            {
                RobotImageLoader.MinPhotoIdCriteria = -1;
                RobotImageLoader.IsZeroSearchNeeded = true;
                RobotImageLoader.currentCount = 0;
                RobotImageLoader.LoadImages();
                LoadImages();
                FillImageList();
                CheckSelectAllGroup();
            }

        }

        public enum SearchTypeConfig
        {
            Image = 1,
            Video = 2,
            ImageVideo = 3
        }

        private List<string> imageList = new List<string>() { "EPS", "TIFF", "JPEG", "GIF", "PNG", "BMP", "JPG" };
        private List<string> videoList = new List<string>() { "3G2", "3GP", "ASF", "ASX", "AVI", "FLV", "MOV", "MP4", "MPG", "RM", "SWF", "VOB", "WMV" };

        #endregion

        private void btnAdvanceVideoEditing_Click(object sender, RoutedEventArgs e)
        {
            //VideoEditor ve = new VideoEditor();
            //ve.Show();
            if (vwGroup.Text == "View All")
            {
                RobotImageLoader.isGroup = true;
            }
            else
            {
                RobotImageLoader.isGroup = false;
            }
            MediaStop();
            vsMediaFileName = string.Empty;
            try
            {
                bool isvideoEditorHide = false;
                MLLiveCapture objMLLiveCapture;
                Window ve = null;
                foreach (Window wnd in Application.Current.Windows)
                {
                    if (wnd.Title == "MLLiveCapture")
                    {
                        ve = wnd as MLLiveCapture;
                        isvideoEditorHide = true;
                    }
                }

                if ((lstImages.SelectedItem != null && RobotImageLoader.PrintImages.Count > 0) || isvideoEditorHide)
                {
                    if (ve != null)
                    {
                        objMLLiveCapture = (MLLiveCapture)ve;
                    }
                    else
                    {
                        objMLLiveCapture = new MLLiveCapture();
                    }
                    //Added towards clearing the group items which are not to be printed
                    if (vwGroup.Text == "View All" || pagename == "Saveback")
                    {

                        itemsNotPrinted = new List<LstMyItems>();
                        foreach (LstMyItems itx in RobotImageLoader.GroupImages)
                        {
                            var teItem = RobotImageLoader.PrintImages.Where(xs => xs.PhotoId == itx.PhotoId).FirstOrDefault();
                            if (teItem == null)
                            {
                                itemsNotPrinted.Add(itx);

                            }
                        }
                    }

                    //if (pagename == "Saveback")
                    //    objVideoEditor.IsGoupped = "View All";
                    //else
                    //    objVideoEditor.IsGoupped = vwGroup.Text;

                    //if (LoadFirstTimeOnly)
                    FrameworkHelper.Common.ContantValueForMainWindow.RedEyeSize = .0105;
                    objMLLiveCapture.WindowState = System.Windows.WindowState.Maximized;
                    objMLLiveCapture.WindowStyle = System.Windows.WindowStyle.None;
                    objMLLiveCapture.Show();
                    this.Hide();
                }
                else
                {
                    MessageBox.Show("No images or videos selected!", "DigiPhoto", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                }
                // this.Hide()
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
            finally
            {
                // MemoryManagement.FlushMemory();
            }

        }

        protected void ShowMediaPlayer(object sender, EventArgs e)
        {
            gdMediaPlayer.Visibility = Visibility.Visible;
            if (!string.IsNullOrEmpty(vsMediaFileName))
            {
                MediaPlay();
            }
            //gdMediaPlayer.UpdateLayout();
            //mplayer.winMFileSeeking1.UpdateLayout();
            //mplayer.MPreviewControl1.Update();//.Refresh();
            //mplayer.MFileSeeking1.Update();
            //mplayer.MFileSeeking1.Refresh();
            //mplayer.winMFileSeeking1.UpdateWindowPos();
            //.Refresh();
        }
        private void btnLockmage_Click(object sender, RoutedEventArgs e)
        {
            string photoId = string.Empty;
            try
            {
                List<string> deleteFileName = new List<string>();
                bool status = false;
                foreach (var item in RobotImageLoader.PrintImages)
                {

                    string fileName = item.Name + "@" + item.PhotoId;
                    deleteFileName.Add(fileName);
                }

                if (deleteFileName != null && deleteFileName.Count > 0)
                {
                    string[] rfidPhotoid;
                    string _photoId = string.Empty;
                    var allFiles = Directory.GetFiles(System.IO.Path.Combine(LoginUser.DigiFolderPath, "PreviewWall"), "*.jpg", SearchOption.AllDirectories).ToList();
                    //var photoToDelete = allFiles.Where(x => x.Contains(deleteFileName)).ToList();
                    foreach (string str in deleteFileName)
                    {
                        try
                        {
                            if (allFiles.Any(s => s.Contains(str)))
                            {
                                rfidPhotoid = str.Split('@');
                                _photoId = rfidPhotoid[0];
                                File.Delete(allFiles.FirstOrDefault(x => x.Contains(str)));
                                photoId = _photoId + ',' + photoId;
                                status = true;
                            }
                        }
                        catch (Exception ex)
                        {
                            ErrorHandler.ErrorHandler.LogError(ex);
                            string msg = "Image number " + _photoId + " is not locked for preview.";
                            MsgBox.ShowHandlerDialog(msg, DigiMessageBox.DialogType.OK);
                        }


                    }
                    if (status)
                    {
                        string msg = "Images with number " + photoId.Remove(photoId.Length - 1) + " has been locked successfully.";
                        MsgBox.ShowHandlerDialog(msg, DigiMessageBox.DialogType.OK);
                    }
                    else
                    {
                        string msg = "Images are already locked for preview.";
                        MsgBox.ShowHandlerDialog(msg, DigiMessageBox.DialogType.OK);
                    }
                    //ClearGroupforPrint();
                    btnRemoveGroup_Click(null, null);
                    // ClearGroup(null, null);
                }
                else
                {
                    string msg = "Please select image for preview lock.";
                    MsgBox.ShowHandlerDialog(msg, DigiMessageBox.DialogType.OK);
                }

            }
            catch (Exception ex)
            {

                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);


            }

        }

        private void btnExtractVideoFrame_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                // vsMediaFileName = string.Empty;

                AddVideoFrameExtractionPopup();
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
            finally
            {
                // MemoryManagement.FlushMemory();
            }
        }
        MLFrameExtractor mExtractor;
        void AddVideoFrameExtractionPopup()
        {
            try
            {
                string strePath = string.Empty;
                string subStoreName = string.Empty;
                int photographerId = 0;
                int locationId = 0;
                LstMyItems objLast = RobotImageLoader.PrintImages.LastOrDefault();
                if (objLast != null)
                {
                    PhotoCaptureInfo photoCaptureInfo = (new PhotoBusiness()).GetphotoCapturedetails(objLast.PhotoId).FirstOrDefault();
                    if (photoCaptureInfo != null)
                    {
                        photographerId = photoCaptureInfo.PhotoGrapherId;
                        subStoreName = photoCaptureInfo.SubstoreName;
                        locationId = photoCaptureInfo.LocationId;
                        string cropRatio = new PhotoBusiness().GetVideoFrameCropRatio(locationId);
                        cropRatio = string.IsNullOrEmpty(cropRatio) ? "None" : cropRatio;
                        if (objLast.MediaType == 1)
                        {
                            //string msg = "Please select a video to extract frames.";
                            //MsgBox.ShowHandlerDialog(msg, DigiMessageBox.DialogType.OK);
                            MessageBox.Show("Please select a video to extract frames.!", "DigiPhoto", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                            return;
                        }
                        else
                        {
                            MediaStop();
                            if (objLast.MediaType == 2)
                                strePath = System.IO.Path.Combine(objLast.HotFolderPath, "Videos", objLast.CreatedOn.ToString("yyyyMMdd"), objLast.FileName);
                            else if (objLast.MediaType == 3)
                                strePath = System.IO.Path.Combine(objLast.HotFolderPath, "ProcessedVideos", objLast.CreatedOn.ToString("yyyyMMdd"), objLast.FileName);
                            mExtractor = new DigiPhoto.MLFrameExtractor(strePath, "FrameExtractionPreview", true, objLast.MediaType, objLast.HotFolderPath, objLast.Name, subStoreName, photographerId, cropRatio);

                            gdMediaPlayer.Visibility = Visibility.Collapsed;
                            mExtractor.ExecuteParentMethod += new EventHandler(ShowMediaPlayer);
                            mExtractor.SetParent(ModalDialogParent);
                            grdCotrol.Children.Add(mExtractor);
                            mExtractor.Visibility = Visibility.Visible;
                            // gdMediaPlayer.Visibility = Visibility.Visible;
                        }
                    }
                }
                else
                {
                    MessageBox.Show("No videos selected!", "DigiPhoto", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }


        #region Added by Ajay

        private string GetOrderProductName(int PhotoId)
        {
            string ProductName = string.Empty;
            PhotoBusiness photoBiz = new PhotoBusiness();
            ProductName = photoBiz.GetOrderProductName(PhotoId);
            return ProductName;
        }
        WatermarkModel objWatermarkModel = new WatermarkModel();
        private void GetClientWatermarkConfig(int SubStoreId)
        {
            ConfigBusiness configBiz = new ConfigBusiness();

            ConfigBusiness conBiz = new ConfigBusiness();
            objWatermarkModel = configBiz.GetClientWatermarkConfig(SubStoreId);
            if (ExampleStackPanel.Visibility != Visibility.Collapsed)
            {
                DynamicGrid.Children.Clear();
                if (mainFrame.Height > mainFrame.Width)
                {
                    CreateDynamicGrid(objWatermarkModel, 1);
                }
                else
                    CreateDynamicGrid(objWatermarkModel, 2);

            }
        }
        /// <summary>
        /// Creating dynamic grid watermark as per the image vertical/horizontal
        /// </summary>
        /// <param name="WatermarkModel"></param>
        /// 
        private List jsonAsList = new List();
        private void CreateDynamicGrid(WatermarkModel watermarkModel, int type)
        {
            try
            {
                string ConfigPath = @"C:\Program Files (x86)\iMix";
                var directory = ConfigPath + "\\ConfiDetails\\ConfigDetailsJSON-Digiphoto.json";
                string jrf = System.IO.File.ReadAllText(directory);
                JavaScriptSerializer serializer = new JavaScriptSerializer();

                jsonAsList = serializer.Deserialize<List>(jrf.ToString());
                var jsonObj = JsonConvert.DeserializeObject<JObject>(jrf).First.First;


                System.Configuration.Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);

                if (type == 1)
                {
                    int vx = Convert.ToInt16((jsonObj["VX"]).ToString());//5;
                    int vy = Convert.ToInt16((jsonObj["VY"]).ToString());//2;

                    for (int i = 0; i <= vx; i++)
                    {
                        RowDefinition gridRow = new RowDefinition();
                        gridRow.Height = new GridLength(100);
                        DynamicGrid.RowDefinitions.Add(gridRow);
                    }
                    for (int i = 0; i <= vy; i++)
                    {
                        ColumnDefinition gridCol1 = new ColumnDefinition();
                        gridCol1.Width = new GridLength(200);
                        DynamicGrid.ColumnDefinitions.Add(gridCol1);
                    }

                    for (int i = 0; i < vx; i++)
                    {
                        for (int j = 0; j < vy; j++)
                        {
                            TextBlock MyControl1 = new TextBlock();
                            MyControl1 = AddTextBlock(MyControl1, watermarkModel);
                            Grid.SetColumn(MyControl1, j);
                            Grid.SetRow(MyControl1, i);
                            DynamicGrid.Children.Add(MyControl1);
                        }
                    }
                    //READ THE CONFIG FILE.
                    DynamicGrid.Margin = new Thickness(Convert.ToDouble((jsonObj["VL"]).ToString()),
                                                      Convert.ToDouble((jsonObj["VT"]).ToString()),
                                                      Convert.ToDouble((jsonObj["VR"]).ToString()),
                                                       Convert.ToDouble((jsonObj["VB"]).ToString())
                                                     );
                }
                else
                {
                    int hx = Convert.ToInt16((jsonObj["HX"]).ToString());//4;
                    int hy = Convert.ToInt16((jsonObj["HY"]).ToString());//3;
                    for (int i = 0; i <= hx; i++)
                    {
                        RowDefinition gridRow = new RowDefinition();
                        gridRow.Height = new GridLength(100);
                        DynamicGrid.RowDefinitions.Add(gridRow);
                    }
                    for (int i = 0; i <= hy; i++)
                    {
                        ColumnDefinition gridCol1 = new ColumnDefinition();
                        gridCol1.Width = new GridLength(200);
                        DynamicGrid.ColumnDefinitions.Add(gridCol1);
                    }

                    ColumnDefinition gridCol3 = new ColumnDefinition();
                    gridCol3.Width = new GridLength(200);
                    //DynamicGrid.RowDefinitions.Add(gridRow4);
                    DynamicGrid.ColumnDefinitions.Add(gridCol3);

                    for (int i = 0; i < hx; i++)
                    {
                        for (int j = 0; j < hy; j++)
                        {
                            TextBlock MyControl1 = new TextBlock();
                            MyControl1 = AddTextBlock(MyControl1, watermarkModel);
                            Grid.SetColumn(MyControl1, j);
                            Grid.SetRow(MyControl1, i);
                            DynamicGrid.Children.Add(MyControl1);
                        }
                    }
                    DynamicGrid.Margin = new Thickness(Convert.ToDouble((jsonObj["HL"]).ToString()),
                                                      Convert.ToDouble((jsonObj["HT"]).ToString()),
                                                      Convert.ToDouble((jsonObj["HR"]).ToString()),
                                                       Convert.ToDouble((jsonObj["HB"]).ToString())
                                                     );
                }
                DynamicGrid.UpdateLayout();
            }
            catch (Exception)
            {

            }
        }


        private TextBlock AddTextBlock(TextBlock myControl1, WatermarkModel watermarkModel)
        {
            var converter = new System.Windows.Media.BrushConverter();
            myControl1.Text = watermarkModel.WatermarkText;
            myControl1.Name = "TextB" + "lock" + count.ToString();
            myControl1.FontSize = watermarkModel.TextFontSize;
            myControl1.FontWeight = (FontWeight)new FontWeightConverter().ConvertFromString(watermarkModel.TextFontWeights.ToString());
            myControl1.Foreground = (Brush)converter.ConvertFromString(watermarkModel.TextFontColor.ToString());
            myControl1.Opacity = Convert.ToDouble(watermarkModel.Opacity);
            return myControl1;
        }
        #endregion
        /// <summary>
        /// This button is used to Launch SHAREit on the window, if it is minimised or closed
        /// Added by Manoj at 17-Apr-2019 - InstaMobile transfer
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnShare_Click(object sender, RoutedEventArgs e)
        {
            var shareitProcesses = Process.GetProcesses().
                                   Where(pr => pr.ProcessName == "SHAREit");
            string path = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
            path = path + "\\SHAREit\\SHAREit.exe";

            Process process = new Process();
            process.EnableRaisingEvents = true;
            process.StartInfo = new ProcessStartInfo();
            process.StartInfo.FileName = path;
            process.Start();

            //Disable task bar
            Microsoft.Win32.RegistryKey HKCU = Microsoft.Win32.Registry.CurrentUser;
            Microsoft.Win32.RegistryKey key = HKCU.CreateSubKey(@"Software\Microsoft\Windows\CurrentVersion\Policies\System");
            key.SetValue("DisableTaskMgr", true ? 0 : 1, Microsoft.Win32.RegistryValueKind.DWord);
        }
        /// <summary>
        /// Added by Monika-17-july-2019 for photogrpher report GroupCount 
        /// </summary>
        /// <param name="PhotBiz"></param>
        private void AddGroupCount(PhotographerBusiness PhotBiz)
        {
            Int32 Operator_ID = LoginUser.UserId;
            var item = PhotBiz.Update_GrpCount(Operator_ID);
        }
        private void btnMorePhoto_Click(object sender, RoutedEventArgs e)
        {

            if (lstImages.Items != null && lstImages.Items.Count > 0)
            {

                LstMyItems myItems = ((DigiPhoto.LstMyItems)(lstImages.SelectedItem));
                if (myItems != null)
                {
                    this.Cursor = Cursors.Wait;
                    try
                    {
                        if (myItems.MediaType != 1)
                        {
                            imageInfo.IsVideo = true;
                            gdMediaPlayer.Visibility = Visibility.Collapsed;
                        }
                        else
                            imageInfo.IsVideo = false;

                        RobotImageLoader.curItem = myItems;
                        ErrorHandler.ErrorHandler.LogFileWrite(" FR Photo ID :" + myItems.PhotoId.ToString());
                        int currentImageId = myItems.PhotoId;
                        ErrorHandler.ErrorHandler.LogFileWrite(" imageid ID :" + currentImageId.ToString());

                        //DateTime startdate = myItems.CreatedOn;
                        //DateTime enddate = startdate.AddHours(1);
                        //startdate = startdate.AddHours(-1);




                        List<PhotoCaptureInfo> captureDetails = new List<PhotoCaptureInfo>();
                        captureDetails = (new PhotoBusiness()).GetphotoCapturedetails(currentImageId);

                        int noofdays = this.cmbNoofDays.SelectedIndex;
                        DateTime enddate = DateTime.Now.Date;
                        DateTime startdate;
                        switch (noofdays)
                        {
                            case 1:
                                startdate = enddate.AddDays(-1);
                                break;
                            case 2:
                                startdate = enddate.AddDays(-2);
                                break;
                            case 3:
                                startdate = enddate.AddDays(-3);
                                break;
                            default:
                                startdate = enddate;
                                break;

                        }
                        enddate = enddate.AddMinutes(59);
                        enddate = enddate.AddHours(23);


                        //DateTime startdate = captureDetails[0].CaptureDate.Date;
                        //DateTime enddate = startdate;
                        //enddate = enddate.AddHours(23);
                        //enddate = enddate.AddMinutes(59);


                        //myItems.
                        //if (APIWrapper.SelectedSite == 0)
                        //    APIWrapper.SelectedSite = ConfigManager.SubStoreId;

                        APIWrapper apiwrapper = new APIWrapper();
                        APIResponse response = apiwrapper.GetImages(currentImageId, startdate.ToString("dd-MM-yyyy HH:mm:ss"), enddate.ToString("dd-MM-yyyy HH:mm:ss"), APIWrapper.SelectedThreshold, APIWrapper.SelectedSite);
                        this.Cursor = Cursors.Arrow;
                        if (response != null)
                        {
                            if (response.SiteIDs != null)
                            {
                                RobotImageLoader.IsZeroSearchNeeded = false;
                                RobotImageLoader.RFID = String.Join(",", response.SiteIDs);
                                RobotImageLoader.SearchCriteria = "FaceScan";
                                RobotImageLoader.PhotoIds = response.SiteIDs;
                                this.UpdateThreshold();
                                this.SearchImages(response.SiteIDs);

                            }
                        }

                        e.Handled = true;
                    }
                    catch
                    {

                    }
                    finally
                    {
                        this.Cursor = Cursors.Arrow;
                    }
                    //string[] ids = new string[] { "28", "27", "26", "25", "24" };
                    //this.SearchImages(ids.ToList());
                }
            }
        }

        private void thLower_Checked(object sender, RoutedEventArgs e)
        {
            thbthreshold1.IsChecked = false;
            APIWrapper.SelectedThreshold = "LOWER";
        }

        private void thHigh_Checked(object sender, RoutedEventArgs e)
        {
            if (thbthreshold4 != null)
                thbthreshold4.IsChecked = false;
            APIWrapper.SelectedThreshold = "HIGH";
        }
        public void UpdateThreshold()
        {
            if (APIWrapper.SelectedThreshold == "HIGH")
                thbthreshold1.IsChecked = true;
            else
                thbthreshold4.IsChecked = true;
        }
    }
}
