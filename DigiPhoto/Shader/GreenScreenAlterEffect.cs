﻿using System;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Effects;
using System.Windows.Media.Media3D;

namespace DigiPhoto.Shader
{
    public class GreenScreenAlterEffect : ShaderEffect
    {
        public static readonly DependencyProperty InputProperty = ShaderEffect.RegisterPixelShaderSamplerProperty("Input", typeof(GreenScreenAlterEffect), 0);
        public static readonly DependencyProperty GreenColorProperty = DependencyProperty.Register("GreenColor", typeof(double), typeof(GreenScreenAlterEffect), new UIPropertyMetadata(((double)(0D)), PixelShaderConstantCallback(0)));
        public static readonly DependencyProperty GainProperty = DependencyProperty.Register("Gain", typeof(double), typeof(GreenScreenAlterEffect), new UIPropertyMetadata(((double)(0D)), PixelShaderConstantCallback(1)));
        public GreenScreenAlterEffect()
        {
            PixelShader pixelShader = new PixelShader();
            pixelShader.UriSource = new Uri(@"/Shader/GreenScreenAlter.ps", UriKind.Relative);
            this.PixelShader = pixelShader;

            this.UpdateShaderValue(InputProperty);
            this.UpdateShaderValue(GreenColorProperty);
            this.UpdateShaderValue(GainProperty);
        }
        public Brush Input
        {
            get
            {
                return ((Brush)(this.GetValue(InputProperty)));
            }
            set
            {
                this.SetValue(InputProperty, value);
            }
        }
        public double GreenColor
        {
            get
            {
                return ((double)(this.GetValue(GreenColorProperty)));
            }
            set
            {
                this.SetValue(GreenColorProperty, value);
            }
        }
        public double Gain
        {
            get
            {
                return ((double)(this.GetValue(GainProperty)));
            }
            set
            {
                this.SetValue(GainProperty, value);
            }
        }
    }
}
