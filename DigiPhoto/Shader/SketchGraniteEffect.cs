﻿using System;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Effects;
using System.Windows.Media.Media3D;

namespace DigiPhoto.Shader
{
    public class SketchGraniteEffect : ShaderEffect
    {
        public static readonly DependencyProperty InputProperty = ShaderEffect.RegisterPixelShaderSamplerProperty("Input", typeof(SketchGraniteEffect), 0);
        public static readonly DependencyProperty BrushSizeProperty = DependencyProperty.Register("BrushSize", typeof(double), typeof(SketchGraniteEffect), new UIPropertyMetadata(((double)(0.003D)), PixelShaderConstantCallback(0)));
        public SketchGraniteEffect()
        {
            PixelShader pixelShader = new PixelShader();
            pixelShader.UriSource = new Uri("/DigiPhoto;component/Shader/SketchGranite.ps", UriKind.Relative);
            this.PixelShader = pixelShader;

            this.UpdateShaderValue(InputProperty);
            this.UpdateShaderValue(BrushSizeProperty);
        }
        public Brush Input
        {
            get
            {
                return ((Brush)(this.GetValue(InputProperty)));
            }
            set
            {
                this.SetValue(InputProperty, value);
            }
        }
        /// <summary>The brush size of the sketch effect.</summary>
        public double BrushSize
        {
            get
            {
                return ((double)(this.GetValue(BrushSizeProperty)));
            }
            set
            {
                this.SetValue(BrushSizeProperty, value);
            }
        }
    }
}
