﻿using System;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Effects;
using System.Windows.Media.Media3D;

namespace DigiPhoto.Shader
{
    public class EmbossedEffect : ShaderEffect
    {
        public static readonly DependencyProperty InputProperty = ShaderEffect.RegisterPixelShaderSamplerProperty("Input", typeof(EmbossedEffect), 0);
        public static readonly DependencyProperty AmountProperty = DependencyProperty.Register("Amount", typeof(double), typeof(EmbossedEffect), new UIPropertyMetadata(((double)(0.5D)), PixelShaderConstantCallback(0)));
        public static readonly DependencyProperty WidthProperty = DependencyProperty.Register("Width", typeof(double), typeof(EmbossedEffect), new UIPropertyMetadata(((double)(0.003D)), PixelShaderConstantCallback(1)));
        public EmbossedEffect()
        {
            PixelShader pixelShader = new PixelShader();
            pixelShader.UriSource = new Uri("/DigiPhoto;component/Shader/Embossed.ps", UriKind.Relative);
            this.PixelShader = pixelShader;

            this.UpdateShaderValue(InputProperty);
            this.UpdateShaderValue(AmountProperty);
            this.UpdateShaderValue(WidthProperty);
        }
        public Brush Input
        {
            get
            {
                return ((Brush)(this.GetValue(InputProperty)));
            }
            set
            {
                this.SetValue(InputProperty, value);
            }
        }
        /// <summary>The amplitude of the embossing.</summary>
        public double Amount
        {
            get
            {
                return ((double)(this.GetValue(AmountProperty)));
            }
            set
            {
                this.SetValue(AmountProperty, value);
            }
        }
        /// <summary>The separation between samples (as a fraction of input size).</summary>
        public double Width
        {
            get
            {
                return ((double)(this.GetValue(WidthProperty)));
            }
            set
            {
                this.SetValue(WidthProperty, value);
            }
        }
    }
}
