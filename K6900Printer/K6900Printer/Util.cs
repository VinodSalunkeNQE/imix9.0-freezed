﻿using K6900Printer.Entity;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace K6900Printer
{
    internal static class Util
    {
        [DllImport("kernel32.dll", SetLastError = true, CallingConvention = CallingConvention.Winapi)]
        [return: MarshalAs(UnmanagedType.Bool)]
        private static extern bool IsWow64Process([In] IntPtr hProcess, [Out] out bool wow64Process);


        static bool is64BitProcess = (IntPtr.Size == 8);
        internal static bool is64BitOperatingSystem = is64BitProcess || InternalCheckIsWow64();

        private static List<PrintPaperMapping> _printPaperMappings;

        private static bool InternalCheckIsWow64()
        {
            if ((Environment.OSVersion.Version.Major == 5 && Environment.OSVersion.Version.Minor >= 1) ||
                Environment.OSVersion.Version.Major >= 6)
            {
                using (Process p = Process.GetCurrentProcess())
                {
                    bool retVal;
                    if (!IsWow64Process(p.Handle, out retVal))
                    {
                        return false;
                    }
                    return retVal;
                }
            }
            else
            {
                return false;
            }
        }

        public static byte[] ResizeApplyProfile(string filename, int printWidth, int printHeight, string inputProfile, string outputProfile)
        {
            using (MemoryStream ms = new MemoryStream())
            {
                int imageWidth = 0;
                int imageHeight = 0;

                using (System.Drawing.Image img = System.Drawing.Image.FromFile(filename))
                {
                    imageWidth = img.Width;
                    imageHeight = img.Height;
                }

                double currentAspectRatio = (double)imageWidth / (double)imageHeight;
                double desiredAspectRatio = (double)printWidth / (double)printHeight;
                Int32Rect rect;

                // Rotate
                bool rotateImage = (desiredAspectRatio > 1 && currentAspectRatio < 1) || (desiredAspectRatio < 1 && currentAspectRatio > 1);

                // Scale
                bool scaleOnWidth = true;
                if (rotateImage)
                {
                    if (desiredAspectRatio > 1)
                    {
                        if ((1 / currentAspectRatio) > desiredAspectRatio)
                        {
                            scaleOnWidth = true;
                        }
                        else
                        {
                            scaleOnWidth = false;
                        }
                    }
                    else
                    {
                        if ((1 / currentAspectRatio) < desiredAspectRatio)
                        {
                            scaleOnWidth = false;
                        }
                        else
                        {
                            scaleOnWidth = true;
                        }
                    }
                }
                else
                {
                    if (desiredAspectRatio > 1)
                    {
                        if (currentAspectRatio > desiredAspectRatio)
                        {
                            scaleOnWidth = false;
                        }
                        else
                        {
                            scaleOnWidth = true;
                        }
                    }
                    else
                    {
                        if (currentAspectRatio < desiredAspectRatio)
                        {

                            scaleOnWidth = true;
                        }
                        else
                        {
                            scaleOnWidth = false;
                        }
                    }
                }
                //Setup Image
                BitmapImage bitmapImage = new BitmapImage();
                bitmapImage.BeginInit();
                bitmapImage.UriSource = new Uri(filename);
                bitmapImage.CacheOption = BitmapCacheOption.OnLoad;

                // Scale
                if (rotateImage)
                {
                    if (scaleOnWidth)
                    {
                        bitmapImage.DecodePixelWidth = printHeight;
                    }
                    else
                    {
                        bitmapImage.DecodePixelHeight = printWidth;
                    }
                }
                else
                {
                    if (scaleOnWidth)
                    {
                        bitmapImage.DecodePixelWidth = printWidth;
                    }
                    else
                    {
                        bitmapImage.DecodePixelHeight = printHeight;
                    }
                }

                bitmapImage.EndInit();

                // Crop
                if (rotateImage)
                {
                    if (desiredAspectRatio > 1)
                    {
                        if ((1 / currentAspectRatio) > desiredAspectRatio)
                        {
                            rect = new Int32Rect(0, (int)Math.Round((bitmapImage.PixelHeight - (bitmapImage.PixelWidth * desiredAspectRatio)) / 2), bitmapImage.PixelWidth, (int)Math.Round(bitmapImage.PixelWidth * desiredAspectRatio));
                        }
                        else
                        {
                            rect = new Int32Rect((int)Math.Round((bitmapImage.PixelWidth - (bitmapImage.PixelHeight / desiredAspectRatio)) / 2), 0, (int)Math.Round(bitmapImage.PixelHeight / desiredAspectRatio), bitmapImage.PixelHeight);
                        }
                    }
                    else
                    {
                        if ((1 / currentAspectRatio) < desiredAspectRatio)
                        {
                            rect = new Int32Rect((int)Math.Round((bitmapImage.PixelWidth - (bitmapImage.PixelHeight / desiredAspectRatio)) / 2), 0, (int)Math.Round(bitmapImage.PixelHeight / desiredAspectRatio), bitmapImage.PixelHeight);
                        }
                        else
                        {
                            rect = new Int32Rect(0, (int)Math.Round((bitmapImage.PixelHeight - (bitmapImage.PixelWidth * desiredAspectRatio)) / 2), bitmapImage.PixelWidth, (int)Math.Round(bitmapImage.PixelWidth * desiredAspectRatio));
                        }
                    }
                }
                else
                {
                    if (desiredAspectRatio > 1)
                    {
                        if (currentAspectRatio > desiredAspectRatio)
                        {
                            rect = new Int32Rect((int)Math.Round((bitmapImage.PixelWidth - (bitmapImage.PixelHeight * desiredAspectRatio)) / 2), 0, (int)Math.Round(bitmapImage.PixelHeight * desiredAspectRatio), bitmapImage.PixelHeight);
                        }
                        else
                        {
                            rect = new Int32Rect(0, (int)Math.Round((bitmapImage.PixelHeight - (bitmapImage.PixelWidth / desiredAspectRatio)) / 2), bitmapImage.PixelWidth, (int)Math.Round(bitmapImage.PixelWidth / desiredAspectRatio));
                        }
                    }
                    else
                    {
                        if (currentAspectRatio < desiredAspectRatio)
                        {
                            rect = new Int32Rect(0, (int)Math.Round((bitmapImage.PixelHeight - (bitmapImage.PixelWidth / desiredAspectRatio)) / 2), bitmapImage.PixelWidth, (int)Math.Round(bitmapImage.PixelWidth / desiredAspectRatio));
                        }
                        else
                        {
                            rect = new Int32Rect((int)Math.Round((bitmapImage.PixelWidth - (bitmapImage.PixelHeight * desiredAspectRatio)) / 2), 0, (int)Math.Round(bitmapImage.PixelHeight * desiredAspectRatio), bitmapImage.PixelHeight);
                        }
                    }
                }

                CroppedBitmap croppedBitmap = new CroppedBitmap(bitmapImage, rect);

                // Rotate
                TransformedBitmap rotatedBitmap = new TransformedBitmap();
                rotatedBitmap.BeginInit();
                rotatedBitmap.Source = croppedBitmap;
                double rotateAngle = 0;
                if (rotateImage)
                {
                    rotateAngle = 90;
                }
                RotateTransform rotate = new RotateTransform(rotateAngle);
                rotatedBitmap.Transform = rotate;
                rotatedBitmap.EndInit();

                // Apply color profiles
                if (File.Exists(inputProfile) && File.Exists(outputProfile))
                {
                    BitmapSource bitmapSource = BitmapFrame.Create(rotatedBitmap);
                    Uri inputProfileUri = new Uri(inputProfile);
                    ColorContext sourceColorContext = new ColorContext(inputProfileUri);

                    Uri outputProfileUri = new Uri(outputProfile);
                    ColorContext destColorContext = new ColorContext(outputProfileUri);

                    ColorConvertedBitmap ccb = new ColorConvertedBitmap(bitmapSource, sourceColorContext, destColorContext, bitmapImage.Format);
                    BitmapEncoder bEncoder = new BmpBitmapEncoder();
                    bEncoder.Frames.Add(BitmapFrame.Create(ccb));
                    bEncoder.Save(ms);
                }
                else
                {
                    BitmapEncoder bEncoder = new BmpBitmapEncoder();
                    bEncoder.Frames.Add(BitmapFrame.Create(rotatedBitmap));
                    bEncoder.Save(ms);
                }

                return ms.ToArray();
            }
        }

        public static List<PrintPaperMapping> GetPrintPaperMappings()
        {
            return _printPaperMappings ?? (_printPaperMappings = new List<PrintPaperMapping>
            {
                new PrintPaperMapping(PrintAreaInfoCodeType.ThreeByFive, PrintSize.ThreeByFive,
                    PrintTypeInfoCodeType.Normal),
                new PrintPaperMapping(PrintAreaInfoCodeType.FourBySix, PrintSize.FourBySix,
                    PrintTypeInfoCodeType.Normal),
                new PrintPaperMapping(PrintAreaInfoCodeType.FiveBySeven, PrintSize.FiveBySeven,
                    PrintTypeInfoCodeType.Normal),
                new PrintPaperMapping(PrintAreaInfoCodeType.SixByEight, PrintSize.SixByEight,
                    PrintTypeInfoCodeType.Normal),
                new PrintPaperMapping(PrintAreaInfoCodeType.SixByEight, PrintSize.FourBySix,
                    PrintTypeInfoCodeType.Combo1Image),
                new PrintPaperMapping(PrintAreaInfoCodeType.SixByEight, PrintSize.FiveBySeven,
                    PrintTypeInfoCodeType.WithBorder),
                new PrintPaperMapping(PrintAreaInfoCodeType.SixByEight, PrintSize.ThreeByFive,
                    PrintTypeInfoCodeType.BorderCombo1Image)
            });
        }

        public static void GetIdsFromWord(int word, out int pageId, out int printerId)
        {
            // get printer ID (upper byte) and page ID (lower byte)
            var number = Convert.ToUInt16(word);
            var upper = Convert.ToByte(number >> 8);
            var lower = Convert.ToByte(number & 0x00FF);
            printerId = upper;
            pageId = lower;
        }
    }
}
