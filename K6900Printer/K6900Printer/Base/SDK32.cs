﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace K6900Printer.Base
{
    /// <summary>
    /// Authors Name: Kailash Changed Behera
    /// Created Date: 06 Aug 2019
    /// Description :Used to Load Unsafe CHCUSB 32 Bit DLL API.
    /// </summary>
    internal class SDK32
    {
        #region Unsafe CHCUSB DLL API

         [DllImport("KA6900.dll")]
        internal static extern unsafe bool chcusb_MakeThread(ushort maxCount);

         [DllImport("KA6900.dll")]
        internal static extern unsafe bool chcusb_ReleaseThread(ushort* rResult);

         [DllImport("KA6900.dll")]
        internal static extern unsafe bool chcusb_AttachThreadCount(ushort* rCount, ushort* rMaxCount);

         [DllImport("KA6900.dll")]
        internal static extern bool chcusb_open(ref ushort result);

        [DllImport(@"\KA6900\x32\KA6900.dll")]
        internal static extern void chcusb_close();

         [DllImport("KA6900.dll")]
        internal static extern unsafe bool chcusb_blinkLED(ushort* result);

         [DllImport("KA6900.dll")]
        internal static extern int chcusb_listupPrinterSN([In, Out] SerialNumRec[] rSerialArray);

         [DllImport("KA6900.dll")]
        internal static extern unsafe bool chcusb_status(ushort* result);

         [DllImport("KA6900.dll")]
        internal static extern unsafe bool chcusb_copies(ushort numberOfCopies, ushort* result);

         [DllImport("KA6900.dll")]
        internal static extern unsafe bool chcusb_startpage(ushort* startPage, ushort* result);

         [DllImport("KA6900.dll")]
        internal static extern unsafe bool chcusb_getPrinterInfo(ushort key, void* buff, int* bufferSize);

         [DllImport("KA6900.dll")]
        internal static extern unsafe bool chcusb_setPrinterInfo(ushort key, void* buff, int* bufferSize, ushort* result);

         [DllImport("KA6900.dll")]
        internal static extern unsafe bool chcusb_imageformat(ushort rgbbgrFormat, ushort comp, ushort depth, ushort width, ushort height, ushort x_offset, ushort y_offset, ushort* result);

         [DllImport("KA6900.dll")]
        internal static extern unsafe bool chcusb_makeGamma(ushort wk, void* toneR, void* toneG, void* toneB);

         [DllImport("KA6900.dll")]
        internal static extern unsafe bool chcusb_setIcctable(char* inProfileName, char* outProfileName, ushort renderingIntents, void* intoneR, void* intoneG, void* intoneB, void* outtoneR, void* outtoneG, void* outtoneB, ushort* result);

         [DllImport("KA6900.dll")]
        internal static extern unsafe bool chcusb_write(void* imageBuff, int* bufferSize, ushort* result);

         [DllImport("KA6900.dll")]
        internal static extern unsafe bool chcusb_endpage(ushort* result);

         [DllImport("KA6900.dll")]
        internal static extern unsafe IntPtr chcusb_selectPrinterSN(IntPtr printerSN, ushort* rResult);

         [DllImport("KA6900.dll")]
        internal static extern unsafe bool chcusb_setCutList(IntPtr cutList, ushort* rResult);

        #endregion
    }
}
