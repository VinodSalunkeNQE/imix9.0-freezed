﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace K6900Printer.Base
{
    /// <summary>
    /// Authors Name: Kailash Changed Behera
    /// Created Date: 06 Aug 2019
    /// Description :Used to call Unsafe CHUSB DLLs.
    /// </summary>
    internal static class SDK
    {

        #region Safe CHCUSB DLL API
        /// <summary>
        /// Sets the number of maximum threads accessing CHCUSB DLL from a multi-thread application. 
        /// </summary>
        /// <param name="maxCount">Set the number of maximum threads (range from 1 to 100). Default value = 1. </param>
        /// <returns>When the number of maximum threads can be set normally, TRUE is returned. If failed, FALSE is returned. </returns>
        internal static bool MakeThreads(int maxCount)
        {
            unsafe
            {
                bool successfulCall;
                if (Util.is64BitOperatingSystem == true)
                    successfulCall = SDK64.chcusb_MakeThread((ushort)maxCount);
                else
                    successfulCall = SDK32.chcusb_MakeThread((ushort)maxCount);
                return successfulCall;
            }
        }
        /// <summary>
        /// Obtains numbers of objects that the application uses and number of maxium objects that is set by schcusb_makeThread function. 
        /// </summary>
        /// <param name="count">Pointer to the intiger type variable for storing the number of objects that the application uses</param>
        /// <param name="maxCount">Pointer to the intiger type variable for storing the number of objects set by chcusb_makeThread function </param>
        /// <returns>Returns "TRUE" when this function ends normally or "FALSE" in the other cases. </returns>
        internal static bool AttachThreadCount(out int count, out int maxCount)
        {
            unsafe
            {
                ushort countPointer = 0;
                ushort maxCountPointer = 0;
                bool successfulCall;
                if (Util.is64BitOperatingSystem == true)
                    successfulCall = SDK64.chcusb_AttachThreadCount(&countPointer, &maxCountPointer);
                else
                    successfulCall = SDK32.chcusb_AttachThreadCount(&countPointer, &maxCountPointer);
                count = countPointer;
                maxCount = maxCountPointer;
                return successfulCall;
            }
        }
        /// <summary>
        /// Initializes the execution environment of CHCUSB DLL. 
        /// </summary>
        /// <param name="result">Pointer to the intiger type variable for storing the execution status.Returns TRUE when the function ends normally and FALSE in the case of an error.  </param>
        /// <returns>Returns "TRUE" when this function ends normally or "FALSE" in the other cases. </returns>
        internal static bool Open(out int result)
        {
            try
            {
                unsafe
                {
                    ushort resultPointer = 0;
                    bool successfulCall;

                    if (Util.is64BitOperatingSystem == true)
                        successfulCall = SDK64.chcusb_open(ref resultPointer);
                    else
                        successfulCall = SDK32.chcusb_open(ref resultPointer);

                    result = resultPointer;
                    return successfulCall;
                }
            }
            catch (Exception ex)
            {
                result = -1;
                return false;
            }
        }
        /// <summary>
        /// Closes the work memory and DLL files that the CHCUSB DLL used
        /// </summary>
        internal static void Close()
        {
            unsafe
            {
                if (Util.is64BitOperatingSystem == true)
                    SDK64.chcusb_close();
                else
                    SDK32.chcusb_close();
            }
        }
        /// <summary>
        /// Closes the all objects that specified by “chcusb_makeThread” function and closes the work memory and DLL files that the CHCUSB DLL used. 
        /// </summary>
        /// <param name="result">Pointer to the intiger type variable for storing the execution status</param>
        /// <returns>Returns "TRUE" when this function ends normally or "FALSE" in the other cases. </returns>
        internal static bool ReleaseThreads(out int result)
        {
            unsafe
            {
                ushort resultPointer = 0;
                bool successfulCall;

                if (Util.is64BitOperatingSystem == true)
                    successfulCall = SDK64.chcusb_ReleaseThread(&resultPointer);
                else
                    successfulCall = SDK32.chcusb_ReleaseThread(&resultPointer);

                result = resultPointer;
                return successfulCall;
            }
        }
        /// <summary>
        /// Blinks the LED of the selected printer five times. 
        /// </summary>
        /// <param name="result">Pointer to the intiger type variables for executing status storage.Returns TRUE when this function is executed to completion, or returns FALSE in all other cases.  </param>
        /// <returns>Returns "TRUE" when this function ends normally or "FALSE" in the other cases. </returns>
        internal static bool BlinkLED(out int result)
        {
            unsafe
            {
                ushort resultPointer = 0;
                bool successfulCall;

                if (Util.is64BitOperatingSystem == true)
                    successfulCall = SDK64.chcusb_blinkLED(&resultPointer);
                else
                    successfulCall = SDK32.chcusb_blinkLED(&resultPointer);
                result = resultPointer;
                return successfulCall;
            }
        }
        /// <summary>
        /// Acquires the status of the specified printer. 
        /// </summary>
        /// <param name="result">Pointer to the intiger type variable for storing the executing status. Returns  executing status </param>
        /// <returns>Returns "TRUE" when this function ends normally or "FALSE" in the other cases. </returns>
        internal static bool PrinterStatus(out int result)
        {
            unsafe
            {
                ushort resultPointer = 0;
                bool successfulCall;

                if (Util.is64BitOperatingSystem == true)
                    successfulCall = SDK64.chcusb_status(&resultPointer);
                else
                    successfulCall = SDK32.chcusb_status(&resultPointer);

                result = resultPointer;
                return successfulCall;
            }
        }
        /// <summary>
        /// Sets the number of prints. 
        /// </summary>
        /// <param name="numberOfCopies">Specify the number of print copies in the range from 1 to 65535. </param>
        /// <param name="result">Pointer to the intiger type variable for storing the execution result code. </param>
        /// <returns>Returns TRUE when the function ends normally and FALSE in the case of an error. </returns>
        internal static bool Copies(int numberOfCopies, out int result)
        {
            unsafe
            {
                ushort resultPointer = 0;
                bool successfulCall;

                if (Util.is64BitOperatingSystem == true)
                    successfulCall = SDK64.chcusb_copies((ushort)numberOfCopies, &resultPointer);
                else
                    successfulCall = SDK32.chcusb_copies((ushort)numberOfCopies, &resultPointer);
                result = resultPointer;
                return successfulCall;
            }
        }
        /// <summary>
        /// Sends a picture data parameter (printer command) in order to transfer the picture data to the printer. 
        /// </summary>
        /// <param name="startPage">Pointer to the intiger type variables for page ID storage </param>
        /// <param name="result">Pointer to the intiger type variable for storing the executing status</param>
        /// <returns>Returns "TRUE" when this function ends normally or "FALSE" in the other case.</returns>
        internal static bool StartPage(out int startPage, out int result)
        {
            unsafe
            {
                ushort startPagePointer = 0;
                ushort resultPointer = 0;
                bool successfulCall;

                if (Util.is64BitOperatingSystem == true)
                    successfulCall = SDK64.chcusb_startpage(&startPagePointer, &resultPointer);
                else
                    successfulCall = SDK32.chcusb_startpage(&startPagePointer, &resultPointer);

                startPage = startPagePointer;
                result = resultPointer;
                return successfulCall;
            }
        }
        /// <summary>
        /// Obtain the information of the specified printer based on tag identifier.
        /// </summary>
        /// <param name="key">Specify the tag number of information you want to get. </param>
        /// <param name="bufferSize">Pointer to the memory that stores obtained information. </param>
        /// <param name="buff">Stores the length (in bytes) of information this function obtained. When "NULL" is specified for "rBuffer," this function sets the size of memory (in bytes) required to get information in "rLen". When unidentified tag identifier is selected, “0” is stored. </param>
        /// <returns>Return "TRUE" when this function ends normally Return "FALSE" when an undefined tag identifier is specified, or it cannot get the communicated information with the printer is disabled.</returns>
        internal static bool GetPrinterInfo(PINF_Tag key, PINF_Tag_Size_Get bufferSize, out byte[] buff)
        {
            unsafe
            {
                int bufferSizePointer = (int)bufferSize;
                buff = new byte[bufferSizePointer];
                IntPtr ptr = Marshal.AllocHGlobal(Marshal.SizeOf(typeof(byte)) * bufferSizePointer);
                Marshal.Copy(buff, 0, ptr, bufferSizePointer);

                bool successfulCall;

                if (Util.is64BitOperatingSystem == true)
                    successfulCall = SDK64.chcusb_getPrinterInfo((ushort)key, (void*)ptr, &bufferSizePointer);
                else
                    successfulCall = SDK32.chcusb_getPrinterInfo((ushort)key, (void*)ptr, &bufferSizePointer);

                if (successfulCall)
                {
                    Marshal.Copy(ptr, buff, 0, bufferSizePointer);
                }

                Marshal.FreeHGlobal(ptr);

                return successfulCall;
            }
        }
        /// <summary>
        /// Sets information of media of the specified printer. 
        /// </summary>
        /// <param name="key">Specify the tag number of information you want to set. </param>
        /// <param name="buff">Pointer to memory that stores information to be set </param>
        /// <param name="result">Pointer to a intiger type variable that stores the executing status Output </param>
        /// <returns>Returns "TRUE" when this function ends normally. Returns "FALSE" when an undefined tag identifier is specified or when the communication with the printer is disabled. </returns>
        internal static bool SetPrinterInfo(PINF_Tag key, byte[] buff, out int result)
        {
            unsafe
            {
                ushort resultPointer = 0;
                int bufferSizePointer = (int)buff.Length;
                IntPtr ptr = Marshal.AllocHGlobal(Marshal.SizeOf(typeof(byte)) * bufferSizePointer);
                Marshal.Copy(buff, 0, ptr, bufferSizePointer);

                bool successfulCall;

                if (Util.is64BitOperatingSystem == true)
                    successfulCall = SDK64.chcusb_setPrinterInfo((ushort)key, (void*)ptr, &bufferSizePointer, &resultPointer);
                else
                    successfulCall = SDK32.chcusb_setPrinterInfo((ushort)key, (void*)ptr, &bufferSizePointer, &resultPointer);
                result = resultPointer;

                Marshal.FreeHGlobal(ptr);

                return successfulCall;
            }
        }
        /// <summary>
        /// Set image information.  
        /// </summary>
        /// <param name="rgbFormat">Specify the format of image to be transferred. </param>
        /// <param name="comp">Specify the number of color components. </param>
        /// <param name="depth">Specify 8.</param>
        /// <param name="width">Specify the width of image (in pixels). </param>
        /// <param name="height">Specify the height of image (in pixels). </param>
        /// <param name="result">Pointer to a intiger type variable for storing the executing status. </param>
        /// <returns>Returns "TRUE" when the function ends normally or "FALSE" in the other cases. </returns>
        internal static bool ImageFormat(PixelFormat rgbFormat, int comp, int depth, int width, int height, out int result)
        {
            unsafe
            {
                ushort resultPointer = 0;
                bool successfulCall;

                if (Util.is64BitOperatingSystem == true)
                    successfulCall = SDK64.chcusb_imageformat((ushort)rgbFormat, (ushort)comp, (ushort)depth, (ushort)width, (ushort)height, 0, 0, &resultPointer);
                else
                    successfulCall = SDK32.chcusb_imageformat((ushort)rgbFormat, (ushort)comp, (ushort)depth, (ushort)width, (ushort)height, 0, 0, &resultPointer);
                result = resultPointer;
                return successfulCall;
            }
        }
        /// <summary>
        /// Creates tone curve data to be used by the "chcusb_setIcctable" function.
        /// </summary>
        /// <param name="wk">Value multiplying a gamma coefficient (3.00 to 0.33 at intervals of 0.01) by 100 </param>
        /// <param name="toneR">Pointer to a byte type 256-element array for the tone curve R </param>
        /// <param name="toneG">Pointer to a byte type 256-element array for the tone curve G </param>
        /// <param name="toneB">Pointer to a byte type 256-element array for the tone curve B </param>
        /// <returns>To use a gamma correction coefficient for a tone curve specified by the "chcusb_setIcctable" function, create the table by this function.</returns>
        internal static bool MakeGamma(int wk, out byte[] toneR, out byte[] toneG, out byte[] toneB)
        {
            unsafe
            {
                toneR = new byte[256];
                toneG = new byte[256];
                toneB = new byte[256];

                IntPtr ptrToneR = Marshal.AllocHGlobal(Marshal.SizeOf(typeof(byte)) * toneR.Length);
                Marshal.Copy(toneR, 0, ptrToneR, toneR.Length);

                IntPtr ptrToneG = Marshal.AllocHGlobal(Marshal.SizeOf(typeof(byte)) * toneG.Length);
                Marshal.Copy(toneG, 0, ptrToneG, toneG.Length);

                IntPtr ptrToneB = Marshal.AllocHGlobal(Marshal.SizeOf(typeof(byte)) * toneB.Length);
                Marshal.Copy(toneB, 0, ptrToneB, toneB.Length);

                bool successfulCall;

                if (Util.is64BitOperatingSystem == true)
                    successfulCall = SDK64.chcusb_makeGamma((ushort)wk, (void*)ptrToneR, (void*)ptrToneG, (void*)ptrToneB);
                else
                    successfulCall = SDK32.chcusb_makeGamma((ushort)wk, (void*)ptrToneR, (void*)ptrToneG, (void*)ptrToneB);
                if (successfulCall)
                {
                    Marshal.Copy(ptrToneR, toneR, 0, toneR.Length);
                    Marshal.Copy(ptrToneG, toneG, 0, toneG.Length);
                    Marshal.Copy(ptrToneB, toneB, 0, toneB.Length);
                }

                Marshal.FreeHGlobal(ptrToneR);
                Marshal.FreeHGlobal(ptrToneG);
                Marshal.FreeHGlobal(ptrToneB);

                return successfulCall;
            }
        }
        /// <summary>
        /// Enables (ON) or disables (OFF) the color matching function. 
        /// </summary>
        /// <param name="inProfileName">Specify the path to the input profile. </param>
        /// <param name="outProfileName">Specify the path to the output profile. To enable the color matching function, specify the paths to input and output profiles. To disable the color matching function, specify "NULL" for "inProfileName" and "outProfileName." </param>
        /// <param name="renderingIntents">Specify “0”. </param>
        /// <param name="intoneR">Pointer to the byte type 256-element array for the input tone curve R </param>
        /// <param name="intoneG">Pointer to the byte type 256-element array for the input tone curve G</param>
        /// <param name="intoneB">Pointer to the byte type 256-element array for the input tone curve B</param>
        /// <param name="outtoneR">Pointer to the byte type 256-element array for the output tone curve R </param>
        /// <param name="outtoneG">Pointer to the byte type 256-element array for the output tone curve G </param>
        /// <param name="outtoneB">Pointer to the byte type 256-element array for the output tone curve B </param>
        /// <param name="result"></param>
        /// <returns>Returns "TRUE" when the color matching function is enabled with valid I/O profile values or when "NULL" is specified for "inProfileName" and "outProfileName" or "FALSE" when the specified profile is not available or when a memory allocation error occurs. </returns>
        internal static bool SetIcctable(char? inProfileName, char? outProfileName, int renderingIntents, byte[] intoneR, byte[] intoneG, byte[] intoneB, byte[] outtoneR, byte[] outtoneG, byte[] outtoneB, out int result)
        {
            unsafe
            {
                ushort resultPointer = 0;
                IntPtr ptrInToneR = Marshal.AllocHGlobal(Marshal.SizeOf(typeof(byte)) * intoneR.Length);
                Marshal.Copy(intoneR, 0, ptrInToneR, intoneR.Length);

                IntPtr ptrInToneG = Marshal.AllocHGlobal(Marshal.SizeOf(typeof(byte)) * intoneG.Length);
                Marshal.Copy(intoneG, 0, ptrInToneG, intoneG.Length);

                IntPtr ptrInToneB = Marshal.AllocHGlobal(Marshal.SizeOf(typeof(byte)) * intoneB.Length);
                Marshal.Copy(intoneB, 0, ptrInToneB, intoneB.Length);

                IntPtr ptrOutToneR = Marshal.AllocHGlobal(Marshal.SizeOf(typeof(byte)) * outtoneR.Length);
                Marshal.Copy(outtoneR, 0, ptrOutToneR, outtoneR.Length);

                IntPtr ptrOutToneG = Marshal.AllocHGlobal(Marshal.SizeOf(typeof(byte)) * outtoneG.Length);
                Marshal.Copy(outtoneG, 0, ptrOutToneG, outtoneG.Length);

                IntPtr ptrOutToneB = Marshal.AllocHGlobal(Marshal.SizeOf(typeof(byte)) * outtoneB.Length);
                Marshal.Copy(outtoneB, 0, ptrOutToneB, outtoneB.Length);

                bool successfulCall;

                if (inProfileName.HasValue)
                {
                    char inPro = inProfileName.Value;

                    if (outProfileName.HasValue)
                    {
                        char outPro = outProfileName.Value;
                        if (Util.is64BitOperatingSystem == true)
                            successfulCall = SDK64.chcusb_setIcctable(&inPro, &outPro, (ushort)renderingIntents, (void*)ptrInToneR, (void*)ptrInToneG, (void*)ptrInToneB, (void*)ptrOutToneR, (void*)ptrOutToneG, (void*)ptrOutToneB, &resultPointer);
                        else
                            successfulCall = SDK32.chcusb_setIcctable(&inPro, &outPro, (ushort)renderingIntents, (void*)ptrInToneR, (void*)ptrInToneG, (void*)ptrInToneB, (void*)ptrOutToneR, (void*)ptrOutToneG, (void*)ptrOutToneB, &resultPointer);
                    }
                    else
                    {
                        if (Util.is64BitOperatingSystem == true)
                            successfulCall = SDK64.chcusb_setIcctable(&inPro, null, (ushort)renderingIntents, (void*)ptrInToneR, (void*)ptrInToneG, (void*)ptrInToneB, (void*)ptrOutToneR, (void*)ptrOutToneG, (void*)ptrOutToneB, &resultPointer);
                        else
                            successfulCall = SDK32.chcusb_setIcctable(&inPro, null, (ushort)renderingIntents, (void*)ptrInToneR, (void*)ptrInToneG, (void*)ptrInToneB, (void*)ptrOutToneR, (void*)ptrOutToneG, (void*)ptrOutToneB, &resultPointer);
                    }
                }
                else
                {
                    if (outProfileName.HasValue)
                    {
                        char outPro = outProfileName.Value;
                        if (Util.is64BitOperatingSystem == true)
                            successfulCall = SDK64.chcusb_setIcctable(null, &outPro, (ushort)renderingIntents, (void*)ptrInToneR, (void*)ptrInToneG, (void*)ptrInToneB, (void*)ptrOutToneR, (void*)ptrOutToneG, (void*)ptrOutToneB, &resultPointer);
                        else
                            successfulCall = SDK32.chcusb_setIcctable(null, &outPro, (ushort)renderingIntents, (void*)ptrInToneR, (void*)ptrInToneG, (void*)ptrInToneB, (void*)ptrOutToneR, (void*)ptrOutToneG, (void*)ptrOutToneB, &resultPointer);
                    }
                    else
                    {
                        if (Util.is64BitOperatingSystem == true)
                            successfulCall = SDK64.chcusb_setIcctable(null, null, (ushort)renderingIntents, (void*)ptrInToneR, (void*)ptrInToneG, (void*)ptrInToneB, (void*)ptrOutToneR, (void*)ptrOutToneG, (void*)ptrOutToneB, &resultPointer);
                        else
                            successfulCall = SDK32.chcusb_setIcctable(null, null, (ushort)renderingIntents, (void*)ptrInToneR, (void*)ptrInToneG, (void*)ptrInToneB, (void*)ptrOutToneR, (void*)ptrOutToneG, (void*)ptrOutToneB, &resultPointer);
                    }
                }
                result = resultPointer;

                Marshal.FreeHGlobal(ptrInToneR);
                Marshal.FreeHGlobal(ptrInToneG);
                Marshal.FreeHGlobal(ptrInToneB);
                Marshal.FreeHGlobal(ptrOutToneR);
                Marshal.FreeHGlobal(ptrOutToneG);
                Marshal.FreeHGlobal(ptrOutToneB);

                return successfulCall;
            }
        }
        /// <summary>
        /// Sends picture data to the printer. 
        /// </summary>
        /// <param name="imageBuff">Pointer to the output picture data. </param>
        /// <param name="result">Pointer to the word type variable for storing the executing status.</param>
        /// <returns>Returns "TRUE" when this function ends normally or "FALSE" in the other cases. </returns>
        internal static bool Write(byte[] imageBuff, out int result)
        {
            unsafe
            {
                ushort resultPointer = 0;
                int imageBuffSize = imageBuff.Length;
                IntPtr ptrImageBuff = Marshal.AllocHGlobal(Marshal.SizeOf(typeof(byte)) * imageBuffSize);
                Marshal.Copy(imageBuff, 0, ptrImageBuff, imageBuffSize);



                //MemoryStream ms = new MemoryStream(imageBuff);
                ////write to file
                //FileStream file = new FileStream("D:\\imagebuffer.bin", FileMode.Create, FileAccess.Write);
                //ms.WriteTo(file);
                //file.Close();
                //ms.Close();
                //FileStream file1 = new FileStream("d:\\imagebuffer.txt", FileMode.Create, FileAccess.Write);
                //ms.WriteTo(file);
                //file.Close();
                //ms.Close();


                bool successfulCall;
                if (Util.is64BitOperatingSystem == true)
                    successfulCall = SDK64.chcusb_write((void*)ptrImageBuff, &imageBuffSize, &resultPointer);
                else
                    successfulCall = SDK32.chcusb_write((void*)ptrImageBuff, &imageBuffSize, &resultPointer);

                result = resultPointer;
                Marshal.FreeHGlobal(ptrImageBuff);

                return successfulCall;
            }
        }


        public static Image BinaryToImage(byte[] imageBuff)
        {
            MemoryStream memStream = new MemoryStream();
            memStream.Write(imageBuff, 0, imageBuff.Length);
            return Image.FromStream(memStream);

        }
        /// <summary>
        /// Performs the page end processing. 
        /// </summary>
        /// <param name="result">Pointer to the intiger type variable for storing the executing status </param>
        /// <returns>This function always returns "TRUE." </returns>
        internal static bool EndPage(out int result)
        {
            unsafe
            {
                ushort resultPointer = 0;
                bool successfulCall;

                if (Util.is64BitOperatingSystem == true)
                    successfulCall = SDK64.chcusb_endpage(&resultPointer);
                else
                    successfulCall = SDK32.chcusb_endpage(&resultPointer);

                result = resultPointer;
                return successfulCall;
            }
        }
        /// <summary>
        /// Searches for all USB devices connected to the host computer and returns the number of printers which the CHCUSB DLL recognized. It creates a list of recognized printer serial number (8character). The serial number is set when the printer is shipped out. 
        /// </summary>
        /// <param name="result">Pointer to 128-element byte type array to store printer serial number. </param>
        /// <returns>The number of printers recognized. </returns>
        internal static bool ListupPrinterSN(out SerialNumRec[] result)
        {
            unsafe
            {
                result = new SerialNumRec[128];
                for (var i = 0; i < result.Length; i++)
                {
                    result[i].SerialNum = new byte[8];
                }
                int successfulCall;

                if (Util.is64BitOperatingSystem == true)
                    successfulCall = SDK64.chcusb_listupPrinterSN(result);
                else
                    successfulCall = SDK32.chcusb_listupPrinterSN(result);
                return (successfulCall != -1);
            }
        }
        /// <summary>
        /// Select the printer which is intended as output by the specified printer serial number (6 character). The serial number would be set when the printer is shipped out. 
        /// </summary>
        /// <param name="serialNumber">Specify the printer serial number. </param>
        /// <param name="result">Pointer to intiger type variable that stores the executing status </param>
        /// <returns>Specified Printer serial number. Returns a currently-selected printer’s serial number when " NULL" is specified in "printerSN." (Returns "NULL" when no printer is specified.) </returns>
        internal static IntPtr SelectPrinterSN(string serialNumber, out int result)
        {
            unsafe
            {
                //ushort resultPointer = 0;
                //var serialNumRec = new SerialNumRec {SerialNum = System.Text.Encoding.ASCII.GetBytes(serialNumber)};
                //var structPtr = Marshal.AllocHGlobal(Marshal.SizeOf(serialNumRec));
                //Marshal.StructureToPtr(serialNumRec, structPtr, false);
                //var serialNumPtr = chcusb_selectPrinterSN(structPtr, &resultPointer);
                //result = resultPointer;
                //Marshal.FreeHGlobal(structPtr);
                //return serialNumPtr;

                ushort resultPointer = 0;
                byte[] serialNumRec = System.Text.Encoding.ASCII.GetBytes(serialNumber);
                fixed (byte* structPtr = serialNumRec)
                {
                    IntPtr serialNumPtr;
                    if (Util.is64BitOperatingSystem == true)
                        serialNumPtr = SDK64.chcusb_selectPrinterSN(new IntPtr(structPtr), &resultPointer);
                    else
                        serialNumPtr = SDK32.chcusb_selectPrinterSN(new IntPtr(structPtr), &resultPointer);

                    result = resultPointer;
                    return serialNumPtr;
                }
            }
        }
        /// <summary>
        /// Set line to cut. 
        /// </summary>
        /// <param name="cutList">Pointer to cut line information </param>
        /// <param name="result">Pointer to a intiger type variable for storing the executing status. </param>
        /// <returns>Returns "TRUE" when the function ends normally or "FALSE" in the other cases. </returns>
        internal static bool SetCutList(byte[] cutList, out int result)
        {
            result = 0;
            bool success = false;
            unsafe
            {
                // if cutList is null, pList will be null
                fixed (byte* pList = cutList)
                {
                    ushort resultPointer = 0;

                    if (Util.is64BitOperatingSystem == true)
                        success = SDK64.chcusb_setCutList(new IntPtr(pList), &resultPointer);
                    else
                        success = SDK32.chcusb_setCutList(new IntPtr(pList), &resultPointer);
                    result = resultPointer;
                }
            }
            return success;
        }
        #endregion
        #region const



        // The number of the USB device maximum connection
        private const int MAX_USB_DEVICE = 128;

        private const int DEFAULT_BUFFER_SIZE = 30;

        // number definitions for chcusb_imageformat
        private const int kFORMAT_PIXEL_RGB = 3;			// RGB point sequential format
        private const int kFORMAT_PIXEL_BGR = 4;			// BGR point sequential format
        private const int kCOMPONENT_RGB = 3;	    		// Red, green, and blue colors (3 colors)

        // number definitions for chcusb_setIcctable
        private const int kICC_RI_Perceptual = 0;           // Perceptual
        private const int kICC_RI_RelativeColorimetric = 1; // Relative Colorimetric
        private const int kICC_RI_Saturation = 2;           // Saturation

        // number definitions for chcusb_setPrinterToneCurve
        private const int kSetPrinterTCTbl_StandardTbl = 0;
        private const int kSetPrinterTCTbl_UserTbl = 1;
        private const int kSetPrinterTCTbl_CurrentTbl = 2;

        // number definitions for chcusb_getPrinterToneCurve
        private const int kGetPrinterTCTbl_UserTbl = 1;
        private const int kGetPrinterTCTbl_CurrentTbl = 2;

        private const int kSetPrinterTCTbl_HighSpeed_1 = 0x01;	    // High Speed Slot 1
        private const int kSetPrinterTCTbl_HighSpeed_2 = 0x02;	    // High Speed Slot 2
        private const int kSetPrinterTCTbl_HighSpeed_3 = 0x03;	    // High Speed Slot 3
        private const int kSetPrinterTCTbl_HighSpeed_4 = 0x04;	    // High Speed Slot 4
        private const int SetPrinterTCTbl_HighSpeed_5 = 0x05;	    // High Speed Slot 5
        private const int kSetPrinterTCTbl_HighSpeed_6 = 0x06;      // High Speed Slot 6
        private const int kSetPrinterTCTbl_HighQuality_1 = 0x21;	// High Quality Slot 1
        private const int kSetPrinterTCTbl_HighQuality_2 = 0x22;	// High Quality Slot 2
        private const int kSetPrinterTCTbl_HighQuality_3 = 0x23;	// High Quality Slot 3
        private const int kSetPrinterTCTbl_HighQuality_4 = 0x24;	// High Quality Slot 4
        private const int kSetPrinterTCTbl_HighQuality_5 = 0x25;    // High Quality Slot 5
        private const int SetPrinterTCTbl_HighQuality_6 = 0x26;     // High Quality Slot 6
        private const int kSetPrinterTCTbl_Panorama_1 = 0x41;       // Panoramic Print Slot 1
        private const int kSetPrinterTCTbl_Panorama_2 = 0x42;       // Panoramic Print Slot 2
        private const int kSetPrinterTCTbl_Panorama_3 = 0x43;       // Panoramic Print Slot 3
        private const int kSetPrinterTCTbl_Panorama_4 = 0x44;       // Panoramic Print Slot 4
        private const int kSetPrinterTCTbl_Panorama_5 = 0x45;       // Panoramic Print Slot 5
        private const int kSetPrinterTCTbl_Panorama_6 = 0x46;       // Panoramic Print Slot 6

        // printer toneCurve table size
        private const int kSIZEOF_PRINTER_TONETBL = 256 * 3;  // (IN)256step * 3colors(YMC)

        #endregion
    }
}
