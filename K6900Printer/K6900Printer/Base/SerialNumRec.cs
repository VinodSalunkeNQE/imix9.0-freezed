﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace K6900Printer.Base
{
    /// <summary>
    /// Authors Name: Kailash Changed Behera
    /// Created Date: 06 Aug 2019
    /// Description :Used to store attached printer serial numbers.
    /// </summary>
    [StructLayout(LayoutKind.Sequential)]
    internal struct SerialNumRec
    {
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 8)]
        internal byte[] SerialNum;
    }
}
