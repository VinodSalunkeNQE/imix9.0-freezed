﻿using K6900Printer.Base;
using K6900Printer.Entity;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace K6900Printer
{
    /// <summary>
    /// Authors Name: Kailash Changed Behera
    /// Created Date: 06 Aug 2019
    /// Description :Used to access KODAK 6900 PRINTER.
    /// </summary>
    public class K6900Driver : IK6900Driver
    {
        private readonly object _lock = new object();
        private const int _lockTimeout = 500;
        public K6900Driver()
        {

        }
        public string Description
        {
            get { return "K6900"; }
        }
        public bool GetStatus(out int result)
        {
            var success = false;
            result = -1;

            try
            {
                lock (_lock)
                {
                    if (SDK.Open(out result))
                    {
                        success = SDK.PrinterStatus(out result);
                    }
                }
            }
            catch
            {
                result = -1;
                success = false;
            }
            finally
            {
                SDK.Close();
            }
            return success;
        }

        public bool GetStatusAll(out List<PrinterStatus> result)
        {
            result = null;
            return false;
        }

        public bool GetStatus(string serialNumber, out int result)
        {
            System.Diagnostics.Debug.WriteLine("GetStatus in");
            var success = false;
            result = -1;

            try
            {
                lock (_lock)
                {
                    if (SDK.Open(out result))
                    {
                        if (!SelectPrinter(serialNumber)) return false;
                        success = SDK.PrinterStatus(out result);
                    }
                }
            }
            catch
            {
                result = -1;
                success = false;
            }
            finally
            {
                SDK.Close();
                System.Diagnostics.Debug.WriteLine("GetStatus out");
            }
            return success;
        }

        public bool GetUsbInfo(out UsbInfo result)
        {
            var success = false;
            result = null;
            try
            {
                lock (_lock)
                {
                    int openResult;
                    if (SDK.Open(out openResult))
                    {
                        byte[] buffer;
                        success = SDK.GetPrinterInfo(PINF_Tag.USBINQ, PINF_Tag_Size_Get.USBINQ, out buffer);
                        if (success)
                        {
                            result = new UsbInfo
                            {
                                VendorId = BitConverter.ToInt16(buffer, 0).ToString("X4"),
                                ProductId = BitConverter.ToInt16(buffer, 2).ToString("X4"),
                                Description = Encoding.ASCII.GetString(buffer, 4, buffer.Length - 4).TrimEnd(new char[] { '\0' })
                            };
                        }
                    }
                }
            }
            catch
            {
                result = null;
                success = false;
            }
            finally
            {
                SDK.Close();
            }
            return success;
        }

        public bool GetPaperInfo(out List<PaperInfo> result)
        {
            result = null;
            return false;
        }

        public bool GetSerialNumbers(out string[] result)
        {
            var success = false;
            result = null;
            try
            {
                lock (_lock)
                {
                    int openResult;
                    if (SDK.Open(out openResult))
                    {
                        SerialNumRec[] serialNumberArray;
                        success = SDK.ListupPrinterSN(out serialNumberArray);

                        var validSerialNumbers = new List<string>();
                        foreach (var serialNumRec in serialNumberArray)
                        {
                            bool valid = serialNumRec.SerialNum.Any(b => b != 255);
                            if (!valid) continue;
                            string convertedSN = System.Text.Encoding.ASCII.GetString(serialNumRec.SerialNum);
                            validSerialNumbers.Add(convertedSN);
                        }

                        result = validSerialNumbers.ToArray();
                    }
                }
            }
            catch
            {
                result = null;
                success = false;
            }
            finally
            {
                SDK.Close();
            }
            return success;
        }

        public IntPtr SelectPrinter(string serialNumber, out int result)
        {
            var success = IntPtr.Zero;
            result = -1;

            try
            {
                lock (_lock)
                {
                    int openResult;
                    if (SDK.Open(out openResult))
                    {
                        var serialNumberPtr = SDK.SelectPrinterSN(serialNumber, out result);
                        success = serialNumberPtr;
                    }
                }
            }
            catch
            {
                result = -1;
                success = IntPtr.Zero;
            }
            finally
            {
                SDK.Close();
            }
            return success;
        }

        public bool GetEngineInfo(out EngineInfo result)
        {
            result = null;
            var success = false;
            try
            {
                lock (_lock)
                {
                    int openResult;
                    if (SDK.Open(out openResult))
                    {
                        result = GetEngineInfo();
                        success = true;
                    }
                }
            }
            catch
            {
                result = null;
                success = false;
            }
            finally
            {
                SDK.Close();
            }
            return success;
        }

        private EngineInfo GetEngineInfo()
        {
            EngineInfo result = null;
            byte[] buffer;
            var success = SDK.GetPrinterInfo(PINF_Tag.ENGID, PINF_Tag_Size_Get.ENGID, out buffer);
            if (success)
            {
                result = new EngineInfo()
                {
                    MainBootVer = BitConverter.ToInt16(buffer, 0),
                    MainControlVer = BitConverter.ToInt16(buffer, 2),
                    InputBootVer = BitConverter.ToInt16(buffer, 4),
                    InputControlVer = BitConverter.ToInt16(buffer, 6),
                    SerialNumber = System.Text.Encoding.ASCII.GetString(buffer, 8, 8).TrimEnd(new char[] { '\0' })
                };
            }
            return result;
        }

        public bool GetPrintCountInfo(string serialNumber, out PrintCountInfo result)
        {
            result = null;
            var success = false;

            try
            {
                lock (_lock)
                {
                    int openResult;
                    if (SDK.Open(out openResult))
                    {
                        if (!SelectPrinter(serialNumber)) return false;
                        result = GetPrintCountInfo();
                    }
                    success = true;
                }
            }
            catch
            {
                result = null;
                success = false;
            }
            finally
            {
                SDK.Close();
            }
            return success;
        }

        private PrintCountInfo GetPrintCountInfo()
        {
            PrintCountInfo result = null;
            byte[] buffer;
            var success = SDK.GetPrinterInfo(PINF_Tag.PRINTCNT, PINF_Tag_Size_Get.PRINTCNT, out buffer);
            if (success)
            {
                result = new PrintCountInfo()
                {
                    Permanent = BitConverter.ToInt32(buffer, 0),
                    Maintenance = BitConverter.ToInt32(buffer, 4),
                    AfterMediumReplacement = BitConverter.ToInt32(buffer, 8),
                    Cutter = BitConverter.ToInt32(buffer, 12),
                    RemainingMediuam = BitConverter.ToInt32(buffer, 16)
                };
            }
            return result;
        }

        public bool GetCompletedPageIds(out List<PrintInfo> result)
        {
            return GetCompletedPageIds(null, out result);
        }

        public bool GetCompletedPageIds(string serialNumber, out List<PrintInfo> result)
        {
            result = new List<PrintInfo>();
            var success = false;
            try
            {
                lock (_lock)
                {
                    int openResult;
                    if (SDK.Open(out openResult))
                    {
                        if (!SelectPrinter(serialNumber)) return false;
                        byte[] buffer;
                        success = SDK.GetPrinterInfo(PINF_Tag.MEMORY, PINF_Tag_Size_Get.MEMORY, out buffer);
                        if (success)
                        {
                            for (var i = 0; i < 2; i++)
                            {
                                var offset = i;
                                offset *= 9;

                                var pageIdWord = BitConverter.ToInt16(buffer, offset + 0);
                                var printsLeft = BitConverter.ToInt16(buffer, offset + 2);
                                var completed = BitConverter.ToInt16(buffer, offset + 4);
                                var specified = BitConverter.ToInt16(buffer, offset + 6);
                                var bankStatus = buffer[offset + 7];

                                if (pageIdWord == 0) continue;
                                int pageId;
                                int printerId;
                                Util.GetIdsFromWord(pageIdWord, out pageId, out printerId);
                                if (pageId == 0) continue;
                                if (printsLeft > 0) continue;
                                if (completed != specified) continue; ;

                                var printInfo = new PrintInfo
                                {
                                    SerialNumber = serialNumber,
                                    CheckedOn = DateTime.Now,
                                    PageId = pageId,
                                    Left = printsLeft,
                                    Completed = completed,
                                    Specified = specified
                                };
                                result.Add(printInfo);
                            }
                        }
                    }
                }
            }
            catch
            {
                result = null;
                success = false;
            }
            finally
            {
                SDK.Close();
            }
            return success;
        }

        public bool Print(Image img, PrintSize printSize, int numberOfCopies, out int result, out int pageId, string inputProfile = "", string outputProfile = "")
        {
            if (!Directory.Exists("@C:\\Temp)"))
                Directory.CreateDirectory("@C:\\Temp)");

            Guid guid = new Guid();
            string filename = "@C:\\Temp\\" + guid + ".jpg";
            img.Save(filename);
            if (Print(filename, printSize, numberOfCopies, out result, out pageId, inputProfile, outputProfile))
            {
                File.Delete(filename);
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool Print(string filename, PrintSize printSize, int numberOfCopies, out int result, out int pageId, string inputProfile = "", string outputProfile = "")
        {
            result = 0;
            pageId = 0;
            int printerId = 0;
            bool success = false;

            success = Print(null, filename, printSize, numberOfCopies, out result, out pageId, out printerId, inputProfile, outputProfile);

            return success;
        }

        public bool Print(string serialNumber, string filename, PrintSize printSize, int numberOfCopies, out int result, out int pageId, out int printerId, string inputProfile = "", string outputProfile = "")
        {

            result = 0;
            pageId = 0;
            printerId = 0;

            try
            {
#if DEBUG
                System.Diagnostics.Debug.WriteLine("Open");
#endif
                if (!SDK.Open(out result))
                {
                    return false;
                }

                if (serialNumber != null)
                {
#if DEBUG
                    System.Diagnostics.Debug.WriteLine("SelectPrinter");
#endif
                    if (!SelectPrinter(serialNumber))
                    {
                        return false;
                    }
                }

                // comments from KODAK 6900 Digital Photo Printer  (CHC-S2245-6A) CHCUSB DLL function specifications 
                // 6.6.4. Printing sequence 

                // (1) Call the "chcusb_status" function and make sure the selected printer is ready 
#if DEBUG
                System.Diagnostics.Debug.WriteLine("Status");
#endif
                //Commented as suggested by Kodak team
                //if (!SDK.PrinterStatus(out result))
                //{
                //    return false;
                //}
                //End
                bool bQuit = false;
                do
                {
                    if (!SDK.PrinterStatus(out result))
                    {
                        Console.WriteLine("Status result:{0}", result);
                        if (IgnoreError(result))
                        {
                            // Can ignore so sleep for 1 second 
                            System.Threading.Thread.Sleep(1000);

                        }
                        else
                        {
                            return false;
                        }
                    }
                    else
                    {
                        // No error
                        bQuit = true;
                    }
                } while (bQuit == false);
                // (2) Set the printer paper information (page size, etc.) 
                // by the "chcusb_setPrinterInfo" function. 
                int printWidth = 1844;
                int printHeight = 1240;
                int printMediaType = 1;

                switch (printSize)
                {
                    case PrintSize.FourBySix:
                        printWidth = 1844;
                        printHeight = 1240;
                        printMediaType = 1;
                        break;
                    case PrintSize.FiveBySeven:
                        printWidth = 1548;
                        printHeight = 2140;
                        printMediaType = 2;
                        break;
                    case PrintSize.SixByEight:
                        printWidth = 1844;
                        printHeight = 2434;
                        printMediaType = 3;
                        break;
                    case PrintSize.SixByFourteenPanoramic:
                        printWidth = 1844;
                        printHeight = 4240;
                        printMediaType = 3;
                        break;
                    case PrintSize.SixByTwentyPanoramic:
                        printWidth = 1844;
                        printHeight = 6040;
                        printMediaType = 3;
                        break;
                    default:
                        break;
                }

                byte[] array = new byte[(int)PINF_Tag_Size_Set.PAPER];
                array[0] = (byte)printMediaType;        // Media Type Code, 0 = Normal print, 2 = SDK Combo (4x6-2up)
                array[1] = (byte)printWidth;            // Width of the print area( in dots )
                array[2] = (byte)(printWidth >> 8);
                array[3] = (byte)printHeight;           // Height of the print area( in dots )
                array[4] = (byte)(printHeight >> 8);
                array[5] = (byte)0x00;                  // reserved
                array[6] = 0x00;                        // Print Type( 0x00 Fixation )
                array[7] = (byte)0x00;                  // reserved
                array[8] = (byte)0x00;                  // reserved
                array[9] = (byte)0x00;                  // reserved

#if DEBUG
                System.Diagnostics.Debug.WriteLine("PAPER");
#endif
                if (!SDK.SetPrinterInfo(PINF_Tag.PAPER, array, out result))
                {
                    return false;
                }

                // (3) Specify the laminate mode
                array = new byte[1];
                array[0] = (byte)0x01;
#if DEBUG
                System.Diagnostics.Debug.WriteLine("LAMINATE");
#endif
                if (!SDK.SetPrinterInfo(PINF_Tag.LAMINATE_MODE, array, out result))
                {
                    return false;
                }

                //// (optional) Set the Quality mode
                //array = new byte[1];
                //array[0] = (byte)0x00;
                //if (!K6900.SetPrinterInfo(K6900.PINF_Tag.QUALITY_MODE, array, out result))
                //{
                //    return false;
                //}

                // (optional?) other KPPHelper derived classes call PrinterStatus here
#if DEBUG
                System.Diagnostics.Debug.WriteLine("Status");
#endif
                //Commented as suggested by Kodak team
                //if (!SDK.PrinterStatus(out result))
                //{
                //    return false;
                //}
                //END
                bQuit = false;
                do
                {
                    if (!SDK.PrinterStatus(out result))
                    {
                        Console.WriteLine("Status result:{0}", result);
                        if (IgnoreError(result))
                        {
                            // Can ignore so sleep for 1 second 
                            Thread.Sleep(1000);

                        }
                        else
                        {

                            return false;
                        }
                    }
                    else
                    {
                        // No error
                        bQuit = true;
                    }
                } while (bQuit == false);
                // (4) Set the printer paper information (page size, etc.) by the "chcusb_setPrinterInfo" function.
                PixelFormat rgbbgr = PixelFormat.RGB;
                int comp = 3;
#if DEBUG
                System.Diagnostics.Debug.WriteLine("ImageFormat");
#endif
                if (!SDK.ImageFormat(rgbbgr, comp, 8, printWidth, printHeight, out result))
                {
                    return false;
                }

                // (5) Specify the input profile, the output profile, the input tone curve 
                // and the output tone curve for color matching by the "chcusb_setIccTable" function. 
                byte[] intoneR;
                byte[] intoneG;
                byte[] intoneB;
                byte[] outtoneR;
                byte[] outtoneG;
                byte[] outtoneB;

                SDK.MakeGamma(100, out intoneR, out intoneG, out intoneB);
                SDK.MakeGamma(100, out outtoneR, out outtoneG, out outtoneB);

                int rIntents = 0;
#if DEBUG
                System.Diagnostics.Debug.WriteLine("Icctable");
#endif
                if (!SDK.SetIcctable(null, null, rIntents, intoneR, intoneG, intoneB, outtoneR, outtoneG, outtoneB, out result))
                {
                    return false;
                }

                // (6) Set the profile emphasis/smoothing (MTF) parameters by the "chcusb_setmtf" function. 

                // (7) Set the number of prints for the transferring picture by chcusb_copies function. 
#if DEBUG
                System.Diagnostics.Debug.WriteLine("Copies");
#endif
                if (!SDK.Copies(numberOfCopies, out result))
                {
                    return false;
                }

                // (8) Execure the “chcusb_setCutList” function to set the line to cut. 

                //int top = 18;
                //int bottom = printHeight - 18;
                //array = new byte[33];
                //array[0] = (byte)0x02;
                //array[1] = (byte)top;
                //array[2] = (byte)(top >> 8);
                //array[3] = (byte)bottom;
                //array[4] = (byte)(bottom >> 8);
                //for (int n = 5; n < 33; n++) array[n] = 0x00;

                // instead of above, send null to tell Printer to cut paper on Default line.
                array = null;
                if (!SDK.SetCutList(array, out result))
                {
                    return false;
                }

                // (9) Execute the "chcusb_startpage" function 
                // and send the picture header and the print parameters.  
#if DEBUG
                System.Diagnostics.Debug.WriteLine("StartPage");
#endif
                //Commented as suggested by Kodak team
                //if (!SDK.StartPage(out pageId, out result))
                //{
                //    return false;
                //}
                //End
                bQuit = false;
                do
                {
                    if (!SDK.StartPage(out pageId, out result))
                    {
                        Console.WriteLine("StartPage result:{0}", result);
                        if (IgnoreError(result))
                        {
                            // Can ignore so sleep for 1 second 
                            System.Threading.Thread.Sleep(1000);

                        }
                        else
                        {
                            return false;
                        }
                    }
                    else
                    {
                        // No error
                        bQuit = true;
                    }
                } while (bQuit == false);

                if (serialNumber != null)
                {
                    // get printer ID (upper byte) and page ID (lower byte)
                    Util.GetIdsFromWord(pageId, out pageId, out printerId);
                }

                // 
                byte[] imgbytes = Util.ResizeApplyProfile(filename, printWidth, printHeight, inputProfile, outputProfile);
                Array.Reverse(imgbytes);

                #region ADDED 

                Bitmap bmp = new Bitmap(filename);
                bmp.RotateFlip(RotateFlipType.RotateNoneFlipY);
                MemoryStream ms = new MemoryStream();
                bmp.Save(ms, ImageFormat.Bmp);
                byte[] b = ms.ToArray();
                Array.Copy(b, 54, b, 0, b.Length - 54);             

                byte[] jpegBytes = ConvertBytestoJpegBytes(b, bmp.Width, bmp.Height);
                File.WriteAllBytes(@"D:\Temp\TestForest.jpg", jpegBytes);
                imgbytes = null;
                imgbytes = jpegBytes;
                printWidth = bmp.Width;
                printHeight = bmp.Height;
                #endregion



                byte[] imgbytesNoAlpha = new byte[(printWidth * printHeight) * 3];

                try
                {
                    int lineLengthOrig = (printWidth * 4);
                    int lineLengthNoAlpha = (printWidth * 3);
                    int index = 0;
                    for (int i = 0; i < imgbytes.Length; i++)
                    {
                        if (i % 4 != 0 && i < (printWidth * printHeight * 4))
                        {
                            switch (rgbbgr)
                            {
                                case PixelFormat.BGR:
                                    imgbytesNoAlpha[index] = imgbytes[i];
                                    index++;
                                    break;
                                case PixelFormat.RGB:
                                default:
                                    int row = (int)Math.Ceiling((double)i / (double)lineLengthOrig);
                                    int endOfCurrentLine = (row * lineLengthNoAlpha) - 1;
                                    int pixelOnRow = (((int)Math.Ceiling((i % lineLengthOrig) / (double)4)) * 3);
                                    int rgbOffset = (3 - (i % 4));
                                    index = endOfCurrentLine - pixelOnRow + 3 - rgbOffset;
                                    imgbytesNoAlpha[index] = imgbytes[i];
                                    break;
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    string error = ex.ToString();
                }

                // (10) Execute the "chcusb_write" function to send picture data. 
#if DEBUG
                System.Diagnostics.Debug.WriteLine("Write");
#endif
                if (!SDK.Write(imgbytesNoAlpha, out result))
                //if (!SDK.Write(imgbytes, out result))
                {
                    return false;
                }

                // (11) Execute the "chcusb_endpage" function to end page processing. 
#if DEBUG
                System.Diagnostics.Debug.WriteLine("End");
#endif
                if (!SDK.EndPage(out result))
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
#if DEBUG
                System.Diagnostics.Debug.WriteLine(ex.Message);
#endif
            }
            finally
            {
                SDK.Close();
            }
            return true;
        }

        private string ConvertStatusEnum(Status status)
        {
            switch (status)
            {
                //-----------------------------------------------------------------------
                //	Executing status
                //-----------------------------------------------------------------------
                //case K6900.Status.NOERROR:
                //    return "Normal (\"True\" is returned.)";
                case Status.MEMFULLERR:
                    return "Memory Full";
                case Status.USBNOTFOUND:
                    return "Cannot load UsbCtrl.DLL.";
                case Status.ICCNOTFOUND:
                    return "Cannot load ColorMatch.DLL.";
                case Status.OPENPORT:
                    return "Cannot open the USB port.";
                case Status.FILEACCESS:
                    return "Cannot access the specified profile.";
                case Status.PRMERROR:
                    return "The specified parameter is not available.";
                case Status.NOTFOUND:
                    return "Printer with selected number is not found.";
                case Status.IPNOTFOUND:
                    return "Cannot load IP.DLL.";
                case Status.IPUNMATCH:
                    return "IP DLL version unmatch.";

                case Status.MUTEX_Wait:
                    return "Printer is used by another thread.";
                case Status.THREAD_MAX:
                    return "Cannot generate new thread.";
                case Status.THREAD_Used:
                    return "Now thread is loading.";
                case Status.NOTCONNECT:
                    return "The printer cannot be recognized.";
                case Status.Unknow:
                    return "Undefined error";

                //------------------------------------------------------------------------------------------------------
                // USB communication failure factor
                case Status.USB_CommErr:
                    return "USB communication error.";

                //------------------------------------------------------------------------------------------------------
                // Printer status
                case Status.READY:
                    return "Printer is ready.";
                case Status.BUSY:
                    return "Buffer full.";
                case Status.MainCpuInitialize:
                    return "Initializing MAIN CPU.";
                case Status.RibbonInitialize:
                    return "Initializing Ribbon.";
                case Status.PaperLoading:
                    return "loading Paper Process.";
                case Status.ThermalProtect:
                    return "Thermal protect.";
                case Status.Operation:
                    return "Using Operation panel.";
                case Status.Selfdiagnosis:
                    return "Processing Self-Diagnostics.";
                case Status.DownLoading:
                    return "Processing download.";

                case Status.PaperStartPosition:
                    return "Feeding paper (at print start position).";
                case Status.Heat:
                    return "Processing pre-heat.";
                case Status.Y_Printing:
                    return "Printing Yellow.";
                case Status.Y_PaperReturn:
                    return "Paper back feeding after printing Yellow.";
                case Status.M_Printing:
                    return "Printing Magenta.";
                case Status.M_PaperReturn:
                    return "Paper back feeding after printing Magenta.";
                case Status.C_Printing:
                    return "printing Cyan.";
                case Status.C_PaperReturn:
                    return "Paper back feeding after printing Cyan.";
                case Status.OP_Printing:
                    return "Printing OP.";
                case Status.CutPositionLoading:
                    return "Feeding paper (at paper cut position).";
                case Status.PaperEjecting:
                    return "Processing paper ejection.";
                case Status.WaitPositionReturn:
                    return "Paper back feeding (at ready position).";
                case Status.PrintingComplete:
                    return "Finish printing.";
                case Status.NoPrinting:
                    return "No printing.";
                case Status.UNKNOWNPAPER:
                    return "The specified paper information is not available to the currently selected printer.";

                case Status.NoPaper:
                    return "Paper is not set.";
                case Status.OutOfPaper:
                    return "Out of Paper.";
                case Status.NoRibbon:
                    return "Ribbon is not set.";
                case Status.OutOfRibbon:
                    return "Out of Ribbon";
                case Status.IncorrectRibbon:
                    return "Incorrect ribbon.";
                case Status.DoorOpen:
                    return "Door open.";
                case Status.DoorCloseErr:
                    return "Door close error.";
                case Status.RibbonErrRewind:
                    return "Ribbon rewinding error.";
                case Status.RibbonErrSensing:
                    return "Ribbon sensing error.";
                case Status.ThvTuningErr:
                    return "THV tuning error.";
                case Status.ThvUntuned:
                    return "THV untuned.";
                case Status.SHORTAGE_MEMORY:
                    return "Memory shortage (Panoramic).";
                case Status.SHORTAGE_RIBBONV:
                    return "Ribbon shortage (Panoramic).";

                case Status.PaperJamError01:
                case Status.PaperJamError02:
                    return "Errror when the printer tried to begin paper transporting.";
                case Status.PaperJamError16:
                case Status.PaperJamError17:
                case Status.PaperJamError18:
                case Status.PaperJamError19:
                    return "Errror when the printer is transporting paper.";
                case Status.PaperJamError48:
                case Status.PaperJamError49:
                case Status.PaperJamError50:
                    return "Error when the printer is printing.";
                case Status.PaperJamError64:
                case Status.PaperJamError65:
                    return "Error when the printer is cutting paper.";
                case Status.PaperJamError81:
                case Status.PaperJamError82:
                case Status.PaperJamError83:
                    return "Error when the printer is idle state.";

                case Status.ControlError01:
                    return "EEPROM write error.";
                case Status.ControlError10:
                    return "Invalid Print Parameter TABLE.";
                case Status.ControlError12:
                    return "Print Parameter TABLE version mismatch.";
                case Status.ControlError13:
                    return "FPGA configuration failure.";
                case Status.ControlError15:
                    return "MAIN firmware data error.";
                case Status.ControlError16:
                    return "MAIN firmware writing error.";
                case Status.ControlError19:
                    return "Print Parameter TABLE checksum error.";
                case Status.ControlError20:
                    return "Print Parameter TABLE writing error.";
                case Status.ControlError21:
                    return "User Tone Curve write error.";
                case Status.ControlError22:
                    return "MAIN-MSP communication error.";
                case Status.ControlError25:
                    return "Thermal head power error.";

                case Status.MechanicalError01:
                    return "Error of Head/ Pinch Roller driving (home).";
                case Status.MechanicalError02:
                    return "Error of Head/ Pinch Roller driving (mode-1).";
                case Status.MechanicalError03:
                    return "Error of Head/ Pinch Roller driving (mode-2).";
                case Status.MechanicalError04:
                    return "Error of Head/ Pinch Roller driving (mode-3).";
                case Status.MechanicalError05:
                    return "Error of Head/ Pinch Roller driving (mode-4).";
                case Status.MechanicalError20:
                    return "Error of Cutter driving (L to R).";
                case Status.MechanicalError21:
                    return "Error of Cutter driving (R to L).";

                case Status.SensorError01:
                    return "Error of Cutter Sensors.";
                case Status.SensorError02:
                    return "Error of Left Cutter Sensor after drove.";
                case Status.SensorError03:
                    return "Error of Right Cutter Sensor after drove.";
                case Status.SensorError30:
                    return "Error of Left Front-Lock Sensor.";
                case Status.SensorError31:
                    return "Error of Right Front-Lock Sensor.";
                case Status.SensorError32:
                    return "Error of Left Cutter Sensor.";
                case Status.SensorError33:
                    return "Error of Right Cutter Sensor.";
                case Status.SensorError34:
                    return "Error of Left Head/ Pinch Roller Sensor.";
                case Status.SensorError35:
                    return "Error of Right Head/ Pinch Roller Sensor.";
                case Status.SensorError36:
                    return "Error of Head/ Pinch Roller Encoder.";
                case Status.SensorError37:
                    return "Error of Supply Ribbon Encoder.";

                case Status.TemperatureError01:
                    return "Error of the thermal print head thermistor (low temperature).";
                case Status.TemperatureError02:
                    return "Error of the thermal print head thermistor (high temperature).";
                case Status.TemperatureError05:
                    return "Error of the printer thermistor (low temperature).";
                case Status.TemperatureError06:
                    return "Error of the printer thermistor (high temperature).";
                case Status.TemperatureError07:
                    return "Error of the preheat process.";
                case Status.TemperatureError08:
                    return "Error of the thermal-protect process.";

                default:
                    return String.Format("Status={0}", status);
            }
        }

        public StatusInfo GetStatusInfo(int statusCode)
        {
            var status = (Status)Enum.Parse(typeof(Status), statusCode.ToString());
            var highLevelStatusCode = -1;
            string statusMessage = null;

            switch (status)
            {
                case Status.READY:
                    highLevelStatusCode = (int)HighLevelStatusCode.Ready;
                    statusMessage = "Ready";
                    break;
                case Status.MEMFULLERR:
                case Status.USBNOTFOUND:
                case Status.ICCNOTFOUND:
                case Status.OPENPORT:
                case Status.FILEACCESS:
                case Status.PRMERROR:
                case Status.NOTFOUND:
                case Status.IPNOTFOUND:
                case Status.IPUNMATCH:
                case Status.THREAD_MAX:
                case Status.THREAD_Used:
                case Status.NOTCONNECT:
                    highLevelStatusCode = (int)HighLevelStatusCode.Error;
                    statusMessage = "Printer Error";
                    break;
                case Status.MUTEX_Wait:
                    highLevelStatusCode = (int)HighLevelStatusCode.Busy;
                    statusMessage = "Printer Busy";
                    break;
                case Status.USB_CommErr:
                    highLevelStatusCode = (int)HighLevelStatusCode.Error;
                    statusMessage = "USB Error";
                    break;
                case Status.BUSY:
                    highLevelStatusCode = (int)HighLevelStatusCode.Busy;
                    statusMessage = "Printer Busy";
                    break;
                case Status.MainCpuInitialize:
                    highLevelStatusCode = (int)HighLevelStatusCode.Busy;
                    statusMessage = "Initializing MAIN CPU";
                    break;
                case Status.RibbonInitialize:
                    highLevelStatusCode = (int)HighLevelStatusCode.Busy;
                    statusMessage = "Initializing Ribbon";
                    break;
                case Status.PaperLoading:
                    highLevelStatusCode = (int)HighLevelStatusCode.Busy;
                    statusMessage = "Loading Paper Process";
                    break;
                case Status.Operation:
                    highLevelStatusCode = (int)HighLevelStatusCode.Error;
                    statusMessage = "Operator Panel In Use";
                    break;

                case Status.ThermalProtect:
                case Status.Selfdiagnosis:
                case Status.DownLoading:

                case Status.PaperStartPosition:
                case Status.Heat:
                case Status.Y_Printing:
                case Status.Y_PaperReturn:
                case Status.M_Printing:
                case Status.M_PaperReturn:
                case Status.C_Printing:
                case Status.C_PaperReturn:
                case Status.OP_Printing:
                case Status.CutPositionLoading:
                case Status.PaperEjecting:
                case Status.WaitPositionReturn:
                case Status.PrintingComplete:
                case Status.NoPrinting:
                    highLevelStatusCode = (int)HighLevelStatusCode.Ready;
                    statusMessage = "Processing";
                    break;

                case Status.UNKNOWNPAPER:
                case Status.NoPaper:
                case Status.OutOfPaper:
                    highLevelStatusCode = (int)HighLevelStatusCode.Error;
                    statusMessage = "Paper Error";
                    break;
                case Status.NoRibbon:
                case Status.OutOfRibbon:
                case Status.IncorrectRibbon:
                case Status.RibbonErrRewind:
                case Status.RibbonErrSensing:
                    highLevelStatusCode = (int)HighLevelStatusCode.Error;
                    statusMessage = "Ribbon Error";
                    break;

                case Status.ControlError01:
                case Status.ControlError10:
                case Status.ControlError12:
                case Status.ControlError13:
                case Status.ControlError15:
                case Status.ControlError16:
                case Status.ControlError19:
                case Status.ControlError20:
                case Status.ControlError21:
                case Status.ControlError22:
                case Status.ControlError25:
                    highLevelStatusCode = (int)HighLevelStatusCode.Error;
                    statusMessage = "Control Error";
                    break;

                case Status.MechanicalError01:
                case Status.MechanicalError02:
                case Status.MechanicalError03:
                case Status.MechanicalError04:
                case Status.MechanicalError05:
                case Status.MechanicalError20:
                case Status.MechanicalError21:
                    highLevelStatusCode = (int)HighLevelStatusCode.Error;
                    statusMessage = "Mechanical Error";
                    break;

                case Status.SensorError01:
                case Status.SensorError02:
                case Status.SensorError03:
                case Status.SensorError30:
                case Status.SensorError31:
                case Status.SensorError32:
                case Status.SensorError33:
                case Status.SensorError34:
                case Status.SensorError35:
                case Status.SensorError36:
                case Status.SensorError37:
                    highLevelStatusCode = (int)HighLevelStatusCode.Error;
                    statusMessage = "Sensor Error";
                    break;

                case Status.PaperJamError01:
                case Status.PaperJamError02:
                case Status.PaperJamError16:
                case Status.PaperJamError17:
                case Status.PaperJamError18:
                case Status.PaperJamError19:
                case Status.PaperJamError48:
                case Status.PaperJamError49:
                case Status.PaperJamError50:
                case Status.PaperJamError64:
                case Status.PaperJamError65:
                case Status.PaperJamError81:
                case Status.PaperJamError82:
                case Status.PaperJamError83:
                    highLevelStatusCode = (int)HighLevelStatusCode.Error;
                    statusMessage = "Paper Jam";
                    break;

                case Status.TemperatureError01:
                case Status.TemperatureError02:
                case Status.TemperatureError05:
                case Status.TemperatureError06:
                case Status.TemperatureError07:
                case Status.TemperatureError08:
                    highLevelStatusCode = (int)HighLevelStatusCode.Error;
                    statusMessage = "Temperature Error";
                    break;

                case Status.DoorOpen:
                case Status.DoorCloseErr:
                case Status.ThvTuningErr:
                case Status.ThvUntuned:
                case Status.SHORTAGE_MEMORY:
                case Status.SHORTAGE_RIBBONV:
                    highLevelStatusCode = (int)HighLevelStatusCode.Error;
                    statusMessage = "Error";
                    break;

                case Status.Unknow:
                default:
                    statusMessage = String.Format("Unknown Error, code: {0}", status);
                    break;
            }

            if (highLevelStatusCode > -1)
            {
                var statusInfo = new StatusInfo
                {
                    StatusCode = highLevelStatusCode,
                    StatusMessage = statusMessage,
                    StatusCodeDetail = (int)status,
                    StatusMessageDetail = ConvertStatusEnum(status)
                };
                return statusInfo; ;
            }
            return null;
        }

        public bool SetMaxThreads(int maxThreads)
        {
            var success = SDK.MakeThreads(maxThreads);
            return success;
        }

        public bool ReleaseThreads(out int result)
        {
            var success = SDK.ReleaseThreads(out result);
            return success;
        }

        public bool GetThreadCount(out int count, out int maxCount)
        {
            var success = SDK.AttachThreadCount(out count, out maxCount);
            return success;
        }

        #region Private Methods

        private bool SelectPrinter(string serialNumber)
        {
            System.Diagnostics.Debug.WriteLine("SelectPrinter in");
            if (string.IsNullOrEmpty(serialNumber)) return false;

            int result;
            var ptr = SDK.SelectPrinterSN(serialNumber, out result);
            System.Diagnostics.Debug.WriteLine("SelectPrinter out");
            return (result == 0 && ptr != IntPtr.Zero);
        }

        private bool IgnoreError(int result)
        {
            // Ignore List
            if ((Status.BUSY == (Status)result) ||
                (Status.RibbonInitialize == (Status)result) ||
                (Status.ThermalProtect == (Status)result) ||
                (Status.PaperLoading == (Status)result) ||
                (Status.MainCpuInitialize == (Status)result) ||
                (Status.SHORTAGE_MEMORY == (Status)result) //||
                                                           //(kRESULT_STATUS_User_ReadyLoadingErr == result) // Ready loading
                )
            {
                // Ignore error
                return true;
            }
            else
            {
                // Error not in the ignore list - handle
                return false;
            }

        }

        #endregion



        private byte[] ConvertBytestoJpegBytes(byte[] pixels24bpp, int W, int H)
        {
            GCHandle gch = GCHandle.Alloc(pixels24bpp, GCHandleType.Pinned);
            int stride = 4 * ((24 * W + 31) / 32);
            Bitmap bmp = new Bitmap(W, H, stride, System.Drawing.Imaging.PixelFormat.Format24bppRgb, gch.AddrOfPinnedObject());
            MemoryStream ms = new MemoryStream();
            bmp.Save(ms, ImageFormat.Jpeg);
            gch.Free();
            return ms.ToArray();
        }
    }
}
