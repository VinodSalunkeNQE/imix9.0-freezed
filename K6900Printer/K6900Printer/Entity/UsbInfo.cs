﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace K6900Printer.Entity
{

    /// <summary>
    /// Authors Name: Kailash Changed Behera
    /// Created Date: 06 Aug 2019
    /// Description :Used to fetch attached USB information.
    /// </summary>
    public class UsbInfo
    {
        public string VendorId { get; set; }
        public string ProductId { get; set; }
        public string Description { get; set; }
    }
}
