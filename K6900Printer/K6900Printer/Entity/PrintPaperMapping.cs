﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace K6900Printer.Entity
{
    /// <summary>
    /// Authors Name: Kailash Changed Behera
    /// Created Date: 06 Aug 2019
    /// Description :Used to store printer paper information.
    /// </summary>
    public class PrintPaperMapping
    {
        public PrintPaperMapping(PrintAreaInfoCodeType printAreaInfoCode, PrintSize printSize, PrintTypeInfoCodeType printTypeInfoCode)
        {
            PrintAreaInfoCode = printAreaInfoCode;
            PrintSize = printSize;
            PrintTypeInfoCode = printTypeInfoCode;
        }

        public PrintAreaInfoCodeType PrintAreaInfoCode { get; set; }
        public PrintSize PrintSize { get; set; }
        public PrintTypeInfoCodeType PrintTypeInfoCode { get; set; }
    }
}
