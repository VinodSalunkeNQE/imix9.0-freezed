﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace K6900Printer.Entity
{
    /// <summary>
    /// Authors Name: Kailash Changed Behera
    /// Created Date: 06 Aug 2019
    /// Description :Used to store printer status details.
    /// </summary>
    public class PrinterStatus
    {
        public PrinterStatus(string serialNumber, int status)
        {
            SerialNumber = serialNumber;
            Result = status;
        }

        public string SerialNumber { get; set; }
        public int Result { get; set; }
    }
}
