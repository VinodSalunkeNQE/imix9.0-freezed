﻿using K6900Printer.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace K6900Printer.Entity
{
    /// <summary>
    /// ================================================================================
    /// Authors Name: Kailash Changed Behera
    /// Created Date: 06 Aug 2019
    /// Description :Used to specify printer paper info.
    /// </summary>
    public class PaperInfo
    {
        public PrintAreaInfoCodeType PrintAreaInfoCode { get; set; }
        public int PrintAreaWidth { get; set; }
        public int PrintAreaHeight { get; set; }
        public MediumType MediumType { get; set; }
        public PrintTypeInfoCodeType PrintTypeInfoCode { get; set; }
    }
}
