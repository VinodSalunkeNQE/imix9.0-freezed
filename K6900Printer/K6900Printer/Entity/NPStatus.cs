﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace K6900Printer.Entity
{
    /// <summary>
    /// Authors Name: Kailash Changed Behera
    /// Created Date: 06 Aug 2019
    /// Description :Used to store Printing report details.
    /// </summary>
    public class NPStatus
    {
        public bool PaperNearEndDetection { get; set; }
        public bool ThermalHeadCoverOpen { get; set; }
        public bool PaperEnd { get; set; }
        public bool HeadTemperatureIrregular { get; set; }
        public bool AutoCutterError { get; set; }
        public bool PaperDetectionError { get; set; }
        public bool PaperClampStatus { get; set; }
        public bool PrintStartStatus { get; set; }
    }
}
