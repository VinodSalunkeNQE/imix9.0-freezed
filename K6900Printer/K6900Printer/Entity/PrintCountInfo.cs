﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace K6900Printer.Entity
{
    /// <summary>
    /// Authors Name: Kailash Changed Behera
    /// Created Date: 06 Aug 2019
    /// Description :Used to specify total number of print given, media availability.
    /// </summary>
    public class PrintCountInfo
    {
        public int Permanent { get; set; }
        public int Maintenance { get; set; }
        public int AfterMediumReplacement { get; set; }
        public int Cutter { get; set; }
        public int RemainingMediuam { get; set; }
    }
}
