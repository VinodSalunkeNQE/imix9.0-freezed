﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace K6900Printer.Entity
{
    /// <summary>
    /// Authors Name: Kailash Changed Behera
    /// Created Date: 06 Aug 2019
    /// Description :Used to store printer details like serial number, PageID etc.
    /// </summary>
    public class PrintInfo
    {
        public string SerialNumber { get; set; }
        public DateTime CheckedOn { get; set; }
        public int PageId { get; set; }
        public int Left { get; set; }
        public int Completed { get; set; }
        public int Specified { get; set; }
    }
}
