﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace K6900Printer.Entity
{
    /// <summary>
    /// Authors Name: Kailash Changed Behera
    /// Created Date: 06 Aug 2019
    /// Description :Used to store printer status information.
    /// </summary>
    public class StatusInfo
    {
        public int StatusCode { get; set; }
        public string StatusMessage { get; set; }
        public int StatusCodeDetail { get; set; }
        public string StatusMessageDetail { get; set; }
    }
}
