﻿/// <summary>
/// Authors Name: Kailash Changed Behera
/// Created Date: 06 Aug 2019
/// Description :Used to specify printer hardware information.
/// </summary>
public class EngineInfo
{
    public int MainBootVer { get; set; }
    public int MainControlVer { get; set; }
    public int InputBootVer { get; set; }
    public int InputControlVer { get; set; }
    public string SerialNumber { get; set; }
}