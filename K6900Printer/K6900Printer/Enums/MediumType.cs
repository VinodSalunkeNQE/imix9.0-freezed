﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace K6900Printer.Enums
{
    /// <summary>
    /// Authors Name: Kailash Changed Behera
    /// Created Date: 06 Aug 2019
    /// Description :Used to specify printer media type.
    /// </summary>
    public enum MediumType
    {
        Unknown = 0,
        Paper = 1
    }
}
