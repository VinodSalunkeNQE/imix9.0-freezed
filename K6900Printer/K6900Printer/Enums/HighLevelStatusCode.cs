﻿/// <summary>
///   /// <summary>
/// Authors Name: Kailash Changed Behera
/// Created Date: 06 Aug 2019
/// Description :Used to specify hardware availability.
/// </summary>
/// </summary>
public enum HighLevelStatusCode
{
    Ready = 0,      // 0 = printer ready, accepting jobs
    Busy = 1,       // 1 = printer busy / printing, accepting jobs
    Error = 2       // 2 = printer error, unable to print
}