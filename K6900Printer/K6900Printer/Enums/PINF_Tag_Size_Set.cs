﻿/// <summary>
/// Authors Name: Kailash Changed Behera
/// Created Date: 06 Aug 2019
/// Description : Number of bytes for PINF each data structure(chcusb_setPrinterInfo).
/// </summary>
public enum PINF_Tag_Size_Set
{
    PAPER = 10,
    LAMINATE_MODE = 1,
    QUALITY_MODE = 1,
    PRINT_CONFIG = 1,
    ERROR_RECOVERY = 1,
    PREHEAT_MODE = 1,
    CONTOUR_ENHANCEMENT = 1,
    WAITTIME = 1,
    MOD_TABLE = 2,
    SERVICE_EVENT_TABLE = 3
}