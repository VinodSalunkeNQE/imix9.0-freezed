﻿/// <summary>
/// Authors Name: Kailash Changed Behera
/// Created Date: 06 Aug 2019
/// Description :Number of bytes for PINF each data structure(chcusb_getPrinterInfo).
/// </summary>
public enum PINF_Tag_Size_Get
{
    PAPER = 103,
    USBINQ = 23,
    ENGID = 115,
    ENGID2 = 153,
    SVCINFO = 2,
    MEMORY = 18,
    SERIALINFO = 8,
    USER_SN = 8,
    WAITTIME = 1,
    PRINTCNT = 28,
    MEMORY2 = 48,
    ERRHISTORY = 61,
    TONETABLE = 18,
    TRICOLOR_SENSOR = 15,
    MOD_TABLE = 40,
    SERVICE_EVENT_TABLE = 242,
    COMPONENT_TIMING = 320
}