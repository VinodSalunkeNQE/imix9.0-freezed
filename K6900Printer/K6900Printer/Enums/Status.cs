﻿/// <summary>
/// Authors Name: Kailash Changed Behera
/// Created Date: 06 Aug 2019
/// Description :Specifies printer current status.
/// </summary>
public enum Status
{
    //-----------------------------------------------------------------------
    //	Executing status
    //-----------------------------------------------------------------------
    NOERROR = 0,            // Normal ("True" is returned.)
    MEMFULLERR = 1000,      // Memory Full
    USBNOTFOUND = 1002,     // Cannot load UsbCtrl.DLL.
    ICCNOTFOUND = 1003, // Cannot load ColorMatch.DLL.
    OPENPORT = 1004,    // Cannot open the USB port.
    FILEACCESS = 1005,      // Cannot access the specified profile.
    PRMERROR = 1006,        // The specified parameter is not available.
    NOTFOUND = 1008,    // Printer with selected number is not found.
    IPNOTFOUND = 1009,      // Cannot load IP.DLL.
    IPUNMATCH = 1010,       // IP DLL version unmatch.

    MUTEX_Wait = 1500,      // Printer is used by another thread.
    THREAD_MAX = 1501,      // Cannot generate new thread.
    THREAD_Used = 1502,     // Now thread is loading.
    NOTCONNECT = 2000,      // The printer cannot be recognized.
    Unknow = 2504,      // Undefined error

    //------------------------------------------------------------------------------------------------------
    // USB communication failure factor
    USB_CommErr = 2600,     // USB communication error.

    //------------------------------------------------------------------------------------------------------
    // Printer status
    READY = 0,          // Printer is ready.
    BUSY = 2100,        // Buffer full.
    MainCpuInitialize = 2101,       // Initializing MAIN CPU.
    RibbonInitialize = 2102,        // Initializing Ribbon.
    PaperLoading = 2103,        // loading Paper Process.
    ThermalProtect = 2104,      // Thermal protect.
    Operation = 2105,       // Using Operation panel.
    Selfdiagnosis = 2106,       // Processing Self-Diagnostics.
    DownLoading = 2107,     // Processing download.

    PaperStartPosition = 2200,      // Feeding paper (at print start position).
    Heat = 2201,    // Processing pre-heat.
    Y_Printing = 2202,      // Printing Yellow.
    Y_PaperReturn = 2203,       // Paper back feeding after printing Yellow.
    M_Printing = 2204,      // Printing Magenta.
    M_PaperReturn = 2205,       // Paper back feeding after printing Magenta.
    C_Printing = 2206,      // printing Cyan.
    C_PaperReturn = 2207,       // Paper back feeding after printing Cyan.
    OP_Printing = 2208,     // Printing OP.
    CutPositionLoading = 2209,      // Feeding paper (at paper cut position).
    PaperEjecting = 2210,       // Processing paper ejection.
    WaitPositionReturn = 2211,      // Paper back feeding (at ready position).
    PrintingComplete = 2212,        // Finish printing.
    NoPrinting = 2300,      // No printing.
    UNKNOWNPAPER = 2301,    // The specified paper information is not available to the currently selected printer.

    NoPaper = 2304,     // Paper is not set.
    OutOfPaper = 2305,      // Out of Paper.
    NoRibbon = 2306,        // Ribbon is not set.
    OutOfRibbon = 2307,     // Out of Ribbon
    IncorrectRibbon = 2308,     // Incorrect ribbon,
    DoorOpen = 2309,    // Door open.
    DoorCloseErr = 2310,    // Door close error.
    RibbonErrRewind = 2314,     // Ribbon rewinding error.
    RibbonErrSensing = 2315,        // Ribbon sensing error.
    ThvTuningErr = 2332,    // THV tuning error.
    ThvUntuned = 2333,      // THV untuned.
    SHORTAGE_MEMORY = 2340,     // Memory shortage (Panoramic).
    SHORTAGE_RIBBONV = 2341,        // Ribbon shortage (Panoramic).

    PaperJamError01 = 3001,     // Errror when the printer tried to begin paper transporting.
    PaperJamError02 = 3002,     // Same as above
    PaperJamError16 = 3016,     // Errror when the printer is transporting paper.
    PaperJamError17 = 3017,     // Same as above
    PaperJamError18 = 3018,     // Same as above
    PaperJamError19 = 3019,     // Same as above
    PaperJamError48 = 3048,     // Error when the printer is printing.
    PaperJamError49 = 3049,     // Same as above
    PaperJamError50 = 3050,     // Same as above
    PaperJamError64 = 3064,     // Error when the printer is cutting paper.
    PaperJamError65 = 3065,     // Same as above
    PaperJamError81 = 3081,     // Error when the printer is idle state.
    PaperJamError82 = 3082,     // Same as above
    PaperJamError83 = 3083,     // Same as above

    ControlError01 = 4001,      // EEPROM write error.
    ControlError10 = 4010,      // Invalid Print Parameter TABLE.
    ControlError12 = 4012,      // Print Parameter TABLE version mismatch.
    ControlError13 = 4013,      // FPGA configuration failure.
    ControlError15 = 4015,      // MAIN firmware data error.
    ControlError16 = 4016,      // MAIN firmware writing error.
    ControlError19 = 4019,      // Print Parameter TABLE checksum error.
    ControlError20 = 4020,      // Print Parameter TABLE writing error.
    ControlError21 = 4021,      // User Tone Curve write error.
    ControlError22 = 4022,      // MAIN-MSP communication error.
    ControlError25 = 4025,      // Thermal head power error.

    MechanicalError01 = 4101,       // Error of Head/ Pinch Roller driving (home).
    MechanicalError02 = 4102,       // Error of Head/ Pinch Roller driving (mode-1).
    MechanicalError03 = 4103,       // Error of Head/ Pinch Roller driving (mode-2).
    MechanicalError04 = 4104,       // Error of Head/ Pinch Roller driving (mode-3).
    MechanicalError05 = 4105,       // Error of Head/ Pinch Roller driving (mode-4).
    MechanicalError20 = 4120,       // Error of Cutter driving (L to R).
    MechanicalError21 = 4121,       // Error of Cutter driving (R to L).

    SensorError01 = 4201,       // Error of Cutter Sensors.
    SensorError02 = 4202,       // Error of Left Cutter Sensor after drove.
    SensorError03 = 4203,       // Error of Right Cutter Sensor after drove.
    SensorError30 = 4230,       // Error of Left Front-Lock Sensor.
    SensorError31 = 4231,       // Error of Right Front-Lock Sensor.
    SensorError32 = 4232,       // Error of Left Cutter Sensor.
    SensorError33 = 4233,       // Error of Right Cutter Sensor.
    SensorError34 = 4234,       // Error of Left Head/ Pinch Roller Sensor.
    SensorError35 = 4235,       // Error of Right Head/ Pinch Roller Sensor.
    SensorError36 = 4236,       // Error of Head/ Pinch Roller Encoder.
    SensorError37 = 4237,       // Error of Supply Ribbon Encoder.

    TemperatureError01 = 4301,      // Error of the thermal print head thermistor (low temperature).
    TemperatureError02 = 4302,      // Error of the thermal print head thermistor (high temperature).
    TemperatureError05 = 4305,      // Error of the printer thermistor (low temperature).
    TemperatureError06 = 4306,      // Error of the printer thermistor (high temperature).
    TemperatureError07 = 4307,      // Error of the preheat process.
    TemperatureError08 = 4308       // Error of the thermal-protect process.

}