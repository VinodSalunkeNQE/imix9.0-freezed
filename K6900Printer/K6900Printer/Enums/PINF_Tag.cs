﻿/// <summary>
/// Authors Name: Kailash Changed Behera
/// Created Date: 06 Aug 2019
/// Description : Tag identifier number definitions for chcusb_getPrinterInfo / chcusb_setPrinterInfo
/// </summary>
public enum PINF_Tag
{
    PAPER = 0,
    USBINQ = 2,
    ENGID = 3,
    PRINTCNT = 4,
    ENGID2 = 5,
    SVCINFO = 7,
    MEMORY = 16,
    MEMORY2 = 17,
    LAMINATE_MODE = 20,
    QUALITY_MODE = 21,
    SERIALINFO = 26,
    PRINT_CONFIG = 30,
    ERROR_RECOVERY = 31,
    PREHEAT_MODE = 32,
    CONTOUR_ENHANCEMENT = 33,
    ERRHISTORY = 50,
    TONETABLE = 60,
    WAITTIME = 70,
    TRICOLOR_SENSOR = 90,
    MOD_TABLE = 91,
    SERVICE_EVENT_TABLE = 92,
    COMPONENT_TIMING = 93
}