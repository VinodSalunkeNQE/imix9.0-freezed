﻿/// <summary>
/// Authors Name: Kailash Changed Behera
/// Created Date: 06 Aug 2019
/// Description :Used to specify image color format.
/// </summary>
public enum PixelFormat
{
    RGB = 3,    // RGB point sequential format
    BGR = 4     // BGR point sequential format
}