﻿/// <summary>
/// Authors Name: Kailash Changed Behera
/// Created Date: 06 Aug 2019
/// Description :Used to specify type of printing.
/// </summary>
public enum PrintTypeInfoCodeType
{
    Normal = 0,
    Combo1Image = 1,
    Combo2Image = 2,
    WithBorder = 3,
    BorderCombo1Image = 4,
    BorderCombo2Image = 5
}