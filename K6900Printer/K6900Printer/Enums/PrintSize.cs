﻿
/// <summary>
/// Authors Name: Kailash Changed Behera
/// Created Date: 06 Aug 2019
/// Description :Defines media size to be print.
/// </summary>
public enum PrintSize
{
    ThreeByFive,
    FourBySix,
    FiveBySeven,
    SixByEight,
    SixByFourteenPanoramic,
    SixByTwentyPanoramic

}