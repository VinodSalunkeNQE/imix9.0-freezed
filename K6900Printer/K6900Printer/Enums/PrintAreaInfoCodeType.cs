﻿/// <summary>
/// Authors Name: Kailash Changed Behera
/// Created Date: 06 Aug 2019
/// Description :Specifies printer media size.
/// </summary>
public enum PrintAreaInfoCodeType
{
    FourBySix = 0,
    ThreeByFive = 1,
    SixByEight = 6,
    FiveBySeven = 7
}