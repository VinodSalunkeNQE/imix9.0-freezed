﻿using K6900Printer.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace K6900Printer
{
    /// <summary>
    /// Authors Name: Kailash Changed Behera
    /// Created Date: 06 Aug 2019
    /// Description :KODAK 6900 Printer interface.
    /// </summary>
    public interface IK6900Driver
    {
        string Description { get; }
        bool GetStatus(out int result);
        bool GetStatus(string serialNumber, out int result);
        bool GetStatusAll(out List<PrinterStatus> result);
        bool GetUsbInfo(out UsbInfo result);
        bool GetPaperInfo(out List<PaperInfo> result);
        bool GetEngineInfo(out EngineInfo result);
        bool GetPrintCountInfo(string serialNumber, out PrintCountInfo result);
        bool GetSerialNumbers(out string[] result);
        IntPtr SelectPrinter(string serialNumber, out int result);
        bool GetCompletedPageIds(out List<PrintInfo> result);
        bool GetCompletedPageIds(string serialNumber, out List<PrintInfo> result);
        bool Print(System.Drawing.Image img, PrintSize printSize, int numberOfCopies, out int result, out int pageId, string inputProfile = "", string outputProfile = "");
        bool Print(string filename, PrintSize printSize, int numberOfCopies, out int result, out int pageId, string inputProfile = "", string outputProfile = "");
        bool Print(string serialNumber, string filename, PrintSize printSize, int numberOfCopies, out int result, out int pageId, out int printerId, string inputProfile = "", string outputProfile = "");
        StatusInfo GetStatusInfo(int statusCode);
        bool SetMaxThreads(int maxThreads);
        bool ReleaseThreads(out int result);
        bool GetThreadCount(out int count, out int maxCount);
    }
}
