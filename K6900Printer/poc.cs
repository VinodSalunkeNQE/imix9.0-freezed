do
{
    int str = -1;
	bool iswait = true;
	if (k6900Driver.GetStatus(out str) != true)
	{
        if (needTowaitPrinter6900(str) == true) // If needTowaitPrinter6900 checks if the error is ignorable (ex. 2100), it returns true. We are assuming this function returns true if the error is ignorable. 
        {
                           
            Console.WriteLine("Kodak 6900 is waiting due to Print Status code:-" + str.ToString());
            Thread.Sleep(1000);
        }
        else // Else occurs if there is a non-ingorable error (ex. 2309 - Cover Open). The error must be cleared before printing.
        {
            Console.WriteLine("Kodak 6900 Printer Status code:-" + str.ToString());
            iswait = false;
            return false; 
        }
 	}
	else // Else occurs if the printer returns no error (printer is ready). We can continue to the print command. 
    {
        Console.WriteLine("Kodak 6900 Printer Status code:-" + str.ToString());
        iswait = false;
    }
}
while (iswait == true);
rs = 0;
PageID = 0;
if (k6900Driver.Print(imagePath, PrintSize, 1, out rs, out PageID, "", "") == true)
{
	Console.WriteLine(" Print Command Status:" + rs.ToString());
}
				