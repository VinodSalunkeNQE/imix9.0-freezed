﻿using K6900Printer;
using K6900Printer.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace KODAKPrinterTester
{
    class Program
    {
        static void Main(string[] args)
        {
            string path = Environment.CurrentDirectory;
            int rs = 0;
            int PageID = 0;
            K6900Driver k6900Driver = new K6900Driver();
            string[] serialnumber = new string[1];
            bool result = k6900Driver.GetSerialNumbers(out serialnumber);
            PrintCountInfo printCountInfo = new PrintCountInfo();
            result = k6900Driver.GetPrintCountInfo(serialnumber[0], out printCountInfo);
            //for(int n = 0; n < 20; n++ )
            //{
            k6900Driver.Print(@"\\imixdbindlab\DigiImages\EditedImages\23630210.jpg", PrintSize.FourBySix, 1, out rs, out PageID, "", "");
                Console.WriteLine("6X8: Success");
            //}
            return;
            Console.WriteLine("6X8 Print Command Status : " + rs.ToString());
            int str = -1;
            bool iswait = true;
            do
            {
                Thread.Sleep(25000);
                if (k6900Driver.GetStatus(out str) == true)
                {
                    if (str == 0)
                    {
                        if (needTowaitPrinter6900(str) == true)
                        {
                           
                            Console.WriteLine("Kodak 6900 is waiting due to Print Status code:-" + str.ToString());
                            Thread.Sleep(10000);
                        }
                        else
                        {
                            Console.WriteLine("Kodak 6900 Printer Status code:-" + str.ToString());
                            iswait = false;
                        }
                    }
                }
                else
                {
                    Console.WriteLine("Kodak 6900 Printer Status code:-" + str.ToString());
                    iswait = false;
                }
            }
            while (iswait == true);
            if (iswait == false)
            {
                //Thread.Sleep(10000);
                rs = 0;
                PageID = 0;
                if (k6900Driver.Print(@"D:\DigiImages\20190807\783_4.jpg", PrintSize.SixByTwentyPanoramic, 1, out rs, out PageID, "", "") == true)
                {
                    Console.WriteLine(" Print Command Status:" + rs.ToString());
                }
            }
            //Console.WriteLine("6X20: " + rs.ToString());
            Console.ReadLine();
        }
        private static bool needTowaitPrinter6900(int code)
        {
            switch (code)
            {
                case 1500:
                case 2100:
                case 2101:
                case 2102:
                case 2103:
                case 2104:
                case 2340:
                //case 2105:
                //case 2106:
                //case 2107:
                //case 2304:
                //case 2600:
                    return true;
                default:
                    return false;
            }
        }
    }
}
