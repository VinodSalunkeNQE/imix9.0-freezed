﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DigiPhoto.IMIX.Model
{
    public class ChangeTrackingProcessingStatusDtl
    {
        public long ExceptionDtlID { get; set; }
        public int Status { get; set; }
        public string StatusValue { get; set; }
        public string Message { get; set; }
        public int Stage { get; set; }
        public DateTime CreatedOn { get; set; }

        public string ExceptionType { get; set; }
        public string ExceptionSource { get; set; }
        public string ExceptionStackTrace { get; set; }
        public string InnerException { get; set; }
        public long IncomingChangeId { get; set; }

        public string SyncCode { get; set; }
        public long ChangeTrackingId { get; set; }

        public string VenueName { get; set; }
        public Int32 ErrorID { get; set; }

    }
}
