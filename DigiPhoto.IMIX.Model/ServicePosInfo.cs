﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DigiPhoto.IMIX.Model
{
    public class ServicePosInfo
    {
        public int ServiceId { get; set; }
        public long ImixPosId { get; set; }
        public int SubstoreId { get; set; }
        public string ServiceName { get; set; }
        public string SystemName { get; set; }
        public string SubStoremName { get; set; }
        public string UniqueCode { get; set; }

       public int RunLevel{get;set;}
       public string Status { get; set; }

    }
}