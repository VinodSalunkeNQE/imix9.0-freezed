﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DigiPhoto.IMIX.Model
{
    public class EMailInfo
    {
        public string UserId { get; set; }
        public string UserName { get; set; }
        public string StoreId { get; set; }
        public string SubstoreId { get; set; }
        public string VenueId { get; set; }
        public string OrderId { get; set; }
        public string Emailto { get; set; }
        public string EmailBcc { get; set; }
        public string EmailIsSent { get; set; }
        public string Sendername { get; set; }
        public string EmailMessage { get; set; }
        public string MediaName { get; set; }
        public string OtherMessage { get; set; }
        public string MailSubject { get; set; }
        public string MediaType { get; set; }
        public string FileExtension { get; set; }
    }


    public class EmailDetailInfo
    {
        public Int64 EmailDetailId_Pkey { get; set; }
        public Int32 EmailKey { get; set; }
        public string OrderId { get; set; }
        public Int64 PhotoId { get; set; }
        public string DG_Email_To { get; set; }
        public string DG_Email_Bcc { get; set; }
        public string DG_EmailSender { get; set; }
        public string DG_Message { get; set; }
        public string EmailTemplate { get; set; }
        public string DG_ReportMailBody { get; set; }
        public string DG_OtherMessage { get; set; }
        public string DG_MessageType { get; set; }
        public string DG_EmailSubject { get; set; }
        public string DG_MediaName { get; set; }
        public string message { get; set; }
    }
}
