﻿ using System;
using System.ComponentModel;

namespace DigiPhoto.IMIX.Model
{
    public class ViewModelBase : INotifyPropertyChanged, INotifyPropertyChanging
    {
        public event PropertyChangedEventHandler PropertyChanged;
        public event PropertyChangingEventHandler PropertyChanging;

        protected void NotifyPropertyChanged(String info)
        {
            info = info.Replace("get_", "").Replace("set_", "");
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(info));
            }
        }

        protected void NotifyPropertyChanging(String info)
        {
            info = info.Replace("get_", "").Replace("set_", "");
            if (PropertyChanging != null)
            {
                PropertyChanging(this, new PropertyChangingEventArgs(info));
            }
        }

    }
}
