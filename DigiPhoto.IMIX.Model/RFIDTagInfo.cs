﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DigiPhoto.IMIX.Model
{
    public class RFIDTagInfo
    {
        public int IdentifierId { get; set; }
        public int DeviceID { get; set; }

        public string TagId { get; set; }
        public DateTime? ScanningTime { get; set; }
        public int Status { get; set; }
        public string RawData { get; set; }

        public string SerialNo { get; set; }
        public bool IsActive { get; set; }
        public int DummyRFIDTagID { get; set; }
        public DateTime? CreatedOn { get; set; }
		////////////////created by latika for table flow
        public string TableName { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Contacts { get; set; }
        public string ScanningDatetime { get; set; }
		///////end


    }
}
