﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DigiPhoto.IMIX.Model
{
    public class RFIDField
    {
        public int DeviceID { get; set; }

        public string SerialNo { get; set; }

        public string HotFolderpath { get; set; }

        public int SubStoreID { get; set; }
    }
}
