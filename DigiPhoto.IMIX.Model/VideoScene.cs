﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DigiPhoto.IMIX.Model
{
    public class VideoScene
    {
        public int SceneId { get; set; }
        public string Name { get; set; }
        public string ScenePath { get; set; }
        public int LocationId { get; set; }
        public string Settings { get; set; }
        public int VideoLength { get; set; }
        public bool IsActive { get; set; }
        public bool IsActiveForAdvanceProcessing { get; set; }
        public string IsActiveForAdvanceProcessingStatus { get; set; }
        public string  LocationName { get; set; }

        public bool IsMixerScene { get; set; }
        public int CG_ConfigID { get; set; }

        public List<VideoSceneObject> lstVideoSceneObject { get; set; }
        public bool IsVerticalVideo { get; set; }
    }
    public class VideoSceneObject
    {
        public int VideoObject_Pkey { get; set; }
        public string VideoObjectId { get; set; } //stream id
        public int SceneId { get; set; }
        public bool GuestVideoObject { get; set; }
        public VideoObjectFileMapping ObjectFileMapping { get; set; }
        public bool streamAudioEnabled { get; set; }
        public string FileName { get; set; }
    }
    public class VideoObjectFileMapping
    {
        public int ID { get; set; }
        public string VideoSceneObjectId { get; set; }
        public int ValueTypeId { get; set; }
        public string ChromaPath { get; set; }
        public string RoutePath { get; set; }

        public string StreamAudioEnabled { get; set; }

      
        public string ResourcePath { get; set; }


    }

    public class VideoSceneViewModel
    {
        public VideoScene VideoScene { get; set; }
        public VideoSceneObject VideoSceneObject { get; set; }
        public List<VideoSceneObject> ListVideoSceneObject { get; set; }
        public List<VideoObjectFileMapping> ListVideoObjectFileMapping { get; set; }
        public VideoObjectFileMapping VideoObjectFileMapping { get; set; }

    }



    public class Watchersetting
    {
        public bool Isstream { get; set; }
        public bool Dg_Semiorder { get; set; }
        public string  SceneName { get; set; }
        public string ProfileName { get; set; }
        public bool IsMixerScene { get; set; }

 
    }
    public class QuickSettings
    {
        public bool FullScreen { get; set; }
        public bool Video { get; set; }
        public bool AspectRatio { get; set; }
        public bool Audio { get; set; }
        public int Volume { get; set; }
        public int LocationID { get; set; }
        public int SubStorID { get; set; }
        public int ScenID { get; set; }
    }
}
