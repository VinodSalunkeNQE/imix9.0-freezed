﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DigiPhoto.IMIX.Model
{
    public class UserInfo
    {
        public int UserId { get; set; }
        public string Photographer { get; set; }
        public string UserName { get; set; }
        public int Role_ID { get; set; }
    }
}
