﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DigiPhoto.IMIX.Model
{
    public class iMIXConfigurationInfo
    {
        public long IMIXConfigurationValueId { get; set; }
        public long IMIXConfigurationMasterId { get; set; }
        public string ConfigurationValue { get; set; }
        public int SubstoreId { get; set; }
        public string SyncCode { get; set; }

        public bool IsSynced { get; set; }
		///changed by latika for table flow
        public int FontSize { get; set; }
        public string FontStyle { get; set; }
        public string FontFamily { get; set; }
        public string FontWeight { get; set; }
        public string Settings { get; set; }
        public int SubStoreID { get; set; }
        public int LocationID { get; set; }
        public int UserID { get; set; }
        public int IsActive { get; set; }
		///end
    }
		///changed by latika for table flow
    public class iMIXRFIDTableWorkflowInfo
    {
        public int FontSize { get; set; }
        public string FontStyle { get; set; }
        public string FontFamily { get; set; }
        public string FontWeight { get; set; }
        public string Settings { get; set; }
        public int SubStoreID { get; set; }
        public int LocationID { get; set; }
        public int UserID { get; set; }
        public bool IsActive { get; set; }
        public string Status { get; set; }
        public string ErrorMessage { get; set; }
        public string LocationName { get; set; }
        public string UserName { get; set; }
        public string TypesName { get; set; }
        public string Position { get; set; }
        public string BackColor { get; set; }
        public string Font { get; set; }
        public int MarginLeft { get; set; }
        public int MarginTop { get; set; }
        public int MarginRight { get; set; }
        public int MarginBottom { get; set; }
        public string Orientation { get; set; }
    }
    //end
}
