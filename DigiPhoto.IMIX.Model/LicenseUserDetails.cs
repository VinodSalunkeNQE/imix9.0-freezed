﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DigiPhoto.IMIX.Model
{
    public class LicenseUserDetails
    {
        public int ID { get; set; }
        public int LicensePrefix { get; set; }
        public int LicenseSufix { get; set; }
        public string Name { get; set; }
        public int Age { get; set; }
        public DateTime ExpiryDate { get; set; }
        public DateTime IssueDate { get; set; } 
        public string PofIssue { get; set; }
        public int PhotoID { get; set; }

    }
}
