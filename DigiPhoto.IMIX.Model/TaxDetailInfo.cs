﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DigiPhoto.IMIX.Model
{
    [Serializable]
    public class TaxDetailInfo
    {
        public int TaxId { get; set; }
        public decimal TaxAmount { get; set; }
        public decimal TaxPercentage { get; set; }
        public string TaxName { get; set; }
        public string CurrencyName { get; set; }
        public bool IsActive { get; set; }
    }
}
