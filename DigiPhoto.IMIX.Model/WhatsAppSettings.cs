﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DigiPhoto.IMIX.Model
{
    [Serializable]
    public class WhatsAppSettings
    {
        public string MobileNumber { get; set; }

        public string CountryName { get; set; }

        public string CountryCode { get; set; }

        public string APIKey { get; set; }

        public string HostUrl { get; set; }



    }


    [Serializable]
    public class WhatsAppSettingsTracking
    {
        public long WhatsAppId { get; set; }
        public string Order_Number { get; set; }

        public string Photo_FileName { get; set; }

        public DateTime WhtsApp_Order_Date { get; set; }

        public int Status { get; set; }

        public string Share_Images { get; set; }

        public string Status_Description { get; set; }

        public string CountryName { get; set; }

        public string CountryMobileCode { get; set; }

        public string Guest_MobileNumber { get; set; }
        public string Guest_Resend_MobileNumber { get; set; }

        public string WhtsApp_Image_SourcePath { get; set; }

        public string WhtsApp_URL_SourcePath { get; set; }


        public int RetryCount { get; set; }

        public DateTime? creation_date { get; set; }

        public DateTime? process_date { get; set; }

        public DateTime? failed_date { get; set; }

        public string custom_data { get; set; }

        public bool? IsAvailable { get; set; }
    }
}
