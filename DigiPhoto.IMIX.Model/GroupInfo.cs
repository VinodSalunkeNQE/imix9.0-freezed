﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DigiPhoto.IMIX.Model
{
    public class GroupInfo
    {
        public int GroupID { get; set; }
        public string GroupName { get; set; }
    }
}
