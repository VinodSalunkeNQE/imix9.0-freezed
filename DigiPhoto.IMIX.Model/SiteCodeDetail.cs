﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DigiPhoto.IMIX.Model
{
    public class SiteCodeDetail
    {
        public int SiteId { get; set; }

        public string SiteCode { get; set; }
    }
}
