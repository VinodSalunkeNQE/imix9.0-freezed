﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DigiPhoto.IMIX.Model
{
    public class PrintSummaryDetail
    {
        public string SaleType { get; set; }
        public string PhotoNumbers { get; set; }
    }
}
