﻿#region Assembly DigiPhoto.IMIX.Model, Version=7.0.1.1, Culture=neutral, PublicKeyToken=null
// D:\Kailash\Soucecodes\BitBucket\IMIX\7011_Facial\DigiPhotoEnhancedCode\DigiPhoto.IMIX.Model\obj\Debug\DigiPhoto.IMIX.Model.dll
#endregion
using System.Configuration;
using System;

namespace DigiPhoto.IMIX.Model
{/// <summary>
/// Added by Kailash ON 25 SEP 2019 to capture FR token API
/// </summary>
    public class FRAPIDetails
    {
        public string TokenPAI { get; set; }
        public string ImageAPI { get; set; }
        public string client_id { get; set; }
        public string client_secret { get; set; }
        public string grant_type { get; set; }
        public string scope { get; set; }
        public string username { get; set; }
        public string password { get; set; }
        public string Token { get; set; }
        public DateTime ExpireDate { get; set; }
        public bool HasError { get; private set; }
        public string ErrorMessage { get; private set; }
        public FRAPIDetails()
        {
            this.grant_type = "password";
            this.Token = "NA";
            this.scope = "euclid";
            this.GetDetals();
        }
        private void GetDetals()
        {
            try
            {
                this.client_id = ConfigurationManager.AppSettings["FRClientID"];
                this.client_secret = ConfigurationManager.AppSettings["FRSecretKey"];
                this.username = ConfigurationManager.AppSettings["FRusername"];
                this.password = ConfigurationManager.AppSettings["FRpassword"];
                this.ImageAPI = ConfigurationManager.AppSettings["FRImageAPI"];
                this.TokenPAI = ConfigurationManager.AppSettings["FRTokenAPI"];
            }
            catch (Exception ex)
            {
                this.HasError = true;
                this.ErrorMessage = ex.Message;
            }
        }
    }
}