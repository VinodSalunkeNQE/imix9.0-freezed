﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DigiPhoto.IMIX.Model
{
    public class FileInfoViewModel
    {
        public FileInfo FileInfo { get; set; }
        public long DisplayOrder { get; set; }
    }
}
