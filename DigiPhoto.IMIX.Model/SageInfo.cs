﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace DigiPhoto.IMIX.Model
{
    public class SageInfo
    {
        //By KCB ON 11 APR 2018 For handling exception of transaction date
        public SageInfo()
        {
            if (this.TransDate == null)
                this.TransDate = DateTime.Now.AddDays(-1);//01/01/1753
            if (this.ServerTime == null || this.ServerTime.Equals(new DateTime(0001,01,01))==true)
                this.ServerTime = DateTime.Now;
            if(this.BusinessDate==null)
                this.BusinessDate = DateTime.Now;
        }
        //end
        /*public Int64 OpeningFormDetailID { get; set; }
        public Int64 sixEightStartingNumber { get; set; }
        public Int64 eightTenStartingNumber { get; set; }
        public Int64 sixEightAutoStartingNumber { get; set; }
        public Int64 eightTenAutoStartingNumber { get; set; }*/

        public Int64 OpeningFormDetailID { get; set; }
         public Int64 sixEightStartingNumber { get; set; }
        public Int64 sixTwentyStartingNumber { get; set; }// BY KCB ON 26 MARCH 2018 FOR storing panorama printer inventory
        public Int64 eightTenStartingNumber { get; set; }
        public Int64 eightTwentyStartingNumber { get; set; }// BY KCB ON 26 MARCH 2018 FOR storing panorama printer inventory
        public Int64 K6900StartingNumber { get; set; }// BY KCB ON 08 AUG 2019 FOR storing panorama printer inventory
        public Int64 sixEightAutoStartingNumber { get; set; }
        public Int64 sixTwentyAutoStartingNumber { get; set; }// BY KCB ON 26 MARCH 2018 FOR storing panorama printer inventory
        public Int64 eightTenAutoStartingNumber { get; set; }
        public Int64 eightTwentyAutoStartingNumber { get; set; }// BY KCB ON 26 MARCH 2018 FOR storing panorama printer inventory
        public Int64 K6900AutoStartingNumber { get; set; }// BY KCB ON 08 AUG 2019 FOR storing panorama printer inventory


        public Int64 PosterStartingNumber { get; set; }
        public decimal CashFloatAmount { get; set; }
        [XmlIgnore]
        public int SubStoreID { get; set; }
        public DateTime? OpeningDate { get; set; }
        public string FilledBySyncCode { get; set; }
        [XmlIgnore]
        public Int32 FilledBy { get; set; }
        [XmlIgnore]
        public Int64 OpenCloseProcDetailID { get; set; }
        [XmlIgnore]
        public Int32 FormTypeID { get; set; }
        [XmlIgnore]
        public DateTime? FilledOn { get; set; }
        [XmlIgnore]
        public DateTime? TransDate { get; set; }
        [XmlIgnore]
        public int FormID { get; set; }
        public string SyncCode { get; set; }
        public DateTime? BusinessDate { get; set; }
        [XmlIgnore]
        public DateTime ServerTime { get; set; }

    }
    public class SageOpenClose
    {
        [XmlElement("ClosingFrom")]
        public SageInfoClosing objClose { get; set; }

        [XmlElement("OpeningFrom")]
        public SageInfo objOpen { get; set; }
    }
    [XmlRoot(ElementName = "ClosingForm")]
    public class SageInfoClosing
    {
        public Int64 ClosingFormDetailID { get; set; }
        [XmlElement("SubStore")]
        public SubStoresInfo objSubStore { get; set; }
        public Int64 sixEightClosingNumber { get; set; }
        public Int64 eightTenClosingNumber { get; set; }
        public Int64 PosterClosingNumber { get; set; }
        public Int64 sixEightAutoClosingNumber { get; set; }
        public Int64 eightTenAutoClosingNumber { get; set; }
        public Int64 SixEightWestage { get; set; }
        public Int64 EightTenWestage { get; set; }

        public Int64 SixEightAutoWestage { get; set; }
        public Int64 EightTenAutoWestage { get; set; }
        
        // BY KCB ON 26 MARCH 2018 FOR storing panorama printer inventory
       public Int64 SixTwentyClosingNumber { get; set; }
       public Int64 EightTwentyClosingNumber { get; set; }

       public Int64 SixTwentyAutoClosingNumber { get; set; }
       public Int64 EightTwentyAutoClosingNumber { get; set; }

       public Int64 SixTwentytWestage { get; set; }
       public Int64 EightTwentyWestage { get; set; }

       public Int64 SixTwentyAutoWestage { get; set; }
       public Int64 EightTwentyAutoWestage { get; set; }

        //END
        // BY KCB ON 08 AUG 2019 FOR storing panorama printer inventory
        public Int64 K6900ClosingNumber { get; set; }
        public Int64 K6900AutoClosingNumber { get; set; }
        public Int64 K6900AutoWestage { get; set; }
        //END

        public Int64 PosterWestage { get; set; }
        public int Attendance { get; set; }
        public decimal LaborHour { get; set; }
        public Int64 NoOfCapture { get; set; }
        public Int64 NoOfPreview { get; set; }
        public Int64 NoOfImageSold { get; set; }
        public string Comments { get; set; }
        public DateTime? ClosingDate { get; set; }

        [XmlIgnore]
        public Int32 FilledBy { get; set; }
        public string FilledBySyncCode { get; set; }
        [XmlIgnore]
        public Int64 OpenCloseProcDetailID { get; set; }
        [XmlIgnore]
        public Int32 FormTypeID { get; set; }
        public DateTime? TransDate { get; set; }
        public decimal Cash { get; set; }
        public decimal CreditCard { get; set; }
        public decimal Amex { get; set; }
        public decimal FCV { get; set; }
        public decimal RoomCharges { get; set; }
        public decimal KVL { get; set; }
        public decimal Vouchers { get; set; }
        public Int64 SixEightPrintCount { get; set; }
        public Int64 EightTenPrintCount { get; set; }
        public Int64 PosterPrintCount { get; set; }
        public string SyncCode { get; set; }
        [XmlIgnore]
        public DateTime? OpeningDate { get; set; }
        public List<TransDetail> TransDetails { get; set; }
        public List<InventoryConsumables> InventoryConsumable { get; set; }
        public int NoOfTransactions { get; set; }
    }
    public class SageInfoWestage
    {
        public int ProductType { get; set; }
        public Int64 Printed { get; set; }
        public int Reprint { get; set; }
    }
    public class SageClosingFormDownloadInfo
    {
        public Int64 ClosingFormDetailID { get; set; }
        public string FilledBy { get; set; }
        public string SubstoreName { get; set; }
        public DateTime BusinessDate { get; set; }
        public DateTime ClosingDate { get; set; }
    }
    public class InventoryConsumables
    {
        [XmlIgnore]
        public long InventoryConsumablesID { get; set; }
        // public long ClosingFormDetailID { get; set; }
        public Int64 AccessoryID { get; set; }
        public long ConsumeValue { get; set; }
        [XmlIgnore]
        public string AccessoryName { get; set; }
        public string AccessorySyncCode { get; set; }
        public string AccessoryCode { get; set; }
    }
    public class TransDetail
    {
        [XmlIgnore]
        public long TransDetailID { get; set; }

        [XmlIgnore]
        public Int32 SubstoreID { get; set; }

        public DateTime TransDate { get; set; }

        public Int32 PackageID { get; set; }

        public string PackageSyncCode { get; set; }

        public decimal UnitPrice { get; set; }

        public decimal Quantity { get; set; }

        public decimal Discount { get; set; }

        public decimal Total { get; set; }

        // public long ClosingFormDetailID { get; set; }
        // public string PackageSyncCode{get;set;}
        public string PackageCode { get; set; }
    }


    public class Printer8810
    {
        public string ErrorMessage { get; set; }
        public long ImageCount { get; set; }
        public long ImageNewCount { get; set; }
        public long ImageOldCount { get; set; }
        public bool IsOnline { get; set; }
        public bool IsPrinting { get; set; }
        public int PrinterID { get; set; }
        public string PrinterSerialNumber { get; set; }
        public string PrinterStatus { get; set; }



    }

    public class Printer6850
    {
        public string ErrorMessage { get; set; }
        public long ImageCount { get; set; }
        public long ImageNewCount { get; set; }
        public long ImageOldCount { get; set; }
        public bool IsOnline { get; set; }
        public bool IsPrinting { get; set; }
        public int PrinterID { get; set; }
        public string PrinterSerialNumber { get; set; }
        public string PrinterStatus { get; set; }

    }

}
