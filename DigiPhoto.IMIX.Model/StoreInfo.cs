﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
namespace DigiPhoto.IMIX.Model
{
    public class StoreInfo
    {
        public int DG_Store_pkey { get; set; }

        public string DG_Store_Name { get; set; }

        public string Country { get; set; }

        public string DG_CentralServerIP { get; set; }

        public string DG_StoreCode { get; set; }

        public string DG_CenetralServerUName { get; set; }

        public string DG_CenetralServerPassword { get; set; }

        public decimal? DG_PreferredTimeToSyncFrom { get; set; }

        public decimal? DG_PreferredTimeToSyncTo { get; set; }

        public string DG_QRCodeWebUrl { get; set; }

        public string CountryCode { get; set; }

        //public string StoreCode { get; set; }

        //new 
        public string Address { get; set; }

        public string BillReceiptTitle { get; set; }

        public bool? IsSequenceNoRequired { get; set; }

        public bool IsTaxEnabled { get; set; }

        public string PhoneNo { get; set; }

        public string StoreCode { get; set; }

        public string StoreName { get; set; }

        public long TaxMaxSequenceNo { get; set; }

        public long TaxMinSequenceNo { get; set; }

        public string TaxRegistrationNumber { get; set; }

        public string TaxRegistrationText { get; set; }
        public string WebsiteURL { get; set; }
        public string EmailID { get; set; }
        public bool RunApplicationsSubStoreLevel { get; set; }
        public string ServerHotFolderPath { get; set; }
        public bool IsActiveStockShot { get; set; }
        public bool RunImageProcessingEngineLocationWise { get; set; }
        public bool RunVideoProcessingEngineLocationWise { get; set; }

        public bool IsTaxIncluded { get; set; }
        public bool IsOnline { get; set; }
        public bool? PlaceSpecOrderAcrossSites { get; set; }
        public bool RunManualDownloadLocationWise { get; set; }

        
    }
}

