﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DigiPhoto.IMIX.Model
{
   public class MappedPos       
   {
       public long ImixPOSDetailID { get; set; }

       public string SystemName { get; set; }

       public long SubStoreID { get; set; }

       public string SubStoreName { get; set; }

       public string UniqueCode { get; set; }

       public string MACAddress { get; set; }

       public string IPAddress { get; set; }
    }
}
