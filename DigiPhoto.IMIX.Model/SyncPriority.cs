﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DigiPhoto.IMIX.Model
{
    public class SyncPriority
    {
        public long ApplicationObjectID { get; set; }
        public string ApplicationObjectName { get; set; }
        public int OnlineSyncPriority { get; set; }
    }
}

