﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DigiPhoto.IMIX.Model
{
    public class FolderStructureInfo
    {
        public string HotFolderPath { get; set; }
        public string BorderPath { get; set; }
        public string BackgroundPath { get; set; }
        public string GraphicPath { get; set; }
        public string CroppedPath { get; set; }
        public string EditedImagePath { get; set; }
       // public string GreenImagePath { get; set; }
        public string ThumbnailsPath { get; set; }
        public string BigThumbnailsPath { get; set; }
        public string PrintImagesPath { get; set; }
    }
}
