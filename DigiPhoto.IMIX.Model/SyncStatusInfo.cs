﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DigiPhoto.IMIX.Model
{
    
    public class SyncStatusInfo
    {
        //private string status;
        public string Syncstatus
        {
            get;
            set;
        }

        //private string ordernumber;


        public string SyncOrderNumber
        {
            get;
            set;
        }

        //private QR Code;

        public string QRCode
        {
            get;
            set;
        }

        //private DateTime? OrderDate;


        public DateTime? SyncOrderdate
        {
            get;
            set;
        }
        // private string photoNumber;


        public string PhotoRfid
        {
            get;
            set;
        }

        //private DateTime? Date;


        public DateTime? Syncdate
        {
            get;
            set;
        }
        public string syncDateDisplay
        {
            get;
            set;
        }

        public int DGOrderspkey
        {
            get;
            set;
        }
        public int SyncStatusID
        {
            get;
            set;
        }

        public bool? IsAvailable { get; set; }
        public Int64 ChangeTrackingId
        {
            get;
            set;
        }
        public String ImageSynced { get; set; }
		 public long SyncFormID { get; set; }
        public DateTime SyncFormTransDate { get; set; }
        public string Form { get; set; }
    }
}
