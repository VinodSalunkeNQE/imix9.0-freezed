﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DigiPhoto.IMIX.Model
{
    public class ReportParams
    {
        public string ReportType { get; set; }
        public Dictionary<string, string> ReportFormats { get; set; }
    }

}
