﻿
using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Effects;
namespace DigiPhoto.IMIX.Model
{
    /// <summary>
    /// This class is a  model of a Watermark on Client view added by Ajay on 10 Feb 2020
    /// </summary>
    public class WatermarkModel : INotifyPropertyChanged
    {
        #region Members

        FontFamily _TextFontfamily { get; set; }

        FontStyle _FontStyle { get; set; }
        FontWeight _TextFontWeights { get; set; }
        int _TextFontSize;
        string _TextFontColour = string.Empty;
        string _WatermarkTextPreview { get; set; }
        string _WatermarkText = string.Empty;
        public Brush _WatermarkPreviewForeground;
        bool _IsBold = false;
        bool _IsUnderline = false;
        bool _IsSelected { get; set; }
        bool _IsBlurChecked { get; set; }

        Color _colorSelected { get; set; }
        Color _TextFontColor { get; set; }
        double _Opacity { get; set; }
        bool _ShowStackPanel { get; set; }
        bool _NumericOnly { get; set; }
        #endregion


        #region Properties
        /// <summary>
        /// The artist name.
        /// </summary>

        private bool isChecked;
        public bool IsChecked
        {
            get { return isChecked; }
            set
            {
                isChecked = value;
                OnPropertyChanged("IsChecked");
            }
        }
        public int SubStoreId
        {
            get;
            set;
        }
        public FontFamily TextFontfamilyNames
        {
            get { return _TextFontfamily; }
            //set { _TextFontfamily = value; }
            set
            {
                _TextFontfamily = value;
                OnPropertyChanged("TextFontfamilyNames");
            }
        }               
        private Brush _ForegroundText { get; set; }
        public Brush ForegroundText
        {
            get { return _ForegroundText; }
            //set { _TextFontfamily = value; }
            set
            {
                _ForegroundText = value;
                OnPropertyChanged("ForegroundText");
            }
        }
        public bool NumericOnly
        {
            get { return _NumericOnly; }
            //set { _TextFontfamily = value; }
            set
            {
                _NumericOnly = IsTextNumeric(value);
                OnPropertyChanged("NumericOnly");
            }
        }
        private bool IsTextNumeric(bool str)
        {
            System.Text.RegularExpressions.Regex reg = new System.Text.RegularExpressions.Regex("[^0-9]");

            return reg.IsMatch(Convert.ToString(str));
        }
        public Color ColorSelected
        {
            get { return _colorSelected; }
            //set { _TextFontfamily = value; }
            set
            {
                _colorSelected = value;
                OnPropertyChanged("ColorSelected");
            }
        }
        public Brush WatermarkPreviewForeground
        {
            get { return _WatermarkPreviewForeground; }
            //set { _TextFontfamily = value; }
            set
            {
                _WatermarkPreviewForeground = value;
                OnPropertyChanged("WatermarkPreviewForeground");
            }
        }

        public string WatermarkTextPreview
        {
            get { return _WatermarkTextPreview; }
            //set { _TextFontfamily = value; }
            set
            {
                _WatermarkTextPreview = value;
                OnPropertyChanged("WatermarkTextPreview");
            }
        }

        

        public Color TextFontColor
        {
            get { return _TextFontColor; }
            //set { _TextFontfamily = value; }
            set
            {
                _TextFontColor = value;
                OnPropertyChanged("TextFontColor");
            }
        }
        public bool IsSelected
        {
            get
            {
                return _IsSelected;
            }
            set
            {
                _IsSelected = value;
                OnPropertyChanged("IsSelected");
            }
        }


        const string Msg1 = "blah 1";
        const string Msg2 = "blah 2";

        public string MyBoundMessage { get; set; }

        public bool IsBlurChecked
        {
            get { return _IsBlurChecked; }
            set
            {
                if (_IsBlurChecked == value) return;

                _IsBlurChecked = value;
                OnPropertyChanged("IsBlurChecked");
            }
        }
        private BlurEffect _blurEffect;

        public BlurEffect Blureffect
        {
            get { return _blurEffect; }
            set
            {
                _blurEffect = value;
            }
        }
        public bool ShowStackPanel
        {
            get
            {
                return _ShowStackPanel;
            }
            set
            {
                _ShowStackPanel = value;
                OnPropertyChanged("ShowStackPanel");
            }
        }

        public string WatermarkText
        {
            get { return _WatermarkText; }
            //set { _TextFontfamily = value; }
            set
            {
                _WatermarkText = value;
                OnPropertyChanged("WatermarkText");
            }
        }

        public bool IsUnderline
        {
            get { return _IsUnderline; }
            //set { _TextFontfamily = value; }
            set
            {
                _IsUnderline = value;
                OnPropertyChanged("_IsUnderline");
            }
        }

        public FontWeight TextFontWeights
        {
            get { return _TextFontWeights; }
            //set { _TextFontWeights = value; }
            set
            {
                _TextFontWeights = value;
                OnPropertyChanged("TextFontWeights");
            }
        }

        public double Opacity
        {
            get { return _Opacity; }
            //set { _TextFontWeights = value; }
            set
            {
                _Opacity = value;
                OnPropertyChanged("Opacity");
            }
        }

        public int TextFontSize
        {
            get { return _TextFontSize; }
            //set { _TextFontWeights = value; }
            set
            {
                _TextFontSize = value;
                OnPropertyChanged("TextFontSize");
            }
        }

        public FontStyle FontStyle
        {
            get { return _FontStyle; }
            //set { _TextFontWeights = value; }
            set
            {
                _FontStyle = value;
                OnPropertyChanged("FontStyle");
            }
        }
        public Int32 UserId { get; set; }






        /// <summary>
        /// The Watermark title.
        /// </summary>

        #endregion

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged(string propertyName)
        {
            if (this.PropertyChanged != null)
                this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }
    }

}
