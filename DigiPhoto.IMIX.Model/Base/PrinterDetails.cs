﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;

namespace DigiPhoto.IMIX.Model
{
    public class PrinterDetails
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PrinterDetails"/> class.
        /// </summary>
        /// <param name="printername">The printername.</param>
        /// <param name="printerjOb">The printerj object.</param>
        /// <param name="questatus">The questatus.</param>
        public PrinterDetails(string printername, ObservableCollection<PrinterJobInfo> printerjOb, string questatus)
        {
            this.PrinterName = printername + " (" + questatus + ")";
            this.PrinterJOb = printerjOb;
            this.PrinterStatus = questatus;
        }
        public string PrinterName { get; set; }
        public ObservableCollection<PrinterJobInfo> PrinterJOb { get; set; }
        public string PrinterStatus { get; set; }


    }
}
