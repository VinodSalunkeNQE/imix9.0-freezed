﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DigiPhoto.IMIX.Model
{
    public class ProductTypeInfo
    {
        public int DG_Orders_ProductType_pkey { get; set; }

        public string DG_Orders_ProductType_Name { get; set; }

        public string DG_Orders_ProductType_Desc { get; set; }

        public bool? DG_Orders_ProductType_IsBundled { get; set; }

        public bool? DG_Orders_ProductType_DiscountApplied { get; set; }

        public string DG_Orders_ProductType_Image { get; set; }

        public bool DG_IsPackage { get; set; }

        public int DG_MaxQuantity { get; set; }

        public bool? DG_Orders_ProductType_Active { get; set; }

        public bool? DG_IsActive { get; set; }

        public bool? DG_IsAccessory { get; set; }

        public bool? DG_IsTaxEnabled { get; set; }

        public bool? DG_IsPrimary { get; set; }

        public string DG_Orders_ProductCode { get; set; }

        public int? DG_Orders_ProductNumber { get; set; }

        public bool? DG_IsBorder { get; set; }

        public string SyncCode { get; set; }

        public bool IsSynced { get; set; }


        public double DG_Product_Pricing_ProductPrice { get; set; }

        public int DG_Product_Pricing_Currency_ID { get; set; }

        public int Itemcount { get; set; }
        public int IsPrintType { get; set; }
        public bool IsChecked { get; set; }
         
        public bool? DG_IsWaterMarkIncluded { get; set; }
        public int DG_SubStore_pkey { get; set; }

        #region Ajay property for panorama selection
        public bool IsPanorama { get; set; }
        #endregion
        public string DG_SubStore_Name { get; set; }////// added by latika for manage product gride substore
        public string DG_Orders_ProductType_ProductCode { get; set; } // Added by Anisur Rahman for Dubai frame (Product code + Barcode) receipt requirement
        #region latika property for PersonalizedAR selection
        public bool? IsPersonalizedAR { get; set; }///added by latika for AR Personalised
        #endregion
    }
}
