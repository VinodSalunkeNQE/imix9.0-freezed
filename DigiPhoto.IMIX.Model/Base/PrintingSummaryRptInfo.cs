﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DigiPhoto.IMIX.Model
{
    public class PrintSummary_Result
    {
        public string DG_Orders_ProductType_Name { get; set; }
        public int PrintedQuantity { get; set; }
        public int PrintedSold { get; set; }
      
    }
}
