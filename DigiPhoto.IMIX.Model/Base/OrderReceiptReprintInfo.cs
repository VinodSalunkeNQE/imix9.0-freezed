﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DigiPhoto.IMIX.Model
{
    public class OrderReceiptReprintInfo
    {
        public long OrderId { get; set; }
        public string OrderNumber { get; set; }
        public int PaymentMode { get; set; }
        public double NetCost { get; set; }
        public double TotalCost { get; set; }
        public string CurrencySymbol { get; set; }
        public double DiscountTotal { get; set; }
        public string PaymentDetail { get; set; }
        public string PhotoIds { get; set; }
        public string QRCode { get; set; }///change made by latika for Re-pring receipt QR Code is not coming. 
    }
}
