﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DigiPhoto.IMIX.Model
{
    public class BillFormatInfo
    {
        public int DG_Bill_Format_pkey { get; set; }

        public int? DG_Bill_Type { get; set; }

        public string DG_Refund_Slogan { get; set; }
    }
}
