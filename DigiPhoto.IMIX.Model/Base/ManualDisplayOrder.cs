﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DigiPhoto.IMIX.Model.Base
{
    public class ManualDisplayOrder
    {
        public Int64 PhotoNumber { get; set; }

        public string FileName { get; set; }

        public Int64 DisplayOrder { get; set; }
    }
}
