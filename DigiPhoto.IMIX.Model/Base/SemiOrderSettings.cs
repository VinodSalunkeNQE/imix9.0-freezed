﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DigiPhoto.IMIX.Model
{
    public class SemiOrderSettings
    {
        public int DG_SemiOrder_Settings_Pkey { get; set; }
        public bool? DG_SemiOrder_Settings_AutoBright { get; set; }
        public double? DG_SemiOrder_Settings_AutoBright_Value { get; set; }
        public bool? DG_SemiOrder_Settings_AutoContrast { get; set; }
        public double? DG_SemiOrder_Settings_AutoContrast_Value { get; set; }
        public string DG_SemiOrder_Settings_ImageFrame { get; set; }
        public bool? DG_SemiOrder_Settings_IsImageFrame { get; set; }
        public string ImageFrame_Horizontal { get; set; }
        public string ImageFrame_Vertical { get; set; }
        public string DG_SemiOrder_ProductTypeId { get; set; }
        public string DG_SemiOrder_Settings_ImageFrame_Vertical { get; set; }
        public bool? DG_SemiOrder_Environment { get; set; }
        public string Background_Horizontal { get; set; }
        public string Background_Vertical { get; set; }
        public string DG_SemiOrder_BG { get; set; }
        public bool? DG_SemiOrder_Settings_IsImageBG { get; set; }
        public string Graphics_layer_Horizontal { get; set; }
        public string DG_SemiOrder_Graphics_layer { get; set; }
        public string Graphics_layer_Vertical { get; set; }
        public string ZoomInfo_Horizontal { get; set; }
        public string DG_SemiOrder_Image_ZoomInfo { get; set; }
        public string ZoomInfo_Vertical { get; set; }
        public int? DG_SubStoreId { get; set; }
        public bool? DG_SemiOrder_IsPrintActive { get; set; }
        public bool? DG_SemiOrder_IsCropActive { get; set; }
        public string VerticalCropValues { get; set; }
        public string HorizontalCropValues { get; set; }
        public int? DG_LocationId { get; set; }
        public string ChromaColor { get; set; }
        public string ColorCode { get; set; }
        public string ClrTolerance { get; set; }
        public string ProductName { get; set; }
        public string TextLogo_Horizontal { get; set; }
        public string TextLogo_Vertical { get; set; }
        public string TextLogos { get; set; }
        
    }

    public class SemiOrderSettingsList
    {
        public SemiOrderSettings SemiOrderSetting { get; set; }
        public int LocationId { get; set; }
    }
}
