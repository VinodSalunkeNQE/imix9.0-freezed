﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DigiPhoto.IMIX.Model
{
    public class ModratePhotoInfo
    {
        public int DG_Mod_Photo_pkey { get; set; }

        public int DG_Mod_Photo_ID { get; set; }

        public DateTime DG_Mod_Date { get; set; }

        public int DG_Mod_User_ID { get; set; }
    }
}
