﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DigiPhoto.IMIX.Model
{
    public class CashBoxInfo
    {
        public int Id { get; set; }

        public string Reason { get; set; }

        public int UserId { get; set; }

        public DateTime CreatedDate { get; set; }
    }
}
