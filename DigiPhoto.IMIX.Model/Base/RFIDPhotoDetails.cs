﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DigiPhoto.IMIX.Model
{
    public class RFIDPhotoDetails
    {
        public DateTime StartDate { get; set; }

        public DateTime EndDate { get; set; }

        public int PhotoGrapherId { get; set; }

        public string FileName { get; set; }
    }
}
