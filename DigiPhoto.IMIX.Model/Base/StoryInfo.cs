﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DigiPhoto.IMIX.Model.Base
{
  public  class StoryInfo
    {
        public int DG_StoryBookId { get; set; }
        public int DG_ThemeId { get; set; }
        public string DG_Theme { get; set; }
        public string DG_PageNo { get; set; }
        public string DG_Text_Chinese { get; set; }
        public string DG_Text_English { get; set; }
        public string DG_Site { get; set; }
        public string DG_Location { get; set; }
        public string DG_CoOrdinate_ChineseText { get; set; }
        public string DG_CoOrdinate_EnglishText { get; set; }
        public string DG_FontSize_English { get; set; }
        public string DG_FontSize_Chinese { get; set; }
        public string DG_FontColor_English { get; set; }
        public string DG_FontColor_Chinese { get; set; }
        public bool DG_IsActive { get; set; }

        public string DG_FontChinese { get; set; }
        public string DG_FontEnglish { get; set; }

    }
}
