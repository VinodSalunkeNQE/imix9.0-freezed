﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DigiPhoto.IMIX.Model
{
    public class Product
    {
        public string ProductName { get; set; }
        public string ProductIcon { get; set; }
        public int ProductID { get; set; }
        public int MaxQuantity { get; set; }
        public bool DiscountOption { get; set; }
        public bool IsBundled { get; set; }
        public bool IsPackage { get; set; }
        public bool IsAccessory { get; set; }
        public bool IsWaterMarked { get; set; }
        public int? IsPrintType { get; set; }
        public bool? IsPersonalizedAR { get; set; }
        public string ProductCode { get; set; } // Added by Anisur Rahman for Dubai frame (Product code + Barcode) receipt requirement
        /// //added by latika for Evoucher 2019Dec21
        /// </summary>
        public string EvoucherCode { get; set; }
        public int EvoucherPer { get; set; }
        public bool? IsEvoucherDisc { get; set; }
        /////ended by latika

        public static class InstaMobileProd
        {
            public static string InstaMobileProductName { get; set; }
        }

    }
}
