﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DigiPhoto.IMIX.Model
{
    public class CheckedItems
    {
        public string SelectetdItems { get; set; }
        public int LineItemId { get; set; }
        public decimal? RefundPrice { get; set; }
        public string _Reason
        {
            get;
            set;
        }
    }
}
