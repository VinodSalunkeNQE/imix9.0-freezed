﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DigiPhoto.IMIX.Model.Base
{
    public class AssociateImageDetails
    {
      public long  IMIXImageAssociationId { get; set; }
      public int IMIXCardTypeId { get; set; }
      public long PhotoId { get; set; }
      public string CardUniqueIdentifier { get; set; }
      public string  MappedIdentifier { get; set; }
      public DateTime  ModifiedDate { get; set; }
      public int IsOrdered { get; set; }
      public int RfidIdentifierId { get; set; }
      public int IsMoved { get; set; }
      public int Nationality { get; set; }
      public string Email { get; set; }
    }
}
