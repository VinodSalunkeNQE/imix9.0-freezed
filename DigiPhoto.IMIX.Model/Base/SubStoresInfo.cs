﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace DigiPhoto.IMIX.Model
{
  public  class SubStoresInfo
  {
      [XmlElement("SiteID")]
      public int DG_SubStore_pkey { get; set; }
      [XmlElement("SiteName")]
      public string DG_SubStore_Name { get; set; }
      [XmlIgnore]
      public string DG_SubStore_Description { get; set; }
      [XmlIgnore]
      public bool DG_SubStore_IsActive { get; set; }
      public string SyncCode { get; set; }
      [XmlIgnore]
      public bool IsSynced { get; set; }
      [XmlIgnore]
      public string DG_SubStore_Locations { get; set; }  
      [XmlIgnore]
      public bool IsLogicalSubStore { get; set; }
      [XmlIgnore]
      public int ? LogicalSubStoreID { get; set; }
       [XmlElement("SiteCode")]
      public string DG_SubStore_Code { get; set; }
      [XmlIgnore]
      public int SiteID { get; set; }
        public bool VisiblVOrderStation { get; set; } ///  /// ////////////// Created by latika for visible in ViewOrderStation 2019 Nov 11
       
        [XmlIgnore]
        public string VisiblVOrderStationlst { get; set; } ///  /// ////////////// Created by latika for visible in ViewOrderStation 2019 Nov 11
  }
  ///changed by latika for Product search
    public class SearchbyInfo
    {
        public string SearchByID { get; set; }
        [XmlIgnore]
        public string SearchBy { get; set; }
       

    }
}
