﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DigiPhoto.IMIX.Model
{
    public class PhotoCaptureInfo
    {
        public int PhotoGrapherId { get; set; }
        public string PhotoName { get; set; }
        public DateTime  SysDate { get; set; }
        public DateTime  CaptureDate { get; set; }
        public string PhotoGrapherName { get; set; }
        public string GroupName { get; set; }
        public string RFIDNo { get; set; }
        public string LocationName { get; set; }
        public string SubstoreName { get; set; }
        public Int32 pkey { get; set; }
        public string CharacterId{ get; set; }
        public string MetaData { get; set; }
        public int LocationId { get; set; }
        public string UniqueIdentifier { get; set; }
		////changed by latika for table workflow
        public string TableName { get; set; }
        public string GuestName { get; set; }
        public string EmailID { get; set; }
        public string ContactNo { get; set; }
        ///end

    }
}
