﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DigiPhoto.IMIX.Model.Base
{
    /// <summary>
    /// Created By Ajinkya For Theme
    /// </summary>
  public  class ThemeInfo
    {
        public int DG_ThemeId { get; set; }
        public string DG_ThemeName { get; set; }
        public bool DG_IsActive { get; set; }
    }
}
