﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DigiPhoto.IMIX.Model
{
    public class PrintLogInfo
    {
        public int ID { get; set; }

        public int PhotoId { get; set; }

        public DateTime PrintTime { get; set; }

        public int ProductTypeId { get; set; }

        public int UserID { get; set; }

    }
}
