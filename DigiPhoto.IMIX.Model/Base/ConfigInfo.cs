﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DigiPhoto.IMIX.Model
{
    public class ConfigInfo
    {
        public int ConfigID { get; set; }
        public int SubStoreID { get; set; }
        public string ConfigKey { get; set; }
        public string ConfigValue { get; set; }

        public int MasterID { get; set; }
      
    }
}
