﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DigiPhoto.IMIX.Model
{
    public class PermissionRoleInfo
    {
        public int DG_Permission_Role_pkey { get; set; }

        public int DG_User_Roles_Id { get; set; }

        public int DG_Permission_Id { get; set; }
    }
}
