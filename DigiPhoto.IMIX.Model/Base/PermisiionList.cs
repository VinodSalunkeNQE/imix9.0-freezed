﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DigiPhoto.IMIX.Model
{
    public class PermisiionList
    {
        public bool? IsAvailable { get; set; }
        public string PermissionName { get; set; }
        public int PermissionId { get; set; }
    }
}
