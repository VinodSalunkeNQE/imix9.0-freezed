﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;


namespace DigiPhoto.IMIX.Model
{
    public class TripCamFeaturesInfo : INotifyPropertyChanged
    {
        private int _Height;
        public int Height
        {
            get
            {
                return _Height;
            }
            set
            {
                _Height = value;
                OnPropertyChanged(new PropertyChangedEventArgs("Height"));
            }
        }

        private int _HeightMax;
        public int HeightMax
        {
            get
            {
                return _HeightMax;
            }
            set
            {
                _HeightMax = value;
                OnPropertyChanged(new PropertyChangedEventArgs("HeightMax"));
            }
        }
        private int _ImageSize;
        public int ImageSize
        {
            get
            {
                return _ImageSize;
            }
            set
            {
                _ImageSize = value;
                OnPropertyChanged(new PropertyChangedEventArgs("ImageSize"));
            }
        }
        private int _OffsetX;
        public int OffsetX
        {
            get
            {
                return _OffsetX;
            }
            set
            {
                _OffsetX = value;
                OnPropertyChanged(new PropertyChangedEventArgs("OffsetX"));
            }
        }
        private int _OffsetY;
        public int OffsetY
        {
            get
            {
                return _OffsetY;
            }
            set
            {
                _OffsetY = value;
                OnPropertyChanged(new PropertyChangedEventArgs("OffsetY"));
            }
        }
        private int _Width;
        public int Width
        {
            get
            {
                return _Width;
            }
            set
            {
                _Width = value;
                OnPropertyChanged(new PropertyChangedEventArgs("Width"));
            }
        }
        private int _WidthMax;
        public int WidthMax
        {
            get
            {
                return _WidthMax;
            }
            set
            {
                _WidthMax = value;
                OnPropertyChanged(new PropertyChangedEventArgs("WidthMax"));
            }
        }
        private float _Hue;
        public float Hue
        {
            get
            {
                return _Hue;
            }
            set
            {
                _Hue = value;
                OnPropertyChanged(new PropertyChangedEventArgs("Hue"));
            }
        }
        private float _Saturation;
        public float Saturation
        {
            get
            {
                return _Saturation;
            }
            set
            {
                _Saturation = value;
                OnPropertyChanged(new PropertyChangedEventArgs("Saturation"));
            }
        }
        private float _WhiteBalanceRed;
        public float WhiteBalanceRed
        {
            get
            {
                return _WhiteBalanceRed;
            }
            set
            {
                _WhiteBalanceRed = value;
                OnPropertyChanged(new PropertyChangedEventArgs("WhiteBalanceRed"));
            }
        }
        private float _WhiteBalanceBlue;
        public float WhiteBalanceBlue
        {
            get
            {
                return _WhiteBalanceBlue;
            }
            set
            {
                _WhiteBalanceBlue = value;
                OnPropertyChanged(new PropertyChangedEventArgs("WhiteBalanceBlue"));
            }
        }
        private long _ImagesCaptured;
        public long ImagesCaptured
        {
            get
            {
                return _ImagesCaptured;
            }
            set
            {
                _ImagesCaptured = value;
                OnPropertyChanged(new PropertyChangedEventArgs("ImagesCaptured"));
            }
        }
        private long _ImagesDropped;
        public long ImagesDropped
        {
            get
            {
                return _ImagesDropped;
            }
            set
            {
                _ImagesDropped = value;
                OnPropertyChanged(new PropertyChangedEventArgs("ImagesDropped"));
            }
        }
        private string _CameraName;
        public string CameraName
        {
            get
            {
                return _CameraName;
            }
            set
            {
                _CameraName = value;
                OnPropertyChanged(new PropertyChangedEventArgs("CameraName"));
            }
        }
        private int _NoOfImages;
        public int NoOfImages
        {
            get
            {
                return _NoOfImages;
            }
            set
            {
                _NoOfImages = value;
                OnPropertyChanged(new PropertyChangedEventArgs("NoOfImages"));
            }
        }
        private float _TriggerDelay;
        public float TriggerDelay
        {
            get
            {
                return _TriggerDelay;
            }
            set
            {
                _TriggerDelay = value;
                OnPropertyChanged(new PropertyChangedEventArgs("TriggerDelay"));
            }
        }
        private float _ExposureTime;
        public float ExposureTime
        {
            get
            {
                return _ExposureTime;
            }
            set
            {
                _ExposureTime = value;
                OnPropertyChanged(new PropertyChangedEventArgs("ExposureTime"));
            }
        }
        private int _CameraRotation;
        public int CameraRotation
        {
            get
            {
                return _CameraRotation;
            }
            set
            {
                _CameraRotation = value;
                OnPropertyChanged(new PropertyChangedEventArgs("CameraRotation"));
            }
        }
        private string _DeviceTemperatureSelector;
        public string DeviceTemperatureSelector
        {
            get
            {
                return _DeviceTemperatureSelector;
            }
            set
            {
                _DeviceTemperatureSelector = value;
                OnPropertyChanged(new PropertyChangedEventArgs("DeviceTemperatureSelector"));
            }
        }
        private double _DeviceTemperature;
        public double DeviceTemperature
        {
            get
            {
                return _DeviceTemperature;
            }
            set
            {
                _DeviceTemperature = value;
                OnPropertyChanged(new PropertyChangedEventArgs("DeviceTemperature"));
            }
        }
        private long _CCDTemperatureOK;
        public long CCDTemperatureOK
        {
            get
            {
                return _CCDTemperatureOK;
            }
            set
            {
                _CCDTemperatureOK = value;
                OnPropertyChanged(new PropertyChangedEventArgs("CCDTemperatureOK"));
            }
        }
        private int _StrobeDelay;
        public int StrobeDelay
        {
            get
            {
                return _StrobeDelay;
            }
            set
            {
                _StrobeDelay = value;
                OnPropertyChanged(new PropertyChangedEventArgs("StrobeDelay"));
            }
        }
        private int _StrobeDuration;
        public int StrobeDuration
        {
            get
            {
                return _StrobeDuration;
            }
            set
            {
                _StrobeDuration = value;
                OnPropertyChanged(new PropertyChangedEventArgs("StrobeDuration"));
            }
        }
        private string _StrobeDurationMode;
        public string StrobeDurationMode
        {
            get
            {
                return _StrobeDurationMode;
            }
            set
            {
                _StrobeDurationMode = value;
                OnPropertyChanged(new PropertyChangedEventArgs("StrobeDurationMode"));
            }
        }
        private string _StrobeSource;
        
       
        public string StrobeSource
        {
            get
            {
                return _StrobeSource;
            }
            set
            {
                _StrobeSource = value;
                OnPropertyChanged(new PropertyChangedEventArgs("StrobeSource"));
            }
        }
        private int _PacketSize;
        public int PacketSize
        {
            get
            {
                return _PacketSize;
            }
            set
            {
                _PacketSize = value;
                OnPropertyChanged(new PropertyChangedEventArgs("PacketSize"));
            }
        }
        private string _MACAddress;
        public string MACAddress
        {
            get
            {
                return _MACAddress;
            }
            set
            {
                _MACAddress = value;
                OnPropertyChanged(new PropertyChangedEventArgs("MACAddress"));
            }
        }
        private string _IPConfigurationMode;
        public string IPConfigurationMode
        {
            get
            {
                return _IPConfigurationMode;
            }
            set
            {
                _IPConfigurationMode = value;
                OnPropertyChanged(new PropertyChangedEventArgs("IPConfigurationMode"));
            }
        }
        private string _IPAddress;
        public string IPAddress
        {
            get
            {
                return _IPAddress;
            }
            set
            {
                _IPAddress = value;
                OnPropertyChanged(new PropertyChangedEventArgs("IPAddress"));
            }
        }
        private string _Subnet;
        public string Subnet
        {
            get
            {
                return _Subnet;
            }
            set
            {
                _Subnet = value;
                OnPropertyChanged(new PropertyChangedEventArgs("Subnet"));
            }
        }
        private string _Gateway;
        public string Gateway
        {
            get
            {
                return _Gateway;
            }
            set
            {
                _Gateway = value;
                OnPropertyChanged(new PropertyChangedEventArgs("Gateway"));
            }
        }
        private string _PixelFormat;
        public string PixelFormat
        {
            get
            {
                return _PixelFormat;
            }
            set
            {
                _PixelFormat = value;
                OnPropertyChanged(new PropertyChangedEventArgs("PixelFormat"));
            }
        }
        private string _TriggerSource;
        public string TriggerSource
        {
            get
            {
                return _TriggerSource;
            }
            set
            {
                _TriggerSource = value;
                OnPropertyChanged(new PropertyChangedEventArgs("TriggerSource"));
            }
        }
        

        private double _AquisitionFrameRate;
        public double AquisitionFrameRate
        {
            get
            {
                return _AquisitionFrameRate;
            }
            set
            {
                _AquisitionFrameRate = value;
                OnPropertyChanged(new PropertyChangedEventArgs("AquisitionFrameRate"));
            }
        }

        private int _LensFocus;
        public int LensFocus
        {
            get
            {
                return _LensFocus;
            }
            set
            {
                _LensFocus = value;
                OnPropertyChanged(new PropertyChangedEventArgs("LensFocus"));
            }
        }
        private int _LensFocusMax;
        public int LensFocusMax
        {
            get
            {
                return _LensFocusMax;
            }
            set
            {
                _LensFocusMax = value;
                OnPropertyChanged(new PropertyChangedEventArgs("LensFocusMax"));
            }
        }
        private double _Aperture;
        public double Aperture
        {
            get
            {
                return _Aperture;
            }
            set
            {
                _Aperture = value;
                OnPropertyChanged(new PropertyChangedEventArgs("Aperture"));
            }
        }
        private double _ApertureMin;
        public double ApertureMin
        {
            get
            {
                return _ApertureMin;
            }
            set
            {
                _ApertureMin = value;
                OnPropertyChanged(new PropertyChangedEventArgs("ApertureMin"));
            }
        }
        private double _ApertureMax;
        public double ApertureMax
        {
            get
            {
                return _ApertureMax;
            }
            set
            {
                _ApertureMax = value;
                OnPropertyChanged(new PropertyChangedEventArgs("ApertureMax"));
            }
        }

        private int _StreamBytesPerSec;
        public int StreamBytesPerSec
        {
            get
            {
                return _StreamBytesPerSec;
            }
            set
            {
                _StreamBytesPerSec = value;
                OnPropertyChanged(new PropertyChangedEventArgs("StreamBytesPerSec"));
            }
        }

        private float _Gamma;
        public float Gamma
        {
            get
            {
                return _Gamma;
            }
            set
            {
                _Gamma = value;
                OnPropertyChanged(new PropertyChangedEventArgs("Gamma"));
            }
        }
        private float _BlackLevel;
        public float BlackLevel
        {
            get
            {
                return _BlackLevel;
            }
            set
            {
                _BlackLevel = value;
                OnPropertyChanged(new PropertyChangedEventArgs("BlackLevel"));
            }
        }
        private float _Gain;
        public float Gain
        {
            get
            {
                return _Gain;
            }
            set
            {
                _Gain = value;
                OnPropertyChanged(new PropertyChangedEventArgs("Gain"));
            }
        }

        private float _Gain00;
        public float Gain00
        {
            get
            {
                return _Gain00;
            }
            set
            {
                _Gain00 = value;
                OnPropertyChanged(new PropertyChangedEventArgs("Gain00"));
            }
        }

        private float _Gain01;
        public float Gain01
        {
            get
            {
                return _Gain01;
            }
            set
            {
                _Gain01 = value;
                OnPropertyChanged(new PropertyChangedEventArgs("Gain01"));
            }
        }

        private float _Gain02;
        public float Gain02
        {
            get
            {
                return _Gain02;
            }
            set
            {
                _Gain02 = value;
                OnPropertyChanged(new PropertyChangedEventArgs("Gain02"));
            }
        }

        private float _Gain10;
        public float Gain10
        {
            get
            {
                return _Gain10;
            }
            set
            {
                _Gain10 = value;
                OnPropertyChanged(new PropertyChangedEventArgs("Gain10"));
            }
        }

        private float _Gain11;
        public float Gain11
        {
            get
            {
                return _Gain11;
            }
            set
            {
                _Gain11 = value;
                OnPropertyChanged(new PropertyChangedEventArgs("Gain11"));
            }
        }

        private float _Gain12;
        public float Gain12
        {
            get
            {
                return _Gain12;
            }
            set
            {
                _Gain12 = value;
                OnPropertyChanged(new PropertyChangedEventArgs("Gain12"));
            }
        }

        private float _Gain20;
        public float Gain20
        {
            get
            {
                return _Gain20;
            }
            set
            {
                _Gain20 = value;
                OnPropertyChanged(new PropertyChangedEventArgs("Gain20"));
            }
        }

        private float _Gain21;
        public float Gain21
        {
            get
            {
                return _Gain21;
            }
            set
            {
                _Gain21 = value;
                OnPropertyChanged(new PropertyChangedEventArgs("Gain21"));
            }
        }

        private float _Gain22;
        public float Gain22
        {
            get
            {
                return _Gain22;
            }
            set
            {
                _Gain22 = value;
                OnPropertyChanged(new PropertyChangedEventArgs("Gain22"));
            }
        }

        private string _ColorTransformationMode;
        public string ColorTransformationMode
        {
            get
            {
                return _ColorTransformationMode;
            }
            set
            {
                _ColorTransformationMode = value;
                OnPropertyChanged(new PropertyChangedEventArgs("ColorTransformationMode"));
            }
        }
        
        private string _ExposureMode;
        public string ExposureMode
        {
            get
            {
                return _ExposureMode;
            }
            set
            {
                _ExposureMode = value;
                OnPropertyChanged(new PropertyChangedEventArgs("ExposureMode"));
            }
        }


        private string _SyncOutPolarity;
        public string SyncOutPolarity
        {
            get
            {
                return _SyncOutPolarity;
            }
            set
            {
                _SyncOutPolarity = value;
                OnPropertyChanged(new PropertyChangedEventArgs("SyncOutPolarity"));
            }
        }

        private string _SyncOutSelector;
        public string SyncOutSelector
        {
            get
            {
                return _SyncOutSelector;
            }
            set
            {
                _SyncOutSelector = value;
                OnPropertyChanged(new PropertyChangedEventArgs("SyncOutSelector"));
            }
        }

        private string _SyncOutSource;
        public string SyncOutSource
        {
            get
            {
                return _SyncOutSource;
            }
            set
            {
                _SyncOutSource = value;
                OnPropertyChanged(new PropertyChangedEventArgs("SyncOutSource"));
            }
        }

       private double _MaxStreamByteValue;
       public double MaxStreamByteValue
        {
            get
            {
                return _MaxStreamByteValue;
            }
            set
            {
                _MaxStreamByteValue = value;
                OnPropertyChanged(new PropertyChangedEventArgs("MaxStreamByteValue"));
            }
        }
       public string AcquisitionMode = "Continuous";

        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged(PropertyChangedEventArgs e)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, e);
            }
        }

      
      
    }


    public class SliderInfo
    {
        public double MinValue { get; set; }
        public double MaxValue { get; set; }
        public string PropertyName { get; set; }
        public bool IsFloat { get; set; }
        public double tickFrequency { get; set; }

    }
}
