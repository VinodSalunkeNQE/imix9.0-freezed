﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DigiPhoto.IMIX.Model
{
    public class GetLocationPerformance_Result
    {
        public string selectedSubStore { get; set; }
        public int Number_of_Capture { get; set; }
        public int Number_of_Sales { get; set; }
        public int ImageSold { get; set; }
        public decimal Revenue { get; set; }
        public string DG_Location_Name { get; set; }
        public string StoreName { get; set; }
        public string Printedby { get; set; }
        public int DG_Location_pkey { get; set; }
        public string DG_SubStore_Name { get; set; }
        public int Shots_Previewed { get; set; }
        public string DataFlag { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public DateTime SecondFromDate { get; set; }
        public DateTime SecondToDate { get; set; }
        public decimal AveragePrice { get; set; }
        public decimal SellThru { get; set; }
        public string defaultCurrency { get; set; }
        public decimal TotalSiteRevenue { get; set; }
        public string UserName { get; set; }
        public int DG_User_pkey{ get; set; }


    }
}
