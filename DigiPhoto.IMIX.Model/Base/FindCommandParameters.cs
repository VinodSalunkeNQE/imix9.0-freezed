﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DigiPhoto.IMIX.Model
{
    public class FindCommandParameters
    {
        public string Text { get; set; }
        public string IgnoreCase { get; set; }
    }
}
