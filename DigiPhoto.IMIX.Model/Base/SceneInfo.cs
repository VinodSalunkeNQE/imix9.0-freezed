﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DigiPhoto.IMIX.Model
{
    public class SceneInfo
    {
        public int SceneId { get; set; }
        public int ProductId { get; set; }
        public string ProductName { get; set; }
        public int BackGroundId { get; set; }
        public string BackgroundName { get; set; }
        public int BorderId { get; set; }
        public string BorderName { get; set; }
        public int GraphicsId { get; set; }
        public string GraphicName { get; set; }
        public string SyncCode { get; set; }
        public bool IsSynced { get; set; }
        public bool IsActive { get; set; }
        public bool IsBlankPage { get; set; } 
        public string SceneName { get; set; }
        public int ThemeId { get; set; }   //Added by Ajinkya
        public string ThemeName { get; set; } //Added by Ajinkya
        public string PageNo { get; set; }  //Added by Ajinkya
        public string ChineseFont { get; set; }  //Added by Ajinkya
        public string EnglishFont { get; set; }  //Added by Ajinkya
    }
}
