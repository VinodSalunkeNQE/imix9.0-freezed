﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DigiPhoto.IMIX.Model
{
    public class CharacterInfo
    {
        public int DG_Character_Pkey { get; set; }

        public string DG_Character_Name { get; set; }

        public int DG_Character_IsActive { get; set; }
        public DateTime DG_Character_CreatedDate { get; set; }

        public int DG_Character_OperationType { get; set; }

    }
}
