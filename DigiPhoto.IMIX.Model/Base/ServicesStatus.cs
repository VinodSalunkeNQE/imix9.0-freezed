﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DigiPhoto.IMIX.Model
{
   public class ServicesStatus
    {
        private string _serviceName;
        public string ServiceName
        {
            get { return _serviceName; }
            set { _serviceName = value; }
        }

        private string _runningStatus;
        public string RunningStatus
        {
            get { return _runningStatus; }
            set { _runningStatus = value; }
        }
        private string _buttonText;
        public string ButtonText
        {
            get { return _buttonText; }
            set { _buttonText = value; }
        }
        private string _originalservicename;

        public string Originalservicename
        {
            get { return _originalservicename; }
            set { _originalservicename = value; }
        }
        private string _servicePath;

        public string ServicePath
        {
            get { return _servicePath; }
            set { _servicePath = value; }
        }
        private bool? _isInterface;

        public bool? IsInterface
        {
            get { return _isInterface; }
            set { _isInterface = value; }
        }

        private string _backOffsetColor;
        public string BackOffsetColor
        {
            get { return _backOffsetColor; }
            set { _backOffsetColor = value; }
        }

        private string _backColor;
        public string BackColor
        {
            get { return _backColor; }
            set { _backColor = value; }
        }
    }
}
