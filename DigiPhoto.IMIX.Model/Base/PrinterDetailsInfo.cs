﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DigiPhoto.IMIX.Model
{
    public class PrinterDetailsInfo
    {
        public string PrinterName { get; set; }
        public int PrinterID { get; set; }
    }
}
