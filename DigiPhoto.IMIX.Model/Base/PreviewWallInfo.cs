﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DigiPhoto.IMIX.Model.Base
{
    public class PreviewWallInfo
    {
        public string LocationNamePW { get; set; }
        public int LocationIDPW { get; set; }
        public int NoOfScreenPW { get; set; }
        public int DelayTimePW { get; set; }
        public int PreviewTimePW { get; set; }
        public int HighLightTimePW { get; set; }
        public bool IsMktImgPW { get; set; }
        public string WaterMarkPathPW { get; set; }
        public string VerticalBorderPathPW { get; set; }
        public string HorizentalBorderPathPW { get; set; }
        public string MarketingImgPathPW { get; set; }
        public bool IsLogoPW { get; set; }
        public string LogoPathPW { get; set; }
        public bool IsPreviewEnabledPW { get; set; }
        public bool IsSpecImgPW { get; set; }
        public string LogoPosition { get; set; }
        public string RFIDPostion { get; set; }
        public string IsVisibleGumBallZeroScore { get; set; }

        public string GuestExplanatoryImgPathPW { get; set; }
        public bool IsHorizontalGuestExplatory { get; set; }
        public bool IsLoopPreviewPhotos { get; set; }
        public string LoopPreviewPhotosTime { get; set; }
        public string RideNoOfCaptures { get; set; }
        public string RideNoOfCapturesIterations { get; set; }

    }
}
