﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DigiPhoto.IMIX.Model
{
    public class TripCamSettingInfo
    {
        public int TripCamValueId { get; set; }

        public long TripCamSettingsMasterId { get; set; }

        public string SettingsValue { get; set; }

        public int CameraId { get; set; }

        public DateTime ModifiedDate { get; set; }
    }

    public class TripCamInfo
    {
        public int Camera_pKey { get; set; }
        public string CameraModel { get; set; }
        public string CameraFolderpath { get; set; }
        public int TripCamTypeId { get; set; }

    }
}
