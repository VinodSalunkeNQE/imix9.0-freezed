﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DigiPhoto.IMIX.Model
{
    public class LinetItemsDetails
    {
        string productname;
        public string Productname
        {
            get { return productname; }
            set { productname = value; }
        }

        // // Added by Anisur Rahman for Dubai frame (Product code + Barcode) receipt requirement
        string productcode;
        public string Productcode
        {
            get { return productcode; }
            set { productcode = value; }
        }

        string productprice;

        public string Productprice
        {
            get { return productprice; }
            set { productprice = value; }
        }

        string productquantity;

        public string Productquantity
        {
            get { return productquantity; }
            set { productquantity = value; }
        }

        double discount;
        public double Discount
        {
            get { return discount; }
            set { discount = value; }
        }

        string arabicname;
        public string ArabicName
        {
            get { return arabicname; }
            set { arabicname = value; }
        }

        string qrcode;
        public string QRCode
        {
            get { return qrcode; }
            set { qrcode = value; }
        }
    }
}
