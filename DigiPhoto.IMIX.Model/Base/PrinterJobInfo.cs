﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DigiPhoto.IMIX.Model
{
    public class PrinterJobInfo
    {

        private string _jobName;

        public string JobName
        {
            get { return _jobName; }
            set { _jobName = value; }
        }
        private int _jobId;

        public int JobId
        {
            get { return _jobId; }
            set { _jobId = value; }
        }

        private string _jobStatus;

        public string JobStatus
        {
            get { return _jobStatus; }
            set { _jobStatus = value; }
        }
        private string _printername;

        public string Printername
        {
            get { return _printername; }
            set { _printername = value; }
        }
        private long _imageId;
        public long ImageId
        {
            get { return _imageId; }
            set { _imageId = value; }
        }
        private string Filepath;
        public string Filepath1
        {
            get { return Filepath; }
            set { Filepath = value; }
        }

        public string DG_Orders_ProductType_Name { get; set; }
        public string DG_Orders_Number { get; set; }
        public string RFID { get; set; }
        public int DG_Orders_LineItems_pkey { get; set; }
        public string PhotoID { get; set; }
    }
}
