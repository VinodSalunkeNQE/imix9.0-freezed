﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DigiPhoto.IMIX.Model
{
public    class BackupHistory
{
    public int BackupId { get; set; }

    public DateTime ScheduleDate { get; set; }

    public DateTime StartDate { get; set; }

    public DateTime EndDate { get; set; }

    public int Status { get; set; }
    //----Code Added by Anis 18-Jan-19-------
    public string SetVisibility { get; set; }

    public string ErrorMessage { get; set; }

    public int SubStoreId { get; set; }
    //-----------Code Added by Manoj at 26-Dec-2018 for cleanup status-------
   public int CleanupStatus { get; set; }
    }
}
