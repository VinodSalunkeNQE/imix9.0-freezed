﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DigiPhoto.IMIX.Model
{
    public class BackGroundInfo : BaseDataModel,ICloneable
    {
        int backgroundId;
        public int DG_Background_pkey
        {
            get
            {
                return backgroundId;
            }
            set
            {
                backgroundId = value;
                PropertyModified("DG_Background_pkey");
            }
        }
        int productId;
        public int DG_Product_Id
        {
            get
            {
                return productId;
            }
            set
            {
                productId = value;
                PropertyModified("DG_Product_Id");
            }
        }
        string backgroundImageName;
        public string DG_BackGround_Image_Name
        {
            get
            {
                return backgroundImageName;
            }
            set
            {
                backgroundImageName = value;
                PropertyModified("DG_BackGround_Image_Name");
            }
        }
        string backgroundImageDisplayName;
        public string DG_BackGround_Image_Display_Name
        {
            get
            {
                return backgroundImageDisplayName;
            }
            set
            {
                backgroundImageDisplayName = value;
                PropertyModified("DG_BackGround_Image_Display_Name");
            }
        }
        int? backgroundGroupId;
        public int? DG_BackGround_Group_Id
        {
            get
            {
                return backgroundGroupId;
            }
            set
            {
                backgroundGroupId = value;
                PropertyModified("DG_BackGround_Group_Id");
            }
        }
        string syncCode;
        public string SyncCode
        {
            get
            {
                return syncCode;
            }
            set
            {
                syncCode = value;
                PropertyModified("SyncCode");
            }
        }
        bool isSynced;
        public bool IsSynced
        {
            get
            {
                return isSynced;
            }
            set
            {
                isSynced = value;
                PropertyModified("IsSynced");
            }
        }
        string backgroundPath;
        public string DG_BackgroundPath
        {
            get
            {
                return backgroundPath;
            }
            set
            {
                backgroundPath = value;
                PropertyModified("DG_BackgroundPath");
            }
        }
        bool? backgroundIsActive;
        public bool? DG_Background_IsActive
        {
            get
            {
                return backgroundIsActive;
            }
            set
            {
                backgroundIsActive = value;
                PropertyModified("DG_Background_IsActive");
            }
        }

        #region Added by Ajay for adding panoramic background on 27 April 2018 

        bool? backgroundIsPanorama;
        public bool? DG_Background_IsPanorama
        {
            get
            {
                return backgroundIsPanorama;
            }
            set
            {
                backgroundIsPanorama = value;
                PropertyModified("DG_Background_IsPanorama");
            }
        } 
        #endregion

        int? createdBy;
        public int? CreatedBy
        {
            get
            {
                return createdBy;
            }
            set
            {
                createdBy = value;
                PropertyModified("CreatedBy");
            }
        }
        int? modifiedBy;
        public int? ModifiedBy
        {
            get
            {
                return modifiedBy;
            }
            set
            {
                modifiedBy = value;
                PropertyModified("ModifiedBy");
            }
        }

        DateTime? createdDate;
        public DateTime? CreatedDate
        {
            get
            {
                return createdDate;
            }
            set
            {
                createdDate = value;
                PropertyModified("CreatedDate");
            }
        }

        DateTime? modifiedDate;
        public DateTime? ModifiedDate
        {
            get
            {
                return modifiedDate;
            }
            set
            {
                modifiedDate = value;
                PropertyModified("ModifiedDate");
            }
        }
        public string IsActiveLabel
        {
            get
            {
                if (DG_Background_IsActive.HasValue && DG_Background_IsActive.Value)
                    return "Active";
                else
                    return "InActive";
            }
        }
        public string IsPanoramicLabel
        {
            get
            {
                if (DG_Background_IsPanorama.HasValue && DG_Background_IsPanorama.Value)
                    return "Yes";
                else
                    return "No";
            }
        }

        public object Clone()
        {
            return this.MemberwiseClone();
        }
    }
}
