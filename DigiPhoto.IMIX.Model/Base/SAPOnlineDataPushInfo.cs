﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DigiPhoto.IMIX.Model.Base
{
    public class SAPOnlineDataPushInfo
    {
        public Int64 Id { get; set; }
        public string SalesOrderno { get; set; }
        public int ItemNo { get; set; }
        public string Customer { get; set; }
        public string CustomerDescription { get; set; }
        public string SalesOrg { get; set; }
        public string SalesOrgDescription { get; set; }
        public string DistributionChannel { get; set; }
        public string DistributionChannelDescription { get; set; }
        public string Division { get; set; }
        public string DivisionDescription { get; set; }
        public string Material { get; set; }
        public string MaterialDescription { get; set; }
        public string SendingPlant { get; set; }
        public string SendingPlantDescription { get; set; }
        public int Quantity { get; set; }
        public string UOM { get; set; }
        [Required]
        public decimal TotalPrice { get; set; }
        public decimal Cash { get; set; }
        public decimal Visa { get; set; }
        public decimal Master { get; set; }
        public decimal Amex { get; set; }
        public decimal JCB { get; set; }
        public decimal Unionpay { get; set; }
        public decimal Voucher { get; set; }
        public decimal MPU { get; set; }
        public decimal Exchange { get; set; }
        public decimal MobileBanking { get; set; }
        public decimal KVL { get; set; }
        public decimal Room { get; set; }
        public decimal FC { get; set; }
        public decimal Others { get; set; }
        public decimal Tax_deduction { get; set; }
        public decimal Tax { get; set; }
        public decimal Discount { get; set; }
        public DateTime OrderDate { get; set; }
        public string Customerreference { get; set; }
        public string CanceledInvoicelineitem { get; set; }
        public string CanceledBillDoc { get; set; }
        public DateTime Createddate { get; set; }
        public string CreatedTime { get; set; }
        public string CountryCode { get; set; }
        public string PkgSyncCode { get; set; }
        [Required]
        public string Syncstatus { get; set; }
        [Required]
        public string UploadStatus { get; set; }
        public int RetryCount { get; set; }
        public string Field1 { get; set; }
        public string Field2 { get; set; }
    }
}
