﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DigiPhoto.IMIX.Model
{
    public class VersionHistoryInfo
    {
        public int DG_Version_Pkey { get; set; }

        public string DG_Version_Number { get; set; }

        public DateTime DG_Version_Date { get; set; }

        public int DG_UpdatedBY { get; set; }

        public string DG_Machine { get; set; }
    }
}
