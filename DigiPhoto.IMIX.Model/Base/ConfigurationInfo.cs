﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DigiPhoto.IMIX.Model;
using System.Data;

namespace DigiPhoto.IMIX.Model
{
    public class ConfigurationInfo
    {
        public int DG_Config_pkey { get; set; }

        public string DG_Hot_Folder_Path { get; set; }

        public string DG_Frame_Path { get; set; }

        public string DG_BG_Path { get; set; }

        public string DG_Mod_Password { get; set; }

        public int? DG_NoOfPhotos { get; set; }

        public bool? DG_Watermark { get; set; }

        public bool? DG_SemiOrder { get; set; }

        public bool? DG_HighResolution { get; set; }

        public bool? DG_AllowDiscount { get; set; }

        public bool? DG_EnableDiscountOnTotal { get; set; }

        public decimal? WiFiStartingNumber { get; set; }

        public decimal? FolderStartingNumber { get; set; }

        public bool? IsAutoLock { get; set; }

        public bool? DG_SemiOrderMain { get; set; }

        public bool? PosOnOff { get; set; }

        public string DG_ReceiptPrinter { get; set; }

        public bool? DG_IsAutoRotate { get; set; }

        public string DG_Graphics_Path { get; set; }

        public bool? DG_IsCompression { get; set; }

        public bool? DG_IsEnableGroup { get; set; }

        public int? DG_Substore_Id { get; set; }

        public int? DG_NoOfBillReceipt { get; set; }

        public string DG_ChromaColor { get; set; }

        public decimal? DG_ChromaTolerance { get; set; }

        public string DG_DbBackupPath { get; set; }

        public string DG_CleanupTables { get; set; }

        #region Ajay
        public string DG_ArchivedTables { get; set; } 
        #endregion

        public string DG_HfBackupPath { get; set; }

        public string DG_ScheduleBackup { get; set; }

        public bool? DG_IsBackupScheduled { get; set; }

        public double? DG_Brightness { get; set; }

        public double? DG_Contrast { get; set; }

        public int? DG_PageCountGrid { get; set; }

        public int? DG_PageCountPreview { get; set; }

        public int? DG_NoOfPhotoIdSearch { get; set; }

        public bool? IsRecursive { get; set; }

        public int? IntervalCount { get; set; }

        public int? intervalType { get; set; }

        public string DG_MktImgPath { get; set; }

        public int? DG_MktImgTimeInSec { get; set; }

        public string EK_SampleImagePath { get; set; }

        public int? EK_DisplayDuration { get; set; }

        public int? EK_ScreenStartTime { get; set; }

        public bool? EK_IsScreenSaverActive { get; set; }

        public bool? IsDeleteFromUSB { get; set; }

        public int? DG_CleanUpDaysBackUp { get; set; }

        public string FtpIP { get; set; }

        public string FtpUid { get; set; }

        public string FtpPwd { get; set; }

        public string FtpFolder { get; set; }

        public string SyncCode { get; set; }

        public bool IsSynced { get; set; }

        public int? DefaultCurrencyId { get; set; }

        public int DefaultCurrency { get; set; }

        public bool? IsExportReportToAnyDrive { get; set; }
    }
}
