﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DigiPhoto.IMIX.Model
{
   public class BurnImagesInfo
    {
        private int _ImageID;
        public int ImageID
        {
            get { return _ImageID; }
            set { _ImageID = value; }
        }

        private int producttype;

        public int Producttype
        {
            get { return producttype; }
            set { producttype = value; }
        }
    }
}
