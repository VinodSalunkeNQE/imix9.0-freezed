﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DigiPhoto.IMIX.Model;
using System.Data;

namespace DigiPhoto.IMIX.Model
{
    public class PrinterQueueforPrint
    {
        public int DG_PrinterQueue_Pkey { get; set; }

        public int DG_Order_Details_Pkey { get; set; }

        public bool DG_SentToPrinter { get; set; }

        public string DG_AssociatedPrinters_Name { get; set; }

        public string DG_Orders_ProductType_Name { get; set; }

        public string DG_Photos_RFID { get; set; }

        public int DG_Associated_PrinterId { get; set; }

        public int DG_PrinterQueue_ProductID { get; set; }

        public bool is_Active { get; set; }

        public int QueueIndex { get; set; }

        public int DG_Orders_ProductType_pkey { get; set; }

        public string DG_Orders_Number { get; set; }

        public int DG_Orders_pkey { get; set; }

        public int DG_Order_SubStoreId { get; set; }

        public string RotationAngle { get; set; }

        public string DG_PrinterQueue_Image_Pkey { get; set; }
        public string DG_Photos_ID { get; set; }
        public string DG_Order_ImageUniqueIdentifier { get; set; }

        public bool DG_IsSpecPrint { get; set; }
        /// <summary>
        /// //create by latika for table flow
        /// </summary>
        public string TableName { get; set; }
        public string GuestName { get; set; }
        public string EmailID { get; set; }
        public string ContactNo { get; set; }
        public int LocationID { get; set; }
        ////////////end
    }
}
