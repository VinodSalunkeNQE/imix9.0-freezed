﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;


namespace DigiPhoto.IMIX.Model.Base
{
  public class FontInfo : INotifyPropertyChanged
    {

        private string _fontfamily;
        private string _familyText;

        public string FamilyName
        {
            get
            {
                return _fontfamily;
            }
            set
            {
                _fontfamily = value;
                this.NotifyPropertyChanged("FamilyName");
            }
        }
        public string FamilyText
        {
            get
            {
                return _familyText;
            }
            set
            {
                _familyText = value;
                this.NotifyPropertyChanged("FamilyText");
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        private void NotifyPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
