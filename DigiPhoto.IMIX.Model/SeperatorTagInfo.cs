﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DigiPhoto.IMIX.Model
{
    public class SeperatorTagInfo
    {
        public int SeparatorRFIDTagID { get; set; }
        public string TagID { get; set; }
        public DateTime CreatedOn { get; set; }
        public int LocationId { get; set; }
        public string LocationName { get; set; }
        public bool IsActive { get; set; }
    }
}
