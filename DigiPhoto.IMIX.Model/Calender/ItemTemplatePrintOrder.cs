﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DigiPhoto.IMIX.Model
{
    public class ItemTemplatePrintOrderModel
    {
        public Int32 Id { get; set; }
        public Int32 OrderLineItemId { get; set; }
        public Int32 MasterTemplateId { get; set; }
        public Int32 DetailTemplateId { get; set; }
        public Int32 PrintTypeId { get; set; }
        public Int32 PhotoId { get; set; }
        public Int32 PageNo { get; set; }
        public Int32 PrintPosition { get; set; }
        public Int32 RotationAngle { get; set; }
        public Int32 Status { get; set; }
        public Int32 PrinterQueueId { get; set; }
        public string CreatedBy { get; set; }

    }
}
