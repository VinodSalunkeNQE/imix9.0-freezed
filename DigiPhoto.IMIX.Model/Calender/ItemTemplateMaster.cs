﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DigiPhoto.IMIX.Model
{
    public class ItemTemplateMasterModel
    {
        public Int32 Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public Int32 SubtemplateCount { get; set; }
        public Int32 PathType { get; set; }
        public Int32 TemplateType { get; set; }
        public string ThumbnailImagePath { get; set; }
        public string ThumbnailImageName { get; set; }
        public Int32 Status { get; set; }
        public string BasePath { get; set; }
        public List<ItemTemplateDetailModel> ItemTemplateDetailList { get; set; }


    }
    public enum CalenderViewOperationType
    {
        LoadCalenderMasterData,
        LoadCalenderDetailData
    }

}
