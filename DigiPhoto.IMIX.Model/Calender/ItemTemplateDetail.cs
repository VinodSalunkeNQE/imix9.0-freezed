﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DigiPhoto.IMIX.Model
{
    public class ItemTemplateDetailModel
    {
        public Int32 AssociationPhotoId { get; set; }

        public string AssociationPhotoName { get; set; }

        public string AssociationPhotoFileName { get; set; }

        public string AssociationPhotoFilePath { get; set; }

        public Int32 FileOrder { get; set; }

        public Int32 Id { get; set; }
        public Int32 MasterTemplateId { get; set; }
        public Int32 TemplateType { get; set; }
        public string FileName { get; set; }
        public string FilePath { get; set; }
        public Int32 Status { get; set; }
        public Int32 I1_X1 { get; set; }
        public Int32 I1_Y1 { get; set; }
        public Int32 I1_X2 { get; set; }
        public Int32 I1_Y2 { get; set; }

    }
}

