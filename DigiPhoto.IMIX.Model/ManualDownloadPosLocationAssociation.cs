﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DigiPhoto.IMIX.Model
{
    public class ManualDownloadPosLocationAssociation
    {
        public int PoslocationId { get; set; }
        public string PosName { get; set; }
        public int LocationId { get; set; }
        public string LocationName { get; set; }

    }
}
