﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DigiPhoto.IMIX.Model
{
    public class CameraDeviceAssociationInfo
    {
        public int DeviceId { get; set; }
        public int CameraId { get; set; }
    }
}
