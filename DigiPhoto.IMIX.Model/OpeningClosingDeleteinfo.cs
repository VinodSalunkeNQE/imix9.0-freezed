﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DigiPhoto.IMIX.Model
{
   public class OpeningClosingDeleteinfo
    {
        public int TypeID { get; set; }
        public string FDate { get; set; }
        public int UserID { get; set; }
        public string UserName { get; set; }
        public string MachineID { get; set; }
        public string Reason { get; set; }
        public int FormID { get; set; }
        public int SubStoreID { get; set; }
    }
}
