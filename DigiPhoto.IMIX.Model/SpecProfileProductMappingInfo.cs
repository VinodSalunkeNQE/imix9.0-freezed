﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DigiPhoto.IMIX.Model
{
    public class SpecProfileProductMappingInfo
    {
        public int SemiOrderProfileId { get; set; }
        public int ProductTypeId { get; set; }
    }
}
