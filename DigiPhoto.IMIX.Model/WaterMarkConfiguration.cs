﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DigiPhoto.IMIX.Model
{
   public class WaterMarkConfiguration
    {
      public  int LocationId { get; set; }
      public int SubstoreId { get; set; }
      public double ProductPrice { get; set; }
      public TimeSpan ScheduleTime { get; set; }
      public int ProductId { get; set; }
      public bool EnableWaterMark { get; set; }

      public bool IsLocationProcessed { get; set; }
    }

   public class WaterMarkTagsUpload
   {
       public long IMIXImageAssociationId { get; set; }
       public int IMIXCardTypeId { get; set; }
       public int PhotoId { get; set; }
       public string CardUniqueIdentifier { get; set; }
       public int IsOrdered { get; set; }
       public int ImageIdentificationType { get; set; }

   }
}
