﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DigiPhoto.IMIX.Model
{
    public class iMixConfigurationLocationInfo:iMIXConfigurationInfo
    {
        public long ConfigurationLocationValueId { get; set; }
        public string ConfigurationValue { get; set; }
        public int LocationId { get; set; }
    }

    public class iMixConfigurationLocationInfoList
    {
        public List<iMixConfigurationLocationInfo> iMixConfigurationLocationList {get;set;}
        public int SubstoreId { get; set; }
        public int LocationId { get; set; }
    }
}
