﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DigiPhoto.IMIX.Model
{
    public class VideoConfigProfiles
    {
        public long ProfileId { get; set; }
        public string ProfileName { get; set; }
        public string AspectRatio { get; set; }
        public int FrameRate { get; set; }
        public string OutputFormat { get; set; }
        public string VideoCodec { get; set; }
        public string AudioCodec { get; set; }
        public string AutoVideoEffects { get; set; }
        public int LocationId { get; set; }

    }
    public class CGConfigSettings
    {
        public int ID { get; set; }
        public string ConfigFileName { get; set; }
        public string Extension { get; set; }
        public string DisplayName { get; set; }
        public bool IsActive { get; set; }
    }
	////added by latika for presold functioality
    public class Presold
    {
        
        public int PreSoldDelayTime { get; set; }
        public bool IsPrefixActiveFlow { get; set; }
    }

}
