﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DigiPhoto.IMIX.Model
{
    public class OpeningCloseSettinginfo
    {
        #region Class created by latika for management opening and closing form for deletion on 26 Dec 18
        public int ID { get; set; }
        public string Type { get; set; }
        public string Openingclosingdate { get; set; }
        public string DeletedOn { get; set; }

        public string Machine { get; set; }
        public int UserID { get; set; }
        public string UserName { get; set; }
        public int FormID { get; set; }
        public string FormName { get; set; }

        public string VisibilitySett { get; set; }
        public string SubStoreName { get; set; }

        #endregion
    }
}
