﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DigiPhoto.IMIX.Model
{
    public class QRcodes
    {
        public string QRCode { get; set; }
        public string DG_Orders_Number { get; set; }
    }
}