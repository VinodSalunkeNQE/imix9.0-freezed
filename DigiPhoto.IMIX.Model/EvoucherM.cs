﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DigiPhoto.IMIX.Model
{
    public class EvoucherM
    {
    }
    public class EvoucherMaster
    {
        public long EVoucherID { get; set; }
        public int CampTourID { get; set; }
        public int NoofVouchers { get; set; }
        public int CountryID { get; set; }
        public DateTime VoucherValidity { get; set; }
        public int CreatedBy { get; set; }
        public int CreatedDate { get; set; }
        public int Discount { get; set; }
        public int UsageLimit { get; set; }
        public int isActive { get; set; }
        public int IsMoved { get; set; }
        public int ApprovedStatus { get; set; }
        public int ApprovedBy { get; set; }
        public DateTime ApprovedDt { get; set; }
    }
    public class EVoucherProductDetails
    {
        public int ID { get; set; }
        public int EVoucherProductID { get; set; }
        public int EVoucherID { get; set; }
        public int VenueID { get; set; }
        public int ProductID { get; set; }
        public int IsMoved { get; set; }
    }
    public class EVoucherBarcodes
    {
        public long ID { get; set; }
        public long EVoucherBarcodeID { get; set; }
        public int EVoucherID { get; set; }
        public string Barcode { get; set; }
        public bool Redeem { get; set; }
        public bool IsMoved { get; set; }
        public int NoofTimeUsed { get; set; }
        public string OrderNos { get; set; }
    }
    public class EvoucherClaiminfo
    {
        public string Barcode { get; set; }
        public Nullable<decimal> Amount { get; set; }
        public DateTime VoucherValidity { get; set; }
        public bool Redeem { get; set; }
        public string Result { get; set; }
    }
    public class Evoucher
    {
      public long EvoucherID { get; set; }
    }
    public class EvoucherDtlsRpt
    {
        public string EvoucherNo { get; set; }
        public int PhotoCount { get; set; }
    }
}
