﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DigiPhoto.IMIX.Model
{
    public class GetServiceStatus
    {
        public Int32 ServiceId { get; set; }

        public long SubStoreID { get; set; }

        public Int32 Runlevel { get; set; }

        public long iMixPosId { get; set; }

    }
}
