﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DigiPhoto.IMIX.Model
{
    public class ArchivedPhotoInfo
    {
        public string FileName { get; set; }

        public int MediaType { get; set; }

        public int ArchivedPhotoId { get; set; }

        public bool FileDeleted { get; set; }
    }
}
