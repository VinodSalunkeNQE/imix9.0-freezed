﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace DigiPhoto.IMIX.Model
{
    public class PosLocationAssociationModel
    {
        public int PosLocationId { get; set; }
        public string PosName { get; set; }
        public string DG_Location_Name { get; set; }
        public bool IsActive { get; set; }
        public string Settings { get; set; }
        public int IMIXConfigurationValueId { get; set; }
        public string IsActiveName { get; set; }
        public string LocationIDs { get; set; }


    }
}
