﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace DigiPhoto.IMIX.Model
{
    public class PanoramaProperties : INotifyPropertyChanged
    {
        private string _panoramatext;
        private int _panoramavalue;
        private bool _selected;

        ///private int _ProductId;

        public string TheText
        {
            get
            {
                return _panoramatext;
            }
            set
            {
                _panoramatext = value;
                this.NotifyPropertyChanged("TheText");
            }
        }
        public int TheValue
        {
            get
            {
                return _panoramavalue;
            }
            set
            {
                _panoramavalue = value;
                this.NotifyPropertyChanged("TheValue");
            }
        }
        public event PropertyChangedEventHandler PropertyChanged;
        private void NotifyPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        public bool IsSelected
        {
            get
            {
                return _selected;
            }
            set
            {
                _selected = value;
                this.NotifyPropertyChanged("IsSelected");
            }
        }

        


    }
}
