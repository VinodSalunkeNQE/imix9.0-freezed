﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Media;



namespace DigiPhoto.IMIX.Model
{
    public class ReportTypeDetails : BaseDataModel
    {
        int id;
        string reportTypeName;
        bool isActive;
        string reportLabel;
        public int Id
        {
            get { return id; }
            set
            {
                id = value;
                PropertyModified("Id");
            }
        }
        public string ReportTypeName
        {
            get
            {
                return reportTypeName;
            }
            set
            {
                reportTypeName = value;
                PropertyModified("ReportTypeName");
            }
        }
        public bool IsActive
        {
            get { return isActive; }
            set { isActive = value; PropertyModified("IsActive"); }
        }
        public string ReportLabel
        {
            get
            {
                return reportLabel;
            }
            set
            {
                reportLabel = value; PropertyModified("ReportLabel");

            }

        }

    }

    public class ReportConfigurationDetails : BaseDataModel
    {
        static public ReportConfigurationDetails LoadDefaultData()
        {
            return new ReportConfigurationDetails()
            {

                EmailAddress = string.Empty,
                ExportPath = string.Empty,
                IsRecursive = false,
                ReportTypeDetails = new List<ReportTypeDetails>(),
                SelectedHour = -1,
                SelectedMinute = -1,
                SelectedFormat = string.Empty,
                HasError = false,
                ErrorDetails = string.Empty,
                ErrorMessage = string.Empty,
                EmailFormat = string.Empty,

            };

        }
        string exportPath;
        string emailAddress;
        ///string scheduleTime;
        bool isRecursive;
        List<ReportTypeDetails> reportTypeDetails;
        public string ExportPath
        {
            get
            {
                return exportPath;
            }
            set
            {

                exportPath = value;
                PropertyModified("ExportPath");
            }

        }
        public string EmailAddress
        {
            get
            {
                return emailAddress;
            }
            set
            {

                emailAddress = value;
                PropertyModified("EmailAddress");
            }

        }
        public string ScheduleTime
        {
            get
            {
                return string.Format("{0}:{1}", SelectedHour, SelectedMinute);
            }


        }
        public bool IsRecursive
        {
            get
            {
                return isRecursive;
            }
            set
            {

                isRecursive = value;
                PropertyModified("IsRecursive");
            }

        }

        public List<ReportTypeDetails> ReportTypeDetails
        {
            get
            {
                return reportTypeDetails;
            }
            set
            {

                reportTypeDetails = value;
                PropertyModified("ReportTypeDetails");
            }

        }
        public bool HasError { get; set; }
        public string ErrorDetails { get; set; }
        public string ErrorMessage { get; set; }

        public List<int> Hours
        {
            get
            {
                List<int> hours = new List<int>();
                for (int i = 0; i <= 23; i++)
                {
                    hours.Add(i);
                }
                return hours;
            }
        }
        public List<int> Minutes
        {
            get
            {
                List<int> minutes = new List<int>();
                for (int i = 0; i <= 60; i++)
                {
                    minutes.Add(i);
                }
                return minutes;
            }
        }
        public List<string> DateFormat
        {
            get
            {
                return new List<string>() { "AM", "PM" };
            }
        }
        int selectedHour;
        int selectedminute;
        string selectedFormat;
        public int SelectedHour
        {
            get
            {
                return selectedHour;
            }
            set
            {
                selectedHour = value;
                PropertyModified("SelectedHour");
            }
        }
        public int SelectedMinute
        {
            get
            {
                return selectedminute;
            }
            set
            {
                selectedminute = value;
                PropertyModified("SelectedMinute");
            }
        }
        public string SelectedFormat
        {
            get
            {
                return selectedFormat;
            }
            set
            {
                selectedFormat = value;
                PropertyModified("Selectedformat");
            }
        }

        public string StoreName { get; set; }
        public int StoreId { get; set; }
        public string UserName { get; set; }
        public int UserId { get; set; }
        public string LocationName { get; set; }
        public string Validate()
        {

            if (string.IsNullOrEmpty(ExportPath))
                return "Please browse export path.";

            if (string.IsNullOrEmpty(EmailAddress))
                return "Please enter a email address.";

            if ((!Regex.IsMatch(EmailAddress, @"^[a-zA-Z][\w\.-]*[a-zA-Z0-9]@[a-zA-Z0-9][\w\.-]*[a-zA-Z0-9]\.[a-zA-Z][a-zA-Z\.]*[a-zA-Z]$")))
                return "Enter valid mail address.";

            if (SelectedHour == -1 || SelectedMinute == -1)
                return "Please select time";

            return string.Empty;
        }
        public List<ExportServiceLog> ExportServiceLogs
        {
            get;
            set;
        }
        public string EmailFormat { get; set; }
    }


    //public class iMIXStoreConfigurationInfo
    //{
    //    public long iMIXStoreConfigurationId { get; set; }
    //    public long IMIXConfigurationMasterId { get; set; }
    //    public string ConfigurationValue { get; set; }
    //    public string SyncCode { get; set; }
    //    public bool IsSynced { get; set; }
    //    public DateTime ModifiedDate { get; set; }
    //}

    

    public class ExportServiceLog
    {
        public long Id { get; set; }
        public string ReportType { get; set; }
        public bool ReportSent { get; set; }
        public DateTime EventTime { get; set; }
        public string ExportFile { get; set; }
        public string ErrorDetails { get; set; }
        public string ExportPath { get; set; }
        //public SolidColorBrush RowbackGround
        //{

        //    get
        //    {
        //        return (ReportSent) ? new SolidColorBrush(Colors.LightGreen) : new SolidColorBrush(Colors.LightCoral);
        //    }
        //}
        //public SolidColorBrush RowforeGround
        //{

        //    get
        //    {
        //        return (ReportSent) ? new SolidColorBrush(Colors.Blue) : new SolidColorBrush(Colors.Black);
        //    }
        //}
        public string ReportStatus
        {
            get { return ReportSent ? "Successful" : "Failed"; }
        }
    }
    [Serializable]
    public class ServiceEmailContent
    {
        public string ReportType
        {
            get;
            set;
        }
        public string Status
        {
            get;
            set;

        }
        public string StatusDetails
        {
            get;
            set;
        }
    }
}
