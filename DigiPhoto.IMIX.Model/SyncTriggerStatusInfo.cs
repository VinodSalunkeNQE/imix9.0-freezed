﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DigiPhoto.IMIX.Model
{
   public  class SyncTriggerStatusInfo
    {
        public long TableId { get; set; }
        public string TableName { get; set; }
        public bool IsSyncTriggerEnable { get; set; }
    }
}
