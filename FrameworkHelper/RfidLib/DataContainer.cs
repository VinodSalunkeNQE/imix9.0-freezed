﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FrameworkHelper.RfidLib
{
    public class DataContainer
    {
        /// <summary>
        /// Used to store No of Id Received
        /// </summary>
        public int NoOfIdReceived { get; set; }

        /// <summary>
        /// The id used to acknoledge
        /// </summary>
        public string AckId { get; set; }

        /// <summary>
        /// The identifier of the device from which the data is incoming
        /// </summary>
        
        public string Id { get; set; }

        /// <summary>
        /// The RFID/Tag Id
        /// </summary>
        public string TagId { get; set; }

        /// <summary>
        /// The time the data came
        /// </summary>
        public DateTime Time { get; set; }

        /// <summary>
        /// The content of the data
        /// </summary>
        public string Content { get; set; }
    }
}
