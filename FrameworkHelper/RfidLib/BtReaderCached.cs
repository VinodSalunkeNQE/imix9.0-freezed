﻿using Baracoda.Cameleon.PC.Modularity.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FrameworkHelper.RfidLib
{
    public class BtReaderCached : DataModel
    {
        /// <summary>
        /// Reader ID
        /// </summary>
        public string Id
        {
            get { return id; }
            set
            {
                if (string.Equals(id, value)) return;
                id = value;
                SendPropertyChanged(() => Id);
            }
        }

        /// <summary>
        /// Reader name
        /// </summary>
        public string Name
        {
            get { return name; }
            set
            {
                if (string.Equals(name, value)) return;
                name = value;
                SendPropertyChanged(() => Name);
            }
        }

        private string id;
        private string name;
    }
}
