﻿using Baracoda.Cameleon.PC.Readers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FrameworkHelper.RfidLib
{
    public class ReadersAddedEventArgs : EventArgs
    {
        /// <summary>
        /// The list of added readers
        /// </summary>
        public IEnumerable<BaracodaReaderBase> AddedReaders { get; set; }
    }

    public class DataEventArgs : EventArgs
    {
        /// <summary>
        /// The list of added readers
        /// </summary>
        public DataContainer RfidData { get; set; }
    }
}
