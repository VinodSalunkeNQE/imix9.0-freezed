﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Collections.ObjectModel;
using DigiPhoto.Common;

namespace DigiPhoto.ImageGallery
{

    /// <summary>
    /// 
    /// </summary>
    public class ImageFile : INotifyPropertyChanged
    {
        /// <summary>
        /// Occurs when a property value changes.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;
        /// <summary>
        /// Notifies the property changed.
        /// </summary>
        /// <param name="info">The information.</param>
        private void NotifyPropertyChanged(String info)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(info));
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ImageFile"/> class.
        /// </summary>
        public ImageFile() { }
        /// <summary>
        /// Initializes a new instance of the <see cref="ImageFile"/> class.
        /// </summary>
        /// <param name="filePath">The file path.</param>
        public ImageFile(string filePath)
        {
            this.FilePath = filePath;
        }

        /// <summary>
        /// The _file path
        /// </summary>
        private string _filePath;
        /// <summary>
        /// Gets or sets the file path.
        /// </summary>
        /// <value>
        /// The file path.
        /// </value>
        public string FilePath
        {
            get { return _filePath; }
            set
            {
                _filePath = value;
                _parentPath = new System.IO.FileInfo(_filePath).DirectoryName;
                this.NotifyPropertyChanged("FilePath");
                this.NotifyPropertyChanged("ParentPath");
                this.NotifyPropertyChanged("ColorCode");
            }
        }
        /// <summary>
        /// The _parent path
        /// </summary>
        private string _parentPath;
        /// <summary>
        /// Gets the parent path.
        /// </summary>
        /// <value>
        /// The parent path.
        /// </value>
        public string ParentPath { get { return _parentPath; } }

        /// <summary>
        /// The _color code
        /// </summary>
        private System.Windows.Media.SolidColorBrush _colorCode;
        /// <summary>
        /// Gets or sets the color code.
        /// </summary>
        /// <value>
        /// The color code.
        /// </value>
        public System.Windows.Media.SolidColorBrush ColorCode
        {
            get { return _colorCode; }
            set
            {
                _colorCode = value;

                this.NotifyPropertyChanged("ColorCode");
            }
        }
    }

    /// <summary>
    /// 
    /// </summary>
    public class ImageCollection
    {
        /// <summary>
        /// The _collection path
        /// </summary>
        private string _collectionPath = "";
        /// <summary>
        /// Gets the collection path.
        /// </summary>
        /// <value>
        /// The collection path.
        /// </value>
        public string CollectionPath { get { return _collectionPath; } }
        /// <summary>
        /// The _flatten collection
        /// </summary>
        private ObservableCollection<ImageFile> _flattenCollection;
        /// <summary>
        /// Gets the flatten collection.
        /// </summary>
        /// <value>
        /// The flatten collection.
        /// </value>
        public ObservableCollection<ImageFile> FlattenCollection { get { return _flattenCollection; } }
        /// <summary>
        /// The _collection
        /// </summary>
        private ObservableCollection2D<string, string, ImageFile> _collection;
        /// <summary>
        /// The argument
        /// </summary>
        private Random r = new Random();

        /// <summary>
        /// Initializes a new instance of the <see cref="ImageCollection"/> class.
        /// </summary>
        public ImageCollection() { }
        /// <summary>
        /// Initializes a new instance of the <see cref="ImageCollection"/> class.
        /// </summary>
        /// <param name="folderPath">The folder path.</param>
        /// <param name="linkedCollection">The linked collection.</param>
        public ImageCollection(string folderPath, ObservableCollection<ImageFile> linkedCollection)
        {
            _collectionPath = folderPath;
            _flattenCollection = linkedCollection;
            _collection = new ObservableCollection2D<string, string, ImageFile>(_flattenCollection);
        }

        /// <summary>
        /// Adds the specified image file.
        /// </summary>
        /// <param name="imageFile">The image file.</param>
        public void Add(ImageFile imageFile)
        {
            if (_collection.HasKey(imageFile.ParentPath))
            {
                foreach (ImageFile fromSameParent in _collection.Get(imageFile.ParentPath).Values)
                {
                    imageFile.ColorCode = fromSameParent.ColorCode;
                    break;
                }
            }
            else
            {
                imageFile.ColorCode = new System.Windows.Media.SolidColorBrush(
                    System.Windows.Media.Color.FromArgb(150, (byte)r.Next(255), (byte)r.Next(255), (byte)r.Next(255)));
            }

            _collection.Set(imageFile.ParentPath, imageFile.FilePath, imageFile);
        }

        /// <summary>
        /// Adds the specified image file.
        /// </summary>
        /// <param name="imageFile">The image file.</param>
        public void Add(ImageFile[] imageFile)
        {
            foreach (ImageFile file in imageFile)
                Add(file);
        }

        /// <summary>
        /// Removes the specified image file.
        /// </summary>
        /// <param name="imageFile">The image file.</param>
        public void Remove(ImageFile imageFile)
        {
            _collection.Remove(imageFile.ParentPath, imageFile.FilePath);
        }

        /// <summary>
        /// Removes the specified image file.
        /// </summary>
        /// <param name="imageFile">The image file.</param>
        public void Remove(ImageFile[] imageFile)
        {
            foreach (ImageFile file in imageFile)
                Remove(file);
        }

        /// <summary>
        /// Determines whether [contains] [the specified image file].
        /// </summary>
        /// <param name="imageFile">The image file.</param>
        /// <returns></returns>
        public bool Contains(ImageFile imageFile)
        {
            return _collection.HasKey(imageFile.ParentPath, imageFile.FilePath);
        }

        /// <summary>
        /// Counts the information dir.
        /// </summary>
        /// <param name="path">The path.</param>
        /// <returns></returns>
        public int CountInDir(string path)
        {
            return _collection.CountIn(path);
        }
    }
    /// <summary>
    /// 
    /// </summary>
    public class ImageGallery
    {
        /// <summary>
        /// The _collections
        /// </summary>
        private ObservableCollection<ImageCollection> _collections = new ObservableCollection<ImageCollection>();
        /// <summary>
        /// Gets the collections.
        /// </summary>
        /// <value>
        /// The collections.
        /// </value>
        public ObservableCollection<ImageCollection> Collections { get { return _collections; } }
        /// <summary>
        /// Initializes a new instance of the <see cref="ImageGallery"/> class.
        /// </summary>
        public ImageGallery() { }
    }
}
