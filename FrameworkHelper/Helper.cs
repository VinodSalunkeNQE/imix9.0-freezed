﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Permissions;
using System.Text;
using System.ComponentModel;
using System.Runtime.InteropServices;
using System.Windows;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Interop;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using DigiPhoto;
using Microsoft.Win32.SafeHandles;
using System.Diagnostics;
using System.Collections;
using DigiPhoto.DataLayer;
using System.Diagnostics;
using System.Reflection;
using System.Windows.Controls;
using DigiPhoto.IMIX.Model;
using System.Data;
using System.Configuration;

namespace FrameworkHelper
{
    public struct LogObject
    {
        public float value;
        public string opName;
    }

    public struct LogEffect
    {
        public double effvalue;
        public string optname;
    }
    public class CropContraint
    {

        public static void Main(string[] args)
        {
            //var abc = FrameworkHelper.CommonUtility.Watch();
            //abc.Start();
        }
        public string Name { get; private set; }
        public double? AspectRatio { get; private set; }

        public CropContraint(string name, double? aspectRatio)
        {
            Name = name;
            AspectRatio = aspectRatio;
        }
    }


  

    public class Crop : INotifyPropertyChanged
    {
        public Crop()
        {
            Constraints = new BindingList<CropContraint>
            {
                new CropContraint( "4\" x 6\"", 4.0 / 6 ),
                new CropContraint( "8.5\" x 11\"", 8.5 / 11 ),
                new CropContraint( "Custom", null )
            };
        }

        public BindingList<CropContraint> Constraints { get; private set; }

        public CropContraint Constraint
        {
            get { return CollectionViewSource.GetDefaultView(Constraints).CurrentItem as CropContraint; }
        }

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        protected void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null) PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion
    }

    public class DisposableResource : IDisposable
    {
        private Stream _resource;
        private bool _disposed;

        // The stream passed to the constructor  
        // must be readable and not null. 
        public DisposableResource(Stream stream)
        {
            if (stream == null)
                throw new ArgumentNullException("Stream in null.");
            if (!stream.CanRead)
                throw new ArgumentException("Stream must be readable.");

            _resource = stream;

            _disposed = false;
        }

        // Demonstrates using the resource.  
        // It must not be already disposed. 
        public void DoSomethingWithResource()
        {
            if (_disposed)
                throw new ObjectDisposedException("Resource was disposed.");

            // Show the number of bytes. 
            // int numBytes = (int)_resource.Length;
            //Console.WriteLine("Number of bytes: {0}", numBytes.ToString());
        }

        public void Dispose()
        {
            Dispose(true);

            // Use SupressFinalize in case a subclass 
            // of this type implements a finalizer.
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            // If you need thread safety, use a lock around these  
            // operations, as well as in your methods that use the resource. 
            if (!_disposed)
            {
                if (disposing)
                {
                    if (_resource != null)
                        _resource.Dispose();
                    // Console.WriteLine("Object disposed.");
                }

                // Indicate that the instance has been disposed.
                _resource = null;
                _disposed = true;
            }
        }
    }

    public class CursorHelper
    {
        private static class NativeMethods
        {
            public struct IconInfo
            {
                public bool fIcon;
                public int xHotspot;
                public int yHotspot;
                public IntPtr hbmMask;
                public IntPtr hbmColor;
            }

            [DllImport("user32.dll")]
            public static extern SafeIconHandle CreateIconIndirect(ref IconInfo icon);

            [DllImport("user32.dll")]
            public static extern bool DestroyIcon(IntPtr hIcon);

            [DllImport("user32.dll")]
            [return: MarshalAs(UnmanagedType.Bool)]
            public static extern bool GetIconInfo(IntPtr hIcon, ref IconInfo pIconInfo);
        }

        [SecurityPermission(SecurityAction.LinkDemand, UnmanagedCode = true)]
        private class SafeIconHandle : SafeHandleZeroOrMinusOneIsInvalid
        {
            public SafeIconHandle()
                : base(true)
            {
            }

            override protected bool ReleaseHandle()
            {
                return NativeMethods.DestroyIcon(handle);
            }
        }

        [DllImport("gdi32.dll")]
        public static extern bool DeleteObject(IntPtr hObject);
        private static Cursor InternalCreateCursor(System.Drawing.Bitmap bmp)
        {
            try
            {
                var iconInfo = new NativeMethods.IconInfo();
                try
                {

                    NativeMethods.GetIconInfo(bmp.GetHicon(), ref iconInfo);
                }
                catch (Exception ex)
                {
                    SafeIconHandle cursorHandle1 = NativeMethods.CreateIconIndirect(ref iconInfo);
                    return CursorInteropHelper.Create(cursorHandle1);
                }

                iconInfo.xHotspot = (int)(globalelement.DesiredSize.Width * 0.5);
                iconInfo.yHotspot = (int)(globalelement.DesiredSize.Height * 0.5);

                iconInfo.fIcon = false;
                globalelement = null;
                SafeIconHandle cursorHandle = NativeMethods.CreateIconIndirect(ref iconInfo);
                return CursorInteropHelper.Create(cursorHandle);
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                return null;
            }
            finally
            {
                IntPtr deleteObject = bmp.GetHbitmap();
                DeleteObject(deleteObject);
                //MemoryManagement.FlushMemory();
            }
        }

        static UIElement globalelement;
        public static Cursor CreateCursor(UIElement element)
        {
            try
            {
                GC.AddMemoryPressure(20000);
                element.Measure(new Size(double.PositiveInfinity, double.PositiveInfinity));
                element.Arrange(new Rect(new Point(), element.DesiredSize));

                double ht = Math.Round(element.DesiredSize.Width);
                double wt = Math.Round(element.DesiredSize.Height);
                RenderTargetBitmap rtb =
                  new RenderTargetBitmap(
                    (int)ht,
                    (int)wt,
                    96, 96, PixelFormats.Pbgra32);

                rtb.Render(element);
                globalelement = element;
                var encoder = new PngBitmapEncoder();
                encoder.Frames.Add(BitmapFrame.Create(rtb));

                using (var ms = new MemoryStream())
                {
                    encoder.Save(ms);
                    using (var bmp = new System.Drawing.Bitmap(ms))
                    {
                        return InternalCreateCursor(bmp);
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                return null;
            }
            finally
            {
                GC.RemoveMemoryPressure(20000);
            }
        }
    }


    #region Class TO disable Taskbar
    public class Taskbar
    {
        [DllImport("user32.dll")]
        private static extern int GetWindowText(IntPtr hWnd, StringBuilder text, int count);
        [DllImport("user32.dll", CharSet = CharSet.Auto)]
        private static extern bool EnumThreadWindows(int threadId, EnumThreadProc pfnEnum, IntPtr lParam);
        [DllImport("user32.dll", SetLastError = true)]
        private static extern System.IntPtr FindWindow(string lpClassName, string lpWindowName);
        [DllImport("user32.dll", SetLastError = true)]
        private static extern IntPtr FindWindowEx(IntPtr parentHandle, IntPtr childAfter, string className, string windowTitle);
        [DllImport("user32.dll")]
        private static extern IntPtr FindWindowEx(IntPtr parentHwnd, IntPtr childAfterHwnd, IntPtr className, string windowText);
        [DllImport("user32.dll")]
        private static extern int ShowWindow(IntPtr hwnd, int nCmdShow);
        [DllImport("user32.dll")]
        private static extern uint GetWindowThreadProcessId(IntPtr hwnd, out int lpdwProcessId);

        private const int SW_HIDE = 0;
        private const int SW_SHOW = 5;

        private const string VistaStartMenuCaption = "Start";
        private static IntPtr vistaStartMenuWnd = IntPtr.Zero;
        private delegate bool EnumThreadProc(IntPtr hwnd, IntPtr lParam);

        /// <summary>
        /// Show the taskbar.
        /// </summary>
        public static void Show()
        {
            SetVisibility(true);
        }

        /// <summary>
        /// Hide the taskbar.
        /// </summary>
        public static void Hide()
        {
            SetVisibility(false);
        }

        /// <summary>
        /// Sets the visibility of the taskbar.
        /// </summary>
        public static bool Visible
        {
            set { SetVisibility(value); }
        }

        /// <summary>
        /// Hide or show the Windows taskbar and startmenu.
        /// </summary>
        /// <param name="show">true to show, false to hide</param>
        private static void SetVisibility(bool show)
        {
            // get taskbar window
            IntPtr taskBarWnd = FindWindow("Shell_TrayWnd", null);

            // try it the WinXP way first...
            IntPtr startWnd = FindWindowEx(taskBarWnd, IntPtr.Zero, "Button", "Start");

            if (startWnd == IntPtr.Zero)
            {
                // try an alternate way, as mentioned on CodeProject by Earl Waylon Flinn
                startWnd = FindWindowEx(IntPtr.Zero, IntPtr.Zero, (IntPtr)0xC017, "Start");
            }

            if (startWnd == IntPtr.Zero)
            {
                // ok, let's try the Vista easy way...
                startWnd = FindWindow("Button", null);

                if (startWnd == IntPtr.Zero)
                {
                    // no chance, we need to to it the hard way...
                    startWnd = GetVistaStartMenuWnd(taskBarWnd);
                }
            }

            ShowWindow(taskBarWnd, show ? SW_SHOW : SW_HIDE);
            ShowWindow(startWnd, show ? SW_SHOW : SW_HIDE);
        }

        /// <summary>
        /// Returns the window handle of the Vista start menu orb.
        /// </summary>
        /// <param name="taskBarWnd">windo handle of taskbar</param>
        /// <returns>window handle of start menu</returns>
        private static IntPtr GetVistaStartMenuWnd(IntPtr taskBarWnd)
        {
            // get process that owns the taskbar window
            int procId;
            GetWindowThreadProcessId(taskBarWnd, out procId);

            Process p = Process.GetProcessById(procId);
            if (p != null)
            {
                // enumerate all threads of that process...
                foreach (ProcessThread t in p.Threads)
                {
                    EnumThreadWindows(t.Id, MyEnumThreadWindowsProc, IntPtr.Zero);
                }
            }
            return vistaStartMenuWnd;
        }

        /// <summary>
        /// Callback method that is called from 'EnumThreadWindows' in 'GetVistaStartMenuWnd'.
        /// </summary>
        /// <param name="hWnd">window handle</param>
        /// <param name="lParam">parameter</param>
        /// <returns>true to continue enumeration, false to stop it</returns>
        private static bool MyEnumThreadWindowsProc(IntPtr hWnd, IntPtr lParam)
        {
            StringBuilder buffer = new StringBuilder(256);
            if (GetWindowText(hWnd, buffer, buffer.Capacity) > 0)
            {
                // Console.WriteLine(buffer);
                if (buffer.ToString() == VistaStartMenuCaption)
                {
                    vistaStartMenuWnd = hWnd;
                    return false;
                }
            }
            return true;
        }
    }
    #endregion

    public class MyImageClass : ICloneable
    {
        string jpgExtension = ConfigurationManager.AppSettings["jpgFileExtension"];
        string pngExtension = ConfigurationManager.AppSettings["pngFileExtension"];
        public MyImageClass()
        {

        }
        public MyImageClass(string title, ImageSource image, bool ischeckd, DateTime createdDate, string imagePath, string fileExtension = "", string newLayeringXML = null, string newEffectsXML = null, SettingStatus settingStatus = 0, long photoNumber = 0, bool isAutoRotated = false)
        {
            this.PhotoNumber = photoNumber;
            this.Title = title;
            this.Image = image;
            this.IsChecked = ischeckd;
            this.FileExtension = fileExtension;
            if (fileExtension.ToLower().Equals(jpgExtension) || fileExtension.ToLower().Equals(pngExtension))
            {
                this.IsVideo = Visibility.Collapsed;
            }
            else
            {
                this.IsVideo = Visibility.Visible;
            }
            this.CreatedDate = createdDate;
            this.ImagePath = imagePath;
            this.NewEffectsXML = newEffectsXML;
            this.NewLayeringXML = newLayeringXML;
            this.SettingStatus = settingStatus;
            this.IsAutorotated = isAutoRotated;
        }

        public bool? IsAutorotated { get; set; }
        public string Title { get; set; }
        public string FileExtension { get; set; }
        public string NewLayeringXML { get; set; }
        public string NewEffectsXML { get; set; }
        public bool IsCropped { get; set; }
        public bool IsGreen { get; set; }
        public SettingStatus SettingStatus { get; set; }
        public ImageSource Image { get; set; }
        public DateTime CreatedDate { get; set; }
        public bool IsChecked { get; set; }
        public Visibility IsVideo { get; set; }
        public bool IsCodeType { get; set; }
        public string ImagePath { get; set; }
        public bool IsCorrupt { get; set; }

        public long PhotoNumber { get; set; }
        public int PhotoGrapherID { get; set; }
        public DateTime? Datetaken { get; set; }

        public int UserIdSequence { get; set; }


        public string srcPath { get; set; }

        //---19 Jan 2017----Hari-------PNG Ext-----
        public string ActFileExtension { get; set; }

        #region ICloneable Members

        public object Clone()
        {
            return this.MemberwiseClone();
        }

        #endregion
    }

    public class MyImagesList 
    {
        public MyImageClass MyImageClass { get; set; }
        public long DisplayOrder { get; set; }
    }
    public class MyImageClassList : ICloneable
    {
        public MyImageClassList()
        {
            this.ListMyImageClass = new List<MyImageClass>();
        }

        public List<MyImageClass> ListMyImageClass { get; set; }
        public string Barcode { get; set; }
        public string Format { get; set; }

        public object Clone()
        {
            return new MyImageClassList
            {
                ListMyImageClass = this.ListMyImageClass,
                Barcode = this.Barcode,
                Format = this.Format
            };
        }
    }
    public class VideoProcessingClass
    {
        public Hashtable SupportedVideoFormats = new Hashtable();
        public VideoProcessingClass()
        {
            SupportedVideoFormats.Add("mp4", ".mp4");
            SupportedVideoFormats.Add("avi", ".avi");
            SupportedVideoFormats.Add("mov", ".mov");
            SupportedVideoFormats.Add("wmv", ".wmv");
            SupportedVideoFormats.Add("3gp", ".3gp");
            SupportedVideoFormats.Add("3g2", ".3gp");
            SupportedVideoFormats.Add("m2v", ".m2v");
            SupportedVideoFormats.Add("m4v", ".m4v");
            SupportedVideoFormats.Add("flv", ".flv");
            SupportedVideoFormats.Add("mpeg", ".mpg");
            SupportedVideoFormats.Add("ffmpeg", ".ffmpeg");
            SupportedVideoFormats.Add("mts", ".mts");
            SupportedVideoFormats.Add("mkv", ".mkv");
        }
        /// <summary>
        /// Generates an output XML for all the effects applied on a video
        /// </summary>
        /// <param name="lightness"></param>
        /// <param name="saturation"></param>
        /// <param name="contrast"></param>
        /// <param name="darkness"></param>
        /// <param name="greyScale"></param>
        /// <param name="invert"></param>
        /// <param name="textLogo"></param>
        /// <param name="textLogoPosition"></param>
        /// <param name="graphicLogo"></param>
        /// <param name="graphicLogoPosition"></param>
        /// <param name="zoom"></param>
        /// <param name="fadeInOut"></param>
        /// <param name="chroma"></param>
        /// <param name="chromaKeyColor"></param>
        /// <param name="chromaKeyBG"></param>
        /// <param name="audio"></param>
        /// <param name="audioEffects"></param>
        /// <returns></returns>
        public string GetVideoEffectsXML(string lightness, string saturation, string contrast, string darkness,
            bool greyScale, bool invert, string textLogo, string textLogoPosition, string graphicLogo,
            string graphicLogoPosition, string zoom, string fadeInOut, bool chroma, string chromaKeyColor,
            string chromaKeyBG, string audio, string audioEffects, string textfontName, string textfontColor,
            string textfontSize, string textfontStyle, string amplify, string equal1,
                string equal2, string equal3, string equal4, string equal5, string equal6)
        {
            string outXML = string.Empty;
            outXML += "<effects>";
            outXML += "<lightness>";
            outXML += lightness;
            outXML += "</lightness>";
            outXML += "<saturation>";
            outXML += saturation;
            outXML += "</saturation>";
            outXML += "<contrast>";
            outXML += contrast;
            outXML += "</contrast>";
            outXML += "<darkness>";
            outXML += darkness;
            outXML += "</darkness>";
            outXML += "<greyScale>";
            outXML += greyScale.ToString();
            outXML += "</greyScale>";
            outXML += "<invert>";
            outXML += invert.ToString();
            outXML += "</invert>";
            outXML += "<textLogo textLogoPosition=\"" + textLogoPosition + "\" textfontName=\"" + textfontName + "\" textfontColor=\"" + textfontColor + "\" textfontSize=\"" + textfontSize + "\" textfontStyle=\"" + textfontStyle + "\">";
            outXML += textLogo;
            outXML += "</textLogo>";
            outXML += "<graphicLogo graphicLogoPosition=\"" + graphicLogoPosition + "\">";
            outXML += graphicLogo;
            outXML += "</graphicLogo>";
            outXML += "<zoom>";
            outXML += zoom;
            outXML += "</zoom>";
            outXML += "<fadeInOut>";
            outXML += fadeInOut;
            outXML += "</fadeInOut>";
            outXML += "<chroma chromaKeyColor=\"" + chromaKeyColor + "\" chromaKeyBG=\"" + chromaKeyBG + "\" >";
            outXML += chroma.ToString();
            outXML += "</chroma>";
            outXML += "<audio amplify=\"" + amplify + "\" equal1=\"" + equal1 + "\" equal2=\"" + equal2 + "\" equal3=\"" + equal3 + "\" equal4=\"" + equal4 + "\" equal5=\"" + equal5 + "\" equal6=\"" + equal6 + "\">";
            outXML += audio;
            outXML += "</audio>";
            outXML += "</effects>";
            //bool chroma, string chromaKeyColor, string chromaKeyBG, string audio, string audioEffects
            return outXML;
        }
        public void ExtractThumbnailFromVideo(string mediaFile, int waitTime, int position, string acquiredPath)
        {
            MediaPlayer player = new MediaPlayer { Volume = 0, ScrubbingEnabled = true };
            player.Open(new Uri(mediaFile));
            player.Pause();
            player.Position = TimeSpan.FromSeconds(position);
            System.Threading.Thread.Sleep(waitTime * 1000);
            //RenderTargetBitmap rtb = new RenderTargetBitmap(640, 480, 96, 96, PixelFormats.Pbgra32);
            RenderTargetBitmap rtb = new RenderTargetBitmap(120, 90, 96, 96, PixelFormats.Pbgra32);
            DrawingVisual dv = new DrawingVisual();
            using (DrawingContext dc = dv.RenderOpen())
            {
                dc.DrawVideo(player, new Rect(0, 0, 120, 90));
            }
            rtb.Render(dv);
            //RenderTargetBitmap rtb = new RenderTargetBitmap(160, 120, 96, 96, PixelFormats.Pbgra32);
            //DrawingVisual dv = new DrawingVisual();
            //using (DrawingContext dc = dv.RenderOpen())
            //{
            //    dc.DrawVideo(player, new Rect(0, 0, 160, 120));
            //}
            Duration duration = player.NaturalDuration;
            int videoLength = 0;
            if (duration.HasTimeSpan)
            {
                videoLength = (int)duration.TimeSpan.TotalSeconds;
            }
            BitmapFrame frame = BitmapFrame.Create(rtb).GetCurrentValueAsFrozen() as BitmapFrame;
            BitmapEncoder encoder = new JpegBitmapEncoder();
            encoder.Frames.Add(frame as BitmapFrame);
            MemoryStream memoryStream = new MemoryStream();
            encoder.Save(memoryStream);
            FileStream file = new FileStream(acquiredPath, FileMode.Create, FileAccess.Write);
            memoryStream.WriteTo(file);
            file.Close();
            memoryStream.Close();
            player.Close();
        }

        #region Image Cropping for video editing
        public string CropeImageAsperAspectRatio(string sourceImage, string SavePath, int width, int height)
        {
            string aspectRatio = string.Empty;
            BitmapImage bi = new BitmapImage();
            using (FileStream fileStream = File.OpenRead(sourceImage.ToString()))
            {
                MemoryStream ms = new MemoryStream();
                fileStream.CopyTo(ms);
                ms.Seek(0, SeekOrigin.Begin);
                fileStream.Close();
                bi.BeginInit();
                bi.StreamSource = ms;
                bi.EndInit();
                bi.Freeze();
            }
            System.Drawing.Bitmap bitmap = null;
            using (MemoryStream outStream = new MemoryStream())
            {
                // BitmapEncoder enc = new BmpBitmapEncoder();
                PngBitmapEncoder enc = new PngBitmapEncoder();
                enc.Frames.Add(BitmapFrame.Create(bi));
                enc.Save(outStream);
                bitmap = new System.Drawing.Bitmap(outStream);
            }
            System.Drawing.Image currentPicture = (System.Drawing.Image)bitmap;
            int x = width;// = 1080;
            int y = height; // = 1920;
            if (width == 1072)
            {
                x = 1080;
            }
            else if (height == 1072)
            {
                y = 1080;
            }
            aspectRatio = string.Format("{0}:{1}", x / GCD(x, y), y / GCD(x, y));
            int aspectRatioX = x / GCD(x, y);
            int aspectRatioY = y / GCD(x, y);
            System.Drawing.Image imgy = resizeImg(currentPicture, width, aspectRatioX, aspectRatioY); //width=1080
            imgy.Save(SavePath);
            return aspectRatio;
        }
        System.Drawing.Image resizeImg(System.Drawing.Image img, int width, double aspectRatio_X, double aspectRatio_Y)
        {
            double targetHeight = Convert.ToDouble(width) / (aspectRatio_X / aspectRatio_Y);
            img = cropImg(img, aspectRatio_X, aspectRatio_Y);
            System.Drawing.Bitmap bmp = new System.Drawing.Bitmap(width, (int)targetHeight);
            System.Drawing.Graphics grp = System.Drawing.Graphics.FromImage(bmp);
            grp.DrawImage(img, new System.Drawing.Rectangle(0, 0, bmp.Width, bmp.Height), new System.Drawing.Rectangle(0, 0, img.Width, img.Height), System.Drawing.GraphicsUnit.Pixel);
            return (System.Drawing.Image)bmp;

        }

        System.Drawing.Image cropImg(System.Drawing.Image img, double aspectRatio_X, double aspectRatio_Y)
        {
            // 4:3 Aspect Ratio. You can also add it as parameters
            //double aspectRatio_X = 9;
            //double aspectRatio_Y = 16;

            double imgWidth = Convert.ToDouble(img.Width);
            double imgHeight = Convert.ToDouble(img.Height);

            //if (imgWidth / imgHeight > (aspectRatio_X / aspectRatio_Y))
            {
                double extraWidth = imgWidth - (imgHeight * (aspectRatio_X / aspectRatio_Y));
                double cropStartFrom = extraWidth / 2;
                System.Drawing.Bitmap bmp = new System.Drawing.Bitmap((int)(img.Width - extraWidth), img.Height);
                System.Drawing.Graphics grp = System.Drawing.Graphics.FromImage(bmp);
                grp.DrawImage(img, new System.Drawing.Rectangle(0, 0, (int)(img.Width - extraWidth), img.Height), new System.Drawing.Rectangle((int)cropStartFrom, 0, (int)(imgWidth - extraWidth), img.Height), System.Drawing.GraphicsUnit.Pixel);
                //grp.RotateTransform(90);
                return (System.Drawing.Image)bmp;
            }
            //else
            //    return null;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        public int GCD(int a, int b)
        {
            int Remainder;

            while (b != 0)
            {
                Remainder = a % b;
                a = b;
                b = Remainder;
            }

            return a;
        }

        #endregion Image Croping for video editing

    }
    public class CommonUtility
    {

        public static Dictionary<string, int> ProductIdFolders = new Dictionary<string, int>
        {
            { "4x6",30 },
            { "3x3",98 },
            { "5x7",103 },
            { "6x8",1 },
             { "6x14",123},
              { "6x20",125},
              { "8x10",2  },
              { "8x24",120},
              { "8x26",121},
              { "8x18",122 },
              { "Unique(4x6)&QR",124 },
              { "(4x6) &QR",104 },
               { "4 * 6(2)",102 },
              

        };

        public static int GetRandomNumber(int maxNumber)
        {
            if (maxNumber < 1)
                throw new System.Exception("The maxNumber value should be greater than 1");
            byte[] b = new byte[4];
            new System.Security.Cryptography.RNGCryptoServiceProvider().GetBytes(b);
            int seed = (b[0] & 0x7f) << 24 | b[1] << 16 | b[2] << 8 | b[3];
            System.Random r = new System.Random(seed);
            return r.Next(1, maxNumber);
        }
        public static string GetRandomString(int length)
        {
            string[] array = new string[32]
            {
                "0","1","2","3","4","5","6","8","9","A","B","C","D","E","F","G","H","J","K","L","M","N","P","R","S","T","U","V","W","X","Y","Z"
            };
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            for (int i = 0; i < length; i++) sb.Append(array[GetRandomNumber(32)]);
            return sb.ToString();
        }


        public static string GetUniqueSynccode(string ApplicationObject, string countryCode, string storeCode, string subStoreid)
        {
            string dateString = String.Format("{0:yyyyMMdd}", DateTime.Now);
            string timeString = String.Format("{0}{1:HHmmssfff}", "T", DateTime.Now);
            string guidString = Guid.NewGuid().ToString();
            string guidst = guidString.Substring(0, 25).Replace("-", "");
            if (string.IsNullOrEmpty(countryCode))
                countryCode = "000";
            if (string.IsNullOrEmpty(storeCode))
                storeCode = "0000";
            if (subStoreid.Length < 2)
                subStoreid = "0" + subStoreid.ToString();
            string sb = countryCode + storeCode + subStoreid + dateString + timeString + guidst + ApplicationObject;
            return sb.ToUpper();
        }
        public static Stopwatch Watch()
        {
            Stopwatch Watch = new Stopwatch();
            //Watch.Start();
            return Watch;
        }

        private static volatile object _object = new object();
        public static void WatchStop(string processName, Stopwatch watch)
        {
            watch.Stop();
            lock (_object)
            {
                LogPerfWrite(processName, watch.Elapsed);
            }
        }
        public static void LogPerfWrite(string processName, TimeSpan timesapn)
        {


            FileStream fileStream = null;
            StreamWriter streamWriter = null;
            try
            {
                //string root = Environment.CurrentDirectory;
                string root = Path.Combine(AppDomain.CurrentDomain.BaseDirectory);
                string logFilePath = root + "\\";
                logFilePath = System.IO.Path.Combine(logFilePath, "DigiLogError");
                logFilePath = logFilePath + "\\" + "DigiPerfLog" + "-" + DateTime.Today.ToString("yyyyMMdd") + "." + "txt";

                if (logFilePath.Equals(""))
                    return;

                #region Create the Log file directory if it does not exists
                DirectoryInfo logDirInfo = null;
                FileInfo logFileInfo = new FileInfo(logFilePath);
                logDirInfo = new DirectoryInfo(logFileInfo.DirectoryName);

                if (!logDirInfo.Exists)
                    logDirInfo.Create();
                #endregion Create the Log file directory if it does not exists

                if (!logFileInfo.Exists)
                {
                    fileStream = logFileInfo.Create();
                }
                else
                {
                    fileStream = new FileStream(logFilePath, FileMode.Append);
                }
                streamWriter = new StreamWriter(fileStream);
                streamWriter.WriteLine(String.Format("Time elapsed for: {0} : {1} | {2}", processName, timesapn.ToString(), timesapn.Milliseconds.ToString()));
                //streamWriter.WriteLine(" ");
            }
            finally
            {
                if (streamWriter != null) streamWriter.Close();
                if (fileStream != null) fileStream.Close();
            }

        }


        public static BitmapImage GetImageFromPath(string path)
        {
            try
            {

                BitmapImage bi = new BitmapImage();
                using (FileStream fileStream = File.OpenRead(path))
                {
                    MemoryStream ms = new MemoryStream();
                    fileStream.CopyTo(ms);
                    ms.Seek(0, SeekOrigin.Begin);
                    fileStream.Close();
                    bi.CacheOption = BitmapCacheOption.OnLoad;
                    bi.BeginInit();
                    bi.StreamSource = ms;
                    bi.EndInit();
                    bi.Freeze();
                    fileStream.Close();
                    ms.Flush();
                }
                return bi;

            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogFileWrite("Exception Funcion name GetImageFromPath(string path)");
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                return null;
            }
        }

        public static void BindCombo<T>(ComboBox combo, List<T> oList, string displayMemberPath, string selectedValuePath)
        {
            combo.DisplayMemberPath = displayMemberPath;
            combo.SelectedValuePath = selectedValuePath;
            combo.ItemsSource = oList;
        }

        public static void BindComboWithSelect<T>(ComboBox combo, List<T> oList, string displayMemberPath, string selectedValuePath, int val, string text) where T : new()
        {
            combo.DisplayMemberPath = displayMemberPath;
            combo.SelectedValuePath = selectedValuePath;

            PropertyInfo pText = typeof(T).GetProperty(displayMemberPath);
            PropertyInfo pValue = typeof(T).GetProperty(selectedValuePath);

            var obj = new T();
            pText.SetValue(obj, Convert.ChangeType(text, pText.PropertyType), null);
            pValue.SetValue(obj, Convert.ChangeType(val, pValue.PropertyType), null);

            oList.Insert(0, obj);
            combo.ItemsSource = oList;
        }
        //public static void BindComboWithSelect<T>(ComboBox combo, List<T> oList, int val, string text) where T : new()
        //{
        //    var obj = new T();
        //    oList.Insert(0, obj);
        //    combo.ItemsSource = oList;
        //}
        public static void BindListBox<T>(ListBox list, List<T> oList)
        {
            list.ItemsSource = oList;
        }

        public static void BindComboWithSelect(ComboBox combo, List<ValueTypeInfo> oList, int value, string Text)
        {
            combo.DisplayMemberPath = "Name";
            combo.SelectedValuePath = "ValueTypeId";

            oList.Insert(0, new ValueTypeInfo { ValueTypeId = value, Name = Text });
            combo.ItemsSource = oList;

        }

        /// <summary>
        /// Automatics the data table.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="data">The data.</param>
        /// <returns></returns>
        public static DataTable ToDataTable<T>(IList<T> data)// T is any generic type
        {
            PropertyDescriptorCollection props = TypeDescriptor.GetProperties(typeof(T));

            DataTable table = new DataTable();
            for (int i = 0; i < props.Count; i++)
            {
                PropertyDescriptor prop = props[i];
                table.Columns.Add(prop.Name, prop.PropertyType);
            }
            object[] values = new object[props.Count];
            foreach (T item in data)
            {
                for (int i = 0; i < values.Length; i++)
                {
                    values[i] = props[i].GetValue(item);
                }
                table.Rows.Add(values);
            }
            return table;
        }
        public static string getRequiredMessage(string param)
        {
            return string.Format(UIConstant.requiredParam, param);
        }
        public static string getRequiredMessageForDdl(string param)
        {
            return string.Format(UIConstant.requiredParamForDdl, param);
        }
        public static void CleanFolder(string DirectoryName, params string[] FolderNames)
        {

            foreach (string folder in FolderNames)
            {
                if (Directory.Exists(System.IO.Path.Combine(DirectoryName, folder)))
                {
                    DirectoryInfo dir = new DirectoryInfo(System.IO.Path.Combine(DirectoryName, folder));
                    foreach (var item in dir.GetFiles())
                    {
                        try
                        {
                            item.Delete();
                        }
                        catch (Exception ex)
                        {
                            string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                            ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                        }
                    }
                }
            }

        }

    }
    #region read flv file meta info
    public class FlvMetaInfo
    {
        private Double _duration;


        private bool _hasMetaData;

        public Double Duration
        {
            get { return _duration; }
            set { _duration = value; }
        }

        public bool HasMetaData
        {
            get { return _hasMetaData; }
            set { _hasMetaData = value; }
        }
        internal FlvMetaInfo(bool hasMetaData, Double duration)
        {
            _hasMetaData = hasMetaData;
            _duration = duration;

        }

    }
    public class FlvMetaDataReader
    {
        /// <summary>
        /// Reads the meta information (if present) in an FLV
        /// </summary>
        /// <param name="path">The path to the FLV file</returns>
        public static FlvMetaInfo GetFlvMetaInfo(string path)
        {
            if (!File.Exists(path))
            {
                throw new Exception(String.Format("File '{0}' doesn't exist for FlvMetaDataReader.GetFlvMetaInfo(path)", path));
            }
            bool hasMetaData = false;
            double duration = 0;

            DateTime creationDate = DateTime.MinValue;
            // open file 
            FileStream fileStream = new FileStream(path, FileMode.Open);
            try
            {
                // read where "onMetaData"
                byte[] bytes = new byte[10];
                fileStream.Seek(27, SeekOrigin.Begin);
                int result = fileStream.Read(bytes, 0, 10);
                // if "onMetaData" exists then proceed to read the attributes
                string onMetaData = ByteArrayToString(bytes);
                if (onMetaData == "onMetaData")
                {
                    hasMetaData = true;
                    // 16 bytes past "onMetaData" is the data for "duration" 
                    duration = GetNextDouble(fileStream, 16, 8);

                }
            }
            catch (Exception e)
            {
                // no error handling
            }
            finally
            {
                fileStream.Close();
            }
            return new FlvMetaInfo(hasMetaData, duration);
        }
        private static Double GetNextDouble(FileStream fileStream, int offset, int length)
        {
            // move the desired number of places in the array
            fileStream.Seek(offset, SeekOrigin.Current);
            // create byte array
            byte[] bytes = new byte[length];
            // read bytes
            int result = fileStream.Read(bytes, 0, length);
            // convert to double (all flass values are written in reverse order)
            return ByteArrayToDouble(bytes, true);
        }
        private static string ByteArrayToString(byte[] bytes)
        {
            string byteString = string.Empty;
            foreach (byte b in bytes)
            {
                byteString += Convert.ToChar(b).ToString();
            }
            return byteString;
        }
        private static Double ByteArrayToDouble(byte[] bytes, bool readInReverse)
        {
            if (bytes.Length != 8)
                throw new Exception("bytes must be exactly 8 in Length");
            if (readInReverse)
                Array.Reverse(bytes);
            return BitConverter.ToDouble(bytes, 0);
        }
    }
    # endregion

}
