﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Windows;
using System.Windows.Media.Imaging;
using System.ComponentModel;
using System.Threading;
using System.Drawing;
using System.Drawing.Imaging;
using System.Runtime.InteropServices;
using System.Threading.Tasks;

namespace DigiPhoto.Common
{
    public class FileInformation : INotifyPropertyChanged, IInstantiateProperty
    {
        public FileInformation(FileInfo fileInfo)
        {
            FileInfo = fileInfo;
        }

        public FileInfo FileInfo { get; private set; }

        /// <summary>
        /// item thumbnail, should NOT be invoked in UI thread
        /// </summary>
        public BitmapImage SlowBitmap
        {
            get
            {
                return (BitmapImage)(_weakBitmap.Target ?? (_weakBitmap.Target = GetBitmap(FileInfo.FullName)));
            }
        }



        /// <summary>
        /// item thumbnail, may be invoked in UI thread
        /// return DependencyProperty.UnsetValue if WeakReference.Target = null
        /// </summary>
        public object FastBitmap
        {
            get
            {
                return _weakBitmap.Target ?? DependencyProperty.UnsetValue;
            }
        }

        private static BitmapImage GetBitmap(string path)
        {
            try
            {
                return GetThumbnail(path);
            }
            catch (Exception)
            {
                return null;
            }
        }

        const int THUMBNAIL_DATA = 0x501B;
        static BitmapImage GetThumbnail(string path)
        {

            FileStream fs = File.OpenRead(path);
            Image img = Image.FromStream(fs, false, false);


            bool isFound = false;
            for (int i = 0; i < img.PropertyIdList.Length; i++)
            {
                if (img.PropertyIdList[i] == THUMBNAIL_DATA)
                {
                    isFound = true;
                }
            }

            BitmapImage bmp = null;
            if (isFound)
            {
                try
                {
                    PropertyItem prop = img.GetPropertyItem(THUMBNAIL_DATA);
                    byte[] imgBytes = prop.Value;
                    MemoryStream stream = new MemoryStream(imgBytes.Length);
                    stream.Write(imgBytes, 0, imgBytes.Length);
                    Bitmap imgB = (Bitmap)Image.FromStream(stream);
                    bmp = new BitmapImage();
                    using (MemoryStream memStream2 = new MemoryStream())
                    {
                        imgB.Save(memStream2, System.Drawing.Imaging.ImageFormat.Jpeg);
                        memStream2.Seek(0, SeekOrigin.Begin);
                        bmp.BeginInit();
                        bmp.CacheOption = BitmapCacheOption.OnLoad;
                        bmp.UriSource = null;
                        bmp.StreamSource = memStream2;
                        bmp.EndInit();
                        bmp.Freeze();
                    }
                }
                catch (Exception ss)
                {
                    //get thumbnail from image file log if any error
                    bmp = GetThumbnailFromFile(path, bmp);

                }
            }
            else
            {
                //get thumbnail from image file log if any error
                bmp = GetThumbnailFromFile(path, bmp);
            }
            fs.Close();
            img.Dispose();

            return bmp;
        }

        //[Flags]
        //public enum SIIGBF
        //{
        //    SIIGBF_RESIZETOFIT = 0x00000000,
        //    SIIGBF_BIGGERSIZEOK = 0x00000001,
        //    SIIGBF_MEMORYONLY = 0x00000002,
        //    SIIGBF_ICONONLY = 0x00000004,
        //    SIIGBF_THUMBNAILONLY = 0x00000008,
        //    SIIGBF_INCACHEONLY = 0x00000010,
        //    SIIGBF_CROPTOSQUARE = 0x00000020,
        //    SIIGBF_WIDETHUMBNAILS = 0x00000040,
        //    SIIGBF_ICONBACKGROUND = 0x00000080,
        //    SIIGBF_SCALEUP = 0x00000100,
        //}

        //[DllImport("shell32.dll", CharSet = CharSet.Unicode)]
        //private static extern int SHCreateItemFromParsingName(string path, IntPtr pbc, [MarshalAs(UnmanagedType.LPStruct)] Guid riid, out IShellItemImageFactory factory);

        //[ComImport]
        //[InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
        //[Guid("bcc18b79-ba16-442f-80c4-8a59c30c463b")]
        //private interface IShellItemImageFactory
        //{
        //    [PreserveSig]
        //    int GetImage(System.Drawing.Size size, SIIGBF flags, out IntPtr phbm);
        //}

        //public static Bitmap ExtractThumbnailNew(string filePath, System.Drawing.Size size, SIIGBF flags)
        //{
        //    if (filePath == null)
        //        throw new ArgumentNullException("filePath");

        //    // TODO: you might want to cache the factory for different types of files
        //    // as this simple call may trigger some heavy-load underground operations
        //    IShellItemImageFactory factory;
        //    int hr = SHCreateItemFromParsingName(filePath, IntPtr.Zero, typeof(IShellItemImageFactory).GUID, out factory);
        //    if (hr != 0)
        //        throw new Win32Exception(hr);

        //    IntPtr bmp;
        //    hr = factory.GetImage(size, flags, out bmp);
        //    if (hr != 0)
        //        throw new Win32Exception(hr);

        //    return Bitmap.FromHbitmap(bmp);
        //}


        /// <summary>
        /// Gets the thumbnail from file.
        /// </summary>
        /// <param name="path">The path.</param>
        /// <param name="bmp">The BMP.</param>
        /// <returns></returns>
        private static BitmapImage GetThumbnailFromFile(string path, BitmapImage bmp)
        {
            try
            {
                bmp = new BitmapImage();
                bmp.BeginInit();
                bmp.CacheOption = BitmapCacheOption.OnLoad;
                bmp.UriSource = new Uri(path);
                bmp.DecodePixelHeight = 100;
                bmp.EndInit();
                bmp.Freeze();

                return bmp;
            }
            catch (Exception)
            {
                return null;
            }
        }



        private WeakReference _weakBitmap = new WeakReference(null);

        protected void OnPropertyChanged(string propName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propName));
            }
        }

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        #endregion



        #region IInstantiateProperty Members

        public void InstantiateProperty(string propertyName, System.Globalization.CultureInfo culture, SynchronizationContext callbackExecutionContext)
        {
            switch (propertyName)
            {
                case "FastBitmap":
                    callbackExecutionContext.Post((o) => OnPropertyChanged(propertyName), _weakBitmap.Target ?? (_weakBitmap.Target = GetBitmap(FileInfo.FullName)));
                    break;
                default:
                    break;
            }
        }

        #endregion

    }
}
