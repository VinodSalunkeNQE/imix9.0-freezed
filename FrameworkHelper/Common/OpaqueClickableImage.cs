﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows;

namespace DigiPhoto.Common
{
    public class OpaqueClickableImage : Image
    {
        /// <summary>
        /// Implements <see cref="M:System.Windows.Media.Visual.HitTestCore(System.Windows.Media.PointHitTestParameters)" /> to supply base element hit testing behavior (returning <see cref="T:System.Windows.Media.HitTestResult" />).
        /// </summary>
        /// <param name="hitTestParameters">Describes the hit test to perform, including the initial hit point.</param>
        /// <returns>
        /// Results of the test, including the evaluated point.
        /// </returns>
        protected override HitTestResult HitTestCore(PointHitTestParameters hitTestParameters) 
            {
                var source = (BitmapSource)Source;
                var x = (int)(hitTestParameters.HitPoint.X / ActualWidth * source.PixelWidth);
                var y = (int)(hitTestParameters.HitPoint.Y / ActualHeight * source.PixelHeight);
                if (x == source.PixelWidth)
                    x--;
                if (y == source.PixelHeight)
                    y--;
                var pixels = new byte[4];
                source.CopyPixels(new Int32Rect(x, y, 1, 1), pixels, 4, 0);
                if (pixels[3] < 1) return null;
                return new PointHitTestResult(this, hitTestParameters.HitPoint);
                //try
                //{
                //    var source = (BitmapSource)Source;
                //    var x = (int)(hitTestParameters.HitPoint.X / ActualWidth * source.PixelWidth);
                //    var y = (int)(hitTestParameters.HitPoint.Y / ActualHeight * source.PixelHeight);
                //    var pixels = new byte[4];
                //    source.CopyPixels(new Int32Rect(x, y, 1, 1), pixels, 4, 0);
                //    if (pixels[3] < 10) return null;

                //    return new PointHitTestResult(this, hitTestParameters.HitPoint);
                //}
                //catch (Exception ex)
                //{
                //    return new PointHitTestResult(this, hitTestParameters.HitPoint);
                //}
            }
    }
}
