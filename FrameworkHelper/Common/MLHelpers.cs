﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MControls;
using MPLATFORMLib;
using System.IO;
using System.Runtime.InteropServices;
using System.Windows.Media.Imaging;
using System.Xml;
using System.Windows;
using System.Threading;


namespace FrameworkHelper.Common
{

    public class MLHelpers
    {

        #region Declaration

        static double startTime;
        static double stopTime;
        public static double VideoLength;
        #endregion
        public MLHelpers()
        {
            ChromaKeypluginLic.IntializeProtection();
            DecoderlibLic.IntializeProtection();
            EncoderlibLic.IntializeProtection();
            MComposerlibLic.IntializeProtection();
            MPlatformSDKLic.IntializeProtection();
        }

        /// <summary>
        /// Method to extract thumbnail from Image and Videos
        /// </summary>
        /// <param name="fileName">filename for image or video</param>
        /// <returns>returns the generated thumbnail path to calling method</returns>
        public static string ExtractThumbnail(string fileName)
        {
            try
            {
                MFileClass mFile = new MFileClass();
                mFile.FileNameSet(fileName, "");
                mFile.FilePlayStart();
                System.Threading.Thread.Sleep(1000);
                string path_tempThumbnail = string.Empty;
                string processVideoTemp = AppDomain.CurrentDomain.BaseDirectory;

                double VideoLength;
                mFile.FileInOutGet(out startTime, out stopTime, out VideoLength);
                MFrame mf1 = null;

                //If jpg is consumed then if condition else video is processed in Else part to extract thumbnail_ Added by VinodSalunke_17062019
                if (fileName.Contains(".jpg") || fileName.Contains(".JPG"))
                {

                    mFile.FileFrameGet(4, 0, out mf1);

                    path_tempThumbnail = processVideoTemp + "\\Output.jpg";
                    if (File.Exists(path_tempThumbnail))
                    {
                        File.Delete(path_tempThumbnail);
                    }
                    mf1.FrameVideoSaveToFile(path_tempThumbnail);
                }
                //Else condition added by Vinod Salunke for extracting video thumbnail at perticular second_21Jun2019
                else if (fileName.ToLower().Contains(".mp4") || fileName.ToLower().Contains(".mov") || fileName.ToLower().Contains(".wmv"))
                {
                    int secondsCount = 14;
                    M_TIMECODE myTime;
                    M_TIMECODE pOutTC;
                    int pnOutSpec;

                    if (VideoLength < 14 && VideoLength >= 4)
                    {
                        secondsCount = (int)VideoLength - 4;
                    }

                    mFile.FileInOutGetTC(out myTime, out pOutTC, out pnOutSpec);
                    myTime.nHours = 0;
                    myTime.nMinutes = 0;
                    myTime.nSeconds = secondsCount;
                    myTime.nFrames = 0;

                    mFile.FileFrameGetByTC(ref myTime, out mf1);
                    path_tempThumbnail = processVideoTemp + "\\Output.jpg";
                    if (File.Exists(path_tempThumbnail))
                    {
                        File.Delete(path_tempThumbnail);
                    }
                    mf1.FrameVideoSaveToFile(path_tempThumbnail);

                }

                mFile.FilePlayStop(0.0);
                mFile.ObjectClose();

                Marshal.ReleaseComObject(mf1);
                Marshal.ReleaseComObject(mFile);
                return path_tempThumbnail;
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                return null;
            }
        }

        public static string ExtractThumbnail_old(string fileName)
        {
            try
            {
                MFileClass mFile = new MFileClass();
                mFile.FileNameSet(fileName, "");
                mFile.FilePlayStart();
                System.Threading.Thread.Sleep(1000);
                string path_tempThumbnail = string.Empty;
                string processVideoTemp = AppDomain.CurrentDomain.BaseDirectory;
                MFrame mf1;
                mFile.FileFrameGet(4, 0, out mf1);
                path_tempThumbnail = processVideoTemp + "\\Output.jpg";
                if (File.Exists(path_tempThumbnail))
                {
                    File.Delete(path_tempThumbnail);
                }
                mf1.FrameVideoSaveToFile(path_tempThumbnail);

                mFile.FileInOutGet(out startTime, out stopTime, out VideoLength);
                mFile.FilePlayStop(0.0);
                mFile.ObjectClose();
                Marshal.ReleaseComObject(mf1);
                Marshal.ReleaseComObject(mFile);
                return path_tempThumbnail;
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                return null;
            }

        }

        //Video Edito Manual Frame Download
        public static string  ExtractFrame(string frameCropRatio,string fileName,string tempFolderPath,string extractedImagePath)
        {
            try
            {
                string resizeFolerPath = tempFolderPath + "ResizeFrames\\";
                if (!Directory.Exists(resizeFolerPath))
                    Directory.CreateDirectory(resizeFolerPath);
                string saveImagePath = resizeFolerPath + fileName;
                VideoProcessingClass obj = new VideoProcessingClass();
                int height = 0;
                int width = 0;
                bool cropRatioStatus = false;
                cropRatioStatus = getFrameRatio(frameCropRatio, out width, out height);
                MFileClass mFile = new MFileClass();
                MFrame mf1 = null;
               
                        if (cropRatioStatus)
                        {
                            if (!GetCropRatio(extractedImagePath, frameCropRatio))
                            {
                                obj.CropeImageAsperAspectRatio(extractedImagePath, saveImagePath, width, height);
                                                                
                            }

                            else
                                File.Copy(extractedImagePath, saveImagePath, true);
                        }
                        else
                            File.Copy(extractedImagePath, saveImagePath, true);
                    try
                    {
                        File.Delete(extractedImagePath);
                    }
                    catch (Exception e)
                    { }
                    return saveImagePath;
                }
              
          
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
            return null;
        }

        //FrameExtration in FileWatcer
        public static List<string> ExtractFrame(string FileName, List<double> FrameSec, string Userid, string FileFullName, string frameCropRatio)
        {
            try
            {
                #region Declaration
                int i = 0;
                VideoProcessingClass obj = new VideoProcessingClass();
                string saveResizeimagepath = string.Empty;
                string resizeImageName = string.Empty;
                int height = 0;
                int width = 0;
                bool cropRatioStatus = false;
                cropRatioStatus = getFrameRatio(frameCropRatio, out width, out height);
                List<String> FrameFileName = new List<string>();
                List<String> RemoveFile = new List<string>();
                MFileClass mFile = new MFileClass();
                #endregion
                mFile.FileNameSet(FileName, "");
                mFile.FilePlayStart();
                System.Threading.Thread.Sleep(1000);
                string path_tempThumbnail = string.Empty;
                string processVideoTemp = System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Frames");
                saveResizeimagepath = System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Frames", "ResizeImagePath");
                if (!Directory.Exists(processVideoTemp))
                    Directory.CreateDirectory(processVideoTemp);
                if (!Directory.Exists(saveResizeimagepath))
                    Directory.CreateDirectory(saveResizeimagepath);

                MFrame mf1 = null;
                foreach (double framesec in FrameSec)
                {
                    if (framesec <= VideoLength)
                    {
                        i = i + 1;
                        mFile.FileFrameGet(framesec, 0, out mf1);
                        path_tempThumbnail = processVideoTemp + "\\" + FileFullName + "_" + i + "@" + Userid + ".jpg";
                        resizeImageName = saveResizeimagepath + "\\" + FileFullName + "_" + i + "@" + Userid + ".jpg";
                        mf1.FrameVideoSaveToFile(path_tempThumbnail);

                        if (cropRatioStatus) 
                        {
                            if (!GetCropRatio(path_tempThumbnail, frameCropRatio))
                            {
                                obj.CropeImageAsperAspectRatio(path_tempThumbnail, resizeImageName, width, height);
                                FrameFileName.Add(resizeImageName);
                                RemoveFile.Add(path_tempThumbnail);
                            }

                            else
                            {
                                File.Copy(path_tempThumbnail, resizeImageName);
                                FrameFileName.Add(resizeImageName);
                                RemoveFile.Add(path_tempThumbnail);
                            }
                        }
                        else
                        {
                            File.Copy(path_tempThumbnail, resizeImageName);
                            FrameFileName.Add(resizeImageName);
                            RemoveFile.Add(path_tempThumbnail);
                        }
                    }
                }
                //mFile.FileInOutGet(out startTime, out stopTime, out VideoLength);
                mFile.FilePlayStop(0.0);
                mFile.ObjectClose();
                Marshal.ReleaseComObject(mf1);
                Marshal.ReleaseComObject(mFile);
                foreach (string file in RemoveFile)
                {
                    try
                    {
                        File.Delete(file);
                    }
                    catch (Exception e)
                    { }
                }
                return FrameFileName;
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                return null;
            }
        }

        private static bool GetCropRatio(string imagePath,string frameCropRatio)
        {
            string[] frameCrop = frameCropRatio.Split('x');
            string _frameCrop = string.Format("{0}:{1}", frameCrop[0], frameCrop[1]);
            string aspectRatio = string.Empty;
            BitmapImage bi = new BitmapImage();
            using (FileStream fileStream = File.OpenRead(imagePath.ToString()))
            {
                MemoryStream ms = new MemoryStream();
                fileStream.CopyTo(ms);
                ms.Seek(0, SeekOrigin.Begin);
                fileStream.Close();
                bi.BeginInit();
                bi.StreamSource = ms;
                bi.EndInit();
                bi.Freeze();
            }
            System.Drawing.Bitmap bitmap = null;
            using (MemoryStream outStream = new MemoryStream())
            {
                // BitmapEncoder enc = new BmpBitmapEncoder();
                PngBitmapEncoder enc = new PngBitmapEncoder();
                enc.Frames.Add(BitmapFrame.Create(bi));
                enc.Save(outStream);
                bitmap = new System.Drawing.Bitmap(outStream);
            }
            System.Drawing.Image currentPicture = (System.Drawing.Image)bitmap;
            int width = currentPicture.Width;
            int height = currentPicture.Height;
            int x = width;// = 1080;
            int y = height; // = 1920;
            VideoProcessingClass obj = new VideoProcessingClass();
            aspectRatio = string.Format("{0}:{1}", x / obj.GCD(x, y), y / obj.GCD(x, y));
            if (aspectRatio == _frameCrop)
                return true;
            else
                return false;
        }

        private static bool getFrameRatio(string frameCropRatio, out int width, out int height)
        {
            if (frameCropRatio == "None" || string.IsNullOrEmpty(frameCropRatio))
            {
                width = 0;
                height = 0;
                return false;

            }
            else if (frameCropRatio == "4x6")
            {
                width = 1800;
                height = 1200;

            }
            else if (frameCropRatio == "6x8")
            {
                width = 2400;
                height = 1800;
            }
            else  //8x10
            {
                width = 3000;
                height = 2400;
            }
            return true;
        }

        //Frame Extraction in Manual Download Service
        public static List<string> ExtractFramesManualDownload(List<double> FrameSec, string title, string videopath, string extension, string frameCropRatio)
        {
            try
            {
                string playFilePath = Path.Combine(videopath, title + extension);
                int i = 0;
                List<String> FrameFileName = new List<string>();
                List<String> RemoveFile = new List<string>();
                string saveResizeimagepath = string.Empty;
                VideoProcessingClass obj = new VideoProcessingClass();
                string resizeImageName = string.Empty;
                int height = 0;
                int width = 0;
                bool cropRatioStatus = false;
                cropRatioStatus = getFrameRatio(frameCropRatio, out width, out height);
                MFileClass mFile = new MFileClass();
                mFile.FileNameSet(playFilePath, "");
                mFile.FilePlayStart();
                System.Threading.Thread.Sleep(1000);
                //double videolength;
                //mFile.FileInOutGet(out startTime, out stopTime, out videolength);

                string path_tempThumbnail = string.Empty;
                string processVideoTemp = System.IO.Path.Combine(System.IO.Path.GetDirectoryName(AppDomain.CurrentDomain.BaseDirectory), "ManualDownloadFrames");
                saveResizeimagepath = System.IO.Path.Combine(System.IO.Path.GetDirectoryName(AppDomain.CurrentDomain.BaseDirectory), "ManualDownloadFrames", "ResizeImagePath");
                if (!Directory.Exists(processVideoTemp))
                    Directory.CreateDirectory(processVideoTemp);
                if (!Directory.Exists(saveResizeimagepath))
                    Directory.CreateDirectory(saveResizeimagepath);
                MFrame mf1 = null;
                foreach (double framesec in FrameSec)
                {
                    if (framesec <= VideoLength)
                    {
                        i = i + 1;
                        mFile.FileFrameGet(framesec, 0, out mf1);
                        path_tempThumbnail = processVideoTemp + "\\" + title + "_#" + i + ".jpg";
                        resizeImageName = saveResizeimagepath + "\\" + title + "_#" + i + ".jpg";
                        mf1.FrameVideoSaveToFile(path_tempThumbnail);
                        if (cropRatioStatus) 
                        {
                            if (!GetCropRatio(path_tempThumbnail, frameCropRatio))
                            {
                                obj.CropeImageAsperAspectRatio(path_tempThumbnail, resizeImageName, width, height);
                                FrameFileName.Add(resizeImageName);
                                RemoveFile.Add(path_tempThumbnail);
                            }

                            else
                            {
                                File.Copy(path_tempThumbnail, resizeImageName);
                                FrameFileName.Add(resizeImageName);
                                RemoveFile.Add(path_tempThumbnail);
                            }
                        }
                        else
                        {
                            File.Copy(path_tempThumbnail, resizeImageName);
                            FrameFileName.Add(resizeImageName);
                            RemoveFile.Add(path_tempThumbnail);
                        }
                    
                    }
                }
                //mFile.FileInOutGet(out startTime, out stopTime, out VideoLength);
                mFile.FilePlayStop(0.0);
                mFile.ObjectClose();
                Marshal.ReleaseComObject(mf1);
                Marshal.ReleaseComObject(mFile);
                foreach (string file in RemoveFile)
                {
                    try
                    {
                        File.Delete(file);
                    }
                    catch (Exception e)
                    { }
                }
                return FrameFileName;
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                return null;
            }
        }
        public static void ResizeImage(string sourceImage, int maxHeight, string saveToPath)
        {
            try
            {
                BitmapImage bi = new BitmapImage();
                BitmapImage bitmapImage = new BitmapImage();
                using (FileStream fileStream = File.OpenRead(sourceImage.ToString()))
                {
                    MemoryStream ms = new MemoryStream();
                    fileStream.CopyTo(ms);
                    ms.Seek(0, SeekOrigin.Begin);
                    fileStream.Close();
                    bi.BeginInit();
                    bi.StreamSource = ms;
                    bi.EndInit();
                    bi.Freeze();
                    decimal ratio = Convert.ToDecimal(bi.Width) / Convert.ToDecimal(bi.Height);

                    int newWidth = Convert.ToInt32(maxHeight * ratio);
                    int newHeight = maxHeight;

                    ms.Seek(0, SeekOrigin.Begin);
                    bitmapImage.BeginInit();
                    bitmapImage.StreamSource = ms;
                    bitmapImage.DecodePixelWidth = newWidth;
                    bitmapImage.DecodePixelHeight = newHeight;
                    bitmapImage.EndInit();
                    bitmapImage.Freeze();
                }

                using (var fileStreamForSave = new FileStream(saveToPath, FileMode.Create, FileAccess.ReadWrite))
                {
                    JpegBitmapEncoder encoder = new JpegBitmapEncoder();
                    encoder.QualityLevel = 94;
                    encoder.Frames.Add(BitmapFrame.Create(bitmapImage));
                    encoder.Save(fileStreamForSave);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
            finally
            {
                //if (DateTime.Now.Subtract(lastmemoryUpdateTime).Seconds > 30)
                //{
                //    MemoryManagement.FlushMemory();
                //    lastmemoryUpdateTime = DateTime.Now;
                //}
            }
        }

        /// <summary>
        /// Function is Used to read the medialooks color effects from a XML file 
        /// </summary>
        /// <param name="path">XML file Path</param>
        /// <returns>VideoColorEffects object</returns>
        /// <CreatedBy>Shailee Rawat</CreatedBy>
        public static VideoColorEffects ReadColorXml(string path)
        {
            VideoColorEffects vce = new VideoColorEffects();
            try
            {
                if (File.Exists(path))
                {
                    XmlDocument xmlDocument = new XmlDocument();
                    xmlDocument.Load(path);
                    vce.ULevel = Convert.ToDouble(xmlDocument.GetElementsByTagName("ULevel")[0].InnerText);
                    vce.UVGain = Convert.ToDouble(xmlDocument.GetElementsByTagName("UVGain")[0].InnerText);
                    vce.VLevel = Convert.ToDouble(xmlDocument.GetElementsByTagName("VLevel")[0].InnerText);
                    vce.YGain = Convert.ToDouble(xmlDocument.GetElementsByTagName("YGain")[0].InnerText);
                    vce.YLevel = Convert.ToDouble(xmlDocument.GetElementsByTagName("YLevel")[0].InnerText);
                    vce.BlackLevel = Convert.ToDouble(xmlDocument.GetElementsByTagName("BlackLevel")[0].InnerText);
                    vce.Brightness = Convert.ToDouble(xmlDocument.GetElementsByTagName("Brightness")[0].InnerText);
                    vce.ColorGain = Convert.ToDouble(xmlDocument.GetElementsByTagName("ColorGain")[0].InnerText);
                    vce.Contrast = Convert.ToDouble(xmlDocument.GetElementsByTagName("Contrast")[0].InnerText);
                    vce.WhiteLevel = Convert.ToDouble(xmlDocument.GetElementsByTagName("WhiteLevel")[0].InnerText);
                    vce.PresetEffects = (xmlDocument.GetElementsByTagName("PresetEffects")[0].InnerText);
                }
                else {
                    vce = VideoPresetColorEffects(3);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
            return vce;
        }
        public static VideoColorEffects VideoPresetColorEffects(int PresetEffectId)
        {
            VideoColorEffects vce = new VideoColorEffects();
            try
            {
                if (PresetEffectId == 1)//Grey Scale
                {
                    vce.ULevel = 0; vce.UVGain = 0; vce.VLevel = 0; vce.YGain = 100; vce.YLevel = 0;
                    vce.BlackLevel = 0; vce.Brightness = 0; vce.ColorGain = 100; vce.Contrast = 0; vce.WhiteLevel = 100;
                }
                if (PresetEffectId == 2)//Sepia
                {
                    vce.ULevel = -50; vce.UVGain = 15; vce.VLevel = 45; vce.YGain = 100; vce.YLevel = 0;
                    vce.BlackLevel = 0; vce.Brightness = 0; vce.ColorGain = 100; vce.Contrast = 0; vce.WhiteLevel = 100;
                }
                if (PresetEffectId == 3)//None
                {
                    vce.ULevel = 0; vce.UVGain = 100; vce.VLevel = 0; vce.YGain = 100; vce.YLevel = 0;
                    vce.BlackLevel = 0; vce.Brightness = 0; vce.ColorGain = 100; vce.Contrast = 0; vce.WhiteLevel = 100;
                }
                if (PresetEffectId == 4)//Invert --not applied yet
                {
                    vce.ULevel = -50; vce.UVGain = 15; vce.VLevel = 45; vce.YGain = 100; vce.YLevel = 0;
                    vce.BlackLevel = 0; vce.Brightness = 0; vce.ColorGain = 100; vce.Contrast = 0; vce.WhiteLevel = 100;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
            return vce;
        }
        public MCHROMAKEYLib.MChromaKey LoadChromaScreen(MItem pFile, string streamId)
        {
            MCHROMAKEYLib.MChromaKey objChromaKey = null;
            try
            {
                //    int ind; MItem pFile;
                //  m_objMixer.StreamsGet(streamId, out ind, out pFile);
                if (pFile != null)
                {
                    LoadChromaPlugin(true, pFile);
                    Thread.Sleep(500);
                    objChromaKey = GetChromakeyFilter(pFile);
                    try
                    {
                        ((IMProps)objChromaKey).PropsSet("gpu_mode", "true");
                    }
                    catch { }

                    Thread.Sleep(500);
                    if (objChromaKey != null)
                    {
                        // buttonChromaProps.IsEnabled = false;
                        FormChromaKey formCK = new FormChromaKey(objChromaKey);
                        formCK.ShowDialog();
                        // buttonChromaProps.IsEnabled = true;
                    }
                }
                return objChromaKey;
            }
            catch (Exception ex)
            {
                return null;
                //  LogConfigurator.log.Error("Error Message: " + ex.Message + " Error Stack Trace: " + ex.StackTrace);
            }
        }
        private MCHROMAKEYLib.MChromaKey GetChromakeyFilter(object source)
        {
            MCHROMAKEYLib.MChromaKey pChromaKey = null;
            try
            {
                int nCount = 0;
                IMPlugins pPlugins = (IMPlugins)source;
                pPlugins.PluginsGetCount(out nCount);
                for (int i = 0; i < nCount; i++)
                {
                    object pPlugin;
                    long nCBCookie;
                    pPlugins.PluginsGetByIndex(i, out pPlugin, out nCBCookie);
                    try
                    {
                        pChromaKey = (MCHROMAKEYLib.MChromaKey)pPlugin;
                        break;
                    }
                    catch { }
                }
            }
            catch { }
            return pChromaKey;
        }
        private MCHROMAKEYLib.MChromaKey LoadChromaPlugin(bool onloadChroma, object source, int a)
        {
            MCHROMAKEYLib.MChromaKey pChromaKey = null;
            try
            {
                //  if (mMixerList1.SelectedItem != null)
                {
                    IMPlugins pPlugins = (IMPlugins)source;
                    int nCount;
                    long nCBCookie;
                    object pPlugin = null;
                    bool isCK = false;
                    pPlugins.PluginsGetCount(out nCount);
                    for (int i = 0; i < nCount; i++)
                    {
                        pPlugins.PluginsGetByIndex(i, out pPlugin, out nCBCookie);

                        if (pPlugin.GetType().Name == "CoMChromaKeyClass" || pPlugin.GetType().Name == "MChromaKeyClass")
                        {
                            isCK = true;
                            break;
                        }
                    }
                    if ((isCK == false) || (onloadChroma && isCK == false))
                    {
                        pChromaKey = new MCHROMAKEYLib.MChromaKey();
                        pPlugins.PluginsAdd(pChromaKey, 0);
                    }
                    else if (isCK == true)
                    {
                        pPlugins.PluginsRemove(pPlugin);
                    }
                    //buttonChromaProps.IsEnabled = true;
                    //  Marshal.ReleaseComObject(pPlugins);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
            return pChromaKey;
        }
        private void LoadChromaPlugin(bool onloadChroma, object source)
        {
            try
            {
                if (source != null)
                {
                    IMPlugins pPlugins = source as IMPlugins;
                    int nCount;
                    long nCBCookie;
                    object pPlugin = null;
                    bool isCK = false;
                    pPlugins.PluginsGetCount(out nCount);
                    for (int i = 0; i < nCount; i++)
                    {
                        pPlugins.PluginsGetByIndex(i, out pPlugin, out nCBCookie);

                        if (pPlugin.GetType().Name == "CoMChromaKeyClass" || pPlugin.GetType().Name == "MChromaKeyClass")
                        {
                            isCK = true;
                            break;
                        }
                    }
                    if ((isCK == false) || (onloadChroma && isCK == false))
                    {
                        pPlugins.PluginsAdd(new MCHROMAKEYLib.MChromaKey(), 0);
                    }
                    // buttonChromaProps.IsEnabled = true;
                    //btnChromaSettings.IsEnabled = true;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }
    }

    /// <summary>
    /// Medialooks Color effects class
    /// <CreatedBy>Shailee Rawat</CreatedBy>
    /// </summary>
    public class VideoColorEffects
    {
        public double ULevel { get; set; }
        public double UVGain { get; set; }
        public double VLevel { get; set; }
        public double YGain { get; set; }
        public double YLevel { get; set; }
        public double BlackLevel { get; set; }
        public double Brightness { get; set; }
        public double ColorGain { get; set; }
        public double Contrast { get; set; }
        public double WhiteLevel { get; set; }
        public string PresetEffects { get; set; }

    }
}
