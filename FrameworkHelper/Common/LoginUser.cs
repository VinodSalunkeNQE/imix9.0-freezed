﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DigiPhoto.DataLayer;
using DigiPhoto.DataLayer.Model;
using DigiPhoto.IMIX.Model;
namespace DigiPhoto.Common
{
    public class LoginUser
    {
        /// <summary>
        /// My single ton instance
        /// </summary>
        private static LoginUser mySingleTonInstance = null;
        /// <summary>
        /// The digi folder path
        /// </summary>
        public static string DigiFolderPath;
        /// <summary>
        /// The digi images count
        /// </summary>
        public static int DigiImagesCount;
        /// <summary>
        /// The digi folder thumbnail path
        /// </summary>
        public static string DigiFolderThumbnailPath;
        /// <summary>
        /// The digi folder big thumbnail path
        /// </summary>
        public static string DigiFolderBigThumbnailPath;
        /// <summary>
        /// The receipt printer path
        /// </summary>
        public static string ReceiptPrinterPath;
        /// <summary>
        /// The order prefix
        /// </summary>
        public static string OrderPrefix = "DG";
        /// <summary>
        /// The is discount allowed
        /// </summary>
        public static bool? IsDiscountAllowed;
        /// <summary>
        /// The is discount allowedon total
        /// </summary>
        public static bool? IsDiscountAllowedonTotal;
        /// <summary>
        /// The is watermark
        /// </summary>
        public static bool? IsWatermark;
        /// <summary>
        /// The is high resolution
        /// </summary>
        public static bool? IsHighResolution;
        /// <summary>
        /// The is semi order
        /// </summary>
        public static bool? IsSemiOrder;

        // public static List<DigiPhoto.IMIX.Model.SemiOrderSettings> ListSemiOrderSettingsSubStoreWise1;

        public static List<SemiOrderSettings> ListSemiOrderSettingsSubStoreWise;
        /// <summary>
        /// The digi folder frame path
        /// </summary>
        public static string DigiFolderFramePath;
        /// <summary>
        /// The digi folder back ground path
        /// </summary>
        public static string DigiFolderBackGroundPath;
        /// <summary>
        /// The digi folder graphics path
        /// </summary>
        public static string DigiFolderGraphicsPath;
        /// <summary>
        /// The digi folder croped path
        /// </summary>
        public static string DigiFolderCropedPath;
        /// <summary>
        /// The digi folder Audio path
        /// </summary>
        public static string DigiFolderAudioPath;

        /// <summary>

        /// Video template path
        /// </summary>
        public static string DigiFolderVideoTemplatePath;
        /// <summary>

        /// The digi folder video Background path
        /// </summary>
        public static string DigiFolderVideoBackGroundPath;
        /// The digi folder video Background path
        /// </summary>
        public static string DigiFolderProcessedVideosPath;
        /// <summary>
        ///  /// The digi folder video Background path
        /// </summary>
        public static string DigiFolderVideoOverlayPath;

        /// Gets or sets the name of the user.
        /// </summary>
        /// <value>
        /// The name of the user.
        /// </value>
        public static string UserName { get; set; }
        public static string Storecode { get; set; }
        public static string countrycode { get; set; }
        /// <summary>
        /// Gets or sets the user unique identifier.
        /// </summary>
        /// <value>
        /// The user unique identifier.
        /// </value>
        public static Int32 UserId { get; set; }
        /// <summary>
        /// Gets or sets the role unique identifier.
        /// </summary>
        /// <value>
        /// The role unique identifier.
        /// </value>
        public static Int32 RoleId { get; set; }
        /// <summary>
        /// The mod password
        /// </summary>
        public static string ModPassword;
        /// <summary>
        /// Gets or sets the store unique identifier.
        /// </summary>
        /// <value>
        /// The store unique identifier.
        /// </value>
        public static Int32 StoreId { get; set; }
        /// <summary>
        /// Gets or sets the name of the store.
        /// </summary>
        /// <value>
        /// The name of the store.
        /// </value>
        public static string StoreName { get; set; }
        /// <summary>
        /// Gets or sets the sub store unique identifier.
        /// </summary>
        /// <value>
        /// The sub store unique identifier.
        /// </value>
        public static int SubStoreId { get; set; }
        /// <summary>
        /// Gets or sets the default substores.
        /// </summary>
        /// <value>
        /// The default substores.
        /// </value>
        public static string DefaultSubstores { get; set; }
        /// <summary>
        /// Gets or sets the name of the substore.
        /// </summary>
        /// <value>
        /// The name of the substore.
        /// </value>
        public static string SubstoreName { get; set; }

        public static string ServerHotFolderPath { get; set; }

        public static string GroupValue { get; set; }
        public static string ItemTemplatePath { get; set; }
        public static string ItemCalenderPath { get; set; }

        public static int MasterTemplateId { get; set; }

        /// <summary>
        /// Gets the instace.
        /// </summary>
        /// <returns></returns>
        public static LoginUser GetInstace()
        {
            if (mySingleTonInstance == null)
            {
                mySingleTonInstance = new LoginUser();
            }
            return mySingleTonInstance;
        }
        /// <summary>
        /// The default border path
        /// </summary>
        public static string DefaultBorderPath = "";
        /// <summary>
        /// The rotate
        /// </summary>
        public const int Rotate = 6;
        /// <summary>
        /// The flip
        /// </summary>
        public const int Flip = 5;
        /// <summary>
        /// The contrast
        /// </summary>
        public const int Contrast = 1;
        /// <summary>
        /// The brightness
        /// </summary>
        public const int Brightness = 2;
        public static int DGNoOfPhotoIdSearch;
        public static bool IsPhotographerSerailSearchActive = false;
        public static int PageCountGrid;
        public static string DigiReportPath { get; set; }
        ////public static void GetConfigData()
        // {
        //     try
        //     {
        //         DigiPhotoDataServices _objDataLayer = new DigiPhotoDataServices();
        //         vw_GetConfigdata _objdata = _objDataLayer.GetConfigurationData(LoginUser.SubStoreId);
        //         if (_objdata != null)
        //         {
        //             LoginUser.DigiFolderBackGroundPath = _objdata.DG_BG_Path;
        //             LoginUser.DigiFolderFramePath = _objdata.DG_Frame_Path;
        //             LoginUser.DigiFolderPath = _objdata.DG_Hot_Folder_Path;
        //             LoginUser.ModPassword = _objdata.DG_Mod_Password;
        //             LoginUser.DigiFolderGraphicsPath = _objdata.DG_Graphics_Path;
        //             LoginUser.DigiImagesCount = _objdata.DG_NoOfPhotos;
        //             LoginUser.IsWatermark = _objdata.DG_Watermark;
        //             LoginUser.IsHighResolution = _objdata.DG_HighResolution;
        //             LoginUser.IsSemiOrder = _objdata.DG_SemiOrder;
        //             LoginUser.DigiFolderBigThumbnailPath = _objdata.DG_Hot_Folder_Path + "Thumbnails_Big\\";
        //             LoginUser.DigiFolderCropedPath = _objdata.DG_Hot_Folder_Path + "Croped\\";
        //             LoginUser.DigiFolderThumbnailPath = _objdata.DG_Hot_Folder_Path + "Thumbnails\\";
        //             LoginUser.IsDiscountAllowed = _objdata.DG_AllowDiscount;
        //             LoginUser.IsDiscountAllowedonTotal = _objdata.DG_EnableDiscountOnTotal;
        //         }
        //     }
        //     catch (Exception ex)
        //     {
        //         string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
        //         ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
        //     }

        // }
    }

}
