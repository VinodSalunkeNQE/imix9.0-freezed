﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Drawing.Imaging;
using System.Diagnostics;

namespace DigiPhoto.Common
{
    public class BarCodeGenerator
    {
        /// <summary>
        /// Code39
        /// </summary>
        public class Code39
        {
            #region Static initialization

            /// <summary>
            /// The codes
            /// </summary>
            static Dictionary<char, Pattern> codes;

            /// <summary>
            /// Initializes the <see cref="Code39"/> class.
            /// </summary>
            static Code39()
            {
                object[][] chars = new object[][] 
            {
                new object[] {'0', "n n n w w n w n n"},
                new object[] {'1', "w n n w n n n n w"},
                new object[] {'2', "n n w w n n n n w"},
                new object[] {'3', "w n w w n n n n n"},
                new object[] {'4', "n n n w w n n n w"},
                new object[] {'5', "w n n w w n n n n"},
                new object[] {'6', "n n w w w n n n n"},
                new object[] {'7', "n n n w n n w n w"},
                new object[] {'8', "w n n w n n w n n"},
                new object[] {'9', "n n w w n n w n n"},
                new object[] {'A', "w n n n n w n n w"},
                new object[] {'B', "n n w n n w n n w"},
                new object[] {'C', "w n w n n w n n n"},
                new object[] {'D', "n n n n w w n n w"},
                new object[] {'E', "w n n n w w n n n"},
                new object[] {'F', "n n w n w w n n n"},
                new object[] {'G', "n n n n n w w n w"},
                new object[] {'H', "w n n n n w w n n"},
                new object[] {'I', "n n w n n w w n n"},
                new object[] {'J', "n n n n w w w n n"},
                new object[] {'K', "w n n n n n n w w"},
                new object[] {'L', "n n w n n n n w w"},
                new object[] {'M', "w n w n n n n w n"},
                new object[] {'N', "n n n n w n n w w"},
                new object[] {'O', "w n n n w n n w n"},
                new object[] {'P', "n n w n w n n w n"},
                new object[] {'Q', "n n n n n n w w w"},
                new object[] {'R', "w n n n n n w w n"},
                new object[] {'S', "n n w n n n w w n"},
                new object[] {'T', "n n n n w n w w n"},
                new object[] {'U', "w w n n n n n n w"},
                new object[] {'V', "n w w n n n n n w"},
                new object[] {'W', "w w w n n n n n n"},
                new object[] {'X', "n w n n w n n n w"},
                new object[] {'Y', "w w n n w n n n n"},
                new object[] {'Z', "n w w n w n n n n"},
                new object[] {'-', "n w n n n n w n w"},
                new object[] {'.', "w w n n n n w n n"},
                new object[] {' ', "n w w n n n w n n"},
                new object[] {'*', "n w n n w n w n n"},
                new object[] {'$', "n w n w n w n n n"},
                new object[] {'/', "n w n w n n n w n"},
                new object[] {'+', "n w n n n w n w n"},
                new object[] {'%', "n n n w n w n w n"}
            };

                codes = new Dictionary<char, Pattern>();
                foreach (object[] c in chars)
                    codes.Add((char)c[0], Pattern.Parse((string)c[1]));
            }

            #endregion

            /// <summary>
            /// The pen
            /// </summary>
            private static Pen pen = new Pen(Color.Black);
            /// <summary>
            /// The brush
            /// </summary>
            private static Brush brush = Brushes.Black;

            /// <summary>
            /// The code
            /// </summary>
            private string code;
            /// <summary>
            /// The settings
            /// </summary>
            private Code39Settings settings;

            /// <summary>
            /// Initializes a new instance of the <see cref="Code39"/> class.
            /// </summary>
            /// <param name="code">The code.</param>
            public Code39(string code)
                : this(code, new Code39Settings())
            {
            }

            /// <summary>
            /// Initializes a new instance of the <see cref="Code39"/> class.
            /// </summary>
            /// <param name="code">The code.</param>
            /// <param name="settings">The settings.</param>
            /// <exception cref="System.ArgumentException">Invalid character encountered in specified code.</exception>
            public Code39(string code, Code39Settings settings)
            {
                foreach (char c in code)
                    if (!codes.ContainsKey(c))
                        throw new ArgumentException("Invalid character encountered in specified code.");

                if (!code.StartsWith("*"))
                    code = "*" + code;
                if (!code.EndsWith("*"))
                    code = code + "*";

                this.code = code;
                this.settings = settings;
            }

            /// <summary>
            /// Paints this instance.
            /// </summary>
            /// <returns></returns>
            public Bitmap Paint()
            {
                string code = this.code.Trim('*');

                SizeF sizeCodeText = Graphics.FromImage(new Bitmap(1, 1)).MeasureString(code, settings.Font);

                int w = settings.LeftMargin + settings.RightMargin;
                foreach (char c in this.code)
                    w += codes[c].GetWidth(settings) + settings.InterCharacterGap;
                w -= settings.InterCharacterGap;

                int h = settings.TopMargin + settings.BottomMargin + settings.BarCodeHeight;
                if (settings.DrawText)
                    h += settings.BarCodeToTextGapHeight + (int)sizeCodeText.Height;

                Bitmap bmp = new Bitmap(w, h, PixelFormat.Format32bppArgb);
                Graphics g = Graphics.FromImage(bmp);
                g.Clear(Color.White);
                int left = settings.LeftMargin;

               
                foreach (char c in this.code)
                    left += codes[c].Paint(settings, g, left) + settings.InterCharacterGap;

                if (settings.DrawText)
                {
                    int tX = settings.LeftMargin + (w - settings.LeftMargin - settings.RightMargin - (int)sizeCodeText.Width) / 2;

                    if (tX < 0)
                        tX = 0;

                    int tY = settings.TopMargin + settings.BarCodeHeight + settings.BarCodeToTextGapHeight;
                  //  g.DrawString(code, settings.Font, brush, tX, tY);
                }

                return bmp;
            }

            /// <summary>
            /// 
            /// </summary>
            private class Pattern
            {
                /// <summary>
                /// The nw
                /// </summary>
                private bool[] nw = new bool[9];

                /// <summary>
                /// Parses the specified arguments.
                /// </summary>
                /// <param name="s">The arguments.</param>
                /// <returns></returns>
                public static Pattern Parse(string s)
                {
                    Debug.Assert(s != null);

                    s = s.Replace(" ", "").ToLower();

                    Debug.Assert(s.Length == 9);
                    Debug.Assert(s.Replace("n", "").Replace("w", "").Length == 0);

                    Pattern p = new Pattern();

                    int i = 0;
                    foreach (char c in s)
                        p.nw[i++] = c == 'w';

                    return p;
                }

                /// <summary>
                /// Gets the width.
                /// </summary>
                /// <param name="settings">The settings.</param>
                /// <returns></returns>
                public int GetWidth(Code39Settings settings)
                {
                    int width = 0;

                    for (int i = 0; i < 9; i++)
                        width += (nw[i] ? settings.WideWidth : settings.NarrowWidth);

                    return width;
                }

                /// <summary>
                /// Paints the specified settings.
                /// </summary>
                /// <param name="settings">The settings.</param>
                /// <param name="g">The aggregate.</param>
                /// <param name="left">The left.</param>
                /// <returns></returns>
                public int Paint(Code39Settings settings, Graphics g, int left)
                {

                    int x = left;

                    int w = 0;
                    for (int i = 0; i < 9; i++)
                    {
                        int width = (nw[i] ? settings.WideWidth : settings.NarrowWidth);

                        if (i % 2 == 0)
                        {
                            Rectangle r = new Rectangle(x, settings.TopMargin, width, settings.BarCodeHeight);
                            g.FillRectangle(brush, r);
                            
                        }

                        x += width;
                        w += width;
                    }

                    return w;
                }
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public class Code39Settings
        {
            /// <summary>
            /// The height
            /// </summary>
            private int height = 20;//80

            /// <summary>
            /// Gets or sets the height of the bar code.
            /// </summary>
            /// <value>
            /// The height of the bar code.
            /// </value>
            public int BarCodeHeight
            {
                get { return height; }
                set { height = value; }
            }

            /// <summary>
            /// The draw text
            /// </summary>
            private bool drawText = true;

            /// <summary>
            /// Gets or sets a value indicating whether [draw text].
            /// </summary>
            /// <value>
            ///   <c>true</c> if [draw text]; otherwise, <c>false</c>.
            /// </value>
            public bool DrawText
            {
                get { return drawText; }
                set { drawText = value; }
            }

            /// <summary>
            /// The left margin
            /// </summary>
            private int leftMargin = 10;

            /// <summary>
            /// Gets or sets the left margin.
            /// </summary>
            /// <value>
            /// The left margin.
            /// </value>
            public int LeftMargin
            {
                get { return leftMargin; }
                set { leftMargin = value; }
            }

            /// <summary>
            /// The right margin
            /// </summary>
            private int rightMargin = 10;

            /// <summary>
            /// Gets or sets the right margin.
            /// </summary>
            /// <value>
            /// The right margin.
            /// </value>
            public int RightMargin
            {
                get { return rightMargin; }
                set { rightMargin = value; }
            }

            /// <summary>
            /// The top margin
            /// </summary>
            private int topMargin = 0;

            /// <summary>
            /// Gets or sets the top margin.
            /// </summary>
            /// <value>
            /// The top margin.
            /// </value>
            public int TopMargin
            {
                get { return topMargin; }
                set { topMargin = value; }
            }

            /// <summary>
            /// The bottom margin
            /// </summary>
            private int bottomMargin = 0;

            /// <summary>
            /// Gets or sets the bottom margin.
            /// </summary>
            /// <value>
            /// The bottom margin.
            /// </value>
            public int BottomMargin
            {
                get { return bottomMargin; }
                set { bottomMargin = value; }
            }

            /// <summary>
            /// The inter character gap
            /// </summary>
            private int interCharacterGap = 2;

            /// <summary>
            /// Gets or sets the inter character gap.
            /// </summary>
            /// <value>
            /// The inter character gap.
            /// </value>
            public int InterCharacterGap
            {
                get { return interCharacterGap; }
                set { interCharacterGap = value; }
            }

            /// <summary>
            /// The wide width
            /// </summary>
            private int wideWidth = 6;

            /// <summary>
            /// Gets or sets the width of the wide.
            /// </summary>
            /// <value>
            /// The width of the wide.
            /// </value>
            public int WideWidth
            {
                get { return wideWidth; }
                set { wideWidth = value; }
            }

            /// <summary>
            /// The narrow width
            /// </summary>
            private int narrowWidth = 2;

            /// <summary>
            /// Gets or sets the width of the narrow.
            /// </summary>
            /// <value>
            /// The width of the narrow.
            /// </value>
            public int NarrowWidth
            {
                get { return narrowWidth; }
                set { narrowWidth = value; }
            }

            /// <summary>
            /// The font
            /// </summary>
            private Font font = new Font(FontFamily.GenericSansSerif, 8);

            /// <summary>
            /// Gets or sets the font.
            /// </summary>
            /// <value>
            /// The font.
            /// </value>
            public Font Font
            {
                get { return font; }
                set { font = value; }
            }

            /// <summary>
            /// The code automatic text gap height
            /// </summary>
            private int codeToTextGapHeight = 0;

            /// <summary>
            /// Gets or sets the height of the bar code automatic text gap.
            /// </summary>
            /// <value>
            /// The height of the bar code automatic text gap.
            /// </value>
            public int BarCodeToTextGapHeight
            {
                get { return codeToTextGapHeight; }
                set { codeToTextGapHeight = value; }
            }
        }
    }
}
