﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Shapes;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;

//using Point = System.Drawing.Point;

namespace DigiPhoto.Common
{
    public class CroppingAdorner : Adorner
    {
        #region Private variables
        // Width of the thumbs.  I know these really aren't "pixels", but px
        // is still a good mnemonic.
        /// <summary>
        /// The _CPX thumb width
        /// </summary>
        private const int _cpxThumbWidth = 16;

        // PuncturedRect to hold the "Cropping" portion of the adorner
        /// <summary>
        /// The _PR crop mask
        /// </summary>
        private PuncturedRect _prCropMask;

        // Canvas to hold the thumbs so they can be moved in response to the user
        /// <summary>
        /// The _CNV thumbs
        /// </summary>
        private Canvas _cnvThumbs;

        // Cropping adorner uses Thumbs for visual elements.  
        // The Thumbs have built-in mouse input handling.
        /// <summary>
        /// The _CRT top left
        /// </summary>
        private CropThumb _crtTopLeft, _crtTopRight, _crtBottomLeft, _crtBottomRight;
        /// <summary>
        /// The _CRT top
        /// </summary>
        private CropThumb _crtTop, _crtLeft, _crtBottom, _crtRight;

        // To store and manage the adorner's visual children.
        /// <summary>
        /// The _VC
        /// </summary>
        private VisualCollection _vc;

        // DPI for screen
        /// <summary>
        /// The s_dpi executable
        /// </summary>
        public static double s_dpiX, s_dpiY;
        #endregion

        #region Properties
        /// <summary>
        /// Gets the clipping rectangle.
        /// </summary>
        /// <value>
        /// The clipping rectangle.
        /// </value>
        public Rect ClippingRectangle
        {
            get
            {
                return _prCropMask.RectInterior;
            }
        }
        #endregion

        #region Routed Events
        //public static readonly RoutedEvent CropChangedEvent = EventManager.RegisterRoutedEvent(
        //    "CropChanged",
        //    RoutingStrategy.Bubble,
        //    typeof(RoutedEventHandler),
        //    typeof(CroppingAdorner));

        //public event RoutedEventHandler CropChanged
        //{
        //    add
        //    {
        //        base.AddHandler(CroppingAdorner.CropChangedEvent, value);
        //    }
        //    remove
        //    {
        //        base.RemoveHandler(CroppingAdorner.CropChangedEvent, value);
        //    }
        //}
        #endregion

        #region Dependency Properties
        /// <summary>
        /// The fill property
        /// </summary>
        static public DependencyProperty FillProperty = Shape.FillProperty.AddOwner(typeof(CroppingAdorner));

        /// <summary>
        /// Gets or sets the fill.
        /// </summary>
        /// <value>
        /// The fill.
        /// </value>
        public Brush Fill
        {
            get { return (Brush)GetValue(FillProperty); }
            set { SetValue(FillProperty, value); }
        }

        /// <summary>
        /// Fills the property changed.
        /// </summary>
        /// <param name="d">The command.</param>
        /// <param name="args">The <see cref="DependencyPropertyChangedEventArgs"/> instance containing the event data.</param>
        private static void FillPropChanged(DependencyObject d, DependencyPropertyChangedEventArgs args)
        {
            CroppingAdorner crp = d as CroppingAdorner;

            if (crp != null)
            {
                crp._prCropMask.Fill = (Brush)args.NewValue;
            }
        }
        #endregion

        #region Constructor
        /// <summary>
        /// Initializes the <see cref="CroppingAdorner"/> class.
        /// </summary>
        static CroppingAdorner()
        {
            Color clr = Colors.Red;
            //System.Drawing.Graphics g = System.Drawing.Graphics.FromHwnd((IntPtr)0);

            s_dpiX = 300;
            s_dpiY = 300;
            clr.A = 80;
            FillProperty.OverrideMetadata(typeof(CroppingAdorner),
                new PropertyMetadata(
                    new SolidColorBrush(clr),
                    new PropertyChangedCallback(FillPropChanged)));
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="CroppingAdorner"/> class.
        /// </summary>
        /// <param name="adornedElement">The adorned element.</param>
        /// <param name="rcInit">The source initialize.</param>
        public CroppingAdorner(UIElement adornedElement, Rect rcInit)
            : base(adornedElement)
        {
            _vc = new VisualCollection(this);
            _prCropMask = new PuncturedRect();
            _prCropMask.IsHitTestVisible = false;
            _prCropMask.RectInterior = rcInit;
            _prCropMask.Fill = Fill;

            _vc.Add(_prCropMask);
            _cnvThumbs = new Canvas();
            _cnvThumbs.HorizontalAlignment = HorizontalAlignment.Stretch;
            _cnvThumbs.VerticalAlignment = VerticalAlignment.Stretch;

            _vc.Add(_cnvThumbs);
            BuildCorner(ref _crtTop, Cursors.SizeNS);
            BuildCorner(ref _crtBottom, Cursors.SizeNS);
            BuildCorner(ref _crtLeft, Cursors.SizeWE);
            BuildCorner(ref _crtRight, Cursors.SizeWE);
            BuildCorner(ref _crtTopLeft, Cursors.SizeNWSE);
            BuildCorner(ref _crtTopRight, Cursors.SizeNESW);
            BuildCorner(ref _crtBottomLeft, Cursors.SizeNESW);
            BuildCorner(ref _crtBottomRight, Cursors.SizeNWSE);

            // Add handlers for Cropping.
            _crtBottomLeft.DragDelta += new DragDeltaEventHandler(HandleBottomLeft);
            _crtBottomRight.DragDelta += new DragDeltaEventHandler(HandleBottomRight);
            _crtTopLeft.DragDelta += new DragDeltaEventHandler(HandleTopLeft);
            _crtTopRight.DragDelta += new DragDeltaEventHandler(HandleTopRight);
            _crtTop.DragDelta += new DragDeltaEventHandler(HandleTop);
            _crtBottom.DragDelta += new DragDeltaEventHandler(HandleBottom);
            _crtRight.DragDelta += new DragDeltaEventHandler(HandleRight);
            _crtLeft.DragDelta += new DragDeltaEventHandler(HandleLeft);

            //add eventhandler to drag and drop 
            adornedElement.MouseLeftButtonDown += new MouseButtonEventHandler(Handle_MouseLeftButtonDown);
            adornedElement.MouseLeftButtonUp += new MouseButtonEventHandler(Handle_MouseLeftButtonUp);
            adornedElement.MouseMove += new MouseEventHandler(Handle_MouseMove);


            // We have to keep the clipping interior withing the bounds of the adorned element
            // so we have to track it's size to guarantee that...
            FrameworkElement fel = adornedElement as FrameworkElement;

            if (fel != null)
            {
                fel.SizeChanged += new SizeChangedEventHandler(AdornedElement_SizeChanged);
            }
            OrigenY = 0;
            OrigenX = 0;
        }

        /// <summary>
        /// Releases the resources.
        /// </summary>
        /// <param name="adornedElement">The adorned element.</param>
        public void ReleaseResources(UIElement adornedElement)
        {
            //add eventhandler to drag and drop 
            adornedElement.MouseLeftButtonDown -= new MouseButtonEventHandler(Handle_MouseLeftButtonDown);
            adornedElement.MouseLeftButtonUp -= new MouseButtonEventHandler(Handle_MouseLeftButtonUp);
            adornedElement.MouseMove -= new MouseEventHandler(Handle_MouseMove);
        }

        #endregion

        #region Drag and drop handlers

        /// <summary>
        /// The origen executable
        /// </summary>
        Double OrigenX;
        /// <summary>
        /// The origen asynchronous
        /// </summary>
        Double OrigenY;

        //  generic handler move selection with Drag'n'Drop
        /// <summary>
        /// Handles the drag.
        /// </summary>
        /// <param name="dx">The index.</param>
        /// <param name="dy">The dy.</param>
        private void HandleDrag(double dx, double dy)
        {
            Rect rcInterior = _prCropMask.RectInterior;
            rcInterior = new Rect(
               dx,
               dy,
                rcInterior.Width,
                rcInterior.Height);

            _prCropMask.RectInterior = rcInterior;
            SetThumbs(_prCropMask.RectInterior);
           // RaiseEvent(new RoutedEventArgs(CropChangedEvent, this));
        }

        /// <summary>
        /// Handles the MouseMove event of the Handle control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="args">The <see cref="MouseEventArgs"/> instance containing the event data.</param>
        private void Handle_MouseMove(object sender, MouseEventArgs args)
        {
            Grid Marco = new Grid();

            if (sender is Grid)
            {
                Marco = sender as Grid;
                if (Marco.Name != "GrdCrop")
                {
                    return;
                }
            }

            if (Marco != null && Marco.IsMouseCaptured)
            {
                //AdornedElement_SizeChanged(sender, args);
                Double x = args.GetPosition(Marco).X; //posición actual cursor
                Double y = args.GetPosition(Marco).Y;
                Double _x = _prCropMask.RectInterior.X; // posición actual esquina superior izq del marco interior
                Double _y = _prCropMask.RectInterior.Y;
                Double _width = _prCropMask.RectInterior.Width; //dimensiones del marco interior
                Double _height = _prCropMask.RectInterior.Height;

                if (x == OrigenX && y == OrigenY)
                {
                    return;
                }
                //si el click es dentro del marco interior
                if (((x > _x) && (x < (_x + _width))) && ((y > _y) && (y < (_y + _height))))
                {
                    //calculamos la diferencia de la posición actual del cursor con respecto al punto de origen del arrastre
                    //y se la añadimos a la esquina sup. izq. del marco interior.
                    _x = _x + (x - OrigenX);
                    _y = _y + (y - OrigenY);

                    //comprobamos si es posible mover sin salirse del marco exterior por ninguna de sus dimensiones
                    //no supera el borde izquierdo de la imagen: !(_x < 0)
                    if (_x < 0)
                    {
                        _x = 0;
                    }
                    //no supera el borde derecho de la imagen: !((_x + _width) > Marco.Width)
                    if ((_x + _width) > Marco.ActualWidth)
                    {
                        _x = Marco.ActualWidth - _width;
                    }
                    //no supera el borde superior de la imagen: !(_y<0)
                    if (_y < 0)
                    {
                        _y = 0;
                    }
                    //no supera el borde inferior de la imagen: !((_y + _height) > Marco.Height)
                    if ((_y + _height) > Marco.ActualHeight)
                    {
                        _y = Marco.ActualHeight - _height;
                    }

                    //asignamos nuevo punto origen del arrastre y movemos el marco interior
                    OrigenX = x;
                    OrigenY = y;
                    HandleDrag(_x, _y);

                }
            }

        }

        /// <summary>
        /// Handles the MouseLeftButtonDown event of the Handle control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="MouseButtonEventArgs"/> instance containing the event data.</param>
        private void Handle_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {

            Grid Marco = new Grid();

            if (sender is Grid)
            {
                Marco = sender as Grid;
                if (Marco.Name != "GrdCrop")
                {
                    return;
                }
            }

            if (Marco != null)
            { OrigenX = e.GetPosition(Marco).X; //iniciamos las variables en el punto de origen del arrastre
                OrigenY = e.GetPosition(Marco).Y;
                Marco.CaptureMouse();
               
                //AdornedElement_SizeChanged(sender, e);
            }
        }

        /// <summary>
        /// Handles the MouseLeftButtonUp event of the Handle control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="MouseButtonEventArgs"/> instance containing the event data.</param>
        private void Handle_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            Grid Marco = new Grid();

            if (sender is Grid)
            {
                Marco = sender as Grid;
                if (Marco.Name != "GrdCrop")
                {
                    return;
                }
            }

            if (Marco != null)
            {
                Marco.ReleaseMouseCapture();
                //AdornedElement_SizeChanged(sender, e);
            }
        }

        #endregion

        #region Thumb handlers
        // Generic handler for Cropping
        /// <summary>
        /// Handles the thumb.
        /// </summary>
        /// <param name="drcL">The DRC calculate.</param>
        /// <param name="drcT">The DRC attribute.</param>
        /// <param name="drcW">The DRC forward.</param>
        /// <param name="drcH">The DRC authentication.</param>
        /// <param name="dx">The index.</param>
        /// <param name="dy">The dy.</param>
        private void HandleThumb(
            double drcL,
            double drcT,
            double drcW,
            double drcH,
            double dx,
            double dy)
        {
            Rect rcInterior = _prCropMask.RectInterior;

            if (rcInterior.Width + drcW * dx < 0)
            {
                dx = -rcInterior.Width / drcW;
            }

            if (rcInterior.Height + drcH * dy < 0)
            {
                dy = -rcInterior.Height / drcH;
            }

            rcInterior = new Rect(
                rcInterior.Left + drcL * dx,
                rcInterior.Top + drcT * dy,
                rcInterior.Width + drcW * dx,
                rcInterior.Height + drcH * dy);

            _prCropMask.RectInterior = rcInterior;
            SetThumbs(_prCropMask.RectInterior);
            //RaiseEvent(new RoutedEventArgs(CropChangedEvent, this));
        }

        // Handler for Cropping from the bottom-left.
        /// <summary>
        /// Handles the bottom left.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="args">The <see cref="DragDeltaEventArgs"/> instance containing the event data.</param>
        private void HandleBottomLeft(object sender, DragDeltaEventArgs args)
        {
            //if (sender is CropThumb)
            //{
            //    HandleThumb(
            //        1, 0, -1, 1,
            //        args.HorizontalChange,
            //        args.VerticalChange);
            //}
        }

        // Handler for Cropping from the bottom-right.
        /// <summary>
        /// Handles the bottom right.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="args">The <see cref="DragDeltaEventArgs"/> instance containing the event data.</param>
        private void HandleBottomRight(object sender, DragDeltaEventArgs args)
        {
            //if (sender is CropThumb)
            //{
            //    HandleThumb(
            //        0, 0, 1, 1,
            //        args.HorizontalChange,
            //        args.VerticalChange);
            //}
        }

        // Handler for Cropping from the top-right.
        /// <summary>
        /// Handles the top right.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="args">The <see cref="DragDeltaEventArgs"/> instance containing the event data.</param>
        private void HandleTopRight(object sender, DragDeltaEventArgs args)
        {
            //if (sender is CropThumb)
            //{
            //    HandleThumb(
            //        0, 1, 1, -1,
            //        args.HorizontalChange,
            //        args.VerticalChange);
            //}
        }

        // Handler for Cropping from the top-left.
        /// <summary>
        /// Handles the top left.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="args">The <see cref="DragDeltaEventArgs"/> instance containing the event data.</param>
        private void HandleTopLeft(object sender, DragDeltaEventArgs args)
        {
            //if (sender is CropThumb)
            //{
            //    HandleThumb(
            //        1, 1, -1, -1,
            //        args.HorizontalChange,
            //        args.VerticalChange);
            //}
        }

        // Handler for Cropping from the top.
        /// <summary>
        /// Handles the top.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="args">The <see cref="DragDeltaEventArgs"/> instance containing the event data.</param>
        private void HandleTop(object sender, DragDeltaEventArgs args)
        {
            //if (sender is CropThumb)
            //{
            //    HandleThumb(
            //        0, 1, 0, -1,
            //        args.HorizontalChange,
            //        args.VerticalChange);
            //}
        }

        // Handler for Cropping from the left.
        /// <summary>
        /// Handles the left.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="args">The <see cref="DragDeltaEventArgs"/> instance containing the event data.</param>
        private void HandleLeft(object sender, DragDeltaEventArgs args)
        {
            //if (sender is CropThumb)
            //{
            //    HandleThumb(
            //        1, 0, -1, 0,
            //        args.HorizontalChange,
            //        args.VerticalChange);
            //}
        }

        // Handler for Cropping from the right.
        /// <summary>
        /// Handles the right.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="args">The <see cref="DragDeltaEventArgs"/> instance containing the event data.</param>
        private void HandleRight(object sender, DragDeltaEventArgs args)
        {
            //if (sender is CropThumb)
            //{
            //    HandleThumb(
            //        0, 0, 1, 0,
            //        args.HorizontalChange,
            //        args.VerticalChange);
            //}
        }

        // Handler for Cropping from the bottom.
        /// <summary>
        /// Handles the bottom.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="args">The <see cref="DragDeltaEventArgs"/> instance containing the event data.</param>
        private void HandleBottom(object sender, DragDeltaEventArgs args)
        {
            //if (sender is CropThumb)
            //{
            //    HandleThumb(
            //        0, 0, 0, 1,
            //        args.HorizontalChange,
            //        args.VerticalChange);
            //}
        }
        #endregion

        #region Other handlers
        /// <summary>
        /// Handles the SizeChanged event of the AdornedElement control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="SizeChangedEventArgs"/> instance containing the event data.</param>
        private void AdornedElement_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            FrameworkElement fel = sender as FrameworkElement;
            Rect rcInterior = _prCropMask.RectInterior;
            bool fFixupRequired = false;
            double
                intLeft = rcInterior.Left,
                intTop = rcInterior.Top,
                intWidth = rcInterior.Width,
                intHeight = rcInterior.Height;

            if (rcInterior.Left > fel.RenderSize.Width)
            {
                intLeft = fel.RenderSize.Width;
                intWidth = 0;
                fFixupRequired = true;
            }

            if (rcInterior.Top > fel.RenderSize.Height)
            {
                intTop = fel.RenderSize.Height;
                intHeight = 0;
                fFixupRequired = true;
            }

            if (rcInterior.Right > fel.RenderSize.Width)
            {
                intWidth = Math.Max(0, fel.RenderSize.Width - intLeft);
                fFixupRequired = true;
            }

            if (rcInterior.Bottom > fel.RenderSize.Height)
            {
                intHeight = Math.Max(0, fel.RenderSize.Height - intTop);
                fFixupRequired = true;
            }
            if (fFixupRequired)
            {
                _prCropMask.RectInterior = new Rect(intLeft, intTop, intWidth, intHeight);
            }
        }
        /// <summary>
        /// Handles the SizeChanged event of the AdornedElement control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="MouseEventArgs"/> instance containing the event data.</param>
        private void AdornedElement_SizeChanged(object sender, MouseEventArgs e)
        {
            FrameworkElement fel = sender as FrameworkElement;
            Rect rcInterior = _prCropMask.RectInterior;
            bool fFixupRequired = false;
            double
                intLeft = rcInterior.Left,
                intTop = rcInterior.Top,
                intWidth = rcInterior.Width,
                intHeight = rcInterior.Height;

            if (rcInterior.Left > fel.RenderSize.Width)
            {
                intLeft = fel.RenderSize.Width;
                intWidth = 0;
                fFixupRequired = true;
            }

            if (rcInterior.Top > fel.RenderSize.Height)
            {
                intTop = fel.RenderSize.Height;
                intHeight = 0;
                fFixupRequired = true;
            }

            if (rcInterior.Right > fel.RenderSize.Width)
            {
                intWidth = Math.Max(0, fel.RenderSize.Width - intLeft);
                fFixupRequired = true;
            }

            if (rcInterior.Bottom > fel.RenderSize.Height)
            {
                intHeight = Math.Max(0, fel.RenderSize.Height - intTop);
                fFixupRequired = true;
            }
            if (fFixupRequired)
            {
                _prCropMask.RectInterior = new Rect(intLeft, intTop, intWidth, intHeight);
            }
        }
        #endregion

        #region Arranging/positioning
        /// <summary>
        /// Sets the thumbs.
        /// </summary>
        /// <param name="rc">The source.</param>
        private void SetThumbs(Rect rc)
        {
            _crtBottomRight.SetPos(rc.Right, rc.Bottom);
            _crtTopLeft.SetPos(rc.Left, rc.Top);
            _crtTopRight.SetPos(rc.Right, rc.Top);
            _crtBottomLeft.SetPos(rc.Left, rc.Bottom);
            _crtTop.SetPos(rc.Left + rc.Width / 2, rc.Top);
            _crtBottom.SetPos(rc.Left + rc.Width / 2, rc.Bottom);
            _crtLeft.SetPos(rc.Left, rc.Top + rc.Height / 2);
            _crtRight.SetPos(rc.Right, rc.Top + rc.Height / 2);
        }

        // Arrange the Adorners.
        /// <summary>
        /// When overridden in a derived class, positions child elements and determines a size for a <see cref="T:System.Windows.FrameworkElement" /> derived class.
        /// </summary>
        /// <param name="finalSize">The final area within the parent that this element should use to arrange itself and its children.</param>
        /// <returns>
        /// The actual size used.
        /// </returns>
        protected override Size ArrangeOverride(Size finalSize)
        {
            Rect rcExterior = new Rect(0, 0, AdornedElement.RenderSize.Width, AdornedElement.RenderSize.Height);
            _prCropMask.RectExterior = rcExterior;
            Rect rcInterior = _prCropMask.RectInterior;

            _prCropMask.Arrange(rcExterior);

            SetThumbs(rcInterior);
            _cnvThumbs.Arrange(rcExterior);
            return finalSize;
        }
        #endregion

        #region Public interface
        /// <summary>
        /// Gets the cordinate.
        /// </summary>
        /// <returns></returns>
        public Int32Rect getCordinate()
        {
            Thickness margin = AdornerMargin();
            Rect rcInterior = _prCropMask.RectInterior;

            Point pxFromSize = UnitsToPx(rcInterior.Width, rcInterior.Height);

            // It appears that CroppedBitmap indexes from the upper left of the margin whereas RenderTargetBitmap renders the
            // control exclusive of the margin.  Hence our need to take the margins into account here...

            Point pxFromPos = UnitsToPx(rcInterior.Left + margin.Left, rcInterior.Top + margin.Top);
            Point pxWhole = UnitsToPx(AdornedElement.RenderSize.Width + margin.Left, AdornedElement.RenderSize.Height + margin.Left);
            pxFromSize.X = Math.Max(Math.Min(pxWhole.X - pxFromPos.X, pxFromSize.X), 0);
            pxFromSize.Y = Math.Max(Math.Min(pxWhole.Y - pxFromPos.Y, pxFromSize.Y), 0);
            //if (pxFromSize.X == 0 || pxFromSize.Y == 0)
            //{
            //    return null;
            //}
            System.Windows.Int32Rect rcFrom = new System.Windows.Int32Rect((int)pxFromPos.X, (int)pxFromPos.Y, (int)pxFromSize.X, (int)pxFromSize.Y);
            return rcFrom;
        }
        /// <summary>
        /// BPSs the crop.
        /// </summary>
        /// <returns></returns>
        public BitmapSource BpsCrop()
        {
            try
            {
                Thickness margin = AdornerMargin();
                Rect rcInterior = _prCropMask.RectInterior;

                Point pxFromSize = UnitsToPx(rcInterior.Width, rcInterior.Height);

                // It appears that CroppedBitmap indexes from the upper left of the margin whereas RenderTargetBitmap renders the
                // control exclusive of the margin.  Hence our need to take the margins into account here...

                Point pxFromPos = UnitsToPx(rcInterior.Left + margin.Left, rcInterior.Top + margin.Top);
                Point pxWhole = UnitsToPx(AdornedElement.RenderSize.Width + margin.Left, AdornedElement.RenderSize.Height  + margin.Left);
                pxFromSize.X = Math.Max(Math.Min(pxWhole.X - pxFromPos.X, pxFromSize.X), 0);
                pxFromSize.Y = Math.Max(Math.Min(pxWhole.Y - pxFromPos.Y, pxFromSize.Y), 0);
                if (pxFromSize.X == 0 || pxFromSize.Y == 0)
                {
                    return null;
                }
                System.Windows.Int32Rect rcFrom = new System.Windows.Int32Rect((int)pxFromPos.X, (int)pxFromPos.Y, (int)pxFromSize.X, (int)pxFromSize.Y);

                RenderTargetBitmap rtb = new RenderTargetBitmap((int)pxWhole.X, (int)pxWhole.Y, s_dpiX, s_dpiY, PixelFormats.Default);
                rtb.Render(AdornedElement);
                return new CroppedBitmap(rtb, rcFrom);
            }
            catch (Exception ex)
            {
                return null;
            }
            finally
            {
                MemoryManagement.FlushMemory();
            }
        }

        #endregion

        #region Helper functions
        /// <summary>
        /// Adorners the margin.
        /// </summary>
        /// <returns></returns>
        private Thickness AdornerMargin()
        {
            Thickness thick = new Thickness(0);
            if (AdornedElement is FrameworkElement)
            {
                thick = ((FrameworkElement)AdornedElement).Margin;
            }
            return thick;
        }

        /// <summary>
        /// Builds the corner.
        /// </summary>
        /// <param name="crt">The CRT.</param>
        /// <param name="crs">The CRS.</param>
        private void BuildCorner(ref CropThumb crt, Cursor crs)
        {
            if (crt != null) return;

            crt = new CropThumb(_cpxThumbWidth);

            // Set some arbitrary visual characteristics.
            crt.Cursor = crs;
            crt.Background = new SolidColorBrush(Colors.MediumBlue);

            _cnvThumbs.Children.Add(crt);
        }

        /// <summary>
        /// Unitses the automatic px.
        /// </summary>
        /// <param name="x">The executable.</param>
        /// <param name="y">The asynchronous.</param>
        /// <returns></returns>
        private Point UnitsToPx(double x, double y)
        {
            return new Point((int)(x * s_dpiX / 96), (int)(y * s_dpiY / 96));
        }
        #endregion

        #region Visual tree overrides
        // Override the VisualChildrenCount and GetVisualChild properties to interface with 
        // the adorner's visual collection.
        /// <summary>
        /// Gets the number of visual child elements within this element.
        /// </summary>
        /// <returns>The number of visual child elements for this element.</returns>
        protected override int VisualChildrenCount { get { return _vc.Count; } }
        /// <summary>
        /// Overrides <see cref="M:System.Windows.Media.Visual.GetVisualChild(System.Int32)" />, and returns a child at the specified index from a collection of child elements.
        /// </summary>
        /// <param name="index">The zero-based index of the requested child element in the collection.</param>
        /// <returns>
        /// The requested child element. This should not return null; if the provided index is out of range, an exception is thrown.
        /// </returns>
        protected override Visual GetVisualChild(int index) { return _vc[index]; }
        #endregion

        #region Internal Classes
        /// <summary>
        /// 
        /// </summary>
        class CropThumb : Thumb
        {
            #region Private variables
            /// <summary>
            /// The _CPX
            /// </summary>
            int _cpx;
            #endregion

            #region Constructor
            /// <summary>
            /// Initializes a new instance of the <see cref="CropThumb"/> class.
            /// </summary>
            /// <param name="cpx">The CPX.</param>
            internal CropThumb(int cpx)
                : base()
            {
                _cpx = cpx;
            }
            #endregion

            #region Overrides
            /// <summary>
            /// Overrides <see cref="M:System.Windows.Media.Visual.GetVisualChild(System.Int32)" />, and returns a child at the specified index from a collection of child elements.
            /// </summary>
            /// <param name="index">The zero-based index of the requested child element in the collection.</param>
            /// <returns>
            /// The requested child element. This should not return null; if the provided index is out of range, an exception is thrown.
            /// </returns>
            protected override Visual GetVisualChild(int index)
            {
                return null;
            }

            /// <summary>
            /// When overridden in a derived class, participates in rendering operations that are directed by the layout system. The rendering instructions for this element are not used directly when this method is invoked, and are instead preserved for later asynchronous use by layout and drawing.
            /// </summary>
            /// <param name="drawingContext">The drawing instructions for a specific element. This context is provided to the layout system.</param>
            protected override void OnRender(DrawingContext drawingContext)
            {
                drawingContext.DrawEllipse(Brushes.Wheat, new Pen(Brushes.Brown, 1), new System.Windows.Point(8, 8), 16, 16);
                //drawingContext.DrawRoundedRectangle(Brushes.White, new Pen(Brushes.Black, 1), new Rect(new Size(_cpx, _cpx)), 1, 1);
            }
            #endregion

            #region Positioning
            /// <summary>
            /// Sets the position.
            /// </summary>
            /// <param name="x">The executable.</param>
            /// <param name="y">The asynchronous.</param>
            internal void SetPos(double x, double y)
            {
                Canvas.SetTop(this, y - _cpx / 2);
                Canvas.SetLeft(this, x - _cpx / 2);
            }
            #endregion
        }
        #endregion
    }
}

