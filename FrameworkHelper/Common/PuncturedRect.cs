using System;
using System.Collections.Generic;
using System.Windows.Shapes;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace DigiPhoto.Common
{

	public class PuncturedRect : Shape
	{
		#region Dependency properties
        /// <summary>
        /// The rect interior property
        /// </summary>
		public static readonly DependencyProperty RectInteriorProperty =
			DependencyProperty.Register(
				"RectInterior",
				typeof(Rect),
				typeof(FrameworkElement),
				new FrameworkPropertyMetadata(
					new Rect(0, 0, 0, 0),
					FrameworkPropertyMetadataOptions.AffectsRender,
					null,
					new CoerceValueCallback(CoerceRectInterior),
					false
				),
				null
			);

        /// <summary>
        /// Coerces the rect interior.
        /// </summary>
        /// <param name="d">The command.</param>
        /// <param name="value">The value.</param>
        /// <returns></returns>
		private static object CoerceRectInterior(DependencyObject d, object value)
		{
			PuncturedRect pr = (PuncturedRect)d;
			Rect rcExterior = pr.RectExterior;
			Rect rcProposed = (Rect)value;
			double left = Math.Max(rcProposed.Left, rcExterior.Left);
			double top = Math.Max(rcProposed.Top, rcExterior.Top);
			double width = Math.Min(rcProposed.Right, rcExterior.Right) - left;
			double height = Math.Min(rcProposed.Bottom, rcExterior.Bottom) - top;
			rcProposed = new Rect(left, top, width, height);
			return rcProposed;
		}

        /// <summary>
        /// Gets or sets the rect interior.
        /// </summary>
        /// <value>
        /// The rect interior.
        /// </value>
		public Rect RectInterior
		{
			get { return (Rect)GetValue(RectInteriorProperty); }
			set { SetValue(RectInteriorProperty, value); }
		}


        /// <summary>
        /// The rect exterior property
        /// </summary>
		public static readonly DependencyProperty RectExteriorProperty =
			DependencyProperty.Register(
				"RectExterior",
				typeof(Rect),
				typeof(FrameworkElement),
				new FrameworkPropertyMetadata(
					new Rect(0, 0, double.MaxValue, double.MaxValue),
					FrameworkPropertyMetadataOptions.AffectsMeasure |
					FrameworkPropertyMetadataOptions.AffectsArrange |
					FrameworkPropertyMetadataOptions.AffectsParentMeasure |
					FrameworkPropertyMetadataOptions.AffectsParentArrange |
					FrameworkPropertyMetadataOptions.AffectsRender,
					null,
					null,
					false
				),
				null
			);

        /// <summary>
        /// Gets or sets the rect exterior.
        /// </summary>
        /// <value>
        /// The rect exterior.
        /// </value>
		public Rect RectExterior
		{
			get { return (Rect)GetValue(RectExteriorProperty); }
			set { SetValue(RectExteriorProperty, value); }
		}
		#endregion

		#region Constructors
        /// <summary>
        /// Initializes a new instance of the <see cref="PuncturedRect"/> class.
        /// </summary>
		public PuncturedRect() : this(new Rect(0, 0, double.MaxValue, double.MaxValue), new Rect()) { }

        /// <summary>
        /// Initializes a new instance of the <see cref="PuncturedRect"/> class.
        /// </summary>
        /// <param name="rectExterior">The rect exterior.</param>
        /// <param name="rectInterior">The rect interior.</param>
		public PuncturedRect(Rect rectExterior, Rect rectInterior)
		{
			RectInterior = rectInterior;
			RectExterior = rectExterior;
		}
		#endregion

		#region Geometry
        /// <summary>
        /// Gets a value that represents the <see cref="T:System.Windows.Media.Geometry" /> of the <see cref="T:System.Windows.Shapes.Shape" />.
        /// </summary>
        /// <returns>The <see cref="T:System.Windows.Media.Geometry" /> of the <see cref="T:System.Windows.Shapes.Shape" />.</returns>
		protected override Geometry DefiningGeometry
		{
			get
			{
				PathGeometry pthgExt = new PathGeometry();
				PathFigure pthfExt = new PathFigure();
				pthfExt.StartPoint = RectExterior.TopLeft;
				pthfExt.Segments.Add(new LineSegment(RectExterior.TopRight, false));
				pthfExt.Segments.Add(new LineSegment(RectExterior.BottomRight, false));
				pthfExt.Segments.Add(new LineSegment(RectExterior.BottomLeft, false));
				pthfExt.Segments.Add(new LineSegment(RectExterior.TopLeft, false));
				pthgExt.Figures.Add(pthfExt);

				Rect rectIntSect = Rect.Intersect(RectExterior, RectInterior);
				PathGeometry pthgInt = new PathGeometry();
				PathFigure pthfInt = new PathFigure();
				pthfInt.StartPoint = rectIntSect.TopLeft;
				pthfInt.Segments.Add(new LineSegment(rectIntSect.TopRight, false));
				pthfInt.Segments.Add(new LineSegment(rectIntSect.BottomRight, false));
				pthfInt.Segments.Add(new LineSegment(rectIntSect.BottomLeft, false));
				pthfInt.Segments.Add(new LineSegment(rectIntSect.TopLeft, false));
				pthgInt.Figures.Add(pthfInt);

				CombinedGeometry cmbg = new CombinedGeometry(GeometryCombineMode.Exclude, pthgExt, pthgInt);
				return cmbg;
			}
		}
		#endregion
	}
}
