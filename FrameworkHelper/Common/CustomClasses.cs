﻿using DigiPhoto.Common;
using DigiPhoto.DataLayer;
using DigiPhoto.IMIX.Business;
using DigiPhoto.IMIX.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Security.Cryptography;
using System.Text;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Threading;
using FrameworkHelper;

namespace DigiPhoto
{
    public static class RobotImageLoader
    {
        public static LstMyItems curItem;

        public static bool IsLastPage;

        public static bool IsNextPage = false;

        public static int StartIndex = 0;
        public static long StartIndexRFID = 0;
        public static long MaxPhotoId = 0;
        public static long MinPhotoId = 0;
        public static long MaxPhotoIdCriteria = 0;
        public static long MinPhotoIdCriteria = 0;
        public static long ImgCount = 0;
        public static bool IsMorePrevImages = true;
        public static bool IsMoreNextImages = false;
        public static int? CharacterId = 0;
        public static int StopIndex = 0;
        public static int BeforerPinterCount = 0;
        public static bool IsZeroSearchNeeded = false;
        public static List<string> ViewGroupedImagesCount;
        public static int NewRecord = 0;
        public static int _rfidSearch = 0;
        /// <summary>
        /// /////////created by latika for table work flow////////////////
        /// </summary>

        public static string TableName;
        public static string GuestName;
        public static string EmailID;
        public static string ContactNo;
        /// <summary>
        /// /////////end
        /// </summary>

        public static List<LstMyItems> NotPrintedImages;
        /// <summary>
        /// The print images
        /// </summary>
        public static List<LstMyItems> PrintImages;
        /// <summary>
        /// The LST unlocknames
        /// </summary>
        public static List<string> LstUnlocknames;
        /// <summary>
        /// The robot images
        /// </summary>
        public static List<LstMyItems> robotImages;
        /// <summary>
        /// The group images
        /// </summary>
        public static List<LstMyItems> GroupImages;
        /// <summary>
        /// The rfid
        /// </summary>
        public static string RFID = "1";
        /// <summary>
        /// The search criteria
        /// </summary>
        public static string SearchCriteria;
        //public static int MediaType = 0;
        /// <summary>
        /// The file path
        /// </summary>
        public static string FilePath;
        /// <summary>
        /// From time
        /// </summary>
        public static DateTime FromTime;
        /// <summary>
        /// The automatic time
        /// </summary>
        public static DateTime ToTime;
        /// <summary>
        /// The user unique identifier
        /// </summary>
        public static Int32 UserId;
        /// <summary>
        /// The page name
        /// </summary>
        public static string PageName;
        /// <summary>
        /// The location unique identifier
        /// </summary>
        public static Int32 LocationId;
        /// <summary>
        /// The photo unique identifier
        /// </summary>
        public static string PhotoId;


        public static string GroupId;
        /// <summary>
        /// The unique photo unique identifier
        /// </summary>
        public static Int32 UniquePhotoId;
        /// <summary>
        /// The searched store unique identifier
        /// </summary>
        public static string SearchedStoreId;

        /// <summary>
        /// QR/BAR Code 
        /// </summary>
        public static string Code;
        /// <summary>
        /// QR Code Type
        /// </summary>
        public static int CodeType;

        /// <summary>
        /// Used to Check if anonymous QR code enabled.
        /// </summary>
        public static bool IsAnonymousQrCodeEnabled;
        /// <summary>
        /// The objdb layer
        /// </summary>
        /// 
        //
        public static bool isGroup = false;

        //Added by Adarsh
        /// <summary>
        /// The position of the image in the LstMyItems
        /// </summary>
        public static int ListPosition;
        public static int totalCount = 0;
        public static int currentCount;
        public static int startLoad;
        public static List<LstMyItems> _objnewincrement = new List<LstMyItems>();
        /// <summary>
        /// Set the number of thumbnails to be loaded in one shot.
        /// </summary>
        public static int thumbSet = 20;

        public static int MediaTypes = 3;
        //public static DigiPhotoDataServices objdbLayer = new DigiPhotoDataServices();
        public static bool Is9ImgViewActive = false;//True if the 9 image view on VOS is active
        public static bool Is16ImgViewActive = false;//True if the 16 image view on VOS is active
        public static bool IsPreview9or16active = false;//True if last view selected was either 9 or 16 image view on VOS. Is used to set the start index for next page.
        public static bool IsPreviewModeActive = false;//True if the Enlarged image view on VOS is active
                                                       //-----Start-------Nilesh----Zero search --27 Dec 2018---      
        public static bool IsSearchPhotoForSubStore = false;
        //-----End-------Nilesh----Zero search --27 Dec 2018---
        public static List<string> PhotoIds { get; set; }
        public static List<LstMyItems> LoadImages()
        {
            List<LstMyItems> temprobotImages = new List<LstMyItems>();
            SearchDetailInfo Searchdetails = new SearchDetailInfo();
            SearchCriteriaInfo SearchBiz = new SearchCriteriaInfo();
            DirectoryInfo robotImageDir = new DirectoryInfo(LoginUser.DigiFolderPath);
            string ssInfo = GetShowAllSubstorePhotos() ? String.Empty : LoginUser.DefaultSubstores;
            List<ModratePhotoInfo> moderatePhotosList = new List<ModratePhotoInfo>();
            PhotoBusiness phBiz = new PhotoBusiness();
            moderatePhotosList = phBiz.GetModeratePhotos();
            ErrorHandler.ErrorHandler.LogFileWrite("RobotImageLoader.SearchCriteria =" + RobotImageLoader.SearchCriteria);
            try
            {

                if (RobotImageLoader.SearchCriteria == "TimeWithQrcode")
                {
#if DEBUG
                    var watch = FrameworkHelper.CommonUtility.Watch();
                    watch.Start();
#endif
                    #region TimewithQrcode
                    temprobotImages = SelectPhotoByTimeAndQRCode(temprobotImages, Searchdetails, SearchBiz, moderatePhotosList);

                    # endregion
#if DEBUG
                    if (watch != null)
                        FrameworkHelper.CommonUtility.WatchStop("Search TimeWithQrcode", watch);
#endif
                }
                else if (RobotImageLoader.SearchCriteria == "Time")
                {
#if DEBUG
                    var watch = FrameworkHelper.CommonUtility.Watch();
                    watch.Start();
#endif
                    #region Time Range Search
                    SelectPhotoByTime(temprobotImages, moderatePhotosList, phBiz);
                    #endregion
#if DEBUG
                    if (watch != null)
                        FrameworkHelper.CommonUtility.WatchStop("Search Time", watch);
#endif
                }
                else if (RobotImageLoader.SearchCriteria == "Group")
                {
#if DEBUG
                    var watch = FrameworkHelper.CommonUtility.Watch();
                    watch.Start();
#endif
                    #region Group Search
                    SelectPhotoByGroup(temprobotImages, moderatePhotosList, phBiz);
                    #endregion
#if DEBUG
                    if (watch != null)
                        FrameworkHelper.CommonUtility.WatchStop("Search Group", watch);
#endif
                }
                else if (RobotImageLoader.SearchCriteria == "QRCODE")
                {
#if DEBUG
                    var watch = FrameworkHelper.CommonUtility.Watch();
                    watch.Start();
#endif
                    #region QRCODE Search
                    SelectPhotoByQRCode(temprobotImages, moderatePhotosList, phBiz);
                    #endregion
#if DEBUG
                    if (watch != null)
                        FrameworkHelper.CommonUtility.WatchStop("Search QRCODE", watch);
#endif
                }
                else if (RobotImageLoader.SearchCriteria == "QRCODEGROUP")
                {
#if DEBUG
                    var watch = FrameworkHelper.CommonUtility.Watch();
                    watch.Start();
#endif
                    #region Group Search
                    SelectPhotoByQRCodeGroup(temprobotImages, moderatePhotosList, phBiz);
                    #endregion
#if DEBUG
                    if (watch != null)
                        FrameworkHelper.CommonUtility.WatchStop("Search QRCODE GROUP", watch);
#endif
                }
                else
                {

                    if (RFID == "0")
                    {
#if DEBUG
                        var watch = FrameworkHelper.CommonUtility.Watch();
                        watch.Start();
#endif
                        #region For Zero Search
                        SelectPhotoByZeroSearch(temprobotImages, ssInfo, moderatePhotosList, phBiz, RobotImageLoader.MediaTypes);
                        #endregion
#if DEBUG
                        if (watch != null)
                            FrameworkHelper.CommonUtility.WatchStop("Search 0", watch);
#endif
                    }

                    else
                    {
#if DEBUG
                        var watch = FrameworkHelper.CommonUtility.Watch();
                        watch.Start();
#endif
                        #region RFID search
                        SelectPhotoByRFID(temprobotImages, moderatePhotosList, phBiz);
                        #endregion
#if DEBUG
                        if (watch != null)
                            FrameworkHelper.CommonUtility.WatchStop("Search RFID", watch);
#endif
                    }

                }
                if (RobotImageLoader.SearchCriteria == "Group")
                {
                    GroupImages = temprobotImages;

                }
                else if (RobotImageLoader.SearchCriteria == "Time")
                {
                    robotImages = temprobotImages;
                }
                else if (RobotImageLoader.SearchCriteria == "TimeWithQrcode")
                {
                    robotImages = temprobotImages;
                }
                else if (RobotImageLoader.SearchCriteria == "QRCODE")
                {
                    robotImages = temprobotImages;
                }
                else
                {
                    if(_objnewincrement == null)
                    {
                        _objnewincrement = new List<LstMyItems>();
                    }
                    if ((_objnewincrement.Count == 0) && (temprobotImages.Count != 0))
                    {
                        robotImages = temprobotImages;
                    }
                    else
                    {
                        robotImages = _objnewincrement;
                    }
                }
                temprobotImages = null;
                return robotImages;
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                return robotImages;
            }
            finally
            {

                Searchdetails = null;
                SearchBiz = null;
            }
        }
        public static List<LstMyItems> LoadImages(SearchDetailInfo Searchdetails)
        {
            List<LstMyItems> temprobotImages = new List<LstMyItems>();
            SearchCriteriaInfo SearchBiz = new SearchCriteriaInfo();
            
            try
            {
                string ssInfo = GetShowAllSubstorePhotos() ? String.Empty : LoginUser.DefaultSubstores;
                //-----Start-------Nilesh----Zero search --27 Dec 2018---
                SetSearchSubstorePhotos();
                if (IsSearchPhotoForSubStore)
                 {
                    ssInfo = LoginUser.DefaultSubstores;
                    //if (LoginUser.DefaultSubstores.Split(',').Count() >= 2)
                    //{
                    //    ssInfo = LoginUser.DefaultSubstores.Substring(1).Substring(1);
                    //}
                }
                //-----End-------Nilesh----Zero search --27 Dec 2018---
               List <ModratePhotoInfo> moderatePhotosList = new List<ModratePhotoInfo>();
                PhotoBusiness phBiz = new PhotoBusiness();
                moderatePhotosList = phBiz.GetModeratePhotos();
                if (RobotImageLoader.SearchCriteria == "TimeWithQrcode")
                {
#if DEBUG
                    var watch = FrameworkHelper.CommonUtility.Watch();
                    watch.Start();
#endif
                    #region TimewithQrcode
                    temprobotImages = SelectPhotoByTimeAndQRCode(temprobotImages, Searchdetails, SearchBiz, moderatePhotosList);

                    # endregion
#if DEBUG
                    if (watch != null)
                        FrameworkHelper.CommonUtility.WatchStop("Search TimeWithQrcode", watch);
#endif
                }
                else if (RobotImageLoader.SearchCriteria == "Time")
                {
#if DEBUG
                    var watch = FrameworkHelper.CommonUtility.Watch();
                    watch.Start();
#endif
                    #region Time Range Search
                    SelectPhotoByTime(temprobotImages, moderatePhotosList, phBiz);
                    #endregion
#if DEBUG
                    if (watch != null)
                        FrameworkHelper.CommonUtility.WatchStop("Search Time", watch);
#endif
                }
                else if (RobotImageLoader.SearchCriteria == "Group")
                {
#if DEBUG
                    var watch = FrameworkHelper.CommonUtility.Watch();
                    watch.Start();
#endif
                    #region Group Search
                    SelectPhotoByGroup(temprobotImages, moderatePhotosList, phBiz);
                    #endregion
#if DEBUG
                    if (watch != null)
                        FrameworkHelper.CommonUtility.WatchStop("Search Group", watch);
#endif
                }
                else if (RobotImageLoader.SearchCriteria == "QRCODE")
                {
#if DEBUG
                    var watch = FrameworkHelper.CommonUtility.Watch();
                    watch.Start();
#endif
                    #region QRCODE Search
                    SelectPhotoByQRCode(temprobotImages, moderatePhotosList, phBiz);
                    #endregion
#if DEBUG
                    if (watch != null)
                        FrameworkHelper.CommonUtility.WatchStop("Search QRCODE", watch);
#endif
                }
                else if (RobotImageLoader.SearchCriteria == "QRCODEGROUP")
                {
#if DEBUG
                    var watch = FrameworkHelper.CommonUtility.Watch();
                    watch.Start();
#endif
                    #region Group Search
                    SelectPhotoByQRCodeGroup(temprobotImages, moderatePhotosList, phBiz);
                    #endregion
#if DEBUG
                    if (watch != null)
                        FrameworkHelper.CommonUtility.WatchStop("Search QRCODE GROUP", watch);
#endif
                }
                else if (RobotImageLoader.SearchCriteria == "FaceScan")
                {
#if DEBUG
                    var watch = FrameworkHelper.CommonUtility.Watch();
                    watch.Start();
#endif
                    #region Group Search
                    SelectPhotoByFaceScan(temprobotImages, moderatePhotosList, phBiz);
                    #endregion
#if DEBUG
                    if (watch != null)
                        FrameworkHelper.CommonUtility.WatchStop("Search QRCODE GROUP", watch);
#endif
                }
                else
                {

                    if (RFID == "0")
                    {
#if DEBUG
                        var watch = FrameworkHelper.CommonUtility.Watch();
                        watch.Start();
#endif
                        #region For Zero Search
                        SelectPhotoByZeroSearch(temprobotImages, ssInfo, moderatePhotosList, phBiz, RobotImageLoader.MediaTypes);
                        #endregion
                        
#if DEBUG
                        if (watch != null)
                            FrameworkHelper.CommonUtility.WatchStop("Search 0", watch);
#endif
                    }

                    else
                    {
#if DEBUG
                        var watch = FrameworkHelper.CommonUtility.Watch();
                        watch.Start();
#endif
                        #region RFID search
                        SelectPhotoByRFID(temprobotImages, moderatePhotosList, phBiz);
                        #endregion
#if DEBUG
                        if (watch != null)
                            FrameworkHelper.CommonUtility.WatchStop("Search RFID", watch);
#endif
                    }

                }
                if (RobotImageLoader.SearchCriteria == "Group")
                {
                    GroupImages = temprobotImages;

                }
                else if (RobotImageLoader.SearchCriteria == "Time")
                {
                    robotImages = temprobotImages;
                }
                else if (RobotImageLoader.SearchCriteria == "TimeWithQrcode")
                {
                    robotImages = temprobotImages;
                }
                else if (RobotImageLoader.SearchCriteria == "QRCODE")
                {
                    robotImages = temprobotImages;
                }
                else
                {
                    if ((_objnewincrement == null || _objnewincrement.Count == 0) && (temprobotImages.Count != 0))
                    {
                        robotImages = temprobotImages;
                    }
                    else
                    {
                        robotImages = _objnewincrement;
                    }
                }
                temprobotImages = null;
                
                return robotImages;
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                return robotImages;
            }
            finally
            {

                Searchdetails = null;
                SearchBiz = null;
            }
        }
        private static List<LstMyItems> SelectPhotoByTimeAndQRCode(List<LstMyItems> temprobotImages, SearchDetailInfo Searchdetails, SearchCriteriaInfo SearchBiz, List<ModratePhotoInfo> moderatePhotosList)
        {
            if (RobotImageLoader.robotImages == null)
            {
                RobotImageLoader.robotImages = new List<LstMyItems>();
            }
            if (RobotImageLoader.GroupImages == null)
            {
                RobotImageLoader.GroupImages = new List<LstMyItems>();
            }

            if (RobotImageLoader.PrintImages == null)
            {
                RobotImageLoader.PrintImages = new List<LstMyItems>();
            }
            GroupId = "";

            IsZeroSearchNeeded = false;
            StartIndex = 0;
            StopIndex = 0;
            //if (String.IsNullOrEmpty(SearchedStoreId))
            //{
            //    SearchedStoreId = LoginUser.DefaultSubstores;
            //}
            Searchdetails.FromDate = RobotImageLoader.FromTime;
            Searchdetails.ToDate = RobotImageLoader.ToTime;
            Searchdetails.Locationid = LocationId;
            Searchdetails.Userid = UserId;
            Searchdetails.PageSize = thumbSet;
            //if (Searchdetails.PageNumber == 0)
            //    Searchdetails.PageNumber = 1;
            //string[] strArray = SearchedStoreId.Split(',');
            Searchdetails.SubstoreId = RobotImageLoader.SearchedStoreId == "0" ? null : RobotImageLoader.SearchedStoreId;// strArray[0];
            Searchdetails.CharacterId = RobotImageLoader.CharacterId;
            Searchdetails.Qrcode = Code;
            Searchdetails.IsAnonymousQrcodeEnabled = IsAnonymousQrCodeEnabled;
/////changed by latika for table flow
		    Searchdetails.TableName = TableName;
            Searchdetails.GuestName = GuestName;
            Searchdetails.EmailID = EmailID;
            Searchdetails.ContactNo = ContactNo;
            ////by Latika for table flow
            // List<SearchDetailInfo> _objall = SearchBiz.GetSearchDetailWithQrcode(Searchdetails, out MaxPhotoIdCriteria, out MinPhotoIdCriteria, out ImgCount, RobotImageLoader.MediaTypes).OrderByDescending(o => o.PhotoId).ToList();
            List<SearchDetailInfo> _objall = SearchBiz.GetSearchDetailWithQrcodeTableFlow(Searchdetails, out MaxPhotoIdCriteria, out MinPhotoIdCriteria, out ImgCount, RobotImageLoader.MediaTypes).OrderByDescending(o => o.PhotoId).ToList();
           ////end

            ///List<SearchDetailInfo> _objall = SearchBiz.GetSearchDetailWithQrcode(Searchdetails, out MaxPhotoIdCriteria, out MinPhotoIdCriteria, out ImgCount, RobotImageLoader.MediaTypes).OrderByDescending(o => o.PhotoId).ToList();
            //List<vw_GetPhotoList> _objall = objdbLayer.GetSearchedPhoto(RobotImageLoader.FromTime, RobotImageLoader.ToTime, UserId, LocationId, SearchedStoreId);
            //List<ModratePhotoInfo> moderatePhotosList = new List<ModratePhotoInfo>();
            //PhotoBusiness phBiz = new PhotoBusiness();
            //moderatePhotosList = phBiz.GetModeratePhotos();
            VideoProcessingClass vpc = new VideoProcessingClass();
            if (_objall.Count > 0)
            {
                Searchdetails.TotalRecords = _objall.FirstOrDefault().TotalRecords;
                foreach (var photoItem in _objall)
                {
                    LstMyItems _objnew = new LstMyItems();
                    _objnew.OnlineQRCode = photoItem.OnlineQRCode;
                    _objnew.Name = photoItem.Name;
                    _objnew.PhotoId = photoItem.PhotoId;
                    _objnew.FileName = photoItem.FileName;
                    _objnew.HotFolderPath = photoItem.HotFolderPath;
                    _objnew.MediaType = photoItem.MediaType;
                    _objnew.VideoLength = photoItem.VideoLength;
                    _objnew.CreatedOn = photoItem.CreatedOn;
                    _objnew.PhotoLocation = photoItem.Locationid;
                    var moderateItem = moderatePhotosList.AsEnumerable().Where(x => x.DG_Mod_Photo_ID == photoItem.PhotoId);

                    if (moderateItem != null && moderateItem.Count() > 0)
                    {
                        _objnew.FilePath = _objnew.HotFolderPath + "/Locked.png";
                        _objnew.IsLocked = Visibility.Collapsed;
                        _objnew.IsPassKeyVisible = Visibility.Visible;
                    }
                    else
                    {
                        string fileName = photoItem.FileName.Replace(vpc.SupportedVideoFormats["avi"].ToString(), ".jpg").Replace(vpc.SupportedVideoFormats["mp4"].ToString(), ".jpg").Replace(vpc.SupportedVideoFormats["wmv"].ToString(), ".jpg").Replace(vpc.SupportedVideoFormats["mov"].ToString(), ".jpg").Replace(vpc.SupportedVideoFormats["3gp"].ToString(), ".jpg").Replace(vpc.SupportedVideoFormats["3g2"].ToString(), ".jpg").Replace(vpc.SupportedVideoFormats["m2v"].ToString(), ".jpg").Replace(vpc.SupportedVideoFormats["m4v"].ToString(), ".jpg").Replace(vpc.SupportedVideoFormats["flv"].ToString(), ".jpg").Replace(vpc.SupportedVideoFormats["mpeg"].ToString(), ".jpg").Replace(vpc.SupportedVideoFormats["mkv"].ToString(), ".jpg").Replace(vpc.SupportedVideoFormats["mts"].ToString(), ".jpg");
                        _objnew.BigThumbnailPath = Path.Combine(_objnew.BigDBThumnailPath, photoItem.CreatedOn.ToString("yyyyMMdd"), fileName);
                        _objnew.FilePath = Path.Combine(_objnew.ThumnailPath, photoItem.CreatedOn.ToString("yyyyMMdd"), fileName);
                        //_objnew.FilePath = Path.Combine(_objnew.ThumnailPath, photoItem.CreatedOn.ToString("yyyyMMdd"), photoItem.FileName);                                         
                        //_objnew.FilePath = LoginUser.DigiFolderThumbnailPath + photoItem.FileName;
                        _objnew.BmpImageGroup = new BitmapImage(new Uri(@"/images/view-group.png", UriKind.Relative));
                        _objnew.IsLocked = Visibility.Visible;
                        _objnew.IsPassKeyVisible = Visibility.Collapsed;
                    }
                    ////changed by latika for pre sold
                    if (Convert.ToInt32(photoItem.UploadStatus) == 2)
                    {
                        _objnew.BmpImageUplaoad = new BitmapImage(new Uri(@"/images/UploadRed.png", UriKind.Relative));
                    }
                    else if (Convert.ToInt32(photoItem.UploadStatus) == 1)
                    {
                        _objnew.BmpImageUplaoad = new BitmapImage(new Uri(@"/images/UploadGreen.png", UriKind.Relative));
                    }
                    ///end
                    if (RobotImageLoader.GroupImages != null)
                    {
                        var Groupeditem = RobotImageLoader.GroupImages.Where(t => t.PhotoId == photoItem.PhotoId).FirstOrDefault();
                        if (Groupeditem != null)
                        {
                            _objnew.BmpImageGroup = new BitmapImage(new Uri(@"/images/view-accept.png", UriKind.Relative));
                        }
                        else
                        {
                            _objnew.BmpImageGroup = new BitmapImage(new Uri(@"/images/view-group.png", UriKind.Relative));
                        }
                    }
                    else
                    {
                        _objnew.BmpImageGroup = new BitmapImage(new Uri(@"/images/view-group.png", UriKind.Relative));
                    }
                    if (RobotImageLoader.PrintImages != null)
                    {
                        var printitem = RobotImageLoader.PrintImages.Where(t => t.PhotoId == photoItem.PhotoId).FirstOrDefault();
                        if (printitem != null)
                        {
                            _objnew.PrintGroup = new BitmapImage(new Uri(@"/images/print-accept.png", UriKind.Relative));
                        }
                        else
                        {
                            _objnew.PrintGroup = new BitmapImage(new Uri(@"/images/print-group.png", UriKind.Relative));
                        }
                    }
                    else
                    {
                        // Added By KCB on 26 JUL 2018 for image selection
                        //_objnew.PrintGroup = new BitmapImage(new Uri(@"/images/print-group.png", UriKind.Relative));
                        _objnew.PrintGroup = new BitmapImage(new Uri(@"/images/Select-group.png", UriKind.Relative));
                        //End
                    }
                    //new 20/7/2017
                    if (RobotImageLoader.PrintImages != null)
                    {
                        var printitem = RobotImageLoader.PrintImages.Where(t => t.PhotoId == photoItem.PhotoId).FirstOrDefault();
                        if (printitem != null)
                        {
                            _objnew.SelectGroup = new BitmapImage(new Uri(@"/images/view-accept.png", UriKind.Relative));
                        }
                        else
                        {
                            _objnew.SelectGroup = new BitmapImage(new Uri(@"/images/select-group.png", UriKind.Relative));
                        }
                    }
                    else
                    {
                        _objnew.SelectGroup = new BitmapImage(new Uri(@"/images/Select-group.png", UriKind.Relative));
                    }
                    //end
                    if (RobotImageLoader.Is9ImgViewActive)
                    {
                        _objnew.GridMainHeight = 190;
                        _objnew.GridMainWidth = 226;
                        _objnew.GridMainRowHeight1 = 140;
                        _objnew.GridMainRowHeight2 = 50;
                    }
                    else
                    {
                        _objnew.GridMainHeight = 140;
                        _objnew.GridMainWidth = 170;
                        _objnew.GridMainRowHeight1 = 90;
                        _objnew.GridMainRowHeight2 = 60;
                    }
                    temprobotImages.Add(_objnew);
                }
            }
            else
            {
                temprobotImages = new List<LstMyItems>();
            }
            return temprobotImages;
        }
        private static void SelectPhotoByTime(List<LstMyItems> temprobotImages, List<ModratePhotoInfo> moderatePhotosList, PhotoBusiness phBiz)
        {
            GroupId = "";

            IsZeroSearchNeeded = false;
            StartIndex = 0;
            StopIndex = 0;
            if (String.IsNullOrEmpty(SearchedStoreId))
            {
                SearchedStoreId = LoginUser.DefaultSubstores;
            }

            List<PhotoInfo> _objall = phBiz.GetSearchedPhoto(RobotImageLoader.FromTime, RobotImageLoader.ToTime, UserId, LocationId, SearchedStoreId);
            //List<ModratePhotoInfo> moderatePhotosList = new List<ModratePhotoInfo>();
            //moderatePhotosList = phBiz.GetModeratePhotos();
            VideoProcessingClass vpc = new VideoProcessingClass();
            foreach (var photoItem in _objall)
            {
                LstMyItems _objnew = new LstMyItems();
                _objnew.Name = photoItem.DG_Photos_RFID;
                _objnew.PhotoId = photoItem.DG_Photos_pkey;
                _objnew.FileName = photoItem.DG_Photos_FileName;
                _objnew.HotFolderPath = photoItem.HotFolderPath;
                _objnew.MediaType = photoItem.DG_MediaType;
                _objnew.VideoLength = photoItem.DG_VideoLength;
                _objnew.CreatedOn = photoItem.DG_Photos_CreatedOn;
                _objnew.OnlineQRCode = photoItem.OnlineQRCode;
                _objnew.PhotoLocation = photoItem.DG_Location_Id;
                var moderateItem = moderatePhotosList.AsEnumerable().Where(x => x.DG_Mod_Photo_ID == photoItem.DG_Photos_pkey);
                if (moderateItem != null && moderateItem.Count() > 0)
                {
                    _objnew.FilePath = _objnew.HotFolderPath + "/Locked.png";
                    _objnew.IsLocked = Visibility.Collapsed;
                    _objnew.IsPassKeyVisible = Visibility.Visible;
                }
                else
                {
                    string fileName = photoItem.DG_Photos_FileName.Replace(vpc.SupportedVideoFormats["avi"].ToString(), ".jpg").Replace(vpc.SupportedVideoFormats["mp4"].ToString(), ".jpg").Replace(vpc.SupportedVideoFormats["wmv"].ToString(), ".jpg").Replace(vpc.SupportedVideoFormats["mov"].ToString(), ".jpg").Replace(vpc.SupportedVideoFormats["3gp"].ToString(), ".jpg").Replace(vpc.SupportedVideoFormats["3g2"].ToString(), ".jpg").Replace(vpc.SupportedVideoFormats["m2v"].ToString(), ".jpg").Replace(vpc.SupportedVideoFormats["m4v"].ToString(), ".jpg").Replace(vpc.SupportedVideoFormats["flv"].ToString(), ".jpg").Replace(vpc.SupportedVideoFormats["mpeg"].ToString(), ".jpg").Replace(vpc.SupportedVideoFormats["mkv"].ToString(), ".jpg").Replace(vpc.SupportedVideoFormats["mts"].ToString(), ".jpg");
                    _objnew.BigThumbnailPath = Path.Combine(_objnew.BigDBThumnailPath, photoItem.DG_Photos_CreatedOn.ToString("yyyyMMdd"), fileName);
                    _objnew.FilePath = Path.Combine(_objnew.ThumnailPath, photoItem.DG_Photos_CreatedOn.ToString("yyyyMMdd"), fileName);
                    // _objnew.FilePath = Path.Combine(_objnew.ThumnailPath, photoItem.DG_Photos_CreatedOn.ToString("yyyyMMdd"), photoItem.DG_Photos_FileName);                   
                    //_objnew.FilePath = LoginUser.DigiFolderThumbnailPath + photoItem.DG_Photos_FileName;
                    _objnew.BmpImageGroup = new BitmapImage(new Uri(@"/images/view-group.png", UriKind.Relative));
                    _objnew.IsLocked = Visibility.Visible;
                    _objnew.IsPassKeyVisible = Visibility.Collapsed;
                }
                ////changed by latika for pre sold
                if (Convert.ToInt32(photoItem.UploadStatus) == 2)
                {
                    _objnew.BmpImageUplaoad = new BitmapImage(new Uri(@"/images/UploadRed.png", UriKind.Relative));
                }
                else if (Convert.ToInt32(photoItem.UploadStatus) == 1)
                {
                    _objnew.BmpImageUplaoad = new BitmapImage(new Uri(@"/images/UploadGreen.png", UriKind.Relative));
                }
                ///end
                if (RobotImageLoader.GroupImages != null)
                {
                    var Groupeditem = RobotImageLoader.GroupImages.Where(t => t.PhotoId == photoItem.DG_Photos_pkey).FirstOrDefault();
                    if (Groupeditem != null)
                    {
                        _objnew.BmpImageGroup = new BitmapImage(new Uri(@"/images/view-accept.png", UriKind.Relative));
                    }
                    else
                    {
                        _objnew.BmpImageGroup = new BitmapImage(new Uri(@"/images/view-group.png", UriKind.Relative));
                    }
                }
                else
                {
                    _objnew.BmpImageGroup = new BitmapImage(new Uri(@"/images/view-group.png", UriKind.Relative));
                }
                if (RobotImageLoader.PrintImages != null)
                {
                    var printitem = RobotImageLoader.PrintImages.Where(t => t.PhotoId == photoItem.DG_Photos_pkey).FirstOrDefault();
                    if (printitem != null)
                    {
                        _objnew.PrintGroup = new BitmapImage(new Uri(@"/images/print-accept.png", UriKind.Relative));
                    }
                    else
                    {
                        _objnew.PrintGroup = new BitmapImage(new Uri(@"/images/print-group.png", UriKind.Relative));
                    }
                }
                else
                {
                    // Added By KCB on 26 JUL 2018 for image selection
                    //_objnew.PrintGroup = new BitmapImage(new Uri(@"/images/print-group.png", UriKind.Relative));
                    _objnew.PrintGroup = new BitmapImage(new Uri(@"/images/Select-group.png", UriKind.Relative));
                    //End
                }
                if (RobotImageLoader.Is9ImgViewActive)
                {
                    _objnew.GridMainHeight = 190;
                    _objnew.GridMainWidth = 226;
                    _objnew.GridMainRowHeight1 = 140;
                    _objnew.GridMainRowHeight2 = 50;
                }
                else
                {
                    _objnew.GridMainHeight = 140;
                    _objnew.GridMainWidth = 170;
                    _objnew.GridMainRowHeight1 = 90;
                    _objnew.GridMainRowHeight2 = 60;
                }
                temprobotImages.Add(_objnew);
            }
        }
        private static void SelectPhotoByGroup(List<LstMyItems> temprobotImages, List<ModratePhotoInfo> moderatePhotosList, PhotoBusiness phBiz)
        {
            IsZeroSearchNeeded = false;
            StartIndex = 0;
            StopIndex = 0;
            RFID = "";

            List<PhotoInfo> _objGrpList = phBiz.GetSavedGroupImages(GroupId);
            VideoProcessingClass vpc = new VideoProcessingClass();

            foreach (var item in _objGrpList)
            {
                LstMyItems _objnew = new LstMyItems();
                _objnew.Name = item.DG_Photos_RFID;
                _objnew.FileName = item.DG_Photos_FileName;
                _objnew.PhotoId = item.DG_Photos_pkey;
                _objnew.HotFolderPath = item.HotFolderPath;
                _objnew.MediaType = item.DG_MediaType;
                _objnew.VideoLength = item.DG_VideoLength;
                _objnew.PhotoLocation = item.DG_Location_Id;
                _objnew.CreatedOn = item.DG_Photos_CreatedOn;
                _objnew.OnlineQRCode = item.OnlineQRCode;
                var moderateItem = moderatePhotosList.AsEnumerable().Where(x => x.DG_Mod_Photo_ID == item.DG_Photos_pkey);
                if (moderateItem != null && moderateItem.Count() > 0)
                {
                    _objnew.BigThumbnailPath = Path.Combine(_objnew.BigDBThumnailPath, item.DG_Photos_CreatedOn.ToString("yyyyMMdd"), item.DG_Photos_FileName);
                    _objnew.FilePath = _objnew.HotFolderPath + "/Locked.png";
                    _objnew.IsLocked = Visibility.Collapsed;
                    _objnew.IsPassKeyVisible = Visibility.Visible;
                }
                else
                {
                    string fileName = item.DG_Photos_FileName.Replace(vpc.SupportedVideoFormats["avi"].ToString(), ".jpg").Replace(vpc.SupportedVideoFormats["mp4"].ToString(), ".jpg").Replace(vpc.SupportedVideoFormats["wmv"].ToString(), ".jpg").Replace(vpc.SupportedVideoFormats["mov"].ToString(), ".jpg").Replace(vpc.SupportedVideoFormats["3gp"].ToString(), ".jpg").Replace(vpc.SupportedVideoFormats["3g2"].ToString(), ".jpg").Replace(vpc.SupportedVideoFormats["m2v"].ToString(), ".jpg").Replace(vpc.SupportedVideoFormats["m4v"].ToString(), ".jpg").Replace(vpc.SupportedVideoFormats["flv"].ToString(), ".jpg").Replace(vpc.SupportedVideoFormats["mpeg"].ToString(), ".jpg").Replace(vpc.SupportedVideoFormats["mkv"].ToString(), ".jpg").Replace(vpc.SupportedVideoFormats["mts"].ToString(), ".jpg");
                    _objnew.BigThumbnailPath = Path.Combine(_objnew.BigDBThumnailPath, item.DG_Photos_CreatedOn.ToString("yyyyMMdd"), fileName);
                    _objnew.FilePath = Path.Combine(_objnew.ThumnailPath, item.DG_Photos_CreatedOn.ToString("yyyyMMdd"), fileName);
                    // _objnew.FilePath = Path.Combine(_objnew.ThumnailPath, item.DG_Photos_CreatedOn.ToString("yyyyMMdd"), item.DG_Photos_FileName);                                  
                    //_objnew.FilePath = LoginUser.DigiFolderThumbnailPath + item.DG_Photos_FileName;
                    if (string.IsNullOrEmpty(item.DG_Photos_Layering))
                    {
                        if (!string.IsNullOrEmpty(LoginUser.DefaultBorderPath))
                            _objnew.FrameBrdr = LoginUser.DefaultBorderPath;
                    }

                    _objnew.IsLocked = Visibility.Visible;
                    _objnew.IsPassKeyVisible = Visibility.Collapsed;
                }
                ////changed by latika for pre sold
                if (Convert.ToInt32(item.UploadStatus) == 2)
                {
                    _objnew.BmpImageUplaoad = new BitmapImage(new Uri(@"/images/UploadRed.png", UriKind.Relative));
                }
                else if (Convert.ToInt32(item.UploadStatus) == 1)
                {
                    _objnew.BmpImageUplaoad = new BitmapImage(new Uri(@"/images/UploadGreen.png", UriKind.Relative));
                }
                ///end
                var Groupeditem = RobotImageLoader.GroupImages.Where(t => t.PhotoId == item.DG_Photos_pkey).FirstOrDefault();
                if (Groupeditem == null)
                {
                    _objnew.BmpImageGroup = new BitmapImage(new Uri(@"/images/view-accept.png", UriKind.Relative));
                    RobotImageLoader.GroupImages.Add(_objnew);
                }
                _objnew.BmpImageGroup = new BitmapImage(new Uri(@"/images/view-accept.png", UriKind.Relative));
                // By KCB on 26 JUL 2018 for image selection
                //if (RobotImageLoader.PrintImages != null)
                //{
                //    var printitem = RobotImageLoader.PrintImages.Where(t => t.PhotoId == item.DG_Photos_pkey).FirstOrDefault();
                //    if (printitem != null)
                //    {
                //        _objnew.PrintGroup = new BitmapImage(new Uri(@"/images/print-accept.png", UriKind.Relative));
                //    }
                //    else
                //    {
                //        _objnew.PrintGroup = new BitmapImage(new Uri(@"/images/print-group.png", UriKind.Relative));
                //    }
                //}
                //else
                //{
                //    _objnew.PrintGroup = new BitmapImage(new Uri(@"/images/print-group.png", UriKind.Relative));
                //}

                if (RobotImageLoader.PrintImages != null)
                {
                    var printitem = RobotImageLoader.PrintImages.Where(t => t.PhotoId == item.DG_Photos_pkey).FirstOrDefault();
                    if (printitem != null)
                    {
                        _objnew.SelectGroup = new BitmapImage(new Uri(@"/images/Select-group.png", UriKind.Relative));
                    }
                    else
                    {
                        _objnew.SelectGroup = new BitmapImage(new Uri(@"/images/Select-group.png", UriKind.Relative));
                    }
                }
                else
                {
                    _objnew.SelectGroup = new BitmapImage(new Uri(@"/images/Select-group.png", UriKind.Relative));
                }
                //end
                if (RobotImageLoader.Is9ImgViewActive)
                {
                    _objnew.GridMainHeight = 190;
                    _objnew.GridMainWidth = 226;
                    _objnew.GridMainRowHeight1 = 140;
                    _objnew.GridMainRowHeight2 = 50;
                }
                else
                {
                    _objnew.GridMainHeight = 140;
                    _objnew.GridMainWidth = 170;
                    _objnew.GridMainRowHeight1 = 90;
                    _objnew.GridMainRowHeight2 = 60;
                }
                temprobotImages.Add(_objnew);
            }
        }
        private static void SelectPhotoByQRCode(List<LstMyItems> temprobotImages, List<ModratePhotoInfo> moderatePhotosList, PhotoBusiness phBiz)
        {
            if (RobotImageLoader.robotImages == null)
            {
                RobotImageLoader.robotImages = new List<LstMyItems>();
            }
            if (RobotImageLoader.GroupImages == null)
            {
                RobotImageLoader.GroupImages = new List<LstMyItems>();
            }

            if (RobotImageLoader.PrintImages == null)
            {
                RobotImageLoader.PrintImages = new List<LstMyItems>();
            }
            VideoProcessingClass vpc = new VideoProcessingClass();
            List<PhotoInfo> _objList = phBiz.GetImagesBYQRCode(Code, IsAnonymousQrCodeEnabled);
            foreach (var item in _objList)
            {
                LstMyItems _objnew = new LstMyItems();
                _objnew.HotFolderPath = item.HotFolderPath;
                _objnew.OnlineQRCode = item.OnlineQRCode;
                var moderateItem = moderatePhotosList.AsEnumerable().Where(x => x.DG_Mod_Photo_ID == item.DG_Photos_pkey);
                if (moderateItem != null && moderateItem.Count() > 0)
                {
                    _objnew.PhotoId = item.DG_Photos_pkey;
                    _objnew.Name = item.DG_Photos_RFID;
                    _objnew.FileName = item.DG_Photos_FileName;
                    _objnew.BmpImageGroup = null;

                    _objnew.FilePath = _objnew.HotFolderPath + "/Locked.png";
                    _objnew.IsLocked = Visibility.Collapsed;
                    _objnew.IsPassKeyVisible = Visibility.Visible;
                }
                else
                {
                    _objnew.PhotoId = item.DG_Photos_pkey;
                    _objnew.Name = item.DG_Photos_RFID;
                    _objnew.FileName = item.DG_Photos_FileName;
                    string fileName = item.DG_Photos_FileName.Replace(vpc.SupportedVideoFormats["avi"].ToString(), ".jpg").Replace(vpc.SupportedVideoFormats["mp4"].ToString(), ".jpg").Replace(vpc.SupportedVideoFormats["wmv"].ToString(), ".jpg").Replace(vpc.SupportedVideoFormats["mov"].ToString(), ".jpg").Replace(vpc.SupportedVideoFormats["3gp"].ToString(), ".jpg").Replace(vpc.SupportedVideoFormats["3g2"].ToString(), ".jpg").Replace(vpc.SupportedVideoFormats["m2v"].ToString(), ".jpg").Replace(vpc.SupportedVideoFormats["m4v"].ToString(), ".jpg").Replace(vpc.SupportedVideoFormats["flv"].ToString(), ".jpg").Replace(vpc.SupportedVideoFormats["mpeg"].ToString(), ".jpg").Replace(vpc.SupportedVideoFormats["mkv"].ToString(), ".jpg").Replace(vpc.SupportedVideoFormats["mts"].ToString(), ".jpg"); ;
                    _objnew.BigThumbnailPath = Path.Combine(_objnew.BigDBThumnailPath, item.DG_Photos_CreatedOn.ToString("yyyyMMdd"), fileName);
                    _objnew.FilePath = Path.Combine(_objnew.ThumnailPath, item.DG_Photos_CreatedOn.ToString("yyyyMMdd"), fileName);
                    ////changed by latika for pre sold
                    if (Convert.ToInt32(item.UploadStatus) == 2)
                    {
                        _objnew.BmpImageUplaoad = new BitmapImage(new Uri(@"/images/UploadRed.png", UriKind.Relative));
                    }
                    else if (Convert.ToInt32(item.UploadStatus) == 1)
                    {
                        _objnew.BmpImageUplaoad = new BitmapImage(new Uri(@"/images/UploadGreen.png", UriKind.Relative));
                    }
                    ///end
                    // _objnew.FilePath = Path.Combine(_objnew.ThumnailPath, item.DG_Photos_CreatedOn.ToString("yyyyMMdd"), item.DG_Photos_FileName);                                    
                    //_objnew.FilePath = LoginUser.DigiFolderThumbnailPath + item.DG_Photos_FileName;
                    if (string.IsNullOrEmpty(item.DG_Photos_Layering))
                    {
                        if (!string.IsNullOrEmpty(LoginUser.DefaultBorderPath))
                            _objnew.FrameBrdr = LoginUser.DefaultBorderPath;
                    }

                    _objnew.IsLocked = Visibility.Visible;
                    _objnew.IsPassKeyVisible = Visibility.Collapsed;
                }
                _objnew.PhotoLocation = item.DG_Location_Id;
                if (RobotImageLoader.GroupImages != null)
                {
                    var Groupeditem = RobotImageLoader.GroupImages.Where(t => t.PhotoId == item.DG_Photos_pkey).FirstOrDefault();
                    if (Groupeditem != null)
                        _objnew.BmpImageGroup = new BitmapImage(new Uri(@"/images/view-accept.png", UriKind.Relative));
                    else
                        _objnew.BmpImageGroup = new BitmapImage(new Uri(@"/images/view-group.png", UriKind.Relative));
                }
                else
                {
                    _objnew.BmpImageGroup = new BitmapImage(new Uri(@"/images/view-group.png", UriKind.Relative));
                }
                if (RobotImageLoader.PrintImages != null)
                {
                    var printitem = RobotImageLoader.PrintImages.Where(t => t.PhotoId == item.DG_Photos_pkey).FirstOrDefault();
                    if (printitem != null)
                        _objnew.PrintGroup = new BitmapImage(new Uri(@"/images/print-accept.png", UriKind.Relative));
                    else
                        _objnew.PrintGroup = new BitmapImage(new Uri(@"/images/print-group.png", UriKind.Relative));
                }
                else
                {
                    // Added By KCB on 26 JUL 2018 for image selection
                    //_objnew.PrintGroup = new BitmapImage(new Uri(@"/images/print-group.png", UriKind.Relative));
                    _objnew.PrintGroup = new BitmapImage(new Uri(@"/images/Select-group.png", UriKind.Relative));
                    //end
                }


                var rbtitem = RobotImageLoader.robotImages.Where(t => t.PhotoId == item.DG_Photos_pkey).FirstOrDefault();
                if (rbtitem == null)
                {
                    //Verify is exists in group images
                    var grpItem1 = RobotImageLoader.GroupImages.Where(gp1 => gp1.PhotoId == item.DG_Photos_pkey).FirstOrDefault();
                    if (grpItem1 != null)
                    {
                        _objnew.BmpImageGroup = new BitmapImage(new Uri(@"/images/view-accept.png", UriKind.Relative));
                    }
                    else
                    {
                        _objnew.BmpImageGroup = new BitmapImage(new Uri(@"/images/view-group.png", UriKind.Relative));
                    }
                    //Verify is exists in print images
                    var prtItem1 = RobotImageLoader.PrintImages.Where(pr1 => pr1.PhotoId == item.DG_Photos_pkey).FirstOrDefault();
                    if (prtItem1 != null)
                    {
                        _objnew.PrintGroup = new BitmapImage(new Uri(@"/images/print-accept.png", UriKind.Relative));
                    }
                    else
                    {
                        _objnew.PrintGroup = new BitmapImage(new Uri(@"/images/print-group.png", UriKind.Relative));
                    }
                    RobotImageLoader.robotImages.Add(_objnew);
                }

                if (RobotImageLoader.Is9ImgViewActive)
                {
                    _objnew.GridMainHeight = 190;
                    _objnew.GridMainWidth = 226;
                    _objnew.GridMainRowHeight1 = 140;
                    _objnew.GridMainRowHeight2 = 50;
                }
                else
                {
                    _objnew.GridMainHeight = 140;
                    _objnew.GridMainWidth = 170;
                    _objnew.GridMainRowHeight1 = 90;
                    _objnew.GridMainRowHeight2 = 60;
                }
                temprobotImages.Add(_objnew);
            }
        }
        private static void SelectPhotoByQRCodeGroup(List<LstMyItems> temprobotImages, List<ModratePhotoInfo> moderatePhotosList, PhotoBusiness phBiz)
        {
            IsZeroSearchNeeded = false;
            StartIndex = 0;
            StopIndex = 0;
            RFID = "";
            if (RobotImageLoader.GroupImages == null)
                RobotImageLoader.GroupImages = new List<LstMyItems>();
            //moderatePhotosList = objdbLayer.GetModeratePhotos();
            VideoProcessingClass vpc = new VideoProcessingClass();
            List<PhotoInfo> _objGrpList = phBiz.GetImagesBYQRCode(Code, IsAnonymousQrCodeEnabled);
            foreach (var item in _objGrpList)
            {
                LstMyItems _objnew = new LstMyItems();
                _objnew.Name = item.DG_Photos_RFID;
                _objnew.FileName = item.DG_Photos_FileName;
                _objnew.PhotoId = item.DG_Photos_pkey;
                _objnew.HotFolderPath = item.HotFolderPath;
                _objnew.MediaType = item.DG_MediaType;
                _objnew.VideoLength = item.DG_VideoLength;
                _objnew.PhotoLocation = item.DG_Location_Id;
                _objnew.CreatedOn = item.DG_Photos_CreatedOn;
                _objnew.OnlineQRCode = item.OnlineQRCode;
                var moderateItem = moderatePhotosList.AsEnumerable().Where(x => x.DG_Mod_Photo_ID == item.DG_Photos_pkey);
                if (moderateItem != null && moderateItem.Count() > 0)
                {
                    _objnew.FilePath = LoginUser.DigiFolderPath + "/Locked.png";
                    _objnew.IsLocked = Visibility.Collapsed;
                    _objnew.IsPassKeyVisible = Visibility.Visible;
                }
                else
                {
                    string fileName = item.DG_Photos_FileName.Replace(vpc.SupportedVideoFormats["avi"].ToString(), ".jpg").Replace(vpc.SupportedVideoFormats["mp4"].ToString(), ".jpg").Replace(vpc.SupportedVideoFormats["wmv"].ToString(), ".jpg").Replace(vpc.SupportedVideoFormats["mov"].ToString(), ".jpg").Replace(vpc.SupportedVideoFormats["3gp"].ToString(), ".jpg").Replace(vpc.SupportedVideoFormats["3g2"].ToString(), ".jpg").Replace(vpc.SupportedVideoFormats["m2v"].ToString(), ".jpg").Replace(vpc.SupportedVideoFormats["m4v"].ToString(), ".jpg").Replace(vpc.SupportedVideoFormats["flv"].ToString(), ".jpg").Replace(vpc.SupportedVideoFormats["mpeg"].ToString(), ".jpg").Replace(vpc.SupportedVideoFormats["mkv"].ToString(), ".jpg").Replace(vpc.SupportedVideoFormats["mts"].ToString(), ".jpg"); ;
                    _objnew.BigThumbnailPath = Path.Combine(_objnew.BigDBThumnailPath, item.DG_Photos_CreatedOn.ToString("yyyyMMdd"), fileName);
                    _objnew.FilePath = Path.Combine(_objnew.ThumnailPath, item.DG_Photos_CreatedOn.ToString("yyyyMMdd"), fileName);
                    // _objnew.FilePath = Path.Combine(_objnew.ThumnailPath, item.DG_Photos_CreatedOn.ToString("yyyyMMdd"), item.DG_Photos_FileName);                    
                    //_objnew.FilePath = LoginUser.DigiFolderThumbnailPath + item.DG_Photos_FileName;
                    if (string.IsNullOrEmpty(item.DG_Photos_Layering))
                    {
                        if (!string.IsNullOrEmpty(LoginUser.DefaultBorderPath))
                            _objnew.FrameBrdr = LoginUser.DefaultBorderPath;
                    }

                    _objnew.IsLocked = Visibility.Visible;
                    _objnew.IsPassKeyVisible = Visibility.Collapsed;
                }

                var Groupeditem = RobotImageLoader.GroupImages.Where(t => t.PhotoId == item.DG_Photos_pkey).FirstOrDefault();
                if (Groupeditem == null)
                {
                    _objnew.BmpImageGroup = new BitmapImage(new Uri(@"/images/view-accept.png", UriKind.Relative));
                    RobotImageLoader.GroupImages.Add(_objnew);
                }
                _objnew.BmpImageGroup = new BitmapImage(new Uri(@"/images/view-accept.png", UriKind.Relative));
                ////changed by latika for pre sold
                if (Convert.ToInt32(item.UploadStatus) == 2)
                {
                    _objnew.BmpImageUplaoad = new BitmapImage(new Uri(@"/images/UploadRed.png", UriKind.Relative));
                }
                else if (Convert.ToInt32(item.UploadStatus) == 1)
                {
                    _objnew.BmpImageUplaoad = new BitmapImage(new Uri(@"/images/UploadGreen.png", UriKind.Relative));
                }
                ///end
                if (RobotImageLoader.PrintImages != null)
                {
                    var printitem = RobotImageLoader.PrintImages.Where(t => t.PhotoId == item.DG_Photos_pkey).FirstOrDefault();
                    if (printitem != null)
                    {
                        _objnew.PrintGroup = new BitmapImage(new Uri(@"/images/print-accept.png", UriKind.Relative));
                    }
                    else
                    {
                        _objnew.PrintGroup = new BitmapImage(new Uri(@"/images/print-group.png", UriKind.Relative));
                    }
                }
                else
                {
                    // Added By KCB on 26 JUL 2018 for image selection
                    //_objnew.PrintGroup = new BitmapImage(new Uri(@"/images/print-group.png", UriKind.Relative));
                    _objnew.PrintGroup = new BitmapImage(new Uri(@"/images/Select-group.png", UriKind.Relative));
                    //end
                }
                if (RobotImageLoader.Is9ImgViewActive)
                {
                    _objnew.GridMainHeight = 190;
                    _objnew.GridMainWidth = 226;
                    _objnew.GridMainRowHeight1 = 140;
                    _objnew.GridMainRowHeight2 = 50;
                }
                else
                {
                    _objnew.GridMainHeight = 140;
                    _objnew.GridMainWidth = 170;
                    _objnew.GridMainRowHeight1 = 90;
                    _objnew.GridMainRowHeight2 = 60;
                }
                temprobotImages.Add(_objnew);
            }
        }
        private static void SelectPhotoByZeroSearch(List<LstMyItems> temprobotImages, string ssInfo, List<ModratePhotoInfo> moderatePhotosList, PhotoBusiness phBiz, int mediaType)
        {
            try
            {
                GroupId = "";

                long startIndex = 0;
                if (currentCount == 0 && IsZeroSearchNeeded)
                    startIndex = -1;
                else
                {
                    if (_objnewincrement != null && _objnewincrement.Count > 0 && !IsNextPage)
                        startIndex = _objnewincrement[_objnewincrement.Count - 1].DisplayOrder;
                    else if (_objnewincrement != null && _objnewincrement.Count > 0 && IsNextPage)
                        startIndex = _objnewincrement[0].DisplayOrder;
                    else
                        startIndex = -1;

                    if (!IsZeroSearchNeeded && currentCount == 0 && StartIndex != 0)
                    {
                        if (!IsNextPage)
                            startIndex = StartIndex;
                        else
                            startIndex = StopIndex;
                    }

                    if (IsPreview9or16active)
                    {
                        startIndex = StartIndex + 1;
                        IsPreview9or16active = false;
                    }
                }

                IsZeroSearchNeeded = false;
                StartIndex = 0;
                StopIndex = 0;
                List<PhotoInfo> lstAllLatest = new List<PhotoInfo>();
                if (_objnewincrement == null)
                    _objnewincrement = new List<LstMyItems>();
                _objnewincrement.Clear();

                if (IsNextPage)
                {
                    //lstAllLatest = objdbLayer.GetAllPhotosByPage(LoginUser.DefaultSubstores, startIndex, thumbSet, IsNextPage, out MaxPhotoId).OrderByDescending(x => x.DG_Photos_pkey).ToList();
                    //-----Start-------Nilesh----Zero search --27 Dec 2018---
                    //lstAllLatest = phBiz.GetAllPhotosByPage(ssInfo, startIndex, thumbSet, IsNextPage, out MaxPhotoId, out IsMoreNextImages, out MinPhotoId, mediaType, IsSearchPhotoForSubStore).OrderByDescending(x => x.DisplayOrder).ToList();
                    lstAllLatest = phBiz.GetAllPhotosByPageStoryBook(ssInfo, startIndex, thumbSet, IsNextPage, out MaxPhotoId, out IsMoreNextImages, out MinPhotoId, mediaType, IsSearchPhotoForSubStore).OrderByDescending(x => x.DisplayOrder).ToList();
                    //lstAllLatest = phBiz.GetAllPhotosByPage(ssInfo, startIndex, thumbSet, IsNextPage, out MaxPhotoId, out IsMoreNextImages, out MinPhotoId, mediaType, IsSearchPhotoForSubStore).ToList(); // For Sequnce Suraj.
                    IsMorePrevImages = true;
                }
                else
                {
                    //lstAllLatest = objdbLayer.GetAllPhotosByPage(LoginUser.DefaultSubstores, startIndex, thumbSet, IsNextPage, out MaxPhotoId).ToList();
                    //-----Start-------Nilesh----Zero search --27 Dec 2018---
                    lstAllLatest = phBiz.GetAllPhotosByPageStoryBook(ssInfo, startIndex, thumbSet, IsNextPage, out MaxPhotoId, out IsMorePrevImages, out MinPhotoId, mediaType, IsSearchPhotoForSubStore).ToList();
                    IsMoreNextImages = startIndex == -1 || MaxPhotoId + 1 == startIndex ? false : true;
                }

                IsNextPage = false;
                totalCount = lstAllLatest.Count;
                startLoad = 0;
                VideoProcessingClass vpc = new VideoProcessingClass();
                for (int j = 0; j < totalCount; j++)
                {
                    var item = lstAllLatest[j];
                    LstMyItems _objnew = new LstMyItems();
                    var moderateItem = moderatePhotosList.AsEnumerable().Where(x => x.DG_Mod_Photo_ID == item.DG_Photos_pkey);
                    _objnew.Name = item.DG_Photos_RFID;
                    _objnew.FileName = item.DG_Photos_FileName;
                    _objnew.ListPosition = currentCount;
                    _objnew.PhotoId = item.DG_Photos_pkey;
                    _objnew.HotFolderPath = item.HotFolderPath;
                    _objnew.MediaType = item.DG_MediaType;
                    _objnew.CreatedOn = item.DG_Photos_CreatedOn;
                    _objnew.VideoLength = item.DG_VideoLength;
                    _objnew.OnlineQRCode = item.OnlineQRCode;
                    _objnew.PhotoLocation = item.DG_Location_Id;
                    _objnew.DisplayOrder = item.DisplayOrder; // added by swapnil 03082021
                    _objnew.StoryBookId = item.StoryBookID;  //Added by VINS 25Oct2021
                    _objnew.PageNo = Convert.ToInt32(item.PageNo);//Added by VINS 25Oct2021
                    _objnew.IsVosDisplay = item.IsVosDisplay;
                    if (moderateItem != null && moderateItem.Count() > 0)
                    {
                        _objnew.FilePath = _objnew.HotFolderPath + "/Locked.png";
                        _objnew.IsLocked = Visibility.Collapsed;
                        _objnew.IsPassKeyVisible = Visibility.Visible;
                    }
                    else
                    {
                        string fileName = item.DG_Photos_FileName.Replace(vpc.SupportedVideoFormats["avi"].ToString(), ".jpg").Replace(vpc.SupportedVideoFormats["mp4"].ToString(), ".jpg").Replace(vpc.SupportedVideoFormats["wmv"].ToString(), ".jpg").Replace(vpc.SupportedVideoFormats["mov"].ToString(), ".jpg").Replace(vpc.SupportedVideoFormats["3gp"].ToString(), ".jpg").Replace(vpc.SupportedVideoFormats["3g2"].ToString(), ".jpg").Replace(vpc.SupportedVideoFormats["m2v"].ToString(), ".jpg").Replace(vpc.SupportedVideoFormats["m4v"].ToString(), ".jpg").Replace(vpc.SupportedVideoFormats["flv"].ToString(), ".jpg").Replace(vpc.SupportedVideoFormats["mpeg"].ToString(), ".jpg").Replace(vpc.SupportedVideoFormats["mkv"].ToString(), ".jpg").Replace(vpc.SupportedVideoFormats["mts"].ToString(), ".jpg");
                        _objnew.BigThumbnailPath = Path.Combine(_objnew.BigDBThumnailPath, item.DG_Photos_CreatedOn.ToString("yyyyMMdd"), fileName);
                        _objnew.FilePath = Path.Combine(_objnew.ThumnailPath, item.DG_Photos_CreatedOn.ToString("yyyyMMdd"), fileName);
                        //VideoProcessingClass vpc = new VideoProcessingClass();                   
                        //_objnew.FilePath = LoginUser.DigiFolderThumbnailPath + item.DG_Photos_FileName;
                        if (string.IsNullOrEmpty(item.DG_Photos_Layering))
                        {
                            if (!string.IsNullOrEmpty(LoginUser.DefaultBorderPath))
                            {
                                _objnew.FrameBrdr = LoginUser.DefaultBorderPath;
                            }
                        }
                        _objnew.IsLocked = Visibility.Visible;
                        _objnew.IsPassKeyVisible = Visibility.Collapsed;
                    }
                    ////changed by latika for pre sold
                    if (Convert.ToInt32(item.UploadStatus) == 2)
                    {
                        _objnew.BmpImageUplaoad = new BitmapImage(new Uri(@"/images/UploadRed.png", UriKind.Relative));
                    }
                    else if (Convert.ToInt32(item.UploadStatus) == 1)
                    {
                        _objnew.BmpImageUplaoad = new BitmapImage(new Uri(@"/images/UploadGreen.png", UriKind.Relative));
                    }

                    //Display Storybook image
                    if (Convert.ToInt32(item.StoryBookID) > 0)
                    {
                        _objnew.BmpImageUplaoad = new BitmapImage(new Uri(@"/images/flipbook.png", UriKind.Relative));
                    }

                    //end
                    if (RobotImageLoader.GroupImages != null)
                    {
                        var Groupeditem = RobotImageLoader.GroupImages.Where(t => t.PhotoId == item.DG_Photos_pkey).FirstOrDefault();
                        if (Groupeditem != null)
                            _objnew.BmpImageGroup = new BitmapImage(new Uri(@"/images/view-accept.png", UriKind.Relative));
                        else
                            _objnew.BmpImageGroup = new BitmapImage(new Uri(@"/images/view-group.png", UriKind.Relative));
                    }
                    else
                    {
                        _objnew.BmpImageGroup = new BitmapImage(new Uri(@"/images/view-group.png", UriKind.Relative));
                    }
                    if (RobotImageLoader.PrintImages != null)
                    {
                        var printitem = RobotImageLoader.PrintImages.Where(t => t.PhotoId == item.DG_Photos_pkey).FirstOrDefault();
                        if (printitem != null)
                            _objnew.PrintGroup = new BitmapImage(new Uri(@"/images/print-accept.png", UriKind.Relative));
                        else
                            _objnew.PrintGroup = new BitmapImage(new Uri(@"/images/print-group.png", UriKind.Relative));
                    }
                    else
                    {
                        // Added By KCB on 26 JUL 2018 for image selection
                        _objnew.PrintGroup = new BitmapImage(new Uri(@"/images/print-group.png", UriKind.Relative));
                        ///_objnew.PrintGroup = new BitmapImage(new Uri(@"/images/Select-group.png", UriKind.Relative)); //By KCB
                        //end

                    }

                    if (!IsPreviewModeActive && Is9ImgViewActive)
                    {
                        _objnew.GridMainHeight = 190;
                        _objnew.GridMainWidth = 226;
                        _objnew.GridMainRowHeight1 = 140;
                        _objnew.GridMainRowHeight2 = 50;
                    }
                    else if (!IsPreviewModeActive && Is16ImgViewActive)
                    {
                        _objnew.GridMainHeight = 140;
                        _objnew.GridMainWidth = 170;
                        _objnew.GridMainRowHeight1 = 90;
                        _objnew.GridMainRowHeight2 = 60;
                    }
                    else
                    {
                        _objnew.GridMainHeight = 140;
                        _objnew.GridMainWidth = 170;
                        _objnew.GridMainRowHeight1 = 90;
                        _objnew.GridMainRowHeight2 = 60;
                    }
                    temprobotImages.Add(_objnew);
                    if (_objnewincrement == null)
                    {
                        _objnewincrement = new List<LstMyItems>();
                    }

                    
                    _objnewincrement.Add(_objnew);
                    startLoad++;
                    currentCount++;
                }
            }
            catch (Exception ec)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ec);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }
        /// <summary>
        /// By KCB ON 08 JUN 2019 to bind photo metadata to searched <list type="."
        /// </summary>
        /// <param name="temprobotImages"></param>
        /// <param name="moderatePhotosList"></param>
        /// <param name="phBiz"></param>
        private static void SelectPhotoByFaceScan(List<LstMyItems> temprobotImages, List<ModratePhotoInfo> moderatePhotosList, PhotoBusiness phBiz)
        {
            GroupId = "";
            IsZeroSearchNeeded = false;
            StartIndex = 0;
            StopIndex = 0;
            int intNoOfPhotoIdSearch = thumbSet;
            VideoProcessingClass vpc = new VideoProcessingClass();
            List<PhotoInfo> lstAllLatest = new List<PhotoInfo>();
            lstAllLatest = phBiz.GetPhotoDetailsByPhotoIds(RobotImageLoader.PhotoIds).OrderByDescending(t => t.DG_Photos_pkey).ToList();
            List<PhotoInfo> SearchedImage = new List<PhotoInfo>();
            SearchedImage = lstAllLatest.ToList();
            //if (LoginUser.IsPhotographerSerailSearchActive == false)
            //{
            //    if (RobotImageLoader._rfidSearch == 0)
            //        SearchedImage = lstAllLatest.Where(T => T.DG_Photos_RFID == RobotImageLoader.RFID).ToList();
            //    else
            //        SearchedImage = lstAllLatest.ToList();
            //}
            //else
            //{
            //    SearchedImage = lstAllLatest.ToList();
            //}
            if (SearchedImage.Count != 0)
            {
                int imageid = SearchedImage.First().DG_Photos_pkey;
                var preImages = lstAllLatest.Where(t => t.DG_Photos_pkey < imageid).OrderByDescending(t => t.DG_Photos_pkey).ToList();
                if (preImages.Count > intNoOfPhotoIdSearch)
                {
                    preImages = preImages.Take(intNoOfPhotoIdSearch).ToList();
                }
                preImages = preImages.OrderBy(t => t.DG_Photos_pkey).ToList();
                preImages = preImages.Union(SearchedImage).ToList();
                var NextImages = lstAllLatest.Where(t => t.DG_Photos_pkey > imageid).OrderBy(t => t.DG_Photos_pkey).ToList();
                if (NextImages.Count > intNoOfPhotoIdSearch)
                {
                    NextImages = NextImages.Take(intNoOfPhotoIdSearch).ToList();
                }
                preImages = preImages.Union(NextImages).ToList();

                foreach (var item in preImages)
                {
                    LstMyItems _objnew = new LstMyItems();
                    _objnew.Name = item.DG_Photos_RFID;
                    _objnew.FileName = item.DG_Photos_FileName;
                    _objnew.PhotoId = item.DG_Photos_pkey;
                    _objnew.HotFolderPath = item.HotFolderPath;
                    _objnew.MediaType = item.DG_MediaType;
                    _objnew.CreatedOn = item.DG_Photos_CreatedOn;
                    _objnew.VideoLength = item.DG_VideoLength;
                    _objnew.OnlineQRCode = item.OnlineQRCode;
                    _objnew.PhotoLocation = item.DG_Location_Id;
                    var moderateItem = moderatePhotosList.AsEnumerable().Where(x => x.DG_Mod_Photo_ID == item.DG_Photos_pkey);
                    if (moderateItem != null && moderateItem.Count() > 0)
                    {
                        _objnew.FilePath = _objnew.HotFolderPath + "/Locked.png";
                        _objnew.IsLocked = Visibility.Collapsed;
                        _objnew.IsPassKeyVisible = Visibility.Visible;
                    }
                    else
                    {
                        string fileName = item.DG_Photos_FileName.Replace(vpc.SupportedVideoFormats["avi"].ToString(), ".jpg").Replace(vpc.SupportedVideoFormats["mp4"].ToString(), ".jpg").Replace(vpc.SupportedVideoFormats["wmv"].ToString(), ".jpg").Replace(vpc.SupportedVideoFormats["mov"].ToString(), ".jpg").Replace(vpc.SupportedVideoFormats["3gp"].ToString(), ".jpg").Replace(vpc.SupportedVideoFormats["3g2"].ToString(), ".jpg").Replace(vpc.SupportedVideoFormats["m2v"].ToString(), ".jpg").Replace(vpc.SupportedVideoFormats["m4v"].ToString(), ".jpg").Replace(vpc.SupportedVideoFormats["flv"].ToString(), ".jpg").Replace(vpc.SupportedVideoFormats["mpeg"].ToString(), ".jpg").Replace(vpc.SupportedVideoFormats["mkv"].ToString(), ".jpg").Replace(vpc.SupportedVideoFormats["mts"].ToString(), ".jpg"); ;
                        _objnew.BigThumbnailPath = Path.Combine(_objnew.BigDBThumnailPath, item.DG_Photos_CreatedOn.ToString("yyyyMMdd"), fileName);
                        _objnew.FilePath = Path.Combine(_objnew.ThumnailPath, item.DG_Photos_CreatedOn.ToString("yyyyMMdd"), fileName);

                        _objnew.IsLocked = Visibility.Visible;
                        if (string.IsNullOrEmpty(item.DG_Photos_Layering))
                        {
                            if (!string.IsNullOrEmpty(LoginUser.DefaultBorderPath))
                            {
                                _objnew.FrameBrdr = LoginUser.DefaultBorderPath;
                            }
                        }
                        _objnew.IsPassKeyVisible = Visibility.Collapsed;
                        _objnew.BmpImageGroup = new BitmapImage(new Uri(@"/images/view-group.png", UriKind.Relative));
                    }
                    ////changed by latika for pre sold
                    if (Convert.ToInt32(item.UploadStatus) == 2)
                    {
                        _objnew.BmpImageUplaoad = new BitmapImage(new Uri(@"/images/UploadRed.png", UriKind.Relative));
                    }
                    else if (Convert.ToInt32(item.UploadStatus) == 1)
                    {
                        _objnew.BmpImageUplaoad = new BitmapImage(new Uri(@"/images/UploadGreen.png", UriKind.Relative));
                    }
                    ///end
                    if (RobotImageLoader.GroupImages != null)
                    {
                        var Groupeditem = RobotImageLoader.GroupImages.Where(t => t.PhotoId == item.DG_Photos_pkey).FirstOrDefault();
                        if (Groupeditem != null)
                        {
                            _objnew.BmpImageGroup = new BitmapImage(new Uri(@"/images/view-accept.png", UriKind.Relative));
                        }
                        else
                        {
                            _objnew.BmpImageGroup = new BitmapImage(new Uri(@"/images/view-group.png", UriKind.Relative));
                        }
                    }
                    else
                    {
                        // Added By KCB on 26 JUL 2018 for image selection
                        //_objnew.PrintGroup = new BitmapImage(new Uri(@"/images/print-group.png", UriKind.Relative));
                        _objnew.PrintGroup = new BitmapImage(new Uri(@"/images/Select-group.png", UriKind.Relative));
                        //end
                    }
                    if (RobotImageLoader.PrintImages != null)
                    {
                        var printitem = RobotImageLoader.PrintImages.Where(t => t.PhotoId == item.DG_Photos_pkey).FirstOrDefault();
                        if (printitem != null)
                        {
                            _objnew.PrintGroup = new BitmapImage(new Uri(@"/images/print-accept.png", UriKind.Relative));
                        }
                        else
                        {
                            _objnew.PrintGroup = new BitmapImage(new Uri(@"/images/print-group.png", UriKind.Relative));
                        }
                    }
                    else
                    {
                        _objnew.PrintGroup = new BitmapImage(new Uri(@"/images/print-group.png", UriKind.Relative));
                    }
                    if (RobotImageLoader.Is9ImgViewActive)
                    {
                        _objnew.GridMainHeight = 190;
                        _objnew.GridMainWidth = 226;
                        _objnew.GridMainRowHeight1 = 140;
                        _objnew.GridMainRowHeight2 = 50;
                    }
                    else
                    {
                        _objnew.GridMainHeight = 140;
                        _objnew.GridMainWidth = 170;
                        _objnew.GridMainRowHeight1 = 90;
                        _objnew.GridMainRowHeight2 = 60;
                    }
                    temprobotImages.Add(_objnew);
                }
            }
        }
        private static void SelectPhotoByRFID(List<LstMyItems> temprobotImages, List<ModratePhotoInfo> moderatePhotosList, PhotoBusiness phBiz)
        {
            GroupId = "";
            IsZeroSearchNeeded = false;
            StartIndex = 0;
            StopIndex = 0;
            int intNoOfPhotoIdSearch = thumbSet;
            VideoProcessingClass vpc = new VideoProcessingClass();
            List<PhotoInfo> lstAllLatest = new List<PhotoInfo>();
            lstAllLatest = phBiz.GetAllPhotosforSearch(LoginUser.DefaultSubstores, Convert.ToInt64(RobotImageLoader.RFID), intNoOfPhotoIdSearch, LoginUser.IsPhotographerSerailSearchActive
                , RobotImageLoader.StartIndexRFID, RobotImageLoader._rfidSearch, RobotImageLoader.NewRecord, out MaxPhotoIdCriteria, out MinPhotoIdCriteria, RobotImageLoader.MediaTypes
                ).OrderByDescending(t => t.DisplayOrder).ToList();  // Suraj.. Change pkey to DisplayOrder
            List<PhotoInfo> SearchedImage = new List<PhotoInfo>();
            if (LoginUser.IsPhotographerSerailSearchActive == false)
            {
                if (RobotImageLoader._rfidSearch == 0)
                    SearchedImage = lstAllLatest.Where(T => T.DG_Photos_RFID == RobotImageLoader.RFID).ToList();
                else
                    SearchedImage = lstAllLatest.ToList();
            }
            else
            {
                SearchedImage = lstAllLatest.ToList();
            }
            if (SearchedImage.Count != 0)
            {
                long imageid = SearchedImage.First().DisplayOrder; // Suraj.. Change pkey to DisplayOrder
                var preImages = lstAllLatest.Where(t => t.DisplayOrder < imageid).OrderByDescending(t => t.DisplayOrder).ToList();// Suraj.. Change pkey to DisplayOrder
                if (preImages.Count > intNoOfPhotoIdSearch)
                {
                    preImages = preImages.Take(intNoOfPhotoIdSearch).ToList();
                }
                preImages = preImages.OrderBy(t => t.DisplayOrder).ToList(); // Suraj.. Change pkey to DisplayOrder
                preImages = preImages.Union(SearchedImage).ToList();
                var NextImages = lstAllLatest.Where(t => t.DisplayOrder > imageid).OrderBy(t => t.DisplayOrder).ToList(); // Suraj.. Change pkey to DisplayOrder
                if (NextImages.Count > intNoOfPhotoIdSearch)
                {
                    NextImages = NextImages.Take(intNoOfPhotoIdSearch).ToList();
                }
                preImages = preImages.Union(NextImages).ToList();

                foreach (var item in preImages)
                {
                    LstMyItems _objnew = new LstMyItems();
                    _objnew.Name = item.DG_Photos_RFID;
                    _objnew.FileName = item.DG_Photos_FileName;
                    _objnew.PhotoId = item.DG_Photos_pkey;
                    _objnew.HotFolderPath = item.HotFolderPath;
                    _objnew.MediaType = item.DG_MediaType;
                    _objnew.CreatedOn = item.DG_Photos_CreatedOn;
                    _objnew.VideoLength = item.DG_VideoLength;
                    _objnew.OnlineQRCode = item.OnlineQRCode;
                    _objnew.PhotoLocation = item.DG_Location_Id;
                    _objnew.DisplayOrder = item.DisplayOrder;// Suraj.. Change pkey to DisplayOrder

                    _objnew.StoryBookId = item.StoryBookID;  //Added by VINS 25Oct2021
                    _objnew.PageNo = Convert.ToInt32(item.PageNo);//Added by VINS 25Oct2021
                    var moderateItem = moderatePhotosList.AsEnumerable().Where(x => x.DG_Mod_Photo_ID == item.DG_Photos_pkey);
                    if (moderateItem != null && moderateItem.Count() > 0)
                    {
                        _objnew.FilePath = _objnew.HotFolderPath + "/Locked.png";
                        _objnew.IsLocked = Visibility.Collapsed;
                        _objnew.IsPassKeyVisible = Visibility.Visible;
                    }
                    else
                    {
                        string fileName = item.DG_Photos_FileName.Replace(vpc.SupportedVideoFormats["avi"].ToString(), ".jpg").Replace(vpc.SupportedVideoFormats["mp4"].ToString(), ".jpg").Replace(vpc.SupportedVideoFormats["wmv"].ToString(), ".jpg").Replace(vpc.SupportedVideoFormats["mov"].ToString(), ".jpg").Replace(vpc.SupportedVideoFormats["3gp"].ToString(), ".jpg").Replace(vpc.SupportedVideoFormats["3g2"].ToString(), ".jpg").Replace(vpc.SupportedVideoFormats["m2v"].ToString(), ".jpg").Replace(vpc.SupportedVideoFormats["m4v"].ToString(), ".jpg").Replace(vpc.SupportedVideoFormats["flv"].ToString(), ".jpg").Replace(vpc.SupportedVideoFormats["mpeg"].ToString(), ".jpg").Replace(vpc.SupportedVideoFormats["mkv"].ToString(), ".jpg").Replace(vpc.SupportedVideoFormats["mts"].ToString(), ".jpg");
                        _objnew.BigThumbnailPath = Path.Combine(_objnew.BigDBThumnailPath, item.DG_Photos_CreatedOn.ToString("yyyyMMdd"), fileName);
                        _objnew.FilePath = Path.Combine(_objnew.ThumnailPath, item.DG_Photos_CreatedOn.ToString("yyyyMMdd"), fileName);

                        _objnew.IsLocked = Visibility.Visible;
                        if (string.IsNullOrEmpty(item.DG_Photos_Layering))
                        {
                            if (!string.IsNullOrEmpty(LoginUser.DefaultBorderPath))
                            {
                                _objnew.FrameBrdr = LoginUser.DefaultBorderPath;
                            }
                        }
                        _objnew.IsPassKeyVisible = Visibility.Collapsed;
                        _objnew.BmpImageGroup = new BitmapImage(new Uri(@"/images/view-group.png", UriKind.Relative));
                    }

                    //Display Storybook image
                    if (Convert.ToInt32(item.StoryBookID) > 0)
                    {
                        _objnew.BmpImageUplaoad = new BitmapImage(new Uri(@"/images/flipbook.png", UriKind.Relative));
                    }

                    ////changed by latika for pre sold
                    if (Convert.ToInt32(item.UploadStatus) == 2)
                    {
                        _objnew.BmpImageUplaoad = new BitmapImage(new Uri(@"/images/UploadRed.png", UriKind.Relative));
                    }
                    else if (Convert.ToInt32(item.UploadStatus) == 1)
                    {
                        _objnew.BmpImageUplaoad = new BitmapImage(new Uri(@"/images/UploadGreen.png", UriKind.Relative));
                    }
                    ///end
                    if (RobotImageLoader.GroupImages != null)
                    {
                        var Groupeditem = RobotImageLoader.GroupImages.Where(t => t.PhotoId == item.DG_Photos_pkey).FirstOrDefault();
                        if (Groupeditem != null)
                        {
                            _objnew.BmpImageGroup = new BitmapImage(new Uri(@"/images/view-accept.png", UriKind.Relative));
                        }
                        else
                        {
                            _objnew.BmpImageGroup = new BitmapImage(new Uri(@"/images/view-group.png", UriKind.Relative));
                        }
                    }
                    else
                    {
                        // Added By KCB on 26 JUL 2018 for image selection
                        //_objnew.PrintGroup = new BitmapImage(new Uri(@"/images/print-group.png", UriKind.Relative));
                        _objnew.PrintGroup = new BitmapImage(new Uri(@"/images/Select-group.png", UriKind.Relative));
                        //end
                    }
                    if (RobotImageLoader.PrintImages != null)
                    {
                        var printitem = RobotImageLoader.PrintImages.Where(t => t.PhotoId == item.DG_Photos_pkey).FirstOrDefault();
                        if (printitem != null)
                        {
                            _objnew.PrintGroup = new BitmapImage(new Uri(@"/images/print-accept.png", UriKind.Relative));
                        }
                        else
                        {
                            _objnew.PrintGroup = new BitmapImage(new Uri(@"/images/print-group.png", UriKind.Relative));
                        }
                    }
                    else
                    {
                        _objnew.PrintGroup = new BitmapImage(new Uri(@"/images/print-group.png", UriKind.Relative));
                    }
                    if (RobotImageLoader.Is9ImgViewActive)
                    {
                        _objnew.GridMainHeight = 190;
                        _objnew.GridMainWidth = 226;
                        _objnew.GridMainRowHeight1 = 140;
                        _objnew.GridMainRowHeight2 = 50;
                    }
                    else
                    {
                        _objnew.GridMainHeight = 140;
                        _objnew.GridMainWidth = 170;
                        _objnew.GridMainRowHeight1 = 90;
                        _objnew.GridMainRowHeight2 = 60;
                    }
                    temprobotImages.Add(_objnew);
                }
            }
        }





        //        public static List<LstMyItems> LoadImages1()
        //        {
        //            List<LstMyItems> temprobotImages = new List<LstMyItems>();
        //            SearchDetailInfo Searchdetails = new SearchDetailInfo();
        //            SearchCriteriaInfo SearchBiz = new SearchCriteriaInfo();
        //            DirectoryInfo robotImageDir = new DirectoryInfo(LoginUser.DigiFolderPath);
        //            string ssInfo = GetShowAllSubstorePhotos() ? String.Empty : LoginUser.DefaultSubstores;
        //            List<ModratePhotoInfo> moderatePhotosList = new List<ModratePhotoInfo>();
        //            PhotoBusiness phBiz = new PhotoBusiness();
        //            moderatePhotosList = phBiz.GetModeratePhotos();
        //            try
        //            {

        //                if (RobotImageLoader.SearchCriteria == "TimeWithQrcode")
        //                {
        //#if DEBUG
        //                    var watch = FrameworkHelper.CommonUtility.Watch();
        //                    watch.Start();
        //#endif
        //                    #region TimewithQrcode
        //                    //RobotImageLoader.GroupImages = new List<LstMyItems>();
        //                    //Code = "";
        //                    if (RobotImageLoader.robotImages == null)
        //                    {
        //                        RobotImageLoader.robotImages = new List<LstMyItems>();
        //                    }
        //                    if (RobotImageLoader.GroupImages == null)
        //                    {
        //                        RobotImageLoader.GroupImages = new List<LstMyItems>();
        //                    }

        //                    if (RobotImageLoader.PrintImages == null)
        //                    {
        //                        RobotImageLoader.PrintImages = new List<LstMyItems>();
        //                    }
        //                    GroupId = "";

        //                    IsZeroSearchNeeded = false;
        //                    StartIndex = 0;
        //                    StopIndex = 0;
        //                    //if (String.IsNullOrEmpty(SearchedStoreId))
        //                    //{
        //                    //    SearchedStoreId = LoginUser.DefaultSubstores;
        //                    //}
        //                    Searchdetails.FromDate = RobotImageLoader.FromTime;
        //                    Searchdetails.ToDate = RobotImageLoader.ToTime;
        //                    Searchdetails.Locationid = LocationId;
        //                    Searchdetails.Userid = UserId;
        //                    //string[] strArray = SearchedStoreId.Split(',');
        //                    Searchdetails.SubstoreId = RobotImageLoader.SearchedStoreId == "0" ? null : RobotImageLoader.SearchedStoreId;
        //                    Searchdetails.Qrcode = Code;
        //                    Searchdetails.IsAnonymousQrcodeEnabled = IsAnonymousQrCodeEnabled;


        //                    List<SearchDetailInfo> _objall = SearchBiz.GetSearchDetailWithQrcode(Searchdetails);
        //                    // List<vw_GetPhotoList> _objall = objdbLayer.GetSearchedPhoto(RobotImageLoader.FromTime, RobotImageLoader.ToTime, UserId, LocationId, SearchedStoreId);
        //                    //List<ModratePhotoInfo> moderatePhotosList = new List<ModratePhotoInfo>();
        //                    //PhotoBusiness phBiz = new PhotoBusiness();
        //                    //moderatePhotosList = phBiz.GetModeratePhotos();
        //                    if (_objall.Count > 0)
        //                    {

        //                        foreach (var photoItem in _objall)
        //                        {
        //                            LstMyItems _objnew = new LstMyItems();
        //                            _objnew.Name = photoItem.Name;
        //                            _objnew.PhotoId = photoItem.PhotoId;
        //                            _objnew.FileName = photoItem.FileName;
        //                            var moderateItem = moderatePhotosList.AsEnumerable().Where(x => x.DG_Mod_Photo_ID == photoItem.PhotoId);

        //                            if (moderateItem != null && moderateItem.Count() > 0)
        //                            {
        //                                _objnew.FilePath = LoginUser.DigiFolderPath + "/Locked.png";
        //                                _objnew.IsLocked = Visibility.Collapsed;
        //                                _objnew.IsPassKeyVisible = Visibility.Visible;
        //                            }
        //                            else
        //                            {
        //                                _objnew.FilePath = System.IO.Path.Combine(LoginUser.DigiFolderThumbnailPath, photoItem.CreatedOn.ToString("yyyyMMdd"), photoItem.FileName);
        //                                _objnew.BmpImageGroup = new BitmapImage(new Uri(@"/images/view-group.png", UriKind.Relative));
        //                                _objnew.IsLocked = Visibility.Visible;
        //                                _objnew.IsPassKeyVisible = Visibility.Collapsed;
        //                            }
        //                            if (RobotImageLoader.GroupImages != null)
        //                            {
        //                                var Groupeditem = RobotImageLoader.GroupImages.Where(t => t.PhotoId == photoItem.PhotoId).FirstOrDefault();
        //                                if (Groupeditem != null)
        //                                {
        //                                    _objnew.BmpImageGroup = new BitmapImage(new Uri(@"/images/view-accept.png", UriKind.Relative));
        //                                }
        //                                else
        //                                {
        //                                    _objnew.BmpImageGroup = new BitmapImage(new Uri(@"/images/view-group.png", UriKind.Relative));
        //                                }
        //                            }
        //                            else
        //                            {
        //                                _objnew.BmpImageGroup = new BitmapImage(new Uri(@"/images/view-group.png", UriKind.Relative));
        //                            }
        //                            if (RobotImageLoader.PrintImages != null)
        //                            {
        //                                var printitem = RobotImageLoader.PrintImages.Where(t => t.PhotoId == photoItem.PhotoId).FirstOrDefault();
        //                                if (printitem != null)
        //                                {
        //                                    _objnew.PrintGroup = new BitmapImage(new Uri(@"/images/print-accept.png", UriKind.Relative));
        //                                }
        //                                else
        //                                {
        //                                    _objnew.PrintGroup = new BitmapImage(new Uri(@"/images/print-group.png", UriKind.Relative));
        //                                }
        //                            }
        //                            else
        //                            {
        //                                _objnew.PrintGroup = new BitmapImage(new Uri(@"/images/print-group.png", UriKind.Relative));
        //                            }
        //                            if (RobotImageLoader.Is9ImgViewActive)
        //                            {
        //                                _objnew.GridMainHeight = 190;
        //                                _objnew.GridMainWidth = 226;
        //                                _objnew.GridMainRowHeight1 = 140;
        //                                _objnew.GridMainRowHeight2 = 50;
        //                            }
        //                            else
        //                            {
        //                                _objnew.GridMainHeight = 140;
        //                                _objnew.GridMainWidth = 170;
        //                                _objnew.GridMainRowHeight1 = 90;
        //                                _objnew.GridMainRowHeight2 = 60;
        //                            }
        //                            temprobotImages.Add(_objnew);
        //                        }
        //                    }
        //                    else
        //                        temprobotImages = new List<LstMyItems>();
        //                    # endregion
        //#if DEBUG
        //                    if (watch != null)
        //                        FrameworkHelper.CommonUtility.WatchStop("Search TimeWithQrcode", watch);
        //#endif
        //                }
        //                else if (RobotImageLoader.SearchCriteria == "Time")
        //                {
        //#if DEBUG
        //                    var watch = FrameworkHelper.CommonUtility.Watch();
        //                    watch.Start();
        //#endif
        //                    #region Time Range Search
        //                    GroupId = "";

        //                    IsZeroSearchNeeded = false;
        //                    StartIndex = 0;
        //                    StopIndex = 0;
        //                    if (String.IsNullOrEmpty(SearchedStoreId))
        //                    {
        //                        SearchedStoreId = LoginUser.DefaultSubstores;
        //                    }

        //                    List<PhotoInfo> _objall = phBiz.GetSearchedPhoto(RobotImageLoader.FromTime, RobotImageLoader.ToTime, UserId, LocationId, SearchedStoreId);
        //                    //List<ModratePhotoInfo> moderatePhotosList = new List<ModratePhotoInfo>();
        //                    //moderatePhotosList = phBiz.GetModeratePhotos();

        //                    foreach (var photoItem in _objall)
        //                    {
        //                        LstMyItems _objnew = new LstMyItems();
        //                        _objnew.Name = photoItem.DG_Photos_RFID;
        //                        _objnew.PhotoId = photoItem.DG_Photos_pkey;
        //                        _objnew.FileName = photoItem.DG_Photos_FileName;

        //                        var moderateItem = moderatePhotosList.AsEnumerable().Where(x => x.DG_Mod_Photo_ID == photoItem.DG_Photos_pkey);
        //                        if (moderateItem != null && moderateItem.Count() > 0)
        //                        {
        //                            _objnew.FilePath = LoginUser.DigiFolderPath + "/Locked.png";
        //                            _objnew.IsLocked = Visibility.Collapsed;
        //                            _objnew.IsPassKeyVisible = Visibility.Visible;
        //                        }
        //                        else
        //                        {
        //                            _objnew.FilePath = System.IO.Path.Combine(LoginUser.DigiFolderThumbnailPath, photoItem.DG_Photos_CreatedOn.ToString("yyyyMMdd"), photoItem.DG_Photos_FileName);
        //                            _objnew.BmpImageGroup = new BitmapImage(new Uri(@"/images/view-group.png", UriKind.Relative));
        //                            _objnew.IsLocked = Visibility.Visible;
        //                            _objnew.IsPassKeyVisible = Visibility.Collapsed;
        //                        }
        //                        if (RobotImageLoader.GroupImages != null)
        //                        {
        //                            var Groupeditem = RobotImageLoader.GroupImages.Where(t => t.PhotoId == photoItem.DG_Photos_pkey).FirstOrDefault();
        //                            if (Groupeditem != null)
        //                            {
        //                                _objnew.BmpImageGroup = new BitmapImage(new Uri(@"/images/view-accept.png", UriKind.Relative));
        //                            }
        //                            else
        //                            {
        //                                _objnew.BmpImageGroup = new BitmapImage(new Uri(@"/images/view-group.png", UriKind.Relative));
        //                            }
        //                        }
        //                        else
        //                        {
        //                            _objnew.BmpImageGroup = new BitmapImage(new Uri(@"/images/view-group.png", UriKind.Relative));
        //                        }
        //                        if (RobotImageLoader.PrintImages != null)
        //                        {
        //                            var printitem = RobotImageLoader.PrintImages.Where(t => t.PhotoId == photoItem.DG_Photos_pkey).FirstOrDefault();
        //                            if (printitem != null)
        //                            {
        //                                _objnew.PrintGroup = new BitmapImage(new Uri(@"/images/print-accept.png", UriKind.Relative));
        //                            }
        //                            else
        //                            {
        //                                _objnew.PrintGroup = new BitmapImage(new Uri(@"/images/print-group.png", UriKind.Relative));
        //                            }
        //                        }
        //                        else
        //                        {
        //                            _objnew.PrintGroup = new BitmapImage(new Uri(@"/images/print-group.png", UriKind.Relative));
        //                        }
        //                        if (RobotImageLoader.Is9ImgViewActive)
        //                        {
        //                            _objnew.GridMainHeight = 190;
        //                            _objnew.GridMainWidth = 226;
        //                            _objnew.GridMainRowHeight1 = 140;
        //                            _objnew.GridMainRowHeight2 = 50;
        //                        }
        //                        else
        //                        {
        //                            _objnew.GridMainHeight = 140;
        //                            _objnew.GridMainWidth = 170;
        //                            _objnew.GridMainRowHeight1 = 90;
        //                            _objnew.GridMainRowHeight2 = 60;
        //                        }
        //                        temprobotImages.Add(_objnew);
        //                    }
        //                    #endregion
        //#if DEBUG
        //                    if (watch != null)
        //                        FrameworkHelper.CommonUtility.WatchStop("Search Time", watch);
        //#endif
        //                }
        //                else if (RobotImageLoader.SearchCriteria == "Group")
        //                {
        //#if DEBUG
        //                    var watch = FrameworkHelper.CommonUtility.Watch();
        //                    watch.Start();
        //#endif
        //                    #region Group Search
        //                    IsZeroSearchNeeded = false;
        //                    StartIndex = 0;
        //                    StopIndex = 0;
        //                    RFID = "";

        //                    List<PhotoInfo> _objGrpList = phBiz.GetSavedGroupImages(GroupId);


        //                    foreach (var item in _objGrpList)
        //                    {
        //                        LstMyItems _objnew = new LstMyItems();
        //                        _objnew.Name = item.DG_Photos_RFID;
        //                        _objnew.FileName = item.DG_Photos_FileName;
        //                        _objnew.PhotoId = item.DG_Photos_pkey;
        //                        var moderateItem = moderatePhotosList.AsEnumerable().Where(x => x.DG_Mod_Photo_ID == item.DG_Photos_pkey);
        //                        if (moderateItem != null && moderateItem.Count() > 0)
        //                        {
        //                            _objnew.FilePath = LoginUser.DigiFolderPath + "/Locked.png";
        //                            _objnew.IsLocked = Visibility.Collapsed;
        //                            _objnew.IsPassKeyVisible = Visibility.Visible;
        //                        }
        //                        else
        //                        {
        //                            _objnew.FilePath = System.IO.Path.Combine(LoginUser.DigiFolderThumbnailPath, item.DG_Photos_CreatedOn.ToString("yyyyMMdd"), item.DG_Photos_FileName);
        //                            if (string.IsNullOrEmpty(item.DG_Photos_Layering))
        //                            {
        //                                if (!string.IsNullOrEmpty(LoginUser.DefaultBorderPath))
        //                                    _objnew.FrameBrdr = LoginUser.DefaultBorderPath;
        //                            }

        //                            _objnew.IsLocked = Visibility.Visible;
        //                            _objnew.IsPassKeyVisible = Visibility.Collapsed;
        //                        }
        //                        _objnew.BmpImageGroup = new BitmapImage(new Uri(@"/images/view-accept.png", UriKind.Relative));

        //                        if (RobotImageLoader.PrintImages != null)
        //                        {
        //                            var printitem = RobotImageLoader.PrintImages.Where(t => t.PhotoId == item.DG_Photos_pkey).FirstOrDefault();
        //                            if (printitem != null)
        //                            {
        //                                _objnew.PrintGroup = new BitmapImage(new Uri(@"/images/print-accept.png", UriKind.Relative));
        //                            }
        //                            else
        //                            {
        //                                _objnew.PrintGroup = new BitmapImage(new Uri(@"/images/print-group.png", UriKind.Relative));
        //                            }
        //                        }
        //                        else
        //                        {
        //                            _objnew.PrintGroup = new BitmapImage(new Uri(@"/images/print-group.png", UriKind.Relative));
        //                        }
        //                        if (RobotImageLoader.Is9ImgViewActive)
        //                        {
        //                            _objnew.GridMainHeight = 190;
        //                            _objnew.GridMainWidth = 226;
        //                            _objnew.GridMainRowHeight1 = 140;
        //                            _objnew.GridMainRowHeight2 = 50;
        //                        }
        //                        else
        //                        {
        //                            _objnew.GridMainHeight = 140;
        //                            _objnew.GridMainWidth = 170;
        //                            _objnew.GridMainRowHeight1 = 90;
        //                            _objnew.GridMainRowHeight2 = 60;
        //                        }
        //                        temprobotImages.Add(_objnew);
        //                    }
        //                    #endregion
        //#if DEBUG
        //                    if (watch != null)
        //                        FrameworkHelper.CommonUtility.WatchStop("Search Group", watch);
        //#endif
        //                }
        //                else if (RobotImageLoader.SearchCriteria == "QRCODE")
        //                {
        //#if DEBUG
        //                    var watch = FrameworkHelper.CommonUtility.Watch();
        //                    watch.Start();
        //#endif
        //                    #region QRCODE Search
        //                    //RobotImageLoader.GroupImages = new List<LstMyItems>();
        //                    //Code = "";
        //                    if (RobotImageLoader.robotImages == null)
        //                    {
        //                        RobotImageLoader.robotImages = new List<LstMyItems>();
        //                    }
        //                    if (RobotImageLoader.GroupImages == null)
        //                    {
        //                        RobotImageLoader.GroupImages = new List<LstMyItems>();
        //                    }

        //                    if (RobotImageLoader.PrintImages == null)
        //                    {
        //                        RobotImageLoader.PrintImages = new List<LstMyItems>();
        //                    }

        //                    List<PhotoInfo> _objList = phBiz.GetImagesBYQRCode(Code, IsAnonymousQrCodeEnabled);
        //                    foreach (var item in _objList)
        //                    {
        //                        LstMyItems _objnew = new LstMyItems();
        //                        var moderateItem = moderatePhotosList.AsEnumerable().Where(x => x.DG_Mod_Photo_ID == item.DG_Photos_pkey);
        //                        if (moderateItem != null && moderateItem.Count() > 0)
        //                        {
        //                            _objnew.PhotoId = item.DG_Photos_pkey;
        //                            _objnew.Name = item.DG_Photos_RFID;
        //                            _objnew.FileName = item.DG_Photos_FileName;
        //                            _objnew.BmpImageGroup = null;

        //                            _objnew.FilePath = LoginUser.DigiFolderPath + "/Locked.png";
        //                            _objnew.IsLocked = Visibility.Collapsed;
        //                            _objnew.IsPassKeyVisible = Visibility.Visible;
        //                        }
        //                        else
        //                        {
        //                            _objnew.PhotoId = item.DG_Photos_pkey;
        //                            _objnew.Name = item.DG_Photos_RFID;
        //                            _objnew.FileName = item.DG_Photos_FileName;
        //                            _objnew.FilePath = System.IO.Path.Combine(LoginUser.DigiFolderThumbnailPath, item.DG_Photos_CreatedOn.ToString("yyyyMMdd"), item.DG_Photos_FileName);
        //                            if (string.IsNullOrEmpty(item.DG_Photos_Layering))
        //                            {
        //                                if (!string.IsNullOrEmpty(LoginUser.DefaultBorderPath))
        //                                    _objnew.FrameBrdr = LoginUser.DefaultBorderPath;
        //                            }

        //                            _objnew.IsLocked = Visibility.Visible;
        //                            _objnew.IsPassKeyVisible = Visibility.Collapsed;
        //                        }
        //                        if (RobotImageLoader.GroupImages != null)
        //                        {
        //                            var Groupeditem = RobotImageLoader.GroupImages.Where(t => t.PhotoId == item.DG_Photos_pkey).FirstOrDefault();
        //                            if (Groupeditem != null)
        //                                _objnew.BmpImageGroup = new BitmapImage(new Uri(@"/images/view-accept.png", UriKind.Relative));
        //                            else
        //                                _objnew.BmpImageGroup = new BitmapImage(new Uri(@"/images/view-group.png", UriKind.Relative));
        //                        }
        //                        else
        //                        {
        //                            _objnew.BmpImageGroup = new BitmapImage(new Uri(@"/images/view-group.png", UriKind.Relative));
        //                        }
        //                        if (RobotImageLoader.PrintImages != null)
        //                        {
        //                            var printitem = RobotImageLoader.PrintImages.Where(t => t.PhotoId == item.DG_Photos_pkey).FirstOrDefault();
        //                            if (printitem != null)
        //                                _objnew.PrintGroup = new BitmapImage(new Uri(@"/images/print-accept.png", UriKind.Relative));
        //                            else
        //                                _objnew.PrintGroup = new BitmapImage(new Uri(@"/images/print-group.png", UriKind.Relative));
        //                        }
        //                        else
        //                        {
        //                            _objnew.PrintGroup = new BitmapImage(new Uri(@"/images/print-group.png", UriKind.Relative));
        //                        }


        //                        var rbtitem = RobotImageLoader.robotImages.Where(t => t.PhotoId == item.DG_Photos_pkey).FirstOrDefault();
        //                        if (rbtitem == null)
        //                        {
        //                            //Verify is exists in group images
        //                            var grpItem1 = RobotImageLoader.GroupImages.Where(gp1 => gp1.PhotoId == item.DG_Photos_pkey).FirstOrDefault();
        //                            if (grpItem1 != null)
        //                            {
        //                                _objnew.BmpImageGroup = new BitmapImage(new Uri(@"/images/view-accept.png", UriKind.Relative));
        //                            }
        //                            else
        //                            {
        //                                _objnew.BmpImageGroup = new BitmapImage(new Uri(@"/images/view-group.png", UriKind.Relative));
        //                            }
        //                            //Verify is exists in print images
        //                            var prtItem1 = RobotImageLoader.PrintImages.Where(pr1 => pr1.PhotoId == item.DG_Photos_pkey).FirstOrDefault();
        //                            if (prtItem1 != null)
        //                            {
        //                                _objnew.PrintGroup = new BitmapImage(new Uri(@"/images/print-accept.png", UriKind.Relative));
        //                            }
        //                            else
        //                            {
        //                                _objnew.PrintGroup = new BitmapImage(new Uri(@"/images/print-group.png", UriKind.Relative));
        //                            }
        //                            RobotImageLoader.robotImages.Add(_objnew);
        //                        }

        //                        if (RobotImageLoader.Is9ImgViewActive)
        //                        {
        //                            _objnew.GridMainHeight = 190;
        //                            _objnew.GridMainWidth = 226;
        //                            _objnew.GridMainRowHeight1 = 140;
        //                            _objnew.GridMainRowHeight2 = 50;
        //                        }
        //                        else
        //                        {
        //                            _objnew.GridMainHeight = 140;
        //                            _objnew.GridMainWidth = 170;
        //                            _objnew.GridMainRowHeight1 = 90;
        //                            _objnew.GridMainRowHeight2 = 60;
        //                        }
        //                        temprobotImages.Add(_objnew);
        //                    }
        //                    #endregion
        //#if DEBUG
        //                    if (watch != null)
        //                        FrameworkHelper.CommonUtility.WatchStop("Search QRCODE", watch);
        //#endif
        //                }
        //                else if (RobotImageLoader.SearchCriteria == "QRCODEGROUP")
        //                {
        //#if DEBUG
        //                    var watch = FrameworkHelper.CommonUtility.Watch();
        //                    watch.Start();
        //#endif
        //                    #region Group Search
        //                    IsZeroSearchNeeded = false;
        //                    StartIndex = 0;
        //                    StopIndex = 0;
        //                    RFID = "";
        //                    if (RobotImageLoader.GroupImages == null)
        //                        RobotImageLoader.GroupImages = new List<LstMyItems>();
        //                    //moderatePhotosList = objdbLayer.GetModeratePhotos();

        //                    List<PhotoInfo> _objGrpList = phBiz.GetImagesBYQRCode(Code, IsAnonymousQrCodeEnabled);
        //                    foreach (var item in _objGrpList)
        //                    {
        //                        LstMyItems _objnew = new LstMyItems();
        //                        _objnew.Name = item.DG_Photos_RFID;
        //                        _objnew.FileName = item.DG_Photos_FileName;
        //                        _objnew.PhotoId = item.DG_Photos_pkey;
        //                        var moderateItem = moderatePhotosList.AsEnumerable().Where(x => x.DG_Mod_Photo_ID == item.DG_Photos_pkey);
        //                        if (moderateItem != null && moderateItem.Count() > 0)
        //                        {
        //                            _objnew.FilePath = LoginUser.DigiFolderPath + "/Locked.png";
        //                            _objnew.IsLocked = Visibility.Collapsed;
        //                            _objnew.IsPassKeyVisible = Visibility.Visible;
        //                        }
        //                        else
        //                        {
        //                            _objnew.FilePath = System.IO.Path.Combine(LoginUser.DigiFolderThumbnailPath, item.DG_Photos_CreatedOn.ToString("yyyyMMdd"), item.DG_Photos_FileName);
        //                            if (string.IsNullOrEmpty(item.DG_Photos_Layering))
        //                            {
        //                                if (!string.IsNullOrEmpty(LoginUser.DefaultBorderPath))
        //                                    _objnew.FrameBrdr = LoginUser.DefaultBorderPath;
        //                            }

        //                            _objnew.IsLocked = Visibility.Visible;
        //                            _objnew.IsPassKeyVisible = Visibility.Collapsed;
        //                        }

        //                        var Groupeditem = RobotImageLoader.GroupImages.Where(t => t.PhotoId == item.DG_Photos_pkey).FirstOrDefault();
        //                        if (Groupeditem == null)
        //                        {
        //                            _objnew.BmpImageGroup = new BitmapImage(new Uri(@"/images/view-accept.png", UriKind.Relative));
        //                            RobotImageLoader.GroupImages.Add(_objnew);
        //                        }
        //                        _objnew.BmpImageGroup = new BitmapImage(new Uri(@"/images/view-accept.png", UriKind.Relative));

        //                        if (RobotImageLoader.PrintImages != null)
        //                        {
        //                            var printitem = RobotImageLoader.PrintImages.Where(t => t.PhotoId == item.DG_Photos_pkey).FirstOrDefault();
        //                            if (printitem != null)
        //                            {
        //                                _objnew.PrintGroup = new BitmapImage(new Uri(@"/images/print-accept.png", UriKind.Relative));
        //                            }
        //                            else
        //                            {
        //                                _objnew.PrintGroup = new BitmapImage(new Uri(@"/images/print-group.png", UriKind.Relative));
        //                            }
        //                        }
        //                        else
        //                        {
        //                            _objnew.PrintGroup = new BitmapImage(new Uri(@"/images/print-group.png", UriKind.Relative));
        //                        }
        //                        if (RobotImageLoader.Is9ImgViewActive)
        //                        {
        //                            _objnew.GridMainHeight = 190;
        //                            _objnew.GridMainWidth = 226;
        //                            _objnew.GridMainRowHeight1 = 140;
        //                            _objnew.GridMainRowHeight2 = 50;
        //                        }
        //                        else
        //                        {
        //                            _objnew.GridMainHeight = 140;
        //                            _objnew.GridMainWidth = 170;
        //                            _objnew.GridMainRowHeight1 = 90;
        //                            _objnew.GridMainRowHeight2 = 60;
        //                        }
        //                        temprobotImages.Add(_objnew);
        //                    }
        //                    #endregion
        //#if DEBUG
        //                    if (watch != null)
        //                        FrameworkHelper.CommonUtility.WatchStop("Search QRCODE GROUP", watch);
        //#endif
        //                }
        //                else
        //                {

        //                    if (RFID == "0")
        //                    {
        //#if DEBUG
        //                        var watch = FrameworkHelper.CommonUtility.Watch();
        //                        watch.Start();
        //#endif
        //                        GroupId = "";
        //                        #region For Latest
        //                        int startIndex = 0;
        //                        if (currentCount == 0 && IsZeroSearchNeeded)
        //                            startIndex = -1;
        //                        else
        //                        {
        //                            if (_objnewincrement != null && _objnewincrement.Count > 0 && !IsNextPage)
        //                                startIndex = _objnewincrement[_objnewincrement.Count - 1].PhotoId;
        //                            else if (_objnewincrement != null && _objnewincrement.Count > 0 && IsNextPage)
        //                                startIndex = _objnewincrement[0].PhotoId;
        //                            else
        //                                startIndex = -1;

        //                            if (!IsZeroSearchNeeded && currentCount == 0 && StartIndex != 0)
        //                            {
        //                                if (!IsNextPage)
        //                                    startIndex = StartIndex;
        //                                else
        //                                    startIndex = StopIndex;
        //                            }

        //                            if (IsPreview9or16active)
        //                            {
        //                                startIndex = StartIndex + 1;
        //                                IsPreview9or16active = false;
        //                            }
        //                        }

        //                        IsZeroSearchNeeded = false;
        //                        StartIndex = 0;
        //                        StopIndex = 0;
        //                        List<PhotoInfo> lstAllLatest = new List<PhotoInfo>();
        //                        if (_objnewincrement == null)
        //                            _objnewincrement = new List<LstMyItems>();
        //                        _objnewincrement.Clear();

        //                        if (IsNextPage)
        //                        {
        //                            //lstAllLatest = objdbLayer.GetAllPhotosByPage(LoginUser.DefaultSubstores, startIndex, thumbSet, IsNextPage, out MaxPhotoId).OrderByDescending(x => x.DG_Photos_pkey).ToList();
        //                            lstAllLatest = phBiz.GetAllPhotosByPage(ssInfo, startIndex, thumbSet, IsNextPage, out MaxPhotoId, out IsMoreNextImages, out MinPhotoId).OrderByDescending(x => x.DG_Photos_pkey).ToList();
        //                            IsMorePrevImages = true;
        //                        }
        //                        else
        //                        {
        //                            //lstAllLatest = objdbLayer.GetAllPhotosByPage(LoginUser.DefaultSubstores, startIndex, thumbSet, IsNextPage, out MaxPhotoId).ToList();
        //                            lstAllLatest = phBiz.GetAllPhotosByPage(ssInfo, startIndex, thumbSet, IsNextPage, out MaxPhotoId, out IsMorePrevImages, out MinPhotoId).ToList();
        //                            IsMoreNextImages = startIndex == -1 || MaxPhotoId + 1 == startIndex ? false : true;
        //                        }

        //                        IsNextPage = false;
        //                        totalCount = lstAllLatest.Count;
        //                        startLoad = 0;

        //                        for (int j = 0; j < totalCount; j++)
        //                        {
        //                            var item = lstAllLatest[j];
        //                            LstMyItems _objnew = new LstMyItems();
        //                            var moderateItem = moderatePhotosList.AsEnumerable().Where(x => x.DG_Mod_Photo_ID == item.DG_Photos_pkey);
        //                            _objnew.Name = item.DG_Photos_RFID;
        //                            _objnew.FileName = item.DG_Photos_FileName;
        //                            _objnew.ListPosition = currentCount;
        //                            _objnew.PhotoId = item.DG_Photos_pkey;
        //                            if (moderateItem != null && moderateItem.Count() > 0)
        //                            {
        //                                _objnew.FilePath = LoginUser.DigiFolderPath + "/Locked.png";
        //                                _objnew.IsLocked = Visibility.Collapsed;
        //                                _objnew.IsPassKeyVisible = Visibility.Visible;
        //                            }
        //                            else
        //                            {
        //                                _objnew.FilePath = System.IO.Path.Combine(LoginUser.DigiFolderThumbnailPath, item.DG_Photos_CreatedOn.ToString("yyyyMMdd"), item.DG_Photos_FileName);
        //                                if (string.IsNullOrEmpty(item.DG_Photos_Layering))
        //                                {
        //                                    if (!string.IsNullOrEmpty(LoginUser.DefaultBorderPath))
        //                                    {
        //                                        _objnew.FrameBrdr = LoginUser.DefaultBorderPath;
        //                                    }
        //                                }
        //                                _objnew.IsLocked = Visibility.Visible;
        //                                _objnew.IsPassKeyVisible = Visibility.Collapsed;
        //                            }

        //                            if (RobotImageLoader.GroupImages != null)
        //                            {
        //                                var Groupeditem = RobotImageLoader.GroupImages.Where(t => t.PhotoId == item.DG_Photos_pkey).FirstOrDefault();
        //                                if (Groupeditem != null)
        //                                    _objnew.BmpImageGroup = new BitmapImage(new Uri(@"/images/view-accept.png", UriKind.Relative));
        //                                else
        //                                    _objnew.BmpImageGroup = new BitmapImage(new Uri(@"/images/view-group.png", UriKind.Relative));
        //                            }
        //                            else
        //                            {
        //                                _objnew.BmpImageGroup = new BitmapImage(new Uri(@"/images/view-group.png", UriKind.Relative));
        //                            }
        //                            if (RobotImageLoader.PrintImages != null)
        //                            {
        //                                var printitem = RobotImageLoader.PrintImages.Where(t => t.PhotoId == item.DG_Photos_pkey).FirstOrDefault();
        //                                if (printitem != null)
        //                                    _objnew.PrintGroup = new BitmapImage(new Uri(@"/images/print-accept.png", UriKind.Relative));
        //                                else
        //                                    _objnew.PrintGroup = new BitmapImage(new Uri(@"/images/print-group.png", UriKind.Relative));
        //                            }
        //                            else
        //                            {
        //                                _objnew.PrintGroup = new BitmapImage(new Uri(@"/images/print-group.png", UriKind.Relative));
        //                            }

        //                            if (!IsPreviewModeActive && Is9ImgViewActive)
        //                            {
        //                                _objnew.GridMainHeight = 190;
        //                                _objnew.GridMainWidth = 226;
        //                                _objnew.GridMainRowHeight1 = 140;
        //                                _objnew.GridMainRowHeight2 = 50;
        //                            }
        //                            else if (!IsPreviewModeActive && Is16ImgViewActive)
        //                            {
        //                                _objnew.GridMainHeight = 140;
        //                                _objnew.GridMainWidth = 170;
        //                                _objnew.GridMainRowHeight1 = 90;
        //                                _objnew.GridMainRowHeight2 = 60;
        //                            }
        //                            else
        //                            {
        //                                _objnew.GridMainHeight = 140;
        //                                _objnew.GridMainWidth = 170;
        //                                _objnew.GridMainRowHeight1 = 90;
        //                                _objnew.GridMainRowHeight2 = 60;
        //                            }
        //                            temprobotImages.Add(_objnew);
        //                            if (_objnewincrement == null)
        //                            {
        //                                _objnewincrement = new List<LstMyItems>();
        //                            }
        //                            _objnewincrement.Add(_objnew);
        //                            startLoad++;
        //                            currentCount++;
        //                        }
        //                        #endregion
        //#if DEBUG
        //                        if (watch != null)
        //                            FrameworkHelper.CommonUtility.WatchStop("Search 0", watch);
        //#endif
        //                    }

        //                    else
        //                    {
        //#if DEBUG
        //                        var watch = FrameworkHelper.CommonUtility.Watch();
        //                        watch.Start();
        //#endif
        //                        GroupId = "";
        //                        #region RFID search
        //                        IsZeroSearchNeeded = false;
        //                        StartIndex = 0;
        //                        StopIndex = 0;
        //                        int intNoOfPhotoIdSearch = LoginUser.DGNoOfPhotoIdSearch;
        //                        List<PhotoInfo> lstAllLatest = new List<PhotoInfo>();
        //                        lstAllLatest = phBiz.GetAllPhotosforSearch(LoginUser.DefaultSubstores, Convert.ToInt64(RobotImageLoader.RFID), intNoOfPhotoIdSearch, LoginUser.IsPhotographerSerailSearchActive).OrderByDescending(t => t.DG_Photos_pkey).ToList();
        //                        var SearchedImage = lstAllLatest.Where(T => T.DG_Photos_RFID == RobotImageLoader.RFID).ToList();
        //                        if (SearchedImage.Count != 0)
        //                        {
        //                            int imageid = SearchedImage.First().DG_Photos_pkey;
        //                            var preImages = lstAllLatest.Where(t => t.DG_Photos_pkey < imageid).OrderByDescending(t => t.DG_Photos_pkey).ToList();
        //                            if (preImages.Count > intNoOfPhotoIdSearch)
        //                            {
        //                                preImages = preImages.Take(intNoOfPhotoIdSearch).ToList();
        //                            }
        //                            preImages = preImages.OrderBy(t => t.DG_Photos_pkey).ToList();
        //                            preImages = preImages.Union(SearchedImage).ToList();
        //                            var NextImages = lstAllLatest.Where(t => t.DG_Photos_pkey > imageid).OrderBy(t => t.DG_Photos_pkey).ToList();
        //                            if (NextImages.Count > intNoOfPhotoIdSearch)
        //                            {
        //                                NextImages = NextImages.Take(intNoOfPhotoIdSearch).ToList();
        //                            }
        //                            preImages = preImages.Union(NextImages).ToList();
        //                            //LstUnlocknames.Clear();
        //                            //foreach (var item in moderatePhotosList)
        //                            //{
        //                            //    LstUnlocknames.Add(item.DG_Mod_Photo_ID.ToString());
        //                            //}
        //                            foreach (var item in preImages)
        //                            {
        //                                LstMyItems _objnew = new LstMyItems();
        //                                _objnew.Name = item.DG_Photos_RFID;
        //                                _objnew.FileName = item.DG_Photos_FileName;
        //                                _objnew.PhotoId = item.DG_Photos_pkey;
        //                                var moderateItem = moderatePhotosList.AsEnumerable().Where(x => x.DG_Mod_Photo_ID == item.DG_Photos_pkey);
        //                                if (moderateItem != null && moderateItem.Count() > 0)
        //                                {
        //                                    _objnew.FilePath = LoginUser.DigiFolderPath + "/Locked.png";
        //                                    _objnew.IsLocked = Visibility.Collapsed;
        //                                    _objnew.IsPassKeyVisible = Visibility.Visible;
        //                                }
        //                                else
        //                                {
        //                                    _objnew.FilePath = System.IO.Path.Combine(LoginUser.DigiFolderThumbnailPath, item.DG_Photos_CreatedOn.ToString("yyyyMMdd"), item.DG_Photos_FileName);
        //                                    _objnew.IsLocked = Visibility.Visible;
        //                                    if (string.IsNullOrEmpty(item.DG_Photos_Layering))
        //                                    {
        //                                        if (!string.IsNullOrEmpty(LoginUser.DefaultBorderPath))
        //                                        {
        //                                            _objnew.FrameBrdr = LoginUser.DefaultBorderPath;
        //                                        }
        //                                    }
        //                                    _objnew.IsPassKeyVisible = Visibility.Collapsed;
        //                                    _objnew.BmpImageGroup = new BitmapImage(new Uri(@"/images/view-group.png", UriKind.Relative));

        //                                }
        //                                if (RobotImageLoader.GroupImages != null)
        //                                {
        //                                    var Groupeditem = RobotImageLoader.GroupImages.Where(t => t.PhotoId == item.DG_Photos_pkey).FirstOrDefault();
        //                                    if (Groupeditem != null)
        //                                    {
        //                                        _objnew.BmpImageGroup = new BitmapImage(new Uri(@"/images/view-accept.png", UriKind.Relative));
        //                                    }
        //                                    else
        //                                    {
        //                                        _objnew.BmpImageGroup = new BitmapImage(new Uri(@"/images/view-group.png", UriKind.Relative));
        //                                    }
        //                                }
        //                                else
        //                                {
        //                                    _objnew.BmpImageGroup = new BitmapImage(new Uri(@"/images/view-group.png", UriKind.Relative));
        //                                }
        //                                if (RobotImageLoader.PrintImages != null)
        //                                {
        //                                    var printitem = RobotImageLoader.PrintImages.Where(t => t.PhotoId == item.DG_Photos_pkey).FirstOrDefault();
        //                                    if (printitem != null)
        //                                    {
        //                                        _objnew.PrintGroup = new BitmapImage(new Uri(@"/images/print-accept.png", UriKind.Relative));
        //                                    }
        //                                    else
        //                                    {
        //                                        _objnew.PrintGroup = new BitmapImage(new Uri(@"/images/print-group.png", UriKind.Relative));
        //                                    }
        //                                }
        //                                else
        //                                {
        //                                    _objnew.PrintGroup = new BitmapImage(new Uri(@"/images/print-group.png", UriKind.Relative));
        //                                }
        //                                if (RobotImageLoader.Is9ImgViewActive)
        //                                {
        //                                    _objnew.GridMainHeight = 190;
        //                                    _objnew.GridMainWidth = 226;
        //                                    _objnew.GridMainRowHeight1 = 140;
        //                                    _objnew.GridMainRowHeight2 = 50;
        //                                }
        //                                else
        //                                {
        //                                    _objnew.GridMainHeight = 140;
        //                                    _objnew.GridMainWidth = 170;
        //                                    _objnew.GridMainRowHeight1 = 90;
        //                                    _objnew.GridMainRowHeight2 = 60;
        //                                }
        //                                temprobotImages.Add(_objnew);
        //                            }
        //                        }
        //                        #endregion
        //#if DEBUG
        //                        if (watch != null)
        //                            FrameworkHelper.CommonUtility.WatchStop("Search RFID", watch);
        //#endif
        //                    }

        //                }
        //                if (RobotImageLoader.SearchCriteria == "Group")
        //                {
        //                    GroupImages = temprobotImages;

        //                }
        //                else if (RobotImageLoader.SearchCriteria == "Time")
        //                {
        //                    robotImages = temprobotImages;
        //                }
        //                else if (RobotImageLoader.SearchCriteria == "TimeWithQrcode")
        //                {
        //                    robotImages = temprobotImages;
        //                }
        //                else if (RobotImageLoader.SearchCriteria == "QRCODE")
        //                {
        //                    robotImages = temprobotImages;
        //                }
        //                else
        //                {
        //                    if ((_objnewincrement.Count == 0) && (temprobotImages.Count != 0))
        //                    {
        //                        robotImages = temprobotImages;
        //                    }
        //                    else
        //                    {
        //                        //_objnewincrement.AddRange(temprobotImages);
        //                        robotImages = _objnewincrement;
        //                    }
        //                }
        //                temprobotImages = null;
        //                return robotImages;
        //            }
        //            catch (Exception ex)
        //            {
        //                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
        //                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
        //                return robotImages;
        //            }
        //            finally
        //            {

        //                Searchdetails = null;
        //                SearchBiz = null;
        //            }
        //        }
        /// <summary>
        /// Gets the configuration data.
        /// </summary>
        public static void GetConfigData()
        {
            string strprint = "";
            string strGroupPath = "";
            try
            {
                try
                {
                    string readFilePath = AppDomain.CurrentDomain.BaseDirectory;
                    //readFilePath = readFilePath.Replace("FrameworkHelper", "DigiPhoto");   //Commented by Suraj

                    if (File.Exists(readFilePath + "\\ss.dat"))
                    {
                        string line;
                        string SubStoreId;
                        using (StreamReader reader = new StreamReader(readFilePath + "\\ss.dat"))
                        {
                            line = reader.ReadLine();
                            SubStoreId = CryptorEngine.Decrypt(line, true);
                            LoginUser.DefaultSubstores = SubStoreId;
                            StoreSubStoreDataBusniess storeObj = new StoreSubStoreDataBusniess();
                            LoginUser.SubstoreName = storeObj.GetSubstoreNameById(Convert.ToInt32(SubStoreId.Split(',')[0]));
                            LoginUser.SubStoreId = Convert.ToInt32(SubStoreId.Split(',')[0]);
                        }
                    }
                    //if (File.Exists(readFilePath + "\\slipPrinter.dat"))
                    //{
                    //    using (StreamReader reader = new StreamReader(readFilePath + "\\slipPrinter.dat"))
                    //    {
                    //        string strprint1 = reader.ReadLine();
                    //        strprint = strprint1.Split(',').ToArray().First().ToString();
                    //        strGroupPath = strprint1.Split(',').ToArray().Last().ToString();
                    //    }
                    //}
                }
                catch (Exception ex)
                {
                    string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                    ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                }
                ConfigBusiness confObj = new ConfigBusiness();

                ConfigurationInfo _objdata = confObj.GetConfigurationData(LoginUser.SubStoreId);
                if (_objdata != null)
                {
                    LoginUser.DigiFolderBackGroundPath = _objdata.DG_BG_Path;
                    LoginUser.DigiFolderFramePath = _objdata.DG_Frame_Path;
                    LoginUser.DigiFolderPath = _objdata.DG_Hot_Folder_Path;
                    LoginUser.ModPassword = _objdata.DG_Mod_Password;
                    LoginUser.DigiFolderGraphicsPath = _objdata.DG_Graphics_Path;
                    LoginUser.DigiImagesCount = Convert.ToInt32(_objdata.DG_NoOfPhotos);
                    LoginUser.IsWatermark = _objdata.DG_Watermark;
                    LoginUser.IsHighResolution = _objdata.DG_HighResolution;
                    LoginUser.IsSemiOrder = _objdata.DG_SemiOrderMain;
                    LoginUser.DigiFolderBigThumbnailPath = _objdata.DG_Hot_Folder_Path + "Thumbnails_Big\\";
                    LoginUser.DigiFolderCropedPath = _objdata.DG_Hot_Folder_Path + "Croped\\";
                    LoginUser.DigiFolderThumbnailPath = _objdata.DG_Hot_Folder_Path + "Thumbnails\\";
                    LoginUser.DigiFolderAudioPath = _objdata.DG_Hot_Folder_Path + "Audio\\";
                    LoginUser.DigiFolderVideoTemplatePath = _objdata.DG_Hot_Folder_Path + "VideoTemplate\\";
                    LoginUser.DigiFolderVideoBackGroundPath = _objdata.DG_Hot_Folder_Path + "VideoBackGround\\";
                    LoginUser.DigiFolderProcessedVideosPath = _objdata.DG_Hot_Folder_Path + "ProcessedVideos\\";
                    LoginUser.DigiFolderVideoOverlayPath = _objdata.DG_Hot_Folder_Path + "VideoOverlay\\";

                    LoginUser.IsDiscountAllowed = _objdata.DG_AllowDiscount;
                    LoginUser.IsDiscountAllowedonTotal = _objdata.DG_EnableDiscountOnTotal;
                    LoginUser.DGNoOfPhotoIdSearch = (int)_objdata.DG_NoOfPhotoIdSearch;

                    LoginUser.PageCountGrid = (int)_objdata.DG_PageCountGrid;
                    LoginUser.DigiReportPath = _objdata.DG_Hot_Folder_Path + "Reports\\";

                    LoginUser.ItemTemplatePath = _objdata.DG_Hot_Folder_Path + "ItemTemplate\\";
                    LoginUser.ItemCalenderPath = _objdata.DG_Hot_Folder_Path + "ItemTemplate\\Calendar\\";

                    
                    //if (!string.IsNullOrEmpty(strprint))
                     if (!string.IsNullOrEmpty(_objdata.DG_ReceiptPrinter))
                    {
                        LoginUser.ReceiptPrinterPath = _objdata.DG_ReceiptPrinter;//strprint
                    }
                    else
                    {
                        LoginUser.ReceiptPrinterPath = "";
                    }

                    if (!string.IsNullOrEmpty(strGroupPath))
                    {
                        LoginUser.GroupValue = strGroupPath;
                    }
                    else
                    {
                        LoginUser.GroupValue = "";
                    }
                    //if (_objdata.DG_SemiOrderMain == true)
                    {
                        SemiOrderBusiness semiOrder = new SemiOrderBusiness();
                        List<SemiOrderSettings> lstSemiSettings = semiOrder.GetLstSemiOrderSettings(null);
                        if (lstSemiSettings != null)
                            LoginUser.ListSemiOrderSettingsSubStoreWise = lstSemiSettings;
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }
        public static Dictionary<string, string> GetReports()
        {
            Dictionary<string, string> lstReportTypeList = new Dictionary<string, string>();
            lstReportTypeList = new Dictionary<string, string>();
            lstReportTypeList.Add("Activity Reports", "0");
            lstReportTypeList.Add("Production Summary Report", "1");
            lstReportTypeList.Add("Operators Performance Report", "2");
            lstReportTypeList.Add("Taking Reports", "3");
            lstReportTypeList.Add("Operation Audit Trail", "4");
            lstReportTypeList.Add("Photographer Performance", "5");
            lstReportTypeList.Add("Site Performance Report", "6");
            lstReportTypeList.Add("Financial Audit Report", "7");
            //lstReportTypeList.Add("Order Detailed Report", "8");
            lstReportTypeList.Add("Printing Report", "9");
            lstReportTypeList.Add("Payment Summary Report", "10");
            lstReportTypeList.Add("IP Print Tracking Report", "11");
            lstReportTypeList.Add("IP Content Tracking Report", "12");
            #region
            //Added by Saroj on 04-12-2019 to get the result for video products performance Report 
            lstReportTypeList.Add("Photographer Video Report", "13");///changed by monica for evoucher 25 march 20202
            #endregion
            #region added by Monika 13-feb-2020
            lstReportTypeList.Add("Evoucher Details", "14");
            
            #endregion
            return lstReportTypeList;
        }
        public static Dictionary<string, string> GetEmailType()
        {
            Dictionary<string, string> lstReportTypeList = new Dictionary<string, string>();
            lstReportTypeList = new Dictionary<string, string>();
            lstReportTypeList.Add("Select", "10");
            lstReportTypeList.Add("Not Sent", "0");
            lstReportTypeList.Add("Sent successfully", "1");
            lstReportTypeList.Add("Error", "2");
            return lstReportTypeList;
        }
        public static bool GetShowAllSubstorePhotos()
        {
            bool shwAllPhotos = false;
            try
            {
                //if (objdbLayer == null)
                //    objdbLayer = new DigiPhotoDataServices();
                ConfigBusiness conFigBiz = new ConfigBusiness();
                List<long> objList = new List<long>();
                objList.Add((long)ConfigParams.ShowAllSubstorePhotos);

                List<iMIXConfigurationInfo> ConfigValuesList = conFigBiz.GetNewConfigValues(LoginUser.SubStoreId).Where(o => objList.Contains(o.IMIXConfigurationMasterId)).ToList();
                if (ConfigValuesList != null || ConfigValuesList.Count > 0)
                {
                    for (int i = 0; i < ConfigValuesList.Count; i++)
                    {
                        switch (ConfigValuesList[i].IMIXConfigurationMasterId)
                        {
                            case (int)ConfigParams.ShowAllSubstorePhotos:
                                shwAllPhotos = ConfigValuesList[i].ConfigurationValue != null ? Convert.ToBoolean(ConfigValuesList[i].ConfigurationValue) : false;
                                break;
                            case (int)ConfigParams.ShowCheckSubstorePhotos:
                                IsSearchPhotoForSubStore = ConfigValuesList[i].ConfigurationValue != null ? Convert.ToBoolean(ConfigValuesList[i].ConfigurationValue) : false;
                                break;
                        }
                    }
                }
                return shwAllPhotos;
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                return false;
            }
        }

        public static void SetSearchSubstorePhotos()
        {
            try
            {
                //if (objdbLayer == null)
                //    objdbLayer = new DigiPhotoDataServices();
                ConfigBusiness conFigBiz = new ConfigBusiness();
                List<long> objList = new List<long>();
                objList.Add((long)ConfigParams.ShowCheckSubstorePhotos);

                List<iMIXConfigurationInfo> ConfigValuesList = conFigBiz.GetNewConfigValues(LoginUser.SubStoreId).Where(o => objList.Contains(o.IMIXConfigurationMasterId)).ToList();
                if (ConfigValuesList != null || ConfigValuesList.Count > 0)
                {
                    for (int i = 0; i < ConfigValuesList.Count; i++)
                    {
                        switch (ConfigValuesList[i].IMIXConfigurationMasterId)
                        {
                            case (int)ConfigParams.ShowCheckSubstorePhotos:
                                IsSearchPhotoForSubStore = ConfigValuesList[i].ConfigurationValue != null ? Convert.ToBoolean(ConfigValuesList[i].ConfigurationValue) : false;
                                break;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }

    }


    public class LstMyItems : INotifyPropertyChanged

    {
        // Added By KCB on 26 JUL 2018 for image selection
        private BitmapImage selectGroup;// = new BitmapImage(new Uri(@"/images/Select-group.png", UriKind.Relative));

        public BitmapImage SelectGroup
        {
            get { return selectGroup; }
            set { selectGroup = value; }
        }
        //end

        [System.Runtime.InteropServices.DllImport("gdi32.dll")]
        public static extern bool DeleteObject(IntPtr hObject);
        public void DestroyBitmap()
        {
            try
            {
                // Added By KCB on 26 JUL 2018 for image selection
                if (selectGroup != null)
                {
                    IntPtr deleteselectObject = MemoryManagement.BitmapImage2Bitmap(selectGroup).GetHbitmap();
                    DeleteObject(deleteselectObject);
                }
                //end
                if (printGroup != null)
                {
                    IntPtr deletePrintObject = MemoryManagement.BitmapImage2Bitmap(printGroup).GetHbitmap();
                    DeleteObject(deletePrintObject);
                }
                //
                if (_bmpImage != null)
                {
                    IntPtr deleteBmpObject = MemoryManagement.BitmapImage2Bitmap(_bmpImage).GetHbitmap();
                    DeleteObject(deleteBmpObject);
                }

                if (_bmpImageGroup != null)
                {
                    IntPtr deleteBmpGroupObject = MemoryManagement.BitmapImage2Bitmap(_bmpImageGroup).GetHbitmap();
                    DeleteObject(deleteBmpGroupObject);
                }
            }
            catch { }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(PropertyChangedEventArgs e)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, e);
            }
        }
        //Added by Adarsh
        /// <summary>
        /// The position of the image in the LstMyItems
        /// </summary>
        private int _listPosition;
        public int ListPosition
        {
            get { return _listPosition; }
            set { _listPosition = value; }
        }

        //Added by Vishal for License Product
        /// <summary>
        /// The primary key for the Borders table
        /// </summary>        
        public int BorderId { get; set; }

        /// <summary>
        /// The _photo unique identifier
        /// </summary>
        private int _photoId;
        private long _displayOrder;
        ///private int _PhotoId;
        /// <summary>
        /// The print group
        /// </summary>
        private BitmapImage printGroup;// = new BitmapImage(new Uri(@"/images/print-group.png", UriKind.Relative));
        /// <summary>
        /// The _name
        /// new 19/7/2017
        //private BitmapImage selectGroup;// = new BitmapImage(new Uri(@"/images/Select-group.png", UriKind.Relative));
        /// </summary>
        private string _name;
        private string _nameRowNo;
        /// <summary>
        /// The _frame BRDR
        /// </summary>
        private string _frameBrdr = null;
        /// <summary>
        /// The _file path
        /// </summary>
        private string _filePath;
        /// <summary>
        /// The _BMP image
        /// </summary>
        private BitmapImage _bmpImage;
        /// <summary>
        /// The _BMP image group
        /// </summary>
        private BitmapImage _bmpImageGroup;
        private BitmapImage _bmpImageUplaod;////made changes by latika for presold service
        private int _semiOrderProfileId;
        /// <summary>
        /// The _is locked
        /// </summary>
        private Visibility _isLocked;
        /// <summary>
        /// The _is pass key visible
        /// </summary>
        private Visibility _isPassKeyVisible;
        /// <summary>
        /// The photoname
        /// </summary>
        private string Photoname;
        /// <summary>
        /// The _counter
        /// </summary>
        private int _counter;
        /// <summary>
        /// The _max count
        /// </summary>
        private int _maxCount;
        /// <summary>
        /// The _file name
        /// </summary>
        private string _fileName;
        /// <summary>
        /// The automatic showno copy
        /// </summary>
        bool toShownoCopy;
        /// <summary>
        /// The media type - Image 1, Video 2, Processed Video 3
        /// </summary>
        private int _mediaType;
        /// <summary>
        /// Play button visibility
        /// </summary>
        private Visibility _playVisible;

        private Visibility _processedVisible;
        /// <summary>
        /// Video file path
        /// </summary>
        private string _vidFilePath;

        public string VidFilePath
        {
            get { return _vidFilePath; }
            set { _vidFilePath = value; }
        }

        private DateTime _createdOn;

        public DateTime CreatedOn
        {
            get { return _createdOn; }
            set { _createdOn = value; }
        }

        public Visibility PlayVisible
        {
            get
            {
                //return _playVisible; 
                //Video and Processed Video.
                if (MediaType == 2 || MediaType == 3)
                {
                    return Visibility.Visible;
                }
                else
                {
                    return Visibility.Collapsed;
                }
            }
            set
            {
                _playVisible = value;
            }
        }

        private bool _isvosDisplpay;
        public bool IsVosDisplay
        {
            get { return _isvosDisplpay; }
            set { _isvosDisplpay = value; }
        }

        public Visibility ProcessedVisible
        {
            get
            {
                //Processed Video.
                if (MediaType == 3)
                {
                    return Visibility.Visible;
                }
                else
                {
                    return Visibility.Collapsed;
                }
            }
            set
            {
                _processedVisible = value;
            }
        }
        /// <summary>
        /// Gets or sets a value indicating whether [automatic showno copy].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [automatic showno copy]; otherwise, <c>false</c>.
        /// </value>
        public bool ToShownoCopy
        {
            get { return toShownoCopy; }
            set { toShownoCopy = value; }
        }

        /// <summary>
        /// Gets the media type - 1 for Image and 2 for Video
        /// </summary>
        public int MediaType
        {
            get { return _mediaType; }
            set { _mediaType = value; }
        }

        /// <summary>
        /// Gets or sets the name of the file.
        /// </summary>
        /// <value>
        /// The name of the file.
        /// </value>
        public string FileName
        {
            get { return _fileName; }
            set { _fileName = value; }
        }
        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }
        public string NameRowNo
        {
            get { return _nameRowNo; }
            set { _nameRowNo = value; }
        }
        //private long _storyBookId;
        //public long StoryBookId
        //{
        //    get { return _storyBookId; }
        //    set { _storyBookId = value; }
        //}
        //private bool _isvosDisplpay;
        //public bool IsVosDisplay
        //{
        //    get { return _isvosDisplpay; }
        //    set { _isvosDisplpay = value; }
        //}

        /// <summary>
        /// Gets or sets the photoname1.
        /// </summary>
        /// <value>
        /// The photoname1.
        /// </value>
        public string Photoname1
        {
            get { return Photoname; }
            set { Photoname = value; }
        }
        /// <summary>
        /// Gets or sets the frame BRDR.
        /// </summary>
        /// <value>
        /// The frame BRDR.
        /// </value>
        public string FrameBrdr
        {
            get { return _frameBrdr; }
            set { _frameBrdr = value; }
        }
        /// <summary>
        /// Gets or sets the print group.
        /// </summary>
        /// <value>
        /// The print group.
        /// </value>
        public BitmapImage PrintGroup
        {
            get { return printGroup; }
            set { printGroup = value; }
        }
        /// <summary>
        /// Gets or sets the counter.
        /// </summary>
        /// <value>
        /// The counter.
        /// </value>
        /// new 19/7/2017
        //public BitmapImage SelectGroup
        //{
        //    get { return selectGroup; }
        //    set { selectGroup = value; }
        //}



        public int Counter
        {
            get { return _counter; }
            set { _counter = value; }
        }
        /// <summary>
        /// Gets or sets the maximum count.
        /// </summary>
        /// <value>
        /// The maximum count.
        /// </value>
        public int MaxCount
        {
            get { return _maxCount; }
            set { _maxCount = value; }
        }

        /// <summary>
        /// Gets or sets the file path.
        /// </summary>
        /// <value>
        /// The file path.
        /// </value>
        public string FilePath
        {
            get { return _filePath; }
            set { _filePath = value; }
        }

        private string _hotFolderPath = string.Empty;
        public string HotFolderPath
        {
            get { return _hotFolderPath; }
            set { _hotFolderPath = value; }
        }

        public string BigDBThumnailPath
        {
            get { return Path.Combine(HotFolderPath, "Thumbnails_Big"); }
        }

        public string ThumnailPath
        {
            get { return Path.Combine(HotFolderPath, "Thumbnails"); }
        }
        /// <summary>
        /// Gets or sets the photo unique identifier.
        /// </summary>
        /// <value>
        /// The photo unique identifier.
        /// </value>
        public int PhotoId
        {
            get { return _photoId; }
            set { _photoId = value; }
        }       

        private long _storyBookId;
        public long StoryBookId
        {
            get { return _storyBookId; }
            set { _storyBookId = value; }
        }
        public long DisplayOrder
        {
            get { return _displayOrder; }
            set { _displayOrder = value; }
        }

        /// <summary>
        /// Gets or sets the BMP image.
        /// </summary>
        /// <value>
        /// The BMP image.
        /// </value>
        public BitmapImage BmpImage
        {
            get { return _bmpImage; }
            set { _bmpImage = value; }
        }
        /// <summary>
        /// Gets or sets the BMP image group.
        /// </summary>
        /// <value>
        /// The BMP image group.
        /// </value>
        public BitmapImage BmpImageGroup
        {
            get { return _bmpImageGroup; }
            set { _bmpImageGroup = value; }
        }
        /////made changes by latika for presold service
        public BitmapImage BmpImageUplaoad
        {
            get { return _bmpImageUplaod; }
            set { _bmpImageUplaod = value; }
        }

        // *** Code Added by Anis for Magic Shot Kitted on 26th Nov 18 *** //
        public int SemiOrderProfileId
        {
            get { return _semiOrderProfileId; }
            set { _semiOrderProfileId = value; }
        }
        //*** End Here

        /// <summary>
        /// Gets or sets the is locked.
        /// </summary>
        /// <value>
        /// The is locked.
        /// </value>
        public Visibility IsLocked
        {
            get { return _isLocked; }
            set { _isLocked = value; }
        }
        /// <summary>
        /// Gets or sets the is pass key visible.
        /// </summary>
        /// <value>
        /// The is pass key visible.
        /// </value>
        public Visibility IsPassKeyVisible
        {
            get { return _isPassKeyVisible; }
            set { _isPassKeyVisible = value; }
        }
        public bool IsChecked { get; set; }

        public long? VideoLength { get; set; }
        private int _GridMainWidth;
        public int GridMainWidth
        {
            get { return this._GridMainWidth; }
            set
            {
                if (this._GridMainWidth != value)
                {
                    this._GridMainWidth = value;
                    this.OnPropertyChanged(new PropertyChangedEventArgs("GridMainWidth"));
                }
            }
        }

        private int _GridMainHeight;
        public int GridMainHeight
        {
            get { return this._GridMainHeight; }
            set
            {
                if (this._GridMainHeight != value)
                {
                    this._GridMainHeight = value;
                    this.OnPropertyChanged(new PropertyChangedEventArgs("GridMainHeight"));
                }
            }
        }

        private int _GridMainRowHeight1;
        public int GridMainRowHeight1
        {
            get { return this._GridMainRowHeight1; }
            set
            {
                if (this._GridMainRowHeight1 != value)
                {
                    this._GridMainRowHeight1 = value;
                    this.OnPropertyChanged(new PropertyChangedEventArgs("GridMainRowHeight1"));
                }
            }
        }

        private int _GridMainRowHeight2;
        public int GridMainRowHeight2
        {
            get { return this._GridMainRowHeight2; }
            set
            {
                if (this._GridMainRowHeight2 != value)
                {
                    this._GridMainRowHeight2 = value;
                    this.OnPropertyChanged(new PropertyChangedEventArgs("GridMainRowHeight2"));
                }
            }
        }

        Int32 _ItemTemplateHeaderId;

        /// <summary>
        /// the item template header id
        /// </summary>
        public Int32 ItemTemplateHeaderId
        {
            get { return _ItemTemplateHeaderId; }
            set
            {
                _ItemTemplateHeaderId = value;
                this.OnPropertyChanged(new PropertyChangedEventArgs("ItemTemplateHeaderId"));
            }
        }

        Int32 _ItemTemplateDetailId;
        /// <summary>
        ///  the item template detail id
        /// </summary>
        public Int32 ItemTemplateDetailId
        {
            get { return _ItemTemplateDetailId; }
            set
            {
                _ItemTemplateDetailId = value;
                this.OnPropertyChanged(new PropertyChangedEventArgs("ItemTemplateDetailId"));
            }
        }

        int _PageNo;
        /// <summary>
        ///  the item template detail id
        /// </summary>
        public int PageNo
        {
            get { return _PageNo; }
            set
            {
                _PageNo = value;
                this.OnPropertyChanged(new PropertyChangedEventArgs("PageNo"));
            }
        }

        public List<PhotoPrintPosition> PhotoPrintPositionList = new List<PhotoPrintPosition>();

        public bool IsItemSelected { get; set; }

        public int updownCount { get; set; }

        public string BigThumbnailPath { get; set; }

        public string OnlineQRCode { get; set; }
        public int? PhotoLocation { get; set; }
    }


    public class PrintOrderPage : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(PropertyChangedEventArgs e)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, e);
            }
        }

        private int _PageNo;
        public int PageNo
        {
            get { return this._PageNo; }
            set
            {
                if (this._PageNo != value)
                {
                    this._PageNo = value;
                    this.OnPropertyChanged(new PropertyChangedEventArgs("PageNo"));
                }
            }
        }
        private string _Name1;
        public string Name1
        {
            get { return this._Name1; }
            set
            {
                if (this._Name1 != value)
                {
                    this._Name1 = value;
                    this.OnPropertyChanged(new PropertyChangedEventArgs("Name1"));
                }
            }
        }

        private string _Name2;
        public string Name2
        {
            get { return this._Name2; }
            set
            {
                if (this._Name2 != value)
                {
                    this._Name2 = value;
                    this.OnPropertyChanged(new PropertyChangedEventArgs("Name2"));
                }
            }
        }

        private string _FilePath1;
        public string FilePath1
        {
            get { return this._FilePath1; }
            set
            {
                if (this._FilePath1 != value)
                {
                    this._FilePath1 = value;
                    this.OnPropertyChanged(new PropertyChangedEventArgs("FilePath1"));
                }
            }
        }

        private string _FilePath2;
        public string FilePath2
        {
            get { return this._FilePath2; }
            set
            {
                if (this._FilePath2 != value)
                {
                    this._FilePath2 = value;
                    this.OnPropertyChanged(new PropertyChangedEventArgs("FilePath2"));
                }
            }
        }

        private int _PhotoPosition1;
        public int PhotoPosition1
        {
            get { return this._PhotoPosition1; }
            set
            {
                if (this._PhotoPosition1 != value)
                {
                    this._PhotoPosition1 = value;
                    this.OnPropertyChanged(new PropertyChangedEventArgs("PhotoPosition1"));
                }
            }
        }

        private int _PhotoPosition2;
        public int PhotoPosition2
        {
            get { return this._PhotoPosition2; }
            set
            {
                if (this._PhotoPosition2 != value)
                {
                    this._PhotoPosition2 = value;
                    this.OnPropertyChanged(new PropertyChangedEventArgs("PhotoPosition2"));
                }
            }
        }

        private int _PhotoId1;
        public int PhotoId1
        {
            get { return this._PhotoId1; }
            set
            {
                if (this._PhotoId1 != value)
                {
                    this._PhotoId1 = value;
                    this.OnPropertyChanged(new PropertyChangedEventArgs("PhotoId1"));
                }
            }
        }

        private int _PhotoId2;
        public int PhotoId2
        {
            get { return this._PhotoId2; }
            set
            {
                if (this._PhotoId2 != value)
                {
                    this._PhotoId2 = value;
                    this.OnPropertyChanged(new PropertyChangedEventArgs("PhotoId2"));
                }
            }
        }

        private ImageSource _ImageSource1;
        public ImageSource ImageSource1
        {
            get { return this._ImageSource1; }
            set
            {
                if (this._ImageSource1 != value)
                {
                    this._ImageSource1 = value;
                    this.OnPropertyChanged(new PropertyChangedEventArgs("ImageSource1"));
                }
            }
        }


        private ImageSource _ImageSource2;
        public ImageSource ImageSource2
        {
            get { return this._ImageSource2; }
            set
            {
                if (this._ImageSource2 != value)
                {
                    this._ImageSource2 = value;
                    this.OnPropertyChanged(new PropertyChangedEventArgs("ImageSource2"));
                }
            }
        }

        private int _RotationAngle1;
        public int RotationAngle1
        {
            get { return this._RotationAngle1; }
            set
            {
                if (this._RotationAngle1 != value)
                {
                    this._RotationAngle1 = value;
                    this.OnPropertyChanged(new PropertyChangedEventArgs("RotationAngle1"));
                }
            }
        }


        private int _RotationAngle2;
        public int RotationAngle2
        {
            get { return this._RotationAngle2; }
            set
            {
                if (this._RotationAngle2 != value)
                {
                    this._RotationAngle2 = value;
                    this.OnPropertyChanged(new PropertyChangedEventArgs("RotationAngle2"));
                }
            }
        }

        Int32 _ItemTemplateHeaderId;

        /// <summary>
        /// the item template header id
        /// </summary>
        public Int32 ItemTemplateHeaderId
        {
            get { return _ItemTemplateHeaderId; }
            set
            {
                _ItemTemplateHeaderId = value;
                this.OnPropertyChanged(new PropertyChangedEventArgs("ItemTemplateHeaderId"));
            }
        }

        Int32 _ItemTemplateDetailId;
        /// <summary>
        ///  the item template detail id
        /// </summary>
        public Int32 ItemTemplateDetailId
        {
            get { return _ItemTemplateDetailId; }
            set
            {
                _ItemTemplateDetailId = value;
                this.OnPropertyChanged(new PropertyChangedEventArgs("ItemTemplateDetailId"));
            }
        }

        //public int PageNo { get; set; }
        //public string Name1 { get; set; }
        //public string FilePath1 { get; set; }
        //public int PhotoId1 { get; set; }
        //public int PhotoPosition1 { get; set; }
        //
        //public string Name2 { get; set; }
        //public string FilePath2 { get; set; }
        //public int PhotoId2 { get; set; }
        //public int PhotoPosition2 { get; set; }

        /*
        public LstMyItems PagePosition1 = new LstMyItems();
        public LstMyItems PagePosition2 = new LstMyItems();
        */
    }


    public class MemoryManagement
    {
        /// <summary>
        /// Sets the size of the process working set.
        /// </summary>
        /// <param name="process">The process.</param>
        /// <param name="minimumWorkingSetSize">Minimum size of the working set.</param>
        /// <param name="maximumWorkingSetSize">Maximum size of the working set.</param>
        /// <returns></returns>
        [DllImportAttribute("kernel32.dll", EntryPoint = "SetProcessWorkingSetSize", ExactSpelling = true, CharSet =
        CharSet.Ansi, SetLastError = true)]
        private static extern int SetProcessWorkingSetSize(IntPtr process, int minimumWorkingSetSize, int
        maximumWorkingSetSize);
        /// <summary>
        /// Flushes the memory.
        /// </summary>
        public static void FlushMemory()
        {
            GC.Collect();
            GC.WaitForPendingFinalizers();
            if (Environment.OSVersion.Platform == PlatformID.Win32NT)
            {
                SetProcessWorkingSetSize(System.Diagnostics.Process.GetCurrentProcess().Handle, -1, -1);
            }
            GC.Collect();
        }
        public static void DisposeImage(BitmapImage image)
        {
            if (image != null)
            {
                try
                {
                    image.UriSource = null;
                }
                catch (Exception)
                {
                }
            }
        }

        [System.Runtime.InteropServices.DllImport("gdi32.dll")]
        public static extern bool DeleteObject(IntPtr hObject);
        public static void DestroyBitmap(BitmapImage bitmapImage)
        {
            try
            {
                IntPtr deletePrintObject = BitmapImage2Bitmap(bitmapImage).GetHbitmap();
                DeleteObject(deletePrintObject);
            }
            catch { }
        }
        public static System.Drawing.Bitmap BitmapImage2Bitmap(BitmapImage bitmapImage)
        {
            using (MemoryStream outStream = new MemoryStream())
            {
                BitmapEncoder enc = new BmpBitmapEncoder();
                enc.Frames.Add(BitmapFrame.Create(bitmapImage));
                enc.Save(outStream);
                System.Drawing.Bitmap bitmap = new System.Drawing.Bitmap(outStream);

                return new System.Drawing.Bitmap(bitmap);
            }
        }
    }


    public static class DispatcherHelper
    {
        /// <summary>
        /// Gets the unique identifier dispatcher.
        /// </summary>
        /// <value>
        /// The unique identifier dispatcher.
        /// </value>
        public static Dispatcher UIDispatcher { get; private set; }
        /// <summary>
        /// Checks the begin invoke configuration unique identifier.
        /// </summary>
        /// <param name="action">The action.</param>
        public static void CheckBeginInvokeOnUI(Action action)
        {
            if (UIDispatcher.CheckAccess()) action(); else UIDispatcher.BeginInvoke(action);
            /// <summary>
            /// Initializes this instance.
            /// </summary>
        }
        public static void Initialize() { if (UIDispatcher != null) return; UIDispatcher = Dispatcher.CurrentDispatcher; }
    }



    #region Class for Encryption/Decryption bin file

    public class CryptorEngine
    {
        /// <summary>
        /// Encrypt a string using dual encryption method. Return a encrypted cipher Text
        /// </summary>
        /// <param name="toEncrypt">string to be encrypted</param>
        /// <param name="useHashing">use hashing? send to for extra secirity</param>
        /// <returns></returns>
        public static string Encrypt(string toEncrypt, bool useHashing)
        {
            byte[] keyArray;
            byte[] toEncryptArray = UTF8Encoding.UTF8.GetBytes(toEncrypt);

            // System.Configuration.AppSettingsReader settingsReader = new AppSettingsReader();
            // Get the key from config file
            // string key = (string)settingsReader.GetValue("SecurityKey", typeof(String));
            string key = "mykey";
            //System.Windows.Forms.MessageBox.Show(key);
            if (useHashing)
            {
                MD5CryptoServiceProvider hashmd5 = new MD5CryptoServiceProvider();
                keyArray = hashmd5.ComputeHash(UTF8Encoding.UTF8.GetBytes(key));
                hashmd5.Clear();
            }
            else
                keyArray = UTF8Encoding.UTF8.GetBytes(key);

            TripleDESCryptoServiceProvider tdes = new TripleDESCryptoServiceProvider();
            tdes.Key = keyArray;
            tdes.Mode = CipherMode.ECB;
            tdes.Padding = PaddingMode.PKCS7;

            ICryptoTransform cTransform = tdes.CreateEncryptor();
            byte[] resultArray = cTransform.TransformFinalBlock(toEncryptArray, 0, toEncryptArray.Length);
            tdes.Clear();
            return Convert.ToBase64String(resultArray, 0, resultArray.Length);
        }
        /// <summary>
        /// DeCrypt a string using dual encryption method. Return a DeCrypted clear string
        /// </summary>
        /// <param name="cipherString">encrypted string</param>
        /// <param name="useHashing">Did you use hashing to encrypt this data? pass true is yes</param>
        /// <returns></returns>
        public static string Decrypt(string cipherString, bool useHashing)
        {
            byte[] keyArray;
            byte[] toEncryptArray = Convert.FromBase64String(cipherString);

            // System.Configuration.AppSettingsReader settingsReader = new AppSettingsReader();
            //Get your key from config file to open the lock!
            //string key = (string)settingsReader.GetValue("SecurityKey", typeof(String));
            string key = "mykey";
            if (useHashing)
            {
                MD5CryptoServiceProvider hashmd5 = new MD5CryptoServiceProvider();
                keyArray = hashmd5.ComputeHash(UTF8Encoding.UTF8.GetBytes(key));
                hashmd5.Clear();
            }
            else
                keyArray = UTF8Encoding.UTF8.GetBytes(key);

            TripleDESCryptoServiceProvider tdes = new TripleDESCryptoServiceProvider();
            tdes.Key = keyArray;
            tdes.Mode = CipherMode.ECB;
            tdes.Padding = PaddingMode.PKCS7;

            ICryptoTransform cTransform = tdes.CreateDecryptor();
            byte[] resultArray = cTransform.TransformFinalBlock(toEncryptArray, 0, toEncryptArray.Length);

            tdes.Clear();
            return UTF8Encoding.UTF8.GetString(resultArray);
        }
    }
    #endregion

    #region Class for BackGroundDetails
    /// <summary>
    /// Class for Background details
    /// </summary>
    public class BGMaster
    {
        /// <summary>
        /// The debug name
        /// </summary>
        private string bgName;

        /// <summary>
        /// Gets or sets the name of the debug.
        /// </summary>
        /// <value>
        /// The name of the debug.
        /// </value>
        public string BgName
        {
            get { return bgName; }
            set { bgName = value; }
        }
        /// <summary>
        /// The debug display name
        /// </summary>
        private string bgDisplayName;

        /// <summary>
        /// Gets or sets the display name of the debug.
        /// </summary>
        /// <value>
        /// The display name of the debug.
        /// </value>
        public string BgDisplayName
        {
            get { return bgDisplayName; }
            set { bgDisplayName = value; }
        }
        /// <summary>
        /// The filepath
        /// </summary>
        private string filepath;

        /// <summary>
        /// Gets or sets the filepath.
        /// </summary>
        /// <value>
        /// The filepath.
        /// </value>
        public string Filepath
        {
            get { return filepath; }
            set { filepath = value; }
        }
        /// <summary>
        /// The debug unique identifier
        /// </summary>
        private int bgID;

        /// <summary>
        /// Gets or sets the debug unique identifier.
        /// </summary>
        /// <value>
        /// The debug unique identifier.
        /// </value>
        public int BgID
        {
            get { return bgID; }
            set { bgID = value; }
        }
        /// <summary>
        /// The LST product type
        /// </summary>
        private List<AllProductType> lstProductType;

        /// <summary>
        /// Gets or sets the type of the LST product.
        /// </summary>
        /// <value>
        /// The type of the LST product.
        /// </value>
        public List<AllProductType> LstProductType
        {
            get { return lstProductType; }
            set { lstProductType = value; }
        }
    }

    public class AllProductType
    {
        /// <summary>
        /// The product name
        /// </summary>
        private string ProductName;

        /// <summary>
        /// Gets or sets the product name1.
        /// </summary>
        /// <value>
        /// The product name1.
        /// </value>
        public string ProductName1
        {
            get { return ProductName; }
            set { ProductName = value; }
        }
        /// <summary>
        /// The product unique identifier
        /// </summary>
        private int ProductId;

        /// <summary>
        /// Gets or sets the product id1.
        /// </summary>
        /// <value>
        /// The product id1.
        /// </value>
        public int ProductId1
        {
            get { return ProductId; }
            set { ProductId = value; }
        }
        /// <summary>
        /// The isvisible
        /// </summary>
        private Visibility Isvisible;

        /// <summary>
        /// Gets or sets the isvisible1.
        /// </summary>
        /// <value>
        /// The isvisible1.
        /// </value>
        public Visibility Isvisible1
        {
            get { return Isvisible; }
            set { Isvisible = value; }
        }

        /// <summary>
        /// Gets or sets the debug unique identifier.
        /// </summary>
        /// <value>
        /// The debug unique identifier.
        /// </value>
        public int BGId { get; set; }
    }
    #endregion

    #region Class for GraphicsDetails

    public class AllGraphics : ICloneable
    {

        /// <summary>
        /// The graphics name
        /// </summary>
        private string GraphicsName;

        /// <summary>
        /// Gets or sets the graphics name1.
        /// </summary>
        /// <value>
        /// The graphics name1.
        /// </value>
        public string GraphicsName1
        {
            get { return GraphicsName; }
            set { GraphicsName = value; }
        }
        /// <summary>
        /// The graphics display name
        /// </summary>
        private string GraphicsDisplayName;

        /// <summary>
        /// Gets or sets the graphics display name1.
        /// </summary>
        /// <value>
        /// The graphics display name1.
        /// </value>
        public string GraphicsDisplayName1
        {
            get { return GraphicsDisplayName; }
            set { GraphicsDisplayName = value; }
        }
        /// <summary>
        /// The is active
        /// </summary>
        private bool IsActive;

        /// <summary>
        /// Gets or sets a value indicating whether [is active1].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [is active1]; otherwise, <c>false</c>.
        /// </value>
        public bool IsActive1
        {
            get { return IsActive; }
            set { IsActive = value; }
        }
        /// <summary>
        /// The filepath
        /// </summary>
        private string Filepath;

        /// <summary>
        /// Gets or sets the filepath1.
        /// </summary>
        /// <value>
        /// The filepath1.
        /// </value>
        public string Filepath1
        {
            get { return Filepath; }
            set { Filepath = value; }
        }

        /// <summary>
        /// The pkey
        /// </summary>
        private int Pkey;

        /// <summary>
        /// Gets or sets the pkey1.
        /// </summary>
        /// <value>
        /// The pkey1.
        /// </value>
        public int Pkey1
        {
            get { return Pkey; }
            set { Pkey = value; }
        }
        public string IsActiveLabel
        {
            get
            {
                if (IsActive1)
                    return "Active";
                else
                    return "InActive";
            }
        }
        public object Clone()
        {
            return this.MemberwiseClone();
        }
    }
    #endregion

    #region Class for Substore

    public class SelectedSubStores
    {
        /// <summary>
        /// Gets or sets the name of the sub store.
        /// </summary>
        /// <value>
        /// The name of the sub store.
        /// </value>
        public string SubStoreName { get; set; }
        /// <summary>
        /// Gets or sets the sub store unique identifier.
        /// </summary>
        /// <value>
        /// The sub store unique identifier.
        /// </value>
        public int SubStoreId { get; set; }
        /// <summary>
        /// Gets or sets a value indicating whether [isselected].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [isselected]; otherwise, <c>false</c>.
        /// </value>
        public bool Isselected { get; set; }
        public bool VisiblVOrderStation { get; set; } ///  /// ////////////// Created by latika for visible in ViewOrderStation 2019 Nov 11
    }
    #endregion

    public class BurnOrderElements
    {
        public static bool isExecuting = false;
        private string BurnOrderNumber;
        /// <summary>
        /// DG Order Number
        /// </summary>
        public string BurnOrderNumber1
        {
            get { return BurnOrderNumber; }
            set { BurnOrderNumber = value; }
        }
        private string Status;
        /// <summary>
        /// Order Status
        /// </summary>
        public string Status1
        {
            get { return Status; }
            set { Status = value; }
        }

        private long BurnKey;

        /// <summary>
        /// Gets or sets the BurnKey1.
        /// </summary>
        /// <value>
        /// The BurnKey1.
        /// </value>
        public long BurnKey1
        {
            get { return BurnKey; }
            set { BurnKey = value; }
        }

        private string MediaType;

        public string MediaType1
        {
            get { return MediaType; }
            set { MediaType = value; }
        }

        private bool IsBurnEnable;

        public bool IsBurnEnable1
        {
            get { return IsBurnEnable; }
            set { IsBurnEnable = value; }
        }

        private Visibility IsBurnVisible;
        public Visibility IsBurnVisible1
        {
            get { return IsBurnVisible; }
            set { IsBurnVisible = value; }
        }

        private Visibility IsDisableBurnVisible;
        public Visibility IsDisableBurnVisible1
        {
            get { return IsDisableBurnVisible; }
            set { IsDisableBurnVisible = value; }
        }



    }

    public class GenerateQRCode
    {
        private static Random random = new Random();
        public static string GetNextQRCode(Int32 QRCodeLenth)
        {
            //string retval = string.Empty;
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            return new string(Enumerable.Repeat(chars, QRCodeLenth)
              .Select(s => s[random.Next(s.Length)]).ToArray());
            //return retval;
        }
    }

}
