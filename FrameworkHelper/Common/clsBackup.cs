﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.SqlServer.Management.Smo;
using Microsoft.SqlServer.Management.Common;
using Microsoft.SqlServer.Management.Utility;
using System.Data.SqlClient;
using System.IO;
using Ionic.Zip;
using System.Data;
using DigiPhoto.DataLayer.Model;
using DigiPhoto.DataLayer;
using System.Windows.Documents;
using System.Collections;
using System.Runtime.InteropServices;
using System.Reflection;
using DigiPhoto.IMIX.Model;
using System.Configuration;

namespace FrameworkHelper.Common
{
    public class clsBackup
    {
        static string jpgExtension = ConfigurationManager.AppSettings["jpgFileExtension"];
        static string pngExtension = ConfigurationManager.AppSettings["pngFileExtension"];
        static Hashtable htPhotoname = new Hashtable();
        static Dictionary<int, DateTime> dicPhotoCreated = new Dictionary<int, DateTime>();
        static DataTable dtPhotos = new DataTable();
        static Hashtable _PhotosToDelete = new Hashtable();
        static string _connectionString;
        public static bool BackupDatabase(string databaseName, string userName, string password, string serverName, string destinationPath, ref int errorId)
        {
            bool IsBackupSuccess = false;
            string LocalPath = string.Empty;
            bool IsNetworkBackup = false;
            string filePath = string.Empty;
            string bakfilename = databaseName + "_" + DateTime.Now.ToString("dd-MMM-yyyy-HH-mm-ss") + ".bak";//shifted this line by VINS
            try
            {
                ErrorHandler.ErrorHandler.LogFileWrite("Backup is being executed...");
                BackupDatabasebySQLQuery(databaseName, bakfilename, userName, password, serverName, destinationPath);

                string backUpZipPath = SaveBackUp(destinationPath, ref IsBackupSuccess, LocalPath, IsNetworkBackup, bakfilename, ref errorId);
                if (errorId != 1)
                {
                    ErrorHandler.ErrorHandler.LogFileWrite("Backup successfully taken at: " + backUpZipPath);
                }

                /*
                //Define a Backup object variable.
                Backup sqlBackup = new Backup();
                //Specify the type of backup, the description, the name, and the database to be backed up.
                sqlBackup.Action = BackupActionType.Database;
                sqlBackup.BackupSetDescription = "BackUp of:" + databaseName + "on" + DateTime.Now.ToShortDateString();
                sqlBackup.BackupSetName = "FullBackUp";
                sqlBackup.Database = databaseName;
                //string bakfilename = databaseName + "_" + DateTime.Now.ToString("dd-MMM-yyyy-HH-mm-ss") + ".bak";
                if (!IsBackupAvailable(destinationPath, databaseName))
                {
                    filePath = new Uri(Assembly.GetExecutingAssembly().CodeBase).LocalPath;
                    LocalPath = Path.GetDirectoryName(filePath) + "\\";
                }
                else
                {
                    Console.WriteLine("Backup already taken for today at : " + destinationPath);
                    errorId = Convert.ToInt32(BackupErrorMessage.DatabaseBackupAlreadyExists);
                    return true;
                }

                //Declare a BackupDeviceItem
                //BackupDeviceItem deviceItem = new BackupDeviceItem(destinationPath + "FullBackUp.bak", DeviceType.File);

                BackupDeviceItem deviceItem = new BackupDeviceItem(LocalPath + bakfilename, DeviceType.File);
                //Define Server connection
                ServerConnection connection = new ServerConnection(serverName, userName, password);
                //To Avoid TimeOut Exception
                Server sqlServer = new Server(connection);
                sqlServer.ConnectionContext.StatementTimeout = 60 * 60;
                Database db = sqlServer.Databases[databaseName];

                sqlBackup.Initialize = true;
                sqlBackup.Checksum = true;
                sqlBackup.ContinueAfterError = true;

                //Add the device to the Backup object.
                sqlBackup.Devices.Add(deviceItem);
                //Set the Incremental property to False to specify that this is a full database backup.
                sqlBackup.Incremental = false;

                sqlBackup.ExpirationDate = DateTime.Now.AddDays(3);
                //Specify that the log must be truncated after the backup is complete.
                sqlBackup.LogTruncation = BackupTruncateLogType.Truncate;

                sqlBackup.FormatMedia = false;
                //Run SqlBackup to perform the full database backup on the instance of SQL Server.
                ErrorHandler.ErrorHandler.LogFileWrite("DB Backup Start : " +System.DateTime.Now);
                sqlBackup.SqlBackup(sqlServer);
                ErrorHandler.ErrorHandler.LogFileWrite("DB Backup End : " + System.DateTime.Now);
                //Remove the backup device from the Backup object.
                sqlBackup.Devices.Remove(deviceItem);
                string backUpZipPath = SaveBackUp(destinationPath, ref IsBackupSuccess, LocalPath, IsNetworkBackup, bakfilename, ref errorId);
                if (errorId != 1)
                    Console.WriteLine("Backup successfully taken at: " + backUpZipPath);
                Console.WriteLine("Press any key to exit...");
                */

            }
            catch (Exception ex)
            {
                if (IsBackupSuccess)
                {
                    return IsBackupSuccess;
                }
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                throw new Exception("Backup operation could not be executed. Error: " + ex.Message);
            }
            return IsBackupSuccess;
        }

        /// <summary>
        /// Method added for Backup of SQL database
        /// Added by VINS 26Aug2020
        /// </summary>
        /// <param name="databaseName"></param>
        /// <param name="bakfilename"></param>
        /// <param name="userName"></param>
        /// <param name="password"></param>
        /// <param name="serverName"></param>
        /// <param name="destinationPath"></param>
        public static void BackupDatabasebySQLQuery(string databaseName, string bakfilename, string userName, string password, string serverName, string destinationPath)
        {
            string filePath = bakfilename;
            //Define Server connection
            ServerConnection connectionStr = new ServerConnection(serverName, userName, password);
            //To Avoid TimeOut Exception
            ErrorHandler.ErrorHandler.LogFileWrite("connection=" + connectionStr.ConnectionString);
            _connectionString = connectionStr.ConnectionString;
            using (var connection = new SqlConnection(_connectionString))
            {
                var query = String.Format("BACKUP DATABASE [{0}] TO DISK='{1}'", databaseName, destinationPath + bakfilename);
                ErrorHandler.ErrorHandler.LogFileWrite("SQL query=" + query);
                using (var command = new SqlCommand(query, connection))
                {
                    connection.Open();
                    command.CommandTimeout = 0;
                    command.ExecuteNonQuery();
                }
            }
        }

        private static string SaveBackUp(string destinationPath, ref bool IsBackupSuccess, string localPath, bool IsNetworkBackup, string bakfilename, ref int errorId)
        {
            try
            {
                long freeBytes = 0;
                localPath = destinationPath;
                string backUpZipPath = Path.Combine(destinationPath, Path.GetFileNameWithoutExtension(bakfilename) + ".zip");
                string dbBackUpFilePath = Path.Combine(localPath, bakfilename);
                if (File.Exists(dbBackUpFilePath))
                {
                    DriveFreeBytes(destinationPath, out freeBytes);
                    FileInfo file = new FileInfo(dbBackUpFilePath);
                    if (freeBytes > file.Length)
                    {

                        using (ZipFile zip = new ZipFile())
                        {
                            zip.AddFile(Path.Combine(dbBackUpFilePath), "");
                            zip.UseZip64WhenSaving = Zip64Option.Always;
                            //zip.CompressionMethod = CompressionMethod.BZip2;
                            zip.Save(backUpZipPath);
                            IsBackupSuccess = true;
                        }
                        File.Delete(dbBackUpFilePath);
                        return backUpZipPath;
                    }
                    else
                    {
                        errorId = (int)BackupErrorMessage.DestinationDiskFull;
                        //backUpZipPath = "There is not enough space in the drive.";
                        IsBackupSuccess = false;
                    }

                }
            }
            catch
            {
                //string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite("Catch----Save BackUp");
                IsBackupSuccess = false;
            }
            return string.Empty;
        }
        private static bool IsBackupAvailable(string destinationPath, string dataBaseName)
        {
            try
            {
                DirectoryInfo dirInfo = new DirectoryInfo(destinationPath);
                foreach (FileInfo file in dirInfo.GetFiles("*.zip"))
                {
                    if (file.CreationTime.Date == DateTime.Today && file.Name.Contains(dataBaseName + "_" + DateTime.Now.ToString("dd-MMM-yyyy")))
                        return true;
                    else
                        return false;
                }
                return false;
            }
            catch
            {
                return false;
            }
        }
        public static bool EmptyTables(int _cleanupdaysBackup, int oldCleanUpDays, int subStoreId, int _FailedOnlineOrderCleanupdays)
        {
            bool IsTableEmpty;
            try
            {
                int _sqlCommandTimeOutInSec;
                string _connstr = System.Configuration.ConfigurationManager.ConnectionStrings["DigiConnectionString"].ConnectionString;
                if (ConfigurationManager.AppSettings["SQLCommandTimeOut"] != null)
                    _sqlCommandTimeOutInSec = Convert.ToInt32(ConfigurationManager.AppSettings["SQLCommandTimeOut"]);
                else
                    _sqlCommandTimeOutInSec = 300;
                //if (!String.IsNullOrEmpty(heavytables))
                //{
                using (SqlConnection con = new SqlConnection(_connstr))
                {
                    //using (SqlCommand cmd = new SqlCommand("DG_TableCleanup", con))
                    using (SqlCommand cmd = new SqlCommand("DG_TableCleanup_Archive", con))
                    {
                        cmd.CommandTimeout = _sqlCommandTimeOutInSec;
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add("@BackUpDays", SqlDbType.Int).Value = _cleanupdaysBackup;
                        cmd.Parameters.Add("@OldCleanUpDays", SqlDbType.Int).Value = oldCleanUpDays;
                        cmd.Parameters.Add("@SubStoreId", SqlDbType.Int).Value = subStoreId;
                        cmd.Parameters.Add("@FaliledOnlineOrderCleanupdays", SqlDbType.Int).Value = _FailedOnlineOrderCleanupdays;
                        con.Open();
                        cmd.ExecuteNonQuery(); //uncomment
                    }
                }
                //}
                IsTableEmpty = true;
            }
            catch (Exception ex)
            {

                throw;
            }
            return IsTableEmpty;

        }

        //*******************************Manoj added code for Cleanup at 31-May-2018****************************************

        public static bool EmptyTables(string heavyTables, int _cleanupdaysBackup, int oldCleanUpDays, int subStoreId, int _FailedOnlineOrderCleanupdays)
        {
            bool IsTableEmpty;
            try
            {
                int _sqlCommandTimeOutInSec;
                string _connstr = System.Configuration.ConfigurationManager.ConnectionStrings["DigiConnectionString"].ConnectionString;
                if (ConfigurationManager.AppSettings["SQLCommandTimeOut"] != null)
                    _sqlCommandTimeOutInSec = Convert.ToInt32(ConfigurationManager.AppSettings["SQLCommandTimeOut"]);
                else
                    _sqlCommandTimeOutInSec = 300;

                using (SqlConnection con = new SqlConnection(_connstr))
                {
                    using (SqlCommand cmd = new SqlCommand("DG_TableCleanup_Backup_New", con))
                    {
                        cmd.CommandTimeout = _sqlCommandTimeOutInSec;
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add("@tables", SqlDbType.VarChar).Value = heavyTables;
                        cmd.Parameters.Add("@BackUpDays", SqlDbType.Int).Value = _cleanupdaysBackup;
                        cmd.Parameters.Add("@OldCleanUpDays", SqlDbType.Int).Value = oldCleanUpDays;
                        cmd.Parameters.Add("@SubStoreId", SqlDbType.Int).Value = subStoreId;
                        cmd.Parameters.Add("@FaliledOnlineOrderCleanupdays", SqlDbType.Int).Value = _FailedOnlineOrderCleanupdays;
                        con.Open();
                        cmd.ExecuteNonQuery(); //uncomment
                    }
                }
                IsTableEmpty = true;
            }
            catch (Exception ex)
            {

                throw;
            }
            return IsTableEmpty;

        }

        //-----------------Nilesh---------------26th Mar 2018-----------------------Start--------------------
        public static bool CleanDBTables(string heavytables, int _cleanupdaysBackup, int oldCleanUpDays, int subStoreId, int _FailedOnlineOrderCleanupdays)
        {
            bool IsTableEmpty;
            try
            {
                int _sqlCommandTimeOutInSec;
                string _connstr = System.Configuration.ConfigurationManager.ConnectionStrings["DigiConnectionString"].ConnectionString;
                if (ConfigurationManager.AppSettings["SQLCommandTimeOut"] != null)
                    _sqlCommandTimeOutInSec = Convert.ToInt32(ConfigurationManager.AppSettings["SQLCommandTimeOut"]);
                else
                    _sqlCommandTimeOutInSec = 300;
                if (!String.IsNullOrEmpty(heavytables))
                {
                    using (SqlConnection con = new SqlConnection(_connstr))
                    {
                        using (SqlCommand cmd = new SqlCommand("DG_TableCleanup_Backup_New", con))
                        {
                            cmd.CommandTimeout = _sqlCommandTimeOutInSec;
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.Add("@tables", SqlDbType.VarChar).Value = heavytables;
                            cmd.Parameters.Add("@BackUpDays", SqlDbType.Int).Value = _cleanupdaysBackup;
                            cmd.Parameters.Add("@OldCleanUpDays", SqlDbType.Int).Value = oldCleanUpDays;
                            cmd.Parameters.Add("@SubStoreId", SqlDbType.Int).Value = subStoreId;
                            cmd.Parameters.Add("@FaliledOnlineOrderCleanupdays", SqlDbType.Int).Value = _FailedOnlineOrderCleanupdays;
                            con.Open();
                            cmd.ExecuteNonQuery(); //uncomment
                        }
                    }
                }
                IsTableEmpty = true;
            }
            catch (Exception ex)
            {

                throw;
            }
            return IsTableEmpty;

        }


        public static bool IsDbBackupDone(int subStoreId)
        {
            bool IsDbBackupDone = false;
            try
            {
                int isbackCount = 0;
                DataSet ds = new DataSet();
                string _connstr = System.Configuration.ConfigurationManager.ConnectionStrings["DigiConnectionString"].ConnectionString;
                using (SqlConnection con = new SqlConnection(_connstr))
                {
                    //using (SqlCommand cmd = new SqlCommand("DG_TableCleanup", con))
                    using (SqlCommand cmd = new SqlCommand("usp_dg_IsDbBackupDone", con))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add("@SubStoreId", SqlDbType.Int).Value = subStoreId;
                        con.Open();
                        isbackCount = Convert.ToInt32(cmd.ExecuteScalar());
                    }

                    if (isbackCount == 1)
                    {
                        IsDbBackupDone = true;
                    }
                }

            }
            catch (Exception ex)
            {

                throw;
            }
            return IsDbBackupDone;

        }
        public static bool BackupCompress(string strSource, string strDestination)
        {
            bool isBackupCompressed;
            try
            {
                string currDir = "";
                currDir = Path.GetFileNameWithoutExtension(strSource);
                //string newpath = strDestination + currDir + "_" + DateTime.Now.ToString("dd-MMM-yyyy-HH-mm-ss") + ".zip";
                currDir = !String.IsNullOrEmpty(currDir) ? currDir : "HotFolderBackup";
                string newpath = strDestination + currDir + "_" + DateTime.Now.ToString("dd-MMM-yyyy-HH-mm-ss") + ".zip";
                using (ZipFile zip = new ZipFile())
                {
                    zip.AddDirectory(strSource, currDir);
                    zip.UseZip64WhenSaving = Zip64Option.Always;
                    zip.CompressionLevel = Ionic.Zlib.CompressionLevel.BestCompression;
                    zip.BufferSize = 65536 * 8;
                    zip.Save(newpath);
                }
                isBackupCompressed = true;
            }
            catch
            {

                isBackupCompressed = false;
            }
            return isBackupCompressed;

        }

        //public static int CleanDirectory(string clrDirectoryPath)
        //{
        //    DigiPhotoDataServices _objDataLayer1 = new DigiPhotoDataServices();
        //    List<vw_GetPhotoList> _objdata = _objDataLayer1.GetPhoto();

        //    int retCount = 0;
        //    try
        //    {
        //        if (Directory.Exists(clrDirectoryPath))
        //        {
        //            System.IO.DirectoryInfo myDirInfo = new DirectoryInfo(clrDirectoryPath);

        //            foreach (FileInfo file in myDirInfo.GetFiles())
        //            {
        //                var tempFile = _objdata.Where(te => te.DG_Photos_FileName == file.Name).FirstOrDefault();
        //                if (tempFile == null)
        //                {
        //                    if (file.Extension.ToUpper() == jpgExtension)
        //                    {
        //                        try
        //                        {
        //                            file.Delete(); //uncomment
        //                            retCount++;
        //                        }
        //                        catch
        //                        { }
        //                    }
        //                }
        //            }
        //        }
        //    }
        //    catch
        //    {
        //        retCount = -1;
        //    }
        //    return retCount;
        //}

        public static string CreateBackupFolderExits(string strSource, string strDestination)
        {
            try
            {
                string currDir = "";
                currDir = Path.GetFileNameWithoutExtension(strSource);
                currDir = !String.IsNullOrEmpty(currDir) ? currDir : "HotFolderBackup";
                string newpath = strDestination + currDir + "_" + DateTime.Now.ToString("dd-MMM-yyyy");
                if (!Directory.Exists(newpath))
                {
                    Directory.CreateDirectory(newpath);
                }
                return newpath;
            }
            catch
            {
                return null;
            }
        }
        public static string CreateBackupFolderExits(string strDestination)
        {
            try
            {
                string newpath = strDestination + "HotFolderBackup_" + DateTime.Now.ToString("dd-MMM-yyyy");
                if (!Directory.Exists(newpath))
                {
                    Directory.CreateDirectory(newpath);
                }
                return newpath;
            }
            catch
            {
                return null;
            }
        }
        public static bool UpdateArchivedPhotoDetails()
        {
            bool isTableEmpty;
            try
            {
                string _connstr = System.Configuration.ConfigurationManager.ConnectionStrings["DigiConnectionString"].ConnectionString;
                if (dtPhotos.Rows.Count > 0)
                {
                    using (SqlConnection con = new SqlConnection(_connstr))
                    {
                        using (SqlCommand cmd = new SqlCommand("UpdateArchivedPhotoDetails", con))
                        {
                            cmd.CommandType = CommandType.StoredProcedure;

                            cmd.Parameters.Add("@ArchivedPhotoDetails", dtPhotos);

                            con.Open();
                            cmd.ExecuteNonQuery(); //uncomment
                        }
                    }
                }
                isTableEmpty = true;
                return isTableEmpty;
            }
            catch
            {
                throw;
            }

        }
        /// <summary>
        /// once backup is done then cleanup will start
        /// </summary>
        /// <returns></returns>
        public static bool IsBackupCompleted(int backupId)
        {
            bool isBackHistoryUpdated;
            try
            {
                string _connstr = System.Configuration.ConfigurationManager.ConnectionStrings["DigiConnectionString"].ConnectionString;

                using (SqlConnection con = new SqlConnection(_connstr))
                {
                    using (SqlCommand cmd = new SqlCommand("usp_dg_ScheduledBackupDone", con))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add("@BackupId", SqlDbType.Int).Value = backupId;
                        cmd.Parameters.Add("@BackUpStatus", SqlDbType.Bit).Value = 1;
                        con.Open();
                        cmd.ExecuteNonQuery(); //uncomment
                    }
                }

                isBackHistoryUpdated = true;
                return isBackHistoryUpdated;
            }
            catch
            {
                throw;
            }

        }
        public static int CopyDirectoryFiles(string destinationFld, string clrDirectoryPath)
        {
            int retCount = 0;
            Hashtable htCleanDir = new Hashtable();
            try
            {
                //Add all the directories(directory names) to be cleared
                htCleanDir.Add(1, "Croped");
                htCleanDir.Add(2, "Download");
                htCleanDir.Add(3, "PendingItems");
                htCleanDir.Add(4, "PrintImages");
                htCleanDir.Add(5, "Thumbnails");
                htCleanDir.Add(6, "Thumbnails//Temp");
                htCleanDir.Add(7, "Thumbnails_Big");
                htCleanDir.Add(8, "");//this points to _imgSource directory itself i.e. D:\\DigiImages
                htCleanDir.Add(9, "EditedImages");
                htCleanDir.Add(10, "Videos");
                htCleanDir.Add(11, "ProcessedVideos");
                htCleanDir.Add(12, "PartialEditedImages"); //This folder is created only when the partial edited images are enabled 
                foreach (int strPhotoId in htPhotoname.Keys)
                {
                    try
                    {
                        bool fileCopied = false;
                        bool fileDeleted = false;
                        string directoryVal = string.Empty;
                        //string strPhoto = htPhotoname[strPhotoId].ToString();
                        var objArchivedPhotoInfo = (ArchivedPhotoInfo)htPhotoname[strPhotoId];
                        string strPhoto = objArchivedPhotoInfo.FileName;
                        int mediaType = objArchivedPhotoInfo.MediaType;
                        foreach (string dirval in htCleanDir.Values)
                        {
                            if (dirval.Equals("Videos") && mediaType == 2)
                            {
                                directoryVal = "Videos\\" + dicPhotoCreated[strPhotoId].ToString("yyyyMMdd");
                            }
                            else if (dirval.Equals("ProcessedVideos") && mediaType == 3)
                            {
                                directoryVal = "ProcessedVideos\\" + dicPhotoCreated[strPhotoId].ToString("yyyyMMdd");
                            }
                            else if (dirval == "")
                            {
                                directoryVal = dicPhotoCreated[strPhotoId].ToString("yyyyMMdd");
                            }
                            else if (dirval.Equals("Thumbnails", StringComparison.OrdinalIgnoreCase))
                            {
                                directoryVal = "Thumbnails\\" + dicPhotoCreated[strPhotoId].ToString("yyyyMMdd");
                            }
                            else if (dirval.Equals("Thumbnails_Big", StringComparison.OrdinalIgnoreCase))
                            {
                                directoryVal = "Thumbnails_Big\\" + dicPhotoCreated[strPhotoId].ToString("yyyyMMdd");
                            }
                            else if (dirval.Equals("Videos", StringComparison.OrdinalIgnoreCase))
                            {
                                directoryVal = "Videos\\" + dicPhotoCreated[strPhotoId].ToString("yyyyMMdd");
                            }
                            else
                            {
                                directoryVal = dirval;
                            }

                            if (dirval == "" || dirval.Contains("Videos") || dirval.Contains("ProcessedVideos"))
                            {
                                if (!Directory.Exists(destinationFld + "\\" + directoryVal))
                                {
                                    Directory.CreateDirectory(destinationFld + "\\" + directoryVal);
                                }
                                fileCopied = false;
                            }
                            else { fileCopied = true; }
                            if (mediaType != 1 && dirval == "Thumbnails")
                            {
                                strPhoto = Path.GetFileNameWithoutExtension(strPhoto);
                                strPhoto = strPhoto + jpgExtension;
                            }
                            if (Directory.Exists(destinationFld + "\\" + directoryVal))
                            {
                                if (File.Exists(clrDirectoryPath + directoryVal + "\\" + strPhoto))
                                {
                                    try
                                    {
                                        File.Copy(clrDirectoryPath + directoryVal + "\\" + strPhoto, destinationFld + "\\" + directoryVal + "\\" + strPhoto, true);
                                        fileCopied = true;
                                    }
                                    catch
                                    {
                                        fileCopied = false;
                                    }
                                }
                                else if (File.Exists(clrDirectoryPath + directoryVal + "\\" + "tmp" + strPhoto))
                                {
                                    try
                                    {
                                        File.Copy(clrDirectoryPath + directoryVal + "\\" + "tmp" + strPhoto, destinationFld + "\\" + directoryVal + "\\" + "tmp" + strPhoto, true);
                                        fileCopied = true;
                                    }
                                    catch
                                    {
                                        fileCopied = false;
                                    }
                                }
                                if (mediaType == 1)
                                {
                                    if (File.Exists(clrDirectoryPath + directoryVal + "\\" + strPhoto.Replace(jpgExtension, pngExtension)))
                                    {
                                        try
                                        {
                                            File.Copy(clrDirectoryPath + directoryVal + "\\" + strPhoto.Replace(jpgExtension, pngExtension), destinationFld + "\\" + directoryVal + "\\" + strPhoto.Replace(jpgExtension, pngExtension), true);
                                            fileCopied = true;
                                        }
                                        catch
                                        {
                                            fileCopied = false;
                                        }
                                    }
                                }

                            }
                            if (fileCopied)
                            {
                                try
                                {
                                    if (File.Exists(clrDirectoryPath + directoryVal + "\\" + strPhoto))
                                    {
                                        try
                                        {
                                            File.Delete(clrDirectoryPath + directoryVal + "\\" + strPhoto);
                                            fileDeleted = true;
                                        }
                                        catch
                                        {
                                            fileDeleted = false;
                                        }
                                    }
                                    else if (File.Exists(clrDirectoryPath + directoryVal + "\\" + "tmp" + strPhoto))
                                    {
                                        try
                                        {
                                            File.Delete(clrDirectoryPath + directoryVal + "\\" + "tmp" + strPhoto);
                                            fileDeleted = true;
                                        }
                                        catch
                                        {
                                            fileDeleted = false;
                                        }
                                    }

                                    if (mediaType == 1)
                                    {

                                        if (File.Exists(clrDirectoryPath + directoryVal + "\\" + strPhoto.Replace(jpgExtension, pngExtension)))
                                        {
                                            try
                                            {
                                                File.Delete(clrDirectoryPath + directoryVal + "\\" + strPhoto.Replace(jpgExtension, pngExtension));
                                                fileDeleted = true;
                                            }
                                            catch
                                            {
                                                fileDeleted = false;
                                            }
                                        }
                                    }
                                    try
                                    {
                                        if (dirval == "")
                                        {
                                            string[] files = System.IO.Directory.GetFiles(clrDirectoryPath + directoryVal + "\\");
                                            if (files.Length == 0)
                                                Directory.Delete(clrDirectoryPath + directoryVal + "\\");
                                        }
                                    }
                                    catch (Exception ex1)
                                    {
                                        string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex1);
                                        ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                                        string[] files = System.IO.Directory.GetFiles(clrDirectoryPath + directoryVal + "\\");
                                        if (files.Length == 0)
                                            Directory.Delete(clrDirectoryPath + directoryVal + "\\");
                                    }
                                }
                                catch (Exception ex2)
                                {
                                    string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex2);
                                    ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                                    //throw;
                                }
                            }
                        }
                        if (fileDeleted)
                        {
                            try
                            {
                                DataRow[] result = dtPhotos.Select("ArchivedPhotoId =" + strPhotoId + "");
                                foreach (DataRow dr in result)
                                {
                                    dr["FileDeleted"] = 1;
                                    dr["FileDeletedOn"] = DateTime.Now.ToString();
                                }
                            }
                            catch (Exception ex1)
                            {
                                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex1);
                                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                            }
                        }
                        else
                        {
                            DataRow[] result = dtPhotos.Select("ArchivedPhotoId =" + strPhotoId + "");
                            foreach (DataRow dr in result)
                            {
                                dr["FileDeleted"] = 1;
                            }
                        }
                        retCount++;
                    }
                    catch (Exception e)
                    {
                        throw;
                    }
                }
                return retCount;
            }
            //catch (Exception ex)
            catch
            {
                //throw;
                //string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite("Catch");
                return retCount;
            }
        }

        public static void CreateBackupFolderExits(object hfBackupPath)
        {
            throw new NotImplementedException();
        }

        public static int GetPhotoArchived()
        {
            int tablecount = 0;
            try
            {

                if (!dtPhotos.Columns.Contains("ArchivedPhotoId"))
                    dtPhotos.Columns.Add("ArchivedPhotoId", typeof(long));
                if (!dtPhotos.Columns.Contains("FileDeleted"))
                    dtPhotos.Columns.Add("FileDeleted", typeof(bool));
                if (!dtPhotos.Columns.Contains("FileDeletedOn"))
                    dtPhotos.Columns.Add("FileDeletedOn", typeof(DateTime));

                htPhotoname.Clear();
                dtPhotos.Clear();
                dicPhotoCreated.Clear();
                string _connstr = System.Configuration.ConfigurationManager.ConnectionStrings["DigiConnectionString"].ConnectionString;
                using (SqlConnection con = new SqlConnection(_connstr))
                {
                    using (SqlCommand cmd = new SqlCommand("dbo.usp_dg_GetArchivedPhotoName", con))
                    {
                        cmd.CommandTimeout = 300;
                        cmd.CommandType = CommandType.StoredProcedure;
                        //cmd.Parameters.Add("@SubStoreId", SqlDbType.Int).Value = subStoreId; // Code commented by Manoj for BackupService 26March18
                        con.Open();
                        using (SqlDataReader rdr = cmd.ExecuteReader(CommandBehavior.CloseConnection))
                        {
                            if (rdr.HasRows)
                            {
                                while (rdr.Read())
                                {
                                    string PhotofileName = rdr["DG_Photos_FileName"].ToString();
                                    int archivedPhotoId = Convert.ToInt32(rdr["ArchivedPhotoId"]);
                                    int mediaType = Convert.ToInt32(rdr["DG_MediaType"]);
                                    DateTime photoCreatedDate = Convert.ToDateTime(rdr["DG_Photos_CreatedOn"]);
                                    bool fileDeleted = Convert.ToBoolean(rdr["FileDeleted"]);
                                    if (!htPhotoname.ContainsKey(archivedPhotoId))
                                    {
                                        htPhotoname.Add(archivedPhotoId, new ArchivedPhotoInfo { FileName = PhotofileName, MediaType = mediaType, ArchivedPhotoId = archivedPhotoId, FileDeleted = fileDeleted });
                                        dicPhotoCreated.Add(Convert.ToInt32(archivedPhotoId), photoCreatedDate);
                                        DataRow dr = dtPhotos.NewRow();
                                        dr["ArchivedPhotoId"] = archivedPhotoId;
                                        dr["FileDeleted"] = fileDeleted;
                                        dtPhotos.Rows.Add(dr);
                                        tablecount++;
                                    }
                                }
                            }
                        }
                    }
                }
                return tablecount;
            }
            catch
            {
                //   return tablecount;
                throw;
            }
        }
        public static int GetPhotosToDelete(int subStoreId)
        {
            int rowCount = 0;
            try
            {
                _PhotosToDelete.Clear();
                string _connstr = System.Configuration.ConfigurationManager.ConnectionStrings["DigiConnectionString"].ConnectionString;
                using (SqlConnection con = new SqlConnection(_connstr))
                {
                    using (SqlCommand cmd = new SqlCommand("dbo.usp_GET_PhotosToDelete", con))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add("@SubStoreId", SqlDbType.Int).Value = subStoreId;
                        con.Open();

                        using (SqlDataReader rdr = cmd.ExecuteReader(CommandBehavior.CloseConnection))
                        {
                            if (rdr.HasRows)
                            {

                                while (rdr.Read())
                                {
                                    string PhotofileName = rdr["DG_Photos_FileName"].ToString();
                                    DateTime photoCreatedDate = Convert.ToDateTime(rdr["DG_Photos_CreatedOn"]);
                                    //int DGPhotosID = Convert.ToInt32(rdr["DG_Photos_pkey"]);
                                    try
                                    {
                                        if (!_PhotosToDelete.Contains(PhotofileName))
                                            _PhotosToDelete.Add(PhotofileName, photoCreatedDate);
                                    }
                                    catch { }
                                    rowCount++;
                                }
                            }
                        }
                    }
                }
                return rowCount;
            }
            catch
            {
                //   return tablecount;
                throw;
            }
        }

        public static bool IsMemoryAvailable(string sourcePath, string destinationPath, int subStoreID)
        {
            long freeVolume = 0;
            long size = 0;
            try
            {
                DriveFreeBytes(destinationPath, out freeVolume);
                //ErrorHandler.ErrorHandler.LogFileWrite("Free bytes found at : " + destinationPath + " is " + freeVolume);
                long AvailableMemoryInGb = freeVolume / 1073741824;
                GetPhotosToDelete(subStoreID);
                DirectoryInfo dir = new DirectoryInfo(sourcePath);
                List<FileInfo> files = dir.GetFiles("*.jpg", SearchOption.AllDirectories).Where(x => _PhotosToDelete.ContainsKey(x.Name)).ToList();
                foreach (FileInfo file in files)
                {
                    size += file.Length;
                }
                if (freeVolume > (size + (2.5 * 1073741824)))
                {
                    ErrorHandler.ErrorHandler.LogFileWrite("Sufficient space available for backup : ");
                    return true;
                }
                else
                {
                    ErrorHandler.ErrorHandler.LogFileWrite("Not sufficient space available for backup : ");
                    return false;
                }
            }

            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                return false;
            }

        }
        [DllImport("kernel32.dll", SetLastError = true, CharSet = CharSet.Auto)]
        [return: MarshalAs(UnmanagedType.Bool)]
        public static extern bool GetDiskFreeSpaceEx(string lpDirectoryName,
        out long lpFreeBytesAvailable,
        out long lpTotalNumberOfBytes,
        out long lpTotalNumberOfFreeBytes);
        public static bool DriveFreeBytes(string folderName, out long freespace)
        {
            freespace = 0;
            if (string.IsNullOrEmpty(folderName))
            {
                //MessageBox.Show("Plese provide drive name");
                throw new ArgumentNullException("folderName");

            }

            if (!folderName.EndsWith("\\"))
            {
                folderName += '\\';
            }

            long free = 0, dummy1 = 0, dummy2 = 0;

            if (GetDiskFreeSpaceEx(folderName, out free, out dummy1, out dummy2))
            {
                freespace = free;
                return true;
            }
            else
            {
                return false;
            }
        }
        static string directoriesToClean = "Videos,ProcessedVideos,Thumbnails,Thumbnails_Big,PrintImages,PartialImages,EditedImages," +
            "PrintImages,DigiOrderdImages,Archived,Camera,GreenImage,Croped,MobileTags";
        static string dirWithSubDirctories = "Videos,ProcessedVideos,Thumbnails,Thumbnails_Big";
        public static void CleanLeftOverFolders(string sourcePath, string destinationPath, int retentionDays)
        {
            int result = 0;
            string exception = string.Empty;
            try
            {
                DirectoryInfo hotFolderDirectory = new DirectoryInfo(sourcePath);
                List<DirectoryInfo> lstDir = hotFolderDirectory.GetDirectories().Where(x => (x.CreationTime < DateTime.Now.Date.AddDays(-retentionDays)) && (directoriesToClean.Contains(x.Name) || Int32.TryParse(x.Name, out result))).ToList();
                string destPath = destinationPath;
                if (!Directory.Exists(destPath))
                    Directory.CreateDirectory(destPath);
                foreach (DirectoryInfo rootDir in lstDir)
                {
                    if (dirWithSubDirctories.Contains(rootDir.Name))
                    {
                        DirectoryInfo dirSubDirectory = new DirectoryInfo(rootDir.FullName);
                        List<DirectoryInfo> subList = dirSubDirectory.GetDirectories().Where(x => x.CreationTime < DateTime.Now.Date.AddDays(-retentionDays) && Int32.TryParse(x.Name, out result)).ToList();
                        if (rootDir.Name.ToLower() == "Videos".ToLower())
                        {
                            foreach (DirectoryInfo childDir in subList)
                            {
                                CopyFiles(childDir, Path.Combine(destPath, rootDir.Name, childDir.Name), ref exception);
                                if (exception == string.Empty)
                                    childDir.Delete(true);
                            }
                        }
                        else if (rootDir.Name.ToLower() == "ProcessedVideos".ToLower())
                        {
                            foreach (DirectoryInfo childDir in subList)
                            {
                                CopyFiles(childDir, Path.Combine(destPath, rootDir.Name, childDir.Name), ref exception);
                                if (exception == string.Empty)
                                    childDir.Delete(true);
                            }
                        }
                        else
                        {
                            DirectoryInfo childDirectory = new DirectoryInfo(rootDir.FullName);
                            List<DirectoryInfo> subDirList = childDirectory.GetDirectories().Where(x => x.CreationTime < DateTime.Now.Date.AddDays(-retentionDays)).ToList();
                            foreach (DirectoryInfo childDir in subDirList)
                            {
                                if (childDir.FullName.ToLower().Contains("temp"))
                                {
                                    List<FileInfo> tempImageToDelete = childDir.GetFiles().Where(x => x.CreationTime < DateTime.Now.Date.AddDays(-retentionDays)).ToList();
                                    foreach (FileInfo file in tempImageToDelete)
                                    {
                                        try
                                        {
                                            file.Delete();
                                        }
                                        catch { }
                                    }
                                    DirectoryInfo tempDirectory = new DirectoryInfo(childDir.FullName);
                                    List<DirectoryInfo> tempSubDirList = tempDirectory.GetDirectories().Where(x => x.CreationTime < DateTime.Now.Date.AddDays(-retentionDays)).ToList();
                                    foreach (DirectoryInfo tempChildDir in tempSubDirList)
                                    {
                                        if (exception == string.Empty)
                                            tempChildDir.Delete(true);
                                    }
                                }
                                else if (childDir.FullName.ToLower().Contains("email"))
                                {
                                    List<FileInfo> emailImageToDelete = childDir.GetFiles().Where(x => x.CreationTime < DateTime.Now.Date.AddDays(-retentionDays)).ToList();
                                    foreach (FileInfo file in emailImageToDelete)
                                    {
                                        try
                                        {
                                            file.Delete();
                                        }
                                        catch { }
                                    }
                                    DirectoryInfo tempDirectory = new DirectoryInfo(childDir.FullName);
                                    List<DirectoryInfo> tempSubDirList = tempDirectory.GetDirectories().Where(x => x.CreationTime < DateTime.Now.Date.AddDays(-retentionDays)).ToList();
                                    foreach (DirectoryInfo tempChildDir in tempSubDirList)
                                    {
                                        if (exception == string.Empty)
                                            tempChildDir.Delete(true);
                                    }
                                }
                                else if (exception == string.Empty)
                                    childDir.Delete(true);
                            }
                        }
                    }
                    else if (rootDir.Name.ToLower() == "PrintImages".ToLower() || rootDir.Name.ToLower() == "PartialImages".ToLower()
                        || rootDir.Name.ToLower() == "EditedImages".ToLower() || rootDir.Name.ToLower() == "DigiOrderdImages".ToLower()
                        || rootDir.Name.ToLower() == "GreenImage".ToLower())
                    {
                        List<FileInfo> filesToDelete = rootDir.GetFiles().Where(x => x.CreationTime < DateTime.Now.Date.AddDays(-retentionDays)).ToList();
                        foreach (FileInfo file in filesToDelete)
                        {
                            try
                            {
                                file.Delete();
                            }
                            catch { }
                        }
                        DirectoryInfo dirSubDirectory = new DirectoryInfo(rootDir.FullName);
                        List<DirectoryInfo> subDirectoryList = dirSubDirectory.GetDirectories().Where(x => x.CreationTime < DateTime.Now.Date.AddDays(-retentionDays)).ToList();
                        foreach (DirectoryInfo subRootDir in subDirectoryList)
                        {

                            if (subRootDir.Name.ToLower().Contains("email"))
                            {
                                List<FileInfo> emailImageToDelete = subRootDir.GetFiles().Where(x => x.CreationTime < DateTime.Now.Date.AddDays(-retentionDays)).ToList();
                                foreach (FileInfo file in emailImageToDelete)
                                {
                                    try
                                    {
                                        file.Delete();
                                    }
                                    catch { }
                                }
                                DirectoryInfo emailDirectory = new DirectoryInfo(subRootDir.FullName);
                                List<DirectoryInfo> emailDirList = emailDirectory.GetDirectories().Where(x => x.CreationTime < DateTime.Now.Date.AddDays(-retentionDays)).ToList();
                                foreach (DirectoryInfo childDir in emailDirList)
                                {
                                    if (exception == string.Empty)
                                        childDir.Delete(true);
                                }
                            }
                            else if (subRootDir.Name.ToLower().Contains("temp"))
                            {
                                List<FileInfo> tempImageToDelete = subRootDir.GetFiles().Where(x => x.CreationTime < DateTime.Now.Date.AddDays(-retentionDays)).ToList();
                                foreach (FileInfo file in tempImageToDelete)
                                {
                                    try
                                    {
                                        file.Delete();
                                    }
                                    catch { }
                                }
                                DirectoryInfo tempChildDirectory = new DirectoryInfo(subRootDir.FullName);
                                List<DirectoryInfo> tempSubDirList = tempChildDirectory.GetDirectories().Where(x => x.CreationTime < DateTime.Now.Date.AddDays(-retentionDays)).ToList();
                                foreach (DirectoryInfo childDir in tempSubDirList)
                                {
                                    if (exception == string.Empty)
                                        childDir.Delete(true);
                                }
                            }
                            else if (subRootDir.Name.ToLower().Contains("Ridec"))
                            {
                                List<FileInfo> rideFileToDelete = subRootDir.GetFiles().Where(x => x.CreationTime < DateTime.Now.Date.AddDays(-retentionDays)).ToList();
                                foreach (FileInfo file in rideFileToDelete)
                                {
                                    try
                                    {
                                        file.Delete();
                                    }
                                    catch { }
                                }
                            }
                            else
                            {
                                DirectoryInfo childDirectory = new DirectoryInfo(rootDir.FullName);
                                List<DirectoryInfo> subDirList = childDirectory.GetDirectories().Where(x => x.CreationTime < DateTime.Now.Date.AddDays(-retentionDays)).ToList();
                                foreach (DirectoryInfo childDir in subDirList)
                                {
                                    if (exception == string.Empty)
                                        childDir.Delete(true);
                                }
                            }
                        }
                    }
                    else if (rootDir.Name.ToLower() == "Archived".ToLower())
                    {
                        List<FileInfo> filesToDelete = rootDir.GetFiles().Where(x => x.CreationTime < DateTime.Now.Date.AddDays(-retentionDays)).ToList();
                        foreach (FileInfo file in filesToDelete)
                        {
                            try
                            {
                                file.Delete();
                            }
                            catch { }
                        }
                        DirectoryInfo dirSubDirectory = new DirectoryInfo(rootDir.FullName);
                        List<DirectoryInfo> subDirectoryList = dirSubDirectory.GetDirectories().Where(x => x.CreationTime < DateTime.Now.Date.AddDays(-retentionDays)).ToList();
                        foreach (DirectoryInfo subRootDir in subDirectoryList)
                        {
                            List<FileInfo> ArchivedfilesToDelete = subRootDir.GetFiles().Where(x => x.CreationTime < DateTime.Now.Date.AddDays(-retentionDays)).ToList();
                            foreach (FileInfo file in ArchivedfilesToDelete)
                            {
                                try
                                {
                                    file.Delete();
                                }
                                catch { }
                            }
                        }
                    }
                    else if (rootDir.Name.ToLower() == "Croped".ToLower())
                    {
                        List<FileInfo> filesToDelete = rootDir.GetFiles().Where(x => x.CreationTime < DateTime.Now.Date.AddDays(-retentionDays)).ToList();
                        foreach (FileInfo file in filesToDelete)
                        {
                            try
                            {
                                file.Delete();
                            }
                            catch { }
                        }
                        DirectoryInfo dirSubDirectory = new DirectoryInfo(rootDir.FullName);
                        List<DirectoryInfo> subDirectoryList = dirSubDirectory.GetDirectories().Where(x => x.CreationTime < DateTime.Now.Date.AddDays(-retentionDays)).ToList();
                        foreach (DirectoryInfo subRootDir in subDirectoryList)
                        {
                            List<FileInfo> CropfilesToDelete = subRootDir.GetFiles().Where(x => x.CreationTime < DateTime.Now.Date.AddDays(-retentionDays)).ToList();
                            foreach (FileInfo file in CropfilesToDelete)
                            {
                                try
                                {
                                    file.Delete();
                                }
                                catch { }
                            }
                        }
                    }
                    else if (rootDir.Name.ToLower() == "Camera".ToLower() || rootDir.Name.ToLower() == "MobileTags".ToLower())
                    {
                        List<FileInfo> parentFolderfilesToDelete = rootDir.GetFiles().Where(x => x.CreationTime < DateTime.Now.Date.AddDays(-retentionDays)).ToList();
                        foreach (FileInfo file in parentFolderfilesToDelete)
                        {
                            try
                            {
                                file.Delete();
                            }
                            catch { }
                        }
                        DirectoryInfo dirSubDirectory = new DirectoryInfo(rootDir.FullName);
                        List<DirectoryInfo> subDirectoryList = dirSubDirectory.GetDirectories().Where(x => x.CreationTime < DateTime.Now.Date.AddDays(-retentionDays)).ToList();
                        foreach (DirectoryInfo subRootDir in subDirectoryList)
                        {
                            if (subRootDir.Name.ToLower().Contains("ridec"))
                            {
                                List<FileInfo> filesToDelete = subRootDir.GetFiles().Where(x => x.CreationTime < DateTime.Now.Date.AddDays(-retentionDays)).ToList();
                                foreach (FileInfo file in filesToDelete)
                                {
                                    try
                                    {
                                        file.Delete();
                                    }
                                    catch { }
                                }
                            }

                            if (subRootDir.Name.ToLower() == "Email".ToLower())
                            {
                                List<FileInfo> filesToDelete = subRootDir.GetFiles().Where(x => x.CreationTime < DateTime.Now.Date.AddDays(-retentionDays)).ToList();
                                foreach (FileInfo file in filesToDelete)
                                {
                                    try
                                    {
                                        file.Delete();
                                    }
                                    catch { }
                                }
                            }
                            if (subRootDir.Name.ToLower() == "Processed".ToLower())
                            {
                                List<FileInfo> filesToDelete = subRootDir.GetFiles().Where(x => x.CreationTime < DateTime.Now.Date.AddDays(-retentionDays)).ToList();
                                foreach (FileInfo file in filesToDelete)
                                {
                                    try
                                    {
                                        file.Delete();
                                    }
                                    catch { }
                                }
                            }
                        }
                    }
                    else
                    {
                        CopyFiles(rootDir, Path.Combine(destPath, rootDir.Name), ref exception);
                        if (exception == string.Empty)
                            rootDir.Delete(true);
                    }

                }
            }
            catch (Exception ex)
            {
                //exception = "Exception Message :" + ex.Message + "\n" + "Inner Exception :" + ex.InnerException + "\n" + "Stack Trace :" + ex.StackTrace;
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                exception = string.Empty;
            }

        }
        public static void CopyFiles(DirectoryInfo dir, string destPath, ref string exception)
        {
            try
            {
                if (dir.GetFiles().Count() == 0)
                    return;
                if (!Directory.Exists(destPath))
                    Directory.CreateDirectory(destPath);
                foreach (FileInfo file in dir.GetFiles())
                {
                    file.CopyTo(Path.Combine(destPath, file.Name));
                }
                exception = string.Empty;
            }
            catch (Exception ex)
            {
                exception = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(exception);
                exception = string.Empty;
            }
        }
    }
}
