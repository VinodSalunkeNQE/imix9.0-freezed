﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Drawing;
using System.Drawing.Imaging;
using System.Drawing.Drawing2D;

namespace DigiPhoto.Common
{
    public class ResetDPI
    {
        private ResetDPI() { }
        //Scale the image to a percentage of its actual size. 
        public static Image ScaleByPercentage(Image img, double percent)
        {
            double fractionalPercentage = (percent / 100.0);
            int outputWidth = (int)(img.Width * fractionalPercentage);
            int outputHeight = (int)(img.Height * fractionalPercentage);
            return ResetDPI.ScaleImage(img, outputWidth, outputHeight);
        }
        //Scale down the image till it fits the given size.
        public static Image ScaleByWidth(Image img, int width)
        {
            double fractionalPercentage = ((double)width / (double)img.Width);
            int outputWidth = width;
            int outputHeight = (int)(img.Height * fractionalPercentage);
            return ResetDPI.ScaleImage(img, outputWidth, outputHeight);
        }
        //Scale an image by a set height. The width will be set proportionally.
        public static Image ScaleByHeight(Image img, int height)
        {
            double fractionalPercentage = ((double)height / (double)img.Height);
            int outputWidth = (int)(img.Width * fractionalPercentage);
            int outputHeight = height;
            return ResetDPI.ScaleImage(img, outputWidth, outputHeight);
        }
        //Scale an image by a set Height and set Specific Resolution. The width will be set proportionally. 
        public static Image ScaleByHeightAndResolution(Image img, int height, float Resolution)
        {
            double fractionalPercentage = ((double)height / (double)img.Height);
            int outputWidth = (int)(img.Width * fractionalPercentage);
            int outputHeight = height;
            return ResetDPI.ScaleImageAndResolution(img, outputWidth, outputHeight, Resolution);
        }
        //Scale an image to a given width and height.
        public static Image ScaleImage(Image img, int outputWidth, int outputHeight)
        {
            Bitmap outputImage = new Bitmap(outputWidth, outputHeight, img.PixelFormat);
            outputImage.SetResolution(img.HorizontalResolution, img.VerticalResolution);
            Graphics graphics = Graphics.FromImage(outputImage);
            graphics.InterpolationMode = InterpolationMode.Bilinear;
            graphics.DrawImage(img, new Rectangle(0, 0, outputWidth, outputHeight), new Rectangle(0, 0, img.Width, img.Height), GraphicsUnit.Pixel);
            graphics.Dispose();
            return outputImage;
        }

        //Change image scale by Width and Height and Set New Resoltion too.
        public static Image ScaleImageAndResolution(Image img, int outputWidth, int outputHeight, float Resolution)
        {
            Bitmap outputImage = new Bitmap(outputWidth, outputHeight, img.PixelFormat);
            outputImage.SetResolution(Resolution, Resolution);
            Graphics graphics = Graphics.FromImage(outputImage);
            graphics.InterpolationMode = InterpolationMode.Bilinear;
            graphics.DrawImage(img, new Rectangle(0, 0, outputWidth, outputHeight), new Rectangle(0, 0, img.Width, img.Height), GraphicsUnit.Pixel);
            graphics.Dispose();
            return outputImage;
        }
    }
}
