﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.ComponentModel;
using System.Threading;
using DigiPhoto.ImageGallery;

namespace DigiPhoto.Common
{
    public class AsyncImageLoader
    {
        /// <summary>
        /// The _is canceled
        /// </summary>
        private volatile bool _isCanceled;
        /// <summary>
        /// The _background worker
        /// </summary>
        private BackgroundWorker _backgroundWorker;
        /// <summary>
        /// The _tasks
        /// </summary>
        private Queue<ImageCollection> _tasks;
        /// <summary>
        /// The _image count
        /// </summary>
        private int _imageCount = 0;
        /// <summary>
        /// The _run deep
        /// </summary>
        private bool _runDeep = false;
        /// <summary>
        /// The _dispather
        /// </summary>
        private System.Windows.Threading.Dispatcher _dispather;
        /// <summary>
        /// The _add images
        /// </summary>
        private Collection<ImageFile> _addImages = new Collection<ImageFile>();
        /// <summary>
        /// The _del images
        /// </summary>
        private Collection<ImageFile> _delImages = new Collection<ImageFile>();

        /// <summary>
        /// The preview images
        /// </summary>
        private Collection<string> PreviewImages;


        /// <summary>
        /// Sets the load images.
        /// </summary>
        /// <value>
        /// The load images.
        /// </value>
        public Collection<string> LoadImages
        {
            set
            {
                PreviewImages = value;
            }
        }
        /// <summary>
        /// Initializes a new instance of the <see cref="AsyncImageLoader"/> class.
        /// </summary>
        /// <param name="tasks">The tasks.</param>
        /// <param name="imageCount">The image count.</param>
        /// <param name="runDeep">if set to <c>true</c> [run deep].</param>
        /// <param name="dispather">The dispather.</param>
        public AsyncImageLoader(Queue<ImageCollection> tasks, int imageCount, bool runDeep, System.Windows.Threading.Dispatcher dispather)
        {
            this._tasks = tasks;
            this._imageCount = imageCount;
            this._runDeep = runDeep;
            this._dispather = dispather;
            this._isCanceled = false;
            this._backgroundWorker = new BackgroundWorker();
            this._backgroundWorker.DoWork += new DoWorkEventHandler(_backgroundWorker_DoWork);
            this._backgroundWorker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(_backgroundWorker_RunWorkerCompleted);
        }

        /// <summary>
        /// Cancels the asynchronous.
        /// </summary>
        public void CancelAsync()
        {
            this._isCanceled = true;
        }

        /// <summary>
        /// Clears the q.
        /// </summary>
        public void ClearQ()
        {
            lock (this._tasks)
                this._tasks.Clear();
        }

        /// <summary>
        /// Runs the asynchronous.
        /// </summary>
        public void RunAsync()
        {
            //_processingWindow = new Window_Processing();
            //_processingWindow.Show();
            ProcessNextItem();
        }

        /// <summary>
        /// Processes the next item.
        /// </summary>
        private void ProcessNextItem()
        {
            // Need this "if" statement to end this thread. 
            lock (this._tasks)
                if (this._tasks.Count > 0)
                {
                    this._backgroundWorker.RunWorkerAsync();
                }
                else
                {
                    Thread.Sleep(1000);
                    
                    return;
                }
        }

        // delegate for the next mothod.
        /// <summary>
        /// 
        /// </summary>
        public delegate void delegate_Void();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="imageFile">The image file.</param>
        public delegate void delegate_Void_ImageFile(ImageFile[] imageFile);

        /// <summary>
        /// Handles the DoWork event of the _backgroundWorker control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="DoWorkEventArgs"/> instance containing the event data.</param>
        void _backgroundWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            ImageCollection tempTask;
            lock (this._tasks)
                if (this._tasks.Count > 0)
                    tempTask = this._tasks.Dequeue();
                else
                    return;

            // Create all images at Background.
            _addImages.Clear();
            _delImages.Clear();
            AutoAdd(tempTask, PreviewImages);

            // Invoke to add images to display
            if (_addImages.Count > 0)
            {
                ImageFile[] images2 = new ImageFile[_addImages.Count];
                _addImages.CopyTo(images2, 0);
                delegate_Void_ImageFile myD1 = new delegate_Void_ImageFile(tempTask.Add);
                _dispather.Invoke(myD1, new object[1] { images2 });
            }

            // Invoke to del images to display
            if (_delImages.Count > 0)
            {
                ImageFile[] images2 = new ImageFile[_delImages.Count];
                _delImages.CopyTo(images2, 0);
                delegate_Void_ImageFile myD1 = new delegate_Void_ImageFile(tempTask.Remove);
                _dispather.Invoke(myD1, new object[1] { images2 });
            }


            e.Result = tempTask;
        }

        /// <summary>
        /// Automatics the add.
        /// </summary>
        /// <param name="addTo">The add automatic.</param>
        /// <param name="ParentImages">The parent images.</param>
        private void AutoAdd(ImageCollection addTo,Collection<string> ParentImages)
        {
            try
            {
                // Prevent scanning its own data store.
                //if (dir.FullName.ToLower().Contains(string.Empty))//AR._MyGalleryDataPath.ToLower()))
                //    return;

                // Get Total Images of the Directory
                Collection<string> imagesInDir = ParentImages;

                //foreach (System.IO.FileInfo file in dir.GetFiles())
                //    if (AR.GalleryConfig.IsImage(file.Extension))
                //        imagesInDir.Add(file.FullName);


                int totalImagesInDir = imagesInDir.Count;

                // Get Total Images of the Visual Tree
                int totalImagesInVisualTree = imagesInDir.Count;//addTo.CountInDir(dir.FullName);

                // Get desired image count
                int wantImageCount = _imageCount;
                if (wantImageCount > 10)
                    wantImageCount = (int)(totalImagesInDir * wantImageCount / 100);    // apply percentage
                wantImageCount = Math.Max(1, wantImageCount);                           // at least one image
                wantImageCount = Math.Min(totalImagesInDir, wantImageCount);            // max at total images in directory

                // Add or Remove based on desired image count
                if (totalImagesInVisualTree < wantImageCount)
                {
                    for (int i = totalImagesInVisualTree; i < wantImageCount; i++)
                        _addImages.Add(new ImageFile(imagesInDir[i]));
                }
                else if (totalImagesInVisualTree > wantImageCount)
                {
                    for (int i = wantImageCount; i < totalImagesInVisualTree; i++)
                        _delImages.Add(new ImageFile(imagesInDir[i]));
                }

                // Go Deeper
                //if (_runDeep)
                //    foreach (System.IO.DirectoryInfo sub in dir.GetDirectories())
                //        AutoAdd(addTo, sub);
            }
            catch { }
        }

        /// <summary>
        /// Handles the RunWorkerCompleted event of the _backgroundWorker control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RunWorkerCompletedEventArgs"/> instance containing the event data.</param>
        void _backgroundWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (this._isCanceled == false && e.Error == null)
                ProcessNextItem();
        }
    }
}
