using System;
using System.Globalization;
using System.Windows.Data;
using System.Windows.Media.Imaging;
using System.IO;
using FrameworkHelper;

namespace DigiPhoto.Common
{
    public class UriToBitmapConverter : IValueConverter
    {
        /// <summary>
        /// Converts a value.
        /// </summary>
        /// <param name="value">The value produced by the binding source.</param>
        /// <param name="targetType">The type of the binding target property.</param>
        /// <param name="parameter">The converter parameter to use.</param>
        /// <param name="culture">The culture to use in the converter.</param>
        /// <returns>
        /// A converted value. If the method returns null, the valid null value is used.
        /// </returns>
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            try
            {
                BitmapImage bi = new BitmapImage();
                if (value != null)
                {
                    GC.AddMemoryPressure(10000);
                    using (FileStream fileStream = File.OpenRead(value.ToString()))
                    {

                        MemoryStream ms = new MemoryStream();
                        fileStream.CopyTo(ms);
                        ms.Seek(0, SeekOrigin.Begin);
                        fileStream.Close();
                        bi.BeginInit();
                        bi.CacheOption = BitmapCacheOption.OnLoad;
                        bi.StreamSource = ms;
                        bi.EndInit();
                        bi.Freeze();

                    }
                }
                else
                {
                    bi = new BitmapImage();
                }

                return bi;
            }
            catch (Exception ex)
            {
                //ex = new ErrorHandler.DigiException("Error in Converter", ex);
                //ErrorHandler.ErrorHandler.LogFileWrite(ex.ToString());

                //string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                //ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                return null;
            }
            finally
            {
                GC.RemoveMemoryPressure(10000);
                // GC.WaitForPendingFinalizers();
                //GC.Collect();
                //MS.
            }

        }
        /// <summary>
        /// Converts a value.
        /// </summary>
        /// <param name="value">The value that is produced by the binding target.</param>
        /// <param name="targetType">The type to convert to.</param>
        /// <param name="parameter">The converter parameter to use.</param>
        /// <param name="culture">The culture to use in the converter.</param>
        /// <returns>
        /// A converted value. If the method returns null, the valid null value is used.
        /// </returns>
        /// <exception cref="System.Exception">The method or operation is not implemented.</exception>
        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new Exception("The method or operation is not implemented.");
        }
    }

    public class IntToBooleanConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is int)
            {
                int quantity = (int)value;
                if (quantity == 0) return false;
                if (quantity == 1) return true;

            }
            return true;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }


}
