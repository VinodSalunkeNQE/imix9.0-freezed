﻿using log4net;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;

namespace DigiPhoto.ExtensionMethods
{
    public static class ExtensionMethods
    {
        public static void ExportToCSV(this DataTable dtDataTable, string removableWords, string exportPath)
        {
            try
            {
                using (StreamWriter sw = new StreamWriter(exportPath, false))
                {
                    //headers
                    for (int i = 0; i < dtDataTable.Columns.Count; i++)
                    {
                        var columnName = dtDataTable.Columns[i].ColumnName;
                        columnName = columnName.Replace(removableWords, string.Empty);
                        sw.Write(columnName.Trim());
                        if (i < dtDataTable.Columns.Count - 1)
                        {
                            sw.Write(",");
                        }
                    }
                    sw.Write(sw.NewLine);
                    foreach (DataRow dr in dtDataTable.Rows)
                    {
                        for (int i = 0; i < dtDataTable.Columns.Count; i++)
                        {
                            if (!Convert.IsDBNull(dr[i]))
                            {
                                string value = dr[i].ToString();
                                if (value.Contains(','))
                                {
                                    value = String.Format("\"{0}\"", value);
                                    sw.Write(value);
                                }
                                else
                                {
                                    sw.Write(dr[i].ToString());
                                }
                            }
                            if (i < dtDataTable.Columns.Count - 1)
                            {
                                sw.Write(",");
                            }
                        }
                        sw.Write(sw.NewLine);
                    }
                }
            }
            catch (Exception ex)
            {
                LogError("DigiReportExportService", "Extensions", "ExportToCSV", ex.Message + "\n" + ex.StackTrace);
            }
        }
        public static bool ExportToCSV(this DataTable dtDataTable, string removableWords, string exportPath, out string exceptionMessage)
        {
            bool isExported = false;
            exceptionMessage = string.Empty;
            try
            {

                using (StreamWriter sw = new StreamWriter(exportPath, false))
                {
                    //headers
                    for (int i = 0; i < dtDataTable.Columns.Count; i++)
                    {
                        var columnName = dtDataTable.Columns[i].ColumnName;
                        columnName = columnName.Replace(removableWords, string.Empty);
                        sw.Write(columnName.Trim());
                        if (i < dtDataTable.Columns.Count - 1)
                        {
                            sw.Write(",");
                        }
                    }
                    sw.Write(sw.NewLine);
                    foreach (DataRow dr in dtDataTable.Rows)
                    {
                        for (int i = 0; i < dtDataTable.Columns.Count; i++)
                        {
                            if (!Convert.IsDBNull(dr[i]))
                            {
                                string value = dr[i].ToString();
                                if (value.Contains(','))
                                {
                                    value = String.Format("\"{0}\"", value);
                                    sw.Write(value);
                                }
                                else
                                {
                                    sw.Write(dr[i].ToString());
                                }
                            }
                            if (i < dtDataTable.Columns.Count - 1)
                            {
                                sw.Write(",");
                            }
                        }
                        sw.Write(sw.NewLine);
                        isExported = true;
                    }

                }
            }
            catch (Exception ex)
            {
                LogError("DigiReportExportService", "Extensions", "ExportToCSV", ex.Message + "\n" + ex.StackTrace);
                exceptionMessage = ex.Message + "\n" + ex.StackTrace;
                isExported = false;
            }
            return isExported;
        }
        public static void LogError(string applicationName, string className, string methodNaame, string message)
        {
            string errorMessage = string.Format("Application : {0} , Module : {1} , Method : {2} , ErrorMessage : {3}", applicationName, className, methodNaame, message);
            LogConfigurator.log.Error(errorMessage);
            ErrorHandler.ErrorHandler.LogError(new Exception(errorMessage));


        }


    }
    public class LogConfigurator
    {
        /// <summary>
        /// Configures the log4net.
        /// </summary>
        static LogConfigurator()
        {
            log4net.Config.XmlConfigurator.Configure();
        }
        public static ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
    }
}
