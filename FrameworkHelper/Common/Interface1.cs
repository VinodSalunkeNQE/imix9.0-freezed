﻿using System;

namespace DigiPhoto.Common
{

    /// <summary>
    /// 
    /// </summary>
    public interface ICanXMLExportImport
    {
        /// <summary>
        /// Exports the XML.
        /// </summary>
        /// <returns></returns>
        String ExportXML();
        /// <summary>
        /// Imports the XML.
        /// </summary>
        /// <param name="reader">The reader.</param>
        void ImportXML(System.Xml.XmlTextReader reader);
    }

    /// <summary>
    /// 
    /// </summary>
    public interface ICanGetThumbPath
    {
        /// <summary>
        /// Gets the mini thumb path.
        /// </summary>
        /// <param name="path">The path.</param>
        /// <param name="checkExist">if set to <c>true</c> [check exist].</param>
        /// <returns></returns>
        string GetMiniThumbPath(string path, bool checkExist);
        /// <summary>
        /// Gets the thumb path.
        /// </summary>
        /// <param name="path">The path.</param>
        /// <param name="checkExist">if set to <c>true</c> [check exist].</param>
        /// <returns></returns>
        string GetThumbPath(string path, bool checkExist);
        /// <summary>
        /// Gets the mini source path.
        /// </summary>
        /// <param name="thumbPath">The thumb path.</param>
        /// <param name="checkExist">if set to <c>true</c> [check exist].</param>
        /// <returns></returns>
        string GetMiniSourcePath(string thumbPath, bool checkExist);
        /// <summary>
        /// Gets the source path.
        /// </summary>
        /// <param name="thumbPath">The thumb path.</param>
        /// <param name="checkExist">if set to <c>true</c> [check exist].</param>
        /// <returns></returns>
        string GetSourcePath(string thumbPath, bool checkExist);
    }

    /// <summary>
    /// 
    /// </summary>
    public interface ICanGetThumbBitmapImage
    {
        /// <summary>
        /// Gets the mini thumb_ bitmap image.
        /// </summary>
        /// <param name="path">The path.</param>
        /// <param name="decodeWidth">Width of the decode.</param>
        /// <returns></returns>
        System.Windows.Media.Imaging.BitmapImage GetMiniThumb_BitmapImage(string path, int decodeWidth);
        /// <summary>
        /// Gets the thumb_ bitmap image.
        /// </summary>
        /// <param name="path">The path.</param>
        /// <param name="decodeWidth">Width of the decode.</param>
        /// <returns></returns>
        System.Windows.Media.Imaging.BitmapImage GetThumb_BitmapImage(string path, int decodeWidth);
    }

}
