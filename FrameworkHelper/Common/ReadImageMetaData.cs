﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Media.Imaging;

namespace DigiPhoto.Common
{
  public class ReadImageMetaData
    {
        /// <summary>
        /// Gets the image meta data.
        /// </summary>
        /// <param name="filename">The filename.</param>
        /// <returns></returns>
      public string GetImageMetaData(string filename)
      {
          string metadata=string.Empty;
          try
          {
              BitmapSource img = BitmapFrame.Create(new Uri(filename));
              BitmapMetadata meta = (BitmapMetadata)img.Metadata;

              /* Image data */
              double mpixel = (img.PixelHeight * img.PixelWidth) / (double)1000000;
              string Dimensions = img.PixelWidth.ToString() + "x" + img.PixelHeight.ToString();
              string size = mpixel.ToString() + " mb";
              string DPI = img.DpiX.ToString() + "x" + img.DpiY.ToString();


              /* Image metadata */
              string title = "##";
              string subject = "##";
              string comment = "##";
              string Datetaken = "##";
              string Camera = "##";
              string Copyrightinfo = "##";
              string KeyWords = "##";
              metadata = Dimensions + "@" + size + "@" + DPI + "@" + title + "@" + subject + "@" + comment + "@" + Datetaken + "@" + Camera + "@" + Copyrightinfo + "@" + KeyWords;
              try
              {
                  if (meta.Title != null)
                  {
                      title = meta.Title.ToString();
                  }
              }
              catch
              {
                  title = "";
              }
              try
              {
                  if (meta.Subject != null)
                  {
                      subject = meta.Subject.ToString();
                  }
              }
              catch(Exception ex)
              {
                  subject = "";
              }
              try
              {
                  if (meta.Comment != null)
                  {
                      comment = meta.Comment.ToString();
                  }
              }
              catch
              {
                  comment = "";
              }
              try
              {
                  if (meta.DateTaken != null)
                  {
                      Datetaken = meta.DateTaken.ToString();
                  }
              }
              catch
              {
                  Datetaken = "";
              }
              try
              {
                  if (meta.CameraManufacturer != null || meta.CameraModel != null)
                  {
                      Camera = meta.CameraManufacturer.ToString() + " " + meta.CameraModel.ToString();
                  }
              }
              catch (Exception ex)
              {
                  Camera = "";
              }
              try
              {
                  if (meta.Copyright != null)
                  {
                      Copyrightinfo = meta.Copyright.ToString();
                  }
              }
              catch
              {
                  Copyrightinfo = "";
              }

              try
              {
                  if (meta.Keywords != null)
                  {
                      KeyWords = meta.Keywords.ToString();
                  }
              }
              catch
              {
                  KeyWords = "";
              }
             
              metadata = Dimensions + "@" + size + "@" + DPI + "@" + title + "@" + subject + "@" + comment + "@" + Datetaken + "@" + Camera + "@" + Copyrightinfo + "@" + KeyWords;
              return metadata;
          }
          catch (Exception ex)
          {
              string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
              ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
              return metadata;
          }
      }
    }
}
