﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Forms;


namespace DemoVideoPlayer
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : System.Windows.Application
    {
        //protected override void OnStartup(StartupEventArgs e)
        //{
        //    base.OnStartup(e);
        //    //int ScreenCount = Screen.AllScreens.Count();
        //    //MainWindow Me = new MainWindow();
        //    //if (ScreenCount > 1)
        //    //{
        //    //    Screen s = Screen.AllScreens[0];

        //    //    System.Drawing.Rectangle r = s.WorkingArea;
        //    //    Me.Top = r.Top;
        //    //    Me.Left = r.Left;
                
        //    //}
        //    var primaryDisplay = Screen.AllScreens.ElementAtOrDefault(0);
        //    var extendedDisplay = Screen.AllScreens.FirstOrDefault(s => s != primaryDisplay) ?? primaryDisplay;

        //    this.Left = extendedDisplay.WorkingArea.Left + (extendedDisplay.Bounds.Size.Width / 2) - (this.Size.Width / 2);
        //    this.Top = extendedDisplay.WorkingArea.Top + (extendedDisplay.Bounds.Size.Height / 2) - (this.Size.Height / 2);
        //}
      
        private void Application_Startup(object sender, StartupEventArgs e)
        {
            //DataCacheFactory.Register();
            //Timeline.DesiredFrameRateProperty.OverrideMetadata(typeof(Timeline),
            //                   new FrameworkPropertyMetadata { DefaultValue = 5 });
            // Get Reference to the current Process
            Process thisProc = Process.GetCurrentProcess();
            // Check how many total processes have the same name as the current one
            if (Process.GetProcessesByName(thisProc.ProcessName).Length > 1)
            {
                // If there is more than one, then it is already running.
                System.Windows.MessageBox.Show("Video display utility is already running.","Digiphoto",MessageBoxButton.OK,MessageBoxImage.Information);
                System.Windows.Application.Current.Shutdown();
                return;
            }
        }
    }

}
