﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Forms;
using System.IO;
using System.Xml;
using System.Windows.Threading;
using System.Text.RegularExpressions;
using MPLATFORMLib;
using System.Runtime.InteropServices;
using System.Diagnostics;


namespace DemoVideoPlayer
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        #region Declaration
        static string xmlFileName = string.Empty;
        static string xmlFilePath = string.Empty;
        static string xmlFilePathMaster = string.Empty;
        public static string VideoPath = string.Empty;
        List<string> mediaInformation = new List<string>();
        int PreviewDelay = 0;
        string MktVideoPath = string.Empty;
        string strRotation = "None";
        string MonitorId = "0";
        string RestartAfter = string.Empty;
        string ExcludeFiles = string.Empty;
        List<String> lstMktVideos = new List<string>();
        public DispatcherTimer timerAddInList;
        int PeriodInterval = 1;
        bool IsMktVideosActive = false;
        DateTime lastDt = DateTime.Now;
        double rotateAngle = 0;
        MPlaylistClass m_objPlaylist = new MPlaylistClass();
        MPlaylistClass m_guestVideoPlayList = new MPlaylistClass();
        int fileCount = 0;
        int currentFileIndex = 0;
        #endregion

        #region Constructor
        public MainWindow()
        {
            ChromaKeypluginLic.IntializeProtection();
            DecoderlibLic.IntializeProtection();
            EncoderlibLic.IntializeProtection();
            MComposerlibLic.IntializeProtection();
            MPlatformSDKLic.IntializeProtection();
            InitializeComponent();
            Loadcombo();
            xmlFilePathMaster = System.IO.Path.Combine(Environment.CurrentDirectory, "Display.xml");
            if (File.Exists(xmlFilePathMaster) && LoadInitialSettings())
            {
                btnSave_Click(null, null);
            }
            else
            {
                grdSettings.Visibility = Visibility.Visible;
            }
            mFormatControl.SetControlledObject(m_objPlaylist);
            mFormatControl.SetControlledObject(m_guestVideoPlayList);
            if (mFormatControl.comboBoxVideo.Items.Count > 0)
                mFormatControl.comboBoxVideo.SelectedIndex = 12;
            if (mFormatControl.comboBoxAudio.Items.Count > 0)
                mFormatControl.comboBoxAudio.SelectedIndex = 8;

        }
        #endregion

        private bool LoadInitialSettings()
        {
            bool retVal = true;
            try
            {
                if (File.Exists(xmlFilePathMaster))
                {
                    XmlDataDocument xmldoc = new XmlDataDocument();
                    xmldoc.Load(xmlFilePathMaster);
                    VideoPath = xmldoc.GetElementsByTagName("VideoPath")[0].ChildNodes.Item(0).InnerText.Trim();
                    txtVideoPath.Text = VideoPath;

                    DirectoryInfo dr = new DirectoryInfo(VideoPath);
                    String[] Name = dr.FullName.Split('\\');
                    xmlFilePath = System.IO.Path.Combine(dr.FullName, Name[Name.Length - 1] + ".xml");

                    int PreviewDelay = Convert.ToInt32(xmldoc.GetElementsByTagName("PreviewDelay")[0].ChildNodes.Item(0).InnerText.Trim());
                    txtPreviewDelay.Text = PreviewDelay.ToString();
                    string MarketingVideoPath = xmldoc.GetElementsByTagName("MarketingVideoPath")[0].ChildNodes.Item(0).InnerText.Trim();
                    txtMktVideoPath.Text = MarketingVideoPath;
                    strRotation = xmldoc.GetElementsByTagName("RotationAngle")[0].ChildNodes.Item(0).InnerText.Trim();
                    if (!String.IsNullOrEmpty(strRotation))
                        getRotationSettings(strRotation);
                    else
                        getRotationSettings("None");
                    MonitorId = xmldoc.GetElementsByTagName("MonitorId")[0].ChildNodes.Item(0).InnerText.Trim();
                    cmbSelectmonitor.SelectedIndex = Convert.ToInt32(MonitorId);
                }
                else
                {
                    txtMktVideoPath.Text = string.Empty;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                retVal = false;
            }
            return retVal;
        }
        #region MediaPlayer
        public List<string> vsMediaFileName = new List<string>();

        #endregion

        private void btnBrowse_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                FolderBrowserDialog fbd = new FolderBrowserDialog();
                System.Windows.Forms.DialogResult result = fbd.ShowDialog();
                if (result == System.Windows.Forms.DialogResult.OK)
                {
                    txtVideoPath.Text = fbd.SelectedPath.ToString();
                    DirectoryInfo dr = new DirectoryInfo(fbd.SelectedPath.ToString());
                    String[] Name = dr.FullName.Split('\\');
                    xmlFileName = Name[Name.Length - 1] + ".xml";
                    xmlFilePathMaster = System.IO.Path.Combine(Environment.CurrentDirectory, "Display.xml"); //System.IO.Path.Combine(dr.FullName, xmlFileName);
                    xmlFilePath = System.IO.Path.Combine(dr.FullName, xmlFileName);
                    if (File.Exists(xmlFilePath))
                    {
                        XmlDataDocument xmldoc = new XmlDataDocument();
                        xmldoc.Load(xmlFilePath);
                        string VideoPath = xmldoc.GetElementsByTagName("VideoPath")[0].ChildNodes.Item(0).InnerText.Trim();
                        int PreviewDelay = Convert.ToInt32(xmldoc.GetElementsByTagName("PreviewDelay")[0].ChildNodes.Item(0).InnerText.Trim());
                        txtPreviewDelay.Text = PreviewDelay.ToString();
                        string MarketingVideoPath = xmldoc.GetElementsByTagName("MarketingVideoPath")[0].ChildNodes.Item(0).InnerText.Trim();
                        txtMktVideoPath.Text = MarketingVideoPath;
                        strRotation = xmldoc.GetElementsByTagName("RotationAngle")[0].ChildNodes.Item(0).InnerText.Trim();
                        if (!String.IsNullOrEmpty(strRotation))
                            getRotationSettings(strRotation);
                        else
                            getRotationSettings("None");

                        // RestartAfter = xmldoc.GetElementsByTagName("RestartAfter")[0].ChildNodes.Item(0).InnerText.Trim();
                        //txtRestartAfter.Text = RestartAfter;

                        ExcludeFiles = xmldoc.GetElementsByTagName("ExcludeFiles")[0].ChildNodes.Item(0).InnerText.Trim();
                        //txtExcludeFiles.Text = ExcludeFiles;
                        cmbSelectmonitor.Text = xmldoc.GetElementsByTagName("MonitorName")[0].ChildNodes.Item(0).InnerText.Trim();
                        cmbSelectmonitor.SelectedValue = xmldoc.GetElementsByTagName("MonitorId")[0].ChildNodes.Item(0).InnerText.Trim();

                    }
                    else
                    {
                        txtMktVideoPath.Text = System.IO.Path.Combine(fbd.SelectedPath, "Marketing Videos");
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }
        private void btnBrowseMkt_Click(object sender, RoutedEventArgs e)
        {
            FolderBrowserDialog fbd = new FolderBrowserDialog();
            System.Windows.Forms.DialogResult result = fbd.ShowDialog();
            if (result == System.Windows.Forms.DialogResult.OK)
            {
                txtMktVideoPath.Text = fbd.SelectedPath.ToString();
            }
        }
        private void btnReset_Click(object sender, RoutedEventArgs e)
        {
            CloseExit();
        }

        private void CloseExit()
        {
            try
            {
                timerAddInList.Stop();
                m_objPlaylist.FilePlayStop(0);
                m_objPlaylist.ObjectClose();
                m_guestVideoPlayList.FilePlayStop(0);
                m_guestVideoPlayList.ObjectClose();
                Process thisProc = Process.GetCurrentProcess();
                // Check how many total processes have the same name as the current one
                if (Process.GetProcessesByName(thisProc.ProcessName).Length >= 1)
                {
                    System.Windows.Application.Current.Shutdown();
                    thisProc.Kill();
                }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }

        }
        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(txtVideoPath.Text))
                {
                    System.Windows.MessageBox.Show("Please select video path.", "Video Display Utility", MessageBoxButton.OK, MessageBoxImage.Information);
                    return;
                }
                if (string.IsNullOrWhiteSpace(txtPreviewDelay.Text))
                {
                    System.Windows.MessageBox.Show("Please enter preview delay in seconds.", "Video Display Utility", MessageBoxButton.OK, MessageBoxImage.Information);
                    return;
                }
                if (string.IsNullOrWhiteSpace(txtMktVideoPath.Text))
                {
                    System.Windows.MessageBox.Show("Please select marketing video path.", "Video Display Utility", MessageBoxButton.OK, MessageBoxImage.Information);
                    return;
                }

                if (File.Exists(xmlFilePath))
                {
                    File.Delete(xmlFilePath);
                }

                XmlTextWriter writer = new XmlTextWriter(xmlFilePath, System.Text.Encoding.UTF8);
                writer.WriteStartDocument(true);
                writer.Formatting = Formatting.Indented;
                writer.Indentation = 2;
                writer.WriteStartElement("Settings");
                writer.WriteStartElement("VideoPath");
                writer.WriteString(txtVideoPath.Text);
                writer.WriteEndElement();
                VideoPath = txtVideoPath.Text;
                writer.WriteStartElement("PreviewDelay");
                writer.WriteString(txtPreviewDelay.Text);
                writer.WriteEndElement();
                PreviewDelay = Convert.ToInt32(txtPreviewDelay.Text);
                writer.WriteStartElement("MarketingVideoPath");
                writer.WriteString(txtMktVideoPath.Text);
                writer.WriteEndElement();
                MktVideoPath = txtMktVideoPath.Text;
                writer.WriteStartElement("RotationAngle");
                writer.WriteString(setRotationSettings());
                writer.WriteEndElement();
                strRotation = setRotationSettings();
                writer.WriteStartElement("MonitorId");
                writer.WriteString(Convert.ToString(cmbSelectmonitor.SelectedIndex));
                MonitorId = Convert.ToString(cmbSelectmonitor.SelectedIndex);
                writer.WriteEndElement();
                writer.WriteStartElement("MonitorName");
                writer.WriteString(((System.Collections.Generic.KeyValuePair<string, int>)(cmbSelectmonitor.SelectedItem)).Key.ToString());
                writer.WriteEndElement();
                writer.WriteEndElement();
                writer.WriteEndDocument();
                writer.Close();
                File.Copy(xmlFilePath, xmlFilePathMaster, true);
                if (sender != null)
                    System.Windows.MessageBox.Show("Video display utility settings saved successfully", "Video Display Utility", MessageBoxButton.OK, MessageBoxImage.Information);
                //   grdSettings.Visibility = Visibility.Hidden;
                //if (strRotation.Equals("Clockwise"))
                // //   RotateClock();
                //else if (strRotation.Equals("Anticlockwise"))
                //    RotateAnticlock();
                timerAddInList = new DispatcherTimer();
                //Period Interval in Seconds
                timerAddInList.Interval = TimeSpan.FromSeconds(PeriodInterval);// tsTimer1;
                timerAddInList.Tick += new EventHandler(timerAddInList_Tick);
                timerAddInList.IsEnabled = true;
                System.Threading.Thread.Sleep(500);
                //MarketingImages();
                MarketingVideos();
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }
        private string setRotationSettings()
        {
            string retRotation = "None";

            if (rdbNone.IsChecked == true)
                retRotation = "None";
            else if (rdbClock.IsChecked == true)
                retRotation = "Clockwise";
            else if (rdbAntiClock.IsChecked == true)
                retRotation = "Anticlockwise";

            return retRotation;
        }
        private void getRotationSettings(string rotate)
        {
            if (rotate.Equals("None"))
                rdbNone.IsChecked = true;
            else if (rotate.Equals("Clockwise"))
                rdbClock.IsChecked = true;
            else if (rotate.Equals("Anticlockwise"))
                rdbAntiClock.IsChecked = true;

        }
        MPLATFORMLib.eMState eMState;
        List<String> listToPlay = new List<string>();
        void timerAddInList_Tick(object sender, EventArgs e)
        {
            try
            {
                listToPlay = GetVideosFromFolder();
                if (listToPlay != null && listToPlay.Count > 0)
                {
                    foreach (var item in listToPlay)
                    {
                        if (!vsMediaFileName.Contains(item))
                        {
                            vsMediaFileName.Add(item);
                            AddGuestVideo(item);
                            IsMktVideosActive = false;
                            mktCount = 1;
                        }
                    }
                    PlayGuestVideo(vsMediaFileName);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }

        }

        private List<String> GetVideosFromFolder()
        {
            DirectoryInfo Dir = new DirectoryInfo(VideoPath);
            FileInfo[] FileList = Dir.GetFiles("*.mp4");
            List<String> lstFileNames = new List<string>();
            DateTime currentDt = DateTime.Now.AddSeconds(-PreviewDelay);
            lstFileNames = FileList.Where(file => file.CreationTime <= currentDt && file.CreationTime > lastDt).OrderBy(file => file.CreationTime).Select(p => p.FullName).ToList();
            lastDt = currentDt;
            return lstFileNames;
        }
        private void MarketingVideos()
        {
            try
            {
                DirectoryInfo Dir = new DirectoryInfo(MktVideoPath);
                FileInfo[] FileList = Dir.GetFiles("*.mp4");
                lstMktVideos = FileList.OrderByDescending(file => file.CreationTime).Select(p => p.FullName).ToList();
                MItem pPlaylist;
                m_guestVideoPlayList.PlaylistBackgroundSet(null, lstMktVideos[0].ToString(), "playlist", out pPlaylist);

                for (int i = 1; i < lstMktVideos.Count; i++)
                {
                    MItem pFile2;
                    int nIndex = -1;
                    ((IMPlaylist)pPlaylist).PlaylistAdd(null, lstMktVideos[i].ToString(), "", ref nIndex, out pFile2);
                }

                if (gdMediaPlayer.Visibility == Visibility.Hidden)
                {
                    gdMediaPlayer.Visibility = Visibility.Visible;
                }
                mPreviewControl1.SetControlledObject(m_guestVideoPlayList);
                previewThird.PreviewWindowSet("", mPreviewControl1.panelPreview.Handle.ToInt32());

                //winMPreviewControl.Visibility = Visibility.Collapsed;
                winMPreviewControl1.Visibility = Visibility.Visible;
                m_guestVideoPlayList.ObjectStart(null);
                ((IMObject)previewThird).ObjectStart(m_guestVideoPlayList);
                mFormatControl.SetControlledObject(previewThird);
                m_guestVideoPlayList.FilePlayStart();
                previewThird.PreviewEnable("", 1, 1);
                System.Threading.Thread.Sleep(1000);
                previewThird.PreviewFullScreen("", 1, Convert.ToInt32(MonitorId));

            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }
        private void btnRotateClock_Click(object sender, RoutedEventArgs e)
        {
            RotateClock();
        }
        private void RotateClock()
        {
            try
            {
                //RotateTransform rt = new RotateTransform();
                //rt.Angle = rotateAngle + 90;
                //rotateAngle = rt.Angle;
                //grdPreview.LayoutTransform = rt;
            }
            catch (Exception ex)
            {

            }
        }
        private void btnRotateAntiClock_Click(object sender, RoutedEventArgs e)
        {
            RotateAnticlock();
        }
        private void RotateAnticlock()
        {

        }
        private void btnCloseVideo_Click(object sender, RoutedEventArgs e)
        {
            System.Windows.Application.Current.Shutdown();
        }
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {

        }
        private void NumberValidationTextBox(object sender, TextCompositionEventArgs e)
        {
            System.Windows.Controls.TextBox txb = (System.Windows.Controls.TextBox)sender;
            string rex = "[^0-9]+";
            Regex regex = new Regex(rex);
            e.Handled = regex.IsMatch(e.Text);
            if (txb.Text == "" && e.Text == "0")
                e.Handled = true;
        }

        MPreview previewSecondary = new MPreviewClass();
        int mktCount = 0;
        bool MT = false;
        bool GT = false;

        int Filecount = 0;
        MPreview previewThird = new MPreviewClass();
        void PlayGuestVideo(List<string> vsMediaFileName)
        {
            m_guestVideoPlayList.OnEvent += MFileClass_OnEvent;
            double totalDuration = 0;
            m_objPlaylist.PlaylistPosSet(-1, 0, 0);
            m_guestVideoPlayList.PlaylistGetCount(out Filecount, out totalDuration);
            m_guestVideoPlayList.FilePlayStart();
            System.Threading.Thread.Sleep(1000);
        }

        int playCount = 0;
        private void MFileClass_OnEvent(string bsChannelID, string bsEventName, string bsEventParam, object pEventObject)
        {
            if ((bsEventName == "EOF" || bsEventName == "EOL") && (bsChannelID != "MPlaylist"))
            {
                if (vsMediaFileName.Contains(bsChannelID))
                {
                    double totalDuration = 0;
                    m_guestVideoPlayList.PlaylistGetCount(out Filecount, out totalDuration);
                    m_guestVideoPlayList.OnEvent -= MFileClass_OnEvent;
                    playCount = 0;
                    MItem myItem;
                    string strPath;
                    double dblFilePos;

                    for (int i = 0; i < Filecount; i++)
                    {
                        m_guestVideoPlayList.PlaylistGetByIndex(i, out dblFilePos, out strPath, out myItem);
                        if (strPath == bsChannelID)
                        {
                            m_guestVideoPlayList.PlaylistRemove(myItem);
                            vsMediaFileName.Remove(bsChannelID);
                            break;
                        }
                    }

                    m_guestVideoPlayList.FilePlayStop(0);
                    if (vsMediaFileName.Count > 0)
                    {
                        PlayGuestVideo(vsMediaFileName);
                    }
                    try
                    {
                        if (File.Exists(bsChannelID))
                            File.Delete(bsChannelID);
                    }
                    catch (Exception ex)
                    {
                        string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                        ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                        vsMediaFileName.Clear();
                    }
                }

            }
            Marshal.ReleaseComObject(pEventObject);
        }
        private void AddGuestVideo(string item)
        {
            double totalDuration = 0;
            m_guestVideoPlayList.PlaylistGetCount(out Filecount, out totalDuration);
            MItem pFile; int nIndex = 0;
            m_guestVideoPlayList.PlaylistAdd(null, item, null, ref nIndex, out pFile);
            m_guestVideoPlayList.PropsSet("loop", "false");
            m_guestVideoPlayList.PropsSet("active_frc", "false");
            m_guestVideoPlayList.PropsSet("preview.drop_frames", "true");

            //if (strRotation == "Clockwise")
            //{
            //    ((IMProps)pFile).PropsSet("object::rotate", "right");
            //    m_guestVideoPlayList.FileRateSet(2.0);
            //}
            //else if (strRotation == "Anticlockwise")
            //{
            //    ((IMProps)pFile).PropsSet("object::rotate", "left");
            //    m_guestVideoPlayList.FileRateSet(2.0);
            //}
            //else
            //{
            //    m_guestVideoPlayList.FileRateSet(1.0);
            //}
        }
        public void Loadcombo()
        {
            Dictionary<string, int> monitors = new Dictionary<string, int>();
            foreach (Screen s in Screen.AllScreens)
            {
                string DeviceName = string.Empty;
                int i = 0;
                DeviceName = Regex.Replace(s.DeviceName, @"[^0-9a-zA-Z]+", "");
                monitors.Add(DeviceName, i);
                i++;
            }
            cmbSelectmonitor.ItemsSource = monitors;
        }
        private void Window_Closed(object sender, EventArgs e)
        {
            CloseExit();
        }
    }
}
