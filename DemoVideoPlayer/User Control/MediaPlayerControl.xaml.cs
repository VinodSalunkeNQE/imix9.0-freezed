﻿//using FrameworkHelper;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using MControls;
using MPLATFORMLib;

namespace DemoVideoPlayer
{
    /// <summary>
    /// Interaction logic for MediaPlayerControl.xaml
    /// </summary>
    public partial class MediaPlayerControl : UserControl
    {

        //private readonly DispatcherTimer timer = new DispatcherTimer();
        List<string> vsMediaFileName = new List<string>();
        bool isMuted = false;

        public MediaPlayerControl(List<string> fileName, string type)
        {
            InitializeComponent();
            if (fileName == null || fileName.Count <= 0)
                return;
            vsMediaFileName.Clear();
            vsMediaFileName = fileName;
            AddMediaPlayer();
        }
        string replayFilePath = string.Empty;
        string fileJustPlayed = string.Empty;
        string vf_LicenseKey = string.Empty;
        string vf_UserName = string.Empty;
        string vf_Email = string.Empty;
       
        public MediaPlayerControl()
        {
            InitializeComponent();
        }
        int fileCount = 0;
        int currentFileIndex = 0;
        public void AddMediaPlayer()
        {
            try
            {
                fileCount = vsMediaFileName.Count;
                PlayFile(vsMediaFileName[0]);
                currentFileIndex = 0;

               
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }
      
       
       
      

       
        #region Commented Code
        private void StopDelegateMethod()
        {
            // tbTimeline.Value = 0;
            //tbTimeline.IsEnabled = false;
            //timer.Stop();
            //lbTimeMax.Content = "00:00:00";
            //lbTimeCurrent.Content = "00:00:00";
        }
        //private void InitTimer()
        //{
        //    timer.Interval = TimeSpan.FromMilliseconds(500);
        //    timer.Tick += new EventHandler(
        //        delegate(object s, EventArgs a)
        //        {
        //            timer_Tick();
        //        });
        //}
        //void timer_Tick()
        //{
        //    try
        //    {
        //        timer.Tag = 1;
        //        string extension = System.IO.Path.GetExtension(vsMediaFileName[0]).ToLower();
        //        if (extension.Equals(".flv"))
        //        {
        //            FlvMetaInfo metaInfo = FlvMetaDataReader.GetFlvMetaInfo(vsMediaFileName[0]);
        //            tbTimeline.Maximum = Convert.ToInt64(metaInfo.Duration);
        //        }
        //        else
        //        {
        //            tbTimeline.Maximum = (int)(VisioMediaPlayer.Duration_Time() / 1000.0);
        //        }
        //        if (tbTimeline.Maximum == 0)
        //        {
        //            var player = new WindowsMediaPlayer();
        //            var clip = player.newMedia(vsMediaFileName[0]);
        //            tbTimeline.Maximum = string.IsNullOrEmpty(TimeSpan.FromSeconds(clip.duration).ToString()) ? 0 : (long)(TimeSpan.FromSeconds(clip.duration).TotalSeconds);
        //        }
        //        int value = (Convert.ToInt32(VisioMediaPlayer.Position_Get_Time() / 1000.0));
        //        if ((value > 0) && (value <= tbTimeline.Maximum))
        //        {
        //            tbTimeline.Value = value;
        //        }
        //        lbTimeMax.Content = MediaPlayer.Helpful_SecondsToTimeFormatted((int)tbTimeline.Maximum);
        //        lbTimeCurrent.Content = MediaPlayer.Helpful_SecondsToTimeFormatted((int)tbTimeline.Value);
        //        timer.Tag = 0;
        //    }
        //    catch (Exception ex)
        //    {
        //        //string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
        //        //ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
        //    }
        //}
        //private void btMute_Click(object sender, RoutedEventArgs e)
        //{
        //    try
        //    {
        //        if (!isMuted)
        //        {
        //            imgMute.Source = new BitmapImage(new Uri(@"/images/mute-on.png", UriKind.Relative));
        //            btMute.ToolTip = "Unmute";
        //            isMuted = true;
        //            VisioMediaPlayer.Audio_Volume_Set(0, 0);
        //        }
        //        else
        //        {
        //            imgMute.Source = new BitmapImage(new Uri(@"/images/mute.png", UriKind.Relative));
        //            btMute.ToolTip = "Mute";
        //            isMuted = false;
        //            VisioMediaPlayer.Audio_Volume_Set(0, (int)tbVolume.Value);
        //            //lbVolPercent.Content = tbVolume.Value.ToString() + "%";
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //    }
        //}
        //private void tbVolume_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        //{

        //    if ((int)tbVolume.Value > 0)
        //    {
        //        isMuted = false;
        //        imgMute.Source = new BitmapImage(new Uri(@"/images/mute.png", UriKind.Relative));
        //        btMute.ToolTip = "Mute";
        //    }
        //    VisioMediaPlayer.Audio_Volume_Set(0, (int)tbVolume.Value);
        //    lbVolPercent.Content = Convert.ToInt32(tbVolume.Value).ToString() + "%";
        //}
        //private void btStop_Click(object sender, RoutedEventArgs e)
        //{
        //    VisioMediaPlayer.Stop();
        //    timer.Stop();
        //    tbTimeline.Value = 0;
        //    lbTimeMax.Content = "00:00:00";
        //    lbTimeCurrent.Content = "00:00:00";
        //}
        //private void btStart_Click(object sender, RoutedEventArgs e)
        //{
        //    if (isReplayFile)
        //    {
        //        VisioMediaPlayer.Stop();
        //        VisioMediaPlayer.FilenamesOrURL.Clear();
        //        timer.Stop();
        //        tbTimeline.Value = 0;
        //        lbTimeMax.Content = "00:00:00";
        //        lbTimeCurrent.Content = "00:00:00";
        //    }
        //    AddMediaPlayer();
        //}
        //private void tbTimeline_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        //{
        //    try
        //    {
        //        if (Convert.ToInt32(timer.Tag) == 0)
        //        {

        //            if (VisioMediaPlayer.Status == VFMediaPlayerStatus.Pause)
        //            {
        //                timer.Stop();
        //                VisioMediaPlayer.Position_Set_Time((Convert.ToInt32(tbTimeline.Value) * 1000));
        //            }
        //            else
        //            {
        //                timer.Stop();
        //                //VisioMediaPlayer.Pause();
        //                VisioMediaPlayer.Position_Set_Time((Convert.ToInt32(tbTimeline.Value) * 1000));
        //                //VisioMediaPlayer.Play();
        //                timer.Start();
        //            }
        //            //MemoryManagement.FlushMemory();
        //        }

        //    }
        //    catch (Exception ex)
        //    {
        //        //string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
        //        //ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
        //    }
        //}
        //private void btReplay_Click(object sender, RoutedEventArgs e)
        //{
        //    VisioMediaPlayer.Stop();
        //    VisioMediaPlayer.FilenamesOrURL.Clear();
        //    timer.Stop();
        //    tbTimeline.Value = 0;
        //    lbTimeMax.Content = "00:00:00";
        //    lbTimeCurrent.Content = "00:00:00";
        //    VisioMediaPlayer.FilenamesOrURL.Add(replayFilePath);
        //    VisioMediaPlayer.Play();
        //    if (!isMuted)
        //    {
        //        VisioMediaPlayer.Audio_Volume_Set(0, (int)tbVolume.Value);
        //        //lbVolPercent.Content = tbVolume.Value.ToString() + "%";
        //    }
        //    else
        //        VisioMediaPlayer.Audio_Volume_Set(0, 0);
        //    timer.Start();
        //}
        //private void btPause_Click(object sender, RoutedEventArgs e)
        //{
        //    VisioMediaPlayer.Pause();
        //}
        #endregion Commented Code
       
        private void hoverRectangle_MouseEnter(object sender, MouseEventArgs e)
        {
            //if (lowerGrid.Visibility == Visibility.Collapsed)
            //{
            //    lowerGrid.Visibility = Visibility.Visible;
            //}
        }
        private void Grid_MouseDown(object sender, MouseButtonEventArgs e)
        {
            //if (lowerGrid.Visibility == Visibility.Visible)
            //    lowerGrid.Visibility = Visibility.Collapsed;
        }
        public MFileClass mFile = new MFileClass();
        private void PlayFile(string fileName)
        {
            mFile = new MFileClass();
            mFile.FileNameSet(fileName, "");
            mFile.FilePlayStart();
       

            mPreviewControl.SetControlledObject(mFile);
            mPreviewControl.m_pPreview.PreviewWindowSet("", mPreviewControl.panelPreview.Handle.ToInt32());
            mPreviewControl.m_pPreview.PreviewEnable("", 1, 1);
         
        }

        private void MFileClass_OnEvent(string bsChannelID, string bsEventName, string bsEventParam, object pEventObject)
        {
            if (bsEventName == "EOF")
            {
                
                if (currentFileIndex + 1 < fileCount)
                {
                    if (vsMediaFileName.ElementAtOrDefault(currentFileIndex + 1) != null)
                    {
                        PlayFile(vsMediaFileName[currentFileIndex + 1]);
                        currentFileIndex++;
                    }
                }
                else
                {
                    mFile.ObjectClose();
                }
            }
        }

        private void UserControl_Unloaded(object sender, RoutedEventArgs e)
        {

        }
    }
}
