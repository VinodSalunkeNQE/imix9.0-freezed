﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DemoVideoPlayer
{
    public class FlvMetaInfo
    {
        private Double _duration;


        private bool _hasMetaData;

        public Double Duration
        {
            get { return _duration; }
            set { _duration = value; }
        }

        public bool HasMetaData
        {
            get { return _hasMetaData; }
            set { _hasMetaData = value; }
        }
        internal FlvMetaInfo(bool hasMetaData, Double duration)
        {
            _hasMetaData = hasMetaData;
            _duration = duration;

        }

    }

    public class FlvMetaDataReader
    {
        /// <summary>
        /// Reads the meta information (if present) in an FLV
        /// </summary>
        /// <param name="path">The path to the FLV file</returns>
        public static FlvMetaInfo GetFlvMetaInfo(string path)
        {
            if (!File.Exists(path))
            {
                throw new Exception(String.Format("File '{0}' doesn't exist for FlvMetaDataReader.GetFlvMetaInfo(path)", path));
            }
            bool hasMetaData = false;
            double duration = 0;

            DateTime creationDate = DateTime.MinValue;
            // open file 
            FileStream fileStream = new FileStream(path, FileMode.Open);
            try
            {
                // read where "onMetaData"
                byte[] bytes = new byte[10];
                fileStream.Seek(27, SeekOrigin.Begin);
                int result = fileStream.Read(bytes, 0, 10);
                // if "onMetaData" exists then proceed to read the attributes
                string onMetaData = ByteArrayToString(bytes);
                if (onMetaData == "onMetaData")
                {
                    hasMetaData = true;
                    // 16 bytes past "onMetaData" is the data for "duration" 
                    duration = GetNextDouble(fileStream, 16, 8);

                }
            }
            catch (Exception e)
            {
                // no error handling
            }
            finally
            {
                fileStream.Close();
            }
            return new FlvMetaInfo(hasMetaData, duration);
        }
        private static Double GetNextDouble(FileStream fileStream, int offset, int length)
        {
            // move the desired number of places in the array
            fileStream.Seek(offset, SeekOrigin.Current);
            // create byte array
            byte[] bytes = new byte[length];
            // read bytes
            int result = fileStream.Read(bytes, 0, length);
            // convert to double (all flass values are written in reverse order)
            return ByteArrayToDouble(bytes, true);
        }
        private static string ByteArrayToString(byte[] bytes)
        {
            string byteString = string.Empty;
            foreach (byte b in bytes)
            {
                byteString += Convert.ToChar(b).ToString();
            }
            return byteString;
        }
        private static Double ByteArrayToDouble(byte[] bytes, bool readInReverse)
        {
            if (bytes.Length != 8)
                throw new Exception("bytes must be exactly 8 in Length");
            if (readInReverse)
                Array.Reverse(bytes);
            return BitConverter.ToDouble(bytes, 0);
        }
    }
}
