﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Collections.ObjectModel;
using DigiPhoto.DataLayer;
using System.Text.RegularExpressions;
using System.IO;
using System.Runtime.InteropServices;
using System.ComponentModel;
using System.Runtime.InteropServices.ComTypes;
using DigiPhoto.IMIX.Business;
using FrameworkHelper;
using DigiPhoto.IMIX.Model;
using System.Security.Cryptography;
using System.Configuration;
using System.Timers;
using System.ServiceProcess;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Management;
using DigiPhoto.Common;


namespace PreSoldService
{
    public partial class PreSoldService : ServiceBase
    {
        static System.Timers.Timer timer;
        static int _timerMiliSeconds = 6000;



        public bool _issemiorder;
        //TextBox controlon;
        //private List<DG_Currency> _lstCurrency;
        private List<CurrencyInfo> _lstCurrency;
        private double _totalBillAmount;
        private string _billDiscountDetails;
        private int _defaultCurrencyId;
        string orderNumber = string.Empty;

        #region Properties
        public String DiscountDetails
        {
            get
            {
                return _billDiscountDetails;
            }
            set
            {
                _billDiscountDetails = value;
            }
        }
        #endregion


        private static volatile object _object = new object();

        public PreSoldService()
        {

            InitializeComponent();
            // ServicePosInfoBusiness svcPosinfoBusiness = new ServicePosInfoBusiness();
            // svcPosinfoBusiness.ServiceStart();

        }

        protected override void OnStart(string[] args)
        {
            try
            {

                ServicePosInfoBusiness svcPosinfoBusiness = new ServicePosInfoBusiness();
                string ret = svcPosinfoBusiness.ServiceStart(false);
                //ret = 1;
                if (string.IsNullOrEmpty(ret))
                {
                    StartScheduler();
                }
                else
                {
                    throw new Exception("Already Started");

                }
            }
            catch (Exception ex)
            {
                ExitCode = 13816;
                this.Stop();
            }
        }



        private void timer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            try
            {
                timer.Stop();
                timer.Enabled = false;
                //Worker Process

                ProcessOnlineOrder();
                timer.Start();
                timer.Enabled = true;

            }
            catch (Exception ex)
            {
                //string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                //ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                timer.Start();
            }
            finally
            {
                timer.Start();
            }
        }


        protected override void OnStop()
        {
            ServicePosInfoBusiness svcPosinfoBusiness = new ServicePosInfoBusiness();
            svcPosinfoBusiness.StopService(false);
        }




        private void StartScheduler()
        {
            try
            {

                timer = new System.Timers.Timer();
                timer.Interval = _timerMiliSeconds;
                timer.AutoReset = true;
                timer.Elapsed += new System.Timers.ElapsedEventHandler(this.timer_Elapsed);
                timer.Start();
                timer.Enabled = true;
            }
            catch (Exception ex)
            {
                //string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                //ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
        }


        //get the list after executing usp_SEL_AutoOnlineOrder
        public void ProcessOnlineOrder()
        {
            try
            {
                if ((new PreSoldAutoOnlineOrderBusiness()).chKIsAutoPurchaseActiveOrNot(1))
                {
                    PreSoldAutoOnlineOrderBusiness objB = new PreSoldAutoOnlineOrderBusiness();
                    List<PreSoldAutoOnlineOrderInfo> obj = objB.getAutoOnlineOrderDetails();
                    if (obj.Count() > 0)
                    {
                        var UniquCode = obj
                                    .GroupBy(g => new
                                    {
                                        g.CardUniqueIdentifier,
                                        g.IsWaterMarked
                                    })
                                    .Select(group => new
                                    {
                                        CardUniqueIdentifier = group.Key.CardUniqueIdentifier,
                                        IsWaterMarked = group.Key.IsWaterMarked
                                    });
                        foreach (var item in UniquCode)
                        {
                            PreSoldAutoOnlineOrderInfo OnlineOrderDetails = obj.Where(cond => cond.CardUniqueIdentifier == item.CardUniqueIdentifier).ToList().FirstOrDefault();
                            string photoIDs = string.Join(",", obj.Where(x => x.CardUniqueIdentifier == item.CardUniqueIdentifier).ToList().Select(i => i.PhotoId.ToString()).ToArray().Take(OnlineOrderDetails.MaxImages));
                            string IMIXImageAssociationIds = string.Join(",", obj.Where(x => x.CardUniqueIdentifier == item.CardUniqueIdentifier).ToList().Select(i => i.IMIXImageAssociationId.ToString()).ToArray().Take(OnlineOrderDetails.MaxImages));
                            SaveOrder(OnlineOrderDetails, photoIDs, IMIXImageAssociationIds, item.IsWaterMarked);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogFileWrite(ex.Message);
            }
        }

        private bool SaveOrder(PreSoldAutoOnlineOrderInfo OnlineOrderDetails, string photoIDs, string IMIXImageAssociationIds, bool IsWaterMarked)
        {
            try
            {

                //Enhancement .....
                int subStoreId = 0;
                bool result = (new PreSoldAutoOnlineOrderBusiness()).getOrderStatus(OnlineOrderDetails, IMIXImageAssociationIds, photoIDs, IsWaterMarked);
                OrderBusiness objOrder = new OrderBusiness();
                List<QRcodes> QrCodes = new List<QRcodes>();
                StoreSubStoreDataBusniess storeObj = new StoreSubStoreDataBusniess();
                DigiPhoto.IMIX.Model.StoreInfo store = storeObj.GetStore();
                //Set current SubStoreId
                try
                {
                    string pathtosave = Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location);
                    if (File.Exists(pathtosave + "\\ss.dat"))
                    {
                        string line;
                        using (StreamReader reader = new StreamReader(pathtosave + "\\ss.dat"))
                        {
                            line = reader.ReadLine();
                            string subID = DigiPhoto.CryptorEngine.Decrypt(line, true);
                            if (subID.Contains(','))
                            {
                                subStoreId = Convert.ToInt32((subID.Split(',')[0]));
                            }
                        }
                    }
                    else
                    {
                        ErrorHandler.ErrorHandler.LogFileWrite("Please select substore from Configuration Section in Imix for this machine.");
                        subStoreId = 1;
                    }
                }
                catch (Exception ex)
                {
                    string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                    ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                }
                if (result == false)
                {
                    //enhancement end ......
                    #region variables

                   
                    List<BurnImagesInfo> burnedImages = new List<BurnImagesInfo>();
                    String paymentDetails = string.Empty;
                    int paymentMode = -1;
                    string paymentType = string.Empty;
                    double _totalAmount = 0;

                    double _totalBillDiscount = 0;

                    #endregion
                    _lstCurrency = (new CurrencyBusiness()).GetCurrencyOnly();
                    _defaultCurrencyId = (new CurrencyBusiness()).GetDefaultCurrency();
                    var CurrencySymbol = (from currlist in _lstCurrency
                                          where currlist.DG_Currency_pkey == _defaultCurrencyId
                                          select currlist).FirstOrDefault();

                 

                  

                    StoreInfo objStore = (new StoreSubStoreDataBusniess()).GetStore();

                    #region Condition for Card/Cash flow
                    paymentDetails = "<Payments>";

                    //Create transaction xml for Cash Amount
                    paymentMode = (Int32)DigiPhoto.DataLayer.PaymentMode.Cash;
                    paymentType = "CASH";
                    double Amount = OnlineOrderDetails.DG_Product_Pricing_ProductPrice;
                    double OrignalAmount = OnlineOrderDetails.DG_Product_Pricing_ProductPrice;
                    int MaxImages = OnlineOrderDetails.MaxImages;
                    int CurrencyID = _defaultCurrencyId;
                    string CurrencySyncCode = CurrencySymbol.SyncCode;
                    string currencyCode = CurrencySymbol.DG_Currency_Code;
                    _totalAmount = OnlineOrderDetails.DG_Product_Pricing_ProductPrice;
                    _totalBillAmount = OnlineOrderDetails.DG_Product_Pricing_ProductPrice;
                    Amount = Math.Round(ConvertToDefault(CurrencyID, Amount), 3);
                    paymentDetails += "<Payment Mode = 'cash' Amount = '" + Amount.ToString("N2") + "' OrignalAmount = '" + OrignalAmount.ToString() + "' MaxImages ='" + MaxImages.ToString() + "' CurrencyID = '" + CurrencyID.ToString() + "' CurrencyCode = '" + currencyCode + "' CurrencySyncCode = '" + CurrencySyncCode + "'/>";
                    paymentDetails += "</Payments>";

                    #endregion

                    orderNumber = GenerateOrderNumber();
                    //Save Order with details
                    string syncCode = CommonUtility.GetUniqueSynccode(Convert.ToInt32(ApplicationObjectEnum.Order).ToString().PadLeft(2, '0'), objStore.CountryCode, objStore.StoreCode, "14");
                    string newOrderNo = string.Empty;
                    OrderInfo orderInfo = (new OrderBusiness()).GenerateOrder(orderNumber, (decimal)_totalAmount, (decimal)_totalBillAmount, paymentDetails, paymentMode, _totalBillDiscount, DiscountDetails, 1, this._defaultCurrencyId, "0", syncCode, objStore.StoreCode); // 1 for user AP Admin user
                    orderNumber = orderInfo.DG_Orders_Number;
                    TaxBusiness buss = new TaxBusiness();
                    int orderId = orderInfo.DG_Orders_pkey;
                    //buss.SaveOrderTaxDetails(Convert.ToInt32(objStore.StoreCode), orderInfo.DG_Orders_pkey, 1);

                    int parentId = -1;
                    if (orderId > 0)
                    {
                        StringBuilder ItemDetail = new StringBuilder();
                        string images = string.Empty;
                        string photoIDsUnsold = null;
                        String BillLineItems = string.Empty;

                        images = string.Empty;
                        #region Condition If Order is a Package
                        if (IsWaterMarked == true)
                        {
                            photoIDsUnsold = photoIDs;
                            photoIDs = "";
                        }
                        if (subStoreId == 0)
                            subStoreId = 1;
                        string orderDetailsSyncCode = CommonUtility.GetUniqueSynccode(Convert.ToInt32(ApplicationObjectEnum.OrderDetails).ToString().PadLeft(2, '0'), objStore.CountryCode, objStore.StoreCode, 1.ToString());
                        parentId = (new OrderBusiness()).SaveOrderLineItems(OnlineOrderDetails.PackageId, orderId,
                    "", 1, "", 0, (decimal)_totalAmount, (decimal)_totalAmount, (decimal)_totalAmount, -1, subStoreId, 0, "", orderDetailsSyncCode, null,null);
                        int id = 0;

                        string OrderDetailsSyncCode1 = CommonUtility.GetUniqueSynccode(Convert.ToInt32(ApplicationObjectEnum.OrderDetails).ToString().PadLeft(2, '0'), objStore.CountryCode, objStore.StoreCode, "1");
                        id = (new OrderBusiness()).SaveOrderLineItems(84, orderId, photoIDs,
                            1, "", 0, 0, 0, 0, parentId, subStoreId, OnlineOrderDetails.ImageIdentificationType, OnlineOrderDetails.CardUniqueIdentifier,
                            OrderDetailsSyncCode1,null, photoIDsUnsold);

                        if (BillLineItems != string.Empty)
                            BillLineItems += "," + images;
                        else
                            BillLineItems = images;

                        images = string.Empty;

                        (new PreSoldAutoOnlineOrderBusiness()).UpdateOrderStatus(IMIXImageAssociationIds, IsWaterMarked);
                        #endregion
                        //////////////creted by latika fix issue multiple print qrcode text file////////////////////////////
                        if (photoIDs == "") { photoIDs= photoIDsUnsold; }
                        int status = objOrder.UpdOnlineQRCodeForPhotoPresold(OnlineOrderDetails.CardUniqueIdentifier, photoIDs);


                        CreateQROnlineOrderImgFIle(subStoreId, OnlineOrderDetails.CardUniqueIdentifier);
                        ////END
                        return true;
                    }


                }
                else
                {
                    //////////////creted by latika fix issue multiple print qrcode text file////////////////////////////

                    int status = objOrder.UpdOnlineQRCodeForPhotoPresold(OnlineOrderDetails.CardUniqueIdentifier, photoIDs);


                    CreateQROnlineOrderImgFIle(subStoreId, OnlineOrderDetails.CardUniqueIdentifier);
/////END
                }


            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                orderNumber = string.Empty;
                return false;
            }
            finally
            {
                DigiPhoto.MemoryManagement.FlushMemory();
                GC.RemoveMemoryPressure(20000);
            }
            orderNumber = string.Empty;
            return true;
        }

        private double ConvertToDefault(int currencyid, double ToBeConverted)
        {
            double convertedAmount = 0;

            var amount = (from rate in _lstCurrency
                          where rate.DG_Currency_pkey == currencyid
                          select rate).FirstOrDefault();

            if (amount != null)
                convertedAmount = (amount.DG_Currency_Rate * ToBeConverted);

            return Math.Round(convertedAmount, 3, MidpointRounding.ToEven);
        }

        private string GenerateOrderNumber()
        {
            string orderNumber = "DG-";// +"-";
            string uniqueNumber = string.Empty;
            try
            {
                using (RNGCryptoServiceProvider rng = new RNGCryptoServiceProvider())
                {
                    // Buffer storage.
                    byte[] data = new byte[4];

                    // Ten iterations.                    
                    {
                        // Fill buffer.
                        rng.GetBytes(data);

                        // Convert to int 32.
                        int value = BitConverter.ToInt32(data, 0);
                        if (value < 0)
                            value = -value;
                        if (value.ToString().Length > 10)
                        {
                            uniqueNumber = value.ToString().Substring(0, 10);
                        }
                        else
                        {
                            uniqueNumber = value.ToString();
                        }
                    }
                }
            }
            catch (Exception ex)
            { }
            orderNumber = orderNumber + uniqueNumber;
            return orderNumber;
        }

        /// <summary>
        /// ///CReated by latika for generating QRCode Notepad files 24-Apr-2019
        /// </summary>
        /// <param name="subStoreId"></param>
        /// <param name="QrCode"></param>

        private void CreateQROnlineOrderImgFIle(int subStoreId,string QrCode)
        {
            ConfigBusiness confObj = new ConfigBusiness();

            ConfigurationInfo _objdata = confObj.GetConfigurationData(subStoreId);
            if (_objdata != null)
            {
                LoginUser.DigiFolderBackGroundPath = _objdata.DG_BG_Path;
                LoginUser.DigiFolderFramePath = _objdata.DG_Frame_Path;
                LoginUser.DigiFolderPath = _objdata.DG_Hot_Folder_Path;
            }
                OrderBusiness orderBusiness = new OrderBusiness();
            List<OnlineorderImgDetail> GetOrderNumberandImgLst = orderBusiness.GetOrderNumberandImg(QrCode);

            string FilePath = LoginUser.DigiFolderPath;
            if (!File.Exists(System.IO.Path.Combine(FilePath, "QRCodeDetails")))
            {

                System.IO.Directory.CreateDirectory(System.IO.Path.Combine(FilePath, "QRCodeDetails"));
            }
            FilePath = System.IO.Path.Combine(FilePath, "QRCodeDetails");
            if (!File.Exists(System.IO.Path.Combine(FilePath, DateTime.Now.ToString("ddMMyyyy"))))
            {

                System.IO.Directory.CreateDirectory(System.IO.Path.Combine(FilePath, DateTime.Now.ToString("ddMMyyyy")));
            }
            FilePath = System.IO.Path.Combine(FilePath, DateTime.Now.ToString("ddMMyyyy"));

            FilePath = System.IO.Path.Combine(FilePath, QrCode + ".txt");

            //if (System.IO.File.Exists(FilePath))
            //{
            //    File.Delete(FilePath);
            //    Thread.Sleep(1000);
            //}
            using (System.IO.FileStream fs = new FileStream(FilePath, FileMode.Create, FileAccess.Write))
            {
                StreamWriter writer = new StreamWriter(fs);

                string[] orderNos = GetOrderNumberandImgLst.Select(qr => qr.OnlineOrderNumber).Distinct().ToArray();

                foreach (string OrderNo in orderNos)
                {
                    writer.WriteLine(Environment.NewLine);
                    writer.WriteLine("-------------------------------------------");
                    writer.WriteLine(OrderNo);
                    writer.WriteLine("-------------------------------------------");

                    foreach (OnlineorderImgDetail item in GetOrderNumberandImgLst.Where(or => or.OnlineOrderNumber == OrderNo).ToList())
                    {
                        writer.WriteLine(item.OnlinePhotoID + "_" + item.OnlinePhotoName);
                    }
                    writer.WriteLine(Environment.NewLine);
                }

                writer.Flush();
                writer.Close();
            }
            return;


        }
        ////end
    }


}

