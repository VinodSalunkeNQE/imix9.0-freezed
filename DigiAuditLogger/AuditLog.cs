﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using DigiPhoto.DataLayer;
using DigiPhoto.DataLayer.Model;
using FrameworkHelper;
using DigiPhoto.Common;
using DigiPhoto.IMIX.Business;
using DigiPhoto.Utility.Repository.ValueType;
namespace DigiAuditLogger
{
    public class AuditLog
    {
        public static bool AddUserLog(int currentUser, int activityType, String description)
        {
            bool isAdded = false;
            try
            {
                string syncCode = CommonUtility.GetUniqueSynccode(Convert.ToInt32(ApplicationObjectEnum.Activity).ToString().PadLeft(2, '0'), LoginUser.countrycode, LoginUser.Storecode, "00");
                var objActivity = new ActivityBusiness();
                objActivity.RegisterLog(currentUser, activityType, description, syncCode);
                isAdded = true;
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
                isAdded = false;
            }
            return isAdded;
        }
        public static bool AddUserLog(int currentUser, int activityType, String description, int refId)
        {
            bool isAdded = false;
            try
            {
                string syncCode = CommonUtility.GetUniqueSynccode(Convert.ToInt32(ApplicationObjectEnum.Activity).ToString().PadLeft(2, '0'), LoginUser.countrycode, LoginUser.Storecode, "00");
                var objActivity = new ActivityBusiness();
                objActivity.RegisterLog(currentUser, activityType, description, syncCode, refId);
                isAdded = true;
            }
            catch (Exception ex)
            {
                ErrorHandler.ErrorHandler.LogError(ex);
                isAdded = false;
            }
            return isAdded;
        }
    }
}
