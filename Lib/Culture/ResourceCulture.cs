﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Reflection;
using System.Resources;
using System.Text;

namespace DigiPhoto.Culture
{
    public class ResourceCulture
    {
        public static ResourceManager GetCulture()
        {
            ResourceManager strCulture = null;
            string language = ConfigurationManager.AppSettings["Arabic"];
            if (language == "Arabic")
            {                
                strCulture = new ResourceManager("DigiPhoto.Culture.Resource-ar", Assembly.GetExecutingAssembly());
            }
            return strCulture;
        }
    }
}
