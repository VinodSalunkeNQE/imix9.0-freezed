﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using ErrorHandler;
using System.Configuration;
using System.Data.SqlClient;

namespace DigiSyncService
{
    public partial class Service1 : ServiceBase
    {
        #region Decalaration
        public string DGconn =ConfigurationManager.ConnectionStrings["DigiConnectionString"].ConnectionString;
        SqlConnection cn ;
        string servername;
        string username;
        string storename;
        string applicationid;
        string pass;
        decimal from;
        decimal to;
        private System.Timers.Timer timer;
        #endregion
        public Service1()
        {
            InitializeComponent();
           
        }

        /// <summary>
        /// When implemented in a derived class, executes when a Start command is sent to the service by the Service Control Manager (SCM) or when the operating system starts (for a service that starts automatically). Specifies actions to take when the service starts.
        /// </summary>
        /// <param name="args">Data passed by the start command.</param>
        protected override void OnStart(string[] args)
        {
            //System.Diagnostics.Debugger.Launch();
            //System.Diagnostics.Debugger.Break();
            try
            {
                cn = new SqlConnection(DGconn);
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
            this.timer = new System.Timers.Timer();
            this.timer.Interval = 1000 * 60 * 0.2;
            this.timer.AutoReset = true;
            this.timer.Elapsed += new System.Timers.ElapsedEventHandler(this.timer_Elapsed);
            this.timer.Start();

            
        }
        /// <summary>
        /// Handles the Elapsed event of the timer control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Timers.ElapsedEventArgs"/> instance containing the event data.</param>
        private void timer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {

            try
            {
                this.timer.Stop();
                if (GetStatus(DateTime.Now.Date, true))
                {
                    GetStoreData();
                    DateTime fromtime = System.DateTime.Now.Date.AddHours((double)from);
                    DateTime totime = System.DateTime.Now.Date.AddHours((double)to);
                    EventLog.WriteEntry(fromtime.ToString() + " --- " + totime.ToString());
                    if (DateTime.Now > fromtime && DateTime.Now < totime)
                    {
                        EventLog.WriteEntry("Date time is in range");
                        if (validData(servername, username, applicationid, pass, storename))
                        {
                            EventLog.WriteEntry("Store Exists on server");
                            bool status = RunDigiSyncSP(servername, username, applicationid, pass, storename);
                            if (status)
                            {
                                EventLog.WriteEntry(status.ToString());
                                RunSPStatus(DateTime.Now, status);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                this.timer.Start();
            }
            finally
            {
                this.timer.Start();
            }
        }
        /// <summary>
        /// Gets the status.
        /// </summary>
        /// <param name="todaydate">The todaydate.</param>
        /// <param name="currentstatus">if set to <c>true</c> [currentstatus].</param>
        /// <returns></returns>
        public bool GetStatus(DateTime todaydate, bool currentstatus)
        {
            try
            {

                SqlCommand cmd = new SqlCommand("GetSyncStatus", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@checkdate", todaydate);
                cmd.Parameters.AddWithValue("@checkstatus", currentstatus);
                cn.Open();
                if (cmd.ExecuteScalar() == null)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                return false;
            }
            finally
            {
                cn.Close();
            }
        }
        /// <summary>
        /// Runs the sp status.
        /// </summary>
        /// <param name="date">The date.</param>
        /// <param name="status">if set to <c>true</c> [status].</param>
        public void RunSPStatus(DateTime date, bool status)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("InsertSyncStatus", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@currentdate", date);
                cmd.Parameters.AddWithValue("@status", status);
                cn.Open();
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {

                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);

            }
            finally
            {
                cn.Close();
            }
        }
        /// <summary>
        /// Gets the store data.
        /// </summary>
        private void GetStoreData()
        {
            try
            {

                SqlCommand cmd = new SqlCommand("GetStoreData", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cn.Open();
                using (var reader = cmd.ExecuteReader())
                {
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            if (!reader.IsDBNull(0))
                            {
                                storename = reader.GetString(0);
                                servername = reader.GetString(1);
                                username = reader.GetString(2);
                                pass = reader.GetString(3);
                                applicationid = reader.GetValue(4).ToString();
                                from = reader.GetDecimal(5);
                                to = reader.GetDecimal(6);
                            }
                        }
                        reader.Close();
                    }  
                }
            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
            }
            finally
            {
                cn.Close();
            }
        }

        /// <summary>
        /// Runs the digi asynchronous sp.
        /// </summary>
        /// <param name="servername">The servername.</param>
        /// <param name="username">The username.</param>
        /// <param name="appid">The appid.</param>
        /// <param name="password">The password.</param>
        /// <param name="storename">The storename.</param>
        /// <returns></returns>
        private bool RunDigiSyncSP(string servername, string username, string appid, string password, string storename)
        {
            try
            {


                SqlCommand cmd = new SqlCommand("SetDataToCentralServer", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandTimeout = 6000;
                cmd.Parameters.AddWithValue("@servername", servername);
                cmd.Parameters.AddWithValue("@username", username);
                cmd.Parameters.AddWithValue("@ApplicationId", appid);
                cmd.Parameters.AddWithValue("@password", password);
                cmd.Parameters.AddWithValue("@StoreName", storename);
                cn.Open();
               var i= cmd.ExecuteScalar();
               if (i.ToString()== "1")
               {
                   return true;
               }
               else
               {
                   return false;
               }

            }
            catch (Exception ex)
            {
                string errorMessage = ErrorHandler.ErrorHandler.CreateErrorMessage(ex);
                EventLog.WriteEntry(errorMessage);
               
                ErrorHandler.ErrorHandler.LogFileWrite(errorMessage);
                return false;
                
            }
            finally
            {
                cn.Close();
            }
        }


        /// <summary>
        /// Valids the data.
        /// </summary>
        /// <param name="servername">The servername.</param>
        /// <param name="username">The username.</param>
        /// <param name="appid">The appid.</param>
        /// <param name="password">The password.</param>
        /// <param name="storename">The storename.</param>
        /// <returns></returns>
        public bool validData(string servername, string username, string appid, string password, string storename)
        {
            try
            {

                SqlCommand cmd = new SqlCommand("GetStoreDtaFromCentralServer", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandTimeout = 6000;
                cmd.Parameters.AddWithValue("@servername", servername);
                cmd.Parameters.AddWithValue("@username", username);
                cmd.Parameters.AddWithValue("@ApplicationId", appid);
                cmd.Parameters.AddWithValue("@password", password);
                cmd.Parameters.AddWithValue("@StoreName", storename);
                cn.Open();
                var i = cmd.ExecuteScalar();
                cn.Close();
                if (i.ToString() == "1")
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        /// <summary>
        /// When implemented in a derived class, executes when a Stop command is sent to the service by the Service Control Manager (SCM). Specifies actions to take when a service stops running.
        /// </summary>
        protected override void OnStop()
        {
            this.timer.Stop();
            this.timer = null;
        }
    }
}
