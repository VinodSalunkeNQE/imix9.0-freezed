﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Media.Effects;
using System.Windows.Media;
using System.ComponentModel;
using System.Windows;

namespace DigiConfigUtility.Shader
{

    public class ShaderBaseClass : ShaderEffect
    {
        #region Static Members
        public static readonly DependencyProperty InputProperty =
            ShaderEffect.RegisterPixelShaderSamplerProperty("Input", typeof(ShaderBaseClass), 0);
        #endregion

        public ShaderBaseClass(PixelShader shader)
        {
            PixelShader = shader;
            UpdateShaderValue(InputProperty);
        }

        [EditorBrowsable(EditorBrowsableState.Never)]
        public Brush Input
        {
            get { return (Brush)GetValue(InputProperty); }
            set { SetValue(InputProperty, value); }
        }
    }
}

