﻿using DigiConfigUtility.Utility;
using FrameworkHelper;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using DigiPhoto.Cache.DataCache;
using System.IO;
using System.Web.Script.Serialization;
using Newtonsoft.Json;
using System.Windows.Documents;
using Newtonsoft.Json.Linq;

namespace DigiConfigUtility
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        #region Added by Anis
        private List jsonAsList = new List();
        #endregion
        private void Application_Startup(object sender, StartupEventArgs e)
        {
            //DataCacheFactory.Register();
            try
            {
                #region Add Keys Dynamically by Anis on  1 Aug
                //READ THE CONFIG FILE.
                // string ConfigPath = @"C:\Program Files (x86)\iMix";
                try
                {
                    var directory = Environment.CurrentDirectory + "\\ConfiDetails\\ConfigDetailsJSON-DigiConfigUtility.json";
                    string jrf = System.IO.File.ReadAllText(directory);
                    JavaScriptSerializer serializer = new JavaScriptSerializer();
                    jsonAsList = serializer.Deserialize<List>(jrf.ToString());
                    var jsonObj = JsonConvert.DeserializeObject<JObject>(jrf).First.First;
                    System.Configuration.Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);

                    if (ConfigurationManager.AppSettings.AllKeys.Contains("GumballBGImgFolderName") == false)
                    {
                        string gumballBGImgFolderName = Convert.ToString(jsonObj["GumballBGImgFolderName"]);
                        config.AppSettings.Settings.Add("GumballBGImgFolderName", gumballBGImgFolderName);
                    }

                    config.Save(ConfigurationSaveMode.Full);
                    ConfigurationManager.RefreshSection("appSettings");
                }
                catch (Exception ex)
                {
                    LogConfigurator.log.Error("Error Message: 56 " + ex.Message + " Error Stack Trace: " + ex.StackTrace);
                    //  throw;
                }
             

                #endregion

                DataCacheFactory.Register();                
                // Get Reference to the current Process
                Process thisProc = Process.GetCurrentProcess();                
                // Check how many total processes have the same name as the current one
                if (Process.GetProcessesByName(thisProc.ProcessName).Length > 1)
                {                    
                    // If there is more than one, then it is already running.
                    MessageBox.Show("DigiConfigUtility is already running.");
                    Application.Current.Shutdown();
                    return;
                }                                              
                Taskbar.Show();                
                StartupUri = new System.Uri("Login.xaml", UriKind.Relative);
            }
            catch (Exception ex)
            {
                LogConfigurator.log.Error("Error Message: " + ex.Message + " Error Stack Trace: " + ex.StackTrace);
            }
        }

    }
}
