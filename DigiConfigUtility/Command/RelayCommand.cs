﻿using System;
using System.Diagnostics;
using System.Windows.Input;

namespace RelayCommandClass
{
    public class RelayCommand : ICommand
    {
        //#region Members
        //readonly Func<Boolean> _canExecute;
        //readonly Action _execute;
        //#endregion

        //#region Constructors
        //public RelayCommand(Action execute)
        //    : this(execute, null)
        //{
        //}

        //public RelayCommand(Action execute, Func<Boolean> canExecute)
        //{
        //    if (execute == null)
        //        throw new ArgumentNullException("execute");
        //    _execute = execute;
        //    _canExecute = canExecute;
        //}
        //#endregion

        //#region ICommand Members
        //public event EventHandler CanExecuteChanged
        //{
        //    add
        //    {

        //        if (_canExecute != null)
        //            CommandManager.RequerySuggested += value;
        //    }
        //    remove
        //    {

        //        if (_canExecute != null)
        //            CommandManager.RequerySuggested -= value;
        //    }
        //}

        //[DebuggerStepThrough]
        //public Boolean CanExecute(Object parameter)
        //{
        //    return _canExecute == null ? true : _canExecute();
        //}

        //public void Execute(Object parameter)
        //{
        //    _execute();
        //}
        //#endregion

        readonly Action<object> execute;
        readonly Predicate<object> canExecute;

        public RelayCommand(Action<object> executeDelegate, Predicate<object> canExecuteDelegate)
        {
            execute = executeDelegate;
            canExecute = canExecuteDelegate;
        }

        bool ICommand.CanExecute(object parameter)
        {
            return canExecute == null ? true : canExecute(parameter);
        }

        event EventHandler ICommand.CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }

        void ICommand.Execute(object parameter)
        {
            execute(parameter);
        }
    }

}
