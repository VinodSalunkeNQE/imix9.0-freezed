﻿#pragma checksum "..\..\..\UserControl\Trigger.xaml" "{8829d00f-11b8-4213-878b-770e8597ac16}" "D1DBEA167B48505B319441F7D0190E33A1C828EB7FD6D39155ECC1EC95100B9A"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using DigiPhoto.Common;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms.Integration;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;
using Xceed.Wpf.Toolkit;


namespace DigiConfigUtility {
    
    
    /// <summary>
    /// Trigger
    /// </summary>
    public partial class Trigger : System.Windows.Controls.UserControl, System.Windows.Markup.IComponentConnector, System.Windows.Markup.IStyleConnector {
        
        
        #line 23 "..\..\..\UserControl\Trigger.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.CheckBox chkSelectAll;
        
        #line default
        #line hidden
        
        
        #line 30 "..\..\..\UserControl\Trigger.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ListBox listTables;
        
        #line default
        #line hidden
        
        
        #line 49 "..\..\..\UserControl\Trigger.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnUp;
        
        #line default
        #line hidden
        
        
        #line 52 "..\..\..\UserControl\Trigger.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnDown;
        
        #line default
        #line hidden
        
        
        #line 66 "..\..\..\UserControl\Trigger.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ListBox SyncPrioritylistDisplay;
        
        #line default
        #line hidden
        
        
        #line 97 "..\..\..\UserControl\Trigger.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txtSyncServiceInterval;
        
        #line default
        #line hidden
        
        
        #line 101 "..\..\..\UserControl\Trigger.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txtSyncDataDelay;
        
        #line default
        #line hidden
        
        
        #line 105 "..\..\..\UserControl\Trigger.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txtReSyncDataDelay;
        
        #line default
        #line hidden
        
        
        #line 113 "..\..\..\UserControl\Trigger.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.CheckBox chkReSyncFailedOnlineOrder;
        
        #line default
        #line hidden
        
        
        #line 117 "..\..\..\UserControl\Trigger.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Xceed.Wpf.Toolkit.DateTimePicker ccResyncDateTime;
        
        #line default
        #line hidden
        
        
        #line 127 "..\..\..\UserControl\Trigger.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox cmbSelectRecursiveCount;
        
        #line default
        #line hidden
        
        
        #line 128 "..\..\..\UserControl\Trigger.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox cmbSelectRecursiveTime;
        
        #line default
        #line hidden
        
        
        #line 132 "..\..\..\UserControl\Trigger.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txtReSyncMaxCount;
        
        #line default
        #line hidden
        
        
        #line 139 "..\..\..\UserControl\Trigger.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnSave;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/DigiConfigUtility;component/usercontrol/trigger.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\..\UserControl\Trigger.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.chkSelectAll = ((System.Windows.Controls.CheckBox)(target));
            
            #line 23 "..\..\..\UserControl\Trigger.xaml"
            this.chkSelectAll.Click += new System.Windows.RoutedEventHandler(this.chkSelectAll_Click);
            
            #line default
            #line hidden
            return;
            case 2:
            this.listTables = ((System.Windows.Controls.ListBox)(target));
            return;
            case 4:
            this.btnUp = ((System.Windows.Controls.Button)(target));
            
            #line 49 "..\..\..\UserControl\Trigger.xaml"
            this.btnUp.Click += new System.Windows.RoutedEventHandler(this.btnUp_Click);
            
            #line default
            #line hidden
            return;
            case 5:
            this.btnDown = ((System.Windows.Controls.Button)(target));
            
            #line 52 "..\..\..\UserControl\Trigger.xaml"
            this.btnDown.Click += new System.Windows.RoutedEventHandler(this.btnDown_Click);
            
            #line default
            #line hidden
            return;
            case 6:
            this.SyncPrioritylistDisplay = ((System.Windows.Controls.ListBox)(target));
            
            #line 66 "..\..\..\UserControl\Trigger.xaml"
            this.SyncPrioritylistDisplay.SelectionChanged += new System.Windows.Controls.SelectionChangedEventHandler(this.SyncPrioritylistDisplay_SelectionChanged);
            
            #line default
            #line hidden
            return;
            case 7:
            this.txtSyncServiceInterval = ((System.Windows.Controls.TextBox)(target));
            return;
            case 8:
            this.txtSyncDataDelay = ((System.Windows.Controls.TextBox)(target));
            return;
            case 9:
            this.txtReSyncDataDelay = ((System.Windows.Controls.TextBox)(target));
            return;
            case 10:
            this.chkReSyncFailedOnlineOrder = ((System.Windows.Controls.CheckBox)(target));
            return;
            case 11:
            this.ccResyncDateTime = ((Xceed.Wpf.Toolkit.DateTimePicker)(target));
            
            #line 117 "..\..\..\UserControl\Trigger.xaml"
            this.ccResyncDateTime.ValueChanged += new System.Windows.RoutedPropertyChangedEventHandler<object>(this.ccBackupDateTime_ValueChanged);
            
            #line default
            #line hidden
            return;
            case 12:
            this.cmbSelectRecursiveCount = ((System.Windows.Controls.ComboBox)(target));
            return;
            case 13:
            this.cmbSelectRecursiveTime = ((System.Windows.Controls.ComboBox)(target));
            return;
            case 14:
            this.txtReSyncMaxCount = ((System.Windows.Controls.TextBox)(target));
            return;
            case 15:
            this.btnSave = ((System.Windows.Controls.Button)(target));
            
            #line 139 "..\..\..\UserControl\Trigger.xaml"
            this.btnSave.Click += new System.Windows.RoutedEventHandler(this.btnSave_Click);
            
            #line default
            #line hidden
            return;
            }
            this._contentLoaded = true;
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        void System.Windows.Markup.IStyleConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 3:
            
            #line 33 "..\..\..\UserControl\Trigger.xaml"
            ((System.Windows.Controls.CheckBox)(target)).Click += new System.Windows.RoutedEventHandler(this.chkitems_Click);
            
            #line default
            #line hidden
            break;
            }
        }
    }
}

