﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace DigiConfigUtility.Converter
{
    public class Converter: IMultiValueConverter
    {
        public object Convert(object[] values,Type targetType,object parameter,System.Globalization.CultureInfo culture )
        {
            return values.Clone();
        }
        public object[] ConvertBack(object values,Type[]  targetTypes,object parameter,System.Globalization.CultureInfo culture)
        {
            return null;
        }
    }
}
