﻿using DigiPhoto;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using DigiPhoto.Utility.Repository.ValueType;
namespace DigiConfigUtility.Utility
{
    public static class Config
    {
        private static int subStoreId;
        public static int SubStoreId
        {
            get
            {
                if (subStoreId == 0)
                {
                    string pathtosave = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
                    if (File.Exists(pathtosave + "\\ss.dat"))
                    {
                        string line;
                        string SbStoreId;
                        using (StreamReader reader = new StreamReader(pathtosave + "\\ss.dat"))
                        {
                            line = reader.ReadLine();
                            SbStoreId = CryptorEngine.Decrypt(line, true);
                            subStoreId = SbStoreId.Split(',')[0].ToInt32();
                            if (subStoreId == 0)
                                throw new Exception("Please configure your SubStore");
                        }
                    }
                }
                return subStoreId;
            }
            set
            {
                subStoreId = value;
            }
        }
    }
}
