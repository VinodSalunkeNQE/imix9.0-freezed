﻿using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DigiConfigUtility.Utility
{
    class LogConfigurator
    {
        /// <summary>
        /// Configures the log4net.
        /// </summary>
        static LogConfigurator()
        {
            log4net.Config.XmlConfigurator.Configure();
        }
        public static ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);       
    }
}
