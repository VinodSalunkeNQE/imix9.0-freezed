﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.IO;
using DigiPhoto;
using System.Xml.Serialization;
using System.Configuration;

namespace DigiConfigUtility.Utility
{
    public static class CommonUtility
    {
        public static DataTable CopyGenericToDataTable<T>(IEnumerable<T> items)
        {
            var properties = typeof(T).GetProperties().Where(p => (p.Name != "EntityKey" && p.Name != "EntityState"));
            var result = new DataTable();

            //Build the columns
            foreach (var prop in properties)
            {
                Type propType = prop.PropertyType;
                if (propType.IsGenericType &&
                    propType.GetGenericTypeDefinition() == typeof(Nullable<>))
                {
                    propType = Nullable.GetUnderlyingType(propType);
                }
                result.Columns.Add(prop.Name, propType);
            }

            //Fill the DataTable
            foreach (var item in items)
            {
                var row = result.NewRow();

                foreach (var prop in properties)
                {
                    var itemValue = prop.GetValue(item, new object[] { });
                    if (itemValue != null)
                        row[prop.Name] = itemValue;
                    else
                        row[prop.Name] = DBNull.Value;
                }

                result.Rows.Add(row);
            }

            return result;
        }      

        public static string SerializeObject<T>(this T toSerialize)
        {
            XmlSerializer xmlSerializer = new XmlSerializer(toSerialize.GetType());
            StringWriter textWriter = new StringWriter();

            xmlSerializer.Serialize(textWriter, toSerialize);
            return textWriter.ToString().Replace("<?xml version=\"1.0\" encoding=\"utf-16\"?>", "");
        }

        public static string targetDBConStr
        {
            get
            {
                return System.Configuration.ConfigurationManager.ConnectionStrings["DigiConnectionString"].ConnectionString;
            }
        }
    }
}
