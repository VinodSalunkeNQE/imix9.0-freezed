﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using ETS.DataAccess;
using System.Data;

namespace DigiConfigUtility.Utility
{
    class DBTransfer
    {
        /// <summary>
        /// Function returns all the tables with data in a single dataset
        /// </summary>
        /// <param name="uTables"></param>
        /// <returns></returns>
        public static DataSet GetAllTables()
        {
            List<string> uTables = getTablesList();
            DataSet MasterDS = new DataSet();
            foreach (string item in uTables)
            {
                string sqlStr = "SELECT * FROM " + item;
                DataSet dsTemp = SqlHelper.FillDataSet(Utility.CommonUtility.sourceDBConStr, CommandType.Text, sqlStr, null);
                MasterDS.Tables.Add(dsTemp.Tables[0]);
            }
            return MasterDS;
        }
        /// <summary>
        /// Function returns all the user defined tables in a list
        /// </summary>
        /// <returns></returns>
        private static List<string> getTablesList()
        {
            List<string> uTables = new List<string>();
            string sqlStr = "SELECT * FROM sysobjects WHERE xtype='U'";
            SqlDataReader dr = SqlHelper.ExecuteReader(Utility.CommonUtility.sourceDBConStr, System.Data.CommandType.Text, sqlStr, null);
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    uTables.Add(Convert.ToString(dr["name"]));
                }
            }
            return uTables;
        }
        /// <summary>
        /// Function to restore the backup database
        /// </summary>
        /// <param name="backUpPath">BAK file location</param>
        /// <param name="_databasename">New name for the database</param>
        /// <returns></returns>
        public static int RestoreDataBase(string backUpPath, string _databasename)
        {
            int result = 0;
            try
            {
                string UseMaster = "USE master";
                SqlHelper.ExecuteNonQuery(Utility.CommonUtility.sourceDBConStr, CommandType.Text, UseMaster, null);

                string Alter1 = @"ALTER DATABASE [" + _databasename + "] SET Single_User WITH Rollback Immediate";
                SqlHelper.ExecuteNonQuery(Utility.CommonUtility.sourceDBConStr, CommandType.Text, Alter1, null);

                string Restore = @"RESTORE DATABASE [" + _databasename + "] FROM DISK = N'" + backUpPath + @"' WITH  FILE = 1,  NOUNLOAD,  STATS = 10";
                SqlHelper.ExecuteNonQuery(Utility.CommonUtility.sourceDBConStr, CommandType.Text, Restore, null);

                string Alter2 = @"ALTER DATABASE [" + _databasename + "] SET Multi_User";
                SqlHelper.ExecuteNonQuery(Utility.CommonUtility.sourceDBConStr, CommandType.Text, Alter2, null);
                result = 0;
            }
            catch (Exception ex)
            {
                result = -1;
            }
            return result;
        }
    }
}
