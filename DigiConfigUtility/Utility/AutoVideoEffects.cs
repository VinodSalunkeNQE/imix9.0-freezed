﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows;

namespace DigiConfigUtility
{
    public class PanProperty
    {
        public int PanID { get; set; }
        public int PanStartTime { get; set; }
        public int PanStopTime { get; set; }
        public int PanSourceLeft { get; set; }
        public int PanSourceTop { get; set; }
        public int PanSourceWidth { get; set; }
        public int PanSourceHeight { get; set; }
        public int PanDestLeft { get; set; }
        public int PanDestTop { get; set; }
        public int PanDestWidth { get; set; }
        public int PanDestHeight { get; set; }
    }
    public class TransitionProperty
    {
        public int ID { get; set; }
        public int TransitionID { get; set; }
        public int TransitionStartTime { get; set; }
        public int TransitionStopTime { get; set; }
        public string TransitionName { get; set; }
    }
    public class SlotProperty
    {
        public int ID { get; set; }
        public long ItemID { get; set; }
        public int StartTime { get; set; }
        public int StopTime { get; set; }
        public string FilePath { get; set; }
        public string Name { get; set; }
        public int Left { get; set; }
        public int Top { get; set; }
        public int InsertTime { get; set; }
        public string settings { get; set; }
    }
    public class TemplateListItems
    {
        private Visibility _CheckedBoxVisible;
        public Visibility CheckedBoxVisible
        {
            get
            {
                return _CheckedBoxVisible;
            }
            set
            {
                _CheckedBoxVisible = value;
            }
        }
        private long _Item_ID;
        public long Item_ID
        {
            get { return _Item_ID; }
            set { _Item_ID = value; }
        }
        private bool _IsChecked;
        public bool IsChecked
        {
            get
            {
                return _IsChecked;
            }
            set
            {
                _IsChecked = value;
            }
        }
        private string _DisplayName;
        public string DisplayName
        {
            get { return _DisplayName; }
            set { _DisplayName = value; }
        }
        private string _Name;
        public string Name
        {
            get { return _Name; }
            set { _Name = value; }
        }
        private string _FilePath;
        public string FilePath
        {
            get { return _FilePath; }
            set { _FilePath = value; }
        }
        private int _MediaType;
        public int MediaType
        {
            get { return this._MediaType; }
            set
            {
                this._MediaType = value;
            }
        }
        private long _Length;
        public long Length
        {
            get { return this._Length; }
            set
            {
                this._Length = value;
            }
        }
        private string _Tooltip;
        public string Tooltip
        {
            get { return this._Tooltip; }
            set { this._Tooltip = value; }
        }
        public int StartTime { get; set; }
        public int EndTime { get; set; }
        public int InsertTime { get; set; }
        public bool isActive { get; set; }
        public string GuestVideoPath { get; set; }
        public int LeftPositon { get; set; }
        public int TopPositon { get; set; }
    }
    public class GeneralEffects
    {
        public string darkness { get; set; }
        public string fadeInOut { get; set; }
        public bool greyScale { get; set; }
        public bool invert { get; set; }
        public string lightness { get; set; }
        public string saturation { get; set; }
        public string contrast { get; set; }
        public string zoom { get; set; }
    }
    public class TextLogoProperty
    {
        public string textLogoPosition { get; set; }
        public string textfontName { get; set; }
        public string textfontColor { get; set; }
        public string textfontSize { get; set; }
        public string textfontStyle { get; set; }
        public string textLogo { get; set; }
        public bool applyTextLogo { get; set; }

    }
    public class ChromaKeyProperty
    {
        public string chromaKeyColor { get; set; }
        public string chromaKeyBG { get; set; }
        public bool chroma { get; set; }
        public int contrastHigh { get; set; }
        public int contrastLow { get; set; }
        public int backgroundId { get; set; }

    }
    public class AutoVideoEffects
    {
        public static string GetVideoEffectsXML(GeneralEffects obj, TextLogoProperty txtLogoProperty, ChromaKeyProperty chromaProperty, List<PanProperty> PanPropertyLst, List<TransitionProperty> TransitionPropertyLst, List<SlotProperty> borderslotlist, List<SlotProperty> logoSlotList, List<SlotProperty> audioSlotList, List<SlotProperty> videoTemplateSlotList, int productId, int VidLength, int restartEngine, string layeringData)
        {
            string outXML = string.Empty;
            outXML += "<effects>";
            outXML += "<lightness>";
            outXML += obj.lightness;
            outXML += "</lightness>";
            outXML += "<saturation>";
            outXML += obj.saturation;
            outXML += "</saturation>";
            outXML += "<contrast>";
            outXML += obj.contrast;
            outXML += "</contrast>";
            outXML += "<darkness>";
            outXML += obj.darkness;
            outXML += "</darkness>";
            outXML += "<greyScale>";
            outXML += obj.greyScale.ToString();
            outXML += "</greyScale>";
            outXML += "<invert>";
            outXML += obj.invert.ToString();
            outXML += "</invert>";
            outXML += "<textLogo textLogoPosition=\"" + txtLogoProperty.textLogoPosition + "\" textfontName=\"" + txtLogoProperty.textfontName + "\" textfontColor=\"" + txtLogoProperty.textfontColor + "\" textfontSize=\"" + txtLogoProperty.textfontSize + "\" textfontStyle=\"" + txtLogoProperty.textfontStyle + "\" applyTextLogo=\"" + txtLogoProperty.applyTextLogo + "\">";
            outXML += txtLogoProperty.textLogo;
            outXML += "</textLogo>";
            outXML += "<zoom>";
            outXML += obj.zoom;
            outXML += "</zoom>";
            outXML += "<fadeInOut>";
            outXML += obj.fadeInOut;
            outXML += "</fadeInOut>";
            outXML += "<chroma chromaKeyColor=\"" + chromaProperty.chromaKeyColor + "\" chromaKeyBG=\"" + chromaProperty.chromaKeyBG + "\" contrastHigh=\"" + chromaProperty.contrastHigh + "\" contrastLow=\"" + chromaProperty.contrastLow + "\" chromaBackgroundId=\"" + chromaProperty.backgroundId + "\">";
            outXML += chromaProperty.chroma.ToString();
            outXML += "</chroma>";
            //outXML += "<audio amplify=\"" + obj.amplify + "\" equal1=\"" + obj.equal1 + "\" equal2=\"" + obj.equal2 + "\" equal3=\"" + obj.equal3 + "\" equal4=\"" + obj.equal4 + "\" equal5=\"" + obj.equal5 + "\" equal6=\"" + obj.equal6 + "\">";
            //outXML += obj.audio;
            //outXML += "</audio>";
            if (PanPropertyLst != null && PanPropertyLst.Count > 0)
            {
                outXML += "<Pans>";
                foreach (PanProperty pp in PanPropertyLst)
                {
                    outXML += "<Pan>";
                    outXML += "<PanID>" + pp.PanID + "</PanID>";
                    outXML += "<PanStartTime>" + pp.PanStartTime + "</PanStartTime>";
                    outXML += "<PanStopTime>" + pp.PanStopTime + "</PanStopTime>";
                    outXML += "<PanSourceLeft>" + pp.PanSourceLeft + "</PanSourceLeft>";
                    outXML += "<PanSourceTop>" + pp.PanSourceTop + "</PanSourceTop>";
                    outXML += "<PanSourceWidth>" + pp.PanSourceWidth + "</PanSourceWidth>";
                    outXML += "<PanSourceHeight>" + pp.PanSourceHeight + "</PanSourceHeight>";
                    outXML += "<PanDestLeft>" + pp.PanDestLeft + "</PanDestLeft>";
                    outXML += "<PanDestTop>" + pp.PanDestTop + "</PanDestTop>";
                    outXML += "<PanDestWidth>" + pp.PanDestWidth + "</PanDestWidth>";
                    outXML += "<PanDestHeight>" + pp.PanDestHeight + "</PanDestHeight>";
                    outXML += "</Pan>";
                }
                outXML += "</Pans>";
            }
            else
            {
                outXML += "<Pans>";
                outXML += "</Pans>";
            }
            if (TransitionPropertyLst != null && TransitionPropertyLst.Count > 0)
            {
                outXML += "<Transitions>";
                foreach (TransitionProperty tp in TransitionPropertyLst)
                {
                    outXML += "<Transition>";
                    outXML += "<ID>" + tp.ID + "</ID>";
                    outXML += "<TransitionID>" + tp.TransitionID + "</TransitionID>";
                    outXML += "<TransitionStartTime>" + tp.TransitionStartTime + "</TransitionStartTime>";
                    outXML += "<TransitionStopTime>" + tp.TransitionStopTime + "</TransitionStopTime>";
                    outXML += "<TransitionName>" + tp.TransitionName + "</TransitionName>";
                    outXML += "</Transition>";
                }
                outXML += "</Transitions>";
            }
            else
            {
                outXML += "<Transitions>";
                outXML += "</Transitions>";
            }
            if (borderslotlist != null && borderslotlist.Count > 0)
            {
                outXML += "<Borders>";
                foreach (SlotProperty bp in borderslotlist)
                {
                    outXML += "<Border>";
                    outXML += "<ItemID>" + bp.ItemID + "</ItemID>";
                    outXML += "<Name>" + Path.GetFileName(bp.FilePath) + "</Name>";
                    outXML += "<StartTime>" + bp.StartTime + "</StartTime>";
                    outXML += "<StopTime>" + bp.StopTime + "</StopTime>";
                    outXML += "</Border>";
                }
                outXML += "</Borders>";
            }
            else
            {
                outXML += "<Borders>";
                outXML += "</Borders>";
            }
            if (logoSlotList != null && logoSlotList.Count > 0)
            {
                outXML += "<graphics>";
                foreach (SlotProperty lsp in logoSlotList)
                {
                    outXML += "<graphic>";                   
                    outXML += "<ItemID>" + lsp.ItemID + "</ItemID>";
                    outXML += "<Name>" + Path.GetFileName(lsp.FilePath) + "</Name>";
                    outXML += "<StartTime>" + lsp.StartTime + "</StartTime>";
                    outXML += "<StopTime>" + lsp.StopTime + "</StopTime>";
                    outXML += "<PositionTop>" + lsp.Top + "</PositionTop>";
                    outXML += "<PositionLeft>" + lsp.Left + "</PositionLeft>";
                    if (!string.IsNullOrEmpty(lsp.settings))
                    {
                        outXML += lsp.settings ;
                    }
                    else
                    {
                        outXML += "<settings wthsource ='' angle='' scalex ='' scaley ='' zoomfactor = '' zindex='4'></settings>";
                    }
                    outXML += "</graphic>";
                }
                outXML += "</graphics>";
            }
            else
            {
                outXML += "<graphics>";
                outXML += "</graphics>";
            }
            if (audioSlotList != null && audioSlotList.Count > 0)
            {
                outXML += "<audios>";
                foreach (SlotProperty lsp in audioSlotList)
                {
                    outXML += "<audio>";
                    outXML += "<ItemID>" + lsp.ItemID + "</ItemID>";
                    outXML += "<Name>" + lsp.Name + "</Name>";
                    outXML += "<FilePath>" + lsp.FilePath + "</FilePath>";
                    outXML += "<StartTime>" + lsp.StartTime + "</StartTime>";
                    outXML += "<StopTime>" + lsp.StopTime + "</StopTime>";
                    outXML += "<InsertTime>" + lsp.InsertTime + "</InsertTime>";

                    outXML += "</audio>";
                }
                outXML += "</audios>";
            }
            else
            {
                outXML += "<audios>";
                outXML += "</audios>";
            }
            if (videoTemplateSlotList != null && videoTemplateSlotList.Count > 0)
            {
                outXML += "<videoTemplates>";
                foreach (SlotProperty lsp in videoTemplateSlotList)
                {
                    outXML += "<videoTemplate>";
                    outXML += "<ItemID>" + lsp.ItemID + "</ItemID>";
                    outXML += "<Name>" + lsp.Name + "</Name>";
                    outXML += "<FilePath>" + lsp.FilePath + "</FilePath>";
                    outXML += "<StartTime>" + lsp.StartTime + "</StartTime>";
                    outXML += "<StopTime>" + lsp.StopTime + "</StopTime>";
                    outXML += "<InsertTime>" + lsp.InsertTime + "</InsertTime>";

                    outXML += "</videoTemplate>";
                }
                outXML += "</videoTemplates>";
            }
            else
            {
                outXML += "<videoTemplates>";
                outXML += "</videoTemplates>";
            }
            outXML += "<VideoProduct>";
            outXML += productId;
            outXML += "</VideoProduct>";
            outXML += "<VideoLength>";
            outXML += VidLength;
            outXML += "</VideoLength>";
            outXML += "<RestartEngine>";
            outXML += restartEngine;
            outXML += "</RestartEngine>";
            if (!string.IsNullOrEmpty(layeringData))
            {
                outXML += layeringData;
            }
            else
            {
                outXML += "<photo zoomfactor='1' canvasleft='0' canvastop='0' scalecenterx='-1' scalecentery='-1'></photo>";
            }
            outXML += "</effects>";
            return outXML;
        }
    }
    public class SlotValidation
    {
        public static bool IsValidSlot(List<SlotProperty> slotList, int startTime, int stopTime)
        {

            SlotProperty firstItem = slotList.OrderBy(o => o.StartTime).FirstOrDefault();
            SlotProperty LastItem = slotList.OrderBy(o => o.StartTime).LastOrDefault();
            if (firstItem != null)
            {
                if (stopTime <= firstItem.StartTime)
                {
                    return true;
                }
                else if (startTime >= LastItem.StopTime)
                {
                    return true;
                }
                else
                {
                    List<SlotProperty> templst = slotList.OrderBy(o => o.StartTime).ToList();
                    SlotProperty item = templst.Where(o => o.StopTime <= startTime).LastOrDefault();
                    SlotProperty item2 = templst[templst.IndexOf(item) + 1];
                    if (item2 != null)
                    {
                        if (item2.StartTime >= stopTime)
                        {
                            return true;
                        }
                        else
                        {
                            MessageBox.Show("There is already an item in the given time frame.", "DigiPhoto", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                            return false;
                        }
                    }
                    else
                    {
                        MessageBox.Show("There is already an item in the given time frame.", "DigiPhoto", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                        return false;
                    }
                }
            }
            else
            {
                return true;
            }
        }
    }
}
