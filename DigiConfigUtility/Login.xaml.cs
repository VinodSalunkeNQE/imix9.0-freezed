﻿using DigiConfigUtility.Utility;
using DigiPhoto;
using DigiPhoto.Common;
using DigiPhoto.DataLayer;
//using DigiPhoto.DataLayer.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using DigiPhoto.IMIX.Business;
using DigiPhoto.IMIX.Model;
using System.Diagnostics;
using System.IO;

namespace DigiConfigUtility
{
    /// <summary>
    /// Interaction logic for Login.xaml
    /// </summary>
    public partial class Login : Window
    {
        //DigiPhotoDataServices _objDataServices;
        public Login()
        {
            InitializeComponent();
            ClearControls();
        }
        private void ClearControls()
        {
            txtPassword.Password = string.Empty;
            txtUserName.Text = string.Empty;
            txtUserName.Focus();
            txbErrorMessage.Text = string.Empty;
        }
        private bool Isvalidate()
        {
            if (string.IsNullOrWhiteSpace(txtUserName.Text))
            {
                ShowErrorMessage("Please enter username");
                txtUserName.Focus();
                return false;
            }
            if (string.IsNullOrWhiteSpace(txtPassword.Password))
            {
                ShowErrorMessage("Please enter password");
                txtPassword.Focus();
                return false;
            }
            return true;
        }
        private void UserLogin()
        {
            if (Isvalidate())
            {
                try
                {
                    bool validUser = false;
                    if (txtUserName.Text.ToLower() == "superadmin" && txtPassword.Password == "D!g!@321123")    ///Work for old database which do not have encrypted password and ITadmin user.
                    {
                        MainWindow mainWindow = new MainWindow(0);
                        mainWindow.Show();
                        //  mainWindow.tabSubStore.re
                        this.Hide();
                        //this.Close();
                        mainWindow.Focus();
                        mainWindow.Activate();
                        validUser = true;
                    }
                    if (txtUserName.Text.ToLower() == "itadmin" && txtPassword.Password == "D!g!@321123")    ///Work for old database which do not have encrypted password and ITadmin user.
                    {
                        MainWindow mainWindow = new MainWindow();
                        mainWindow.Show();
                        this.Hide();
                        //this.Close();
                        mainWindow.Focus();
                        mainWindow.Activate();
                        validUser = true;
                    }
                    else
                    {
                        UserBusiness useBiz = new UserBusiness();
                        //_objDataServices = new DigiPhotoDataServices();
                        /////////////////////////GET DATA FOR ENTERD USERNAME AND PASSWORD/////////
                        var objUser = useBiz.GetUserDetails(txtUserName.Text, DigiPhoto.CryptorEngine.Encrypt(txtPassword.Password, true));
                        PermissionRoleInfo AllowedPermissions = null;
                        if (objUser != null)
                        {
                            RolePermissionsBusniess rolBiz = new RolePermissionsBusniess();
                            List<PermissionRoleInfo> lstAllowedPermissions = rolBiz.GetPermissionData(objUser.DG_User_Roles_Id);
                            AllowedPermissions = lstAllowedPermissions.Where(x=>x.DG_Permission_Id==31).FirstOrDefault();
                        }
                        if (AllowedPermissions != null && AllowedPermissions.DG_Permission_Id == 31)
                        {
                            ////////////ASSIGNING VALUES TO COMMON VARIABLES(FOR WHOLE APPLICATION//////////
                            LoginUser.StoreName = objUser.DG_Store_Name;
                            LoginUser.UserId = objUser.DG_User_pkey;
                            LoginUser.RoleId = objUser.DG_User_Roles_Id;
                            LoginUser.StoreId = objUser.DG_Store_ID;
                            LoginUser.UserName = (objUser.DG_User_First_Name + " " + objUser.DG_User_Last_Name).Trim();
                            /////////////CREATING OBJECT FOR HOME SCREEN///////////
                            MainWindow mainWindow = new MainWindow();
                            mainWindow.Show();
                            ////////CLOSING CURRENT WINDOW//////////
                            //this.Close();
                            this.Hide();
                            mainWindow.Focus();
                            mainWindow.Activate();
                            validUser = true;
                        }
                    }
                    if (!validUser)
                    {
                        ShowErrorMessage("Incorrect username or password");
                        txtUserName.Focus();
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Could not connect to database! Please check error log for more details.");

                }
            }
        }

       
        private void ShowErrorMessage(string strMessage)
        {
            txbErrorMessage.Text = strMessage;
        }
        private void btnLogin_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                UserLogin();
            }
            catch (Exception ex)
            {
                LogConfigurator.log.Error("Error Message: " + ex.Message + " Error Stack Trace: " + ex.StackTrace);
            }

        }
        private void txtUserName_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.Key == Key.Enter)
                {
                    UserLogin();
                }
            }
            catch (Exception ex)
            {
                LogConfigurator.log.Error("Error Message: " + ex.Message + " Error Stack Trace: " + ex.StackTrace);
            }
        }
        private void txtPassword_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.Key == Key.Enter)
                {
                    UserLogin();
                }
            }
            catch (Exception ex)
            {
                LogConfigurator.log.Error("Error Message: " + ex.Message + " Error Stack Trace: " + ex.StackTrace);
            }
        }

        private void Window_Closed(object sender, EventArgs e)
        {
            Process thisProc = Process.GetCurrentProcess();
            if (Process.GetProcessesByName(thisProc.ProcessName).Length >= 1)
            {
                thisProc.Kill();
            }

        }

      
    }
}
