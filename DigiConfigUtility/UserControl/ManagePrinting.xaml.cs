﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Windows;
using System.Windows.Documents;
using System.Windows.Forms;
using DigiConfigUtility.Utility;
using System.IO;
using DigiPhoto.DataLayer;
using DigiPhoto.DataLayer.Model;
using System.Data;
using System.Text;
using DigiPhoto;
using DigiPhoto.IMIX.Business;
using DigiPhoto.IMIX.Model;
using DigiPhoto.Utility.Repository.ValueType;
namespace DigiConfigUtility
{
    /// <summary>
    /// Interaction logic for DataMigration.xaml
    /// </summary>
    public partial class ManagePrinting : System.Windows.Controls.UserControl
    {
        StoreSubStoreDataBusniess storeBuzz = new StoreSubStoreDataBusniess();
        public ManagePrinting()
        {
            InitializeComponent();
            cmbProductType.Items.Add(new KeyValuePair<string, int>("-Select All-", 0));
            cmbProductType.Items.Add(new KeyValuePair<string, int>("6*8", 1));
            cmbProductType.Items.Add(new KeyValuePair<string, int>("8*10", 2));
            cmbProductType.SelectedIndex = 0;
            FillSubstore();
        }

        private void btnClrPrinterQueue_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                int rowsUpdated = storeBuzz.ClearPrinterQueue(Convert.ToInt32(cmbProductType.SelectedValue), Convert.ToInt32(cmbSelectSubstore.SelectedValue)); ;
                if (rowsUpdated > 0)
                    System.Windows.MessageBox.Show("Printer Queue Cleared.");
                else
                    System.Windows.MessageBox.Show("No Printer Queue Found.");
            }
            catch (Exception ex)
            {
                LogConfigurator.log.Error("Error Message: " + ex.Message + " Error Stack Trace: " + ex.StackTrace);
            }
        }
        private void FillSubstore()
        {
            try
            {
                //_objDataLayer = new DigiPhotoDataServices();
                Dictionary<string, string> lstSubStore = new Dictionary<string, string>();
                StoreSubStoreDataBusniess stoBiz = new StoreSubStoreDataBusniess();
                var list = stoBiz.GetSubstoreData();
                lstSubStore.Add("-Select All-", "0");
                foreach (var item in list)
                {
                    lstSubStore.Add(item.DG_SubStore_Name, item.DG_SubStore_pkey.ToString());
                }
                cmbSelectSubstore.ItemsSource = lstSubStore;
                cmbSelectSubstore.SelectedIndex = 0;
            }
            catch (Exception ex)
            {
                LogConfigurator.log.Error("Error Message: " + ex.Message + " Error Stack Trace: " + ex.StackTrace);
            }
        }
    }
}
