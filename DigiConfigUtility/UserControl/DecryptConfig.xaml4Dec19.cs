﻿using DigiConfigUtility.Utility;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Xml;
using DigiPhoto.IMIX.Business;
using DigiPhoto.IMIX.Model;

namespace DigiConfigUtility
{
    /// <summary>
    /// Interaction logic for DecryptConfig.xaml
    /// </summary>
    public partial class DecryptConfig : System.Windows.Controls.UserControl
    {
        private string FilePath = string.Empty;
        string xmlData = string.Empty;
        bool IsConfigUtityChanged = false;
        int totalExeCount = 19;
        int selectedExeCount = 0;
        public DecryptConfig()
        {
            InitializeComponent();
            TagApplicationNameWithCheckBox();
        }


        private void button1_Click(object sender, RoutedEventArgs e)
        {
            try
            {

                FolderBrowserDialog folderBrowserDialog = new FolderBrowserDialog();

                folderBrowserDialog.SelectedPath = Environment.CurrentDirectory;
                folderBrowserDialog.ShowNewFolderButton = false;

                DialogResult dgResult = folderBrowserDialog.ShowDialog();

                if (dgResult == System.Windows.Forms.DialogResult.OK)
                {
                    textBox1.Text = folderBrowserDialog.SelectedPath;
                    //load data from xml digiphoto

                    FilePath = folderBrowserDialog.SelectedPath;
                    LoadExeFromSelectedFolder(FilePath);
                    if (File.Exists(FilePath + "\\DigiPhoto.exe") && File.Exists(FilePath + "\\DigiPhoto.exe.config"))
                    {
                        decrypt(FilePath + "\\DigiPhoto.exe");
                        ShowXmlData(string.Concat(folderBrowserDialog.SelectedPath, "\\DigiPhoto.exe.config"));
                        encrypt(FilePath + "\\DigiPhoto.exe");
                        chkSelectAll.IsEnabled = true;
                    }
                    else
                    {
                        textBoxDataSource.Text = string.Empty;
                        textBoxInitialCatalog.Text = string.Empty;
                        textBoxPassword.Text = string.Empty;
                        textBoxUserID.Text = string.Empty;
                        System.Windows.MessageBox.Show("DigiPhoto.exe file not found.please provide input for working with other exes.", "Digi");
                    }


                    // FilePath = "C:\\Temp";

                    //decrypt(FilePath + "\\DigiPhoto.exe");
                    //ShowXmlData(FilePath + "\\DigiPhoto.exe.config");
                    //encrypt(FilePath + "\\DigiPhoto.exe");
                }
            }
            catch (Exception ex)
            {
                System.Windows.MessageBox.Show("Please select an exe folder", "Digi");
            }
        }

        private void ShowXmlData(string fileName)
        {
            try
            {
                XmlDocument doc = new XmlDocument();
                doc.Load(fileName);
                XmlElement root = doc.DocumentElement;

                XmlNodeList nodes = root.SelectNodes("connectionStrings"); // You can also use XPath here
                foreach (XmlNode node in nodes)
                {
                    XmlNodeList xmlNodeList = node.ChildNodes;
                    foreach (XmlNode item in xmlNodeList)
                    {

                        foreach (XmlAttribute attrColection in item.Attributes)
                        {
                            if (attrColection.Name == "connectionString")
                            {
                                string stringData = attrColection.Value;

                                string[] strCol = stringData.Split(';');

                                foreach (var strC in strCol)
                                {
                                    if (string.IsNullOrEmpty(strC.Trim()))
                                        break;
                                    string[] strColChild = strC.Split('=');
                                    switch (strColChild[strColChild.Length - 2].Trim(new char[] { ' ', '"' }))
                                    {
                                        case "Data Source":
                                            textBoxDataSource.Text = strColChild[strColChild.Length - 1];
                                            break;
                                        case "Initial Catalog":
                                            textBoxInitialCatalog.Text = strColChild[strColChild.Length - 1];
                                            break;
                                        case "User ID":
                                            textBoxUserID.Text = strColChild[strColChild.Length - 1];
                                            break;
                                        case "Password":
                                            textBoxPassword.Text = strColChild[strColChild.Length - 1];
                                            break;
                                        default:
                                            break;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LogConfigurator.log.Error("Error Message: " + ex.Message + " Error Stack Trace: " + ex.StackTrace);
            }
        }

        static string ConvertStringArrayToString(string[] array)
        {
            StringBuilder builder = new StringBuilder();
            foreach (string value in array)
            {
                builder.Append(value);
            }
            return builder.ToString();
        }

        static string ConvertStringArrayToString(string[] array, string seprator)
        {
            StringBuilder builder = new StringBuilder();
            foreach (string value in array)
            {
                builder.Append(value);
                builder.Append(seprator);
            }
            return builder.ToString();
        }

        private void WriteXmlData(string fileName)
        {
            try
            {
                XmlDocument doc = new XmlDocument();
                doc.Load(fileName);

                XmlElement root = doc.DocumentElement;

                XmlNodeList nodes = root.SelectNodes("connectionStrings");
                foreach (XmlNode node in nodes)
                {
                    XmlNodeList xmlNodeList = node.ChildNodes;
                    foreach (XmlNode item in xmlNodeList)
                    {

                        foreach (XmlAttribute attrColection in item.Attributes)
                        {
                            if (attrColection.Name == "connectionString")
                            {
                                string stringData = attrColection.Value;

                                string[] strCol = stringData.Split(';');
                                StringBuilder strColNew = new StringBuilder();
                                foreach (var strC in strCol)
                                {
                                    if (string.IsNullOrEmpty(strC.Trim()))
                                        break;
                                    string[] strColChild = strC.Split('=');


                                    switch (strColChild[strColChild.Length - 2].Trim(new char[] { ' ', '"' }))
                                    {
                                        case "'Data Source":
                                        case "Data Source":
                                            strColChild[strColChild.Length - 1] = textBoxDataSource.Text;

                                            strColNew.Append(ConvertStringArrayToString(strColChild, "="));
                                            strColNew.Remove(strColNew.Length - 1, 1);
                                            strColNew.Append(";");


                                            break;
                                        case "Initial Catalog":

                                            strColChild[strColChild.Length - 1] = textBoxInitialCatalog.Text;

                                            strColNew.Append(ConvertStringArrayToString(strColChild, "="));
                                            strColNew.Remove(strColNew.Length - 1, 1);
                                            strColNew.Append(";");

                                            //strColNew[strColChild.Length - 1] = textBoxInitialCatalog.Text;
                                            break;
                                        case "User ID":
                                            strColChild[strColChild.Length - 1] = textBoxUserID.Text;

                                            strColNew.Append(ConvertStringArrayToString(strColChild, "="));
                                            strColNew.Remove(strColNew.Length - 1, 1);
                                            strColNew.Append(";");
                                            // strColNew[strColChild.Length - 1] = textBoxUserID.Text;
                                            break;
                                        case "Password":
                                            strColChild[strColChild.Length - 1] = textBoxPassword.Text;

                                            strColNew.Append(ConvertStringArrayToString(strColChild, "="));
                                            strColNew.Remove(strColNew.Length - 1, 1);
                                            strColNew.Append(";");
                                            // strColNew[strColChild.Length - 1] = textBoxPassword.Text;
                                            break;
                                        default:
                                            strColNew.Append(ConvertStringArrayToString(strColChild, "="));
                                            strColNew.Remove(strColNew.Length - 1, 1);
                                            strColNew.Append(";");
                                            break;
                                    }
                                }
                                //strColNew.Append(ConvertStringArrayToString(strColChild, ";"));
                                //stringData = ConvertStringArrayToString(strColNew);

                                attrColection.Value = strColNew.ToString();

                            }
                        }
                    }
                }

                doc.Save(fileName);
            }
            catch (Exception ex)
            {
                LogConfigurator.log.Error("Error Message: " + ex.Message + " Error Stack Trace: " + ex.StackTrace);
            }
        }

        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                bool saved = false;

                if (string.IsNullOrEmpty(FilePath))
                {
                    System.Windows.MessageBox.Show("Select the working folder.", "Digi");
                    return;
                }

                if (FindVisualChildren<System.Windows.Controls.CheckBox>(this).Where(o => o.IsChecked == true).Count() <= 0)
                {
                    System.Windows.MessageBox.Show("Check out some application.", "Digi");
                    return;
                }

                if (checkBoxDigiPhoto.IsChecked == true && !string.IsNullOrEmpty(FilePath))
                {
                    SaveDigiPhoto(FilePath);
                    saved = true;
                }

                if (checkBoxPrintConsole.IsChecked == true && !string.IsNullOrEmpty(FilePath))
                {
                    SavePrintConsole(FilePath);
                    saved = true;
                }

                if (checkBoxWatcher.IsChecked == true && !string.IsNullOrEmpty(FilePath))
                {
                    SaveWatcher(FilePath);
                    saved = true;
                }
                //if (checkBoxVideoWatcher.IsChecked == true && !string.IsNullOrEmpty(FilePath))
                //{
                //    SaveVideoWatcher(FilePath);
                //    saved = true;
                //}
                if (checkBoxWifi.IsChecked == true && !string.IsNullOrEmpty(FilePath))
                {
                    SaveWifi(FilePath);
                    saved = true;
                }

                if (checkBoxRideCaptureUtility.IsChecked == true && !string.IsNullOrEmpty(FilePath))
                {
                    SaveRideCaptureUtility(FilePath);
                    saved = true;
                }

                if (checkBoxRideCameraSetting.IsChecked == true && !string.IsNullOrEmpty(FilePath))
                {
                    SaveRideCameraSetting(FilePath);
                    saved = true;
                }

                //if (checkBoxSyncApp.IsChecked == true && !string.IsNullOrEmpty(FilePath))
                //{
                //    SaveSyncApp(FilePath);
                //    saved = true;
                //}

                if (checkBoxSyncService.IsChecked == true && !string.IsNullOrEmpty(FilePath))
                {
                    SaveSyncService(FilePath);
                    saved = true;
                }
                if (checkBoxImgEngine.IsChecked == true && !string.IsNullOrEmpty(FilePath))
                {
                    SaveImageEngine(FilePath);
                    saved = true;
                }

                if (checkBoxEmailApp.IsChecked == true && !string.IsNullOrEmpty(FilePath))
                {
                    SaveEmailApp(FilePath);
                    saved = true;
                }

                if (checkBoxEmailService.IsChecked == true && !string.IsNullOrEmpty(FilePath))
                {
                    SaveEmailService(FilePath);
                    saved = true;
                }
                if (checkBoxBackupService.IsChecked == true && !string.IsNullOrEmpty(FilePath))
                {
                    SaveBackupService(FilePath);
                    saved = true;
                }

                if (checkBoxConfigUtility.IsChecked == true && !string.IsNullOrEmpty(FilePath))
                {
                    SaveDecryptConfigUtility(FilePath);
                    saved = true;
                    IsConfigUtityChanged = true;
                }
                if (checkBoxVideoManualDownloadConsole.IsChecked == true && !string.IsNullOrEmpty(FilePath))
                {
                    SaveVideoConsole(FilePath);
                    saved = true;
                }
                if (checkBoxVideoProcessor.IsChecked == true && !string.IsNullOrEmpty(FilePath))
                {
                    SaveVideoProcessor(FilePath);
                    saved = true;
                }
                if (checkBoxImageManualDownloadConsole.IsChecked == true && !string.IsNullOrEmpty(FilePath))
                {
                    SaveImageConsole(FilePath);
                    saved = true;
                }
                if (checkBoxRFIDService.IsChecked == true && !string.IsNullOrEmpty(FilePath))
                {
                    SaveService(FilePath, "DigiRfidService.exe");
                    saved = true;
                }
                if (checkBoxRFIDAssociationService.IsChecked == true && !string.IsNullOrEmpty(FilePath))
                {
                    SaveService(FilePath, "DigiRFIDAssociationService.exe");
                    saved = true;
                }
                if (checkBoxDigiUpdates.IsChecked == true && !string.IsNullOrEmpty(FilePath))
                {
                    SaveService(FilePath, "DigiUpdates.exe");
                    saved = true;
                }
                if (checkBoxLiveVideoEditing.IsChecked == true && !string.IsNullOrEmpty(FilePath))
                {
                    SaveService(FilePath, "LiveVideoEditing.exe");
                    saved = true;
                }
                if (checkBoxReportExportService.IsChecked == true && !string.IsNullOrEmpty(FilePath))
                {
                    SaveService(FilePath, "DGReportExportService.exe");
                    saved = true;
                }
                if (checkBoxDigiScreenSaver.IsChecked == true && !string.IsNullOrEmpty(FilePath))
                {
                    SaveService(FilePath, "DigiScreenSaver.exe");
                    saved = true;
                }
                if (checkBoxManualDownloadProcess.IsChecked == true && !string.IsNullOrEmpty(FilePath))
                {
                    SaveService(FilePath, "ManualDownloadProcess.exe");
                    saved = true;
                }
                if (checkBoxPreSoldService.IsChecked == true && !string.IsNullOrEmpty(FilePath))
                {
                    SaveService(FilePath, "PreSoldService.exe ");
                    saved = true;
                }
                if (saved)
                {
                    System.Windows.MessageBox.Show("Changes saved successfully", "Digi");
                    if (IsConfigUtityChanged)
                        RestartApplication();
                }

            }
            catch (Exception ex)
            {
                LogConfigurator.log.Error("Error Message: " + ex.Message + " Error Stack Trace: " + ex.StackTrace);
            }
        }

        #region Save Functions
        private void SaveEmailService(string fileName)
        {
            fileName += "\\EmailService.exe.config";
            if (!File.Exists(fileName))
                return;//message

            decrypt(FilePath + "\\EmailService.exe");

            WriteXmlData(fileName);

            encrypt(FilePath + "\\EmailService.exe");
        }

        private void SaveEmailApp(string fileName)
        {
            fileName += "\\EmailKiosk.exe.config";
            if (!File.Exists(fileName))
                return;//message

            decrypt(FilePath + "\\EmailKiosk.exe");

            WriteXmlData(fileName);

            encrypt(FilePath + "\\EmailKiosk.exe");
        }

        private void SaveSyncService(string fileName)
        {
            fileName += "\\DataSyncWinService.exe.config";
            if (!File.Exists(fileName))
                return;//message

            decrypt(FilePath + "\\DataSyncWinService.exe");

            WriteXmlData(fileName);

            encrypt(FilePath + "\\DataSyncWinService.exe");
        }

        private void SaveSyncApp(string fileName)
        {
            fileName += "\\DigiSync.exe.config";
            if (!File.Exists(fileName))
                return;//message

            decrypt(FilePath + "\\DigiSync.exe");

            WriteXmlData(fileName);

            encrypt(FilePath + "\\DigiSync.exe");

        }

        private void SaveWifi(string fileName)
        {
            fileName += "\\DigiWifiImageProcessing.exe.config";
            if (!File.Exists(fileName))
                return;//message

            decrypt(FilePath + "\\DigiWifiImageProcessing.exe");

            WriteXmlData(fileName);

            encrypt(FilePath + "\\DigiWifiImageProcessing.exe");
        }

        private void SaveRideCaptureUtility(string fileName)
        {
            fileName += "\\RideCaptureUtility.exe.config";
            if (!File.Exists(fileName))
                return;//message

            decrypt(FilePath + "\\RideCaptureUtility.exe");

            WriteXmlData(fileName);

            encrypt(FilePath + "\\RideCaptureUtility.exe");
        }

        private void SaveRideCameraSetting(string fileName)
        {
            fileName += "\\RideCameraSetting.exe.config";
            if (!File.Exists(fileName))
                return;//message

            decrypt(FilePath + "\\RideCameraSetting.exe");

            WriteXmlData(fileName);

            encrypt(FilePath + "\\RideCameraSetting.exe");
        }

        private void SaveDigiPhoto(string fileName)
        {
            fileName += "\\DigiPhoto.exe.config";
            if (!File.Exists(fileName))
                return;//message

            decrypt(FilePath + "\\DigiPhoto.exe");

            WriteXmlData(fileName);

            encrypt(FilePath + "\\DigiPhoto.exe");
        }

        private void SavePrintConsole(string fileName)
        {
            fileName += "//DigiPrintingConsole.exe.config";
            if (!File.Exists(fileName))
                return;//message

            decrypt(FilePath + "\\DigiPrintingConsole.exe");

            WriteXmlData(fileName);

            encrypt(FilePath + "\\DigiPrintingConsole.exe");
        }

        private void SaveWatcher(string fileName)
        {
            fileName += "//DigiWatcher.exe.config";
            if (!File.Exists(fileName))
                return;//message

            decrypt(FilePath + "\\DigiWatcher.exe");

            WriteXmlData(fileName);

            encrypt(FilePath + "\\DigiWatcher.exe");
        }

        //private void SaveVideoWatcher(string fileName)
        //{
        //    fileName += "//DigiVideoWatcher.exe.config";
        //    if (!File.Exists(fileName))
        //        return;//message

        //    decrypt(FilePath + "\\DigiVideoWatcher.exe");

        //    WriteXmlData(fileName);

        //    encrypt(FilePath + "\\DigiVideoWatcher.exe");
        //}
        private void SaveImageEngine(string fileName)
        {
            fileName += "//ImageProcessingEngine.exe.config";
            if (!File.Exists(fileName))
                return;//message

            decrypt(FilePath + "\\ImageProcessingEngine.exe");

            WriteXmlData(fileName);

            encrypt(FilePath + "\\ImageProcessingEngine.exe");
        }

        private void SaveBackupService(string fileName)
        {
            fileName += "//DigiBackupService.exe.config";
            if (!File.Exists(fileName))
                return;//message

            decrypt(FilePath + "\\DigiBackupService.exe");

            WriteXmlData(fileName);

            encrypt(FilePath + "\\DigiBackupService.exe");
        }
        private void SaveDecryptConfigUtility(string fileName)
        {
            fileName += "//DigiConfigUtility.exe.config";
            if (!File.Exists(fileName))
                return;//message
            decrypt(FilePath + "\\DigiConfigUtility.exe");
            WriteXmlData(fileName);
            encrypt(FilePath + "\\DigiConfigUtility.exe");

        }
        private void SaveVideoProcessor(string fileName)
        {
            fileName += "//VideoProcessingEngine.exe.config";
            if (!File.Exists(fileName))
                return;//message

            decrypt(FilePath + "\\VideoProcessingEngine.exe");

            WriteXmlData(fileName);

            encrypt(FilePath + "\\VideoProcessingEngine.exe");
        }
        private void SaveVideoConsole(string fileName)
        {
            fileName += "//Digiphoto.Manual.VideoConsole.exe.config";
            if (!File.Exists(fileName))
                return;//message

            decrypt(FilePath + "\\Digiphoto.Manual.VideoConsole.exe");

            WriteXmlData(fileName);

            encrypt(FilePath + "\\Digiphoto.Manual.VideoConsole.exe");
        }
        private void SaveImageConsole(string fileName)
        {
            fileName += "//Digiphoto.Manual.Console.exe.config";
            if (!File.Exists(fileName))
                return;//message

            decrypt(FilePath + "\\Digiphoto.Manual.Console.exe");

            WriteXmlData(fileName);

            encrypt(FilePath + "\\Digiphoto.Manual.Console.exe");
        }

        //private void SaveRFIDService(string fileName)
        //{
        //    fileName += "//DigiRfidService.exe.config";
        //    if (!File.Exists(fileName))
        //        return;//message

        //    decrypt(FilePath + "\\DigiRfidService.exe");

        //    WriteXmlData(fileName);

        //    encrypt(FilePath + "\\DigiRfidService.exe");
        //}

        #endregion
        private void SaveService(string fileName, string serviceName)
        {
            fileName += "//" + serviceName + ".config";
            if (!File.Exists(fileName))
                return;//message

            decrypt(FilePath + "\\" + serviceName);

            WriteXmlData(fileName);

            encrypt(FilePath + "\\" + serviceName);
        }

        private void decrypt(string configfileName)
        {
            try
            {
                UnprotectConnectionString(configfileName);
                System.IO.StreamReader oStream = new System.IO.StreamReader(configfileName + ".config", System.Text.Encoding.UTF8);
                xmlData = oStream.ReadToEnd();
                oStream.Close();
                oStream.Dispose();
                oStream = null;

                Save(configfileName + ".config");
            }
            catch (Exception ex)
            {
                LogConfigurator.log.Error("Error Message: " + ex.Message + " Error Stack Trace: " + ex.StackTrace);
            }
        }

        private void UnprotectConnectionString(string configfileName)
        {
            ToggleConnectionStringProtection(configfileName, false);
        }

        private void ProtectConnectionString(string configfileName)
        {
            ToggleConnectionStringProtection(configfileName, true);
        }

        private static void ToggleConnectionStringProtection(string pathName, bool protect)
        {
            // Define the Dpapi provider name.
            string strProvider = "DataProtectionConfigurationProvider";
            // string strProvider = "RSAProtectedConfigurationProvider";

            System.Configuration.Configuration oConfiguration = null;
            System.Configuration.ConnectionStringsSection oSection = null;

            try
            {
                // Open the configuration file and retrieve the connectionStrings section.

                // For Web!
                // oConfiguration = System.Web.Configuration.WebConfigurationManager.OpenWebConfiguration("~");

                // For Windows!
                // Takes the executable file name without the config extension.
                oConfiguration = System.Configuration.ConfigurationManager.OpenExeConfiguration(pathName);

                if (oConfiguration != null)
                {
                    bool blnChanged = false;
                    oSection = oConfiguration.GetSection("connectionStrings") as System.Configuration.ConnectionStringsSection;

                    if (oSection != null)
                    {
                        if ((!(oSection.ElementInformation.IsLocked)) && (!(oSection.SectionInformation.IsLocked)))
                        {
                            if (protect)
                            {
                                if (!(oSection.SectionInformation.IsProtected))
                                {
                                    blnChanged = true;

                                    // Encrypt the section.
                                    oSection.SectionInformation.ProtectSection(strProvider);
                                }
                            }
                            else
                            {
                                if (oSection.SectionInformation.IsProtected)
                                {
                                    blnChanged = true;

                                    // Remove encryption.
                                    oSection.SectionInformation.UnprotectSection();
                                }
                            }
                        }

                        if (blnChanged)
                        {
                            // Indicates whether the associated configuration section will be saved even if it has not been modified.
                            oSection.SectionInformation.ForceSave = true;

                            // Save the current configuration.
                            oConfiguration.Save();
                        }
                    }
                }
            }
            catch (System.Exception ex)
            {
                throw (ex);
            }
            finally
            {
            }
        }

        private void encrypt(string configfileName)
        {
            try
            {
                ProtectConnectionString(configfileName);
                System.IO.StreamReader oStream = new System.IO.StreamReader(configfileName + ".config", System.Text.Encoding.UTF8);
                xmlData = oStream.ReadToEnd();
                oStream.Close();
                oStream.Dispose();
                oStream = null;

                Save(configfileName + ".config");
            }
            catch (Exception ex)
            {
                LogConfigurator.log.Error("Error Message: " + ex.Message + " Error Stack Trace: " + ex.StackTrace);
            }
        }

        private void Save(string configfileName)
        {
            try
            {
                StreamWriter b = new StreamWriter(File.Open(configfileName, FileMode.Create));
                b.Write(xmlData);

                b.Close();

            }
            catch (Exception ex)
            {
                LogConfigurator.log.Error("Error Message: " + ex.Message + " Error Stack Trace: " + ex.StackTrace);
            }

        }

        private void textBoxDataSource_KeyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                FilePath = textBox1.Text;
                LoadExeFromSelectedFolder(FilePath);
                if (File.Exists(FilePath + "\\DigiPhoto.exe") && File.Exists(FilePath + "\\DigiPhoto.exe.config"))
                {
                    decrypt(FilePath + "\\DigiPhoto.exe");
                    ShowXmlData(string.Concat(FilePath, "\\DigiPhoto.exe.config"));
                    encrypt(FilePath + "\\DigiPhoto.exe");
                }
                else
                {
                    System.Windows.MessageBox.Show("File not found.", "Digi");
                }
            }
        }

        private void LoadExeFromSelectedFolder(string fileName)
        {
            //Enable-Disable check box.
            if (File.Exists(fileName + "\\DigiPhoto.exe"))
            {
                checkBoxDigiPhoto.IsEnabled = true;
            }
            else
            {
                checkBoxDigiPhoto.IsChecked = checkBoxDigiPhoto.IsEnabled = false;

            }

            if (File.Exists(fileName + "\\DigiPrintingConsole.exe"))
            {
                checkBoxPrintConsole.IsEnabled = true;
            }
            else
            {
                checkBoxPrintConsole.IsChecked = checkBoxPrintConsole.IsEnabled = false;
            }
            if (File.Exists(fileName + "\\DigiWatcher.exe"))
            {
                checkBoxWatcher.IsEnabled = true;
            }
            else
            {
                checkBoxWatcher.IsChecked = checkBoxWatcher.IsEnabled = false;
            }

            //if (File.Exists(fileName + "\\DigiSync.exe"))
            //{
            //    checkBoxSyncApp.IsEnabled = true;
            //}
            //else
            //{
            //    checkBoxSyncApp.IsChecked = checkBoxSyncApp.IsEnabled = false;
            //}

            if (File.Exists(fileName + "\\DataSyncWinService.exe"))
            {
                checkBoxSyncService.IsEnabled = true;
            }
            else
            {
                checkBoxSyncService.IsChecked = checkBoxSyncService.IsEnabled = false;
            }

            if (File.Exists(fileName + "\\EmailKiosk.exe"))
            {
                checkBoxEmailApp.IsEnabled = true;
            }
            else
            {
                checkBoxEmailApp.IsChecked = checkBoxEmailApp.IsEnabled = false;
            }
            if (File.Exists(fileName + "\\EmailService.exe"))
            {
                checkBoxEmailService.IsEnabled = true;
            }
            else
            {
                checkBoxEmailService.IsChecked = checkBoxEmailService.IsEnabled = false;
            }
            if (File.Exists(fileName + "\\RideCameraSetting.exe"))
            {
                checkBoxRideCameraSetting.IsEnabled = true;
            }
            else
            {
                checkBoxRideCameraSetting.IsChecked = checkBoxRideCameraSetting.IsEnabled = false;
            }
            if (File.Exists(fileName + "\\RideCaptureUtility.exe"))
            {
                checkBoxRideCaptureUtility.IsEnabled = true;
            }
            else
            {
                checkBoxRideCaptureUtility.IsChecked = checkBoxRideCaptureUtility.IsEnabled = false;
            }
            if (File.Exists(fileName + "\\DigiWifiImageProcessing.exe"))
            {
                checkBoxWifi.IsEnabled = true;
            }
            else
            {
                checkBoxWifi.IsChecked = checkBoxWifi.IsEnabled = false;
            }
            //Backup setting
            if (File.Exists(fileName + "\\DigiBackupService.exe"))
            {
                checkBoxBackupService.IsEnabled = true;
            }
            else
            {
                checkBoxBackupService.IsChecked = checkBoxBackupService.IsEnabled = false;
            }

            if (File.Exists(fileName + "\\DigiConfigUtility.exe"))
            {
                checkBoxConfigUtility.IsEnabled = true;
            }
            else
            {
                checkBoxConfigUtility.IsChecked = checkBoxConfigUtility.IsEnabled = false;
            }

            //if (File.Exists(fileName + "\\DigiVideoWatcher.exe"))
            //{
            //    checkBoxVideoWatcher.IsEnabled = true;
            //}
            //else
            //{
            //    checkBoxVideoWatcher.IsChecked = checkBoxVideoWatcher.IsEnabled = false;
            //}
            if (File.Exists(fileName + "\\ImageProcessingEngine.exe"))
            {
                checkBoxImgEngine.IsEnabled = true;
            }
            else
            {
                checkBoxImgEngine.IsChecked = checkBoxImgEngine.IsEnabled = false;
            }
            if (File.Exists(fileName + "\\Digiphoto.Manual.VideoConsole.exe"))
            {
                checkBoxVideoManualDownloadConsole.IsEnabled = true;
            }
            else
            {
                checkBoxVideoManualDownloadConsole.IsChecked = checkBoxImgEngine.IsEnabled = false;
            }
            if (File.Exists(fileName + "\\VideoProcessingEngine.exe"))
            {
                checkBoxVideoProcessor.IsEnabled = true;
            }
            else
            {
                checkBoxVideoProcessor.IsChecked = checkBoxImgEngine.IsEnabled = false;
            }
            if (File.Exists(fileName + "\\Digiphoto.Manual.Console.exe"))
            {
                checkBoxImageManualDownloadConsole.IsEnabled = true;
            }
            else
            {
                checkBoxImageManualDownloadConsole.IsChecked = checkBoxImgEngine.IsEnabled = false;
            }
            if (File.Exists(fileName + "\\DigiRfidService.exe"))
            {
                checkBoxRFIDService.IsEnabled = true;
            }
            else
            {
                checkBoxRFIDService.IsChecked = checkBoxRFIDService.IsEnabled = false;
            }
            if (File.Exists(fileName + "\\DigiRFIDAssociationService.exe"))
            {
                checkBoxRFIDAssociationService.IsEnabled = true;
            }
            else
            {
                checkBoxRFIDAssociationService.IsChecked = checkBoxRFIDAssociationService.IsEnabled = false;
            }
            if (File.Exists(fileName + "\\DigiUpdates.exe"))
            {
                checkBoxDigiUpdates.IsEnabled = true;
            }
            else
            {
                checkBoxDigiUpdates.IsChecked = checkBoxDigiUpdates.IsEnabled = false;
            }
            if (File.Exists(fileName + "\\LiveVideoEditing.exe"))
            {
                checkBoxLiveVideoEditing.IsEnabled = true;
            }
            else
            {
                checkBoxLiveVideoEditing.IsChecked = checkBoxLiveVideoEditing.IsEnabled = false;
            }
            if (File.Exists(fileName + "\\DGReportExportService.exe"))
            {
                checkBoxReportExportService.IsEnabled = true;
            }
            else
            {
                checkBoxReportExportService.IsChecked = checkBoxReportExportService.IsEnabled = false;
            }
            if (File.Exists(fileName + "\\DigiScreenSaver.exe"))
            {
                checkBoxDigiScreenSaver.IsEnabled = true;
            }
            else
            {
                checkBoxDigiScreenSaver.IsChecked = checkBoxDigiScreenSaver.IsEnabled = false;
            }
            if (File.Exists(fileName + "\\ManualDownloadProcess.exe"))
            {
                checkBoxManualDownloadProcess.IsEnabled = true;
            }
            else
            {
                checkBoxManualDownloadProcess.IsChecked = checkBoxManualDownloadProcess.IsEnabled = false;
            }
            if (File.Exists(fileName + "\\PreSoldService.exe"))
            {
                checkBoxPreSoldService.IsEnabled = true;
            }
            else
            {
                checkBoxPreSoldService.IsChecked = checkBoxPreSoldService.IsEnabled = false;
            }
        }


        public static IEnumerable<T> FindVisualChildren<T>(DependencyObject depObj) where T : DependencyObject
        {
            if (depObj != null)
            {
                for (int i = 0; i < VisualTreeHelper.GetChildrenCount(depObj); i++)
                {
                    DependencyObject child = VisualTreeHelper.GetChild(depObj, i);
                    if (child != null && child is T)
                    {
                        yield return (T)child;
                    }

                    foreach (T childOfChild in FindVisualChildren<T>(child))
                    {
                        yield return childOfChild;
                    }
                }
            }
        }

        private void RestartApplication()
        {
            try
            {
                string msg = "To reflect the changed Config Utility ConnectionString, Please re-start the application.";
                System.Windows.MessageBox.Show(msg);
                //if (System.Windows.MessageBox.Show(msg, "", MessageBoxButton.YesNo) == MessageBoxResult.Yes)
                //{
                //    System.Windows.Forms.Application.Restart();
                //    System.Windows.Application.Current.Shutdown();
                //}
            }
            catch (Exception ex)
            {
                LogConfigurator.log.Error("Error Message: " + ex.Message + " Error Stack Trace: " + ex.StackTrace);
            }
        }


        private void GetConfigConnectionString(string ApplicationName)
        {
            try
            {
                if (File.Exists(FilePath + ApplicationName) && File.Exists(FilePath + ApplicationName + ".config"))
                {
                    decrypt(FilePath + ApplicationName);
                    ShowXmlData(string.Concat(textBox1.Text.Trim(), ApplicationName + ".config"));
                    encrypt(FilePath + ApplicationName);
                }
                else
                {
                    textBoxDataSource.Text = string.Empty;
                    textBoxInitialCatalog.Text = string.Empty;
                    textBoxPassword.Text = string.Empty;
                    textBoxUserID.Text = string.Empty;
                }
            }
            catch (Exception ex)
            {
                LogConfigurator.log.Error("Error Message: " + ex.Message + " Error Stack Trace: " + ex.StackTrace);
            }
        }

        private void TagApplicationNameWithCheckBox()
        {
            checkBoxConfigUtility.Tag = "\\DigiConfigUtility.exe";
            checkBoxWifi.Tag = "\\DigiWifiImageProcessing.exe";
            checkBoxDigiPhoto.Tag = "\\DigiPhoto.exe";
            checkBoxPrintConsole.Tag = "\\DigiPrintingConsole.exe";
            //   checkBoxSyncApp.Tag = "\\DigiSync.exe";
            checkBoxSyncService.Tag = "\\DataSyncWinService.exe";
            checkBoxWatcher.Tag = "\\DigiWatcher.exe";
            checkBoxRideCaptureUtility.Tag = "\\RideCaptureUtility.exe";
            checkBoxRideCameraSetting.Tag = "\\RideCameraSetting.exe";
            checkBoxEmailService.Tag = "\\EmailService.exe";
            checkBoxEmailApp.Tag = "\\EmailKiosk.exe";
            checkBoxBackupService.Tag = "\\DigiBackupService.exe";
            checkBoxImgEngine.Tag = "\\ImageProcessingEngine.exe";
            checkBoxVideoManualDownloadConsole.Tag = "\\Digiphoto.Manual.VideoConsole.exe";
            checkBoxVideoProcessor.Tag = "VideoProcessingEngine.exe";
            checkBoxImageManualDownloadConsole.Tag = "\\Digiphoto.Manual.Console.exe";
            checkBoxRFIDService.Tag = "\\DigiRfidService.exe";
            checkBoxRFIDAssociationService.Tag = "\\DigiRFIDAssociationService.exe";
            checkBoxDigiUpdates.Tag = "\\DigiUpdates.exe";
            checkBoxLiveVideoEditing.Tag = "\\LiveVideoEditing.exe";
            checkBoxReportExportService.Tag = "\\DGReportExportService.exe";
            checkBoxDigiScreenSaver.Tag = "\\DigiScreenSaver.exe";
            checkBoxManualDownloadProcess.Tag = "\\ManualDownloadProcess.exe";
            checkBoxPreSoldService.Tag = "\\PreSoldService.exe";
        }


        private void checkBoxConfigUtility_Checked(object sender, RoutedEventArgs e)
        {
            CheckUncheckSelectAll(((System.Windows.Controls.CheckBox)sender).IsChecked == true ? true : false);
            GetConfigConnectionString("\\DigiConfigUtility.exe");
        }

        private void checkBoxWifi_Checked(object sender, RoutedEventArgs e)
        {
            CheckUncheckSelectAll(((System.Windows.Controls.CheckBox)sender).IsChecked == true ? true : false);
            GetConfigConnectionString("\\DigiWifiImageProcessing.exe");
        }

        private void checkBoxDigiPhoto_Checked(object sender, RoutedEventArgs e)
        {
            CheckUncheckSelectAll(((System.Windows.Controls.CheckBox)sender).IsChecked == true ? true : false);
            GetConfigConnectionString("\\DigiPhoto.exe");
        }

        private void checkBoxPrintConsole_Checked(object sender, RoutedEventArgs e)
        {
            CheckUncheckSelectAll(((System.Windows.Controls.CheckBox)sender).IsChecked == true ? true : false);
            GetConfigConnectionString("\\DigiPrintingConsole.exe");
        }

        //private void checkBoxSyncApp_Checked(object sender, RoutedEventArgs e)
        //{
        //    GetConfigConnectionString("\\DigiSync.exe");
        //}

        private void checkBoxSyncService_Checked(object sender, RoutedEventArgs e)
        {
            CheckUncheckSelectAll(((System.Windows.Controls.CheckBox)sender).IsChecked == true ? true : false);
            GetConfigConnectionString("\\DataSyncWinService.exe");
        }

        private void checkBoxWatcher_Checked(object sender, RoutedEventArgs e)
        {
            CheckUncheckSelectAll(((System.Windows.Controls.CheckBox)sender).IsChecked == true ? true : false);
            GetConfigConnectionString("\\DigiWatcher.exe");
        }

        private void checkBoxRideCaptureUtility_Checked(object sender, RoutedEventArgs e)
        {
            CheckUncheckSelectAll(((System.Windows.Controls.CheckBox)sender).IsChecked == true ? true : false);
            GetConfigConnectionString("\\RideCaptureUtility.exe");

        }

        private void checkBoxRideCameraSetting_Checked(object sender, RoutedEventArgs e)
        {
            CheckUncheckSelectAll(((System.Windows.Controls.CheckBox)sender).IsChecked == true ? true : false);
            GetConfigConnectionString("\\RideCameraSetting.exe");
        }

        private void checkBoxEmailService_Checked(object sender, RoutedEventArgs e)
        {
            CheckUncheckSelectAll(((System.Windows.Controls.CheckBox)sender).IsChecked == true ? true : false);
            GetConfigConnectionString("\\EmailService.exe");
        }

        private void checkBoxEmailApp_Checked(object sender, RoutedEventArgs e)
        {
            CheckUncheckSelectAll(((System.Windows.Controls.CheckBox)sender).IsChecked == true ? true : false);
            GetConfigConnectionString("\\EmailKiosk.exe");
        }

        private void checkBoxBackupService_Checked(object sender, RoutedEventArgs e)
        {
            CheckUncheckSelectAll(((System.Windows.Controls.CheckBox)sender).IsChecked == true ? true : false);
            GetConfigConnectionString("\\DigiBackupService.exe");
        }

        private void checkBox_Unchecked(object sender, RoutedEventArgs e)
        {
            CheckUncheckSelectAll(((System.Windows.Controls.CheckBox)sender).IsChecked == true ? true : false);
            System.Windows.Controls.CheckBox chkBox = FindVisualChildren<System.Windows.Controls.CheckBox>(this).Where(o => o.IsChecked == true).OrderBy(or => or.Content).FirstOrDefault();
            if (chkBox == null)
                GetConfigConnectionString(checkBoxDigiPhoto.Tag.ToString());
            else
                GetConfigConnectionString(chkBox.Tag.ToString());
        }

        //private void checkBoxVideoWatcher_Checked(object sender, RoutedEventArgs e)
        //{
        //    GetConfigConnectionString("\\DigiVideoWatcher.exe");
        //}

        private void checkBoxImgEngine_Checked(object sender, RoutedEventArgs e)
        {
            CheckUncheckSelectAll(((System.Windows.Controls.CheckBox)sender).IsChecked == true ? true : false);
            GetConfigConnectionString("\\ImageProcessingEngine.exe");
        }

        private void checkBoxVideoManualDownloadConsole_Checked(object sender, RoutedEventArgs e)
        {
            CheckUncheckSelectAll(((System.Windows.Controls.CheckBox)sender).IsChecked == true ? true : false);
            GetConfigConnectionString("\\Digiphoto.Manual.VideoConsole.exe");
        }

        private void checkBoxVideoProcessor_Checked(object sender, RoutedEventArgs e)
        {
            CheckUncheckSelectAll(((System.Windows.Controls.CheckBox)sender).IsChecked == true ? true : false);
            GetConfigConnectionString("\\VideoProcessingEngine.exe");
        }

        private void checkBoxImageManualDownloadConsole_Checked(object sender, RoutedEventArgs e)
        {
            CheckUncheckSelectAll(((System.Windows.Controls.CheckBox)sender).IsChecked == true ? true : false);
            GetConfigConnectionString("\\Digiphoto.Manual.Console.exe");
        }
        private void checkBoxRFIDService_Checked(object sender, RoutedEventArgs e)
        {
            CheckUncheckSelectAll(((System.Windows.Controls.CheckBox)sender).IsChecked == true ? true : false);
            GetConfigConnectionString("\\DigiRfidService.exe");
        }
        private void checkBoxRFIDAssociationService_Checked(object sender, RoutedEventArgs e)
        {
            CheckUncheckSelectAll(((System.Windows.Controls.CheckBox)sender).IsChecked == true ? true : false);
            GetConfigConnectionString("\\DigiRFIDAssociationService.exe");
        }

        private void checkBoxDigiUpdates_Checked(object sender, RoutedEventArgs e)
        {
            CheckUncheckSelectAll(((System.Windows.Controls.CheckBox)sender).IsChecked == true ? true : false);
            GetConfigConnectionString("\\DigiUpdates.exe");
        }
        private void checkBoxLiveVideoEditing_Checked(object sender, RoutedEventArgs e)
        {
            CheckUncheckSelectAll(((System.Windows.Controls.CheckBox)sender).IsChecked == true ? true : false);
            GetConfigConnectionString("\\LiveVideoEditing.exe");
        }
        private void checkBoxReportExportService_Checked(object sender, RoutedEventArgs e)
        {
            CheckUncheckSelectAll(((System.Windows.Controls.CheckBox)sender).IsChecked == true ? true : false);
            GetConfigConnectionString("\\DGReportExportService.exe");
        }
        private void checkBoxDigiScreenSaver_Checked(object sender, RoutedEventArgs e)
        {
            CheckUncheckSelectAll(((System.Windows.Controls.CheckBox)sender).IsChecked == true ? true : false);
            GetConfigConnectionString("\\DigiScreenSaver.exe");
        }
        private void checkBoxVideoManualDownloadProcess_Checked(object sender, RoutedEventArgs e)
        {
            CheckUncheckSelectAll(((System.Windows.Controls.CheckBox)sender).IsChecked == true ? true : false);
            GetConfigConnectionString("\\ManualDownloadProcess.exe");
        }
        private void checkBoxPreSoldService_Checked(object sender, RoutedEventArgs e)
        {
            CheckUncheckSelectAll(((System.Windows.Controls.CheckBox)sender).IsChecked == true ? true : false);
            GetConfigConnectionString("\\PreSoldService.exe");
        }

        private void chkSelectAll_Click(object sender, RoutedEventArgs e)
        {
            if (chkSelectAll.IsChecked == true)
            {
                selectedExeCount = totalExeCount;
                TagApplicationNameWithCheckBox(true);
            }
            else
            {
                selectedExeCount = 0;
                TagApplicationNameWithCheckBox(false);
            }
        }
        private void TagApplicationNameWithCheckBox(bool Ischecked)
        {
            checkBoxConfigUtility.IsChecked = checkBoxConfigUtility.IsEnabled ? Ischecked : false;
            checkBoxWifi.IsChecked = checkBoxWifi.IsEnabled ? Ischecked : false;
            checkBoxDigiPhoto.IsChecked = checkBoxDigiPhoto.IsEnabled ? Ischecked : false;
            checkBoxPrintConsole.IsChecked = checkBoxPrintConsole.IsEnabled ? Ischecked : false;
            checkBoxSyncService.IsChecked = checkBoxSyncService.IsEnabled ? Ischecked : false;
            checkBoxWatcher.IsChecked = checkBoxWatcher.IsEnabled ? Ischecked : false;
            checkBoxRideCaptureUtility.IsChecked = checkBoxRideCaptureUtility.IsEnabled ? Ischecked : false;
            checkBoxRideCameraSetting.IsChecked = checkBoxRideCameraSetting.IsEnabled ? Ischecked : false;
            checkBoxEmailService.IsChecked = checkBoxEmailService.IsEnabled ? Ischecked : false;
            checkBoxEmailApp.IsChecked = checkBoxEmailApp.IsEnabled ? Ischecked : false;
            checkBoxBackupService.IsChecked = checkBoxBackupService.IsEnabled ? Ischecked : false;
            checkBoxImgEngine.IsChecked = checkBoxImgEngine.IsEnabled ? Ischecked : false;
            checkBoxVideoManualDownloadConsole.IsChecked = checkBoxVideoManualDownloadConsole.IsEnabled ? Ischecked : false;
            checkBoxVideoProcessor.IsChecked = checkBoxVideoProcessor.IsEnabled ? Ischecked : false;
            checkBoxImageManualDownloadConsole.IsChecked = checkBoxImageManualDownloadConsole.IsEnabled ? Ischecked : false;
            checkBoxRFIDService.IsChecked = checkBoxRFIDService.IsEnabled ? Ischecked : false;
            checkBoxRFIDAssociationService.IsChecked = checkBoxRFIDAssociationService.IsEnabled ? Ischecked : false;
            checkBoxDigiUpdates.IsChecked = checkBoxDigiUpdates.IsEnabled ? Ischecked : false;
            checkBoxLiveVideoEditing.IsChecked = checkBoxLiveVideoEditing.IsEnabled ? Ischecked : false;
            checkBoxReportExportService.IsChecked = checkBoxReportExportService.IsEnabled ? Ischecked : false;
            checkBoxDigiScreenSaver.IsChecked = checkBoxDigiScreenSaver.IsEnabled ? Ischecked : false;
            checkBoxManualDownloadProcess.IsChecked = checkBoxManualDownloadProcess.IsEnabled ? Ischecked : false;
            checkBoxPreSoldService.IsChecked = checkBoxPreSoldService.IsEnabled ? Ischecked : false;
        }

        public void CheckUncheckSelectAll(bool value)
        {
            if ((selectedExeCount == totalExeCount && value) || selectedExeCount == 0 && !value)
                return;
            if (value == true)
                selectedExeCount++;
            else
                selectedExeCount--;
            if (selectedExeCount >= totalExeCount)
                chkSelectAll.IsChecked = true;
            else
                chkSelectAll.IsChecked = false;
        }
    }
}
