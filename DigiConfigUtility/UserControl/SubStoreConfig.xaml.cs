﻿using DigiConfigUtility.Utility;
using DigiPhoto;
using DigiPhoto.DataLayer;
//using DigiPhoto.DataLayer.Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using DigiPhoto.IMIX.Business;
using DigiPhoto.IMIX.Model;
using DigiPhoto.Utility.Repository.ValueType;
using System.Text.RegularExpressions;
namespace DigiConfigUtility
{
    /// <summary>
    /// Interaction logic for SubStoreConfig.xaml
    /// </summary>
    public partial class SubStoreConfig : System.Windows.Controls.UserControl
    {
        //DigiPhotoDataServices _objDataLayer = null;
        public SubStoreConfig()
        {
            InitializeComponent();
            FillSubstore();
            FillTime();
            FillMinute();
            GetBackImageSettings();
        }

        /// <summary>
        /// Fills the substore.
        /// </summary>
        private void FillSubstore()
        {
            try
            {
                //_objDataLayer = new DigiPhotoDataServices();
                Dictionary<string, string> lstSubStore = new Dictionary<string, string>();
                StoreSubStoreDataBusniess stoBiz = new StoreSubStoreDataBusniess();
                var list = stoBiz.GetSubstoreData();
                lstSubStore.Add("-Select-", "0");
                foreach (var item in list)
                {
                    lstSubStore.Add(item.DG_SubStore_Name, item.DG_SubStore_pkey.ToString());
                }
                cmbSelectSubstore.ItemsSource = lstSubStore;
                cmbSelectSubstore.SelectedValue = Config.SubStoreId.ToString();
            }
            catch (Exception ex)
            {
                LogConfigurator.log.Error("Error Message: " + ex.Message + " Error Stack Trace: " + ex.StackTrace);
            }
        }

        /// <summary>
        /// Handles the Click event of the btnSaveSubStore control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void btnSaveSubStore_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (cmbSelectSubstore.SelectedValue == null || cmbSelectSubstore.SelectedValue.ToInt32() <= 0)
                {
                    MessageBox.Show("Select substore", "Digiphoto", MessageBoxButton.OK, MessageBoxImage.Error);
                    return;
                }
                string pathtosave = Environment.CurrentDirectory;
                if (File.Exists(pathtosave + "\\ss.dat"))
                {
                    File.Delete(pathtosave + "\\ss.dat");
                }
                using (StreamWriter b = new StreamWriter(File.Open(pathtosave + "\\ss.dat", FileMode.Create)))
                {

                    string substoresearch = "";
                    b.Write(DigiPhoto.CryptorEngine.Encrypt(cmbSelectSubstore.SelectedValue.ToString() + substoresearch, true));
                    b.Close();
                    Config.SubStoreId = cmbSelectSubstore.SelectedValue.ToInt32();
                    ConfigManager.IMIXConfigurations.Clear();
                    MessageBox.Show("Substore info saved successfully.");
                    //Refresh the tabs
                    Window windw = Window.GetWindow(this);
                    MainWindow mainWindow = new MainWindow();
                    MainWindow.IsRestart = true;
                    mainWindow.Show();
                    // mainWindow.tabSubStore.IsSelected = true;
                    var matchingItem = mainWindow.MainTab.Items.Cast<TabItem>()
                                        .Where(item => item.Name == "tabSubStore")
                                        .FirstOrDefault();
                    matchingItem.IsSelected = true;
                    windw.Close();
                }
            }
            catch (Exception ex)
            {
                LogConfigurator.log.Error("Error Message: " + ex.Message + " Error Stack Trace: " + ex.StackTrace);
            }
        }



        private void GetBackImageSettings()
        {
            try
            {
                ConfigBusiness conBiz = new ConfigBusiness();

                //_objDataLayer = new DigiPhotoDataServices();
                //to filter 
                List<long> objList = new List<long>();
                objList.Add((long)ConfigParams.IsDefaultBackImageEnabled);
                objList.Add((long)ConfigParams.DefaultBackImagePath);
                objList.Add((long)ConfigParams.IsVersionUpdateEnabled);
                objList.Add((long)ConfigParams.UpdateServicePath);
                objList.Add((long)ConfigParams.IsPhotographerSerailSearchActive);
                objList.Add((long)ConfigParams.ImageBarCodeColor);
                objList.Add((long)ConfigParams.BarCodeOrientation);
                objList.Add((long)ConfigParams.BarCodePostion);
                objList.Add((long)ConfigParams.IsEventEmailPackage);
                objList.Add((long)ConfigParams.ValidateQRCode);
                objList.Add((long)ConfigParams.IsOpeningProcMendatory);
                objList.Add((long)ConfigParams.SiteRevenueForSellingLocation);
                objList.Add((long)ConfigParams.ClosingProcTime);
                objList.Add((long)ConfigParams.IsPrintButtonVisible);
                objList.Add((long)(conBiz.GETEvoucherID()));////added by latika for evoucher 25 march 20202

                #region addded by ajay
                objList.Add((long)ConfigParams.IsClientViewWatermark);
                #endregion
               ///// ConfigBusiness conBiz = new ConfigBusiness();
                List<iMIXConfigurationInfo> ConfigValuesList = conBiz.GetNewConfigValues(Config.SubStoreId).Where(o => objList.Contains(o.IMIXConfigurationMasterId)).ToList();
                if (ConfigValuesList != null || ConfigValuesList.Count > 0)
                {
                    for (int i = 0; i < ConfigValuesList.Count; i++)
                    {
                        switch (ConfigValuesList[i].IMIXConfigurationMasterId)
                        {
                            //33
                            case (int)ConfigParams.IsDefaultBackImageEnabled:
                                ChkDefaultBackImage.IsChecked = ConfigValuesList[i].ConfigurationValue.ToBoolean();
                                SetBackImagePathVisibility(ChkDefaultBackImage.IsChecked);

                                break;


                            case (int)ConfigParams.DefaultBackImagePath:
                                txtBackImagePath.Text = ConfigValuesList[i].ConfigurationValue != null ? ConfigValuesList[i].ConfigurationValue : "";
                                if (!string.IsNullOrEmpty(txtBackImagePath.Text.Trim()))
                                    imgBackImage.Source = new BitmapImage(new Uri(txtBackImagePath.Text.Trim(), UriKind.Absolute));
                                break;
                            case (int)ConfigParams.IsVersionUpdateEnabled:
                                ChkNeedVersionUpdate.IsChecked = ConfigValuesList[i].ConfigurationValue.ToBoolean();
                                SetVersionUpdateService(ChkNeedVersionUpdate.IsChecked);
                                break;
                            case (int)ConfigParams.UpdateServicePath:
                                txtVersionUpdateServicePath.Text = ConfigValuesList[i].ConfigurationValue != null ? ConfigValuesList[i].ConfigurationValue : "";
                                break;
                            case (int)ConfigParams.IsPhotographerSerailSearchActive:
                                ChkIsSearialSearchActive.IsChecked = ConfigValuesList[i].ConfigurationValue != null ? ConfigValuesList[i].ConfigurationValue.ToBoolean() : false;
                                break;
                            case (int)ConfigParams.ImageBarCodeColor:
                                System.Windows.Media.Color color = ((System.Windows.Media.Color)System.Windows.Media.ColorConverter.ConvertFromString(ConfigValuesList[i].ConfigurationValue));
                                cmbSelectColor.SelectedColor = color;
                                break;
                            case (int)ConfigParams.BarCodeOrientation:
                                cmbBarcodeOrientation.Text = ConfigValuesList[i].ConfigurationValue;
                                break;
                            case (int)ConfigParams.BarCodePostion:
                                cmbBarcodePostion.Text = ConfigValuesList[i].ConfigurationValue;
                                break;
                            case (int)ConfigParams.IsEventEmailPackage:
                                ChkIsEventEmailPackage.IsChecked = ConfigValuesList[i].ConfigurationValue != null ? ConfigValuesList[i].ConfigurationValue.ToBoolean() : false;
                                break;
                            case (int)ConfigParams.ValidateQRCode:
                                ChkValidateQRCode.IsChecked = ConfigValuesList[i].ConfigurationValue != null ? ConfigValuesList[i].ConfigurationValue.ToBoolean() : false;
                                break;
                            case (int)ConfigParams.IsOpeningProcMendatory:
                                ChkIsOpeningProcMendatory.IsChecked = ConfigValuesList[i].ConfigurationValue != null ? ConfigValuesList[i].ConfigurationValue.ToBool() : false;
                                break;
                            case (int)ConfigParams.SiteRevenueForSellingLocation:
                                ChkIsSiteRevenueForSellingLocation.IsChecked = ConfigValuesList[i].ConfigurationValue != null ? ConfigValuesList[i].ConfigurationValue.ToBool() : false;
                                break;
                            case (int)ConfigParams.ClosingProcTime:
                                string[] arr = ConfigValuesList[i].ConfigurationValue.ToString().Split(' ');
                                cmbTime.Text = arr[0];
                                cmbMin.Text = arr[1];
                                cmbTimeDiff.Text = arr[2];
                                break;
                            case (int)ConfigParams.IsPrintButtonVisible:
                                ChkIsPrintButtonVisible.IsChecked = ConfigValuesList[i].ConfigurationValue != null ? ConfigValuesList[i].ConfigurationValue.ToBoolean() : false;
                                break;

                            case (int)ConfigParams.IsClientViewWatermark:
                                ChkIsClientViewWatermark.IsChecked = ConfigValuesList[i].ConfigurationValue.ToBoolean();

                                break;

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LogConfigurator.log.Error("Error Message: " + ex.Message + " Error Stack Trace: " + ex.StackTrace);
            }
        }
        public void FillTime()
        {
            cmbTime.Items.Clear();
            cmbTime.Items.Add("Hour");
            for (int i = 1; i <= 12; i++)
            {
                cmbTime.Items.Add(i);
            }
            cmbTime.SelectedIndex = 0;
        }
        public void FillMinute()
        {
            cmbMin.Items.Clear();
            cmbMin.Items.Add("Minute");
            for (int i = 0; i < 60; i++)
            {
                cmbMin.Items.Add(i);
            }
            cmbMin.SelectedIndex = 0;
        }

        private void btnBackImagePath_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog();
                // Set filter for file extension and default file extension 
                dlg.DefaultExt = ".*";// ".png";
                dlg.Filter = "JPEG Files (*.jpeg)|*.jpeg|PNG Files (*.png)|*.png|JPG Files (*.jpg)|*.jpg|GIF Files (*.gif)|*.gif";
                Nullable<bool> result = dlg.ShowDialog();
                // Get the selected file name and display in a TextBox 
                if (result == true)
                {
                    // Open document 
                    string filename = dlg.FileName;
                    txtBackImagePath.Text = filename;
                }
            }
            catch (Exception ex)
            {
                LogConfigurator.log.Error("Error Message: " + ex.Message + " Error Stack Trace: " + ex.StackTrace);
            }
        }



        private void btnSaveBackImage_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (ChkDefaultBackImage.IsChecked == true && string.IsNullOrEmpty(txtBackImagePath.Text.Trim()))
                {
                    MessageBox.Show("Select default background image.");
                    return;
                }
                if (ChkNeedVersionUpdate.IsChecked == true && string.IsNullOrEmpty(txtVersionUpdateServicePath.Text.Trim()))
                {
                    MessageBox.Show("Enter version update service path.");
                    return;
                }
                if (Config.SubStoreId <= 0)
                {
                    MessageBox.Show("Please configure your SubStore.");
                    return;
                }
                if (ChkIsSiteRevenueForSellingLocation.IsChecked == true)////changed by latika for validation
                {
                    if (cmbTime.SelectedIndex == 0)
                    {
                        MessageBox.Show("Please select closing form cut off time hour.");
                        return;
                    }
                    if (cmbMin.SelectedIndex == 0)
                    {
                        MessageBox.Show("Please select closing form cut off time minute.");
                        return;
                    }
                }
                else { cmbMin.SelectedIndex = 0; cmbTime.SelectedIndex = 0; }///changed by latika end
                string BackImagePath = ChkDefaultBackImage.IsChecked.Value == true ? txtBackImagePath.Text.Trim() : string.Empty;
                string ServicePath = ChkNeedVersionUpdate.IsChecked.Value == true ? txtVersionUpdateServicePath.Text.Trim() : string.Empty;

                /// bool IsSaved = SaveConfigData(ChkDefaultBackImage.IsChecked.Value.ToString(), BackImagePath, ChkNeedVersionUpdate.IsChecked.Value.ToString(), ServicePath, ChkIsPrintButtonVisible.IsChecked.Value.ToString());
                bool IsSaved = SaveConfigData(ChkDefaultBackImage.IsChecked.Value.ToString(), BackImagePath, ChkNeedVersionUpdate.IsChecked.Value.ToString(), ServicePath, ChkIsPrintButtonVisible.IsChecked.Value.ToString(), ChkIsEvoucher.IsChecked.ToString());

                if (IsSaved)
                {
                    ///MessageBox.Show("Settings saved successfully.");commented by latika 2019 oct 24
                    if (cmbMin.SelectedIndex > 0) { MessageBox.Show("Settings saved successfully." + Environment.NewLine + " Please restart iMix to reflect the recent time changes in closing from."); }
                    else { MessageBox.Show("Settings saved successfully."); }
                    ///end by latika 
                    GetBackImageSettings();
                }
                else
                    MessageBox.Show("Some problem occured try again.");
            }
            catch (Exception ex)
            {
                LogConfigurator.log.Error("Error Message: " + ex.Message + " Error Stack Trace: " + ex.StackTrace);
            }
        }

        /// private bool SaveConfigData(string IsDefaultBackImageEnabled, string DefaultBackImagePath, string IsVersionUpdateEnabled, string VersionUpdateServicePath, string isPrintButtonVisible)
        private bool SaveConfigData(string IsDefaultBackImageEnabled, string DefaultBackImagePath, string IsVersionUpdateEnabled, string VersionUpdateServicePath, string isPrintButtonVisible, string isEvoucher)
        {
            try
            {

                List<iMIXConfigurationInfo> objConfigList = new List<iMIXConfigurationInfo>();
                iMIXConfigurationInfo ConfigValue = null;
                //_objDataLayer = new DigiPhotoDataServices();

                ConfigValue = new iMIXConfigurationInfo();
                ConfigValue.ConfigurationValue = IsDefaultBackImageEnabled;
                ConfigValue.IMIXConfigurationMasterId = (int)ConfigParams.IsDefaultBackImageEnabled;
                ConfigValue.SubstoreId = Config.SubStoreId;
                objConfigList.Add(ConfigValue);


                ConfigValue = new iMIXConfigurationInfo();
                ConfigValue.ConfigurationValue = DefaultBackImagePath;
                ConfigValue.IMIXConfigurationMasterId = (int)ConfigParams.DefaultBackImagePath;
                ConfigValue.SubstoreId = Config.SubStoreId;
                objConfigList.Add(ConfigValue);


                ConfigValue = new iMIXConfigurationInfo();
                ConfigValue.ConfigurationValue = IsVersionUpdateEnabled;
                ConfigValue.IMIXConfigurationMasterId = (int)ConfigParams.IsVersionUpdateEnabled;
                ConfigValue.SubstoreId = Config.SubStoreId;
                objConfigList.Add(ConfigValue);

                ConfigValue = new iMIXConfigurationInfo();
                ConfigValue.ConfigurationValue = VersionUpdateServicePath;
                ConfigValue.IMIXConfigurationMasterId = (int)ConfigParams.UpdateServicePath;
                ConfigValue.SubstoreId = Config.SubStoreId;
                objConfigList.Add(ConfigValue);

                ConfigValue = new iMIXConfigurationInfo();
                ConfigValue.ConfigurationValue = (ChkIsSearialSearchActive.IsChecked == true ? true : false).ToString();
                ConfigValue.IMIXConfigurationMasterId = (int)ConfigParams.IsPhotographerSerailSearchActive;
                ConfigValue.SubstoreId = Config.SubStoreId;
                objConfigList.Add(ConfigValue);


                ConfigValue = new iMIXConfigurationInfo();
                ConfigValue.ConfigurationValue = cmbSelectColor.SelectedColor.ToString();
                ConfigValue.IMIXConfigurationMasterId = (int)ConfigParams.ImageBarCodeColor;
                ConfigValue.SubstoreId = Config.SubStoreId;
                objConfigList.Add(ConfigValue);

                ConfigValue = new iMIXConfigurationInfo();
                ConfigValue.ConfigurationValue = cmbBarcodeOrientation.Text;
                ConfigValue.IMIXConfigurationMasterId = (int)ConfigParams.BarCodeOrientation;
                ConfigValue.SubstoreId = Config.SubStoreId;
                objConfigList.Add(ConfigValue);

                ConfigValue = new iMIXConfigurationInfo();
                ConfigValue.ConfigurationValue = cmbBarcodePostion.Text;
                ConfigValue.IMIXConfigurationMasterId = (int)ConfigParams.BarCodePostion;
                ConfigValue.SubstoreId = Config.SubStoreId;
                objConfigList.Add(ConfigValue);


                ConfigValue = new iMIXConfigurationInfo();
                ConfigValue.ConfigurationValue = (ChkIsEventEmailPackage.IsChecked == true ? true : false).ToString();
                ConfigValue.IMIXConfigurationMasterId = (int)ConfigParams.IsEventEmailPackage;
                ConfigValue.SubstoreId = Config.SubStoreId;
                objConfigList.Add(ConfigValue);

                ConfigValue = new iMIXConfigurationInfo();
                ConfigValue.ConfigurationValue = (ChkValidateQRCode.IsChecked == true ? true : false).ToString();
                ConfigValue.IMIXConfigurationMasterId = (int)ConfigParams.ValidateQRCode;
                ConfigValue.SubstoreId = Config.SubStoreId;
                objConfigList.Add(ConfigValue);

                ConfigValue = new iMIXConfigurationInfo();
                ConfigValue.ConfigurationValue = (ChkIsClientViewWatermark.IsChecked == true ? true : false).ToString();
                ConfigValue.IMIXConfigurationMasterId = (int)ConfigParams.IsClientViewWatermark;
                ConfigValue.SubstoreId = Config.SubStoreId;
                objConfigList.Add(ConfigValue);

                ConfigValue = new iMIXConfigurationInfo();
                ConfigValue.ConfigurationValue = (ChkIsOpeningProcMendatory.IsChecked == true ? "1" : "0").ToString();
                ConfigValue.IMIXConfigurationMasterId = (int)ConfigParams.IsOpeningProcMendatory;
                ConfigValue.SubstoreId = Config.SubStoreId;
                objConfigList.Add(ConfigValue);

                ConfigValue = new iMIXConfigurationInfo();
                ConfigValue.ConfigurationValue = (ChkIsSiteRevenueForSellingLocation.IsChecked == true ? "1" : "0").ToString();
                ConfigValue.IMIXConfigurationMasterId = (int)ConfigParams.SiteRevenueForSellingLocation;
                ConfigValue.SubstoreId = Config.SubStoreId;
                objConfigList.Add(ConfigValue);

                ConfigValue = new iMIXConfigurationInfo();
                ConfigValue.ConfigurationValue = cmbTime.SelectedItem.ToString() + " " + cmbMin.SelectedItem.ToString() + " " + ((ComboBoxItem)cmbTimeDiff.SelectedItem).Content.ToString();
                ConfigValue.IMIXConfigurationMasterId = (int)ConfigParams.ClosingProcTime;
                ConfigValue.SubstoreId = Config.SubStoreId;
                objConfigList.Add(ConfigValue);

                ConfigValue = new iMIXConfigurationInfo();
                ConfigValue.ConfigurationValue = isPrintButtonVisible;
                ConfigValue.IMIXConfigurationMasterId = (int)ConfigParams.IsPrintButtonVisible;
                ConfigValue.SubstoreId = Config.SubStoreId;
                objConfigList.Add(ConfigValue);
                ConfigBusiness conBiz = new ConfigBusiness();
                ///-------------------------------------created by latika for Evoucher 31-Jan-2020-----///
                ConfigValue = new iMIXConfigurationInfo();
                ConfigValue.ConfigurationValue = isEvoucher;
                ConfigValue.IMIXConfigurationMasterId = (int)conBiz.GETEvoucherID();
                ConfigValue.SubstoreId = Config.SubStoreId;
                objConfigList.Add(ConfigValue);
                ////end by latika

                
                bool IsSaved = conBiz.SaveUpdateNewConfig(objConfigList);
                return IsSaved;
            }
            catch (Exception ex)
            {
                LogConfigurator.log.Error("Error Message: " + ex.Message + " Error Stack Trace: " + ex.StackTrace);
                return false;
            }
        }

        private void ChkDefaultBackImage_Click(object sender, RoutedEventArgs e)
        {
            SetBackImagePathVisibility(((CheckBox)sender).IsChecked);
        }

        private void SetBackImagePathVisibility(bool? IsDefaultSet)
        {
            if (IsDefaultSet == true)
            {
                stkBackImagePath.Visibility = Visibility.Visible;
            }
            else
                stkBackImagePath.Visibility = Visibility.Collapsed;
        }

        private void ChkNeedVersionUpdate_Click(object sender, RoutedEventArgs e)
        {
            SetVersionUpdateService(((CheckBox)sender).IsChecked);
        }

        private void SetVersionUpdateService(bool? IsDefaultSet)
        {
            if (IsDefaultSet == true)
            {
                stkPnlVersionUpdate.Visibility = Visibility.Visible;
            }
            else
                stkPnlVersionUpdate.Visibility = Visibility.Collapsed;
        }

        private void RadioButton_Checked(object sender, RoutedEventArgs e)
        {

        }
        private void NumericOnly(System.Object sender, System.Windows.Input.TextCompositionEventArgs e)
        {
            e.Handled = IsTextNumeric(e.Text);
        }
        private static bool IsTextNumeric(string str)
        {
            System.Text.RegularExpressions.Regex reg = new System.Text.RegularExpressions.Regex("[^0-9]");
            return reg.IsMatch(str);
        }
    }
}
